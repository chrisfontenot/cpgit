This solution is designed to be build with Visual Studio 2015. It will not work with earlier versions
so please use at least Visual Studio 2015 to build the solution. Once the DLL files are built, they are
put into the "bin" directory here.

You can use gacutil to install the files into the GAC. They are signed and strong-named. Or, you can
simply reference the DLL files within the main build. Either way will work.

Al Longyear
16 October 2015
