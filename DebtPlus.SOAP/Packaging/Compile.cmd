@REM Add the path to the programs
@set path=%path%;"%ProgramFiles(x86)%\WiX Toolset v3.9\bin";"%ProgramFiles(x86)%\WiX Toolset v3.8\bin"

@REM Compile the files to the intermediate objects
candle -nologo -arch x64 -ext WixUIExtension -ext WixUtilExtension -ext WixNetFxExtension -dNgenPlatform="64bit" DebtPlus.SOAP.wxs 

@REM Link the intermediate objects to the MSI file
light  -nologo -spdb -ext WixUtilExtension -ext WixNetFxExtension -out "DebtPlus.SOAP.x64.msi" DebtPlus.SOAP.wixobj

@if ERRORLEVEL 1 pause "Something happend that was not supposed to happen..."

@DELETE THE WORKING FILE
@DEL DebtPlus.SOAP.wixobj
