﻿namespace DebtPlus.SOAP.HPF
{
    partial class Communication
    {
        /// <summary>
        /// Perform the search for the applicant
        /// </summary>
        /// <param name="req">Pointer to the request structure for the search</param>
        public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchResponse AgencySearch(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchRequest reqOp)
        {
            // Generate the proxy request
            using (var pxy = new DebtPlus.SOAP.HPF.Agency.AgencyWebService())
            {
                // Generate the authentication information for the request
                var ai = new DebtPlus.SOAP.HPF.Agency.AuthenticationInfo()
                {
                    UserName = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyUserName,
                    Password = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyPassword,
                    Usertype = DebtPlus.SOAP.HPF.Agency.UserType.WS
                };
                pxy.AuthenticationInfoValue = ai;
                // pxy.PreAuthenticate = true;

                // Ignore invalid SSL certs
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                // Finally, do the request
                return pxy.SearchForeclosureCase(reqOp);
            }
        }
    }
}
