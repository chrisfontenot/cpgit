﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF.Agency
{
    partial class ForeclosureCaseWSDTO
    {
        /// <summary>
        /// Convert the foreclosure case request to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(ForeclosureCaseWSDTO), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }

        /// <summary>
        /// Formatted display name. Used in the result grid.
        /// </summary>
        public string DISPLAY_name
        {
            get
            {
                string firstName = BorrowerFname ?? string.Empty;
                string lastName = BorrowerLname ?? string.Empty;

                if (firstName != string.Empty && lastName != string.Empty)
                {
                    return firstName + " " + lastName;
                }
                return firstName + lastName;
            }
        }

        /// <summary>
        /// Formatted display case date. Used in the results grid.
        /// </summary>
        public DateTime? DISPLAY_case_date
        {
            get
            {
                return CompletedDt != null ? CompletedDt : intakeDtField;
            }
        }

        /// <summary>
        /// Formatted address. Used in the results grid.
        /// </summary>
        public string address
        {
            get
            {
                var sb = new System.Text.StringBuilder();
                foreach (string str in new string[] { propCityField, propStateCdField, propZipField })
                {
                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        sb.AppendFormat(" {0}", str.Trim());
                    }
                }

                var cityStateZip = sb.ToString().Trim();
                sb.Clear();

                foreach (string str in new string[] { propAddr1Field, propAddr2Field, cityStateZip })
                {
                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        sb.AppendFormat("{0}{1}", System.Environment.NewLine, str.Trim());
                    }
                }

                // Toss the leading cr/lf
                if (sb.Length > 1)
                {
                    sb.Remove(0, 2);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Formatted address. Used in the results grid.
        /// </summary>
        public string DISPLAY_address
        {
            get
            {
                return address.Replace(Environment.NewLine, " ");
            }
        }

        public string DISPLAY_counselor
        {
            get
            {
                string counselorName = counselorFullNameField ?? string.Empty;
                string counselorPhone = counselorPhoneField ?? string.Empty;
                string counselorExt = counselorExtField ?? string.Empty;

                // Join the extension to the phone
                if (counselorExtField != string.Empty)
                {
                    counselorPhone = counselorPhone + " ext " + counselorExt;
                }

                // Join the phone to the name
                if (counselorPhone != string.Empty)
                {
                    counselorName = counselorName + " (" + counselorPhone + ")";
                }

                return counselorName;
            }
        }
    }
}
