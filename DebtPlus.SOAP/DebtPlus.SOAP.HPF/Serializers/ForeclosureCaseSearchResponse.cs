﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF.Agency
{
    partial class ForeclosureCaseSearchResponse
    {
        /// <summary>
        /// Convert the foreclosure case response to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                using (var txt = new DebtPlus.SOAP.HPF.Agency.StringWriterWithEncoding(sb, Encoding.UTF8))
                {
                    System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                    XmlWriter.Formatting = System.Xml.Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = false;

                    XmlWriter.WriteStartElement("ForeclosureCaseSearchResponse", string.Empty);
                    {
                        XmlWriter.WriteElementString("Status", Status.ToString());
                        XmlWriter.WriteElementString("SearchResultCount", SearchResultCount.ToString());
                        XmlWriter.WriteStartElement("Results");

                        for (int indx = 0; indx <= this.Messages.GetUpperBound(0); ++indx)
                        {
                            var rslt = this.Results[indx];
                            XmlWriter.WriteStartElement("Result");

                            if (rslt.AgencyCaseNum != null)        { XmlWriter.WriteElementString("AgencyCaseNum", rslt.AgencyCaseNum.ToString()); }
                            if (rslt.AgencyClientNum != null)      { XmlWriter.WriteElementString("AgencyClientNum", rslt.AgencyClientNum.ToString()); }
                            if (rslt.AgencyName != null)           { XmlWriter.WriteElementString("AgencyName", rslt.AgencyName.ToString()); }
                            if (rslt.BankruptcyInd != null)        { XmlWriter.WriteElementString("BankruptcyInd", rslt.BankruptcyInd.ToString()); }
                            if (rslt.BankruptcyLoaDt != null)      { XmlWriter.WriteElementString("BankruptcyLoaDt", rslt.BankruptcyLoaDt.ToString()); }
                            if (rslt.BorrowerFname != null)        { XmlWriter.WriteElementString("BorrowerFname", rslt.BorrowerFname.ToString()); }
                            if (rslt.BorrowerLast4SSN != null)     { XmlWriter.WriteElementString("BorrowerLast4SSN", rslt.BorrowerLast4SSN.ToString()); }
                            if (rslt.BorrowerLname != null)        { XmlWriter.WriteElementString("BorrowerLname", rslt.BorrowerLname.ToString()); }
                            if (rslt.CampaignId != null)           { XmlWriter.WriteElementString("CampaignId", rslt.CampaignId.ToString()); }
                            if (rslt.CoBorrowerFname != null)      { XmlWriter.WriteElementString("CoBorrowerFname", rslt.CoBorrowerFname.ToString()); }
                            if (rslt.CoBorrowerLname != null)      { XmlWriter.WriteElementString("CoBorrowerLname", rslt.CoBorrowerLname.ToString()); }
                            if (rslt.CompletedDt != null)          { XmlWriter.WriteElementString("CompletedDt", rslt.CompletedDt.ToString()); }
                            if (rslt.Counseled != null)            { XmlWriter.WriteElementString("Counseled", rslt.Counseled.ToString()); }
                            if (rslt.CounselorEmail != null)       { XmlWriter.WriteElementString("CounselorEmail", rslt.CounselorEmail.ToString()); }
                            if (rslt.CounselorExt != null)         { XmlWriter.WriteElementString("CounselorExt", rslt.CounselorExt.ToString()); }
                            if (rslt.CounselorFullName != null)    { XmlWriter.WriteElementString("CounselorFullName", rslt.CounselorFullName.ToString()); }
                            if (rslt.CounselorPhone != null)       { XmlWriter.WriteElementString("CounselorPhone", rslt.CounselorPhone.ToString()); }
                            if (rslt.DelinquentCd != null)         { XmlWriter.WriteElementString("DelinquentCd", rslt.DelinquentCd.ToString()); }
                            if (rslt.DocPrepInd != null)           { XmlWriter.WriteElementString("DocPrepInd", rslt.DocPrepInd.ToString()); }
                            if (rslt.DocPrepInitInd != null)       { XmlWriter.WriteElementString("DocPrepInitInd", rslt.DocPrepInitInd.ToString()); }
                            if (rslt.FcId != null)                 { XmlWriter.WriteElementString("FcId", rslt.FcId.ToString()); }
                            if (rslt.FcNoticeReceivedInd != null)  { XmlWriter.WriteElementString("FcNoticeReceivedInd", rslt.FcNoticeReceivedInd.ToString()); }
                            if (rslt.IntakeDt != null)             { XmlWriter.WriteElementString("IntakeDt", rslt.IntakeDt.ToString()); }
                            if (rslt.LoanNumber != null)           { XmlWriter.WriteElementString("LoanNumber", rslt.LoanNumber.ToString()); }
                            if (rslt.LoanServicer != null)         { XmlWriter.WriteElementString("LoanServicer", rslt.LoanServicer.ToString()); }
                            if (rslt.ProgramId != null)            { XmlWriter.WriteElementString("ProgramId", rslt.ProgramId.ToString()); }
                            if (rslt.ProgramName != null)          { XmlWriter.WriteElementString("ProgramName", rslt.ProgramName.ToString()); }
                            if (rslt.PropAddr1 != null)            { XmlWriter.WriteElementString("PropAddr1", rslt.PropAddr1.ToString()); }
                            if (rslt.PropAddr2 != null)            { XmlWriter.WriteElementString("PropAddr2", rslt.PropAddr2.ToString()); }
                            if (rslt.PropCity != null)             { XmlWriter.WriteElementString("PropCity", rslt.PropCity.ToString()); }
                            if (rslt.PropStateCd != null)          { XmlWriter.WriteElementString("PropStateCd", rslt.PropStateCd.ToString()); }
                            if (rslt.PropZip != null)              { XmlWriter.WriteElementString("PropZip", rslt.PropZip.ToString()); }
                            if (rslt.ReferralClientNum != null)    { XmlWriter.WriteElementString("ReferralClientNum", rslt.ReferralClientNum.ToString()); }
                            if (rslt.SponsorId != null)            { XmlWriter.WriteElementString("SponsorId", rslt.SponsorId.ToString()); }
                            if (rslt.SponsorLoanNum != null)       { XmlWriter.WriteElementString("SponsorLoanNum", rslt.SponsorLoanNum.ToString()); }
                            if (rslt.SubprogramCd != null)         { XmlWriter.WriteElementString("SubprogramCd", rslt.SubprogramCd.ToString()); }
                            if (rslt.SummarySentDate != null)      { XmlWriter.WriteElementString("SummarySentDate", rslt.SummarySentDate.ToString()); }
                            if (rslt.SummarySentOtherCode != null) { XmlWriter.WriteElementString("SummarySentOtherCode", rslt.SummarySentOtherCode.ToString()); }
                            if (rslt.SummarySentOtherDate != null) { XmlWriter.WriteElementString("SummarySentOtherDate", rslt.SummarySentOtherDate.ToString()); }
                            if (rslt.UsesEventsInd != null)        { XmlWriter.WriteElementString("UsesEventsInd", rslt.UsesEventsInd.ToString()); }

                            XmlWriter.WriteEndElement();
                        }
                        XmlWriter.WriteEndElement();

                        XmlWriter.WriteStartElement("messages");
                        for (int indx = 0; indx <= this.Messages.GetUpperBound(0); ++indx)
                        {
                            var msg = this.Messages[indx];
                            XmlWriter.WriteStartElement("message");
                            XmlWriter.WriteElementString("errorCode", msg.ErrorCode.ToString());
                            XmlWriter.WriteElementString("messageField", msg.Message);
                            XmlWriter.WriteEndElement();
                        }
                        XmlWriter.WriteEndElement();
                    }
                    XmlWriter.WriteEndElement();

                    txt.Flush();
                    txt.Close();
                }
                fs.Flush();
                fs.Close();
            }

            return sb.ToString();
        }
    }
}
