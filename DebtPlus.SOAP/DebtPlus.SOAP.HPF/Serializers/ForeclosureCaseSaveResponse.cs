﻿using System.Text;

namespace DebtPlus.SOAP.HPF.Agency
{
    partial class ForeclosureCaseSaveResponse
    {
        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("\r\nCompletion Status: {0}", Status.ToString());
            if (this.FcId.HasValue)
            {
                sb.AppendFormat("\r\nFCID: {0}", this.FcId.Value);
            }

            if (this.CompletedDt.HasValue)
            {
                sb.AppendFormat("\r\nCompleted Date: {0:d}", this.CompletedDt.Value);
            }

            if (this.Messages.Length > 0)
            {
                sb.AppendFormat("\r\n\r\nMessages:");
                foreach (var msg in this.Messages)
                {
                    sb.AppendFormat("\r\n* {0}", msg.Message);
                }
            }

            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Serialize the current structure to an XML frame so that it may be recorded in the database
        /// </summary>
        /// <returns>The resulting string equivalent of the structure</returns>
        public string Serialize()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
                {
                    System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                    XmlWriter.Formatting = System.Xml.Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = false;

                    XmlWriter.WriteStartElement("ForeclosureCaseSaveResponse", string.Empty);
                    {
                        XmlWriter.WriteElementString("statusField", Status.ToString());

                        XmlWriter.WriteStartElement("fcIdField");
                        if (this.fcIdField.HasValue) XmlWriter.WriteString(fcIdField.Value.ToString());
                        XmlWriter.WriteEndElement();

                        XmlWriter.WriteStartElement("completedDtField");
                        if (this.completedDtField.HasValue) XmlWriter.WriteString(completedDtField.Value.ToString());
                        XmlWriter.WriteEndElement();

                        XmlWriter.WriteStartElement("messagesField");
                        if (this.Messages != null)
                        {
                            for (int indx = 0; indx <= this.Messages.GetUpperBound(0); ++indx)
                            {
                                var msg = this.Messages[indx];
                                XmlWriter.WriteStartElement("message");
                                XmlWriter.WriteElementString("errorCodeField", msg.ErrorCode.ToString());
                                XmlWriter.WriteElementString("messageField", msg.Message);
                                XmlWriter.WriteEndElement();
                            }
                        }
                        XmlWriter.WriteEndElement();
                    }
                    XmlWriter.WriteEndElement();

                    txt.Flush();
                    txt.Close();
                }
                fs.Flush();
                fs.Close();
            }

            return sb.ToString();
        }
    }
}

namespace DebtPlus.SOAP.HPF.Agency
{
    internal class StringWriterWithEncoding : System.IO.StringWriter
    {
        private Encoding m_encoding;

        public StringWriterWithEncoding()
            : base()
        {
        }

        public StringWriterWithEncoding(StringBuilder sb, Encoding encoding)
            : base(sb)
        {
            m_encoding = encoding;
        }

        public override Encoding Encoding
        {
            get
            {
                return m_encoding;
            }
        }
    }
}
