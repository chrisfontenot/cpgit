﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF.Agency
{
    partial class SessionDTO
    {
        /// <summary>
        /// Convert the foreclosure case request to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(SessionDTO), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();
            lines.RemoveAt(0);                              // Discard the <?xml ...> line since we can't change the encoding
            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }
    }

    public partial class SessionSaveResponse
    {
        /// <summary>
        /// Convert the foreclosure case response to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public string Serialize()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
                {
                    System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                    XmlWriter.Formatting = System.Xml.Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = false;

                    XmlWriter.WriteStartElement("SessionSaveResponse", string.Empty);
                    {
                        XmlWriter.WriteElementString("statusField", Status.ToString());

                        XmlWriter.WriteStartElement("messagesField");
                        for (int indx = 0; indx <= this.Messages.GetUpperBound(0); ++indx)
                        {
                            var msg = this.Messages[indx];
                            XmlWriter.WriteStartElement("message");
                            XmlWriter.WriteElementString("errorCodeField", msg.ErrorCode.ToString());
                            XmlWriter.WriteElementString("messageField", msg.Message);
                            XmlWriter.WriteEndElement();
                        }
                        XmlWriter.WriteEndElement();
                    }
                    XmlWriter.WriteEndElement();

                    txt.Flush();
                    txt.Close();
                }
                fs.Flush();
                fs.Close();
            }

            return sb.ToString();
        }
    }
}
