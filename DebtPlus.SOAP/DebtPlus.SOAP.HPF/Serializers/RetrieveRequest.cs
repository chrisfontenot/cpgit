﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF.CallLog
{
    /// <summary>
    /// Class to hold the arguments for the Retrieve request. Some values are defined.
    /// Not all information needs to be specified. The more that you specify, the narrower
    /// the Retrieve operation is.
    /// </summary>
    public class RetrieveRequest : IDisposable
    {
        public RetrieveRequest()
        {
        }

        public string ICTCallId { get; set; }

        /// <summary>
        /// Do we have enough information to perform the Retrieve operation?
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(ICTCallId);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            ICTCallId = null;
        }

        /// <summary>
        /// Convert the foreclosure case request to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(RetrieveRequest), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }
    }
}
