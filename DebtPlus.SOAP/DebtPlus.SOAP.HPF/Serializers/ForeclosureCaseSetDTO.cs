﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF.Agency
{
    partial class ForeclosureCaseSetDTO
    {
        /// <summary>
        /// Convert the foreclosure case request to a suitable string for storing in the database. This is an XML document.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(ForeclosureCaseSetDTO), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();
            lines.RemoveAt(0);                              // Discard the <?xml ...> line since we can't change the encoding
            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }
    }
}
