﻿namespace DebtPlus.SOAP.HPF
{
    partial class Communication
    {
        /// <summary>
        /// Retrieve the call log information
        /// </summary>
        /// <param name="req">Pointer to the request structure for the search</param>
        public DebtPlus.SOAP.HPF.Agency.CallLogSearchResponse SearchCallLog(DebtPlus.SOAP.HPF.Agency.CallLogSearchRequest reqOp)
        {
            // Generate the proxy request
            using (var pxy = new DebtPlus.SOAP.HPF.Agency.AgencyWebService())
            {
                // Generate the authentication information for the request
                var ai = new DebtPlus.SOAP.HPF.Agency.AuthenticationInfo()
                {
                    UserName = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyUserName,
                    Password = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyPassword,
                    Usertype = DebtPlus.SOAP.HPF.Agency.UserType.WS
                };
                pxy.AuthenticationInfoValue = ai;
                pxy.PreAuthenticate         = true;

                // Ignore invalid SSL certs
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                // Finally, do the request
                return pxy.SearchCallLog(reqOp);
            }
        }

        /// <summary>
        /// Retrieve the call log information
        /// </summary>
        /// <param name="req">Pointer to the request structure for the search</param>
        public DebtPlus.SOAP.HPF.Agency.CallLogRetrieveResponse RetrieveCallLog(DebtPlus.SOAP.HPF.Agency.CallLogRetrieveRequest reqOp)
        {
            // Generate the proxy request
            using (var pxy = new DebtPlus.SOAP.HPF.Agency.AgencyWebService())
            {
                // Generate the authentication information for the request
                var ai = new DebtPlus.SOAP.HPF.Agency.AuthenticationInfo()
                {
                    UserName = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyUserName,
                    Password = Properties.Settings.Default.DebtPlus_SOAP_HPF_Agency_AgencyPassword,
                    Usertype = DebtPlus.SOAP.HPF.Agency.UserType.WS
                };
                pxy.AuthenticationInfoValue = ai;
                pxy.PreAuthenticate         = true;

                // Ignore invalid SSL certs
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                // Finally, do the request
                return pxy.RetrieveCallLog(reqOp);
            }
        }
    }
}
