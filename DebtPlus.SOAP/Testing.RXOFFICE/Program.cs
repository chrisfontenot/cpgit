﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing.RXOFFICE
{
    class Program
    {
        static void Main(string[] args)
        {
            var info = new DebtPlus.SOAP.RXOFFICE.RxOffice1.CaseInfo();
            info.Header = GetHeaderInformation();
            info.Details = GetLoanInformation();

            var client = new DebtPlus.SOAP.RXOFFICE.RxOffice1.RxImportV1_1();
            try
            {
                var serviceResult = client.RxLoadNewCase(info);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.CaseInfoHeader GetHeaderInformation()
        {
            var header = new DebtPlus.SOAP.RXOFFICE.RxOffice1.CaseInfoHeader()
            {
                HostSourceID = "cccsat",
                LoginID = "cccsatuser",
                SourceID = "cccsat",
                Password = "qIuVj1yyjcoW5W/fwBTUtg==",
                RecordCount = 1,
                RecordCountSpecified = true,
                TransactionID = "99"
            };
            return header;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformation getLoanDetails()
        {
            var detail = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformation();
            detail.AgencyInformation = getAgencyInformation();
            detail.Balance = 0.0;
            detail.BalanceSpecified = false;
            detail.Borrower = getBorrower();
            detail.BorrowerMailAddress = getBorrowerMailAddress();
            detail.CaseID = "0000000";
            detail.CaseNextAction = string.Empty;
            detail.CaseStatusCode = string.Empty;
            detail.CaseStatusSubCode = string.Empty;
            detail.CaseType = DebtPlus.SOAP.RXOFFICE.RxOffice1.CaseTypeEnumType.frmdocmgmt;
            detail.CategoryOfMedianIncome = string.Empty;
            detail.CertificateNumber = string.Empty;
            detail.CloseReason = getCloseReason();
            detail.CoBorrower = getCoBorrower();
            detail.Compansation1 = string.Empty;
            detail.Compansation2 = string.Empty;
            detail.Compansation3 = string.Empty;
            detail.Conversation = getConversion();
            detail.CounselorInformation = getCounselorInformation();
            detail.DelinquencyAmount = 0.0;
            detail.DelinquencyAmountSpecified = false;
            detail.DesignatedUser = string.Empty;
            detail.Documents = getDocuments();
            detail.DueDate = string.Empty;
            detail.EscrowAmount = 0.0;
            detail.EscrowAmountSpecified = false;
            detail.ExternalUniqueIdentifier = string.Empty;
            detail.FileNumber = string.Empty;
            detail.Financials = getFinancials();
            detail.FirstMortgageCompany = string.Empty;
            detail.FirstMortgageLoanNumber = string.Empty;
            detail.GoodThroughDate = string.Empty;
            detail.Hardship = string.Empty;
            detail.HardshipInformation = getHardship();
            detail.IfForeclosure = string.Empty;
            detail.ImportFromFNMAFlag = string.Empty;
            detail.InstallmentAmount = 0.0;
            detail.InstallmentAmountSpecified = false;
            detail.InsurerCompanyID = string.Empty;
            detail.InsurerName = string.Empty;
            detail.InterestRate = 0.0;
            detail.InterestRateSpecified = false;
            detail.InvestorLoanNumber = string.Empty;
            detail.InvestorName = string.Empty;
            detail.InvestorSourceID = string.Empty;
            detail.LawFirm = string.Empty;
            detail.LawFirmID = string.Empty;
            detail.LiensInformation = getLiens();
            detail.LoanNumber = string.Empty;
            detail.LoanType = string.Empty;
            detail.MessageInformation = getMessageInformation();
            detail.ModifiedInLastSixMonths = string.Empty;
            detail.MonthlyLateFee = 0.0;
            detail.MonthlyLateFeeSpecified = false;
            detail.MonthlyPayment = 0.0;
            detail.MonthlyPaymentSpecified = false;
            detail.MonthsBehind = 0;
            detail.MonthsBehindSpecified = false;
            detail.MortgageCompanySourceId = string.Empty;
            detail.Notes = getNotes();
            detail.OriginalBalance = 0;
            detail.OriginalBalanceSpecified = false;
            detail.OriginalInterestRate = 0;
            detail.OriginalInterestRateSpecified = false;
            detail.OriginalLoan = string.Empty;
            detail.OriginalMonthlyPayment = 0;
            detail.OriginalMonthlyPaymentSpecified = false;
            detail.OriginatingMortgageCompany = string.Empty;
            detail.PartiesInformation = getParties();
            detail.PropertyInformation = getProperty();
            detail.PropertyValuation = getPropertyValuation();
            detail.ReinstatementAmount = 0;
            detail.ReinstatementAmountSpecified = false;
            detail.SaleDate = string.Empty;
            detail.SourceDetails = getSource();
            detail.SuspenseAmount = 0;
            detail.SuspenseAmountSpecified = false;
            detail.Tasks = getTasks();
            detail.ThirdPartyAuthorization = string.Empty;
            detail.TimeZone = string.Empty;
            detail.Workouts = getWorkouts();

            return detail;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.AgencyInformationType getAgencyInformation()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.AgencyInformationType();
            item.Comments = string.Empty;
            item.CounselingMode = string.Empty;
            item.CounselingOutcome = string.Empty;
            item.CounselingOutcomeDate = string.Empty;
            item.DesignatedUser = string.Empty;
            item.GroupCounselingHrs = string.Empty;
            item.IndividualCounselingHrs = string.Empty;
            item.InternalRef = string.Empty;
            item.LevelOfCounseling = string.Empty;
            item.ReferralSource = string.Empty;

            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLiensLiens[] getLiens()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLiensLiens[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationPartyParty[] getParties()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationPartyParty[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationWorkoutWorkout[] getWorkouts()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationWorkoutWorkout[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.TaskType[] getTasks()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.TaskType[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.DocumentType[] getDocuments()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.DocumentType[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.NoteDetailsType[] getNotes()
        {
            return null;
            //var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.NoteDetailsType[1];
            //return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationMessageInformation getMessageInformation()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationMessageInformation();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.PropertyInformationType getProperty()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.PropertyInformationType();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationPropertyValuation getPropertyValuation()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationPropertyValuation();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationSourceDetails getSource()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationSourceDetails();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformation[] GetLoanInformation()
        {
            DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformation detail = getLoanDetails();
            return new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformation[] { detail };
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ConversationType getConversion()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ConversationType();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationCounselorInformation getCounselorInformation()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationCounselorInformation();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.FinancialType getFinancials()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.FinancialType();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationHardshipInformation getHardship()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationHardshipInformation();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.PrimaryBorrowerDetailsType getBorrower()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.PrimaryBorrowerDetailsType();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.AddressInformationType getBorrowerMailAddress()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.AddressInformationType();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationCloseReason getCloseReason()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.ArrayOfImportCaseLoanInformationLoanInformationCloseReason();
            return item;
        }

        private static DebtPlus.SOAP.RXOFFICE.RxOffice1.BorrowerDetailsType[] getCoBorrower()
        {
            var item = new DebtPlus.SOAP.RXOFFICE.RxOffice1.BorrowerDetailsType();
            return new DebtPlus.SOAP.RXOFFICE.RxOffice1.BorrowerDetailsType[] { item };
        }
    }
}
