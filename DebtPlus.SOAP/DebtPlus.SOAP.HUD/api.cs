namespace DebtPlus.SOAP.HUD
{
    public class api : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            System.Net.WebRequest request = base.GetWebRequest(uri);

            if (PreAuthenticate)
            {
                System.Net.NetworkCredential networkCredentials = Credentials.GetCredential(uri, "Basic");
                if (networkCredentials != null)
                {
                    byte[] credentialBuffer = (new System.Text.UTF8Encoding()).GetBytes(networkCredentials.UserName + ":" + networkCredentials.Password);
                    request.Headers["Authorization"] = "Basic " + System.Convert.ToBase64String(credentialBuffer);
                }
                else
                {
                    throw new System.ApplicationException("No network credentials");
                }
            }

            return request;
        }
    }
}
