﻿namespace DebtPlus.SOAP.HUD
{
    public class ConfigurationData
    {
        private System.Collections.Specialized.NameValueCollection _items = null;

        private System.Collections.Specialized.NameValueCollection items
        {
            get
            {
                try
                {
                    if (_items == null)
                    {
                        _items = System.Configuration.ConfigurationManager.GetSection("extract.arm.v4") as System.Collections.Specialized.NameValueCollection;
                    }
                }
                catch { }
                return _items;
            }
        }

        /// <summary>
        /// Web service URL
        /// </summary>
        public string WebService
        {
            get
            {
                try
                {
                    if (items != null)
                    {
                        return items["URL"];
                    }
                }
                catch { }
                return "https://www5.hud.gov/ARM/ARM";
            }
        }

        /// <summary>
        /// Web service Content Management System ID
        /// </summary>
        public string CMSID
        {
            get
            {
                try
                {
                    if (items != null)
                    {
                        return items["CMSID"];
                    }
                }
                catch { }
                return "29";
            }
        }
    }
}