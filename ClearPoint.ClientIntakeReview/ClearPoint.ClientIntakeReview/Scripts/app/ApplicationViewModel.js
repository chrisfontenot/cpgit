﻿var ApplicationViewModel = function (data) {
    var self = this;
    self.User = ko.observable(data.webUserData);
    self.Application = ko.observable(data.applicationData);
    self.ApplicationDescr = ko.observable(data.applicationDescrData);
    self.Address = ko.observable(data.addressData);
    self.Applicant_Creditor = ko.observable(data.creditorData);
    self.Expenses = ko.observable(data.applicantExpenseData);
    self.TotalExpenses = ko.observable(data.totalExpenses);
    self.CoapplicantPresent = ko.observable(data.coapplicantPresent);

};



$(function () {
    var instance = null;
    $.ajax({
        type: "GET",
        url: RequestURL,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            instance = new ApplicationViewModel(data);

            ko.applyBindings(instance);
        },
        error: function (error) {
            alert(error.status + "<--and--> " + error.statusText);
        }
    });
});
