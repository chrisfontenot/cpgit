﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm.aspx.cs" Inherits="ClearPoint.ClientIntakeReview.Asp.WebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<script runat="server">

        void ReviewPage_Click(object sender, EventArgs e)
    {
        // Do some other processing...

        ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('http://localhost:61519/Intake/Application?appId=" + 191784 + "');", true);
        //Page.RegisterStartupScript("test", sb.ToString());
    }

</script>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Long Form</h1>
            <asp:Button OnClick="ReviewPage_Click" Text="Review application" runat="server" />
        </div>
    </form>
</body>
</html>
