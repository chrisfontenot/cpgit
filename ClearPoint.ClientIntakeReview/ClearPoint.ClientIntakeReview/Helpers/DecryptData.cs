﻿using System;
using ASPENCRYPTLib;

namespace ClearPoint.ClientIntakeReview.Helpers
{
    public class DecryptData
    {
        public string Decrypt(Object keyString, Object cipherText)
        {
            if (cipherText != null)
            {
                ICryptoManager objCm = new CryptoManager();

                ICryptoContext objContext = objCm.OpenContext("", 1);


                ICryptoKey exchangeKey = objContext.CreateExponentOneKey();

                ICryptoBlob keyBlob = objCm.CreateBlob();
                keyBlob.Base64 = keyString.ToString();
                ICryptoKey key = objContext.ImportKeyFromBlob(exchangeKey, keyBlob, (CryptoBlobTypes) 1);
                key.EffectiveLength = 40;
                ICryptoBlob blob = objCm.CreateBlob();
                blob.Base64 = cipherText.ToString();

                return key.DecryptText(blob);
            }
            return string.Empty;
            /*
            String strClearText = "";
            if (cipherText != null)
            {
                ICryptoManager objCm = new CryptoManager();

                ICryptoContext objContext = objCm.OpenContext("", 1);


                ICryptoKey exchangeKey = objContext.CreateExponentOneKey();

                ICryptoBlob keyBlob = objCm.CreateBlob();
                keyBlob.Base64 = keyString.ToString();

                ICryptoKey key = objContext.ImportKeyFromBlob(exchangeKey, keyBlob, (CryptoBlobTypes) 1);
                key.EffectiveLength = 40;


                ICryptoBlob blob = objCm.CreateBlob();
                blob.Base64 = cipherText.ToString();

                strClearText = key.DecryptText(blob);
            }
            return strClearText;
             */
        }

        public string DecryptLongFormData(object keyString, object cipherText)
        {
            string clearTextKey = Decrypt(
                "AQIAAAJmAAAApAAAmemqU0fx5+TFBMB3qxlYsgBbvtZo1W9GQ4p0ohcpeC5gBBn7efXsy1KJU4Hu xN8trRalV7XF12+5NxvaD4UCAA==",
                keyString);

            return Decrypt(clearTextKey, cipherText);

            /*
             *  var clearTextKey = Decrypt(
                "AQIAAAJmAAAApAAAmemqU0fx5+TFBMB3qxlYsgBbvtZo1W9GQ4p0ohcpeC5gBBn7efXsy1KJU4Hu xN8trRalV7XF12+5NxvaD4UCAA==",
                keyString);

            string decryptedData = Decrypt(clearTextKey, cipherText);
            return decryptedData;
             */
        }
    }
}