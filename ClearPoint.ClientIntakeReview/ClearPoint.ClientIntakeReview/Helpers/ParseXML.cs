﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using ClearPoint.ClientIntakeReview.Models;

namespace ClearPoint.ClientIntakeReview.Helpers
{
    // ReSharper disable InconsistentNaming
    public class ParseXML
        // ReSharper restore InconsistentNaming
    {
        public static T ParseData<T>(string xmlResponse) where T : new()
        {
            var tObject = new T();
            if (xmlResponse != "")
            {
                var document = new XmlDocument();
                document.LoadXml(xmlResponse);
                //Gets all the tags with tag name row
                XmlNodeList nodeList = document.GetElementsByTagName(new T().GetType().Name.ToLower());
                //Loop through each and every node

                foreach (XmlNode xmlAtr in nodeList[0].ChildNodes)
                {
                    tObject.GetType().GetProperty(xmlAtr.Name).SetValue(tObject, xmlAtr.InnerText);
                }
            }
            return tObject;
        }

        /// <summary>
        ///     Special method to process Expense only because of the wierd xml.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlResponse"></param>
        /// <param name="expenseTypes"></param>
        /// <returns></returns>
        public static List<T> ParseExpenseData<T>(string xmlResponse, IQueryable<expense_types> expenseTypes)
            where T : new()
        {
            var tObjects = new List<T>();
            var document = new XmlDocument();
            if (xmlResponse != "")
            {
                document.LoadXml(xmlResponse);
                //Gets all the tags with tag name row
                XmlNodeList nodeList = document.GetElementsByTagName(new T().GetType().Name.ToLower());
             
                //Loop through each and every node
                foreach (XmlNode node in nodeList)
                {
                    var tObject = new T();
                    if (node.Attributes != null)
                    {
                        foreach (XmlNode xmlAtr in node.Attributes)
                        {
                            short propertyName = Convert.ToInt16(xmlAtr.InnerText);

                            tObject.GetType().GetProperty(xmlAtr.Name).SetValue(tObject, xmlAtr.InnerText, null);
                            tObject.GetType()
                                   .GetProperty("Name")
                                   .SetValue(tObject,
                                             expenseTypes.Where(a => a.expense_type_id.Equals(propertyName))
                                                         .Select(a => a.description).FirstOrDefault(), null);
                        }
                    }
                    tObject.GetType().GetProperty("Value").SetValue(tObject, node.InnerText, null);
                    tObjects.Add(tObject);
                }
                //now nameList has your required values.

            }
            return tObjects;
        }
    }
}