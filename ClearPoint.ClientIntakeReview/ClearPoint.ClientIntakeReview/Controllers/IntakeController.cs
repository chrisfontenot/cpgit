﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClearPoint.ClientIntakeReview.Controllers
{
    public class IntakeController : Controller
    {
        //
        // GET: /Client/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Application(string appId)
        {
            ViewBag.AppId = appId;
            return View("Application");
        }

    }
}
