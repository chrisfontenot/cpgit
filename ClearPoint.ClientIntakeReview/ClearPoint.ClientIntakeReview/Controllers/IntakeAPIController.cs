﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web.Http;
using ClearPoint.ClientIntakeReview.Helpers;
using ClearPoint.ClientIntakeReview.Models;

namespace ClearPoint.ClientIntakeReview.Controllers
{
    public class IntakeAPIController : ApiController
    {
        private static readonly cccs_webContext db;
        private static readonly string encryptionKey;

        private int appId;
        private Guid visitor_id;
        public static DecryptData decryptData;

        static IntakeAPIController()
        {
            db = new cccs_webContext();
            const short encryptionKeyId = 1;
            encryptionKey = (from e in db.encryption_keys
                             where e.encryption_key_id.Equals(encryptionKeyId)
                             select e.value).FirstOrDefault();

            decryptData = new DecryptData();

        }


        // GET api/TodoList/5
        //[HttpGet]
        public LongFormData GetLongFormData(int appId)
        {
            //appId = 191530;

            //appId = 191784;
            visitor_id = (from s in db.web_applications_statuses
                          where s.application_id.Equals(appId)
                          select s.visitor_id).FirstOrDefault();

            Application applicationData = ApplicationData(appId);
            applicationData.total_monthly_income = Convert.ToInt32(applicationData.coapplicant_monthly_income_net) +
                                                   Convert.ToInt32(applicationData.monthly_income_net) +
                                                   Convert.ToInt32(applicationData.monthly_income_other);
                       
            ApplicationDescr applicationDescrData = ApplicationDescrData(applicationData);
                   

            var longForm = new LongFormData
                {
                    ApplicationData = applicationData,
                    ApplicationDescrData = applicationDescrData,
                    AddressData = AddressData(appId),
                    WebUserData = WebUserData(),
                    CreditorData = CreditorData(appId),
                    ApplicantExpenseData = ApplicantExpenseData(appId)
                   
                };
             foreach(Expense expense in longForm.ApplicantExpenseData)
             {
                longForm.totalExpenses += System.Convert.ToInt32( expense.Value);
             }
             if (applicationData.coapplicant_employer == "" || applicationData.coapplicant_job_title == "" || applicationData.coapplicant_monthly_income_gross == "" || applicationData.coapplicant_monthly_income_net == "")
             {
                 longForm.CoapplicantPresent = false;
                
             }
             else
             {
                 longForm.CoapplicantPresent = true;
             }
            return longForm;
        }

        private Application ApplicationData(int appId)
        {
            string decryptedAppData = decryptData.DecryptLongFormData(encryptionKey, (from a in db.web_applications
                                                                                      where
                                                                                          a.application_id.Equals(
                                                                                              appId)
                                                                                      select a.application_data)
                                                                                         .FirstOrDefault());


            return ParseXML.ParseData<Application>(decryptedAppData);
        }

        private ApplicationDescr ApplicationDescrData(Application application)
        {
            
            var applicationDescr = new ApplicationDescr
            {
                ethnicity_descr = (from e in db.ethnicities
                                    where
                                        SqlFunctions.StringConvert((double) e.ethnicity_id).Trim()
                                                    .Equals(application.ethnicity)
                                    select e.description).FirstOrDefault(),
                marital_status_descr = (from m in db.marital_statuses
                                        where m.marital_status_code.Equals(application.marital_status_code)
                                        select m.description).FirstOrDefault(),
                education_descr = (from e in db.educations
                                    where
                                        SqlFunctions.StringConvert((double) e.education_id).Trim()
                                                    .Equals(application.education)
                                    select e.description).FirstOrDefault(),
                problem_type_descr = (from c in db.credit_problem_types
                                        where
                                            SqlFunctions.StringConvert((double) c.problem_type_id).Trim()
                                                        .Equals(application.problem_type_id)
                                        select c.description).FirstOrDefault(),
                referral_type_descr = (from r in db.referral_types
                                        where
                                            SqlFunctions.StringConvert((double) r.referral_type_id).Trim()
                                                        .Equals(application.referral_type_id)
                                        select r.description).FirstOrDefault(),
                housing_status_descr = (from h in db.housing_statuses
                                        where h.housing_status_code.Equals(application.housing_status_code)
                                        select h.description).FirstOrDefault(),

               
                                       
            };
            return applicationDescr;
        }

        private Address AddressData(int appId)
        {
            string addressData = (from w in db.web_applications
                                  join a in db.web_addresses
                                      on w.address_id equals a.address_id
                                  where w.application_id.Equals(appId)
                                  select a.address_data).FirstOrDefault();

            var address = ParseXML.ParseData<Address>(decryptData.DecryptLongFormData(encryptionKey, addressData));
            address.state_code_descr = (from s in db.states
                                        where s.state_code.Equals(address.state_code)
                                        select s.name).FirstOrDefault();


            return address;
        }

       public User WebUserData()
        {
            string userData = (from w in db.web_users
                               where w.visitor_id.Equals(visitor_id)
                               select w.user_data).FirstOrDefault();

            return ParseXML.ParseData<User>(decryptData.DecryptLongFormData(encryptionKey, userData));
        }


        public List<Applicant_Creditor> CreditorData(int appId)
        {
            List<string> creditorList = (from c in db.web_applicant_creditors
                                         where c.application_id.Equals(appId)
                                         select c.creditor_data).ToList();

            return
                creditorList.Select(
                    creditor =>
                    ParseXML.ParseData<Applicant_Creditor>(decryptData.DecryptLongFormData(encryptionKey, creditor)))
                            .ToList();

        }

        private List<Expense> ApplicantExpenseData(int appId)
        {
            IQueryable<expense_types> expenseTypes = (from d in db.expense_types
                                                      select d);
            string expenseData = (from e in db.web_applicant_expenses
                                  where e.application_id.Equals(appId)
                                  select e.expenses_data).FirstOrDefault();
            return
                ParseXML.ParseExpenseData<Expense>(decryptData.DecryptLongFormData(encryptionKey,
                                                                                   expenseData), expenseTypes);
        }

        public class Address
        {
            public string line_1 { get; set; }
            public string line_2 { get; set; }
            public string city { get; set; }
            public string state_code { get; set; }
            public string zip_code { get; set; }
            public string state_code_descr { get; set; }
        }

        public class Applicant_Creditor
        {
            public string creditor_name { get; set; }
            public string defined_creditor_id { get; set; }
            public string account_number { get; set; }
            public string current_balance { get; set; }
            public string monthly_payment { get; set; }
            public string num_months_past_due { get; set; }
            public string interest_rate { get; set; }
        }

        public class Application
        {
            public string ssn { get; set; }
            public string birth_date { get; set; }
            public string marital_status_code { get; set; }

            public string num_dependents { get; set; }
            public string ethnicity { get; set; }
            public string education { get; set; }
            public string problem_type_id { get; set; }
            public string other_reason { get; set; }
            public string referral_type_id { get; set; }
            public string telephone_home { get; set; }
            public string telephone_work { get; set; }
            public string fax_number { get; set; }
            public string housing_status_code { get; set; }
            public string housing_payments_current { get; set; }
            public string housing_agreement { get; set; }
            public string employer { get; set; }
            public string job_title { get; set; }
            public string monthly_income_gross { get; set; }
            public string monthly_income_net { get; set; }
            public string monthly_income_other { get; set; }
            public string coapplicant_employer { get; set; }
            public string coapplicant_job_title { get; set; }
            public string coapplicant_monthly_income_gross { get; set; }
            public string coapplicant_monthly_income_net { get; set; }
            public int total_monthly_income { get; set; }
            public string unpaid_judgements { get; set; }
            public string wages_garnished { get; set; }
            public string accounts_with_attorney { get; set; }
            public string filed_bankruptcy { get; set; }
            public string credit_report_authorized { get; set; }
            public string preferred_method_contact { get; set; }
            public string comments { get; set; }

            public bool coapplicant {get;set;}
        }

        public class ApplicationDescr
        {
            public string marital_status_descr { get; set; }
            public string ethnicity_descr { get; set; }
            public string education_descr { get; set; }
            public string problem_type_descr { get; set; }
            public string referral_type_descr { get; set; }
            public string housing_status_descr { get; set; }
            public string housing_payments_current_descr { get; set; }
            public string unpaid_judgements_descr { get; set; }
            public string wages_garnished_descr { get; set; }
            public string accounts_with_attorney_descr { get; set; }
            public string filed_bankruptcy_descr { get; set; }
            
        }

        public class Expense
        {
            public string expense_type_id { get; set; }
            public string Value { get; set; }
            public string Name { get; set; }
        }

        public class LongFormData
        {
            public Application ApplicationData { get; set; }
            public ApplicationDescr ApplicationDescrData { get; set; }
            public User WebUserData { get; set; }
            public Address AddressData { get; set; }
            public List<Applicant_Creditor> CreditorData { get; set; }
            public List<Expense> ApplicantExpenseData { get; set; }
            public int totalExpenses { get; set; }
            public bool CoapplicantPresent { get; set; }

        }

        public class User
        {
            public string salutation { get; set; }
            public string first_name { get; set; }
            public string middle_initial { get; set; }
            public string last_name { get; set; }
            public string suffix { get; set; }
            public string email_address { get; set; }
        }
    }
}