using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class creditor_logic
    {
        public creditor_logic()
        {
            this.creditors = new List<creditor>();
        }

        public short logic_type_id { get; set; }
        public string description { get; set; }
        public virtual ICollection<creditor> creditors { get; set; }
    }
}
