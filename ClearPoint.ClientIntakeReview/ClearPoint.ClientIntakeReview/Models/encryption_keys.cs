using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class encryption_keys
    {
        public encryption_keys()
        {
            this.web_users = new List<web_users>();
        }

        public short encryption_key_id { get; set; }
        public string value { get; set; }
        public bool current_key { get; set; }
        public virtual ICollection<web_users> web_users { get; set; }
    }
}
