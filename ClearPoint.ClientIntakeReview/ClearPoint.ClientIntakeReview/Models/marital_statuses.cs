using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class marital_statuses
    {
        public string marital_status_code { get; set; }
        public string description { get; set; }
    }
}
