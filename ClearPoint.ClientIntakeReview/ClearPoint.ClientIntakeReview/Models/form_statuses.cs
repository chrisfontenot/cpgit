using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class form_statuses
    {
        public short form_status_code { get; set; }
        public string form_status { get; set; }
    }
}
