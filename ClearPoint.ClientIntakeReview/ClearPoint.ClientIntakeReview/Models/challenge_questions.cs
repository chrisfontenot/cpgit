using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class challenge_questions
    {
        public challenge_questions()
        {
            this.web_users = new List<web_users>();
        }

        public short challenge_id { get; set; }
        public string question { get; set; }
        public virtual ICollection<web_users> web_users { get; set; }
    }
}
