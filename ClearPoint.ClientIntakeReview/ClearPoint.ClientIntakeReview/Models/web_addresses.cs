using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_addresses
    {
        public web_addresses()
        {
            this.web_applications = new List<web_applications>();
            this.web_applicant_associates = new List<web_applicant_associates>();
        }

        public int address_id { get; set; }
        public string address_data { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public Nullable<int> original_address_id { get; set; }
        public virtual ICollection<web_applications> web_applications { get; set; }
        public virtual ICollection<web_applicant_associates> web_applicant_associates { get; set; }
    }
}
