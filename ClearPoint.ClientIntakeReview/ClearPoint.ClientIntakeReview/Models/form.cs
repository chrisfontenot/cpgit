using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class form
    {
        public form()
        {
            this.form_revision = new List<form_revision>();
        }

        public int form_id { get; set; }
        public string form_name { get; set; }
        public string form_description { get; set; }
        public string state_code { get; set; }
        public int form_type_id { get; set; }
        public virtual ICollection<form_revision> form_revision { get; set; }
        public virtual form_types form_types { get; set; }
        public virtual state state { get; set; }
    }
}
