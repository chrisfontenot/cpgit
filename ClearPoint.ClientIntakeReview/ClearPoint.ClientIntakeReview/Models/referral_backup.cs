using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class referral_backup
    {
        public short referral_type_id { get; set; }
        public string description { get; set; }
    }
}
