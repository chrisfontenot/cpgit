using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class associate_types
    {
        public associate_types()
        {
            this.web_applicant_associates = new List<web_applicant_associates>();
        }

        public short associate_type_id { get; set; }
        public string description { get; set; }
        public virtual ICollection<web_applicant_associates> web_applicant_associates { get; set; }
    }
}
