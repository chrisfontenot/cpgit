using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class article_categories
    {
        public short category_id { get; set; }
        public string category_name { get; set; }
        public string site_description { get; set; }
    }
}
