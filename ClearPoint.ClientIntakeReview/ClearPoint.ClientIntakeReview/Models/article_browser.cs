using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class article_browser
    {
        public int article_id { get; set; }
        public string url { get; set; }
        public short category_id { get; set; }
        public short subcategory_id { get; set; }
        public string title { get; set; }
        public string keywords { get; set; }
        public string contents { get; set; }
        public Nullable<System.DateTime> last_modified_date { get; set; }
    }
}
