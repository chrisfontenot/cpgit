using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class visitor_history_archive
    {
        public System.Guid visitor_id { get; set; }
        public short milestone_type_id { get; set; }
        public System.DateTime milestone_date { get; set; }
        public string milestone_data { get; set; }
        public Nullable<int> user_source_id { get; set; }
        public virtual milestone milestone { get; set; }
        public virtual user_sources user_sources { get; set; }
    }
}
