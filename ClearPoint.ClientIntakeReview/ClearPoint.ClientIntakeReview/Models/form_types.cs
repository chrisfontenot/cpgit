using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class form_types
    {
        public form_types()
        {
            this.forms = new List<form>();
        }

        public int form_type_id { get; set; }
        public string form_type_name { get; set; }
        public virtual ICollection<form> forms { get; set; }
    }
}
