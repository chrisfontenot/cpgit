using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class housing_statuses
    {
        public string housing_status_code { get; set; }
        public string description { get; set; }
    }
}
