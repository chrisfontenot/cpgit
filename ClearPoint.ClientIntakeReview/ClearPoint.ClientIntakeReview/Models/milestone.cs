using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class milestone
    {
        public milestone()
        {
            this.visitor_history = new List<visitor_history>();
            this.visitor_history_archive = new List<visitor_history_archive>();
        }

        public short milestone_type_id { get; set; }
        public string description { get; set; }
        public virtual ICollection<visitor_history> visitor_history { get; set; }
        public virtual ICollection<visitor_history_archive> visitor_history_archive { get; set; }
    }
}
