using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class visitor_sessions
    {
        public System.Guid visitor_id { get; set; }
        public string session_id { get; set; }
        public Nullable<System.DateTime> last_modified_date { get; set; }
    }
}
