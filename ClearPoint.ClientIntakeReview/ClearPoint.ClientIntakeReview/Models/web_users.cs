using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_users
    {
        public web_users()
        {
            this.web_applications_statuses = new List<web_applications_statuses>();
        }

        public System.Guid visitor_id { get; set; }
        public string user_name { get; set; }
        public string ssn { get; set; }
        public string password { get; set; }
        public string user_data { get; set; }
        public Nullable<short> challenge_id { get; set; }
        public string challenge_answer { get; set; }
        public short encryption_key_id { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public Nullable<bool> password_expired { get; set; }
        public virtual challenge_questions challenge_questions { get; set; }
        public virtual encryption_keys encryption_keys { get; set; }
        public virtual ICollection<web_applications_statuses> web_applications_statuses { get; set; }
    }
}
