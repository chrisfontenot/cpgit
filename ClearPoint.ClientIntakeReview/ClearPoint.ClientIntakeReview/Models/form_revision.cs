using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class form_revision
    {
        public int form_revision_id { get; set; }
        public int form_id { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public virtual form form { get; set; }
    }
}
