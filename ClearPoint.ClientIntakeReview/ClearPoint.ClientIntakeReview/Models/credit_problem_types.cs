using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class credit_problem_types
    {
        public short problem_type_id { get; set; }
        public string description { get; set; }
    }
}
