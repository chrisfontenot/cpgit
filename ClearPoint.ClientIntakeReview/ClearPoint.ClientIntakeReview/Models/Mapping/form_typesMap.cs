using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class form_typesMap : EntityTypeConfiguration<form_types>
    {
        public form_typesMap()
        {
            // Primary Key
            this.HasKey(t => t.form_type_id);

            // Properties
            this.Property(t => t.form_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.form_type_name)
                .IsRequired()
                .HasMaxLength(75);

            // Table & Column Mappings
            this.ToTable("form_types");
            this.Property(t => t.form_type_id).HasColumnName("form_type_id");
            this.Property(t => t.form_type_name).HasColumnName("form_type_name");
        }
    }
}
