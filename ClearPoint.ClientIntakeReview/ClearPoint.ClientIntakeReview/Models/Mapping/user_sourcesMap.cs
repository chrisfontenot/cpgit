using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class user_sourcesMap : EntityTypeConfiguration<user_sources>
    {
        public user_sourcesMap()
        {
            // Primary Key
            this.HasKey(t => t.user_source_id);

            // Properties
            this.Property(t => t.user_source_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("user_sources");
            this.Property(t => t.user_source_id).HasColumnName("user_source_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
