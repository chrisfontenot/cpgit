using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class stateMap : EntityTypeConfiguration<state>
    {
        public stateMap()
        {
            // Primary Key
            this.HasKey(t => t.state_code);

            // Properties
            this.Property(t => t.state_code)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(2);

            this.Property(t => t.name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.cccs_licensed)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            // Table & Column Mappings
            this.ToTable("states");
            this.Property(t => t.state_code).HasColumnName("state_code");
            this.Property(t => t.name).HasColumnName("name");
            this.Property(t => t.cccs_licensed).HasColumnName("cccs_licensed");
        }
    }
}
