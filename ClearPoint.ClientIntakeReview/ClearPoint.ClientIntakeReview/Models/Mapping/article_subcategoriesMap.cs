using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class article_subcategoriesMap : EntityTypeConfiguration<article_subcategories>
    {
        public article_subcategoriesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.subcategory_id, t.subcategory_name });

            // Properties
            this.Property(t => t.subcategory_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.subcategory_name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.site_description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("article_subcategories");
            this.Property(t => t.subcategory_id).HasColumnName("subcategory_id");
            this.Property(t => t.subcategory_name).HasColumnName("subcategory_name");
            this.Property(t => t.site_description).HasColumnName("site_description");
        }
    }
}
