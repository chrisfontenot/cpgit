using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class encryption_keysMap : EntityTypeConfiguration<encryption_keys>
    {
        public encryption_keysMap()
        {
            // Primary Key
            this.HasKey(t => t.encryption_key_id);

            // Properties
            this.Property(t => t.encryption_key_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.value)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("encryption_keys");
            this.Property(t => t.encryption_key_id).HasColumnName("encryption_key_id");
            this.Property(t => t.value).HasColumnName("value");
            this.Property(t => t.current_key).HasColumnName("current_key");
        }
    }
}
