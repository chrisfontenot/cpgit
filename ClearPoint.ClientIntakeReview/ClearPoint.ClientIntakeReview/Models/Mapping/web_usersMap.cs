using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_usersMap : EntityTypeConfiguration<web_users>
    {
        public web_usersMap()
        {
            // Primary Key
            this.HasKey(t => t.visitor_id);

            // Properties
            this.Property(t => t.user_name)
                .IsRequired()
                .HasMaxLength(28);

            this.Property(t => t.ssn)
                .HasMaxLength(28);

            this.Property(t => t.password)
                .IsRequired()
                .HasMaxLength(28);

            this.Property(t => t.user_data)
                .IsRequired();

            this.Property(t => t.challenge_answer)
                .HasMaxLength(28);

            // Table & Column Mappings
            this.ToTable("web_users");
            this.Property(t => t.visitor_id).HasColumnName("visitor_id");
            this.Property(t => t.user_name).HasColumnName("user_name");
            this.Property(t => t.ssn).HasColumnName("ssn");
            this.Property(t => t.password).HasColumnName("password");
            this.Property(t => t.user_data).HasColumnName("user_data");
            this.Property(t => t.challenge_id).HasColumnName("challenge_id");
            this.Property(t => t.challenge_answer).HasColumnName("challenge_answer");
            this.Property(t => t.encryption_key_id).HasColumnName("encryption_key_id");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
            this.Property(t => t.password_expired).HasColumnName("password_expired");

            // Relationships
            this.HasOptional(t => t.challenge_questions)
                .WithMany(t => t.web_users)
                .HasForeignKey(d => d.challenge_id);
            this.HasRequired(t => t.encryption_keys)
                .WithMany(t => t.web_users)
                .HasForeignKey(d => d.encryption_key_id);

        }
    }
}
