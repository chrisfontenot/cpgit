using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_applicationsMap : EntityTypeConfiguration<web_applications>
    {
        public web_applicationsMap()
        {
            // Primary Key
            this.HasKey(t => t.application_id);

            // Properties
            this.Property(t => t.application_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.application_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("web_applications");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.application_data).HasColumnName("application_data");
            this.Property(t => t.address_id).HasColumnName("address_id");
            this.Property(t => t.submission_date).HasColumnName("submission_date");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");

            // Relationships
            this.HasRequired(t => t.web_addresses)
                .WithMany(t => t.web_applications)
                .HasForeignKey(d => d.address_id);
            this.HasRequired(t => t.web_applications_statuses)
                .WithOptional(t => t.web_applications);

        }
    }
}
