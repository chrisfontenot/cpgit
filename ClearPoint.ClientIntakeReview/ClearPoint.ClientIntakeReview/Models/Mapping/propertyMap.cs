using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class propertyMap : EntityTypeConfiguration<property>
    {
        public propertyMap()
        {
            // Primary Key
            this.HasKey(t => t.property_name);

            // Properties
            this.Property(t => t.property_name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.value)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("properties");
            this.Property(t => t.property_name).HasColumnName("property_name");
            this.Property(t => t.value).HasColumnName("value");
        }
    }
}
