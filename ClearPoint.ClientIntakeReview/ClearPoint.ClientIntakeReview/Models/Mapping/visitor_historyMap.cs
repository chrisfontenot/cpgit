using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class visitor_historyMap : EntityTypeConfiguration<visitor_history>
    {
        public visitor_historyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.visitor_id, t.milestone_type_id, t.milestone_date });

            // Properties
            this.Property(t => t.milestone_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.milestone_data)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("visitor_history");
            this.Property(t => t.visitor_id).HasColumnName("visitor_id");
            this.Property(t => t.milestone_type_id).HasColumnName("milestone_type_id");
            this.Property(t => t.milestone_date).HasColumnName("milestone_date");
            this.Property(t => t.milestone_data).HasColumnName("milestone_data");
            this.Property(t => t.user_source_id).HasColumnName("user_source_id");

            // Relationships
            this.HasRequired(t => t.milestone)
                .WithMany(t => t.visitor_history)
                .HasForeignKey(d => d.milestone_type_id);
            this.HasOptional(t => t.user_sources)
                .WithMany(t => t.visitor_history)
                .HasForeignKey(d => d.user_source_id);

        }
    }
}
