using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class challenge_questionsMap : EntityTypeConfiguration<challenge_questions>
    {
        public challenge_questionsMap()
        {
            // Primary Key
            this.HasKey(t => t.challenge_id);

            // Properties
            this.Property(t => t.challenge_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.question)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("challenge_questions");
            this.Property(t => t.challenge_id).HasColumnName("challenge_id");
            this.Property(t => t.question).HasColumnName("question");
        }
    }
}
