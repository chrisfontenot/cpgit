using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class credit_problem_typesMap : EntityTypeConfiguration<credit_problem_types>
    {
        public credit_problem_typesMap()
        {
            // Primary Key
            this.HasKey(t => t.problem_type_id);

            // Properties
            this.Property(t => t.problem_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("credit_problem_types");
            this.Property(t => t.problem_type_id).HasColumnName("problem_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
