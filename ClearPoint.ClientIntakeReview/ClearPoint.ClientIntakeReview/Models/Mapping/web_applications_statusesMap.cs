using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_applications_statusesMap : EntityTypeConfiguration<web_applications_statuses>
    {
        public web_applications_statusesMap()
        {
            // Primary Key
            this.HasKey(t => t.application_id);

            // Properties
            // Table & Column Mappings
            this.ToTable("web_applications_statuses");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.visitor_id).HasColumnName("visitor_id");
            this.Property(t => t.workflow_step_id).HasColumnName("workflow_step_id");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
            this.Property(t => t.user_source_id).HasColumnName("user_source_id");

            // Relationships
            this.HasOptional(t => t.user_sources)
                .WithMany(t => t.web_applications_statuses)
                .HasForeignKey(d => d.user_source_id);
            this.HasRequired(t => t.web_users)
                .WithMany(t => t.web_applications_statuses)
                .HasForeignKey(d => d.visitor_id);
            this.HasOptional(t => t.workflow_steps)
                .WithMany(t => t.web_applications_statuses)
                .HasForeignKey(d => d.workflow_step_id);

        }
    }
}
