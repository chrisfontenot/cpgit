using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class workflow_stepsMap : EntityTypeConfiguration<workflow_steps>
    {
        public workflow_stepsMap()
        {
            // Primary Key
            this.HasKey(t => t.workflow_step_id);

            // Properties
            this.Property(t => t.workflow_step_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("workflow_steps");
            this.Property(t => t.workflow_step_id).HasColumnName("workflow_step_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
