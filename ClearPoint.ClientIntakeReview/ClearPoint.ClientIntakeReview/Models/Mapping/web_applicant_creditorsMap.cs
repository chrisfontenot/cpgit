using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_applicant_creditorsMap : EntityTypeConfiguration<web_applicant_creditors>
    {
        public web_applicant_creditorsMap()
        {
            // Primary Key
            this.HasKey(t => t.creditor_id);

            // Properties
            this.Property(t => t.creditor_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("web_applicant_creditors");
            this.Property(t => t.creditor_id).HasColumnName("creditor_id");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.creditor_data).HasColumnName("creditor_data");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");

            // Relationships
            this.HasRequired(t => t.web_applications)
                .WithMany(t => t.web_applicant_creditors)
                .HasForeignKey(d => d.application_id);

        }
    }
}
