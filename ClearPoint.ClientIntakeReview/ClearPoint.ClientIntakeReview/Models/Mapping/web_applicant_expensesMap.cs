using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_applicant_expensesMap : EntityTypeConfiguration<web_applicant_expenses>
    {
        public web_applicant_expensesMap()
        {
            // Primary Key
            this.HasKey(t => t.application_id);

            // Properties
            this.Property(t => t.application_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.expenses_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("web_applicant_expenses");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.expenses_data).HasColumnName("expenses_data");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");

            // Relationships
            this.HasRequired(t => t.web_applications)
                .WithOptional(t => t.web_applicant_expenses);

        }
    }
}
