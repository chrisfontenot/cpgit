using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class referral_backupMap : EntityTypeConfiguration<referral_backup>
    {
        public referral_backupMap()
        {
            // Primary Key
            this.HasKey(t => new { t.referral_type_id, t.description });

            // Properties
            this.Property(t => t.referral_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("referral_backup");
            this.Property(t => t.referral_type_id).HasColumnName("referral_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
