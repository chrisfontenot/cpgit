using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class ethnicityMap : EntityTypeConfiguration<ethnicity>
    {
        public ethnicityMap()
        {
            // Primary Key
            this.HasKey(t => t.ethnicity_id);

            // Properties
            this.Property(t => t.ethnicity_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("ethnicities");
            this.Property(t => t.ethnicity_id).HasColumnName("ethnicity_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
