using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class housing_statusesMap : EntityTypeConfiguration<housing_statuses>
    {
        public housing_statusesMap()
        {
            // Primary Key
            this.HasKey(t => t.housing_status_code);

            // Properties
            this.Property(t => t.housing_status_code)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("housing_statuses");
            this.Property(t => t.housing_status_code).HasColumnName("housing_status_code");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
