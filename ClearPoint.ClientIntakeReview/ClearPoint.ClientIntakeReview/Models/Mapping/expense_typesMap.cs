using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class expense_typesMap : EntityTypeConfiguration<expense_types>
    {
        public expense_typesMap()
        {
            // Primary Key
            this.HasKey(t => t.expense_type_id);

            // Properties
            this.Property(t => t.expense_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("expense_types");
            this.Property(t => t.expense_type_id).HasColumnName("expense_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
