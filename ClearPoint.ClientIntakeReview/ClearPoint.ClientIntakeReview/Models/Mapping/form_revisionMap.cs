using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class form_revisionMap : EntityTypeConfiguration<form_revision>
    {
        public form_revisionMap()
        {
            // Primary Key
            this.HasKey(t => t.form_revision_id);

            // Properties
            this.Property(t => t.form_revision_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("form_revision");
            this.Property(t => t.form_revision_id).HasColumnName("form_revision_id");
            this.Property(t => t.form_id).HasColumnName("form_id");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");

            // Relationships
            this.HasRequired(t => t.form)
                .WithMany(t => t.form_revision)
                .HasForeignKey(d => d.form_id);

        }
    }
}
