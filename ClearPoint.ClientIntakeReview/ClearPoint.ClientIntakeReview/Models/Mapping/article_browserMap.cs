using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class article_browserMap : EntityTypeConfiguration<article_browser>
    {
        public article_browserMap()
        {
            // Primary Key
            this.HasKey(t => new { t.article_id, t.url, t.category_id, t.subcategory_id, t.title });

            // Properties
            this.Property(t => t.article_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.url)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.category_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.subcategory_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.title)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.keywords)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("article_browser");
            this.Property(t => t.article_id).HasColumnName("article_id");
            this.Property(t => t.url).HasColumnName("url");
            this.Property(t => t.category_id).HasColumnName("category_id");
            this.Property(t => t.subcategory_id).HasColumnName("subcategory_id");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.keywords).HasColumnName("keywords");
            this.Property(t => t.contents).HasColumnName("contents");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
        }
    }
}
