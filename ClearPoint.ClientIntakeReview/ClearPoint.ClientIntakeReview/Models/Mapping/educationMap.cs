using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class educationMap : EntityTypeConfiguration<education>
    {
        public educationMap()
        {
            // Primary Key
            this.HasKey(t => t.education_id);

            // Properties
            this.Property(t => t.education_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .HasMaxLength(40);

            // Table & Column Mappings
            this.ToTable("education");
            this.Property(t => t.education_id).HasColumnName("education_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
