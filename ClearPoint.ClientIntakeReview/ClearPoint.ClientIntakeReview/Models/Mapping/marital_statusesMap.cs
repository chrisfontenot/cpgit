using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class marital_statusesMap : EntityTypeConfiguration<marital_statuses>
    {
        public marital_statusesMap()
        {
            // Primary Key
            this.HasKey(t => t.marital_status_code);

            // Properties
            this.Property(t => t.marital_status_code)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(1);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("marital_statuses");
            this.Property(t => t.marital_status_code).HasColumnName("marital_status_code");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
