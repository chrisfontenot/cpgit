using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class form_repositoryMap : EntityTypeConfiguration<form_repository>
    {
        public form_repositoryMap()
        {
            // Primary Key
            this.HasKey(t => t.form_store_id);

            // Properties
            this.Property(t => t.form_store_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.form_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("form_repository");
            this.Property(t => t.form_store_id).HasColumnName("form_store_id");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.form_status_code).HasColumnName("form_status_code");
            this.Property(t => t.form_revision_id).HasColumnName("form_revision_id");
            this.Property(t => t.form_data).HasColumnName("form_data");
            this.Property(t => t.form_image).HasColumnName("form_image");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
        }
    }
}
