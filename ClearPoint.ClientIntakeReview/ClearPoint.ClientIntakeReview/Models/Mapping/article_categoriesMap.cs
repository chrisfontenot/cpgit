using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class article_categoriesMap : EntityTypeConfiguration<article_categories>
    {
        public article_categoriesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.category_id, t.category_name });

            // Properties
            this.Property(t => t.category_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.category_name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.site_description)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("article_categories");
            this.Property(t => t.category_id).HasColumnName("category_id");
            this.Property(t => t.category_name).HasColumnName("category_name");
            this.Property(t => t.site_description).HasColumnName("site_description");
        }
    }
}
