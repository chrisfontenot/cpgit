using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class credit_problem_type_bkupMap : EntityTypeConfiguration<credit_problem_type_bkup>
    {
        public credit_problem_type_bkupMap()
        {
            // Primary Key
            this.HasKey(t => new { t.problem_type_id, t.description });

            // Properties
            this.Property(t => t.problem_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("credit_problem_type_bkup");
            this.Property(t => t.problem_type_id).HasColumnName("problem_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
