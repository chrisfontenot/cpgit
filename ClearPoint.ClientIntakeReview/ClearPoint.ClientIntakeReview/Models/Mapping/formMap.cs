using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class formMap : EntityTypeConfiguration<form>
    {
        public formMap()
        {
            // Primary Key
            this.HasKey(t => t.form_id);

            // Properties
            this.Property(t => t.form_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.form_name)
                .IsRequired()
                .HasMaxLength(60);

            this.Property(t => t.form_description)
                .HasMaxLength(150);

            this.Property(t => t.state_code)
                .IsFixedLength()
                .HasMaxLength(2);

            // Table & Column Mappings
            this.ToTable("forms");
            this.Property(t => t.form_id).HasColumnName("form_id");
            this.Property(t => t.form_name).HasColumnName("form_name");
            this.Property(t => t.form_description).HasColumnName("form_description");
            this.Property(t => t.state_code).HasColumnName("state_code");
            this.Property(t => t.form_type_id).HasColumnName("form_type_id");

            // Relationships
            this.HasRequired(t => t.form_types)
                .WithMany(t => t.forms)
                .HasForeignKey(d => d.form_type_id);
            this.HasOptional(t => t.state)
                .WithMany(t => t.forms)
                .HasForeignKey(d => d.state_code);

        }
    }
}
