using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class associate_typesMap : EntityTypeConfiguration<associate_types>
    {
        public associate_typesMap()
        {
            // Primary Key
            this.HasKey(t => t.associate_type_id);

            // Properties
            this.Property(t => t.associate_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("associate_types");
            this.Property(t => t.associate_type_id).HasColumnName("associate_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
