using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class referral_typesMap : EntityTypeConfiguration<referral_types>
    {
        public referral_typesMap()
        {
            // Primary Key
            this.HasKey(t => t.referral_type_id);

            // Properties
            this.Property(t => t.referral_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("referral_types");
            this.Property(t => t.referral_type_id).HasColumnName("referral_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
