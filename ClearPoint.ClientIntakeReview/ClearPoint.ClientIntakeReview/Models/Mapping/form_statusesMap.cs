using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class form_statusesMap : EntityTypeConfiguration<form_statuses>
    {
        public form_statusesMap()
        {
            // Primary Key
            this.HasKey(t => t.form_status_code);

            // Properties
            this.Property(t => t.form_status_code)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.form_status)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("form_statuses");
            this.Property(t => t.form_status_code).HasColumnName("form_status_code");
            this.Property(t => t.form_status).HasColumnName("form_status");
        }
    }
}
