using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_addressesMap : EntityTypeConfiguration<web_addresses>
    {
        public web_addressesMap()
        {
            // Primary Key
            this.HasKey(t => t.address_id);

            // Properties
            this.Property(t => t.address_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("web_addresses");
            this.Property(t => t.address_id).HasColumnName("address_id");
            this.Property(t => t.address_data).HasColumnName("address_data");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
            this.Property(t => t.original_address_id).HasColumnName("original_address_id");
        }
    }
}
