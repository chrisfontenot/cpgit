using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class visitor_sessionsMap : EntityTypeConfiguration<visitor_sessions>
    {
        public visitor_sessionsMap()
        {
            // Primary Key
            this.HasKey(t => t.visitor_id);

            // Properties
            this.Property(t => t.session_id)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("visitor_sessions");
            this.Property(t => t.visitor_id).HasColumnName("visitor_id");
            this.Property(t => t.session_id).HasColumnName("session_id");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");
        }
    }
}
