using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class creditorMap : EntityTypeConfiguration<creditor>
    {
        public creditorMap()
        {
            // Primary Key
            this.HasKey(t => t.creditor_id);

            // Properties
            this.Property(t => t.creditor_name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("creditors");
            this.Property(t => t.creditor_id).HasColumnName("creditor_id");
            this.Property(t => t.creditor_name).HasColumnName("creditor_name");
            this.Property(t => t.percent_balance).HasColumnName("percent_balance");
            this.Property(t => t.percent_payment).HasColumnName("percent_payment");
            this.Property(t => t.minimum_payment).HasColumnName("minimum_payment");
            this.Property(t => t.logic_type_id).HasColumnName("logic_type_id");
            this.Property(t => t.active_creditor).HasColumnName("active_creditor");

            // Relationships
            this.HasRequired(t => t.creditor_logic)
                .WithMany(t => t.creditors)
                .HasForeignKey(d => d.logic_type_id);

        }
    }
}
