using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class milestoneMap : EntityTypeConfiguration<milestone>
    {
        public milestoneMap()
        {
            // Primary Key
            this.HasKey(t => t.milestone_type_id);

            // Properties
            this.Property(t => t.milestone_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("milestones");
            this.Property(t => t.milestone_type_id).HasColumnName("milestone_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
