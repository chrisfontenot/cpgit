using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class creditor_logicMap : EntityTypeConfiguration<creditor_logic>
    {
        public creditor_logicMap()
        {
            // Primary Key
            this.HasKey(t => t.logic_type_id);

            // Properties
            this.Property(t => t.logic_type_id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("creditor_logic");
            this.Property(t => t.logic_type_id).HasColumnName("logic_type_id");
            this.Property(t => t.description).HasColumnName("description");
        }
    }
}
