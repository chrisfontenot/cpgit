using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClearPoint.ClientIntakeReview.Models.Mapping
{
    public class web_applicant_associatesMap : EntityTypeConfiguration<web_applicant_associates>
    {
        public web_applicant_associatesMap()
        {
            // Primary Key
            this.HasKey(t => t.associate_id);

            // Properties
            this.Property(t => t.associate_data)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("web_applicant_associates");
            this.Property(t => t.associate_id).HasColumnName("associate_id");
            this.Property(t => t.associate_type_id).HasColumnName("associate_type_id");
            this.Property(t => t.application_id).HasColumnName("application_id");
            this.Property(t => t.associate_data).HasColumnName("associate_data");
            this.Property(t => t.address_id).HasColumnName("address_id");
            this.Property(t => t.last_modified_date).HasColumnName("last_modified_date");

            // Relationships
            this.HasRequired(t => t.associate_types)
                .WithMany(t => t.web_applicant_associates)
                .HasForeignKey(d => d.associate_type_id);
            this.HasRequired(t => t.web_addresses)
                .WithMany(t => t.web_applicant_associates)
                .HasForeignKey(d => d.address_id);
            this.HasRequired(t => t.web_applications)
                .WithMany(t => t.web_applicant_associates)
                .HasForeignKey(d => d.application_id);

        }
    }
}
