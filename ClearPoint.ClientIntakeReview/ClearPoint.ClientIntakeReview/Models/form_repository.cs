using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class form_repository
    {
        public int form_store_id { get; set; }
        public int application_id { get; set; }
        public short form_status_code { get; set; }
        public int form_revision_id { get; set; }
        public string form_data { get; set; }
        public byte[] form_image { get; set; }
        public System.DateTime last_modified_date { get; set; }
    }
}
