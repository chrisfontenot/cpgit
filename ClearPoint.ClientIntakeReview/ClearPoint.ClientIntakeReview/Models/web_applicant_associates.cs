using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_applicant_associates
    {
        public int associate_id { get; set; }
        public short associate_type_id { get; set; }
        public int application_id { get; set; }
        public string associate_data { get; set; }
        public int address_id { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public virtual associate_types associate_types { get; set; }
        public virtual web_addresses web_addresses { get; set; }
        public virtual web_applications web_applications { get; set; }
    }
}
