using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_applications
    {
        public web_applications()
        {
            this.web_applicant_associates = new List<web_applicant_associates>();
            this.web_applicant_creditors = new List<web_applicant_creditors>();
        }

        public int application_id { get; set; }
        public string application_data { get; set; }
        public int address_id { get; set; }
        public Nullable<System.DateTime> submission_date { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public virtual web_addresses web_addresses { get; set; }
        public virtual ICollection<web_applicant_associates> web_applicant_associates { get; set; }
        public virtual ICollection<web_applicant_creditors> web_applicant_creditors { get; set; }
        public virtual web_applicant_expenses web_applicant_expenses { get; set; }
        public virtual web_applications_statuses web_applications_statuses { get; set; }
    }
}
