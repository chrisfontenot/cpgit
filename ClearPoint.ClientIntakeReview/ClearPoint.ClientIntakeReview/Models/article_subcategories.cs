using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class article_subcategories
    {
        public short subcategory_id { get; set; }
        public string subcategory_name { get; set; }
        public string site_description { get; set; }
    }
}
