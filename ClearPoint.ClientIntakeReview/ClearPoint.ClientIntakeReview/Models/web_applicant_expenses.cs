using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_applicant_expenses
    {
        public int application_id { get; set; }
        public string expenses_data { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public virtual web_applications web_applications { get; set; }
    }
}
