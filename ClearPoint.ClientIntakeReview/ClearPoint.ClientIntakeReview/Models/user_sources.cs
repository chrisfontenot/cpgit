using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class user_sources
    {
        public user_sources()
        {
            this.visitor_history = new List<visitor_history>();
            this.visitor_history_archive = new List<visitor_history_archive>();
            this.web_applications_statuses = new List<web_applications_statuses>();
        }

        public int user_source_id { get; set; }
        public string description { get; set; }
        public virtual ICollection<visitor_history> visitor_history { get; set; }
        public virtual ICollection<visitor_history_archive> visitor_history_archive { get; set; }
        public virtual ICollection<web_applications_statuses> web_applications_statuses { get; set; }
    }
}
