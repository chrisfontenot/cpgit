using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class education
    {
        public int education_id { get; set; }
        public string description { get; set; }
    }
}
