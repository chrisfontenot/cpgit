using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class state
    {
        public state()
        {
            this.forms = new List<form>();
        }

        public string state_code { get; set; }
        public string name { get; set; }
        public string cccs_licensed { get; set; }
        public virtual ICollection<form> forms { get; set; }
    }
}
