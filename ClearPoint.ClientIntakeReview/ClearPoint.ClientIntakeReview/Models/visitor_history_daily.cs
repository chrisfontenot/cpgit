using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class visitor_history_daily
    {
        public System.Guid visitor_id { get; set; }
        public short milestone_type_id { get; set; }
        public System.DateTime milestone_date { get; set; }
        public string milestone_data { get; set; }
        public Nullable<int> user_source_id { get; set; }
    }
}
