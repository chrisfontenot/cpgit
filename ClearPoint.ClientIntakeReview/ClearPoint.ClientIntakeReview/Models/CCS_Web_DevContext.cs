using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ClearPoint.ClientIntakeReview.Models.Mapping;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class CCS_Web_DevContext : DbContext
    {
        static CCS_Web_DevContext()
        {
            Database.SetInitializer<CCS_Web_DevContext>(null);
        }

        public CCS_Web_DevContext()
            : base("Name=CCS_Web_DevContext")
        {
        }

        public DbSet<article_browser> article_browser { get; set; }
        public DbSet<article_categories> article_categories { get; set; }
        public DbSet<article_subcategories> article_subcategories { get; set; }
        public DbSet<associate_types> associate_types { get; set; }
        public DbSet<challenge_questions> challenge_questions { get; set; }
        public DbSet<credit_problem_type_bkup> credit_problem_type_bkup { get; set; }
        public DbSet<credit_problem_types> credit_problem_types { get; set; }
        public DbSet<creditor_logic> creditor_logic { get; set; }
        public DbSet<creditor> creditors { get; set; }
        public DbSet<education> educations { get; set; }
        public DbSet<encryption_keys> encryption_keys { get; set; }
        public DbSet<ethnicity> ethnicities { get; set; }
        public DbSet<expense_types> expense_types { get; set; }
        public DbSet<form_repository> form_repository { get; set; }
        public DbSet<form_revision> form_revision { get; set; }
        public DbSet<form_statuses> form_statuses { get; set; }
        public DbSet<form_types> form_types { get; set; }
        public DbSet<form> forms { get; set; }
        public DbSet<housing_statuses> housing_statuses { get; set; }
        public DbSet<marital_statuses> marital_statuses { get; set; }
        public DbSet<milestone> milestones { get; set; }
        public DbSet<property> properties { get; set; }
        public DbSet<referral_backup> referral_backup { get; set; }
        public DbSet<referral_types> referral_types { get; set; }
        public DbSet<state> states { get; set; }
        public DbSet<user_sources> user_sources { get; set; }
        public DbSet<visitor_history> visitor_history { get; set; }
        public DbSet<visitor_history_archive> visitor_history_archive { get; set; }
        public DbSet<visitor_history_daily> visitor_history_daily { get; set; }
        public DbSet<visitor_sessions> visitor_sessions { get; set; }
        public DbSet<web_addresses> web_addresses { get; set; }
        public DbSet<web_applicant_associates> web_applicant_associates { get; set; }
        public DbSet<web_applicant_creditors> web_applicant_creditors { get; set; }
        public DbSet<web_applicant_expenses> web_applicant_expenses { get; set; }
        public DbSet<web_applications> web_applications { get; set; }
        public DbSet<web_applications_statuses> web_applications_statuses { get; set; }
        public DbSet<web_users> web_users { get; set; }
        public DbSet<workflow_steps> workflow_steps { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new article_browserMap());
            modelBuilder.Configurations.Add(new article_categoriesMap());
            modelBuilder.Configurations.Add(new article_subcategoriesMap());
            modelBuilder.Configurations.Add(new associate_typesMap());
            modelBuilder.Configurations.Add(new challenge_questionsMap());
            modelBuilder.Configurations.Add(new credit_problem_type_bkupMap());
            modelBuilder.Configurations.Add(new credit_problem_typesMap());
            modelBuilder.Configurations.Add(new creditor_logicMap());
            modelBuilder.Configurations.Add(new creditorMap());
            modelBuilder.Configurations.Add(new educationMap());
            modelBuilder.Configurations.Add(new encryption_keysMap());
            modelBuilder.Configurations.Add(new ethnicityMap());
            modelBuilder.Configurations.Add(new expense_typesMap());
            modelBuilder.Configurations.Add(new form_repositoryMap());
            modelBuilder.Configurations.Add(new form_revisionMap());
            modelBuilder.Configurations.Add(new form_statusesMap());
            modelBuilder.Configurations.Add(new form_typesMap());
            modelBuilder.Configurations.Add(new formMap());
            modelBuilder.Configurations.Add(new housing_statusesMap());
            modelBuilder.Configurations.Add(new marital_statusesMap());
            modelBuilder.Configurations.Add(new milestoneMap());
            modelBuilder.Configurations.Add(new propertyMap());
            modelBuilder.Configurations.Add(new referral_backupMap());
            modelBuilder.Configurations.Add(new referral_typesMap());
            modelBuilder.Configurations.Add(new stateMap());
            modelBuilder.Configurations.Add(new user_sourcesMap());
            modelBuilder.Configurations.Add(new visitor_historyMap());
            modelBuilder.Configurations.Add(new visitor_history_archiveMap());
            modelBuilder.Configurations.Add(new visitor_history_dailyMap());
            modelBuilder.Configurations.Add(new visitor_sessionsMap());
            modelBuilder.Configurations.Add(new web_addressesMap());
            modelBuilder.Configurations.Add(new web_applicant_associatesMap());
            modelBuilder.Configurations.Add(new web_applicant_creditorsMap());
            modelBuilder.Configurations.Add(new web_applicant_expensesMap());
            modelBuilder.Configurations.Add(new web_applicationsMap());
            modelBuilder.Configurations.Add(new web_applications_statusesMap());
            modelBuilder.Configurations.Add(new web_usersMap());
            modelBuilder.Configurations.Add(new workflow_stepsMap());
        }
    }
}
