using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class creditor
    {
        public int creditor_id { get; set; }
        public string creditor_name { get; set; }
        public Nullable<decimal> percent_balance { get; set; }
        public Nullable<decimal> percent_payment { get; set; }
        public Nullable<decimal> minimum_payment { get; set; }
        public short logic_type_id { get; set; }
        public Nullable<bool> active_creditor { get; set; }
        public virtual creditor_logic creditor_logic { get; set; }
    }
}
