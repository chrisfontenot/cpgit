using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class web_applications_statuses
    {
        public int application_id { get; set; }
        public System.Guid visitor_id { get; set; }
        public Nullable<short> workflow_step_id { get; set; }
        public System.DateTime last_modified_date { get; set; }
        public Nullable<int> user_source_id { get; set; }
        public virtual user_sources user_sources { get; set; }
        public virtual web_applications web_applications { get; set; }
        public virtual web_users web_users { get; set; }
        public virtual workflow_steps workflow_steps { get; set; }
    }
}
