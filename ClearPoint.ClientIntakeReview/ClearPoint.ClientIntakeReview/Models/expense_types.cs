using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class expense_types
    {
        public short expense_type_id { get; set; }
        public string description { get; set; }
    }
}
