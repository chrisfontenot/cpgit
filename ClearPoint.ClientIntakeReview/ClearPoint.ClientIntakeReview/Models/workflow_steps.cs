using System;
using System.Collections.Generic;

namespace ClearPoint.ClientIntakeReview.Models
{
    public partial class workflow_steps
    {
        public workflow_steps()
        {
            this.web_applications_statuses = new List<web_applications_statuses>();
        }

        public short workflow_step_id { get; set; }
        public string description { get; set; }
        public virtual ICollection<web_applications_statuses> web_applications_statuses { get; set; }
    }
}
