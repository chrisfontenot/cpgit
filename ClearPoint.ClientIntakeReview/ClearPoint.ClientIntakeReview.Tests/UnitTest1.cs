﻿using System;
using ClearPoint.ClientIntakeReview.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClearPoint.ClientIntakeReview.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            IntakeAPIController.Application Application = Helpers.ParseXML.ParseData<IntakeAPIController.Application>(@"<application>
<ssn><![CDATA[454-41-4060]]></ssn>
<birth_date><![CDATA[10/20/1961]]></birth_date>
<marital_status_code><![CDATA[D]]></marital_status_code>
<num_dependents><![CDATA[2]]></num_dependents>
<problem_type_id><![CDATA[2]]></problem_type_id>
<referral_type_id><![CDATA[401]]></referral_type_id>
<telephone_home><![CDATA[817-571-1786]]></telephone_home>
<telephone_work><![CDATA[]]></telephone_work>
<fax_number><![CDATA[]]></fax_number>
<housing_status_code><![CDATA[R]]></housing_status_code>
<housing_payments_current><![CDATA[1]]></housing_payments_current>
<employer><![CDATA[]]></employer>
<job_title><![CDATA[]]></job_title>
<monthly_income_gross><![CDATA[1901.25]]></monthly_income_gross>
<monthly_income_net><![CDATA[1332.50]]></monthly_income_net>
<monthly_income_other><![CDATA[487.50]]></monthly_income_other>
<coapplicant_employer><![CDATA[]]></coapplicant_employer>
<coapplicant_job_title><![CDATA[]]></coapplicant_job_title>
<coapplicant_monthly_income_gross><![CDATA[]]></coapplicant_monthly_income_gross>
<coapplicant_monthly_income_net><![CDATA[]]></coapplicant_monthly_income_net>
<unpaid_judgements><![CDATA[0]]></unpaid_judgements>
<wages_garnished><![CDATA[0]]></wages_garnished>
<accounts_with_attorney><![CDATA[0]]></accounts_with_attorney>
<filed_bankruptcy><![CDATA[0]]></filed_bankruptcy>
<comments><![CDATA[]]></comments>
</application>
");
            Assert.AreEqual("some", Application.employer);     
        }
    }
}
