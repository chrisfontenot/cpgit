﻿<%@ Page Title="Payment" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="false" CodeBehind="Default.aspx.cs" Inherits="FHLB.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        /* The Modal (background) */
        .modal
        {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content
        {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 80%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop
        {
            from {top:-300px; opacity:0} 
            to {top:0; opacity:1}
        }

        @keyframes animatetop
        {
            from {top:-300px; opacity:0}
            to {top:0; opacity:1}
        }

        /* The Close Button */
        .close
        {
            color: white;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus
        {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header
        {
            padding: 2px 16px;
            background-color: blue;
            color: white;
        }

        .modal-body
        {
            padding: 2px 16px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function showDialog() {
            el = document.getElementById("myModal");
            el.style.display = "block";
            var span = document.getElementsByClassName("close")[0];
            span.onclick = closeDialog;
        }

        function closeDialog() {
            el = document.getElementById("myModal");
            el.style.display = "none";
        }

        window.onclick = function (event) {
            el = document.getElementById("myModal");
            if (event.target == el) {
                el.style.display = "none";
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- Modal content -->
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>Session Ended</h2>
            </div>
            <div class="modal-body">
                <p>
                    For your security, this session has timed out. If you wish to continue please 
                    click <a href="#" onclick="javascript:closeDialog(); return false;">here.</a> Otherwise,
                    please close your Internet browser to ensure maximum protection of your personal
                    information. We apologize for any inconvenience.
                </p>
            </div>
        </div>
    </div>

    <asp:Table ID="Table2" runat="server" CssClass="nopad wide">
        <asp:TableRow ID="TableRow3" runat="server" CssClass="nopad">
            <asp:TableCell CssClass="nopad wide">
                <asp:Table ID="Panel1_Form" runat="server">
                    <asp:TableRow ID="TableRow4" runat="server" CssClass="nopad">
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad field">
                            <p>
                                Make Payment
                            </p>
                            <p>
                                <asp:Label CssClass="error" runat="server" ID="Panel1_Errors" />
                            </p>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow5" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Closing Agent's Firm Name:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field"><asp:TextBox ID="firm_name" runat="server" Columns="80" MaxLength="80">
                        </asp:TextBox></asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow6" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Closing Agent's Firm Phone Number:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field"><asp:TextBox ID="phone_number" runat="server" Columns="80" MaxLength="80">
                        </asp:TextBox></asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow7" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Closing Agent's Email Address:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field"><asp:TextBox ID="email" runat="server" Columns="80" MaxLength="80">
                        </asp:TextBox></asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow8" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Confirm Email Address:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field"><asp:TextBox ID="email_confirm" runat="server" Columns="80" MaxLength="80">
                        </asp:TextBox></asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow9" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Number of Reservations to be paid:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field">
                            <asp:DropDownList ID="NumberOfPayments" runat="server">
                                <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Selected="false" Text="2" Value="2"></asp:ListItem>
                                <asp:ListItem Selected="false" Text="3" Value="3"></asp:ListItem>
                                <asp:ListItem Selected="false" Text="4" Value="4"></asp:ListItem>
                                <asp:ListItem Selected="false" Text="5" Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow10" runat="server" CssClass="nopad">
                        <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
                        <asp:TableCell CssClass="nopad lbl">Payment Method:</asp:TableCell>
                        <asp:TableCell CssClass="nopad field">
                            <asp:DropDownList ID="PaymentMethod" runat="server">
                                <asp:ListItem Selected="True" Text="Bank Draft" Value="AC"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow11" runat="server" CssClass="nopad">
                        <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow ID="TableRow12" runat="server" CssClass="nopad">
                        <asp:TableCell ColumnSpan="2"></asp:TableCell>
                        <asp:TableCell CssClass="nopad field no-print">
                            <button runat="server" class="btn btn‐default" id="SubmitPanel1" name="SubmitPanel1" type="submit">Submit</button>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow runat="server" CssClass="nopad">
                        <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow13" runat="server" CssClass="nopad no-print">
            <asp:TableCell CssClass="middle" ColumnSpan="3">
                <asp:Image runat="server" ID="logos" ImageAlign="Middle" ImageUrl="~/images/es3777_btmbanner.gif" Width="1084px" Height="162px" /><br />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow14" runat="server" CssClass="nopad no-print">
            <asp:TableCell CssClass="middle" ColumnSpan="3">
                <asp:Image runat="server" ID="Certification" ImageAlign="Middle" ImageUrl="~/images/securitymetrics_logo.gif" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>