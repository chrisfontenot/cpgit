﻿<%@ Page Title="Confirmation" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="FHLB.Confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
        var idleSeconds = 300; // 300 seconds or 5 minutes
        $(function () {
            var idleTimer;
            function resetTimer() {
                clearTimeout(idleTimer);
                idleTimer = setTimeout(whenUserIdle, idleSeconds * 1000);
            }
            $(document.body).bind('mousemove keydown click', resetTimer); //space separated events list that we want to monitor
            resetTimer(); // Start the timer when the page loads
        });

        function whenUserIdle()
        {
            document.location = "./Default.aspx?t=1";
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <p style="text-align:center">
        Please confirm the following information. You have requested that your
        deposit to CredAbility be made to the Online Payment system as follows:
    </p>

    <div style="width:100%; margin-left:15%">
        <asp:Literal ID="ConfirmationTable" runat="server" Mode="PassThrough"></asp:Literal>
    </div>

    <div style="margin-bottom:10px">&nbsp;</div>

    <div style="text-align:center">
        <button runat="server" class="btn btn‐danger" id="Button2" name="Button2" type="button">Back</button>
        <span style="margin-left:30px">
            <button runat="server" class="btn btn‐default" id="Button1" name="Button1" type="button">Accept</button>
        </span>
    </div>
</asp:Content>