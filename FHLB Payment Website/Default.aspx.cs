﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FHLB.ClearPoint;

namespace FHLB
{
    public partial class Default : MasterPage
    {
        /// <summary>
        /// Initialize the new class.
        /// </summary>
        public Default() : base()
        {
            this.Init += Page_Init;
            this.Load += Page_Load;
        }

        /// <summary>
        /// Handle the page initialization after the controls are initialized but not loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Init(object sender, EventArgs e)
        {
            SubmitPanel1.ServerClick += SubmitPanel1_Click;
        }

        /// <summary>
        /// Confirmation to the email address. Also not important. But, we check it against the email address for a match.
        /// </summary>
        string input_email_confirm = string.Empty;

        /// <summary>
        /// Process the page load sequence. This is after the controls are initialized but before the page is rendered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // If the page is "GET" and the timeout is indicated then enable the form.
                string value = Request.QueryString["t"] ?? string.Empty;
                if (value == "1")
                {
                    ClientScript.RegisterStartupScript(GetType(), "Javascript", "javascript:showDialog(); ", true);
                }

                // Clear the input fields
                firm_name.Text         =
                input_firm_name        =
                email.Text             =
                phone_number.Text      =
                input_phone_number     =
                email.Text             =
                email_confirm.Text     =
                routing_number         =
                account_number         =
                input_email            = string.Empty;
                input_NumberOfPayments = 0;
                input_checking_savings = 'C';

                // Clear the input items
                write_input_items(create_input_items());
            }
        }

        /// <summary>
        /// Handle the click event on the first panel input form
        /// </summary>
        /// <param name="sender">Button clicked</param>
        /// <param name="e">System Event Arguments</param>
        private void SubmitPanel1_Click(object sender, EventArgs e)
        {
            // Validate the input form events
            var sb = new System.Text.StringBuilder();

            // Gather the firm name
            try
            {
                input_firm_name = firm_name.Text.Trim();
                if (string.IsNullOrEmpty(input_firm_name))
                {
                    sb.Append("<li>The firm name must be specified</li>");
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // Gather the telephone number
            try
            {
                input_phone_number = System.Text.RegularExpressions.Regex.Replace(phone_number.Text, @"\D", string.Empty);
                if (input_phone_number.Length != 10)
                {
                    sb.Append("<li>Please use the full telephone number, including the area code. Do not include an extension number.</li>");
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // Gather the email address
            try
            {
                input_email = email.Text.Trim();
                if (string.IsNullOrEmpty(input_email))
                {
                    sb.Append("<li>The email address must be specified</li>");
                }
                else
                {
                    var cls = new IsEMail();
                    if (!cls.IsEmailValid(input_email))
                    {
                        foreach (var item in cls.ResultInfo)
                        {
                            sb.AppendFormat("<li>Email Address: {0}</li>", item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // Gather the email comparison address
            try
            {
                input_email_confirm = email_confirm.Text.Trim();
                if (string.IsNullOrEmpty(input_email_confirm))
                {
                    sb.Append("<li>The email validation address must be specified</li>");
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // Gather the number of payments
            try
            {
                var str = NumberOfPayments.Text.Trim();
                if (!(new string[] { "1", "2", "3", "4", "5" }).Contains(str))
                {
                    sb.Append("<li>The number of payments must be specified</li>");
                }
                input_NumberOfPayments = Int32.Parse(str);
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // Gather the payment method
            try
            {
                input_PaymentMethod = PaymentMethod.Text.Trim();
                if (!(new string[] { "AC" }).Contains(input_PaymentMethod))
                {
                    sb.Append("<li>The payment method must be specified</li>");
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", ex.Message);
            }

            // If the email addresses do not match then say so
            if (string.Compare(input_email_confirm, input_email, true) != 0)
            {
                sb.Append("<li>The two email addresses do not match</li>");
            }

            // If there are errors then log them
            if (sb.Length > 0)
            {
                this.Panel1_Errors.Text = string.Format("<p class=\"error\">Problems with the input:<br/><ul class=\"error\">{0}</ul></p>", sb.ToString());
                return;
            }

            // Transfer to the payments page
            Response.Redirect("~/Payments.aspx", true);
        }
    }
}