﻿<%@ Page Title="Information Saved" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="FHLB.Receipt"%>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">

    <p style="text-align:center; margin-bottom:12pt">
        Congratulations!<br />
        Information has been saved
    </p>

    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tbody>
            <%=print_AccountInfo%>
            <tr>
                <td class="nowrap">Closing Agency's Firm Name:</td>
                <td class="nowrap"><%=print_firm_name%></td>
            </tr>
            <tr>
                <td class="nowrap">Closing Agency's Firm Phone Number:</td>
                <td class="nowrap"><%=print_phone_number%></td>
            </tr>
            <tr>
                <td class="nowrap">Payment Method:</td>
                <td class="nowrap">Bank Draft</td>
            </tr>
        </tbody>
    </table>

    <p style="padding-top:12px">
        Your payment of <b><%=print_totalamount%></b> will be made electronically to Credability on <b><%=print_transaction_date%></b> from your <%=print_account_type%> account (XXXX<%=print_account_number%>).
    </p>

    <p style="padding-top:12px">
        Please make a note of the following:
    </p>

    <ul style="padding-top:12px">
        <li>Your confirmation number for this transaction is <b><%=print_transaction_id%></b>.</li>
        <li>Please <b>print</b> this page for your records.</li>
        <li>Remember to record this transaction in your account register and subtract <b><%=print_totalamount%></b> from the account balance.</li>
    </ul>

    <p style="padding-top:12px">
        Thanks again for using <b><i>Online Payment</i></b> - the simple and secure way to make your payments to Credability!
    </p>

    <div class="no-print">
        <div style="padding-top:12px; text-align:center">
        
            <p style="padding-bottom:12px">
                <a href="javascript:window.print()">click here to print this page</a>
            </p>
            
            <p style="padding-bottom:12px">
                <a href="/">Return to the front page</a>
            </p>
            
        </div>
    </div>
</asp:Content>
