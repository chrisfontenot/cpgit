﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.names")]
        public partial class name
        {
            [Column(Name = "name", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string Prefix { get; set; }
            [Column] public string First { get; set; }
            [Column] public string Middle { get; set; }
            [Column] public string Last { get; set; }
            [Column] public string Suffix { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public name()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Prefix = string.Empty;
                First  = string.Empty;
                Middle = string.Empty;
                Last   = string.Empty;
                Suffix = string.Empty;
            }

            public override string ToString()
            {
                var sb = new System.Text.StringBuilder();
                foreach (var str in new string[] { Prefix, First, Middle, Last, Suffix })
                {
                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        sb.AppendFormat(" {0}", str);
                    }
                }
                return sb.ToString().Trim();
            }

            /// <summary>
            /// Parse the input string to the various fields of the record.
            /// </summary>
            /// <param name="namestring">String to be parsed</param>
            /// <returns>TRUE if the parsing is successful. FALSE otherwise</returns>
            public bool parse(string namestring)
            {
                // This version is a simplified variant of the DebtPlus name record parser. It does not handle the
                // prefix and suffix entries.

                Prefix = string.Empty;
                Suffix = string.Empty;

                if (string.IsNullOrWhiteSpace(namestring))
                {
                    First = string.Empty;
                    Middle = string.Empty;
                    Last = string.Empty;
                    return true;
                }

                // Try to split the name into first and last component pieces
                var m = System.Text.RegularExpressions.Regex.Match(namestring, @"^(?<first>(?:[^ ]+)+)\s*(?<middle>(?:(?!(?:EL|DE|LA)\s)[A-Z\.]+\s?)+)?\s+(?<last>[A-Z\-‌​\s]+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                if (m.Success)
                {
                    First  = m.Groups["first"].Value;
                    Middle = m.Groups["middle"].Value;
                    Last   = m.Groups["last"].Value;
                    return true;
                }

                m = System.Text.RegularExpressions.Regex.Match(namestring, @"^(?<first>(?:(?!(?:EL|DE|LA)\s)[A-Z\.]+\s?)+)\s+(?<last>[A-Z\-‌​\s]+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (m.Success)
                {
                    Middle = string.Empty;
                    First  = m.Groups["first"].Value;
                    Last   = m.Groups["last"].Value;
                    return true;
                }

                First  = string.Empty;
                Middle = string.Empty;
                Last   = namestring;
                return true;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<name> nameTable
            {
                get
                {
                    return context.GetTable<name>();
                }
            }
        }
    }
}