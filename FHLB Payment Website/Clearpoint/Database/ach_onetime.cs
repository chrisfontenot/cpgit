﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.ach_onetimes")]
        public class ach_onetime
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)] public Int32 Id { get; set; }
            [Column] public Int32 client { get; set; }
            [Column] public Int32 bank { get; set; }
            [Column] public String batch_type { get; set; }
            [Column] public System.Nullable<Int32> deposit_batch_id { get; set; }
            [Column] public System.Nullable<Int32> deposit_batch_date { get; set; }
            [Column] public String ABA { get; set; }
            [Column] public String AccountNumber { get; set; }
            [Column] public Char CheckingSavings { get; set; }
            [Column] public Decimal Amount { get; set; }
            [Column] public DateTime EffectiveDate { get; set; }
            [Column] public System.Nullable<Int32> client_product { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "DateTime NOT NULL", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public System.DateTime date_created { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "VarChar(80) NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public string created_by { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }
            [Column] public String temp_fhlb_firm_name { get; set; }
            [Column] public String temp_fhlb_email { get; set; }
            [Column] public String temp_fhlb_firm_phone { get; set; }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<ach_onetime> ach_onetimesTable
            {
                get
                {
                    return context.GetTable<ach_onetime>();
                }
            }
        }
    }
}
