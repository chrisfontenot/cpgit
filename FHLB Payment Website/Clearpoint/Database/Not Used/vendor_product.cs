﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendor_products")]
        public partial class vendor_product
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public Int32 vendor { get; set; }
            [Column] public Int32 product { get; set; }
            [Column] public Int32 EscrowProBono { get; set; }

            public vendor_product()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                vendor        = default(Int32);
                product       = default(Int32);
                EscrowProBono = 0;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor_product> vendor_productTable
            {
                get
                {
                    return context.GetTable<vendor_product>();
                }
            }
        }
    }
}