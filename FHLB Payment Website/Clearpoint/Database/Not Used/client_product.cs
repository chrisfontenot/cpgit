﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.client_products")]
        public partial class client_product
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public Int32 client { get; set; }
            [Column] public Int32 vendor { get; set; }
            [Column] public Int32 product { get; set; }
            [Column] public string message { get; set; }
            [Column] public string reference { get; set; }
            [Column] public decimal cost { get; set; }
            [Column] public decimal discount { get; set; }
            [Column] public decimal tendered { get; set; }
            [Column] public decimal disputed_amount { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<DateTime> disputed_date { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<Int32> disputed_status { get; set; }
            [Column] public string disputed_note { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<DateTime> resolution_date { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<Int32> resolution_status { get; set; }
            [Column] public string resolution_note { get; set; }
            [Column(CanBeNull=true)] public System.Nullable<Int32> invoice_status { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<DateTime> invoice_date { get; set; }
            [Column] public Boolean ActiveFlag { get; set; }

            [Column(AutoSync = AutoSync.OnInsert, DbType = "datetime NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public DateTime date_created { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "varchar(50) NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public string created_by { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public client_product()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                cost            = 0M;
                discount        = 0M;
                tendered        = 0M;
                disputed_amount = 0M;
                ActiveFlag      = true;
                invoice_status  = 0;
                invoice_date    = null;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<client_product> client_productTable
            {
                get
                {
                    return context.GetTable<client_product>();
                }
            }
        }
    }
}