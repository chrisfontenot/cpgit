﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendor_addresses")]
        public partial class vendor_address
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public int vendor { get; set; }
            [Column] public char address_type { get; set; }
            [Column] public string attn { get; set; }
            [Column] public string line_1 { get; set; }
            [Column] public string line_2 { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> addressID { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public vendor_address()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                vendor = default(Int32);
                address_type = 'G';
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor_address> vendor_addressTable
            {
                get
                {
                    return context.GetTable<vendor_address>();
                }
            }
        }
    }
}