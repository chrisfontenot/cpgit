﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendor_notes")]
        public partial class vendor_note
        {
            public vendor_note()
            {
                OnCreated();
            }

            [Column(Name = "oID", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
            public int Id { get; set; }
            [Column()]
            public int vendor { get; set; }
            [Column()]
            public int type { get; set; }
            [Column()]
            public System.Nullable<System.DateTime> expires { get; set; }
            [Column()]
            public bool dont_edit { get; set; }
            [Column()]
            public bool dont_delete { get; set; }
            [Column()]
            public bool dont_print { get; set; }
            [Column()]
            public string subject { get; set; }
            [Column()]
            public string note { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "DateTime NOT NULL", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
            public System.DateTime date_created { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "VarChar(80) NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
            public string created_by { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
            public System.Data.Linq.Binary ts { get; set; }

            private void OnCreated()
            {
                created_by   = string.Empty;
                date_created = default(DateTime);
                dont_delete  = false;
                dont_edit    = false;
                dont_print   = false;
                expires      = null;
                Id           = default(Int32);
                note         = string.Empty;
                subject      = string.Empty;
                type         = 1;
                vendor       = default(Int32);
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor_note> vendor_noteTable
            {
                get
                {
                    return context.GetTable<vendor_note>();
                }
            }
        }
    }
}