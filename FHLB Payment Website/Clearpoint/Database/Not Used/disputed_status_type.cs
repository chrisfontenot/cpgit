﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.disputed_status_types")]
        public partial class disputed_status_type
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string description { get; set; }
            [Column] public Boolean want_note { get; set; }
            [Column] public Boolean ActiveFlag { get; set; }
            [Column] public Boolean @Default { get; set; }

            public disputed_status_type()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                @Default    = false;
                ActiveFlag  = true;
                want_note   = false;
                description = string.Empty;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<disputed_status_type> disputed_status_typeTable
            {
                get
                {
                    return context.GetTable<disputed_status_type>();
                }
            }
        }
    }
}