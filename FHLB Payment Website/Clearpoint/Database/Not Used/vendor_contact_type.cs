﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendor_contact_types")]
        public partial class vendor_contact_type
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string description { get; set; }
            [Column] public Boolean ActiveFlag { get; set; }
            [Column] public Boolean @Default { get; set; }

            public vendor_contact_type()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                @Default    = false;
                ActiveFlag  = true;
                description = string.Empty;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor_contact_type> vendor_contact_typeTable
            {
                get
                {
                    return context.GetTable<vendor_contact_type>();
                }
            }
        }
    }
}