﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.products")]
        public partial class product
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string description { get; set; }
            [Column] public decimal Amount { get; set; }
            [Column] public Boolean ActiveFlag { get; set; }
            [Column] public Boolean @Default { get; set; }

            public product()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                ActiveFlag  = true;
                @Default    = false;
                description = string.Empty;
                Amount      = 0M;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<product> productTable
            {
                get
                {
                    return context.GetTable<product>();
                }
            }
        }
    }
}