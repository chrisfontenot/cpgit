﻿using System;
using System.Data.Linq.Mapping;

namespace FHLB.ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.client_appointments")]
        public class client_appointment
        {
            [Column(Name = "client_appointment", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)] public Int32 Id { get; set; }
            [Column] public int Client { get; set; }
            [Column] public System.Nullable<Int32> appt_type { get; set; }
            [Column] public System.Char status { get; set; }
            [Column] public string partner { get; set; }

#if false // Ignore columns that we don't use for this project
            [Column] public System.Nullable<Int32> appt_time { get; set; }
            [Column] public System.Nullable<Int32> counselor { get; set; }
            [Column] public DateTime start_time { get; set; }
            [Column] public System.Nullable<DateTime> end_time { get; set; }
            [Column] public System.Nullable<Int32> office { get; set; }
            [Column] public System.Nullable<Int32> workshop { get; set; }
            [Column] public System.Nullable<Int32> workshop_people { get; set; }
            [Column] public System.String result { get; set; }
            [Column] public System.Nullable<Int32>  previous_appointment { get; set; }
            [Column] public System.Nullable<Int32> referred_to { get; set; }
            [Column] public System.Nullable<Int32> referred_by { get; set; }
            [Column] public Int32  bankruptcy_class { get; set; }
            [Column] public Boolean priority { get; set; }
            [Column] public String callback_ph { get; set; }
            [Column] public Decimal HousingFeeAmount { get; set; }
            [Column] public Int32 confirmation_status { get; set; }
            [Column] public System.Nullable<DateTime> date_confirmed { get; set; }
            [Column] public System.Nullable<DateTime>  date_uploaded { get; set; }
            [Column] public DateTime date_updated { get; set; }
            [Column] public string created_by { get; set; }
            [Column] public System.DateTime date_created { get; set; }
            [Column] public System.Data.Linq.Binary ts { get; set; }
#endif
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<client_appointment> client_appointmentsTable
            {
                get
                {
                    return context.GetTable<client_appointment>();
                }
            }
        }
    }
}
