﻿<%@ Page Title="Payments" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Payments.aspx.cs" Inherits="FHLB.Payments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        var idleSeconds = 300; // 300 seconds or 5 minutes
        $(function () {
            var idleTimer;
            function resetTimer() {
                clearTimeout(idleTimer);
                idleTimer = setTimeout(whenUserIdle, idleSeconds * 1000);
            }
            $(document.body).bind('mousemove keydown click', resetTimer); //space separated events list that we want to monitor
            resetTimer(); // Start the timer when the page loads
        });

        function whenUserIdle() {
            document.location = "./Default.aspx?t=1";
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- Payment information -->
    <asp:Table ID="Table3" runat="server" CssClass="wide" Visible="true">
        <asp:TableRow>
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell ColumnSpan="2">
                <p>
                    <span class="error">
                        <asp:Label runat="server" ID="ErrorList2"></asp:Label>
                    </span>
                </p>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V1_A">
            <asp:TableCell/>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="V1_B">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V1_reference" Text="Reservation Number:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V1_reference" TextMode="SingleLine" Columns="40" AutoPostBack="true"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V1_C">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V1_fname" Text="Applicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V1_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V1_D">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V1_lname" Text="Applicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V1_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V1_E">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V1_co_fname" Text="CoApplicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V1_co_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V1_F">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V1_co_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V1_co_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V2_A">
            <asp:TableCell/>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="V2_B">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V2_reference" Text="Reservation Number:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V2_reference" TextMode="SingleLine" Columns="40" AutoPostBack="true"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V2_C">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V2_fname" Text="Applicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V2_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V2_D">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V2_lname" Text="Applicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V2_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V2_E">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V2_co_fname" Text="CoApplicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V2_co_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V2_F">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V2_co_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V2_co_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V3_A">
            <asp:TableCell>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="V3_B">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V3_reference" Text="Reservation Number:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V3_reference" TextMode="SingleLine" Columns="40" AutoPostBack="true"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V3_C">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V3_fname" Text="Applicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V3_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V3_D">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V3_lname" Text="Applicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V3_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V3_E">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V3_co_fname" Text="CoApplicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V3_co_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V3_F">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V3_co_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V3_co_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V4_A">
            <asp:TableCell>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="V4_B">
            <asp:TableCell runat="server" class="spacer nopad">
            </asp:TableCell>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V4_reference" Text="Reservation Number:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V4_reference" TextMode="SingleLine" Columns="40" AutoPostBack="true"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V4_C">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V4_fname" Text="Applicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V4_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V4_D">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V4_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V4_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V4_E">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V4_co_fname" Text="CoApplicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V4_co_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V4_F">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V4_co_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V4_co_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V5_A">
            <asp:TableCell>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow runat="server" ID="V5_B">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V5_reference" Text="Reservation Number:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V5_reference" TextMode="SingleLine" Columns="40" AutoPostBack="true"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V5_C">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V5_fname" Text="Applicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V5_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V5_D">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V5_lname" Text="Applicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V5_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V5_E">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlID="V5_co_fname" Text="CoApplicant First Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V5_co_fname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="V5_F">
            <asp:TableCell runat="server" class="spacer nopad"/>
            <asp:TableCell runat="server" class="nopad lbl">
                <asp:Label runat="server" AssociatedControlIDr="V5_co_lname" Text="CoApplicant Last Name:"></asp:Label>
            </asp:TableCell>
            <asp:TableCell runat="server">
                <asp:TextBox runat="server" ID="V5_co_lname" TextMode="SingleLine" Columns="40"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
            </asp:TableCell>
            <asp:TableCell ColumnSpan="2" CssClass="lbl">
                <hr />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow1" runat="server" CssClass="nopad">
            <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell>
            <asp:TableCell CssClass="nopad lbl">Routing Number:</asp:TableCell>
            <asp:TableCell CssClass="nopad field">
                <asp:TextBox ID="RoutingNumber" runat="server" Columns="40" MaxLength="9"/>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" CssClass="nopad">
            <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell><asp:TableCell CssClass="nopad lbl">Account Number:</asp:TableCell><asp:TableCell CssClass="nopad field">
                <asp:TextBox ID="AccountNumber" runat="server" Columns="40" MaxLength="80"/>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" CssClass="nopad">
            <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell><asp:TableCell CssClass="nopad lbl">Account Type:</asp:TableCell><asp:TableCell CssClass="nopad field">
                <asp:DropDownList ID="AccountType" runat="server">
                    <asp:ListItem Selected="True" Value="C" Text="Checking"/>
                    <asp:ListItem Selected="false" Value="S" Text="Savings"/>
                </asp:DropDownList>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" CssClass="nopad">
            <asp:TableCell CssClass="nopad spacer">&nbsp;</asp:TableCell><asp:TableCell CssClass="nopad lbl">Total Amount:</asp:TableCell><asp:TableCell CssClass="nopad field">
                <span style="font-weight:bold">
                    <asp:Literal ID="total_amount" runat="server"/>
                </span>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server" CssClass="nopad">
            <asp:TableCell ColumnSpan="3">&nbsp;</asp:TableCell></asp:TableRow><asp:TableRow runat="server" CssClass="nopad">
            <asp:TableCell ColumnSpan="2"/>
            <asp:TableCell CssClass="nopad field">
                <asp:Button CssClass="button" ID="SubmitPanel2" runat="server" CommandName="SubmitPanel2" Text="Submit"></asp:Button>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
