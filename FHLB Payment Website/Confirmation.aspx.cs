﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FHLB.ClearPoint;

namespace FHLB
{
    public partial class Confirmation : MasterPage
    {
        /// <summary>
        /// Initialize the new page in the system. This occurs after the page controls are created but before they are loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Hook into the events to process the clicks on the submit buttons.
            Button1.ServerClick += SubmitPanel3_Click;
            Button2.ServerClick += CancelPanel3_Click;
        }

        /// <summary>
        /// Process the page load sequence. This is after the controls are initialized but before the page is rendered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure that the first page has been used and has data
            if (string.IsNullOrEmpty(input_email))
            {
                Response.Redirect("~/Default.aspx", true);
                return;
            }

            if (string.IsNullOrEmpty(account_number))
            {
                Response.Redirect("~/Payments.aspx", true);
            }

            // Build the confirmation status
            this.ConfirmationTable.Text = BuildConfirmation();
        }

        /// <summary>
        /// Process the post-back to translate the reference number to a control item and update the resulting page.
        /// </summary>
        /// <param name="db">Pointer to the database context</param>
        /// <param name="item">Pointer to the item to be updated</param>
        /// <param name="input_reference">Reference control on the page</param>
        /// <param name="input_applicant_first">Applicant first name control on the page</param>
        /// <param name="input_applicant_last">Applicant last name control on the page</param>
        /// <param name="input_coapplicant_first">Co-Applicant first name control on the page</param>
        /// <param name="input_coapplicant_last">Co-Applicant last name control on the page</param>
        private void collectItems(DebtPlus.DatabaseContext db, InputItem item, System.Web.UI.WebControls.TextBox input_reference, System.Web.UI.WebControls.TextBox input_applicant_first, System.Web.UI.WebControls.TextBox input_applicant_last, System.Web.UI.WebControls.TextBox input_coapplicant_first, System.Web.UI.WebControls.TextBox input_coapplicant_last)
        {
            if (item == null)
            {
                return;
            }

            // Find the reference string. If they are the same then just leave the input field alone.
            string refnce = System.Text.RegularExpressions.Regex.Replace(input_reference.Text, @"[^A-Za-z0-9]", string.Empty);
            if (refnce == item.reference || refnce == string.Empty)
            {
                return;
            }

            // Empty the information about the record
            item.client             =
            item.client_appointment = 0;
            item.applicant_last     =
            item.applicant_first    =
            item.coapplicant_first  =
            item.coapplicant_last   = string.Empty;

            // Find the client appointment associated with this reference field
            var qappt = db.client_appointmentsTable.Where(s => s.partner == refnce && (s.status == 'K' || s.status == 'W') && s.appt_type == fhlb_appointment_type).FirstOrDefault();
            if (qappt != null)
            {
                item.client_appointment = qappt.Id;
                item.client             = qappt.Client;
                item.reference          =
                input_reference.Text    = refnce;

                // Find the names
                var colNames = (from p in db.PeopleTable
                                join n in db.nameTable on p.NameID equals n.Id
                                where p.Client == qappt.Client
                                select new { p = p, n = n }).ToList();

                if (colNames.Count > 0)
                {
                    var primary = colNames.Find(s => s.p.Relation == 1);
                    if (primary != null)
                    {
                        item.applicant_first = primary.n.First;
                        item.applicant_last = primary.n.Last;
                    }

                    var secondary = colNames.Where(s => s.p.Relation != 1).OrderBy(s => s.p.Relation).FirstOrDefault();
                    if (secondary != null)
                    {
                        item.coapplicant_first = secondary.n.First;
                        item.coapplicant_last = secondary.n.Last;
                    }
                }
            }

            // Update the controls with the values
            input_applicant_first.Text   = item.applicant_first;
            input_applicant_last.Text    = item.applicant_last;
            input_coapplicant_first.Text = item.coapplicant_first;
            input_coapplicant_last.Text  = item.coapplicant_last;
        }

        /// <summary>
        /// Construct the table with the confirmation information. It is too fixed of a table
        /// and has too many issues to use a repeater or some other helpful tool so just generate the HTML
        /// text directly here and paste it into the resulting page.
        /// </summary>
        /// <returns></returns>
        private string BuildConfirmation()
        {
            var sb = new System.Text.StringBuilder();
            sb.Append("<table class=\"nopad\"><tbody>");

            decimal totalAmount = 0M;
            foreach (var item in read_input_items())
            {
                if (item.id > input_NumberOfPayments)
                {
                    continue;
                }

                totalAmount += item.amount;

                sb.AppendFormat("<tr>");
                sb.Append("<th class=\"nowrap nopad bold lbl\">Reservation Number:</th>");
                sb.AppendFormat("<td class=\"nopad\">{0}</td>", item.reference);
                sb.AppendFormat("</tr>");

                sb.AppendFormat("<tr>");
                sb.Append("<th class=\"nowrap nopad bold lbl\">Homebuyer's Name:</th>");
                sb.AppendFormat("<td class=\"nopad\">{0}, {1}</td>", Server.HtmlEncode(item.applicant_last), Server.HtmlEncode(item.applicant_first));
                sb.AppendFormat("</tr>");

                if (! string.IsNullOrWhiteSpace(item.coapplicant_last))
                {
                    sb.AppendFormat("<tr>");
                    sb.Append("<th class=\"nowrap nopad bold lbl\">Spouse's Name:</th>");
                    sb.AppendFormat("<td class=\"nopad\">{0}, {1}</td>", Server.HtmlEncode(item.coapplicant_last), Server.HtmlEncode(item.coapplicant_first));
                    sb.AppendFormat("</tr>");
                }
            }

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Closing Agency's Firm Name:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", Server.HtmlEncode(input_firm_name));
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Closing Agency's Firm Phone Number:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", Server.HtmlEncode(FormatPhone(input_phone_number)));
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Closing Agency's Firm Email Address:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", Server.HtmlEncode(input_email));
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Payment Method:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", "Bank Draft");
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Account Type:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", input_checking_savings == 'S' ? "Savings" : "Checking");
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Routing Number:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", Server.HtmlEncode(routing_number));
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Account Number:</th>");
            sb.AppendFormat("<td class=\"nopad\">{0}</td>", Server.HtmlEncode(account_number));
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Payment Amount:</th>");
            sb.AppendFormat("<td class=\"nopad bold\">{0:c}</td>", totalAmount);
            sb.AppendFormat("</tr>");

            sb.AppendFormat("<tr>");
            sb.Append("<th class=\"nowrap nopad bold lbl\">Transaction Date:</th>");
            sb.AppendFormat("<td class=\"nopad bold\">{0:d}</td>", System.DateTime.Now);
            sb.AppendFormat("</tr>");

            sb.Append("</tbody></table>");

            // Return the resulting confirmation table
            return sb.ToString();
        }

        /// <summary>
        /// The cancel button will go back to the main page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelPanel3_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", true);
        }

        /// <summary>
        /// Limit the length of an input string to the maximum allowed in the database.
        /// </summary>
        /// <param name="inputString">Input string to be used</param>
        /// <param name="maxLength">Maximum number of characters allowed in the string</param>
        /// <returns></returns>
        private static string maxString(string inputString, Int32 maxLength)
        {
            if (maxLength < 1)
            {
                throw new ArgumentException("maxlength < 1");
            }

            if (string.IsNullOrEmpty(inputString))
            {
                return inputString;
            }

            if (inputString.Length > maxLength)
            {
                return inputString.Substring(0, maxLength);
            }

            return inputString;
        }

        /// <summary>
        /// Process the submit button on the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitPanel3_Click(object sender, EventArgs e)
        {
            // Send the email messages
            SendFirmNotice();
            SendAgencyNotice();

            // Insert the payment information into the tables
            using (var db = new DebtPlus.DatabaseContext())
            {
                foreach (var item in read_input_items())
                {
                    if (item.id > input_NumberOfPayments)
                    {
                        continue;
                    }

                    // Generate the payment information for the client in the ACH tables
                    var rec                  = new DebtPlus.ach_onetime();
                    rec.client               = item.client.GetValueOrDefault();
                    rec.Amount               = item.amount;
                    rec.ABA                  = routing_number;
                    rec.AccountNumber        = account_number;
                    rec.CheckingSavings      = input_checking_savings;
                    rec.EffectiveDate        = Tomorrow();
                    rec.bank                 = default_bank;
                    rec.batch_type           = default_batch_type;

                    // These are junk columns which should be relocated someplace else
                    rec.temp_fhlb_email      = maxString(input_email, 256);
                    rec.temp_fhlb_firm_name  = maxString(input_firm_name, 80);
                    rec.temp_fhlb_firm_phone = maxString(input_phone_number, 50);

                    db.ach_onetimesTable.InsertOnSubmit(rec);
                }

                // Add the records to reflect the payment information
                db.SubmitChanges();
            }

            // Change to the completion panel
            Response.Redirect("Receipt.aspx", true);
        }

        /// <summary>
        /// Determine if the input items are valid.
        /// </summary>
        /// <param name="colItems">Collection to be validated</param>
        /// <returns>TRUE if acceptable. FALSE if incomplete.</returns>
        private bool valid_items(System.Collections.Generic.List<InputItem> colItems)
        {
            foreach (var item in colItems)
            {
                // Ignore items that are outside the range of suitable entries
                if (item.id > input_NumberOfPayments)
                {
                    continue;
                }

                // We need the client identifier (which means that we need the appointment
                // identifier which means that we need the case ID to be valid.)
                if (item.client.GetValueOrDefault() <= 0)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Validate the ABA check-digit
        /// </summary>
        private static bool ValidABAChecksum(string AbaNumber)
        {
            // It must be exactly nine digits
            if (AbaNumber.Length != 9)
            {
                return false;
            }

            int sum = 0;
            int[] scale = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };
            for (int index = 0; index < 9; ++index)
            {
                // Characters other than digits are always invalid.
                char chr = AbaNumber[index];
                if (!char.IsDigit(chr))
                {
                    return false;
                }

                // Compute the scaled checksum value
                int code = ((int)chr) - 48;
                sum += (code * scale[index]);
            }

            // The result must be divisible by 10
            return (sum % 10) == 0;
        }

        /// <summary>
        /// Send a copy of the receipt information to the paying agency
        /// </summary>
        private void SendAgencyNotice()
        {
            SendNotice("AgencyNotice");
        }

        /// <summary>
        /// Send an email address to the people at Clearpoint
        /// </summary>
        private void SendFirmNotice()
        {
            SendNotice("FirmNotice");
        }

        /// <summary>
        /// Send out an email address to the destination system
        /// </summary>
        /// <param name="SectionKey"></param>
        /// <param name="input_items"></param>
        /// <param name="revised_to"></param>
        private void SendNotice(string SectionKey)
        {
            // Find the configuration information in the web.config file
            var cnf = System.Configuration.ConfigurationManager.GetSection("EmailMessageConfigurationSection") as FHLB.ClearPoint.Configuration.EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var section = cnf.MailDefinitions.Where(s => string.Compare(s.Event, SectionKey, true) == 0).FirstOrDefault();
            if (section == null || string.IsNullOrWhiteSpace(section.To))
            {
                return;
            }

            // Construct the table with the various fields included
            var sb = new System.Text.StringBuilder();
            var dict = new System.Collections.Generic.Dictionary<string, string>();

            foreach (var item in read_input_items())
            {
                if (item.id > input_NumberOfPayments)
                {
                    continue;
                }

                sb.AppendFormat("<tr>");
                sb.Append("<td>Reservation Number:</td>");
                sb.AppendFormat("<td>{0}</td>", item.reference);
                sb.AppendFormat("</tr>");

                sb.AppendFormat("<tr>");
                sb.Append("<td>Homebuyer's Name:</td>");
                sb.AppendFormat("<td>{0}, {1}</td>", Server.HtmlEncode(item.applicant_last), Server.HtmlEncode(item.applicant_first));
                sb.AppendFormat("</tr>");

                if (!string.IsNullOrWhiteSpace(item.coapplicant_last))
                {
                    sb.AppendFormat("<tr>");
                    sb.Append("<td>Spouse's Name:</td>");
                    sb.AppendFormat("<td>{0}, {1}</td>", Server.HtmlEncode(item.coapplicant_last), Server.HtmlEncode(item.coapplicant_first));
                    sb.AppendFormat("</tr>");
                }
            }

            // The first item is the table portion listing the payment information
            dict.Add("<%AccountInfo%>", sb.ToString());

            // Build the remainder of the substitution dictionary
            dict.Add("<%firm_name%>", input_firm_name);
            dict.Add("<%phone_number%>", FormatPhone(input_phone_number));
            dict.Add("<%totalamount%>", string.Format("{0:c}", input_paymentamount));
            dict.Add("<%transaction_date%>", Tomorrow().ToShortDateString());
            dict.Add("<%account_type%>", input_checking_savings == 'S' ? "savings" : "checking");
            dict.Add("<%transaction_id%>", transaction_ID.ToString("0000000000"));
            dict.Add("<%email%>", input_email);

            // The account number is just the last 4 digits of the value.
            var tempStr = "XXXXX" + (account_number ?? "");
            tempStr = tempStr.Substring(tempStr.Length - 4);
            dict.Add("<%account_number%>",tempStr);

            // Correct the target email address
            string target = section.To;
            if (string.Compare(target, "FIRM", false) == 0)
            {
                target = input_email;
            }

            // Build the message to be sent.
            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var mailDefinition = section.CreateMailDefinition();
                using (var msg = mailDefinition.CreateMailMessage(target, dict, this))
                {
                    // Include the transaction ID in the subject line if needed.
                    msg.Subject = msg.Subject.Replace("<%transaction_id%>", transaction_ID.ToString("0000000000"));

                    // Finally, send the message to the relay host.
                    smtp.Send(msg);
                }
            }
        }
    }
}