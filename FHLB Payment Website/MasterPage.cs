﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FHLB
{
    public partial class MasterPage : System.Web.UI.Page
    {
        // Type of the FHLB appointments
        protected Int32 fhlb_appointment_type = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["appt_type"] ?? "155");
        protected Int32 default_bank = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["bank"] ?? "4");
        protected string default_batch_type = System.Configuration.ConfigurationManager.AppSettings["batch_type"] ?? "TEL";
        protected decimal item_amount = Decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["item_amount"] ?? "175");

        /// <summary>
        /// Class to hold the input field items from the form. There may be up to 5 entries in the list.
        /// </summary>
        public class InputItem
        {
            public int id { get; set; }
            public string key
            {
                get
                {
                    return string.Format("V{0:f0}", id);
                }
            }

            public string reference { get; set; }
            public decimal amount { get; set; }
            public string applicant_first { get; set; }
            public string applicant_last { get; set; }

            public string coapplicant_first { get; set; }
            public string coapplicant_last { get; set; }

            public Int32? client { get; set; }
            public Int32? client_appointment { get; set; }
            public DateTime? appointment_date { get; set; }
        }

        /// <summary>
        /// Bank routing number. This is the ABA bank reference ID and is validated for a proper checksum.
        /// </summary>
        protected string routing_number
        {
            get
            {
                return (string)Session["routing_number"];
            }
            set
            {
                Session["routing_number"] = value;
            }
        }

        /// <summary>
        /// Bank account number. This is validated only that there is a numerical entry.
        /// </summary>
        protected string account_number
        {
            get
            {
                return (string)Session["account_number"];
            }
            set
            {
                Session["account_number"] = value;
            }
        }

        /// <summary>
        /// Type of account for ACH. It is either a checking or savings account.
        /// </summary>
        protected System.Char input_checking_savings
        {
            get
            {
                var obj = Session["input_checking_savings"];
                if (obj is System.Char)
                {
                    if (((System.Char)obj) == 'S')
                    {
                        return 'S';
                    }
                }

                return 'C';
            }
            set
            {
                Session["input_checking_savings"] = value;
            }
        }

        /// <summary>
        /// Name of the paying firm. I do not understand why this is important.
        /// </summary>
        protected string input_firm_name
        {
            get
            {
                return (string)Session["input_firm_name"];
            }
            set
            {
                Session["input_firm_name"] = value;
            }
        }

        /// <summary>
        /// Telephone number of the paying firm. Again, this seems to be extraneous.
        /// </summary>
        protected string input_phone_number
        {
            get
            {
                return (string)Session["input_phone_number"];
            }
            set
            {
                Session["input_phone_number"] = value;
            }
        }

        /// <summary>
        /// Email address of the paying firm. This is also redundant.
        /// </summary>
        protected string input_email
        {
            get
            {
                return (string)Session["input_email"];
            }
            set
            {
                Session["input_email"] = value;
            }
        }

        /// <summary>
        /// Number of payment forms in the second panel. Stupid design, but it is what it is.
        /// </summary>
        protected Int32? input_NumberOfPayments
        {
            get
            {
                var obj = Session["input_NumberOfPayments"];
                if (obj == null)
                {
                    return null;
                }

                Int32 response = default(Int32);
                if (obj is Int32)
                {
                    return (Int32)obj;
                }

                if (Int32.TryParse((string)obj, out response))
                {
                    return response;
                }

                return null;
            }
            set
            {
                Session["input_NumberOfPayments"] = value;
            }
        }

        /// <summary>
        /// Payment method. This is only "Bank Draft" and "Bank Wire" was removed.
        /// </summary>
        protected string input_PaymentMethod
        {
            get
            {
                return (string)Session["input_PaymentMethod"];
            }
            set
            {
                Session["input_PaymentMethod"] = value;
            }
        }

        /// <summary>
        /// Current transaction ID for the payment information
        /// </summary>
        protected Int64 transaction_ID
        {
            get
            {
                object obj = Session["transaction_id"];
                if (obj != null)
                {
                    if (obj is Int64)
                    {
                        return (Int64)obj;
                    }

                    Int64 value;
                    if (Int64.TryParse((string)obj, out value))
                    {
                        return value;
                    }
                }

                return 0;
            }
            set
            {
                Session["transaction_id"] = value;
            }
        }

        /// <summary>
        /// Write the input control storage to the session table. We need to serialize the entries to do the write.
        /// </summary>
        /// <param name="colItems"></param>
        protected void write_input_items(System.Collections.Generic.List<InputItem> colItems)
        {
            if (colItems == null)
            {
                return;
            }

            var sb = new System.Text.StringBuilder();
            using (var mem = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(colItems.GetType());
                ser.Serialize(mem, colItems);
                mem.Flush();
                mem.Close();
            }

            Session["input_items"] = sb.ToString();
        }

        // List of input items
        private System.Collections.Generic.List<InputItem> input_items = null;

        /// <summary>
        /// De-serialize the entries from the session storage.
        /// </summary>
        /// <returns></returns>
        protected System.Collections.Generic.List<InputItem> read_input_items()
        {
            if (input_items != null)
            {
                return input_items;
            }

            var str = Session["input_items"];
            try
            {
                if (!string.IsNullOrEmpty((string)str))
                {
                    using (var mem = new System.IO.StringReader((string)str))
                    {
                        var ser = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<InputItem>));
                        input_items = (System.Collections.Generic.List<InputItem>)ser.Deserialize(mem);
                        return input_items;
                    }
                }
            }
            catch
            {
            }

            return null;
        }

        /// <summary>
        /// Create an empty list of entries. Used when the values can not be read from storage.
        /// </summary>
        /// <returns></returns>
        protected System.Collections.Generic.List<InputItem> create_input_items()
        {
            // Generate the transaction ID for the items
            transaction_ID = DateTime.Now.Ticks % 0x7FFFFFFF;

            // Create the resulting value
            var result = new System.Collections.Generic.List<InputItem>();
            for (int idx = 1; idx < 6; ++idx)
            {
                result.Add(new InputItem() { id = idx, amount = item_amount });
            }
            return result;
        }

        /// <summary>
        /// Total dollar amount of the transaction
        /// </summary>
        protected decimal input_paymentamount
        {
            get
            {
                var answer = 0M;
                var items = read_input_items();
                if (items != null)
                {
                    foreach (var item in items)
                    {
                        if (item.id > input_NumberOfPayments)
                        {
                            continue;
                        }
                        answer += item.amount;
                    }
                }
                return answer;
            }
        }

        /// <summary>
        /// Format the telephone number to a suitable string
        /// </summary>
        /// <param name="PhoneNumber"></param>
        /// <returns></returns>
        protected static string FormatPhone(string PhoneNumber)
        {
            var str = System.Text.RegularExpressions.Regex.Replace(PhoneNumber, @"\D", string.Empty);
            return string.Format("{0}-{1}-{2}", str.Substring(0, 3), str.Substring(3, 3), str.Substring(6));
        }

        /// <summary>
        /// Return the date for "tomorrow" when it does not fall on a weekend.
        /// </summary>
        /// <returns></returns>
        protected static DateTime Tomorrow()
        {
            var date = DateTime.Now.Date.AddDays(1);
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    date = date.AddDays(2);
                    break;

                case DayOfWeek.Sunday:
                    date = date.AddDays(1);
                    break;

                default:
                    break;
            }
            return date;
        }
    }
}