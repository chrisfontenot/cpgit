﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FHLB.ClearPoint;

namespace FHLB
{
    public partial class Payments : MasterPage
    {
        /// <summary>
        /// Initialize the new page in the system. This occurs after the page controls are created but before they are loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Hook into the events to process the clicks on the submit buttons.
            SubmitPanel2.Click += SubmitPanel2_Click;

            // Adjust the display on the second panel set as appropriate
            EnableView(new System.Web.UI.Control[] { V1_A, V1_B, V1_C, V1_D, V1_E, V1_F }, true);
            EnableView(new System.Web.UI.Control[] { V2_A, V2_B, V2_C, V2_D, V2_E, V2_F }, (input_NumberOfPayments >= 2));
            EnableView(new System.Web.UI.Control[] { V3_A, V3_B, V3_C, V3_D, V3_E, V3_F }, (input_NumberOfPayments >= 3));
            EnableView(new System.Web.UI.Control[] { V4_A, V4_B, V4_C, V4_D, V4_E, V4_F }, (input_NumberOfPayments >= 4));
            EnableView(new System.Web.UI.Control[] { V5_A, V5_B, V5_C, V5_D, V5_E, V5_F }, (input_NumberOfPayments >= 5));
        }

        /// <summary>
        /// Enable or disable the rows with the input fields based upon the number of payments desired.
        /// </summary>
        /// <param name="ctlList"></param>
        /// <param name="enabled"></param>
        private void EnableView(System.Web.UI.Control[] ctlList, bool enabled)
        {
            for (var idx = ctlList.GetLowerBound(0); idx <= ctlList.GetUpperBound(0); ++idx)
            {
                ctlList[idx].Visible = enabled;
            }
        }

        /// <summary>
        /// Process the page load sequence. This is after the controls are initialized but before the page is rendered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure that the first page has been used and has data
            if (string.IsNullOrEmpty(input_email))
            {
                Response.Redirect("~/Default.aspx", true);
                return;
            }

            // Build the list of input fields if they are missing.
            var input_items = read_input_items();
            if (input_items == null)
            {
                input_items = create_input_items();
                write_input_items(input_items);
            }

            // Write the amount of the transaction
            total_amount.Text = string.Format("{0:c}", input_paymentamount);

            // Process the PostBack event
            if (Page.IsPostBack)
            {
                // Collect the data from the input controls
                using (var db = new FHLB.ClearPoint.DebtPlus.DatabaseContext())
                {
                    collectItems(db, input_items.Find(s => s.id == 1), V1_reference, V1_fname, V1_lname, V1_co_fname, V1_co_lname);
                    collectItems(db, input_items.Find(s => s.id == 2), V2_reference, V2_fname, V2_lname, V2_co_fname, V2_co_lname);
                    collectItems(db, input_items.Find(s => s.id == 3), V3_reference, V3_fname, V3_lname, V3_co_fname, V3_co_lname);
                    collectItems(db, input_items.Find(s => s.id == 4), V4_reference, V4_fname, V4_lname, V4_co_fname, V4_co_lname);
                    collectItems(db, input_items.Find(s => s.id == 5), V5_reference, V5_fname, V5_lname, V5_co_fname, V5_co_lname);
                }

                // Rewrite the current values
                write_input_items(input_items);
            }
        }

        /// <summary>
        /// Process the post-back to translate the reference number to a control item and update the resulting page.
        /// </summary>
        /// <param name="db">Pointer to the database context</param>
        /// <param name="item">Pointer to the item to be updated</param>
        /// <param name="input_reference">Reference control on the page</param>
        /// <param name="input_applicant_first">Applicant first name control on the page</param>
        /// <param name="input_applicant_last">Applicant last name control on the page</param>
        /// <param name="input_coapplicant_first">Co-Applicant first name control on the page</param>
        /// <param name="input_coapplicant_last">Co-Applicant last name control on the page</param>
        private void collectItems(DebtPlus.DatabaseContext db, InputItem item, System.Web.UI.WebControls.TextBox input_reference, System.Web.UI.WebControls.TextBox input_applicant_first, System.Web.UI.WebControls.TextBox input_applicant_last, System.Web.UI.WebControls.TextBox input_coapplicant_first, System.Web.UI.WebControls.TextBox input_coapplicant_last)
        {
            if (item == null)
            {
                return;
            }

            // Find the reference string. If they are the same then just leave the input field alone.
            string refnce = System.Text.RegularExpressions.Regex.Replace(input_reference.Text, @"[^A-Za-z0-9]", string.Empty);
            if (refnce == item.reference || refnce == string.Empty)
            {
                return;
            }

            // Empty the information about the record
            item.client             =
            item.client_appointment = 0;
            item.applicant_last     =
            item.applicant_first    =
            item.coapplicant_first  =
            item.coapplicant_last   = string.Empty;

            // Find the client appointment associated with this reference field
            var qappt = db.client_appointmentsTable.Where(s => s.partner == refnce && (s.status == 'K' || s.status == 'W') && s.appt_type == fhlb_appointment_type).FirstOrDefault();
            if (qappt != null)
            {
                item.client_appointment = qappt.Id;
                item.client             = qappt.Client;
                item.reference          =
                input_reference.Text    = refnce;

                // Find the names
                var colNames = (from p in db.PeopleTable
                                join n in db.nameTable on p.NameID equals n.Id
                                where p.Client == qappt.Client
                                select new { p = p, n = n }).ToList();

                if (colNames.Count > 0)
                {
                    var primary = colNames.Find(s => s.p.Relation == 1);
                    if (primary != null)
                    {
                        item.applicant_first = primary.n.First;
                        item.applicant_last = primary.n.Last;
                    }

                    var secondary = colNames.Where(s => s.p.Relation != 1).OrderBy(s => s.p.Relation).FirstOrDefault();
                    if (secondary != null)
                    {
                        item.coapplicant_first = secondary.n.First;
                        item.coapplicant_last = secondary.n.Last;
                    }
                }
            }

            // Update the controls with the values
            input_applicant_first.Text = item.applicant_first;
            input_applicant_last.Text = item.applicant_last;
            input_coapplicant_first.Text = item.coapplicant_first;
            input_coapplicant_last.Text = item.coapplicant_last;
        }

        /// <summary>
        /// Handle the click event on the second panel input form
        /// </summary>
        /// <param name="sender">Button clicked</param>
        /// <param name="e">System Event Arguments</param>
        private void SubmitPanel2_Click(object sender, EventArgs e)
        {
            // Clear the errors and validate the list of case IDs
            ErrorList2.Text = string.Empty;
            var sb = new System.Text.StringBuilder();
            if (!valid_items())
            {
                sb.Append("<li>One or more of the cases is missing or does not have a valid ID.</li>");
            }

            // Validate the routing number
            routing_number = System.Text.RegularExpressions.Regex.Replace(RoutingNumber.Text, @"\D", string.Empty);
            if (!ValidABAChecksum(routing_number))
            {
                sb.Append("<li>The routing number is not valid. It must be exactly nine digits. Please check it again.</li>");
            }

            // Validate the account number
            account_number = System.Text.RegularExpressions.Regex.Replace(AccountNumber.Text, @"\D", string.Empty);
            if (account_number.Length < 3)
            {
                sb.Append("<li>The account number seems to be invalid. Please check it again.</li>");
            }

            var test_checking_savings = ((AccountType.SelectedValue ?? string.Empty) + "?")[0];
            if (test_checking_savings != 'C' && test_checking_savings != 'S')
            {
                sb.Append("<li>The type of account (checking or savings) does not seem to be valid. Please check it again.</li>");
            }
            input_checking_savings = test_checking_savings;

            // Set the error code buffer and return to the page
            if (sb.Length > 0)
            {
                sb.Insert(0, "<p class=\"error\">The following items need attention</p><ul class=\"error\">");
                sb.Append("</ul>");
                ErrorList2.Text = sb.ToString();
                return;
            }

            // Render the third panel
            Response.Redirect("~/Confirmation.aspx", true);
        }

        /// <summary>
        /// Format the telephone number to a suitable string
        /// </summary>
        /// <param name="PhoneNumber"></param>
        /// <returns></returns>
        private string FormatPhone(string PhoneNumber)
        {
            var str = System.Text.RegularExpressions.Regex.Replace(PhoneNumber, @"\D", string.Empty);
            return string.Format("{0}-{1}-{2}", str.Substring(0, 3), str.Substring(3, 3), str.Substring(6));
        }

        /// <summary>
        /// Determine if the input items are valid.
        /// </summary>
        /// <param name="colItems">Collection to be validated</param>
        /// <returns>TRUE if acceptable. FALSE if incomplete.</returns>
        private bool valid_items()
        {
            var colItems = read_input_items();
            foreach (var item in colItems)
            {
                // Ignore items that are outside the range of suitable entries
                if (item.id > input_NumberOfPayments)
                {
                    continue;
                }

                // We need the client identifier (which means that we need the appointment
                // identifier which means that we need the case ID to be valid.)
                if (item.client.GetValueOrDefault() <= 0)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Validate the ABA check-digit
        /// </summary>
        private static bool ValidABAChecksum(string AbaNumber)
        {
            // It must be exactly nine digits
            if (AbaNumber.Length != 9)
            {
                return false;
            }

            int sum = 0;
            int[] scale = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };
            for (int index = 0; index < 9; ++index)
            {
                // Characters other than digits are always invalid.
                char chr = AbaNumber[index];
                if (!char.IsDigit(chr))
                {
                    return false;
                }

                // Compute the scaled checksum value
                int code = ((int)chr) - 48;
                sum += (code * scale[index]);
            }

            // The result must be divisible by 10
            return (sum % 10) == 0;
        }
    }
}