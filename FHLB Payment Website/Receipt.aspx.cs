﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FHLB
{
    public partial class Receipt : MasterPage
    {
        public string print_AccountInfo      = "";
        public string print_transaction_date = "";
        public string print_totalamount      = "";
        public string print_transaction_id   = "";
        public string print_phone_number     = "";
        public string print_firm_name        = "";
        public string print_account_type     = "";
        public string print_account_number   = "";
        public string print_email            = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            // Something keeps killing the session data. It is there for the first time
            // but we seem to be called again and it is not there. So, just trap around the
            // error condition and ignore the re-entry.
            try
            {
                var sb = new System.Text.StringBuilder();
                var items = read_input_items();

                foreach (var item in items)
                {
                    if (item.id > input_NumberOfPayments)
                    {
                        continue;
                    }

                    sb.AppendFormat("<tr>");
                    sb.Append("<td class=\"nowrap\">Reservation Number:</td>");
                    sb.AppendFormat("<td class=\"nowrap\">{0}</td>", item.reference);
                    sb.AppendFormat("</tr>");

                    sb.AppendFormat("<tr>");
                    sb.Append("<td class=\"nowrap\">Homebuyer's Name:</td>");
                    sb.AppendFormat("<td class=\"nowrap\">{0}, {1}</td>", Server.HtmlEncode(item.applicant_last), Server.HtmlEncode(item.applicant_first));
                    sb.AppendFormat("</tr>");

                    if (!string.IsNullOrWhiteSpace(item.coapplicant_last))
                    {
                        sb.AppendFormat("<tr>");
                        sb.Append("<td class=\"nowrap\">Spouse's Name:</td>");
                        sb.AppendFormat("<td class=\"nowrap\">{0}, {1}</td>", Server.HtmlEncode(item.coapplicant_last), Server.HtmlEncode(item.coapplicant_first));
                        sb.AppendFormat("</tr>");
                    }
                }

                // The first item is the table portion listing the payment information
                print_AccountInfo = sb.ToString();

                // Build the remainder of the substitution dictionary
                print_firm_name = input_firm_name;
                print_phone_number = FormatPhone(input_phone_number);
                print_totalamount = string.Format("{0:c}", input_paymentamount);
                print_transaction_date = Tomorrow().ToShortDateString();
                print_account_type = input_checking_savings == 'S' ? "savings" : "checking";
                print_transaction_id = transaction_ID.ToString("0000000000");
                print_email = input_email;

                // The account number is just the last 4 digits of the value.
                var tempStr = "XXXXX" + (account_number ?? "");
                print_account_number = tempStr.Substring(tempStr.Length - 4);
            }

            catch
            {
            }
        }
    }
}