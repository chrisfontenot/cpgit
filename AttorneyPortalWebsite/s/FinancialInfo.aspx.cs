﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttorneyPortal.Providers;
using ClearPoint;
using AttorneyPortal;

namespace AttorneyPortal.s
{
    public partial class FinancialInfo : System.Web.UI.Page
    {
        // Connection to the database
        private ClearPoint.DebtPlus.DatabaseContext bc = null;

        // String buffers to hold the note component pieces
        private System.Text.StringBuilder sbChangeNote = null;
        private System.Text.StringBuilder sbSubject = null;

        // Billing address information
        private static readonly char billingAddressType = 'B';

        // Storage for the global functions in this class
        DebtPlus.vendor vendorRecord = null;
        DebtPlus.vendor_address vendorAddressRecord = null;
        DebtPlus.address addressRecord = null;

        /// <summary>
        /// Perform page initialization sequence. This is called immediately after the class is created.
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Allocate a database connection context for the page.
            bc = new ClearPoint.DebtPlus.DatabaseContext();
        }

        /// <summary>
        /// Handle the page destruction sequence. It is called before the object is destroyed.
        /// </summary>
        protected void Page_Unload(object sender, EventArgs e)
        {
            // Dispose of the database connection.
            if (bc != null)
            {
                bc.Dispose();
                bc = null;
            }
        }

        /// <summary>
        /// Handle the page Load sequence. This is attached to the page because the "AutoWireup" is set to true on the page's ASPX definition.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 vendorId = default(Int32);

            // Read the vendor record
            string firmCode = ((string)Session["firm_code"]);
            if (string.IsNullOrEmpty(firmCode))
            {
                return;
            }

            if (!Int32.TryParse(firmCode, out vendorId))
            {
                return;
            }

            // Find the vendor in the database. Abort the page operation if there is no vendor.
            vendorRecord = bc.vendorTable.FirstOrDefault(s => s.Id == vendorId);
            if (vendorRecord == null)
            {
                return;
            }

            // Read the vendor billing address information
            vendorAddressRecord = bc.vendor_addressTable.FirstOrDefault(s => s.vendor == vendorRecord.Id && s.address_type == billingAddressType);
            if (vendorAddressRecord == null)
            {
                vendorAddressRecord = bc.vendor_addressTable.FirstOrDefault(s => s.vendor == vendorRecord.Id && s.address_type == 'G');
            }

            // From the vendor address record, obtain the pointer to the addresses table for the address record itself.
            if (vendorAddressRecord != null && vendorAddressRecord.addressID.HasValue)
            {
                addressRecord = bc.addressTable.FirstOrDefault(s => s.Id == vendorAddressRecord.addressID.Value);
            }

            // Define the controls if this is not the PostBack operation.
            if (!Page.IsPostBack)
            {
                // Load the list of expiration years, starting with this year and for the next 20 years.
                CardExYrD.Items.AddRange(Enumerable.Range(DateTime.Now.Year, 20).Select(s => new ListItem(s.ToString("0000"), s.ToString("0000"))).ToArray());

                // Refresh the page with the proper control values
                updatePageControls();

                // There is no status message at this point since we are just starting.
                StatusMessage.Text = string.Empty;
            }
        }

        /// <summary>
        /// Handle the CLICK event on the Submit button
        /// </summary>
        protected void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Validate the form. If invalid, leave now with the error conditions set.
            ValidatePage();
            if (!IsValidPage)
            {
                StatusMessage.Text = "<span style=\"color:Red\">There is something wrong with your information. Please check it again.</span>";
                return;
            }

            // Allocate the string objects
            sbSubject = new System.Text.StringBuilder();
            sbChangeNote = new System.Text.StringBuilder();

            try
            {
                // Process changes to the account information
                string Change = Request["Change"] ?? string.Empty;
                if (!string.IsNullOrEmpty(Change))
                {
                    string payType = Request["PayType"] ?? string.Empty;
                    switch (payType)
                    {
                        case "B":
                            UpdateACH();
                            break;

                        case "C":
                            UpdateCreditCard();
                            break;
                    }
                }

                // Update the other references
                UpdateVendorAddress();
                UpdateVendor();

                // Commit the changes to the database
                bc.SubmitChanges();

                // Generate the system note showing the changes
                // Record the system note reflecting the change in the data
                if (sbChangeNote.Length > 0)
                {
                    sbSubject.Remove(0, 1);         // toss the leading comma
                    string txtSubject = sbSubject.ToString();
                    if (txtSubject.Length > 77)
                    {
                        txtSubject = txtSubject.Substring(0, 77) + "...";
                    }

                    var note = new ClearPoint.DebtPlus.vendor_note()
                    {
                        vendor      = vendorRecord.Id,
                        subject     = string.Format("Changed {0}", txtSubject),
                        note        = string.Format("<html>\r\n<head><title></title></head>\r\n<body>\r\n<p>Fields changed for the vendor are:</p>\r\n<ul>{0}</ul>\r\n</body>\r\n</html>", sbChangeNote.ToString()),
                        type        = 3,
                        dont_print  = true,
                        dont_edit   = true,
                        dont_delete = true
                    };

                    bc.vendor_noteTable.InsertOnSubmit(note);
                    bc.SubmitChanges();

                    // Send the owner an email message if so desired
                    var dict = new System.Collections.Generic.Dictionary<string, string>();
                    dict.Add("<%Changes%>", sbChangeNote.ToString());
                    dict.Add("<%VendorLabel%>", vendorRecord.Label);
                    dict.Add("<%VendorID%>", vendorRecord.Id.ToString());
                    dict.Add("<%VendorName%>", Server.HtmlEncode(vendorRecord.Name));

                    NotifyOwner(dict, "OnFinancialChange");

                    // Clear the buffers
                    sbChangeNote.Clear();
                    sbSubject.Clear();

                    sbChangeNote = null;
                    sbSubject = null;
                }                         

                // Indicate success on the operation
                StatusMessage.Text = "<span style=\"color:Green;\">The changes were made in the database. Thank you.</span>";

                // Refresh the page with the updated data
                updatePageControls();
            }

#pragma warning disable 168
            catch (System.Data.SqlClient.SqlException ex)
            {
                StatusMessage.Text = "<span style=\"color:Red;\">We are unable to access the database at this time. Please try back later.</span>";
            }
#pragma warning restore 168

            catch (Exception ex)
            {
                StatusMessage.Text = string.Format("<span style=\"color:Red;\">{0}</span>", Server.HtmlEncode(ex.Message));
            }
        }

        /// <summary>
        /// Send the email message to the owner of the website if desired
        /// </summary>
        /// <param name="dict"></param>
        private void NotifyOwner(System.Collections.Generic.Dictionary<string, string> dict, string key)
        {
            // Find the configuration information in the web.config file
            var cnf = System.Configuration.ConfigurationManager.GetSection("EmailMessageConfigurationSection") as ClearPoint.Configuration.EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var OnPwChange = cnf.MailDefinitions.Where(s => string.Compare(s.Event, key, true) == 0).FirstOrDefault();
            if (OnPwChange == null || string.IsNullOrWhiteSpace(OnPwChange.To))
            {
                return;
            }

            // Send the email notice to the owner
            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var mailDefinition = OnPwChange.CreateMailDefinition();
                using (var msg = mailDefinition.CreateMailMessage(OnPwChange.To, dict, this))
                {
                    smtp.Send(msg);
                }
            }
        }

        /// <summary>
        /// Update the web page with the display versions of the data. This is done
        /// when the initial page is loaded as well as after any valid submission data.
        /// </summary>
        private void updatePageControls()
        {
            // Load the list of State codes. Set the default the current item.
            BillState.Items.Clear();
            BillState.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(bc.stateTable, (addressRecord == null ? -1 : addressRecord.state)).ToArray());

            // Use the billing address for the vendor where needed
            if (addressRecord != null)
            {
                BillAdr1.Text = addressRecord.AddressLine1;
                BillAdr2.Text = addressRecord.address_line_2;
                BillCity.Text = addressRecord.city;
                BillZip.Text  = addressRecord.PostalCode;
            }

            // Include the name on the account.
            NameOnCard.Text = vendorRecord.NameOnCard;

            // Display string for the ach account number
            Display_AchNo.Text           = vendorRecord.ACH_AccountNumber.RightMask(4);
            Display_AchAba.Text          = vendorRecord.ACH_RoutingNumber.RightMask(4);
            Display_CrdExp.Text          = vendorRecord.CC_ExpirationDate.HasValue ? string.Format("{0:M/yyyy}", vendorRecord.CC_ExpirationDate.Value) : string.Empty;
            Display_CrdNo.Text           = vendorRecord.ACH_AccountNumber.RightMask(4);
            Display_CheckingSavings.Text = vendorRecord.ACH_CheckingSavings == 'S' ? "Savings" : "Checking";

            // Indicate the appropriate display for the current payment method.
            switch (vendorRecord.BillingMode ?? string.Empty)
            {
                case "ACH":
                    DisplayPayNone.Visible = false;
                    DisplayPayCC.Visible   = false;
                    DisplayPayACH.Visible  = true;
                    break;

                case "CreditCard":
                    DisplayPayNone.Visible = false;
                    DisplayPayCC.Visible   = true;
                    DisplayPayACH.Visible  = false;

                    // Determine the Description of the credit card
                    if (validations.Valid_AMEX(vendorRecord.ACH_AccountNumber))
                    {
                        CreditCardType.Text = "American Express";
                    }
                    else if (validations.Valid_VISA(vendorRecord.ACH_AccountNumber))
                    {
                        CreditCardType.Text = "Visa";
                    }
                    else if (validations.Valid_MASTERCARD(vendorRecord.ACH_AccountNumber))
                    {
                        CreditCardType.Text = "MasterCard";
                    }
                    else
                    {
                        CreditCardType.Text = "Unknown";
                    }
                    break;

                default:
                    DisplayPayNone.Visible = true;
                    DisplayPayCC.Visible   = false;
                    DisplayPayACH.Visible  = false;
                    break;
            }
        }

        /// <summary>
        /// Update the vendor record when it changes
        /// </summary>
        private void UpdateVendor()
        {
            string txt = NameOnCard.Text.Trim().ToUpper();
            if (txt != vendorRecord.NameOnCard)
            {
                sbSubject.Append(",Card Name");
                sbChangeNote.AppendFormat("<li>Name on Card changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.NameOnCard ?? string.Empty), Server.HtmlEncode(txt));
                vendorRecord.NameOnCard = txt;
            }
        }

        /// <summary>
        /// Update the vendor billing address when it changes
        /// </summary>
        private void UpdateVendorAddress()
        {
            if (addressRecord == null)
            {
                addressRecord = new DebtPlus.address();
                bc.addressTable.InsertOnSubmit(addressRecord);
            }

            string oldAddress = addressRecord.ToString();

            // Merge the address into a single string for parsing
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(BillAdr1.Text.Trim());
            sb.AppendLine(BillAdr2.Text.Trim());
            sb.AppendFormat("{0}, {1} {2}", BillCity.Text.Trim(), "CA", BillZip.Text.Trim());

            // pass the address through the parser to find the appropriate fields
            var adr = new AddressParser.AddressParser();
            var resp = adr.ParseAddress(sb.ToString());

            // Set the address fields appropriately
            addressRecord.city              = resp.City ?? string.Empty;
            addressRecord.direction         = resp.Predirectional ?? string.Empty;
            addressRecord.house             = resp.Number ?? string.Empty;
            addressRecord.modifier          = resp.SecondaryUnit ?? string.Empty;
            addressRecord.modifier_value    = resp.SecondaryNumber ?? string.Empty;
            addressRecord.PostalCode        = resp.Zip ?? string.Empty;
            addressRecord.street            = resp.Street ?? string.Empty;
            addressRecord.suffix            = resp.Suffix ?? string.Empty;
            addressRecord.creditor_prefix_1 = (Session["atty_name"].ToString()).Trim() ?? string.Empty;
            addressRecord.creditor_prefix_2 = string.Empty;
            addressRecord.address_line_2    = string.Empty;
            addressRecord.address_line_3    = string.Empty;

            // Some people use "#" to mean suite. Change it here.
            if (addressRecord.modifier == "#")
            {
                addressRecord.modifier = "STE";
            }
            addressRecord.state = Int32.Parse(BillState.Text);

            string newAddress = addressRecord.ToString();

            // Generate the note when the address changes
            if (oldAddress != newAddress)
            {
                sbSubject.Append(",billing address");
                sbChangeNote.AppendFormat("<li>Billing Address changed from:<br/>{0}<br/>to<br/>{1}</li>\r\n", Server.HtmlEncode(oldAddress).Replace("\r\n", "<br/>"), Server.HtmlEncode(newAddress).Replace("\r\n", "<br/>"));
            }                

            // Commit the insert operation
            bc.SubmitChanges();

            // Construct the vendor address record
            if (vendorAddressRecord == null)
            {
                vendorAddressRecord              = new DebtPlus.vendor_address();
                vendorAddressRecord.vendor       = vendorRecord.Id;
                vendorAddressRecord.address_type = billingAddressType;
                vendorAddressRecord.addressID    = addressRecord.Id;
                bc.SubmitChanges();
            }
        }

        /// <summary>
        /// Update the vendor's ACH information when selected
        /// </summary>
        private void UpdateACH()
        {
            // The billing method is simple.
            string newStr = "ACH";
            if (vendorRecord.BillingMode != newStr)
            {
                sbSubject.Append(",Billing mode");
                sbChangeNote.AppendFormat("<li>Billing Mode changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.BillingMode ?? string.Empty), newStr);
                vendorRecord.BillingMode = newStr;
            }

            newStr = AchNo.Text.DigitsOnly();
            if (vendorRecord.ACH_AccountNumber != newStr)
            {
                sbSubject.Append(",Account#");
                sbChangeNote.AppendFormat("<li>Account Number changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.ACH_AccountNumber ?? string.Empty), Server.HtmlEncode(newStr));
                vendorRecord.ACH_AccountNumber = newStr;
            }

            newStr = AchAba.Text.DigitsOnly();
            if (vendorRecord.ACH_RoutingNumber != newStr)
            {
                sbSubject.Append(",ABA#");
                sbChangeNote.AppendFormat("<li>ABA Routing number changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.ACH_RoutingNumber ?? string.Empty), Server.HtmlEncode(newStr));
                vendorRecord.ACH_RoutingNumber = newStr;
            }

            char newChar = CheckingSavings.Text == "S" ? 'S' : 'C';
            if (vendorRecord.ACH_CheckingSavings != newChar)
            {
                sbSubject.Append(",Acct Type");
                sbChangeNote.AppendFormat("<li>Checking/Savings Account Type changed from:'{0}' to '{1}'</li>\r\n", mapCheckingSavings(vendorRecord.ACH_CheckingSavings), mapCheckingSavings(newChar));
                vendorRecord.ACH_CheckingSavings = newChar;
            }
        }

        private string mapCheckingSavings(char? inputCode)
        {
            if (inputCode.HasValue)
            {
                if (inputCode.Value == 'S')
                {
                    return "Savings";
                }
                return "Checking";
            }
            return string.Empty;
        }

        /// <summary>
        /// Update the vendor's credit card information when selected
        /// </summary>
        private void UpdateCreditCard()
        {
            // The billing method is simple.
            string newStr = "CreditCard";
            if (string.Compare(vendorRecord.BillingMode, newStr, true) != 0)
            {
                sbSubject.Append(",Billing mode");
                sbChangeNote.AppendFormat("<li>Billing Mode changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.BillingMode ?? string.Empty), newStr);
                vendorRecord.BillingMode = newStr;
            }

            // Update the credit card information. The data fields have already been validated and
            // are known to be good so just use them.
            newStr = CrdNo.Text.DigitsOnly();
            if (string.Compare(vendorRecord.ACH_AccountNumber, newStr, true) != 0)
            {
                sbSubject.Append(",Account#");
                sbChangeNote.AppendFormat("<li>Account Number changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.ACH_AccountNumber ?? string.Empty), Server.HtmlEncode(newStr));
                vendorRecord.ACH_AccountNumber = newStr;
            }

            DateTime newDate = getExpirationDate().Value.AddMonths(1).AddDays(-1);
            if (vendorRecord.CC_ExpirationDate != newDate)
            {
                sbSubject.Append(",Expiration");
                sbChangeNote.AppendFormat("<li>Expiration date changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.CC_ExpirationDate.GetValueOrDefault().ToShortDateString()), Server.HtmlEncode(newDate.ToShortDateString()));
                vendorRecord.CC_ExpirationDate = newDate;
            }

            newStr = CrdCcv.Text.DigitsOnly();
            if (string.Compare(vendorRecord.CC_CVV, newStr, true) != 0)
            {
                sbSubject.Append(",CVV");
                sbChangeNote.AppendFormat("<li>CVV Number changed from:'{0}' to '{1}'</li>\r\n", Server.HtmlEncode(vendorRecord.CC_CVV ?? string.Empty), Server.HtmlEncode(newStr ?? string.Empty));
                vendorRecord.CC_CVV = newStr;
            }
        }

        /// <summary>
        /// Indicate that the form is a valid State to be used
        /// </summary>
        private bool IsValidPage { get; set; }

        /// <summary>
        /// Local routine to do the form validation.
        /// </summary>
        private void ValidatePage()
        {
            // Clear the error fields since we just set them below.
            foreach (var ctl in new System.Web.UI.WebControls.Literal[] { ErrAcNo, ErrAcRn, ErrAdr1, ErrCdCv, ErrCdEx, ErrCdNo, ErrCheckingSavings, ErrCity, ErrName, ErrPyTp, ErrSt, ErrZip })
            {
                ctl.Text = string.Empty;
            }

            // Do the standard .NET validation checks. If invalid then exit early.
            Validate();
            IsValidPage = IsValid;
            if (!IsValidPage)
            {
                return;
            }

            // The name on the account is required
            if (string.IsNullOrEmpty(NameOnCard.Text))
            {
                ErrName.Text = "This is a required item.";
                IsValidPage = false;
            }

            // Validate the address information
            if (string.IsNullOrEmpty(BillAdr1.Text))
            {
                ErrAdr1.Text = "This is a required item.";
                IsValidPage  = false;
            }

            if (string.IsNullOrEmpty(BillCity.Text))
            {
                ErrCity.Text = "This is a required item.";
                IsValidPage  = false;
            }

            if (! validations.Valid_ZIP(BillZip.Text))
            {
                ErrZip.Text = "Please use 5 or 9 digits for the ZIP Code.";
                IsValidPage = false;
            }

            if (string.IsNullOrWhiteSpace(BillState.Text))
            {
                ErrSt.Text = "Please choose a valid State from the list";
                IsValidPage = false;
            }

            // Validate the payment information
            string Change  = Request["Change"];
            if (string.IsNullOrEmpty(Change))
            {
                return;
            }

            // Validate the payment Description. It must be one or the other at this point.
            string PayType = Request["PayType"];
            if (!string.IsNullOrEmpty(Change))
            {
                // Verify the payment Description
                switch (PayType ?? string.Empty)
                {
                    case "B":
                        ValidateACH();
                        break;

                    case "C":
                        ValidateCreditCard();
                        break;

                    default:
                        ErrPyTp.Text = "Invalid payment Description";
                        IsValidPage = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Validate the ACH payment information.
        /// </summary>
        private void ValidateACH()
        {
            // Validate the American Banking Association number
            string txt = AchAba.Text.DigitsOnly();
            if (txt == null || txt.Length != 9)
            {
                ErrAcRn.Text = "Must be 9 digits";
                IsValidPage = false;
            }
            else
            {
                if (!validations.ValidABAChecksum(txt))
                {
                    ErrAcRn.Text = "Invalid number. Please re-check.";
                    IsValidPage = false;
                }
            }
    
            // Validate the account number
            txt = AchNo.Text.DigitsOnly();
            if (string.IsNullOrEmpty(txt))
            {
                ErrAcNo.Text = "This is a required field. Please use numbers.";
                IsValidPage = false;
            }

            // Validate the checking/savings account number status
            txt = CheckingSavings.Text;
            switch (txt ?? string.Empty)
            {
                case "C":
                    break;
                    
                case "S":
                    break;

                default:
                    ErrCheckingSavings.Text = "Please choose either Checking or Savings.";
                    IsValidPage = false;
                    break;
            }
        }

        /// <summary>
        /// Validate the information for a credit card payment
        /// </summary>
        private void ValidateCreditCard()
        {
            string cardNumber = CrdNo.Text.DigitsOnly();
            if (string.IsNullOrEmpty(cardNumber))
            {
                ErrCdNo.Text = "This is a required item.";
                IsValidPage = false;
            }
            else
            {
                if (!validations.Valid_Luhn(cardNumber))
                {
                    ErrCdNo.Text = "The account number is invalid.";
                    IsValidPage = false;
                }
                else
                {
                    if (!validations.Valid_VISA(cardNumber) &&
                        !validations.Valid_MASTERCARD(cardNumber) &&
                        !validations.Valid_AMEX(cardNumber))
                    {
                        ErrCdNo.Text = "We can only accept VISA, MASTERCARD, and AmericanExpress at this time.";
                        IsValidPage = false;
                    }
                }
            }

            // Get the expiration date.
            System.Nullable<DateTime> expirationDate = getExpirationDate();
            if (!expirationDate.HasValue)
            {
                ErrCdEx.Text = "This is a required value.";
                IsValidPage = false;
            }
            else
            {
                if (DateTime.Now.Date >= expirationDate.Value.AddMonths(1))
                {
                    ErrCdEx.Text = "Card has expired. Please re-check it.";
                    IsValidPage = false;
                }
            }

            // Verify the check code entry
            string verificationCode = (CrdCcv.Text ?? string.Empty).DigitsOnly();
            if (validations.Valid_AMEX(cardNumber) && verificationCode.Length != 4)
            {
                ErrCdCv.Text = "Please enter the 4 digits on the front of the card.";
                IsValidPage = false;
                return;
            }

            if (verificationCode.Length < 3)
            {
                ErrCdCv.Text = "This is a required field of at least 3 digits";
                IsValidPage = false;
            }
        }

        /// <summary>
        /// Convert the two input fields for the expiration date into a single date.
        /// </summary>
        /// <remarks>
        /// We use the 1st of the month for the value. The card is actually good through that month
        /// and expires on the 1st of the following month. Adjustments are made to this date by the
        /// caller.
        /// </remarks>
        /// <returns>NULL if the value is not able to be converted.</returns>
        private System.Nullable<System.DateTime> getExpirationDate()
        {
            string txtMonth = CardExMnD.Text;
            string txtYear  = CardExYrD.Text;

            if (!string.IsNullOrWhiteSpace(txtMonth) && !string.IsNullOrWhiteSpace(txtYear))
            {
                Int32 intMonth = default(Int32);
                Int32 intYear  = default(Int32);

                if (Int32.TryParse(txtMonth, out intMonth) && Int32.TryParse(txtYear, out intYear))
                {
                    try
                    {
                        return new DateTime(intYear, intMonth, 1);
                    }
                    catch { }
                }
            }

            return null;
        }
    }
}

namespace AttorneyPortal
{
    /// <summary>
    /// Class to hold the validation logic.
    /// </summary>
    internal static class validations
    {
        /// <summary>
        /// Determine if the account number is a valid VISA card
        /// </summary>
        /// <remarks>
        /// VISA Card
        /// - numbers always start with "4"
        /// - either have 13 digits for old cards
        /// - 16 digits for all new cards
        /// </remarks>
        /// <returns></returns>
        internal static bool Valid_VISA(string CardNumber)
        {
            const string expression = @"^4[0-9]{12}(?:[0-9]{3})?$";
            return System.Text.RegularExpressions.Regex.IsMatch(CardNumber ?? string.Empty, expression);
        }

        /// <summary>
        /// Determine if the account number is a valid MASTERCARD card
        /// </summary>
        /// <remarks>
        /// MasterCard numbers
        /// - start with the number 51 through 55 or with the numbers 2221 through 2720.
        /// - all have 16 digits.
        /// </remarks>
        /// <returns></returns>
        internal static bool Valid_MASTERCARD(string CardNumber)
        {
            const string expression = @"^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$";
            return System.Text.RegularExpressions.Regex.IsMatch(CardNumber ?? string.Empty, expression);
        }

        /// <summary>
        /// Determine if the account number is a valid MASTERCARD card
        /// </summary>
        /// <remarks>
        /// American Express numbers
        /// - start with the number 34 or 37
        /// - all have 15 digits.
        /// </remarks>
        /// <returns></returns>
        internal static bool Valid_AMEX(string CardNumber)
        {
            const string expression = @"^3[47][0-9]{13}$";
            return System.Text.RegularExpressions.Regex.IsMatch(CardNumber ?? string.Empty, expression);
        }

        /// <summary>
        /// Is the input string a valid ZipCode?
        /// </summary>
        /// <param name="Zipcode">The string to be tested</param>
        /// <remarks>
        /// This is for USA ZipCode rules only.
        /// </remarks>
        /// <returns>TRUE if valid. FALSE if not.</returns>
        internal static bool Valid_ZIP(string Zipcode)
        {
            // The string many not be empty to be a valid ZipCode.
            if (string.IsNullOrWhiteSpace(Zipcode))
            {
                return false;
            }

            // ZipCodes are five digits and optionally have a dash and another 4. We can take 9 digits
            // as well and assume that someone just forgot the dash.
            const string expression = "^[0-9]{5}(-?[0-9]{4})?$";
            return System.Text.RegularExpressions.Regex.IsMatch(Zipcode.Trim(), expression);
        }

        /// <summary>
        /// Determine if the account number has a valid checksum
        /// </summary>
        /// <returns></returns>
        internal static bool Valid_Luhn(string CardNumber)
        {
            return CardNumber.All(char.IsDigit) && CardNumber.Reverse()
                .Select(c => c - 48)
                .Select((thisNum, i) => i % 2 == 0
                    ? thisNum
                    : ((thisNum *= 2) > 9 ? thisNum - 9 : thisNum)
                ).Sum() % 10 == 0;
        }

        /// <summary>
        /// Validate the ABA check-digit
        /// </summary>
        internal static bool ValidABAChecksum(string AbaNumber)
        {
            // It must be exactly nine digits
            if (AbaNumber.Length != 9)
            {
                return false;
            }

            int sum = 0;
            int[] scale = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };
            for (int index = 0; index < 9; ++index)
            {
                // Characters other than digits are always invalid.
                char chr = AbaNumber[index];
                if (!char.IsDigit(chr))
                {
                    return false;
                }

                // Compute the scaled checksum value
                int code = ((int)chr) - 48;
                sum += (code * scale[index]);
            }

            // The result must be divisible by 10
            return (sum % 10) == 0;
        }
    }
}
