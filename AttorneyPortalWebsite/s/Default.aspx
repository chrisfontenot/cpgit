﻿<%@ Page Title="" Language="C#" MasterPageFile="PageMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AttorneyPortal.s.Default" %>
<%@ Register assembly="DevExpress.Web.v11.2, Version=11.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxMenu" tagprefix="dx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
    <table style="width:100%">
        <tbody>
            <tr style="vertical-align:top">

                <!-- Left column -->
                <td>
                    <table style="width:25%">
                        <tbody>
                            <tr>
                                <td class="NormTxt" style="height:40"><span style="font-weight:bold;"><span style="text-decoration:underline">Need more flyers?</span><br/>
                                    Now you can order them yourself — online. It's very simple.
                                    Click below, put in your Firm Code and order as many flyers
                                    as you need.</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="NormTxt"  style="height:40; border-bottom-color: rgb(0, 0, 0); border-bottom-width: 4px; border-bottom-style: solid;">
                                    <a class="NormLink" href="https://onlinecounsel.cccsatl.org/PortalAtty/OrderFly.asp" target="_blank">Order Flyers</a>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <br />
                                    <br /><!-- IMPORTANT! Form/control/id names are predefined - please don't change their names -->
                                
                                    <!-- Begin LivePerson One-Step Combo -->
                                    <div id="Shadow">
                                            <table class="tableWidth" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td class="topImage"></td>
                                                    </tr>
                                                
                                                    <!-- Begin LivePerson Click-to-Chat button -->
                                                    <tr>
                                                        <td id="contentCell">
                                                            Call 866.445.2227											 
                                                            <p class="line" style="padding: 2px 0px;"></p>
                                                            or leave a message<br />
                                                            <a onclick="javascript:window.open('https://server.iad.liveperson.net/hc/29009926/?cmd=file&amp;file=visitorWantsToChat&amp;site=29009926&amp;SESSIONVAR!skill=bkexpress&amp;referrer='+escape(document.location),'chat29009926','width=472,height=320');return false;" href="https://server.iad.liveperson.net/hc/29009926/?cmd=file&amp;file=visitorWantsToChat&amp;site=29009926&amp;byhref=1" target="chat29009926">
                                                                <img name="hcIcon" style="margin-top: 5px;" src="../images/reponline.gif" alt="Online Chat" border="0" />
                                                            </a>
                                                            <p class="line" style="padding: 2px 0px;"></p> <!-- End LivePerson Click-to-Chat button -->
                                                                
                                                            <span class="small">US only. Other countries, <!-- Begin Link to full LiveCall Form -->
                                                                <a onclick="javascript:onTwoStepsClickToTalk(); return false;" href="https://onlinecounsel.cccsatl.org/PortalAtty/index_bam.asp#" target="chat29009926">
                                                                    click here
                                                                </a>.
                                                            </span> <!-- End Link to full LiveCall Form -->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="btmImage"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>

                <!-- Middle column -->
                <td>
                    <img alt="" src="../images/Cccs_005a_400.jpg" />
                </td>

                <!-- Right column -->
                <td>
                    <p>
                        <img alt="NEW" src="../images/new.gif" />
                        Info on our new fee waiver process<br />
                        <a class="NormLink" href="https://onlinecounsel.cccsatl.org/PortalAtty/FeeWaiverInfo.doc">FeeWaiverInfo.doc</a>
                    </p>
                    <p class="line" style="padding: 2px 0px;"></p>
                    <p>
                        Bankruptcy Process Demonstrations — CredAbility offers demonstrations
                        to show how your clients interact with us and how your firm may use
                        the attorney portal to access important information regarding your
                        account. Learn how to leverage our services to best suit your business
                        needs. Select from a date and time below:
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>