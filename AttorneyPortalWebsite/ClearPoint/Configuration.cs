﻿using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;

namespace ClearPoint.Configuration
{
    public class MailDefinitionConfigurationElement : ConfigurationSection
    {
        public MailDefinitionConfigurationElement()
        {
            Cc           = string.Empty;
            BodyFileName = string.Empty;
            IsBodyHtml   = false;
            Priority     = System.Net.Mail.MailPriority.Normal;
            Subject      = string.Empty;
            To           = string.Empty;
            Event        = string.Empty;
        }

        static MailDefinitionConfigurationElement()
        {
            // Define the properties
        }

        // Static properties

        // Expose the properties
        [ConfigurationProperty("event", IsRequired = true, IsKey=true)]
        public string Event
        {
            get { return (string)this["event"]; }
            set { this["event"] = value; }
        }

        [ConfigurationProperty("bodyfilename", IsRequired = false, DefaultValue = "")]
        public string BodyFileName
        {
            get { return (string)this["bodyfilename"]; }
            set { this["bodyfilename"] = value; }
        }

        [ConfigurationProperty("isbodyhtml", IsRequired = false, DefaultValue=false)]
        public Boolean IsBodyHtml
        {
            get { return (Boolean)this["isbodyhtml"]; }
            set { this["isbodyhtml"] = value; }
        }

        [ConfigurationProperty("priority", IsRequired = false, DefaultValue=System.Net.Mail.MailPriority.Normal)]
        public System.Net.Mail.MailPriority Priority
        {
            get { return (System.Net.Mail.MailPriority)this["priority"]; }
            set { this["priority"] = value; }
        }

        [ConfigurationProperty("to", IsRequired = false, DefaultValue="")]
        public string To
        {
            get { return (string)this["to"]; }
            set { this["to"] = value; }
        }

        [ConfigurationProperty("cc", IsRequired = false, DefaultValue="")]
        public string Cc
        {
            get { return (string)this["cc"]; }
            set { this["cc"] = value; }
        }

        [ConfigurationProperty("subject", IsRequired = false, DefaultValue="")]
        public string Subject
        {
            get { return (string)this["subject"]; }
            set { this["subject"] = value; }
        }

        /// <summary>
        /// Create a new MailDefinition object for the current class instance
        /// </summary>
        public System.Web.UI.WebControls.MailDefinition CreateMailDefinition()
        {
            var result = new System.Web.UI.WebControls.MailDefinition()
            {
                BodyFileName = BodyFileName,
                CC           = Cc,
                IsBodyHtml   = IsBodyHtml,
                Priority     = Priority,
                Subject      = Subject
            };

            return result;
        }
    }

    public class GenericConfigurationElementCollection<T> : ConfigurationElementCollection, IEnumerable<T> where T : ConfigurationElement, new()
    {
        List<T> _elements = new List<T>();

        protected override ConfigurationElement CreateNewElement()
        {
            T newElement = new T();
            _elements.Add(newElement);
            return newElement;
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return _elements.Find(e => e.Equals(element));
        }

        public new IEnumerator<T> GetEnumerator()
        {
            return _elements.GetEnumerator();
        }
    }

    public class EmailMessageConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("mailDefinitions", IsRequired = true, IsDefaultCollection=true)]
        [ConfigurationCollection(typeof(MailDefinitionConfigurationElement), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public GenericConfigurationElementCollection<MailDefinitionConfigurationElement> MailDefinitions
        {
            get { return (GenericConfigurationElementCollection<MailDefinitionConfigurationElement>)this["mailDefinitions"]; }
        }
    }
}