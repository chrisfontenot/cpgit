﻿using System;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.product_transactions")]
        public partial class product_transaction
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }
            [Column] public Int32 vendor { get; set; }
            [Column] public Int32 product { get; set; }
            [Column] public Int32 client_product { get; set; }
            [Column] public String transaction_type { get; set; }
            [Column] public decimal cost { get; set; }
            [Column] public decimal discount { get; set; }
            [Column] public decimal payment { get; set; }
            [Column] public decimal disputed { get; set; }
            [Column] public String note { get; set; }

            [Column(AutoSync = AutoSync.OnInsert, DbType = "datetime NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public DateTime date_created { get; set; }
            [Column(AutoSync = AutoSync.OnInsert, DbType = "varchar(50) NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)] public string created_by { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public product_transaction()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Id               = default(Int32);
                vendor           = default(Int32);
                product          = default(Int32);
                client_product   = default(Int32);
                transaction_type = null;
                cost             = 0M;
                discount         = 0M;
                payment          = 0M;
                note             = null;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<product_transaction> product_transactionsTable
            {
                get
                {
                    return context.GetTable<product_transaction>();
                }
            }
        }
    }
}