﻿using System;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendor_contacts")]
        public partial class vendor_contact
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public int vendor { get; set; }
            [Column] public int contact_type { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> nameID { get; set; }
            [Column] public string Title { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> addressID { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> telephoneID { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> faxID { get; set; }
            [Column(CanBeNull = true)] public System.Nullable<int> emailID { get; set; }
            [Column] public string note { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public vendor_contact()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Id           = default(Int32);
                contact_type = default(Int32);
                vendor       = default(Int32);
                nameID       = null;
                addressID    = null;
                emailID      = null;
                telephoneID  = null;
                faxID        = null;
                Title        = null;
                note         = null;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor_contact> vendor_contactTable
            {
                get
                {
                    return context.GetTable<vendor_contact>();
                }
            }
        }
    }
}