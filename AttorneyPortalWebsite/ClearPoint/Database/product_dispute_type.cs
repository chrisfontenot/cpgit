﻿using System;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.product_dispute_types")]
        public partial class product_dispute_type
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string description { get; set; }
            [Column] public Boolean ActiveFlag { get; set; }
            [Column] public Boolean @Default { get; set; }

            public product_dispute_type()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                @Default    = false;
                ActiveFlag  = true;
                description = string.Empty;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<product_dispute_type> product_dispute_typeTable
            {
                get
                {
                    return context.GetTable<product_dispute_type>();
                }
            }
        }
    }
}