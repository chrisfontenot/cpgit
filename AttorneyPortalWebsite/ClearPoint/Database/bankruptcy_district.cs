﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.bankruptcy_districts")]
        public partial class bankruptcy_district
        {
            [Column(Name = "bankruptcy_district", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string Description { get; set; }
            [Column] public bool ActiveFlag { get; set; }
            [Column] public bool @Default { get; set; }
            [Column] public int State { get; set; }
            [Column(Name="ts", DbType="rowversion NOT NULL", CanBeNull=false, IsVersion=true)] public System.Data.Linq.Binary ts { get; set; }

            public bankruptcy_district()
            {
                OnCreated();
            }

            private void OnCreated()
            {
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<bankruptcy_district> bankrupcy_districtTable
            {
                get
                {
                    return context.GetTable<bankruptcy_district>();
                }
            }
        }
    }

    public static partial class Utilitiy
    {
        /// <summary>
        /// Return the collection of items for a state dropdown list
        /// </summary>
        /// <param name="states">Pointer to the source "states" table</param>
        /// <param name="selectedId">ID if the selected state if desired. 0 if none.</param>
        /// <returns></returns>
        public static IEnumerable<System.Web.UI.WebControls.ListItem> GetBankrupcyDistrictListItems(IEnumerable<DebtPlus.bankruptcy_district> bankrupcy_districts, int selectedId)
        {
            // Add the first item which is selected
            var col = new System.Collections.Generic.List<System.Web.UI.WebControls.ListItem>();
            col.Add(new System.Web.UI.WebControls.ListItem("-- Please select --", "0", false));

            // Append the other bankruptcy districts
            col.AddRange(bankrupcy_districts
               .Where(s => s.ActiveFlag && s.Id != 0)
               .Select(s => new System.Web.UI.WebControls.ListItem()
                {
                    Enabled = true,
                    Text = s.Description,
                    Value = s.Id.ToString()
                }).ToArray());

            // Find the selected item if present
            var q = col.Find(s => s.Value == selectedId.ToString());
            if (q != null)
            {
                q.Selected = true;
            }

            // Collection list
            return col.OrderBy(s => s.Text);
        }
    }
}