﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.view_client_address")]
        public partial class view_client_address
        {
            [Column(Name = "client")]
            public Int32 Id { get; set; }

            [Column] public string name { get; set; }
            [Column] public string last_name_first { get; set; }
            [Column] public string coapplicant { get; set; }
            [Column] public string addr1 { get; set; }
            [Column] public string addr2 { get; set; }
            [Column] public string addr3 { get; set; }
            [Column] public string ssn_1 { get; set; }
            [Column] public string ssn_2 { get; set; }

            public view_client_address()
            {
                OnCreated();
            }

            private void OnCreated()
            {
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<view_client_address> view_client_addressTable
            {
                get
                {
                    return context.GetTable<view_client_address>();
                }
            }
        }
    }
}