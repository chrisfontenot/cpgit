﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.states")]
        public partial class state
        {
            [Column(Name = "state", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string MailingCode { get; set; }
            [Column] public string Name { get; set; }
            [Column] public bool ActiveFlag { get; set; }
            [Column] public bool @Default { get; set; }
            [Column] public bool UseInWeb { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public state()
            {
                OnCreated();
            }

            private void OnCreated()
            {
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<state> stateTable
            {
                get
                {
                    return context.GetTable<state>();
                }
            }
        }
    }

    public static partial class Utilitiy
    {
        /// <summary>
        /// Return the collection of items for a state dropdown list
        /// </summary>
        /// <param name="states">Pointer to the source "states" table</param>
        /// <returns></returns>
        public static IEnumerable<System.Web.UI.WebControls.ListItem> GetStateListItems(IEnumerable<DebtPlus.state> states)
        {
            // Add the first item which is selected
            var col = new System.Collections.Generic.List<System.Web.UI.WebControls.ListItem>();
            col.Add(new System.Web.UI.WebControls.ListItem("--Please choose one--", "0", false));

            // Append the other states
            col.AddRange(states
               .Where(s => s.ActiveFlag && s.Id != 0)
               .Select(s => new System.Web.UI.WebControls.ListItem()
               {
                   Enabled = true,
                   Text = s.Name,
                   Value = s.Id.ToString()
               }).ToArray());

            return col.ToArray();
        }

        /// <summary>
        /// Return the collection of items for a state dropdown list
        /// </summary>
        /// <param name="states">Pointer to the source "states" table</param>
        /// <param name="selectedId">ID if the selected state if desired. 0 if none.</param>
        /// <returns></returns>
        public static IEnumerable<System.Web.UI.WebControls.ListItem> GetStateListItems(IEnumerable<DebtPlus.state> states, int selectedId)
        {
            var col = GetStateListItems(states);
            var q = col.Where(s => s.Value == selectedId.ToString()).FirstOrDefault();
            if (q != null)
            {
                q.Selected = true;
            }

            return col;
        }
    }
}