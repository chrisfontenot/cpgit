﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClearPoint
{
    public static class Settings
    {
        /// <summary>
        /// Disable the CAPCHA for testing purposes
        /// </summary>
        public static Boolean DisableCapcha
        {
            get
            {
                string value = System.Configuration.ConfigurationManager.AppSettings["DisableCapcha"];
                if (!string.IsNullOrWhiteSpace(value))
                {
                    bool booleanAnswer;
                    if (bool.TryParse(value, out booleanAnswer))
                    {
                        return booleanAnswer;
                    }
                }
                return false;
            }
        }
    }
}