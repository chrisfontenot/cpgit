﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Web.Security;
using ClearPoint;

namespace AttorneyPortal.Providers
{
    public partial class Membership : System.Web.Security.MembershipProvider
    {
        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public Membership() : base()
        {
        }

        // 
        // Global connection string, generated password length, generic exception message, event log info. 
        // 

        private int newPasswordLength = 8;
        private string eventSource = "DebtPlusMembershipProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "An exception occurred. Please check the Event Log.";
        private string connectionString;

        // True if we are to bypass password checking for this version. It is used only for the
        // admin web site, not the standard item on production. The admin pages have their own
        // web.config file with this option set.
        private bool AdminAccess { get; set; }

        // 
        // Used when determining encryption key values. 
        // 
        private MachineKeySection machineKey;

        // 
        // If false, exceptions are thrown to the caller. If true, 
        // exceptions are written to the event log. 
        // 

        private bool pWriteExceptionsToEventLog;

        public bool WriteExceptionsToEventLog
        {
            get { return pWriteExceptionsToEventLog; }
            set { pWriteExceptionsToEventLog = value; }
        }

        /// <summary>
        /// Perform the initialization sequence for the class
        /// </summary>
        public override void Initialize(string name, NameValueCollection config)
        {
            // 
            // Initialize values from web.config. 
            // 
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "DebtPlusMembershipProvider";
            }

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "DebtPlus Membership provider");
            }

            // Initialize the abstract base class. 
            base.Initialize(name, config);

            ApplicationName                      = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            pMaxInvalidPasswordAttempts           = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            pPasswordAttemptWindow                = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "0"));
            pMinRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            pMinRequiredPasswordLength            = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            pPasswordStrengthRegularExpression    = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            pEnablePasswordReset                  = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            pEnablePasswordRetrieval              = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            pRequiresQuestionAndAnswer            = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            pRequiresUniqueEmail                  = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
            pWriteExceptionsToEventLog            = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));
            AdminAccess                           = Convert.ToBoolean(GetConfigValue(config["adminAccess"], "false"));

            //
            // Initialize the hashing configuration
            //
            string temp_format = config["passwordFormat"] ?? "Hashed";
            switch (temp_format)
            {
                case "Hashed":
                    pPasswordFormat = MembershipPasswordFormat.Hashed;
                    break;

                case "Encrypted":
                    pPasswordFormat = MembershipPasswordFormat.Encrypted;
                    break;

                case "Clear":
                    pPasswordFormat = MembershipPasswordFormat.Clear;
                    break;

                default:
                    throw new ProviderException("Password format not supported.");
            }

            // 
            // Initialize Connection. 
            //
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];
            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            connectionString = ConnectionStringSettings.ConnectionString;


            // Get encryption and decryption key information from the configuration.
            Configuration cfg = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");

            if (machineKey.ValidationKey.Contains("AutoGenerate"))
            {
                if (PasswordFormat == MembershipPasswordFormat.Encrypted)
                {
                    throw new ProviderException("Encrypted passwords are not supported with auto-generated keys.");
                }
            }
        }

        /// <summary>
        /// Helper to convert the config entries to a suitable value
        /// </summary>
        private string GetConfigValue(string configValue, string defaultValue)
        {
            return String.IsNullOrEmpty(configValue) ? defaultValue : configValue;
        }


        // 
        // System.Web.Security.MembershipProvider properties. 
        // 
        private bool pEnablePasswordReset;
        private bool pEnablePasswordRetrieval;
        private bool pRequiresQuestionAndAnswer;
        private bool pRequiresUniqueEmail;
        private int pMaxInvalidPasswordAttempts;
        private int pPasswordAttemptWindow;
        private MembershipPasswordFormat pPasswordFormat;

        public override string ApplicationName { get; set; }

        public override bool EnablePasswordReset
        {
            get { return pEnablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return pEnablePasswordRetrieval; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return pRequiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return pRequiresUniqueEmail; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return pMaxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return pPasswordAttemptWindow; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return pPasswordFormat; }
        }

        private int pMinRequiredNonAlphanumericCharacters;

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return pMinRequiredNonAlphanumericCharacters; }
        }

        private int pMinRequiredPasswordLength;

        public override int MinRequiredPasswordLength
        {
            get { return pMinRequiredPasswordLength; }
        }

        private string pPasswordStrengthRegularExpression;
        public override string PasswordStrengthRegularExpression
        {
            get { return pPasswordStrengthRegularExpression; }
        }

        // ====================================================================================================================
        // ====================================================================================================================
        // ========================         System.Web.Security.MembershipProvider methods.            ======================== 
        // ====================================================================================================================
        // ====================================================================================================================

        /// <summary>
        /// Change the account's password given the old password
        /// </summary>
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            if (!ValidateUser(username, oldPwd))
            {
                return false;
            }

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPwd, true);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null)
                {
                    throw args.FailureInformation;
                }
                throw new MembershipPasswordException("Change password canceled due to new password validation failure.");
            }

            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                    if (record != null)
                    {
                        record.Password = EncodePassword(newPwd);
                        record.LastPasswordChangeDate = DateTime.UtcNow;
                        db.SubmitChanges();
                        return true;
                    }
                }
                return false;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "ChangePassword");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Change the password security question and answer
        /// </summary>
        public override bool ChangePasswordQuestionAndAnswer(string username,
                      string password,
                      string newPwdQuestion,
                      string newPwdAnswer)
        {
            if (!ValidateUser(username, password))
            {
                return false;
            }

            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                    if (record != null)
                    {
                        record.PasswordQuestion = newPwdQuestion;
                        record.PasswordAnswer = EncodePassword(newPwdAnswer);
                        record.LastPasswordChangeDate = DateTime.UtcNow;
                        db.SubmitChanges();
                        return true;
                    }
                }
                return false;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "ChangePasswordQuestionAndAnswer");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Create a new user for access
        /// </summary>
        public override MembershipUser CreateUser(string username,
                 string password,
                 string email,
                 string passwordQuestion,
                 string passwordAnswer,
                 bool isApproved,
                 object providerUserKey,
                 out MembershipCreateStatus status)
        {
            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);
            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && ! string.IsNullOrEmpty(GetUserNameByEmail(email)))
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser u = GetUser(username, false);
            if (u != null)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return null;
            }

            if (providerUserKey == null)
            {
                providerUserKey = System.Guid.NewGuid();
            }
            else
            {
                if (!(providerUserKey is System.Guid))
                {
                    status = MembershipCreateStatus.InvalidProviderUserKey;
                    return null;
                }
            }

            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = new DebtPlus.client_www()
                    {
                        Id               = (System.Guid) providerUserKey,
                        UserName         = username,
                        Password         = EncodePassword(password),
                        Email            = email,
                        PasswordQuestion = passwordQuestion,
                        PasswordAnswer   = EncodePassword(passwordAnswer),
                        IsApproved       = isApproved,
                        ApplicationName  = ApplicationName
                    };

                    db.client_wwwTable.InsertOnSubmit(record);
                    db.SubmitChanges();

                    status = MembershipCreateStatus.Success;
                    return GetUser(username, false);
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "CreateUser");
                }

                status = MembershipCreateStatus.ProviderError;
                return null;
            }
        }

        /// <summary>
        /// Delete the user from the configuration table
        /// </summary>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.UserName == username && s.ApplicationName == ApplicationName).FirstOrDefault();
                    if (record != null)
                    {
                        db.client_wwwTable.DeleteOnSubmit(record);
                        db.SubmitChanges();
                        return true;
                    }
                }
                return false;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "DeleteUser");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Retrieve all possible users from the table
        /// </summary>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            totalRecords = 0;
            try
            {
                MembershipUserCollection users = new MembershipUserCollection();
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    totalRecords = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName).Count();
                    System.Collections.Generic.List<DebtPlus.client_www> colRecords = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName).Take(pageSize).Skip(pageSize * pageIndex).ToList();
                    foreach (var record in colRecords)
                    {
                        users.Add(GetUserFromRecord(record));
                    }
                    return users;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "GetAllUsers");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Determine the number of users that are online
        /// </summary>
        public override int GetNumberOfUsersOnline()
        {
            TimeSpan onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
            DateTime compareTime = DateTime.UtcNow.Subtract(onlineSpan);

            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    return db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.LastActivityDate > compareTime).Count();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "GetNumberOfUsersOnline");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Return the user's password from the encrypted storage
        /// </summary>
        public override string GetPassword(string username, string answer)
        {
            if (!EnablePasswordRetrieval)
            {
                throw new ProviderException("Password Retrieval Not Enabled.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Hashed)
            {
                throw new ProviderException("Cannot retrieve Hashed passwords.");
            }

            using (var db = new DebtPlus.DatabaseContext(connectionString))
            {
                var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                if (record == null)
                {
                    throw new MembershipPasswordException("The supplied user name is not found.");
                }

                // Unlock the record if we can.
                if (record.IsLockedOut && PasswordAttemptWindow > 0)
                {
                    var cutoffTime = record.FailedPasswordAnswerAttemptWindowStart.Add(new TimeSpan(TimeSpan.TicksPerMinute * (long)PasswordAttemptWindow));
                    if (DateTime.UtcNow > cutoffTime)
                    {
                        record.FailedPasswordAnswerAttemptCount = 0;
                        record.IsLockedOut = false;
                    }
                    record.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow;
                    db.SubmitChanges();
                }

                if (record.IsLockedOut && !AdminAccess)
                {
                    throw new MembershipPasswordException("The supplied user is locked out.");
                }

                if (RequiresQuestionAndAnswer && !CheckPassword(answer, record.PasswordAnswer))
                {
                    record.FailedPasswordAnswerAttemptCount += 1;
                    record.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow;
                    if (record.FailedPasswordAttemptCount > MaxInvalidPasswordAttempts)
                    {
                        record.IsLockedOut = true;
                        record.LastLockoutDate = DateTime.UtcNow;
                    }
                    db.SubmitChanges();
                    throw new MembershipPasswordException("Incorrect password answer.");
                }

                if (PasswordFormat == MembershipPasswordFormat.Encrypted)
                {
                    return UnEncodePassword(record.Password);
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieve a user record given the primary key (Id). If needed also indicate the user is online.
        /// </summary>
        public override MembershipUser GetUser(object primaryKey, bool userIsOnline)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.Id == (Guid) primaryKey).FirstOrDefault();
                    if (record == null)
                    {
                        return null;
                    }

                    if (userIsOnline)
                    {
                        record.LastActivityDate = DateTime.UtcNow;
                        db.SubmitChanges();
                    }

                    return GetUserFromRecord(record);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "GetUser(Object, Boolean)");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Retrieve the user record given the user name.
        /// </summary>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                    if (record == null)
                    {
                        return null;
                    }

                    if (userIsOnline)
                    {
                        record.LastActivityDate = DateTime.UtcNow;
                        db.SubmitChanges();
                    }

                    return GetUserFromRecord(record);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "GetUser(String, Boolean)");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Retrieve the user record given the email address
        /// </summary>
        public override string GetUserNameByEmail(string email)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.Email == email).FirstOrDefault();
                    if (record == null)
                    {
                        return string.Empty;
                    }
                    return record.UserName ?? string.Empty;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "GetUserNameByEmail");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Local routine to convert client_www row into a MembershipUser record
        /// </summary>
        private MembershipUser GetUserFromRecord(DebtPlus.client_www record)
        {
            return new MembershipUser(this.Name,
                                      record.UserName,
                                      record.Id,
                                      record.Email,
                                      record.PasswordQuestion,
                                      record.Comment,
                                      record.IsApproved,
                                      record.IsLockedOut,
                                      record.CreationDate,
                                      record.LastLoginDate,
                                      record.LastActivityDate,
                                      record.LastPasswordChangeDate,
                                      record.LastLockoutDate.GetValueOrDefault());
        }

        /// <summary>
        /// Unlock the specified user
        /// </summary>
        public override bool UnlockUser(string username)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                    if (record == null)
                    {
                        return false;
                    }

                    record.LastLockoutDate = DateTime.UtcNow;
                    record.IsLockedOut = false;
                    db.SubmitChanges();
                    return true;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "UnlockUser");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Reset the users password given the answer to the security question
        /// </summary>
        public override string ResetPassword(string username, string answer)
        {
            if (!EnablePasswordReset)
            {
                throw new NotSupportedException("Password reset is not enabled.");
            }

            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.ApplicationName == ApplicationName && s.UserName == username).FirstOrDefault();
                    if (record == null)
                    {
                        throw new MembershipPasswordException("The supplied user name is not found.");
                    }

                    // Unlock the record if we can.
                    if (RequiresQuestionAndAnswer)
                    {
                        // Attempt to unlock the account automatically if possible.
                        if (record.IsLockedOut && PasswordAttemptWindow > 0)
                        {
                            var cutoffTime = record.FailedPasswordAnswerAttemptWindowStart.Add(new TimeSpan(TimeSpan.TicksPerMinute * (long)PasswordAttemptWindow));
                            if (DateTime.UtcNow > cutoffTime)
                            {
                                record.FailedPasswordAnswerAttemptCount = 0;
                                record.IsLockedOut = false;
                            }
                            record.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow;
                            db.SubmitChanges();
                        }

                        if (record.IsLockedOut && !AdminAccess)
                        {
                            throw new MembershipPasswordException("The supplied user is locked out.");
                        }

                        // Validate the password answer.
                        if (!CheckPassword(answer, record.PasswordAnswer))
                        {
                            record.FailedPasswordAnswerAttemptCount += 1;
                            record.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow;
                            if (record.FailedPasswordAnswerAttemptCount > MaxInvalidPasswordAttempts)
                            {
                                record.IsLockedOut = true;
                                record.LastLockoutDate = DateTime.UtcNow;
                            }
                            db.SubmitChanges();
                            throw new MembershipPasswordException("Incorrect password answer.");
                        }
                    }

                    else
                    {
                        // If the system did not need an answer then just check the lockout status
                        if (record.IsLockedOut)
                        {
                            throw new MembershipPasswordException("The supplied user is locked out.");
                        }
                    }

                    string newPassword = System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters);
                    ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPassword, true);
                    OnValidatingPassword(args);
                    if (args.Cancel)
                    {
                        if (args.FailureInformation != null)
                        {
                            throw args.FailureInformation;
                        }
                        else
                        {
                            throw new MembershipPasswordException("Reset password canceled due to password validation failure.");
                        }
                    }

                    record.Password = EncodePassword(newPassword);
                    record.LastPasswordChangeDate = DateTime.UtcNow;
                    db.SubmitChanges();

                    return newPassword;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "ResetPassword");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Update the user in the database from the membership user
        /// </summary>
        public override void UpdateUser(MembershipUser user)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.Id == (System.Guid)user.ProviderUserKey).FirstOrDefault();
                    if (record != null)
                    {
                        record.Email = user.Email;
                        record.Comment = user.Comment;
                        record.IsApproved = user.IsApproved;

                        // Change the approval status to "true" by default.
                        if (!record.IsApproved)
                        {
                            record.IsApproved = true;
                        }

                        db.SubmitChanges();
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "UpdateUser");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Validate the user's account name and password for access.
        /// </summary>
        public override bool ValidateUser(string username, string password)
        {
            try
            {
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    var record = db.client_wwwTable.Where(s => s.UserName == username && s.ApplicationName == ApplicationName).FirstOrDefault();
                    if (record == null)
                    {
                        return false;
                    }

                    // Change the approval status to "true" by default.
                    if (!record.IsApproved)
                    {
                        record.IsApproved = true;
                    }

                    // Attempt to unlock the user if possible
                    if (record.IsLockedOut && PasswordAttemptWindow > 0)
                    {
                        var cutoffTime = record.FailedPasswordAttemptWindowStart.Add(new TimeSpan(TimeSpan.TicksPerMinute * (long)PasswordAttemptWindow));
                        if (DateTime.UtcNow > cutoffTime)
                        {
                            record.FailedPasswordAttemptCount = 0;
                            record.IsLockedOut = false;
                        }
                        record.FailedPasswordAttemptWindowStart = DateTime.UtcNow;
                        db.SubmitChanges();
                    }

                    // If the record is still locked out then do not validate the user.
                    if (record.IsLockedOut && !AdminAccess)
                    {
                        return false;
                    }

                    // Validate the password
                    if (! record.IsApproved || ! CheckPassword(password, record.Password))
                    {
                        record.FailedPasswordAttemptCount += 1;
                        record.FailedPasswordAttemptWindowStart = DateTime.UtcNow;
                        if (record.FailedPasswordAttemptCount > MaxInvalidPasswordAttempts)
                        {
                            record.IsLockedOut = true;
                            record.LastLockoutDate = DateTime.UtcNow;
                        }
                        db.SubmitChanges();
                        return false;
                    }

                    // Update the login date
                    record.LastLoginDate = DateTime.UtcNow;
                    db.SubmitChanges();
                    return true;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "ValidateUser");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Validate the encrypted password with the new clear-text entry
        /// </summary>
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;

                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;

                default:
                    break;
            }

            // If the passwords match or we are Administrative access override then accept the pasword.
            if (pass1 == pass2 || AdminAccess)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Encrypt the clear-text password item
        /// </summary>
        private string EncodePassword(string password)
        {
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return password;

                case MembershipPasswordFormat.Encrypted:
                    return Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));

                case MembershipPasswordFormat.Hashed:
                    return GetMD5Hash(password);

                default:
                    throw new ProviderException("Unsupported password format.");
            }
        }

        /// <summary>
        /// Decrypt the user's password on the system if possible
        /// </summary>
        private string UnEncodePassword(string encodedPassword)
        {
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    return encodedPassword;

                case MembershipPasswordFormat.Encrypted:
                    return Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(encodedPassword)));

                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");

                default:
                    throw new ProviderException("Unsupported password format.");
            }
        }

        /// <summary>
        /// Convert the HEX string to a byte array
        /// </summary>
        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// Find the list of user information items from the username pattern
        /// </summary>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            try
            {
                MembershipUserCollection users = new MembershipUserCollection();
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    totalRecords = db.client_wwwTable.Count();

                    System.Collections.Generic.List<DebtPlus.client_www> colRecords = db.client_wwwTable.Where(s => s.UserName.Contains(usernameToMatch)).Take(pageSize).Skip(pageSize * pageIndex).OrderBy(s => s.UserName).ToList();
                    foreach (var record in colRecords)
                    {
                        users.Add(GetUserFromRecord(record));
                    }
                    return users;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "FindUsersByName");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Find the list of user name records from the email address pattern
        /// </summary>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            try
            {
                MembershipUserCollection users = new MembershipUserCollection();
                using (var db = new DebtPlus.DatabaseContext(connectionString))
                {
                    totalRecords = db.client_wwwTable.Count();

                    System.Collections.Generic.List<DebtPlus.client_www> colRecords = db.client_wwwTable.Where(s => s.Email.Contains(emailToMatch)).Take(pageSize).Skip(pageSize * pageIndex).OrderBy(s => s.UserName).ToList();
                    foreach (var record in colRecords)
                    {
                        users.Add(GetUserFromRecord(record));
                    }
                    return users;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(ex, "FindUsersByEmail");
                    throw new ProviderException(exceptionMessage, ex);
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Write an entry to the event log if needed.
        /// </summary>
        /// <remarks>
        /// This operation may, itself, trip an exception if the user is not allowed to write to the event log.
        /// Do not bother with those exceptions since the result is always a newly generated exception anyway.
        /// </remarks>
        private void WriteToEventLog(Exception e, string action)
        {
            try
            {
                // Create the event log to be written
                EventLog log = new EventLog()
                {
                    Source = eventSource,
                    Log = eventLog
                };

                // Ensure that the source exists for the log
                if (!EventLog.SourceExists(eventSource))
                {
                    EventLog.CreateEventSource(eventSource, eventLog); // this is the proper thing to do but will usually fail with an exception violation.
                }

                // Log the message
                string message = "An exception occurred communicating with the data source.\n\n";
                message += "Action: " + action + "\n\n";
                message += "Exception: " + e.ToString();

                log.WriteEntry(message);
            }
            catch { }
        }
    }
}