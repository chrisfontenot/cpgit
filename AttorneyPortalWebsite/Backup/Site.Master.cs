﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal
{
    public partial class Site : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Load event. Not used on the master page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}