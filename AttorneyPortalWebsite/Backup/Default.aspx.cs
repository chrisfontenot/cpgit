﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal
{
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Handle the page-load sequence. We only do one thing -- redirect the page to the
        /// secured copy. This is only for people who saved the original setting. Everyone else
        /// should bookmark the "/s/" location since that is where they will end up.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/s/Default.aspx");
        }
    }
}