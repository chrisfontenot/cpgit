﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/s/PageMaster.master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="AttorneyPortal.account.s.ChangePassword" ValidateRequest="false" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="PageBody">
    <p>
        Use the form below to change your password.
    </p>
    <p>
        New passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
    </p>

    <asp:ChangePassword ID="ChangePassword1" runat="server" 
        DisplayUserName="True"
        InstructionText="Enter your client id and old password." 
        ContinueDestinationPageUrl="~/Default.aspx"
        Font-Names="Verdana">

        <CancelButtonStyle BackColor="White" BorderColor="#C5BBAF" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#1C5E55" />

        <ChangePasswordButtonStyle BackColor="White" BorderColor="#C5BBAF" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#1C5E55" />

        <ContinueButtonStyle BackColor="White" BorderColor="#C5BBAF" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#1C5E55" />

        <TitleTextStyle Font-Bold="True" ForeColor="Black" />

        <SuccessTemplate>
            <table border="0" cellpadding="4">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" 
                            style="font-family: Verdana;">
                            <tr>
                                <td align="center" colspan="2" 
                                    style="font-weight: bold;">
                                    Change Password Complete</td>
                            </tr>
                            <tr>
                                <td>
                                    Your password has been changed!</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button BackColor="White" 
                                        BorderColor="#C5BBAF" 
                                        BorderStyle="Solid" 
                                        BorderWidth="1px"
                                        CausesValidation="False" 
                                        CommandName="Continue" 
                                        Font-Names="Verdana" 
                                        ForeColor="#1C5E55" 
                                        ID="ContinuePushButton"
                                        runat="server" Text="Continue" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </SuccessTemplate>

        <ChangePasswordTemplate>
            <table border="0" cellpadding="4">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" 
                            style="font-family: Verdana;">
                            <tr>
                                <td align="center" colspan="2" 
                                    font-weight: bold;">
                                    Change Your Password</td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" 
                                    style="color: Black; 
                                    font-style: italic;">
                                    Enter your client id and old password.
                                    </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label 
                                        AssociatedControlID="UserName" 
                                        ID="UserNameLabel" runat="server">
                                        User Name:</asp:Label>
                                    </td>
                                <td>
                                    <asp:TextBox ID="UserName" 
                                        runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator 
                                        ControlToValidate="UserName" 
                                        ErrorMessage="Username is required."
                                        ID="UserNameRequired" 
                                        runat="server" 
                                        ToolTip="Username is required." 
                                        ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label 
                                        AssociatedControlID="CurrentPassword"
                                        ID="CurrentPasswordLabel" 
                                        runat="server">Current Password:</asp:Label>
                                        </td>
                                <td>
                                    <asp:TextBox ID="CurrentPassword" 
                                        runat="server" TextMode="Password">
                                        </asp:TextBox>
                                    <asp:RequiredFieldValidator 
                                        ControlToValidate="CurrentPassword" 
                                        ErrorMessage="Current Password is required."
                                        ID="CurrentPasswordRequired" 
                                        runat="server" 
                                        ToolTip="Current Password is required." 
                                        ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label 
                                        AssociatedControlID="NewPassword" 
                                        ID="NewPasswordLabel" 
                                        runat="server">New Password:
                                        </asp:Label></td>
                                <td>
                                    <asp:TextBox ID="NewPassword" 
                                        runat="server" TextMode="Password">
                                        </asp:TextBox>
                                    <asp:RequiredFieldValidator 
                                        ControlToValidate="NewPassword" 
                                        ErrorMessage="New Password is required."
                                        ID="NewPasswordRequired" 
                                        runat="server" 
                                        ToolTip="New Password is required." 
                                        ValidationGroup="ChangePassword1">*
                                        </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label 
                                        AssociatedControlID="ConfirmNewPassword" 
                                        ID="ConfirmNewPasswordLabel"
                                        runat="server">Confirm New Password:
                                        </asp:Label>
                                    </td>
                                <td>
                                    <asp:TextBox ID="ConfirmNewPassword" 
                                        runat="server" 
                                        TextMode="Password">
                                        </asp:TextBox>
                                    <asp:RequiredFieldValidator 
                                        ControlToValidate="ConfirmNewPassword" 
                                        ErrorMessage="Confirm New Password is required."
                                        ID="ConfirmNewPasswordRequired" 
                                        runat="server" 
                                        ToolTip="Confirm New Password is required."
                                        ValidationGroup="ChangePassword1">*
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator 
                                        ControlToCompare="NewPassword" 
                                        ControlToValidate="ConfirmNewPassword"
                                        Display="Dynamic" 
                                        ErrorMessage="The confirm New Password must match the New Password entry."
                                        ID="NewPasswordCompare" 
                                        runat="server" 
                                        ValidationGroup="ChangePassword1">
                                        </asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" 
                                    style="color: Red;">
                                    <asp:Literal EnableViewState="False" 
                                        ID="FailureText" runat="server">
                                    </asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button BackColor="White" 
                                        BorderColor="#C5BBAF" 
                                        BorderStyle="Solid" 
                                        BorderWidth="1px"
                                        CommandName="ChangePassword" 
                                        Font-Names="Verdana" 
                                        ForeColor="#1C5E55"
                                        ID="ChangePasswordPushButton" 
                                        runat="server" 
                                        Text="Change Password" 
                                        ValidationGroup="ChangePassword1" />
                                </td>
                                <td>
                                    <asp:Button BackColor="White" 
                                        BorderColor="#C5BBAF" 
                                        BorderStyle="Solid" 
                                        BorderWidth="1px"
                                        CausesValidation="False" 
                                        CommandName="Cancel" 
                                        Font-Names="Verdana" 
                                        ForeColor="#1C5E55" 
                                        ID="CancelPushButton" 
                                        runat="server" 
                                        Text="Cancel" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="margin-left:20px">
                                    <p class="bold">Password Guidelines</p>

                                    <p>
                                    Choosing your Password is an important part of establishing the security for your account. The following Do's and Don'ts can help you select something that will
                                    be easy for you to remember but difficult for "outsiders" to guess. We purposely ignore the case of the password so a lower case letter is the same as an upper
                                    case letter. It solves problems should you accidently leave the caps lock set on your keyboard or don't remember exactly if you used an upper case A or a lower
                                    case one.
                                    </p>

                                    <p class="bold">Do's</p>

                                    <p>
                                        <ul>
                                            <li>Mix letters and numbers.</li>
                                            <li>Select codes that will be easy for you to remember.</li>
                                            <li>Use phrases that combine spaces and words (i.e. "Mary had a little lamb").</li>
                                            <li>The longer the password the better for most cases. A phrase is easier to remember than a bunch of jumbled letters.</li>
                                            <li>We do not limit the length of your password however what you use must exactly match when you access our site again, letter by letter, space by space.</li>
                                        </ul>
                                    </p>

                                    <p class="bold">Don'ts</p>

                                    <p>
                                        <ul>
                                            <li>Don't use a single word that can be found in the dictionary.</li>
                                            <li>Don't use something readily identifiable such as your birthday(, spouse Or child) 's name, social security number, phone number or street address. While these may be easy for you to remember, they can be traced directly to you and are easy for potential hackers to guess.</li>
                                            <li>Don't use letters or numbers that are near each other on the key board (i.e. hjkl;) or that appear in a logical sequence (i.e. 123456 or abcdefg).</li>
                                            <li>Don't ever share your codes with anyone. You may trust your friends; but do you trust the friends of your friend or their friends?</li>
                                        </ul>
                                    </p>

                                    <p class="bold">Password Selection Methods</p>

                                    <p>Have some fun and try following one of these formulas:</p>

                                    <p>
                                        <ul>
                                            <li>Choose a meaningful phrase with 6-8 words in it. For example, we bought our home 9 years ago. Now take the first character of each word to make your password: wboh9ya</li>
                                            <li>Pick a word and insert two digits in it, e.g. Decem51ber</li>
                                            <li>Replace vowels or other letters in a short phrase with numbers or other characters</li>
                                            <li>Misspell a word, drop some letters, add other characters, or make up crazy words using symbols instead of vowels or consonants.</li>
                                            <li>Drop the vowels in a long word. Corporation would yield crprtn. Even better, add some numbers: 2crprtn9</li>
                                        </ul>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ChangePasswordTemplate>

        <PasswordHintStyle Font-Italic="True" ForeColor="#1C5E55" />
        <TextBoxStyle Font-Size="0.8em" />
        <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
    </asp:ChangePassword>
</asp:Content>
