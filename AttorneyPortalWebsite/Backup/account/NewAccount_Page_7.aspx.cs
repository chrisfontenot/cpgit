﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_7 : System.Web.UI.Page
    {
        // Pointer to the current structure for the form controls
        Signup_Page7 params7 = null;

        /// <summary>
        /// Initialize the new page
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link7.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link7.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params7 = Session["Signup_Page7"] as Signup_Page7;
                if (params7 == null)
                {
                    params7 = new Signup_Page7();
                    var params5 = Session["Signup_Page5"] as Signup_Page5;
                    if (params5 != null)
                    {
                        (params7.Billing as ICopyable<ContactBlock>).Copy(params5.Principal);
                    }
                    params7.Billing.SameAddress = true; // Do this AFTER the copy operation.
                }

                // Copy the account address if the flag says that the value is to be copied.
                if (params7.Billing.SameAddress)
                {
                    // Copy the address from the firm value if we have something
                    var params4 = Session["Signup_Page4"] as Signup_Page4;
                    if (params4 != null)
                    {
                        params7.Billing.Address.Copy(params4.Address);
                    }
                }

                // On the initial load, set the entries according to the parameters
                if (!Page.IsPostBack)
                {
                    // Load the list of state names
                    using (var db = new ClearPoint.DebtPlus.DatabaseContext())
                    {
                        contact_state.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, params7.Billing.Address.State.GetValueOrDefault(61)).ToArray());
                    }

                    // Set the values from the input record.
                    first_name.Text        = params7.Billing.Name.First;
                    middle_name.Text       = params7.Billing.Name.Middle;
                    last_name.Text         = params7.Billing.Name.Last;
                    contact_address_1.Text = params7.Billing.Address.Line1();
                    contact_city.Text      = params7.Billing.Address.City;
                    contact_zip.Text       = params7.Billing.Address.Zip;
                    telephone.Text         = params7.Billing.TelephoneNumber.ToString();
                    fax.Text               = params7.Billing.FaxNumber.ToString();
                    ext.Text               = params7.Billing.TelephoneNumber.ext;
                    email.Text             = params7.Billing.Email.ToString();
                    same_as_firm.Checked   = params7.Billing.SameAddress;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Decode the first name
            var sb = new System.Text.StringBuilder();

            // Decode the simple fields. We need a "try" for each as the errors are separate and we want to see them all.
            try { params7.Billing.TelephoneNumber.Decode(telephone.Text.Trim()); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.FaxNumber.Decode(fax.Text.Trim());             } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.Name.First          = first_name.Text.Trim();  } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.Name.Middle         = middle_name.Text.Trim(); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.Name.Last           = last_name.Text.Trim();   } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.TelephoneNumber.ext = ext.Text.Trim();         } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params7.Billing.Email.Address       = email.Text.Trim();       } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }

            // This statement can not generate an error. It just saves the status of the checkbox.
            params7.Billing.SameAddress = same_as_firm.Checked;

            // Decode the address information if the address is different from the firm's
            if (!params7.Billing.SameAddress)
            {
                try
                {
                    var adr = new System.Text.StringBuilder();
                    var str = contact_address_1.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = contact_address_2.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = contact_city.Text + " VA " + contact_zip.Text;
                    adr.AppendFormat("\r\n{0}", str);
                    adr.Remove(0, 2);

                    params7.Billing.Address.Normalize(adr.ToString());
                    params7.Billing.Address.State = Int32.Parse(contact_state.SelectedValue);
                }
                catch (Exception ex)
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
                }
            }

            // Finally validate the structure if there are no parsing errors. Skip this if there are errors
            // since we don't need the errors to cascade.
            if (sb.Length == 0)
            {
                try { params7.Validate(); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference and transfer to the next page.
            Session["Signup_Page7"] = params7;
            Response.Redirect("NewAccount_Page_10.aspx", true);
        }
    }
}