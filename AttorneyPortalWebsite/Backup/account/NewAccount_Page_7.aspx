﻿<%@ Page Title="Billing Information" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_7.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_7" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Table ID="Table2" Width="100%" runat="server" CellPadding="0" CellSpacing="0">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell runat="server" HorizontalAlign="Left" ColumnSpan="2">
                <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

                <h1>Billing Contact</h1>
                <p>
                    Since you have specified that one or more of the services are to be associated with an escrow account,
                    we need to know the billing information on how to bill your agency for the consultation fees. Please supply
                    the information for the person to whom we may contact regarding the billing for our services. The contact's address
                    and will be the same as the firm's address.
                </p>

                <p>
                    We do need the contact's name, telephone number (and extension if required), along with their E-mail address
                    to complete the billing contact data. We will send you an Email notice that the invoice is ready to be examined and clients
                    disputed on or around the 1st of each month.
                </p>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="first_name_label" AssociatedControlID="first_name" Font-Bold="true" Text="First Name" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="first_name" MaxLength="40" ToolTip="Person's first name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="middle_name_label" AssociatedControlID="middle_name" Font-Bold="true" Text="Middle Name" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="middle_name" MaxLength="40" ToolTip="Person's middle name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="last_name_label" AssociatedControlID="last_name" Font-Bold="true" Text="Last Name" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="last_name" MaxLength="40" ToolTip="Person's last name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="telephone_label" AssociatedControlID="telephone" Font-Bold="true" Text="Telephone" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="telephone" MaxLength="40" ToolTip="Telephone number for the contact" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="ext_label" AssociatedControlID="ext" Font-Bold="true" Text="Ext #" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="ext" MaxLength="40" ToolTip="Telephone extension number for the contact" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="fax_label" AssociatedControlID="fax" Font-Bold="true" Text="FAX" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="fax" MaxLength="40" ToolTip="FAX number for the contact" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="email_label" AssociatedControlID="email" Font-Bold="true" Text="Email" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="email" MaxLength="512" ToolTip="Email address for the person" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" colspan="2">
                &nbsp;
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" colspan="2">
                <h1>Contact Address</h1>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1" colspan="2">
                <asp:CheckBox ID="same_as_firm" runat="server" Text="Same as firm's address" />
                <div>&nbsp;</div>
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="contact_address_1_label" AssociatedControlID="contact_address_1" Font-Bold="true" Text="Address" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="contact_address_1" MaxLength="50" ToolTip="Please enter your firm's mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="contact_address_2_label" AssociatedControlID="contact_address_2" Font-Bold="true" Text="Address (cont.)" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="contact_address_2" MaxLength="50" ToolTip="Additional line if needed for mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="contact_city_label" AssociatedControlID="contact_city" Font-Bold="true" Text="City" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="contact_city" MaxLength="50" ToolTip="Please enter your firm's city name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="contact_state_label" AssociatedControlID="contact_state" Font-Bold="true" Text="State" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:DropDownList runat="server" ID="contact_state" ToolTip="In which state is your located?" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow runat="server">
            <asp:TableCell runat="server" CssClass="style1">
                <asp:Label runat="server" ID="contact_zip_label" AssociatedControlID="contact_zip" Font-Bold="true" Text="ZIP" />
            </asp:TableCell><asp:TableCell runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="contact_zip" MaxLength="11" ToolTip="Please enter your postal ZIP code" Width="100px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow></asp:Table><div style="text-align:center; height:200px; vertical-align:bottom;">
        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
    </div>

<script language="javascript" type="text/javascript">
    function clearAddress() {
        var same_as_firmID = '<%= same_as_firm.ClientID %>';
        var chk = document.getElementById(same_as_firmID);
        if (chk != null) {
            chk.checked = false;
        }
        return true;
    }
</script>
</asp:Content>
