﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_4 : System.Web.UI.Page
    {
        // Current pointer to the datastorage block
        Signup_Page4 params4 = null;

        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link4.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link4.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the box if it was previously checked
            params4 = Session["Signup_Page4"] as Signup_Page4;
            if (params4 == null)
            {
                params4 = new Signup_Page4();
            }

            // On the initial load, set the entries according to the parameters
            if (! Page.IsPostBack)
            {
                // Load the list of state names
                using (var db = new ClearPoint.DebtPlus.DatabaseContext())
                {
                    firm_state.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, params4.Address.State.GetValueOrDefault(61)).ToArray());
                }

                firm_address_1.Text = params4.Address.Line1();
                firm_address_2.Text = string.Empty;
                firm_name.Text      = params4.FirmName;
                firm_city.Text      = params4.Address.City;
                firm_zip.Text       = params4.Address.Zip;
                website.Text        = params4.WebSite;
                return;
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Update the user name
            var sb = new System.Text.StringBuilder();
            try
            {
                params4.FirmName = firm_name.Text.Trim();
                params4.WebSite  = website.Text.Trim();
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
            }

            try
            {
                var addr = new System.Text.StringBuilder();
                var str1 = firm_address_1.Text.Trim();
                if (str1 != string.Empty)
                {
                    addr.AppendFormat("\r\n{0}", str1);
                }

                str1 = firm_address_2.Text.Trim();
                if (str1 != string.Empty)
                {
                    addr.AppendFormat("\r\n{0}", str1);
                }

                str1 = string.Format("{0} {1} {2}", firm_city.Text.Trim(), "VA", firm_zip.Text.Trim());
                addr.AppendFormat("\r\n{0}", str1);
                addr.Remove(0, 2);

                params4.Address.Normalize(addr.ToString());
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
            }

            try
            {
                var item = firm_state.SelectedItem;
                if (item == null)
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("The state identifier must be specified"));
                }
                else
                {
                    params4.Address.State = Int32.Parse(item.Value);
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
            }

            // Ensure that the required pieces are present
            if (sb.Length == 0)
            {
                try
                {
                    params4.Validate();
                }
                catch (Exception ex)
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
                }
            }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference and exit.
            Session["Signup_Page4"] = params4;
            Response.Redirect("NewAccount_Page_10.aspx", true);
        }
    }
}