﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AttorneyPortal.account.Login" ValidateRequest="false" %>
<%@ MasterType virtualpath="~/Site.Master" %>

<asp:Content ID="HeadContent" runat="server" ContentPlaceHolderID="SiteHead">
    <style type="text/css">
        table.cntr
        {
            width: 90%;
            margin-left: 5%;
            margin-right: 5%;
            text-align: center;
        }
        
        .InputButton
        {
            background-color: #33FF00;
            -moz-border-radius: 15px;
            -webkit-border-radius: 15px;
            border-style:solid;
            border-color:White;
            border-width:2px;
            padding: 5px;
            height: 40px;
            width: 380px;
            color:Black;
            font-weight:bold;
            font-size:12pt;
            text-align:center; 
        }
        
        .TextInput
        {
            padding-left:10px;
            padding-right:10px;
            vertical-align:middle;
            text-align:left;
            height:30px;
            width:380px;
            color:Black;
        }
        
        .ErrorText
        {
            font-family:@Arial Unicode MS;
            font-weight:bold;
            font-size:18pt;
            color:Maroon;
            text-align:center;
        }
    </style>
</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="SiteBody">

    <div style="margin-left:30%; width:40%;">
        <table>
            <tr>
                <td class="ErrorText">
                    <asp:Literal ID="FailureText" Mode="PassThrough"  runat="server"></asp:Literal>
                </td>
            </tr>

            <tr>
                <td style="border: thin solid #CCCCCC; padding: 10px;">
                    <div style="font:Arial; font-size:24pt; font-weight:bold; vertical-align:top; height:34px">
                        Sign In
                    </div>

                    <div>
                        <br />
                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                        <br />
                        <asp:TextBox ID="UserName" runat="server" CssClass="TextInput" Columns="40"/>
                    </div>

                    <div>
                        <br />
                        <div style="float:left">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:&nbsp;&nbsp;</asp:Label>
                        </div>
                        <div style="float:right">
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/account/ForgottenPassword.aspx" Target="_blank" Text="Forgot your password?" Font-Bold="true" />
                        </div>
                    </div>
                    <asp:TextBox ID="Password" runat="server" CssClass="TextInput" TextMode="Password" />

                    <div style="width:100%; text-align:center">
                        <br />
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Sign-In" CssClass="InputButton" BackColor="#FFCC00" />
                    </div>

                    <div style="text-align:center; height:30px">
                        <br />
                        New to Clearpoint?
                    </div>

                    <div style="text-align:center">
                        <input onclick="javascript:window.location='NewAccount_Page_1.aspx';return false;" class="InputButton" type="button" runat="server" id="NewAccount" name="NewAccount" value="Create your Account" style="background-color:#CCCCCC;" />
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="clearboth"></div>
    <br />
    <p style="text-align:center; color:Red; font-weight:bold;">
        This page is for legal professionals only.
    </p>

    <p style="text-align:center">
        If you need to contact us or have questions,
        <br />
        please call 877-877-1995,
        <br />
        or e-mail us:
        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="SmallLink" NavigateUrl="mailto:bkexpress@clearpoint.org" Text="bkexpress@clearpoint.org" />
    </p>

    <div class="footer no-print" style="padding: 0px; margin: 0px; text-align:center; width: 100%;">
        <p class="copyright">
            Copyright 2017
        </p>
        <div class="clearboth"></div>
        <div class="partners-logos" style="text-align:center">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/partners-logos.gif" Height="58" Width="693" AlternateText="Partner Organizations" />
        </div>
    </div>
</asp:Content>