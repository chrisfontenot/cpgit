﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_9 : System.Web.UI.Page
    {
        // Pointer to the current structure for the form controls
        Signup_Page9 params9 = null;

        /// <summary>
        /// Initialize the new page
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link9.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link9.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params9 = Session["Signup_Page9"] as Signup_Page9;
                if (params9 == null)
                {
                    params9 = new Signup_Page9();
                    params9.attorneys = 1;
                    params9.annual_clients = 1;
                }

                // On the initial load, set the entries according to the parameters
                if (!Page.IsPostBack)
                {
                    attorneys.Text      = params9.attorneys.ToString();
                    annual_clients.Text = params9.annual_clients.ToString();
                    referral.Text       = params9.referral;
                    comments.Text       = params9.comments;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                var sb = new System.Text.StringBuilder();
                if (! System.Text.RegularExpressions.Regex.IsMatch(attorneys.Text, @"^[1-9][0-9]*$"))
                {
                    sb.AppendFormat("<li>{0}</li>", "Number of attorneys must be at least 1");
                }
                else
                {
                    params9.attorneys = int.Parse(attorneys.Text.Trim());
                }

                if (! System.Text.RegularExpressions.Regex.IsMatch(annual_clients.Text, @"^[1-9][0-9]*$"))
                {
                    sb.AppendFormat("<li>{0}</li>", "Estimated number of annual clients must be at least 1");
                }
                else
                {
                    params9.annual_clients = int.Parse(annual_clients.Text.Trim());
                }

                params9.referral       = referral.Text;
                params9.comments       = comments.Text;

                // Finally validate the structure if there are no parsing errors. Skip this if there are errors
                // since we don't need the errors to cascade.
                if (sb.Length == 0)
                {
                    try { params9.Validate(); }
                    catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
                }

                // If there is an error then indicate it in red and leave without posting the values
                if (sb.Length > 0)
                {
                    sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                    sb.Append("</ul>");
                    ValidationSummary.Text = sb.ToString();
                    return;
                }

                // Update the form values for later reference and transfer to the next page.
                Session["Signup_Page9"] = params9;
                Response.Redirect("NewAccount_Page_10.aspx", true);
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}