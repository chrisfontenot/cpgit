﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class PasswordRecovery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PWRecovery.VerifyingUser += new System.Web.UI.WebControls.LoginCancelEventHandler(PWRecovery_VerifyingUser);
            if (IsPostBack)
            {
                var cookie = Request.Cookies["usernameCookie"];
                if (cookie != null)
                {
                    Literal personname = ((Literal)PWRecovery.QuestionTemplateContainer.FindControl("personname"));
                    personname.Text = Request.Cookies["usernameCookie"].Value;
                }
            }
        }

        protected void PWRecovery_VerifyingUser(object sender, LoginCancelEventArgs e)
        {
            TextBox EmailAddressTB = ((TextBox)PWRecovery.UserNameTemplateContainer.FindControl("EmailAddressTB"));
            Literal ErrorLiteral = ((Literal)PWRecovery.UserNameTemplateContainer.FindControl("ErrorLiteral"));
            MembershipUser mu = Membership.GetUser(PWRecovery.UserName);

            if (mu == null) // The username not found
            {
                e.Cancel = true;
                ErrorLiteral.Text = "No such user found.";
                return;
            }

            if (!mu.Email.Equals(EmailAddressTB.Text)) // Their email matches
            {
                e.Cancel = true;
                ErrorLiteral.Text = "The email address that you entered does not match your account.";
                return;
            }

            // Set the cookie for the user name
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;
            Int32 ClientID = Int32.Parse(mu.UserName);

            using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                cn.Open();
                using (var cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT [name] FROM [view_client_address] WHERE [client]=@clientID";
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.Add("@clientID", System.Data.SqlDbType.Int).Value = ClientID;
                    var obj = cmd.ExecuteScalar();
                    if (obj != null && !System.Object.Equals(obj, System.DBNull.Value))
                    {
                        HttpCookie appCookie = new HttpCookie("usernameCookie");
                        appCookie.Value = Convert.ToString(obj).Trim();
                        appCookie.Expires = DateTime.Now.AddMinutes(3);
                        Response.Cookies.Add(appCookie);

                        // Ensure that the nme is filled in on the second page if needed.
                        Literal personname = ((Literal)PWRecovery.QuestionTemplateContainer.FindControl("personname"));
                        personname.Text = Convert.ToString(obj).Trim();
                    }
                }
            }
        }
    }
}
