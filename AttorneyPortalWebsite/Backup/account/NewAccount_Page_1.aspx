﻿<%@ Page Title="New Attorney Enrollment" Language="C#" MasterPageFile="~/account/NewAccountMaster.Master" AutoEventWireup="true" CodeBehind="NewAccount_Page_1.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_1" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="NewAccountBody" runat="server">
    <p>Dear Attorney:</p>

    <p>We're pleased you've chosen to enroll in the Clearpoint Express Service program for legal professionals. Our agency is approved by the EOUST to provide bankruptcy credit counseling and debtor education for bankruptcy filers nationwide. We're headquartered in Atlanta, Georgia with 50 offices in 15 states from California to New York.</p>

    <p>We are committed to providing you and your clients with the most efficient way to comply with the counseling and education requirements of the Bankruptcy Abuse Prevention and Consumer Protection Act of 2005 (BAPCPA). We strive to provide your clients with the compassionate, confidential and experienced counseling that we have been known for since 1964. With a solid history of serving consumers, we are a trusted source for both credit counseling and personal financial management education.</p>

    <p>Your free enrollment in Clearpoint Express Service includes:</p>

    <ul>
    <li>A dedicated, secure web portal for legal professionals to monitor client progress and billing.</li>
    <li>Confidential counseling and education available nationwide in English and Spanish online with 24/7 support from our certified chat counselors Fully interactive Internet sessions with no call-backs required</li>
    <lI>Immediate delivery of counseling certificates</lI>
    <li>Convenient payment options including monthly escrow billing or direct payment by clients to Clearpoint</li>
    </ul>

    &nbsp;<br/>

    <p>Your clients may apply for a fee waiver from Clearpoint if they meet one or more of these three criteria:</p>

    <ol>
    <li>their household income is equal to or less than 150% of the poverty level (based on HHS guidelines)</li>
    <li>their cases are handled pro bono</li>
    </ol>

    &nbsp;<br/>

    <p>To be considered for the waiver, your client must apply with supporting documentation before beginning a session. Should a waiver be approved, you are required to refund any escrowed monies collected by the client for bankruptcy counseling and/or education services. Attorneys registered with Clearpoint Express Service program for Legal Professionals can track client progress by logging into <a href="www.CredAbility.org/en/express.aspx">www.CredAbility.org/en/express.aspx</a> with their provided login credentials.</p>

    <p>There is no fee to join the Clearpoint Express Service program. Simply complete and submit the form below. If you would prefer to fax your registration, please print these pages and fax to 800.447.0350.</p>

    <p>We look forward to being of service to your firm and to your clients. I will be happy to answer any questions you may have about registration or our services via my direct line at 877-877-1995 or by email at <a href="mailto:Bethany.Condoll@clearpoint.org">Bethany.Condoll@clearpoint.org</a>.</p>

    <p>There is no fee to join the Clearpoint Express Service program for Legal Professional. To join, please complete the next few pages and submit your firm information. If you would prefer to fax the information, simply print these pages out and fax them to my attention at 1.800.447.0350. If you have questions, please contact me directly at 877-877-1995 or e-mail me at <a href="mailto:Bethany.Condoll@clearpoint.org">Bethany.Condoll@clearpoint.org</a>.</p>

    <p>All of us at Clearpoint look forward to being of service to your firm and to your clients.</p>

    <p class="big">Best Regards,</p>

    <p>
        Bethany Condoll<br />
        Regional Counseling Manager
    </p>

    <div class="no-print" style="text-align:center">
        <asp:Button ID="Button1" CommandName="Submit" runat="server" Text="Continue to Letter of Understanding" UseSubmitBehavior="true" BackColor="Teal" ForeColor="White" Font-Bold="true" />
    </div>
</asp:Content>
