﻿<%@ Page Title="Summary" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_10.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_10" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>
<%@ Register Src="~/controls/WebUserControl_page3.ascx" TagName="page3" TagPrefix="pg3" %>
<%@ Register Src="~/controls/WebUserControl_page4.ascx" TagName="page4" TagPrefix="pg4" %>
<%@ Register Src="~/controls/WebUserControl_page5.ascx" TagName="page5" TagPrefix="pg5" %>
<%@ Register Src="~/controls/WebUserControl_page6.ascx" TagName="page6" TagPrefix="pg6" %>
<%@ Register Src="~/controls/WebUserControl_page7.ascx" TagName="page7" TagPrefix="pg7" %>
<%@ Register Src="~/controls/WebUserControl_page8.ascx" TagName="page8" TagPrefix="pg8" %>
<%@ Register Src="~/controls/WebUserControl_page9.ascx" TagName="page9" TagPrefix="pg9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="NewAccountHead" runat="server">
    <style type="text/css">
    h2
    {
        margin-top:30px;
        margin-bottom:20px;
        font-weight:bold;
        font-size:larger;
        color:Black;
        background-color:White;
    }
    
    p
    {
        margin-bottom:20px;
    }    
    
    ul
    {
        margin-left:30px;
    }
        
    table.escrow
    {
        padding: 0;
        width: 100%; 
        margin-left: 5%; 
        margin-right: 5%;
    }
    
    span.selectedItem
    {
        text-transform:uppercase;
    }
    
    .style5
    {
        margin: 10 10 10 10;
        border-width:1;
        border-color:Black;
        border-style:solid;
        border-collapse:collapse;
    }
    
    td.style1
    {
        width:50%;
        font-weight:bold;
        text-align:left;
        vertical-align:top;
        border-color:Black;
        border-width:thin;
        border-style:solid;
    }
    
    td.style2
    {
        width:50%;
        text-align:left;
        vertical-align:top;
        border-color:Black;
        border-width:thin;
        border-style:solid;
    }
    
    tr
    {
        border-color:Black;
        border-width:thin;
        border-style:solid;
        vertical-align:top;
        text-align:left;
    }
    
</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">

    <asp:Panel ID="InputPanel" runat="server" Visible="true">
        <div>
            &nbsp;
        </div>

        <table cellpadding="3" cellspacing="3">
            <tbody>
                <tr>
                    <td>
                        <pg3:page3 ID="page3" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg4:page4 ID="page4" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg5:page5 ID="page5" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg6:page6 ID="page6" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg7:page7 ID="page7" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg8:page8 ID="page8" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <pg9:page9 ID="page9" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center; height:200px; vertical-align:bottom;">
                        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>

    <asp:Panel ID="completionPanel" runat="server" Visible="false">
        <h1>Process Completed</h1>
        <p>
            We have created a record for your firm in our database. The Firm Identification Code of the record is <span style="font-weight:bold; font-size:x-large"><% =vendorRecord.Label %></span>. Please remember to give this
            to each of your clients when you refer them to us. It is the key under which you know you so that we may give you the credit for the
            client. This ID is not the value that you use to logon to our site. You entered it when you created the account. This is the ID that we have for your
            firm and use it to track the various activity that we do for you.
        </p>
        <p>
            Your account has been created and is usable immediately.
        </p>
        <p>
            <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/Default.aspx" Target="_self">Return to our main site.</asp:HyperLink>
        </p>
    </asp:Panel>
</asp:Content>
