﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_3 : System.Web.UI.Page
    {
        Signup_Page3 params3 = null;

        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link3.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link3.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params3 = Session["Signup_Page3"] as Signup_Page3;
                if (params3 == null)
                {
                    params3 = new Signup_Page3();
                }

                // On the initial load, set the entries according to the parameters
                if (!Page.IsPostBack)
                {
                    username.Text  = params3.userName;
                    password.Text  = params3.Password;
                    password2.Text = params3.Password;
                    return;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Update the user name
            var sb = new System.Text.StringBuilder();
            try
            {
                params3.userName = username.Text;
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
            }

            // Update the password
            try
            {
                params3.Password = password.Text;
            }
            catch (Exception ex)
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
            }

            // If there is no error then validate the second password.
            if (sb.Length == 0)
            {
                var pwd2 = password2.Text.Trim();
                if (string.IsNullOrWhiteSpace(pwd2))
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("The second password must be entered"));
                }
                else
                {
                    if (string.Compare(pwd2, params3.Password, true) != 0)
                    {
                        sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("The two passwords must match"));
                    }
                }
            }

            // Validate the entries as a while record
            try { params3.Validate(); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<p style=\"color:red; font-weight:bold\">Problem list with the input fields:</p><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference
            Session["Signup_Page3"] = params3;
            Response.Redirect("NewAccount_Page_10.aspx", true);
        }
    }
}