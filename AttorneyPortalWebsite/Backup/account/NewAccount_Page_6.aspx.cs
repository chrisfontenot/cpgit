﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_6 : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link6.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link6.Controls.Add(new LiteralControl("</span>"));
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check the box if it was previously checked
            var params6 = Session["Signup_Page6"] as Signup_Page6;
            if (params6 == null)
            {
                params6 = new Signup_Page6();
            }

            // On the initial load, set the entries according to the parameters
            if (! Page.IsPostBack)
            {
                // Set the pre-Filing status
                switch (params6.Service_PreFiling.GetValueOrDefault(-1))
                {
                    case 0:
                        pre_no.Checked = true;
                        break;

                    case 1:
                        pre_yes.Checked = true;
                        break;

                    case 2:
                        pre_free.Checked = true;
                        break;

                    default:
                        break;
                }

                // Set the pre-discharge status
                switch (params6.Service_PreDischarge.GetValueOrDefault(-1))
                {
                    case 0:
                        post_no.Checked = true;
                        break;

                    case 1:
                        post_yes.Checked = true;
                        break;

                    case 2:
                        post_free.Checked = true;
                        break;

                    default:
                        break;
                }
                return;
            }

            // Decode the flag values
            var sb = new System.Text.StringBuilder();

            // Look at the preFiling bankruptcy option
            if (pre_free.Checked)
            {
                params6.Service_PreFiling = 2;
            }
            else if (pre_yes.Checked)
            {
                params6.Service_PreFiling = 1;
            }
            else if (pre_no.Checked)
            {
                params6.Service_PreFiling = 0;
            }
            else
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("Pre Filing escrow option must be specified"));
            }

            // Look at the preDischarge bankruptcy option
            if (post_free.Checked)
            {
                params6.Service_PreDischarge = 2;
            }
            else if (post_yes.Checked)
            {
                params6.Service_PreDischarge = 1;
            }
            else if (post_no.Checked)
            {
                params6.Service_PreDischarge = 0;
            }
            else
            {
                sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("Pre Discharge escrow option must be specified"));
            }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference
            Session["Signup_Page6"] = params6;

            // Transfer to the next page in the list
            Response.Redirect("NewAccount_Page_10.aspx", true);
        }
    }
}