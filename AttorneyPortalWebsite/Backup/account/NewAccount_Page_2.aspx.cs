﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_2 : System.Web.UI.Page
    {
        // Pointer to the current structure for the form controls
        Signup_Page2 params2 = null;

        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link2.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link2.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params2 = Session["Signup_Page2"] as Signup_Page2;
                if (params2 == null)
                {
                    params2 = new Signup_Page2();
                    params2.Required = true;
                }

                // Set the checked status correctly.
                if (!IsPostBack)
                {
                    chkAgree.Checked = params2.Agreed;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Update the form values for later reference
                params2.Agreed = chkAgree.Checked;
                params2.Validate();

                // All is well with the data.
                Session["Signup_Page2"] = params2;
                Response.Redirect("NewAccount_Page_10.aspx", true);
            }
            catch (Exception ex)
            {
                var sb = new System.Text.StringBuilder();

                sb.Append("<div style=\"color:red; font-weight:bold\">");
                sb.Append(Server.HtmlEncode(ex.Message));
                sb.Append("</div>");

                ValidationSummary.Text = sb.ToString();
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}