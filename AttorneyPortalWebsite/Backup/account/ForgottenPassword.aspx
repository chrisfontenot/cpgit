﻿<%@ Page Title="Forgotten Password Recovery" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ForgottenPassword.aspx.cs" Inherits="AttorneyPortal.account.ForgottenPassword" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SiteBody" runat="server">

        <h1>Password Recovery</h1>

        <asp:PasswordRecovery
              SuccessText="Your password was successfully reset and emailed to you. Please check your email and change it as soon as possible."
              QuestionFailureText="Incorrect answer. Please try again." 
              runat="server" ID="PWRecovery" 
              UserNameFailureText="Account ID not found.">

            <MailDefinition IsBodyHtml="true" BodyFileName="~/Templates/ForgottenPassword/PasswordRecoveryMessage.txt"
                   From="DoNotReply@ClearpointCCS.org" 
                   Subject="Password Reset" 
                   Priority="High">
            </MailDefinition>

            <UserNameTemplate>
                <p>
                    The steps below will allow you to have a new password sent to the registered email address.
                </p>

                <p>
                    We request the email address for your account. You may use either email address that was used when the account
                    was created. But, it must match at least one of the addresses to be acceptable. The new password will be sent
                    to the address that you entered below.
                </p>

                <span style="color:Red; font-weight:bold">
                    <asp:Literal ID="ErrorLiteral" runat="server"></asp:Literal>
                </span>

                <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="PWRecovery"/>

                <div class="accountInfo">
                    <fieldset class="recoverPassword">
                        <legend>Account Information</legend>

                        <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="100%" CssClass="fixed_table">

                            <asp:TableRow ID="TableRow1" runat="server" VerticalAlign="Top" Width="100%">
                                <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left" Font-Bold="true" Width="20%">
                                    <asp:Label ID="Label1" runat="server" AssociatedControlID="UserName">Account ID:</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell2" runat="server" HorizontalAlign="Left">
                                    <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" /> <!-- TextMode="Number" -->
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName" 
                                            CssClass="failureNotification" ErrorMessage="The Account ID is required." ToolTip="The Account ID is required." 
                                            ValidationGroup="PWRecovery" Display="None" />
                                </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow2" runat="server" VerticalAlign="Top" Width="100%">
                                <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left" Font-Bold="true" Width="20%">
                                    <asp:Label ID="Label2" runat="server" AssociatedControlID="EmailAddressTB">Email Address:</asp:Label>
                                </asp:TableCell><asp:TableCell ID="TableCell4" runat="server" HorizontalAlign="Left">
                                    <asp:TextBox ID="EmailAddressTB" runat="server" CssClass="textEntry" TextMode="SingleLine" /> <!--  TextMode="Email" -->

                                    <asp:RequiredFieldValidator ID="EmailAddressTBRequired" runat="server" ControlToValidate="EmailAddressTB" 
                                            CssClass="failureNotification" ErrorMessage="Email Address is required." ToolTip="Email Address is required." 
                                            ValidationGroup="PWRecovery" Display="None" />

                                    <asp:RegularExpressionValidator ID="EmailRegularExpressionValidator" runat="server" ControlToValidate="EmailAddressTB"
                                            CssClass="failureNotification" ErrorMessage="The Email address does not seem to be valid" ToolTip="The Email address does not seem to be valid"
                                            ValidationExpression="^[A-Za-z0-9._%+-]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$"
                                            Display="None" ValidationGroup="PWRecovery" />
                                </asp:TableCell></asp:TableRow></asp:Table></fieldset> <p style="text-align:center">
                        <asp:ImageButton ID="Button1" CausesValidation="true" ValidationGroup="PWRecovery" runat="server" CommandName="Submit" ImageUrl="~/images/btn_OC_continue.gif" BorderStyle="None" />
                    </p>
                </div>
            </UserNameTemplate>
        </asp:PasswordRecovery>
</asp:Content>
