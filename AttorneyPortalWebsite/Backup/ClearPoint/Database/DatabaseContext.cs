﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        public partial class DatabaseContext : System.IDisposable
        {
            private System.IO.TextWriter txt = null;
            private System.IO.FileStream fs = null;
            public DataContext context { get; set; }
            public DatabaseContext() : this(System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString)
            {
            }

            public DatabaseContext(System.Data.Common.DbTransaction Transaction) : this(Transaction.Connection)
            {
                context.Transaction = Transaction;
            }

            public DatabaseContext(System.Data.IDbConnection Connection)
            {
                context = new DataContext(Connection);

                // Log database access if desired
                string logFile = System.Configuration.ConfigurationManager.AppSettings["DatabaseLog"];
                if (!string.IsNullOrWhiteSpace(logFile))
                {
                    fs = System.IO.File.Open(logFile, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite);
                    fs.Seek(0L, System.IO.SeekOrigin.End);
                    txt = new System.IO.StreamWriter(fs);
                    context.Log = txt;
                }
            }

            public DatabaseContext(string ConnectionString)
            {
                context = new DataContext(ConnectionString);

                // Log database access if desired
                string logFile = System.Configuration.ConfigurationManager.AppSettings["DatabaseLog"];
                if (!string.IsNullOrWhiteSpace(logFile))
                {
                    fs = System.IO.File.Open(logFile, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite);
                    fs.Seek(0L, System.IO.SeekOrigin.End);
                    txt = new System.IO.StreamWriter(fs);
                    context.Log = txt;
                }
            }

            public void SubmitChanges(ConflictMode failureMode)
            {
                try
                {
                    context.SubmitChanges(failureMode);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    LogError(ex);
                    throw;
                }
            }

            public void SubmitChanges()
            {
                try
                {
                    context.SubmitChanges();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    LogError(ex);
                    throw;
                }
            }

            /// <summary>
            /// Log errors on the database
            /// </summary>
            /// <param name="ex">Exception to be logged</param>
            private void LogError(System.Exception ex)
            {
                // Log the exception condition as well.
                if (txt != null)
                {
                    txt.WriteLine("Exception: {0}\r\n{1}\r\n------\r\n", ex.Message, ex.StackTrace);
                }
            }

            public System.Data.Linq.ChangeSet GetChangeSet()
            {
                return context.GetChangeSet();
            }

            public void Dispose()
            {
                // Flush and close the log file when we are being destroyed.
                if (txt != null)
                {
                    txt.Flush();
                    txt.Close();
                    txt.Dispose();
                    txt = null;
                }

                // Close the output filestream as well
                if (fs != null)
                {
                    fs.Dispose();
                    fs = null;
                }

                context.Dispose();
                GC.SuppressFinalize(this);
            }
        }
    }
}