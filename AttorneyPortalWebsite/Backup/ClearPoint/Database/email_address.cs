﻿using System;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.EmailAddresses")]
        public partial class EmailAddress
        {
            public static readonly int ValidationCode_Empty = 0;     // Validated email address type
            public static readonly int ValidationCode_Valid = 1;     // Validated email address type

            [Column(Name = "email", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string Address { get; set; }
            [Column] public int Validation { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public EmailAddress()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Validation = ValidationCode_Empty;
            }

            public override string ToString()
            {
                return Address ?? string.Empty;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<EmailAddress> EmailAddressTable
            {
                get
                {
                    return context.GetTable<EmailAddress>();
                }
            }
        }
    }
}