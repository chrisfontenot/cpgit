﻿using System;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.TelephoneNumbers")]
        public partial class TelephoneNumber
        {
            [Column(Name = "TelephoneNumber", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column(CanBeNull = true)] public System.Nullable<int> Country { get; set; }
            [Column] public string Acode { get; set; }
            [Column] public string Number { get; set; }
            [Column] public string Ext { get; set; }
            
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public TelephoneNumber()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Country = 1;        // "1" = USA
            }

            /// <summary>
            /// Format the string for display in the website. This is not the normal DebtPlus formatting
            /// routine so don't mix the two. This is for the website only.
            /// </summary>
            public override string ToString()
            {
                string newNumber = Number ?? string.Empty;
                if (newNumber.Length == 7)
                {
                    newNumber = newNumber.Substring(0, 3) + "-" + newNumber.Substring(3);
                }

                if (!string.IsNullOrWhiteSpace(Acode))
                {
                    newNumber = Acode + '-' + newNumber;
                }

                return newNumber;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<TelephoneNumber> TelephoneNumberTable
            {
                get
                {
                    return context.GetTable<TelephoneNumber>();
                }
            }
        }
    }
}