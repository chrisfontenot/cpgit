﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [Table(Name="dbo.client_www")]
        public class client_www
        {
            [Column(Name = "PKID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
            public System.Guid Id { get; set; }

            [Column] public string ApplicationName { get; set; }
            [Column] public string UserName { get; set; }
            [Column] public string Password { get; set; }
            [Column] public string Email { get; set; }
            [Column] public string PasswordQuestion { get; set; }
            [Column] public string PasswordAnswer { get; set; }
            [Column] public string Comment { get; set; }
            [Column] public bool IsLockedOut { get; set; }
            [Column] public bool IsApproved { get; set; }
            [Column] public System.DateTime LastPasswordChangeDate { get; set; }
            [Column] public System.DateTime LastLoginDate { get; set; }
            [Column] public System.DateTime LastActivityDate { get; set; }
            [Column] public System.Nullable<System.DateTime> LastLockoutDate { get; set; }
            [Column] public int FailedPasswordAnswerAttemptCount { get; set; }
            [Column] public System.DateTime FailedPasswordAnswerAttemptWindowStart { get; set; }
            [Column] public int FailedPasswordAttemptCount { get; set; }
            [Column] public System.DateTime FailedPasswordAttemptWindowStart { get; set; }
            [Column] public System.Nullable<System.Int32> DatabaseKeyID { get; set; }
            [Column(IsDbGenerated = true, AutoSync = AutoSync.OnInsert)] public System.DateTime CreationDate { get; set; }
            [Column(IsDbGenerated = true, AutoSync=AutoSync.OnInsert)] public string created_by { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public client_www()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                System.DateTime timeNow                = System.DateTime.UtcNow;
                LastPasswordChangeDate                 = timeNow;
                LastLoginDate                          = timeNow;
                LastActivityDate                       = timeNow;
                FailedPasswordAnswerAttemptWindowStart = timeNow;
                FailedPasswordAttemptWindowStart       = timeNow;
                LastLockoutDate                        = timeNow;
                CreationDate                           = timeNow;

                PasswordQuestion                       = default(string);
                PasswordAnswer                         = default(string);
                Comment                                = System.String.Empty;
                IsLockedOut                            = false;
                IsApproved                             = false;
                FailedPasswordAnswerAttemptCount       = 0;
                FailedPasswordAttemptCount             = 0;
                created_by                             = default(string);
                DatabaseKeyID                          = null;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<client_www> client_wwwTable
            {
                get
                {
                    return context.GetTable<client_www>();
                }
            }
        }
    }
}