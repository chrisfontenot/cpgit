﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.vendors")]
        public partial class vendor
        {
            [Column(Name = "oID", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string Type { get; set; }
            [Column(DbType = "varchar(20)", IsDbGenerated=true, CanBeNull=true, UpdateCheck=UpdateCheck.Never, AutoSync=AutoSync.OnInsert)] public string Label { get; set; }
            [Column] public string Name { get; set; }
            [Column] public bool ActiveFlag { get; set; }
            [Column] public string BillingMode { get; set; }
            [Column] public char ACH_CheckingSavings { get; set; }
            [Column] public string ACH_RoutingNumber { get; set; }
            [Column] public string ACH_AccountNumber { get; set; }
            [Column] public System.Nullable<System.DateTime> CC_ExpirationDate { get; set; }
            [Column] public string CC_CVV { get; set; }
            [Column] public bool SupressPayments { get; set; }
            [Column] public bool SupressInvoices { get; set; }
            [Column] public string NameOnCard { get; set; }
            [Column] public bool SupressPrintingPayments { get; set; }
            [Column] public System.Nullable<int> MonthlyBillingDay { get; set; }
            [Column] public System.String Comment { get; set; }
            [Column] public System.Decimal st_BilledMTDAmt { get; set; }
            [Column] public System.Decimal st_BilledYTDAmt { get; set; }
            [Column] public System.Decimal st_ReceivedMTDAmt { get; set; }
            [Column] public System.Decimal st_ReceivedYTDAmt { get; set; }
            [Column] public System.Nullable<System.DateTime> st_ReturnedMailDate { get; set; }
            [Column] public System.Nullable<System.DateTime> st_FirstBillingDate { get; set; }
            [Column] public int st_ProductsOfferedCount { get; set; }
            [Column] public int st_ClientCountTotalCount { get; set; }
            [Column] public int st_ProductsSoldCnt { get; set; }
            [Column] public System.Decimal st_PaymentsReceivedAmt { get; set; }
            [Column] public int st_DisputeCnt { get; set; }
            [Column] public System.Decimal st_BillingAdjustmentsAmt { get; set; }
            [Column] public string Website { get; set; }
            [Column(DbType="Xml", CanBeNull=true)] public System.Xml.Linq.XElement Additional { get; set; }
            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public vendor()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                Comment                  = string.Empty;
                st_BilledMTDAmt          = 0;
                st_BilledYTDAmt          = 0;
                st_BillingAdjustmentsAmt = 0;
                st_ClientCountTotalCount = 0;
                st_DisputeCnt            = 0;
                st_PaymentsReceivedAmt   = 0;
                st_ProductsOfferedCount  = 0;
                st_ProductsSoldCnt       = 0;
                st_ReceivedMTDAmt        = 0;
                st_ReceivedYTDAmt        = 0;
                st_FirstBillingDate      = null;
                st_ReturnedMailDate      = null;
                Website                  = null;
                st_BilledMTDAmt          = 0;
                st_BilledYTDAmt          = 0;
                st_ReceivedMTDAmt        = 0;
                st_ReceivedYTDAmt        = 0;
                st_ReturnedMailDate      = null;
                st_FirstBillingDate      = null;
                st_ProductsOfferedCount  = 0;
                st_ClientCountTotalCount = 0;
                st_ProductsSoldCnt       = 0;
                st_PaymentsReceivedAmt   = 0;
                st_DisputeCnt            = 0;
                st_BillingAdjustmentsAmt = 0;
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<vendor> vendorTable
            {
                get
                {
                    return context.GetTable<vendor>();
                }
            }
        }
    }
}