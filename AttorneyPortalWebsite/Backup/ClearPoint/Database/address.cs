﻿using System;
using System.Data.Linq.Mapping;
using System.Linq;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.addresses")]
        public partial class address
        {
            [Column(Name = "address", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public string creditor_prefix_1 { get; set; }
            [Column] public string creditor_prefix_2 { get; set; }
            [Column] public string house { get; set; }
            [Column] public string direction { get; set; }
            [Column] public string street { get; set; }
            [Column] public string suffix { get; set; }
            [Column] public string modifier { get; set; }
            [Column] public string modifier_value { get; set; }
            [Column] public string address_line_2 { get; set; }
            [Column] public string address_line_3 { get; set; }
            [Column] public string city { get; set; }
            [Column] public int state { get; set; }
            [Column] public string PostalCode { get; set; }

            [Column(AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)] public System.Data.Linq.Binary ts { get; set; }

            public address()
            {
                OnCreated();
            }

            private void OnCreated()
            {
                state = 0;
            }

            /// <summary>
            /// Formatted first line of the address block. This is a small variation of the real
            /// processing routine in the DebtPlus data context. It is meant for the website only.
            /// </summary>
            public string AddressLine1
            {
                get
                {
                    var sb = new System.Text.StringBuilder();
                    foreach (var str in new string[] { house, direction, street, suffix })
                    {
                        if (!string.IsNullOrWhiteSpace(str))
                        {
                            sb.AppendFormat(" {0}", str);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(modifier_value) || (modifier != null && modifier != "APT" && modifier != "STE"))
                    {
                        sb.AppendFormat(" {0}", modifier ?? string.Empty);
                        sb.AppendFormat(" {0}", modifier_value ?? string.Empty);
                    }

                    return sb.ToString().Trim();
                }
            }

            /// <summary>
            /// Return the last line of the address which has the city, state, and postal code.
            /// This is a simplified version of the DebtPlus module and only handles US addresses.
            /// </summary>
            public string CityStateZip
            {
                get
                {
                    string stateName = string.Empty;

                    // Map the state ID to a mailing code.
                    using (var db = new ClearPoint.DebtPlus.DatabaseContext())
                    {
                        var q = db.stateTable.Where(s => s.Id == state).FirstOrDefault();
                        if (q != null)
                        {
                            stateName = q.MailingCode;
                        }
                    }

                    // Return the information
                    return string.Format("{0} {1}  {2}", city, stateName, PostalCode);
                }
            }

            /// <summary>
            /// Convert the address to a string
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                var sb = new System.Text.StringBuilder();
                foreach (var str in new string[] { AddressLine1, address_line_2, address_line_3, CityStateZip })
                {
                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        sb.AppendFormat("\r\n{0}", str);
                    }
                }

                // Toss the leading cr/lf sequences
                if (sb.Length > 0)
                {
                    sb.Remove(0, 2);
                }

                // Return the string block.
                return sb.ToString();
            }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<address> addressTable
            {
                get
                {
                    return context.GetTable<address>();
                }
            }
        }
    }
}