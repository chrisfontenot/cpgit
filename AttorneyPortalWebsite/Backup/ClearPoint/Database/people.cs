﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace ClearPoint
{
    public partial class DebtPlus
    {
        [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.people")]
        public class people
        {
            [Column(Name = "person", DbType = "Int NOT NULL", IsDbGenerated = true, IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never, AutoSync = AutoSync.OnInsert, CanBeNull = false)]
            public Int32 Id { get; set; }

            [Column] public int Client { get; set; }
            [Column] public int Relation { get; set; }
            [Column] public System.Nullable<int> NameID { get; set; }
            [Column] public System.Nullable<int> WorkTelephoneID { get; set; }
            [Column] public System.Nullable<int> CellTelephoneID { get; set; }
            [Column] public System.Nullable<int> EmailID { get; set; }
            [Column] public string Former { get; set; }
            [Column] public string SSN { get; set; }
            [Column] public int Gender { get; set; }
            [Column] public int Race { get; set; }
            [Column] public int Ethnicity { get; set; }
            [Column] public bool Disabled { get; set; }
            [Column] public int MilitaryServiceID { get; set; }
            [Column] public int MilitaryStatusID { get; set; }
            [Column] public int MilitaryGradeID { get; set; }
            [Column] public int MilitaryDependentID { get; set; }
            [Column] public int Education { get; set; }
            [Column] public System.Nullable<System.DateTime> Birthdate { get; set; }
            [Column] public System.Nullable<int> FICO_Score { get; set; }
            [Column] public string CreditAgency { get; set; }
            [Column] public System.Nullable<int> no_fico_score_reason { get; set; }
            [Column] public System.Nullable<System.DateTime> FICO_pull_date { get; set; }
            [Column] public decimal gross_income { get; set; }
            [Column] public decimal net_income { get; set; }
            [Column] public int Frequency { get; set; }
            [Column] public System.Nullable<System.DateTime> emp_start_date { get; set; }
            [Column] public System.Nullable<System.DateTime> emp_end_date { get; set; }
            [Column] public System.Nullable<int> employer { get; set; }
            [Column] public System.Nullable<int> job { get; set; }
            [Column] public string other_job { get; set; }
            [Column] public System.Nullable<System.DateTime> bkfileddate { get; set; }
            [Column] public System.Nullable<System.DateTime> bkdischarge { get; set; }
            [Column] public System.Nullable<int> bkchapter { get; set; }
            [Column] public System.Nullable<System.DateTime> bkAuthLetterDate { get; set; }
            [Column] public string bkCertificateNo { get; set; }
            [Column] public System.Nullable<System.DateTime> bkCertificateIssued { get; set; }
            [Column] public System.Nullable<System.DateTime> bkCertificateExpires { get; set; }
            [Column] public System.Nullable<int> bkDistrict { get; set; }
            [Column] public string created_by { get; set; }
            [Column] public System.DateTime date_created { get; set; }
            [Column] public System.Data.Linq.Binary ts { get; set; }
        }

        public partial class DatabaseContext
        {
            public System.Data.Linq.Table<people> PeopleTable
            {
                get
                {
                    return context.GetTable<people>();
                }
            }
        }
    }
}