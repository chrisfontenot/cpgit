﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page7.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page7" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">BILLING CONTACT</p>

<asp:Table ID="Table1" runat="server">
    <asp:TableRow ID="TableRow01" runat="server">
        <asp:TableCell ID="TableCell01" runat="server" Text="Name:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="ContactName" runat="server"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow02" runat="server">
        <asp:TableCell ID="TableCell02" runat="server" Text="Telephone Number:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="TelephoneNumber" runat="server"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow03" runat="server">
        <asp:TableCell ID="TableCell03" runat="server" Text="FAX Number:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="FaxNumber" runat="server"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow04" runat="server">
        <asp:TableCell ID="TableCell04" runat="server" Text="E-Mail address:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="Email" runat="server"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="TableRow05" runat="server" VerticalAlign="Top">
        <asp:TableCell ID="TableCell05" runat="server" Text="Mailing address:" VerticalAlign="Top" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="Address" runat="server" VerticalAlign="Top"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
