﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page8.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page8" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">PAYMENT INFORMATION</p>

<asp:Table runat="server" Width="100%">
    <asp:TableRow ID="TableRow1" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="TableCell1" runat="server" Text="Payment Method" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell runat="server" ID="PaymentMethod" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell runat="server" Text="Billing Address" Width="150px"></asp:TableCell>
        <asp:TableCell runat="server" ID="BillingAddress" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="ROW_1" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="CC_ACCOUNT_NAME" runat="server" Text="Name On Account" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="AccountName" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="ROW_2" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="CC_LABEL_ACCOUNT_NUMBER" runat="server" Text="Account Number" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="AccountNumber" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="CC_ROW_1" runat="server">
        <asp:TableCell ID="CC_LABEL_1" runat="server" Text="Expiration Date:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="ExpirationDate" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="CC_ROW_2" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="CC_LABEL_2" runat="server" Text="Verification Code:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="CVV" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="ACH_ROW_1" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="ACH_LABEL_1" runat="server" Text="Routing Number:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="RoutingNumber" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="ACH_ROW_2" runat="server" VerticalAlign="Top" HorizontalAlign="Left">
        <asp:TableCell ID="ACH_CHECKING_SAVINGS" runat="server" Text="Account Type:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell ID="CheckingSavings" runat="server" VerticalAlign="Top" HorizontalAlign="Left"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
