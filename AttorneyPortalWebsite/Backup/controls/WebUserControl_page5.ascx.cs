﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page5 : System.Web.UI.UserControl
    {
        // Information for the page 5 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page5 params5;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params5 = Session["Signup_Page5"] as AttorneyPortal.account.Signup_Page5;
                if (params5 != null)
                {
                    // Copy the address if the value is the same as the company.
                    if (params5.Principal.SameAddress)
                    {
                        var params4 = Session["Signup_Page4"] as AttorneyPortal.account.Signup_Page4;
                        if (params4 != null)
                        {
                            params5.Principal.Address.Copy(params4.Address);
                        }
                    }

                    // Validate the page
                    if (params5.Required)
                    {
                        params5.Validate();
                    }

                    // Display the data
                    attorney_name.Text            = Server.HtmlEncode(params5.Principal.Name.ToString());
                    attorney_address.Text         = Server.HtmlEncode(params5.Principal.Address.ToString()).Replace("\r\n", "<br/>");
                    attorney_email.Text           = Server.HtmlEncode(params5.Principal.Email.ToString());
                    attorney_FaxNumber.Text       = Server.HtmlEncode(params5.Principal.FaxNumber.ToString());
                    attorney_TelephoneNumber.Text = Server.HtmlEncode(params5.Principal.TelephoneNumber.ToString(true));

                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_5.aspx");
        }
    }
}