﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page6.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page6" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">SERVICES OFFERED</p>

<asp:Table ID="Table1" runat="server">
    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableHeaderCell ID="TableCell1" runat="server" style="width:150px; vertical-align:top; text-align:left;">SERVICE</asp:TableHeaderCell>
        <asp:TableHeaderCell ID="TableCell3" runat="server" Text="&nbsp;" Width="30px"></asp:TableHeaderCell>
        <asp:TableHeaderCell ID="TableCell6" runat="server">STATUS</asp:TableHeaderCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" style="width:150px; vertical-align:top; text-align:left;">Pre-Filing Bankruptcy</asp:TableCell>
        <asp:TableCell runat="server" Text="&nbsp;" Width="30px"></asp:TableCell>
        <asp:TableCell runat="server" ID="pre_filing"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow3" runat="server">
        <asp:TableCell ID="TableCell7" runat="server" style="width:150px; vertical-align:top; text-align:left;">Pre-Discharge Bankruptcy</asp:TableCell>
        <asp:TableCell ID="TableCell4" runat="server" Text="&nbsp;" Width="30px"></asp:TableCell>
        <asp:TableCell runat="server" ID="pre_discharge"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
