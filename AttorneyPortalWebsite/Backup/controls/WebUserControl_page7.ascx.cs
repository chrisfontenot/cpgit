﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page7 : System.Web.UI.UserControl
    {
        // Information for the page 6 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page7 params7;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params7 = Session["Signup_Page7"] as AttorneyPortal.account.Signup_Page7;
                if (params7 != null)
                {
                    // Copy the address if the value is the same as the company.
                    if (params7.Billing.SameAddress)
                    {
                        var params4 = Session["Signup_Page4"] as AttorneyPortal.account.Signup_Page4;
                        if (params4 != null)
                        {
                            params7.Billing.Address.Copy(params4.Address);
                        }
                    }
                        
                    // Validate the page
                    if (params7.Required)
                    {
                        params7.Validate();
                    }

                    // Load the display information
                    ContactName.Text     = params7.Billing.Name.ToString();
                    TelephoneNumber.Text = params7.Billing.TelephoneNumber.ToString(true);
                    FaxNumber.Text       = params7.Billing.FaxNumber.ToString();
                    Email.Text           = params7.Billing.Email.ToString();
                    Address.Text         = params7.Billing.Address.ToString().Replace("\r\n", "<br/>");

                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_7.aspx");
        }
    }
}