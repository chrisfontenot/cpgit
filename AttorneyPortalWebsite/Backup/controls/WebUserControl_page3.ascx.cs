﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page3 : System.Web.UI.UserControl
    {
        // Information for the page 3 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page3 params3;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params3 = Session["Signup_Page3"] as AttorneyPortal.account.Signup_Page3;
                if (params3 != null)
                {
                    // Validate the page
                    if (params3.Required)
                    {
                        params3.Validate();
                    }

                    // Display the information from the page
                    this.username.Text = params3.userName;
                    this.password.Text = params3.Password;
                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_3.aspx");
        }
    }
}