﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page9.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page9" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">ADDITIONAL INFORMATION</p>

<asp:Table runat="server">
    <asp:TableRow runat="server">
        <asp:TableCell runat="server" Text="Attorneys using service:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell runat="server" ID="attorneys"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell ID="TableCell1" runat="server" Text="Estimated Clients:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell runat="server" ID="clients"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" Text="Referral Source:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell runat="server" ID="referral"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow3" runat="server">
        <asp:TableCell ID="TableCell3" runat="server" Text="Comments:" style="width:150px; vertical-align:top; text-align:left;"></asp:TableCell>
        <asp:TableCell runat="server" ID="comments"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
