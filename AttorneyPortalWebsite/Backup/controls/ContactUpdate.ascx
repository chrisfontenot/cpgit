﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUpdate.ascx.cs" Inherits="AttorneyPortal.controls.ContactUpdate" %>
<%@ Register Src="~/controls/AddressUpdate.ascx" TagName="AddressUpdate" TagPrefix="adr" %>
<%@ Register Src="~/controls/NameUpdate.ascx" TagName="NameUpdate" TagPrefix="nm" %>
<%@ Register Src="~/controls/TelephoneUpdate.ascx" TagName="TelephoneUpdate" TagPrefix="ph" %>
<%@ Register Src="~/controls/EmailUpdate.ascx" TagName="EmailUpdate" TagPrefix="em" %>

<asp:Table ID="ContactTable" runat="server" Width="100%" >
    <asp:TableRow runat="server" style="table-layout:fixed">
        <asp:TableCell runat="server" style="width:100px; text-align:left; vertical-align:top">Name</asp:TableCell>
        <asp:TableCell ID="TableCell1" style="width:300px;" runat="server">
            <nm:NameUpdate runat="server" ID="NameRecord" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow4" runat="server">
        <asp:TableCell ID="TableCell9" runat="server" style="width:100px; text-align:left; vertical-align:top">Address</asp:TableCell>
        <asp:TableCell ID="TableCell8" style="width:300px;" runat="server">
            <adr:AddressUpdate runat="server" ID="AddressRecord" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" style="width:100px; text-align:left; vertical-align:top">Phone</asp:TableCell>
        <asp:TableCell ID="TableCell3" style="width:300px;" runat="server">
            <ph:TelephoneUpdate runat="server" ID="TelephoneRecord" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell4" runat="server" style="width:100px; text-align:left; vertical-align:top">FAX</asp:TableCell>
        <asp:TableCell ID="TableCell5" style="width:300px;" runat="server">
            <ph:TelephoneUpdate runat="server" ID="FAXRecord" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow3" runat="server">
        <asp:TableCell ID="TableCell6" runat="server" style="width:100px; text-align:left; vertical-align:top">E-Mail</asp:TableCell>
        <asp:TableCell ID="TableCell7" style="width:300px;" runat="server">
            <em:EmailUpdate runat="server" ID="EmailRecord" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
