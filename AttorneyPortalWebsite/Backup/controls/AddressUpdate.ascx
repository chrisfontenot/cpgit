﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressUpdate.ascx.cs" Inherits="AttorneyPortal.controls.AddressUpdate" %>

<span style="orphans:0; white-space:nowrap;">
    <asp:TextBox ID="Address_1" runat="server" MaxLength="80" /><br />
    <asp:TextBox ID="Address_2" runat="server" MaxLength="80" /><br />
    <asp:TextBox ID="City" runat="server" MaxLength="80" />&nbsp;<asp:DropDownList ID="State" runat="server"></asp:DropDownList>&nbsp;<asp:TextBox ID="Zip" runat="server" MaxLength="20" />
</span>