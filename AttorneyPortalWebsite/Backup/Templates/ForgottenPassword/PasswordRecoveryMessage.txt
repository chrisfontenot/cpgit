﻿<p>
	Hello <%Name%>,
</p>

<p>
	You are receiving this email on behalf of CLEARPOINT because you have requested a new password.
</p>

<p>
	If you know nothing about us and do not have an account with us then it would mean that your email
	address was given when the account was created and is in error. If this is the case then please call
	us, toll free, at 1-877-877-1995 and ask to speak to customer service. We will remove the email address
	from our system so that you don't receive any future messages from us. We do apologize for sending you
	this email message. If you choose not to call us then just disregard the message. Again we are sorry
	to have sent you the message in error.
</p>

<p>
	If you did not request the password to be reset then your account has been accessed by someone who
	not only knew the account id and your email address but also the answer to the security question. This
	is not very likely, but on the remote possibility that this has happened, you should reset the security
	question and answer immediately as well.
</p>

<p>
	We have assigned a new password to your account. The new password is    <strong><big><big><%Password%></big></big></strong>
</p>

<p>
	We suggest that you login immediately and change your password.
</p>

<p>
	Thank you,<br/>
	Clearpoint Customer Service
</p>

<p>
	<hr />
	<small><small>
		Account ID information withheld for security concerns.
	</small></small>
</p>

