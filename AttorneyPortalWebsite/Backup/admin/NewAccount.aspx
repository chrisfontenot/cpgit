﻿<%@ Page Title="New Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewAccount.aspx.cs" Inherits="AttorneyPortal.admin.NewAccount" %>

<asp:Content ID="Content3" ContentPlaceHolderID="SiteBody" runat="server">
<div style="text-align:center">
    <asp:CreateUserWizard
        AnswerRequiredErrorMessage="Answer Required Error"
        AutoGeneratePassword="False"
        BackColor="#FFFBD6" 
        BorderColor="#FFDFAD"
        BorderStyle="Solid"
        BorderWidth="1px" 
        CompleteSuccessText="Account Created successfully" 
        DuplicateEmailErrorMessage="Duplicate Email Error"
        DuplicateUserNameErrorMessage="The account name is duplicated" 
        Font-Names="Verdana"
        Font-Size="0.8em"
        HelpPageText="Help Page Text"
        ID="CreateUser" 
        InstructionText="This is the set of instructions"
        InvalidAnswerErrorMessage="Invalid Answer Error"
        InvalidEmailErrorMessage="Invalid Email Error"
        InvalidPasswordErrorMessage="Invalid Password Error"
        InvalidQuestionErrorMessage="Invalid Question Error"
        LabelStyle-Font-Bold="true"
        MembershipProvider="DebtPlusMembershipProvider"
        MailDefinition-From="MailDefintionFrom@cccsatl.org"
        QuestionRequiredErrorMessage="Question Required Error"
        runat="server"
        Width="50%">

        <ContinueButtonStyle BackColor="White" BorderColor="#CC9966" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#990000" />

        <CreateUserButtonStyle BackColor="White" BorderColor="#CC9966" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#990000" />

        <LabelStyle Font-Bold="True"></LabelStyle>

        <MailDefinition From="DoNotReply@cccsatl.org"></MailDefinition>

        <TitleTextStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

     <WizardSteps>
        <asp:CreateUserWizardStep runat="server">

            <ContentTemplate>
                <table style="margin-left:auto;margin-right:auto;background-color:#FFFBD6;font-family:Verdana;font-size:100%;">
                    <tr>
                        <td align="center" colspan="3" 
                            style="color:White;background-color:#990000;font-weight:bold;">
                            Create New Attorney Account</td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="UserName" runat="server" Width="203px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                ControlToValidate="UserName" ErrorMessage="User Name is required." 
                                ToolTip="User Name is required." ValidationGroup="CreateUser">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label ID="AttorneyLabel" runat="server" AssociatedControlID="Attorney">Attorney Label:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Attorney" runat="server" Width="203px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="AttorneyRequired" runat="server" 
                                ControlToValidate="Attorney" ErrorMessage="Attorney name is required." 
                                ToolTip="Attorney name is required." ValidationGroup="CreateUser">*</asp:RequiredFieldValidator>

                            <asp:CustomValidator ID="AttorneyInvalidValidator" runat="server" 
                                ControlToValidate="Attorney" ErrorMessage="There is no attorney by this label" 
                                ToolTip="There is no attorney by this label" ValidationGroup="CreateUser">*</asp:CustomValidator>

                            <asp:CustomValidator ID="AttorneyDuplicateValidator" runat="server" 
                                ControlToValidate="Attorney" ErrorMessage="The Attorney already has an account" 
                                ToolTip="The Attorney already has an account" ValidationGroup="CreateUser">*</asp:CustomValidator>
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Password" runat="server" Width="203px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="PasswordRequiredFieldValidator" runat="server" 
                                ControlToValidate="Attorney" ErrorMessage="Password is required." 
                                ToolTip="Password is required." ValidationGroup="CreateUser">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">E-mail:</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Email" runat="server" Width="203px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        <td>
                    </tr>

                    <tr>
                        <td align="center" colspan="3" style="color:Red;">
                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>

            <CustomNavigationTemplate>
                <table border="0" cellspacing="5" style="width:100%;height:100%;">
                    <tr align="right">
                        <td align="right" colspan="0">
                            <asp:Button ID="StepNextButton" runat="server" BackColor="White" 
                                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                                CommandName="MoveNext" Font-Names="Verdana" ForeColor="#990000" 
                                Text="Create User" ValidationGroup="CreateUser" />
                        </td>
                    </tr>
                </table>
            </CustomNavigationTemplate>

        </asp:CreateUserWizardStep>

        <asp:CompleteWizardStep runat="server">
            <ContentTemplate>
                <table style="width:50%; background-color: #FFFBD6; font-family: Verdana; font-size: 100%;">
                    <tr>
                        <td align="center" style="color: White; background-color: #990000; font-weight: bold;">
                            Complete
                        </td>
                    </tr>
                    <tr>
                        <td>
                            The account was created successfully
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="ContinueButton" runat="server" BackColor="White" 
                                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                                CausesValidation="False" CommandName="Continue" Font-Names="Verdana" 
                                ForeColor="#990000" Text="Continue" ValidationGroup="CreateUser" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:CompleteWizardStep>

     </WizardSteps>
        <FinishNavigationTemplate>
            <asp:Button ID="FinishPreviousButton" runat="server" BackColor="White" 
                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                CausesValidation="False" CommandName="MovePrevious" Font-Names="Verdana" 
                ForeColor="#990000" Text="Previous" />

            <asp:Button ID="FinishButton" runat="server" BackColor="White" 
                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                CommandName="MoveComplete" Font-Names="Verdana" ForeColor="#990000" 
                Text="Finish" />

        </FinishNavigationTemplate>

        <HeaderStyle BackColor="#FFCC66" BorderColor="#FFFBD6" BorderStyle="Solid" 
            BorderWidth="2px" Font-Bold="True" Font-Size="0.9em" ForeColor="#333333" 
            HorizontalAlign="Center" />

        <NavigationButtonStyle BackColor="White" BorderColor="#CC9966" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
            ForeColor="#990000" />

        <SideBarButtonStyle ForeColor="White" />
        <SideBarStyle BackColor="#990000" Font-Size="0.9em" VerticalAlign="Top" />

        <StartNavigationTemplate>
            <asp:Button ID="StartNextButton" runat="server" BackColor="White" 
                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                CommandName="MoveNext" Font-Names="Verdana" ForeColor="#990000" Text="Next" />
        </StartNavigationTemplate>

        <StepNavigationTemplate>
            <asp:Button ID="StepPreviousButton" runat="server" BackColor="White" 
                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                CausesValidation="False" CommandName="MovePrevious" Font-Names="Verdana" 
                ForeColor="#990000" Text="Previous" />

            <asp:Button ID="StepNextButton" runat="server" BackColor="White" 
                BorderColor="#CC9966" BorderStyle="Solid" BorderWidth="1px" 
                CommandName="MoveNext" Font-Names="Verdana" ForeColor="#990000" Text="Next" />

        </StepNavigationTemplate>
    </asp:CreateUserWizard>
</div>
</asp:Content>

<asp:Content ID="Content4" runat="server" contentplaceholderid="SiteHead">
    <style type="text/css">
        .style1
        {
            width: 117px;
            font-weight:bold;
            text-align:right;
        }
    </style>
</asp:Content>
