﻿<%@ Page Title="Billed Activity Report" Language="C#" MasterPageFile="~/s/PageMaster.master" AutoEventWireup="true" CodeBehind="CertificatesIssued.aspx.cs" Inherits="AttorneyPortal.s.CertificatesIssued" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">

<style type="text/css">
    
    td.ReportTitle
    {
        text-align: center;
        vertical-align: top;
        color: Maroon;
        font-family: Arial;
        font-weight: bold;
        font-size: 14pt;
    }
    
    .style1
    {
        vertical-align: top;
        text-align: left;
        width: 80%;
        height: 116px;
    }
    .style2
    {
        vertical-align: top;
        text-align: center;
        width: 20%;
        height: 116px;
    }
    .style3
    {
        text-align: right;
        width: 40%;
        height: 24px;
    }
    .style4
    {
        text-align: left;
        height: 24px;
    }
</style>

<script language="javascript" type="text/javascript">
    function ExecuteMenu(sel) {

        // Get the value from the selected control.
        var value = sel.value;

        // Ignore the "select one of these" options
        if (value == "") {
            return;
        }

        // Start a new window with the new form definition.
        window.open(value, '_self')
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
	<table style="border-color:Green; width:100%; border:2; border-bottom-style:solid; border-width:medium; padding:6px">
        <tbody>
            <tr>
                <!-- filter qualifier information -->
                <td class="style1">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td colspan="3" style="text-align:center; color:Window; font-weight:bold; background-color:Maroon;">FILTER CRITERIA</td>
                            </tr>

                            <!-- starting date -->
                            <tr>
                                <td class="style3">Starting date:&nbsp;&nbsp;</td>
                                <td class="style4"><asp:TextBox runat="server" ID="filterStartingDate" Columns="10" MaxLength="10" TextMode="SingleLine" ToolTip="Starting date for filtering transactions" /></td>
                                <td>
                                    <div>
                                        <asp:RegularExpressionValidator
                                            runat="server"
                                            ID="RegularExpressionValidator1"
                                            ControlToValidate="filterStartingDate"
                                            Display="Dynamic"
                                            CssClass="ReqTxt"
                                            ErrorMessage="Use MM/DD/YYYY"
                                            SetFocusOnError="true"
                                            ValidationGroup="FormValidator"
                                            ValidationExpression="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/(19|20)[0-9]{2}"
                                        />
                                        </div>
                                </td>
                            </tr>

                            <!-- ending date -->
                            <tr>
                                <td class="style3">Ending date:&nbsp;&nbsp;</td>
                                <td class="style4"><asp:TextBox runat="server" ID="filterEndingDate" Columns="10" MaxLength="10" TextMode="SingleLine" ToolTip="Ending date for filtering transactions" /></td>
                                <td>
                                    <div>
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator2"
                                            runat="server"
                                            ControlToValidate="filterEndingDate"
                                            Display="Dynamic"
                                            CssClass="ReqTxt"
                                            ErrorMessage="Use MM/DD/YYYY"
                                            SetFocusOnError="true"
                                            ValidationGroup="FormValidator"
                                            ValidationExpression="(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/(19|20)[0-9]{2}"
                                        />
                                    </div>
                                </td>
                            </tr>

                            <!-- social security number -->
                            <tr>
                                <td class="style3">Social Security Number:&nbsp;&nbsp;</td>
                                <td class="style4"><asp:TextBox runat="server" ID="filterSSN" Columns="11" MaxLength="11" TextMode="SingleLine" ToolTip="Social Security number for a specific client" /></td>
                                <td>
                                    <div>
                                        <asp:RegularExpressionValidator
                                            ID="RegularExpressionValidator3"
                                            runat="server"
                                            ControlToValidate="filterSSN"
                                            Display="Dynamic"
                                            CssClass="ReqTxt"
                                            ErrorMessage="Use ###-##-####"
                                            SetFocusOnError="true"
                                            ValidationGroup="FormValidator"
                                            ValidationExpression="([1-9][0-9][0-9]|0[1-9][0-9]|00[1-9])-?([1-9][0-9]|0[1-9])-?([1-9][0-9][0-9][0-9]|0[1-9][0-9][0-9]|00[1-9][0-9]|000[1-9])"
                                        />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>

                <!-- Submit and export buttons -->
                <td class="style2">
                    <table style="width:100%">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:ImageButton runat="server" ID="View" AlternateText="View" 
                                        CommandArgument="view" CommandName="Submit" ImageAlign="Middle" 
                                        PostBackUrl="~/s/CertificatesIssued.aspx" ImageUrl="~/images/btn_OC_view.gif" 
                                        BorderStyle="Inset" BorderWidth="3" BorderColor="ActiveBorder" 
                                        onclick="View_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton runat="server" ID="Export" AlternateText="Export" 
                                        CommandArgument="export" CommandName="Submit" ImageAlign="Middle" 
                                        PostBackUrl="~/s/CertificatesIssued.aspx" ImageUrl="~/images/btn_OC_export.gif" 
                                        BorderStyle="Inset" BorderWidth="3" BorderColor="ActiveBorder" 
                                        onclick="Export_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>

            <!-- detail table -->
			<tr>
                <td colspan="2">
                    <asp:Literal runat="server" ID="DetailsTable" Mode="PassThrough" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
