﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttorneyPortal.Providers;
using ClearPoint;

namespace AttorneyPortal.s
{
    public partial class BilledActivity : System.Web.UI.Page
    {
        /// <summary>
        /// Class to represent the data that we are displaying for the attorney. The information
        /// is predominately the client_product record reference but the extra tables are included
        /// for current reference.
        /// </summary>
        private class displayRecord
        {
            public string clientID
            {
                get
                {
                    // The key is the Last for of the SSN and the First 4 of the Last name
                    if (view_client_address == null)
                    {
                        return string.Empty;
                    }

                    // The key is the combination of the SSN and the Last name.
                    return (view_client_address.ssn_1 ?? view_client_address.ssn_2 ?? string.Empty).Right(4) + view_client_address.name.Left(4).ToUpper();
                }
            }

            public DebtPlus.client_product client_product { get; set; }
            public DebtPlus.product product { get; set; }
            public DebtPlus.view_client_address view_client_address { get; set; }

            public displayRecord() { }

            public displayRecord(DebtPlus.client_product ClientProduct, DebtPlus.product Product, DebtPlus.view_client_address View)
                : this()
            {
                this.client_product      = ClientProduct;
                this.product             = Product;
                this.view_client_address = View;
            }
        }

        // Current vendor ID
        Int32 vendorID = default(Int32);

        protected void Page_Load(object sender, EventArgs e)
        {
            // There is nothing to pre-load. Only handle the post-back situation
            if (!IsPostBack)
            {
                if (!IsCallback)
                {
                    filterStartingDate.Text = DateTime.Now.Date.AddDays(-28).ToShortDateString();
                    filterEndingDate.Text   = DateTime.Now.Date.ToShortDateString();
                }
                return;
            }

            // Obtain the vendor ID. If we can't then don't complete the page load sequence.
            var obj = Session["firm_code"];
            if (obj == null)
            {
                return;
            }

            string vendorCode = obj.ToString();
            if (string.IsNullOrWhiteSpace(vendorCode))
            {
                return;
            }

            if (! Int32.TryParse(vendorCode, out vendorID))
            {
                return;
            }
        }

        /// <summary>
        /// Do the VIEW function
        /// </summary>
        protected void View_Click(object sender, ImageClickEventArgs e)
        {
            // Validate the controls when we are given the focus. Only accept the data when it is validated.
            Validate();
            if (!IsValid)
            {
                return;
            }

            // Build the string buffer to be downloaded
            var colRecords = getRecordSet();
            if (colRecords == null)
            {
                return;
            }

            // Generate the output table
            System.Collections.Generic.IEnumerator<displayRecord> ieRecordList = colRecords.OrderBy(s => s.product.description).ThenBy(s => s.view_client_address.name).GetEnumerator();
            if (colRecords.Count == 0 || !ieRecordList.MoveNext())
            {
                DetailsTable.Text = "<div style=\"text-align:center;color:Green;\">&nbsp;<br/>There are no items to be shown at this time.<br/>&nbsp;</div>\r\n";
                return;
            }

            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"<table id=""product_table"" cellpadding=""3"" cellspacing=""3"" style=""text-align:center; margin:auto; padding:0 0 0 0"">");
            sb.AppendLine("<tbody>");

            // Generate the tables, based upon the product information
            // Build a string representing the tables, broken down by product.
            bool additionalItems = true;
            while (additionalItems)
            {
                sb.AppendLine(@"<tr><td colspan=""7"" class=""ReportTitle"">");
                sb.AppendLine(Server.HtmlEncode(ieRecordList.Current.product.description));
                sb.AppendLine(@"</td></tr>");

                sb.AppendLine(@"<tr align=""left"" valign=""top"">");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold; width:10%"">Client#</th>");
                sb.AppendLine(@"<th style=""text-align:left; vertical-align:top; font-weight:bold;"">Name</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold;"">Appt Date</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold;"">Posted</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold;"">Client ID</th>");
                sb.AppendLine(@"<th style=""text-align:center; vertical-align:top; font-weight:bold;"">Cost</th>");
                sb.AppendLine(@"<th style=""text-align:left; vertical-align:top; font-weight:bold;"">Outstanding</th>");
                sb.AppendLine(@"</tr>");

                additionalItems = RenderTable(ref sb, ref ieRecordList);
            }

            // Set the display table into the page.
            sb.AppendLine("</tbody></table>");
            DetailsTable.Text = sb.ToString();
        }

        /// <summary>
        /// Do the EXPORT function
        /// </summary>
        protected void Export_Click(object sender, ImageClickEventArgs e)
        {
            // Validate the controls when we are given the focus. Only accept the data when it is validated.
            Validate();
            if (!IsValid)
            {
                return;
            }

            // Build the string buffer to be downloaded
            var colRecords = getRecordSet();
            if (colRecords == null)
            {
                return;
            }

            // Clear any response data First.
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();

            // This is the name of the file under which the download will be saved.
            string attachment = string.Format("attachment; filename={0}-{1:yyyyMMdd}.csv", "BilledActivity", DateTime.Now);

            // Add the headers for the donwload operation
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");

            // Emit the records for the download request
            var sb = new System.Text.StringBuilder();

            // This header line lists the fields that we are exporting
            sb.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"\r\n", "CLIENT#", "CLIENT NAME", "APPOINTMENT DATE", "POSTING DATE", "CLIENT IDENTIFIER", "SERVICE", "COST", "OUTSTANDING");

            foreach (var record in colRecords.OrderBy(s => s.product.description).ThenBy(s => s.client_product.client))
            {
                sb.AppendFormat("{0:0000000},", record.client_product.client);
                sb.AppendFormat("{0},", Utilitiy.ValidCSV(record.view_client_address.name));
                sb.AppendFormat("{0:MM/dd/yyyy},", record.client_product.date_created);
                sb.Append((record.client_product.invoice_date.HasValue ? string.Format("{0:MM/dd/yyyy}", record.client_product.invoice_date) : string.Empty) + ",");
                sb.AppendFormat("{0},", Utilitiy.ValidCSV(record.clientID));
                sb.AppendFormat("{0},", Utilitiy.ValidCSV(record.product.description));
                sb.AppendFormat("{0:f2},", record.client_product.cost);
                sb.AppendFormat("{0:f2}", record.client_product.cost - record.client_product.discount - record.client_product.tendered - record.client_product.disputed_amount);
                sb.AppendLine();
            }

            HttpContext.Current.Response.Write(sb.ToString());

            // Complete the request and we are finished.
            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// Return the list of display records based upon the data and the filter criteria.
        /// </summary>
        private System.Collections.Generic.List<displayRecord> getRecordSet()
        {
            System.Nullable<DateTime> startingDate = Utilitiy.getDate(filterStartingDate.Text);
            System.Nullable<DateTime> endingDate = Utilitiy.getDate(filterEndingDate.Text);
            string SSN = Utilitiy.getSSN(filterSSN.Text);

            // Generate the query statement
            using (var bc = new ClearPoint.DebtPlus.DatabaseContext())
            {
                // This is the base query. Do not get the records just yet.
                var q = (from cp in bc.client_productTable
                         join vp in bc.vendor_productTable      on cp.product equals vp.product
                         join ca in bc.view_client_addressTable on cp.client  equals ca.Id
                         join pr in bc.productTable             on cp.product equals pr.Id

                         where vp.vendor == cp.vendor
                         && cp.vendor == vendorID
                         && cp.disputed_amount == 0M
                         && cp.invoice_status >= 2

                         select new {clientProduct = cp, vendorProduct = vp, nameView = ca, productItem = pr }
                        );

                // Add the starting date
                if (startingDate.HasValue)
                {
                    DateTime cutoff = startingDate.Value.Date;
                    q = q.Where(s => s.clientProduct.date_created >= cutoff);
                }

                // Add the ending date
                if (endingDate.HasValue)
                {
                    DateTime cutoff = endingDate.Value.AddDays(1).Date;
                    q = q.Where(s => s.clientProduct.date_created < cutoff);
                }

                // Add the social security number
                if (!string.IsNullOrEmpty(SSN))
                {
                    q = q.Where(s => (s.nameView.ssn_1 != null && s.nameView.ssn_1 == SSN) || (s.nameView.ssn_2 != null && s.nameView.ssn_2 == SSN));
                }

                // Retrieve the records from the database with the proper sequence
                return q.Select(s => new displayRecord(s.clientProduct, s.productItem, s.nameView)).ToList();
            }
        }

        /// <summary>
        /// Render the recordset into a suitable table for the web page
        /// </summary>
        private Boolean RenderTable(ref System.Text.StringBuilder sb, ref IEnumerator<displayRecord> ieRecordList)
        {
            decimal totalOutstanding = 0M;
            decimal totalCost = 0M;

            // Current product being processed
            displayRecord record = ieRecordList.Current;
            DebtPlus.product currentProduct = record.product;

            // While there are more records to be processed....
            var AdditionalItems = true;
            while (AdditionalItems)
            {
                // Get the pointer to the next record to be processed.
                record = ieRecordList.Current;

                // If the product is for a different item then stop this table generation.
                if (record.product.Id != currentProduct.Id)
                {
                    break;
                }

                decimal outstanding = record.client_product.cost - record.client_product.discount - record.client_product.tendered - record.client_product.disputed_amount;
                totalOutstanding += outstanding;
                totalCost += record.client_product.cost;

                // Generate the table references
                sb.AppendLine("<tr align=\"left\" valign=\"top\">");

                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; width:10%\">{0:0000000}</td>\r\n", record.client_product.client);
                sb.AppendFormat("<td style=\"text-align:left; vertical-align:top;\">{0}</td>\r\n", Server.HtmlEncode((record.view_client_address.name ?? string.Empty).ToUpper()));
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0:MM/dd/yyyy}</td>\r\n", record.client_product.date_created);

                sb.Append("<td style=\"text-align:right; vertical-align:top;\">");
                if (record.client_product.invoice_date.HasValue)
                {
                    sb.Append(record.client_product.invoice_date.Value.ToString("MM/dd/yyyy"));
                }
                sb.AppendLine("</td>");

                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0}</td>\r\n", record.clientID ?? string.Empty);
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0:c2}</td>\r\n", record.client_product.cost);
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0:c2}</td>\r\n", record.client_product.cost - record.client_product.disputed_amount - record.client_product.tendered - record.client_product.disputed_amount);

                sb.AppendLine("</tr>");

                // Go to the next item in the list.
                AdditionalItems = ieRecordList.MoveNext();
            }

            sb.AppendLine("<tr>");
            sb.AppendLine("<td>&nbsp;</td>");
            sb.AppendLine("<td colspan=\"4\" style=\"text-align:left; vertical-align:top; font-weight:bold\">TOTALS:</td>");
            sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; font-weight:bold\">{0:c2}</td>\r\n", totalCost);
            sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; font-weight:bold\">{0:c2}</td>\r\n", totalOutstanding);
            sb.AppendLine("</tr>");

            return AdditionalItems;
        }
    }
}