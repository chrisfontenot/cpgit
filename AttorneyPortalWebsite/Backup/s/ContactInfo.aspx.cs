﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttorneyPortal.Providers;
using ClearPoint;

namespace AttorneyPortal.s
{
    public partial class ContactInfo : System.Web.UI.Page
    {
        /// <summary>
        /// Current vendor ID number
        /// </summary>
        private Int32? VendorID
        {
            get
            {
                Int32 localValue;

                // Read the vendor information from the database
                string firmCode = ((string)Session["firm_code"]);
                if (string.IsNullOrEmpty(firmCode))
                {
                    return null;
                }

                if (!Int32.TryParse(firmCode, out localValue))
                {
                    return null;
                }

                return localValue;
            }
        }

        /// <summary>
        /// Process the page LOAD sequence to handle the processing when the page is being loaded.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Do not process the post-back information here. Wait for the submit button to be decoded.
            if (Page.IsPostBack)
            {
                return;
            }

            // Load the current vendor contact record
            using (var db = new ClearPoint.DebtPlus.DatabaseContext())
            {
                this.read_address_invoice(db);
                this.read_address_main(db);
                this.read_contact_invoice(db);
                this.read_contact_main(db);
                this.read_vendor_record(db);
            }
        }

        /// <summary>
        /// Handle the CLICK event on the submit button to complete the form.
        /// </summary>
        protected void Submit_Click(object sender, ImageClickEventArgs e)
        {
            using (var db = new ClearPoint.DebtPlus.DatabaseContext())
            {
                this.write_vendor_record(db);
                this.write_address_invoice(db);
                this.write_address_main(db);
                this.write_contact_invoice(db);
                this.write_contact_main(db);

                // Ensure that the database is updated correctly at the end
                db.SubmitChanges();
            }

            // Redirect back to the main page
            Response.Redirect("~/s/Default.aspx", true);
        }

        private void read_vendor_record(ClearPoint.DebtPlus.DatabaseContext db)
        {
            // Find the vendor record to be updated
            var vendorRecord = db.vendorTable.Where(s => s.Id == VendorID).FirstOrDefault();
            if (vendorRecord != null)
            {
                firm_name.Text = vendorRecord.Name;
            }
        }

        private void write_vendor_record(ClearPoint.DebtPlus.DatabaseContext db)
        {
            // Find the vendor record to be updated
            var vendorRecord = db.vendorTable.Where(s => s.Id == VendorID).FirstOrDefault();
            if (vendorRecord == null)
            {
                return;
            }

            // Correct the vendor name
            vendorRecord.Name = firm_name.Text.Trim();
        }

        /// <summary>
        /// Send the email message to the owner of the website if desired
        /// </summary>
        /// <param name="dict"></param>
        private void NotifyOwner(System.Collections.Generic.Dictionary<string, string> dict, string key)
        {
            // Find the configuration information in the web.config file
            var cnf = System.Configuration.ConfigurationManager.GetSection("EmailMessageConfigurationSection") as ClearPoint.Configuration.EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var OnPwChange = cnf.MailDefinitions.Where(s => string.Compare(s.Event, key, true) == 0).FirstOrDefault();
            if (OnPwChange == null || string.IsNullOrWhiteSpace(OnPwChange.To))
            {
                return;
            }

            // Send the email notice to the owner
            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var mailDefinition = OnPwChange.CreateMailDefinition();
                using (var msg = mailDefinition.CreateMailMessage(OnPwChange.To, dict, this))
                {
                    smtp.Send(msg);
                }
            }
        }

        private void read_address_main(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendor_address_g = db.vendor_addressTable.Where(s => s.vendor == VendorID.Value && s.address_type == 'G').FirstOrDefault();
            MainAddress.ReadRecord(db, vendor_address_g);
        }

        private void write_address_main(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendor_address_g = db.vendor_addressTable.Where(s => s.vendor == VendorID.Value && s.address_type == 'G').FirstOrDefault();
            if (vendor_address_g == null)
            {
                vendor_address_g = new ClearPoint.DebtPlus.vendor_address();
                vendor_address_g.vendor = VendorID.Value;
                vendor_address_g.address_type = 'G';
            }

            MainAddress.WriteRecord(db, ref vendor_address_g, firm_name.Text.Trim());
            if (vendor_address_g.addressID.HasValue)
            {
                if (vendor_address_g.Id < 1)
                {
                    db.vendor_addressTable.InsertOnSubmit(vendor_address_g);
                }
            }
        }

        private void read_contact_main(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendorContactType = db.vendor_contact_typeTable.Where(s => s.description == "Main").FirstOrDefault();
            if (vendorContactType == null)
            {
                return;
            }

            // Find the contact record
            var vendorContactRecord = db.vendor_contactTable.Where(s => s.vendor == VendorID && s.contact_type == vendorContactType.Id).FirstOrDefault();
            if (vendorContactRecord != null)
            {
                AttorneyContact.ReadRecord(db, vendorContactRecord);
            }
        }

        private void write_contact_main(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendorContactType = db.vendor_contact_typeTable.Where(s => s.description == "Main").FirstOrDefault();
            if (vendorContactType == null)
            {
                return;
            }

            // Find the contact record
            var vendorContactRecord = db.vendor_contactTable.Where(s => s.vendor == VendorID && s.contact_type == vendorContactType.Id).FirstOrDefault();
            if (vendorContactRecord == null)
            {
                vendorContactRecord = new ClearPoint.DebtPlus.vendor_contact();
                vendorContactRecord.vendor = VendorID.Value;
                vendorContactRecord.contact_type = vendorContactType.Id;

                db.vendor_contactTable.InsertOnSubmit(vendorContactRecord);
            }

            // Update the contact record
            AttorneyContact.WriteRecord(db, ref vendorContactRecord, firm_name.Text.Trim());
        }

        private void read_address_invoice(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendor_address_i = db.vendor_addressTable.Where(s => s.vendor == VendorID.Value && s.address_type == 'I').FirstOrDefault();
            InvoiceAddress.ReadRecord(db, vendor_address_i);
        }

        private void write_address_invoice(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendor_address_I = db.vendor_addressTable.Where(s => s.vendor == VendorID.Value && s.address_type == 'I').FirstOrDefault();
            if (vendor_address_I == null)
            {
                vendor_address_I = new ClearPoint.DebtPlus.vendor_address();
                vendor_address_I.vendor = VendorID.Value;
                vendor_address_I.address_type = 'I';
            }

            InvoiceAddress.WriteRecord(db, ref vendor_address_I, firm_name.Text.Trim());
            if (vendor_address_I.addressID.HasValue)
            {
                if (vendor_address_I.Id < 1)
                {
                    db.vendor_addressTable.InsertOnSubmit(vendor_address_I);
                }
            }
        }

        private void read_contact_invoice(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendorContactType = db.vendor_contact_typeTable.Where(s => s.description == "Invoicing").FirstOrDefault();
            if (vendorContactType == null)
            {
                return;
            }

            // Find the contact record
            var vendorContactRecord = db.vendor_contactTable.Where(s => s.vendor == VendorID && s.contact_type == vendorContactType.Id).FirstOrDefault();
            if (vendorContactRecord != null)
            {
                InvoiceContact.ReadRecord(db, vendorContactRecord);
            }
        }

        private void write_contact_invoice(ClearPoint.DebtPlus.DatabaseContext db)
        {
            var vendorContactType = db.vendor_contact_typeTable.Where(s => s.description == "Invoicing").FirstOrDefault();
            if (vendorContactType == null)
            {
                return;
            }

            // Find the contact record
            var vendorContactRecord = db.vendor_contactTable.Where(s => s.vendor == VendorID && s.contact_type == vendorContactType.Id).FirstOrDefault();
            if (vendorContactRecord == null)
            {
                vendorContactRecord = new ClearPoint.DebtPlus.vendor_contact();
                vendorContactRecord.vendor = VendorID.Value;
                vendorContactRecord.contact_type = vendorContactType.Id;

                db.vendor_contactTable.InsertOnSubmit(vendorContactRecord);
            }

            // Update the contact record
            InvoiceContact.WriteRecord(db, ref vendorContactRecord, firm_name.Text.Trim());
        }
    }
}