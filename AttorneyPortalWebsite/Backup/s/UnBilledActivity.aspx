﻿<%@ Page Title="Un-Billed Activity Report" Language="C#" MasterPageFile="~/s/PageMaster.master" AutoEventWireup="true" CodeBehind="UnBilledActivity.aspx.cs" Inherits="AttorneyPortal.s.UnBilledActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">

<style type="text/css">
    h2 
    {
        font-size:x-large;
        font-weight:bold;
        color:White;
        background-color:Green;
        padding:30px, 0px, 30px, 0px;
    }
    
    .ReportTable
    {
        width:100%;
        border: 5px solid black;
        border-style:solid;
        border-collapse:collapse;
        padding:3px;
    }
</style>

<script language="javascript" type="text/javascript">
    function ExecuteMenu(sel) {

        // Get the value from the selected control.
        var value = sel.value;

        // Ignore the "select one of these" options
        if (value == "") {
            return;
        }

        // Start a new window with the new form definition.
        window.open(value, '_self')
    }
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">
	<table style="border-color:Green; width:100%; border:2; border-bottom-style:solid; border-width:medium; padding:6px">
        <tbody>
            <!-- detail table -->
			<tr>
                <td>
                    <asp:Literal runat="server" ID="DetailsTable" Mode="PassThrough" />
                </td>
            </tr>

            <!-- Submit button -->        
            <tr>
                <td style="text-align:center">
                    <asp:ImageButton runat="server" ID="SubmitButton" AlternateText="Submit" ImageUrl="~/images/btn_OC_continue.gif" CausesValidation="true" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
