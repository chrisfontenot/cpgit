﻿<%@ Page Title="Financial Information" Language="C#" MasterPageFile="~/s/PageMaster.master" AutoEventWireup="true" CodeBehind="FinancialInfo.aspx.cs" Inherits="AttorneyPortal.s.FinancialInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <script language="javascript" type="text/javascript">
        function FlipFlopBox()
        {
            document.getElementById('<%=InputPayNone.ClientID%>').style.display = 'none';
            document.getElementById('<%=InputPayCC.ClientID%>').style.display = 'none';
            document.getElementById('<%=InputPayACH.ClientID%>').style.display = 'none';

            if (document.getElementById("CC").checked)
            {
                document.getElementById('<%=InputPayCC.ClientID%>').style.display = 'block';
                return;
            }

            if (document.getElementById("Ach").checked)
            {
                document.getElementById('<%=InputPayACH.ClientID%>').style.display = 'block';
                return;
            }

            document.getElementById('<%=InputPayNone.ClientID%>').style.display = 'block';
        }

        function ShowPay(isChecked)
        {
            if (!isChecked)
            {
                document.getElementById('<%=DisplayPay.ClientID%>').style.display = 'block';
                document.getElementById('<%=InputPay.ClientID%>').style.display = 'none';
                return;
            }

            document.getElementById('<%=DisplayPay.ClientID%>').style.display = 'none';
            document.getElementById('<%=InputPay.ClientID%>').style.display = 'block';
            FlipFlopBox();
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">

    <table id="MainTable" style="border-style:none; padding:3; width:100%">
        <tbody>
            <tr>
                <td colspan="3" align="center">

                    <!-- Name on the account -->
 		            <table style="width:95%; text-align:center; border-bottom-style:solid; border-width:medium; padding:6px">
                        <tbody>
				            <tr style="height:30px; vertical-align:middle">
					            <td style="width:42%;text-align:right;" class="NormTxt">
						            Name on the Account:&nbsp;&nbsp;
					            </td>
					            <td style="text-align:left;" class="NormTxt">
                                    <asp:TextBox runat="server" ID="NameOnCard" Columns="50" MaxLength="50" TextMode="SingleLine" AutoCompleteType="DisplayName" />
					            </td>
					            <td style="text-align:left" class="ReqTxt" >
                                    <asp:Literal runat="server" ID="ErrName" Mode="Encode" />
					            </td>
				            </tr>

				            <tr><td style="height:10" colspan="3"></td></tr>
                        </tbody>
		            </table>

                    <p style="height:15px">&nbsp;</p>

                    <!-- billing address -->
		            <table class="GreyBackDark" style="width:95%; border-style:inset; border-width:thick; border-collapse:collapse; border-spacing:2">
                        <tbody>
				            <tr>
					            <td style="text-align:center;">
						            <table style="border-style:none; width:100%; padding:0" >
                                        <tbody>
								            <tr><td class="NormHead" style="height:20; text-align:center;" colspan="3">Billing Address</td></tr>
								            <tr>
									            <td style="width:42%; height:10"></td>
									            <td style="width:20%"></td>
									            <td style="width:38%"></td>
								            </tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Address Line 1&nbsp;&nbsp;</td>
									            <td style="text-align:left;"><asp:TextBox runat="server" ID="BillAdr1" Columns="30" MaxLength="50" AutoCompleteType="BusinessStreetAddress" TextMode="SingleLine" /></td>
									            <td class="ReqTxt">&nbsp;<asp:Literal runat="server" ID="ErrAdr1" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
                                                <td class="NormTxt" style="text-align:right;">Line 2&nbsp;&nbsp;</td>
									            <td style="text-align:left;"><asp:TextBox runat="server" ID="BillAdr2" Columns="30" MaxLength="50" AutoCompleteType="None" TextMode="SingleLine" /></td>
									            <td></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
                                                <td class="NormTxt" style="text-align:right;">City&nbsp;&nbsp;</td>
									            <td style="text-align:left;"><asp:TextBox runat="server" ID="BillCity" Columns="30" MaxLength="50" AutoCompleteType="BusinessCity" TextMode="SingleLine" /></td>
									            <td class="ReqTxt">&nbsp;<asp:Literal runat="server" ID="ErrCity" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
                                                <td class="NormTxt" style="text-align:right;">State&nbsp;&nbsp;</td>
									            <td style="text-align:left;">
                                                    <asp:DropDownList runat="server" ID="BillState" />
                                                </td>
									            <td class="ReqTxt">&nbsp;<asp:Literal runat="server" ID="ErrSt" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Zip Code&nbsp;&nbsp;</td>
									            <td style="text-align:left;"><asp:TextBox runat="server" ID="BillZip" Columns="10" MaxLength="10" AutoCompleteType="BusinessZipCode" TextMode="SingleLine" /></td>
									            <td class="ReqTxt">&nbsp;<asp:Literal runat="server" ID="ErrZip" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>
                                        </tbody>
						            </table>
					            </td>
				            </tr>
                        </tbody>
		            </table>

                    <p style="height:15px">&nbsp;</p>

                    <!-- Change method of payment -->
                    <table style="width:100%;">
                        <tbody>
				            <tr style="height:40px; vertical-align:middle">
                                <td style="text-align:center">
		                            <input type="checkbox" name="Change" id="Change" value="Changed" onclick="ShowPay(this.checked)" />
		                            <label for="Change">Make changes to method of payment</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Display Payment Methods -->
                    <asp:Panel runat="server" ID="DisplayPay" style="display:block;">
                        <table class="GreyBackDark" style="width:95%; border-style:inset; border-width:thick; border-collapse:collapse; border-spacing:2">
                            <tbody>
					            <tr>
						            <td style="text-align:center;" class="NormTxt">

                                        <!-- Display no payment data -->
                                        <asp:Panel runat="server" ID="DisplayPayNone" Visible="true">
								            <br/>No payment type on record.<br/><br/>
							            </asp:Panel>

                                        <!-- Display Credit Card payment data -->
                                        <asp:Panel runat="server" ID="DisplayPayCC" Visible="false">
								            <br/>Credit Card Payment On File<br/><br/>

								            <table style="border-style:none; width:100%; padding:0" >
                                                <tbody>
										            <tr><td class="NormHead" style="height:20; text-align:center;" colspan="3">Account Information</td></tr>

										            <tr>
											            <td style="width:42%; height:10"></td>
											            <td style="width:20%"></td>
											            <td style="width:38%"></td>
										            </tr>

										            <tr>
											            <td class="NormTxt" style="text-align:right;">Card Type:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal ID="CreditCardType" runat="server" /></td>
											            <td></td>
										            </tr>

										            <tr><td style="height:10" colspan="3"></td></tr>

										            <tr>
											            <td class="NormTxt" style="text-align:right;">Credit/Debit Card Number:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal runat="server" ID="Display_CrdNo" /></td>
											            <td></td>
										            </tr>

										            <tr><td style="height:10" colspan="3"></td></tr>

										            <tr>
                                                        <td class="NormTxt" style="text-align:right;">Expiration Date:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal runat="server" ID="Display_CrdExp" /></td>
											            <td></td>
										            </tr>

										            <tr><td style="height:10" colspan="3"></td></tr>
										            <tr><td style="height:10" colspan="3"></td></tr>
                                                </tbody>
								            </table>
							            </asp:Panel>

                                        <!-- Display ACH payment data -->
                                        <asp:Panel runat="server" ID="DisplayPayACH" Visible="false">
								            <br/>Bank Draft Payment On File<br/><br/>

								            <table style="border-style:none; width:100%; padding:0" >
                                                <tbody>
										            <tr><td class="NormHead" style="height:20; text-align:center;" colspan="3">Account Information</td></tr>

										            <tr>
                                                        <td style="width:40%; height:10"></td>
											            <td style="width:20%"></td>
											            <td style="width:40%"></td>
										            </tr>

										            <tr>
											            <td class="NormTxt" style="text-align:right;">Routing Number:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal ID="Display_AchAba" Mode="Encode" runat="server" /></td>
											            <td></td>
										            </tr>

										            <tr><td style="height:10" colspan="3"></td></tr>

										            <tr>
											            <td class="NormTxt" style="text-align:right;">Account Number:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal ID="Display_AchNo" Mode="Encode" runat="server" /></td>
											            <td></td>
										            </tr>

										            <tr>
											            <td class="NormTxt" style="text-align:right;">Account Type:&nbsp;&nbsp;</td>
											            <td class="NormTxt" style="text-align:left"><asp:Literal ID="Display_CheckingSavings" Mode="Encode" runat="server" /></td>
											            <td></td>
										            </tr>

										            <tr>
											            <td style="height:10" colspan="3"></td>
										            </tr>
                                                </tbody>
								            </table>
							            </asp:Panel>

						            </td>
					            </tr>
                            </tbody>
			            </table>
                    </asp:Panel>

                    <!-- Input payment data -->
		            <asp:Panel runat="server" ID="InputPay" style="display:none;">

                        <!-- Change payment method -->
                        <table style="width:95%; text-align:left; vertical-align:top; border-style:none; padding:0 0 0 0">
				            <tr>
                                <td style="height:5" colspan="2"></td>
                            </tr>

                            <tr>
                                <td style="text-decoration:underline">
                                    Please choose a method of payment:
                                </td>

                                <td class="ReqTxt" style="height:5;text-align:center;">
                                    <asp:Literal ID="ErrPyTp" runat="server" Mode="Encode" />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
									<input type="radio" name="PayType" id="Ach" value="B" disabled="disabled" onclick="JavaScript: FlipFlopBox();" />&nbsp;<label for="Ach">Bank Draft</label>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
									<input type="radio" name="PayType" id="CC" value="C" onclick="JavaScript: FlipFlopBox();" />&nbsp;<label for="CC">Credit Card</label>
                                </td>
                            </tr>
                        </table>

                        <!-- Input Payment method -->
    		            <table class="GreyBackDark" style="width:95%; border-style:inset; border-width:thick; border-collapse:collapse; border-spacing:2">
				            <tr>
					            <td style="text-align:center;" class="NormTxt">

                                    <!-- No payment method -->
                                    <asp:Panel runat="server" ID="InputPayNone" style="display:block;">
    						            <br/>Please select a payment type.<br/><br/>
						            </asp:Panel>

                                    <!-- Credit Card payment method -->
                                    <asp:Panel runat="server" ID="InputPayCC" style="display:none;">
							            <table style="border-style:none; width:100%; padding:0" >
								            <tr>
									            <td class="NormHead" style="height:20; text-align:center;" colspan="3">
										            Account Information
									            </td>
								            </tr>
								            <tr>
									            <td style="width:42%; height:10"></td>
									            <td style="width:20%"></td>
									            <td style="width:38%"></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">
                                                    Credit/Debit Card Number&nbsp;&nbsp;
                                                </td>
									            <td style="text-align:left">
                                                    <asp:TextBox runat="server" ID="CrdNo" Columns="30" MaxLength="50" AutoCompleteType="None" TextMode="SingleLine" ToolTip="Please just use numbers only. Do not include spaces or dash characters, just the digits." />
                                                </td>
									            <td class="ReqTxt">
                                                    &nbsp;<asp:Literal runat="server" ID="ErrCdNo" Mode="Encode" />
                                                </td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Expiration Date&nbsp;&nbsp;</td>

									            <td style="text-align:left">
                                                    <asp:DropDownList CssClass="TxtBox" ID="CardExMnD" runat="server">
                                                        <asp:ListItem Selected="True" Text="---" Value="" />
                                                        <asp:ListItem Text="01 (Jan)" Value="01" />
                                                        <asp:ListItem Text="02 (Feb)" Value="02" />
                                                        <asp:ListItem Text="03 (Mar)" Value="03" />
                                                        <asp:ListItem Text="04 (Apr)" Value="04" />
                                                        <asp:ListItem Text="05 (May)" Value="05" />
                                                        <asp:ListItem Text="06 (Jun)" Value="06" />
                                                        <asp:ListItem Text="07 (Jul)" Value="07" />
                                                        <asp:ListItem Text="08 (Aug)" Value="08" />
                                                        <asp:ListItem Text="09 (Sep)" Value="09" />
                                                        <asp:ListItem Text="10 (Oct)" Value="10" />
                                                        <asp:ListItem Text="11 (Nov)" Value="11" />
                                                        <asp:ListItem Text="12 (Dec)" Value="12" />
                                                    </asp:DropDownList>

											        <span class="MedTxt" style="font-weight:bold">/</span>
                                                    <asp:DropDownList CssClass="TxtBox" ID="CardExYrD" runat="server" AppendDataBoundItems="true" DataTextField="year" DataValueField="year">
                                                        <asp:ListItem Text="---" Value="" Selected="True" />
                                                    </asp:DropDownList>
									            </td>
									            <td class="ReqTxt">
                                                    &nbsp;<asp:Literal runat="server" ID="ErrCdEx" Mode="Encode" />
                                                </td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">CCV Number&nbsp;&nbsp;</td>
									            <td style="text-align:left"><asp:TextBox runat="server" ID="CrdCcv" Columns="5" MaxLength="5" AutoCompleteType="None" TextMode="SingleLine" ToolTip="For American express, it is 4 digits on the front of the card. All others, it is on the back where you sign for it." /></td>
									            <td class="ReqTxt">&nbsp;<asp:Literal runat="server" ID="ErrCdCv" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>
							            </table>
						            </asp:Panel>

                                    <!-- ACH payment method -->
                                    <asp:Panel runat="server" ID="InputPayACH" style="display:none;">
							            <table style="border-style:none;width:100%;padding:0" >
								            <tr><td class="NormHead" style="height:20;text-align:center;" colspan="3">Account Information</td></tr>

								            <tr>
									            <td style="width:40%;height:10"></td>
									            <td style="width:20%"></td>
									            <td style="width:40%"></td>
								            </tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Routing Number&nbsp;&nbsp;</td>
									            <td class="NormTxt" style="text-align:left;"><asp:TextBox runat="server" ID="AchAba" Columns="9" MaxLength="9" AutoCompleteType="None" TextMode="SingleLine" ToolTip="This is the first number on a check. It is always 9 digits long. Do not confuse it with the check number." /></td>
                                                <td class="ReqTxt"><asp:Literal runat="server" ID="ErrAcRn" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Account Number&nbsp;&nbsp;</td>
									            <td style="text-align:left;"><asp:TextBox runat="server" ID="AchNo" Columns="30" MaxMaxLength="50" AutoCompleteType="None" TextMode="SingleLine" ToolTip="Please enter the account number. This is not the check number, nor the ABA number." /></td>
									            <td class="ReqTxt">&nbsp;<asp:Literal ID="ErrAcNo" runat="server" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>

								            <tr>
									            <td class="NormTxt" style="text-align:right;">Account Type&nbsp;&nbsp;</td>
									            <td style="text-align:left;">
                                                    <asp:DropDownList ID="CheckingSavings" runat="server">
                                                        <asp:ListItem Selected="True" Text="Checking" Value="C" />
                                                        <asp:ListItem Text="Savings" Value="S" />
                                                    </asp:DropDownList>
                                                </td>
									            <td class="ReqTxt">&nbsp;<asp:Literal ID="ErrCheckingSavings" runat="server" Mode="Encode" /></td>
								            </tr>

								            <tr><td style="height:10" colspan="3"></td></tr>
							            </table>
						            </asp:Panel>
					            </td>
				            </tr>
			            </table>
                    </asp:Panel>

                    <!-- Status message -->
                    <table style="width:100%;">
                        <tbody>
                            <tr style="vertical-align:middle">
                                <td style="text-align:center">
                                    <asp:Literal ID="StatusMessage" runat="server" Mode="PassThrough" />
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Submit button -->
                    <table style="width:100%;">
                        <tbody>
                            <tr style="vertical-align:middle">
                                <td style="text-align:center">
                                    <asp:ImageButton AlternateText="Continue" runat="server" ID="Submit" 
                                        BorderStyle="None" ImageAlign="Middle" ImageUrl="~/images/btn_OC_continue.gif" 
                                        onclick="Submit_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
