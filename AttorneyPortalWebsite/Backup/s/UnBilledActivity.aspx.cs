﻿using System;
using System.Linq;
using ClearPoint;

namespace AttorneyPortal.s
{
    public partial class UnBilledActivity : System.Web.UI.Page
    {
        private System.Collections.Generic.List<DebtPlus.product_dispute_type> colReasons = null;
        private System.Collections.Generic.List<DisplayRecord> colRecords = null;
        private DebtPlus.DatabaseContext db = null;

        private class DisplayRecord
        {
            public string DisputeKeyId
            {
                get
                {
                    return string.Format("_V_{0:f0}", client_product == null ? 0 : client_product.Id);
                }
            }

            public DebtPlus.client_product client_product { get; set; }
            public DebtPlus.product product { get; set; }
            public DebtPlus.view_client_address view_client_address { get; set; }

            public DisplayRecord() { }

            public DisplayRecord(DebtPlus.client_product clientProduct, DebtPlus.product product, DebtPlus.view_client_address view)
                : this()
            {
                this.client_product      = clientProduct;
                this.product             = product;
                this.view_client_address = view;
            }
        }

        private void RegisterHandlers()
        {
            this.SubmitButton.Click += SubmitButton_Click;
        }

        private void UnRegisterHandlers()
        {
            this.SubmitButton.Click -= SubmitButton_Click;
        }

        private void Page_Init(object sender, EventArgs e)
        {
            db = new DebtPlus.DatabaseContext();
        }

        private void Page_Unload(object sender, EventArgs e)
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        /// <summary>
        /// Process the page LOAD sequence.
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Obtain the vendor ID. If we can't then don't complete the page load sequence.
                Int32 vendorId;
                var obj = Session["firm_code"];
                if (obj == null)
                {
                    return;
                }

                string vendorCode = obj.ToString();
                if (string.IsNullOrWhiteSpace(vendorCode))
                {
                    return;
                }

                if (!Int32.TryParse(vendorCode, out vendorId))
                {
                    return;
                }

                // Get the transactions associated with this vendor
                // Find the list of dispute reasons. We need to do this only once.
                colReasons = db.product_dispute_typeTable.Where(s => s.ActiveFlag).ToList();
                if (colReasons.Find(s => s.Id == 0) == null)
                {
                    colReasons.Add(new DebtPlus.product_dispute_type()
                    {
                        Id = 0,
                        description = "OK. Not disputed",
                        ActiveFlag = true,
                        Default = false
                    });
                }

                colRecords = (from cp in db.client_productTable
                    join p in db.productTable on cp.product equals p.Id
                    join v in db.view_client_addressTable on cp.client equals v.Id
                    join vp in db.vendor_productTable on cp.vendor equals vp.vendor
                    where cp.vendor == vendorId
                          && cp.product == vp.product
                          && vp.EscrowProBono == 1
                          && cp.cost - cp.tendered - cp.discount - cp.disputed_amount > 0M
                          && cp.invoice_status == 1
                    select new DisplayRecord(cp, p, v)
                ).ToList();

                // Display the new information
                if (!Page.IsPostBack && !Page.IsCallback)
                {
                    DisplayPage(colReasons, colRecords);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void SubmitButton_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            if (UpdateInformation())
            {
                db.SubmitChanges();

                // Redirect the browser back to the main page here.
                Response.Redirect("~", true);
                return;
            }
        }

        /// <summary>
        /// Update the client_product record to reflect the dispute status.
        /// </summary>
        private bool UpdateInformation()
        {
            foreach (var record in colRecords)
            {
                // Use the Key ID that was generated when the page was created to find the response value.
                string disputeStatus = Request[record.DisputeKeyId];
                if (string.IsNullOrWhiteSpace(disputeStatus))
                {
                    continue;
                }

                // Translate the dispute status to a valid code
                Int32 disputeCode;
                if (!Int32.TryParse(disputeStatus, out disputeCode))
                {
                    continue;
                }

                // Ensure that the reason is legal
                if (colReasons.Find(s => s.Id == disputeCode) == null)
                {
                    continue;
                }

                // Translate the "0" reason to be "NULL" meaning "no dispute"
                if (disputeCode == 0)
                {
                    if (record.client_product.disputed_amount > 0M)
                    {
                        record.client_product.disputed_amount = 0M;
                        record.client_product.disputed_date   = null;
                        record.client_product.disputed_note   = null;
                        record.client_product.disputed_status = null;

                        // Generate a log entry too
                        var resolvedLog = new DebtPlus.product_transaction
                        {
                            product          = record.client_product.product,
                            client_product   = record.client_product.Id,
                            cost             = record.client_product.cost,
                            discount         = record.client_product.discount,
                            payment          = record.client_product.tendered,
                            transaction_type = "CD",
                            disputed         = record.client_product.disputed_amount,
                            note             = record.client_product.disputed_note,
                            vendor           = record.client_product.vendor
                        };

                        db.product_transactionsTable.InsertOnSubmit(resolvedLog);
                    }
                    continue;
                }

                record.client_product.disputed_status = disputeCode;
                record.client_product.disputed_amount = record.client_product.cost - record.client_product.tendered - record.client_product.discount;
                if (!record.client_product.disputed_date.HasValue)
                {
                    record.client_product.disputed_date = DateTime.Now.Date;
                }

                // Include the reason for the note
                var qReason = colReasons.Find(s => s.Id == disputeCode);
                if (qReason != null)
                {
                    record.client_product.disputed_note = qReason.description;
                }

                // Generate a log entry too
                var disputeLog = new DebtPlus.product_transaction
                {
                    product          = record.client_product.product,
                    client_product   = record.client_product.Id,
                    cost             = record.client_product.cost,
                    discount         = record.client_product.discount,
                    payment          = record.client_product.tendered,
                    transaction_type = "DS",
                    disputed         = record.client_product.disputed_amount,
                    note             = record.client_product.disputed_note,
                    vendor           = record.client_product.vendor
                };

                db.product_transactionsTable.InsertOnSubmit(disputeLog);
            }

            // Indicate that we had no problem with the conversion.
            return true;
        }

        private void DisplayPage(System.Collections.Generic.List<DebtPlus.product_dispute_type> reasonsCollection, System.Collections.Generic.List<DisplayRecord> colRecords)
        {
            // There should be something to display or we don't even start.
            if (colRecords.Count < 1)
            {
                // There is nothing to be shown. Just say so and leave.
                DetailsTable.Text = "<div style=\"text-align:center;color:Green;\">&nbsp;<br/>There are no items to be shown at this time.<br/>&nbsp;</div>\r\n";
                return;
            }

            // Build a string representing the tables, broken down by product.
            string outputTable = string.Empty;
            System.Collections.Generic.IEnumerator<DisplayRecord> ieRecordList = colRecords.Where(s => s.client_product.cost - s.client_product.discount - s.client_product.tendered - s.client_product.disputed_amount > 0M).OrderBy(s => s.product.description).ThenBy(s => s.view_client_address.name).GetEnumerator();
            if (ieRecordList.MoveNext())
            {
                bool additionalItems = true;
                while(additionalItems)
                {
                    string newTable;
                    RenderTable(reasonsCollection, ref ieRecordList, out newTable, out additionalItems);
                    outputTable += newTable;
                }

                // Set the display table into the page.
                DetailsTable.Text = outputTable;
            }
        }

        private void RenderTable(System.Collections.Generic.List<DebtPlus.product_dispute_type> reasonsCollection, ref System.Collections.Generic.IEnumerator<DisplayRecord> ieRecordList, out string resultString, out Boolean additionalItems)
        {
            var sb = new System.Text.StringBuilder();
            decimal totalOutstanding = 0M;
            additionalItems = true;

            // Current product being processed
            DisplayRecord record = ieRecordList.Current;
            DebtPlus.product currentProduct = record.product;

            sb.AppendFormat("<div style=\"display:block\"><table id=\"product_table_{0:f0}\" cellpadding=\"3\" cellspacing=\"3\">\r\n", currentProduct.Id);
            sb.Append("<tbody>\r\n");

            sb.AppendFormat("<tr><td colspan=\"6\"><h2>{0}</h2></td></tr>\r\n", currentProduct.description);

            sb.Append("<tr align=\"left\" valign=\"top\">\r\n");
                sb.Append("<th style=\"text-align:right; vertical-align:top; font-weight:bold; width:10%\">Client#</th>\r\n");
                sb.Append("<th style=\"text-align:left; vertical-align:top; font-weight:bold;\">Client Name</th>\r\n");
                sb.Append("<th style=\"text-align:right; vertical-align:top; font-weight:bold;\">Appt Date</th>\r\n");
                sb.Append("<th style=\"text-align:right; vertical-align:top; font-weight:bold;\">Amount Open</th>\r\n");
                sb.Append("<th style=\"text-align:center; vertical-align:top; font-weight:bold;\">Dispute?</th>\r\n");
                sb.Append("<th style=\"text-align:left; vertical-align:top; font-weight:bold;\">Dispute Reason</th>\r\n");
            sb.Append("</tr>\r\n");

            // While there are more records to be processed....
            while (additionalItems)
            {
                // Get the pointer to the next record to be processed.
                record = ieRecordList.Current;

                // If the product is for a different item then stop this table generation.
                if (record.product.Id != currentProduct.Id)
                {
                    break;
                }

                decimal outstanding = record.client_product.cost - record.client_product.discount - record.client_product.tendered - record.client_product.disputed_amount;
                totalOutstanding += outstanding;

                // Generate the table references
                sb.Append("<tr align=\"left\" valign=\"top\">");
                    sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; width:10%\">{0:0000000}</td>\r\n", record.client_product.client);
                    sb.AppendFormat("<td style=\"text-align:left; vertical-align:top;\">{0}</td>", Server.HtmlEncode((record.view_client_address.name ?? string.Empty).ToUpper()));
                    sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0:MM/dd/yyyy}</td>", record.client_product.date_created);
                    sb.AppendFormat("<td style=\"text-align:right; vertical-align:top;\">{0:c2}</td>", outstanding);
                    sb.AppendFormat("<td style=\"text-align:center; vertical-align:top;\">{0}</td>", record.client_product.disputed_status.HasValue ? "Y" : string.Empty);

                    // Find the current dispute reason. Make sure that something will be selected in the list.
                    Int32 disputeReason = record.client_product.disputed_status.GetValueOrDefault(0);
                    if (reasonsCollection.Find(s => s.Id == disputeReason) == null)
                    {
                        disputeReason = 0;
                    }

                    sb.Append("<td style=\"text-align:left; vertical-align:top;\">");
                        sb.AppendFormat("<select id=\"{0}\" name=\"{0}\">", record.DisputeKeyId);
                        foreach (var reason in reasonsCollection.OrderBy(s => s.Id))
                        {
                            sb.Append("<option ");
                            if (disputeReason == reason.Id)
                            {
                                sb.Append("selected=\"selected\" ");
                            }
                            sb.AppendFormat("value=\"{0:f0}\">{1}</option>", reason.Id, string.IsNullOrWhiteSpace(reason.description) ? string.Empty : Server.HtmlEncode(reason.description));
                        }

                        sb.Append("</select>");
                    sb.Append("</td>\r\n");
                sb.Append("</tr>\r\n");

                // Go to the next item in the list.
                additionalItems = ieRecordList.MoveNext();
            }

            sb.Append("<tr>\r\n");
                sb.Append("<td>&nbsp;</td>");
                sb.Append("<td colspan=\"2\" style=\"text-align:left; vertical-align:top; font-weight:bold\">TOTALS:</td>");
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; font-weight:bold\">{0:c2}</td>", totalOutstanding);
                sb.Append("<td colspan=\"2\">&nbsp;</td>");
            sb.Append("</tr>\r\n");

            sb.Append("</tbody></table></div>\r\n");


            resultString = sb.ToString();
        }
    }
}