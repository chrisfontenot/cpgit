﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttorneyPortal.s
{
    public partial class ContactInfo
    {
        public static readonly char vendorAddressType = 'G';        // General addresses only, please
        public static readonly char invoicingAddressType = 'I';     // Invoicing addresses only, please
    }
}