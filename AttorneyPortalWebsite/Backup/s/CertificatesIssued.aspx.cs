﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using ClearPoint;

namespace AttorneyPortal.s
{
    public partial class CertificatesIssued : Page
    {
        /// <summary>
        /// Class to represent the data that we are displaying for the attorney. The information
        /// is predominately the client_product record reference but the extra tables are included
        /// for current reference.
        /// </summary>
        private class DisplayRecord
        {
            /// <summary>
            /// Is the client fee waved?
            /// </summary>
            public bool IsFeeWaved
            {
                get
                {
                    if (ClientProduct.cost == 0M)
                    {
                        return true;
                    }

                    if (ClientProduct.cost - ClientProduct.discount <= 0M)
                    {
                        return true;
                    }

                    return false;
                }
            }

            public DebtPlus.client_product ClientProduct { get; set; }
            public DebtPlus.product Product { get; set; }
            public DebtPlus.people People { get; set; }
            public DebtPlus.name Name { get; set; }
            public string Status { get; private set; }

            public DisplayRecord()
            {
                Status = "CO";
            }
        }

        // Current vendor ID
        Int32 vendorID = default(Int32);

        protected void Page_Load(object sender, EventArgs e)
        {
            // There is nothing to pre-load. Only handle the post-back situation
            if (!IsPostBack)
            {
                if (!IsCallback)
                {
                    filterStartingDate.Text = DateTime.Now.Date.AddDays(-28).ToShortDateString();
                    filterEndingDate.Text = DateTime.Now.Date.ToShortDateString();
                }
                return;
            }

            // Obtain the vendor ID. If we can't then don't complete the page load sequence.
            var obj = Session["firm_code"];
            if (obj == null)
            {
                return;
            }

            string vendorCode = obj.ToString();
            if (string.IsNullOrWhiteSpace(vendorCode))
            {
                return;
            }

            Int32.TryParse(vendorCode, out vendorID);
        }

        /// <summary>
        /// Do the VIEW function
        /// </summary>
        protected void View_Click(object sender, ImageClickEventArgs e)
        {
            // Validate the controls when we are given the focus. Only accept the data when it is validated.
            Validate();
            if (!IsValid)
            {
                return;
            }

            // Build the string buffer to be downloaded
            var colRecords = GetRecordSet();
            if (colRecords == null)
            {
                return;
            }

            // Generate the output table
            IEnumerator<DisplayRecord> ieRecordList = colRecords.GetEnumerator();
            if (colRecords.Count == 0 || !ieRecordList.MoveNext())
            {
                DetailsTable.Text = "<div style=\"text-align:center;color:Green;\">&nbsp;<br/>There are no items to be shown at this time.<br/>&nbsp;</div>\r\n";
                return;
            }

            // Generate the tables, based upon the product information
            // Build a string representing the tables, broken down by product.
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(@"<table id=""product_table"" cellpadding=""3"" cellspacing=""3"" style=""text-align:center; margin:auto; padding:0 0 0 0"">");
            sb.AppendLine("<tbody>");

            bool additionalItems = true;
            while (additionalItems)
            {
                sb.AppendLine(@"<tr><td colspan=""5"" class=""ReportTitle"">");
                sb.AppendLine(Server.HtmlEncode(ieRecordList.Current.Product.description));
                sb.AppendLine(@"</td></tr>");

                sb.AppendLine(@"</tr>");
                sb.AppendLine(@"<th style=""text-align:left; vertical-align:top; font-weight:bold; width:50%;"">Client Name</th>");
                sb.AppendLine(@"<th style=""text-align:center; vertical-align:top; font-weight:bold; width:10%;"">Status</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold; width:20%;"">Certificate#</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold; width:10%;"">Issue Date</th>");
                sb.AppendLine(@"<th style=""text-align:right; vertical-align:top; font-weight:bold; width:10%;"">Client#</th>");
                sb.AppendLine(@"</tr>");

                additionalItems = RenderTable(ref sb, ref ieRecordList);
            }

            // Set the display table into the page.
            sb.AppendLine("</tbody></table>");
            DetailsTable.Text = sb.ToString();
        }

        /// <summary>
        /// Do the EXPORT function
        /// </summary>
        protected void Export_Click(object sender, ImageClickEventArgs e)
        {
            // Validate the controls when we are given the focus. Only accept the data when it is validated.
            Validate();
            if (!IsValid)
            {
                return;
            }

            // Build the string buffer to be downloaded
            var colRecords = GetRecordSet();
            if (colRecords == null)
            {
                return;
            }

            // Clear any response data First.
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();

            // This is the name of the file under which the download will be saved.
            string attachment = string.Format("attachment; filename={0}-{1:yyyyMMdd}.csv", "CertificatesIssued", DateTime.Now);

            // Add the headers for the donwload operation
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");

            // Emit the records for the download request
            var sb = new System.Text.StringBuilder();

            // This header line lists the fields that we are exporting
            sb.AppendFormat("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"\r\n", "CLIENT NAME", "STATUS", "CERTIFICATE#", "ISSUE DATE", "CLIENT#");

            foreach (var record in colRecords.OrderBy(s => s.Product.description).ThenBy(s => s.ClientProduct.client))
            {
                sb.AppendFormat("{0},{1},{2},{3:MM/dd/yy},{4:0000000}\r\n", Utilitiy.ValidCSV(record.Name.ToString().ToUpper()), Utilitiy.ValidCSV(record.Status), Utilitiy.ValidCSV((record.People.bkCertificateNo ?? string.Empty).ToUpper()), record.People.bkCertificateIssued, record.People.Client);
            }

            HttpContext.Current.Response.Write(sb.ToString());

            // Complete the request and we are finished.
            HttpContext.Current.Response.End();
        }

        /// <summary>
        /// Return the list of display records based upon the data and the filter criteria.
        /// </summary>
        private List<DisplayRecord> GetRecordSet()
        {
            DateTime? startingDate = Utilitiy.getDate(filterStartingDate.Text);
            DateTime? endingDate = Utilitiy.getDate(filterEndingDate.Text);
            string ssn = Utilitiy.getSSN(filterSSN.Text);

            // Generate the query statement
            using (var bc = new DebtPlus.DatabaseContext())
            {
                // This is the base query. Do not get the records just yet.
                var q = (from clientProduct in bc.client_productTable
                         join vendorProduct in bc.vendor_productTable on clientProduct.product equals vendorProduct.product
                         join people        in bc.PeopleTable         on clientProduct.client  equals people.Client
                         join product       in bc.productTable        on clientProduct.product equals product.Id
                         join names         in bc.nameTable           on people.NameID         equals names.Id
                         where clientProduct.vendor == vendorID
                         && vendorProduct.vendor == vendorID
                         && people.bkCertificateNo != null
                         && people.bkCertificateIssued != null

                         select new {clientProduct, vendorProduct, name = names, people, productItem = product }
                        );

                // Add the starting date
                if (startingDate.HasValue)
                {
                    DateTime cutoff = startingDate.Value.Date;
                    q = q.Where(s => s.people.bkCertificateIssued >= cutoff);
                }

                // Add the ending date
                if (endingDate.HasValue)
                {
                    DateTime cutoff = endingDate.Value.AddDays(1).Date;
                    q = q.Where(s => s.people.bkCertificateIssued < cutoff);
                }

                // Add the social security number
                if (!string.IsNullOrEmpty(ssn))
                {
                    q = q.Where(s => s.people.SSN != null && s.people.SSN == ssn);
                }

                // Retrieve the records from the database with the proper sequence
                return q.Select(s => new DisplayRecord() { ClientProduct = s.clientProduct, Product = s.productItem, People = s.people, Name = s.name }).ToList();
            }
        }

        /// <summary>
        /// Render the recordset into a suitable table for the web page
        /// </summary>
        private Boolean RenderTable(ref System.Text.StringBuilder sb, ref IEnumerator<DisplayRecord> ieRecordList)
        {
            // Current product being processed
            DisplayRecord record = ieRecordList.Current;
            DebtPlus.product currentProduct = record.Product;

            // While there are more records to be processed....
            var additionalItems = true;
            while (additionalItems)
            {
                // Get the pointer to the next record to be processed.
                record = ieRecordList.Current;

                // If the product is for a different item then stop this table generation.
                if (record.Product.Id != currentProduct.Id)
                {
                    break;
                }

                // Generate the table references
                sb.Append("<tr style=\"vertical-align:top;\"");
                if (record.IsFeeWaved)
                {
                    sb.Append(" class=\"YelloBack\"");
                }
                sb.AppendLine(">");

                sb.AppendFormat("<td style=\"text-align:left; vertical-align:top; width:50%\">{0}</td>\r\n", Server.HtmlEncode(record.Name.ToString().ToUpper()));
                sb.AppendFormat("<td style=\"text-align:center; vertical-align:top;; width:10%\">{0}</td>\r\n", Server.HtmlEncode(record.Status));
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; width:20%\">{0}</td>\r\n", Server.HtmlEncode((record.People.bkCertificateNo ?? string.Empty).ToUpper()));
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; width:10%\">{0}</td>\r\n", record.People.bkCertificateIssued.HasValue ? record.People.bkCertificateIssued.Value.ToShortDateString() : string.Empty);
                sb.AppendFormat("<td style=\"text-align:right; vertical-align:top; width:10%\">{0:0000000}</td>\r\n", record.People.Client);
                sb.AppendLine("</tr>");

                // Go to the next item in the list.
                additionalItems = ieRecordList.MoveNext();
            }

            return additionalItems;
        }
    }
}