﻿<%@ Page Title="Contact Information" Language="C#" MasterPageFile="~/s/PageMaster.master" AutoEventWireup="true" CodeBehind="ContactInfo.aspx.cs" Inherits="AttorneyPortal.s.ContactInfo" %>
<%@ Register Src="~/controls/ContactUpdate.ascx" TagName="ContactUpdate" TagPrefix="ctu" %>
<%@ Register Src="~/controls/VendorAddressUpdate.ascx" TagName="VendorAddressUpdate" TagPrefix="vendAddr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageHead" runat="server">
    <style type="text/css">
        .style2
        {
            height: 114px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageBody" runat="server">

    <table style="width:100%">
        <tbody>
            <tr>
                <td colspan="2" style="background-color:Teal; color:White; font-weight:bold; text-align:left;">
                    FIRM INFORMATION
                </td>
            </tr>

            <tr>
                <td  style="width:120px; text-align:left; vertical-align:top" >
                    <asp:Label runat="server" AssociatedControlID="firm_name">Firm Name</asp:Label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="firm_name" MaxLength="40" Columns="40" TextMode="SingleLine" />
                </td>
            </tr>

            <tr>
                <td>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="background-color:Teal; color:White; font-weight:bold; text-align:left">
                    FIRM ADDRESS
                </td>
            </tr>

            <tr>
                <td style="width:120px; text-align:left; vertical-align:top" >
                </td>
                <td>
                    <vendAddr:VendorAddressUpdate runat="server" ID="MainAddress" />
                </td>
            </tr>

            <tr>
                <td>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="background-color:Teal; color:White; font-weight:bold; text-align:left">
                    ATTORNEY INFORMATION
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <ctu:ContactUpdate runat="server" ID="AttorneyContact" />
                </td>
            </tr>

            <tr>
                <td>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="background-color:Teal; color:White; font-weight:bold; text-align:left">
                    BILLING ADDRESS
                </td>
            </tr>

            <tr>
                <td style="width:120px; text-align:left; vertical-align:top" >
                </td>
                <td>
                    <vendAddr:VendorAddressUpdate runat="server" ID="InvoiceAddress" />
                </td>
            </tr>

            <tr>
                <td colspan="2" style="background-color:Teal; color:White; font-weight:bold; text-align:left">
                    BILLING INFORMATION
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <ctu:ContactUpdate runat="server" ID="InvoiceContact" />
                </td>
            </tr>

            <!-- Submit button -->
            <tr>
                <td colspan="2" style="text-align:center;">
                    <asp:ImageButton runat="server" ImageUrl="~/images/btn_OC_continue.gif" 
                        AlternateText="Continue" ID="Submit" BorderStyle="None" 
                        onclick="Submit_Click" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
