﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Data.Linq;
using ClearPoint;

namespace AttorneyPortal.admin
{
    public partial class NewAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateUser.CreatingUser += RegisterUser_CreatingUser;
            CreateUser.CreatedUser += RegisterUser_CreatedUser;
            ((CustomValidator)(CreateUser.CreateUserStep.ContentTemplateContainer.FindControl("AttorneyInvalidValidator"))).ServerValidate += AttorneyInvalidValidator_ServerValidate;
            ((CustomValidator)(CreateUser.CreateUserStep.ContentTemplateContainer.FindControl("AttorneyDuplicateValidator"))).ServerValidate += AttorneyDuplicateValidator_ServerValidate;
            CreateUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        }

        /// <summary>
        /// Determine if the vendor ID is a valid entry.
        /// </summary>
        private void AttorneyInvalidValidator_ServerValidate(object sender, System.Web.UI.WebControls.ServerValidateEventArgs e)
        {
            string vendorLabel = e.Value;
            if (!string.IsNullOrWhiteSpace(vendorLabel))
            {
                System.Nullable<Int32> vendorid = GetVendorByLabel(vendorLabel);
                if (vendorid.HasValue)
                {
                    e.IsValid = true;
                    return;
                }
            }

            e.IsValid = false;
        }

        /// <summary>
        /// Determine if the vendor ID is duplicated.
        /// </summary>
        private void AttorneyDuplicateValidator_ServerValidate(object sender, System.Web.UI.WebControls.ServerValidateEventArgs e)
        {
            string vendorLabel = e.Value;
            if (!string.IsNullOrWhiteSpace(vendorLabel))
            {
                var vendorid = GetVendorByLabel(vendorLabel);
                if (! vendorid.HasValue)
                {
                    e.IsValid = true;
                    return;
                }

                var u = GetUserByVendor(vendorid.Value);
                if (u != null)
                {
                    e.IsValid = false;
                    return;
                }
            }
            e.IsValid = true;
        }

        /// <summary>
        /// Validate the additional fields on the user account creation.
        /// </summary>
        private void RegisterUser_CreatingUser(object sender, LoginCancelEventArgs e)
        {
            // Find the user name field
            string userName = CreateUser.UserName.Trim();
            if (userName == string.Empty)
            {
                e.Cancel = true;
                ((Literal)CreateUser.CreateUserStep.ContentTemplateContainer.FindControl("ErrorMessage")).Text = "The user name is not valid.";
                return;
            }

            // Ensure that the user does not exist
            var u = System.Web.Security.Membership.GetUser(userName);
            if (u != null)
            {
                e.Cancel = true;
                ((Literal)CreateUser.CreateUserStep.ContentTemplateContainer.FindControl("ErrorMessage")).Text = "The account has already been created.";
                return;
            }
        }

        /// <summary>
        /// Handle the completion of the account registration
        /// </summary>
        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            string userName = CreateUser.UserName.Trim();

            // Connection string to the database
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;

            // Find the ID of the new record
            var PKID = (System.Guid)System.Web.Security.Membership.GetUser(userName).ProviderUserKey;

            // Find the ID of the vendor record
            string vendorLabel = ((TextBox)CreateUser.CreateUserStep.ContentTemplateContainer.FindControl("Attorney")).Text;
            Int32? vendorID = GetVendorByLabel(vendorLabel);
            if (! vendorID.HasValue)
            {
                return;
            }

            using (var db = new ClearPoint.DebtPlus.DatabaseContext(connectionString))
            {
                var clientRecord = db.client_wwwTable.Where(s => s.Id == PKID).FirstOrDefault();
                if (clientRecord != null)
                {
                    clientRecord.DatabaseKeyID = vendorID.Value;
                    db.SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Retrieve the indicated vendor primary key from its label ID
        /// </summary>
        protected System.Nullable<System.Int32> GetVendorByLabel(string VendorLabel)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;
            using (var db = new ClearPoint.DebtPlus.DatabaseContext(connectionString))
            {
                var vendorRecord = db.vendorTable.Where(s => s.Label == VendorLabel).FirstOrDefault();
                if (vendorRecord != null)
                {
                    return vendorRecord.Id;
                }
            }

            return null;
        }

        /// <summary>
        /// Return the user associated with the indicated Vendor ID
        /// </summary>
        protected System.Web.Security.MembershipUser GetUserByVendor(Int32 VendorID)
        {
            // Find the vendor ID in the client_www table
            string applicationName = System.Web.Security.Membership.ApplicationName;
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;

            using (var db = new ClearPoint.DebtPlus.DatabaseContext(connectionString))
            {
                var clientRecord = db.client_wwwTable.Where(s => s.ApplicationName == applicationName && s.DatabaseKeyID == VendorID).FirstOrDefault();
                if (clientRecord != null)
                {
                    return System.Web.Security.Membership.GetUser(clientRecord.Id);
                }
            }

            return null;
        }
    }
}
