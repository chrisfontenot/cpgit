﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AttorneyPortal.Providers;

namespace AttorneyPortal
{
    public static partial class Utilitiy
    {
        /// <summary>
        /// Return the left-most number of characters from the input string. A null string
        /// is translated to an empty string for the purposes of this routine.
        /// </summary>
        /// <param name="source">The string to be used in the substring operation</param>
        /// <param name="n">The number of characters desired from the string</param>
        /// <returns></returns>
        public static string Left(this string source, int n)
        {
            // The count must be legal
            if (n <= 0)
            {
                throw new ArgumentException("\"n\" must be greater than zero");
            }

            // If there is no string then return an empty string. We don't want NULL
            // from this routine, ever.
            if (source == null)
            {
                return string.Empty;
            }

            // If the string is too short then just return the string
            if (source.Length < n)
            {
                return source;
            }

            // Otherwise return the leftmost "n" characters
            return source.Substring(0, n);
        }

        /// <summary>
        /// Return the right-most number of characters from the input string. A null string
        /// is translated to an empty string for the purposes of this routine.
        /// </summary>
        /// <param name="source">The string to be used in the substring operation</param>
        /// <param name="n">The number of characters desired from the string</param>
        /// <returns></returns>
        public static string Right(this string source, int n)
        {
            // The count must be legal
            if (n <= 0)
            {
                throw new ArgumentException("\"n\" must be greater than zero");
            }

            // If there is no string then return an empty string. We don't want NULL
            // from this routine, ever.
            if (source == null)
            {
                return string.Empty;
            }

            // If the string is too short then just return the string
            if (source.Length < n)
            {
                return source;
            }

            // Otherwise return the rightmost "n" characters
            return source.Substring(source.Length - n, n);
        }

        /// <summary>
        /// Return a masked string showing the rightmost digits of the input.
        /// </summary>
        public static string RightMask(this string source, int n)
        {
            // The count must be legal
            if (n <= 0)
            {
                throw new ArgumentException("\"n\" must be greater than zero");
            }

            // use and empty string for an empty input
            if (string.IsNullOrEmpty(source))
            {
                return string.Empty;
            }

            // Ensure that there are the number of characters desired
            if (source.Length < n)
            {
                source = source.PadLeft(n, '0');
            }

            // Return the marker for the rightmost number of characters
            return "********" + source.Substring(source.Length - n);
        }

        /// <summary>
        /// Remove everything but the digit sequences. Toss spaces, dashes, etc. that people may have added.
        /// </summary>
        /// <param name="input">The input string to manipulate</param>
        /// <returns>The string with just the numeric digits. All digits are string together.</returns>
        public static string DigitsOnly(this string input)
        {
            if (input != null)
            {
                return System.Text.RegularExpressions.Regex.Replace(input, @"\D", "");
            }
            return null;
        }

        /// <summary>
        /// Return a valid CSV string for the input. Add quotes if needed to the string so that special characters are accepted.
        /// </summary>
        /// <param name="input">The string to be processed</param>
        /// <returns></returns>
        public static string ValidCSV(string input)
        {
            // Do not be bothered with null strings here.
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            // Look for special characters that must be treated separately
            if (input.IndexOfAny(new char[] { '"', ',' }) >= 0)
            {
                return string.Format("\"{0}\"", input.Replace("\"", "\"\""));
            }

            // Leading or trailing blanks must be quoted as well.
            if (input.StartsWith(" ") || input.EndsWith(" "))
            {
                return string.Format("\"{0}\"", input.Replace("\"", "\"\""));
            }

            return input;
        }

        /// <summary>
        /// Retrieve the date from the indicated string
        /// </summary>
        /// <param name="input">String to be parsed for the date field.</param>
        public static System.Nullable<DateTime> getDate(string input)
        {
            // If there is a date then try to return the appropriate value.
            if (!string.IsNullOrWhiteSpace(input))
            {
                try
                {
                    DateTime currentDate;
                    if (DateTime.TryParse(input, out currentDate))
                    {
                        // Someone stupid may try to force a time. Ignore it.
                        currentDate = currentDate.Date;

                        // Don't allow some bozo to be funny either.
                        if (currentDate.Year >= 1800 && currentDate.Year < 2030)
                        {
                            return currentDate;
                        }
                    }
                }
                catch
                {
                }
            }

            // The NULL value means that there is no date or the date is not valid.
            return null;
        }

        /// <summary>
        /// Retrieve the SSN from the input string.
        /// </summary>
        /// <param name="input">String to be parsed for the SSN field.</param>
        public static string getSSN(string input)
        {
            // We need a valid string.
            if (!string.IsNullOrWhiteSpace(input))
            {
                // It must be nine digits long. Ignore any formatting characters.
                input = input.DigitsOnly();
                if (input.Length == 9)
                {
                    // The view inserts dashes so we need to do the same thing.
                    return input.Substring(0, 3) + "-" + input.Substring(3, 2) + "-" + input.Substring(5);
                }
            }

            // NULL means that there is no valid input for the SSN field.
            return null;
        }
    }
}