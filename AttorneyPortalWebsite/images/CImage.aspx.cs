﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal
{
    public partial class CImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Create a random code and store it in the Session object.
            string key = GenerateRandomCode(6);
            this.Session["CaptchaImageText"] = key;

            // Create a CAPTCHA image using the text stored in the Session object.
            using (var ci = new RandomImage(key, 300, 75))
            {
                // Change the response headers to output a JPEG image.
                this.Response.Clear();
                this.Response.ContentType = "image/jpeg";

                // Write the image to the response stream in JPEG format.
                ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);
            }
        }

        /// <summary>
        /// Generate the random string code
        /// </summary>
        /// <returns>The generated string</returns>
        private string GenerateRandomCode()
        {
            return GenerateRandomCode(5);
        }

        /// <summary>
        /// Generate the random string code
        /// </summary>
        /// <param name="length">Length of the desired string</param>
        /// <returns>The generated string</returns>
        private string GenerateRandomCode(int length)
        {
            return GenerateRandomCode(length, "23456789abcdefgjkmnopqrstuvwxyz");
        }

        /// <summary>
        /// Generate the random string code
        /// </summary>
        /// <param name="length">Length of the desired string</param>
        /// <param name="charSet">Characters from which to select the sequence</param>
        /// <returns>The generated string</returns>
        private string GenerateRandomCode(int length, string charSet)
        {
            // Start with a somewhat good random sequencer
            var r = new Random( (int) (System.DateTime.Now.Ticks % 10000000L) );
            Int32 charsetLength = charSet.Length;

            // Generate the string from the character set, choosing random characters from it.
            var s = new System.Text.StringBuilder();
            for (int j = 0; j < length; ++j)
            {
                int index = r.Next(charsetLength);
                s.Append(charSet[index]);
            }
            return s.ToString();
        }
    }

    public class RandomImage : System.IDisposable
    {
        //Default Constructor 
        public RandomImage()
        {
            noiseBackground = Color.White;
            noiseForeground = Color.LightGray;
            noiseStyle = HatchStyle.SmallConfetti;

            textBackground = Color.SkyBlue;
            textForeground = Color.Black;
            textStyle = HatchStyle.Percent10;
        }

        //property
        public System.Drawing.Image Image
        {
            get
            {
                return GenerateImage();
            }
        }

        public string Text { get; set; }

        private Int32 _width = 300;
        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                if (value != _width)
                {
                    if (value < 0)
                    {
                        throw new ArgumentOutOfRangeException("width", value, "Argument out of range, must be greater than zero.");
                    }
                    _width = value;
                }
            }
        }

        private int _height = 75;
        public int Height
        {
            get
            {
                return _height;
            }
            set
            {
                if (value != _height)
                {
                    if (value < 0)
                    {
                        throw new ArgumentOutOfRangeException("height", value, "Argument out of range, must be greater than zero.");
                    }
                    _height = value;
                }
            }
        }

        public System.Drawing.Color noiseBackground { get; set; }
        public System.Drawing.Color noiseForeground { get; set; }
        public HatchStyle noiseStyle { get; set; }

        public System.Drawing.Color textBackground { get; set; }
        public System.Drawing.Color textForeground { get; set; }
        public HatchStyle textStyle { get; set; }

        //Private variable
        private string text;
        private int width;
        private int height;
        private Random random = new Random();

        //Methods declaration
        public RandomImage(string s, int width, int height) : this()
        {
            this.text = s;
            this.width = width;
            this.height = height;
        }

        public void Dispose()
        {
            try
            {
                this.Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        private System.Drawing.Image GenerateImage()
        {
            float v = 4F;
            var bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;

                // Clear the rectangle
                Rectangle rect = new Rectangle(0, 0, width, height);
                HatchBrush hatchBrush = new HatchBrush(noiseStyle, noiseForeground, noiseBackground);
                g.FillRectangle(hatchBrush, rect);

                PointF[] points =
                            {
                                new PointF(this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                                new PointF(rect.Width - this.random.Next(rect.Width) / v, this.random.Next(rect.Height) / v),
                                new PointF(this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v),
                                new PointF(rect.Width - this.random.Next(rect.Width) / v, rect.Height - this.random.Next(rect.Height) / v)
                            };

                SizeF size;
                Font font = null;
                float fontSize = rect.Height + 1;

                try
                {
                    do
                    {
                        --fontSize;
                        font = new Font(FontFamily.GenericSansSerif, fontSize, FontStyle.Bold);
                        size = g.MeasureString(this.text, font);
                    } while (size.Width > rect.Width);

                    StringFormat format  = new StringFormat();
                    format.Alignment     = StringAlignment.Center;
                    format.LineAlignment = StringAlignment.Center;
                    GraphicsPath path    = new GraphicsPath();

                    // Paint in the text for the captcha and override the font size to make it 75 points. Otherwise use "font.size" as the size rather than the "75" below.
                    path.AddString(this.text, font.FontFamily, (int)font.Style, 75, rect, format);
                    var matrix = new Matrix();
                    matrix.Translate(0F, 0F);
                    path.Warp(points, rect, matrix, WarpMode.Perspective, 0F);

                    using (hatchBrush = new HatchBrush(textStyle, textForeground, textBackground))
                    {
                        g.FillPath(hatchBrush, path);
                        int m = Math.Max(rect.Width, rect.Height);
                        for (int i = 0; i < (int)(rect.Width * rect.Height / 30F); i++)
                        {
                            int x = this.random.Next(rect.Width);
                            int y = this.random.Next(rect.Height);
                            int w = this.random.Next(m / 50);
                            int h = this.random.Next(m / 50);
                            g.FillEllipse(hatchBrush, x, y, w, h);
                        }
                    }
                }
                finally
                {
                    font.Dispose();
                }
            }

            return bitmap;
        }
    }
}
