﻿using System;

namespace AttorneyPortal.Providers
{
    partial class Membership
    {
        /// <summary>
        /// Encrypt the password with MD5 encryption that is used in the client_www record.
        /// </summary>
        /// <param name="Password">Input password string</param>
        /// <returns></returns>
        public static string GetMD5Hash(string Password)
        {
            // Do not allow for blank passwords.
            if (string.IsNullOrWhiteSpace(Password))
            {
                return string.Empty;
            }

            // Encrypt the password for the entry
            var TempSource = System.Text.ASCIIEncoding.ASCII.GetBytes(Password);

            // Next, encode the password stream
            var MD5_Class = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var TempHash = MD5_Class.ComputeHash(TempSource);

            // Finally, merge the byte stream to a string
            var sbPasswd = new System.Text.StringBuilder();
            for (Int32 index = 0; index <= TempHash.GetUpperBound(0); ++index)
            {
                sbPasswd.Append(TempHash[index].ToString("X2"));
            }

            // The encoded password is the combined string
            return sbPasswd.ToString().ToUpper();
        }
    }
}