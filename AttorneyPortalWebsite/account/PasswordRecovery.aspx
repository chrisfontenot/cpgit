﻿<%@ Page Title="Password Recovery" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PasswordRecovery.aspx.cs" Inherits="AttorneyPortal.account.PasswordRecovery" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteHeader" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SiteContent" runat="server">
    <div class="TitleHeader">
        Password Recovery
    </div>

<div style="height:20px;">&nbsp;</div>

<asp:ValidationSummary runat="server" ID="ValidationSummary1" ValidationGroup="PWRecovery" DisplayMode="BulletList" ShowSummary="true">
</asp:ValidationSummary>

<div style="height:20px;">&nbsp;</div>

<asp:PasswordRecovery
      SuccessText="Your password was successfully reset and emailed to you. Please check your email and change it as soon as possible."
      QuestionFailureText="Incorrect answer. Please try again." 
      runat="server" ID="PWRecovery" 
      UserNameFailureText="Client ID not found.">

    <MailDefinition IsBodyHtml="true" BodyFileName="~/App_Data/PasswordRecoveryMessage.txt"
           From="DoNotReply@ClearpointCCS.org" 
           Subject="Password Reset" 
           Priority="High">
    </MailDefinition>

    <UserNameTemplate>
        <p>
            The steps below will allow you to have a new password sent to the registered email address.
        </p>
        <p>
            You will need your Client ID number associated with your account and the email address that you
            used when the account was registered or later updated. If you don't have a registered email address
            then you will need to call our offices to have the password reset. Our office number is listed in the
            <asp:HyperLink runat="server" NavigateUrl="~/Contact.aspx">Contact Us</asp:HyperLink> page.
        </p>

        <span class="failureNotification">
            <asp:Literal ID="ErrorLiteral" runat="server"></asp:Literal>
        </span>

        <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification" ValidationGroup="PWRecovery"/>

        <div class="accountInfo">
            <fieldset class="recoverPassword">
                <legend>Account Information</legend>

                <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" Width="100%" CssClass="fixed_table">

                    <asp:TableRow runat="server" VerticalAlign="Top" Width="100%">
                        <asp:TableCell runat="server" HorizontalAlign="Left" Font-Bold="true" Width="20%">
                            <asp:Label runat="server" AssociatedControlID="UserName">Client ID:</asp:Label>
                        </asp:TableCell>
                        <asp:TableCell runat="server" HorizontalAlign="Left">
                            <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" /> <!-- TextMode="Number" -->

                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
                                    CssClass="failureNotification" ErrorMessage="Please use your account number for the Client ID field." ToolTip="Client ID must be numbers only." 
                                    ValidationExpression="[1-9][0-9]*"
                                    ValidationGroup="PWRecovery">*</asp:RegularExpressionValidator>

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName" 
                                    CssClass="failureNotification" ErrorMessage="The client ID is required." ToolTip="The client ID is required." 
                                    ValidationGroup="PWRecovery">*</asp:RequiredFieldValidator>
                        </asp:TableCell>
                    </asp:TableRow>

                    <asp:TableRow runat="server" VerticalAlign="Top" Width="100%">
                        <asp:TableCell runat="server" HorizontalAlign="Left" Font-Bold="true" Width="20%">
                            <asp:Label runat="server" AssociatedControlID="EmailAddressTB">Email Address:</asp:Label>
                        </asp:TableCell>

                        <asp:TableCell runat="server" HorizontalAlign="Left">
                            <asp:TextBox ID="EmailAddressTB" runat="server" CssClass="textEntry" TextMode="SingleLine" /> <!--  TextMode="Email" -->

                            <asp:RequiredFieldValidator ID="EmailAddressTBRequired" runat="server" ControlToValidate="EmailAddressTB" 
                                    CssClass="failureNotification" ErrorMessage="Email Address is required." ToolTip="Email Address is required." 
                                    ValidationGroup="PWRecovery">*</asp:RequiredFieldValidator>

                            <asp:RegularExpressionValidator ID="EmailRegularExpressionValidator" runat="server" ControlToValidate="EmailAddressTB"
                                    CssClass="failureNotification" ErrorMessage="The Email address does not seem to be valid" ToolTip="The Email address does not seem to be valid"
                                    ValidationExpression="^[A-Za-z0-9._%+-]{1,64}@(?:[A-Za-z0-9-]{1,63}\.){1,125}[A-Za-z]{2,63}$"
                                    Display="Dynamic" ValidationGroup="PWRecovery">*</asp:RegularExpressionValidator>
                        </asp:TableCell>
                    </asp:TableRow>

                </asp:Table>
            </fieldset>

            <p class="submitButton">
                <asp:Button ID="Button1" 
                    CausesValidation="true" 
                    ValidationGroup="PWRecovery" 
                    runat="server"
                    CommandName="Submit" 
                    Text="Submit" />
            </p>
        </div>
    </UserNameTemplate>

    <QuestionTemplate>
        Hello <asp:Literal runat="server" ID="personname" />,
        <p>
            You must answer your recovery question in order to have a new email 
            sent to you. The answer is not specific for case (meaning upper case
            letters such as A are the same as lower case letters). However, the
            answer must match EXACTLY, letter by letter, to what we have in our
            computer system for your account. If you do not know the answer to
            the question then please call our offices and a counselor will be
            happy to reassign a new password for you once your identity has been
            confirmed.
        </p>

        <p>
            At the end of this process we will create a new password for your account. Please use it
            to sign back on to the account and don't forget to change the password to something that
            is more meaningful to you as soon as possible.
        </p>

        <asp:Table ID="Table2" runat="server" Width="100%" CellPadding="0" CellSpacing="5">
            <asp:TableRow ID="TableRow1" runat="server">
                <asp:TableCell ID="TableCell2" runat="server" CssClass="failureNotification" ColumnSpan="2">
                    <asp:Literal ID="FailureText" runat="server" />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow ID="TableRow2" runat="server">
                <asp:TableCell ID="TableCell3" runat="server">
                    <asp:Label ID="QuestionLabel" runat="server" Font-Bold="true" Text="Question:" AssociatedControlID="Question" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell1" runat="server">
                    <asp:Literal runat="server" ID="Question" />
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow ID="TableRow3" runat="server">
                <asp:TableCell ID="AnswerLabel" runat="server" Font-Bold="true" Text="Answer:" AssociatedControlID="Answer" />
                <asp:TableCell ID="TableCell4" runat="server">
                    <asp:TextBox ValidationGroup="PWRecovery" runat="server" ID="Answer" /> <!-- TextMode="SingleLine" -->
                    <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" 
                                CssClass="failureNotification" ErrorMessage="Answer is required." ToolTip="Answer is required." 
                                ValidationGroup="PWRecovery">*</asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>

        <div style="text-align:center;">
            <asp:Button ID="submit" 
                CausesValidation="true" 
                ValidationGroup="PWRecovery" 
                runat="server"
                CommandName="submit" 
                Text="Submit" />
        </div>
    </QuestionTemplate>
</asp:PasswordRecovery>

</asp:Content>
