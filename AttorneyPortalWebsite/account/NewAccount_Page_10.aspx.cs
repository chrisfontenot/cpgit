﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Xml.Linq;
using System.Xml.Serialization;
using AttorneyPortal.s;
using ClearPoint;
using ClearPoint.Configuration;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_10 : Page
    {
        // Current vendor record when the vendor is created
        public DebtPlus.vendor vendorRecord { get; set; }

        /// <summary>
        /// Initialize the new page
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = Master;
            p.link10.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link10.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += Submit_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= Submit_Click;
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();

            // The second page has only one control on it and we don't show it in the summary
            var page2 = Session["Signup_Page2"] as Signup_Page2;
            try
            {
                if (page2 == null)
                {
                    Response.Redirect("~/account/NewAccount_Page_2.aspx");
                }

                page2.Validate();
            }
            catch
            {
                Response.Redirect("~/account/NewAccount_Page_2.aspx");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            UnRegiterHandlers();

            // Get the other display pages
            var page3 = Session["Signup_Page3"] as Signup_Page3;
            var page4 = Session["Signup_Page4"] as Signup_Page4;
            var page5 = Session["Signup_Page5"] as Signup_Page5;
            var page6 = Session["Signup_Page6"] as Signup_Page6;
            var page7 = Session["Signup_Page7"] as Signup_Page7;
            var page8 = Session["Signup_Page8"] as Signup_Page8;
            var page9 = Session["Signup_Page9"] as Signup_Page9;

            try
            {
                // Current transaction information
                SqlTransaction txn;

                // Attempt to create the attorney reference at this point.
                string connectionString = ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;
                var cn = new SqlConnection(connectionString);

                cn.Open();
                using (txn = cn.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    using (var bc = new DebtPlus.DatabaseContext(txn))
                    {
                        // Create the new vendor record.
                        vendorRecord = writeVendor(bc, page3, page4, page5, page6, page7, page8, page9);
                        if (vendorRecord != null)
                        {
                            txn.Commit();

                            // At the end of the processing, switch to the completion page.
                            completionPanel.Visible = true;
                            InputPanel.Visible = false;

                            // Log the user on the system so that the main page works correctly without having to signon again.
                            LogonUser(page3.userName, vendorRecord, true);

                            // Send the owner the message that we have created an account
                            NotifyOwner(vendorRecord, page3, page4, page5, page6, page7, page8, page9);
                        }
                    }
                }

                // Catch errors during the creation process
                // showErrorFrame(Server.HtmlEncode("We were unable to create the attorney record in our database due to an error. Please try the operation again a bit later or call us at the number listed below for help."));
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Send the message to the owner that a new account was created
        /// </summary>
        private void NotifyOwner(DebtPlus.vendor vendorRecord, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            // Find the configuration information in the web.config file
            var cnf = ConfigurationManager.GetSection("EmailMessageConfigurationSection") as EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var onCreate = cnf.MailDefinitions.Where(s => string.Compare(s.Event, "OnCreate", true) == 0).FirstOrDefault();
            if (onCreate == null || string.IsNullOrWhiteSpace(onCreate.To))
            {
                return;
            }

            // Create the mail definition object
            var mailDefinition = onCreate.CreateMailDefinition();

            // Find the destination address. it must be present to be notified
            // Construct the dictionary with the new fields
            var dict = new ListDictionary();
            dict.Add("<%UserName%>", page3.userName ?? string.Empty);
            dict.Add("<%Password%>", page3.Password ?? string.Empty);
            dict.Add("<%FirmName%>", page4.FirmName ?? string.Empty);
            dict.Add("<%FirmAddr1%>", page4.Address.Line1() ?? string.Empty);
            dict.Add("<%FirmCity%>", page4.Address.City ?? string.Empty);
            dict.Add("<%FirmState%>", getStateMailingCode(page4.Address.State) ?? string.Empty);
            dict.Add("<%FirmZip%>", page4.Address.Zip ?? string.Empty);
            dict.Add("<%FirmWebsite%>", page4.WebSite ?? string.Empty);
            dict.Add("<%Name1First%>", page5.Principal.Name.First ?? string.Empty);
            dict.Add("<%Name1Last%>", page5.Principal.Name.Last ?? string.Empty);
            dict.Add("<%Name1Email%>", page5.Principal.Email.ToString() ?? string.Empty);
            dict.Add("<%Name2First%>", page7.Billing.Name.First ?? string.Empty);
            dict.Add("<%Name2Last%>", page7.Billing.Name.Last ?? string.Empty);
            dict.Add("<%Name2Email%>", page7.Billing.Email.ToString() ?? string.Empty);
            dict.Add("<%Telephone%>", page7.Billing.TelephoneNumber.ToString(true) ?? string.Empty);
            dict.Add("<%FAX%>", page7.Billing.FaxNumber.ToString() ?? string.Empty);
            dict.Add("<%ClientCount%>", page9.annual_clients.ToString() ?? string.Empty);
            dict.Add("<%AttorneyCount%>", page9.attorneys.ToString() ?? string.Empty);
            dict.Add("<%ReferredBy%>", page9.referral ?? string.Empty);
            dict.Add("<%Comments%>", page9.comments ?? string.Empty);
            dict.Add("<%EscrowPreFiling%>", page6.Service_PreFiling == 1 ? "Yes" : "No");
            dict.Add("<%EscrowPreDischarge%>", page6.Service_PreDischarge == 1 ? "Yes" : "No");
            dict.Add("<%VendorLabel%>", vendorRecord.Label ?? string.Empty);
            dict.Add("<%VendorID%>", vendorRecord.Id.ToString() ?? string.Empty);

            // Send the email notice to the owner
            using (var smtp = new SmtpClient())
            {
                using (var msg = mailDefinition.CreateMailMessage(onCreate.To, dict, this))
                {
                    smtp.Send(msg);
                }
            }
        }

        /// <summary>
        /// Perform the logon sequence without needing a username/password.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="vendorRecord"></param>
        /// <param name="isPersistent"></param>
        private void LogonUser(string username, DebtPlus.vendor vendorRecord, bool isPersistent)
        {
            // sometimes used to persist user roles
            string userData = string.Empty; // string.Join("|",GetCustomUserRoles());

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,                                                          // ticket version
                    username,                                                   // authenticated username
                    DateTime.Now,                                               // issueDate
                    DateTime.Now.AddMinutes(30),                                // expiryDate
                    isPersistent,                                               // true to persist across browser sessions
                    userData,                                                   // can be used to store additional user data
                    FormsAuthentication.FormsCookiePath);   // the path for the cookie

            // Encrypt the ticket using the machine key
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            // Add the cookie to the request to save it
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            Response.Cookies.Add(cookie);

            // Add the session information for the vendor so that we can find it again
            Session["atty_name"] = Server.HtmlEncode(vendorRecord.Name ?? string.Empty);
            Session["firm_code"] = vendorRecord.Id.ToString("0000");
        }

        /// <summary>
        /// Create the vendor reference
        /// </summary>
        private DebtPlus.vendor writeVendor(DebtPlus.DatabaseContext bc, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            // Create the vendor record.
            var vendorRecord = CreateVendorRecord(bc, page3, page4, page5, page6, page7, page8, page9);
            if (vendorRecord == null)
            {
                return null;
            }

            // Write the vendor components
            writeAddresses   (bc, vendorRecord.Id, page3, page4, page5, page6, page7, page8, page9);
            WriteContacts    (bc, vendorRecord.Id, page3, page4, page5, page6, page7, page8, page9);
            WriteProducts    (bc, vendorRecord.Id, page3, page4, page5, page6, page7, page8, page9);
            WriteWebAcccess  (bc, vendorRecord.Id, page3, page4, page5, page6, page7, page8, page9);
            writeNotes       (bc, vendorRecord,    page3, page4, page5, page6, page7, page8, page9);

            // Return the current vendor record
            return vendorRecord;
        }

        /// <summary>
        /// Write all of the addresses to the database for this vendor
        /// </summary>
        private void writeAddresses(DebtPlus.DatabaseContext bc, Int32 VendorID, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            DebtPlus.address invoiceAddress = null;
            DebtPlus.address firmAddress    = null;

            // Generate the vendor address information first
            firmAddress = CreateAddress(bc, page4.FirmName, page4.Address);
            if (firmAddress != null)
            {
                createVendorAddress(bc, VendorID, firmAddress.Id, ContactInfo.vendorAddressType, page4.FirmName, string.Empty);
            }

            // Generate the invoice address information as well
            if (! page8.SameAddress)
            {
                invoiceAddress = CreateAddress(bc, page4.FirmName, page8.Invoice);
            }

            // If there is no invoice address then duplicate the firm address for the record.
            // We can't just use the same pointer because each address must be immutable.
            if (invoiceAddress == null)
            {
                invoiceAddress = CreateAddress(bc, page4.FirmName, page4.Address);
            }

            // Create the invoicing address reference once we have an invoice address
            if (invoiceAddress != null)
            {
                createVendorAddress(bc, VendorID, invoiceAddress.Id, ContactInfo.invoicingAddressType, page4.FirmName, string.Empty);
            }
        }

        /// <summary>
        /// Write all of the contacts for this vendor to the database
        /// </summary>
        private void WriteContacts(DebtPlus.DatabaseContext bc, Int32 VendorID, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            var mainRecord = writeSingleContact(bc, VendorID, "Main", page5.Principal.Name, page4.FirmName, page5.Principal.Address, page5.Principal.TelephoneNumber, page5.Principal.FaxNumber, page5.Principal.Email);

            // If there is an alternate contact then use that name for the contact data.
            if (page7.Billing != null && page7.Billing.Name.ToString() != string.Empty)
            {
                var invoiceRecord = writeSingleContact(bc, VendorID, "Invoicing", page7.Billing.Name, page4.FirmName, page7.Billing.Address, page7.Billing.TelephoneNumber, page7.Billing.FaxNumber, page7.Billing.Email);
                if (invoiceRecord != null)
                {
                    return;
                }
            }

            // Use the primary contact for the invoice operation
            writeSingleContact(bc, VendorID, "Invoicing", page5.Principal.Name, page4.FirmName, page4.Address, page5.Principal.TelephoneNumber, page5.Principal.FaxNumber, page5.Principal.Email);
        }

        /// <summary>
        /// Write all of the product references for this vendor to the database
        /// </summary>
        private void WriteProducts(DebtPlus.DatabaseContext bc, Int32 VendorID, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            var prefilingBankruptcy = WriteSingleProduct(bc, VendorID, "BK Pre-File Internet", page6.Service_PreFiling.GetValueOrDefault());
            var predischargeBanktupcy = WriteSingleProduct(bc, VendorID, "BK Pre-Discharge Internet", page6.Service_PreDischarge.GetValueOrDefault());
        }

        /// <summary>
        /// Create a Web address block for this vendor in the database.
        /// </summary>
        private void WriteWebAcccess(DebtPlus.DatabaseContext bc, Int32 VendorID, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            createWebAccess(bc, VendorID, page3.userName, page3.Password, page5.Principal.Email.Address);
        }

        /// <summary>
        /// Create a vendor Contact record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor_contact writeSingleContact(DebtPlus.DatabaseContext bc, Int32 VendorID, string ContactType, NameBlock nb, string firmName, AddressBlock adr, TelephoneBlock phone, TelephoneBlock fax, EmailBlock email)
        {
            var contactTypeID = getContactType(bc, ContactType);
            if (contactTypeID == null)
            {
                return null;
            }

            DebtPlus.address addressId           = CreateAddress(bc, firmName, adr);
            DebtPlus.name nameId                 = createName(bc, nb);
            DebtPlus.EmailAddress emailId        = createEmailAddress(bc, email);
            DebtPlus.TelephoneNumber telephoneId = createTelephone(bc, phone);
            DebtPlus.TelephoneNumber faxId       = createTelephone(bc, fax);

            var record = createVendorContract(bc,
                        VendorID,
                        contactTypeID.Id,
                        nameId      == null ? (Int32?)null : nameId.Id,
                        addressId   == null ? (Int32?)null : addressId.Id,
                        emailId     == null ? (Int32?)null : emailId.Id,
                        telephoneId == null ? (Int32?)null : telephoneId.Id,
                        faxId       == null ? (Int32?)null : faxId.Id,
                        string.Empty,
                        string.Empty);

            return record;
        }

        /// <summary>
        /// Create a vendor product record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor_product WriteSingleProduct(DebtPlus.DatabaseContext bc, Int32 VendorID, string ProductType, Int32 EscrowProBono)
        {
            // Find the product type reference
            var productTypeID = getProduct(bc, ProductType);
            if (productTypeID == null)
            {
                return null;
            }

            // Create the linkage from the vendor to the product
            var record = createVendorProduct(bc, VendorID, productTypeID.Id, EscrowProBono);
            return record;
        }

        /// <summary>
        /// Write all of the notes to the database for this vendor
        /// </summary>
        private void writeNotes(DebtPlus.DatabaseContext bc, DebtPlus.vendor VendorRecord, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            var note         = new DebtPlus.vendor_note();
            note.vendor      = VendorRecord.Id;
            note.subject     = "Additional fields from creation form";
            note.dont_delete = true;
            note.dont_edit   = true;
            note.dont_print  = true;
            note.type        = 1;      // Permanent note

            var sb = new StringBuilder();
            sb.AppendFormat("clients expected to refer to agency per year: {0:n0}\r\n", page9.annual_clients);
            sb.AppendFormat("attorneys using system: {0:n0}\r\n", page9.attorneys);
            sb.AppendFormat("referral note: {0}\r\n", page9.referral);
            sb.AppendFormat("\r\nnote comment block:\r\n{0}\r\n", page9.comments);

            note.note = sb.ToString();

            // Insert the new vendor note
            bc.vendor_noteTable.InsertOnSubmit(note);
            bc.SubmitChanges();
        }

        /// <summary>
        /// Create a Vendor record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor CreateVendorRecord(DebtPlus.DatabaseContext bc, Signup_Page3 page3, Signup_Page4 page4, Signup_Page5 page5, Signup_Page6 page6, Signup_Page7 page7, Signup_Page8 page8, Signup_Page9 page9)
        {
            // Allocate the vendor record
            var record = new DebtPlus.vendor
            {
                Name                    = page4.FirmName,
                MonthlyBillingDay       = 10,
                Type                    = "A",
                Website                 = page4.WebSite,
                SupressInvoices         = false,
                SupressPayments         = false,
                SupressPrintingPayments = false,
                BillingMode             = page8.BillingMode,
                ACH_AccountNumber       = page8.AccountNumber,
                ACH_CheckingSavings     = page8.CheckingSavings,
                ACH_RoutingNumber       = page8.RoutingNumber,
                ActiveFlag              = true,
                CC_ExpirationDate       = page8.ExpirationDate,
                CC_CVV                  = page8.CVV,
                NameOnCard              = page8.AccountName
            };

            // Do not record an empty record
            if (string.IsNullOrWhiteSpace(record.Name))
            {
                return null;
            }

            // If there are additional fields then add them to the vendor record
            var additionalFields = getVendorXML(page9);
            if (!string.IsNullOrEmpty(additionalFields))
            {
                record.Additional = XElement.Parse(additionalFields);
            }

            // Insert the record and return the new ID for the record
            bc.vendorTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Name record in the database and return the pointer to the caller.
        /// </summary>
        private DebtPlus.name createName(DebtPlus.DatabaseContext bc, NameBlock nm)
        {
            // Create the new Name record
            var record = new DebtPlus.name
            {
                First  = nm.First,
                Last   = nm.Last,
                Middle = nm.Middle,
                Prefix = nm.Prefix,
                Suffix = nm.Suffix
            };

            // Split the middle name from the first if possible
            var ipos = record.First.LastIndexOf(' ');
            if (ipos > 0)
            {
                record.Middle = record.First.Substring(ipos).Trim();
                record.First = record.First.Substring(0, ipos).Trim();
            }

            // Look for a non-null string. If found, record the record.
            foreach (var str in new[] { record.First, record.Last, record.Middle, record.Prefix, record.Suffix })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    bc.nameTable.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    return record;
                }
            }

            // Do not record an empty record
            return null;
        }

        /// <summary>
        /// Create an address record in the database and return the pointer to the new record to the caller.
        /// </summary>
        private DebtPlus.address CreateAddress(DebtPlus.DatabaseContext bc, string line1, AddressBlock adr)
        {
            // Generate the address reference information.
            var record = new DebtPlus.address
            {
                city              = adr.City,
                direction         = adr.PreDirection,
                house             = adr.Number,
                modifier          = adr.SecondaryUnit,
                modifier_value    = adr.SecondaryNumber,
                PostalCode        = adr.Zip,
                state             = adr.State.GetValueOrDefault(0),
                street            = adr.Street,
                suffix            = adr.Suffix,
                address_line_2    = string.Empty,
                address_line_3    = string.Empty,
                creditor_prefix_1 = line1,
                creditor_prefix_2 = string.Empty
            };

            // If there is a post direction then we must put it on the street
            if (!string.IsNullOrEmpty(adr.PostDirection))
            {
                record.street = record.street + " " + adr.PostDirection;
            }

            // Look for an empty address record. We don't record blanks.
            bool isEMpty = (adr.State.GetValueOrDefault(0) == 0);
            foreach (var str in new[] { record.house, record.address_line_2, record.address_line_3, record.city, record.creditor_prefix_1, record.creditor_prefix_2, record.direction, record.modifier, record.modifier_value, record.PostalCode, record.street, record.suffix })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    isEMpty = false;
                    break;
                }
            }

            // If the address record is empty then return a null pointer
            if (isEMpty)
            {
                return null;
            }

            // Insert the new record and return its ID.
            bc.addressTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor contact record in the database and return the ID to the caller.
        /// </summary>
        private DebtPlus.vendor_contact createVendorContract(DebtPlus.DatabaseContext bc, Int32 vendorId, Int32 contactType, Int32? NameID, Int32? AddressID, Int32? EmailID, Int32? TelephoneID, Int32? FaxID, string Note, string Title)
        {
            // Create the new record buffer.
            var record = new DebtPlus.vendor_contact
            {
                vendor       = vendorId,
                contact_type = contactType,
                addressID    = AddressID,
                emailID      = EmailID,
                faxID        = FaxID,
                nameID       = NameID,
                note         = Note,
                telephoneID  = TelephoneID,
                Title        = Title
            };

            bool isEmpty = true;
            foreach (var item in new[] { record.addressID, record.emailID, record.faxID, record.nameID, record.telephoneID })
            {
                if (item.HasValue)
                {
                    isEmpty = false;
                    break;
                }
            }

            foreach (var str in new[] { record.note, record.Title })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    isEmpty = false;
                    break;
                }
            }

            // If the record is empty then do not record it.
            if (isEmpty)
            {
                return null;
            }

            // Write the new record to the database and return its id.
            bc.vendor_contactTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor product record in the database and return the ID to the caller.
        /// </summary>
        private DebtPlus.vendor_product createVendorProduct(DebtPlus.DatabaseContext bc, Int32 VendorID, Int32? ProductType, Int32 EscrowProBono)
        {
            // Ignore blank references
            if (!ProductType.HasValue)
            {
                return null;
            }

            // Create the new record buffer.
            var record = new DebtPlus.vendor_product
            {
                vendor = VendorID,
                product = ProductType.Value,
                EscrowProBono = EscrowProBono
            };

            // Write the new record to the database and return its id.
            bc.vendor_productTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor_Address record in the database. This is used to keep track of the vedor address
        /// blocks.
        /// </summary>
        private DebtPlus.vendor_address createVendorAddress(DebtPlus.DatabaseContext bc, Int32 VendorID, Int32? AddressID, char AddressType, string Line1, string Attn)
        {
            var record = new DebtPlus.vendor_address
            {
                address_type = AddressType,
                addressID    = AddressID,
                vendor       = VendorID,
                attn         = Attn,
                line_1       = Line1,
                line_2       = string.Empty
            };

            if (AddressID.HasValue)
            {
                bc.vendor_addressTable.InsertOnSubmit(record);
                bc.SubmitChanges();
                return record;
            }

            return null;
        }

        /// <summary>
        /// Create an EmailAddress record in the database and return the ID to the caller. It is used in creating
        /// the contact information records.
        /// </summary>
        private DebtPlus.EmailAddress createEmailAddress(DebtPlus.DatabaseContext bc, EmailBlock email)
        {
            const Int32 emailAddressVaid = 1;

            // Create the new address record
            var record = new DebtPlus.EmailAddress
            {
                Address = email.Address,
                Validation = emailAddressVaid
            };

            // If the address is blank then return an empty pointer.
            if (string.IsNullOrWhiteSpace(record.Address))
            {
                return null;
            }

            // Insert the new record and return its ID
            bc.EmailAddressTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a TelephoneNumber record in the database and return the ID to the caller. It is used in creating
        /// the contact information records.
        /// </summary>
        private DebtPlus.TelephoneNumber createTelephone(DebtPlus.DatabaseContext bc, TelephoneBlock phone)
        {
            const Int32 country = 1; // USA

            // Create the new record
            var record = new DebtPlus.TelephoneNumber
            {
                Acode = phone.AreaCode,
                Country = country,
                Ext = phone.ext,
                Number = phone.Number
            };

            // Look for an empty record
            foreach (var str in new[] { record.Acode, record.Number })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    bc.TelephoneNumberTable.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    return record;
                }
            }

            return null;
        }

        /// <summary>
        /// Create a client_www record in the database and return the ID to the caller. This will grant the
        /// vendor web access to the account.
        /// </summary>
        private DebtPlus.client_www createWebAccess(DebtPlus.DatabaseContext bc, Int32 VendorID, string UserName, string Password, string EmailAddress)
        {
            // An empty username is an empty record
            if (string.IsNullOrEmpty(UserName))
            {
                return null;
            }

            // Allocate a new web record
            var record = new DebtPlus.client_www
            {
                Id                                     = Guid.NewGuid(),
                ApplicationName                        = Membership.ApplicationName,
                CreationDate                           = DateTime.UtcNow,
                Email                                  = EmailAddress,
                DatabaseKeyID                          = VendorID,
                FailedPasswordAnswerAttemptCount       = 0,
                FailedPasswordAttemptCount             = 0,
                IsApproved                             = true,
                IsLockedOut                            = false,
                FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow,
                FailedPasswordAttemptWindowStart       = DateTime.UtcNow,
                LastActivityDate                       = DateTime.UtcNow,
                LastLockoutDate                        = DateTime.UtcNow,
                LastLoginDate                          = DateTime.UtcNow,
                LastPasswordChangeDate                 = DateTime.UtcNow,
                PasswordAnswer                         = null,
                PasswordQuestion                       = null,
                UserName                               = UserName,
                Password                               = Providers.Membership.GetMD5Hash(Password)
            };

            // Create the web access
            bc.client_wwwTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Search for a contact Description based upon the description. The descriptions are fixed but the
        /// contact record numbers are not.
        /// </summary>
        private DebtPlus.vendor_contact_type getContactType(DebtPlus.DatabaseContext bc, string description)
        {
            var q = bc.vendor_contact_typeTable.Where(s => s.description == description).FirstOrDefault();
            if (q != null)
            {
                return q;
            }
            return null;
        }

        /// <summary>
        /// Search for a product Description based upon the description. The descriptions are fixed but the
        /// product record numbers are not.
        /// </summary>
        private DebtPlus.product getProduct(DebtPlus.DatabaseContext bc, string Description)
        {
            var q = bc.productTable.Where(s => s.description == Description).FirstOrDefault();
            if (q != null)
            {
                return q;
            }
            return null;
        }

        /// <summary>
        /// Class to hold the note information that we don't really have a spot in the database to contain
        /// </summary>
        public class NoteClass
        {
            public Int32 ClientCount { get; set; }
            public Int32 AttorneyCount { get; set; }
            public string Referral { get; set; }
            public string Comments { get; set; }
        }

        /// <summary>
        /// Return the XML document for the miscellaneous fields that are requested and not kept.
        /// </summary>
        /// <returns></returns>
        private string getVendorXML(Signup_Page9 page9)
        {
            // Allocate the class to hold the "noise" items that we don't track
            var cls = new NoteClass
            {
                AttorneyCount = page9.attorneys.GetValueOrDefault(),
                ClientCount   = page9.annual_clients.GetValueOrDefault(),
                Referral      = page9.referral,
                Comments      = page9.comments
            };

            // Serialize the class to a XML document.
            var sb = new StringBuilder();
            using (var fs = new StringWriter(sb))
            {
                var xml = new XmlSerializer(cls.GetType());
                xml.Serialize(fs, cls);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }

        private string getStateMailingCode(Int32? key)
        {
            if (key.HasValue)
            {
                using (var db = new DebtPlus.DatabaseContext())
                {
                    var q = db.stateTable.Where(s => s.Id == key.Value).FirstOrDefault();
                    if (q != null)
                    {
                        return q.MailingCode ?? string.Empty;
                    }
                }
            }

            return string.Empty;
        }
    }
}