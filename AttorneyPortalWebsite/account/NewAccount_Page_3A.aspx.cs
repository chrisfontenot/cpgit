﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttorneyPortal.Providers;
using System.ComponentModel;
using ClearPoint;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_3A : System.Web.UI.Page
    {
        /// <summary>
        /// Class to hold the note information that we don't really have a spot in the database to contain
        /// </summary>
        public class NoteClass
        {
            public NoteClass() { }

            public Int32 ClientCount { get; set; }
            public Int32 AttorneyCount { get; set; }
            public string Referral { get; set; }
            public string Comments { get; set; }
        }

        // Database context information
        DebtPlus.DatabaseContext db = null;

        // Current vendor record when the vendor is created
        public ClearPoint.DebtPlus.vendor vendorRecord { get; set; }

        /// <summary>
        /// Handle the page Initialization to create a new database context.
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            db = new DebtPlus.DatabaseContext();
        }

        /// <summary>
        /// Handle the unload event for the form. Close the database connection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        /// <summary>
        /// Process the LOAD sequence for the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Do not show the capcha if it is disabled
            validation_entry.Visible = !ClearPoint.Settings.DisableCapcha;

            // We don't need to do anything on these events. They will be handled separately later.
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                NoticePannel.Visible = true;
                InputPanel.Visible = true;
                completionPanel.Visible = false;
                ErrorPanel.Visible = false;

                // Load the dropdown controls with the proper database contents.
                firm_state.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, 0).ToArray());
                return;
            }

            // Register the event handlers on the postback operation.
            Submit.Click += Submit_Click;
        }

        /// <summary>
        /// Validate the page against the expected criteria
        /// </summary>
        private bool ValidatePage()
        {
            string openTag = "<li style=\"color:Red; font-weight:bold\">";
            string closeTag = "</li>";
            var sb = new System.Text.StringBuilder();

            // Validate the user name field before we look at anything else.
            foreach (var err in new string[] { getUserNameError(), getPasswordError(), getPassword2Error(), getFirmNameError(), getFirmAddressError(), getFirmCityError(), getFirmStateError(), getFirmZipCodeError(), getFirmWebsiteError(), getName1FirstError(), getName1LastError(), getTelephoneError(), getFaxError(), getEmail1Error(), getEmail2Error(), getAnnualClientsError(), getAttorneysError(), getEscrowError(), getChapchaError() })
            {
                if (! string.IsNullOrEmpty(err))
                {
                    sb.AppendFormat("{0}{2}.{1}", openTag, closeTag, Server.HtmlEncode(err));
                }
            }

            // If there is no error then there is no error
            if (sb.Length == 0)
            {
                return true;
            }

            // Add the associated tags to the front and end of the list
            sb.Insert(0, "<table><tbody><tr><td style=\"background-color:red; color:white; font-weight:bold; text-align:center;\">Please correct the following issues:</td></tr><tr><td><ul>");
            sb.Append("</ul></td></tr></tbody></table>");

            // Load the validation errors into the summary and enable it to be displayed
            showErrorFrame(sb.ToString());
            return false;
        }

        // Local routines to process the error conditions on the input page.
        private string getUserNameError()      { return getMissingField(username.Text, "user name") ?? getUserNameError(username.Text.Trim()); }
        private string getPasswordError()      { return getMissingField(password.Text, "password") ?? getPasswordError(password.Text); }
        private string getPassword2Error()     { return getMissingField(password2.Text, "password comparison") ?? getPassword2Error(password.Text, password2.Text); }
        private string getFirmNameError()      { return getMissingField(firm_name.Text, "firm's name"); }
        private string getFirmAddressError()   { return getMissingField(firm_address.Text, "firm's address"); }
        private string getFirmCityError()      { return getMissingField(firm_city.Text, "firm's City name"); }
        private string getFirmStateError()     { return getMissingField(firm_state.Text, "firm's State name", "0"); }
        private string getFirmZipCodeError()   { return getMissingField(firm_zip.Text, "ZIPCode") ?? getFirmZipCodeError(firm_zip.Text); }
        private string getFirmWebsiteError()   { return getMissingField(firm_webite.Text, "website") ?? getFirmWebsiteError(firm_webite.Text); }
        private string getName1FirstError()    { return getMissingField(name1_first.Text, "principal attorney's First name"); }
        private string getName1LastError()     { return getMissingField(name1_last.Text, "principal attorney's Last name"); }
        private string getTelephoneError()     { return getMissingField(telephone.Text, "telephone number") ?? getTelephoneError(telephone.Text, "telephone number"); }
        private string getFaxError()           { return getMissingField(fax.Text, "fax number") ?? getTelephoneError(fax.Text, "fax number"); }
        private string getEmail1Error()        { return getMissingField(email1.Text, "principal attorney's email address") ?? getEmailError(email1.Text, "principal attorney's"); }
        private string getEmail2Error()        { return getEmailError(email2.Text, "additional contact's"); }

        private string getPaymentError()
        {
            // There are no payment errors if there are no escrow accounts
            if (! pre_yes.Checked && ! post_yes.Checked)
            {
                return null;
            }

            // Look at the payment type
            if (BillingMethod.SelectedValue == "None")
            {
                return "For escrow accounts, please complete the payment information.";
            }

            // Check the credit card information
            if (BillingMethod.SelectedValue == "CreditCard")
            {
                return getCreditCardError();
            }

            return getACHError();
        }

        /// <summary>
        /// Return errors in the credit card method
        /// </summary>
        /// <returns></returns>
        private string getCreditCardError()
        {
            if (string.IsNullOrWhiteSpace(CreditCardAccountName.Text))
            {
                return "A name is required for the credit card information";
            }

            if (string.IsNullOrWhiteSpace(CreditCardAccountNumber.Text))
            {
                return "A credit card number is required";
            }

            // Isolate the credit card number
            var strNumber = System.Text.RegularExpressions.Regex.Replace(CreditCardAccountNumber.Text, @"\D", string.Empty);
            if (strNumber.Length < 12)
            {
                return "A credit card number is required";
            }

            // Validate the LUHN checksum on the card
            if (!AttorneyPortal.validations.Valid_Luhn(strNumber))
            {
                return "There is an error in the credit card number";
            }

            if (!validations.Valid_AMEX(strNumber) &&
                !validations.Valid_VISA(strNumber) &&
                !validations.Valid_MASTERCARD(strNumber))
            {
                return "We only accept American Express, Visa, or MasterCard cards";
            }

            // Validate the expiration date
            DateTime? expirationDate = getExpirationDate();
            if (!expirationDate.HasValue || expirationDate.Value < System.DateTime.Now)
            {
                return "The expiration date for the credit card needs to be corrected";
            }

            if (System.String.IsNullOrWhiteSpace(CreditCardCVV.Text))
            {
                return "Please check the card verification number";
            }

            var strCheckCode = System.Text.RegularExpressions.Regex.Replace(CreditCardCVV.Text, @"\^D", string.Empty);
            if ((validations.Valid_AMEX(strNumber) && strCheckCode.Length != 4) || strCheckCode.Length != 3)
            {
                return "Please check the card verification number. AMEX cards have 4 digits (on the front of the card). All others have only 3 (on the back)";
            }

            // Everything is valid for the credit card payments
            return null;
        }

        /// <summary>
        /// Get the expiration date for the credit card.
        /// </summary>
        /// <returns></returns>
        private DateTime? getExpirationDate()
        {
            string strMonth = CreditCardExpirationMonth.Text;
            string strYear = CreditCardExpirationYear.Text;

            if (string.IsNullOrWhiteSpace(strMonth) || string.IsNullOrWhiteSpace(strYear))
            {
                // Invalid items are null
                return null;
            }

            try
            {
                // Convert the entry to a suitable expiration date. We use the last day of the month for the value.
                DateTime newDate = new System.DateTime(int.Parse(strYear), int.Parse(strMonth), 1);
                return newDate.AddMonths(1).AddDays(-1);
            }
            catch
            {
                // Invalid items are null
                return null;
            }
        }

        /// <summary>
        /// Return errors in the ACH methods
        /// </summary>
        /// <returns></returns>
        private string getACHError()
        {
            string str = this.ACHType.SelectedValue;
            if (str != "C" && str != "S")
            {
                return "Please choose an account type for Checking or Savings";
            }

            str = this.ACHRouting.Text.Trim();
            if (! System.Text.RegularExpressions.Regex.IsMatch(str, @"\d{9}"))
            {
                return "The routing number is 9 numeric digits";
            }

            if (!AttorneyPortal.validations.ValidABAChecksum(str))
            {
                return "The routing number is not valid. Please check it again";
            }

            // Validate the account number
            str = System.Text.RegularExpressions.Regex.Replace(this.ACHAccountNumber.Text, @"\D", string.Empty);
            if (! System.Text.RegularExpressions.Regex.IsMatch(str, "[0-9]{5,}"))
            {
                return "The account number must be at least 5 digits.";
            }

            // There is no error condition here.
            return null;
        }

        /// <summary>
        /// Look for the proper settings in the escrow options.
        /// </summary>
        /// <returns></returns>
        private string getEscrowError()
        {
            // Look for the escrow options.
            if (pre_free.Checked || pre_no.Checked || pre_yes.Checked)
            {
                if (post_free.Checked || post_no.Checked || post_yes.Checked)
                {
                    return null;
                }
            }

            // The escrow options are not set. We need to know either yes or no on each of the two modes.
            return "please check the escrow options. Choose two of the six options";
        }

        /// <summary>
        /// Parse a ZipCode field to ensure that it is legal.
        /// </summary>
        private string getFirmZipCodeError(string ZipCode) { return getRegexError(ZipCode, "^[0-9]{5}(-[0-9]{4})?$", "Please use a format similar to ##### or #####-#### for the ZIPCode"); }

        /// <summary>
        /// Parse a telephone number field to ensure that it is legal
        /// </summary>
        private string getTelephoneError(string TelephoneNumber, string key) { return getRegexError(TelephoneNumber, @"^\(?[0-9][0-9][0-9]\)?\s*[0-9][0-9][0-9]-?[0-9][0-9][0-9][0-9]$", string.Format("the {0} is invalid", key)); }

        /// <summary>
        /// Parse the client count to ensure it is legal
        /// </summary>
        private string getAnnualClientsError() { return getMissingField(annual_clients.Text, "annual clients") ?? getRegexError(annual_clients.Text, "^[1-9][0-9]*$", "number of annual clients must be greater than zero"); }

        /// <summary>
        /// Parse the attorney count to ensure that it is legal
        /// </summary>
        /// <returns></returns>
        private string getAttorneysError()     { return getMissingField(attorneys.Text, "attorney count") ?? getRegexError(attorneys.Text, "^[1-9][0-9]*$", "number of attorneys must be greater than zero"); }

        /// <summary>
        /// Get the error associated with the username field.
        /// </summary>
        /// <returns></returns>
        private string getUserNameError(string nameString)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(nameString))
            {
                return null;
            }

            // Validate the username against the known patterns
            if (nameString.Length > 20 || !System.Text.RegularExpressions.Regex.IsMatch(nameString, "^[A-Z][A-Z0-9_]*$"))
            {
                return "the user name is not a valid name. Please use only letters and numbers. Also, the name must start with a letter";
            }

            // Find the user in the database. It must not exist for the application
            string applicationId = System.Web.Security.Membership.ApplicationName;
            using (var db = new DebtPlus.DatabaseContext())
            {
                var q = db.client_wwwTable.Where(s => s.ApplicationName == applicationId && s.UserName == nameString).FirstOrDefault();
                if (q != null)
                {
                    return "the user name has been used before. Please choose a new value";
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Look for an error in the password field.
        /// </summary>
        /// <returns></returns>
        private string getPasswordError(string strPassword)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(strPassword))
            {
                return null;
            }

            // Check the password length
            var minPasswordLength = System.Web.Security.Membership.MinRequiredPasswordLength;
            if (minPasswordLength > 0)
            {
                if (strPassword.Length < minPasswordLength)
                {
                    return string.Format("passwords must be at least {0:f0} characters", minPasswordLength);
                }
            }

            // Check the password non-alphanumeric characters
            var reqNonAlphaNumerics = System.Web.Security.Membership.MinRequiredNonAlphanumericCharacters;
            if (reqNonAlphaNumerics > 0)
            {
                var w = nonAlaphaNumeric(strPassword);
                if (w < reqNonAlphaNumerics)
                {
                    return string.Format("passwords require at least {0:f0} non-alphanumeric characters", reqNonAlphaNumerics);
                }
            }

            // Validate the input string against the regular expression
            var regExpr = System.Web.Security.Membership.PasswordStrengthRegularExpression;
            if (!string.IsNullOrEmpty(regExpr))
            {
                try
                {
                    var regex = new System.Text.RegularExpressions.Regex(regExpr);
                    if (regex != null && !regex.IsMatch(strPassword))
                    {
                        return "the password does not match the site's requirements.";
                    }
                }
                catch { }
            }

            // The password matches the sites requirements or the expression is not a valid expression.
            return null;
        }

        /// <summary>
        /// Look for an error in the password2 field.
        /// </summary>
        /// <returns></returns>
        private string getPassword2Error(string strPassword1, string strPassword2)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(strPassword2))
            {
                return null;
            }

            if (string.Compare(strPassword1.Trim(), strPassword2.Trim(), true) != 0)
            {
                return "the two passwords must match";
            }
            return null;
        }

        /// <summary>
        /// Find an problem with the ZIPCode field.
        /// </summary>
        /// <returns></returns>
        private string getRegexError(string inputString, string expressionString, string ErrorMessage)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(inputString))
            {
                return null;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(inputString.Trim(), expressionString))
            {
                return ErrorMessage;
            }
            return null;
        }

        /// <summary>
        /// Look for errors on the website address. It must be a valid URI to be acceptable and use
        /// the scheme for HTTP or HTTPS.
        /// </summary>
        private string getFirmWebsiteError(string webiteAddress)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(webiteAddress))
            {
                return null;
            }

            try
            {
                // Try to parse the website address. Look for http or https as the scheme.
                var urlCode = new Uri(webiteAddress);
                if (urlCode.IsAbsoluteUri && !urlCode.IsLoopback)
                {
                    if (urlCode.Scheme == Uri.UriSchemeHttp || urlCode.Scheme == Uri.UriSchemeHttps)
                    {
                        return null;
                    }
                }
            }
            catch { }

            return "the website is not a valid address";
        }

        /// <summary>
        /// Look at the Email address to ensure that it will be possibly acceptable to an email server. This does
        /// not guarantee that the email address has a valid recipient, only that it is formatted properly for
        /// a valid email address.
        /// </summary>
        private string getEmailError(string emailAddress, string key)
        {
            // Always ignore blank strings at this point
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                return null;
            }

            // Find the email validation class logic and check it.
            var cls = new ClearPoint.IsEMail();
            var answer = cls.IsEmailValid(emailAddress);
            if (answer && (cls.ResultInfo == null || cls.ResultInfo.Count == 0))
            {
                return null;
            }

            return string.Format("the {0} email address is not a valid email address", key);
        }

        /// <summary>
        /// Get the error status from the CHAPCHA challenge
        /// </summary>
        /// <returns></returns>
        private string getChapchaError()
        {
            // The captcha control can not be validated so it must be done separately from the others
            if ((!ClearPoint.Settings.DisableCapcha) && (!ctrlGoogleReCaptcha.Validate()))
            {
                return "please check the validation sequence at the bottom of the page";
            }

            return null;
        }

        /// <summary>
        /// Validate an email address against the valid patterns for a legal value.
        /// </summary>
        /// <summary>
        /// Count the number of non-alphanumeric characters in the input string.
        /// </summary>
        /// <param name="input">string to be tested</param>
        /// <returns></returns>
        private int nonAlaphaNumeric(string input)
        {
            int count = 0;
            foreach (char c in input)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    ++count;
                }
            }
            return count;
        }

        /// <summary>
        /// Look for a missing or empty string. Return the error associated with the field name.
        /// </summary>
        private string getMissingField(string inputString, string key)
        {
            return getMissingField(inputString, key, string.Empty);
        }

        /// <summary>
        /// Look for a missing field. The original value is the default setting and is used for dropdown lists.
        /// </summary>
        private string getMissingField(string inputString, string key, string originalValue)
        {
            if (string.IsNullOrWhiteSpace(inputString) || inputString == originalValue)
            {
                return string.Format("the {0} is missing", key);
            }
            return null;
        }

        /// <summary>
        /// Handle the click event on the submit button. Here is where we do the real work.
        /// </summary>
        protected void showErrorFrame(string ErrorText)
        {
            ValidationSummary.Text  = ErrorText;
            completionPanel.Visible = false;
            InputPanel.Visible      = true;
            ErrorPanel.Visible      = true;
            NoticePannel.Visible    = false;
        }

        /// <summary>
        /// Handle the click event on the submit button. Here is where we do the real work.
        /// </summary>
        protected void Submit_Click(object sender, EventArgs e)
        {
            // If the page is not valid then return with the error conditions already set.
            if (!ValidatePage())
            {
                return;
            }

            System.Data.SqlClient.SqlTransaction txn = null;

            // Allocate a new connection. We need to use one with a transaction. The transaction must be set early so do it now.
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;
            var cn = new System.Data.SqlClient.SqlConnection(connectionString);

            try
            {
                cn.Open();
                using (txn = cn.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    using (var bc = new ClearPoint.DebtPlus.DatabaseContext(txn))
                    {
                        // Create the new vendor record.
                        vendorRecord = writeVendor(bc);
                        if (vendorRecord != null)
                        {
                            txn.Commit();

                            // At the end of the processing, switch to the completion page.
                            completionPanel.Visible = true;
                            InputPanel.Visible      = false;
                            ErrorPanel.Visible      = false;
                            NoticePannel.Visible    = false;

                            // Log the user on the system so that the main page works correctly without having to signon again.
                            LogonUser(username.Text.Trim(), vendorRecord, true);

                            // Send the owner the message that we have created an account
                            NotifyOwner(vendorRecord);
                            return;
                        }
                    }
                }
            }

            catch(System.Exception ex)
            {
            }

            // Catch errors during the creation process
            showErrorFrame(Server.HtmlEncode("We were unable to create the attorney record in our database due to an error. Please try the operation again a bit later or call us at the number listed below for help."));
        }

        /// <summary>
        /// Send the message to the owner that a new account was created
        /// </summary>
        private void NotifyOwner(ClearPoint.DebtPlus.vendor vendorRecord)
        {
            // Find the configuration information in the web.config file
            var cnf = System.Configuration.ConfigurationManager.GetSection("EmailMessageConfigurationSection") as ClearPoint.Configuration.EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var onCreate = cnf.MailDefinitions.Where(s => string.Compare(s.Event, "OnCreate", true) == 0).FirstOrDefault();
            if (onCreate == null || string.IsNullOrWhiteSpace(onCreate.To))
            {
                return;
            }

            // Create the mail definition object
            var mailDefinition = onCreate.CreateMailDefinition();

            // Find the destination address. it must be present to be notified
            // Construct the dictionary with the new fields
            var dict = new System.Collections.Specialized.ListDictionary();
            dict.Add("<%UserName%>", username.Text ?? string.Empty);
            dict.Add("<%Password%>", password.Text ?? string.Empty);
            dict.Add("<%FirmName%>", firm_name.Text ?? string.Empty);
            dict.Add("<%FirmAddr1%>", firm_address.Text ?? string.Empty);
            dict.Add("<%FirmAddr2%>", firm_address2.Text ?? string.Empty);
            dict.Add("<%FirmCity%>", firm_city.Text ?? string.Empty);
            dict.Add("<%FirmState%>", firm_state.Text ?? string.Empty);
            dict.Add("<%FirmZip%>", firm_zip.Text ?? string.Empty);
            dict.Add("<%FirmWebsite%>", firm_webite.Text ?? string.Empty);
            dict.Add("<%Name1First%>", name1_first.Text ?? string.Empty);
            dict.Add("<%Name1Last%>", name1_last.Text ?? string.Empty);
            dict.Add("<%Name1Email%>", email1.Text ?? string.Empty);
            dict.Add("<%Name2First%>", name2_first.Text ?? string.Empty);
            dict.Add("<%Name2Last%>", name2_last.Text ?? string.Empty);
            dict.Add("<%Name2Email%>", email2.Text ?? string.Empty);
            dict.Add("<%Telephone%>", telephone.Text ?? string.Empty);
            dict.Add("<%FAX%>", fax.Text ?? string.Empty);
            dict.Add("<%ClientCount%>", annual_clients.Text ?? string.Empty);
            dict.Add("<%AttorneyCount%>", attorneys.Text ?? string.Empty);
            dict.Add("<%ReferredBy%>", referral.Text ?? string.Empty);
            dict.Add("<%Comments%>", comments.Text ?? string.Empty);
            dict.Add("<%EscrowPreFiling%>", pre_yes.Checked ? "Yes" : "No");
            dict.Add("<%EscrowPreDischarge%>", post_yes.Checked ? "Yes" : "No");
            dict.Add("<%VendorLabel%>", vendorRecord.Label ?? string.Empty);
            dict.Add("<%VendorID%>", vendorRecord.Id.ToString());

            // Send the email notice to the owner
            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                using (var msg = mailDefinition.CreateMailMessage(onCreate.To, dict, this))
                {
                    smtp.Send(msg);
                }
            }
        }

        /// <summary>
        /// Perform the logon sequence without needing a username/password.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="vendorRecord"></param>
        /// <param name="isPersistent"></param>
        private void LogonUser(string username, ClearPoint.DebtPlus.vendor vendorRecord, bool isPersistent)
        {
            // sometimes used to persist user roles
            string userData = string.Empty; // string.Join("|",GetCustomUserRoles());

            System.Web.Security.FormsAuthenticationTicket ticket = new System.Web.Security.FormsAuthenticationTicket(
                    1,                                                          // ticket version
                    username,                                                   // authenticated username
                    DateTime.Now,                                               // issueDate
                    DateTime.Now.AddMinutes(30),                                // expiryDate
                    isPersistent,                                               // true to persist across browser sessions
                    userData,                                                   // can be used to store additional user data
                    System.Web.Security.FormsAuthentication.FormsCookiePath);   // the path for the cookie

            // Encrypt the ticket using the machine key
            string encryptedTicket = System.Web.Security.FormsAuthentication.Encrypt(ticket);

            // Add the cookie to the request to save it
            HttpCookie cookie = new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            Response.Cookies.Add(cookie);

            // Add the session information for the vendor so that we can find it again
            Session["atty_name"] = Server.HtmlEncode(vendorRecord.Name ?? string.Empty);
            Session["firm_code"] = vendorRecord.Id.ToString("0000");
        }

        /// <summary>
        /// Create the vendor reference
        /// </summary>
        private DebtPlus.vendor writeVendor(ClearPoint.DebtPlus.DatabaseContext bc)
        {
            // Create the vendor record.
            var vendorRecord = createVendorRecord(bc, firm_name.Text.Trim(), firm_webite.Text.Trim(), BillingMethod.SelectedValue, CreditCardAccountNumber.Text.Trim(), ACHRouting.Text.Trim(), ACHType.SelectedValue[0], CreditCardAccountName.Text.Trim(), CreditCardCVV.Text.Trim(), getExpirationDate(), getVendorXML());
            if (vendorRecord == null)
            {
                return null;
            }

            // Write the vendor components
            writeAddresses(bc, vendorRecord.Id);
            writeContacts(bc, vendorRecord.Id);
            writeProducts(bc, vendorRecord.Id);
            writeWebAcccess(bc, vendorRecord.Id);
            writeNotes(bc, vendorRecord);

            // Return the current vendor record
            return vendorRecord;
        }

        /// <summary>
        /// Write all of the addresses to the database for this vendor
        /// </summary>
        private void writeAddresses(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID)
        {
            var addressRecord = createAddress(bc, firm_address.Text.Trim(), firm_address2.Text.Trim(), firm_city.Text.Trim(), Int32.Parse(firm_state.Text.Trim()), firm_zip.Text.Trim());
            if (addressRecord != null)
            {
                createVendorAddress(bc, VendorID, addressRecord.Id, AttorneyPortal.s.ContactInfo.vendorAddressType, firm_name.Text.Trim(), string.Empty);
            }
        }

        /// <summary>
        /// Write all of the contacts for this vendor to the database
        /// </summary>
        private void writeContacts(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID)
        {
            var MainRecord = writeSingleContact(bc, VendorID, "Main", name1_first.Text.Trim(), name1_last.Text.Trim(), firm_address.Text.Trim(), firm_address2.Text.Trim(), firm_city.Text.Trim(), Int32.Parse(firm_state.Text.Trim()), firm_zip.Text.Trim(), telephone.Text.Trim(), fax.Text.Trim(), email1.Text.Trim());

            // If there is an alternate contact then use that name for the contact data.
            if (! string.IsNullOrWhiteSpace(name2_first.Text) || ! string.IsNullOrWhiteSpace(name2_last.Text))
            {
                var InvoiceRecord = writeSingleContact(bc, VendorID, "Invoicing", name2_first.Text.Trim(), name2_last.Text.Trim(), firm_address.Text.Trim(), firm_address2.Text.Trim(), firm_city.Text.Trim(), Int32.Parse(firm_state.Text.Trim()), firm_zip.Text.Trim(), telephone.Text.Trim(), fax.Text.Trim(), email2.Text.Trim());
                if (InvoiceRecord != null)
                {
                    return;
                }
            }

            // Use the primary contact for the invoice operation
            writeSingleContact(bc, VendorID, "Invoicing", name1_first.Text.Trim(), name1_last.Text.Trim(), firm_address.Text.Trim(), firm_address2.Text.Trim(), firm_city.Text.Trim(), Int32.Parse(firm_state.Text.Trim()), firm_zip.Text.Trim(), telephone.Text.Trim(), fax.Text.Trim(), email1.Text.Trim());
        }

        /// <summary>
        /// Write all of the product references for this vendor to the database
        /// </summary>
        private void writeProducts(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID)
        {
            var prefilingBankruptcy = writeSingleProduct(bc, VendorID, "BK Pre-File Internet", pre_yes.Checked ? 1 : pre_free.Checked ? 2 : 0);
            var predischargeBanktupcy = writeSingleProduct(bc, VendorID, "BK Pre-Discharge Internet", post_yes.Checked ? 1 : post_free.Checked ? 2 : 0);
        }

        /// <summary>
        /// Create a Web address block for this vendor in the database.
        /// </summary>
        private void writeWebAcccess(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID)
        {
            createWebAccess(bc, VendorID, username.Text.Trim(), password.Text.Trim(), email1.Text.Trim());
        }

        /// <summary>
        /// Create a vendor Contact record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor_contact writeSingleContact(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, string ContactType, string First, string Last, string Line1, string Line2, string City, Int32 State, string Zip, string TelephoneNumber, string FaxNumber, string EmailAddress)
        {
            var contactTypeID = getContactType(bc, ContactType);
            if (contactTypeID == null)
            {
                return null;
            }

            DebtPlus.name            nameID      = createName(bc, First, Last);
            DebtPlus.address         addressID   = createAddress(bc, Line1, Line2, City, State, Zip);
            DebtPlus.EmailAddress    emailID     = createEmailAddress(bc, EmailAddress);
            DebtPlus.TelephoneNumber telephoneID = createTelephone(bc, TelephoneNumber);
            DebtPlus.TelephoneNumber faxID       = createTelephone(bc, FaxNumber);

            var record = createVendorContract(bc, 
                        VendorID,
                        contactTypeID.Id,
                        nameID      == null ? (Int32?)null : nameID.Id,
                        addressID   == null ? (Int32?)null : addressID.Id,
                        emailID     == null ? (Int32?)null : emailID.Id,
                        telephoneID == null ? (Int32?)null : telephoneID.Id,
                        faxID       == null ? (Int32?)null : faxID.Id,
                        string.Empty,
                        string.Empty);

            return record;
        }

        /// <summary>
        /// Create a vendor product record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor_product writeSingleProduct(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, string ProductType, Int32 EscrowProBono)
        {
            // Find the product type reference
            var productTypeID = getProduct(bc, ProductType);
            if (productTypeID == null)
            {
                return null;
            }

            // Create the linkage from the vendor to the product
            var record = createVendorProduct(bc, VendorID, productTypeID.Id, EscrowProBono);
            return record;
        }

        /// <summary>
        /// Write all of the notes to the database for this vendor
        /// </summary>
        private void writeNotes(ClearPoint.DebtPlus.DatabaseContext bc, ClearPoint.DebtPlus.vendor VendorRecord)
        {
            var note         = new ClearPoint.DebtPlus.vendor_note();
            note.vendor      = VendorRecord.Id;
            note.subject     = "Additional fields from creation form";
            note.dont_delete = true;
            note.dont_edit   = true;
            note.dont_print  = true;
            note.type        = 1;      // Permanent note

            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("clients expected to refer to agency per year: {0:n0}\r\n", Int32.Parse(annual_clients.Text.Trim()));
            sb.AppendFormat("attorneys using system: {0:n0}\r\n", Int32.Parse(attorneys.Text.Trim()));
            sb.AppendFormat("referral note: {0}\r\n", referral.Text.Trim());
            sb.AppendFormat("\r\nnote comment block:\r\n{0}\r\n", referral.Text.Trim());

            note.note = sb.ToString();

            // Insert the new vendor note
            bc.vendor_noteTable.InsertOnSubmit(note);
            bc.SubmitChanges();
        }

        /// <summary>
        /// Create a Vendor record in the database and return the pointer to the record to the caller.
        /// </summary>
        private DebtPlus.vendor createVendorRecord(ClearPoint.DebtPlus.DatabaseContext bc, string FirmName, string WebSite, string BillingMode, string ACH_AccountNumber, string ACH_Routing, char ACH_CheckingSavings, string CC_NameOnCard, string CC_CVV, DateTime? CC_ExpirationDate, string AdditionalFields)
        {
            // Allocate the vendor record
            var record = new DebtPlus.vendor()
            {
                Name                    = FirmName,
                MonthlyBillingDay       = 10,
                Type                    = "A",
                Website                 = WebSite,
                SupressInvoices         = false,
                SupressPayments         = false,
                SupressPrintingPayments = false,
                BillingMode             = BillingMode,
                ACH_AccountNumber       = ACH_AccountNumber,
                ACH_CheckingSavings     = ACH_CheckingSavings,
                ACH_RoutingNumber       = ACH_Routing,
                ActiveFlag              = true,
                CC_ExpirationDate       = CC_ExpirationDate,
                CC_CVV                  = CC_CVV,
                NameOnCard              = CC_NameOnCard
            };

            // Do not record an empty record
            if (string.IsNullOrWhiteSpace(record.Name))
            {
                return null;
            }

            // If there are additional fields then add them to the vendor record
            if (!string.IsNullOrEmpty(AdditionalFields))
            {
                record.Additional = System.Xml.Linq.XElement.Parse(AdditionalFields);
            }

            // Insert the record and return the new ID for the record
            bc.vendorTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Name record in the database and return the pointer to the caller.
        /// </summary>
        private DebtPlus.name createName(ClearPoint.DebtPlus.DatabaseContext bc, string First, string Last)
        {
            // Create the new Name record
            var record = new DebtPlus.name()
            {
                First  = First,
                Last   = Last,
                Middle = string.Empty,
                Prefix = string.Empty,
                Suffix = string.Empty
            };

            // Split the middle name from the first if possible
            var ipos = record.First.LastIndexOf(' ');
            if (ipos > 0)
            {
                record.Middle = record.First.Substring(ipos).Trim();
                record.First = record.First.Substring(0, ipos).Trim();
            }

            // Look for a non-null string. If found, record the record.
            foreach (var str in new string[] { record.First, record.Last, record.Middle, record.Prefix, record.Suffix })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    bc.nameTable.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    return record;
                }
            }

            // Do not record an empty record
            return null;
        }

        /// <summary>
        /// Create an address record in the database and return the pointer to the new record to the caller.
        /// </summary>
        private DebtPlus.address createAddress(ClearPoint.DebtPlus.DatabaseContext bc, string Line1, string Line2, string City, Int32 State, string ZipCode)
        {
            // Try to split the first and second line of the address block into component pieces
            var sb = new System.Text.StringBuilder();
            sb.AppendLine(Line1);
            sb.AppendLine(Line2);
            sb.AppendFormat("{0} {1} {2}", City, "CA", ZipCode);

            // Run the merged address block through the address parser to split things according to the standard patterns.
            AddressParser.AddressParser addr   = new AddressParser.AddressParser();
            AddressParser.AddressParseResult x = addr.ParseAddress(sb.ToString());

            // Generate the address reference information.
            var record = new DebtPlus.address()
            {
                city              = x.City.ToUpper() ?? string.Empty,
                direction         = x.Predirectional ?? string.Empty,
                house             = x.Number ?? string.Empty,
                modifier          = x.SecondaryUnit ?? string.Empty,
                modifier_value    = x.SecondaryNumber ?? string.Empty,
                PostalCode        = formatPostalCode(ZipCode.Trim()).ToUpper() ?? string.Empty,
                state             = State,
                street            = x.Street.ToUpper() ?? string.Empty,
                suffix            = x.Suffix ?? string.Empty,
                address_line_2    = string.Empty,
                address_line_3    = string.Empty,
                creditor_prefix_1 = string.Empty,
                creditor_prefix_2 = string.Empty
            };
            
            // Some people use "#" to mean suite. Change it here.
            if (record.modifier == "#")
            {
                record.modifier = "STE";
            }

            // If there is a post direction then we must put it on the street
            if (!string.IsNullOrEmpty(x.Postdirectional))
            {
                record.street = record.street + " " + x.Postdirectional;
            }

            // Look for an empty address record. We don't record blanks.
            bool isEMpty = (State == 0);
            foreach(var str in new string[] { record.house, record.address_line_2, record.address_line_3, record.city, record.creditor_prefix_1, record.creditor_prefix_2, record.direction, record.modifier, record.modifier_value, record.PostalCode, record.street, record.suffix })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    isEMpty = false;
                    break;
                }
            }

            // If the address record is empty then return a null pointer
            if (isEMpty)
            {
                return null;
            }

            // Insert the new record and return its ID.
            bc.addressTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor contact record in the database and return the ID to the caller.
        /// </summary>
        private DebtPlus.vendor_contact createVendorContract(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, Int32 ContactType, Int32? NameID, Int32? AddressID, Int32? EmailID, Int32? TelephoneID, Int32? FaxID, string Note, string Title)
        {
            // Create the new record buffer.
            var record = new DebtPlus.vendor_contact()
            {
                vendor       = VendorID,
                contact_type = ContactType,
                addressID    = AddressID,
                emailID      = EmailID,
                faxID        = FaxID,
                nameID       = NameID,
                note         = Note,
                telephoneID  = TelephoneID,
                Title        = Title
            };

            bool isEmpty = true;
            foreach (var item in new Int32?[] { record.addressID, record.emailID, record.faxID, record.nameID, record.telephoneID })
            {
                if (item.HasValue)
                {
                    isEmpty = false;
                    break;
                }
            }

            foreach (var str in new string[] { record.note, record.Title })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    isEmpty = false;
                    break;
                }
            }

            // If the record is empty then do not record it.
            if (isEmpty)
            {
                return null;
            }

            // Write the new record to the database and return its id.
            bc.vendor_contactTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor product record in the database and return the ID to the caller.
        /// </summary>
        private DebtPlus.vendor_product createVendorProduct(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, Int32? ProductType, Int32 EscrowProBono)
        {
            // Ignore blank references
            if (!ProductType.HasValue)
            {
                return null;
            }

            // Create the new record buffer.
            var record = new DebtPlus.vendor_product()
            {
                vendor        = VendorID,
                product       = ProductType.Value,
                EscrowProBono = EscrowProBono
            };

            // Write the new record to the database and return its id.
            bc.vendor_productTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a Vendor_Address record in the database. This is used to keep track of the vedor address
        /// blocks.
        /// </summary>
        private DebtPlus.vendor_address createVendorAddress(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, Int32? AddressID, char AddressType, string Line1, string Attn)
        {
            var record = new DebtPlus.vendor_address()
            {
                address_type = AddressType,
                addressID    = AddressID,
                vendor       = VendorID,
                attn         = Attn,
                line_1       = Line1,
                line_2       = string.Empty
            };

            if (AddressID.HasValue)
            {
                bc.vendor_addressTable.InsertOnSubmit(record);
                bc.SubmitChanges();
                return record;
            }

            return null;
        }

        /// <summary>
        /// Create an EmailAddress record in the database and return the ID to the caller. It is used in creating
        /// the contact information records.
        /// </summary>
        private DebtPlus.EmailAddress createEmailAddress(ClearPoint.DebtPlus.DatabaseContext bc, string Address)
        {
            const Int32 EmailAddressVaid = 1;

            // Create the new address record
            var record = new DebtPlus.EmailAddress()
            {
                Address    = Address,
                Validation = EmailAddressVaid
            };

            // If the address is blank then return an empty pointer.
            if (string.IsNullOrWhiteSpace(record.Address))
            {
                return null;
            }

            // Insert the new record and return its ID
            bc.EmailAddressTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Create a TelephoneNumber record in the database and return the ID to the caller. It is used in creating
        /// the contact information records.
        /// </summary>
        private DebtPlus.TelephoneNumber createTelephone(ClearPoint.DebtPlus.DatabaseContext bc, string Number)
        {
            const Int32 Country = 1; // USA

            // A null number is a null number
            if (Number == null)
            {
                return null;
            }

            // Remove the non-digits from the telephone
            Number = System.Text.RegularExpressions.Regex.Replace(Number, @"\D", "");

            // If there is nothing left then there is no telephone number
            if (string.IsNullOrWhiteSpace(Number))
            {
                return null;
            }

            // If there is an area code then return the areacode value
            string phoneAcode = default(string);
            string phoneNumber = default(string);
            if (Number.Length >= 10)
            {
                phoneAcode  = Number.Substring(0, 3);
                phoneNumber = Number.Substring(3);

                // Put in he formatting dash if needed
                if (phoneNumber.Length == 7)
                {
                    phoneNumber = phoneNumber.Substring(0, 3) + "-" + phoneNumber.Substring(3);
                }
            }
            else
            {
                phoneAcode  = string.Empty;
                phoneNumber = Number;
            }

            // Create the new record
            var record = new DebtPlus.TelephoneNumber()
            {
                Acode   = phoneAcode,
                Country = Country,
                Ext     = string.Empty,
                Number  = phoneNumber
            };

            // Look for an empty record
            foreach (var str in new string[] { record.Acode, record.Number })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    bc.TelephoneNumberTable.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    return record;
                }
            }

            return null;                     
        }

        /// <summary>
        /// Create a client_www record in the database and return the ID to the caller. This will grant the
        /// vendor web access to the account.
        /// </summary>
        private DebtPlus.client_www createWebAccess(ClearPoint.DebtPlus.DatabaseContext bc, Int32 VendorID, string UserName, string Password, string EmailAddress)
        {
            // An empty username is an empty record
            if (string.IsNullOrEmpty(UserName))
            {
                return null;
            }

            // Allocate a new web record
            var record = new DebtPlus.client_www()
            {
                Id                                     = System.Guid.NewGuid(),
                ApplicationName                        = System.Web.Security.Membership.ApplicationName,
                CreationDate                           = System.DateTime.UtcNow,
                Email                                  = EmailAddress,
                DatabaseKeyID                          = VendorID,
                FailedPasswordAnswerAttemptCount       = 0,
                FailedPasswordAttemptCount             = 0,
                IsApproved                             = true,
                IsLockedOut                            = false,
                FailedPasswordAnswerAttemptWindowStart = System.DateTime.UtcNow,
                FailedPasswordAttemptWindowStart       = System.DateTime.UtcNow,
                LastActivityDate                       = System.DateTime.UtcNow,
                LastLockoutDate                        = System.DateTime.UtcNow,
                LastLoginDate                          = System.DateTime.UtcNow,
                LastPasswordChangeDate                 = System.DateTime.UtcNow,
                PasswordAnswer                         = null,
                PasswordQuestion                       = null,
                UserName                               = UserName,
                Password                               = Providers.Membership.GetMD5Hash(Password)
            };

            // Create the web access
            bc.client_wwwTable.InsertOnSubmit(record);
            bc.SubmitChanges();
            return record;
        }

        /// <summary>
        /// Search for a contact Description based upon the description. The descriptions are fixed but the
        /// contact record numbers are not.
        /// </summary>
        private DebtPlus.vendor_contact_type getContactType(ClearPoint.DebtPlus.DatabaseContext bc, string Description)
        {
            var q = bc.vendor_contact_typeTable.Where(s => s.description == Description).FirstOrDefault();
            if (q != null)
            {
                return q;
            }
            return null;
        }

        /// <summary>
        /// Search for a product Description based upon the description. The descriptions are fixed but the
        /// product record numbers are not.
        /// </summary>
        private DebtPlus.product getProduct(ClearPoint.DebtPlus.DatabaseContext bc, string Description)
        {
            var q = bc.productTable.Where(s => s.description == Description).FirstOrDefault();
            if (q != null)
            {
                return q;
            }
            return null;
        }

        /// <summary>
        /// Format the postal code address field for suitable US formatted zipcodes. This will
        /// remove all formatting supplied by the user and insert the proper format for the database.
        /// </summary>
        private string formatPostalCode(string ZipCode)
        {
            // If there is no ZipCode then there is no ZipCode
            if (string.IsNullOrEmpty(ZipCode))
            {
                return string.Empty;
            }

            // If the ZipCode is nine digits then add the dash where appropriate
            var str = System.Text.RegularExpressions.Regex.Replace(ZipCode, @"\D", "");
            if (str.Length == 9)
            {
                return str.Substring(0, 5) + "-" + str.Substring(5);
            }

            // Otherwise just return the five digits
            return str;
        }

        /// <summary>
        /// Return the XML document for the miscellaneous fields that are requested and not kept.
        /// </summary>
        /// <returns></returns>
        private string getVendorXML()
        {
            // Allocate the class to hold the "noise" items that we don't track
            var cls = new NoteClass()
            {
                AttorneyCount = Int32.Parse(attorneys.Text.Trim()),
                ClientCount = Int32.Parse(annual_clients.Text.Trim()),
                Referral = referral.Text.Trim(),
                Comments = comments.Text.Trim()
            };

            // Serialize the class to a XML document.
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var xml = new System.Xml.Serialization.XmlSerializer(cls.GetType());
                xml.Serialize(fs, cls);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }
    }
}