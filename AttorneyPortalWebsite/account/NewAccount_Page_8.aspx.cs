﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_8 : System.Web.UI.Page
    {
        // Pointer to the current structure for the form controls
        Signup_Page8 params8 = null;

        /// <summary>
        /// Initialize the new page
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link8.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link8.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params8 = Session["Signup_Page8"] as Signup_Page8;
                if (params8 == null)
                {
                    params8 = new Signup_Page8();
                    params8.SameAddress = true;
                }

                // Copy the account address if the flag says that the value is to be copied.
                if (params8.SameAddress)
                {
                    // Copy the address from the firm value if we have something
                    var params4 = Session["Signup_Page4"] as Signup_Page4;
                    if (params4 != null)
                    {
                        params8.Invoice.Copy(params4.Address);
                    }
                }

                // On the initial load, set the entries according to the parameters
                if (!Page.IsPostBack)
                {
                    // Load the list of state names
                    using (var db = new ClearPoint.DebtPlus.DatabaseContext())
                    {
                        invoice_state.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, params8.Invoice.State.GetValueOrDefault(61)).ToArray());
                    }

                    // Set the values from the input record.
                    invoice_address_1.Text       = params8.Invoice.Line1();
                    invoice_address_2.Text       = string.Empty;
                    invoice_city.Text            = params8.Invoice.City;
                    invoice_zip.Text             = params8.Invoice.Zip;

                    same_as_firm.Checked         = params8.SameAddress;

                    ACHAccountNumber.Text        = params8.AccountNumber;
                    ACHRouting.Text              = params8.RoutingNumber;
                    ACHType.SelectedIndex        = params8.CheckingSavings == 'S' ? 1 : 0;

                    CreditCardAccountName.Text   = params8.AccountName;
                    CreditCardAccountNumber.Text = params8.AccountNumber;
                    CreditCardCVV.Text           = params8.CVV;

                    // Billing mode is a bit more complicated
                    var qMethod = BillingMethod.Items.OfType<System.Web.UI.WebControls.ListItem>().Where(s => s.Value == (params8.BillingMode ?? string.Empty)).FirstOrDefault();
                    if (qMethod != null)
                    {
                        qMethod.Selected = true;
                    }

                    // The expiration date is a bit more complicated as well.
                    Int32 defaultMonth = params8.ExpirationDate.Value.Month;
                    Int32 defaultYear  = params8.ExpirationDate.Value.Year;

                    // Select the month
                    var strMonth = defaultMonth.ToString("00");
                    var qMonth = CreditCardExpirationMonth.Items.OfType<System.Web.UI.WebControls.ListItem>().Where(s => s.Value == strMonth).FirstOrDefault();
                    if (qMonth != null)
                    {
                        qMonth.Selected = true;
                    }

                    // Add the list of years for the credit card expiration and set the default at the same time.
                    CreditCardExpirationYear.Items.AddRange(Enumerable.Range(DateTime.Now.Year, 20).Select(s => new System.Web.UI.WebControls.ListItem(s.ToString(), s.ToString(), true) { Selected = (s == defaultYear) }).ToArray());
                    if (CreditCardExpirationYear.SelectedItem == null)
                    {
                        CreditCardExpirationYear.SelectedIndex = 1;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Decode the first name
            var sb = new System.Text.StringBuilder();

            // This statement can not generate an error. It just saves the status of the checkbox.
            params8.SameAddress = same_as_firm.Checked;

            // Decode the address information if the address is different from the firm's
            if (!params8.SameAddress)
            {
                try
                {
                    var adr = new System.Text.StringBuilder();
                    var str = invoice_address_1.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = invoice_address_2.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = invoice_city.Text + " VA " + invoice_zip.Text;
                    adr.AppendFormat("\r\n{0}", str);
                    adr.Remove(0, 2);

                    params8.Invoice.Normalize(adr.ToString());
                    params8.Invoice.State = Int32.Parse(invoice_state.SelectedValue);
                }
                catch (Exception ex)
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
                }
            }

            params8.BillingMode = BillingMethod.SelectedValue ?? string.Empty;
            switch (params8.BillingMode)
            {
                case "CreditCard":
                    params8.AccountName    = CreditCardAccountName.Text.Trim();
                    params8.AccountNumber  = CreditCardAccountNumber.Text.Trim();
                    params8.CVV            = CreditCardCVV.Text.Trim();

                    Int32 selectedMonth    = Int32.Parse(CreditCardExpirationMonth.SelectedValue ?? "0");
                    Int32 selectedYear     = Int32.Parse(CreditCardExpirationYear.SelectedValue ?? "1900");
                    params8.ExpirationDate = new System.DateTime(selectedYear, selectedMonth, 1).AddMonths(1).AddDays(-1);
                    break;

                case "ACH":
                    params8.AccountName     = CreditCardAccountName.Text.Trim();
                    params8.AccountNumber   = ACHAccountNumber.Text.Trim();
                    params8.RoutingNumber   = ACHRouting.Text.Trim();
                    params8.CheckingSavings = ((ACHType.SelectedValue ?? string.Empty) + "C")[0];
                    break;

                case "None":
                    break;

                default:
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode("A billing type must be specified"));
                    break;
            }

            // Finally validate the structure if there are no parsing errors. Skip this if there are errors
            // since we don't need the errors to cascade.
            if (sb.Length == 0)
            {
                try { params8.Validate(); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference and transfer to the next page.
            Session["Signup_Page8"] = params8;
            Response.Redirect("NewAccount_Page_9.aspx", true);
        }
    }
}