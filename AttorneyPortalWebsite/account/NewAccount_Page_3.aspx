﻿<%@ Page Title="Account Information" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_3.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_3" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

    <h1>Account Information</h1>

    <table width="100%" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="username_label" AssociatedControlID="username" Font-Bold="true" Text="User Name" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="username" MaxLength="20" ToolTip="User name for this website" Width="150px" TextMode="SingleLine" CssClass="TxtBox" onKeyUP="this.value = this.value.toUpperCase();" CausesValidation="true" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="password_label" AssociatedControlID="password" Font-Bold="true" Text="Password" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="password" ToolTip="Password for this website" Width="150px" TextMode="Password" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="password2_label" AssociatedControlID="password2" Font-Bold="true" Text="Re-Enter Password" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="password2" ToolTip="Password for this website" Width="150px" TextMode="Password" CssClass="TxtBox" />
                </td>
            </tr>
        </tbody>
    </table>

    <div style="text-align:center; height:200px; vertical-align:bottom;">
        <p>&nbsp;</p><asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
    </div>
</asp:Content>
