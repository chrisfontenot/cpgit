﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using AttorneyPortal.Providers;
using ClearPoint;

namespace AttorneyPortal.account
{
    public partial class ForgottenPassword : System.Web.UI.Page
    {
        // Database context information
        DebtPlus.DatabaseContext db = null;

        // Cookie name for the username.
        const string cookieName = "usernameCookie";

        /// <summary>
        /// Handle the page Initialization to create a new database context.
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            db = new DebtPlus.DatabaseContext();
        }

        /// <summary>
        /// Handle the unload event for the form. Close the database connection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            if (db != null)
            {
                db.Dispose();
                db = null;
            }
        }

        /// <summary>
        /// Process the LOAD sequence for the page
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            PWRecovery.VerifyingUser += new System.Web.UI.WebControls.LoginCancelEventHandler(PWRecovery_VerifyingUser);
            PWRecovery.SendingMail   += new MailMessageEventHandler(PWRecovery_SendingMail);
        }

        /// <summary>
        /// Handle the post-processing where the email and the account password are changed. This is
        /// basically the same logic as the control, with the difference that we send the email to
        /// an address other than what is registered on the account.
        /// </summary>
        protected void PWRecovery_SendingMail(object sender, MailMessageEventArgs e)
        {
            // Find the name of the account
            string accountName = string.Empty;
            var cookie = Request.Cookies["usernameCookie"];
            if (cookie != null && cookie.Value != null)
            {
                accountName = cookie.Value.ToString();
            }

            if (string.IsNullOrWhiteSpace(accountName))
            {
                accountName = "user";
            }

            // Create a new password for the user
            MembershipUser mu = System.Web.Security.Membership.GetUser(PWRecovery.UserName);
            string newPassword = mu.ResetPassword();

            // Locate the email address that was given to the input form.
            TextBox EmailAddressTB = ((TextBox)PWRecovery.UserNameTemplateContainer.FindControl("EmailAddressTB"));
            string emailAddress = EmailAddressTB.Text.Trim();
            if (string.IsNullOrEmpty(emailAddress))
            {
                return;
            }

            // Create the dictionary for message substitutions.
            var dict = new System.Collections.Generic.Dictionary<string, string>();
            dict.Add("<%Password%>", newPassword);
            dict.Add("<%UserName%>", mu.UserName);
            dict.Add("<%Name%>", accountName);
            dict.Add("<%OldPassword%>", "Forgotten Password");
            dict.Add("<%FirmCode%>", "0000");

            // Send the message to the user. All of the mail parameters come from the web.config file.
            using (var msg = PWRecovery.MailDefinition.CreateMailMessage(emailAddress, dict, this))
            {
                using (var mail = new System.Net.Mail.SmtpClient())
                {
                    mail.Send(msg);
                }
            }

            // Send the owner an email message if desired as well.
            NotifyOwner(dict, "OnPwChange");

            // Dispose of the dictionary as cleanly as we can for the time being.
            dict.Clear();
            dict = null;

            // Since we have done all of the work, cancel the control doing the same thing.
            e.Cancel = true;
        }

        /// <summary>
        /// Send the email message to the owner of the website if desired
        /// </summary>
        /// <param name="dict"></param>
        private void NotifyOwner(System.Collections.Generic.Dictionary<string, string> dict, string key)
        {
            // Find the configuration information in the web.config file
            var cnf = System.Configuration.ConfigurationManager.GetSection("EmailMessageConfigurationSection") as ClearPoint.Configuration.EmailMessageConfigurationSection;
            if (cnf == null)
            {
                return;
            }

            // Find the OnCreate item in the definitions.
            var OnPwChange = cnf.MailDefinitions.Where(s => string.Compare(s.Event, key, true) == 0).FirstOrDefault();
            if (OnPwChange == null || string.IsNullOrWhiteSpace(OnPwChange.To))
            {
                return;
            }

            // Send the email notice to the owner
            using (var smtp = new System.Net.Mail.SmtpClient())
            {
                var mailDefinition = OnPwChange.CreateMailDefinition();
                using (var msg = mailDefinition.CreateMailMessage(OnPwChange.To, dict, this))
                {
                    smtp.Send(msg);
                }
            }
        }

        /// <summary>
        /// Handle the verification of the account. We check the account name and email address at this point
        /// and do not bother with the question/answer sequence. If the email address that was entered matches
        /// one of the two that are associated with the account then we will go ahead and change the password.
        /// </summary>
        protected void PWRecovery_VerifyingUser(object sender, LoginCancelEventArgs e)
        {
            TextBox EmailAddressTB = ((TextBox)PWRecovery.UserNameTemplateContainer.FindControl("EmailAddressTB"));
            Literal ErrorLiteral = ((Literal)PWRecovery.UserNameTemplateContainer.FindControl("ErrorLiteral"));
            MembershipUser mu = System.Web.Security.Membership.GetUser(PWRecovery.UserName);

            if (mu == null) // The username not found
            {
                e.Cancel = true;
                ErrorLiteral.Text = "No such user found.";
                return;
            }

            // Find the appropriate entry in the client_www table
            var clientRecord = db.client_wwwTable.Where(s => s.UserName == mu.UserName && s.ApplicationName == System.Web.Security.Membership.ApplicationName).FirstOrDefault();
            if (clientRecord == null)
            {
                e.Cancel = true;
                ErrorLiteral.Text = "No such user found.";
                return;
            }

            // From that record, get the vendor record
            var vendorRecord = db.vendorTable.Where(s => s.Id == clientRecord.DatabaseKeyID).FirstOrDefault();
            if (vendorRecord == null)
            {
                e.Cancel = true;
                ErrorLiteral.Text = "No such user found.";
                return;
            }

            // Find the contact email addresses
            string enteredEmail = EmailAddressTB.Text.Trim();
            var vendorContactRecord = (from con in db.vendor_contactTable
                                  join email in db.EmailAddressTable on con.emailID equals email.Id
                                  where con.vendor == vendorRecord.Id
                                  && email.Address == enteredEmail
                                  select con).FirstOrDefault();

            if (vendorContactRecord == null)
            {
                e.Cancel = true;
                ErrorLiteral.Text = "The email address that you entered does not match your account.";
                return;
            }

            // Find the name associated with that user
            string namestring = vendorRecord.Name.Trim();
            if (vendorContactRecord.nameID.HasValue)
            {
                var nameRecord = db.nameTable.Where(s => s.Id == vendorContactRecord.nameID.Value).FirstOrDefault();
                if (nameRecord != null)
                {
                    namestring = nameRecord.ToString().Trim();
                }
            }

            // Set the cookie for the user name
            HttpCookie appCookie = new HttpCookie(cookieName);
            appCookie.Value = namestring;
            appCookie.Expires = DateTime.Now.AddMinutes(3);
            Response.Cookies.Add(appCookie);
        }
    }
}
