﻿<%@ Page Title="Billing Information" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_8.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_8" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="NewAccountHead" runat="server">

    <script language="javascript" type="text/javascript">
    $(document).ready(function () {
        ChangeBillingMode();
    });

    function ChangeBillingMode() {
        var selection = document.getElementById('<%=BillingMethod.ClientID %>');

        switch (selection.value) {
            case "None":
                setOwner("none");
                setCreditCard("none");
                setACH("none");
                break;

            case "CreditCard":
                setOwner("block");
                setCreditCard("block");
                setACH("none");
                break;

            case "ACH":
                setOwner("block");
                setCreditCard("none");
                setACH("block");
                break;
        }
    }

    function setOwner(displayMode) {
        var cce = document.getElementById("<%=Owner.ClientID %>");
        cce.style.display = displayMode;
    }

    function setCreditCard(displayMode) {
        var cce = document.getElementById("<%=CreditCard.ClientID %>");
        cce.style.display = displayMode;
    }

    function setACH(displayMode) {
        var cce = document.getElementById("<%=ACH.ClientID %>");
        cce.style.display = displayMode;
    }
</script>

<style type="text/css">
    h2
    {
        margin-top:30px;
        margin-bottom:20px;
        font-weight:bold;
        font-size:larger;
        color:Black;
        background-color:White;
    }
    
    p
    {
        margin-bottom:20px;
    }    
    
    ul
    {
        margin-left:30px;
    }
        
    table.escrow
    {
        padding: 0;
        width: 100%; 
        margin-left: 5%; 
        margin-right: 5%;
    }
    
    span.selectedItem
    {
        text-transform:uppercase;
    }
    
    .style5
    {
        margin: 10 10 10 10;
        border-style:none;
    }
    
    td.style1
    {
        width:800px;
        font-weight:bold;
        text-align:left;
        vertical-align:top;
        border-style:none;
    }
    
    td.style2
    {
        text-align:left;
        vertical-align:top;
        border-style:none;
    }
    
    tr
    {
        text-align:left;
        vertical-align:top;
        border-style:none;
    }
    
</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

    <asp:Table ID="Table3" Width="100%" runat="server" CellPadding="0" CellSpacing="0">
        <asp:TableRow ID="TableRow01" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell01" runat="server" ColumnSpan="2">
                <h1>Billing Address</h1>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow02" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell102" runat="server" CssClass="style2" ColumnSpan="2">
                <asp:CheckBox ID="same_as_firm" runat="server" Text="Same as firm's address" />
                <div>&nbsp;</div>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow03" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell03" runat="server" CssClass="style1">
                <asp:Label runat="server" ID="invoice_address_1_label" AssociatedControlID="invoice_address_1" Font-Bold="true" Text="Address" />
            </asp:TableCell><asp:TableCell ID="TableCell04" runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="invoice_address_1" MaxLength="50" ToolTip="Please enter your firm's mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow04" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell05" runat="server" CssClass="style1">
                <asp:Label runat="server" ID="invoice_address_2_label" AssociatedControlID="invoice_address_2" Font-Bold="true" Text="Address (cont.)" />
            </asp:TableCell><asp:TableCell ID="TableCell106" runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="invoice_address_2" MaxLength="50" ToolTip="Additional line if needed for mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow05" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell07" runat="server" CssClass="style1">
                <asp:Label runat="server" ID="invoice_city_label" AssociatedControlID="invoice_city" Font-Bold="true" Text="City" />
            </asp:TableCell><asp:TableCell ID="TableCell108" runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="invoice_city" MaxLength="50" ToolTip="Please enter your firm's city name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow06" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell09" runat="server" CssClass="style1">
                <asp:Label runat="server" ID="invoice_state_label" AssociatedControlID="invoice_state" Font-Bold="true" Text="State" />
            </asp:TableCell><asp:TableCell ID="TableCell110" runat="server" CssClass="style2">
                <asp:DropDownList runat="server" ID="invoice_state" ToolTip="In which state is your located?" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow07" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell11" runat="server" CssClass="style1">
                <asp:Label runat="server" ID="invoice_zip_label" AssociatedControlID="invoice_zip" Font-Bold="true" Text="ZIP" />
            </asp:TableCell><asp:TableCell ID="TableCell12" runat="server" CssClass="style2">
                <asp:TextBox runat="server" ID="invoice_zip" MaxLength="11" ToolTip="Please enter your postal ZIP code" Width="100px" TextMode="SingleLine" CssClass="TxtBox" onchange="javascript:clearAddress();" />
            </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow08" runat="server">
            <asp:TableCell ID="TableCell13" runat="server" ColumnSpan="2">
                <h1>Payment Method</h1>
            </asp:TableCell></asp:TableRow><asp:TableRow ID="PaymentMethoSelctionRow" runat="server" VerticalAlign="Top">
            <asp:TableCell ID="TableCell14" runat="server" CssClass="style1">
                <asp:Label ID="Label1" runat="server" AssociatedControlID="BillingMethod">Payment Method:</asp:Label>
            </asp:TableCell><asp:TableCell ID="TableCell15" runat="server" CssClass="style2">
                <asp:DropDownList ID="BillingMethod" runat="server" onchange="javascript:ChangeBillingMode();">
                    <asp:ListItem Value="None">None (pro bono or non-escrow)</asp:ListItem>
                    <asp:ListItem Value="CreditCard">Credit Card</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell ID="TableCell16" runat="server" ColumnSpan="2">
                <asp:Panel runat="server" ID="Owner" style="display:block">
                    <asp:Table ID="Table4" runat="server" Width="100%" CellPadding="0" CellSpacing="0" >
                        <asp:TableRow ID="CreditCard_1" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell17" runat="server" CssClass="style1">
                                <asp:Label ID="Label2" runat="server" AssociatedControlID="CreditCardAccountName">Account Name:</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell18" runat="server" CssClass="style2">
                                <asp:TextBox ID="CreditCardAccountName" runat="server" MaxLength="50" Width="300px" TextMode="SingleLine" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="CreditCard" style="display:block">
                    <asp:Table ID="Table1" runat="server" Width="100%" CellPadding="0" CellSpacing="0" >
                        <asp:TableRow ID="CreditCard_2" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell19" runat="server" CssClass="style1">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="CreditCardAccountNumber">Account Number:</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell20" runat="server" CssClass="style2">
                                <asp:TextBox ID="CreditCardAccountNumber" runat="server" MaxLength="50" Width="300px" TextMode="SingleLine" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow ID="CreditCard_3" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell21" runat="server" CssClass="style1">
                                <asp:Label ID="Label4" runat="server" AssociatedControlID="CreditCardExpirationMonth">Expiration Date:</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell22" runat="server" CssClass="style2">
                                <asp:DropDownList ID="CreditCardExpirationMonth" runat="server">
                                    <asp:ListItem value="01">01</asp:ListItem>
                                    <asp:ListItem value="02">02</asp:ListItem>
                                    <asp:ListItem value="03">03</asp:ListItem>
                                    <asp:ListItem value="04">04</asp:ListItem>
                                    <asp:ListItem value="05">05</asp:ListItem>
                                    <asp:ListItem value="06">06</asp:ListItem>
                                    <asp:ListItem value="07">07</asp:ListItem>
                                    <asp:ListItem value="08">08</asp:ListItem>
                                    <asp:ListItem value="09">09</asp:ListItem>
                                    <asp:ListItem value="10">10</asp:ListItem>
                                    <asp:ListItem value="11">11</asp:ListItem>
                                    <asp:ListItem value="12">12</asp:ListItem>
                                </asp:DropDownList>
                            /
                                <asp:DropDownList ID="CreditCardExpirationYear" runat="server"/>
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow ID="CreditCard_4" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell23" runat="server" CssClass="style1">
                                <asp:Label ID="Label5" runat="server" AssociatedControlID="CreditCardCVV">Validation Number (on back):</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell24" runat="server" CssClass="style2">
                                <asp:TextBox ID="CreditCardCVV" runat="server" MaxLength="4" TextMode="SingleLine" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="ACH" style="display:none">
                    <asp:Table ID="Table2" runat="server" Width="100%" CellPadding="0" CellSpacing="0">
                        <asp:TableRow ID="ACH_1" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell25" runat="server" CssClass="style1">
                                <asp:Label ID="Label6" runat="server" AssociatedControlID="ACHRouting">ABA Routing number (9 digits):</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell26" runat="server" CssClass="style2">
                                <asp:TextBox ID="ACHRouting" runat="server" MaxLength="9" TextMode="SingleLine" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow ID="ACH_2" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell27" runat="server" CssClass="style1">
                                <asp:Label ID="Label7" runat="server" AssociatedControlID="ACHAccountNumber">Account Number:</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell28" runat="server" CssClass="style2">
                                <asp:TextBox ID="ACHAccountNumber" runat="server" MaxLength="50" Width="300px" TextMode="SingleLine" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow ID="ACH_3" runat="server" VerticalAlign="Top">
                            <asp:TableCell ID="TableCell29" runat="server" CssClass="style1">
                                <asp:Label ID="Label8" runat="server" AssociatedControlID="ACHType">Account Type:</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell30" runat="server" CssClass="style2">
                                <asp:DropDownList ID="ACHType" runat="server">
                                    <asp:ListItem value="C" selected="true">Checking</asp:ListItem>
                                    <asp:ListItem value="S">Savings</asp:ListItem>
                                </asp:DropDownList>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell></asp:TableRow></asp:Table><div style="text-align:center; height:200px; vertical-align:bottom;">
        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
    </div>

<script language="javascript" type="text/javascript">
    function clearAddress() {
        var same_as_firmID = '<%= same_as_firm.ClientID %>';
        var chk = document.getElementById(same_as_firmID);
        if (chk != null) {
            chk.checked = false;
        }
        return true;
    }
</script>
</asp:Content>
