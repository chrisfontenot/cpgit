﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_1 : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link1.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link1.Controls.Add(new LiteralControl("</span>"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                Response.Redirect("NewAccount_Page_10.aspx", true);
            }
        }
    }
}