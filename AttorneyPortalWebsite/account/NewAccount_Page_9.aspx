﻿<%@ Page Title="Billing Information" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_9.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_9" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="NewAccountHead" runat="server">
<style type="text/css">
    h2
    {
        margin-top:30px;
        margin-bottom:20px;
        font-weight:bold;
        font-size:larger;
        color:Black;
        background-color:White;
    }
    
    p
    {
        margin-bottom:20px;
    }    
    
    ul
    {
        margin-left:30px;
    }
        
    table.escrow
    {
        padding: 0;
        width: 100%; 
        margin-left: 5%; 
        margin-right: 5%;
    }
    
    span.selectedItem
    {
        text-transform:uppercase;
    }
    
    .style5
    {
        margin: 10 10 10 10;
        border-width:1;
        border-color:Black;
        border-style:solid;
        border-collapse:collapse;
    }
    
    td.style1
    {
        width:50%;
        font-weight:bold;
        text-align:left;
        vertical-align:top;
        border-color:Black;
        border-width:thin;
        border-style:solid;
    }
    
    td.style2
    {
        width:50%;
        text-align:left;
        vertical-align:top;
        border-color:Black;
        border-width:thin;
        border-style:solid;
    }
    
    tr
    {
        border-color:Black;
        border-width:thin;
        border-style:solid;
        vertical-align:top;
        text-align:left;
    }
    
</style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

    <asp:Table runat="server">
        <asp:TableRow runat="server">
            <asp:TableCell runat="server">
                <asp:Label ID="Label1" AssociatedControlID="annual_clients" runat="server" Font-Bold="true" Text="Estimated number of clients filing annually?" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:TextBox runat="server" ID="annual_clients" MaxLength="15" ToolTip="Please enter the number here. It must be at least 1." Width="125px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell2" runat="server">
                <asp:Label ID="attorneys_label" AssociatedControlID="attorneys" runat="server" Font-Bold="true" Text="Number of attorneys who will participate in ClearPoint program" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server">
                <asp:TextBox runat="server" ID="attorneys" MaxLength="15" ToolTip="Please enter the number here. It must be at least 1." Width="125px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell4" runat="server">
                <asp:Label ID="referral_label" AssociatedControlID="referral" runat="server" Font-Bold="true" Text="How were you referred to ClearPoint?" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <asp:TextBox runat="server" ID="referral" MaxLength="40" ToolTip="How did you hear about us? What sent you here?" Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell6" runat="server">
                <asp:Label ID="comments_label" AssociatedControlID="comments" runat="server" Font-Bold="true" Text="Special requests or comments" />
            </asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server">
                <asp:TextBox runat="server" ID="comments" MaxLength="1024" ToolTip="Enter any additional information here" Width="300px" TextMode="MultiLine" Rows="4" CssClass="TxtBox" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

    <div style="text-align:center; height:200px; vertical-align:bottom;">
        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
    </div>
</asp:Content>
