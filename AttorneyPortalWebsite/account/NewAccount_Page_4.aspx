﻿<%@ Page Title="Firm Name and Address" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_4.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_4" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

    <table width="100%" cellpadding="0" cellspacing="0">
        <tbody>

            <tr>
                <td colspan="2">
                    <h1>Firm Name</h1>
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_name_label" AssociatedControlID="firm_name" Font-Bold="true" Text="Name of Firm" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="firm_name" MaxLength="40" ToolTip="This is your firm's name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <h1>Firm Address</h1>
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_address_1_label" AssociatedControlID="firm_address_1" Font-Bold="true" Text="Firm Address" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="firm_address_1" MaxLength="50" ToolTip="Please enter your firm's mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_address_2_label" AssociatedControlID="firm_address_2" Font-Bold="true" Text="Firm Address (cont.)" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="firm_address_2" MaxLength="50" ToolTip="Additional line if needed for mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_city_label" AssociatedControlID="firm_city" Font-Bold="true" Text="City" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="firm_city" MaxLength="50" ToolTip="Please enter your firm's city name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_state_label" AssociatedControlID="firm_state" Font-Bold="true" Text="State" />
                </td>
                <td class="style2">
                    <asp:DropDownList runat="server" ID="firm_state" ToolTip="In which state is your firm located?" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="firm_zip_label" AssociatedControlID="firm_zip" Font-Bold="true" Text="ZIP" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="firm_zip" MaxLength="11" ToolTip="Please enter your postal ZIP code" Width="100px" TextMode="SingleLine" CssClass="TxtBox" />
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <h1>Website</h1>
                </td>
            </tr>

            <tr>
                <td class="style1">
                    <asp:Label runat="server" ID="website_label" AssociatedControlID="website" Font-Bold="true" Text="Website address" />
                </td>
                <td class="style2">
                    <asp:TextBox runat="server" ID="website" MaxLength="512" ToolTip="Website address for the firm" Width="150px" TextMode="SingleLine" CssClass="TxtBox" CausesValidation="true" />
                </td>
            </tr>
        </tbody>
    </table>

    <div style="text-align:center; height:200px; vertical-align:bottom;">
        <p>&nbsp;</p>
        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
    </div>
</asp:Content>
