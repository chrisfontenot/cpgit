﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AttorneyPortal.account
{
    public interface ICreateAccount
    {
        bool Required { get; set; }
        void Validate();
    }

    /// <summary>
    /// Do a shallow copy operation. This is not "clone". This is "copy".
    /// </summary>
    public interface ICopyable<T>
    {
        void Copy(T Source);
    }

    /// <summary>
    /// Address information is stored in a structure like this
    /// </summary>
    public class AddressBlock : ICreateAccount, ICopyable<AddressBlock>
    {
        public string Number { get; set; }
        public string PreDirection { get; set; }
        public string Street { get; set; }
        public string Suffix { get; set; }
        public string PostDirection { get; set; }
        public string SecondaryUnit { get; set; }
        public string SecondaryNumber { get; set; }
        public string City { get; set; }
        public Int32? State { get; set; }
        public string Zip { get; set; }

        public bool Required { get; set; }

        public AddressBlock()
        {
            Required        = false;
            Number          = string.Empty;
            PreDirection    = string.Empty;
            Street          = string.Empty;
            Suffix          = string.Empty;
            PostDirection   = string.Empty;
            SecondaryUnit   = string.Empty;
            SecondaryNumber = string.Empty;
            City            = string.Empty;
            State           = 61;
            Zip             = string.Empty;
        }

        public void Validate()
        {
            if (Required)
            {
                if (Street == string.Empty || City == string.Empty || !State.HasValue)
                {
                    throw new ArgumentException("The address is required");
                }
            }
        }

        /// <summary>
        /// Copy the fields from another AddressBlock to the current one.
        /// </summary>
        public void Copy(AddressBlock Source)
        {
            if (Source == null)
            {
                return;
            }

            City            = Source.City;
            State           = Source.State;
            Number          = Source.Number;
            PostDirection   = Source.PostDirection;
            PreDirection    = Source.PreDirection;
            SecondaryNumber = Source.SecondaryNumber;
            SecondaryUnit   = Source.SecondaryUnit;
            Street          = Source.Street;
            Suffix          = Source.Suffix;
            Zip             = Source.Zip;
        }

        /// <summary>
        /// Get the first line of the address block
        /// </summary>
        /// <returns></returns>
        public string Line1()
        {
            var sb = new System.Text.StringBuilder();
            foreach (var str in new string[] { Number, PreDirection, Street, Suffix, PostDirection, SecondaryUnit, SecondaryNumber })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    sb.AppendFormat(" {0}", str);
                }
            }
            return sb.ToString().Trim();
        }

        public void Normalize(string Input)
        {
            var parser = new AddressParser.AddressParser();
            var result = parser.ParseAddress(Input);
            if (result == null)
            {
                throw new ArgumentException("Invalid address information");
            }

            City            = result.City ?? string.Empty;
            Number          = result.Number ?? string.Empty;
            PostDirection   = result.Postdirectional ?? string.Empty;
            PreDirection    = result.Predirectional ?? string.Empty;
            SecondaryNumber = result.SecondaryNumber ?? string.Empty;
            SecondaryUnit   = result.SecondaryUnit ?? string.Empty;
            Street          = result.Street ?? string.Empty;
            Suffix          = result.Suffix ?? string.Empty;
            Zip             = result.Zip ?? string.Empty;

            // Remove leading and trailing "#" from the number field in case someone said "STE #301"
            SecondaryNumber = SecondaryNumber.Trim('#');

            // Do not allow the secondary unit to be simple "#". Also if there is a number but no unit, use STE for the unit.
            if (SecondaryUnit == "#" || (SecondaryUnit == string.Empty && SecondaryNumber != string.Empty))
            {
                SecondaryUnit = "STE";
            }
        }

        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("\r\n{0}", Line1());

            using (var db = new ClearPoint.DebtPlus.DatabaseContext())
            {
                var q = db.stateTable.Where(s => s.Id == State.Value).FirstOrDefault();
                if (q != null)
                {
                    sb.AppendFormat("\r\n{0} {1}  {2}", City, q.MailingCode, Zip);
                }
                else
                {
                    sb.AppendFormat("\r\n{0} {1}", City, Zip);
                }
            }

            sb.Remove(0, 2);
            return sb.ToString();
        }
    }

    /// <summary>
    /// Personal name information is stored in a structure like this
    /// </summary>
    public class NameBlock : ICreateAccount, ICopyable<NameBlock>
    {
        private string _prefix = string.Empty;
        private string _first  = string.Empty;
        private string _middle = string.Empty;
        private string _last   = string.Empty;
        private string _suffix = string.Empty;

        /// <summary>
        /// Are the First and Last name fields required?
        /// </summary>
        public Boolean Required { get; set; }

        /// <summary>
        /// Prefix to the name, i.e. "Mr."
        /// </summary>
        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _prefix = string.Empty;
                    return;
                }
                if (value.Length > 10)
                {
                    _prefix = value.Substring(0, 10);
                    return;
                }
                _prefix = value;
            }
        }

        /// <summary>
        /// Suffix to the name, i.e. "Jr."
        /// </summary>
        public string Suffix
        {
            get
            {
                return _suffix;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _suffix = string.Empty;
                    return;
                }
                if (value.Length > 10)
                {
                    _suffix = value.Substring(0, 10);
                    return;
                }
                _suffix = value;
            }
        }

        /// <summary>
        /// Middle name
        /// </summary>
        public string Middle
        {
            get
            {
                return _middle;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _middle = string.Empty;
                    return;
                }
                if (value.Length > 10)
                {
                    _middle = value.Substring(0, 10);
                    return;
                }
                _middle = value;
            }
        }

        /// <summary>
        /// First name
        /// </summary>
        public string First
        {
            get
            {
                return _first;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _first = string.Empty;
                    return;
                }

                if (value.Length > 40)
                {
                    _first = value.Substring(0, 40);
                    return;
                }
                _first = value;
            }
        }

        /// <summary>
        /// Last name
        /// </summary>
        public string Last
        {
            get
            {
                return _last;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _last = string.Empty;
                    return;
                }

                if (value.Length > 60)
                {
                    _last = value.Substring(0, 60);
                    return;
                }
                _last = value;
            }
        }

        /// <summary>
        /// Initialize the new context
        /// </summary>
        public NameBlock()
        {
            Required = false;
        }

        /// <summary>
        /// Validate the information for the fields
        /// </summary>
        public void Validate()
        {
            if (Required)
            {
                if (First == string.Empty)
                {
                    throw new ArgumentException("First name is required");
                }
                if (Last == string.Empty)
                {
                    throw new ArgumentException("Last name is required");
                }
            }
        }

        /// <summary>
        /// Copy the previous structure to the current item
        /// </summary>
        public void Copy(NameBlock Source)
        {
            if (Source == null)
            {
                return;
            }

            First  = Source.First;
            Middle = Source.Middle;
            Last   = Source.Last;
            Prefix = Source.Prefix;
            Suffix = Source.Suffix;
        }

        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            foreach (var str in new string[] { Prefix, First, Middle, Last })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    sb.AppendFormat(" {0}", str);
                }
            }
            if (!string.IsNullOrWhiteSpace(Suffix))
            {
                sb.AppendFormat(", {0}", Suffix);
            }

            return sb.ToString().Trim();
        }
    }

    /// <summary>
    /// Telephone numbers are stored in a structure like this
    /// </summary>
    public class TelephoneBlock : ICreateAccount, ICopyable<TelephoneBlock>
    {
        private string _areaCode = string.Empty;
        private string _number   = string.Empty;
        private string _ext      = string.Empty;

        public bool Required { get; set; }

        /// <summary>
        /// Copy the previous structure to the current item
        /// </summary>
        public void Copy(TelephoneBlock Source)
        {
            if (Source == null)
            {
                return;
            }

            AreaCode = Source.AreaCode;
            Number   = Source.Number;
            ext      = Source.ext;
        }

        private readonly string invalidNumberError = "Invalid telephone number. Use ###-###-####";

        public TelephoneBlock()
        {
            Required = false;
        }

        /// <summary>
        /// Country code is always USA for these numbers.
        /// </summary>
        public Int32 Country
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Telephone area code
        /// </summary>
        public string AreaCode
        {
            get
            {
                return _areaCode;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _areaCode = string.Empty;
                    return;
                }

                string digits = System.Text.RegularExpressions.Regex.Replace(value, @"\D", string.Empty);
                if (digits.Length != 3)
                {
                    throw new ArgumentException(invalidNumberError);
                }

                if (!System.Text.RegularExpressions.Regex.IsMatch(digits, @"^[2-9][0-8][0-9]$"))
                {
                    throw new ArgumentException(invalidNumberError);
                }

                _areaCode = value;
            }
        }

        /// <summary>
        /// Telephone number
        /// </summary>
        public string Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _number = string.Empty;
                    return;
                }

                string digits = System.Text.RegularExpressions.Regex.Replace(value, @"\D", string.Empty);
                if (digits.Length != 7)
                {
                    throw new ArgumentException(invalidNumberError);
                }

                if (! System.Text.RegularExpressions.Regex.IsMatch(digits, @"^[2-9]\d{6}$"))
                {
                    throw new ArgumentException(invalidNumberError);
                }
                _number = value.Substring(0,3) + "-" + value.Substring(3);
            }
        }

        /// <summary>
        /// Telephone number extension
        /// </summary>
        public string ext
        {
            get
            {
                return _ext;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _ext = string.Empty;
                    return;
                }

                _ext = System.Text.RegularExpressions.Regex.Replace(value, @"\D", string.Empty);
            }
        }

        /// <summary>
        /// Validate the information block to ensure that the fields are present.
        /// </summary>
        public void Validate()
        {
            if (Required && (AreaCode == string.Empty || Number == string.Empty))
            {
                throw new ArgumentException("The telephone number is required");
            }
        }

        /// <summary>
        /// Parse the input string into the area code and number
        /// </summary>
        public void Decode(string Input)
        {
            // If there is no string then there is no number
            if (string.IsNullOrWhiteSpace(Input))
            {
                AreaCode = string.Empty;
                Number   = string.Empty;
                return;
            }

            // Remove all of the non-digits from the number
            string digits = System.Text.RegularExpressions.Regex.Replace(Remap(Input), @"\D", string.Empty);
            if (digits.Length != 10)
            {
                throw new ArgumentException(invalidNumberError);
            }

            // The area code and number can now be split
            AreaCode = digits.Substring(0, 3);
            Number   = digits.Substring(3);
        }

        /// <summary>
        /// Remap the letters that are sometimes used in a telephone number to the number itself
        /// </summary>
        private string Remap(string Input)
        {
            var sb = new System.Text.StringBuilder(Input.ToUpper());
            for(var idx = sb.Length; --idx >= 0;)
            {
                char ch = sb[idx];
                switch(ch)
                {
                    case 'A': case 'B': case 'C':
                        sb[idx] = '2';
                        break;

                    case 'D': case 'E': case 'F':
                        sb[idx] = '3';
                        break;

                    case 'G': case 'H': case 'I':
                        sb[idx] = '4';
                        break;

                    case 'J': case 'K': case 'L':
                        sb[idx] = '5';
                        break;

                    case 'M': case 'N': case 'O':
                        sb[idx] = '6';
                        break;

                    case 'P': case 'Q': case 'R': case 'S':
                        sb[idx] = '7';
                        break;

                    case 'T': case 'U': case 'V':
                        sb[idx] = '8';
                        break;

                    case 'W': case 'X': case 'Y': case 'Z':
                        sb[idx] = '9';
                        break;

                    default:
                        break;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Convert the area code and number to a string
        /// </summary>
        public override string ToString()
        {
            if (_areaCode != string.Empty && _number != string.Empty)
            {
                return _areaCode + "-" + _number;
            }

            return _areaCode + _number;
        }

        public string ToString(bool IncludeExt)
        {
            var str = ToString();
            if (IncludeExt && !string.IsNullOrWhiteSpace(ext))
            {
                str += " ext. " + ext;
            }
            return str;
        }
    }

    /// <summary>
    /// Email addresses are stored in a structure like this
    /// </summary>
    public class EmailBlock : ICreateAccount, ICopyable<EmailBlock>
    {
        private string _address = string.Empty;
        private Int32 _validation = 0;

        public bool Required { get; set; }

        /// <summary>
        /// Copy the previous structure to the current item
        /// </summary>
        public void Copy(EmailBlock Source)
        {
            if (Source == null)
            {
                return;
            }

            Address = Source.Address;
        }

        public EmailBlock()
        {
            Required = false;
        }

        /// <summary>
        /// Validate the data block to ensure that the required informtion is present.
        /// </summary>
        public void Validate()
        {
            if (Required && Address == string.Empty)
            {
                throw new ArgumentException("The email address is required");
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                // If there is no address then there is no address
                if (string.IsNullOrWhiteSpace(value))
                {
                    _address = string.Empty;
                    _validation = 0;
                    return;
                }

                // Validate the email address through our logic library
                var valid = new ClearPoint.IsEMail();
                if (valid.IsEmailValid(value))
                {
                    _address = value;
                    _validation = 1;
                    return;
                }

                // Give the error condition to the caller.
                if (valid.ResultInfo.Count < 1)
                {
                    throw new ArgumentException("Invalid E-Mail address");
                }
                throw new ArgumentException(valid.ResultInfo[0]);
            }
        }

        public Int32 Validation
        {
            get
            {
                return _validation;
            }
        }

        public override string ToString()
        {
            return Address;
        }
    }

    /// <summary>
    /// A general contact information is kept for structures like this
    /// </summary>
    public class ContactBlock : ICreateAccount, ICopyable<ContactBlock>
    {
        public NameBlock Name { get; private set; }
        public AddressBlock Address { get; private set; }
        public TelephoneBlock TelephoneNumber { get; private set; }
        public TelephoneBlock FaxNumber { get; private set; }
        public EmailBlock Email { get; private set; }
        public bool SameAddress { get; set; }

        public bool Required { get; set; }

        /// <summary>
        /// Copy the previous structure to the current item
        /// </summary>
        public void Copy(ContactBlock Source)
        {
            if (Source == null)
            {
                return;
            }

            Name.Copy(Source.Name);
            Address.Copy(Source.Address);
            TelephoneNumber.Copy(Source.TelephoneNumber);
            FaxNumber.Copy(Source.FaxNumber);
            Email.Copy(Source.Email);

            SameAddress = Source.SameAddress;
        }

        public ContactBlock()
        {
            Required        = false;
            Name            = new NameBlock();
            Address         = new AddressBlock();
            TelephoneNumber = new TelephoneBlock();
            FaxNumber       = new TelephoneBlock();
            Email           = new EmailBlock();
            SameAddress     = false;
        }

        public void Validate()
        {
            Name.Validate();
            Address.Validate();
            TelephoneNumber.Validate();
            FaxNumber.Validate();
            Email.Validate();
        }
    }

    /// <summary>
    /// Written to the session information for page2 status
    /// </summary>
    public class Signup_Page2 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "Page2";
            }
        }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page2()
        {
            Agreed   = false;
            Required = true;
        }

        /// <summary>
        /// Has the user agreed to the terms of the letter?
        /// </summary>
        public Boolean Agreed { get; set; }

        public bool Required { get; set; }

        public void Validate()
        {
            if (Required && !Agreed)
            {
                throw new ArgumentException("You must agree before we can accept the account");
            }
        }
    }

    /// <summary>
    /// Written to the session information for page3 status
    /// </summary>
    public class Signup_Page3 : ICreateAccount
    {
        private string _userName = string.Empty;
        private string _password = string.Empty;

        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "Page3";
            }
        }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page3()
        {
            _userName = string.Empty;
            _password = string.Empty;
            Required  = true;
        }

        /// <summary>
        /// User name for the account
        /// </summary>
        public string userName
        {
            get
            {
                return _userName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _userName = string.Empty;
                    return;
                }

                if (!System.Text.RegularExpressions.Regex.IsMatch(value, "^[a-z][a-z0-9_-]*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    throw new ArgumentException("User name must be alphabetic");
                }
                _userName = value;
            }
        }

        /// <summary>
        /// Password for the account
        /// </summary>
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                // Ensure that the password is given
                if (string.IsNullOrWhiteSpace(value))
                {
                    _password = string.Empty;
                    return;
                }

                // Check the minimum length of the password
                var reqLength = System.Web.Security.Membership.Provider.MinRequiredPasswordLength;
                if (reqLength > 0 && value.Length < reqLength)
                {
                    throw new ArgumentException(string.Format("Passwords must be at least {0:f0} characters", reqLength));
                }

                // Ensure tha the special character count matches
                var specialsCnt = System.Web.Security.Membership.Provider.MinRequiredNonAlphanumericCharacters;
                if (specialsCnt > 0 && countSpecials(value) < specialsCnt)
                {
                    throw new ArgumentException(string.Format("Passwords require at least {0:f0} special characters", specialsCnt));
                }

                // Ensure that the regular expression matches
                var regexp = System.Web.Security.Membership.Provider.PasswordStrengthRegularExpression;
                if (!string.IsNullOrWhiteSpace(regexp))
                {
                    try
                    {
                        var reg = new System.Text.RegularExpressions.Regex(regexp);
                        if (!reg.IsMatch(value))
                        {
                            throw new ArgumentException("The password does not meet the required format for use");
                        }
                    }
                    catch { }
                }

                _password = value;
            }
        }

        /// <summary>
        /// Count the number of special characters in the input string
        /// </summary>
        private Int32 countSpecials(string value)
        {
            // Ensure that the string has a length.
            if (string.IsNullOrWhiteSpace(value))
            {
                return 0;
            }

            Int32 itemCount = 0;
            for (int idx = value.Length; --idx >= 0; )
            {
                char chr = value[idx];
                if (System.Char.IsLetterOrDigit(chr) && !System.Char.IsControl(chr))
                {
                    ++itemCount;
                }
            }
            return itemCount;
        }

        public bool Required { get; set; }

        public void Validate()
        {
            if (Required)
            {
                if (userName == string.Empty)
                {
                    throw new ArgumentException("User name must be supplied");
                }

                if (Password == string.Empty)
                {
                    throw new ArgumentException("Password must be supplied");
                }
            }

            // Stop here if we don't have a username.
            if (userName == string.Empty)
            {
                return;
            }

            // Check to ensure that the username is not used already.
            using (var db = new ClearPoint.DebtPlus.DatabaseContext())
            {
                var applicationName = System.Web.Security.Membership.Provider.ApplicationName;
                var q = db.client_wwwTable.Where(s => s.UserName == userName && s.ApplicationName == applicationName).FirstOrDefault();
                if (q != null)
                {
                    throw new ArgumentException("User name is currently used. Please use another name");
                }
            }
        }
    }

    /// <summary>
    /// Written to the session information for page4 status
    /// </summary>
    public class Signup_Page4 : ICreateAccount
    {
        public AddressBlock Address { get; private set; }
        private string _firmName = string.Empty;
        private string _website = "http://";
        public bool Required { get; set; }

        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "Page4";
            }
        }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page4()
        {
            Address = new AddressBlock();
            Required = true;
        }

        public override string ToString()
        {
            return Address.ToString();
        }

        /// <summary>
        /// Name of the firm
        /// </summary>
        public string FirmName
        {
            get
            {
                return _firmName;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _firmName = string.Empty;
                    return;
                }
                _firmName = value;
            }
        }

        /// <summary>
        /// Website of the firm address
        /// </summary>
        public string WebSite
        {
            get
            {
                return _website;
            }
            set
            {
                // If there is no webiste then there is no website.
                if (string.IsNullOrWhiteSpace(value))
                {
                    _website = string.Empty;
                    return;
                }

                // Parse the entered website into the proper values.
                var url = new System.Uri(value);
                if (url == null)
                {
                    throw new ArgumentException("The website is not valid");
                }

                if (url.Scheme != System.Uri.UriSchemeHttp &&
                    url.Scheme != System.Uri.UriSchemeHttps)
                {
                    throw new ArgumentException("The website must start with http:// or https://");
                }

                if (url.IsLoopback || url.IsFile)
                {
                    throw new ArgumentException("The website may not be a file name under any circumstances");
                }
                _website = value;
            }
        }

        public void Validate()
        {
            Address.Validate();
            if (Required)
            {
                if (FirmName == string.Empty)
                {
                    throw new ArgumentException("A firm name must be specified");
                }
            }
        }
    }

    /// <summary>
    /// Written to the session information for page5 status
    /// </summary>
    public class Signup_Page5 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "Page5";
            }
        }

        /// <summary>
        /// Principal attorney contact information
        /// </summary>
        public ContactBlock Principal { get; private set; }

        public bool Required { get; set; }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page5()
        {
            Principal = new ContactBlock();
            Required  = true;
        }

        /// <summary>
        /// Validate the resulting structure to find missing fields that may be defined as optional for other instances.
        /// </summary>
        public void Validate()
        {
            if (Principal.Email.Address == string.Empty)
            {
                throw new ArgumentException("The Email address is required");
            }

            if (Principal.TelephoneNumber.Number == string.Empty || Principal.TelephoneNumber.AreaCode == string.Empty)
            {
                throw new ArgumentException("The telephone number is required");
            }
            Principal.Validate();
        }
    }

    /// <summary>
    /// Written to the session information for page6 status
    /// </summary>
    public class Signup_Page6 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "Page6";
            }
        }

        public Int32? Service_PreFiling { get; set; }
        public Int32? Service_PreDischarge { get; set; }

        public bool Required { get; set; }
        public void Validate()
        {
            if (Required)
            {
                if (!Service_PreFiling.HasValue || !Service_PreDischarge.HasValue)
                {
                    throw new ArgumentException("The escrow status must be specified for all services");
                }
            }
        }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page6()
        {
            Service_PreFiling    = new Int32?();
            Service_PreDischarge = new Int32?();
            Required             = true;
        }
    }

    /// <summary>
    /// Written to the session information for page7 status
    /// </summary>
    public class Signup_Page7 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "page7";
            }
        }

        /// <summary>
        /// Billing attorney contact information
        /// </summary>
        public ContactBlock Billing { get; private set; }

        public bool Required { get; set; }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page7()
        {
            Required = true;
            Billing = new ContactBlock();
            Billing.Email.Required = true;
            Billing.TelephoneNumber.Required = true;
        }

        /// <summary>
        /// Validate the resulting structure to find missing fields that may be defined as optional for other instances.
        /// </summary>
        public void Validate()
        {
            Billing.Validate();
        }
    }

    /// <summary>
    /// Written to the session information for page8 status
    /// </summary>
    public class Signup_Page8 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "page8";
            }
        }

        /// <summary>
        /// Billing attorney contact information
        /// </summary>
        public AddressBlock Invoice { get; private set; }
        public bool SameAddress { get; set; }

        public string BillingMode { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string CVV { get; set; }
        public string RoutingNumber { get; set; }
        public char CheckingSavings { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public bool Required { get; set; }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page8()
        {
            Required         = true;
            Invoice          = new AddressBlock();
            Invoice.Required = true;
            BillingMode      = "CreditCard";
            SameAddress      = true;
            CheckingSavings  = 'C';
            ExpirationDate   = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(2).AddDays(-1).Date;
        }

        /// <summary>
        /// Validate the resulting structure to find missing fields that may be defined as optional for other instances.
        /// </summary>
        public void Validate()
        {
            if (!Required)
            {
                return;
            }

            if (BillingMode == "None")
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(AccountNumber))
            {
                throw new ArgumentException("The account number is required");
            }

            if (string.IsNullOrWhiteSpace(AccountName))
            {
                throw new ArgumentException("The account owner's name is required");
            }

            if (BillingMode == "CreditCard")
            {
                if (!ExpirationDate.HasValue || ExpirationDate.Value.AddMonths(1) <= DateTime.Now.Date)
                {
                    throw new ArgumentException("The expiration date must be at least one month in the future");
                }

                if (!validations.Valid_Luhn(AccountNumber))
                {
                    throw new ArgumentException("The Credit Card account number is invalid");
                }

                if (!validations.Valid_MASTERCARD(AccountNumber) &&
                    !validations.Valid_VISA(AccountNumber) &&
                    !validations.Valid_AMEX(AccountNumber))
                {
                    throw new ArgumentException("We only accept VISA, MASTERCARD, and AMERICAN EXPRESS at the current time");
                }

                if (validations.Valid_AMEX(AccountNumber))
                {
                    if (CVV.Length != 4)
                    {
                        throw new ArgumentException("American express CVV numbers are 4 digits on the front of the card");
                    }
                }
                else
                {
                    if (CVV.Length < 3)
                    {
                        throw new ArgumentException("VISA/MASTERCARD CVV numbers are 3 digits on the back of the card");
                    }
                }
            }

            if (BillingMode == "ACH")
            {
                if (validations.ValidABAChecksum(RoutingNumber))
                {
                    throw new ArgumentException("The ACH Routing number is not correct");
                }

                if (CheckingSavings != 'C' && CheckingSavings != 'S')
                {
                    throw new ArgumentException("The Checking or Savings type must be specified");
                }
            }

            // Validate the invoice address information.
            if (! SameAddress)
            {
                Invoice.Validate();
            }
        }
    }

    /// <summary>
    /// Written to the session information for page9 status
    /// </summary>
    public class Signup_Page9 : ICreateAccount
    {
        /// <summary>
        /// Id of the class
        /// </summary>
        public string Name
        {
            get
            {
                return "page9";
            }
        }

        public bool Required { get; set; }

        public Int32? attorneys { get; set; }
        public Int32? annual_clients { get; set; }
        public string referral { get; set; }
        public string comments { get; set; }

        /// <summary>
        /// Initialize for the new class instance
        /// </summary>
        public Signup_Page9()
        {
            Required = true;
        }

        /// <summary>
        /// Validate the resulting structure to find missing fields that may be defined as optional for other instances.
        /// </summary>
        public void Validate()
        {
            if (!Required)
            {
                return;
            }
        }
    }
}