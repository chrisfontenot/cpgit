﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using ClearPoint;

namespace AttorneyPortal.account
{
    public partial class Login : System.Web.UI.Page
    {
        /// <summary>
        /// Process the page load sequence
        /// </summary>
        public void Page_Load(object sender, EventArgs e)
        {
            this.DataBind();
            LoginButton.Click += LoginButton_Click;
        }

        /// <summary>
        /// Process the LOGIN button.
        /// </summary>
        private void LoginButton_Click(object sender, EventArgs e)
        {
            // Retrieve the values from the form.
            string userName = UserName.Text.Trim();
            string password = Password.Text.Trim();

            var errormessage = new System.Text.StringBuilder();
            if (userName == string.Empty)
            {
                errormessage.Append("<li>The username field is required. Please enter a value.</li>");
            }

            if (password == string.Empty)
            {
                errormessage.Append("<li>The password field is required. Please enter a value.</li>");
            }

            if (errormessage.Length > 0)
            {
                FailureText.Text = string.Format("<ul>{0}</ul>", errormessage.ToString());
                FailureText.Visible = true;
                return;
            }

            // Validate the user name and password.
            if (System.Web.Security.Membership.ValidateUser(userName, password) && SetSessionData(userName))
            {
                PerformLogon(userName, false);
                return;
            }

            // Indicate the failure condition.
            FailureText.Text = "Unsuccessful attempt. Please re-enter your information and try again.";

            // Add the lockout status to the message if needed
            var u = System.Web.Security.Membership.GetUser(userName);
            if (u != null && u.IsLockedOut)
            {
                FailureText.Text += " <span style=\"font-weight:bold\">Your account has been locked out.</span>";
            }
            FailureText.Visible = true;
        }

        /// <summary>
        /// Set the information about the attorney into the session tables so that we don't need to refer to the
        /// record for the data. It also helps build the strings in the aspx page.
        /// </summary>
        private bool SetSessionData(string userName)
        {
            // Obtain the vendor name from the record and set the session variables accordingly
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusSqlServer"].ConnectionString;
            using (var db = new DebtPlus.DatabaseContext(connectionString))
            {
                // Get the client_www record for this logon.
                var clientRecord = db.client_wwwTable.Where(s => s.ApplicationName == System.Web.Security.Membership.ApplicationName && s.UserName == userName).FirstOrDefault();
                if (clientRecord == null)
                {
                    return false;
                }

                // The vendor ID pointer should be in the client_www table. Bad things happen if not.
                if (!clientRecord.DatabaseKeyID.HasValue)
                {
                    return false;
                }

                // Access the vendor record in the tables now.
                var vendorRecord = db.vendorTable.Where(s => s.Id == clientRecord.DatabaseKeyID.Value).FirstOrDefault();
                if (vendorRecord == null)
                {
                    return false;
                }

                // Finally, set the vendor ID and the name in the session information
                Session["atty_name"] = Server.HtmlEncode(vendorRecord.Name ?? string.Empty);
                Session["firm_code"] = vendorRecord.Id.ToString("0000");
                return true;
            }
        }

        private string[] GetCustomUserRoles()
        {
            return new string[] { };
        }

        private void PerformLogon(string username, bool isPersistent)
        {
            // sometimes used to persist user roles
            string userData = string.Join("|", GetCustomUserRoles());

            var ticket = new System.Web.Security.FormsAuthenticationTicket(
            1,                                     // ticket version
            username,                              // authenticated username
            DateTime.Now,                          // issueDate
            DateTime.Now.AddMinutes(30),           // expiryDate
            isPersistent,                          // true to persist across browser sessions
            userData,                              // can be used to store additional user data
            System.Web.Security.FormsAuthentication.FormsCookiePath);  // the path for the cookie

            // Encrypt the ticket using the machine key
            string encryptedTicket = System.Web.Security.FormsAuthentication.Encrypt(ticket);

            // Add the cookie to the request to save it
            HttpCookie cookie = new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true; 
            Response.Cookies.Add(cookie);

            // Your redirect logic
            Response.Redirect(System.Web.Security.FormsAuthentication.GetRedirectUrl(username, isPersistent));
        }   
    }
}
