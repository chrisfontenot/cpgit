﻿using System;

namespace AttorneyPortal.account.s
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            // Abandon the session keys and sign-out of the forms authentication
            Session.Abandon();
            System.Web.Security.FormsAuthentication.SignOut();

            // Transfer to the main page. This will prompt the sign-in page again.
            Response.Redirect("~/s/");
        }
    }
}