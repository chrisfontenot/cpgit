﻿<%@ Page Title="Bankruptcy Attorney information" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewAccount_Page_3A.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_3A" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SiteHead" runat="server">
    <style type="text/css">
        .style1
        {
            width:20%;
            height:22px;
            text-align:right;
            padding-right:5px;
            vertical-align:top;
        }
        
        .style2
        {
            width:80%;
            height:22px;
            text-align:left;
            vertical-align:top;
        }
        .style3
        {
            width:50%;
            height:22px;
            text-align:right;
            padding-right:5px;
            vertical-align:top;
        }
        
        .style4
        {
            width:50%;
            height:22px;
            text-align:left;
            vertical-align:top;
        }
        
        .style5
        {
            margin: 10 10 10 10;
            border-width:1;
            border-color:Black;
            border-style:solid;
            border-collapse:collapse;
        }            
        
        input[disabled]
        {
            color:Gray; text-decoration:none;
        }
        
        table.escrow
        {
            width:70%; 
            margin-left:15%; 
            margin-right:15%;
        }
      </style>

    <script language="javascript" type="text/javascript">
        function FlipFlopBox() {
            var selectedValue = document.getElementById('<%= BillingMethod.ClientID %>').value;

            if (selectedValue == "CreditCard") {
                document.getElementById('<%= CreditCard_1.ClientID %>').style.display = 'block';
                document.getElementById('<%= CreditCard_2.ClientID %>').style.display = 'block';
                document.getElementById('<%= CreditCard_3.ClientID %>').style.display = 'block';
                document.getElementById('<%= CreditCard_4.ClientID %>').style.display = 'block';
                document.getElementById('<%= ACH_1.ClientID %>').style.display = 'none';
                document.getElementById('<%= ACH_2.ClientID %>').style.display = 'none';
                document.getElementById('<%= ACH_3.ClientID %>').style.display = 'none';
                return;
            }

            if (selectedValue == "ACH") {
                document.getElementById('<%= CreditCard_1.ClientID %>').style.display = 'none';
                document.getElementById('<%= CreditCard_2.ClientID %>').style.display = 'none';
                document.getElementById('<%= CreditCard_3.ClientID %>').style.display = 'none';
                document.getElementById('<%= CreditCard_4.ClientID %>').style.display = 'none';
                document.getElementById('<%= ACH_1.ClientID %>').style.display = 'block';
                document.getElementById('<%= ACH_2.ClientID %>').style.display = 'block';
                document.getElementById('<%= ACH_3.ClientID %>').style.display = 'block';
                return;
            }

            document.getElementById('<%= CreditCard_1.ClientID %>').style.display = 'none';
            document.getElementById('<%= CreditCard_2.ClientID %>').style.display = 'none';
            document.getElementById('<%= CreditCard_3.ClientID %>').style.display = 'none';
            document.getElementById('<%= CreditCard_4.ClientID %>').style.display = 'none';
            document.getElementById('<%= ACH_1.ClientID %>').style.display = 'none';
            document.getElementById('<%= ACH_2.ClientID %>').style.display = 'none';
            document.getElementById('<%= ACH_3.ClientID %>').style.display = 'none';
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="SiteBody" runat="server">
    <form id="Form1" runat="server" action="NewAccount_Page_3.aspx" method="post">

        <asp:Panel ID="NoticePannel" runat="server" Visible="true">
            <table style="border-width:medium; border-color:Red; border-style:solid" cellspacing="5" cellpadding="5">
                <tbody>
                    <tr valign="middle">
                        <td align="left">
                            <p>
                                This web location is for practicing bankruptcy attorneys and their firms only. If you are not
                                an attorney servicing bankruptcy clients then please return to our main site by clicking on
                                the link below. We offer our services to practicing attorneys only at this time and request
                                that you respect our wishes. Thank you.
                            </p>

                            <p>
                                <a href="http://www.cccsinc.org/crisisManagement/bankruptcy.jsp" target="_self">If you are not an attorney and have come here by mistake then please click here.</a>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>

        <asp:Panel ID="ErrorPanel" runat="server" Visible="false">
            <table cellspacing="5" cellpadding="5">
                <tbody>
                    <tr valign="top">
                        <td style="width:20%">
                            &nbsp;
                        </td>
                        <td style="border-width:medium; border-color:Red; border-style:solid">
                            <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />
                        </td>
                        <td style="width:20%">
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>

        <asp:Panel runat="server" ID="InputPanel" Visible="true">

            <div style="height:20px;">&nbsp;</div>

            <h1>Account Information</h1>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="username_label" AssociatedControlID="username" Font-Bold="true" Text="User Name" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="username" MaxLength="20" ToolTip="User name for this website" Width="150px" TextMode="SingleLine" CssClass="TxtBox" onKeyUP="this.value = this.value.toUpperCase();" CausesValidation="true" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="password_label" AssociatedControlID="password" Font-Bold="true" Text="Password" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="password" ToolTip="Password for this website" Width="150px" TextMode="Password" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="password2_label" AssociatedControlID="password2" Font-Bold="true" Text="Re-Enter Password" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="password2" ToolTip="Password for this website" Width="150px" TextMode="Password" CssClass="TxtBox" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <h1>Firm Information</h1>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_name_label" AssociatedControlID="firm_name" Font-Bold="true" Text="Name of Firm" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_name" MaxLength="40" ToolTip="This is your firm's name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_address_label" AssociatedControlID="firm_address" Font-Bold="true" Text="Firm Address" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_address" MaxLength="50" ToolTip="Please enter your firm's mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_address2_label" AssociatedControlID="firm_address2" Font-Bold="true" Text="Firm Address (cont.)" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_address2" MaxLength="50" ToolTip="Additional line if needed for mailing address" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_city_label" AssociatedControlID="firm_city" Font-Bold="true" Text="City" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_city" MaxLength="50" ToolTip="Please enter your firm's city name" Width="300px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_state_label" AssociatedControlID="firm_state" Font-Bold="true" Text="State" />
                        </td>
                        <td class="style2">
                            <asp:DropDownList runat="server" ID="firm_state" ToolTip="In which state is your firm located?" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_zip_label" AssociatedControlID="firm_zip" Font-Bold="true" Text="ZIP" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_zip" MaxLength="11" ToolTip="Please enter your postal ZIP code" Width="100px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="firm_webite_label" AssociatedControlID="firm_webite" Font-Bold="true" Text="Firm Website" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="firm_webite" MaxLength="512" ToolTip="Please enter your website address" Width="300px" TextMode="Url" CssClass="TxtBox" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>

            <h1>Primary Attorney Information</h1>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="name1_first_label" AssociatedControlID="name1_first" Font-Bold="true" Text="First name" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="name1_first" MaxLength="20" ToolTip="This is the first (christian) name for the principal attorney." Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="name1_last_label" AssociatedControlID="name1_last" Font-Bold="true" Text="Last name" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="name1_last" MaxLength="20" ToolTip="This is the last (sir) name for the principal attorney." Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="telephone_label" AssociatedControlID="telephone" Font-Bold="true" Text="Telephone" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="telephone" MaxLength="15" ToolTip="Please use entries such as (###) ####-####. We do not need an extension number." Width="125px" TextMode="Phone" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="fax_label" AssociatedControlID="fax" Font-Bold="true" Text="FAX" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="fax" MaxLength="15" ToolTip="Please use entries such as (###) ####-####. We do not need an extension number." Width="125px" TextMode="Phone" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="email_label" AssociatedControlID="email1" Font-Bold="true" Text="E-Mail" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="email1" MaxLength="256" ToolTip="E-mail address for the principal attorney." Width="300px" TextMode="Email" CssClass="TxtBox" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>

            <h1>ClearPoint Contact Information</h1>

            <div style="height:60px; vertical-align:top; text-align:left">
                Enter contact below to received ClearPoint reports &amp; correspondence (leave blank if same as the primary attorney above). This is usually the name of the executive secretary.
            </div>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="name2_first_label" AssociatedControlID="name2_first" Font-Bold="true" Text="First name" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="name2_first" MaxLength="20" ToolTip="This is the first (christian) name for the alternate contact." Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="name2_last_label" AssociatedControlID="name2_last" Font-Bold="true" Text="Last name" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="name2_last" MaxLength="20" ToolTip="This is the last (sir) name for the alternate contact." Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                            <asp:Label runat="server" ID="email2_label"  AssociatedControlID="email2" Font-Bold="true" Text="E-Mail" />
                        </td>
                        <td class="style2">
                            <asp:TextBox runat="server" ID="email2" MaxLength="256" ToolTip="E-mail address for the alternate contact." Width="300px" TextMode="Email" CssClass="TxtBox" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>

            <h1>Service Information</h1>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td class="style3">
                            <asp:Label ID="annual_clients_label" AssociatedControlID="annual_clients" runat="server" Font-Bold="true" Text="Estimated number of clients filing annually?" />
                        </td>
                        <td class="style4">
                            <asp:TextBox runat="server" ID="annual_clients" MaxLength="15" ToolTip="Please enter the number here. It must be at least 1." Width="125px" TextMode="Number" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style3">
                            <asp:Label ID="attorneys_label" AssociatedControlID="attorneys" runat="server" Font-Bold="true" Text="Number of attorneys who will participate in ClearPoint program" />
                        </td>
                        <td class="style4">
                            <asp:TextBox runat="server" ID="attorneys" MaxLength="15" ToolTip="Please enter the number here. It must be at least 1." Width="125px" TextMode="Number" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style3">
                            <asp:Label ID="referral_label" AssociatedControlID="referral" runat="server" Font-Bold="true" Text="How were you referred to ClearPoint?" />
                        </td>
                        <td class="style4">
                            <asp:TextBox runat="server" ID="referral" MaxLength="40" ToolTip="How did you hear about us? What sent you here?" Width="200px" TextMode="SingleLine" CssClass="TxtBox" />
                        </td>
                    </tr>

                    <tr>
                        <td class="style3">
                            <asp:Label ID="comments_label" AssociatedControlID="comments" runat="server" Font-Bold="true" Text="Special requests or comments" />
                        </td>
                        <td class="style4">
                            <asp:TextBox runat="server" ID="comments" MaxLength="1024" ToolTip="Enter any additional information here" Width="300px" TextMode="MultiLine" Rows="4" CssClass="TxtBox" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <p>&nbsp;</p>

            <h1>Escrow Options</h1>

            <asp:Table ID="escrow" runat="server" CellSpacing="0" CssClass="escrow">
                <asp:TableHeaderRow ID="TableRow1" runat="server" CssClass="style5">
                    <asp:TableHeaderCell HorizontalAlign="Left" Font-Bold="true" CssClass="style5">
                        Counseling Type
                    </asp:TableHeaderCell>
                    <asp:TableHeaderCell ColumnSpan="3" HorizontalAlign="Center" Font-Bold="true" CssClass="style5">
                        Escrow Options
                    </asp:TableHeaderCell>
                </asp:TableHeaderRow>

                <asp:TableRow ID="TableRow2" runat="server" CssClass="style5">
                    <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="Top" CssClass="style5">
                        Pre-Filing Bankruptcy Counseling
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="pre_yes" Text="Escrow" GroupName="pre" Checked="false" ToolTip="You will collect the fee from the client and we will bill you" />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell3" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="pre_no" Text="Non-Escrow" GroupName="pre" Checked="false" ToolTip="You do not collect the fee from the client and we bill the client directly" />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell7" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="pre_free" Text="pro bono" GroupName="pre" Checked="false" ToolTip="You only offer this pro-bono and collect no fee at all" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow ID="TableRow3" runat="server" CssClass="style5">
                    <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Top" CssClass="style5">
                        Pre-Discharge Bankruptcy Counseling
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell5" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="post_yes" Text="Escrow" GroupName="post" Checked="false" ToolTip="You will collect the fee from the client and we will bill you" />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell6" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="post_no" Text="Non-Escrow" GroupName="post" Checked="false"  ToolTip="You do not collect the fee from the client and we bill the client directly" />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell8" runat="server" VerticalAlign="Top" CssClass="style5">
                        <asp:RadioButton runat="server" ID="post_free" Text="pro bono" GroupName="post" Checked="false" ToolTip="You only offer this pro-bono and collect no fee at all" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

            <h1>Payment Information</h1>
            <p>
                We ask that you provide payment information if you are doing any form of escrow payments. This permits us
                to bill you for the funds that you hold in escrow. The account may not be established if you are doing escrow
                payments and do not provide us with the payment details.
            </p>

            <p>
                If you are doing the services <span style="font-weight:bold">pro bono</span> or do <span style="font-weight:bold">not</span> do escrow payments for any service listed above
                then please choose "None" as the payment method.
            </p>

            <asp:Table Width="100%" runat="server" CellPadding="0" CellSpacing="0">
                <asp:TableRow ID="PaymentMethoSelctionRow" style="display:block">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="BillingMethod">Payment Method:</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="50%">
                        <asp:DropDownList ID="BillingMethod" runat="server" onchange="javascript:FlipFlopBox();">
                            <asp:ListItem Value="None">None (pro bono or non-escrow)</asp:ListItem>
                            <asp:ListItem Value="CreditCard" Selected="True">Credit Card</asp:ListItem>
                            <asp:ListItem Value="ACH">Bank Draft</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow ID="CreditCard_1" style="display:block">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="CreditCardAccountName">Name on card:</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell Width="50%">
                        <asp:TextBox ID="CreditCardAccountName" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="CreditCard_2" style="display:block">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="CreditCardAccountNumber">Account Number:</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:TextBox ID="CreditCardAccountNumber" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="CreditCard_3" style="display:block">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="CreditCardExpirationMonth">Expiration Date:</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:DropDownList ID="CreditCardExpirationMonth" runat="server">
                            <asp:ListItem value="01" Selected="True">01</asp:ListItem>
                            <asp:ListItem value="02">02</asp:ListItem>
                            <asp:ListItem value="03">03</asp:ListItem>
                            <asp:ListItem value="04">04</asp:ListItem>
                            <asp:ListItem value="05">05</asp:ListItem>
                            <asp:ListItem value="06">06</asp:ListItem>
                            <asp:ListItem value="07">07</asp:ListItem>
                            <asp:ListItem value="08">08</asp:ListItem>
                            <asp:ListItem value="09">09</asp:ListItem>
                            <asp:ListItem value="10">10</asp:ListItem>
                            <asp:ListItem value="11">11</asp:ListItem>
                            <asp:ListItem value="12">12</asp:ListItem>
                        </asp:DropDownList>
                        /
                        <asp:DropDownList ID="CreditCardExpirationYear" runat="server">
                            <asp:ListItem value="2017" selected="true">2017</asp:ListItem>
                            <asp:ListItem value="2018">2018</asp:ListItem>
                            <asp:ListItem value="2019">2019</asp:ListItem>
                            <asp:ListItem value="2020">2020</asp:ListItem>
                            <asp:ListItem value="2021">2021</asp:ListItem>
                            <asp:ListItem value="2022">2022</asp:ListItem>
                            <asp:ListItem value="2023">2023</asp:ListItem>
                            <asp:ListItem value="2024">2024</asp:ListItem>
                            <asp:ListItem value="2025">2025</asp:ListItem>
                            <asp:ListItem value="2026">2026</asp:ListItem>
                            <asp:ListItem value="2027">2027</asp:ListItem>
                            <asp:ListItem value="2028">2028</asp:ListItem>
                            <asp:ListItem value="2029">2029</asp:ListItem>
                            <asp:ListItem value="2030">2030</asp:ListItem>
                            <asp:ListItem value="2031">2031</asp:ListItem>
                            <asp:ListItem value="2032">2032</asp:ListItem>
                            <asp:ListItem value="2033">2033</asp:ListItem>
                            <asp:ListItem value="2034">2034</asp:ListItem>
                            <asp:ListItem value="2035">2035</asp:ListItem>
                            <asp:ListItem value="2036">2036</asp:ListItem>
                            <asp:ListItem value="2037">2037</asp:ListItem>
                            <asp:ListItem value="2038">2038</asp:ListItem>
                            <asp:ListItem value="2039">2039</asp:ListItem>
                            <asp:ListItem value="2040">2040</asp:ListItem>
                            <asp:ListItem value="2041">2041</asp:ListItem>
                            <asp:ListItem value="2042">2042</asp:ListItem>
                            <asp:ListItem value="2043">2043</asp:ListItem>
                            <asp:ListItem value="2044">2044</asp:ListItem>
                            <asp:ListItem value="2045">2045</asp:ListItem>
                            <asp:ListItem value="2046">2046</asp:ListItem>
                            <asp:ListItem value="2047">2047</asp:ListItem>
                            <asp:ListItem value="2048">2048</asp:ListItem>
                            <asp:ListItem value="2049">2049</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="CreditCard_4" style="display:block">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="CreditCardCVV">Validation Number (on back):</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:TextBox ID="CreditCardCVV" runat="server" MaxLength="4" TextMode="SingleLine" />
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="ACH_1" style="display:none">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="ACHRouting">ABA Routing number (9 digits):</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:TextBox ID="ACHRouting" runat="server" MaxLength="9" TextMode="SingleLine" />
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="ACH_2" style="display:none">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="ACHAccountNumber">Account Number:</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:TextBox ID="ACHAccountNumber" runat="server" MaxLength="50" TextMode="SingleLine" />
                    </asp:TableCell></asp:TableRow><asp:TableRow ID="ACH_3" style="display:none">
                    <asp:TableCell Width="50%">
                        <asp:Label runat="server" AssociatedControlID="ACHType">Account Type:</asp:Label>
                    </asp:TableCell><asp:TableCell Width="50%">
                        <asp:DropDownList ID="ACHType" runat="server">
                            <asp:ListItem value="C" selected="true">Checking</asp:ListItem>
                            <asp:ListItem value="S">Savings</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell></asp:TableRow></asp:Table><div runat="server" id="validation_entry">
                <p>&nbsp;</p><h1>Validation</h1><cc1:GoogleReCaptcha ID="ctrlGoogleReCaptcha" runat="server" PublicKey="6Ldjaw0UAAAAANDlEdHbtgEo4AQBJF4r34N1nZLO" PrivateKey="6Ldjaw0UAAAAADKbCSgWFGjiURXA8_w-6r8xVLu3" />
            </div>

            <div style="text-align:center; height:200px; vertical-align:bottom;">
                <p>&nbsp;</p><asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
            </div>
        </asp:Panel>

        <asp:Panel ID="completionPanel" runat="server" Visible="false">
            <h1>Process Completed</h1><p>We have created a record for your firm in our database. The Firm Identification Code of the record is <span style="font-weight:bold; font-size:x-large"><% =vendorRecord.Label %></span>. Please remember to give this
            to each of your clients when you refer them to us. It is the key under which you know you so that we may give you the credit for the
            client. This ID is not the value that you use to logon to our site. You entered it when you created the account. This is the ID that we have for your
            firm and use it to track the various activity that we do for you.</p><p>Your account has been created and is usable immediately.</p><p>You have been granted access to our site. We ask that if you have escrow accounts where you collect the fee for our
            counseling from the client and pay us rather than the client paying us directly, that you give us the payment information needed to process the charges.
            You may do this by choosing the "Update Financial Information" on the main screen (after clicking on the link below.)</p><p><asp:HyperLink runat="server" NavigateUrl="~/Default.aspx" Target="_self">Return to our main site.</asp:HyperLink></p></asp:Panel></form></asp:Content>