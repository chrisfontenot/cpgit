﻿<%@ Page Title="Services" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_6.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_6" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />

    <table width="100%" cellpadding="0" cellspacing="0">
        <tbody>
            <tr valign="top" align="left">
                <td>
                    <h1>Services</h1>
                </td>
            </tr>

            <tr valign="top" align="left">
                <td>
                    <asp:Table ID="escrow" runat="server" CellSpacing="0" CssClass="escrow">
                        <asp:TableHeaderRow ID="TableRow1" runat="server" CssClass="style5">
                            <asp:TableHeaderCell HorizontalAlign="Left" Font-Bold="true" CssClass="style5">
                                Counseling Type
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell ColumnSpan="3" HorizontalAlign="Center" Font-Bold="true" CssClass="style5">
                                Escrow Options
                            </asp:TableHeaderCell>
                        </asp:TableHeaderRow>

                        <asp:TableRow ID="TableRow2" runat="server" CssClass="style5">
                            <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="Top" CssClass="style5">
                                Pre-Filing Bankruptcy Counseling
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="pre_yes" Text="Escrow" GroupName="pre" Checked="false" ToolTip="You will collect the fee from the client and we will bill you" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell3" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="pre_no" Text="Non-Escrow" GroupName="pre" Checked="false" ToolTip="You do not collect the fee from the client and we bill the client directly" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell7" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="pre_free" Text="pro bono" GroupName="pre" Checked="false" ToolTip="You only offer this pro-bono and collect no fee at all" />
                            </asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow ID="TableRow3" runat="server" CssClass="style5">
                            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Top" CssClass="style5">
                                Pre-Discharge Bankruptcy Counseling
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell5" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="post_yes" Text="Escrow" GroupName="post" Checked="false" ToolTip="You will collect the fee from the client and we will bill you" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell6" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="post_no" Text="Non-Escrow" GroupName="post" Checked="false"  ToolTip="You do not collect the fee from the client and we bill the client directly" />
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell8" runat="server" VerticalAlign="Top" CssClass="style5">
                                <asp:RadioButton runat="server" ID="post_free" Text="pro bono" GroupName="post" Checked="false" ToolTip="You only offer this pro-bono and collect no fee at all" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td>
                    Pro Bono services are defined as that you will always do each case on a pro bono basis. If you do some cases pro bono but you charge for others
                    then please do not choose pro bono. You may then elect, on a case by case basis, what is pro bono and what is not. But, if you declare that you do
                    the work for a type of counseling as being pro bono then you are saying that you are doing every case as pro bono and you never charge for it to anyone.
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align:center">
                    <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
