﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq.Expressions;

namespace AttorneyPortal.account
{
    public partial class NewAccount_Page_5 : System.Web.UI.Page
    {
        // Pointer to the current structure for the form controls
        Signup_Page5 params5 = null;

        /// <summary>
        /// Initialize the new page
        /// </summary>
        protected void Page_Init(object sender, EventArgs e)
        {
            var p = this.Master as AttorneyPortal.s.NewAccountMaster;
            p.link5.Controls.AddAt(0, new LiteralControl("<span class=\"selectedItem\">"));
            p.link5.Controls.Add(new LiteralControl("</span>"));

            // Register the event handler routines
            RegisterHandlers();
        }

        /// <summary>
        /// Hook the event handlers to the controls
        /// </summary>
        private void RegisterHandlers()
        {
            Submit.Click += new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiterHandlers()
        {
            Submit.Click -= new ImageClickEventHandler(Submit_Click);
        }

        /// <summary>
        /// Handle the LOAD sequence for the webpage
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            UnRegiterHandlers();
            try
            {
                // Check the box if it was previously checked
                params5 = Session["Signup_Page5"] as Signup_Page5;
                if (params5 == null)
                {
                    params5 = new Signup_Page5();
                    params5.Principal.SameAddress = true;
                }

                // Copy the account address if the flag says that the value is to be copied.
                if (params5.Principal.SameAddress)
                {
                    // Copy the address from the firm value if we have something
                    var params4 = Session["Signup_Page4"] as Signup_Page4;
                    if (params4 != null)
                    {
                        params5.Principal.Address.Copy(params4.Address);
                    }
                }

                // On the initial load, set the entries according to the parameters
                if (!Page.IsPostBack)
                {
                    // Load the list of state names
                    using (var db = new ClearPoint.DebtPlus.DatabaseContext())
                    {
                        contact_state.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, params5.Principal.Address.State.GetValueOrDefault(61)).ToArray());
                    }

                    first_name.Text        = params5.Principal.Name.First;
                    middle_name.Text       = params5.Principal.Name.Middle;
                    last_name.Text         = params5.Principal.Name.Last;
                    contact_address_1.Text = params5.Principal.Address.Line1();
                    contact_city.Text      = params5.Principal.Address.City;
                    contact_zip.Text       = params5.Principal.Address.Zip;
                    telephone.Text         = params5.Principal.TelephoneNumber.ToString();
                    fax.Text               = params5.Principal.FaxNumber.ToString();
                    ext.Text               = params5.Principal.TelephoneNumber.ext;
                    email.Text             = params5.Principal.Email.ToString();
                    same_as_firm.Checked   = params5.Principal.SameAddress;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the submit button for the form.
        /// </summary>
        private void Submit_Click(object sender, ImageClickEventArgs e)
        {
            // Decode the first name
            var sb = new System.Text.StringBuilder();

            // Decode the simple fields. We need a "try" for each as the errors are separate and we want to see them all.
            try { params5.Principal.TelephoneNumber.Decode(telephone.Text.Trim());  } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.FaxNumber.Decode(fax.Text.Trim());              } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.Name.First          = first_name.Text.Trim();   } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.Name.Middle         = middle_name.Text.Trim();  } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.Name.Last           = last_name.Text.Trim();    } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.TelephoneNumber.ext = ext.Text.Trim();          } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            try { params5.Principal.Email.Address       = email.Text.Trim();        } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }

            // This statement can not generate an error. It just saves the status of the checkbox.
            params5.Principal.SameAddress = same_as_firm.Checked;

            // Decode the address information if the address is different from the firm's
            if (! params5.Principal.SameAddress)
            {
                try
                {
                    var adr = new System.Text.StringBuilder();
                    var str = contact_address_1.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = contact_address_2.Text.Trim();
                    if (str != string.Empty)
                    {
                        adr.AppendFormat("\r\n{0}", str);
                    }

                    str = contact_city.Text + " VA " + contact_zip.Text;
                    adr.AppendFormat("\r\n{0}", str);
                    adr.Remove(0, 2);

                    params5.Principal.Address.Normalize(adr.ToString());
                    params5.Principal.Address.State = Int32.Parse(contact_state.SelectedValue);
                }
                catch (Exception ex)
                {
                    sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message));
                }
            }

            // Finally validate the structure if there are no parsing errors. Skip this if there are errors
            // since we don't need the errors to cascade.
            if (sb.Length == 0)
            {
                try { params5.Validate(); } catch (Exception ex) { sb.AppendFormat("<li>{0}</li>", Server.HtmlEncode(ex.Message)); }
            }

            // If there is an error then indicate it in red and leave without posting the values
            if (sb.Length > 0)
            {
                sb.Insert(0, "<div style=\"color:red; font-weight:bold\">Problem list with the input fields:</div><ul style=\"color:red; font-weight:bold\">");
                sb.Append("</ul>");
                ValidationSummary.Text = sb.ToString();
                return;
            }

            // Update the form values for later reference and transfer to the next page.
            Session["Signup_Page5"] = params5;
            Response.Redirect("NewAccount_Page_10.aspx", true);
        }
    }
}