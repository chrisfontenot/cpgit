﻿<%@ Page Title="Letter of Understanding" Language="C#" MasterPageFile="~/account/NewAccountMaster.master" AutoEventWireup="true" CodeBehind="NewAccount_Page_2.aspx.cs" Inherits="AttorneyPortal.account.NewAccount_Page_2" %>
<%@ MasterType virtualpath="~/account/NewAccountMaster.master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="NewAccountBody" runat="server">
    <h1>Letter Of Understanding</h1>

    <p>The CredAbility Bankruptcy Attorney Program provides registered law firms with an Express Service option for fulfilling the pre-filing credit counseling and debtor education requirements of the Bankruptcy Abuse Prevention and Consumer Protection Act of 2005 (BAPCPA).</p>

    <h2>The 24/7 CredAbility Express Service includes:</h2>

    <ul>
	    <li>A dedicated, secure web portal for legal professionals to monitor client progress and billing.</li>
	    <li>Confidential counseling and education available nationwide in English and Spanish online with 24/7 support from our certified chat counselors</li>
	    <li>Fully interactive Internet sessions with no call-backs required</li>
	    <li>Immediate delivery of counseling certificates</li>
	    <li>Convenient payment options including monthly escrow billing or direct payment by clients to CreCredAbility</li>
    </ul>

    <h2> Participating Firm Commitment</h2>

    <p><span style="font-weight:bold">Assigned firm code:</span> Once enrolled in the CredAbility Express Service for Legal Professionals program, your firm will be assigned a Firm Identification Code. This code will be a letter, a dash, and a number such as <span style="font-weight:bold">&ldquo;A-1234&rdquo;</span>. You agree to provide your clients this Firm Identification Code and instruct them to give us this unique code at the time of service. This enables CredAbility to automatically send your firm bankruptcy counseling and education certificates once your clients have completed their sessions, both to email addresses of your choice and via the secure web portal that enables you to monitor your clients' counseling and education activity.</p>
    <p><span style="font-weight:bold">Escrow payment:</span> If you wish to escrow client fees for counseling or debtor education, enrollment in the CredAbility Express Service requires that you provide your name, address, city, state, ZIP code and a valid credit card or account debit instructions for payment of client fees incurred using your assigned firm code</p>
    <p>You agree to escrow client payments for pre-filing counseling and/or pre-discharge (debtor) education services and remit those funds to us for the services we provide.  In the event you have collected counseling and/or debtor education fees before a client's fee waiver request is submitted or approved by CredAbility, you agree to expeditiously refund to the client any fees collected for bankruptcy counseling and debtor education.</p>
    <p><span style="font-weight:bold">Preview your billing before it occurs:</span> On the first day of each month, using your unique login and password to access the secure CredAbility web portal, you will review your Client Activity Report for the previous month. After any necessary adjustments are made in collaboration with CredAbility, we will charge the credit card or debit the bank account you provided upon registration to collect fees reflected on the report.</p>
    <p><span style="font-weight:bold">Direct client payment:</span> If you choose not to escrow your client fees, your clients will be asked to pay at the time of service. We accept bank debit cards, direct deposit, pre-paid debit cards, and, in some cases, Western Union.</p>
    <p><span style="font-weight:bold">Report review:</span> You will receive access to a secure CredAbility web portal that enables you to generate various reports that track your clients' sessions, review your billed and unbilled activity and download certificates on-demand. You agree to periodically review this activity and report any discrepancies to us.</p>
    <p><span style="font-weight:bold">IMPORTANT NOTE:</span> To ensure you receive earned certificates and other communications from CredAbility, please add the following e-mails to your address books:</p>

    <ul style="list-style-type:none">
        <li><a class="NormLink" href="mailto:info@CredAbility.org">info@CredAbility.org</a></li>
		<li><a class="NormLink" href="mailto:CredAbility@statement2web.org">CredAbility@statement2web.org</a></li>
		<li><a class="NormLink" href="mailto:Abdul.Mclin@Clearpointccs.org">Abdul.Mclin@Clearpointccs.org</a></li>
		<li><a class="NormLink" href="mailto:bkeducation@CredAbility.org">bkeducation@CredAbility.org</a></li>
    </ul>

    <h2>Additional Terms</h2>

	<p><span style="font-weight:bold">Confidentiality of Client Information.</span> Both parties agree to share and to keep confidential the information necessary to evaluate client situations, file bankruptcy petitions and track program performance.</p>
	<p><span style="font-weight:bold">Termination.</span> Participation in this program is voluntary and either party may elect to disengage from the process at any time, but both parties agree to make a good faith effort at program success.</p>
	<p><span style="font-weight:bold">Non-Exclusivity.</span> This agreement is a non-exclusive relationship. CredAbility may elect at any time to include additional bankruptcy service providers located in and beyond metropolitan Atlanta.</p>
	<p><span style="font-weight:bold">Regular Review.</span> Both parties will participate in ongoing and regular analysis of referral volume, client satisfaction, process flow and financial performance. This will allow for the measured expansion and assessment of the program.</p>

    <div class="no-print" style="text-align:center; height:200px; vertical-align:bottom;">
        <asp:CheckBox ID="chkAgree" runat="server" Checked="false" /> I have read and agree to the terms listed above.</a>
        <p>&nbsp;</p>
        <asp:ImageButton runat="server" CausesValidation="true" AlternateText="Submit" CommandName="Submit" BorderStyle="None" ID="Submit" ImageUrl="~/images/btn_OC_continue.gif" />
        <asp:Literal ID="ValidationSummary" runat="server" Mode="PassThrough" Visible="true" />
    </div>
</asp:Content>