﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page4.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page4" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">FIRM INFORMATION</p>

<asp:Table runat="server">
    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell runat="server" style="width:150px; vertical-align:top; text-align:left;">Firm Name</asp:TableCell>
        <asp:TableCell runat="server" ID="firm_name"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell1" runat="server" style="width:150px; vertical-align:top; text-align:left;">Firm Address</asp:TableCell>
        <asp:TableCell runat="server" ID="firm_address"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow3" runat="server">
        <asp:TableCell ID="TableCell3" runat="server" style="width:150px; vertical-align:top; text-align:left;">Website</asp:TableCell>
        <asp:TableCell runat="server" ID="firm_website"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
