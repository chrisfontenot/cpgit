﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class AddressUpdate : System.Web.UI.UserControl
    {

        /// <summary>
        /// Read the address information
        /// </summary>
        /// <param name="db"></param>
        /// <param name="currentRecord"></param>
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.address currentRecord)
        {
            State.Items.Clear();
            if (currentRecord != null)
            {
                Address_1.Text = currentRecord.AddressLine1;
                Address_2.Text = currentRecord.address_line_2;
                City.Text = currentRecord.city;
                Zip.Text = currentRecord.PostalCode;

                State.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, currentRecord.state).ToArray());
            }
            else
            {
                State.Items.AddRange(ClearPoint.Utilitiy.GetStateListItems(db.stateTable, 61).ToArray());
            }
        }

        /// <summary>
        /// Write the address information
        /// </summary>
        /// <param name="db"></param>
        /// <param name="currentRecord"></param>
        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.address currentRecord, string creditor_prefix_1)
        {
            if (currentRecord != null)
            {
                var adr = new System.Text.StringBuilder();
                var str = Address_1.Text.Trim();
                if (str != string.Empty)
                {
                    adr.AppendFormat("\r\n{0}", str);
                }

                str = Address_2.Text.Trim();
                if (str != string.Empty)
                {
                    adr.AppendFormat("\r\n{0}", str);
                }

                str = City.Text + " VA " + Zip.Text;
                adr.AppendFormat("\r\n{0}", str);
                adr.Remove(0, 2);

                var parse = new AddressParser.AddressParser();
                var result = parse.ParseAddress(adr.ToString());
                if (result != null)
                {
                    Int32 stateID = 61;

                    currentRecord.house             = result.Number ?? string.Empty;
                    currentRecord.direction         = result.Predirectional ?? string.Empty;
                    currentRecord.street            = result.Street ?? string.Empty;
                    currentRecord.suffix            = result.Suffix ?? string.Empty;
                    currentRecord.modifier          = result.SecondaryUnit ?? string.Empty;
                    currentRecord.modifier_value    = result.SecondaryNumber ?? string.Empty;
                    currentRecord.address_line_2    = "";
                    currentRecord.city              = result.City ?? string.Empty;
                    currentRecord.state             = Int32.TryParse(State.SelectedValue ?? "", out stateID) ? stateID : 0;
                    currentRecord.PostalCode        = result.Zip ?? string.Empty;

                    currentRecord.creditor_prefix_1 = creditor_prefix_1 ?? string.Empty;
                }
            }
        }
    }
}