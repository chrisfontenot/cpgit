﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class NameUpdate : System.Web.UI.UserControl
    {
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.name currentRecord)
        {
            if (currentRecord != null)
            {
                First.Text = currentRecord.First;
                Middle.Text = currentRecord.Middle;
                Last.Text = currentRecord.Last;
            }
        }

        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.name currentRecord)
        {
            if (currentRecord != null)
            {
                currentRecord.First = First.Text.Trim();
                currentRecord.Middle = Middle.Text.Trim();
                currentRecord.Last = Last.Text.Trim();
            }
        }
    }
}