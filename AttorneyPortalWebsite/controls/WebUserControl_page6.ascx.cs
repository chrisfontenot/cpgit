﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page6 : System.Web.UI.UserControl
    {
        // Information for the page 6 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page6 params6;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params6 = Session["Signup_Page6"] as AttorneyPortal.account.Signup_Page6;
                if (params6 != null)
                {
                    // Validate the page
                    if (params6.Required)
                    {
                        params6.Validate();
                    }

                    // Display the data
                    pre_filing.Text    = EscrowStatus(params6.Service_PreFiling);
                    pre_discharge.Text = EscrowStatus(params6.Service_PreDischarge);

                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_6.aspx");
        }

        private string EscrowStatus(Int32? CodeValue)
        {
            if (!CodeValue.HasValue)
            {
                return "&nbsp;";
            }

            // Determine the values for the code
            switch (CodeValue.Value)
            {
                case 0:
                    return "Non-Escrow";

                case 1:
                    return "Escrow";

                case 2:
                    return "Pro-Bono (no charge)";

                default:
                    Response.Redirect("~/account/NewAccount_Page_6.aspx");
                    return "Unknown";
            }
        }
    }
}