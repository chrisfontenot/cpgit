﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page9 : System.Web.UI.UserControl
    {
        // Information for the page 9 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page9 params9;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params9 = Session["Signup_Page9"] as AttorneyPortal.account.Signup_Page9;
                if (params9 != null)
                {
                    // Validate the page
                    if (params9.Required)
                    {
                        params9.Validate();
                    }

                    attorneys.Text = params9.attorneys.ToString();
                    clients.Text   = params9.annual_clients.ToString();
                    referral.Text  = params9.referral;
                    comments.Text  = params9.comments;

                    // Display the information from the page
                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_9.aspx");
        }
    }
}