﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class ContactUpdate : System.Web.UI.UserControl
    {
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.vendor_contact vendorContactRecord)
        {
            ReadRecord_Email(db, EmailRecord, vendorContactRecord.emailID);
            ReadRecord_Name(db, NameRecord, vendorContactRecord.nameID);
            ReadRecord_Address(db, AddressRecord, vendorContactRecord.addressID);
            ReadRecord_Phone(db, TelephoneRecord, vendorContactRecord.telephoneID);
            ReadRecord_Phone(db, TelephoneRecord, vendorContactRecord.faxID);
        }

        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.vendor_contact vendorContactRecord, string firmName)
        {
            vendorContactRecord.emailID     = WriteRecord_Email(db, vendorContactRecord.emailID, EmailRecord);
            vendorContactRecord.nameID      = WriteRecord_Name(db, vendorContactRecord.nameID, NameRecord);
            vendorContactRecord.addressID   = WriteRecord_Address(db, vendorContactRecord.addressID, AddressRecord, firmName);
            vendorContactRecord.telephoneID = WriteRecord_Phone(db, vendorContactRecord.telephoneID, TelephoneRecord);
            vendorContactRecord.faxID       = WriteRecord_Phone(db, vendorContactRecord.faxID, FAXRecord);
        }

        private void ReadRecord_Email(ClearPoint.DebtPlus.DatabaseContext db, EmailUpdate currentControl, Int32? Id)
        {
            if (Id.HasValue)
            {
                var rec = db.EmailAddressTable.Where(s => s.Id == Id.Value).FirstOrDefault();
                currentControl.ReadRecord(db, rec);
            }
        }

        private Int32? WriteRecord_Email(ClearPoint.DebtPlus.DatabaseContext db, Int32? currentValue, EmailUpdate currentControl)
        {
            if (currentValue.HasValue)
            {
                var emailRecord = db.EmailAddressTable.Where(s => s.Id == currentValue.Value).FirstOrDefault();
                if (emailRecord != null)
                {
                    currentControl.WriteRecord(db, ref emailRecord);
                    return emailRecord.Id;
                }
            }

            var newRecord = new ClearPoint.DebtPlus.EmailAddress();
            currentControl.WriteRecord(db, ref newRecord);
            if (!string.IsNullOrEmpty(newRecord.Address))
            {
                db.EmailAddressTable.InsertOnSubmit(newRecord);
                db.SubmitChanges();
                return newRecord.Id;
            }

            return null;
        }

        private void ReadRecord_Name(ClearPoint.DebtPlus.DatabaseContext db, NameUpdate currentControl, Int32? Id)
        {
            if (Id.HasValue)
            {
                var rec = db.nameTable.Where(s => s.Id == Id.Value).FirstOrDefault();
                currentControl.ReadRecord(db, rec);
            }
        }

        private Int32? WriteRecord_Name(ClearPoint.DebtPlus.DatabaseContext db, Int32? currentValue, NameUpdate currentControl)
        {
            if (currentValue.HasValue)
            {
                var nameRecord = db.nameTable.Where(s => s.Id == currentValue.Value).FirstOrDefault();
                if (nameRecord != null)
                {
                    currentControl.WriteRecord(db, ref nameRecord);
                    return nameRecord.Id;
                }
            }

            var newRecord = new ClearPoint.DebtPlus.name();
            currentControl.WriteRecord(db, ref newRecord);
            if (!string.IsNullOrEmpty(newRecord.First) || !string.IsNullOrEmpty(newRecord.Last))
            {
                db.nameTable.InsertOnSubmit(newRecord);
                db.SubmitChanges();
                return newRecord.Id;
            }

            return null;
        }

        private void ReadRecord_Address(ClearPoint.DebtPlus.DatabaseContext db, AddressUpdate currentControl, Int32? Id)
        {
            if (Id.HasValue)
            {
                var rec = db.addressTable.Where(s => s.Id == Id.Value).FirstOrDefault();
                currentControl.ReadRecord(db, rec);
            }
        }

        private Int32? WriteRecord_Address(ClearPoint.DebtPlus.DatabaseContext db, Int32? currentValue, AddressUpdate currentControl, string firmName)
        {
            if (currentValue.HasValue)
            {
                var addressRecord = db.addressTable.Where(s => s.Id == currentValue.Value).FirstOrDefault();
                if (addressRecord != null)
                {
                    currentControl.WriteRecord(db, ref addressRecord, firmName);
                    return addressRecord.Id;
                }
            }

            var newRecord = new ClearPoint.DebtPlus.address();
            currentControl.WriteRecord(db, ref newRecord, firmName);
            if (!string.IsNullOrEmpty(newRecord.AddressLine1))
            {
                db.addressTable.InsertOnSubmit(newRecord);
                db.SubmitChanges();
                return newRecord.Id;
            }

            return null;
        }

        private void ReadRecord_Phone(ClearPoint.DebtPlus.DatabaseContext db, TelephoneUpdate currentControl, Int32? Id)
        {
            if (Id.HasValue)
            {
                var rec = db.TelephoneNumberTable.Where(s => s.Id == Id.Value).FirstOrDefault();
                currentControl.ReadRecord(db, rec);
            }
        }

        private Int32? WriteRecord_Phone(ClearPoint.DebtPlus.DatabaseContext db, Int32? currentValue, TelephoneUpdate currentControl)
        {
            if (currentValue.HasValue)
            {
                var telephoneRecord = db.TelephoneNumberTable.Where(s => s.Id == currentValue.Value).FirstOrDefault();
                if (telephoneRecord != null)
                {
                    currentControl.WriteRecord(db, ref telephoneRecord);
                    return telephoneRecord.Id;
                }
            }

            var newRecord = new ClearPoint.DebtPlus.TelephoneNumber();
            currentControl.WriteRecord(db, ref newRecord);
            if (!string.IsNullOrEmpty(newRecord.Number))
            {
                db.TelephoneNumberTable.InsertOnSubmit(newRecord);
                db.SubmitChanges();
                return newRecord.Id;
            }

            return null;
        }
    }
}