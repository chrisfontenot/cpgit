﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NameUpdate.ascx.cs" Inherits="AttorneyPortal.controls.NameUpdate" %>

<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td style="width:30%">
            <asp:TextBox ID="First" runat="server" MaxLength="80" Width="100%" />
        </td>
        <td style="width:10%">
            <asp:TextBox ID="Middle" runat="server" MaxLength="80" Width="100%" />
        </td>
        <td style="width:60%">
            <asp:TextBox ID="Last" runat="server" MaxLength="80" Width="100%" />
        </td>
    </tr>
</table>
