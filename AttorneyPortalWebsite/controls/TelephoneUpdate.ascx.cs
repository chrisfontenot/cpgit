﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class TelephoneUpdate : System.Web.UI.UserControl
    {
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.TelephoneNumber currentRecord)
        {
            if (currentRecord != null)
            {
                Number.Text = string.Format("({0}) {1}", currentRecord.Acode, currentRecord.Number);
                Ext.Text = currentRecord.Ext;
            }
        }

        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.TelephoneNumber currentRecord)
        {
            if (currentRecord != null)
            {
                // Toss all but the digits
                var str = System.Text.RegularExpressions.Regex.Replace(Number.Text, @"\D", string.Empty);

                // IF there are ten digits then use the area code and number values.
                if (str.Length == 10)
                {
                    currentRecord.Acode = str.Substring(0, 3);
                    currentRecord.Number = str.Substring(3, 3) + "-" + str.Substring(6);
                    return;
                }

                // If there is no area code then there is no area code
                if (str.Length == 7)
                {
                    currentRecord.Acode = "";
                    currentRecord.Number = str.Substring(0, 3) + "-" + str.Substring(3);
                    return;
                }

                // The number is just bad. Blank it out.
                currentRecord.Acode = string.Empty;
                currentRecord.Number = string.Empty;
            }
        }
    }
}