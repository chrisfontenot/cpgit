﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebUserControl_page5.ascx.cs" Inherits="AttorneyPortal.controls.WebUserControl_page5" %>

<p style="background-color:Teal; text-align:center; color:White; font-weight:bold">PRINCIPAL ATTORNEY</p>

<asp:Table runat="server">
    <asp:TableRow ID="TableRow1" runat="server">
        <asp:TableCell runat="server" style="width:150px; vertical-align:top; text-align:left;">Name</asp:TableCell>
        <asp:TableCell runat="server" ID="attorney_name"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow2" runat="server">
        <asp:TableCell ID="TableCell1" runat="server" style="width:150px; vertical-align:top; text-align:left;">Address</asp:TableCell>
        <asp:TableCell runat="server" ID="attorney_address"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow4" runat="server">
        <asp:TableCell ID="TableCell2" runat="server" style="width:150px; vertical-align:top; text-align:left;">Telephone Number</asp:TableCell>
        <asp:TableCell runat="server" ID="attorney_TelephoneNumber"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow5" runat="server">
        <asp:TableCell ID="TableCell4" runat="server" style="width:150px; vertical-align:top; text-align:left;">FAX Number</asp:TableCell>
        <asp:TableCell runat="server" ID="attorney_FaxNumber"></asp:TableCell>
    </asp:TableRow>

    <asp:TableRow ID="TableRow6" runat="server">
        <asp:TableCell ID="TableCell5" runat="server" style="width:150px; vertical-align:top; text-align:left;">E-Mail Address</asp:TableCell>
        <asp:TableCell runat="server" ID="attorney_email"></asp:TableCell>
    </asp:TableRow>
</asp:Table>
