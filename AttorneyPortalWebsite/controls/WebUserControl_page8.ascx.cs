﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page8 : System.Web.UI.UserControl
    {
        // Information for the page 8 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page8 params8;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params8 = Session["Signup_Page8"] as AttorneyPortal.account.Signup_Page8;
                if (params8 != null)
                {
                    if (params8.SameAddress)
                    {
                        var params4 = Session["Signup_Page4"] as AttorneyPortal.account.Signup_Page4;
                        if (params4 != null)
                        {
                            params8.Invoice.Copy(params4.Address);
                        }
                    }

                    // Validate the page
                    if (params8.Required)
                    {
                        params8.Validate();
                    }

                    BillingAddress.Text = params8.Invoice.ToString().Replace("\r\n", "<br/>");
                    AccountName.Text = params8.AccountName;
                    AccountNumber.Text = params8.AccountNumber;

                    switch (params8.BillingMode ?? "None")
                    {
                        case "CreditCard":
                            PaymentMethod.Text = "Credit Card";
                            ACH_CHECKING_SAVINGS.Visible = false;
                            ACH_LABEL_1.Visible = false;
                            ACH_ROW_1.Visible = false;
                            ACH_ROW_2.Visible = false;

                            ExpirationDate.Text = string.Format("{0:MMM / yyyy}", params8.ExpirationDate);
                            CVV.Text = params8.CVV;
                            break;

                        case "ACH":
                            PaymentMethod.Text = "Bank Draft";
                            CC_LABEL_1.Visible = false;
                            CC_LABEL_2.Visible = false;
                            CC_ROW_1.Visible = false;
                            CC_ROW_2.Visible = false;

                            RoutingNumber.Text = params8.RoutingNumber;
                            CheckingSavings.Text = (params8.CheckingSavings == 'S') ? "Savings" : "Checking";
                            break;

                        case "None":
                            PaymentMethod.Text = "None - Pro Bono only";
                            CC_LABEL_1.Visible = false;
                            CC_LABEL_2.Visible = false;
                            CC_ROW_1.Visible = false;
                            CC_ROW_2.Visible = false;
                            ACH_CHECKING_SAVINGS.Visible = false;
                            ACH_LABEL_1.Visible = false;
                            ACH_ROW_1.Visible = false;
                            ACH_ROW_2.Visible = false;
                            break;
                    }

                    // Display the information from the page
                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_8.aspx");
        }
    }
}