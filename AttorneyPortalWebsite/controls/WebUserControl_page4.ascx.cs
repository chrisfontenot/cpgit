﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class WebUserControl_page4 : System.Web.UI.UserControl
    {
        // Information for the page 4 data
        [Bindable(false)]
        [Category("Data")]
        [Localizable(true)]
        public AttorneyPortal.account.Signup_Page4 params4;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                params4 = Session["Signup_Page4"] as AttorneyPortal.account.Signup_Page4;
                if (params4 != null)
                {
                    // Validate the page
                    if (params4.Required)
                    {
                        params4.Validate();
                    }

                    firm_name.Text = System.Net.WebUtility.HtmlEncode(params4.FirmName);
                    firm_address.Text = System.Net.WebUtility.HtmlEncode(params4.Address.ToString()).Replace("\r\n", "<br/>");
                    firm_website.Text = System.Net.WebUtility.HtmlEncode(params4.WebSite);

                    return;
                }
            }
            catch { }

            // There is something wrong with this information. Go to the input page again.
            Response.Redirect("~/account/NewAccount_Page_4.aspx");
        }
    }
}