﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class EmailUpdate : System.Web.UI.UserControl
    {
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.EmailAddress currentRecord)
        {
            if (currentRecord != null)
            {
                EmailAddress.Text = currentRecord.Address;
            }
        }

        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.EmailAddress currentRecord)
        {
            if (currentRecord != null)
            {
                currentRecord.Address    = EmailAddress.Text.Trim();
                currentRecord.Validation = string.IsNullOrWhiteSpace(currentRecord.Address) ? 2 : 1;
            }
        }
    }
}