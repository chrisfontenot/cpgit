﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;

namespace AttorneyPortal.controls
{
    [ToolboxData("<{0}:ServerControl1 runat=\"server\"></{0}:ServerControl1>")]
    public partial class VendorAddressUpdate : System.Web.UI.UserControl
    {
        /// <summary>
        /// Save the record contents when the data must be submitted
        /// </summary>
        public void ReadRecord(ClearPoint.DebtPlus.DatabaseContext db, ClearPoint.DebtPlus.vendor_address CurrentValues)
        {
            if (CurrentValues != null && CurrentValues.addressID.HasValue)
            {
                var adr = db.addressTable.Where(s => s.Id == CurrentValues.addressID.Value).FirstOrDefault();
                if (adr != null)
                {
                    AddrUpdate.ReadRecord(db, adr);
                }
            }
        }

        public void WriteRecord(ClearPoint.DebtPlus.DatabaseContext db, ref ClearPoint.DebtPlus.vendor_address CurrentValues, string creditor_prefix_1)
        {
            // If there are no current values then create a new record and insert it.
            if (CurrentValues != null)
            {
                // If there is no address then allocate a new address
                if (CurrentValues.addressID == null)
                {
                    var newAdr = new ClearPoint.DebtPlus.address();
                    AddrUpdate.WriteRecord(db, ref newAdr, creditor_prefix_1);
                    db.SubmitChanges();
                    CurrentValues.addressID = newAdr.Id;
                    return;
                }

                // The address is defined. Update it.
                var Id = CurrentValues.addressID.Value;
                var oldAdr = db.addressTable.Where(s => s.Id == Id).FirstOrDefault();
                if (oldAdr == null)
                {
                    var newAdr = new ClearPoint.DebtPlus.address();
                    AddrUpdate.WriteRecord(db, ref newAdr, creditor_prefix_1);
                    db.SubmitChanges();
                    CurrentValues.addressID = newAdr.Id;
                    return;
                }

                // Update the old address information
                AddrUpdate.WriteRecord(db, ref oldAdr, creditor_prefix_1);
            }
        }
    }
}