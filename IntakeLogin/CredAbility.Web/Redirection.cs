using System;
using System.Web;
using CredAbility.Web.Configuration;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Web
{
    public static class Redirection
    {
        public static void ToVerifyProfile()
        {
            var verifyProfile = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.VerifyProfile.SitecorePath);
            WebUtil.Redirect(LinkManager.GetItemUrl(verifyProfile));
        }

        public static void ToOfflineProfileIdentification()
        {
            WebUtil.Redirect("/offline-user.aspx");
        }

        public static void ToMyRecentActivity()
        {
            HttpContext.Current.Response.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value);
        }
    }
}