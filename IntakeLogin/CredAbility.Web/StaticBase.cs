﻿using System;


namespace CredAbility.Web
{
    public class StaticBase
    {
        public virtual string Code
        {
            get { throw new NotImplementedException("The code property should be override"); }
        }

        public virtual string Text
        {
            get { throw new NotImplementedException("The text property should be override"); }
        }

        public override bool Equals(Object obj)
        {
            // If parameter cannot be cast to ResultMessage return false:
            var staticBase = obj as StaticBase;
            if (staticBase == null)
            {
                return false;
            }

            // Return true if the fields match:
            return this.Code == staticBase.Code;
        }

        public bool Equals(StaticBase staticBase)
        {
            // Return true if the fields match:
            return this.Code == staticBase.Code;
        }

        public static bool operator ==(StaticBase a, StaticBase b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Code == b.Code;
        }

        public static bool operator !=(StaticBase a, StaticBase b)
        {
            return !(a == b);
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
       
    }
}
