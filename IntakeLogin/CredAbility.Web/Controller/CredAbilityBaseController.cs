﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Compilation;
using System.Web.UI;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Layouts;
using Sitecore.Pipelines.RenderLayout;

namespace CredAbility.Web.Controller
{
    public class CredAbilityBaseController : System.Web.Mvc.Controller
    {
        protected override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            if (filterContext.ActionParameters.ContainsKey("culture")){
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(filterContext.ActionParameters["culture"].ToString());
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(filterContext.ActionParameters["culture"].ToString());
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
