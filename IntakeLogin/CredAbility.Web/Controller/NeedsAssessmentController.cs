﻿using System.Web.Mvc;
using CredAbility.Web.SitecoreLib.ViewData;
using Sitecore;

namespace CredAbility.Web.Controller
{
    public class NeedsAssessmentController : CredAbilityBaseController
    {
        public ActionResult Index()
        {
            var VD = new NeedsAssessmentViewData { CurrentStep = 1};
            return View(Context.Item.Visualization.Layout.FilePath,VD);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(NeedsAssessmentViewData VD, FormCollection form)
        {
            VD.DebtOptions = form.GetValue("DebtOptions").AttemptedValue;
            if (form["GoToNextStep"] != null)
                VD.CurrentStep = GetNextStep(VD.CurrentStep, VD);
            if (form["GoToPrevStep"] != null)
                VD.CurrentStep = GetPreviousStep(VD.CurrentStep, VD);
            return View(Context.Item.Visualization.Layout.FilePath, VD);
        }

        private static int GetNextStep(int currentStep, NeedsAssessmentViewData VD)
        {
            currentStep += 1;
            switch (currentStep)
            {
                case 2:
                    return VD.MonthlyRevolvingCreditCardDebt ? currentStep : GetNextStep(currentStep, VD);
                case 3:
                    return VD.BankruptcyFilingProcess ? currentStep : GetNextStep(currentStep, VD);
                case 5:
                    return VD.HomeMortgage ? currentStep : GetNextStep(currentStep, VD);
                case 6:
                    return VD.HomePurchase ? currentStep : GetNextStep(currentStep, VD);
                default:
                    return currentStep;
            }
        }

        private static int GetPreviousStep(int currentStep, NeedsAssessmentViewData VD)
        {
            currentStep += -1;
            switch (currentStep)
            {
                case 2:
                    return VD.MonthlyRevolvingCreditCardDebt ? currentStep : GetPreviousStep(currentStep, VD);
                case 3:
                    return VD.BankruptcyFilingProcess ? currentStep : GetPreviousStep(currentStep, VD);
                case 5:
                    return VD.HomeMortgage ? currentStep : GetPreviousStep(currentStep, VD);
                case 6:
                    return VD.HomePurchase ? currentStep : GetPreviousStep(currentStep, VD);
                default:
                    return currentStep;
            }
        }

        public ActionResult Step1(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step1.ViewUrl, VD);
        }

        public ActionResult Step2(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step2.ViewUrl, VD);
        }

        public ActionResult Step3(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step3.ViewUrl, VD);
        }

        public ActionResult Step4(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step4.ViewUrl, VD);
        }

        public ActionResult Step5(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step5.ViewUrl, VD);
        }

        public ActionResult Step6(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step6.ViewUrl, VD);
        }

        public ActionResult Step7(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step7.ViewUrl, VD);
        }

        public ActionResult Step8(NeedsAssessmentViewData VD)
        {
            return View(NeedsAssessmentSteps.Step8.ViewUrl, VD);
        }

    }
}
