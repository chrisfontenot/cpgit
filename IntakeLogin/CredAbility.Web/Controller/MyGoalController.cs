﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CredAbility.Core.Activity;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.MyGoal;
using CredAbility.Data.Activity;
using CredAbility.Data.MyGoal;
using CredAbility.Data.Validator;
using CredAbility.Web.Authentification;
using CredAbility.Web.UI.WebControls;
using Sitecore.Data.Items;
using ActionResult=System.Web.Mvc.ActionResult;

namespace CredAbility.Web.Controller
{
    public class MyGoalController : CredAbilityBaseController
    {
        protected ClientProfile ClientProfile { get { return ClientProfileManager.CurrentClientProfile; } }

        public ActionResult Index()
        {
            ViewData["currentCulture"] = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            return View("/layouts/MyGoal.aspx");
        }

        public ActionResult UpdateContent(string culture)
        {
            var VD = GoalRepository.GetClientGoals(ClientProfile);
            return View("/Content Controls/MyGoal/Content.ascx", VD);
        }

        public ActionResult AddGoal(string culture)
        {
            ViewData["Type"] =
                new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = Resource.ResourceManager.GetString("Goal_" + GoalType.PayOff.ToString()),
                                Value = ((int) GoalType.PayOff).ToString()
                            },
                        new SelectListItem
                            {
                                Text = Resource.ResourceManager.GetString("Goal_" + GoalType.Save.ToString()),
                                Value = ((int) GoalType.Save).ToString()
                            }
                    };
            var VD = new Core.MyGoal.Goal();
            return View("/Content Controls/MyGoal/AddGoal.ascx", VD);
        }

        [ValidateInput(false)]
        public ActionResult SaveGoal(string culture, Core.MyGoal.Goal goal, FormCollection form)
        {
            DateTime dueDate;
            DateTime.TryParse(form["DueDate"], new CultureInfo("en-us"),DateTimeStyles.None,out dueDate);
            goal.DueDate = dueDate;
            var validator = new GoalValidator();
            
            var actionResult = validator.Validate(goal);
            if(actionResult.IsSuccessful)
            {
                goal.Name = HttpUtility.HtmlEncode(goal.Name);
                GoalRepository.AddGoal(ClientProfile, goal);
                // log activity
                var activity = new UserActivity(ClientProfile.ClientID)
                {
                    Text = string.Format(Resource.UserActivity_AddGoal, goal.Name),
                    Type = UserActivityType.Add
                };

                UserActivityRepository.Save(activity);


                return Content("Successful");
            }
            else
            {
                ViewData["resultMessages"] = new ResultMessages {Messages = actionResult.Messages};
            }
            ViewData["Type"] =
                new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = Resource.ResourceManager.GetString("Goal_" + GoalType.PayOff.ToString()) ,
                                Value = ((int) GoalType.PayOff).ToString()
                            },
                        new SelectListItem
                            {
                                Text = Resource.ResourceManager.GetString("Goal_" + GoalType.Save.ToString()),
                                Value = ((int) GoalType.Save).ToString()
                            }
                    };

            return View("/Content Controls/MyGoal/AddGoal.ascx", goal);
        }

        public ActionResult AddHistory(string culture, int id)
        {
            ViewData["goal"] = GoalRepository.GetClientGoals(ClientProfile).FirstOrDefault(g=>g.Id == id);
            ViewData["history"] = new Core.MyGoal.GoalHistory();
            return View("/Content Controls/MyGoal/AddHistory.ascx");
        }

        [ValidateInput(false)]
        public ActionResult SaveHistory(Core.MyGoal.GoalHistory History, Core.MyGoal.Goal goal, FormCollection form)
        {
            DateTime historyDate;
            DateTime.TryParse(form["History.Date"], new CultureInfo("en-us"), DateTimeStyles.None, out historyDate);
            History.Date = historyDate;
            var goalId = goal.Id;
            goal = GoalRepository.GetClientGoals(ClientProfile).FirstOrDefault(g => g.Id == goalId);

            var validator = new GoalHistoryValidator();

            var actionResult = validator.Validate(History, goal);



            if (actionResult.IsSuccessful)
            {
                GoalRepository.AddHistory(ClientProfile, History, goal);

                UserActivity activity;
                if (goal.RemainingAmount - History.Amount <= 0)
                {
                    // log activity
                    activity = new UserActivity(ClientProfile.ClientID)
                    {
                        Text = string.Format(Resource.UserActivity_CompletedGoal, goal.Name),
                        Type = UserActivityType.Completed
                    };
                    
                }
                else
                {
                    // log activity
                    activity = new UserActivity(ClientProfile.ClientID)
                    {
                        Text = string.Format(Resource.UserActivity_UpdatedGoal, goal.Name),
                        Type = UserActivityType.Updated
                    };

                }


                UserActivityRepository.Save(activity);

                return Content("Successful");
            }
            else
            {
                ViewData["resultMessages"] = new ResultMessages { Messages = actionResult.Messages };
            }
            ViewData["goal"] = goal;
            ViewData["history"] = History;

            return View("/Content Controls/MyGoal/AddHistory.ascx");
        }

        [ValidateInput(false)]
        public ActionResult DeleteGoal(string culture ,Core.MyGoal.GoalHistory History, Core.MyGoal.Goal goal, FormCollection form)
        {
            DateTime historyDate;
            DateTime.TryParse(form["History.Date"], new CultureInfo("en-us"), DateTimeStyles.None, out historyDate);
            History.Date = historyDate;
            var goalId = goal.Id;
            var goalToDelete = GoalRepository.GetClientGoals(ClientProfile).FirstOrDefault(g => g.Id == goalId);

            try
            {
                GoalRepository.DeleteGoal(ClientProfile, goalToDelete);
                return Content("Successful");
            }
            catch(Exception)
            {
                ViewData["resultMessages"] = new ResultMessages { Messages = (ResultMessageList) new List<ResultMessage> { new ResultMessage(ResultMessageLevelType.Error, Resource.Goal_DeleteError, "failed") } };
            }

            ViewData["goal"] = goalToDelete;
            ViewData["history"] = History;

            return View("/Content Controls/MyGoal/AddHistory.ascx");
        }
    }
}