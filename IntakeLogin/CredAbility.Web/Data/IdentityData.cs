﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Data.Identity;
using CredAbility.Web.Cache;

namespace CredAbility.Web.Data
{
    public class IdentityData
    {
        public static List<Core.Identity.SecurityQuestion> GetAllSecurityQuestions(string languageCode)
        {
            string cacheKey = string.Format("IdentityData.GetAllSecurityQuestions.{0}", languageCode);

            if (!CurrentCache.Exists(cacheKey))
            {
                var questions = IdentityRepository.GetAllSecurityQuestions(languageCode);
                CurrentCache.Add(questions, cacheKey);
            }

            return (List<Core.Identity.SecurityQuestion>)CurrentCache.Get(cacheKey);
        }
    }
}
