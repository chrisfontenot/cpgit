﻿
namespace CredAbility.Web.Parsers
{
    public interface IFileParser
    {
        string Parse(string s);
    }
}