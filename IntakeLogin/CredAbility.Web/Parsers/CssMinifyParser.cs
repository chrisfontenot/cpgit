﻿using CredAbility.Web.API;

namespace CredAbility.Web.Parsers
{
    internal class CssMinifyParser : IFileParser
    {
        #region IFileParser Members

        public string Parse(string s)
        {
            var mini = new CssMinifier();
            var outs = mini.Minify(s);
            return outs;
        }

        #endregion
    }
}