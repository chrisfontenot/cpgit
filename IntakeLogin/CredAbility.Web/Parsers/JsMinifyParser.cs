﻿using CredAbility.Web.API;

namespace CredAbility.Web.Parsers
{
    internal class JsMinifyParser : IFileParser
    {
        #region IFileParser Members

        public string Parse(string s)
        {
            var mini = new JavaScriptMinifier();
            var outs = mini.Minify(s);
            return outs;
        }

        #endregion
    }
}