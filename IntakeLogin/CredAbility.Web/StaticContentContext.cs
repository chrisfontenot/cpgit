﻿using System;
using System.Configuration;
using System.Web;

namespace CredAbility.Web
{
    public class StaticContentContext
    {
        private static StaticContentContext _current;
        private const string UnsecureProtocolPrefix = "http://";
        private const string SecureProtocolPrefix = "https://";

        public static StaticContentContext Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new StaticContentContext();
                }

                return _current;
            }
        }

        protected StaticContentContext()
        {
            //var version = ConfigurationManager.AppSettings["static.content.version"];
            //var rootPath = ConfigurationManager.AppSettings["static.content.rootpath"];

            /*if(!string.IsNullOrEmpty(version))
            {
                this._version = version;
            }
            else
            {
                throw new Exception("You must define 'static.content.version' in the configuration file.");
            }

            if (!string.IsNullOrEmpty(rootPath))
            {
                this._rootPath = rootPath;
            }
            else
            {
                throw new Exception("You must define 'static.content.rootpath' in the configuration file.");
            }*/
        }

        //private readonly string _version;
       // private readonly string _rootPath;


        /* public string Version
         {
             get { return this._version; }
         }


         public string RootPath
         {
             get { return this._rootPath; }
         }*/


        public void GenerateSource(string absolutePath)
        {
            var request = HttpContext.Current.Request;
            var result = string.Format("{0}://{1}:{2}{3}", request.Url.Scheme, request.UserHostAddress, request.Url.Port, absolutePath);

            HttpContext.Current.Response.Write(result);
        }


        /*public string FullUserHostAddress
        {
            get
            {
                HttpRequest request = HttpContext.Current.Request;
                string userHost = string.Concat(request.Url.Scheme, "://", request.ServerVariables["REMOTE_ADDR"]);

                if (!request.Url.IsDefaultPort)
                {
                    userHost = string.Concat(userHost, ":", request.Url.Port);
                }
                return userHost;
            }

        }
       

       

        private string AddVersionHasParam(string filePath)
        {
            string[] split = filePath.Split('?');
            string delimiter = (split.Length > 1)? "&" : "?";
            
            return string.Concat(filePath, delimiter,"scv=", this.Version);
        }


        public string GeneratePath(string filePath)
        {
            return GeneratePath(filePath, false, false);
        }

        public string GeneratePath(string filePath, bool versionHasFolder)
        {
            return GeneratePath(filePath, versionHasFolder, false);
        }
 

        public string GeneratePath(string filePath, bool versionHasFolder, bool excludeRooPath)
        {
            string path = this.FullUserHostAddress;

            if(!excludeRooPath)
            {
                path = string.Concat(path, this.RootPath);
            }


            if (versionHasFolder && excludeRooPath)
            {
                path = string.Concat(path, "/", this.Version, filePath);
            }
            else
            {
                path = string.Concat(path, this.AddVersionHasParam(filePath));
            }

            return path;

        }*/
    }
}
