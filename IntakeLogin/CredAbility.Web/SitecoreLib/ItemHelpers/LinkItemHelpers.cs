﻿using System.Collections.Generic;
using System.Linq;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.SitecoreLib.ItemHelpers
{
    public static class LinkItemHelpers
    {
        private static readonly List<string> FolderTemplates = new List<string>()
                                                                 {
                                                                     "Section Folder",
                                                                     "Sub-Section Folder"
                                                                 };

        private static readonly List<string> LinkTemplates = new List<string>()
                                                                 {
                                                                     "External Link",
                                                                     "Internal Link"
                                                                 };

        public static ILinkItem GetFirstSectionFolderParent(this ILinkItem linkItem)
        {
            if (linkItem.Parent.TemplateName == "Folder")
                return linkItem;
            var parentItem = linkItem.GetParent();
            if (parentItem.Name == "Home")
                return linkItem;
            return parentItem.TemplateName == "Section Folder" ? parentItem : parentItem.GetFirstSectionFolderParent();
        }

        public static List<ILinkItem> GetNavigationChild(this ILinkItem linkItem)
        {
            return linkItem.GetChildrens().Where(
                ic => FolderTemplates.Contains(ic.TemplateName) || LinkTemplates.Contains(ic.TemplateName) ||
                ic.ShowInNavigation).ToList();
        }

        public static bool HasNavigationChild(this ILinkItem linkItem)
        {
            return linkItem.GetChildrens().Any(
                ic =>
                LinkTemplates.Contains(ic.TemplateName) ||
                (
                    (FolderTemplates.Contains(ic.TemplateName) ||
                    ic.ShowInNavigation) && ic.GetParent().HisParentOfItem(Sitecore.Context.Item.GetCredAbilityLinkItem())
                )
                );
        }

        public static bool HisParentOfItem(this ILinkItem parent, ILinkItem child )
        {
            if (parent.ID == child.ID ||
                child.Name == "Home" ||
                child.ID.ToString() == "{760AAB26-05CA-4CAF-884F-39CAC23BC281}" /* this is the subdomain home item */ ||
                child.Parent.TemplateName == "Folder")
            {
                return false;
            }
            return parent.ID == child.GetParent().ID ? true:parent.HisParentOfItem(child.GetParent());
        }
    }
}
