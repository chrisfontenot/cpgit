﻿using Sitecore.Data.Items;

namespace CredAbility.Web.SitecoreLib.TemplateHelpers
{
    public static class SitecoreTemplateHelpers
    {
        public static bool InheritFromTemplate(this TemplateItem templateItem, string templatename)
        {
            foreach (var template in templateItem.BaseTemplates)
            {
                if (template.Name == templatename)
                    return true;
                if (template.InheritFromTemplate(templatename))
                    return true;
            }
            return false;
        }
    }
}
