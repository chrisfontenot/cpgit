﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data;

namespace CredAbility.Web.SitecoreLib.Item
{
    public static class ItemHelper
    {
        public static EmailTemplateItem FirstEmailTemplate(this Sitecore.Data.Items.Item item)
        {
            try
            {
                var emailTemplate =
                    item.GetChildren().InnerChildren.FirstOrDefault(i => i.TemplateName == "Email Template");

                return new EmailTemplateItem(emailTemplate);
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
