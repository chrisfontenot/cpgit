﻿using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class PartnerLogoItem : Sitecore.Data.Items.Item, IImageItem
    {
        public PartnerLogoItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {
            Assert.IsNotNull(Fields["Image"], "The 'Image' field value of item : '" + Paths.ContentPath + "' is required.");
            Assert.IsNotNull(((ImageField)Fields["Image"]).MediaItem, "The 'Image' field value of item : '" + Paths.ContentPath + "' point toward an item that doesnt exist.");
            Image = ((ImageField)Fields["Image"]).MediaItem.GetCredAbilityImageItem();
        }

        private IImageItem Image { get; set; }

        #region IImageItem implementation

        public string Width
        {
            get { return Image.Width; }
        }

        public string Height
        {
            get { return Image.Height; }
        }

        public string AltText
        {
            get { return Image.AltText; }
        }

        public string ImageHtmlTag()
        {
            return Image.ImageHtmlTag();
        }

        public string Src
        {
            get { return Image.Src; }
        }

        #endregion
    }
}
