﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Rate;
using CredAbility.Data.Rate;
using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore.Data;
using Sitecore.Data.Fields;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class CourseItem : Sitecore.Data.Items.Item, ICourseItem
    {
        public CourseItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {

            if (this.Fields["Thumbnail"] == null || ((ImageField)Fields["Thumbnail"]).MediaItem == null)
            {
                foreach (Sitecore.Globalization.Language compare in Sitecore.Context.Item.Database.Languages)
                {
                    if (compare.Name == "en")
                    {
                        Thumbnail = ((ImageField)Sitecore.Context.Item.Database.GetItem(this.ID, compare).Fields["Thumbnail"]).MediaItem.GetCredAbilityImageItem();
                    }
                }
            }
            else
            {

                Thumbnail = ((ImageField) Fields["Thumbnail"]).MediaItem.GetCredAbilityImageItem();
            }

        }

        public string Subhead
        {
            get { return Fields["Subhead"].Value; }
        }

        public string Summary
        {
            get { return Fields["Summary"].Value; }
        }

        public DateTime FeaturedStartDate
        {
            get { return ((Sitecore.Data.Fields.DateField) Fields["Featured Start Date"]).DateTime; }
        }

        public DateTime FeaturedEndDate
        {
            get { return ((Sitecore.Data.Fields.DateField)Fields["Featured End Date"]).DateTime; }
        }

        public DateTime Posted
        {
            get { return ((Sitecore.Data.Fields.DateField)Fields["Posted"]).DateTime; }
        }

        public IImageItem Thumbnail { get; protected set; }

        public ItemRate Rate
        {
            get
            {
                ItemRate rate = ItemRateRepository.Get(this.ID.Guid);
                return rate ?? new ItemRate();
            }
        }
     
    }
}
