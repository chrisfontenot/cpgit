﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore.Data;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class PodcastItem : CourseItem, IPodcastItem, IMediaCourseItem
    {
        public PodcastItem(Sitecore.Data.Items.Item item)
            : base(item)
        {
        }

        public string Runtime
        {
            get { return Fields["Runtime"].Value; }
        }

        public string MediaHTMLEmbed
        {
            get { return Fields["Media Embed"].Value; }
        }
    }
}
