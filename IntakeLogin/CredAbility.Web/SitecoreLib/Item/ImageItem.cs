using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore.Diagnostics;
using Sitecore.Resources.Media;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class ImageItem : Sitecore.Data.Items.Item, IImageItem{
        public ImageItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {
            Assert.IsNotNull(Fields["Alt"].Value, "The 'Alt' field value of item : '" + Paths.ContentPath + "' is required.");
            Assert.IsNotNull(Fields["Width"].Value, "The 'Width' field value of item : '" + Paths.ContentPath + "' is required.");
            Assert.IsNotNull(Fields["Height"].Value, "The 'Height' field value of item : '" + Paths.ContentPath + "' is required.");
        }

        public string Width
        {
            get { return Fields["Width"].Value; }
        }

        public string Height
        {
            get { return Fields["Height"].Value; }
        }

        public string AltText
        {
            get { return Fields["Alt"].Value; }
        }

        public string ImageHtmlTag()
        {
            return "<img src=\"" + Src + "\" alt=\"" + AltText + "\" width=\"" + Width + "\" height=\"" + Height + "\" />";
        }

        public string Src
        {
            get { return MediaManager.GetMediaUrl(this); }
        }
    }
}