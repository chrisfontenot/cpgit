﻿using System;
using System.Collections.Generic;
using Sitecore.Links;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class LinkItem : Sitecore.Data.Items.Item, ILinkItem
    {
        
        public LinkItem(Sitecore.Data.Items.Item item) : base(item.ID,item.InnerData,item.Database)
        {
        }

        public virtual string Url
        {
            get { return Fields["Url"] != null ? Fields["Url"].Value : LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(Fields["Linked Item"].Value)); }
        }

        public virtual string Text
        {
            get { return Fields["Text"].Value; }
        }

        public virtual string Nofollow
        {
            get
            {
                var nofollow = false;
                if(Fields["nofollow"].Value == "1")
                    nofollow = true;
                return nofollow?" rel=\"nofollow\"":"";
            }
        }

        #region ILinkItem Implementation

        public bool ShowInNavigation
        {
            get
            {
                var value = Fields["Section Nav"] != null ? Fields["Section Nav"].Value : "0";
                if (value == "1")
                    return true;
                return false;
            }
        }

        public ILinkItem GetParent()
        {
            return Parent.GetCredAbilityLinkItem();
        }

        public List<ILinkItem> GetChildrens()
        {
            var result = new List<ILinkItem>();
            foreach (var child in Children.InnerChildren)
            {
                var item = child.GetNullableCredAbilityLinkItem();
                if (item != null)
                    result.Add(item);
            }
            return result;
        }

        public virtual ILinkItem LinkedItem
        {
            get { return this; }
        }

        public string HyperLinkHtml()
        {
            return String.Format("<a href=\"{0}\"{1}>{2}</a>",Url,Nofollow,Text);
        }

        #endregion
    }
}
