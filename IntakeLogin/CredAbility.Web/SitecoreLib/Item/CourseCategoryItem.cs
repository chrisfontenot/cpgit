﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data;
using Sitecore.Links;

namespace CredAbility.Web.SitecoreLib.Item
{  
    public class CourseCategoryItem : Sitecore.Data.Items.Item
    {
        public CourseCategoryItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {
        }


        public string Text
        {
            get { return Fields["Text"].Value; }

        }

        public string LandingPage
        {
            get
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    string itemUrl = LinkManager.GetItemUrl(((Sitecore.Data.Fields.InternalLinkField) (Fields["Course Category Item"])).TargetItem);

                    //split to get only the page name without folders
                    return itemUrl.Substring(itemUrl.LastIndexOf("/") + 1, itemUrl.Length - itemUrl.LastIndexOf("/") - 1);    
                }
            }

        }
    }
}
