﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class SubSectionFolderItem : WebFolderItem
    {
        public SubSectionFolderItem(Sitecore.Data.Items.Item item)
            : base(item)
        {
            Assert.IsNotNull(Fields["Text"], "The 'Text' field of item : '" + Paths.ContentPath + "' doesnt exist and is required.");
            Assert.IsNotNull(Fields["Landing Page"], "The 'Landing Page' field of item : '" + Paths.ContentPath + "' doesnt exist and is required.");
            Assert.IsNotNull(Context.Database.GetItem(Fields["Landing Page"].Value), "The 'Landing Page' field value of item : '" + Paths.ContentPath + "' point toward an item that doesnt exist.");
        }

        public override string FolderName
        {
            get { return Fields["Text"].Value; }
        }

        public override string LandingPageUrl
        {
            get
            {
                return LinkManager.GetItemUrl(Context.Database.GetItem(Fields["Landing Page"].Value));
            }
        }
    }
}
