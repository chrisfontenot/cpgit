﻿using System;
using CredAbility.Web.SitecoreLib.Item.Interfaces;
using CredAbility.Web.SitecoreLib.TemplateHelpers;

namespace CredAbility.Web.SitecoreLib.Item
{
    public static class CredAbilityItemFactory
    {
        public static ILinkItem GetNullableCredAbilityLinkItem(this Sitecore.Data.Items.Item item)
        {
            try
            {
                return item.GetCredAbilityLinkItem();
            }
            catch(InvalidCastException)
            {
                return null;
            }
        }

        public static ILinkItem GetCredAbilityLinkItem(this Sitecore.Data.Items.Item item)
        {
            switch (item.TemplateName)
            {
                case "External Link":
                    return new ExternalLinkItem(item);
                case "Internal Link":
                    return new InternalLinkItem(item);
                case "Section Folder":
                    return new SectionFolderItem(item);
                case "Sub-Section Folder":
                    return new SubSectionFolderItem(item);
            }
            //Check if the item template inherit from a Page Base template
            if (item.Template.InheritFromTemplate("Page Base"))
                return new PageItem(item);
            throw new InvalidCastException("The item: " + item.Name + ", is not a valid ILinkItem");
        }

        public static IImageItem GetCredAbilityImageItem(this Sitecore.Data.Items.Item item)
        {
            switch (item.TemplateName)
            {
                case "Logo":
                    return new PartnerLogoItem(item);
                case "Image":
                    return new ImageItem(item);
                
            }

            if (item.Template != null && item.Template.InheritFromTemplate("Image"))
            {
                return new ImageItem(item);
            }

            throw new InvalidCastException("The item: " + item.Name + ", is not a valid IImageItem");
        }

        public static IImageItem GetNullableCredAbilityImageItem(this Sitecore.Data.Items.Item item)
        {
            try
            {
                return item.GetCredAbilityImageItem();
            }
            catch (InvalidCastException)
            {
                return null;
            }
        }

        public static MediaFolderItem GetCredAbilityMediaFolderItem(this Sitecore.Data.Items.Item item)
        {
            switch (item.TemplateName)
            {
                case "Media folder":
                    return new MediaFolderItem(item);
            }

            throw new InvalidCastException("The item: " + item.Name + ", is not a valid MediaFolderItem");
        }

        public static CourseItem GetCredAbilityCourseItem(this Sitecore.Data.Items.Item item)
        {
            switch (item.TemplateName)
            {
                case "Article":
                    return new ArticleItem(item);
                case "Online Video":
                    return new OnlineVideoItem(item);
                case "Paid Offering":
                    return new PaidOfferingItem(item);
                case "Class on Demand":
                    return new ClassOnDemandItem(item);
                case "Podcast":
                    return new PodcastItem(item);
                case "Webinar":
                    return new WebinarItem(item);
                case "Course":  
                    return new CourseItem(item);
            }

            throw new InvalidCastException("The item: " + item.Name + ", is not a valid CourseItem");
        }


    }
}
