﻿using System.Collections.Generic;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class FolderItem : Sitecore.Data.Items.Item
    {
        public FolderItem(Sitecore.Data.Items.Item item): base(item.ID, item.InnerData, item.Database)
        {
        }

        public Sitecore.Data.Items.Item GetParent()
        {
            return Parent;
        }

        public List<Sitecore.Data.Items.Item> GetChildrens()
        {
            return Children.InnerChildren;
        }
    }
}
