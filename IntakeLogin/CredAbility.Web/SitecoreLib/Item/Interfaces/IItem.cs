﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.Item.Interfaces
{
    public interface IItem
    {
        Sitecore.Data.ID ID { get; }
        string Name { get; }
        string TemplateName { get; }
    }
}
