﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.Item.Interfaces
{
    public interface ICourseItem : IItem
    {
        DateTime FeaturedStartDate { get; }
        DateTime FeaturedEndDate { get; }
        DateTime Posted { get; }
        string Subhead { get; }
        string Summary { get; }
    }
}
