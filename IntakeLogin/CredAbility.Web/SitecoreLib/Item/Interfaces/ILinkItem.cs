﻿using System.Collections.Generic;

namespace CredAbility.Web.SitecoreLib.Item.Interfaces
{
    public interface ILinkItem : IItem
    {
        bool ShowInNavigation { get; }
        ILinkItem GetParent();
        List<ILinkItem> GetChildrens();
        ILinkItem LinkedItem { get; }
        Sitecore.Data.Items.Item Parent { get; }
        string HyperLinkHtml();
    }
}
