﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Data.Items;

namespace CredAbility.Web.SitecoreLib.Item.Interfaces
{
    public interface IMediaCourseItem
    {
        string Runtime { get; }
        string MediaHTMLEmbed { get; }
    }
}
