﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.Item.Interfaces
{
    public interface IImageItem : IItem
    {
        string Width { get; }
        string Height { get; }
        string AltText { get; }
        string ImageHtmlTag();
        string Src { get; }
    }
}
