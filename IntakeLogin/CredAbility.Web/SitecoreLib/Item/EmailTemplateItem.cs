using System;
using System.Net.Mail;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class EmailTemplateItem: Sitecore.Data.Items.Item
	{
        public EmailTemplateItem(Sitecore.Data.Items.Item item) : base(item.ID, item.InnerData, item.Database)
        {
            if(item.TemplateName != "Email Template")
            {
                throw new ArgumentException(string.Format("The item ({0}) with template name = \"{1}\" is not a valid item for EmailTemplateItem. Make sure you use an item with \"Email Template\"",item.Name, item.TemplateName));
            }
        }

        #region Properties

        public string Subject
        {
            get { return Fields["Subject"].Value; }
        }

        public string Body
        {
            get { return Fields["Body"].Value; }
        }

        public MailAddressCollection Receivers
        {
            get
            {
                var toAddresses = new MailAddressCollection();
                var receivers = Fields["Receiver email"].Value.Split(';');
                foreach (var receiver in receivers)
                {
                    toAddresses.Add(new MailAddress(receiver));
                }
                return toAddresses;
            }
        }

        public MailAddress Sender
        {
            get
            {
                if (!string.IsNullOrEmpty(Fields["Sender email"].Value))
                {

                    return new MailAddress(Fields["Sender email"].Value);
                }
                return null;
            }
        }

        #endregion
    }
}