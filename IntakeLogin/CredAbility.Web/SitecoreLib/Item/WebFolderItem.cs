﻿using System;
using System.Collections.Generic;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class WebFolderItem : FolderItem, ILinkItem
    {
        public WebFolderItem(Sitecore.Data.Items.Item item)
            : base(item)
        {
        }

        public virtual string FolderName
        {
            get { return DisplayName; }
        }

        public virtual string LandingPageUrl
        {
            get
            {
                return string.Empty;
            }
        }

        #region ILinkItem Implementation

        public bool ShowInNavigation
        {
            get
            {
                var value = Fields["Section Nav"] != null ? Fields["Section Nav"].Value : "0";
                return value == "1";
            }
        }

        public new ILinkItem GetParent()
        {
            return Parent.GetCredAbilityLinkItem();
        }

        public new List<ILinkItem> GetChildrens()
        {
            var result = new List<ILinkItem>();
            foreach (var child in Children.InnerChildren)
            {
                var item = child.GetNullableCredAbilityLinkItem();
                if (item != null)
                    result.Add(item);
            }
            return result;
        }

        public ILinkItem LinkedItem
        {
            get { return this; }
        }

        public string HyperLinkHtml()
        {
            return String.Format("<a href=\"{0}\">{1}</a>", LandingPageUrl, FolderName);
        }

        #endregion
        
    }
}
