using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore;
using Sitecore.Links;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class InternalLinkItem : LinkItem
    {
        public InternalLinkItem(Sitecore.Data.Items.Item item) : base(item)
        {
        }

        public override string Url
        {
            get
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    return LinkManager.GetItemUrl(Context.Database.GetItem(Fields["Linked Item"].Value));
                }
            }
        }

        public override ILinkItem LinkedItem
        {
            get
            {
                var linkedItem = Context.Database.GetItem(Fields["Linked Item"].Value);
                if (linkedItem != null)
                    return Context.Database.GetItem(Fields["Linked Item"].Value).GetCredAbilityLinkItem();
                return null;
            }
        }
    }
}