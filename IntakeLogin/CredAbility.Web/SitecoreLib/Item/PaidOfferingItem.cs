﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Web.SitecoreLib.Item.Interfaces;
using Sitecore.Data;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class PaidOfferingItem : CourseItem
    {
        public PaidOfferingItem(Sitecore.Data.Items.Item item) : base(item)
        {
        }
    }
}
