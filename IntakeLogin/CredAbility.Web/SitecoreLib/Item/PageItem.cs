﻿using System;
using System.Collections.Generic;
using Sitecore.Links;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class PageItem : Sitecore.Data.Items.Item, ILinkItem
    {
        public PageItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {
        }

        public string Url 
        { 
            get 
            {
                return LinkManager.GetItemUrl(this);
            }
        }

        public string Text
        {
            get { return Fields["Text"].Value; }
        }

        public string Content
        {
            get { return Fields["Content"].Value; }
        }

        #region ILinkItem Implementation

        public bool ShowInNavigation
        {
            get
            {
                var value = Fields["Section Nav"] != null ? Fields["Section Nav"].Value : "0";
                return value == "1";
            }
        }

        public ILinkItem GetParent()
        {
            return Parent.GetCredAbilityLinkItem();
        }

        public List<ILinkItem> GetChildrens()
        {
            var result = new List<ILinkItem>();
            foreach (var child in Children.InnerChildren)
            {
                var item = child.GetNullableCredAbilityLinkItem();
                if (item != null)
                    result.Add(item);
            }
            return result;
        }

        public ILinkItem LinkedItem
        {
            get { return this; }
        }

        public string HyperLinkHtml()
        {
            return String.Format("<a href=\"{0}\">{1}</a>", Url, Text);
        }

        #endregion
    }
}
