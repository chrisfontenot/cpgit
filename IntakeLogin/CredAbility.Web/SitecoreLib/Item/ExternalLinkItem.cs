namespace CredAbility.Web.SitecoreLib.Item
{
    public class ExternalLinkItem : LinkItem
    {
        public ExternalLinkItem(Sitecore.Data.Items.Item item) : base(item)
        {
        }

        public override string Url
        {
            get { return Fields["Url"].Value; }
        }
    }
}