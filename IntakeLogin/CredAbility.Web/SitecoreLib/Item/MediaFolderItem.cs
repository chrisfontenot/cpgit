﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class MediaFolderItem : Sitecore.Data.Items.Item
    {
        public MediaFolderItem(Sitecore.Data.Items.Item item): base(item.ID, item.InnerData, item.Database)
        {
        }

        public List<IImageItem> GetImageItems()
        {
            var result = new List<IImageItem>();
            foreach (var child in Children.InnerChildren)
            {
                var item = child.GetCredAbilityImageItem();
                if (item != null)
                    result.Add(item);
            }
            return result;
        }

        public IImageItem GetRandomImage()
        {

            var list = GetImageItems();

            if (list.Count == 0)
            {
                return null;

            }

            Random random = new Random();

            var number = random.Next(list.Count);

            return list[number];
        }
    }
}
