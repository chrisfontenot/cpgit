﻿using System;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;

namespace CredAbility.Web.SitecoreLib.Item
{
    public class LanguageItem : Sitecore.Data.Items.Item
    {
        public LanguageItem(Sitecore.Data.Items.Item item)
            : base(item.ID, item.InnerData, item.Database)
        {
            Assert.IsNotNull(Fields["Display Name"], "The 'Display' field of item : '" + Paths.ContentPath + "' doesnt exist and is required.");
            Assert.IsNotNull(Fields["Iso"], "The 'Iso' field of item : '" + Paths.ContentPath + "' doesnt exist and is required.");
        }

        public string Text
        {
            get { return Fields["Display Name"].Value; }
        }

        public string Culture
        {
            get { return Fields["Iso"].Value; }
        }

        public string Url
        {
            get
            {
                var urlOpts = LinkManager.GetDefaultUrlOptions();
                if (Lang != null)
                    urlOpts.Language = Lang;
                return LinkManager.GetItemUrl(Context.Item, urlOpts);
            }
        }

        public Sitecore.Globalization.Language Lang
        {
            get
            {
                Sitecore.Globalization.Language lang;
                Sitecore.Globalization.Language.TryParse(Culture, out lang);
                return lang;
            }
        }

        public string GetLinkHtml()
        {
            return String.Format("<a href=\"{0}\">{1}</a>", Url, Text);
        }

    }
}
