﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;
using CredAbility.Core.Rate;
using CredAbility.Data.Rate;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Data;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.SitecoreLib.TemplateHelpers;
using Sitecore.Data.Fields;
using Sitecore.Security.Accounts;

namespace CredAbility.Web.SitecoreLib.Data
{
    public static class CourseItemRepository
    {
        public static CourseItemList GetAll()
        {
            var courseList = new CourseItemList();
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                var folderItem = new FolderItem(Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.Course.SitecorePath));

                var items = folderItem.GetChildrens().Where(i => i.Template.InheritFromTemplate("Course"));

                foreach (var item in items)
                {
                    courseList.Add(item.GetCredAbilityCourseItem());
                }
            }

            return courseList;
        }

       
    }
}
