﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.SitecoreLib.TemplateHelpers;

namespace CredAbility.Web.SitecoreLib.Data
{
    public static class CourseCategoryItemRepository
    {
        public static List<CourseCategoryItem> GetAll()
        {
            var result = new List<CourseCategoryItem>();
            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                var folderItem =
                    new FolderItem(
                        Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.CourseCategory.SitecorePath));

                var items = folderItem.GetChildrens().Where(i => i.TemplateName == "Course Category Ref");

                foreach (var item in items)
                {
                    result.Add(new CourseCategoryItem(item));
                }
            }

            return result;
        }
    }
}
