using System;
using System.Collections.Generic;
using System.Globalization;
using CredAbility.Web.Cache;
using CredAbility.Web.SitecoreLib.Item;

namespace CredAbility.Web.SitecoreLib.Data
{
	public static class CourseCategoryItemRepositoryProxy
	{
		private const string GETALL_CACHE = "CourseCategoryItemRepositoryProxyGetALL";
		private const int REMAINING_IN_CACHE = 5;

		public static List<CourseCategoryItem> GetAll()
		{
			var courseList = new CourseItemList();
			var cacheKey = string.Concat(GETALL_CACHE, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);
			if(!CurrentCache.Exists(cacheKey))
			{
				System.Web.HttpContext.Current.Cache.Insert(cacheKey, CourseCategoryItemRepository.GetAll(), null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(REMAINING_IN_CACHE));
			}
			return (List<CourseCategoryItem>)CurrentCache.Get(cacheKey);
		}
	}
}