﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Web.SitecoreLib.Item;

namespace CredAbility.Web.SitecoreLib.Data
{
    public class CourseItemList : List<CourseItem>
    {
        public bool HasFeaturedCourses
        {
            get{
                return (this.GetFeatured().Count > 0);
            }
        }

        public CourseItemList GetFeatured()
        {
            var list = new CourseItemList();
            foreach (CourseItem item in this)
            {
                if (item.FeaturedStartDate <= DateTime.Today && item.FeaturedEndDate >= DateTime.Today)
                {
                    list.Add(item);
                }

            }

            return list;

        }


        public CourseItemList GetByCourseType(Type courseType)
        {
            var list = new CourseItemList();
            foreach (CourseItem item in this)
            {
                if(item.GetType() == courseType)
                {
                    list.Add(item);
                }
                
            }

            return list;
        }

        #region Sorting Functionnalities

        public void SortBy(OrderBy sortBy)
        {
            switch (sortBy)
            {
                case OrderBy.MostRecent: SortByMostRecent();
                    break;
                case OrderBy.MostPopular: SortByMostPopular();
                    break;
                default: SortByAlphabetical();
                    break;
            }
        }

        private void SortByAlphabetical()
        {
            this.Sort((item1, item2) => item1.Subhead.CompareTo(item2.Subhead));
        }

        private void SortByMostRecent()
        {
            this.Sort((item1, item2) => -(item1.Posted.CompareTo(item2.Posted)));
        }

        private void SortByMostPopular()
        {
            this.Sort((item1, item2) => -(item1.Rate.CompareTo(item2.Rate)));
        }

        #endregion

        public enum OrderBy { Alphabetical,MostRecent, MostPopular }
    }
}
