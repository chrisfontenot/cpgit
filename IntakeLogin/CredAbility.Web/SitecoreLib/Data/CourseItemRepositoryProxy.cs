﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Web.Cache;
using CredAbility.Web.SitecoreLib.Item;
using Sitecore.Data.Fields;
using Sitecore.Security.Accounts;

namespace CredAbility.Web.SitecoreLib.Data
{
    public sealed class CourseItemRepositoryProxy
    {
        private const string GETALL_CACHE = "CourseItemProxyGetALL-";
        private const int REMAINING_IN_CACHE = 5;

        public static CourseItemList GetAll()
        {
            var cacheKey = string.Concat(GETALL_CACHE, CultureInfo.CurrentUICulture.TwoLetterISOLanguageName);

            if (!CurrentCache.Exists(cacheKey))
            {
                System.Web.HttpContext.Current.Cache.Insert(cacheKey, CourseItemRepository.GetAll(), null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(REMAINING_IN_CACHE));
            }
            return (CourseItemList)CurrentCache.Get(cacheKey); 
        }

        public static CourseItemList GetAllByCategory(Sitecore.Data.ID categoryItemId)
        {
            var courseList = new CourseItemList();
            var list = CourseItemRepositoryProxy.GetAll();
            var items = list.Where(i => (((MultilistField)i.Fields["Category"]).GetItems().Any(c => c.ID == categoryItemId)));

            foreach (var item in items)
            {
                courseList.Add(item.GetCredAbilityCourseItem());
            }

            return courseList;
        }
      

        public static CourseItemList GetAllByRole(List<Role> clientRoles)
        {
            //used to store categories in
            var categories = new List<Sitecore.Data.ID>();

            //will grab the Dictionary of roles and categories
            var rolesToCategory = CourseCategoryRoles.GetRolesCategories();

            foreach (var clientRole in clientRoles)
            {
                if (!rolesToCategory.ContainsKey(clientRole))
                    continue;
                foreach (var category in rolesToCategory[clientRole])
                {
                    if (categories.Contains(category))
                        continue;
                    categories.Add(category);
                }
            }

            //used to store courses in
            var courses = new CourseItemList();

            foreach (var category in categories)
            {
                foreach (var course in GetAllByCategory(category))
                {
                    if (courses.Contains(course))
                        continue;
                    courses.Add(course);
                }
            }

            return courses;
        }

    }
}
