﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;
using CredAbility.Web.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Security.Accounts;

namespace CredAbility.Web.SitecoreLib.Data
{
    class CourseCategoryRoles
    {
        public static Dictionary<Role, List<Sitecore.Data.ID>> GetRolesCategories()
        {
            var rolesToCategories = new Dictionary<Role, List<Sitecore.Data.ID>>();
            foreach (RoleElement role in ConfigReader.ConfigSection.RecommendedContent)
            {
                var SCRole = Role.FromName(role.Name);
                rolesToCategories.Add(SCRole,new List<ID>());
                foreach (CategoryElement category in role.Categories)
                {
                    var id = new ID(category.Id);
                    if (rolesToCategories[SCRole].Contains(id))
                        continue;
                    rolesToCategories[SCRole].Add(id);
                }
            }

            return rolesToCategories;
        }

    }
}
