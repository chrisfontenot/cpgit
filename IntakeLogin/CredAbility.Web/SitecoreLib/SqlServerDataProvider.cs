﻿
using System.Linq;
using Sitecore.Collections;
using Sitecore.Data;

namespace CredAbility.Web.SitecoreLib
{
        public class SqlServerDataProvider : Sitecore.Data.SqlServer.SqlServerDataProvider
    {

        public SqlServerDataProvider(string connectionString)
            : base(connectionString)
        {
        }
        protected override void LoadItemFields(string itemCondition, string fieldCondition, object[] parameters, Sitecore.Collections.SafeDictionary<ID, Sitecore.Data.DataProviders.PrefetchData> prefetchData)
        {
            base.LoadItemFields(itemCondition, fieldCondition, parameters, prefetchData);

            //set the default language to use
            Sitecore.Globalization.Language defaultLang;
            Sitecore.Globalization.Language.TryParse("en", out defaultLang);

            //full list of languages the site uses
            LanguageCollection coll = this.GetLanguages();

            foreach (var data in prefetchData)
            {
                if (data.Value != null)
                {
                    var uris = data.Value.GetVersionUris();
                    //check the item contains the default language before doing anything
                    if (uris.Cast<VersionUri>().Any(x => x.Language == defaultLang))
                    {
                        //get the default language fields 
                        var defaultUri = uris.Cast<VersionUri>().First(x => x.Language == defaultLang);
                        //loop through each language
                        foreach (Sitecore.Globalization.Language lang in coll)
                        {
                            //check if language doesn't exist
                            if (!uris.Cast<VersionUri>().Any(x => x.Language == lang))
                            {
                                //update language with default language values
                                FieldList fields = data.Value.GetFieldList("en", defaultUri.Version.Number);
                                foreach (var key in fields.GetFieldIDs())
                                {
                                    data.Value.AddField(lang.Name, 1, key, fields[key]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
