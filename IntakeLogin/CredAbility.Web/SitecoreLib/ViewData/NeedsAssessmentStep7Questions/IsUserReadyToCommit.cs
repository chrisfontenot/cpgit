﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep7Questions
{
    public class IsUserReadyToCommit : NeedsAssessmentSliderQuestionBase
    {

        public static IsUserReadyToCommit ReadyAndWilling { get { return new AReadyAndWilling(); } }
        public static IsUserReadyToCommit Somewhat { get { return new ASomewhat(); } }
        public static IsUserReadyToCommit Maybe { get { return new AMaybe(); } }
        public static IsUserReadyToCommit Unsure { get { return new AUnsure(); } }

        public static int Count() {
            return 4;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step7_Commitment;
        }

        public static IsUserReadyToCommit GetAnswer(int value)
        {
            return
                typeof(IsUserReadyToCommit).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new IsUserReadyToCommit(), null) as IsUserReadyToCommit).Value == value).GetValue(new IsUserReadyToCommit(), null)
                as IsUserReadyToCommit;
        }


        #region Nested class

        protected class AReadyAndWilling : IsUserReadyToCommit
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_Commitment_Ready; }
            }
        }

        protected class ASomewhat : IsUserReadyToCommit
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_Commitment_Somewhat; }
            }
        }

        protected class AMaybe : IsUserReadyToCommit
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_Commitment_Maybe; }
            }
        }

        protected class AUnsure : IsUserReadyToCommit
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_Commitment_Unsure; }
            }
        }
        #endregion

    }


}
