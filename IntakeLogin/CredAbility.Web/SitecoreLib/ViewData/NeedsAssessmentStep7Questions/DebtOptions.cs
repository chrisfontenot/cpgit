﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep7Questions
{
    public class DebtOptions : NeedsAssessmentSliderQuestionBase
    {

        public static DebtOptions CreditCounseling { get { return new ACreditCounseling(); } }
        public static DebtOptions DMP { get { return new ADMP(); } }
        public static DebtOptions BankruptcyFiling { get { return new ABankruptcyFiling(); } }
        public static DebtOptions DebtSettlementDeal { get { return new ADebtSettlementDeal(); } }
        public static DebtOptions DebtSettlementLoan { get { return new ADebtSettlementLoan(); } }
        public static DebtOptions FinancialAndDebtEducation { get { return new AFinancialAndDebtEducation(); } }
        public static DebtOptions SelfManagement { get { return new ASelfManagement(); } }
        public static DebtOptions None { get { return new ANone(); } }

        public static int Count() {
            return 8;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step7_DebtOptions;
        }

        public static List<DebtOptions> GetAnswer(string value)
        {
            var resultList = new List<DebtOptions>();
            var test = value.Split(',');
            foreach (var s in test)
            {
                var stringValue = 0;
                if (s == "false" || string.IsNullOrEmpty(s))
                    continue;

                if (!int.TryParse(s, out stringValue))
                    continue;

                    resultList.Add(typeof (DebtOptions).GetProperties().FirstOrDefault(
                                   p =>
                                   (p.GetValue(new DebtOptions(), null) as DebtOptions).Value == stringValue).
                                   GetValue(new DebtOptions(), null)
                               as DebtOptions);
            }
            return resultList;
                
        }


        #region Nested class

        protected class ANone : DebtOptions
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_None; }
            }
        }

        protected class ASelfManagement : DebtOptions
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_SelfManagement; }
            }
        }

        protected class AFinancialAndDebtEducation : DebtOptions
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_FinancialEducation; }
            }
        }

        protected class ADebtSettlementLoan : DebtOptions
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_DebtLoan; }
            }
        }

        protected class ADebtSettlementDeal : DebtOptions
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_DebtDeal; }
            }
        }

        protected class ABankruptcyFiling : DebtOptions
        {
            public override int Value
            {
                get { return 6; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_Bankcruptcy; }
            }
        }

        protected class ADMP : DebtOptions
        {
            public override int Value
            {
                get { return 7; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_DMP; }
            }
        }

        protected class ACreditCounseling : DebtOptions
        {
            public override int Value
            {
                get { return 8; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7_DebtOptions_CreditCounseling; }
            }
        }
        #endregion

    }


}
