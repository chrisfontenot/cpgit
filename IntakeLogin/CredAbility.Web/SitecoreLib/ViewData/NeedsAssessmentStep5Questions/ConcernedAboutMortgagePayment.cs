﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep5Questions
{
    public class ConcernedAboutMortgagePayment : NeedsAssessmentSliderQuestionBase
    {

        public static ConcernedAboutMortgagePayment Extremely { get { return new AExtremely(); } }
        public static ConcernedAboutMortgagePayment Very { get { return new AVery(); } }
        public static ConcernedAboutMortgagePayment Somewhat { get { return new ASomewhat(); } }
        public static ConcernedAboutMortgagePayment NotVery { get { return new ANotVery(); } }
        public static ConcernedAboutMortgagePayment NotAtAll { get { return new ANotAtAll(); } }

        public static int Count() {
            return 5;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step5_ConcernedMortgagePayment;
        }

        public static ConcernedAboutMortgagePayment GetAnswer(int value)
        {
            return
                typeof(ConcernedAboutMortgagePayment).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new ConcernedAboutMortgagePayment(), null) as ConcernedAboutMortgagePayment).Value == value).GetValue(new ConcernedAboutMortgagePayment(), null)
                as ConcernedAboutMortgagePayment;
        }


        #region Nested class

        protected class AExtremely : ConcernedAboutMortgagePayment
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_ConcernedMortgagePayment_Extremely; }
            }
        }

        protected class AVery : ConcernedAboutMortgagePayment
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_ConcernedMortgagePayment_Very; }
            }
        }

        protected class ASomewhat : ConcernedAboutMortgagePayment
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_ConcernedMortgagePayment_Somewhat; }
            }
        }

        protected class ANotVery : ConcernedAboutMortgagePayment
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_ConcernedMortgagePayment_NotVery; }
            }
        }

        protected class ANotAtAll : ConcernedAboutMortgagePayment
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_ConcernedMortgagePayment_NotAtAll; }
            }
        }
        #endregion

    }


}
