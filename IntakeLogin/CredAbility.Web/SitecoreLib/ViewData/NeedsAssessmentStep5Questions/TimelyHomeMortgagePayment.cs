﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep5Questions
{
    public class TimelyHomeMortgagePayment : NeedsAssessmentSliderQuestionBase
    {

        public static TimelyHomeMortgagePayment Behing60Days { get { return new ABehing60Days(); } }
        public static TimelyHomeMortgagePayment Behing30Days { get { return new ABehing30Days(); } }
        public static TimelyHomeMortgagePayment AbitLate { get { return new AAbitLate(); } }
        public static TimelyHomeMortgagePayment UsuallyOnTime { get { return new AUsuallyOnTime(); } }
        public static TimelyHomeMortgagePayment AlwaysOnTime { get { return new AAlwaysOnTime(); } }

        public static int Count() {
            return 5;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step5_TimelyMortgagePayment;
        }


        public static TimelyHomeMortgagePayment GetAnswer(int value)
        {
            return
                typeof(TimelyHomeMortgagePayment).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new TimelyHomeMortgagePayment(), null) as TimelyHomeMortgagePayment).Value == value).GetValue(new TimelyHomeMortgagePayment(), null)
                as TimelyHomeMortgagePayment;
        }


        #region Nested class

        protected class ABehing60Days : TimelyHomeMortgagePayment
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_TimelyMortgagePayment_60day; }
            }
        }

        protected class ABehing30Days : TimelyHomeMortgagePayment
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_TimelyMortgagePayment_30day; }
            }
        }

        protected class AAbitLate : TimelyHomeMortgagePayment
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_TimelyMortgagePayment_Late; }
            }
        }

        protected class AUsuallyOnTime : TimelyHomeMortgagePayment
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_TimelyMortgagePayment_Usually; }
            }
        }

        protected class AAlwaysOnTime : TimelyHomeMortgagePayment
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5_TimelyMortgagePayment_Alway; }
            }
        }
        #endregion

    }


}
