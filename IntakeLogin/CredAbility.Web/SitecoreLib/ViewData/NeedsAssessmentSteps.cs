﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CredAbility.Web.SitecoreLib.ViewData
{
    public class NeedsAssessmentSteps : StepBase
    {
        public static NeedsAssessmentSteps Step1 { get { return new NeedsAssessmentStep1(); } }
        public static NeedsAssessmentSteps Step2 { get { return new NeedsAssessmentStep2(); } }
        public static NeedsAssessmentSteps Step3 { get { return new NeedsAssessmentStep3(); } }
        public static NeedsAssessmentSteps Step4 { get { return new NeedsAssessmentStep4(); } }
        public static NeedsAssessmentSteps Step5 { get { return new NeedsAssessmentStep5(); } }
        public static NeedsAssessmentSteps Step6 { get { return new NeedsAssessmentStep6(); } }
        public static NeedsAssessmentSteps Step7 { get { return new NeedsAssessmentStep7(); } }
        public static NeedsAssessmentSteps Step8 { get { return new NeedsAssessmentStep8(); } }
    
        public static NeedsAssessmentSteps GetStep(int step)
        {
            return typeof(NeedsAssessmentSteps).GetProperty("Step" + step).GetValue(new NeedsAssessmentSteps(), null) as NeedsAssessmentSteps;
        }
        #region Nested class

        protected class NeedsAssessmentStep1 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step1HeadText; }
            }

            public override string ActionName
            {
                get { return "Step1"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step1.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step1"; }
            }
        }

        protected class NeedsAssessmentStep2 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2HeadText; }
            }

            public override string ActionName
            {
                get { return "Step2"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step2.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step2"; }
            }
        }

        protected class NeedsAssessmentStep3 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step3HeadText; }
            }

            public override string ActionName
            {
                get { return "Step3"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step3.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step3"; }
            }
        }

        protected class NeedsAssessmentStep4 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4HeadText; }
            }

            public override string ActionName
            {
                get { return "Step4"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step4.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step4"; }
            }
        }

        protected class NeedsAssessmentStep5 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step5HeadText; }
            }

            public override string ActionName
            {
                get { return "Step5"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step5.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step5"; }
            }
        }

        protected class NeedsAssessmentStep6 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 6; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step6HeadText; }
            }

            public override string ActionName
            {
                get { return "Step6"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step6.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step6"; }
            }
        }

        protected class NeedsAssessmentStep7 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 7; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step7HeadText; }
            }

            public override string ActionName
            {
                get { return "Step7"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step7.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step7"; }
            }
        }

        protected class NeedsAssessmentStep8 : NeedsAssessmentSteps
        {
            public override int Value
            {
                get { return 8; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step8HeadText; }
            }

            public override string ActionName
            {
                get { return "Step8"; }
            }

            public override string ViewUrl
            {
                get { return "/Content Controls/NeedsAssessment/Step8.ascx"; }
            }

            public override string ClassName
            {
                get { return "Step8"; }
            }
        }



        #endregion

    }

    public class StepBase
    {
        public virtual int Value
        {
            get { return 0; }
        }

        public virtual string Text
        {
            get { return ""; }
        }

        public virtual string ActionName
        {
            get { return ""; }
        }

        public virtual string ViewUrl
        {
            get { return ""; }
        }

        public virtual string ClassName
        {
            get { return ""; }
        }

        #region Operators
        public static bool operator ==(StepBase first, StepBase second)
        {
            if (ReferenceEquals(first, second))
            {
                return true;
            }

            if (ReferenceEquals(first, null) || ReferenceEquals(second, null))
            {
                return false;
            }

            return first.Value == second.Value;
        }

        public static bool operator !=(StepBase first, StepBase second)
        {
            return !first.Equals(second);
        }

        #endregion

        public override bool Equals(object obj)
        {
            return (obj is StepBase) && Equal((StepBase)obj);
        }

        public bool Equal(StepBase other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            return Value == other.Value;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

    }
    
}
