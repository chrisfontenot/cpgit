﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.ToStringHelpers;
using Sitecore;
using Sitecore.Links;

namespace CredAbility.Web.SitecoreLib.ViewData
{
    public class NeedsAssessmentViewData
    {
        public int CurrentStep { get; set; }

        public int CurrentPage
        {
            get
            {
                return this.GetStepPage(CurrentStep);
            }
        }


        private int GetStepPage(int stepNumber)
        {
            switch (stepNumber)
            {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    if (MonthlyRevolvingCreditCardDebt)
                    {
                        return 3;
                    }
                    else
                    {
                        return 2;
                    }
                case 4:
                    if (MaxPage == 4)
                    {
                        return 2;
                    }
                    else if (MaxPage == 8)
                    {
                        return 4;
                    }
                    else if (MonthlyRevolvingCreditCardDebt && BankruptcyFilingProcess)
                    {
                        return 4;
                    }
                    else if ((MonthlyRevolvingCreditCardDebt && !BankruptcyFilingProcess) || (!MonthlyRevolvingCreditCardDebt && BankruptcyFilingProcess))
                    {
                        return 3;
                    }
                    else
                    {
                        return 2;

                    }
                case 5:
                    if (this.HomePurchase)
                    {
                        return MaxPage - 3;
                    }
                    else
                    {
                        return MaxPage - 2;
                    }
                case 6:
                    return MaxPage - 2;
                case 7:
                    return MaxPage - 1;
                case 8:
                    return MaxPage;
                default:
                    throw new NotSupportedException("The current step is not supported. Most be between 1 to 8.");
            }

        }


        public int MaxPage
        {
            get
            {
                int remove = 0;

                if (!this.MonthlyRevolvingCreditCardDebt)
                {
                    remove += 1;
                }

                if (!this.BankruptcyFilingProcess)
                {
                    remove += 1;
                }

                if (!this.HomeMortgage)
                {
                    remove += 1;
                }

                if (!this.HomePurchase)
                {
                    remove += 1;
                }

                //8 is the maximum step available.
                return 8 - remove;
            }
        }

        public NeedsAssessmentSteps GetCurrentStep { get { return NeedsAssessmentSteps.GetStep(CurrentStep); } }
        public string CurrentStepText { get { return string.Format(NeedsAssessmentResource.CurrentStep, CurrentStep); } }
        public string NextButtonText { get { return NeedsAssessmentResource.Global_Next; } }
        public string PreviousButtonText { get { return NeedsAssessmentResource.Global_Previous; } }


        //TODO: Use the right value type
        #region Step1

        public bool MonthlyRevolvingCreditCardDebt { get; set; }
        public string MonthlyRevolvingCreditCardDebtText { get { return NeedsAssessmentResource.Step1_MonthlyRevolvingCCDebt; } }
        public NeedsAssessmentYesNo GetMonthlyRevolvingCreditCardDebt { get { return NeedsAssessmentYesNo.GetYesNo(MonthlyRevolvingCreditCardDebt); } }
        public bool HomeMortgage { get; set; }
        public string HomeMortgageText { get { return NeedsAssessmentResource.Step1_HomeMortgage; } }
        public NeedsAssessmentYesNo GetHomeMortgage { get { return NeedsAssessmentYesNo.GetYesNo(HomeMortgage); } }
        public bool OtherDebt { get; set; }
        public string OtherDebtText { get { return NeedsAssessmentResource.Step1_OtherDebt; } }
        public NeedsAssessmentYesNo GetOtherDebt { get { return NeedsAssessmentYesNo.GetYesNo(OtherDebt); } }
        public bool InterestedInDMP { get; set; }
        public string InterestedInDMPText { get { return NeedsAssessmentResource.Step1_InterestedDMP; } }
        public NeedsAssessmentYesNo GetInterestedInDMP { get { return NeedsAssessmentYesNo.GetYesNo(InterestedInDMP); } }
        public bool HomePurchase { get; set; }
        public string HomePurchaseText { get { return NeedsAssessmentResource.Step1_HomePurchase; } }
        public NeedsAssessmentYesNo GetHomePurchase { get { return NeedsAssessmentYesNo.GetYesNo(HomePurchase); } }
        public bool InterestedInReverseMortgage { get; set; }
        public string InterestedInReverseMortgageText { get { return NeedsAssessmentResource.Step1_ReverseMortgage; } }
        public NeedsAssessmentYesNo GetInterestedInReverseMortgage { get { return NeedsAssessmentYesNo.GetYesNo(InterestedInReverseMortgage); } }
        public bool BankruptcyFilingProcess { get; set; }
        public string BankruptcyFilingProcessText { get { return NeedsAssessmentResource.Step1_BankruptcyFiling; } }
        public NeedsAssessmentYesNo GetBankruptcyFilingProcess { get { return NeedsAssessmentYesNo.GetYesNo(BankruptcyFilingProcess); } }

        #endregion

        #region Step2

        public int HowTimelyCreditCardPayment { get; set; }
        public NeedsAssessmentStep2Questions.HowTimelyCreditCardPayment GetHowTimelyCreditCardPayment { get { return NeedsAssessmentStep2Questions.HowTimelyCreditCardPayment.GetAnswer(HowTimelyCreditCardPayment); } }
        public int MonthlyPaymentSort { get; set; }
        public NeedsAssessmentStep2Questions.MonthlyPaymentSort GetMonthlyPaymentSort { get { return NeedsAssessmentStep2Questions.MonthlyPaymentSort.GetAnswer(MonthlyPaymentSort); } }
        public int PayDebtWithCreditCard { get; set; }
        public NeedsAssessmentStep2Questions.PayDebtWithCreditCard GetPayDebtWithCreditCard { get { return NeedsAssessmentStep2Questions.PayDebtWithCreditCard.GetAnswer(PayDebtWithCreditCard); } }
        public int NearCreditCardLimit { get; set; }
        public NeedsAssessmentStep2Questions.NearCreditCardLimit GetNearCreditCardLimit { get { return NeedsAssessmentStep2Questions.NearCreditCardLimit.GetAnswer(NearCreditCardLimit); } }


        #endregion

        #region Step3

        public string BKCounselingText { get { return NeedsAssessmentResource.Step3_BkCounseling; } }
        public string GoToBKCounselingUrl
        {
            get
            {
                var createAccountItem = Context.Database.GetItem(ConfigReader.ConfigSection.createaccount.SitecorePath);

                if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName == "es")
                {
                    return "<a href=\"" + LinkManager.GetItemUrl(createAccountItem) + "?LOS=BKC-ES\">" + NeedsAssessmentResource.Step3_GoToBKCounseling + "</a>";
                }
                else
                {

                    return "<a href=\"" + LinkManager.GetItemUrl(createAccountItem) + "?LOS=BKC\">" + NeedsAssessmentResource.Step3_GoToBKCounseling + "</a>";
                }
            }
        }
        public string ContinueAssessment { get { return NeedsAssessmentResource.Step3_ContinueAssessment; } }

        #endregion

        #region Step4

        public int TotalDebtAmount { get; set; }
        public int TotalMonthlyPayment { get; set; }
        public int AverageInterest { get; set; }

        #endregion

        #region Step5

        public int ConcernedAboutMortgagePayment { get; set; }
        public NeedsAssessmentStep5Questions.ConcernedAboutMortgagePayment GetConcernedAboutMortgagePayment { get { return NeedsAssessmentStep5Questions.ConcernedAboutMortgagePayment.GetAnswer(ConcernedAboutMortgagePayment); } }
        public int TimelyHomeMortgagePayment { get; set; }
        public NeedsAssessmentStep5Questions.TimelyHomeMortgagePayment GetTimelyHomeMortgagePayment { get { return NeedsAssessmentStep5Questions.TimelyHomeMortgagePayment.GetAnswer(TimelyHomeMortgagePayment); } }
        public bool HomeForclosureThreat { get; set; }
        public string HomeForclosureThreatText { get { return NeedsAssessmentResource.Step5_HomeForclosure; } }
        public NeedsAssessmentYesNo GetHomeForclosureThreat { get { return NeedsAssessmentYesNo.GetYesNo(HomeForclosureThreat); } }

        #endregion

        #region Step6

        public bool CanMakeHomeDownPayment { get; set; }
        public string CanMakeHomeDownPaymentText { get { return NeedsAssessmentResource.Step6_HomeDownPayment; } }
        public NeedsAssessmentYesNo GetCanMakeHomeDownPayment { get { return NeedsAssessmentYesNo.GetYesNo(CanMakeHomeDownPayment); } }

        #endregion

        #region Step7

        public int IsUserReadyToCommit { get; set; }
        public NeedsAssessmentStep7Questions.IsUserReadyToCommit GetIsUserReadyToCommit { get { return NeedsAssessmentStep7Questions.IsUserReadyToCommit.GetAnswer(IsUserReadyToCommit); } }
        public string DebtOptions { get; set; }
        public List<NeedsAssessmentStep7Questions.DebtOptions> GetDebtOptions { get { return NeedsAssessmentStep7Questions.DebtOptions.GetAnswer(DebtOptions); } }

        #endregion

        #region Step8

        private double TimeToPayInMonth
        {
            get
            {
                if (AverageInterest == 0)
                    return (double)(TotalDebtAmount) / (double)(TotalMonthlyPayment);
                var percentage = ((double)AverageInterest / 100);
                return -(Math.Log(1 - ((double)(TotalDebtAmount) / (double)(TotalMonthlyPayment)) * (percentage / 12)) / Math.Log(1 + (percentage / 12)));
            }
        }

        public double TimeToPayInYears
        {
            get
            {
                return Math.Round((TimeToPayInMonth / 12), 1);
            }
        }
        public int DebtLevel { get { return NeedsAssessmentStep8.DebtLevel.GetDebtLevel(TotalMonthlyPayment, TotalDebtAmount, TimeToPayInMonth); } }
        public string DebtLevelText { get { return NeedsAssessmentResource.Step8_DebtLevel; } }
        public decimal TotalPaid { get { return (decimal)Math.Round((TotalMonthlyPayment*TimeToPayInMonth),2); } }
        public string CurrentSituationShort
        {
            get
            {
                return double.IsNaN(TimeToPayInMonth) ? "" : string.Format(NeedsAssessmentResource.Step8_CurrentSituationShort, TimeToPayInYears, TotalPaid.ConvertToCurrency());
            }
        }
        public string CurrentSituation
        {
            get
            {
                if (TotalDebtAmount > 0)
                {
                    if (double.IsNaN(TimeToPayInMonth))
                    {
                        return string.Format(NeedsAssessmentResource.Step8_CurrentSituationImpossible, ((decimal)TotalDebtAmount).ConvertToCurrency(), AverageInterest);
                    }
                    else
                    {
                        return string.Format(NeedsAssessmentResource.Step8_CurrentSituation,((decimal) TotalDebtAmount).ConvertToCurrency(), TimeToPayInYears,TotalPaid.ConvertToCurrency());
                    }
                }
                else
                {
                    return string.Format(NeedsAssessmentResource.Step8_UnabledAccurateAssessment, this.GetStepPage(4));
                }
            }
        }
        public string LegalLanguage { get { return NeedsAssessmentResource.Step8_LegalLanguage; } }
        public string MayHelpYou { get { return NeedsAssessmentResource.Step8_MayHelpYou; } }
        public List<string> KeyItem
        {
            get
            {
                var results = new List<string>();
                var AssessmentKeyItems = Context.Item.Children.InnerChildren.Where(c => c.TemplateName == "Assessment Key Item");
                foreach (var item in AssessmentKeyItems)
                {
                    results.Add(item.Fields["Text"].Value);
                }

                return results;
            }
        }
        public string Messaging
        {
            get
            {
                var result = "";
                if (ShowLowCommitment)
                    result += NeedsAssessmentResource.Step8_Messaging_LowCommitment;
                if (ShowModerateCommitment)
                    result += NeedsAssessmentResource.Step8_Messaging_ModerateCommitment;
                if (ShowHighCommitment)
                    result += NeedsAssessmentResource.Step8_Messaging_HighCommitment;
                if (ShowDMP)
                    result += NeedsAssessmentResource.Step8_Messaging_DMP;
                if (ShowReverseMortgage)
                    result += NeedsAssessmentResource.Step8_Messaging_ReverseMortgage;
                if (ShowBankruptcy)
                    result += NeedsAssessmentResource.Step8_Messaging_Bankruptcy;
                if (ShowHousingCounseling)
                    result += NeedsAssessmentResource.Step8_Messaging_HousingCounseling;
                if (ShowHousingPrePurchase)
                    result += NeedsAssessmentResource.Step8_Messaging_PrePurchase;
                if (ShowNoFlag)
                    result += NeedsAssessmentResource.Step8_Messaging_NoFlag;

                return result;
            }
        }

        public string GetStartedUrl
        {
            get
            {
                var url = "";
                var getStartedFolder = Context.Database.GetItem(ConfigReader.ConfigSection.getstarted.SitecorePath).GetCredAbilityLinkItem();

                if (ShowBankruptcy)
                {
                    url =
                        ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                        i => i.Name == "bankruptcy-pre-filing-counseling" && i.TemplateName == "Content")).Url;

                    return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url , NeedsAssessmentResource.Global_GetStarted);
                }
                if (ShowHousingCounseling)
                {
                    url =
                        ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                        i => i.Name == "foreclosure-prevention" && i.TemplateName == "Content")).Url;

                    return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url, NeedsAssessmentResource.Global_GetStarted);
                }
                if (ShowHousingPrePurchase)
                {
                    url =
                        ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                        i => i.Name == "housing-pre-purchase" && i.TemplateName == "Content")).Url;

                    return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url, NeedsAssessmentResource.Global_GetStarted);
                }
                if (ShowReverseMortgage)
                {
                    url =
                        ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                        i => i.Name == "reverse-mortgage" && i.TemplateName == "Content")).Url;

                    return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url, NeedsAssessmentResource.Global_GetStarted);
                }
                if (ShowDMP)
                {
                    url =
                        ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                        i => i.Name == "debt-management-plan" && i.TemplateName == "Content")).Url;

                    return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url, NeedsAssessmentResource.Global_GetStarted);
                }
                //default page to link to
                url =
                    ((PageItem)getStartedFolder.GetChildrens().FirstOrDefault(
                                    i => i.Name == "budget-credit-counseling" && i.TemplateName == "Content")).Url;

                return string.Format("<a href=\"{0}\"><span>{1}</span></a>", url, NeedsAssessmentResource.Global_GetStarted);
            }
        }

        public bool ShowLowCommitment
        {
            get
            {
                return (IsUserReadyToCommit >= NeedsAssessmentStep7Questions.IsUserReadyToCommit.Unsure.Value);
            }
        }

        public bool ShowModerateCommitment
        {
            get
            {
                return ((IsUserReadyToCommit < NeedsAssessmentStep7Questions.IsUserReadyToCommit.Unsure.Value) && (IsUserReadyToCommit > NeedsAssessmentStep7Questions.IsUserReadyToCommit.ReadyAndWilling.Value));
            }
        }

        public bool ShowHighCommitment
        {
            get
            {
                return (IsUserReadyToCommit <= NeedsAssessmentStep7Questions.IsUserReadyToCommit.ReadyAndWilling.Value);
            }
        }

        public bool ShowDMP
        {
            get
            {
                return OtherDebt || InterestedInDMP ||
                   (HowTimelyCreditCardPayment >= NeedsAssessmentStep2Questions.HowTimelyCreditCardPayment.AbitLate.Value) ||
                   (MonthlyPaymentSort >= NeedsAssessmentStep2Questions.MonthlyPaymentSort.MinimumBalance.Value) ||
                   (PayDebtWithCreditCard >= NeedsAssessmentStep2Questions.PayDebtWithCreditCard.EveryMonth.Value) ||
                   (NearCreditCardLimit >= NeedsAssessmentStep2Questions.NearCreditCardLimit.MaxedOut.Value);
            }
        }

        public bool ShowReverseMortgage
        {
            get
            {
                return InterestedInReverseMortgage;
            }
        }

        public bool ShowBankruptcy
        {
            get
            {
                return BankruptcyFilingProcess;
            }
        }

        public bool ShowHousingCounseling
        {
            get
            {
                return (ConcernedAboutMortgagePayment >= NeedsAssessmentStep5Questions.ConcernedAboutMortgagePayment.Somewhat.Value) ||
                    (TimelyHomeMortgagePayment >= NeedsAssessmentStep5Questions.TimelyHomeMortgagePayment.AbitLate.Value) ||
                    HomeForclosureThreat;
            }
        }

        public bool ShowHousingPrePurchase
        {
            get
            {
                return CanMakeHomeDownPayment;
            }
        }

        public bool ShowNoFlag
        {
            get
            {
                return !(ShowDMP && ShowReverseMortgage && ShowBankruptcy && ShowHousingCounseling &&
                        ShowHousingPrePurchase);
            }
        }
        #endregion

    }
}
