﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep2Questions
{
    public class MonthlyPaymentSort : NeedsAssessmentSliderQuestionBase
    {

        public static MonthlyPaymentSort NotPayingAnything { get { return new ANotPayingAnything(); } }
        public static MonthlyPaymentSort LessThanTheMinimum { get { return new ALessThanTheMinimum(); } }
        public static MonthlyPaymentSort MinimumBalance { get { return new AMinimumBalance(); } }
        public static MonthlyPaymentSort MoreThanMinimum { get { return new AMoreThanMinimum(); } }
        public static MonthlyPaymentSort PayCompleteBalance { get { return new APayCompleteBalance(); } }

        public static int Count() {
            return 5;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort;
        }

        public static MonthlyPaymentSort GetAnswer(int value)
        {
            return
                typeof(MonthlyPaymentSort).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new MonthlyPaymentSort(), null) as MonthlyPaymentSort).Value == value).GetValue(new MonthlyPaymentSort(), null)
                as MonthlyPaymentSort;
        }


        #region Nested class

        protected class ANotPayingAnything : MonthlyPaymentSort
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort_NotPaying; }
            }
        }

        protected class ALessThanTheMinimum : MonthlyPaymentSort
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort_Less; }
            }
        }

        protected class AMinimumBalance : MonthlyPaymentSort
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort_Minimum; }
            }
        }

        protected class AMoreThanMinimum : MonthlyPaymentSort
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort_More; }
            }
        }

        protected class APayCompleteBalance : MonthlyPaymentSort
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_MonthlyCreditCardPaymentSort_complete; }
            }
        }

        #endregion

    }


}
