﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep2Questions
{
    public class HowTimelyCreditCardPayment : NeedsAssessmentSliderQuestionBase
    {

        public static HowTimelyCreditCardPayment DaysBehind60 { get { return new ADaysBehind60(); } }
        public static HowTimelyCreditCardPayment DaysBehind30 { get { return new ADaysBehind30(); } }
        public static HowTimelyCreditCardPayment AbitLate { get { return new AAbitLate(); } }
        public static HowTimelyCreditCardPayment UsuallyOnTime { get { return new AUsuallyOnTime(); } }
        public static HowTimelyCreditCardPayment AlwaysOnTime { get { return new AAlwaysOnTime(); } }

        public static int Count()
        {
            return 5;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step2_TimelyCreditCardPayment;
        }

        public static HowTimelyCreditCardPayment GetAnswer(int value)
        {
            return
                typeof (HowTimelyCreditCardPayment).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new HowTimelyCreditCardPayment(), null) as HowTimelyCreditCardPayment).Value == value).GetValue(new HowTimelyCreditCardPayment(), null)
                as HowTimelyCreditCardPayment;
        }


        #region Nested class

        protected class ADaysBehind60 : HowTimelyCreditCardPayment
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_TimelyCreditCardPayment_60day; }
            }
        }

        protected class ADaysBehind30 : HowTimelyCreditCardPayment
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_TimelyCreditCardPayment_30day; }
            }
        }

        protected class AAbitLate : HowTimelyCreditCardPayment
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_TimelyCreditCardPayment_Late; }
            }
        }

        protected class AUsuallyOnTime : HowTimelyCreditCardPayment
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_TimelyCreditCardPayment_Usually; }
            }
        }

        protected class AAlwaysOnTime : HowTimelyCreditCardPayment
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_TimelyCreditCardPayment_Alway; }
            }
        }

        #endregion

    }


}
