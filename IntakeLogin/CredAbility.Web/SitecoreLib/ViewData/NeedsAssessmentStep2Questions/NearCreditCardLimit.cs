﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep2Questions
{
    public class NearCreditCardLimit : NeedsAssessmentSliderQuestionBase
    {

        public static NearCreditCardLimit MaxedOut { get { return new AMaxedOut(); } }
        public static NearCreditCardLimit NearLimit { get { return new ANearLimit(); } }
        public static NearCreditCardLimit SomewhatFar { get { return new ASomewhatFar(); } }
        public static NearCreditCardLimit FarFromLimit { get { return new AFarFromLimit(); } }
        public static NearCreditCardLimit DontKnow { get { return new ADontKnow(); } }

        public static int Count() {
            return 5;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step2_CreditCardLimit;
        }

        public static NearCreditCardLimit GetAnswer(int value)
        {
            return
                typeof(NearCreditCardLimit).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new NearCreditCardLimit(), null) as NearCreditCardLimit).Value == value).GetValue(new NearCreditCardLimit(), null)
                as NearCreditCardLimit;
        }


        #region Nested class

        protected class AMaxedOut : NearCreditCardLimit
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardLimit_max; }
            }
        }

        protected class ANearLimit : NearCreditCardLimit
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardLimit_Near; }
            }
        }

        protected class ASomewhatFar : NearCreditCardLimit
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardLimit_SomewhatFar; }
            }
        }

        protected class AFarFromLimit : NearCreditCardLimit
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardLimit_FarFromLimit; }
            }
        }

        protected class ADontKnow : NearCreditCardLimit
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardLimit_DontKnow; }
            }
        }

        #endregion

    }


}
