﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep2Questions
{
    public class PayDebtWithCreditCard : NeedsAssessmentSliderQuestionBase
    {

        public static PayDebtWithCreditCard Never { get { return new ANever(); } }
        public static PayDebtWithCreditCard Sometimes { get { return new ASometimes(); } }
        public static PayDebtWithCreditCard EveryMonth { get { return new AEveryMonth(); } }

        public static int Count() {
            return 3;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step2_CreditCardPayOtherDebt;
        }

        public static PayDebtWithCreditCard GetAnswer(int value)
        {
            return
                typeof(PayDebtWithCreditCard).GetProperties().FirstOrDefault(
                    p =>
                    (p.GetValue(new PayDebtWithCreditCard(), null) as PayDebtWithCreditCard).Value == value).GetValue(new PayDebtWithCreditCard(), null)
                as PayDebtWithCreditCard;
        }


        #region Nested class

        protected class ANever : PayDebtWithCreditCard
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardPayOtherDebt_never; }
            }
        }

        protected class ASometimes : PayDebtWithCreditCard
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardPayOtherDebt_Sometimes; }
            }
        }

        protected class AEveryMonth : PayDebtWithCreditCard
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step2_CreditCardPayOtherDebt_EveryMonth; }
            }
        }

        #endregion

    }


}
