﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep4Questions
{
    public class TotalMonthlyPayment : NeedsAssessmentSliderQuestionBase
    {

        public static TotalMonthlyPayment D0 { get { return new Amount0(); } }
        public static TotalMonthlyPayment D1 { get { return new FirstAmountOption(); } }
        public static TotalMonthlyPayment D2 { get { return new SecondAmountOption(); } }
        public static TotalMonthlyPayment D3 { get { return new ThirdAmountOption(); } }
        public static TotalMonthlyPayment D4 { get { return new FourthAmountOption(); } }

        public static int Count() {
            return 2000;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step4_TotalMonthlyPayment;
        }

        #region Nested class

        protected class Amount0 : TotalMonthlyPayment
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalMonthlyPayment_0; }
            }
        }

        protected class FirstAmountOption : TotalMonthlyPayment
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalMonthlyPayment_FirstAmount; }
            }
        }

        protected class SecondAmountOption : TotalMonthlyPayment
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalMonthlyPayment_SecondAmount; }
            }
        }

        protected class ThirdAmountOption : TotalMonthlyPayment
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalMonthlyPayment_ThirdAmount; }
            }
        }

        protected class FourthAmountOption : TotalMonthlyPayment
        {
            public override int Value
            {
                get { return 6; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalMonthlyPayment_FourthAmount; }
            }
        }

        #endregion

    }


}
