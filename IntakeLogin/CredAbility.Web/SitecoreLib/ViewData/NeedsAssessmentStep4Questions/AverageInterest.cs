﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep4Questions
{
    public class AverageInterest : NeedsAssessmentSliderQuestionBase
    {

        public static AverageInterest D0 { get { return new Percent0(); } }
        public static AverageInterest D1 { get { return new FirstPercentOption(); } }
        public static AverageInterest D2 { get { return new SecondPercentOption(); } }
        public static AverageInterest D3 { get { return new ThirdPercentOption(); } }
        public static AverageInterest D4 { get { return new FourthPercentOption(); } }
        public static AverageInterest D5 { get { return new FifthPercentOption(); } }
        public static AverageInterest D6 { get { return new SixthPercentOption(); } }

        public static int Count() {
            return 30;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step4_AverageInterest;
        }

        #region Nested class


        protected class Percent0 : AverageInterest
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_0; }
            }
        }

        protected class FirstPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_FirstPercent; }
            }
        }

        protected class SecondPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_SecondPercent; }
            }
        }

        protected class ThirdPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_ThirdPercent; }
            }
        }
        protected class FourthPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 6; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_FourthPercent; }
            }
        }
        protected class FifthPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 8; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_FifthPercent; }
            }
        }


        protected class SixthPercentOption : AverageInterest
        {
            public override int Value
            {
                get { return 9; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_AverageInterest_SixthPercent; }
            }
        }


        #endregion

    }


}
