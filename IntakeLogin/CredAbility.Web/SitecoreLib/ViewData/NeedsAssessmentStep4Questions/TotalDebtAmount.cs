﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep4Questions
{
    public class TotalDebtAmount : NeedsAssessmentSliderQuestionBase
    {

        public static TotalDebtAmount D0 { get { return new Amount0(); } }
        public static TotalDebtAmount D1 { get { return new FirstAmountOption(); } }
        public static TotalDebtAmount D2 { get { return new SecondAmountOption(); } }
        public static TotalDebtAmount D3 { get { return new ThirdAmountOption(); } }
        public static TotalDebtAmount D4 { get { return new FourthAmountOption(); } }
        public static TotalDebtAmount D5 { get { return new FifthAmountOption(); } }

        public static int Count() {
            return 150000;
        }

        public static string GetQuestion()
        {
            return NeedsAssessmentResource.Step4_TotalDebt;
        }

        #region Nested class

        protected class Amount0 : TotalDebtAmount
        {
            public override int Value
            {
                get { return 1; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_0; }
            }
        }

        protected class FirstAmountOption : TotalDebtAmount
        {
            public override int Value
            {
                get { return 2; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_FirstAmount; }
            }
        }


        protected class SecondAmountOption : TotalDebtAmount
        {
            public override int Value
            {
                get { return 3; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_SecondAmount; }
            }
        }

        protected class ThirdAmountOption : TotalDebtAmount
        {
            public override int Value
            {
                get { return 4; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_ThirdAmount; }
            }
        }

        protected class FourthAmountOption : TotalDebtAmount
        {
            public override int Value
            {
                get { return 5; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_FourthAmount; }
            }
        }


        protected class FifthAmountOption : TotalDebtAmount
        {
            public override int Value
            {
                get { return 6; }
            }

            public override string Text
            {
                get { return NeedsAssessmentResource.Step4_TotalDebt_FifthAmount; }
            }
        }

        #endregion

    }


}
