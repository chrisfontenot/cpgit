﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep8
{
    public static class DebtLevel
    {
        public static string Bad { get; set; }
        public static string Moderate { get; set; }
        public static string Good { get; set; }

        public static int GetDebtLevel(int monthlyPayment, int debtTotal, double timeToPayInMonth)
        {
            //Verify if time to pay is possible with interest rate, if not return bad DebtLevel
            if (double.IsNaN(timeToPayInMonth))
                return 400;
            var percentage = ((double)monthlyPayment / (double)debtTotal)*100;
            if (percentage <= 1)
                return 400;
            if (percentage > 1 && percentage <= 3)
                return 220;
            if (percentage > 3)
                return 30;
            return 1;
        }
    }
}
