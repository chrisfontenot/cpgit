﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.SitecoreLib.ViewData
{
    public class NeedsAssessmentYesNo : YesNoBase
    {
        public static NeedsAssessmentYesNo Yes { get { return new NeedsAssessmentYes(); } }
        public static NeedsAssessmentYesNo No { get { return new NeedsAssessmentNo(); } }

        public static NeedsAssessmentYesNo GetYesNo(bool value)
        {
            return
                typeof (NeedsAssessmentYesNo).GetProperties().FirstOrDefault(
                    p => (p.GetValue(new NeedsAssessmentYesNo(), null) as NeedsAssessmentYesNo).Value == value).GetValue
                    (new NeedsAssessmentYesNo(), null) as NeedsAssessmentYesNo;
        }

        #region Nested class

        protected class NeedsAssessmentYes : NeedsAssessmentYesNo
        {
            public override bool Value
            {
                get { return true; }
            }

            public override string Text
            {
                get { return Resource.Course_AlreadyRated; }
            }
        }

        protected class NeedsAssessmentNo : NeedsAssessmentYesNo
        {
            public override bool Value
            {
                get { return false; }
            }

            public override string Text
            {
                get { return Resource.Course_AlreadyRated; }
            }
        }

        #endregion
    }

    public class YesNoBase
    {
        public virtual bool Value
        {
            get { return false; }
        }

        public virtual string Text
        {
            get { return ""; }
        }

        #region Operators
        public static bool operator ==(YesNoBase first, YesNoBase second)
        {
            if (ReferenceEquals(first, second))
            {
                return true;
            }

            if (ReferenceEquals(first, null) || ReferenceEquals(second, null))
            {
                return false;
            }

            return first.Value == second.Value;
        }

        public static bool operator !=(YesNoBase first, YesNoBase second)
        {
            return !first.Equals(second);
        }

        #endregion

        public override bool Equals(object obj)
        {
            return (obj is YesNoBase) && Equal((YesNoBase)obj);
        }

        public bool Equal(YesNoBase other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            return Value == other.Value;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

    }
}
