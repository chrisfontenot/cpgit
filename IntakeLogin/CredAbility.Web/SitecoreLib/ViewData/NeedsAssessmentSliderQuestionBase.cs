﻿namespace CredAbility.Web.SitecoreLib.ViewData
{
    public class NeedsAssessmentSliderQuestionBase
    {
        public virtual int Value
        {
            get { return 0; }
        }

        public virtual string Text
        {
            get { return ""; }
        }

        #region Operators
        public static bool operator ==(NeedsAssessmentSliderQuestionBase first, NeedsAssessmentSliderQuestionBase second)
        {
            if (ReferenceEquals(first, second))
            {
                return true;
            }

            if (ReferenceEquals(first, null) || ReferenceEquals(second, null))
            {
                return false;
            }

            return first.Value == second.Value;
        }

        public static bool operator !=(NeedsAssessmentSliderQuestionBase first, NeedsAssessmentSliderQuestionBase second)
        {
            return !first.Equals(second);
        }

        #endregion

        public override bool Equals(object obj)
        {
            return (obj is NeedsAssessmentSliderQuestionBase) && Equal((NeedsAssessmentSliderQuestionBase)obj);
        }

        public bool Equal(NeedsAssessmentSliderQuestionBase other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }

            return Value == other.Value;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

    }
}
