using System;
using System.Web;

namespace CredAbility.Web.Authentification
{
    public sealed class RememberMeContext{
        #region Constants

        private const string COOKIES_KEY = "RememberMeContext"; 

        #endregion

        public string Username
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[COOKIES_KEY];

                if (cookie != null && cookie["username"] != null)
                {
                    return cookie["username"];
                }
                return "";
            }
            set
            {
                var cookie = HttpContext.Current.Request.Cookies[COOKIES_KEY] ?? new HttpCookie(COOKIES_KEY);
                cookie["username"] = value;
                cookie.Expires = DateTime.Today.AddMonths(6);
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }

        public bool IsActive
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[COOKIES_KEY];

                if (cookie != null && cookie["remember-me"] != null)
                {
                    return bool.Parse(cookie["remember-me"]);
                }
                return false;
            }
            set
            {
                var cookie = HttpContext.Current.Request.Cookies[COOKIES_KEY] ?? new HttpCookie(COOKIES_KEY);
                cookie["remember-me"] = value.ToString();
                cookie.Expires = DateTime.Today.AddMonths(6);
                HttpContext.Current.Response.SetCookie(cookie);
            }
        }
    }
}