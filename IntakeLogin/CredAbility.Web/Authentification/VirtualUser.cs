﻿using System;
using System.Collections.Generic;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using Sitecore.Security;
using Sitecore.Security.Accounts;
using User=Sitecore.Security.Accounts.User;

namespace CredAbility.Web.Authentification
{
    /// <summary>
    /// Class creating a Sitecore Virtual User
    /// </summary>
    public class VirtualUser
    {
        private readonly User _sitecoreUser;

        public User SitecoreUser
        {
            get { return _sitecoreUser; }
        }

        protected VirtualUser()
        {
        }

        /// <summary>
        /// Create a default Sitecre Virtual User with the DEFAULT_ROLE.
        /// </summary>
        public VirtualUser(ClientProfile profile)
        {
            this._sitecoreUser = Sitecore.Security.Authentication.AuthenticationManager.BuildVirtualUser(profile.Username, false);
            this.AssignRoles(profile);
        }

        #region Roles Assignment

        private void AssignRoles(ClientProfile profile)
        {
            //By default a Sitecore Virtual User have all the extranet role. So me remove all of them.
            this.ClearAllAssignedRoles();

            this.AddRole("extranet\\WebsiteUser");
            this.AddRole("extranet\\NewUser");

            this.AssignRolesByWebsiteStatus(profile);
            this.AssignRolesByTypes(profile);
            this.AssignRolesForCreditScoreChallenge(profile);
            this.AssignRolesForLimitedAccess(profile);
        }

        private void AssignRolesForLimitedAccess(ClientProfile profile)
        {
            if (profile.MustChangePassword || profile.IsProfileUpdateRequired)
            {
                this.AddRole("extranet\\LimitedAccess");
            }  

        }

        private void AssignRolesForCreditScoreChallenge(ClientProfile profile)
        {
            if (profile.IsCreditScoreChallengeContestant)
            {
                this.AddRole("extranet\\CreditScoreChallenge");
            }
        }

        private void AssignRolesByTypes(ClientProfile profile)
        {
            foreach (string code in profile.Types)
            {
                switch (code.Trim().ToUpper())
                {
                    case "BR.CAM":
                        this.AddRole("extranet\\BankruptcyPreFilingCounseling");
                        break;
                    case "CAM":
                        this.AddRole("extranet\\BudgetAndCredit");
                        break;
                    case "CM":
                        this.AddRole("extranet\\ClientAccessDMP");
                        break;
                    case "WS.CAM":
                        this.AddRole("extranet\\BankruptcyEducation");
                        break;
                    default:
                        break;
                }
            }
        }

        private void AssignRolesByWebsiteStatus(ClientProfile profile)
        {
            ClientWebsiteStatus status = ClientProfileRepository.GetLastVisitedWebsite(profile);

            if (status != null)
            {
                switch (status.WebsiteCode.Trim().ToUpper())
                {
                    case "BKC":
                        this.AddRole("extranet\\BankruptcyPreFilingCounseling");
                        break;
                    case "BCH":
                        this.AddRole("extranet\\BudgetAndCredit");
                        break;
                    case "CA":
                        this.AddRole("extranet\\ClientAccessDMP");
                        break;
                    case "BKDE":
                        this.AddRole("extranet\\BankruptcyEducation");
                        break;
                    case "DMP":
                        this.AddRole("extranet\\DMPCounseling");
                        break;
                    case "HUD":
                        this.AddRole("extranet\\HousingForeclosure");
                        break;
                    case "PRP":
                        this.AddRole("extranet\\HousingPrePurchase");
                        break;
                    case "RVM":
                        this.AddRole("extranet\\HousingReverseMortgage");
                        break;
                    default:
                        break;

                }
            }
        }

        #endregion

        private void ClearAllAssignedRoles()
        {
            this._sitecoreUser.Roles.RemoveAll();
        }

        private void AddRole(string sitecoreRoleName)
        {
            if (!this._sitecoreUser.Roles.Contains(Role.FromName(sitecoreRoleName)))
            {
                this._sitecoreUser.Roles.Add(Role.FromName(sitecoreRoleName));
            }
        }

    }
}