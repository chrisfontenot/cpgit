﻿using System;
using System.Collections.Generic;
using System.Web;
using CredAbility.Core.Activity;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.DMP;
using CredAbility.Core.Document;
using CredAbility.Data.Activity;
using CredAbility.Data.DMP;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Data.Validator;
using CredAbility.Web.Configuration;
using CredAbility.Web.CreditScoreChallenge;
using Sitecore.Security.Accounts;
using Sitecore.Web;

namespace CredAbility.Web.Authentification
{
	public class ClientProfileManager
	{
		private const string CLIENTPROFILE_SESSION_KEY = "ClientProfile";
		private AuthentificationManager _authentificationManager = new AuthentificationManager();

		public ActionResult SaveProfile(Profile profile)
		{
			ActionResult result = new ActionResult(true);
			ProfileValidator validator = new ProfileValidator();
			result = validator.Validate(profile);
			if(result.IsSuccessful)
			{
				result.MergeActionResult(IdentityRepository.SaveProfile(profile));
				if(result.IsSuccessful)
				{
					// log activity
					var activity = new UserActivity(profile.ClientID)
					{
						Text = Resource.UserActivity_UpdatedProfile,
						Type = UserActivityType.Updated
					};
					UserActivityRepository.Save(activity);
					ClientProfileManager.SaveClientProfile(profile);
					Sitecore.Diagnostics.Log.Audit(string.Format("Profile update successed. Session informations: username: {0}, clientId: {1}, session Id: {2}", profile.Username, profile.ClientID, HttpContext.Current.Session.SessionID), profile);
				}
			}
			return result;
		}

		public static void SaveClientProfile(ClientProfile clientProfile)
		{
			HttpContext.Current.Session.Add(CLIENTPROFILE_SESSION_KEY, clientProfile);
			Sitecore.Diagnostics.Log.Audit(string.Format("ClientProfile saved into session. Session informations: username: {0}, clientId: {1}, session Id: {2}", clientProfile.Username, clientProfile.ClientID, HttpContext.Current.Session.SessionID), clientProfile);
		}

		public static ClientProfile CurrentClientProfile
		{
			get
			{
				if(HttpContext.Current.Session[CLIENTPROFILE_SESSION_KEY] != null)
				{
					return (ClientProfile)HttpContext.Current.Session[CLIENTPROFILE_SESSION_KEY];
				}
				if(Sitecore.Context.IsLoggedIn)
				{
					ClientProfileManager profile = new ClientProfileManager();
					profile.Logout();
				}
				else
				{
					HttpContext.Current.Response.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value);
				}
				return null;
			}
		}

		public static ClientProfile GetClientProfile
		{
			get
			{
				return (ClientProfile)HttpContext.Current.Session[CLIENTPROFILE_SESSION_KEY];
			}
		}

		public void Logout()
		{
			AuthentificationManager authentificationManager = new AuthentificationManager();
			ActionResult actionResult = authentificationManager.Logout(CurrentClientProfile.ClientID);
			if(actionResult.IsSuccessful)
			{
				HttpContext.Current.Session.Remove(CLIENTPROFILE_SESSION_KEY);
				if(CreditScoreChallengeContext.Current.IsFirstTime)
				{
					CreditScoreChallengeContext.Current.IsFirstTime = false;
				}
				WebUtil.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value);
			}
			else
			{
				WebUtil.Redirect(WebUtil.CurrentPage.Request.UrlReferrer.AbsolutePath);
			}
		}

		public ActionResult ChangePassword(string currentPassword, string newPassword, string confirmPassword)
		{
			PasswordValidator validator = new PasswordValidator();
			ClientProfileManager.CurrentClientProfile.Password = newPassword;
			ActionResult result = validator.Validate(ClientProfileManager.CurrentClientProfile);
			if(result.IsSuccessful)
			{
				result.MergeActionResult(IdentityRepository.ChangePassword(ClientProfileManager.CurrentClientProfile, currentPassword, newPassword, confirmPassword));
				if(result.IsSuccessful && Sitecore.Context.User.Roles.Contains(Role.FromName("extranet\\LimitedAccess")))
				{
					Sitecore.Context.User.Roles.Remove(Role.FromName("extranet\\LimitedAccess"));
				}
			}
			return result;
		}

		public ActionResult ResetPassword(string newPassword, string confirmPassword)
		{
			PasswordValidator validator = new PasswordValidator();
			ClientProfileManager.CurrentClientProfile.Password = newPassword;
			ActionResult result = validator.Validate(ClientProfileManager.CurrentClientProfile);
			if(result.IsSuccessful)
			{
				result.MergeActionResult(IdentityRepository.ResetPassword(ClientProfileManager.CurrentClientProfile.ClientID, newPassword, confirmPassword));
				AuthentificationContext.Current.IsActiveLosCodeRedirection = true;
				Sitecore.Diagnostics.Log.Audit("Reset Password", this);
			}
			return result;
		}

		public Core.Identity.SecurityQuestion ForgotUsername(string email, out ActionResult actionResult)
		{
			var validator = new ForgotUsernameValidator();
			var result = new List<Core.Identity.SecurityQuestion>();
			actionResult = validator.Validate(email);
			if(actionResult.IsSuccessful)
			{
				return IdentityRepository.GetSecurityQuestionByEmail(email);
			}
			return null;
		}

		// forgot password step1
		public Core.Identity.SecurityQuestion ForgotPassword(string username, out ActionResult result)
		{
			var validator = new ForgotPasswordValidator();
			result = validator.Validate(username);
			if(result.IsSuccessful)
			{
				Sitecore.Diagnostics.Log.Audit(string.Format("Forgot Password : Step1 : Successed for {0}", username), this);
				return IdentityRepository.GetSecurityQuestionByUsername(username);
			}
			return null;
		}

		// forgot password step2
		public CustomResponse<UserSecurityQuestionChallengeResult> ValidateSecurityAnswersByUsername(string username, string answer1, out ActionResult result)
		{
			var validator = new SecurityAnswerValidator();
			result = validator.Validate(answer1);
			var response = new CustomResponse<UserSecurityQuestionChallengeResult>();
			if(result.IsSuccessful)
			{
				response = IdentityRepository.ValidateSecurityQuestionChallengeByUsername(username, answer1);
				if(!response.HasError)
				{
					if(response.ResponseValue != null)
					{
						UserSecurityQuestionChallengeResult challengeResult = response.ResponseValue;
						var clientProfile = new ClientProfile { ClientID = challengeResult.UserId.Value.ToString() };
						ClientProfileManager.SaveClientProfile(clientProfile);
						Sitecore.Diagnostics.Log.Audit(string.Format("Forgot Password : Step2 : Successed for {0}", username), this);
					}
				}
				else
				{
					result.IsSuccessful = false;
					result.Messages.Add(new ResultMessage(ResultMessageLevelType.Error, Resource.ForgotPassword_SecurityChallengeFailed, string.Empty));
				}
			}
			return response;
		}

		public CustomResponse<UsernameRecoverResult> ValidateSecurityAnswersByEmail(string email, string answer1, out ActionResult result)
		{
			var response = new CustomResponse<UsernameRecoverResult>();
			var validator = new SecurityAnswerValidator();
			result = validator.Validate(answer1);
			if(result.IsSuccessful)
			{
				response = IdentityRepository.ValidateSecurityQuestionChallengeByEmail(email, answer1);
				if(response.HasError)
				{
					result.IsSuccessful = false;
					result.Messages.Add(new ResultMessage(ResultMessageLevelType.Error
						,Resource.ForgotPassword_SecurityChallengeFailed, "WS-SecurityFailed"));
					return response;
				}
				result.IsSuccessful = true;
				Sitecore.Diagnostics.Log.Audit(string.Format("Forgot Username : Step2 : Successed for {0}", email), this);
				return response;
			}
			return response;
		}

		public ActionResult SaveAvatar(string avatarKey)
		{
			var result = IdentityRepository.SaveAvatar(CurrentClientProfile, avatarKey);

			if(result.IsSuccessful)
			{
				ClientProfileManager.CurrentClientProfile.Avatar = avatarKey;

				Sitecore.Diagnostics.Log.Audit("Avatar update successed.", this);
			}
			return result;
		}


		public void LoginWithToken(string userID, string tokenID, string redirectUrl)
		{
			ActionResult<ClientProfile> actionResult = _authentificationManager.LoginWithToken(userID, tokenID);
			if(!actionResult.IsSuccessful || string.IsNullOrEmpty(redirectUrl))
			{
				redirectUrl = ConfigReader.ConfigSection.Url.MyActivity.Value;
			}
			else
			{
				HttpContext.Current.Session.Add(CLIENTPROFILE_SESSION_KEY, actionResult.Value);
			}
			HttpContext.Current.Response.Redirect(redirectUrl);
		}

		//TAG VALIDATION OF GOOD USER
		public ActionResult CreateAccountLogin(string firstName, string lastName, string userName, string password, bool rememberMe, bool hasToRedirect, bool isFromCreateAccount)
		{
			var clientProfile = new ClientProfile { Username = userName, Password = password };
			var val = new LoginValidator();
			var actionResult = val.Validate(clientProfile);
			if(!actionResult.IsSuccessful)
			{
				return actionResult;
			}
			var response = IdentityRepository.Login(userName, password);
			if(response.HasError)
			{
				actionResult.IsSuccessful = false;
				actionResult.Messages.Add(new ResultMessage(response.ErrorMessage, string.Empty));
				return actionResult;
			}
			else
			{
				clientProfile = response.ResponseValue;
			}

			//TAG VALIDATION OF GOOD USER
			if(clientProfile.FirstName != firstName || clientProfile.LastName != lastName || clientProfile.Username != userName)
			{
				actionResult.IsSuccessful = false;
				actionResult.Messages.Add(new ResultMessage(Resource.ClientProfileManager_UserIdValidation, string.Empty));
				this.Logout();
			}
			else
			{
				if(isFromCreateAccount)
				{
					clientProfile.IsFirstTime = true;
				}
				var virtualUser = new VirtualUser(clientProfile);
				AuthentificationContext.Current.IsLogged = Sitecore.Security.Authentication.AuthenticationManager.Login(virtualUser.SitecoreUser);
				if(AuthentificationContext.Current.IsLogged)
				{
					HttpContext.Current.Session.Add(CLIENTPROFILE_SESSION_KEY, clientProfile);
					AuthentificationContext.Current.RememberMe.Username = rememberMe ? userName : string.Empty;
					AuthentificationContext.Current.RememberMe.IsActive = rememberMe;
					Sitecore.Diagnostics.Log.Audit(string.Format("Account creation login successed. Session informations, username: {0}, clientId: {1}, session Id: {2}", clientProfile.Username, clientProfile.ClientID, HttpContext.Current.Session.SessionID), this);
					if(hasToRedirect)
					{
						RedirectBack();
					}
				}
			}
			return actionResult;
		}

		public ActionResult Login(string userName, string password, bool rememberMe, bool hasToRedirect, bool isFromCreateAccount)
		{
			var clientProfile = new ClientProfile { Username = userName, Password = password };
			var val = new LoginValidator();
			var actionResult = val.Validate(clientProfile);
			if(!actionResult.IsSuccessful)
			{
				return actionResult;
			}
			var response = IdentityRepository.Login(userName, password);
			if(response.HasError)
			{
				actionResult.IsSuccessful = false;
				actionResult.Messages.Add(new ResultMessage(response.ErrorMessage, string.Empty));
				return actionResult;
			}
			else
			{
				clientProfile = response.ResponseValue;
			}
			if(isFromCreateAccount)
			{
				clientProfile.IsFirstTime = true;
			}
			var virtualUser = new VirtualUser(clientProfile);
			AuthentificationContext.Current.IsLogged = Sitecore.Security.Authentication.AuthenticationManager.Login(virtualUser.SitecoreUser);
			if(AuthentificationContext.Current.IsLogged)
			{
				HttpContext.Current.Session.Add(CLIENTPROFILE_SESSION_KEY, clientProfile);
				AuthentificationContext.Current.RememberMe.Username = rememberMe ? userName : string.Empty;
				AuthentificationContext.Current.RememberMe.IsActive = rememberMe;
				Sitecore.Diagnostics.Log.Audit(string.Format("Login successed. Session informations, username: {0}, clientId: {1}, session Id: {2}", clientProfile.Username, clientProfile.ClientID, HttpContext.Current.Session.SessionID), this);
				if(hasToRedirect)
				{
					RedirectBack();
				}
			}
			return actionResult;
		}

		public void RedirectBack()
		{
			AuthentificationContext.Current.Redirect(CurrentClientProfile);
		}

		private DMPInfo _getDMP;
		public DMPInfo GetDMP()
		{
			if(!string.IsNullOrEmpty(CurrentClientProfile.DMPAccessID) && _getDMP == null)
			{
				_getDMP = DMPRepository.GetDMPInfo(CurrentClientProfile.DMPAccessID, DateTime.Now.AddMonths(-12), DateTime.Now).ResponseValue;
			}
			return _getDMP;
		}

		private DocumentList _getClientDocuments;
		public DocumentList GetClientDocuments()
		{
			//coments are for reverse compatibility
			//if(!string.IsNullOrEmpty(CurrentClientProfile.DocumentAccessID) && _getDMP == null)
			if(_getClientDocuments == null)
			{
				_getClientDocuments = ClientProfileRepository.GetClientDocuments(CurrentClientProfile);
			}
			//else
			//{
			//    _getClientDocuments = new DocumentList();
			//}
			return _getClientDocuments;
		}

		private ClientCounselingSummaryInfo _getClientCounselingSummary;
		public ClientCounselingSummaryInfo GetClientCounselingSummary()
		{
			//coments are for reverse compatibility
			//if(!string.IsNullOrEmpty(CurrentClientProfile.DocumentAccessID) && _getClientCounselingSummary == null)
			if(_getClientCounselingSummary == null)
			{
				_getClientCounselingSummary = ClientProfileRepository.GetClientCounselingSummaryInfo(CurrentClientProfile);
			}
			if(_getClientCounselingSummary != null)
			{
				if(_getClientCounselingSummary.IsSuccessful)
				{
					return _getClientCounselingSummary;
				}
			}
			return null;
		}
	}
}