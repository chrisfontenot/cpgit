﻿using System;
using System.Web;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;

namespace CredAbility.Web.Authentification
{
    public sealed class AuthentificationManager
    {
        private AuthentificationWithThirdParty _authentificationWithThirdParty = new AuthentificationWithThirdParty();
        private AuthentificationWithSitecore _authentificationWithSitecore = new AuthentificationWithSitecore();
       
        public ActionResult<ClientProfile> LoginWithThirdParty(string username, string password)
        {
            return _authentificationWithThirdParty.Login(username, password);
        }

        public ActionResult LogoutFromThirdParty(string userID)
        {
            return _authentificationWithThirdParty.Logout(userID);
        }


        public ActionResult Logout(string userID)
        {
            Sitecore.Diagnostics.Log.Audit("START: Logout", this);

            ActionResult actionResult = _authentificationWithSitecore.Logout();

            if(actionResult.IsSuccessful)
            {
                actionResult = _authentificationWithThirdParty.Logout(userID);
            }

            AuthentificationContext.Current.IsLogged = !actionResult.IsSuccessful;

            Sitecore.Diagnostics.Log.Info("END: Logout", this);

            return actionResult;
        }

        public ActionResult<ClientProfile> LoginWithToken(string userID, string tokenID)
        {
            ActionResult<ClientProfile> actionResult = _authentificationWithThirdParty.LoginWithToken(userID, tokenID);

            if(actionResult.IsSuccessful)
            {
                actionResult.MergeActionResult(_authentificationWithSitecore.Login(actionResult.Value));
            }

            if(actionResult.IsSuccessful)
            {
                Sitecore.Diagnostics.Log.Audit(string.Format("Single Sin-on Login successed. Session informations, username: {0}, clientId: {1}, session Id: {2}", actionResult.Value.Username, actionResult.Value.ClientID, HttpContext.Current.Session.SessionID), this);
            }

            return actionResult;
        }

    }
}
