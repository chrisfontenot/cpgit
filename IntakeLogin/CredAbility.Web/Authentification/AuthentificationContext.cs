using System;
using System.Web;
using CredAbility.Core.Client;

namespace CredAbility.Web.Authentification
{
    public sealed class AuthentificationContext
    {
        #region Constructors

        public AuthentificationContext()
        {
            _authentificationContextRedirectionManager = new AuthentificationContextRedirectionManager(this);
            _rememberMe = new RememberMeContext();
        }

        #endregion
        public static AuthentificationContext Current
        {
            get
            {
                if (HttpContext.Current.Session[CURRENT_SESSIONKEY] == null)
                {
                    HttpContext.Current.Session.Add(CURRENT_SESSIONKEY, new AuthentificationContext());
                }
                return (AuthentificationContext)HttpContext.Current.Session[CURRENT_SESSIONKEY];
            }

        }

        #region Constants

        private const string CURRENT_SESSIONKEY = "AuthentificationContext_Current";
        
        #endregion

        #region Fields

        private readonly AuthentificationContextRedirectionManager _authentificationContextRedirectionManager;
        private readonly RememberMeContext _rememberMe;

        #endregion

        #region Properties

        public bool IsLogged { get; set; }

        public bool IsActiveLosCodeRedirection
        {
            get; set;
            //            get
            //            {
            //                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY];
            //
            //                if (cookie != null && cookie["IsActiveLosCodeRedirection"] != null)
            //                {
            //                    return Boolean.Parse(cookie["IsActiveLosCodeRedirection"]);
            //                }
            //
            //                return true;
            //            }
            //            set
            //            {
            //                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY] ?? new HttpCookie(CURRENT_SESSIONKEY);
            //                cookie["IsActiveLosCodeRedirection"] = Convert.ToString(value);
            //
            //                HttpContext.Current.Response.Cookies.Remove(CURRENT_SESSIONKEY);
            //
            //                HttpContext.Current.Response.Cookies.Add(cookie);
            //            }
        }
        public string LosCode
        {
            get;
            set;
//            get
//            {
//                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY];
//
//                if (cookie != null && cookie["los-code"] != null)
//                {
//                    return cookie["los-code"];
//                }
//
//                return string.Empty;
//            }
//            set
//            {
//                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY] ?? new HttpCookie(CURRENT_SESSIONKEY);
//                cookie["los-code"] = value;
//                HttpContext.Current.Response.Cookies.Set(cookie);
//            }
        }
        public string ReferralCode
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY];

                if (cookie != null && cookie["referral-code"] != null)
                {
                    return cookie["referral-code"];
                }

                return string.Empty;
            }
            set
            {
               
                    var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY] ?? new HttpCookie(CURRENT_SESSIONKEY);
                    cookie["referral-code"] = value;
                    HttpContext.Current.Response.SetCookie(cookie);
                
            }
        }

        public RememberMeContext RememberMe
        {
            get
            {
                return this._rememberMe;
            }

        }

        public bool HasReferralCode
        {
            get
            {
                return !string.IsNullOrEmpty(this.ReferralCode);
            }
        }

        public bool HasLosCode{ 
            get
            {
                return !string.IsNullOrEmpty(this.LosCode);
            }
        
        }


        #endregion

        public void ResetLosCode()
        {
            this.LosCode = string.Empty;
        }

        public void Redirect(ClientProfile clientProfile)
        {
            _authentificationContextRedirectionManager.Redirect(clientProfile);
        }
    }
}