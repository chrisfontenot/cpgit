using System.Collections.Generic;
using System.Net.Mail;
using System.Web;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Validator;
using CredAbility.Web.API.Email;
using CredAbility.Web.Identity;
using CredAbility.Web.SitecoreLib.Item;


namespace CredAbility.Web.Authentification
{
    public class AccountManager
    {

        public void SendAccountCreationEmail(ClientProfile clientProfile)
        {
            EmailTemplateItem emailTemplateItem = Sitecore.Context.Item.FirstEmailTemplate();
           
            if(emailTemplateItem == null)
            {
                return;
            }
            Dictionary<string, string> convertedTags = new Dictionary<string, string>
                                                      {
                                                          {"{FIRSTNAME}", clientProfile.FirstName},
                                                          {"{LASTNAME}", clientProfile.LastName},
                                                          {"{EMAIL}", clientProfile.EmailAddress},
                                                          {"{USERNAME}", clientProfile.Username},
                                                          {"{FULLNAME}", string.Format("{0}, {1}",clientProfile.LastName, clientProfile.FirstName)}
                                                      };
			try
			{
				EmailManager.SendEmailTo(new MailAddress(clientProfile.EmailAddress), emailTemplateItem, convertedTags);
			}
			catch
			{// Make sure system dosent chrash if email not sent.
			}
        }

        public ActionResult CreateAccountFromOfflineProfile(Profile profile, string confirmPassword)
        {
            ActionResult result = this.ValidateOfflineProfile(profile); 
            profile.ReferringSite = AuthentificationContext.Current.ReferralCode;
            if (result.IsSuccessful)
            {
                if (!OfflineProfileContext.IsAccountCreated)
                {
                    result.MergeActionResult(AccountProxy.CreateAccountFromOfflineProfile(profile, confirmPassword));
                }
                if (result.IsSuccessful)
                {
                    profile.ClientID = IdentityRepository.GetUserIdByUsername(profile.Username).Value.ToString();
                    result.MergeActionResult(AccountProxy.SaveAccountFromOfflineProfile(profile, profile.HostId));
                    if (result.IsSuccessful)
                    {
                        this.SendAccountCreationEmail(profile);
                        ClientProfileManager manager = new ClientProfileManager();
                        manager.Login(profile.Username, profile.Password, false, true, true);
                    }
                }
            }
            return result;
        }

        private ActionResult ValidateOfflineProfile(Profile profile)
        {
            OfflineProfileUpdateValidator validator = new OfflineProfileValidator();
            if (OfflineProfileContext.IsAccountCreated)
            {
                validator = new OfflineProfileUpdateValidator();
                
            }
            return validator.Validate(profile);
        }

        public ActionResult CreateAccount(ClientProfile profile, string confirmPassword)
        {
            ClientProfileValidator validator = new ClientProfileValidator();
            ActionResult result = validator.Validate(profile);
            if (result.IsSuccessful)
            {
                profile.ReferringSite = AuthentificationContext.Current.ReferralCode;
                result.MergeActionResult(AccountProxy.CreateAccount(profile, confirmPassword));
                if (result.IsSuccessful)
                {
                    ClientProfileManager profileManager = new ClientProfileManager();
                    Sitecore.Diagnostics.Log.Audit(string.Format("Account creation successed. Session informations: username: {0}, clientId: {1}, session Id: {2}", profile.Username, profile.ClientID, HttpContext.Current.Session.SessionID), profile);
                    this.SendAccountCreationEmail(profile);
                    result.MergeActionResult(profileManager.CreateAccountLogin(profile.FirstName, profile.LastName, profile.Username, profile.Password, false, true, true));
                }
            }
            return result;
        }
    }
}