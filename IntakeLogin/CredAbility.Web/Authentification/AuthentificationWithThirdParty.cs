﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;

namespace CredAbility.Web.Authentification
{
    internal class AuthentificationWithThirdParty
    {
        public ActionResult<ClientProfile> Login(string username, string password)
        {
            ActionResult<ClientProfile> actionResult = new ActionResult<ClientProfile>();

            var response = IdentityRepository.Login(username, password);

            actionResult.IsSuccessful = !response.HasError;

            if (response.HasError)
            {
                actionResult.Messages.Add(new ResultMessage(response.ErrorMessage, response.ErrorCode));
            }
            else
            {
                actionResult.Value = response.ResponseValue;
            }

            return actionResult;
        }

        public ActionResult Logout(string userID)
        {
            ActionResult actionResult = new ActionResult();
            bool isLoggedOut = IdentityRepository.Logout(userID);

            actionResult.IsSuccessful = isLoggedOut;

            if (isLoggedOut)
            {
                Sitecore.Diagnostics.Log.Info("Logout from third party, SUCCESSED", this);
            }
            else
            {
                Sitecore.Diagnostics.Log.Info("Logout from third party, FAILED", this);
            }

            return actionResult;
        }

        public ActionResult<ClientProfile> LoginWithToken(string userID, string tokenID)
        {
            ActionResult<ClientProfile> actionResult = new ActionResult<ClientProfile>();
            ClientProfile clientProfile = new ClientProfile { ClientID = userID, IdentityTokenID = tokenID };

            var response = IdentityRepository.LoginByToken(userID, tokenID);

            actionResult.IsSuccessful = !response.HasError;

            if(actionResult.IsSuccessful)
            {
                actionResult.Value = response.ResponseValue;
            }

            return actionResult;
        }
    }
}
