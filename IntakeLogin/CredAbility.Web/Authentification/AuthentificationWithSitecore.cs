﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;
using CredAbility.Core.Common;

namespace CredAbility.Web.Authentification
{
    internal class AuthentificationWithSitecore
    {
        public ActionResult Login(ClientProfile clientProfile)
        {
            ActionResult actionResult = new ActionResult(false);

            var virtualUser = new VirtualUser(clientProfile);

            actionResult.IsSuccessful = Sitecore.Security.Authentication.AuthenticationManager.Login(virtualUser.SitecoreUser);

            return actionResult;
        }

        public ActionResult Logout()
        {
            ActionResult actionResult = new ActionResult();

            Sitecore.Security.Authentication.AuthenticationManager.Logout();

            //successful if you're not logged in
            actionResult.IsSuccessful = !Sitecore.Context.IsLoggedIn;


            if(actionResult.IsSuccessful)
            {
                Sitecore.Diagnostics.Log.Info("Logout from sitecore, successed", this);
            }
            else
            {
                Sitecore.Diagnostics.Log.Info("Logout from sitecore, failed", this);
            }

            return actionResult;
        }
    }
}
