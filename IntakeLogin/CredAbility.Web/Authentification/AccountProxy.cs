using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Web.Identity;

namespace CredAbility.Web.Authentification
{
    public class AccountProxy
    {

        #region Offline Profile

        public static ActionResult CreateAccountFromOfflineProfile(ClientProfile profile, string confirmPassword)
        {
            ActionResult result = CreateAccount(profile, confirmPassword);

            if (result.IsSuccessful)
            {
                OfflineProfileContext.AccountCreated();
            }

            return result;
        }

        public static ActionResult SaveAccountFromOfflineProfile(Profile profile, string hostId)
        {
            ActionResult result = SaveAccount(profile, hostId);

            if (result.IsSuccessful)
            {
                OfflineProfileContext.Remove();
            }

            return result;

        }

        #endregion

        #region Profile

        public static ActionResult CreateAccount(ClientProfile profile, string confirmPassword)
        {
            ActionResult result = new ActionResult(true);
            UserRegisterResult userRegisterResult = AccountRepository.CreateAccount(profile, confirmPassword);

            result.IsSuccessful = userRegisterResult.IsSuccessful;

            if (!userRegisterResult.IsSuccessful)
            {
                result.Messages = IdentityErrorMessages.GetMessages(userRegisterResult);
            }

            return result;
        }

        public static ActionResult SaveAccount(Profile profile, string hostId)
        {
            ActionResult result = new ActionResult(true);
            UserProfileSaveResult userProfileSaveResult = AccountRepository.SaveProfile(profile, hostId);

            result.IsSuccessful = userProfileSaveResult.IsSuccessful;
            if (!userProfileSaveResult.IsSuccessful)
            {
                result.Messages = IdentityErrorMessages.GetMessages(userProfileSaveResult);
            }
            return result;
        }

        #endregion
    }
}