using System;
using System.Web;
using CredAbility.Core.Client;
using CredAbility.Web.Configuration;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Web.Authentification
{
    public class AuthentificationContextRedirectionManager
    {
        public AuthentificationContextRedirectionManager(AuthentificationContext context)
        {
            this._context = context;
        }

        private readonly AuthentificationContext _context;

        public void Redirect()
        {
            var item = WebUtil.GetQueryString("item");
            var result = item.Length > 0 ? item + ".aspx" : ConfigReader.ConfigSection.Url.MyActivity.Value;

            HttpContext.Current.Response.Redirect(result);
        }

        public void Redirect(ClientProfile clientProfile)
        {
            if(clientProfile == null)
            {
                throw new ArgumentNullException("clientProfile");
            }

            string url = string.Empty;

            if (clientProfile.UpdateSecurityInfoRequired)
            {
                var mustChangeUsername = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangeUsername.SitecorePath);
                WebUtil.Redirect(LinkManager.GetItemUrl(mustChangeUsername));
            }
            else if (clientProfile.MustChangePassword)
            {
                var mustChangePassword = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangePassword.SitecorePath);
                WebUtil.Redirect(LinkManager.GetItemUrl(mustChangePassword));

            }
            else
            {
                this._context.IsActiveLosCodeRedirection = true;
            }


            //If coming from a Third Party
            Sitecore.Diagnostics.Log.Audit(string.Format("AuthentificationRedirection, LOS={0}, IsActiveLosCodeRedirection={1}", this._context.LosCode, this._context.IsActiveLosCodeRedirection), this);
            if (this._context.HasLosCode && this._context.IsActiveLosCodeRedirection)
            {
                string tmpUrl = LosCodes.GetLosUrlWithSso(this._context.LosCode, clientProfile.ClientID, clientProfile.IdentityTokenID, this._context.ReferralCode);

                if (!string.IsNullOrEmpty(tmpUrl))
                {
                    this._context.ResetLosCode();

                    if (Sitecore.Context.User.IsAuthenticated || this._context.IsLogged)
                    {
                        url = tmpUrl;
                    }
                }

                //If coming from the inside of the sitecore site
            }


            if (!string.IsNullOrEmpty(url))
            {
                HttpContext.Current.Response.Redirect(url);
            }
            else
            {
                this.Redirect();
            }

            throw new Exception("Should redirect, but the redirection never happen.");
        }
    }
}