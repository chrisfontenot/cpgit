using CredAbility.Web.Configuration;

namespace CredAbility.Web.Authentification
{
    public sealed class LosCodes
    {
        #region Url Generation

        public static string GetLosUrlWithSso(string code, string clientId, string identityTokenId)
        {

            return GetLosUrlWithSso(code, clientId, identityTokenId, null);
            
        }

        public static string GetLosUrlWithSso(string code, string clientId, string identityTokenId, string referralCode)
        {
            

                string result = string.Empty;
                
            string losCode = GetLosCodeFromWebsiteCode(code);

            var losElement = ConfigReader.ConfigSection.LOSCode[losCode];

            if (losElement != null)
            {
                result = losElement.Url;
                if (!string.IsNullOrEmpty(result))
                {
                    if (losElement.SsoRequired)
                    {
                        result = AppendSsoParamToUrl(result, clientId, identityTokenId, referralCode);
                    }
                }
            }

            return result;
        }

        #endregion

        private static string AppendSsoParamToUrl(string losUrl, string clientId, string identityTokenId, string referralCode)
        {
            if (string.IsNullOrEmpty(referralCode))
            {
                return string.Format("{0}?u={1}&t={2}", losUrl, clientId, identityTokenId);
            }
            
            return string.Format("{0}?u={1}&t={2}&r={3}", losUrl, clientId, identityTokenId, referralCode);
        }


        private static string GetLosCodeFromWebsiteCode(string websiteCode)
        {
            string losCode = "";
            switch (websiteCode.Trim().ToUpper())
            {
                case "BCH":
                    losCode = "BnC";
                    break;
                case "BKDE":
                    losCode = "BKED";
                    break;
                case "HUD":
                    losCode = "FOR";
                    break;
                case "PRP":
                    losCode = "HPP";
                    break;
                case "RVM":
                    losCode = "RM";
                    break;
                default:
                    losCode = websiteCode;
                    break;
            }

            return losCode;
        }

    }
}