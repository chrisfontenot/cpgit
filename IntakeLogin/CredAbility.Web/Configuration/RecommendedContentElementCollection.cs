using System.Configuration;

namespace CredAbility.Web.Configuration
{
    [ConfigurationCollection(typeof(RoleElement), AddItemName = "Role", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class RecommendedContentElementCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new RoleElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RoleElement)element).Name;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "Role"; }
        }

        public new RoleElement this[string name]
        {
            get { return (RoleElement)base.BaseGet(name); }
        }

        public RoleElement this[int id]
        {
            get { return (RoleElement)base.BaseGet(id); }
        }
    }
}