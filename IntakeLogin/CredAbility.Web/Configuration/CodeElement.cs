using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class CodeElement : ConfigurationElement
    {
        [ConfigurationProperty("los")]
        public string LOS
        {
            get { return (string)this["los"]; }
        }

        [ConfigurationProperty("url")]
        public string Url
        {
            get { return (string)this["url"]; }
        }

        [ConfigurationProperty("sso-required")]
        public bool SsoRequired
        {
            get
            {
                if (this["sso-required"] == null)
                    return true;
                
                return (bool)this["sso-required"];
            }
        }

    }
}