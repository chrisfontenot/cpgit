using System.Configuration;

namespace CredAbility.Web.Configuration
{
    [ConfigurationCollection(typeof(CodeElement), AddItemName = "code", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class CodeElementCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new CodeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CodeElement)element).LOS;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "code"; }
        }

        public new CodeElement this[string name]
        {
            get { return (CodeElement)base.BaseGet(name); }
        }
    }
}