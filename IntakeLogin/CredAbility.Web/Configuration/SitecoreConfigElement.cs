using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public  class SitecoreConfigElement : ConfigurationElement
    {
        [ConfigurationProperty("sitecorePath")]
        public string SitecorePath
        {
            get { return (string)this["sitecorePath"]; }
            set
            {
                this["sitecorePath"] = value;
            }

        }
    }
}