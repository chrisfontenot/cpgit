using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class HeroElement : ConfigurationElement
    {
        [ConfigurationProperty("timer")]
        public int Timer
        {
            get { return (int)this["timer"]; }
        }

    }
}