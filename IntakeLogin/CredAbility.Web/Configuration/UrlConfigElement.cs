using System.Configuration;

namespace CredAbility.Web.Configuration
{
	public class UrlConfigElement:ConfigurationElement
	{
		[ConfigurationProperty("my-activity")]
		public ValueElement MyActivity
		{
			get
			{
				return (ValueElement)this["my-activity"];
			}
			set
			{
				this["my-activity"] = value;
			}
		}

		[ConfigurationProperty("need-zip")]
		public ValueElement NeedZip
		{
			get
			{
				return (ValueElement)this["need-zip"];
			}
			set
			{
				this["need-zip"] = value;
			}
		}

		[ConfigurationProperty("return-to-class")]
		public ValueElement ReturnToClass
		{
			get
			{
				return (ValueElement)this["return-to-class"];
			}
			set
			{
				this["return-to-class"] = value;
			}
		}

		[ConfigurationProperty("logout")]
		public ValueElement Logout
		{
			get
			{
				return (ValueElement)this["logout"];
			}
			set
			{
				this["logout"] = value;
			}
		}

		[ConfigurationProperty("change-password")]
		public ValueElement ChangePassword
		{
			get
			{
				return (ValueElement)this["change-password"];
			}
			set
			{
				this["change-password"] = value;
			}
		}

		[ConfigurationProperty("change-username")]
		public ValueElement ChangeUsername
		{
			get
			{
				return (ValueElement)this["change-username"];
			}
			set
			{
				this["change-username"] = value;
			}
		}

        [ConfigurationProperty("get-document")]
        public ValueElement GetDocument
        {
            get
            {
                return (ValueElement)this["get-document"];
            }
            set
            {
                this["get-document"] = value;
            }
        }
    }
}