using System.Configuration;

namespace CredAbility.Web.Configuration
{
    [ConfigurationCollection(typeof(KeyValueElement), AddItemName = "service", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class KeyValueElementCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new KeyValueElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((KeyValueElement)element).Key;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "service"; }
        }

        public new KeyValueElement this[string name]
        {
            get { return (KeyValueElement)base.BaseGet(name); }
        }
    }
}