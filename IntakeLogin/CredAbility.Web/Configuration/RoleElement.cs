using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class RoleElement : ConfigurationElement
    {
        [ConfigurationProperty("name")]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        [ConfigurationProperty("categories")]
        public CategoryElementCollection Categories
        {
            get { return (CategoryElementCollection)this["categories"]; }
        }
    }

    
}