﻿using System;
using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class ConfigReader : ConfigurationSection
    {
        public static ConfigReader ConfigSection
        {
            get
            {
                return (ConfigReader) System.Configuration.ConfigurationManager.GetSection("credability");
            }
        }

        [ConfigurationProperty("max-activity-by-page")]
        public ValueElement MaxActivityByPage
        {
            get
            {
                return (ValueElement)this["max-activity-by-page"];
            }
            set
            {
                this["max-activity-by-page"] = value;
            }

        }


        [ConfigurationProperty("max-activity-to-show")]
        public ValueElement MaxActivityToShow
        {
            get
            {
                return (ValueElement)this["max-activity-to-show"];
            }
            set
            {
                this["max-activity-to-show"] = value;
            }

        }

        [ConfigurationProperty("currency-cultureInfo")]
        public ValueElement CurrencyCultureInfo
        {
            get
            {
                return (ValueElement)this["currency-cultureInfo"];
            }
            set
            {
                this["currency-cultureInfo"] = value;
            }

        }


        [ConfigurationProperty("copyright")]
        public SitecoreConfigElement Copyright
        {
            get
            {
                return (SitecoreConfigElement)this["copyright"];
            }
            set
            {
                this["copyright"] = value;
            }

        }

        [ConfigurationProperty("header")]
        public SitecoreConfigElement Header
        {
            get
            {
                return (SitecoreConfigElement)this["header"];
            }
            set
            {
                this["header"] = value;
            }

        }

        [ConfigurationProperty("subHeader")]
        public SitecoreConfigElement SubHeader
        {
            get
            {
                return (SitecoreConfigElement)this["subHeader"];
            }
            set
            {
                this["subHeader"] = value;
            }

        }

        [ConfigurationProperty("must-change-password")]
        public SitecoreConfigElement MustChangePassword
        {
            get
            {
                return (SitecoreConfigElement)this["must-change-password"];
            }
            set
            {
                this["must-change-password"] = value;
            }
        }

        [ConfigurationProperty("must-change-username")]
        public SitecoreConfigElement MustChangeUsername
        {
            get
            {
                return (SitecoreConfigElement)this["must-change-username"];
            }
            set
            {
                this["must-change-username"] = value;
            }
        }

        [ConfigurationProperty("verify-profile")]
        public SitecoreConfigElement VerifyProfile
        {
            get
            {
                return (SitecoreConfigElement)this["verify-profile"];
            }
            set
            {
                this["verify-profile"] = value;
            }
        }

        [ConfigurationProperty("create-account")]
        public SitecoreConfigElement CreateAccount
        {
            get
            {
                return (SitecoreConfigElement)this["create-account"];
            }
            set
            {
                this["create-account"] = value;
            }
        }

     

        [ConfigurationProperty("partnersLogo")]
        public SitecoreConfigElement PartnersLogo
        {
            get
            {
                return (SitecoreConfigElement)this["partnersLogo"];
            }
            set
            {
                this["partnersLogo"] = value;
            }

        }

        [ConfigurationProperty("course")]
        public SitecoreConfigElement Course
        {
            get
            {
                return (SitecoreConfigElement)this["course"];
            }
            set
            {
                this["course"] = value;
            }

        }

        [ConfigurationProperty("footer")]
        public SitecoreConfigElement Footer
        {
            get
            {
                return (SitecoreConfigElement)this["footer"];
            }
            set
            {
                this["footer"] = value;
            }

        }



        [ConfigurationProperty("home")]
        public SitecoreConfigElement Home
        {
            get
            {
                return (SitecoreConfigElement)this["home"];
            }
            set
            {
                this["home"] = value;
            }

        }

        [ConfigurationProperty("sitecore-home")]
        public SitecoreConfigElement SitecoreHome
        {
            get
            {
                return (SitecoreConfigElement)this["sitecore-home"];
            }
            set
            {
                this["sitecore-home"] = value;
            }

        }

        [ConfigurationProperty("course-category")]
        public SitecoreConfigElement CourseCategory
        {
            get
            {
                return (SitecoreConfigElement)this["course-category"];
            }
            set
            {
                this["course-category"] = value;
            }

        }

        [ConfigurationProperty("faq-categories")]
        public SitecoreConfigElement FaqCategories
        {
            get
            {
                return (SitecoreConfigElement)this["faq-categories"];
            }
            set
            {
                this["faq-categories"] = value;
            }

        }

        [ConfigurationProperty("los-codes")]
        public CodeElementCollection LOSCode
        {
            get
            {
                return (CodeElementCollection)this["los-codes"];
            }
        }

        [ConfigurationProperty("recommended-content")]
        public RecommendedContentElementCollection RecommendedContent
        {
            get
            {
                return (RecommendedContentElementCollection)this["recommended-content"];
            }
        }


        [ConfigurationProperty("services")]
        public KeyValueElementCollection Services
        {
            get
            {
                return (KeyValueElementCollection)this["services"];
            }
        }

        [ConfigurationProperty("url")]
        public UrlConfigElement Url
        {
            get
            {
                return (UrlConfigElement)this["url"];
            }
            set
            {
                this["url"] = value;
            }

        }

      

        [ConfigurationProperty("language")]
        public SitecoreConfigElement Language
        {
            get
            {
                return (SitecoreConfigElement)this["language"];
            }
            set
            {
                this["language"] = value;
            }

        }

        [ConfigurationProperty("partner-program")]
        public SitecoreConfigElement PartnerProgram
        {
            get
            {
                return (SitecoreConfigElement)this["partner-program"];
            }
            set
            {
                this["partner-program"] = value;
            }

        }

        [ConfigurationProperty("get-started")]
        public SitecoreConfigElement getstarted
        {
            get
            {
                return (SitecoreConfigElement)this["get-started"];
            }
            set
            {
                this["get-started"] = value;
            }

        }

        [ConfigurationProperty("create-account")]
        public SitecoreConfigElement createaccount
        {
            get
            {
                return (SitecoreConfigElement)this["create-account"];
            }
            set
            {
                this["create-account"] = value;
            }

        }

        [ConfigurationProperty("action-plan")]
        public SitecoreConfigElement ActionPlan
        {
            get
            {
                return (SitecoreConfigElement)this["action-plan"];
            }
            set
            {
                this["action-plan"] = value;
            }

        }

        [ConfigurationProperty("past-summary")]
        public SitecoreConfigElement PastSummary
        {
            get
            {
                return (SitecoreConfigElement)this["past-summary"];
            }
            set
            {
                this["past-summary"] = value;
            }

        }

        [ConfigurationProperty("summary")]
        public SitecoreConfigElement Summary
        {
            get
            {
                return (SitecoreConfigElement)this["summary"];
            }
            set
            {
                this["summary"] = value;
            }

        }

        [ConfigurationProperty("my-certificate")]
        public SitecoreConfigElement MyCertificate
        {
            get
            {
                return (SitecoreConfigElement)this["my-certificate"];
            }
            set
            {
                this["my-certificate"] = value;
            }

        }

        [ConfigurationProperty("HeroShotRotationTime")]
        public HeroElement HeroShotRotationTime
        {
            get
            {
                return (HeroElement)this["HeroShotRotationTime"];
            }
            set
            {
                this["HeroShotRotationTime"] = value;
            }

        }

        [ConfigurationProperty("FeeWaiverPercentage")]
        public ValueElement FeeWaiverPercentage
        {
            get
            {
                return (ValueElement)this["FeeWaiverPercentage"];
            }
            set
            {
                this["FeeWaiverPercentage"] = value;
            }
        }
    }
}
