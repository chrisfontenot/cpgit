using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class CategoryElement : ConfigurationElement
    {
        [ConfigurationProperty("id")]
        public string Id
        {
            get { return (string)this["id"]; }
        }
    }
}