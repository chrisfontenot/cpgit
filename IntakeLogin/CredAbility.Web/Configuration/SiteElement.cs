using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class SiteElement : ConfigurationElement
    {
        [ConfigurationProperty("ref-code")]
        public string RefCode
        {
            get { return (string)this["ref-code"]; }
        }

        [ConfigurationProperty("partner-code")]
        public string PartnerCode
        {
            get { return (string)this["partner-code"]; }
        }

    }
}