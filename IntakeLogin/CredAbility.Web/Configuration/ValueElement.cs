using System.Configuration;

namespace CredAbility.Web.Configuration
{
    public class ValueElement  : ConfigurationElement
    {

        [ConfigurationProperty("value")]
        public string Value
        {
            get { return (string)this["value"]; }
            set
            {
                this["value"] = value;
            }

        }
    }
}