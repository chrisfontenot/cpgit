using System.Configuration;

namespace CredAbility.Web.Configuration
{
    [ConfigurationCollection(typeof(CategoryElement), AddItemName = "category", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class CategoryElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CategoryElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CategoryElement)element).Id;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "category"; }
        }

        public CategoryElement this[int id]
        {
            get { return (CategoryElement)base.BaseGet(id); }
        }
    }
}