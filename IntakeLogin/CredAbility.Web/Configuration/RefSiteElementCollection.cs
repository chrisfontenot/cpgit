using System.Configuration;

namespace CredAbility.Web.Configuration
{
    [ConfigurationCollection(typeof(SiteElement), AddItemName = "site", CollectionType = ConfigurationElementCollectionType.BasicMap)]
    public class RefSiteElementCollection : ConfigurationElementCollection
    {

        protected override ConfigurationElement CreateNewElement()
        {
            return new SiteElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SiteElement)element).RefCode;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "site"; }
        }

        public new SiteElement this[string name]
        {
            get { return (SiteElement)base.BaseGet(name); }
        }



    }
}