﻿
using CredAbility.Core;
using CredAbility.Core.Common;
using CredAbility.Data.Validator;

namespace CredAbility.Web.Validator
{
    public class ContactUsValidator : Validator<ContactUs>
    {
        public override ActionResult Validate(ContactUs obj)
        {
            var actionResult = new ActionResult {IsSuccessful = true};
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.Firstname, "FIRSTNAME-REQUIRED", Resource.ContactUsValidator_FirstNameRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.Lastname, "LASTNAME-REQUIRED", Resource.ContactUsValidator_LastNameRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.Email, "EMAIL-REQUIRED", Resource.ContactUsValidator_EmailRequired));
            actionResult.MergeActionResult(this.ValidateEmailFormat(obj.Email, "EMAIL-WRONGFORMAT", Resource.ContactUsValidator_InvalidEmailFormat));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.Comment, "COMMENT-REQUIRED", Resource.ContactUsValidator_CommentRequired));
            return actionResult;
        }

    }
}
