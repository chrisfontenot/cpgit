﻿
using CredAbility.Core;
using CredAbility.Core.Common;
using CredAbility.Core.CreditScoreChallenge;
using CredAbility.Data.Validator;

namespace CredAbility.Web.Validator
{
    public class CreditScoreChallengeAuthorizationValidator : Validator<Contestant>
    {
        public override ActionResult Validate(Contestant obj)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.SSN.FormatedSSN, "SSN-REQUIRED", Resource.CSCAuthorization_SsnRequired));
            actionResult.MergeActionResult(this.ValidateSsnFormat(obj.SSN.FormatedSSN, "SSN-WRONGFORMAT", Resource.CscAuthorization_SsnWrongFormat));
            
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.PrimaryPhoneNumber, "PRIMARY_PHONENUMBER-REQUIRED", Resource.CSCAuthorization_PrimaryPhonenumberRequired));

            actionResult.MergeActionResult(this.ValidateStringRequired(obj.StreetAddress, "STREET_ADDRESS-REQUIRED", Resource.CscAuthorization_StreetAddressRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.StreetAddress, "STATE-REQUIRED", Resource.CscAuthorization_StateRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.StreetAddress, "CITY-REQUIRED", Resource.CscAuthorization_CityRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.StreetAddress, "ZIP_CODE-REQUIRED", Resource.CscAuthorization_ZipCodeRequired));
            
            return actionResult;
        }

    }
}