using System.Collections.Generic;
using System.IO;
using CredAbility.Web.Parsers;
using CredAbility.Web.Cache;

namespace CredAbility.Web.API
{
    public class FileCombo
    {
        public static string GetTextFile(List<string> files) {
            return GetTextFile(files, null);            
        }

        public static string GetTextFile(List<string> files, IFileParser parser){
            var output = new List<string>(files.Count);
            StreamReader sr;
            foreach (var file in files) {
                if (parser == null)
                {
                    sr = new StreamReader(file,false);
                    output.Add(sr.ReadToEnd());
                    sr.Close();
                }
                else {
                    output.Add(FileCache.GetTextFile(file, parser));
                }
            }

            return string.Join("", output.ToArray());
        }
    }
}
