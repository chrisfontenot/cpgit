﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Documents;
using Lucene.Net.Search;

namespace CredAbility.Web.API.Search
{
    public class SearchResult
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Url { get; set; }

        public SearchResult(Document hit)
        {
            Title = hit.Get("name");
            Summary = hit.Get("summary");
            Url = hit.Get("url");
        }
    }
}
