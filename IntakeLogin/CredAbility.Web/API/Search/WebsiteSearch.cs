﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Sitecore;
using Sitecore.Data.Items;

namespace CredAbility.Web.API.Search
{
    public class WebsiteSearch
    {
        public List<SearchResult> FullTextSearch(string args, string category)
        {
            var list = new List<SearchResult>();
            //Get index for the database by index name
            const string indexName = "CredAbility";
            var db = Context.Database;
            var index = db.Indexes[indexName];
            
            Sitecore.Diagnostics.Error.AssertNotNull(index,
                                                     "There is no " + indexName + " index on the current database (" +
                                                     db.Name + ")");

            //IndexSearcher is the object which performs search through the index
            var searcher = index.GetSearcher(db);
            try
            {
                Analyzer analyzer = new StandardAnalyzer();

                var qp = new QueryParser("summary", analyzer);
                qp.SetDefaultOperator(QueryParser.Operator.AND);
                var query = qp.Parse(args);
                Hits hits;

                if (category == "All")
                {
                    hits = searcher.Search(query);
                }
                else
                {
                    var qpCategory = new QueryParser("category", analyzer);
                    qpCategory.SetDefaultOperator(QueryParser.Operator.AND);
                    var queryCategory = qpCategory.Parse(category.Replace("-",""));
                    var qf = new QueryFilter(queryCategory);
                    hits = searcher.Search(query, qf);
                }



                for (var i = 0; i < hits.Length(); i++)
                {
                    list.Add(new SearchResult(hits.Doc(i)));
                }
            }

            //Check for injection CREDA-539
            catch (ParseException)
            {
                return new List<SearchResult>();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message, e);
            }
            finally
            {
                // DO NOT forget to close searcher. Because it may lock index.
                // Be careful not to call this method while you are still using objects like Hits. 
                searcher.Close();
            }
            return list;
        }
    }
}
