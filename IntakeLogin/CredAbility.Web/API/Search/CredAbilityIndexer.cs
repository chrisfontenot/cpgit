﻿using System.Text.RegularExpressions;
using Lucene.Net.Documents;
using Sitecore.Data.Items;
using Sitecore.Links;
using System.Linq;

namespace CredAbility.Web.API.Search
{
    public class CredAbilityIndexer : Sitecore.Data.Indexing.Index
    {
        public CredAbilityIndexer(string indexName)
            : base(indexName)
        {
        }

        protected override void AddFields(Item item, Document document)
        {
            document.Add(new Field("name", GetItemName(item), Field.Store.YES, Field.Index.TOKENIZED));
            var summaryValue = "";
            foreach (Sitecore.Data.Fields.Field field in item.Fields)
            {
                if ((field.Type != "Single-Line Text" && field.Type != "Multi-Line Text") && field.Type != "Rich Text")
                    continue;
                summaryValue += field.Value + (field == item.Fields.Last()? "" : " | ");
            }
            document.Add(new Field("summary", StripHTML(summaryValue), Field.Store.YES, Field.Index.TOKENIZED));
            document.Add(new Field("category", GetSectionName(item), Field.Store.YES, Field.Index.UN_TOKENIZED));
            document.Add(new Field("url", GetItemUrl(item), Field.Store.YES, Field.Index.NO));
        }

        const string HtmlTagPattern = "<.*?>";

        static string StripHTML(string inputString)
        {
            return Regex.Replace
              (inputString, HtmlTagPattern, string.Empty);
        }

        private static string GetItemName(Item item)
        {
            if (item.TemplateName == "FAQ Element")
                return item.Fields["Question"].Value;

            return item.Fields["Page Title"].Value;
        }

        private static string GetItemUrl(Item item)
        {
            var urlOpts = LinkManager.GetDefaultUrlOptions();
            urlOpts.Site = Sitecore.Sites.SiteContextFactory.GetSiteContext("website");
            if (item.TemplateName == "FAQ Element")
                return LinkManager.GetItemUrl(item.Parent.Parent, urlOpts);
            return LinkManager.GetItemUrl(item, urlOpts);
        }

        private static string GetSectionName(Item item)
        {
            if (item.TemplateName == "FAQ Element")
                return "faq";
            if (item.Parent == null)
                return "all";
            if (item.Parent.TemplateName == "Section Folder")
                return item.Parent.Name.ToLower().Replace("-","");
            if (item.Parent.TemplateName == "Sub-Section Folder" && item.Parent.Name == "CredAbilityU")
                return item.Parent.Name.ToLower().Replace("-", "");
            return GetSectionName(item.Parent);

        }
    }
}