﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using CredAbility.Web.SitecoreLib.Item;

namespace CredAbility.Web.API.Email
{
    public static class EmailManager
    {
        public static void SendEmail(string fullName, string email, string subject, string body, IEnumerable<MailAddress> toAddresses)
        {
			var mailMessage = new MailMessage
			{
				Sender = new MailAddress(email, fullName),
				From = new MailAddress(email, fullName),
				ReplyTo = new MailAddress(email, fullName),
				Subject = subject,
				Body = body,
				IsBodyHtml = true
			};
            foreach (var addresse in toAddresses)
            {
                mailMessage.To.Add(addresse);
            }
            Sitecore.MainUtil.SendMail(mailMessage);
        }

        public static void SendEmailTo(MailAddress to, EmailTemplateItem emailTemplateItem, Dictionary<string, string> tags)
        {
            if(emailTemplateItem == null)
            {
                throw new ArgumentNullException("emailTemplateItem");
            }

            string body = emailTemplateItem.Body;
            string subject = emailTemplateItem.Subject;

            if(tags != null)
            {
                subject = ReplaceTags(subject, tags);
                body = ReplaceTags(body, tags);
            }

            MailAddress sender = emailTemplateItem.Sender ?? new MailAddress("no-reply@credability.org");
            var mailMessage = new MailMessage
            {
                Sender = sender,
                From = sender,
                ReplyTo = sender,
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };
            mailMessage.To.Add(to);
            Sitecore.MainUtil.SendMail(mailMessage);
        }

        public static string ReplaceTags(string value, Dictionary<string, string> tags)
        {
            foreach (var tag in tags)
            {
                value = value.Replace(tag.Key, tag.Value);
            }

            return value;
        }
    }
}
