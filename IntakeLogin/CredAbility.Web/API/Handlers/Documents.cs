﻿using System.Web;
using System.Web.SessionState;
using CredAbility.Data.Identity;
using CredAbility.Web.Authentification;

namespace CredAbility.Web.API.Handlers
{
	public class Documents:IHttpHandler, IRequiresSessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var documentUrl = "";
			var documentId = string.IsNullOrEmpty(context.Request.QueryString["id"]) 
				? ClientProfileRepository.GetDocumentTokenId(ClientProfileManager.CurrentClientProfile) 
				: context.Request.QueryString["id"];
			var documentType = context.Request.QueryString["type"];
			if(!string.IsNullOrEmpty(documentId))
			{
				if(string.IsNullOrEmpty(documentType))
				{
					documentUrl = ClientProfileRepository.GetDocumentUrl(ClientProfileManager.CurrentClientProfile, documentId);
				}
				else if(documentType == "certificate")
				{
					documentUrl = ClientProfileRepository.GetClientDocuments(ClientProfileManager.CurrentClientProfile).GetAllCertificates().Find(DocIndex => DocIndex.ID == documentId).Url;
				}
			}
			context.Response.Redirect(documentUrl);
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}