﻿using System.Web;
using CredAbility.Web.Parsers;
using CredAbility.Web.Cache;


namespace CredAbility.Web.API.Handlers
{
    public class JsMinify : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var objResponse = context.Response;
            var file = context.Request.PhysicalPath;
            objResponse.Write(FileCache.GetTextFile(file, new JsMinifyParser()));
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
