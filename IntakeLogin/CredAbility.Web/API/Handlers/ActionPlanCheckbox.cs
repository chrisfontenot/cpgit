﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Core.Activity;
using CredAbility.Data.ActionPlan;
using CredAbility.Data.Activity;

namespace CredAbility.Web.API.Handlers
{
    public class ActionPlanCheckbox : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {

            var actionCheckId = context.Request.QueryString["actionid"];
            var clientId = context.Request.QueryString["clientid"];
            var actionTitle = context.Server.HtmlDecode(context.Request.QueryString["title"]);
            var idIndex = actionCheckId.IndexOf('-') + 1;
            var id = actionCheckId.Substring(idIndex);
            var actionId = Convert.ToInt32(id);
            var result = ActionPlanRepository.SaveActionItemCompletion(clientId, actionId);
            if (result)
            {
                var userActivity = new UserActivity(clientId)
                                       {
                                           Type = UserActivityType.Marked,
                                           Text =
                                               string.Format(Resource.UserActivity_MarkActionAsCompleted, actionTitle)
                                       };
                UserActivityRepository.Save(userActivity);
                context.Response.Write(string.Format(Resource.ActionPlan_CompletedOn,DateTime.Now.ToLongDateString()));
            }
            else
                context.Response.StatusCode = 500;
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
