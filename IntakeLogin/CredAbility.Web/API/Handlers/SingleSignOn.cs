﻿using System.Web;
using System.Web.SessionState;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using Sitecore.Web;

namespace CredAbility.Web.API.Handlers
{
	public class SingleSignOn:IHttpHandler, IRequiresSessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			string redirectUrl = ConfigReader.ConfigSection.Url.MyActivity.Value;
			string userId = WebUtil.GetQueryString("u");
			string token = WebUtil.GetQueryString("t");

			if(!string.IsNullOrEmpty(WebUtil.GetQueryString("r")))
			{
				redirectUrl = WebUtil.GetQueryString("r");
			}

			if(!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(token))
			{
				ClientProfileManager profileManager = new ClientProfileManager();
				if(!Sitecore.Context.User.IsAuthenticated)
				{
					profileManager.Logout();
				}
				profileManager.LoginWithToken(userId, token, redirectUrl);
			}
			context.Response.Redirect(redirectUrl);
		}

		public bool IsReusable
		{
			get { return false; }
		}
	}
}