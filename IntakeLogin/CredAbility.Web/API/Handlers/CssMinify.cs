﻿using System.Web;
using CredAbility.Web.Cache;


namespace CredAbility.Web.Web.API.Handlers
{
    public class CssMinify : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;
            string file = context.Request.PhysicalPath;

            response.ContentType = "text/css";
            response.Write(FileCache.GetTextFile(file, new Parsers.CssMinifyParser()));
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
