﻿using System.Collections.Generic;
using System.Web;
using CredAbility.Web.Parsers;


namespace CredAbility.Web.API.Handlers
{
    public abstract class ComboBase : IHttpHandler
    {
        protected HttpContext _context;
        protected string _contentResult;
        public void ProcessRequest(HttpContext context)
        {
            this._context = context;
            var response = context.Response;
            this.SetContentType();

            string[] requestedFileNames = GetRequestedFilenames();

            if (IsRequestAcceptable(requestedFileNames))
            {
                List<string> mappedFileNames = MapFileNames(requestedFileNames);
                this._contentResult = FileCombo.GetTextFile(mappedFileNames, GetMinifierParser());
                this.PreResponseWrite();
                response.Write(this._contentResult);
            }
            else
            {
                response.Redirect("/Error.aspx");
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }


        public virtual IFileParser GetMinifierParser()
        {
            return null;
        }

        public List<string> MapFileNames(string[] requestedFileNames)
        {
            var mappedFileNames = new List<string>(requestedFileNames.Length);

            foreach (var file in requestedFileNames)
            {
                mappedFileNames.Add(this._context.Server.MapPath(file));
            }

            return mappedFileNames;
        }

        public string[] GetRequestedFilenames()
        {
            var filesParam = this._context.Request.QueryString.Get("f");
            var splitedFiles = filesParam.Split(',');
            return splitedFiles;
        }

        private void SetContentType()
        {
            var contentType = this._context.Request.QueryString.Get("ct");

            if (!string.IsNullOrEmpty(contentType))
            {
                this._context.Response.ContentType = contentType;
            }
        }

        protected virtual void PreResponseWrite()
        {
        }

        private bool IsRequestAcceptable(string[] requestedFileNames)
        {
            foreach (string fileName in requestedFileNames)
            {
                if (!(fileName.EndsWith(".css") || fileName.EndsWith(".js")))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
