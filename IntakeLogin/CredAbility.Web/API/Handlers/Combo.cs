using System.Collections.Generic;
using System.Web;
using CredAbility.Web.Parsers;


namespace CredAbility.Web.API.Handlers
{
    public class Combo : ComboBase
    {
        public override IFileParser GetMinifierParser()
        {
            var minifer = this._context.Request.QueryString.Get("m");
            IFileParser parser = null;
            switch (minifer)
            {
                case "js":
                    parser = new JsMinifyParser();
                    break;
                case "css":
                    parser = new CssMinifyParser();
                    break;
            }

            return parser;
        }

    }
}
