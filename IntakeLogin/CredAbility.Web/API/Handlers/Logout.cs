﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using CredAbility.Web.Authentification;

namespace CredAbility.Web.API.Handlers
{
    public class Logout : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            var manager = new ClientProfileManager();
            manager.Logout();
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
