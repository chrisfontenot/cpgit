﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Web.Authentification;

namespace CredAbility.Web.API.Handlers
{
    public class SessionInfo : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var r = context.Response;
            r.Write("<h1></h1><hr>");
            r.Write("<h2>Session</h2>");

            foreach (var key in HttpContext.Current.Session.Keys)
            {
                r.Write(key  + "<br>");
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
