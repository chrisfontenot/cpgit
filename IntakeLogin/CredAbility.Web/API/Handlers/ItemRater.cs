﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Core.Rate;
using CredAbility.Data.Rate;

namespace CredAbility.Web.API.Handlers
{
    public class ItemRater : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var itemRate = ItemRateRepository.Get(new Guid(context.Request.QueryString["i"])) ?? new ItemRate {ItemID = new Guid(context.Request.QueryString["i"])};

            var result = "";
            
                itemRate.AddRate(context.Request.QueryString["r"]);
                ItemRateRepository.Save(itemRate);

                result = string.Format("'totalRate':'{0}','average':'{1}'", itemRate.TotalRate, itemRate.Average);

                var cookie = context.Request.Cookies["itemrated"] ?? new HttpCookie("itemrated");

                cookie[itemRate.ItemID.ToString()] = context.Request.QueryString["r"];

                context.Response.SetCookie(cookie);
                context.Response.Write("{" + result + "}");
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
