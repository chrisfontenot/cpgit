﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using Sitecore.Links;

namespace CredAbility.Web.API.Handlers
{
    public class DocumentSelector : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            var clientProfile = ClientProfileManager.CurrentClientProfile;
            clientProfile.DocumentID = context.Request.QueryString["id"];
            ClientProfileManager.SaveClientProfile(clientProfile);
            var mySummaryPage = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.Summary.SitecorePath);
            var mySummaryPageUrl = LinkManager.GetItemUrl(mySummaryPage);
            context.Response.Redirect(mySummaryPageUrl);
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
