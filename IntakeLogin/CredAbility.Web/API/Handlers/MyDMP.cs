﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;

namespace CredAbility.Web.API.Handlers
{
    public class MyDMP : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {

            //todo : code clean up with new functionnality "AuthentificationContext"
            string url = ConfigReader.ConfigSection.LOSCode["CA"].Url;

            ClientProfileManager manager = new ClientProfileManager();
            if (!string.IsNullOrEmpty(url))
            {
                //TODO : CHECK REFCODE
                HttpContext.Current.Response.Redirect(url + "?u=" + ClientProfileManager.CurrentClientProfile.ClientID + "&t=" + ClientProfileManager.CurrentClientProfile.IdentityTokenID + "&r=" + AuthentificationContext.Current.ReferralCode);
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}
