﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CredAbility.Web.API
{
    public abstract class MinifierBase
    {
        protected const int EOF = -1;

        protected StreamReader sr;
        protected StringWriter sw;
        protected int theA;
        protected int theB;
        protected int theLookahead = EOF;


        /// <summary>
        /// Minify the input script
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public string Minify(string src)
        {

            theA = '\n';
            theB = 0;
            theLookahead = EOF;


            using (sr = new StreamReader(src))
            {
                using (sw = new StringWriter())  //used to be a StreamWriter
                {
                    this.Minifier();
                    return sw.ToString(); // return the minified string
                }
            }
        }


        protected void put(int c)
        {
            sw.Write((char)c);
        }


        protected abstract void Minifier();
    }
}
