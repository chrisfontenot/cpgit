using System;
using System.Web;
using System.Web.Configuration;
using CredAbility.Web.Configuration;
using CredAbility.Web.Security;

namespace CredAbility.Web.API.Processors
{
    public class SecureWebPageProcessor 
    {
        public void Process(Sitecore.Pipelines.HttpRequest.HttpRequestArgs args)
        {
            var settings = WebConfigurationManager.GetSection("secureWebPages") as SecureWebPageSettings;
            args.Context.Application["SecureWebPageSettings"] = settings;

            // Evaluate the response against the settings.
            var secure = RequestEvaluator.Evaluate(args.Context.Request, settings, false);

            // Take appropriate action.
            switch (secure)
            {
                case SecurityType.Secure:
                    SslHelper.RequestSecurePage(settings);
                    break;
                case SecurityType.Insecure:
                    SslHelper.RequestUnsecurePage(settings);
                    break;
            }
        }
    }
}