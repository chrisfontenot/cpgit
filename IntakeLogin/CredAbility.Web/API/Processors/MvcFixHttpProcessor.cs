﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace CredAbility.Web.API.Processors
{
    public class MvcFixHttpProcessor : Sitecore.Pipelines.HttpRequest.HttpRequestProcessor
    {
        public override void Process(Sitecore.Pipelines.HttpRequest.HttpRequestArgs args)
        {
            //when using a path such as /Controller.aspx/Blahblahblah, Sitecore's parsing of FilePath can break if Blahblahblah is too long
            RouteData routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(args.Context));
            if (routeData != null)
            {
                args.Url.FilePath = args.Context.Request.Url.LocalPath;
            }
        }
    }

    
}
