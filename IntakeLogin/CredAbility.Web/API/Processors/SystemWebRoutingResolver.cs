﻿using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Sitecore;
using Sitecore.Pipelines.RenderLayout;

namespace CredAbility.Web.API.Processors
{
    public class SystemWebRoutingResolver : Sitecore.Pipelines.HttpRequest.HttpRequestProcessor
    {
        public override void Process(Sitecore.Pipelines.HttpRequest.HttpRequestArgs args)
        {
            var httpContextWrapper = new HttpContextWrapper(args.Context);
            var routeData = RouteTable.Routes.GetRouteData(httpContextWrapper);
            if (routeData != null)
            {
                Sitecore.Pipelines.CorePipeline.Run("renderLayout", new RenderLayoutArgs(new Page(), Context.Item));
                args.AbortPipeline();
            }
        }
    }
}