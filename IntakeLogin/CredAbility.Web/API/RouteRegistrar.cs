﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CredAbility.Web.API
{
    public class RouteRegistrar
    {
        public static void RegisterRoutesTo(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute("Needs Assessment", "our-services/needs-assessment.aspx",
                            new { controller = "NeedsAssessment", action = "Index" }
                            );
            routes.MapRoute("MyGoal", "my-account/my-goals/default.aspx",
                            new { controller = "MyGoal", action = "Index" }
                            );
            routes.MapRoute("MyGoalContent", "goal/{culture}/update.aspx",
                            new { controller = "MyGoal", action = "UpdateContent" }
                            );
            routes.MapRoute("MyGoalAdd", "goal/{culture}/add.aspx",
                            new { controller = "MyGoal", action = "AddGoal" }
                            );
            routes.MapRoute("MyGoalSave", "goal/{culture}/save.aspx",
                            new { controller = "MyGoal", action = "SaveGoal" }
                            );
            routes.MapRoute("MyGoalAddHistory", "goal/{culture}/history/add.aspx/{id}",
                            new { controller = "MyGoal", action = "AddHistory" }
                            );
            routes.MapRoute("MyGoalSaveHistory", "goal/{culture}/history/save.aspx",
                            new { controller = "MyGoal", action = "SaveHistory" }
                            );
            routes.MapRoute("MyGoalDelete", "goal/{culture}/delete.aspx",
                            new { controller = "MyGoal", action = "DeleteGoal" }
                            );
        }
    }
}
