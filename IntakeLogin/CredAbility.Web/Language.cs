﻿using System.Collections.Generic;

namespace CredAbility.Web
{
    public class Language : StaticBase
    {

        public static List<Language> GetAllLanguage()
        {
            var result = new List<Language>(2) {English, Spanish};
            return result;
        }

        public static Language GetLanguage(string code)
        {
            foreach (var language in GetAllLanguage())
            {
                if(language.Code == code)
                {
                    return language;
                }
            }

            return null;
        }

        public static Language English = new LanguageEnum.English();
        public static Language Spanish = new LanguageEnum.Spanish();

        public static class LanguageEnum
        {
            public class English : Language
            {

                public override string Code
                {
                    get { return "EN"; }
                }

                public override string Text
                {
                    get { return Resource.Language_English; }
                }
            }

            public class Spanish : Language
            {

                public override string Code
                {
                    get { return "ES"; }
                }

                public override string Text
                {
                    get { return Resource.Language_Spanish; }
                }
            }
        }

    }
}
