﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Core.CreditScoreChallenge;
using CredAbility.Data.CreditScoreChallenge;
using Sitecore.Security.Accounts;

namespace CredAbility.Web.CreditScoreChallenge
{
    public class CreditScoreChallengeManager
    {

        

        #region Constructors

        protected CreditScoreChallengeManager(){}

        #endregion

        #region Singleton

        private static CreditScoreChallengeManager _current;
        public static CreditScoreChallengeManager Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new CreditScoreChallengeManager();
                }
                return _current;
            }
        }

        #endregion


        public void AuthorizeContestant(Contestant contestant)
        {
            ContestantRepository.Save(contestant);

            //add sitecore role to the current user
            this.AddCreditScoreChallengeRoles();
            this.FlagFirstTime();

        }

        private void FlagFirstTime()
        {
            CreditScoreChallengeContext.Current.IsFirstTime = true;
        }

        private void AddCreditScoreChallengeRoles()
        {
            if(!Sitecore.Context.User.IsInRole("extranet\\CreditScoreChallenge"))
            {
                Sitecore.Context.User.Roles.Add(Role.FromName("extranet\\CreditScoreChallenge"));
            }
        }
    }


    public class CreditScoreChallengeContext
    {
        #region Constants

        private const string CURRENT_SESSIONKEY = "CreditScoreChallengeContext";

        #endregion
        private static CreditScoreChallengeContext _current;
        protected CreditScoreChallengeContext(){}

        public static CreditScoreChallengeContext Current
        {
            get
            {
                if(_current == null)
                {
                    _current = new CreditScoreChallengeContext();
                }
                return _current;
            }
        }

        public bool IsFirstTime
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY];

                if (cookie != null && cookie["IsFirstTime"] != null)
                {
                    return Boolean.Parse(cookie["IsFirstTime"]);
                }

                return false;
            }
            set
            {
                var cookie = HttpContext.Current.Request.Cookies[CURRENT_SESSIONKEY] ?? new HttpCookie(CURRENT_SESSIONKEY);
                cookie["IsFirstTime"] = Convert.ToString(value);
                HttpContext.Current.Response.Cookies.Set(cookie);
            }

        }

    }
}
