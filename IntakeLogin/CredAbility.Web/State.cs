﻿
using System.Collections.Generic;


namespace CredAbility.Web
{
    public class State: StaticBase
    {
        public static List<State> GetAllState()
        {

            var result = new List<State>();

            result.Add(Alabama);
            result.Add(Alaska);
            result.Add(ArmyForcesAmericas);
            result.Add(ArmyForcesOther);
            result.Add(ArmyForcesPacific);
            result.Add(AmericanSamoa);
            result.Add(Arizona);
            result.Add(Arkansas);
            result.Add(California);
            result.Add(Colorado);
            result.Add(Connecticut);
            result.Add(Delaware);
            result.Add(DistrictOfColumbia);
            result.Add(FederatedStatesMicronesia);
            result.Add(Florida);
            result.Add(Georgia);
            result.Add(Guam);
            result.Add(Hawaii);
            result.Add(Idaho);
            result.Add(Illinois);
            result.Add(Indiana);
            result.Add(Iowa);
            result.Add(Kansas);
            result.Add(Kentucky);
            result.Add(Louisiana);
            result.Add(Maine);
            result.Add(MarshallIsland);
            result.Add(Maryland);
            result.Add(Massachusetts);
            result.Add(Michigan);
            result.Add(Minnesota);
            result.Add(Mississippi);
            result.Add(Missouri);
            result.Add(Montana);
            result.Add(Nebraska);
            result.Add(Nevada);
            result.Add(NewHampshire);
            result.Add(NewJersey);
            result.Add(NewMexico);
            result.Add(NewYork);
            result.Add(NorthCarolina);
            result.Add(NorthDakota);
            result.Add(NorthernMarianaIslands);
            result.Add(Ohio);
            result.Add(Oklahoma);
            result.Add(Oregon);
            result.Add(Palau);
            result.Add(Pennsylvania);
            result.Add(PuertoRico);
            result.Add(RhodeIsland);
            result.Add(SouthCarolina);
            result.Add(SouthDakota);
            result.Add(Tennessee);
            result.Add(Texas);
            result.Add(Utah);
            result.Add(Vermont);
            result.Add(VirginIsland);
            result.Add(Virginia);
            result.Add(Washington);
            result.Add(WestVirginia);
            result.Add(Wisconsin);
            result.Add(Wyoming);

            return result;
        }

        public static State GetState(string code)
        {
            foreach (var state in GetAllState())
            {
                if (state.Code == code)
                {
                    return state;
                }
            }

            return null;
        }


        public static State ArmyForcesAmericas = new StateEnum.ArmyForcesAmericas();
        public static State ArmyForcesOther = new StateEnum.ArmyForcesOther();
        public static State ArmyForcesPacific = new StateEnum.ArmyForcesPacific();
        public static State AmericanSamoa = new StateEnum.AmericanSamoa();
        public static State FederatedStatesMicronesia = new StateEnum.FederatedStatesMicronesia();
        public static State Guam = new StateEnum.Guam();
        public static State MarshallIsland = new StateEnum.MarshallIsland();
        public static State NorthernMarianaIslands = new StateEnum.NorthernMarianaIslands();
        public static State Palau = new StateEnum.Palau();
        public static State PuertoRico = new StateEnum.PuertoRico();
        public static State VirginIsland = new StateEnum.VirginIsland();


        

        public static State Alabama = new StateEnum.Alabama();
        public static State Alaska = new StateEnum.Alaska();
        public static State Arizona = new StateEnum.Arizona();
        public static State Arkansas = new StateEnum.Arkansas();
        public static State California = new StateEnum.California();
        public static State Colorado = new StateEnum.Colorado();
        public static State Connecticut = new StateEnum.Connecticut();
        public static State Delaware = new StateEnum.Delaware();
        public static State DistrictOfColumbia = new StateEnum.DistrictOfColumbia();
        public static State Florida = new StateEnum.Florida();
        public static State Georgia = new StateEnum.Georgia();
        public static State Hawaii = new StateEnum.Hawaii();
        public static State Idaho = new StateEnum.Idaho();
        public static State Illinois = new StateEnum.Illinois();
        public static State Indiana = new StateEnum.Indiana();
        public static State Iowa = new StateEnum.Iowa();
        public static State Kansas = new StateEnum.Kansas();
        public static State Kentucky = new StateEnum.Kentucky();
        public static State Louisiana = new StateEnum.Louisiana();
        public static State Maine = new StateEnum.Maine();
        public static State Maryland = new StateEnum.Maryland();
        public static State Massachusetts = new StateEnum.Massachusetts();
        public static State Michigan = new StateEnum.Michigan();
        public static State Minnesota = new StateEnum.Minnesota();
        public static State Mississippi = new StateEnum.Mississippi();
        public static State Missouri = new StateEnum.Missouri();
        public static State Montana = new StateEnum.Montana();
        public static State Nebraska = new StateEnum.Nebraska();
        public static State Nevada = new StateEnum.Nevada();
        public static State NewHampshire = new StateEnum.NewHampshire();
        public static State NewJersey = new StateEnum.NewJersey();
        public static State NewMexico = new StateEnum.NewMexico();
        public static State NewYork = new StateEnum.NewYork();
        public static State NorthCarolina = new StateEnum.NorthCarolina();
        public static State NorthDakota = new StateEnum.NorthDakota();
        public static State Ohio = new StateEnum.Ohio();
        public static State Oklahoma = new StateEnum.Oklahoma();
        public static State Oregon = new StateEnum.Oregon();
        public static State Pennsylvania = new StateEnum.Pennsylvania();
        public static State RhodeIsland = new StateEnum.RhodeIsland();
        public static State SouthCarolina = new StateEnum.SouthCarolina();
        public static State SouthDakota = new StateEnum.SouthDakota();
        public static State Tennessee = new StateEnum.Tennessee();
        public static State Texas = new StateEnum.Texas();
        public static State Utah = new StateEnum.Utah();
        public static State Vermont = new StateEnum.Vermont();
        public static State Virginia = new StateEnum.Virginia();
        public static State Washington = new StateEnum.Washington();
        public static State WestVirginia = new StateEnum.WestVirginia();
        public static State Wisconsin = new StateEnum.Wisconsin();
        public static State Wyoming = new StateEnum.Wyoming();
        
       
         
         
        public static class StateEnum
        {

            public class ArmyForcesAmericas : State
            {

                public override string Code
                {
                    get { return "AA"; }
                }

                public override string Text
                {
                    get { return Resource.State_AA; }
                }
            }


            public class ArmyForcesOther : State
            {

                public override string Code
                {
                    get { return "AE"; }
                }

                public override string Text
                {
                    get { return Resource.State_AE; }
                }
            }

            public class ArmyForcesPacific : State
            {

                public override string Code
                {
                    get { return "AP"; }
                }

                public override string Text
                {
                    get { return Resource.State_AP; }
                }
            }

            public class AmericanSamoa : State
            {

                public override string Code
                {
                    get { return "AS"; }
                }

                public override string Text
                {
                    get { return Resource.State_AS; }
                }
            }


            public class FederatedStatesMicronesia : State
            {

                public override string Code
                {
                    get { return "FM"; }
                }

                public override string Text
                {
                    get { return Resource.State_FM; }
                }
            }

            public class Guam : State
            {

                public override string Code
                {
                    get { return "GU"; }
                }

                public override string Text
                {
                    get { return Resource.State_GU; }
                }
            }

            public class MarshallIsland : State
            {

                public override string Code
                {
                    get { return "MH"; }
                }

                public override string Text
                {
                    get { return Resource.State_MH; }
                }
            }

            public class NorthernMarianaIslands : State
            {

                public override string Code
                {
                    get { return "MP"; }
                }

                public override string Text
                {
                    get { return Resource.State_MP; }
                }
            }

            public class Palau : State
            {

                public override string Code
                {
                    get { return "PW"; }
                }

                public override string Text
                {
                    get { return Resource.State_PW; }
                }
            }

            public class PuertoRico : State
            {

                public override string Code
                {
                    get { return "PR"; }
                }

                public override string Text
                {
                    get { return Resource.State_PR; }
                }
            }

            public class VirginIsland : State
            {

                public override string Code
                {
                    get { return "VI"; }
                }

                public override string Text
                {
                    get { return Resource.State_VI; }
                }
            }
            //-----


            public class Alabama : State
            {

                public override string Code
                {
                    get { return "AL"; }
                }

                public override string Text
                {
                    get { return Resource.State_AL; }
                }
            }

            public class Alaska : State
            {

                public override string Code
                {
                    get { return "AK"; }
                }

                public override string Text
                {
                    get { return Resource.State_AK; }
                }
            }


            public class Arizona : State
            {

                public override string Code
                {
                    get { return "AZ"; }
                }

                public override string Text
                {
                    get { return Resource.State_AZ; }
                }
            }

            public class Arkansas : State
            {

                public override string Code
                {
                    get { return "AR"; }
                }

                public override string Text
                {
                    get { return Resource.State_AR; }
                }
            }




            public class California : State
            {

                public override string Code
                {
                    get { return "CA"; }
                }

                public override string Text
                {
                    get { return Resource.State_CA; }
                }
            }




            public class Colorado : State
            {

                public override string Code
                {
                    get { return "CO"; }
                }

                public override string Text
                {
                    get { return Resource.State_CO; }
                }
            }

            public class Connecticut : State
            {

                public override string Code
                {
                    get { return "CT"; }
                }

                public override string Text
                {
                    get { return Resource.State_CT; }
                }
            }

            public class Delaware : State
            {

                public override string Code
                {
                    get { return "DE"; }
                }

                public override string Text
                {
                    get { return Resource.State_DE; }
                }
            }

            public class DistrictOfColumbia : State
            {

                public override string Code
                {
                    get { return "DC"; }
                }

                public override string Text
                {
                    get { return Resource.State_DC; }
                }
            }

           public class Florida : State
            {

                public override string Code
                {
                    get { return "FL"; }
                }

                public override string Text
                {
                    get { return Resource.State_FL; }
                }
            }

            public class Georgia : State
            {

                public override string Code
                {
                    get { return "GA"; }
                }

                public override string Text
                {
                    get { return Resource.State_GA; }
                }
            }



            public class Hawaii : State
            {

                public override string Code
                {
                    get { return "HI"; }
                }

                public override string Text
                {
                    get { return Resource.State_HI; }
                }
            }


            public class Idaho : State
            {

                public override string Code
                {
                    get { return "ID"; }
                }

                public override string Text
                {
                    get { return Resource.State_ID; }
                }
            }

            public class Illinois : State
            {

                public override string Code
                {
                    get { return "IL"; }
                }

                public override string Text
                {
                    get { return Resource.State_IL; }
                }
            }

            public class Indiana : State
            {

                public override string Code
                {
                    get { return "IN"; }
                }

                public override string Text
                {
                    get { return Resource.State_IN; }
                }
            }

            public class Iowa : State
            {

                public override string Code
                {
                    get { return "IA"; }
                }

                public override string Text
                {
                    get { return Resource.State_IA; }
                }
            }

            public class Kansas : State
            {

                public override string Code
                {
                    get { return "KS"; }
                }

                public override string Text
                {
                    get { return Resource.State_KS; }
                }
            }

            public class Kentucky : State
            {

                public override string Code
                {
                    get { return "KY"; }
                }

                public override string Text
                {
                    get { return Resource.State_KY; }
                }
            }

            public class Louisiana : State
            {

                public override string Code
                {
                    get { return "LA"; }
                }

                public override string Text
                {
                    get { return Resource.State_LA; }
                }
            }

            public class Maine : State
            {

                public override string Code
                {
                    get { return "ME"; }
                }

                public override string Text
                {
                    get { return Resource.State_ME; }
                }
            }

           
            public class Maryland : State
            {

                public override string Code
                {
                    get { return "MD"; }
                }

                public override string Text
                {
                    get { return Resource.State_MD; }
                }
            }

            public class Massachusetts : State
            {

                public override string Code
                {
                    get { return "MA"; }
                }

                public override string Text
                {
                    get { return Resource.State_MA; }
                }
            }

            public class Michigan : State
            {

                public override string Code
                {
                    get { return "MI"; }
                }

                public override string Text
                {
                    get { return Resource.State_MI; }
                }
            }

            public class Minnesota : State
            {

                public override string Code
                {
                    get { return "MN"; }
                }

                public override string Text
                {
                    get { return Resource.State_MN; }
                }
            }

            public class Mississippi : State
            {

                public override string Code
                {
                    get { return "MS"; }
                }

                public override string Text
                {
                    get { return Resource.State_MS; }
                }
            }

            public class Missouri : State
            {

                public override string Code
                {
                    get { return "MO"; }
                }

                public override string Text
                {
                    get { return Resource.State_MO; }
                }
            }

            public class Montana : State
            {

                public override string Code
                {
                    get { return "MT"; }
                }

                public override string Text
                {
                    get { return Resource.State_MT; }
                }
            }

            public class Nebraska : State
            {

                public override string Code
                {
                    get { return "NE"; }
                }

                public override string Text
                {
                    get { return Resource.State_NE; }
                }
            }

            public class Nevada : State
            {

                public override string Code
                {
                    get { return "NV"; }
                }

                public override string Text
                {
                    get { return Resource.State_NV; }
                }
            }

            public class NewHampshire : State
            {

                public override string Code
                {
                    get { return "NH"; }
                }

                public override string Text
                {
                    get { return Resource.State_NH; }
                }
            }

            public class NewJersey : State
            {

                public override string Code
                {
                    get { return "NJ"; }
                }

                public override string Text
                {
                    get { return Resource.State_NJ; }
                }
            }

            public class NewMexico : State
            {

                public override string Code
                {
                    get { return "NM"; }
                }

                public override string Text
                {
                    get { return Resource.State_NM; }
                }
            }

            public class NewYork : State
            {

                public override string Code
                {
                    get { return "NY"; }
                }

                public override string Text
                {
                    get { return Resource.State_NY; }
                }
            }

            public class NorthCarolina : State
            {

                public override string Code
                {
                    get { return "NC"; }
                }

                public override string Text
                {
                    get { return Resource.State_NC; }
                }
            }

            public class NorthDakota : State
            {

                public override string Code
                {
                    get { return "ND"; }
                }

                public override string Text
                {
                    get { return Resource.State_ND; }
                }
            }

        

            public class Ohio : State
            {

                public override string Code
                {
                    get { return "OH"; }
                }

                public override string Text
                {
                    get { return Resource.State_OH; }
                }
            }

            public class Oklahoma : State
            {

                public override string Code
                {
                    get { return "OK"; }
                }

                public override string Text
                {
                    get { return Resource.State_OK; }
                }
            }

            public class Oregon : State
            {

                public override string Code
                {
                    get { return "OR"; }
                }

                public override string Text
                {
                    get { return Resource.State_OR; }
                }
            }

            public class Pennsylvania : State
            {

                public override string Code
                {
                    get { return "PA"; }
                }

                public override string Text
                {
                    get { return Resource.State_PA; }
                }
            }


            public class RhodeIsland : State
            {

                public override string Code
                {
                    get { return "RI"; }
                }

                public override string Text
                {
                    get { return Resource.State_RI; }
                }
            }

            public class SouthCarolina : State
            {

                public override string Code
                {
                    get { return "SC"; }
                }

                public override string Text
                {
                    get { return Resource.State_SC; }
                }
            }

            public class SouthDakota : State
            {

                public override string Code
                {
                    get { return "SD"; }
                }

                public override string Text
                {
                    get { return Resource.State_SD; }
                }
            }

            public class Tennessee : State
            {

                public override string Code
                {
                    get { return "TN"; }
                }

                public override string Text
                {
                    get { return Resource.State_TN; }
                }
            }

            public class Texas : State
            {

                public override string Code
                {
                    get { return "TX"; }
                }

                public override string Text
                {
                    get { return Resource.State_TX; }
                }
            }

            public class Utah : State
            {

                public override string Code
                {
                    get { return "UT"; }
                }

                public override string Text
                {
                    get { return Resource.State_UT; }
                }
            }

            public class Vermont : State
            {

                public override string Code
                {
                    get { return "VT"; }
                }

                public override string Text
                {
                    get { return Resource.State_VT; }
                }
            }

       

            public class Virginia : State
            {

                public override string Code
                {
                    get { return "VA"; }
                }

                public override string Text
                {
                    get { return Resource.State_VA; }
                }
            }

            public class Washington : State
            {

                public override string Code
                {
                    get { return "WA"; }
                }

                public override string Text
                {
                    get { return Resource.State_WA; }
                }
            }

            public class WestVirginia : State
            {

                public override string Code
                {
                    get { return "WV"; }
                }

                public override string Text
                {
                    get { return Resource.State_WV; }
                }
            }

            public class Wisconsin : State
            {

                public override string Code
                {
                    get { return "WI"; }
                }

                public override string Text
                {
                    get { return Resource.State_WI; }
                }
            }

            public class Wyoming : State
            {

                public override string Code
                {
                    get { return "WY"; }
                }

                public override string Text
                {
                    get { return Resource.State_WY; }
                }
            }


        }
    }
}
