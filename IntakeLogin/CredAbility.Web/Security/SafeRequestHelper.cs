﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Security.Application;

namespace CredAbility.Web.Security
{
    public static class SafeRequestHelper
    {
        public static string SanitizeHtml(string candidate)
        {
            return AntiXss.HtmlEncode(candidate);
        }
    }
}
