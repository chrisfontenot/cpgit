using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.Identity;

namespace CredAbility.Web.Identity
{
    public class IdentityManager
    {
        public ActionResult IdentifyOfflineProfile(OfflineProfileIdentity offlineProfileIdentity)
        {
            OfflineProfileIdentityValidator validator = new OfflineProfileIdentityValidator();
            ActionResult result = validator.Validate(offlineProfileIdentity);

            if (result.IsSuccessful)
            {
                ActionResult<Profile> resultProfile = IdentityProxy.GetDmpProfile(offlineProfileIdentity);

                if (resultProfile.IsSuccessful)
                {
                    ActionResult<bool> resultIsExistingProfile = IdentityProxy.IsExistingProfileByHostId(resultProfile.Value.HostId);

                    if (resultIsExistingProfile.IsSuccessful)
                    {
                        if (!resultIsExistingProfile.Value)
                        {
                            OfflineProfileContext.SetProfile(resultProfile.Value);
                            Redirection.ToVerifyProfile();
                        }
                        else
                        {
                            result.IsSuccessful = false;
                            result.Messages.Add(new ResultMessage(ResultMessageLevelType.Error,IdentityErrorMessageResources.IdentityManager_AccountAlreadyExist,"WS-ACCOUNTALREADYEXIST"));
                        }
                    }
                    else
                    {
                        result.MergeActionResult(resultIsExistingProfile);
                    }
                }
                else
                {
                    result.MergeActionResult(resultProfile);
                }
            }

            return result;
        }
    }
}