using CredAbility.Core.Common;
using CredAbility.Core.Identity;
using CredAbility.Data.Validator;

namespace CredAbility.Web.Identity
{   
    public class OfflineProfileIdentityValidator : Validator<OfflineProfileIdentity>
    {
        #region Overrides of Validator<OfflineProfileIdentity>

        public override ActionResult Validate(OfflineProfileIdentity obj)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.ClientId, "CLIENTID-REQUIRED", Resource.OfflineProfile_ClientIdRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(obj.SocialSecurityNumber, "SSN-REQUIRED", Resource.OfflineProfile_SsnRequired));
            actionResult.MergeActionResult(this.ValidateSsnFormat(obj.SocialSecurityNumber, "SSN-WRONGFORMAT", Resource.OfflineProfile_SsnWrongFormat));

            return actionResult;
        }

        #endregion
    }
}