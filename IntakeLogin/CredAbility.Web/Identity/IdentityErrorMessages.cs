using System;
using CredAbility.Core.Common;
using CredAbility.Data.Services.Identity;

namespace CredAbility.Web.Identity
{
    public static class IdentityErrorMessages
    {
        public static ResultMessageList GetMessages(UserRegisterResult urr)
        {
            ResultMessageList l = new ResultMessageList();

            if (urr.IsDuplicateEmail.HasValue && urr.IsDuplicateEmail.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_DuplicateEmailAddress, "WS-UserRegister.DuplicateEmail"));
            }

            if (urr.IsDuplicateUsername.HasValue && urr.IsDuplicateUsername.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_DuplicateUsername, "WS-UserRegister.DuplicateUsername"));
            }

            if (urr.IsFirstNameMissing.HasValue && urr.IsFirstNameMissing.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_FirstNameRequired, "WS-UserRegister.FirstNameMissing"));
            }

            if (urr.IsLastNameMissing.HasValue && urr.IsLastNameMissing.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_LastNameRequired, "WS-UserRegister.LastNameMissing"));
            }

            if (urr.IsUsernameMissing.HasValue && urr.IsUsernameMissing.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_UsernameRequired, "WS-UserRegister.UsernameMissing"));
            }

            if (urr.IsInvalidUsername.HasValue && urr.IsInvalidUsername.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidUsername, "WS-UserRegister.UsernameInvalid"));
            }

            if (urr.IsInvalidOldPassword.HasValue && urr.IsInvalidOldPassword.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidPassword, "WS-UserRegister.InvalidOldPassword"));
            }

            if (urr.IsInvalidNewPasswordConfirm.HasValue && urr.IsInvalidNewPasswordConfirm.Value)
            {
                l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidNewPasswordConfirm, "WS-UserRegister.InvalidNewPasswordConfirm"));
            }

            if (urr.NewPasswordErrors != null)
            {
                l.AddRange(GetMessages(urr.NewPasswordErrors));
            }

            return l;
        }

        public static ResultMessageList GetMessages(UserProfileSaveResult result)
        {
            ResultMessageList l = new ResultMessageList();

            // 1. since UserProfileSaveResult inherits from UserSaveResult, go get any errors from the UserSaveResult aspect of this object
            // 2. this will also collect the NewPasswordErrors
            l.AddRange(GetMessages((UserSaveResult)result));

            // AccountSaveResult
            l.AddRange(GetMessages(result.AccountSaveResults));

            // AddressSaveResult
            l.AddRange(GetMessages(result.AddressSaveResults));

            // UserDetailSaveResultPrimary
            l.AddRange(GetMessages(result.UserDetailSaveResultPrimary));

            // UserDetailSaveResultSecondary
            l.AddRange(GetMessages(result.UserDetailSaveResultSecondary));

            // UserDetailSaveResultSecondary
            l.AddRange(GetMessages(result.UserWebsiteSaveResults));

            return l;
        }

        private static ResultMessageList GetMessages(NewPasswordErrors results)
        {
            ResultMessageList l = new ResultMessageList();

            if (results != null)
            {
                if (results.ContainsFirstName.HasValue && results.ContainsFirstName.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsFirstName, "WS-PasswordChange_ContainsFirstName"));
                }

                if (results.ContainsInvalidChar.HasValue && results.ContainsInvalidChar.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsInvalidChar, "WS-PasswordChange_ContainsInvalidChar"));
                }

                if (results.ContainsLastName.HasValue && results.ContainsLastName.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsLastName, "WS-PasswordChange_ContainsLastName"));
                }

                if (results.ContainsNoLetter.HasValue && results.ContainsNoLetter.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsNoLetter, "WS-PasswordChange_ContainsNoLetter"));
                }

                if (results.ContainsNoNumber.HasValue && results.ContainsNoNumber.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsNoNumber, "WS-PasswordChange_ContainsNoNumber"));
                }

                if (results.ContainsPassword.HasValue && results.ContainsPassword.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsPassword, "WS-PasswordChange_ContainsPassword"));
                }

                if (results.ContainsSequence.HasValue && results.ContainsSequence.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsSequence, "WS-PasswordChange_ContainsSequence"));
                }

                if (results.ContainsUsername.HasValue && results.ContainsUsername.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_ContainsUsername, "WS-PasswordChange_ContainsUsername"));
                }

                if (results.IsTooLong.HasValue && results.IsTooLong.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_IsTooLong, "WS-PasswordChange_IsTooLong"));
                }

                if (results.IsTooShort.HasValue && results.IsTooShort.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_IsTooShort, "WS-PasswordChange_IsTooShort"));
                }
            }

            return l;
        }

        private static ResultMessageList GetMessages(AccountSaveResult[] results)
        {
            ResultMessageList l = new ResultMessageList();
            if (results != null)
            {
                foreach (AccountSaveResult asr in results)
                {
                    if (!asr.IsSuccessful)
                    {
                        if (asr.IsCompleteWithoutCompletedDate.HasValue && asr.IsCompleteWithoutCompletedDate.Value)
                        {
                            l.Add(new ResultMessage(IdentityErrorMessageResources.Account_IsCompleteWithoutCompletedDate, "WS-Account_IsCompleteWithoutCompletedDate"));
                        }

                        if (asr.IsIncompleteWithCompletedDate.HasValue && asr.IsIncompleteWithCompletedDate.Value)
                        {
                            l.Add(new ResultMessage(IdentityErrorMessageResources.Account_IsIncompleteWithCompletedDate, "WS-Account_IsIncompleteWithCompletedDate"));
                        }
                    }
                }
            }
            return l;
        }

        private static ResultMessageList GetMessages(UserSaveResult res)
        {
            ResultMessageList l = new ResultMessageList();

            if (res != null)
            {
                if (res.IsDuplicateUsername.HasValue && res.IsDuplicateUsername.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_DuplicateUsername, "WS-UserRegister.DuplicateUsername"));
                }

                if (res.IsUsernameMissing.HasValue && res.IsUsernameMissing.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_UsernameRequired, "WS-UserRegister.UsernameMissing"));
                }

                if (res.IsInvalidUsername.HasValue && res.IsInvalidUsername.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidUsername, "WS-UserRegister.UsernameInvalid"));
                }

                if (res.IsInvalidOldPassword.HasValue && res.IsInvalidOldPassword.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidPassword, "WS-UserRegister.InvalidOldPassword"));
                }

                if (res.IsInvalidNewPasswordConfirm.HasValue && res.IsInvalidNewPasswordConfirm.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_InvalidNewPasswordConfirm, "WS-UserRegister.InvalidNewPasswordConfirm"));
                }

                if (res.IsInvalidUserId.HasValue && res.IsInvalidUserId.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.IdentityRepository_IsInvalidUserId, "WS-IdentityRepository_IsInvalidUserId"));
                }

                if (res.IsUnchangedNewPassword.HasValue && res.IsUnchangedNewPassword.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserPasswordReset_UnchangedNewPassword, "WS-UserPasswordReset_UnchangedNewPassword"));
                }

                if (res.IsUserLocked.HasValue && res.IsUserLocked.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.IdentityRepository_UserLocked, "WS-IdentityRepository_UserLocked"));
                }

                if (res.NewPasswordErrors != null)
                {
                    l.AddRange(GetMessages(res.NewPasswordErrors));
                }
            }
            return l;
        }

        private static ResultMessageList GetMessages(AddressSaveResult[] results)
        {
            ResultMessageList l = new ResultMessageList();
            if (results != null)
            {
                foreach (AddressSaveResult asr in results)
                {
                    if (!asr.IsSuccessful)
                    {
                        if (asr.IsInvalidZipCode.HasValue && asr.IsInvalidZipCode.Value)
                        {
                            l.Add(new ResultMessage(IdentityErrorMessageResources.SaveProfile_IsInvalidZipCode, "WS-SaveProfile_IsInvalidZipCode"));
                        }

                        if (!string.IsNullOrEmpty(asr.StateExpectedForZip))
                        {
                            l.Add(new ResultMessage(string.Format(IdentityErrorMessageResources.SaveProfile_StateExpectedForZip, asr.StateExpectedForZip), "WS-SaveProfile_StateExpectedForZip"));
                        }
                    }
                }
            }
            return l;
        }

        private static ResultMessageList GetMessages(UserDetailSaveResult res)
        {
            ResultMessageList l = new ResultMessageList();

            if (res != null)
            {
                if (res.IsDuplicateEmail.HasValue && res.IsDuplicateEmail.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.SaveProfile_IsDuplicateEmail, "WS-SaveProfile_IsDuplicateEmail"));
                }

                if (res.IsDuplicateSsn.HasValue && res.IsDuplicateSsn.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.SaveProfile_IsDuplicateSsn, "WS-SaveProfile_IsDuplicateSsn"));
                }

                if (res.IsFirstNameMissing.HasValue && res.IsFirstNameMissing.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_FirstNameRequired, "WS-UserRegister_FirstNameRequired"));
                }

                if (res.IsLastNameMissing.HasValue && res.IsLastNameMissing.Value)
                {
                    l.Add(new ResultMessage(IdentityErrorMessageResources.UserRegister_LastNameRequired, "WS-UserRegister_LastNameRequired"));
                }
            }

            return l;
        }

        public static ResultMessageList GetMessages(UserWebsiteSaveResult[] results)
        {
            ResultMessageList l = new ResultMessageList();
            if (results != null)
            {
                foreach (UserWebsiteSaveResult r in results)
                {
                    if (!r.IsSuccessful)
                    {
                        if (r.IsCompleteWithoutCompletedDate.HasValue && r.IsCompleteWithoutCompletedDate.Value)
                        {
                            l.Add(new ResultMessage(IdentityErrorMessageResources.Website_IsCompleteWithoutCompletedDate, "WS-Website_IsCompleteWithoutCompletedDate"));
                        }

                        if (r.IsIncompleteWithCompletedDate.HasValue && r.IsIncompleteWithCompletedDate.Value)
                        {
                            l.Add(new ResultMessage(IdentityErrorMessageResources.Website_IsIncompleteWithCompletedDate, "WS-Website_IsIncompleteWithCompletedDate"));
                        }
                    }
                }
            }
            return l;
        }
    }
}