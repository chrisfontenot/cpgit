using System;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.Identity;
using CredAbility.Data.DMP;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.DMP;
using CredAbility.Web.Identity;

namespace CredAbility.Web.Identity
{
    public class IdentityProxy
    {
        public static ActionResult<Profile> GetDmpProfile(OfflineProfileIdentity args)
        {
            ActionResult<Profile> result = new ActionResult<Profile>();

            CustomResponse<ResultUserInfo> response = DMPRepository.GetUserInfo(args.ClientId, args.SocialSecurityNumber);

            if(!response.HasError)
            {
                result.Value = response.ResponseValue.ToProfile();
                result.IsSuccessful = true;
            }
            else
            {
                result.Messages.Add(new ResultMessage(response.ErrorMessage, "WS-" + response.ErrorCode));
                result.IsSuccessful = false;   
            }

            return result;
        }

        public static ActionResult<bool> IsExistingProfileByHostId(string hostId)
        {
            ActionResult<bool> result = new ActionResult<bool>();
            try
            {
                result.IsSuccessful = true;
                result.Value = IdentityRepository.IsExistingProfileByHostId(hostId);
            }
            catch(IdentityClientException exception)
            {
                result.IsSuccessful = false;
                result.Value = false;

                result.Messages.Add(new ResultMessage(ResultMessageLevelType.Error,"An error occurred with the web server, please try again.","WS-IdentityClientException"));

                Sitecore.Diagnostics.Log.Error(exception.Message, exception, typeof(IdentityRepository));
            }

            return result;
        }
    }
}