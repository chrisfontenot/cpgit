using System;
using System.Web;
using CredAbility.Core.Client;

namespace CredAbility.Web.Identity
{
    public static class OfflineProfileContext
    {

        #region Offline Profile

        private const string SESSION_KEY = "OfflineProfileAction-Profile";

        public static void Remove()
        {
            HttpContext.Current.Session.Remove(SESSION_KEY);
            HttpContext.Current.Session.Remove(SESSION_KEY_ACCOUNT_CREATED);
        }

        public static Profile GetProfile()
        {
            if (HttpContext.Current.Session[SESSION_KEY] != null)
            {
                return (Profile)HttpContext.Current.Session[SESSION_KEY];
            }

            return null;
        }

        public static void SetProfile(Profile profile)
        {
            HttpContext.Current.Session.Add(SESSION_KEY, profile);
        }


        #endregion

        #region Account Creation Process

        private const string SESSION_KEY_ACCOUNT_CREATED = "OfflineProfileAction-AccountCreated";

        public static void AccountCreated()
        {
            IsAccountCreated = true;
        }

        public static bool IsAccountCreated
        {
            get
            {
                if (HttpContext.Current.Session[SESSION_KEY_ACCOUNT_CREATED] != null)
                {
                    return (bool)HttpContext.Current.Session[SESSION_KEY_ACCOUNT_CREATED];
                }

                return false;
            }

            private set
            {
                HttpContext.Current.Session.Add(SESSION_KEY_ACCOUNT_CREATED, value);
            }
        }

        #endregion

    }
}