﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Web.Cache
{
    public static class CurrentCache
    {
        /// <summary>
        /// Adds the specified cache object.
        /// </summary>
        /// <param name="cacheObject">The cache object.</param>
        /// <param name="keyName">Name of the key.</param>
        internal static void Add(object cacheObject, string keyName)
        {
            System.Web.HttpContext.Current.Cache.Insert(keyName, cacheObject);
        }

        /// <summary>
        /// Check if object exists in cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static bool Exists(string key)
        {
            return System.Web.HttpContext.Current.Cache[key] != null;
        }

        /// <summary>
        /// remove object from cache
        /// </summary>
        /// <param name="key"></param>
        internal static void Remove(string key)
        {
            System.Web.HttpContext.Current.Cache.Remove(key);
        }

        /// <summary>
        /// get object from cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static object Get(string key)
        {
            return System.Web.HttpContext.Current.Cache[key];
        }
    }
}
