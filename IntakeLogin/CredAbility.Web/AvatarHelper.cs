﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace CredAbility.Web
{
    public static class AvatarHelper
    {
        private const string AVATAR_PATH = "/static/images/avatar/";

        public static string GetThumbnailFilePath(string avatarKey)
        {
            return string.Format("{0}{1}_thumb.gif",AVATAR_PATH, avatarKey);
        }

        public static string GetHeadlineFilePath(string avatarKey)
        {
            return string.Format("{0}{1}_header.jpg", AVATAR_PATH, avatarKey);
        }

        public static List<string> GetAvailableAvatarKeys()
        {
            var result = new List<string>();
            string mappedAvatarPath = HttpContext.Current.Server.MapPath(AVATAR_PATH);
            string[] avatarFiles = Directory.GetFiles(mappedAvatarPath, "*_thumb.gif");
            
            foreach (string avatarFile in avatarFiles)
            {

                string filename = avatarFile.Replace(mappedAvatarPath, "");
                filename = filename.Replace("_thumb.gif", "");
                result.Add(filename);
            }
            return result;
        }
    }
}
