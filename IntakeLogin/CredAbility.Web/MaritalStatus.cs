﻿
using System.Collections.Generic;

namespace CredAbility.Web
{
    public class MaritalStatus: StaticBase
    {
        public static List<MaritalStatus> GetAllMaritalStatus()
        {
            var result = new List<MaritalStatus>();


            result.Add(Divorced);
            result.Add(Married);
            result.Add(Single);
            result.Add(Separated);
            result.Add(Widowed);
            result.Add(Other);
            return result;
        }

        public static MaritalStatus GetMaritalStatus(string code)
        {
            foreach (var maritalStatus in GetAllMaritalStatus())
            {
                if (maritalStatus.Code == code)
                {
                    return maritalStatus;
                }
            }

            return null;
        }

        public static MaritalStatus Divorced = new MaritalStatusEnum.Divorced();
        public static MaritalStatus Married = new MaritalStatusEnum.Married();
        public static MaritalStatus Single = new MaritalStatusEnum.Single();
        public static MaritalStatus Separated = new MaritalStatusEnum.Separated();
        public static MaritalStatus Widowed = new MaritalStatusEnum.Widowed();
        public static MaritalStatus Other = new MaritalStatusEnum.Other();
       

        public static class MaritalStatusEnum
        {
            public class Divorced : MaritalStatus
            {

                public override string Code
                {
                    get { return "D"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Divorced; }
                }
            }

            public class Married : MaritalStatus
            {

                public override string Code
                {
                    get { return "M"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Married; }
                }
            }

            public class Single : MaritalStatus
            {

                public override string Code
                {
                    get { return "S"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Single; }
                }
            }

            public class Widowed : MaritalStatus
            {

                public override string Code
                {
                    get { return "W"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Widowed; }
                }
            }

            public class Separated : MaritalStatus
            {

                public override string Code
                {
                    get { return "X"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Separated; }
                }
            }

            public class Other : MaritalStatus
            {

                public override string Code
                {
                    get { return "Z"; }
                }

                public override string Text
                {
                    get { return Resource.MaritalStatus_Other; }
                }
            }

           
        }
    }
}
