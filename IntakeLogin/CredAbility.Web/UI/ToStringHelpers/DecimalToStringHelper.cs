﻿using System;
using System.Configuration;
using System.Globalization;
using CredAbility.Web.Configuration;

namespace CredAbility.Web.UI.ToStringHelpers
{
	public static class DecimalToStringHelper
	{
		public static string ConvertToCurrency(this decimal currency)
		{
			NumberFormatInfo nfi = new NumberFormatInfo();
			nfi.CurrencyNegativePattern = 1;
			nfi.CurrencySymbol = "&#36;";
			return currency.ToString("C", nfi);
		}
	}
}