﻿using System.Reflection;
using System.Resources;

namespace CredAbility.Web.UI.XSLT
{
    public class XSLTHelper : Sitecore.Xml.Xsl.XslHelper
    {
        public string GetResources(string resourceCode)
        {
            ResourceManager resourceManager = new ResourceManager("CredAbility.Web.Resource", Assembly.GetExecutingAssembly());
            return resourceManager.GetString(resourceCode);
        }

    }
}
