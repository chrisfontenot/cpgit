﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CredAbility.Web.UI.WebControlsHelpers
{
    public static class WebControlHelpers
    {
        public static string RenderControl(this WebControl control)
        {
            var sb = new StringBuilder();
            using (var tw = new StringWriter(sb))
            {
                using (var w = new HtmlTextWriter(tw))
                {
                    control.RenderControl(w);
                }
            }
            return sb.ToString(); 
        }

        public static string RenderControl(this Control control)
        {
            var sb = new StringBuilder();
            using (var tw = new StringWriter(sb))
            {
                using (var w = new HtmlTextWriter(tw))
                {
                    control.RenderControl(w);
                }
            }
            return sb.ToString();
        }

    }
}
