﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.SitecoreLib.ItemHelpers;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.UI.WebControlsHelpers
{
    public static class HtmlTextWriterHelpers
    {
        public static void WriteUlList(this HtmlTextWriter writer, IEnumerable<ILinkItem> linkItems)
        {
            writer.RenderBeginTag("ul");
            foreach (var item in linkItems)
            {
                string classAttributeValue = null;
                if (linkItems.FirstOrDefault() == item)
                    classAttributeValue = "first ";
                if (item == Sitecore.Context.Item)
                    classAttributeValue += "current ";
                if (!string.IsNullOrEmpty(classAttributeValue))
                    writer.AddAttribute("class", classAttributeValue);

                writer.RenderBeginTag("li");
                writer.Write(item.HyperLinkHtml());
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }

        public static void WriteUlList(this HtmlTextWriter writer, ILinkItem linkItem)
        {
            writer.RenderBeginTag("ul");
            var linkItems = linkItem.GetNavigationChild();
            foreach (var item in linkItems)
            {
                ILinkItem cli = Sitecore.Context.Item.GetNullableCredAbilityLinkItem();

                string classAttributeValue = null;
                if (linkItems.FirstOrDefault() == item)
                    classAttributeValue = "first ";
                if (item.ID == Sitecore.Context.Item.ID)
                    classAttributeValue += "current ";
                else if (cli != null && item.HisParentOfItem(cli))
                    classAttributeValue += "current-parent ";

                if (!Sitecore.Context.Item.GetCredAbilityLinkItem().ShowInNavigation && Sitecore.Context.Item.ParentID == item.ID)
                    classAttributeValue += "current ";

                
                
                if (!string.IsNullOrEmpty(classAttributeValue))
                    writer.AddAttribute("class", classAttributeValue);
                
                writer.RenderBeginTag("li");
                writer.RenderBeginTag("span");
                writer.Write(item.HyperLinkHtml());
                writer.RenderEndTag();//Close span tag

                if (item.HasNavigationChild())
                    writer.WriteUlList(item);

                writer.RenderEndTag();//Close li tag
            }
            writer.RenderEndTag();//close ul tag
        }

        public static void WriteUlList(this HtmlTextWriter writer, IEnumerable<IImageItem> imageItems)
        {
            writer.RenderBeginTag("ul");
            foreach (var item in imageItems)
            {
                string classAttributeValue = null;
                if (imageItems.FirstOrDefault() == item)
                    classAttributeValue = "first ";
                if (!string.IsNullOrEmpty(classAttributeValue))
                    writer.AddAttribute("class", classAttributeValue);

                writer.RenderBeginTag("li");
                writer.Write(item.ImageHtmlTag());
                writer.RenderEndTag();//Close li tag
            }
            writer.RenderEndTag();//close ul tag
        }

        public static void WriteUlList(this HtmlTextWriter writer, IEnumerable<LanguageItem> languageItems)
        {
            writer.RenderBeginTag("ul");
            foreach (var item in languageItems)
            {
                string classAttributeValue = null;
                if (languageItems.FirstOrDefault() == item)
                    classAttributeValue = "first ";
                if (item.Culture == Sitecore.Context.Item.Language.Name)
                    classAttributeValue += "current ";
                if (!string.IsNullOrEmpty(classAttributeValue))
                    writer.AddAttribute("class", classAttributeValue);

                writer.RenderBeginTag("li");
                writer.Write(item.GetLinkHtml());
                writer.RenderEndTag();
            }
            writer.RenderEndTag();
        }
      
        public static void WriteMenu(this HtmlTextWriter writer, ILinkItem linkItem)
        {
            writer.AddAttribute("class", "yui3-menu yui3-menu-horizontal");
            writer.RenderBeginTag("div");

            writer.AddAttribute("class", "yui3-menu-content");
            writer.RenderBeginTag("div");

            writer.RenderBeginTag("ul");
            var linkItems = linkItem.GetNavigationChild();
            foreach (var item in linkItems)
            {
                string classAttributeValue = null;
                if (linkItems.FirstOrDefault() == item)
                    classAttributeValue = "first ";
                if (linkItems.LastOrDefault() == item)
                    classAttributeValue = "last ";
                if (item.LinkedItem != null && Sitecore.Context.Item.GetCredAbilityLinkItem().GetFirstSectionFolderParent().HisParentOfItem(item.LinkedItem))
                    classAttributeValue += "current ";
                classAttributeValue += " yui3-menuitem";
                if (!string.IsNullOrEmpty(classAttributeValue))
                    writer.AddAttribute("class", classAttributeValue);

                writer.RenderBeginTag("li");

                writer.AddAttribute("class", "yui3-menu-label");
                writer.RenderBeginTag("span");
                writer.Write(item.HyperLinkHtml());
                writer.RenderEndTag();//Close span tag

                if (item.HasNavigationChild())
                    writer.WriteMenu(item);

                writer.RenderEndTag();//Close li tag
            }
            writer.RenderEndTag();//close ul tag
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
    }
}