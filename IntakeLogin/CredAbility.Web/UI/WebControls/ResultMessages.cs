﻿using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Common;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.WebControlsHelpers;

namespace CredAbility.Web.UI.WebControls
{
    public class ResultMessages : WebControl
    {

        public ResultMessageList Messages { get; set; }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Messages == null || Messages.Count <= 0) return;

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "result-messages " + this.CssClass);

            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            

            WriteResultMessages(writer, ResultMessageLevelType.Error);
            WriteResultMessages(writer, ResultMessageLevelType.Warning);
            WriteResultMessages(writer, ResultMessageLevelType.Info);
            
            writer.RenderEndTag();
        }

        private void WriteResultMessages(HtmlTextWriter writer, ResultMessageLevelType levelType)
        {

            var resultMessageList = (this.Messages.GetByLevelType(levelType));

            if (resultMessageList.Count > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, GetCssClass(levelType));
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                foreach (ResultMessage resultMessage in resultMessageList)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.Write(resultMessage.Message);
                    writer.RenderEndTag();

                }
                writer.RenderEndTag();
            }
        }

        private string GetCssClass(ResultMessageLevelType levelType)
        {
            string result = "info";
            if (levelType == ResultMessageLevelType.Error)
            {
                result = "error";
            }
            else if (levelType == ResultMessageLevelType.Warning)
            {
                result = "warning";
            }
          

            return result;
        }

    }
}
