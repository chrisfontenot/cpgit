﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CredAbility.Web.UI.WebControls
{
    [ToolboxData("<{0}:Copyright runat=server></{0}:Copyright>")]
    public class Copyright : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var contextDb = Sitecore.Context.Database;

            var copyright = contextDb.GetItem(CredAbility.Web.Configuration.ConfigReader.ConfigSection.Copyright.SitecorePath);

            if (copyright != null && copyright.TemplateName == "Simple Rich Text")
            {
                writer.Write(copyright.Fields["Content"].Value);
            }
        }

        
    }
}
