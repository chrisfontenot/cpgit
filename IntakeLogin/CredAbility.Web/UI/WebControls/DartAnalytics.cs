﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.SitecoreLib.Item;
using Sitecore.Data.Items;

namespace CredAbility.Web.UI.WebControls
{
    public class DartAnalytics : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (Sitecore.Context.Item.Fields["Floodlight Code"] != null && !string.IsNullOrEmpty(Sitecore.Context.Item.Fields["Floodlight Code"].Value))
            {
                writer.Write(Sitecore.Context.Item.Fields["Floodlight Code"].Value);
            }
        }

        
    }
}
