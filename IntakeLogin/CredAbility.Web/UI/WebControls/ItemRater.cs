﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Data.Rate;


namespace CredAbility.Web.UI.WebControls
{
    public class ItemRater: WebControl
    {
        public string ItemID { get; set; }
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
             Guid itemID = string.IsNullOrEmpty(ItemID) ? Sitecore.Context.Item.ID.Guid : new Guid(ItemID);
            var cookie = this.Page.Request.Cookies["itemrated"];
            var alreadyRated = cookie != null && cookie[itemID.ToString()] != null;
           
            

            writer.AddAttribute(HtmlTextWriterAttribute.Id, "item-rater");
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "item-rater");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            if (!alreadyRated)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Id, "stars");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderEndTag();

                writer.AddStyleAttribute("clear", "both");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.RenderEndTag();
            }
            else
            {
                writer.Write(Resource.Course_AlreadyRated);
            }
            writer.RenderEndTag();

            if (alreadyRated) return;
            var itemRate = ItemRateRepository.Get(itemID) ?? new Core.Rate.ItemRate { ItemID = itemID };

            writer.RenderBeginTag(HtmlTextWriterTag.Script);
            writer.Write("var itemRater = {defaultAverage: " + itemRate.Average + ", itemId: '" + itemRate.ItemID + "'}");
            writer.RenderEndTag();
        }
    }
}
