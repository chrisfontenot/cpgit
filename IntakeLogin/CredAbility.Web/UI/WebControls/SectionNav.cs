﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.SitecoreLib.ItemHelpers;
using CredAbility.Web.UI.WebControlsHelpers;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Web.UI.WebControls
{
    public class SectionNav : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {

            var currentItem = Sitecore.Context.Item.GetCredAbilityLinkItem();

            if (currentItem.GetParent().Name == "Home")
                writer.WriteUlList(currentItem.GetParent());
            else
                writer.WriteUlList(currentItem.GetFirstSectionFolderParent());
        }
    }
}
