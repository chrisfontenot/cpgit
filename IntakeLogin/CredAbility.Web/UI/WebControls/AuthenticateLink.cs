﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using Sitecore.Data.Items;

namespace CredAbility.Web.UI.WebControls
{
    
    public class AuthenticateLink : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var linkText = "";
            var link = "";
            if (!Sitecore.Context.IsLoggedIn)
            {
                linkText = Resource.LoginLinkText;
                link = ConfigReader.ConfigSection.Url.MyActivity.Value;

            }
            else
            {
                linkText = Resource.LogoutLinkText;
                link = ConfigReader.ConfigSection.Url.Logout.Value;
            }


            writer.AddAttribute(HtmlTextWriterAttribute.Href, link);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(linkText);
            writer.RenderEndTag();
        }

        
    }
}
