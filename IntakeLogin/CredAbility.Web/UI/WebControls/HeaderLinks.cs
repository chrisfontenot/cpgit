﻿using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.WebControlsHelpers;

namespace CredAbility.Web.UI.WebControls
{
    public class HeaderLinks : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var contextDb = Sitecore.Context.Database;

            var headerlinks = new WebFolderItem(contextDb.GetItem(ConfigReader.ConfigSection.Header.SitecorePath));

            writer.WriteMenu(headerlinks);
        }

    }
}
