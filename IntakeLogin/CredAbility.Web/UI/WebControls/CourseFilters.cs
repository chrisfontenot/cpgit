﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;

namespace CredAbility.Web.UI.WebControls
{

    public class CourseSortings : WebControl
    {
        #region Constants

        public const string SORT_ALPHABETICAL = "alphabetical";
        public const string SORT_MOST_RECENT = "morerecent";
        public const string SORT_MOST_POPULAR = "mostpopular";
        private const string DEFAULT_SORT = SORT_ALPHABETICAL;

        #endregion

        public static string CurrentSorting
        {
            get
            {
                string value = HttpContext.Current.Request.QueryString["s"];
                return string.IsNullOrEmpty(value) ? DEFAULT_SORT : value;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            RenderLi(writer, Resource.CourseSorting_Alphabetical, GetSortUrl(SORT_ALPHABETICAL), (CurrentSorting == SORT_ALPHABETICAL));
            RenderLi(writer, Resource.CourseSorting_MostRecent, GetSortUrl(SORT_MOST_RECENT), (CurrentSorting == SORT_MOST_RECENT));
            //RenderLi(writer, Resource.CourseSorting_MostPopular, GetSortUrl(SORT_MOST_POPULAR), (CurrentSorting == SORT_MOST_POPULAR));
            writer.RenderEndTag();
        }

        private static string GetSortUrl(string filter)
        {
            var url = new Sitecore.Text.UrlBuilder(HttpContext.Current.Request.RawUrl);
            url.AddQueryString("s=" + filter);
            url.AddQueryString("f=" + CourseFilters.CurrentFilter);
            return url.ToString();
        }

        private static void RenderLi(HtmlTextWriter writer, string text, string url, bool selectedLi)
        {
            if (selectedLi)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "selected");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Li);

            writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(text);
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

    }

    public class CourseFilters : WebControl
    {
        private const string FILTER_ALL = "all";

        public const string FILTER_CLASSONDEMAND = "classondemand";
        public const string FILTER_ARTICLES = "articles";
        public const string FILTER_WEBINARS = "webinars";
        public const string FILTER_PODCASTS = "podcasts";
        public const string FILTER_ONLINEVIDEOS = "onlinevideos";
        public const string FILTER_PAIDOFFERINGS = "paidofferings";
        private const string DEFAULT_FILTER = FILTER_ALL;

        public static string CurrentFilter
        {
            get
            {
                string value = HttpContext.Current.Request.QueryString["f"];
                return string.IsNullOrEmpty(value) ? DEFAULT_FILTER : value;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Ul);

            System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;

            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_Articles", ci), GetFilterUrl(FILTER_ARTICLES), (CurrentFilter == FILTER_ARTICLES));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_ClassOnDemands", ci), GetFilterUrl(FILTER_CLASSONDEMAND), (CurrentFilter == FILTER_CLASSONDEMAND));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_OnlineVideos", ci), GetFilterUrl(FILTER_ONLINEVIDEOS), (CurrentFilter == FILTER_ONLINEVIDEOS));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_PaidOfferings", ci), GetFilterUrl(FILTER_PAIDOFFERINGS), (CurrentFilter == FILTER_PAIDOFFERINGS));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_Podcasts", ci), GetFilterUrl(FILTER_PODCASTS), (CurrentFilter == FILTER_PODCASTS));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_Webinars", ci), GetWebinarsUrl(), (CurrentFilter == FILTER_WEBINARS));
            RenderLi(writer, Resource.ResourceManager.GetString("CourseType_All", ci), GetFilterUrl(FILTER_ALL), (CurrentFilter == FILTER_ALL));

            writer.RenderEndTag();
        }

        private static void RenderLi(HtmlTextWriter writer, string text, string url, bool selectedLi)
        {
            if (selectedLi)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "selected");
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Li);
            writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write(text);
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        private static string GetFilterUrl(string filter)
        {
            var url = new Sitecore.Text.UrlBuilder(HttpContext.Current.Request.RawUrl);
            url.AddQueryString("s=" + CourseSortings.CurrentSorting);
            url.AddQueryString("f=" + filter);
            return url.ToString();
        }

        private static string GetWebinarsUrl()
        {
            switch (System.Threading.Thread.CurrentThread.CurrentCulture.Name)
            {
                case "es-ES":
                    return "/webinarsesp";
                default:
                    return "/webinars";
            }
        }
    }
}
