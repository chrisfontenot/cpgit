﻿using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.WebControlsHelpers;

namespace CredAbility.Web.UI.WebControls
{
    public class LanguageLinks : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var contextDb = Sitecore.Context.Database;

            var languageFolder = contextDb.GetItem(ConfigReader.ConfigSection.Language.SitecorePath);
            var languageList = new List<LanguageItem>();
            foreach (var child in languageFolder.Children.InnerChildren)
            {
                    languageList.Add(new LanguageItem(child));
            }

            writer.WriteUlList(languageList);

        }
    }
}
