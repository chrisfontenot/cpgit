﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Data.Rate;

namespace CredAbility.Web.UI.WebControls
{
    public class ItemRate : WebControl
    {

        public string ItemID { get; set; }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            Guid itemID = string.IsNullOrEmpty(ItemID) ? Sitecore.Context.Item.ID.Guid : new Guid(ItemID);

            var itemRate = ItemRateRepository.Get(itemID) ?? new Core.Rate.ItemRate();

            writer.AddAttribute("class", "item-rate");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.AddAttribute("class", "stars rate-" + itemRate.Average.ToString().Replace(".", ""));

            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(itemRate.Average);
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "rated-by");
            writer.RenderBeginTag(HtmlTextWriterTag.Span);
            writer.Write(string.Format(Resource.RatedByNUsers, "<span id='total-rate'>" + itemRate.TotalRate + "</span>"));
            writer.RenderEndTag();
            writer.RenderEndTag();
        }
    }
}
