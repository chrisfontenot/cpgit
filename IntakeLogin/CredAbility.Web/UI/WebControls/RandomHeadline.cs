﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.SitecoreLib.Item;
using Sitecore.Data.Items;

namespace CredAbility.Web.UI.WebControls
{

    public class RandomHeadline : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (Sitecore.Context.Item.Fields["Headline Folder"] != null && !string.IsNullOrEmpty(Sitecore.Context.Item.Fields["Headline Folder"].Value))
            {
                var mediaFolderItem =
                    Sitecore.Context.Database.GetItem(Sitecore.Context.Item.Fields["Headline Folder"].Value).
                        GetCredAbilityMediaFolderItem();

                var randomImage = mediaFolderItem.GetRandomImage();
                if (randomImage != null)
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundImage, randomImage.Src);
                }
            }

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "headline");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();
        }

       
    }
}
