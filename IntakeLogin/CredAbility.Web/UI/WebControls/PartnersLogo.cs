﻿using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.WebControlsHelpers;

namespace CredAbility.Web.UI.WebControls
{
    public class PartnersLogo : WebControl
    {
        protected override void Render(HtmlTextWriter writer)
        {
            var contextDb = Sitecore.Context.Database;

            var partnerlogofolder = new MediaFolderItem(contextDb.GetItem(ConfigReader.ConfigSection.PartnersLogo.SitecorePath));

            writer.WriteUlList(partnerlogofolder.GetImageItems());
        }


    }
}
