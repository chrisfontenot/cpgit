﻿using System;
using CredAbility.Core.Activity;
using CredAbility.Data.Activity;
using CredAbility.Web.Authentification;
using Sitecore.Links;
using CredAbility.Data.Identity;
using CredAbility.Web.Configuration;
using CredAbility.Data.Services.Identity;
using CredAbility.Data.CredU;

namespace CredAbility.Web.UI.UserControls
{
	public class CourseBaseControl:CredUClass
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			EndClass();
			long? UserID;
			CredAbility.Core.Client.ClientProfile CurProfile = ClientProfileManager.GetClientProfile;
			if(CurProfile == null)
			{
				UserID = null;
			}
			else
			{
				UserID = long.Parse(CurProfile.ClientID);
				UserProfile CurUser = IdentityRepository.GetUserProfile(CurProfile.ClientID).ResponseValue;
				if(CurUser != null)
				{
					if(CurUser.Addresses != null)
					{
						if(Session["ZipCode"] == null)
						{
							Session.Add("ZipCode", CurUser.Addresses[0].Zip);
						}
						else if(Session["ZipCode"].ToString() != CurUser.Addresses[0].Zip)
						{
							Session["ZipCode"] = CurUser.Addresses[0].Zip;
						}
					}
				}
			}

			if(Session["ZipCode"] != null)
			{
				StartClass(Session["ZipCode"].ToString(), UserID);
			}
			else
			{
				Session.Add("CourseName", Sitecore.Context.Item.Name);
				Session.Add("ReturnUrl", String.Format(ConfigReader.ConfigSection.Url.ReturnToClass.Value.ToString(), Sitecore.Context.Item.Name));
				Response.Redirect(ConfigReader.ConfigSection.Url.NeedZip.Value.ToString());
			}

			if(AuthentificationContext.Current.IsLogged)
			{
				LogActivity();
			}
		}

		protected void LogActivity()
		{
			string message = Resource.UserActivity_ReadCourse;
			UserActivityType type = UserActivityType.Read;

			if(Sitecore.Context.Item.TemplateName == "Online Video")
			{
				message = Resource.UserActivity_WatchedCourse;
				type = UserActivityType.Watched;
			}
			else if(Sitecore.Context.Item.TemplateName == "Podcast")
			{
				message = Resource.UserActivity_ListenedCourse;
				type = UserActivityType.Listened;
			}

			var activity = new UserActivity(ClientProfileManager.CurrentClientProfile.ClientID)
			{
				Text = string.Format(message
				, LinkManager.GetItemUrl(Sitecore.Context.Item)
				,Sitecore.Context.Item.Fields["Subhead"].Value)
				,Type = type
			};

			UserActivityRepository.Save(activity);
		}
	}
}
