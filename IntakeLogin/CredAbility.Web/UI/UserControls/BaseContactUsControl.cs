﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using CredAbility.Core.Common;
using CredAbility.Web.API.Email;

namespace CredAbility.Web.UI.UserControls
{
    public class BaseContactUsControl : FormBaseControl
    {
        protected ActionResult SendEmail(string fullName, string senderEmail, Dictionary<string,string> bodyTags)
        {
            var emailTemplate = Sitecore.Context.Item.GetChildren().InnerChildren.FirstOrDefault(i => i.TemplateName == "Email Template");
            var subject = emailTemplate.Fields["Subject"].Value;

            var bodyText = emailTemplate.Fields["Body"].Value;

            foreach (var bodyTag in bodyTags)
            {
                bodyText = bodyText.Replace(bodyTag.Key, bodyTag.Value);
            }

            var toAddresses = new MailAddressCollection();
            var receivers = emailTemplate.Fields["Receiver email"].Value.Split(';');
            foreach (var receiver in receivers)
            {
                toAddresses.Add(new MailAddress(receiver));    
            }

            EmailManager.SendEmail(fullName, senderEmail, subject, bodyText, toAddresses);

            var actionResult = new ActionResult();

            actionResult.IsSuccessful = true;
            actionResult.Messages.Add(new ResultMessage(ResultMessageLevelType.Info, Resource.ContactUsValidator_Congratulations, "EMAIL-SEND"));

            return actionResult;
        }
    }
}
