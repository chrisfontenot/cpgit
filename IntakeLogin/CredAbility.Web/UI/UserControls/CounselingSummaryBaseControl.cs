﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;

namespace CredAbility.Web.UI.UserControls
{
    public class CounselingSummaryBaseControl : MyAccountBaseControl
    {
        
        protected ClientCounselingSummaryInfo Summary { 
            get{
                return this.Manager.GetClientCounselingSummary();
            }
        }

    }
}
