﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.Web;

namespace CredAbility.Web.UI.UserControls
{
	public class MyAccountBaseControl:System.Web.UI.UserControl
	{
		protected ClientProfile ClientProfile { get { return ClientProfileManager.CurrentClientProfile; } }
		private ClientProfileManager _manager;

		protected ClientProfileManager Manager
		{
			get
			{
				if(this._manager == null)
				{
					this._manager = new ClientProfileManager();
				}
				return this._manager;
			}

		}

		public string PastSummariesUrl
		{
			get
			{
				var pastSummaryItem = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.PastSummary.SitecorePath);
				if(pastSummaryItem != null && !string.IsNullOrEmpty(this.ClientProfile.DocumentID))
				{
					return LinkManager.GetItemUrl(pastSummaryItem);
				}
				return null;
			}
		}

		protected virtual void Page_Load(object sender, EventArgs e)
		{
			if(this.ClientProfile.MustChangePassword)
			{
				var mustChangePassword = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangePassword.SitecorePath);
				WebUtil.Redirect(LinkManager.GetItemUrl(mustChangePassword));
			}
			else if(this.ClientProfile.IsProfileUpdateRequired)
			{
				var mustChangeUsername = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangeUsername.SitecorePath);
				WebUtil.Redirect(LinkManager.GetItemUrl(mustChangeUsername));
			}
		}
	}
}