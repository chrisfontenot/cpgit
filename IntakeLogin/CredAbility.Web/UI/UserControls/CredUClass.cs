﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Data.CredU;

namespace CredAbility.Web.UI.UserControls
{
	public class CredUClass:System.Web.UI.UserControl
	{
		protected void StartClass(string ZipCode, long? UserID)
		{
			string RunTime = String.Empty;
			if(Sitecore.Context.Item.Fields["Runtime"] != null)
			{
				RunTime = Sitecore.Context.Item.Fields["Runtime"].Value;
			}
			long ClassTakenID = CredURepository.AddClass(ZipCode, UserID
				, Sitecore.Context.Item.DisplayName, Sitecore.Context.Item.TemplateName
				, Sitecore.Context.Culture.EnglishName, RunTime);
			if(ClassTakenID > 0)
			{
				Session.Add("ClassTakenID", ClassTakenID.ToString());
			}
		}

		public void EndClass()
		{
			if(Session["ClassTakenID"] != null)
			{
				long ClassTakenID;
				if(long.TryParse(Session["ClassTakenID"].ToString(), out ClassTakenID))
				{
					CredURepository.EndClass(ClassTakenID);
				}
				Session.Remove("ClassTakenID");
			}
		}
	}
}