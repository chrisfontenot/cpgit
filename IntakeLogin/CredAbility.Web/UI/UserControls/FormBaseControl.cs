﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Web.UI.UserControls
{
    public class FormBaseControl : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Messages = new ResultMessageList();
        }
        protected ResultMessageList Messages { get; set; }

        public string GetCssClassIfMessageCodeExist(string code, string cssClass )
        {
            return this.Messages.HasMessage(code) ? cssClass : string.Empty;
        }

        public string GetCssClassIfMessageCodeExist(string[] codes, string cssClass)
        {
            foreach (string code in codes)
            {
                if (this.Messages.HasMessage(code))
                {
                    return cssClass;
                }
            }

            return string.Empty;
        }

        public string GetMessage(string code, string defaultMessage)
        {
            string result = GetMessage(code);
            return string.IsNullOrEmpty(result) ? defaultMessage : result;
        }

        public string GetMessage(string code)
        {
            ResultMessage result = this.Messages.GetResultMessage(code);
            return result == null ? string.Empty : result.Message;
        }

        public string GetMessage(string[] codes, string defaultMessage)
        {
            string result = GetMessage(codes);
            return string.IsNullOrEmpty(result) ? defaultMessage : result;
        }

        public string GetMessage(string[] codes)
        {
            ResultMessage result;

            foreach (string code in codes)
            {
                result = this.Messages.GetResultMessage(code);  
                if(result != null)
                {
                    return result.Message;
                }
            }

            return string.Empty;
        }
    }
}
