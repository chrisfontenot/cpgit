﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Service.Master" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageTitle">
 <div class="headline" style="background-image:url(/static/images/layout/primary_error_500.jpg);">
 
</div>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageBody">
    <div class="c-body c-large">
        <h1>
            There is a problem with the page you are trying to reach and it cannot be displayed. 
        </h1>
        
        <li>Click the "<a href="javascript:window.history.back()">Back</a>" button on your browser or go to our home page</li>

    </div>
</asp:Content>
