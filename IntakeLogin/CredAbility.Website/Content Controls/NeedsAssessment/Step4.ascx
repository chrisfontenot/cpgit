﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep4Questions"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>

		<div class="list yui3-skin-sam yui-skin-sam">
		
<% using (Html.BeginForm()){%>
<%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
    <%--//Step 1--%>
    <%= Html.Hidden("MonthlyRevolvingCreditCardDebt", NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebt)%>
    <%= Html.Hidden("HomeMortgage", NeedsAssessmentViewData.HomeMortgage)%>
    <%= Html.Hidden("OtherDebt", NeedsAssessmentViewData.OtherDebt)%>
    <%= Html.Hidden("InterestedInDMP", NeedsAssessmentViewData.InterestedInDMP)%>
    <%= Html.Hidden("HomePurchase", NeedsAssessmentViewData.HomePurchase)%>
    <%= Html.Hidden("InterestedInReverseMortgage", NeedsAssessmentViewData.InterestedInReverseMortgage)%>
    <%= Html.Hidden("BankruptcyFilingProcess", NeedsAssessmentViewData.BankruptcyFilingProcess)%>
    <%--//Step 2--%>
    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment)%>
    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort)%>
    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard)%>
    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit)%>
    <%--//Step 3--%>
    <%--//Step 5--%>
    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment)%>
    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment)%>
    <%= Html.Hidden("HomeForclosureThreat",NeedsAssessmentViewData.HomeForclosureThreat)%>
    <%--//Step 6--%>
    <%= Html.Hidden("CanMakeHomeDownPayment", NeedsAssessmentViewData.CanMakeHomeDownPayment)%>
    <%--//Step 7--%>
    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit)%>
    <%= Html.Hidden("DebtOptions", NeedsAssessmentViewData.DebtOptions)%>
    
    
    
    
                <div class="item">
					<p><%= TotalDebtAmount.GetQuestion() %><span class="slider-label"></span><%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount, new { Class = "slider", max = TotalDebtAmount.Count(), min=0, ReadOnly= "readonly", style="border:0" })%></p>
					<div style="text-align:center">
					    <span id="TotalDebtAmount-slider"></span>
					</div>
					<div class="scale">
					<table style="width:880px; margin: 0px 0px 0px 30px">
					<tr>
						<td style="width:12%; text-align:left"><%= TotalDebtAmount.D0.Text%></td>
						<td style="width:19%; text-align:center"><%= TotalDebtAmount.D1.Text%></td>
						<td style="width:20%; text-align:center"><%= TotalDebtAmount.D2.Text%></td>
						<td style="width:17%; text-align:center"><%= TotalDebtAmount.D3.Text%></td>
						<td style="width:16%; text-align:right"><%= TotalDebtAmount.D4.Text%></td>
						<td style="width:18%; text-align:right"><%= TotalDebtAmount.D5.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
			    <div class="item">
					<p><%= TotalMonthlyPayment.GetQuestion() %><span class="slider-label"></span><%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment, new { Class = "slider", max = TotalMonthlyPayment.Count(), min=0, ReadOnly= "readonly", style="border:0" })%></p>
					<div style="text-align:center">
					    <span id="TotalMonthlyPayment-slider"></span>
					</div>
					<div class="scale">
					<table style="width:880px; margin: 0px 0px 0px 30px">
					<tr>
						<td style="width:25%; text-align:left"><%= TotalMonthlyPayment.D0.Text%></td>
						<td style="width:23%; text-align:left"><%= TotalMonthlyPayment.D1.Text%></td>
						<td style="width:8%; text-align:left"><%= TotalMonthlyPayment.D2.Text%></td>
						<td style="width:20%; text-align:right"><%= TotalMonthlyPayment.D3.Text%></td>
						<td style="width:20%; text-align:right"><%= TotalMonthlyPayment.D4.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
			    <div class="item">
					<p><%= AverageInterest.GetQuestion() %><span class="slider-label"></span><%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest, new { Class = "slider", min = 0, max = AverageInterest.Count(),  ReadOnly = "readonly", style = "border:0;width:25px;text-align:right" })%>%</p>
					<div style="text-align:center">
					    <span id="AverageInterest-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px; margin: 0px 0px 0px 30px">
					<tr>
						<td style="width:17%; text-align:left"><%= AverageInterest.D0.Text%></td>
						<td style="width:14%; text-align:left"><%= AverageInterest.D1.Text%></td>
						<td style="width:15%; text-align:left"><%= AverageInterest.D2.Text%></td>
						<td style="width:15%; text-align:left"><%= AverageInterest.D3.Text%></td>
						<td style="width:15%; text-align:left"><%= AverageInterest.D4.Text%></td>
						<td style="width:16%; text-align:left"><%= AverageInterest.D5.Text%></td>
						<td style="width:16%; text-align:left"><%= AverageInterest.D6.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
    
    <div class="previousNextButtons">
	<span class="submitBackButton">
		<%= Html.SubmitButton("GoToPrevStep", NeedsAssessmentViewData.PreviousButtonText)%>
	</span>
	<span class="submitNextButton">
		<%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%>
	</span>
    </div>
<% } %>
<script>
	var CurrencyConvert = true;
	var totalDebtAmountConfirm = "<%= Resources.Website.NeedAssessment_Step4_TotalDebtConfirm %>";
	var monthlyPaymentConfirm = "<%= Resources.Website.NeedAssessment_Step4_TotaMonthlyPaymentConfirm %>";
</script>

        </div>