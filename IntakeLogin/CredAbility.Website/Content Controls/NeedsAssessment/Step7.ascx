﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep7Questions"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>


		<div class="list yui3-skin-sam yui-skin-sam">

<% using (Html.BeginForm()){%>

<%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
<%--//Step 1--%>
    <%= Html.Hidden("MonthlyRevolvingCreditCardDebt", NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebt)%>
    <%= Html.Hidden("HomeMortgage", NeedsAssessmentViewData.HomeMortgage)%>
    <%= Html.Hidden("OtherDebt", NeedsAssessmentViewData.OtherDebt)%>
    <%= Html.Hidden("InterestedInDMP", NeedsAssessmentViewData.InterestedInDMP)%>
    <%= Html.Hidden("HomePurchase", NeedsAssessmentViewData.HomePurchase)%>
    <%= Html.Hidden("InterestedInReverseMortgage", NeedsAssessmentViewData.InterestedInReverseMortgage)%>
    <%= Html.Hidden("BankruptcyFilingProcess", NeedsAssessmentViewData.BankruptcyFilingProcess)%>
    <%--//Step 2--%>
    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment)%>
    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort)%>
    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard)%>
    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit)%>
    <%--//Step 3--%>
    <%--//Step 4--%>
    <%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount)%>
    <%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment)%>
    <%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest)%>
    <%--//Step 5--%>
    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment)%>
    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment)%>
    <%= Html.Hidden("HomeForclosureThreat",NeedsAssessmentViewData.HomeForclosureThreat)%>
    <%--//Step 6--%>
    <%= Html.Hidden("CanMakeHomeDownPayment", NeedsAssessmentViewData.CanMakeHomeDownPayment)%>
    
    
                <div class="item">
					<p><%= IsUserReadyToCommit.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit, new { Class = "slider", max = IsUserReadyToCommit.Count() })%>
					    <span id="IsUserReadyToCommit-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:25%; text-align:left"><%= IsUserReadyToCommit.ReadyAndWilling.Text%></td>
						<td style="width:15%; text-align:left"><%= IsUserReadyToCommit.Somewhat.Text%></td>
						<td style="width:19%; text-align:center"><%= IsUserReadyToCommit.Maybe.Text%></td>
						<td style="width:17%; text-align:right"><%= IsUserReadyToCommit.Unsure.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
			    <div class="item">
					<p><%= DebtOptions.GetQuestion() %></p>
					<div style="text-align:center">
					    <span id="DebtOptions-slider"></span>
					</div>
					<div>
					<table class="checkbox-table" style="width:950px">
					<tr>
						<td><label for="cb<%=DebtOptions.CreditCounseling.Value%>"><%=DebtOptions.CreditCounseling.Text %></label><%= Html.CheckBox("DebtOptions",DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.CreditCounseling),new {value = DebtOptions.CreditCounseling.Value,id="cb" + DebtOptions.CreditCounseling.Value})%></td>
						<td><label for="cb<%=DebtOptions.DMP.Value%>"><%=DebtOptions.DMP.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.DMP), new { value = DebtOptions.DMP.Value, id = "cb" + DebtOptions.DMP.Value })%></td>
					</tr><tr>
						<td><label for="cb<%=DebtOptions.BankruptcyFiling.Value%>"><%=DebtOptions.BankruptcyFiling.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.BankruptcyFiling), new { value = DebtOptions.BankruptcyFiling.Value, id = "cb" + DebtOptions.BankruptcyFiling.Value })%></td>
						<td><label for="cb<%=DebtOptions.DebtSettlementDeal.Value%>"><%=DebtOptions.DebtSettlementDeal.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.DebtSettlementDeal), new { value = DebtOptions.DebtSettlementDeal.Value, id = "cb" + DebtOptions.DebtSettlementDeal.Value })%></td>
					</tr><tr>
						<td><label for="cb<%=DebtOptions.DebtSettlementLoan.Value%>"><%=DebtOptions.DebtSettlementLoan.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.DebtSettlementLoan), new { value = DebtOptions.DebtSettlementLoan.Value, id = "cb" + DebtOptions.DebtSettlementLoan.Value })%></td>
						<td><label for="cb<%=DebtOptions.FinancialAndDebtEducation.Value%>"><%=DebtOptions.FinancialAndDebtEducation.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.FinancialAndDebtEducation), new { value = DebtOptions.FinancialAndDebtEducation.Value, id = "cb" + DebtOptions.FinancialAndDebtEducation.Value })%></td>
					</tr><tr>
						<td><label for="cb<%=DebtOptions.SelfManagement.Value%>"><%=DebtOptions.SelfManagement.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.SelfManagement), new { value = DebtOptions.SelfManagement.Value, id = "cb" + DebtOptions.SelfManagement.Value })%></td>
						<td><label for="cb<%=DebtOptions.None.Value%>"><%=DebtOptions.None.Text %></label><%= Html.CheckBox("DebtOptions", DebtOptions.GetAnswer(NeedsAssessmentViewData.DebtOptions).Contains(DebtOptions.None), new { value = DebtOptions.None.Value, id = "cb" + DebtOptions.None.Value })%></td>
					</tr>
					</table>
					</div>
			    </div>
    
    
    <div class="previousNextButtons">
	<span class="submitBackButton">
		<%= Html.SubmitButton("GoToPrevStep", NeedsAssessmentViewData.PreviousButtonText)%>
	</span>
	<span class="submitNextButton">
		<%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%>
	</span>
    </div>
<% } %>

        </div>