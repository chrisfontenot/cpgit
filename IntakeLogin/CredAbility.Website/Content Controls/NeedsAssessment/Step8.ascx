﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>


<div id="assessment">
										
	<div class="assessment-content">
		<div class="data"><%= NeedsAssessmentViewData.CurrentSituationShort %></div>
		<div class="description">
			<p><%= NeedsAssessmentViewData.CurrentSituation %></p>
			<p class="note"><%= NeedsAssessmentViewData.LegalLanguage %></p>
		</div>
	</div>
	<div style="clear:both"></div>
	<div>
		<h3><%=NeedsAssessmentViewData.DebtLevelText %></h3>
		<div class="drag-bar">
			<div class="bar" style="width:<%=NeedsAssessmentViewData.DebtLevel%>px"></div>
		</div>
		<div class="scale">
			<table width="600" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="200" align="left"><%= Resources.Website.NeedAssessment_LowRisk%></td>
					<td width="200" align="left"><%= Resources.Website.NeedAssessment_Normal%></td>
					<td width="200" align="left"><%= Resources.Website.NeedAssessment_HighRisk%></td>
				</tr>
			</table>
		</div>
	</div>
	<div>
		<p><%= NeedsAssessmentViewData.Messaging %></p>
	</div>
</div>
<div class="aside">
	<div class="may-help-u">
		<h3><%= NeedsAssessmentViewData.MayHelpYou %></h3>
		<ul>
		    <% foreach (var KeyItemText in NeedsAssessmentViewData.KeyItem){ %>
            <li><%=KeyItemText%></li>
            <% } %>
		</ul>
	<div class="max-btn get-started"><%= NeedsAssessmentViewData.GetStartedUrl %></div>
	</div>
</div>