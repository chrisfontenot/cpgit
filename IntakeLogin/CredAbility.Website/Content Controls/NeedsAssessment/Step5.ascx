﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep5Questions"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>


		<div class="list yui3-skin-sam yui-skin-sam">
		
<% using (Html.BeginForm()){%>
<%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
<%--//Step 1--%>
    <%= Html.Hidden("MonthlyRevolvingCreditCardDebt", NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebt)%>
    <%= Html.Hidden("HomeMortgage", NeedsAssessmentViewData.HomeMortgage)%>
    <%= Html.Hidden("OtherDebt", NeedsAssessmentViewData.OtherDebt)%>
    <%= Html.Hidden("InterestedInDMP", NeedsAssessmentViewData.InterestedInDMP)%>
    <%= Html.Hidden("HomePurchase", NeedsAssessmentViewData.HomePurchase)%>
    <%= Html.Hidden("InterestedInReverseMortgage", NeedsAssessmentViewData.InterestedInReverseMortgage)%>
    <%= Html.Hidden("BankruptcyFilingProcess", NeedsAssessmentViewData.BankruptcyFilingProcess)%>
    <%--//Step 2--%>
    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment)%>
    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort)%>
    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard)%>
    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit)%>
    <%--//Step 3--%>
    <%--//Step 4--%>
    <%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount)%>
    <%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment)%>
    <%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest)%>
    <%--//Step 6--%>
    <%= Html.Hidden("CanMakeHomeDownPayment", NeedsAssessmentViewData.CanMakeHomeDownPayment)%>
    <%--//Step 7--%>
    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit)%>
    <%= Html.Hidden("DebtOptions", NeedsAssessmentViewData.DebtOptions)%>
    
    
    
    
                <div class="item">
					<p><%= ConcernedAboutMortgagePayment.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment, new { Class = "slider", max = ConcernedAboutMortgagePayment.Count() })%>
					    <span id="ConcernedAboutMortgagePayment-slider"></span>
					</div>
					<div class="scale">
					<table style="width:910px;margin:0px 0px 0px 20px">
					<tr>
						<td style="width:25%; text-align:left"><%= ConcernedAboutMortgagePayment.Extremely.Text%></td>
						<td style="width:15%; text-align:left"><%= ConcernedAboutMortgagePayment.Very.Text%></td>
						<td style="width:18%; text-align:center"><%= ConcernedAboutMortgagePayment.Somewhat.Text%></td>
						<td style="width:18%; text-align:right"><%= ConcernedAboutMortgagePayment.NotVery.Text%></td>
						<td style="width:24%; text-align:right"><%= ConcernedAboutMortgagePayment.NotAtAll.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
			    <div class="item">
					<p><%= TimelyHomeMortgagePayment.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment, new { Class = "slider", max = TimelyHomeMortgagePayment.Count() })%>
					    <span id="TimelyHomeMortgagePayment-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:24%; text-align:left"><%= TimelyHomeMortgagePayment.Behing60Days.Text%></td>
						<td style="width:17%; text-align:left"><%= TimelyHomeMortgagePayment.Behing30Days.Text%></td>
						<td style="width:20%; text-align:center"><%= TimelyHomeMortgagePayment.AbitLate.Text%></td>
						<td style="width:17%; text-align:right"><%= TimelyHomeMortgagePayment.UsuallyOnTime.Text%></td>
						<td style="width:24%; text-align:right"><%= TimelyHomeMortgagePayment.AlwaysOnTime.Text%></td>
					</tr>
					</table>
					</div>
			    </div>
			    <div id="assessment-selection">
                <table width="950" border="0" cellpadding="0" cellspacing="0">
	                <tr class="odd">
			                <td width="660" height="60" class="selection">
					                <p><%= NeedsAssessmentViewData.HomeForclosureThreatText %></p>
			                </td>
			                <td width="160" class="radio">
					                <%= Html.RadioButton("HomeForclosureThreat", true, NeedsAssessmentViewData.GetHomeForclosureThreat.Value == NeedsAssessmentYesNo.Yes.Value, new { id = "HomeForclosureThreatYes" })%>
					                <label for="HomeForclosureThreatYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					                <%= Html.RadioButton("HomeForclosureThreat", false, NeedsAssessmentViewData.GetHomeForclosureThreat.Value == NeedsAssessmentYesNo.No.Value, new { id = "HomeForclosureThreatNo" })%>
					                <label for="HomeForclosureThreatNo"><%= Resources.Website.NeedAssessment_No %></label>
			                </td>
			                
	                </tr>
	            </table>
	            </div>
    
    <div class="previousNextButtons">
	<span class="submitBackButton">
		<%= Html.SubmitButton("GoToPrevStep", NeedsAssessmentViewData.PreviousButtonText)%>
	</span>
	<span class="submitNextButton">
		<%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%>
	</span>
    </div>
<% } %>


        </div>