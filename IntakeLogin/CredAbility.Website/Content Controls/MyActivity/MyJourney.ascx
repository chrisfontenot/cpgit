﻿<%@ Import Namespace="CredAbility.Web.Authentification"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyJourney.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyActivity.MyJourney" %>
<%@ Register TagName="DMP" TagPrefix="uc" Src="~/Content Controls/DMP.ascx" %>
<div id="recent-activity" class="recent-activity">
 
 
 <% if (MaxPage != 1){ %>
    <div class="pager">
    <% if (CurrentPage != 1){ %>
        <span><a href="<%= GetUrlForPage(CurrentPage - 1) %>" ><%= Resources.Website.Global_Previous %></a></span>
    <% } %>
    
    <% for (var i = 1;i<=MaxPage;i++){ %>
	<% if (i == CurrentPage){ %>
		<span class="current"><%= i %></span>
	<% }else{ %>
        <span><a href="<%= GetUrlForPage(i) %>" ><%= i %></a></span>
	<% } %>
    <% } %>
    
    <% if (CurrentPage != MaxPage){ %>
        <span><a href="<%= GetUrlForPage(CurrentPage + 1) %>" ><%= Resources.Website.Global_Next %></a></span>
    <% } %>
</div>
<% } %>
 
 
 
 
 
    <div style="clear:both">
 
    	<% foreach (var activity in Activities.GetActivitiesByDate()) { %>
		
		<div class="bypast">
			<% if(activity.Key !=  ClientProfileManager.CurrentClientProfile.LastLogin) {%>
				<span class="calendar-icon"></span>
				<h4><%=activity.Key.ToLongDateString() %></h4>		
			<% } %>
			<div class="info-list">
				<% foreach (var userActivity in activity.Value){%>
					<p><span class="activityIcon <%= userActivity.Type %>"></span><%=userActivity.Text %></p>
				<% } %>	
			</div>
		</div>
	<%} %>
    </div>
    
</div>