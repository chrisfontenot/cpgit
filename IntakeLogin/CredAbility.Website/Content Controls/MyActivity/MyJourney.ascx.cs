﻿using System;
using CredAbility.Core.Activity;
using CredAbility.Core.DMP;
using CredAbility.Data.Activity;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;


namespace CredAbility.Website.Content_Controls.MyActivity
{
    public partial class MyJourney : MyAccountBaseControl
    {
        protected UserActivityList Activities { get; set; }
        
        protected int CurrentPage { get; set; }
        protected int MaxPage { get; set; }

        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            int totalCount;
            int maxResult = int.Parse(ConfigReader.ConfigSection.MaxActivityByPage.Value);


            int currentPage;
            int.TryParse(Request.QueryString["p"], out currentPage);
            CurrentPage = currentPage < 1 ? 1 : currentPage;


            Activities = UserActivityRepository.GetUserActivities(ClientProfile.ClientID, maxResult, CurrentPage, out totalCount, Manager.GetDMP());

            MaxPage = (totalCount / maxResult) + ((totalCount % maxResult) > 0 ? 1 : 0);
            
        }


        protected string GetUrlForPage(int pageNumber)
        {
            var url = "";
            var queryString = "";
            queryString += "?p=" + pageNumber;

            url += "my-journey.aspx"; 
            url += queryString;
            return url;
        }
    }
}