﻿using System;
using CredAbility.Core.Activity;
using CredAbility.Core.Client;
using CredAbility.Data.Activity;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;

namespace CredAbility.Website.Content_Controls.MyActivity
{
	public partial class MyRecentActivity : CounselingSummaryBaseControl
	{
		protected UserActivityList Activities { get; set; }
		protected override void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
			Activities = UserActivityRepository.GetUserActivities(ClientProfile.ClientID, int.Parse(ConfigReader.ConfigSection.MaxActivityToShow.Value), Manager.GetDMP());
		}
	}
}