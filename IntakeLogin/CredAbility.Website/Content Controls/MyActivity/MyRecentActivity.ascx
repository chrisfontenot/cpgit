﻿<%@ Import Namespace="CredAbility.Web.Authentification" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyRecentActivity.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.MyActivity.MyRecentActivity" %>
<%@ Import Namespace="CredAbility.Web.Configuration" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<%@ Import Namespace="Sitecore.Security.Accounts" %>
<%@ Register TagName="DMP" TagPrefix="ca" Src="~/Content Controls/DMP.ascx" %>
<%@ Register TagName="FeeWaiver" TagPrefix="ca" Src="~/Content Controls/FeeWaiver.ascx" %>
<%@ Register TagName="CscAlert" TagPrefix="ca" Src="~/Content Controls/CreditScoreChallengeAlert.ascx" %>
<div id="recent-activity" class="recent-activity">
    <div>
        <ca:DMP runat="server" />
        <div class="last-login">
            <%= string.Format(Resources.MyAccount.LastLogin, ClientProfileManager.CurrentClientProfile.LastLogin.ToLongDateString(),ClientProfileManager.CurrentClientProfile.LastLogin.ToShortTimeString() )%></div>
    </div>
    <div style="clear: both">
        <ca:FeeWaiver runat="server" />
        <ca:CscAlert ID="FeeWaiver1" runat="server" />
    </div>
    <% if (PastSummariesUrl != null){ %>
        <div class="links">
            <div class="link-btn download">
                <a href="~/Documents/Documents.ashx?id=<%=ClientProfile.DocumentID%>"><%=Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a>
            </div>
            <div class="link-btn view">
                <a href="<%=PastSummariesUrl%>"><%=Resources.MyAccount.MyCounseling_Summary_ViewMyPastCounseling%></a>
            </div>
        </div>
    <%   } %>
</div>
