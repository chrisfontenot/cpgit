﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorSeeBelow.ascx.cs" Inherits="CredAbility.Website.Content_Controls.ErrorSeeBelow" %>
<%@ Import Namespace="MvcContrib.Filters"%>

<div class="error-see-below">
<%=Resources.Website.Global_ErrorSeeBelow%>
</div>