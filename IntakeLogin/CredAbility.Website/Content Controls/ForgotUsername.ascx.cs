﻿using System;
using System.Collections.Generic;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Web.Authentification;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Security;

namespace CredAbility.Website.Content_Controls
{
    public partial class ForgotUsername : FormBaseControl
    {
        string emailAddress = "";
        protected bool _displayAccountLockedMsg;

        

        protected void Page_Load(object sender, EventArgs e)
        {
            
            ShowUsernameLabel.Text = "";

            if (!this.IsPostBack)
            {
                GetEmailPanel.Visible = true;
                ContactCustomerServicePanel.Visible = false;
                SecurityQuestionsPanel.Visible = false;
                SeeUsernamePanel.Visible = false;
            }

            ErrorSeeBelow1.Visible = false; 
            ErrorSeeBelow2.Visible = false;
        }

        protected void GoToLoginPageButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Login.aspx");
        }

        protected void SecurityQuestionsSubmitButton_Click(object sender, EventArgs e)
        {
            ActionResult actionResult;

            var clientProfileManager = new ClientProfileManager();
            var responseUsernameRecoverResult = clientProfileManager.ValidateSecurityAnswersByEmail(EmailAddressTextBox.Text, Question1AnswerTextBox.Text, out actionResult);

            if (actionResult.IsSuccessful)
            {
                var usernameRecoverResult = responseUsernameRecoverResult.ResponseValue;

                if (usernameRecoverResult.IsSuccessful)
                {
                    // show the username panel
                    GetEmailPanel.Visible = false;
                    ContactCustomerServicePanel.Visible = false;
                    SecurityQuestionsPanel.Visible = false;
                    SeeUsernamePanel.Visible = true;
                    ShowUsernameLabel.Text = usernameRecoverResult.Username;
                }
                else
                {
                    if (usernameRecoverResult.IsUserLocked.HasValue && usernameRecoverResult.IsUserLocked.Value)
                    {
                        // show the contact customer service panel
                        SeeUsernamePanel.Visible = false;
                        GetEmailPanel.Visible = false;
                        _displayAccountLockedMsg = true;
                        ContactCustomerServicePanel.Visible = true;
                        SecurityQuestionsPanel.Visible = false;
                    }
                    else
                    {
                        // show the security questions panel again
                        SeeUsernamePanel.Visible = false;
                        GetEmailPanel.Visible = false;
                        ContactCustomerServicePanel.Visible = false;
                        SecurityQuestionsPanel.Visible = true;
                    }
                }
            }

            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty) && !actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow2.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty) || actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow2.Visible = false;
                this.ResultMessagesStep2.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            emailAddress = SafeRequestHelper.SanitizeHtml(EmailAddressTextBox.Text);

            ActionResult actionResult;
            var clientProfileManager = new ClientProfileManager();
            var resultStep1 = clientProfileManager.ForgotUsername(emailAddress, out actionResult);

            if (actionResult.IsSuccessful)
            {
                if (resultStep1 != null)
                {
                    GetEmailPanel.Visible = false;
                    ContactCustomerServicePanel.Visible = false;
                    SecurityQuestionsPanel.Visible = true;
                    SeeUsernamePanel.Visible = false;

                    Question1Label.Text = resultStep1.Text;
                }
                else
                {
                    SeeUsernamePanel.Visible = false;
                    GetEmailPanel.Visible = false;
                    ContactCustomerServicePanel.Visible = true;
                    SecurityQuestionsPanel.Visible = false;
                }
            }
            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty) && !actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow1.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty) || actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow1.Visible = false;
                this.ResultMessagesStep1.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;

        }

    }
}