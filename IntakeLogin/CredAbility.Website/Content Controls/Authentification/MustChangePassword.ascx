﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MustChangePassword.ascx.cs" Inherits="CredAbility.Website.Content_Controls.Authentification.MustChangePassword" %>
<div id="change-password">
    
    
    <div>
        
        <ca:ResultMessages ID="ResultMessages" runat="server"></ca:ResultMessages>
    </div>

    <div class="password-info">
            
                    <asp:Label ID="CurrentPasswordLabel" runat="server" Text="<%$Resources:MyAccount, ChangePassword_CurrentPassword%>" AssociatedControlID="CurrentPasswordTextBox"></asp:Label>
            
                    <asp:TextBox ID="CurrentPasswordTextBox" runat="server" TextMode="Password"></asp:TextBox>
                
            
            
                    <asp:Label ID="NewPasswordLabel" runat="server" Text="<%$Resources:MyAccount, ChangePassword_NewPassword%>" AssociatedControlID="NewPasswordTextBox"></asp:Label>
                     <div class="passwordRules" id="myPanel" style="visiblity:hidden;display:none">
                        <div id="panel1">
                            <div class="hd"><%= Resources.MyAccount.Global_PasswordRules %></div>
                            <div class="bd">
                                <iframe src="/global/password-rules.aspx" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>

                <asp:TextBox ID="NewPasswordTextBox" runat="server" TextMode="Password"></asp:TextBox>
                <a href="#" id="show1" class="CreateAccountTextLink" title="<%= Resources.MyAccount.Global_PasswordRules %>"><img src="/static/images/layout/ico-questionmark.gif"/></a> 
                <div id="strength"></div>
                    
    
    
    
    
                    <asp:Label ID="RetypePasswordLabel" runat="server" Text="<%$Resources:MyAccount, ChangePassword_RetypePassword%>" AssociatedControlID="RetypePasswordTextBox"></asp:Label>
    
                <asp:TextBox ID="RetypePasswordTextBox" runat="server" TextMode="Password"></asp:TextBox>
                <div id="confirmed"></div>
    </div>
    
    <div class="button">
            <div class="submitNextButton"><asp:LinkButton ID="CancelButton" runat="server" CssClass="rounded_button" BorderStyle="None" OnClick="CancelButton_Click"><% =Resources.MyAccount.Profile_CancelButton %></asp:LinkButton></div><div class="submitNextButton"><asp:LinkButton ID="SaveProfileButton" runat="server" CssClass="rounded_button" BorderStyle="None" OnClick="SaveProfileButton_Click" ValidationGroup="Password"><% =Resources.MyAccount.Profile_SaveProfileButton %></asp:LinkButton></div>
    </div>
</div>




<script>
var PasswordRuleInit = true;
</script>
        