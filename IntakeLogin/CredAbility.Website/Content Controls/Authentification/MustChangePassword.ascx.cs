﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Validator;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls.Authentification
{
    public partial class MustChangePassword : MyAccountBaseControl
    {

        protected override void Page_Load(object sender, EventArgs e)
        {
            //Kill base action.
        }
        
        protected void CancelButton_Click(object sender, EventArgs e)
        {
            // TODO : Define the url into web.config and ConfigReader
            Response.Redirect("/my-account/my-profile/default.aspx");
        }

        protected void SaveProfileButton_Click(object sender, EventArgs e)
        {
            ActionResult actionResult = new ActionResult(true);

           


                ClientProfileManager manager = new ClientProfileManager();

                this.FieldValidator(actionResult);

                if (actionResult.IsSuccessful)
                {
                    actionResult.MergeActionResult(manager.ChangePassword(CurrentPasswordTextBox.Text,NewPasswordTextBox.Text,RetypePasswordTextBox.Text));

                    if (actionResult.IsSuccessful)
                    {
                        Sitecore.Diagnostics.Log.Audit("Change Password successed", this);
                        AuthentificationContext.Current.Redirect(this.ClientProfile);
                    }
                }


            

            ResultMessages.Messages = actionResult.Messages;

        }


        private void FieldValidator(ActionResult result)
        {
            result.IsSuccessful = true;

            

            if (string.IsNullOrEmpty(this.CurrentPasswordTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ChangePassword_CurrentPasswordRequired, "CPR"));
            }
          

            if (string.IsNullOrEmpty(this.NewPasswordTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ChangePassword_NewPasswordRequired, "NPR"));
            }


            if (string.IsNullOrEmpty(this.RetypePasswordTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ChangePassword_RetypePasswordRequired, "RPR"));
            }

            if (this.RetypePasswordTextBox.Text != this.NewPasswordTextBox.Text)
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ChangePassword_RetypePasswordComparison, "RPC"));
            }


           

        }
    }
}