﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Core.Common;
using CredAbility.Data.Validator;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.Data;
using CredAbility.Web.UI.UserControls;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls.Authentification
{
	public partial class MustChangeUsername:MyAccountBaseControl
	{
		private string ads = "";
		ActionResult actionResult = new ActionResult();

		protected override void Page_Load(object sender, EventArgs e)
		{
			if(!this.IsPostBack)
			{
				GetSecurityQuestions();
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if(!actionResult.IsSuccessful)
			{
				this.ResultMessages.Messages = actionResult.Messages;
			}
		}

		private void GetSecurityQuestions()
		{
			var questions = IdentityData.GetAllSecurityQuestions(ClientProfile.PreferredLanguage);
			foreach(var question in questions)
			{
				Question1DDL.Items.Add(new ListItem(question.Text, question.ID.ToString()));
			}
		}

		protected void SaveProfileButton_Click(object sender, EventArgs e)
		{
			actionResult.IsSuccessful = true;
			PasswordValidator validator = new PasswordValidator();
			ClientProfile profile = new ClientProfile();
			profile.Password = this.NewPassswordTextBox.Text;
			actionResult.MergeActionResult(validator.Validate(profile));
			this.FieldFieldValidator(actionResult);
			if(actionResult.IsSuccessful)
			{
				CustomResponse<Data.Services.Identity.User> responseUser = IdentityRepository.GetUser(ClientProfile.ClientID);
				Data.Services.Identity.User user = responseUser.ResponseValue;
				if(!string.IsNullOrEmpty(UsernameTextBox.Text))
				{
					user.Username = UsernameTextBox.Text;
				}
				SecurityQuestionAnswer sqa1 = new SecurityQuestionAnswer();
				sqa1.SecurityQuestionId = byte.Parse(Question1DDL.SelectedValue);
				sqa1.SecurityAnswer = Question1AnswerTextBox.Text;
				user.SecurityQuestionAnswer = sqa1;
				user.IsProfileUpdateRequired = false;
				user.NewPassword = NewPassswordTextBox.Text;
				user.NewPasswordConfirm = RetypeNewPasswordTextBox.Text;
				ActionResult result = IdentityRepository.SaveUser(ClientProfile, user);
				if(!result.IsSuccessful)
				{
					this.ResultMessages.Messages = result.Messages;
				}
				else
				{
					Sitecore.Diagnostics.Log.Audit(string.Format("CHANGE USERNAME: {0}", ClientProfile.Username), this);
					this.ClientProfile.Username = UsernameTextBox.Text;
					this.ClientProfile.SecurityQuestion = Question1DDL.SelectedValue;
					this.ClientProfile.SecurityAnswer = Question1AnswerTextBox.Text;
					this.ClientProfile.IsProfileUpdateRequired = false;
					AuthentificationContext.Current.IsActiveLosCodeRedirection = true;
					if(result.IsSuccessful && Sitecore.Context.User.Roles.Contains(Role.FromName("extranet\\LimitedAccess")))
					{
						Sitecore.Context.User.Roles.Remove(Role.FromName("extranet\\LimitedAccess"));
					}
					AuthentificationContext.Current.Redirect(this.ClientProfile);
				}
			}
		}

		private void FieldFieldValidator(ActionResult result)
		{
			result.IsSuccessful = true;
			if(string.IsNullOrEmpty(this.RetypeNewPasswordTextBox.Text))
			{
				result.IsSuccessful = false;
				result.Messages.Add(new ResultMessage(Resources.MyAccount.ForgotPassword_RetypePasswordRequired, "RPR"));
			}
			if(this.RetypeNewPasswordTextBox.Text != this.NewPassswordTextBox.Text)
			{
				result.IsSuccessful = false;
				result.Messages.Add(new ResultMessage(Resources.MyAccount.ForgotPassword_RetypePasswordComparison, "RPC"));
			}
			if(string.IsNullOrEmpty(Question1AnswerTextBox.Text))
			{
				actionResult.IsSuccessful = false;
				actionResult.Messages.Add(new ResultMessage(Resources.MyAccount.ChangeUsername_Answer1Required, ""));
			}
		}

		protected void Check_Username(object sender, EventArgs e)
		{
			if(IdentityRepository.ConfirmValidUsername(UsernameTextBox.Text))
			{
				UsernameAvailableLabel.Text = "available";
				UsernameAvailableLabel.CssClass = "CreateAccountUsernameAvailable";
			}
			else
			{
				UsernameAvailableLabel.Text = "not available";
				UsernameAvailableLabel.CssClass = "CreateAccountUsernameNotAvailable";
			}
		}
	}
}