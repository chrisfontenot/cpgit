﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="CredAbility.Website.Content_Controls.Login" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div class="form-block">
    <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
    <ca:ResultMessages id="ResultMessages" runat="server"/>


    <p><asp:Literal runat="server" Text="<%$Resources:MyAccount, Login_HeaderText%>"></asp:Literal></p>

    <div class="field-block <%= GetCssClassIfMessageCodeExist("USERNAME-REQUIRED", "field-block-error") %>">
        <asp:Label runat="server" AssociatedControlID="Username" Text="<%$Resources:MyAccount, Login_Username%>"/>
        <div class="field-element">
            <asp:TextBox TabIndex="1"  ID="Username" runat="server" Columns="40"/>
            <div class="field-message"><%= GetMessage("USERNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
       </div>
       <div class="clearboth"></div>
    </div>



    <div class="field-block <%= GetCssClassIfMessageCodeExist("PASSWORD-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label1" runat="server" AssociatedControlID="Password" Text="<%$Resources:MyAccount, Login_Password%>"/>
        <div class="field-element">
            <asp:TextBox TabIndex="2" ID="Password" TextMode="Password" runat="server" Columns="40"/>
            <div class="field-message"><%= GetMessage("PASSWORD-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>

    <div class="field-block">
        <asp:Label ID="Label2" runat="server" AssociatedControlID="RememberUsername" Text="&nbsp;"/>
        <div class="field-element">
            <asp:Label ID="Label3" runat="server" Text="<%$Resources:MyAccount, Login_RememberUsername%>"/>
            <asp:CheckBox ID="RememberUsername" runat="server" />
       </div>
       <div class="clearboth"></div>
    </div>

    <div class="form-action"  >
        <span class="submitNextButton"><asp:Button runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:MyAccount,Login_LoginButton %>" /></span>
    </div>
</div>