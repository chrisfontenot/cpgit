﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeWaiver.ascx.cs" Inherits="CredAbility.Website.Content_Controls.FeeWaiver" %>
<%@ Import Namespace="CredAbility.Core.Client"%>
<%@ Import Namespace="CredAbility.Web.Authentification"%>
<%@ Import Namespace="CredAbility.Web.Configuration"%>

<% 
        
            if (LastVisitedWebsiteInfo != null)
            { %>
        <%if (!ClientProfileManager.CurrentClientProfile.Types.Contains("CA"))
          {%>
        <div class="activity-status">
            <div class="icon">
            </div>
            <h3>
                <% 
          
                    if (LastVisitedWebsiteInfo.PercentComplete <= int.Parse(ConfigReader.ConfigSection.FeeWaiverPercentage.Value))
                    {

                        if (this.CheckFeeWaiver)
                        {
                            %>

                            <%= this.GetFreeWaiverText() %>

                           <%

                        }
                        else
                        {
                %>
                <%=string.Format(Resources.MyAccount.MyRecentActivity_LastVisit,
                                                        LastVisitedWebsiteInfo.PercentComplete,
                                                        LastVisitedWebsiteInfo.WebsiteText)%>
                <%
                    }
                    }
                    else
                    {%>
                <%=string.Format(Resources.MyAccount.MyRecentActivity_LastVisit, LastVisitedWebsiteInfo.PercentComplete, LastVisitedWebsiteInfo.WebsiteText)%>
                <%} %>
            </h3>
            
            <% if (LastVisitedWebsiteInfo.PercentComplete < 100)
               { %>
            <div class="question">
                <%= Resources.MyAccount.MyRecentActivity_ContinueSession%></div>
            <div class="link-block">
                <div class="link-btn continue">
                    <a href="<%=LastVisitedWebsiteInfo.Url %>">
                        <%= Resources.MyAccount.MyRecentActivity_Yes%></a></div>
            </div>
            <%} %>
        </div>
        <% } %>
        <% } %>
