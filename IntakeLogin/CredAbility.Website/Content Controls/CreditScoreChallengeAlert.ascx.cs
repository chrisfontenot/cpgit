﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.CreditScoreChallenge;

namespace CredAbility.Website.Content_Controls
{
    public partial class CreditScoreChallengeAlert : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = CreditScoreChallengeContext.Current.IsFirstTime;
        }
    }
}