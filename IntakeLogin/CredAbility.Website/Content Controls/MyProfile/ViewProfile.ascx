﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewProfile.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.ViewProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:ScriptManager ID="ScriptManager" runat="server">
</asp:ScriptManager>
    <div>
        
        <ca:ResultMessages ID="ResultMessages" runat="server" CssClass="AccountFormErrorMessage"></ca:ResultMessages>
    </div>
<div id="view-profile">
		<div class="button">
				<div class="submitNextButton"><a href="edit-profile.aspx"><% =Resources.MyAccount.Profile_EditProfileButton %></a></div>
		</div>
		
		<div class="account-info  clearfix">
				<h3><%= Resources.MyAccount.Profile_AccountInformation%></h3>
				
				<div class="avatar"> 
				    <asp:Image ID="AvatarImage" runat="server" AlternateText="Avatar" /> <asp:LinkButton id="ChangeAvatarButton" runat="server"><%=Resources.MyAccount.Profile_ChangeAvatar%></asp:LinkButton> 
				</div>
				<div class="personal-info">
				    <div class="item user-name">
				            <label><%= Resources.MyAccount.Profile_Username %></label>
    						
						    <span><%= this.profile.Username%></span>
    						
				    </div>
				    <div class="item float first-name ">
						    <label><%=Resources.MyAccount.Profile_FirstName %></label>
						    <span><%= this.profile.FirstName%></span>
						    
				    </div>
				    <div class="item middle-initial float">
						    <label><%=Resources.MyAccount.Profile_MiddleInitial %></label>
						    <span><%= this.profile.MiddleName%></span>
				    </div>
				    <div class="item last-name float">
						    <label><%=Resources.MyAccount.Profile_LastName %></label>
						    <span><%= this.profile.LastName%></span>
				    </div>
				    
				    <div class="item long">
			            <label><%=Resources.MyAccount.Profile_SSN%></label>
					    <span><%= this.profile.SSN.SecuredSSN%></span>
				    </div>
				    
				    
				    <div class="item">
			            <label><%=Resources.MyAccount.Profile_MaritalStatus%></label>
					    <span><%= this.profile.MaritalStatus%></span>
				    </div>
				    
				     <div class="item long">
			            <label><%=Resources.MyAccount.Profile_LanguagePreference%></label>
					    <span><%= this.profile.PreferredLanguage%></span>
				    </div>
				</div>
				
				
				
		</div>
		<div class="mail-password  clearfix">
				<h3><%= Resources.MyAccount.Profile_EmailAndPassword%></h3>
				
				<div class="clear">
				    <div class="item mail-view">
						    <label><%=Resources.MyAccount.Profile_EmailAddress%></label>
						    <span><%= this.profile.EmailAddress %></span>
    						
    						
				            <div class="submitNextButton save"><a href="change-password.aspx"><%=Resources.MyAccount.Profile_ChangePassword %></a></div>
				    </div>
				
				</div>
				
				
				
				
				
				
				<div class="item question-view">
						<label><%=Resources.MyAccount.Profile_SecretChallengeQuestion1%></label>
						<span><%= this.profile.SecurityQuestion %></span>
						<label><%=Resources.MyAccount.Profile_Answer1%></label>
						<span><%= this.profile.SecurityAnswer %></span>
				</div>
				
		</div>
		<div class="address-phone  clearfix">
			<h3><%=Resources.MyAccount.Profile_MailingAddressAndPhone%></h3>
			
			<div class="clear">
			    <div class="item home">
						    <label><%=Resources.MyAccount.Profile_HomePhone%></label>
						    <span><%= this.profile.HomePhone %></span>
			    </div>
			    <div class="item work">
						    <label><%=Resources.MyAccount.Profile_WorkPhone%></label>
						    <span><%= this.profile.WorkPhone %></span>
			    </div>
			    <div class="item cell">
						    <label><%=Resources.MyAccount.Profile_CellPhone%></label>
						    <span><%= this.profile.CellPhone %></span>
			    </div>
			</div>
			
			<div class="clear">
			    <div class="item address">
						    <label><%=Resources.MyAccount.Profile_MailingAddress%></label>
						    <span><%= this.profile.MailingAddressLine1 %></span>
			    </div>
			    <div class="item address2">
						    <label><%=Resources.MyAccount.Profile_MailingAddressLine2%></label>
						    <span><%= this.profile.MailingAddressLine2 %></span>
			    </div>
			</div>
			<div class="clear">
			    <div class="item city">
						    <label><%=Resources.MyAccount.Profile_City%></label>
						    <span><%= this.profile.City %></span>
			    </div>
			    <div class="item state">
						    <label><%=Resources.MyAccount.Profile_State%></label>
						    <span><%= this.profile.State %></span>
			    </div>
			    <div class="item zip">
						    <label><%=Resources.MyAccount.Profile_Zip%></label>
						    <span><%= this.profile.Zip %></span>
			    </div>
			</div>
		</div>
		<div class="button">
				<div class="submitNextButton"><a href="edit-profile.aspx"><% =Resources.MyAccount.Profile_EditProfileButton %></a></div>
		</div>
		
		
</div>
				
				
<asp:Panel ID="AvatarModalPanel" runat="server" style="padding:10px;background:#000" >
<asp:Panel ID="AvatarPanel" runat="server"></asp:Panel>
</asp:Panel>
<ajaxtoolkit:ModalPopupExtender ID="AvatarModal" runat="server" TargetControlID="ChangeAvatarButton"
    PopupControlID="AvatarModalPanel" BackgroundCssClass="AccountModalBackground"/>
