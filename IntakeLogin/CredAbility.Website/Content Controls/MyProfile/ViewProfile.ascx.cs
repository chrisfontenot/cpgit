﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.UI.UserControls;
using MaritalStatus = CredAbility.Web.MaritalStatus;

namespace CredAbility.Website.Content_Controls
{
    public partial class ViewProfile : MyAccountBaseControl
    {
        protected UserProfile userProfile;
        protected UserDetail userDetail;
        protected Profile profile;

        protected override void Page_Load(object sender, EventArgs e)
        {

            base.Page_Load(sender, e);

            //Patch

            Sitecore.Diagnostics.Log.Audit(string.Format("View profile page accessed. Session informations: username: {0}, clientId: {1}, session Id: {2}", ClientProfileManager.CurrentClientProfile.Username, ClientProfileManager.CurrentClientProfile.ClientID, HttpContext.Current.Session.SessionID), this);
       
            // get the user profile and user detail
            CustomResponse<UserProfile> responseUserProfile = IdentityRepository.GetUserProfile(ClientProfile.ClientID);
            userProfile = responseUserProfile.ResponseValue;
            if (userProfile.UserDetailPrimary != null)
            {
                userDetail = userProfile.UserDetailPrimary;
            }

            //TAG USERID VALIDATION
            if (userDetail == null || ClientProfileManager.CurrentClientProfile.ClientID != userDetail.UserId.ToString())
             {
                 Manager.Logout();
             }



            this.InitializeAvatar();

            this.InitializeProfile();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.DisplaySavedProfileMessage();
        }

        private void DisplaySavedProfileMessage()
        {
            if (Request.QueryString["saved"] != null)
            {
                string message = "Profile Saved.";
                if (ResultMessages.Messages == null)
                {
                    ResultMessages.Messages = new ResultMessageList();
                }
                ResultMessages.Messages.Add(new ResultMessage(ResultMessageLevelType.Info, message, ""));
            }
        }

        public void ImageButtonClick(object sender, ImageClickEventArgs e)
        {
            ActionResult result = Manager.SaveAvatar(((ImageButton)sender).ID);
            
            if (!result.IsSuccessful)
            {
                ResultMessages.Messages = result.Messages;
            }
            else
            {
                //note : do we really need that redirection?
                Response.Redirect("/my-account/my-profile/default.aspx");
            }
        }

        private void InitializeAvatar()
        {
            AvatarImage.ImageUrl = AvatarHelper.GetThumbnailFilePath(ClientProfile.Avatar);

            foreach (var avatarKey in AvatarHelper.GetAvailableAvatarKeys())
            {
                ImageButton imageButton = new ImageButton();
                imageButton.Click += ImageButtonClick;
                imageButton.CssClass = "SelectAvatarModalImage";
                imageButton.ID = avatarKey;
                imageButton.ImageUrl = AvatarHelper.GetThumbnailFilePath(avatarKey);
                AvatarPanel.Controls.Add(imageButton);
            }
        }

        private void InitializeProfile()
        {

            profile = new Profile();

            if (userProfile != null && userDetail != null)
            {
                profile.Username = userProfile.Username;
                profile.FirstName = userDetail.FirstName;
                profile.LastName = userDetail.LastName;

                if (!string.IsNullOrEmpty(userDetail.MiddleName))
                {
                    profile.MiddleName = userDetail.MiddleName.Substring(0, 1);
                }

                
                profile.SSN = new SocialSecurityNumber(userDetail.Ssn);
                
                

                if (userDetail.MaritalStatus != null && MaritalStatus.GetMaritalStatus(userDetail.MaritalStatus.MaritalStatusCode.ToString()) != null)
                {
                    profile.MaritalStatus = MaritalStatus.GetMaritalStatus(userDetail.MaritalStatus.MaritalStatusCode.ToString()).Text;
                }
                else
                {
                    profile.MaritalStatus = string.Empty;
                }


                if (Language.GetLanguage(userProfile.LanguageCode) != null)
                {
                    profile.PreferredLanguage = Language.GetLanguage(userProfile.LanguageCode).Text;
                }
                else
                {
                    profile.PreferredLanguage = string.Empty;
                }


                profile.EmailAddress = userDetail.Email;


                if (userProfile.SecurityQuestionAnswer != null)
                {
                    if (userProfile.SecurityQuestionAnswer != null)
                    {
                        profile.SecurityQuestion = userProfile.SecurityQuestionAnswer.Text;
                        profile.SecurityAnswer = userProfile.SecurityQuestionAnswer.SecurityAnswer;
                    }
                }


                profile.CellPhone = userDetail.PhoneCell;
                profile.HomePhone = userDetail.PhoneHome;
                profile.WorkPhone = userDetail.PhoneWork;

                if ((userProfile.Addresses != null) && (userProfile.Addresses.Length > 0))
                {
                    Data.Services.Identity.Address address = userProfile.Addresses[0];
                    profile.MailingAddressLine1 = address.StreetLine1;
                    profile.MailingAddressLine2 = address.StreetLine2;
                    profile.City = address.City;
                    if (State.GetState(address.State) != null)
                    {
                        profile.State = State.GetState(address.State).Text;
                    }
                    else
                    {
                        profile.State = string.Empty;
                    }


                    profile.Zip = address.Zip;
                }
            }



        }
    }
}