﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.Data;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Security;
using MaritalStatus = CredAbility.Data.Services.Identity.MaritalStatus;

namespace CredAbility.Website.Content_Controls
{
    public partial class EditProfile : MyAccountBaseControl
    {
        protected UserProfile userProfile;
        protected UserDetail userDetail;
        long addressID;

        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            Sitecore.Diagnostics.Log.Audit(string.Format("Edit profile page accessed. Session informations: username: {0}, clientId: {1}, session Id: {2}", ClientProfileManager.CurrentClientProfile.Username, ClientProfileManager.CurrentClientProfile.ClientID, HttpContext.Current.Session.SessionID), this);

            CustomResponse<UserProfile> responseUserProfile = IdentityRepository.GetUserProfile(ClientProfile.ClientID);
            userProfile = responseUserProfile.ResponseValue;
            CustomResponse<UserDetail> responseUserDetail = IdentityRepository.GetUserDetail(ClientProfile.ClientID);
            userDetail = responseUserDetail.ResponseValue;

            //TAG USERID VALIDATION
            if (userDetail == null || ClientProfileManager.CurrentClientProfile.ClientID != userDetail.UserId.ToString())
            {
                Manager.Logout();
            }


            this.InitializeLanguage();
            this.InitializeState();
            this.InitializeMaritalStatus();

            if (userProfile != null && userDetail != null)
            {


                this.InitializeAccountInformation();

                this.InitializeAvatar();

                if (!this.Page.IsPostBack)
                {
                    this.InitializeQuestion();
                }

                this.InitializeAddressAndEmail();
            }


        }

        private void InitializeLanguage()
        {
            DDLLanguage.Items.Clear();
            DDLLanguage.Items.Add(new ListItem("", ""));
            foreach (var language in CredAbility.Web.Language.GetAllLanguage())
            {
                var item = new ListItem(language.Text, language.Code);
                DDLLanguage.Items.Add(item);
            }
        }

        private void InitializeState()
        {
            StateDDL.Items.Clear();
            StateDDL.Items.Add(new ListItem("", ""));
            foreach (var state in CredAbility.Web.State.GetAllState())
            {
                var item = new ListItem(state.Text, state.Code);
                StateDDL.Items.Add(item);
            }
        }

        private void InitializeMaritalStatus()
        {
            MaritalStatusDDL.Items.Clear();
            MaritalStatusDDL.Items.Add(new ListItem("", ""));
            foreach (var maritalStatus in CredAbility.Web.MaritalStatus.GetAllMaritalStatus())
            {
                var item = new ListItem(maritalStatus.Text, maritalStatus.Code);
                MaritalStatusDDL.Items.Add(item);
            }
        }


        private void InitializeAccountInformation()
        {
            UsernameLabel.Text = userProfile.Username;
            FirstNameTextBox.Text = userDetail.FirstName;
            LastNameTextBox.Text = userDetail.LastName;


            if (!string.IsNullOrEmpty(userDetail.Ssn) && userDetail.Ssn.Contains("-") && userDetail.Ssn != "--")
            {
                string ssn = userDetail.Ssn.Replace("-", "");
                SSNTextBox1.Text = "***";
                SSNTextBox1.ReadOnly = true;
                SSNTextBox2.Text = "**";
                SSNTextBox2.ReadOnly = true;
                SSNTextBox3.Text = ssn.Substring(5, 4);
                SSNTextBox3.ReadOnly = true;
            }
            else
            {
                SSNTextBox1.Text = string.Empty;
                SSNTextBox1.ReadOnly = false;
                SSNTextBox2.Text = string.Empty;
                SSNTextBox2.ReadOnly = false;
                SSNTextBox3.Text = string.Empty;
                SSNTextBox3.ReadOnly = false;

            }


            if (userDetail.MaritalStatus != null)
            {
                if (MaritalStatusDDL.Items.FindByValue(userDetail.MaritalStatus.MaritalStatusCode.ToString()) != null)
                {
                    MaritalStatusDDL.Items.FindByValue(userDetail.MaritalStatus.MaritalStatusCode.ToString()).Selected = true;
                }
            }

            if (!string.IsNullOrEmpty(userDetail.MiddleName))
            {
                MiddleInitialTextBox.Text = userDetail.MiddleName.Substring(0, 1);
            }


            if ((userProfile.LanguageCode != "") && (DDLLanguage.Items.Count > 0))
            {
                DDLLanguage.Items.FindByValue(userProfile.LanguageCode).Selected = true;
            }
        }

        private void InitializeQuestion()
        {
            if (userProfile.SecurityQuestionAnswer != null)
            {

                if (userProfile.SecurityQuestionAnswer != null)
                {
                    Question1AnswerTextBox.Text = userProfile.SecurityQuestionAnswer.SecurityAnswer;
                }


            }

            this.GetSecurityQuestions();
        }


        private void InitializeAddressAndEmail()
        {
            CellPhoneTextBox.Text = userDetail.PhoneCell;
            HomePhoneTextBox.Text = userDetail.PhoneHome;
            WorkPhoneTextBox.Text = userDetail.PhoneWork;

            if ((userProfile.Addresses != null) && (userProfile.Addresses.Length > 0))
            {
                Data.Services.Identity.Address address = userProfile.Addresses[0];
                addressID = address.AddressId;
                MailingAddressLine1TextBox.Text = address.StreetLine1;
                MailingAddressLine2TextBox.Text = address.StreetLine2;
                CityTextBox.Text = address.City;
                ZipTextBox.Text = address.Zip;
                if ((address.State != "") && (StateDDL.Items.Count > 0) && StateDDL.Items.FindByValue(address.State) != null)
                {
                    StateDDL.Items.FindByValue(address.State).Selected = true;
                }
            }




        }

        public void ImageButtonClick(object sender, ImageClickEventArgs e)
        {
            ActionResult result = Manager.SaveAvatar(((ImageButton)sender).ID);

            if (!result.IsSuccessful)
            {
                ResultMessages.Messages = result.Messages;
            }
            else
            {
                //note : do we really need that redirection?
                Response.Redirect("/my-account/my-profile/edit-profile.aspx");
            }
        }

        private void GetSecurityQuestions()
        {
            var questions = IdentityData.GetAllSecurityQuestions(userProfile.LanguageCode);


            foreach (var question in questions)
            {
                var li = new ListItem(question.Text, question.ID.ToString());
                if (userProfile.SecurityQuestionAnswer != null && li.Value == userProfile.SecurityQuestionAnswer.SecurityQuestionId.ToString())
                {
                    li.Selected = true;
                }

                Question1DDL.Items.Add(li);
            }
        }

        protected void SaveProfileButton_Click(object sender, EventArgs e)
        {

            Profile profile = new Profile();

            profile.ClientID = ClientProfileManager.CurrentClientProfile.ClientID;
            profile.EmailAddress = ClientProfileManager.CurrentClientProfile.EmailAddress;
            profile.DocumentAccessID = ClientProfileManager.CurrentClientProfile.DocumentAccessID;
            profile.DocumentTokenID = ClientProfileManager.CurrentClientProfile.DocumentTokenID;
            profile.DocumentID = ClientProfileManager.CurrentClientProfile.DocumentID;
            profile.DMPAccessID = ClientProfileManager.CurrentClientProfile.DMPAccessID;
            profile.IdentityTokenID = ClientProfileManager.CurrentClientProfile.IdentityTokenID;
            profile.Types = ClientProfileManager.CurrentClientProfile.Types;
            profile.LastLogin = ClientProfileManager.CurrentClientProfile.LastLogin;
            profile.IsProfileUpdateRequired = ClientProfileManager.CurrentClientProfile.IsProfileUpdateRequired;
            profile.ReferringSite = ClientProfileManager.CurrentClientProfile.ReferringSite;
            profile.ClientAccounts = ClientProfileManager.CurrentClientProfile.ClientAccounts;
            profile.Avatar = ClientProfileManager.CurrentClientProfile.Avatar;
            profile.PasswordResetDate = ClientProfileManager.CurrentClientProfile.PasswordResetDate;


            profile.Username = SafeRequestHelper.SanitizeHtml(UsernameLabel.Text);
            profile.FirstName = SafeRequestHelper.SanitizeHtml(FirstNameTextBox.Text);
            profile.LastName = SafeRequestHelper.SanitizeHtml(LastNameTextBox.Text);
            profile.MiddleName = SafeRequestHelper.SanitizeHtml(MiddleInitialTextBox.Text);


            if (!string.IsNullOrEmpty(DDLLanguage.SelectedValue))
            {

                profile.PreferredLanguage = SafeRequestHelper.SanitizeHtml(DDLLanguage.SelectedValue);
            }
            else
            {
                profile.PreferredLanguage = string.Empty;
            }

            if (!string.IsNullOrEmpty(MaritalStatusDDL.SelectedValue))
            {
                MaritalStatus maritalStatus = new MaritalStatus();
                maritalStatus.MaritalStatusCode = MaritalStatusDDL.SelectedValue[0];
                maritalStatus.Text = SafeRequestHelper.SanitizeHtml(MaritalStatusDDL.SelectedItem.Text);
                profile.MaritalStatus = maritalStatus.MaritalStatusCode.ToString();
            }
            //else
            //{
            //    maritalStatus.MaritalStatusCode = ' ';
            //}


            //If it is true then the user cant modify it
            if (!SSNTextBox1.ReadOnly)
            {
                profile.SSN = new SocialSecurityNumber(SafeRequestHelper.SanitizeHtml(SSNTextBox1.Text), SafeRequestHelper.SanitizeHtml(SSNTextBox2.Text), SafeRequestHelper.SanitizeHtml(SSNTextBox3.Text));
            }
            else
            {
                profile.SSN = new SocialSecurityNumber(userDetail.Ssn);

            }
            if (!string.IsNullOrEmpty(Question1DDL.SelectedValue))
            {
                profile.SecurityQuestion = SafeRequestHelper.SanitizeHtml(Question1DDL.SelectedValue);
            }
            profile.SecurityAnswer = SafeRequestHelper.SanitizeHtml(Question1AnswerTextBox.Text);


            profile.CellPhone = SafeRequestHelper.SanitizeHtml(CellPhoneTextBox.Text);
            profile.HomePhone = SafeRequestHelper.SanitizeHtml(HomePhoneTextBox.Text);
            profile.WorkPhone = SafeRequestHelper.SanitizeHtml(WorkPhoneTextBox.Text);
            profile.MailingAddressLine1 = SafeRequestHelper.SanitizeHtml(MailingAddressLine1TextBox.Text);
            profile.MailingAddressLine2 = SafeRequestHelper.SanitizeHtml(MailingAddressLine2TextBox.Text);
            profile.City = SafeRequestHelper.SanitizeHtml(CityTextBox.Text);
            profile.State = SafeRequestHelper.SanitizeHtml(StateDDL.SelectedValue);
            profile.Zip = SafeRequestHelper.SanitizeHtml(ZipTextBox.Text);

            profile.MailingAddressId = addressID;


            ClientProfileManager manager = new ClientProfileManager();
            ActionResult result = new ActionResult(true);

            this.FieldValidator(result);

            if (result.IsSuccessful)
            {

                if (!string.IsNullOrEmpty(this.NewEmailTextBox.Text))
                {
                    profile.EmailAddress = SafeRequestHelper.SanitizeHtml(this.NewEmailTextBox.Text);
                }


                result.MergeActionResult(manager.SaveProfile(profile));

                if (result.IsSuccessful)
                {
                    Response.Redirect("/my-account/my-profile/default.aspx?saved=1");
                }
            }

            this.ResultMessages.Messages = result.Messages;


        }


        private void FieldValidator(ActionResult result)
        {
            result.IsSuccessful = true;



            if (!string.IsNullOrEmpty(this.NewEmailTextBox.Text) && string.IsNullOrEmpty(this.ConfirmNewEmailTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.EditProfile_ConfirmEmailRequired, "CER"));
            }

            if (string.IsNullOrEmpty(this.NewEmailTextBox.Text) && !string.IsNullOrEmpty(this.ConfirmNewEmailTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.EditProfile_NewEmailRequired, "CNER"));
            }

            if (!string.IsNullOrEmpty(this.NewEmailTextBox.Text) && !string.IsNullOrEmpty(this.ConfirmNewEmailTextBox.Text) && this.NewEmailTextBox.Text != this.ConfirmNewEmailTextBox.Text)
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.EditProfile_EmailComparison, "EC"));
            }


        }



        private void InitializeAvatar()
        {
            AvatarImage.ImageUrl = AvatarHelper.GetThumbnailFilePath(ClientProfile.Avatar);

            foreach (var avatarKey in AvatarHelper.GetAvailableAvatarKeys())
            {
                ImageButton imageButton = new ImageButton();
                imageButton.Click += ImageButtonClick;
                imageButton.CssClass = "SelectAvatarModalImage";
                imageButton.ID = avatarKey;
                imageButton.ImageUrl = AvatarHelper.GetThumbnailFilePath(avatarKey);
                AvatarPanel.Controls.Add(imageButton);
            }
        }

    }
}