﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="referencesandresources.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.referencesandresources" %>
<div id="references" class="account">
<%
	if (Document != null)
	{
%>
	<p class="date"><%= Resources.MyAccount.MyCounseling_Summary_AsOfDate%> <%= Document.Date.ToShortDateString()%></p>
	<div class="document">
		<iframe src ="<%= Document.DocumentUrl %>" width="100%" height="500" frameborder="0">
			<p>Your browser does not support iframes.</p>
		</iframe>
	</div>
<%
	}
	else
	{
%>
   <%= Resources.MyAccount.MyCounseling_Summary_DocumentNotAvailable %>
<%
	}
	if (PastSummariesUrl != null)
	{
%>
	<div class="links">
		<p class="link-btn download-summary"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
		<p class="link-btn download-summary"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
</div>