﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="debts.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.debts" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<div id="debt" class="account">
<%
	if(ClientProfileRepository.GetLastVisitedWebsite(ClientProfile) != null && ClientProfileRepository.GetLastVisitedWebsite(ClientProfile).PercentComplete >= 100)
	{
		if(Document != null)
		{
%>
	<p class="date">
		<%= Resources.MyAccount.MyCounseling_Summary_AsOfDate%>
		<%= Document.Date.ToShortDateString()%></p>
	<div class="document">
		<iframe src="<%= Document.DocumentUrl %>" width="100%" height="500" frameborder="0">
			<p>
				Your browser does not support iframes.</p>
		</iframe>
	</div>
<%
		}
		else
		{
%>
	<div style="padding-bottom: 20px;">
		<%= Resources.MyAccount.MyCounseling_Summary_DocumentNotAvailable%>
	</div>
<%
		}
	}
	else
	{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NeverHadCounseling%>
	</div>
<%
	}
	if(PastSummariesUrl != null)
	{
%>
	<div class="links">
		<p class="link-btn download-summary"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
		<p class="link-btn download-summary"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
</div>
