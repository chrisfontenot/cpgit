﻿using System;
using System.Collections.Generic;
using CredAbility.Core.Document;
using CredAbility.Data.Document;
using CredAbility.Web.UI.UserControls;
using StructureMap;
using System.Linq;

namespace CredAbility.Website.Content_Controls.MyCounseling
{
	public partial class MyPastSummaries:MyAccountBaseControl
	{
		protected List<DocumentSummary> Documents { get; set; }

		protected override void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
			List<DocumentSummary> HoldDoc = new List<DocumentSummary>();
			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				var resultlist = Item.GetPastSummaryList(ClientProfile);
				HoldDoc.AddRange(resultlist);
			}
			Documents = HoldDoc.OrderByDescending(CurDoc => CurDoc.Date).ToList();
		}
	}
}