﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreditProfile.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.CreditProfile" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<%@ Import Namespace="CredAbility.Web.UI.ToStringHelpers" %>
<%@ Import Namespace="System.Globalization" %>
<div id="credit-profile">
<%
	if(ClientProfileRepository.GetLastVisitedWebsite(ClientProfile) != null && ClientProfileRepository.GetLastVisitedWebsite(ClientProfile).PercentComplete >= 100)
	{
		if(this.Summary != null)
		{
%>
	<div class="profile-date">
		<%=Resources.MyAccount.CreditProfile_Date%>
		<%=this.Summary.AsOfDate.ToShortDateString() %>
	</div>
	<div id="demo" class="tab">
		<ul class="tab-menu">
			<li>
				<a href="#NetWorth"><h3><%=Resources.MyAccount.CreditProfile_TabTitleNetWorth%></h3></a>
			</li>
			<li>
				<a href="#CreditScore"><h3><%=Resources.MyAccount.CreditProfile_TabTitleCreditScore%></h3></a>
			</li>
		</ul>
		<div>
			<div id="NetWorth" class="validTag">
				<div class="tab-content">
					<div id="NetWorthChart" class="ColumnChart" assets="<%= this.Summary.PersonalAssetsInfo.TotalAssets %>" liabilities="<% =this.Summary.PersonalAssetsInfo.TotalLiabilities %>" style="width: 449px; height: 223px;"></div>
					<%--<img src="images/en/visual/my-net-worth.png" width="449" height="223" class="date"/>--%>
					<table width="390" class="data-table">
						<tr class="assets">
							<td>
								<%=Resources.MyAccount.CreditProfile_TotalAssets%>
							</td>
							<td align="right">
								<span>
									<%=this.Summary.PersonalAssetsInfo.TotalAssets.ConvertToCurrency()%>
								</span>
							</td>
						</tr>
						<tr class="liabilities">
							<td>
								<%=Resources.MyAccount.CreditProfile_TotalLiabilities%>
							</td>
							<td align="right">
								<span>
									<%=(this.Summary.PersonalAssetsInfo.TotalLiabilities * -1).ConvertToCurrency()%></span>
							</td>
						</tr>
						<tr class="net-worth">
							<td align="right" colspan="2">
								<%=Resources.MyAccount.CreditProfile_NetWorth%><span><%=this.Summary.PersonalAssetsInfo.TotalNetWorth.ConvertToCurrency()%></span>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div id="CreditScore">
				<div class="tab-content">
					<h1>
						<%=this.Summary.Demographics.CreditScore%></h1>
					<%=Resources.MyAccount.CreditProfile_CreditScoreText%>
				</div>
			</div>
		</div>
	</div>
<%
		}
		else
		{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NotAvailable%>
	</div>
<%
		}
	}
	else
	{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NeverHadCounseling%>
	</div>
<%
	}
	if(PastSummariesUrl != null)
	{
%>
	<div class="links" style="padding-top: 10px;">
		<p class="link-btn download"><a target="_blank" href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%=Resources.MyAccount.CreditProfile_DownloadFull%></a></p>
		<p class="link-btn download"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
	<script>
var CreditProfileTabInit = true;
var CreditProfileChartInit = true;
var AssetsDisplayName = "<%=Resources.MyAccount.CreditProfile_Assets %>";
var LiabilitiesDisplayName = "<%=Resources.MyAccount.CreditProfile_Liabilities %>";
	</script>
</div>
