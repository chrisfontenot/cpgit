﻿using System;
using System.Collections.Generic;
using CredAbility.Core.Document;
using CredAbility.Data.Document;
using CredAbility.Web.UI.UserControls;
using StructureMap;

namespace CredAbility.Website.Content_Controls.MyCounseling
{
	public partial class certificates:MyAccountBaseControl
	{
		protected List<Certificate> Certificates { get; set; }
		
		protected override void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
			Certificates = new List<Certificate>();
			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				var resultlist = Item.GetCertificates(ClientProfile);
				Certificates.AddRange(resultlist);
			}
		}
	}
}