﻿using System;
using CredAbility.Core.Document;
using CredAbility.Web.UI.UserControls;

namespace CredAbility.Website.Content_Controls.MyCounseling
{
	public partial class debts:MyAccountBaseControl
	{
		protected DocumentSummary Document { get; set; }

		protected override void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
			Document = Manager.GetClientDocuments().GetMyDebtsDocument();
		}
	}
}