﻿using System;
using CredAbility.Data.Identity;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Authentification;
using Sitecore.Links;


namespace CredAbility.Website.Content_Controls
{
    public partial class MyCounselingSummary : CounselingSummaryBaseControl
    {
        protected string ActionPlanUrl { get; set; }
        protected string MyCertificateUrl { get; set; }
        protected string GetDocumentSiteUrl { get; set; }

        protected override void Page_Load(object sender, EventArgs e)
        {
			// the redirect is untill the summary gets hooked back up.
			//Response.Redirect("my-past-summaries.aspx");
            var sitecoreActionPlanUrl = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.ActionPlan.SitecorePath);
            if (sitecoreActionPlanUrl != null)
            {
                ActionPlanUrl = LinkManager.GetItemUrl(sitecoreActionPlanUrl);
            }
			else
            {
                ActionPlanUrl = null;
            }

            var myCertificateUrl = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MyCertificate.SitecorePath);
            if (myCertificateUrl != null)
            {
                MyCertificateUrl = LinkManager.GetItemUrl(myCertificateUrl);
            }
			else
            {
                MyCertificateUrl = null;
            }


            if (string.IsNullOrEmpty(ClientProfile.DocumentTokenID))
            {
                ClientProfileManager.CurrentClientProfile.DocumentTokenID = ClientProfileRepository.GetDocumentTokenId(ClientProfileManager.CurrentClientProfile);
                ClientProfileManager.SaveClientProfile(ClientProfileManager.CurrentClientProfile);
                System.Web.HttpCookie cookie = new System.Web.HttpCookie("PcpSes", "234234324");
                cookie.Secure = true;
                Response.Cookies.Add(cookie);
            }

            if (!string.IsNullOrEmpty(ClientProfileManager.CurrentClientProfile.DocumentTokenID))
            {
                GetDocumentSiteUrl = ConfigReader.ConfigSection.Url.GetDocument.Value.Replace("{0}", ClientProfileManager.CurrentClientProfile.DocumentTokenID);
            }

            base.Page_Load(sender, e);
        }
    }
}