﻿using System;
using System.Linq;
using CredAbility.Core.Client;
using CredAbility.Web.UI.ExportHelper;
using CredAbility.Web.UI.UserControls;

namespace CredAbility.Website.Content_Controls.MyCounseling
{
	public partial class IncomeAndExpenses:CounselingSummaryBaseControl
	{
		public string GetExpenseItemInfo(ExpenseCategory expenseCategory)
		{
			string subCategories = string.Empty;
			foreach(ExpenseItem expenseItem in expenseCategory.ExpenseItems)
			{
				if(expenseCategory.ExpenseItems.Last() == expenseItem)
				{
					subCategories += expenseItem.Name + "|" + expenseItem.Cost;
				}
				else
				{
					subCategories += expenseItem.Name + "|" + expenseItem.Cost + "£";
				}
			}
			return subCategories;
		}

		public void OpenCSV(Object sender, EventArgs args)
		{
			CSVExportHelper.ExportToCSVFile(this.Summary.ExpenseInfo.IncomeAndExpenseRawData, Guid.NewGuid().ToString());
		}
	}
}