﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DMP.ascx.cs" Inherits="CredAbility.Website.Content_Controls.DMP" %>
<%@ Import Namespace="CredAbility.Web.UI.ToStringHelpers" %>

<% if (Manager.GetDMP() != null){ %>
<div class="DMPControl">
    <div class="DMPHeader">
        
            <%if (Manager.GetDMP().DmpStatus.DmpAmountDue != 0)
              { %>
    
        <%=     Resources.MyAccount.DMP_Header + Manager.GetDMP().DmpStatus.TotalDebtAmount.ConvertToCurrency()%>
            <%}
              else
              {
             %>
                <%=     Resources.MyAccount.DMP_Header + "$0.00"%>
             <%} %>
    </div>
    <div style="width: 200px; float: left;">
        <div>
            <%= DMPStatus %>
        </div>
        <div>
            <a href="/~/redirect/mydmp.ashx"><%= Resources.MyAccount.DMP_MyDMPClientAccessSite %></a>
        </div>
    </div>
    <div style="float: left;">
        <span class="submitNextButton"><a href="/~/redirect/mydmp.ashx"></a></span>        
    </div>
</div>
<% } %>