﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using CredAbility.Data.Identity;
using CredAbility.Core.Common;
using CredAbility.Core.DMP;
using CredAbility.Data.DMP;
using CredAbility.Web.UI.ToStringHelpers;
using CredAbility.Web.UI.UserControls;

namespace CredAbility.Website.Content_Controls
{
    public partial class DMP : MyAccountBaseControl
    {
        protected string DMPStatus { get; set; }
        
        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            if ((this.Manager.GetDMP() == null) || (!this.Manager.GetDMP().IsSuccessful))
            {
                return;
            }

            if (this.Manager.GetDMP().DmpStatus.DaysTilNextPaymentDue < 0)
            {
                this.DMPStatus = Resources.MyAccount.DMP_Overdue;
            }
            else if (this.Manager.GetDMP().DmpStatus != null && (this.Manager.GetDMP().DmpStatus.DmpAmountDue == 0 || !this.Manager.GetDMP().DmpStatus.DmpNextDueDate.HasValue || this.Manager.GetDMP().DmpStatus.DaysTilNextPaymentDue == null))
            {
                this.DMPStatus = string.Empty;
            }
            else
            {
                this.DMPStatus = string.Format(Resources.MyAccount.DMP_Due,this.Manager.GetDMP().DmpStatus.DmpAmountDue.ConvertToCurrency(),Math.Abs(this.Manager.GetDMP().DmpStatus.DaysTilNextPaymentDue.Value));
            }
            
        }

    }
}