﻿using System;
using System.Collections.Generic;
using CredAbility.Core;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Validator;

namespace CredAbility.Website.Content_Controls
{
    public partial class HowYouCanHelpContactUs : BaseContactUsControl
    {
        
   protected string LabelFirstName { get { return Resources.Website.ContactUs_FirstName; } }
        protected string LabelLastName { get { return Resources.Website.ContactUs_LastName; } }
        protected string LabelCompany { get { return Resources.Website.ContactUs_Company; } }
        protected string LabelPhone { get { return Resources.Website.ContactUs_Phone; } }
        protected string LabelEmail { get { return Resources.Website.ContactUs_Email; } }
        protected string LabelComment { get { return Resources.Website.ContactUs_Comment; } }
        protected string LabelSubmit { get { return Resources.Website.ContactUs_Submit; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            submit.Text = LabelSubmit;
            
            if (IsPostBack) return;


            ErrorSeeBelow.Visible = false;
        }

        protected void Submit_Click(Object sender, EventArgs e)
        {

            var val = new ContactUsValidator();
            var contactUs = new ContactUs();
            this.AssignValueToContactUs(contactUs);
            var actionResult = val.Validate(contactUs);

            this.Messages = actionResult.Messages;

            if (!actionResult.IsSuccessful)
            {
                ErrorSeeBelow.Visible = true;
            }
            else
            {
                var bodyTagsToReplace = new Dictionary<string, string>
                                        {
                                            {"{FullName}", contactUs.Fullname},
                                            {"{Company}", contactUs.Company},
                                            {"{Phone}", contactUs.Phone},
                                            {"{Email}", contactUs.Email},
                                            {"{Comment}", contactUs.Comment}
                                            
                                        };
                actionResult = SendEmail(contactUs.Fullname, contactUs.Email, bodyTagsToReplace);
                this.ResultMessages.Messages = this.Messages = actionResult.Messages;

            }

           
        }


        private void AssignValueToContactUs(ContactUs contactUs)
        {
            contactUs.Firstname = txtFirstName.Text;
            contactUs.Lastname = txtLastName.Text;
            contactUs.Company = txtCompany.Text;
            contactUs.Phone = txtPhone.Text;
            contactUs.Email = txtEmail.Text;
            contactUs.Comment = txtComment.Text;
        }
        
    }
}