﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Item;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls.CredAbilityU
{
    public partial class Course_Element : System.Web.UI.UserControl
    {

        public CourseItem CourseItem
        {
            get; set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ClientProfileManager.CurrentClientProfile.MustChangePassword)
            {
                var mustChangePassword = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangePassword.SitecorePath);
                WebUtil.Redirect(LinkManager.GetItemUrl(mustChangePassword));
            }else if (ClientProfileManager.CurrentClientProfile.IsProfileUpdateRequired)
            {
                var mustChangeUsername = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.MustChangeUsername.SitecorePath);
                WebUtil.Redirect(LinkManager.GetItemUrl(mustChangeUsername));
            }

        }
    }
}