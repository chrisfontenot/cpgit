﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.SitecoreLib.Data;
using CredAbility.Web.SitecoreLib.Item;
using CredAbility.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls.CredAbilityU
{
    public partial class Course_Category : System.Web.UI.UserControl
    {

        protected CourseItemList Courses
        {
            get; set;

        }
        protected string FilterValue
        {
            get { return Request.QueryString["f"]; }
        }

        protected string SortingValue
        {
            get { return Request.QueryString["s"]; }
        }

        protected Dictionary<object, List<CourseItem>> CoursesByType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var type = GetFiterType();
            if (type != null)
            {
                var tmpCourses =CourseItemRepositoryProxy.GetAllByCategory(new ID(Sitecore.Context.Item.Fields["Linked Category"].Value));
                Courses = tmpCourses.GetByCourseType(type);
            }
            else
            {
                Courses = CourseItemRepositoryProxy.GetAllByCategory(new ID(Sitecore.Context.Item.Fields["Linked Category"].Value));
            }
            
            
            if (SortingValue == CourseSortings.SORT_MOST_RECENT)
            {
                this.Courses.SortBy(CourseItemList.OrderBy.MostRecent);
            }
            else  if (SortingValue == CourseSortings.SORT_MOST_POPULAR)
            {
                this.Courses.SortBy(CourseItemList.OrderBy.MostPopular);
            }
            else
            {
                this.Courses.SortBy(CourseItemList.OrderBy.Alphabetical);
            }

        }

        protected Type GetFiterType()
        {
            Type courseType = null;

            switch (FilterValue)
            {
                case CourseFilters.FILTER_ARTICLES:
                    return typeof(ArticleItem);
                    break;
                case CourseFilters.FILTER_CLASSONDEMAND:
                    courseType = typeof(ClassOnDemandItem);
                    
                    break;
                case CourseFilters.FILTER_WEBINARS:
                    courseType = typeof(WebinarItem);
                    break;
                case CourseFilters.FILTER_PODCASTS:
                    courseType = typeof(PodcastItem);
                    break;
                case CourseFilters.FILTER_ONLINEVIDEOS:
                    courseType = typeof(OnlineVideoItem);
                    break;
                case CourseFilters.FILTER_PAIDOFFERINGS:
                    courseType = typeof(PaidOfferingItem);
                    break;
            }
            return courseType;

        }


    }
}