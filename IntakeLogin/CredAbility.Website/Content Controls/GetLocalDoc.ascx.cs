﻿using System;
using CredAbility.Web.Authentification;
using CredAbility.Data.Document;
using CredAbility.Website.layouts;

namespace CredAbility.Website.Content_Controls
{
	public partial class GetLocalDoc:User_Control
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			var docId = Request.QueryString["id"];
			CredAbility.Core.Client.ClientProfile CurProfile = ClientProfileManager.CurrentClientProfile;
			if(CurProfile != null)
			{
				CredAbilityRepository DocRep = new CredAbilityRepository();
				Response.Expires = 0;
				Response.Buffer = true;
				Response.ClearHeaders();
				Response.Clear();
				Response.AddHeader("content-disposition", String.Format("filename=Document-{0}.pdf", docId));
				Response.ContentType = "application/pdf";
				Response.BinaryWrite(DocRep.DisplayDocument(CurProfile, docId));
				Response.Flush();
				Response.End();
			}
		}
	}
}