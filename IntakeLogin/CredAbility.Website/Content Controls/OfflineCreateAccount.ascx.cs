﻿using System;
using CredAbility.Core.Common;
using CredAbility.Core.Identity;
using CredAbility.Web.Identity;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Security;


namespace CredAbility.Website.Content_Controls
{
    public partial class OfflineCreateAccount : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorSeeBelow.Visible = false;
        }


        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            var manager = new IdentityManager();

            OfflineProfileIdentity opi = new OfflineProfileIdentity()
                {
                    ClientId = SafeRequestHelper.SanitizeHtml(this.ClientIDTextBox.Text),
                    SocialSecurityNumber = SafeRequestHelper.SanitizeHtml(this.SSNTextBox1.Text) + SafeRequestHelper.SanitizeHtml(this.SSNTextBox2.Text) + SafeRequestHelper.SanitizeHtml(this.SSNTextBox3.Text)
                };


            ActionResult result = manager.IdentifyOfflineProfile(opi);


            // display error messages
            if (!result.IsSuccessful && !result.Messages.HasMessage(string.Empty) && !result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = true;
            }
            else if (result.Messages.HasMessage(string.Empty) || result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = false;
                this.ResultMessages.Messages = result.Messages;
            }
            this.Messages = result.Messages;


            
        }
    }
}