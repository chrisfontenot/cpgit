﻿using System;
using Sitecore.Data.Items;
using CredAbility.Data.Rvm;

namespace CredAbility.Website.Content_Controls
{
	public partial class RvmBlock : System.Web.UI.UserControl
	{
		protected string TheText = String.Empty;
		protected string TheSubHead = String.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			StateNames HoldStates = RvmRepository.RvmBlockedStatesGet();
			Item PageItem = Sitecore.Context.Database.GetItem("/sitecore/content/Home/get-started/reverse-mortgage-license");
			TheSubHead = String.Format(PageItem["Subhead"], HoldStates.And);
			TheText = String.Format(PageItem["Content"], HoldStates.And, HoldStates.Or);
		}
	}
}