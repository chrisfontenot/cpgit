﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VerifyProfile.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.VerifyProfile" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>   
   
   
   
<div id="create-account" class="form-block">
    <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
    <ca:ResultMessages ID="ResultMessages" runat="server"></ca:ResultMessages>
    
    <h2><%= Resources.MyAccount.Profile_AccountInformation%></h2>
    <div class="fields-block">
    <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"USERNAME-REQUIRED","USERNAME-UNAVAILABLE"}, "field-block-error") %>">
        <asp:Label ID="Label1" runat="server" Text="<%$Resources:MyAccount, Profile_Username%>" AssociatedControlID="UsernameTextBox"></asp:Label>
        
        <div class="field-element">
            <asp:TextBox ID="UsernameTextBox" runat="server" Columns="40"></asp:TextBox>
            <div class="field-message"><%= GetMessage(new[] { "USERNAME-REQUIRED", "USERNAME-UNAVAILABLE" }, Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block <%= GetCssClassIfMessageCodeExist("FIRSTNAME-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label2" runat="server" Text="<%$Resources:MyAccount, Profile_FirstName%>" AssociatedControlID="FirstNameTextBox"></asp:Label>
        
        <div class="field-element">
            <asp:TextBox ID="FirstNameTextBox" runat="server" Columns="40"></asp:TextBox>
            <div class="field-message"><%= GetMessage("FIRSTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block">
        <asp:Label ID="Label3" runat="server" Text="<%$Resources:MyAccount, Profile_MiddleInitial%>" AssociatedControlID="MiddleInitialTextBox"></asp:Label>
        
        <div class="field-element">
            <asp:TextBox ID="MiddleInitialTextBox" runat="server" Columns="40"></asp:TextBox>
            <div class="field-message"></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block <%= GetCssClassIfMessageCodeExist("LASTNAME-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label4" runat="server" Text="<%$Resources:MyAccount, Profile_LastName%>" AssociatedControlID="LastNameTextBox"></asp:Label>
        
        <div class="field-element">
            <asp:TextBox ID="LastNameTextBox" runat="server" Columns="40"></asp:TextBox>
            <div class="field-message"><%= GetMessage("LASTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"SSN-REQUIRED","SSN-WRONGFORMAT"}, "field-block-error") %>">
        <label><%=Resources.MyAccount.Profile_SSN %></label>
                
        <div class="field-element">
            <strong><%= this._Profile.SSN.FormatedSSN %></strong>
        

            <div class="field-message"></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block">
        <asp:Label ID="Label6" runat="server" Text="<%$Resources:MyAccount, Profile_MaritalStatus%>" AssociatedControlID="MaritalStatusDDL"></asp:Label>
        
        <div class="field-element">
            <asp:DropDownList ID="MaritalStatusDDL" runat="server"></asp:DropDownList>
            <div class="field-message"></div>
        </div>
        <div class="clearboth"></div>
    </div>
    <div class="field-block">
        <asp:Label ID="Label7" runat="server" Text="<%$Resources:MyAccount, Profile_LanguagePreference%>" AssociatedControlID="DDLLanguage"></asp:Label>
        
        <div class="field-element">
             <asp:DropDownList ID="DDLLanguage" runat="server">
                    </asp:DropDownList>
            <div class="field-message"></div>
        </div>
        <div class="clearboth"></div>
    </div>
    </div>
   
    <h2><%= Resources.MyAccount.Profile_EmailAndPassword%></h2>
    <div class="fields-block">
        <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, "field-block-error") %>">
            <asp:Label ID="Label8" runat="server" Text="<%$Resources:MyAccount, Profile_EmailAddress%>" AssociatedControlID="NewEmailTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="NewEmailTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage(new[] { "EMAIL-REQUIRED", "EMAIL-WRONGFORMAT" }, Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
    
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"PASSWORD-REQUIRED","PASSWORD-WRONGFORMAT"}, "field-block-error") %>">
            <asp:Label ID="Label10" runat="server" Text="<%$Resources:MyAccount, Profile_NewPassword%>" AssociatedControlID="NewPasswordTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="NewPasswordTextBox" TextMode="Password" runat="server" Columns="40"></asp:TextBox>
                
                 <a href="#" id="show1" class="CreateAccountTextLink" title="<%= Resources.MyAccount.Global_PasswordRules %>"><img src="/static/images/layout/ico-questionmark.gif"/></a> 
            
           <div class="passwordRules" id="myPanel" style="visiblity:hidden;display:none">
                <div id="panel1">
                    <div class="hd"><%= Resources.MyAccount.Global_PasswordRules %></div>
                    <div class="bd">
                        <iframe src="/global/password-rules.aspx" frameborder="0"></iframe>
                    </div>
                </div>

            </div>
                <div class="field-message"><%= GetMessage(new[] { "PASSWORD-REQUIRED", "PASSWORD-WRONGFORMAT" }, Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"CONFIRMPWD-REQUIRED","CONFIRMPWD-NOMATCH"}, "field-block-error") %>">
            <asp:Label ID="Label11" runat="server" Text="<%$Resources:MyAccount, Profile_ConfirmPassword%>" AssociatedControlID="ConfirmPasswordTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="ConfirmPasswordTextBox" runat="server"  TextMode="Password" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage(new[] { "CONFIRMPWD-REQUIRED", "CONFIRMPWD-NOMATCH" }, Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"QUESTION1-REQUIRED"}, "field-block-error") %>">
            <asp:Label ID="Label12" runat="server" Text="<%$Resources:MyAccount, Profile_EditQuestion1%>" AssociatedControlID="Question1DDL"></asp:Label>
            
            <div class="field-element">
                <asp:DropDownList ID="Question1DDL" runat="server"></asp:DropDownList>
                <div class="field-message"><%= GetMessage(new[] { "QUESTION1-REQUIRED"}, Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist("ANSWER1-REQUIRED", "field-block-error") %>">
            <asp:Label ID="Label13" runat="server" Text="<%$Resources:MyAccount, Profile_EditQuestion1%>" AssociatedControlID="Question1AnswerTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="Question1AnswerTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage("ANSWER1-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
       
        
        
    </div>
        
        
    
    <h2><%=Resources.MyAccount.Profile_MailingAddressAndPhone%></h2>
    <div class="fields-block">
        <div class="field-block <%= GetCssClassIfMessageCodeExist("HOMEPHONE-REQUIRED", "field-block-error") %>">
            <asp:Label  runat="server" Text="<%$Resources:MyAccount, Profile_HomePhone%>" AssociatedControlID="HomePhoneTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="HomePhoneTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage("HOMEPHONE-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        <div class="field-block">
            <asp:Label runat="server" Text="<%$Resources:MyAccount, Profile_WorkPhone%>" AssociatedControlID="WorkPhoneTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="WorkPhoneTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"></div>
            </div>
            <div class="clearboth"></div>
        </div>
        <div class="field-block">
            <asp:Label runat="server" Text="<%$Resources:MyAccount, Profile_CellPhone%>" AssociatedControlID="CellPhoneTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="CellPhoneTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"></div>
            </div>
            <div class="clearboth"></div>
        </div>
        <div class="field-block <%= GetCssClassIfMessageCodeExist("ADDRESS-REQUIRED", "field-block-error") %>">
            <asp:Label ID="Label14" runat="server" Text="<%$Resources:MyAccount, Profile_MailingAddress%>" AssociatedControlID="MailingAddressLine1TextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="MailingAddressLine1TextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage("ADDRESS-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
         <div class="field-block ">
            <asp:Label ID="Label15" runat="server" Text="<%$Resources:MyAccount, Profile_MailingAddressLine2%>" AssociatedControlID="MailingAddressLine2TextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="MailingAddressLine2TextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
         <div class="field-block <%= GetCssClassIfMessageCodeExist("CITY-REQUIRED", "field-block-error") %>">
            <asp:Label ID="Label18" runat="server" Text="<%$Resources:MyAccount, Profile_City%>" AssociatedControlID="CityTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="CityTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage("CITY-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist("STATE-REQUIRED", "field-block-error") %>">
            <asp:Label ID="Label19" runat="server" Text="<%$Resources:MyAccount, Profile_State%>" AssociatedControlID="StateDDL"></asp:Label>
            
            <div class="field-element">
                <asp:DropDownList ID="StateDDL" runat="server"></asp:DropDownList>
                <div class="field-message"><%= GetMessage("STATE-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist("ZIP-REQUIRED", "field-block-error") %>">
            <asp:Label ID="Label20" runat="server" Text="<%$Resources:MyAccount, Profile_Zip%>" AssociatedControlID="ZipTextBox"></asp:Label>
            
            <div class="field-element">
                <asp:TextBox ID="ZipTextBox" runat="server" Columns="40"></asp:TextBox>
                <div class="field-message"><%= GetMessage("ZIP-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
            </div>
            <div class="clearboth"></div>
        </div>
        
    </div>
    
    <div class="form-action">
        <div class="submitNextButton"><asp:LinkButton ID="LinkButton2" OnClick="SaveProfileButton_Click" runat="server"><%=Resources.Website.OfflineProfile_CreateAccountButton%></asp:LinkButton></div>
    </div>
    
</div>


<script>
var PasswordRuleInit = true;
</script>