﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.CreditScoreChallenge;
using CredAbility.Data.CreditScoreChallenge;
using CredAbility.Web.Authentification;

namespace CredAbility.Website.Content_Controls.CreditScoreChallenge
{
    public partial class ChallengeActivity : System.Web.UI.UserControl
    {
        public ChallengeActivityViewData ViewData;
         

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewData = new ChallengeActivityViewData();

            ViewData.ChallengeHistoryItems = ChallengeHistoryItemRepository.GetChallengeHistoryItemById(ClientProfileManager.CurrentClientProfile.ClientID);

            if (ViewData.ChallengeHistoryItems.Count > 0)
            {
                ViewData.LatestItem = ViewData.ChallengeHistoryItems.Last();
            }
        }
    }


    public class ChallengeActivityViewData
    {
        public ChallengeHistoryItem LatestItem { get; set;}
        public List<ChallengeHistoryItem> ChallengeHistoryItems{ get; set;}

    }
}