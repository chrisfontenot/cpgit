﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChallengeActivity.ascx.cs" Inherits="CredAbility.Website.Content_Controls.CreditScoreChallenge.ChallengeActivity" %>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="CredAbility.Core.CreditScoreChallenge"%>
<%@ Import Namespace="CredAbility.Data.CreditScoreChallenge"%>
<%@ Import Namespace="CredAbility.Web.Authentification"%>
<%@ Import Namespace="CredAbility.Web.CreditScoreChallenge"%>

<div style="margin-top: 30px; ">


<%if (this.ViewData.LatestItem != null)
  { %>


<div id="NetWorthChart"  class="ColumnChart" style=" float:left; width: 450px; height: 230px;"></div>
<div style="float:left; width: 250px; margin-left: 20px;">
    <div>
        <span><%=Resources.MyAccount.Csc_MyCurrentScore %></span>
        <span style="float:left; width: 90px; color: #578600; font-size:2.2em; display:block; font-weight:bold;"><%=this.ViewData.LatestItem.Score %></span>
        <div style="float:left; width: 100px; padding-left:15px; border-left: 1px solid #CCCCCC">
        
        <span style="color: #578600; font-size:1.4em; font-weight:bold; display:block;"><%=this.ViewData.LatestItem.PourcentageImprovement %>%</span> <span style="font-size:0.875em">
        <%
        if (this.ViewData.LatestItem.ImprovmentDirection == ImprovmentDirection.Increase){
            Response.Write(Resources.MyAccount.Csc_Increase);
        }
        else
        {
            Response.Write(Resources.MyAccount.Csc_Decrease);
        }
%>
        
        
        </span>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div style="margin-top: 20px;">
        <span style="display:block"><%=Resources.MyAccount.Csc_MyCurrentRank %></span>
        <span style="color: #578600; font-size:1.4em; font-weight:bold;"><%=this.ViewData.LatestItem.Rank %></span> <span> <%=Resources.MyAccount.Csc_RankOutOf %> 5000</span>
    </div>
</div>

          
        
 <div class="clearboth"></div>


 <script>

     var CscChartInit = true;

     var datasource = [];
     <%
    List<string> datasource = new List<string>(); 
    
    foreach(ChallengeHistoryItem item in  ViewData.ChallengeHistoryItems)
    {
        datasource.Add(string.Format("{{score:'{0}', rank:'{1}', pourcentage:'{2}',date: '{3}', direction:'{4}'}}", item.Score, item.Rank,item.PourcentageImprovement,item.Date.ToString("d"),item.ImprovmentDirection));
        
    }

 %>
 datasource= [<%= string.Join(",", datasource.ToArray()) %>];
 
 
 var tooltipsTemplate = "<%=Resources.MyAccount.Csc_TooltipTemplate %>";
 var direction = ["<%=Resources.MyAccount.Csc_Increase %>", "<%=Resources.MyAccount.Csc_Decrease %>"];
 
</script>

<%} else { %>

<%= Resources.MyAccount.CscNoHistoryToDisplay %>

<%} %>

 </div>