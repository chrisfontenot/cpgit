﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.CreditScoreChallenge;
using CredAbility.Data.CreditScoreChallenge;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.CreditScoreChallenge;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Validator;

namespace CredAbility.Website.Content_Controls.CreditScoreChallenge
{
    public partial class AuthorizationForm : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorSeeBelow.Visible = false;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            var validator = new CreditScoreChallengeAuthorizationValidator();

            var contestant = new Contestant(ClientProfileManager.CurrentClientProfile.ClientID);
            this.AssignValueToObject(contestant);

            var actionResult = validator.Validate(contestant);


            this.CheckIAgree(actionResult);


            this.Messages = actionResult.Messages;

            if (!actionResult.IsSuccessful)
            {
                ErrorSeeBelow.Visible = true;
            }
            else
            {
                ErrorSeeBelow.Visible = false;

                try
                {
                    CreditScoreChallengeManager.Current.AuthorizeContestant(contestant);
                }
                catch(Exception)
                {
                    Sitecore.Diagnostics.Log.Audit("CreditScoreChallenge - duplicate authorization",this);
                }

                HttpContext.Current.Response.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value);
            }
        }

        private void CheckIAgree(ActionResult result)
        {
            if(!IAgree.Checked)
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.Website.CSCAuthentification_IAgreeRequired, "IAGREE-REQUIRED"));
            }
        }

        private void AssignValueToObject(Contestant contestant)
        {
            contestant.Fullname = string.Format("{0} {1}", ClientProfileManager.CurrentClientProfile.FirstName, ClientProfileManager.CurrentClientProfile.LastName);
            contestant.PrimaryPhoneNumber = this.PrimaryPhoneNumber.Text;
            contestant.SecondaryPhoneNumber = this.SecondaryPhoneNumber.Text;
            contestant.SSN = new SocialSecurityNumber(this.SSNTextBox1.Text, this.SSNTextBox2.Text, this.SSNTextBox3.Text);
            contestant.State = State.Text;
            contestant.Zip = ZipCode.Text;
            contestant.StreetAddress = StreetAddress.Text;
            contestant.City = City.Text;
            contestant.PreferredLanguage = ClientProfileManager.CurrentClientProfile.PreferredLanguage;
        }
    }
}