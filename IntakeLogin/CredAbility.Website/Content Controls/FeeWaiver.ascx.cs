﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using CredAbility.Web.Authentification;
using Sitecore.Security.Accounts;

namespace CredAbility.Website.Content_Controls
{
    public partial class FeeWaiver : System.Web.UI.UserControl
    {
        private bool? _CheckFeeWaiver;

        public bool CheckFeeWaiver
        {

            get
            {
                if (this._CheckFeeWaiver == null)
                {


                    if (Sitecore.Context.User.IsInRole(Role.FromName("extranet\\BankruptcyPreFilingCounseling")) ||
                        Sitecore.Context.User.IsInRole(Role.FromName("extranet\\BankruptcyEducation")))
                    {
                        this._CheckFeeWaiver = true;
                    }
                    else
                    {
                        this._CheckFeeWaiver = false;
                    }

                }

                return this._CheckFeeWaiver.Value;

            }
        }
        protected ClientWebsiteStatus LastVisitedWebsiteInfo { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            LastVisitedWebsiteInfo = ClientProfileRepository.GetLastIncompleteWebsite(ClientProfileManager.CurrentClientProfile);

            if ((LastVisitedWebsiteInfo != null) && (string.IsNullOrEmpty(LastVisitedWebsiteInfo.Url)))
            {
                LastVisitedWebsiteInfo.Url = LosCodes.GetLosUrlWithSso(LastVisitedWebsiteInfo.WebsiteCode, ClientProfileManager.CurrentClientProfile.ClientID, ClientProfileManager.CurrentClientProfile.IdentityTokenID);
            }

        }

        protected string GetFreeWaiverText()
        {
            switch (InfoRepository.GetFeeWaiverStatus(ClientProfileManager.CurrentClientProfile))
            {
                case FeeWaiverStatus.Approved:

                    return string.Format(Resources.MyAccount.MyRecentActivity_LastVisitFeeWaiverApproved,
                                         LastVisitedWebsiteInfo.PercentComplete, LastVisitedWebsiteInfo.WebsiteText);
                case FeeWaiverStatus.Denied:

                    return string.Format(Resources.MyAccount.MyRecentActivity_LastVisitFeeWaiverDenied,
                                         LastVisitedWebsiteInfo.PercentComplete, LastVisitedWebsiteInfo.WebsiteText);


                default:
                    return string.Format(Resources.MyAccount.MyRecentActivity_LastVisit,
                                         LastVisitedWebsiteInfo.PercentComplete, LastVisitedWebsiteInfo.WebsiteText);
            }
        }
    }
}