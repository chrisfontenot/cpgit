﻿using System;
using System.Collections.Generic;
using CredAbility.Web.API.Search;
using CredAbility.Web.Configuration;
using Sitecore.Links;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls
{
    public partial class Search : System.Web.UI.UserControl
    {
		public bool ShowNext { get; set; }
        protected string SearchArgument { get; set; }
        protected string SearchCategory { get; set; }
		protected int CurrentPage { get; set; }
		protected int ResultsCount { get; set; }
		protected int MaxPage { get; set; }
		private const int ResultsByPage = 10;

        private List<SearchResult> _results;
		protected List<SearchResult> Results
		{
			get { return _results; }
            set
            {
				var startIndex = (CurrentPage - 1) * ResultsByPage;
                var endIndex = value.Count >= startIndex + ResultsByPage ? ResultsByPage : value.Count % ResultsByPage;
                
				if(endIndex > 0)
                {
                    _results = value.GetRange(startIndex, endIndex);
				}
				else
                {
                    _results = new List<SearchResult>();
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SearchArgument = Request.QueryString["q"];
            SearchCategory = Request.QueryString["category"];

			if(SearchArgument != null && SearchCategory != null)
            {
                int currentPage;
                int.TryParse(Request.QueryString["p"], out currentPage);
                CurrentPage = currentPage < 1 ? 1 : currentPage;

                var searcher = new WebsiteSearch();
                var results = searcher.FullTextSearch(SearchArgument, SearchCategory);

                ResultsCount = results.Count;
				MaxPage = (ResultsCount / ResultsByPage) + ((ResultsCount % ResultsByPage) > 0 ? 1 : 0);
                Results = results;
			}
			else
            {
                var homePage = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.SitecoreHome.SitecorePath);
                WebUtil.Redirect(LinkManager.GetItemUrl(homePage));
            }
        }

        protected string GetUrlForPage(int pageNumber)
        {
            var url = "";
            var queryString = "";
            queryString += "?q=" + SearchArgument;
            queryString += "&category=" + SearchCategory;
            queryString += "&p=" + pageNumber;

            url += "/search.aspx";
            url += queryString;
            return url;
        }
    }
}