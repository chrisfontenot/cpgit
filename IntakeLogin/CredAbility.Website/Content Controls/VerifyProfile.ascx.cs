﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Validator;
using CredAbility.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.Data;
using CredAbility.Web.Identity;
using CredAbility.Web.UI.UserControls;
using Sitecore.Links;
using Sitecore.Web;
using MaritalStatus = CredAbility.Data.Services.Identity.MaritalStatus;

namespace CredAbility.Website.Content_Controls
{
    public partial class VerifyProfile : FormBaseControl
    {

        protected Profile _Profile;

        protected void Page_Load(object sender, EventArgs e)
        {

            this._Profile = OfflineProfileContext.GetProfile();

            if(this._Profile == null)
            {
                Redirection.ToOfflineProfileIdentification();
            }

            if(!Page.IsPostBack)
            {
                this.InitializeLanguage();
                this.InitializeState();
                this.InitializeMaritalStatus();

                this.InitializeAccountInformation();

                this.InitializeQuestion();

                this.InitializeAddressAndEmail();
            }

            this.InitReadonlyFields();
            
            ErrorSeeBelow.Visible = false;
        }

        private void InitReadonlyFields()
        {
            if(OfflineProfileContext.IsAccountCreated)
            {
                UsernameTextBox.ReadOnly = true;
                UsernameTextBox.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#ccc");
            }
        }

        #region Dropdown Initialization

        private void InitializeLanguage()
        {
            DDLLanguage.Items.Clear();
            foreach (var language in CredAbility.Web.Language.GetAllLanguage())
            {
                var item = new ListItem(language.Text, language.Code);

                if (language.Code.ToUpper() == Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToUpper())
                {
                    item.Selected = true;
                }
                DDLLanguage.Items.Add(item);
            }
        }

        private void InitializeState()
        {
            StateDDL.Items.Clear();
            StateDDL.Items.Add(new ListItem("", ""));
            foreach (var state in CredAbility.Web.State.GetAllState())
            {
                var item = new ListItem(state.Text, state.Code);
                StateDDL.Items.Add(item);
            }
        }

        private void InitializeMaritalStatus()
        {
            MaritalStatusDDL.Items.Clear();
            MaritalStatusDDL.Items.Add(new ListItem("", ""));
            foreach (var maritalStatus in CredAbility.Web.MaritalStatus.GetAllMaritalStatus())
            {
                var item = new ListItem(maritalStatus.Text, maritalStatus.Code);
                MaritalStatusDDL.Items.Add(item);
            }
        }

        #endregion

        #region Data Initialization
        private void InitializeAccountInformation()
        {
            UsernameTextBox.Text = this._Profile.Username;
            FirstNameTextBox.Text = this._Profile.FirstName;
            LastNameTextBox.Text = this._Profile.LastName;

            if (this._Profile.MaritalStatus != null)
            {
                if (MaritalStatusDDL.Items.FindByValue(this._Profile.MaritalStatus) != null)
                {
                    MaritalStatusDDL.Items.FindByValue(this._Profile.MaritalStatus).Selected = true;
                }
            }

            if (!string.IsNullOrEmpty(this._Profile.MiddleName))
            {
                MiddleInitialTextBox.Text = this._Profile.MiddleName.Substring(0, 1);
            }


            if (!string.IsNullOrEmpty(this._Profile.PreferredLanguage) && (DDLLanguage.Items.Count > 0))
            {
                DDLLanguage.Items.FindByValue(this._Profile.PreferredLanguage).Selected = true;
            }
        }

        private void InitializeAddressAndEmail()
        {
            NewEmailTextBox.Text = this._Profile.EmailAddress;
            CellPhoneTextBox.Text = this._Profile.CellPhone;
            HomePhoneTextBox.Text = this._Profile.HomePhone;
            WorkPhoneTextBox.Text = this._Profile.WorkPhone;
            MailingAddressLine1TextBox.Text = this._Profile.MailingAddressLine1;
            MailingAddressLine2TextBox.Text = this._Profile.MailingAddressLine2;
            CityTextBox.Text = this._Profile.City;
            ZipTextBox.Text = this._Profile.Zip;

            if (StateDDL.Items.FindByValue(this._Profile.State) != null)
            {
                StateDDL.Items.FindByValue(this._Profile.State).Selected = true;
            }

        }
        #endregion

        

        private void InitializeQuestion()
        {

            Question1AnswerTextBox.Text = this._Profile.SecurityAnswer;

           

            this.GetSecurityQuestions();
        }

        private void GetSecurityQuestions()
        {
            var questions = IdentityData.GetAllSecurityQuestions(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName);
            bool hasSelectedValue = false;
            foreach (var question in questions)
            {

                var li = new ListItem(question.Text, question.ID.ToString());
                if (!string.IsNullOrEmpty(this._Profile.SecurityQuestion) && li.Value == this._Profile.SecurityQuestion)
                {
                    li.Selected = hasSelectedValue = true;
                }

                Question1DDL.Items.Add(li);
            }

            if (!hasSelectedValue)
            {
                Question1DDL.Items.Insert(0, new ListItem(Resources.MyAccount.CreateAccount_SelectOne, ""));
            }

            
        }

        protected void SaveProfileButton_Click(object sender, EventArgs e)
        {
            ActionResult result = new ActionResult(true);
            AccountManager accountManager = new AccountManager();

            this.AssignValueToObject();

            result.MergeActionResult(accountManager.CreateAccountFromOfflineProfile(this._Profile, this.ConfirmPasswordTextBox.Text));


            // display error messages
            if (!result.IsSuccessful && !result.Messages.HasMessage(string.Empty) && !result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = true;
            }
            else if (result.Messages.HasMessage(string.Empty) || result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = false;
                this.ResultMessages.Messages = result.Messages;
            }
            this.Messages = result.Messages;
            this.InitReadonlyFields();

        }

        private void AssignValueToObject()
        {
            this._Profile.Username = UsernameTextBox.Text;
            this._Profile.FirstName = FirstNameTextBox.Text;
            this._Profile.LastName = LastNameTextBox.Text;
            this._Profile.MiddleName = MiddleInitialTextBox.Text;
            this._Profile.Password = this.ConfirmPasswordTextBox.Text;
            this._Profile.EmailAddress = this.NewEmailTextBox.Text;

            if (!string.IsNullOrEmpty(DDLLanguage.SelectedValue))
            {

                this._Profile.PreferredLanguage = DDLLanguage.SelectedValue;
            }
            else
            {
                this._Profile.PreferredLanguage = string.Empty;
            }

            if (!string.IsNullOrEmpty(MaritalStatusDDL.SelectedValue))
            {
                MaritalStatus maritalStatus = new MaritalStatus();
                maritalStatus.MaritalStatusCode = MaritalStatusDDL.SelectedValue[0];
                maritalStatus.Text = MaritalStatusDDL.SelectedItem.Text;
                this._Profile.MaritalStatus = maritalStatus.MaritalStatusCode.ToString();
            }


            
            if (!string.IsNullOrEmpty(Question1DDL.SelectedValue))
            {
                this._Profile.SecurityQuestion = Question1DDL.SelectedValue;
            }
            this._Profile.SecurityAnswer = Question1AnswerTextBox.Text;

            this._Profile.CellPhone = CellPhoneTextBox.Text;
            this._Profile.HomePhone = HomePhoneTextBox.Text;
            this._Profile.WorkPhone = WorkPhoneTextBox.Text;
            this._Profile.MailingAddressLine1 = MailingAddressLine1TextBox.Text;
            this._Profile.MailingAddressLine2 = MailingAddressLine2TextBox.Text;
            this._Profile.City = CityTextBox.Text;
            this._Profile.State = StateDDL.SelectedValue;
            this._Profile.Zip = ZipTextBox.Text;
        }


    }
}