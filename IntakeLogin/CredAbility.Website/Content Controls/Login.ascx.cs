﻿using System;
using System.Web;
using CredAbility.Core.Common;
using CredAbility.Web;
using CredAbility.Web.API.Processors;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.UI.WebControls;
using CredAbility.Web.Security;
using Sitecore.Web;


namespace CredAbility.Website.Content_Controls
{
    public partial class Login : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Sitecore.Context.User.IsAuthenticated && ClientProfileManager.CurrentClientProfile != null)
            {
                Redirection.ToMyRecentActivity();
            }

            if (!Page.IsPostBack)
            {
                this.Username.Text = AuthentificationContext.Current.RememberMe.Username;
                this.RememberUsername.Checked = AuthentificationContext.Current.RememberMe.IsActive;
            }

            ErrorSeeBelow.Visible = false;
        }



        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            var clientProfileManager = new ClientProfileManager();
            var actionResult = clientProfileManager.Login(SafeRequestHelper.SanitizeHtml(this.Username.Text), SafeRequestHelper.SanitizeHtml(this.Password.Text), RememberUsername.Checked, true, false);

            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty))
            {
                ErrorSeeBelow.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty))
            {
                ErrorSeeBelow.Visible = false;
                this.ResultMessages.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;
            
            
        }
    }
}