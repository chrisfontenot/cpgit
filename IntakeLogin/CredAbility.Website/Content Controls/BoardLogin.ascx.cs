﻿using System;
using System.Web;
using CredAbility.Core.Common;
using CredAbility.Web;
using CredAbility.Web.API.Processors;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.UI.WebControls;
using CredAbility.Web.Security;
using Sitecore.Web;


namespace CredAbility.Website.Content_Controls
{
    public partial class BoardLogin : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ResultMessages.Messages = new ResultMessageList();
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            // only allow a user who has the extranet\BoardMember role to login
            if (!string.IsNullOrEmpty(this.Username.Text) && !string.IsNullOrEmpty(this.Password.Text))
            {
                if (Sitecore.Security.Authentication.AuthenticationManager.Login("extranet\\" + SafeRequestHelper.SanitizeHtml(this.Username.Text), SafeRequestHelper.SanitizeHtml(this.Password.Text)))
                {
                    if (Sitecore.Context.User.IsInRole("extranet\\BoardMember"))
                    {
                        Response.Redirect("/board/default.aspx");
                    }
                    else
                    {
                        Sitecore.Context.Logout();
                        ResultMessages.Messages.Add(new ResultMessage("You do not have sufficient permissions.", "FAIL"));
                    }
                }
                else
                {
                    ResultMessages.Messages.Add(new ResultMessage("Login failed.", "FAIL"));
                }
            }
            else
            {
                ResultMessages.Messages.Add(new ResultMessage("Username or Password cannot be blank.", "FAIL"));
            }
        }
    }
}