﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotUsername.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.ForgotUsername" %>
    <%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
    
    <asp:Panel ID="GetEmailPanel" runat="server">
    
        <div class="form-block">
            <ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow1" />
            <ca:ResultMessages ID="ResultMessagesStep1" runat="server"></ca:ResultMessages>
            <p>
                <asp:literal runat="server" Text="<%$Resources:MyAccount, ForgotUsername_HeaderText%>"></asp:literal>
            </p>
        
             <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, "field-block-error") %>">
                <asp:Label ID="EmailAddressLabel" runat="server"  Text="<%$Resources:MyAccount, ForgotUsername_EmailAddress%>" AssociatedControlID="EmailAddressTextBox"></asp:Label>
                <div class="field-element">            
                    <asp:TextBox ID="EmailAddressTextBox" runat="server" Columns="40"></asp:TextBox>
                 <div class="field-message"><%= GetMessage(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                <div class="clearboth"></div>
            </div>
             <div class="form-action">
                <span class="submitNextButton">
                    <asp:Button ID="Button1"  runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:MyAccount, ForgotUsername_Submit%>"></asp:Button>
                </span>
            </div>
        </div>
    </asp:Panel>
     
    <asp:Panel ID="ContactCustomerServicePanel" runat="server">
        <div class="form-block">
            <%
             if(_displayAccountLockedMsg)
                {
                    Response.Write(Resources.MyAccount.ForgotUsername_UserLocked);
                }
            else
                {
                    Response.Write(Resources.MyAccount.ForgotPassword_ContactCustomerService);
                }
            %>
        </div>
    </asp:Panel>
    
    
    <asp:Panel ID="SecurityQuestionsPanel" runat="server">
         <div  class="form-block">
            <ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow2" />
         <ca:ResultMessages ID="ResultMessagesStep2" runat="server"></ca:ResultMessages>
            <p>
                <asp:literal runat="server" Text="<%$Resources:MyAccount, ForgotUsername_HeaderText%>"></asp:literal>
            </p>
            
            <div class="field-block">
                <label><%=Resources.MyAccount.ForgotPassword_SecurityQuestion1%></label>
                <div class="field-element" style="font-weight:bold">
                    <asp:Literal ID="Question1Label" runat="server"/>
                </div>
                 <div class="clearboth"></div>
            </div>     
            <div class="field-block <%= GetCssClassIfMessageCodeExist("ANSWER1-REQUIRED", "field-block-error") %>">
                <asp:Label ID="Label3" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_SecurityAnswer1%> " AssociatedControlID="Question1AnswerTextBox" ></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="Question1AnswerTextBox" runat="server" Columns="40"></asp:TextBox>
                    <div class="field-message"><%= GetMessage("ANSWER1-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                 <div class="clearboth"></div>
            </div>
            
            
            
            
            
             <div class="form-action">
                <span class="submitNextButton" >
                    <asp:Button ID="Button2"  runat="server" OnClick="SecurityQuestionsSubmitButton_Click" Text="<%$Resources:MyAccount, ForgotUsername_Submit%>"></asp:Button>
                </span>
            </div>
        </div>
    </asp:Panel>
    
    
    
    
    <asp:Panel ID="SeeUsernamePanel" runat="server">
        <div  class="form-block">
        
        <p>
            <% =Resources.MyAccount.ForgotUsername_SeeUsernameHeader %>
        </p>

        <div class="field-block">
            <label><%=Resources.MyAccount.ForgotPassword_Username%></label>
            <div class="field-element">
                <asp:literal ID="ShowUsernameLabel" runat="server"/>
            </div>
        </div>
        
          <div class="form-action">
                <span class="submitNextButton">
                    <asp:Button  runat="server" OnClick="GoToLoginPageButton_Click" Text="<%$Resources:MyAccount, ForgotUsername_ProceedToLogin%>"></asp:Button>
                </span>
            </div>
        </div>
    </asp:Panel>
</div>
