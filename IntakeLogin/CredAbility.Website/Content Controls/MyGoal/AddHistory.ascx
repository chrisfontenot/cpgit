﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CredAbility.Core.MyGoal" %>
<%@ Import Namespace="CredAbility.Web.UI.ToStringHelpers" %>
<%@ Import Namespace="System.Collections.Generic"%>

<% var Goal = ViewData["goal"] as Goal; %>

<div class="AddHistory">
	<form id="AddHistoryForm">
		
		<%= Html.Hidden("Goal.Id") %>
		<%= Html.Hidden("Goal.Name") %>
		<%= Html.Hidden("Goal.Type") %>
		<%= Html.Hidden("Goal.TotalAmount") %>
		<%= Html.Hidden("Goal.DueDate") %>
		
		<div class="Title"><%= Resources.Website.Goal_UpdateGoal %></div>
		<div class="resultMessage"><%= ViewData["resultMessages"] != null?((ResultMessages)ViewData["resultMessages"]).RenderControl():"" %></div>
		<div class="Name"><%= Goal.Name %></div>
		<div class="deleteBox"><%= Resources.Website.Goal_Delete %><%= Html.CheckBox("delete", new { Class = "delete" }) %></div>
		<div class="clearboth"></div>
		<div class="Want"><%= Resources.Website.Goal_I %> <%= Resources.Website.ResourceManager.GetString("Goal_" + Goal.Type + "2") %> $<%= Html.TextBox("History.Amount") %><%= Resources.Website.Goal_on %><%= Html.TextBox("History.Date","", new {Class = "CalendarBox", disabled="disabled"}) %><a class="calendarIcon" onclick="Goal.showCalendar();"></a></div>
		
		<div class="review">
			<div class="updates">
				<div class="title"><%= Resources.Website.Goal_Updates %></div>
			<% foreach (var history in Goal.History){ %>
				<div><%= history.Date.ToLongDateString() %> - <%= Resources.Website.Goal_I %> <%= Resources.Website.ResourceManager.GetString("Goal_" + Goal.Type + "2") %> <%= history.Amount.ConvertToCurrency() %></div>
			<% } %>
			</div>
			<div class="savings">
				<div><b><%= Resources.Website.Goal_TotalSaved %></b> : <%= Goal.AmountUptoDate.ConvertToCurrency() %></div>
				<div><b><%= Resources.Website.Goal_Remaining %></b> : <%= Goal.RemainingAmount.ConvertToCurrency() %></div>
			</div>
		</div>
		<div class="clearboth"></div>
		<div class="buttons">
			<span class="submitNextButton"><a onclick="Goal.SaveHistory();"><%= Resources.Website.Goal_Update %></a></span>
			<span class="submitNextButton"><a onclick="Goal.overlay.hide();"><%= Resources.Website.Goal_Cancel %></a></span>
		</div>
		
	</form>

</div>