﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HowYouCanHelpContactUs.ascx.cs" Inherits="CredAbility.Website.Content_Controls.HowYouCanHelpContactUs" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div class="form-block">
    <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
    <ca:ResultMessages id="ResultMessages" runat="server"/>

    <div class="field-block <%= GetCssClassIfMessageCodeExist("FIRSTNAME-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label1" AssociatedControlId="txtFirstName" runat="server" ><%= LabelFirstName %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="txtFirstName" Columns="40" name="firstName" />
            <div class="field-message"><%= GetMessage("FIRSTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    <div class="field-block <%= GetCssClassIfMessageCodeExist("LASTNAME-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label2" AssociatedControlId="txtLastName" runat="server" ><%= LabelLastName %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="txtLastName" Columns="40" name="lastName" />            
            <div class="field-message"><%= GetMessage("LASTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div> 
       </div>     
        <div class="clearboth"></div>
   </div>
    <div class="field-block">
        <asp:Label ID="Label3" AssociatedControlId="txtCompany" runat="server" ><%= LabelCompany %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="txtCompany" Columns="40" name="company" />
        </div>
        <div class="clearboth"></div>
    </div>
    <div class="field-block">
        <asp:Label ID="Label4" AssociatedControlId="txtPhone" runat="server" ><%= LabelPhone %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="txtPhone" Columns="40" name="phone" />
        </div>
        <div class="clearboth"></div>
    </div>
    
   <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, "field-block-error") %>">
        <asp:Label ID="Label6" AssociatedControlId="txtEmail" runat="server" ><%= LabelEmail %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="txtEmail"  Columns="40" name="email" /> 
            <div class="field-message"><%= GetMessage(new[] { "EMAIL-REQUIRED", "EMAIL-WRONGFORMAT" }, Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    <div class="field-block <%= GetCssClassIfMessageCodeExist("COMMENT-REQUIRED", "field-block-error") %>">
   
        <asp:Label ID="Label7" AssociatedControlId="txtComment" runat="server" ><%= LabelComment %></asp:Label>
        <div class="field-element">
               <asp:TextBox runat="server" TextMode="MultiLine" Columns="40" rows="6" id="txtComment" name="comment" />
               <div class="field-message"><%= GetMessage("COMMENT-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>      
        <div class="clearboth"></div>
    </div>
    
    <div class="form-action">
        <span class="submitNextButton"><asp:Button ID="submit" runat="server" OnClick="Submit_Click" /></span>
    </div>
    
</div>