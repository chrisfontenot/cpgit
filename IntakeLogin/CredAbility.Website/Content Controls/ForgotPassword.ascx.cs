﻿using System;
using System.Collections.Generic;
using CredAbility.Core.Common;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Identity;
using CredAbility.Data.Validator;
using CredAbility.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.Security;

namespace CredAbility.Website.Content_Controls
{
    public partial class ForgotPassword : FormBaseControl
    {
        string username = "";
        protected bool _displayAccountLockedMsg;

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!this.IsPostBack)
            {
                GetUsernamePanel.Visible = true;
                ContactCustomerServicePanel.Visible = false;
                SecurityQuestionsPanel.Visible = false;
                ResetPasswordPanel.Visible = false;
            }

            ErrorSeeBelow1.Visible = false;
            ErrorSeeBelow2.Visible = false;
            ErrorSeeBelow3.Visible = false;
        }

        protected void ResetPasswordButton_Click(object sender, EventArgs e)
        {
            ClientProfileManager manager = new ClientProfileManager();
            ActionResult ar = new ActionResult(true);
            this.PasswordFieldValidator(ar);

            if (ar.IsSuccessful)
            {
                ar.MergeActionResult(manager.ResetPassword(NewPassswordTextBox.Text, RetypeNewPasswordTextBox.Text));
                if (ar.IsSuccessful)
                {
                    //Response.Redirect(ConfigReader.ConfigSection.Url.Login.Value);
                    manager.Login(SafeRequestHelper.SanitizeHtml(UsernameTextBox.Text), NewPassswordTextBox.Text, false, false, false);
                    Response.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value); 
                }
            }

            if (!ar.IsSuccessful && !ar.Messages.HasMessage(string.Empty) && !ar.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow3.Visible = true;
            }
            else if (ar.Messages.HasMessage(string.Empty) || ar.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow3.Visible = false;
                this.ResultMessagesStep3.Messages = ar.Messages;
            }
            this.Messages = ar.Messages;
        }

        protected void SecurityQuestionsSubmitButton_Click(object sender, EventArgs e)
        {
            ActionResult actionResult;
            var clientProfileManager = new ClientProfileManager();
            var response = new CustomResponse<UserSecurityQuestionChallengeResult>();

            response = clientProfileManager.ValidateSecurityAnswersByUsername(UsernameTextBox.Text, Question1AnswerTextBox.Text, out actionResult);
            UserSecurityQuestionChallengeResult challengeResult = response.ResponseValue;

            if (actionResult.IsSuccessful)
            {

                if (challengeResult.IsSuccessful)
                {
                    // show the reset password panel
                    GetUsernamePanel.Visible = false;
                    ContactCustomerServicePanel.Visible = false;
                    SecurityQuestionsPanel.Visible = false;
                    ResetPasswordPanel.Visible = true;
                }
                else
                {
                    if (challengeResult.IsUserLocked.HasValue && challengeResult.IsUserLocked.Value)
                    {
                        // show the contact customer service panel
                        GetUsernamePanel.Visible = false;
                        _displayAccountLockedMsg = true;
                        ContactCustomerServicePanel.Visible = true;
                        SecurityQuestionsPanel.Visible = false;
                        ResetPasswordPanel.Visible = false;
                    }
                    else
                    {
                        // show the security questions panel again
                        GetUsernamePanel.Visible = false;
                        ContactCustomerServicePanel.Visible = false;
                        SecurityQuestionsPanel.Visible = true;
                        ResetPasswordPanel.Visible = false;
                    }
                }
            }

            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty) && !actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow2.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty) || actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow2.Visible = false;
                this.ResultMessagesStep2.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            username = SafeRequestHelper.SanitizeHtml(UsernameTextBox.Text);
            ActionResult actionResult;
            var clientProfileManager = new ClientProfileManager();
            var resultStep1 = clientProfileManager.ForgotPassword(username, out actionResult);


           


            if (actionResult.IsSuccessful)
            {
                if (resultStep1 != null)
                {
                    GetUsernamePanel.Visible = false;
                    ContactCustomerServicePanel.Visible = false;
                    SecurityQuestionsPanel.Visible = true;

                    Question1Label.Text = resultStep1.Text;
                }
                else
                {
                    GetUsernamePanel.Visible = false;
                    _displayAccountLockedMsg = false;
                    ContactCustomerServicePanel.Visible = true;
                    SecurityQuestionsPanel.Visible = false;
                }
            }

            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty) && !actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow1.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty) || actionResult.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow1.Visible = false;
                this.ResultMessagesStep1.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;
           
        }


        private void PasswordFieldValidator(ActionResult result)
        {
            result.IsSuccessful = true;

            if (string.IsNullOrEmpty(this.RetypeNewPasswordTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ForgotPassword_RetypePasswordRequired, "CONFIRMPWD-REQUIRED"));
            }

            if (this.RetypeNewPasswordTextBox.Text != this.NewPassswordTextBox.Text)
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.ForgotPassword_RetypePasswordComparison, "CONFIRMPWD-NOMATCH"));
            }



        }
    }
}