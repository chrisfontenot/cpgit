﻿using System;
using System.Threading;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Web.Authentification;
using CredAbility.Web.Data;
using CredAbility.Web.Security;
using CredAbility.Web.UI.UserControls;
using Sitecore.Web;

namespace CredAbility.Website.Content_Controls
{
    public partial class CreateAccount : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["FirstName"]))
                {
                    FirstNameTextBox.Text = Request.QueryString["FirstName"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["LastName"]))
                {
                    LastNameTextBox.Text = Request.QueryString["LastName"];
                }

                if (!string.IsNullOrEmpty(Request.QueryString["EmailAddress"]))
                {
                    EmailAddressTextBox.Text= Request.QueryString["EmailAddress"];
                }

                var losCode = WebUtil.GetQueryString("LOS");
                if (!string.IsNullOrEmpty(losCode))
                {
                    AuthentificationContext.Current.LosCode = losCode;
                }
                if (Sitecore.Context.User.IsAuthenticated && !string.IsNullOrEmpty(losCode) && AuthentificationContext.Current.IsActiveLosCodeRedirection)
                {
                    AuthentificationContext.Current.Redirect(ClientProfileManager.CurrentClientProfile);
                }
                GetSecurityQuestions();
                GetLanguageOptions();
            }
            ErrorSeeBelow.Visible = false;
        }

        private void GetLanguageOptions()
        {
            foreach (var language in CredAbility.Web.Language.GetAllLanguage())
            {
                var item = new ListItem(language.Text, language.Code);
                if (language.Code.ToUpper() == Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToUpper())
                {
                    item.Selected = true;
                }
                LanguageDDL.Items.Add(item);
            }
        }

        private void GetSecurityQuestions()
        {
            var questions = IdentityData.GetAllSecurityQuestions(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToUpper());
            SecurityQuestionDDL.Items.Add(new ListItem(Resources.MyAccount.CreateAccount_SelectOne, ""));
            foreach (var question in questions)
            {
                SecurityQuestionDDL.Items.Add(new ListItem(question.Text, question.ID.ToString()));
            }
        }

        protected void CreateAccountButton_Click(object sender, EventArgs e)
        {
            AccountManager accountManager = new AccountManager();
            var profile = new ClientProfile();
            this.AssignValueToClientProfile(profile);
            var result = new ActionResult(true);
            this.FieldValidator(result);
            if (result.IsSuccessful)
            {
                result.MergeActionResult(accountManager.CreateAccount(profile, this.RetypePasswordTextBox.Text));
            }
            if (!result.IsSuccessful && !result.Messages.HasMessage(string.Empty) && !result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = true;
            }
            else if (result.Messages.HasMessage(string.Empty) || result.Messages.HasMessageCodeStartingWith("WS"))
            {
                ErrorSeeBelow.Visible = false;
                this.ResultMessages.Messages = result.Messages;
            }
            this.Messages = result.Messages;
        }

        private void FieldValidator(ActionResult result)
        {
            result.IsSuccessful = true;
            if (string.IsNullOrEmpty(this.RetypePasswordTextBox.Text))
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.CreateAccount_RetypePasswordRequired, "CONFIRMPWD-REQUIRED"));
            }

            if (this.RetypePasswordTextBox.Text != this.NewPasswordTextBox.Text)
            {
                result.IsSuccessful = false;
                result.Messages.Add(new ResultMessage(Resources.MyAccount.CreateAccount_RetypePasswordComparison, "CONFIRMPWD-NOMATCH"));
            }
        }

        private void AssignValueToClientProfile(ClientProfile profile)
        {
            profile.FirstName = SafeRequestHelper.SanitizeHtml(this.FirstNameTextBox.Text);
            profile.LastName = SafeRequestHelper.SanitizeHtml(this.LastNameTextBox.Text);
            profile.EmailAddress = SafeRequestHelper.SanitizeHtml(this.EmailAddressTextBox.Text);
            profile.Username = SafeRequestHelper.SanitizeHtml(this.UsernameTextBox.Text);
            profile.Password = SafeRequestHelper.SanitizeHtml(this.NewPasswordTextBox.Text);
            profile.SecurityQuestion = SafeRequestHelper.SanitizeHtml(this.SecurityQuestionDDL.SelectedValue);
            profile.SecurityAnswer = SafeRequestHelper.SanitizeHtml(this.SecurityAnswerTextBox.Text);
            profile.PreferredLanguage = SafeRequestHelper.SanitizeHtml(this.LanguageDDL.SelectedValue);
        }
    }
}