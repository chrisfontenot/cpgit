﻿using System;
using System.Web;
using CredAbility.Core.Common;
using CredAbility.Web;
using CredAbility.Web.API.Processors;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;
using CredAbility.Web.UI.WebControls;
using Sitecore.Web;
using CredAbility.Data.CredU;
using Sitecore.Data.Items;

namespace CredAbility.Website.Content_Controls
{
	public partial class NeedZip:FormBaseControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				if(Session["CourseName"] != null)
				{
					MasterPages.Main MasterControl = this.Page.Master as MasterPages.Main;
					MasterControl.SetTitle(Session["CourseName"].ToString());
				}
			}
			ErrorSeeBelow.Visible = false;
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			if(CredURepository.CheckZip(ZipCode.Text))
			{
				Session.Add("ZipCode", ZipCode.Text);
				string ReturnUrl = String.Empty;
				if(Session["ReturnUrl"] == null)
				{
					ReturnUrl = "education/default.aspx";
				}
				else
				{
					ReturnUrl = Session["ReturnUrl"].ToString();
					Session.Remove("ReturnUrl");
					Session.Remove("CourseName");
				}
				Response.Redirect(ReturnUrl);
			}
			else
			{
				ResultMessage ErrMsg = new ResultMessage(Resources.Website.CredU_ZipErrMsg,"BAD-ZIP");
				Messages.Add(ErrMsg);
			}
		}
	}
}