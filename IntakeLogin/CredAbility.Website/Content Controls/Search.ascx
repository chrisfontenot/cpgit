﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="CredAbility.Website.Content_Controls.Search" %>

<div class="search-results">
    <div><%=string.Format(Resources.Website.Search_ResultsFor,ResultsCount, Server.HtmlEncode(SearchArgument))%></div>    
<%
	foreach (var result in Results)
	{
%>
    <div class="result">
        <div class="title"><a href="<%=result.Url%>"><%=result.Title%></a></div>
        <div class="summary"><%=(result.Summary.Substring(0, result.Summary.Length > 300 ? 300 : result.Summary.Length)) + "..."%></div>
        <div class="url"><a href="<%=result.Url%>"><%="http://" + Request.Url.Host + result.Url%></a></div>
    </div>
<%
	}
%>
</div>
<%
	if (Results.Count > 0 && MaxPage != 1)
   {
%>
<div class="pager">
<%
		if (CurrentPage != 1)
		{
%>
        <span><a href="<%=GetUrlForPage(CurrentPage - 1)%>" ><%=Resources.Website.Global_Previous%></a></span>
<%
		}
		for (var i = 1;i<=MaxPage;i++)
		{
			if (i == CurrentPage)
			{
%>
		<span class="current"><%= i %></span>
<%
			}
			else
			{
%>
        <span><a href="<%= GetUrlForPage(i) %>" ><%= i %></a></span>
<%
			}
		}    
		if (CurrentPage != MaxPage)
		{
%>
        <span><a href="<%= GetUrlForPage(CurrentPage + 1) %>" ><%= Resources.Website.Global_Next %></a></span>
<%
		}
%>
</div>
<%
	}
%>