﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

    <!-- output directives -->
    <xsl:output method="html" indent="no" encoding="UTF-8" />

    <!-- parameters -->
    <xsl:param name="lang" select="'en'"/>
    <xsl:param name="id" select="''"/>
    <xsl:param name="sc_item"/>
    <xsl:param name="sc_currentitem"/>

    <!-- variables -->
    <!-- Uncomment one of the following lines if you need a "home" variable in you code -->
    <!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
    <!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
    <!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


    <!-- entry point -->
    <xsl:template match="*">
        <div class="segments-3-col">

            <xsl:apply-templates select="$sc_item/item[@template = 'landing segment']" mode="main"/>
            <div class="clearboth"></div>
        </div>
    </xsl:template>

    <!--==============================================================-->
    <!-- main                                                         -->
    <!--==============================================================-->
    <xsl:template match="*" mode="main">
        <div style="background:url({sc:GetMediaUrl(sc:item(sc:fld('Background Image',current(),'mediaid'),.))}) no-repeat bottom right">

            <xsl:attribute name="class">
                <xsl:if test="position() = 1">segment segment-tall</xsl:if>
                <xsl:if test="position() &gt; 1">segment</xsl:if>
            </xsl:attribute>
        
            <h2>
                <sc:text field="Title"/>
            </h2>
            <div class="bd">
                <sc:text field="Content"/>
                <xsl:if test="sc:fld('Link Text',.) != ''">

                    <p class="link">
                        <a href="{sc:path(sc:item(sc:fld('Linked Item',.),.))}">
                            <sc:text field="Link Text"/>
                        </a>
                    </p>
                </xsl:if>
            </div>
        </div>


    </xsl:template>

</xsl:stylesheet>