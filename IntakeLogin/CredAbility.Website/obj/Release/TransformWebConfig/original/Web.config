﻿<?xml version="1.0"?>
<configuration>
    <configSections>
        <section name="sitecore" type="Sitecore.Configuration.ConfigReader, Sitecore.Kernel"/>
        <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, Sitecore.Logging"/>
        <section name="credability" type="CredAbility.Web.Configuration.ConfigReader, CredAbility.Web"/>
        <section name="secureWebPages" type="CredAbility.Web.Configuration.SecureWebPageSettings, CredAbility.Web"/>
        <sectionGroup name="system.web.extensions" type="System.Web.Configuration.SystemWebExtensionsSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
            <sectionGroup name="scripting" type="System.Web.Configuration.ScriptingSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
                <section name="scriptResourceHandler" type="System.Web.Configuration.ScriptingScriptResourceHandlerSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                <sectionGroup name="webServices" type="System.Web.Configuration.ScriptingWebServicesSectionGroup, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
                    <section name="jsonSerialization" type="System.Web.Configuration.ScriptingJsonSerializationSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="Everywhere"/>
                    <section name="profileService" type="System.Web.Configuration.ScriptingProfileServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                    <section name="authenticationService" type="System.Web.Configuration.ScriptingAuthenticationServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                    <section name="roleService" type="System.Web.Configuration.ScriptingRoleServiceSection, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" allowDefinition="MachineToApplication"/>
                </sectionGroup>
            </sectionGroup>
        </sectionGroup>
    </configSections>
    <secureWebPages configSource="App_Config\DEV\SecureWebPages.config"/>
    <credability configSource="App_Config\DEV\credability.config"/>
    <connectionStrings configSource="App_Config\DEV\ConnectionStrings.config"/>
    <appSettings configSource="App_Config\DEV\appSettings.config"/>
    <sitecore configSource="App_Config\DEV\sitecore.config"/>
    <log4net configSource="App_Config\log4net.config"/>
	<location inheritInChildApplications="false"> 
    <system.web>
        <!-- Continue to run Sitecore without script validations -->
        <pages enableViewState="true" validateRequest="false">
            <controls>
                <add tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel"/>
                <add tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Analytics"/>
                <add tagPrefix="ca" namespace="CredAbility.Web.UI.WebControls" assembly="CredAbility.Web"/>
                <add tagPrefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add tagPrefix="asp" namespace="System.Web.UI.WebControls" assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            </controls>
            <namespaces>
                <add namespace="Microsoft.Web.Mvc"/>
                <add namespace="System.Web.Mvc"/>
                <add namespace="System.Web.Mvc.Html"/>
                <add namespace="System.Web.Routing"/>
                <add namespace="System.Linq"/>
                <add namespace="CredAbility.Web.UI.WebControlsHelpers"/>
                <add namespace="CredAbility.Web.UI.WebControls"/>
            </namespaces>
        </pages>
        <httpModules>
            <add name="UrlRoutingModule" type="System.Web.Routing.UrlRoutingModule, System.Web.Routing, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add type="Sitecore.Nexus.Web.HttpModule,Sitecore.Nexus" name="SitecoreHttpModule"/>
            <add type="Sitecore.Resources.Media.UploadWatcher, Sitecore.Kernel" name="SitecoreUploadWatcher"/>
            <add type="Sitecore.IO.XslWatcher, Sitecore.Kernel" name="SitecoreXslWatcher"/>
            <add type="Sitecore.IO.LayoutWatcher, Sitecore.Kernel" name="SitecoreLayoutWatcher"/>
            <add type="Sitecore.Configuration.ConfigWatcher, Sitecore.Kernel" name="SitecoreConfigWatcher"/>
            <add name="ScriptModule" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </httpModules>
        <httpHandlers>
            <add verb="*" path="sitecore_media.ashx" type="Sitecore.Resources.Media.MediaRequestHandler, Sitecore.Kernel"/>
            <add verb="*" path="sitecore_xaml.ashx" type="Sitecore.Web.UI.XamlSharp.Xaml.XamlPageHandlerFactory, Sitecore.Kernel"/>
            <add verb="*" path="sitecore_icon.ashx" type="Sitecore.Resources.IconRequestHandler, Sitecore.Kernel"/>
            <!--<add verb="*" path="*.js" type="CredAbility.Web.API.Handlers.JsMinify, CredAbility.Web"/>
<add verb="*" path="*.css" type="CredAbility.Web.API.Handlers.CssMinify, CredAbility.Web"/>-->
            <add verb="*" path="combo.ashx" type="CredAbility.Web.API.Handlers.Combo, CredAbility.Web"/>
            <add verb="*" path="logout.ashx" type="CredAbility.Web.API.Handlers.Logout, CredAbility.Web"/>
            <add verb="*" path="info.ashx" type="CredAbility.Web.API.Handlers.SessionInfo, CredAbility.Web"/>
            <add verb="*" path="itemrater.ashx" type="CredAbility.Web.API.Handlers.ItemRater, CredAbility.Web"/>
            <add verb="*" path="sso.ashx" type="CredAbility.Web.API.Handlers.SingleSignOn, CredAbility.Web"/>
            <add verb="*" path="actionplancheckbox.ashx" type="CredAbility.Web.API.Handlers.ActionPlanCheckbox, CredAbility.Web"/>
            <add verb="*" path="documents.ashx" type="CredAbility.Web.API.Handlers.Documents, CredAbility.Web"/>
            <add verb="*" path="documentselector.ashx" type="CredAbility.Web.API.Handlers.DocumentSelector, CredAbility.Web"/>
            <add verb="*" path="mydmp.ashx" type="CredAbility.Web.API.Handlers.MyDMP, CredAbility.Web"/>
            <remove verb="*" path="*.asmx"/>
            <add verb="*" path="*.asmx" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add verb="*" path="*_AppService.axd" validate="false" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add verb="GET,HEAD" path="ScriptResource.axd" validate="false" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </httpHandlers>
        <membership defaultProvider="sitecore">
            <providers>
                <clear/>
                <add name="sitecore" type="Sitecore.Security.SitecoreMembershipProvider, Sitecore.Kernel" realProviderName="sql" providerWildcard="%" raiseEvents="true"/>
                <add name="sql" type="System.Web.Security.SqlMembershipProvider" connectionStringName="core" applicationName="sitecore" minRequiredPasswordLength="1" minRequiredNonalphanumericCharacters="0" requiresQuestionAndAnswer="false" requiresUniqueEmail="false" maxInvalidPasswordAttempts="256"/>
                <add name="switcher" type="Sitecore.Security.SwitchingMembershipProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/membership"/>
            </providers>
        </membership>
        <roleManager defaultProvider="sitecore" enabled="true">
            <providers>
                <clear/>
                <add name="sitecore" type="Sitecore.Security.SitecoreRoleProvider, Sitecore.Kernel" realProviderName="sql" raiseEvents="true"/>
                <add name="sql" type="System.Web.Security.SqlRoleProvider" connectionStringName="core" applicationName="sitecore"/>
                <add name="switcher" type="Sitecore.Security.SwitchingRoleProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/roleManager"/>
            </providers>
        </roleManager>
        <profile defaultProvider="sql" enabled="true" inherits="Sitecore.Security.UserProfile, Sitecore.Kernel">
            <providers>
                <clear/>
                <add name="sql" type="System.Web.Profile.SqlProfileProvider" connectionStringName="core" applicationName="sitecore"/>
                <add name="switcher" type="Sitecore.Security.SwitchingProfileProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/profile"/>
            </providers>
            <properties>
                <clear/>
                <add type="System.String" name="SC_UserData"/>
            </properties>
        </profile>
        <compilation defaultLanguage="c#" debug="true">
            <assemblies>
                <add assembly="System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
                <add assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add assembly="System.Web.Abstractions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add assembly="System.Web.Routing, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add assembly="System.Web.Mvc, Version=1.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
                <add assembly="System.Xml.Linq, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
                <add assembly="System.Data.DataSetExtensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
                <add assembly="System.Data.Linq, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
            </assemblies>
        </compilation>
		<customErrors mode="RemoteOnly" defaultRedirect="~/Error.aspx" />
			<!--  AUTHENTICATION 
This section sets the authentication policies of the application. Possible modes are "Windows", "Forms", 
"Passport" and "None"
-->
        <authentication mode="Forms">
            <forms name=".ASPXAUTH" cookieless="UseCookies"/>
        </authentication>
        <!--  IDENTITY 
If this setting is true, aspnet will run in the security context of the IIS authenticated 
user (ex. IUSR_xxx).
If false, aspnet will run in the security context of the default ASPNET user.
-->
        <identity impersonate="false"/>
        <!--  APPLICATION-LEVEL TRACE LOGGING
Application-level tracing enables trace log output for every page within an application. 
Set trace enabled="true" to enable application trace logging.  If pageOutput="true", the
trace information will be displayed at the bottom of each page.  Otherwise, you can view the 
application trace log by browsing the "trace.axd" page from your web application
root. 
-->
        <trace enabled="false" requestLimit="50" pageOutput="false" traceMode="SortByTime" localOnly="true"/>
        <!--  SESSION STATE SETTINGS
By default ASP .NET uses cookies to identify which requests belong to a particular session. 
If cookies are not available, a session can be tracked by adding a session identifier to the URL. 
To disable cookies, set sessionState cookieless="true".

Note that Sitecore does not support cookieless sessions
<sessionState mode="StateServer" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;user id=sa;password=" cookieless="false" timeout="20"/>
-->
        <sessionState mode="InProc" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;user id=sa;password=" cookieless="false" timeout="60"/>
        <!--  GLOBALIZATION
This section sets the globalization settings of the application. 
-->
        <globalization requestEncoding="utf-8" responseEncoding="utf-8"/>
        <!--
httpRuntime Attributes:
executionTimeout="[seconds]" - time in seconds before request is automatically timed out
maxRequestLength="[KBytes]" - KBytes size of maximum request length to accept
useFullyQualifiedRedirectUrl="[true|false]" - Fully qualifiy the URL for client redirects
minFreeThreads="[count]" - minimum number of free thread to allow execution of new requests
minLocalRequestFreeThreads="[count]" - minimum number of free thread to allow execution of new local requests
appRequestQueueLimit="[count]" - maximum number of requests queued for the application

If you change the maxRequestLength setting, you should also change the Media.MaxSizeInDatabase setting. 
Media.MaxSizeInDatabase should always be less than maxRequestLength.

default = 16384
-->
        <httpRuntime maxRequestLength="106384" executionTimeout="600" enableVersionHeader="false"/>
        <!--
<machineKey 
validationKey="7CDE5CF447AAB3E1C4B883895C0E5E0F05D244E912FFA1A84DF7A7A20CEA2CF80AC5F8E6D83EAA61944B03C85FE44D52F0B9346363DB699CBF9E6A06621D04C6"
decryptionKey="95924271BB899FF32D0549F8690ABF32FA5BF495370416E4F930F4D11A7AA1D5"
validation="SHA1" decryption="AES" />
-->
    </system.web>
	</location>
    <system.serviceModel>
        <bindings configSource="App_Config\DEV\system.serviceModel.bindings.config"/>
        <client configSource="App_Config\DEV\system.serviceModel.client.config"/>
    </system.serviceModel>
    <system.webServer>
        <validation validateIntegratedModeConfiguration="false"/>
        <modules>
            <remove name="ScriptModule"/>
            <add name="ScriptModule" preCondition="managedHandler" type="System.Web.Handlers.ScriptModule, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </modules>
        <handlers>
            <remove name="WebServiceHandlerFactory-Integrated"/>
            <remove name="ScriptHandlerFactory"/>
            <remove name="ScriptHandlerFactoryAppServices"/>
            <remove name="ScriptResource"/>
            <add name="ScriptHandlerFactory" verb="*" path="*.asmx" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add name="ScriptHandlerFactoryAppServices" verb="*" path="*_AppService.axd" preCondition="integratedMode" type="System.Web.Script.Services.ScriptHandlerFactory, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
            <add name="ScriptResource" verb="GET,HEAD" path="ScriptResource.axd" preCondition="integratedMode" type="System.Web.Handlers.ScriptResourceHandler, System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35"/>
        </handlers>
    </system.webServer>
    <system.codedom>
        <compilers>
            <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CSharp.CSharpCodeProvider, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" warningLevel="4">
                <providerOption name="CompilerVersion" value="v3.5"/>
                <providerOption name="WarnAsError" value="false"/>
            </compiler>
        </compilers>
    </system.codedom>
    <runtime>
        <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1" appliesTo="v2.0.50727">
            <dependentAssembly>
                <assemblyIdentity name="System.Web.Extensions" publicKeyToken="31bf3856ad364e35"/>
                <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
            </dependentAssembly>
            <dependentAssembly>
                <assemblyIdentity name="System.Web.Extensions.Design" publicKeyToken="31bf3856ad364e35"/>
                <bindingRedirect oldVersion="1.0.0.0-1.1.0.0" newVersion="3.5.0.0"/>
            </dependentAssembly>
        </assemblyBinding>
    </runtime>
</configuration>
