﻿<%@ Import Namespace="CredAbility.Web" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.ascx.cs" Inherits="CredAbility.Website.Content_Controls.CreateAccount" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<asp:ScriptManager ID="ScriptManager" runat="server">
</asp:ScriptManager>
<asp:Panel runat="server" DefaultButton="loginButton">
	<div id="create-account" class="form-block create-account-form">
		<ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow" />
		<ca:ResultMessages ID="ResultMessages" runat="server" />
		<div class="field-block <%= GetCssClassIfMessageCodeExist("FIRSTNAME-REQUIRED", "field-block-error") %>">
			<asp:Label runat="server" Text="<%$Resources:MyAccount, CreateAccount_FirstName%>" AssociatedControlID="FirstNameTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="1" ID="FirstNameTextBox" Columns="40" runat="server" autocomplete="off"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage("FIRSTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist("LASTNAME-REQUIRED", "field-block-error") %>">
			<asp:Label runat="server" Text="<%$Resources:MyAccount, CreateAccount_LastName%>" AssociatedControlID="LastNameTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="2" ID="LastNameTextBox" Columns="40" runat="server" autocomplete="off"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage("LASTNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, "field-block-error") %>">
			<asp:Label ID="EmailAddress" runat="server" Text="<%$Resources:MyAccount, CreateAccount_EmailAddress%>" AssociatedControlID="EmailAddressTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="3" ID="EmailAddressTextBox" Columns="40" runat="server" autocomplete="off"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage(new []{"EMAIL-REQUIRED","EMAIL-WRONGFORMAT"}, Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"USERNAME-REQUIRED","USERNAME-UNAVAILABLE"}, "field-block-error") %>">
			<asp:Label ID="Username" runat="server" Text="<%$Resources:MyAccount, CreateAccount_Username%>" AssociatedControlID="UsernameTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="4" ID="UsernameTextBox" runat="server" Columns="40" autocomplete="off"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage(new []{"USERNAME-REQUIRED","USERNAME-UNAVAILABLE"}, Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist(new []{ "PASSWORD-REQUIRED","PASSWORD-WRONGFORMAT"}, "field-block-error") %>">
			<asp:Label ID="Password" runat="server" Text="<%$Resources:MyAccount, CreateAccount_Password%>" AssociatedControlID="PasswordTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="5" ID="PasswordTextBox" runat="server" Columns="40" TextMode="Password"></asp:TextBox>
				<a href="#" id="show1" class="CreateAccountTextLink" title="<%= Resources.MyAccount.Global_PasswordRules %>">
					<img src="/static/images/layout/ico-questionmark.gif" /></a>
				<div class="passwordRules" id="myPanel" style="visibility: hidden; display: none">
					<div id="panel1">
						<div class="hd">
							<%= Resources.MyAccount.Global_PasswordRules %></div>
						<div class="bd">
							<iframe src="/global/password-rules.aspx" frameborder="0"></iframe>
						</div>
					</div>
				</div>
				<div class="field-message">
					<%= GetMessage(new []{ "PASSWORD-REQUIRED","PASSWORD-WRONGFORMAT"}, Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"CONFIRMPWD-REQUIRED","CONFIRMPWD-NOMATCH"}, "field-block-error") %>">
			<asp:Label ID="RetypePassword" runat="server" Text="<%$Resources:MyAccount, CreateAccount_RetypePassword%>" AssociatedControlID="RetypePasswordTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="6" ID="RetypePasswordTextBox" runat="server" Columns="40" TextMode="Password"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage(new []{"CONFIRMPWD-REQUIRED","CONFIRMPWD-NOMATCH"}, Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"QUESTION1-REQUIRED"}, "field-block-error") %>">
			<asp:Label ID="SecurityQuestion" runat="server" Text="<%$Resources:MyAccount, CreateAccount_SecurityQuestion%>" AssociatedControlID="SecurityQuestionDDL"></asp:Label>
			<div class="field-element">
				<asp:DropDownList TabIndex="6" ID="SecurityQuestionDDL" runat="server" />
				<div class="field-message">
					<%= GetMessage(new []{"QUESTION1-REQUIRED"}, Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block <%= GetCssClassIfMessageCodeExist("ANSWER1-REQUIRED", "field-block-error") %>">
			<asp:Label ID="SecurityAnswer" runat="server" Text="<%$Resources:MyAccount, CreateAccount_SecurityAnswer%>" AssociatedControlID="SecurityAnswerTextBox"></asp:Label>
			<div class="field-element">
				<asp:TextBox TabIndex="7" ID="SecurityAnswerTextBox" Columns="40" runat="server" autocomplete="off"></asp:TextBox>
				<div class="field-message">
					<%= GetMessage("ANSWER1-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="field-block">
			<asp:Label ID="Language" runat="server" Text="<%$Resources:MyAccount, CreateAccount_Language%>" AssociatedControlID="LanguageDDL"></asp:Label>
			<div class="field-element">
				<asp:DropDownList TabIndex="10" ID="LanguageDDL" runat="server" />
			</div>
			<div class="clearboth">
			</div>
		</div>
		<div class="form-action">
			<span class="submitNextButton">
				<asp:Button TabIndex="11" ID="loginButton" runat="server" OnClick="CreateAccountButton_Click" Text="<%$Resources:MyAccount,CreateAccount_CreateAccountButton %>" />
			</span>
		</div>
	</div>
</asp:Panel>

<script>
var PasswordRuleInit = true;
</script>

