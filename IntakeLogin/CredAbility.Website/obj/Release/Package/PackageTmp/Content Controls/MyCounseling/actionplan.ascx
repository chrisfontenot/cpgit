﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="actionplan.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.actionplan" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<div id="action-plan" class="account">
<%
	if(ClientProfileRepository.GetLastVisitedWebsite(ClientProfile) != null && ClientProfileRepository.GetLastVisitedWebsite(ClientProfile).PercentComplete >= 100)
	{
		if(Summary != null)
		{
%>
	<div>
		<p class="date">
			<%=Resources.MyAccount.MyCounseling_Summary_AsOfDate%>
			<%=Summary.AsOfDate.ToShortDateString()%>
		</p>
	</div>
<%
			foreach(var item in Summary.ActionItems)
			{
%>
	<div class="item">
		<div class="content">
			<h3>
				<%=item.Title%></h3>
			<p>
				<%=item.Text%></p>
			<p class="completeCheckbox">
				<input type="checkbox" id="check-<%= item.Id %>" <%= item.CompletionDate.HasValue? " checked=\"checked\"" : "" %> />
				<span class="undone">
					<%=item.CompletionDate.HasValue ? Resources.MyAccount.MyCounseling_Summary_ActionPlan_CompletedOn + item.CompletionDate.Value.ToLongDateString() : Resources.MyAccount.MyCounseling_Summary_ActionPlan_Undone%>
				</span>
			</p>
		</div>
	</div>
<%
			}
		}
		else
		{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NotAvailable%>
	</div>
<%
		}
	}
	else
	{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NeverHadCounseling%>
	</div>
<%
	}
	if(PastSummariesUrl != null)
	{
%>
	<div class="links">
		<p class="link-btn download-summary"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
		<p class="link-btn download-summary"><a href="<%= PastSummariesUrl %>"><%=Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
	<script>
        var ActionPlanCheckboxesInit = true;
        var actionPlanClientId = "<%= ClientProfile.ClientID %>";
	</script>
</div>
