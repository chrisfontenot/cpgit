﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyPastSummaries.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.MyPastSummaries" %>
<div id="past-summary">
<ul>
<%
	foreach (var document in Documents)
	{
%>
	<li>
		<div class="document">
            <span class="icon"><img src="/static/images/layout/icons/icon-pdf.gif" /></span>
			<div class="Description"><%= document.Description %>&nbsp;&nbsp;<%= document.Date.ToShortDateString() %></div>
			<div class="submitNextButton"><a href="~/Documents/Documents.ashx?id=<%= document.Id %>" target="blank"><%= Resources.MyAccount.PastCounselingSummaries_View %></a></div>
			<div class="submitNextButton"><a href="~/DocumentSelector/DocumentSelector.ashx?id=<%= document.Id %>"><%= Resources.MyAccount.PastCounselingSummaries_Select %></a></div>
		</div>
		<div class="clearboth"/>
	</li>
<%
	}
%>
</ul>
</div>