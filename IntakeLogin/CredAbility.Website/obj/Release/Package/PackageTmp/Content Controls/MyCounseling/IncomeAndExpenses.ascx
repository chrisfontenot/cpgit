﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IncomeAndExpenses.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.IncomeAndExpenses" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<%@ Import Namespace="CredAbility.Core.Client" %>
<%@ Import Namespace="CredAbility.Web.UI.ToStringHelpers" %>
<%@ Import Namespace="CredAbility.Website.Content_Controls.MyCounseling" %>
<div id="income-expenses">
<%
	if(ClientProfileRepository.GetLastVisitedWebsite(ClientProfile) != null && ClientProfileRepository.GetLastVisitedWebsite(ClientProfile).PercentComplete >= 100)
	{
		if(this.Summary != null)
		{
%>
	<div class="date">
		<%=Resources.MyAccount.IncomeAndExpenses_AsOf%><%=this.Summary.AsOfDate.ToShortDateString()%>
		<div class="link-btn download-budget">
			<asp:LinkButton runat="server" OnClick="OpenCSV"><%=Resources.MyAccount.IncomeAndExpenses_DownloadBudget%></asp:LinkButton></div>
	</div>
	<div id="demo" class="tab">
		<ul class="tab-menu">
			<div class="tab-menu">
				<h3>
					<%=Resources.MyAccount.IncomeAndExpenses_Income%></h3>
				<p class="income">
					<span>
						<%= this.Summary.ExpenseInfo.TotalIncome.ConvertToCurrency()%>
					</span>
					<%=Resources.MyAccount.IncomeAndExpenses_EveryMonth%>
				</p>
				<h3>
					<%=Resources.MyAccount.IncomeAndExpenses_YouExpenses%>
				</h3>
			</div>
<%
			foreach(ExpenseCategory expenseCategory in this.Summary.ExpenseInfo.ExpenseCategories)
			{
%>
			<li>
				<a href="#<%= expenseCategory.Name%>"></a>
				<table>
					<thead>
						<td>
							<%= expenseCategory.Name%>
						</td>
						<td align="right">
							<%= expenseCategory.TotalCategoryCost.ConvertToCurrency()%>
						</td>
					</thead>
<%
				foreach(ExpenseItem expenseItem in expenseCategory.ExpenseItems)
				{
%>
					<tr>
						<td>
							<%= expenseItem.Name%>
						</td>
						<td align="right">
							<%= expenseItem.Cost.ConvertToCurrency()%>
						</td>
					</tr>
<%
				}
%>
				</table>
			</li>
<%
			}
%>
		</ul>
		<div>
<%
			foreach(ExpenseCategory expenseCategory in this.Summary.ExpenseInfo.ExpenseCategories)
			{
%>
			<div id="<%= expenseCategory.Name%>" class="invalidTag">
				<div class="tab-content">
					<h3>
						<%=Resources.MyAccount.IncomeAndExpenses_MonthlyVsRecommended%>
					</h3>
					<div id="chart-<%= expenseCategory.Id%>" class="PieChart" chartvalue="<%= this.GetExpenseItemInfo(expenseCategory)%>" style="width: 425px; height: 337px;"></div>
				</div>
			</div>
<%
			}
%>
		</div>
	</div>
<%
		}
		else
		{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NotAvailable%>
	</div>
<%
		}
	}
	else
	{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NeverHadCounseling%>
	</div>
<%
	}
	if(!String.IsNullOrEmpty(PastSummariesUrl))
	{
%>
	<div class="links">
		<p class="link-btn download"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
		<p class="link-btn download"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
	<!-- chart container -->

<script>
var CreditProfileTabInit = true;
var CreditProfileChartInit = true;
var AssetsDisplayName = "<%=Resources.MyAccount.CreditProfile_Assets %>";
var LiabilitiesDisplayName = "<%=Resources.MyAccount.CreditProfile_Liabilities %>";
</script>
</div>
