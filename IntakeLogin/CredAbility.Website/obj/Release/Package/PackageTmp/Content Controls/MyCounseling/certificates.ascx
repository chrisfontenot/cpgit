﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="certificates.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounseling.certificates" %>
<div id="certificates" class="account">
	<table style="width: 100%">
<%
	if(Certificates.Count() == 0)
	 {
%>
		<tr>
			<td>
				<%= Resources.MyAccount.Certificates_NoCertificates %>
			</td>
		</tr>
<%
	}
	foreach(var certificate in Certificates)
	{
%>
		<tr>
			<td style="width: 6%">
				<span class="icon <%= certificate.Type %>">
				</span>
			</td>
			<td style="width: 24%">
				<div class="Name">
					<%= certificate.Name %>
				</div>
				<div>
					<%= Resources.MyAccount.Certificates_DateIssued %><%= certificate.Date.ToShortDateString() %>
				</div>
			</td>
			<td style="width: 46%">
				<span>
					<%= certificate.Description %>
				</span>
			</td>
			<td style="width: 25%">
<%
		if(certificate.HasExpired)
		{
%>
				<span class="submitNextButton">
					<a href=""><%= Resources.MyAccount.Certificates_Schedule %></a>
				</span>
<%
		}
		else if(certificate.HasPdfVersion)
		{
%>
				<span class="submitNextButton">
					<a href="~/Documents/Documents.ashx?id=<%= certificate.Id %>&type=certificate" target="blank"><%= Resources.MyAccount.Certificates_ViewCertificate %></a>
				</span>
<%
		}
		else
		{
%>
				<span class="submitNextButton">
					<a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.Certificates_ViewFullSummary %></a>
				</span>
<%
		}
%>
			</td>
		</tr>
<%
	}
%>
	</table>
<%
	if(PastSummariesUrl != null)
	{
%>
	<div class="links">
		<p class="link-btn download"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
		<p class="link-btn download"><a href="<%= PastSummariesUrl %>"><%=Resources.MyAccount.CreditProfile_ViewPast%></a></p>
	</div>
<%
	}
%>
</div>