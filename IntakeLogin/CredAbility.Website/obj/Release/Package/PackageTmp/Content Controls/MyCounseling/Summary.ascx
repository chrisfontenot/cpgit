﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="CredAbility.Website.Content_Controls.MyCounselingSummary" %>
<%@ Import Namespace="CredAbility.Data.Identity" %>
<div class="account">
<%
	if(ClientProfileRepository.GetLastVisitedWebsite(ClientProfile) != null && ClientProfileRepository.GetLastVisitedWebsite(ClientProfile).PercentComplete >= 100)
	{
		if(this.Summary != null)
		{
%>
	<div class="summary-item">
		<p class="date">
			<%= Resources.MyAccount.MyCounseling_Summary_AsOfDate%><%= Summary.AsOfDate.ToShortDateString()%></p>
	</div>
	<div class="summary-sheet">
		<table width="500" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<td colspan="2">
						<h3>
							<%= Resources.MyAccount.MyCounseling_Summary_MonthlyIncomeExpenses%></h3>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="income">
						<%= Resources.MyAccount.MyCounseling_Summary_Income%>
					</td>
					<td class="income" align="right">
						<%= Summary.PersonalAssetsInfo.TotalIncome.ToString("C")%>
					</td>
				</tr>
				<tr>
					<td class="expenses">
						<%= Resources.MyAccount.MyCounseling_Summary_LivingExpenses%>
					</td>
					<td class="expenses" align="right">
						-<%= Summary.PersonalAssetsInfo.TotalExpenses.ToString("C")%>
					</td>
				</tr>
				<tr>
					<td class="expenses">
						<%= Resources.MyAccount.MyCounseling_Summary_DebtPayment%>
					</td>
					<td class="expenses" align="right">
						-<%= Summary.PersonalAssetsInfo.TotalDebtPayment.ToString("C")%>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2" class="number">
						<%= Resources.MyAccount.MyCounseling_Summary_CashAtMonthEnd%>
						:
						<span>
							<%= Summary.PersonalAssetsInfo.CashAmountMonthEnd.ToString("C")%>
						</span>
					</td>
				</tr>
			</tfoot>
		</table>
		<table width="500" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<td colspan="2">
					<h3>
						<%= Resources.MyAccount.MyCounseling_Summary_NetWorthHead%>
					</h3>
				</td>
			</thead>
			<tbody>
				<tr>
					<td class="income">
						<%= Resources.MyAccount.MyCounseling_Summary_Assets%>
					</td>
					<td class="income" align="right">
						<%= Summary.PersonalAssetsInfo.TotalAssets.ToString("C")%>
					</td>
				</tr>
				<tr>
					<td class="expenses">
						<%= Resources.MyAccount.MyCounseling_Summary_Liabilities%>
					</td>
					<td class="expenses" align="right">
						-<%= Summary.PersonalAssetsInfo.TotalLiabilities.ToString("C")%>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2" class="number">
						<%= Resources.MyAccount.MyCounseling_Summary_NetWorth%>
						:
						<span>
							<%= Summary.PersonalAssetsInfo.TotalNetWorth.ToString("C")%></span>
					</td>
				</tr>
			</tfoot>
		</table>
		<table width="500" border="0" cellpadding="0" cellspacing="0">
			<thead>
				<td>
					<h3>
						<%= Resources.MyAccount.MyCounseling_Summary_CreditScore%></h3>
				</td>
			</thead>
			<tbody>
				<tr>
					<td class="score">
						<%= Summary.Demographics.CreditScore%>
					</td>
				</tr>
			</tbody>
		</table>
<%
			if(PastSummariesUrl != null)
			{
%>
		<div class="links">
			<p class="link-btn download-summary"><a href="<%= GetDocumentSiteUrl %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
			<p class="link-btn download-summary"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
		</div>
<%
			}
%>
	</div>
<%
		}
		else
		{
%>
	<div class="summary-sheet" style="padding-top: 37px;">
		<%= Resources.MyAccount.MyCounseling_Summary_NotAvailable%>
<%
			if(PastSummariesUrl != null)
			{
%>
		<div class="links">
			<p class="link-btn download-summary"><a href="~/Documents/Documents.ashx?id=<%= ClientProfile.DocumentID %>" target="blank"><%= Resources.MyAccount.MyCounseling_Summary_DownloadFullCounseling%></a></p>
			<p class="link-btn view-summary"><a href="<%= PastSummariesUrl %>"><%= Resources.MyAccount.CreditProfile_ViewPast%></a></p>
		</div>
<%
			}
%>
	</div>
<%
		}
	}
	else
	{
%>
	<div>
		<%= Resources.MyAccount.MyCounseling_Summary_NeverHadCounseling%>
	</div>
<%
	}
%>
	<div class="aside">
<%
	if(ActionPlanUrl != null)
	{
%>
		<div class="action-plan">
			<h3>
				<%= Resources.MyAccount.MyCounseling_Summary_ActionPlan%></h3>
			<p>
				<%= Resources.MyAccount.MyCounseling_Summary_ActionPlanText%></p>
			<p class="link">
				<a href="<%= ActionPlanUrl %>">
					<%= Resources.MyAccount.MyCounseling_Summary_SeeCompleteActionPlan%></a></p>
		</div>
<%
	}
	if(MyCertificateUrl != null)
	{
%>
		<div class="my-certificate">
			<h3>
				<%= Resources.MyAccount.MyCounseling_Summary_MyCertificate %>
			</h3>
			<p>
				<%= Resources.MyAccount.MyCounseling_Summary_MyCertificateText %>
			</p>
			<p class="link">
				<a href="<%= MyCertificateUrl %>"><%= Resources.MyAccount.MyCounseling_Summary_ViewYourCertificate %></a>
			</p>
		</div>
<%
	}
%>
	</div>
</div>
<div class="clearboth">
</div>