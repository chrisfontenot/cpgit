﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthorizationForm.ascx.cs" Inherits="CredAbility.Website.Content_Controls.CreditScoreChallenge.AuthorizationForm" %>
<%@ Import Namespace="Resources"%>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div class="form-block">
    <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
    <ca:ResultMessages id="ResultMessages" runat="server"/>

    <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"SSN-REQUIRED", "SSN-WRONGFORMAT"}, "field-block-error") %>">
        <asp:Label ID="Label1" AssociatedControlId="SSNTextBox1" runat="server" ><%=Resources.MyAccount.Csc_SSN %></asp:Label>
        <div class="field-element">
		        <asp:TextBox  class="ssn1" ID="SSNTextBox1" runat="server" Columns="3" MaxLength="3"></asp:TextBox>
                    -
                    <asp:TextBox class="ssn2" ID="SSNTextBox2" runat="server" Columns="2" MaxLength="2"></asp:TextBox>
                    -
                    <asp:TextBox class="ssn3" ID="SSNTextBox3" runat="server" Columns="4" MaxLength="4"></asp:TextBox>
            <div class="field-message"><%= GetMessage(new[] { "SSN-REQUIRED", "SSN-WRONGFORMAT" }, Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    
 <div class="field-block <%= GetCssClassIfMessageCodeExist("STREET_ADDRESS-REQUIRED", "field-block-error") %>" >
        <asp:Label ID="Label6" AssociatedControlId="StreetAddress" runat="server" ><%=Resources.MyAccount.Csc_StreetAddress %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="StreetAddress" Columns="40" name="phone" />
            <div class="field-message"><%= GetMessage("STREET_ADDRESS-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
    <div class="field-block <%= GetCssClassIfMessageCodeExist("CITY-REQUIRED", "field-block-error") %>" >
        <asp:Label ID="Label7" AssociatedControlId="City" runat="server" ><%=Resources.MyAccount.Csc_City %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="City" Columns="40" name="phone" />
            <div class="field-message"><%= GetMessage("CITY-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div> 
    
    <div class="field-block <%= GetCssClassIfMessageCodeExist("STATE-REQUIRED", "field-block-error") %>" >
        <asp:Label ID="Label8" AssociatedControlId="State" runat="server" ><%=Resources.MyAccount.Csc_State %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="State" Columns="40" name="phone" />
            <div class="field-message"><%= GetMessage("STATE-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div> 
        
        <div class="field-block <%= GetCssClassIfMessageCodeExist("ZIP_CODE-REQUIRED", "field-block-error") %>" >
        <asp:Label ID="Label9" AssociatedControlId="ZipCode" runat="server" ><%=Resources.MyAccount.Csc_ZipCode %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="ZipCode" Columns="40" name="phone" />
            <div class="field-message"><%= GetMessage("ZIP_CODE-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div> 
        
    
     <div class="field-block <%= GetCssClassIfMessageCodeExist("PRIMARY_PHONENUMBER-REQUIRED", "field-block-error") %>" >
        <asp:Label ID="Label2" AssociatedControlId="PrimaryPhoneNumber" runat="server" ><%=Resources.MyAccount.Csc_PrimaryPhoneNumber %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="PrimaryPhoneNumber" Columns="40" name="phone" />
            <div class="field-message"><%= GetMessage("PRIMARY_PHONENUMBER-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
        </div>
        <div class="clearboth"></div>
    </div>
    
     <div class="field-block">
        <asp:Label ID="Label3" AssociatedControlId="SecondaryPhoneNumber" runat="server" ><%=Resources.MyAccount.Csc_SecondaryPhone %></asp:Label>
        <div class="field-element">
            <asp:TextBox runat="server" id="SecondaryPhoneNumber" Columns="40" name="phone" />
        </div>
        <div class="clearboth"></div>
    </div>
    
    
    <div style="width: 690px; height: 300px; margin-bottom: 10px; overflow:auto" >
   
   <%= Resources.MyAccount.Csc_Rules%>
    </div>

    <div class="field-block <%= GetCssClassIfMessageCodeExist("IAGREE-REQUIRED", "field-block-error") %>">
        <asp:Label ID="Label4" runat="server" AssociatedControlID="IAgree" Text="&nbsp;"/>
        <div class="field-element">
            <table>
                <tr>
                
                <td valign="top"><asp:CheckBox ID="IAgree" runat="server" />&nbsp;&nbsp;&nbsp;</td>
                <td><asp:Label ID="Label5" runat="server"><%=Resources.MyAccount.Csc_Agree %></asp:Label></td></tr>
            </table>
            
            
            <div class="field-message"><%= GetMessage("IAGREE-REQUIRED")%></div>
       </div>
       <div class="clearboth"></div>
    </div>
      <div class="form-action">
        <span class="submitNextButton"><asp:Button ID="submit" runat="server" OnClick="Submit_Click" Text="<%$Resources:MyAccount, Csc_Submit%>" /></span>
    </div>
</div>