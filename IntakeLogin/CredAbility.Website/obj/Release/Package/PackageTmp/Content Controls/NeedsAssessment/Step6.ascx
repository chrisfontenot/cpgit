﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>

		<div class="list yui3-skin-sam yui-skin-sam">
		
<% using (Html.BeginForm()){%>
<%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
<%--//Step 1--%>
    <%= Html.Hidden("MonthlyRevolvingCreditCardDebt", NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebt)%>
    <%= Html.Hidden("HomeMortgage", NeedsAssessmentViewData.HomeMortgage)%>
    <%= Html.Hidden("OtherDebt", NeedsAssessmentViewData.OtherDebt)%>
    <%= Html.Hidden("InterestedInDMP", NeedsAssessmentViewData.InterestedInDMP)%>
    <%= Html.Hidden("HomePurchase", NeedsAssessmentViewData.HomePurchase)%>
    <%= Html.Hidden("InterestedInReverseMortgage", NeedsAssessmentViewData.InterestedInReverseMortgage)%>
    <%= Html.Hidden("BankruptcyFilingProcess", NeedsAssessmentViewData.BankruptcyFilingProcess)%>
    <%--//Step 2--%>
    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment)%>
    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort)%>
    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard)%>
    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit)%>
    <%--//Step 3--%>
    <%--//Step 4--%>
    <%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount)%>
    <%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment)%>
    <%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest)%>
    <%--//Step 5--%>
    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment)%>
    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment)%>
    <%= Html.Hidden("HomeForclosureThreat",NeedsAssessmentViewData.HomeForclosureThreat)%>
    <%--//Step 7--%>
    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit)%>
    <%= Html.Hidden("DebtOptions", NeedsAssessmentViewData.DebtOptions)%>
    
    
            <div id="assessment-selection">
                <table width="950" border="0" cellpadding="0" cellspacing="0">
	                <tr class="odd">
			                <td width="660" height="60" class="selection">
					                <p><%= NeedsAssessmentViewData.CanMakeHomeDownPaymentText %></p>
			                </td>
			                <td width="160" class="radio">
					                <%= Html.RadioButton("CanMakeHomeDownPayment", true, NeedsAssessmentViewData.GetCanMakeHomeDownPayment.Value == NeedsAssessmentYesNo.Yes.Value, new { id = "CanMakeHomeDownPaymentYes" })%>
					                <label for="CanMakeHomeDownPaymentYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					                <%= Html.RadioButton("CanMakeHomeDownPayment", false, NeedsAssessmentViewData.GetCanMakeHomeDownPayment.Value == NeedsAssessmentYesNo.No.Value, new { id = "CanMakeHomeDownPaymentNo" })%>
					                <label for="CanMakeHomeDownPaymentNo"><%= Resources.Website.NeedAssessment_No %></label>
			                </td>
			                
	                </tr>
	            </table>
            </div>
    
    <div class="previousNextButtons">
	<span class="submitBackButton">
		<%= Html.SubmitButton("GoToPrevStep", NeedsAssessmentViewData.PreviousButtonText)%>
	</span>
	<span class="submitNextButton">
		<%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%>
	</span>
    </div>
<% } %>

        </div>