﻿<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData.NeedsAssessmentStep2Questions"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>


		<div class="list yui3-skin-sam yui-skin-sam">
			
<% using (Html.BeginForm()){%>

    <%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
    <%--//Step 1--%>
    <%= Html.Hidden("MonthlyRevolvingCreditCardDebt", NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebt)%>
    <%= Html.Hidden("HomeMortgage", NeedsAssessmentViewData.HomeMortgage)%>
    <%= Html.Hidden("OtherDebt", NeedsAssessmentViewData.OtherDebt)%>
    <%= Html.Hidden("InterestedInDMP", NeedsAssessmentViewData.InterestedInDMP)%>
    <%= Html.Hidden("HomePurchase", NeedsAssessmentViewData.HomePurchase)%>
    <%= Html.Hidden("InterestedInReverseMortgage", NeedsAssessmentViewData.InterestedInReverseMortgage)%>
    <%= Html.Hidden("BankruptcyFilingProcess", NeedsAssessmentViewData.BankruptcyFilingProcess)%>
    <%--//Step 3--%>
    <%--//Step 4--%>
    <%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount)%>
    <%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment)%>
    <%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest)%>
    <%--//Step 5--%>
    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment)%>
    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment)%>
    <%= Html.Hidden("HomeForclosureThreat",NeedsAssessmentViewData.HomeForclosureThreat)%>
    <%--//Step 6--%>
    <%= Html.Hidden("CanMakeHomeDownPayment", NeedsAssessmentViewData.CanMakeHomeDownPayment)%>
    <%--//Step 7--%>
    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit)%>
    <%= Html.Hidden("DebtOptions", NeedsAssessmentViewData.DebtOptions)%>
    
    
            <div class="item">
					<p><%= HowTimelyCreditCardPayment.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment, new { Class = "slider", max = HowTimelyCreditCardPayment.Count()})%>
					    <span id="HowTimelyCreditCardPayment-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:20%; text-align:left"><%= HowTimelyCreditCardPayment.AlwaysOnTime.Text%></td>
						<td style="width:20%; text-align:left"><%= HowTimelyCreditCardPayment.UsuallyOnTime.Text%></td>
						<td style="width:20%; text-align:center"><%= HowTimelyCreditCardPayment.AbitLate.Text%></td>
						<td style="width:20%; text-align:right"><%= HowTimelyCreditCardPayment.DaysBehind30.Text%></td>
						<td style="width:20%; text-align:right"><%= HowTimelyCreditCardPayment.DaysBehind60.Text%></td>
					</tr>
					</table>
					</div>
			</div>
			<div class="item">
					<p><%= MonthlyPaymentSort.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort, new { Class = "slider", max = MonthlyPaymentSort.Count() })%>
					    <span id="MonthlyPaymentSort-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:20%; text-align:left"><%= MonthlyPaymentSort.PayCompleteBalance.Text%></td>
						<td style="width:20%; text-align:left"><%= MonthlyPaymentSort.MoreThanMinimum.Text%></td>
						<td style="width:20%; text-align:center"><%= MonthlyPaymentSort.MinimumBalance.Text%></td>
						<td style="width:20%; text-align:right"><%= MonthlyPaymentSort.LessThanTheMinimum.Text%></td>
						<td style="width:20%; text-align:right"><%= MonthlyPaymentSort.NotPayingAnything.Text%></td>
					</tr>
					</table>
					</div>
			</div>
			<div class="item">
					<p><%= PayDebtWithCreditCard.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard, new { Class = "slider", max = PayDebtWithCreditCard.Count() })%>
					    <span id="PayDebtWithCreditCard-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:33%; text-align:left"><%= PayDebtWithCreditCard.EveryMonth.Text%></td>
						<td style="width:33%; text-align:center"><%= PayDebtWithCreditCard.Sometimes.Text%></td>
						<td style="width:33%; text-align:right"><%= PayDebtWithCreditCard.Never.Text%></td>
					</tr>
					</table>
					</div>
			</div>
			<div class="item">
					<p><%= NearCreditCardLimit.GetQuestion() %></p>
					<div style="text-align:center">
					    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit, new { Class = "slider", max = NearCreditCardLimit.Count() })%>
					    <span id="NearCreditCardLimit-slider"></span>
					</div>
					<div class="scale">
					<table style="width:930px">
					<tr>
						<td style="width:20%; text-align:left"><%= NearCreditCardLimit.DontKnow.Text%></td>
						<td style="width:20%; text-align:left"><%= NearCreditCardLimit.FarFromLimit.Text%></td>
						<td style="width:20%; text-align:center"><%= NearCreditCardLimit.SomewhatFar.Text%></td>
						<td style="width:20%; text-align:right"><%= NearCreditCardLimit.NearLimit.Text%></td>
						<td style="width:20%; text-align:right"><%= NearCreditCardLimit.MaxedOut.Text%></td>
					</tr>
					</table>
					</div>
			</div>
    
    <div class="previousNextButtons">
	<span class="submitBackButton">
		<%= Html.SubmitButton("GoToPrevStep", NeedsAssessmentViewData.PreviousButtonText)%>
	</span>
	<span class="submitNextButton">
		<%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%>
	</span>
    </div>
<% } %>

        </div>