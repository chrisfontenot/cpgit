﻿<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<% var NeedsAssessmentViewData = Model as NeedsAssessmentViewData; %>
<% using (Html.BeginForm()){%>
    <%--//Step 2--%>
    <%= Html.Hidden("CurrentStep", NeedsAssessmentViewData.CurrentStep)%>
    <%= Html.Hidden("HowTimelyCreditCardPayment", NeedsAssessmentViewData.HowTimelyCreditCardPayment)%>
    <%= Html.Hidden("MonthlyPaymentSort", NeedsAssessmentViewData.MonthlyPaymentSort)%>
    <%= Html.Hidden("PayDebtWithCreditCard", NeedsAssessmentViewData.PayDebtWithCreditCard)%>
    <%= Html.Hidden("NearCreditCardLimit", NeedsAssessmentViewData.NearCreditCardLimit)%>
    <%--//Step 3--%>
    <%--//Step 4--%>
    <%= Html.Hidden("TotalDebtAmount", NeedsAssessmentViewData.TotalDebtAmount)%>
    <%= Html.Hidden("TotalMonthlyPayment", NeedsAssessmentViewData.TotalMonthlyPayment)%>
    <%= Html.Hidden("AverageInterest", NeedsAssessmentViewData.AverageInterest)%>
    <%--//Step 5--%>
    <%= Html.Hidden("ConcernedAboutMortgagePayment", NeedsAssessmentViewData.ConcernedAboutMortgagePayment)%>
    <%= Html.Hidden("TimelyHomeMortgagePayment", NeedsAssessmentViewData.TimelyHomeMortgagePayment)%>
    <%= Html.Hidden("HomeForclosureThreat",NeedsAssessmentViewData.HomeForclosureThreat)%>
    <%--//Step 6--%>
    <%= Html.Hidden("CanMakeHomeDownPayment", NeedsAssessmentViewData.CanMakeHomeDownPayment)%>
    <%--//Step 7--%>
    <%= Html.Hidden("IsUserReadyToCommit", NeedsAssessmentViewData.IsUserReadyToCommit)%>
    <%= Html.Hidden("DebtOptions", NeedsAssessmentViewData.DebtOptions)%>
    
		<div id="assessment-selection">
            <table width="950" border="0" cellpadding="0" cellspacing="0">
	            <tr class="odd">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.MonthlyRevolvingCreditCardDebtText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("MonthlyRevolvingCreditCardDebt", true, NeedsAssessmentViewData.GetMonthlyRevolvingCreditCardDebt.Value == NeedsAssessmentYesNo.Yes.Value,new {id="MonthlyRevolvingCreditCardDebtYes",iconClass="credit-card", Class="radio yes"})%>
					            <label for="MonthlyRevolvingCreditCardDebtYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("MonthlyRevolvingCreditCardDebt", false, NeedsAssessmentViewData.GetMonthlyRevolvingCreditCardDebt.Value == NeedsAssessmentYesNo.No.Value,new {id="MonthlyRevolvingCreditCardDebtNo",iconClass="credit-card", Class="radio no"})%>
					            <label for="MonthlyRevolvingCreditCardDebtNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="credit-card<%= NeedsAssessmentViewData.GetMonthlyRevolvingCreditCardDebt.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
	            <tr class="even">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.HomeMortgageText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("HomeMortgage", true, NeedsAssessmentViewData.GetHomeMortgage.Value == NeedsAssessmentYesNo.Yes.Value,new {id="HomeMortgageYes",iconClass="home-mortgage", Class="radio yes"})%>
					            <label for="HomeMortgageYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("HomeMortgage", false, NeedsAssessmentViewData.GetHomeMortgage.Value == NeedsAssessmentYesNo.No.Value,new {id="HomeMortgageNo",iconClass="home-mortgage", Class="radio no"})%>
					            <label for="HomeMortgageNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="home-mortgage<%= NeedsAssessmentViewData.GetHomeMortgage.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
	            <tr class="odd">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.OtherDebtText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("OtherDebt", true, NeedsAssessmentViewData.GetOtherDebt.Value == NeedsAssessmentYesNo.Yes.Value,new {id="OtherDebtYes",iconClass="other-debt", Class="radio yes"})%>
					            <label for="OtherDebtYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("OtherDebt", false, NeedsAssessmentViewData.GetOtherDebt.Value == NeedsAssessmentYesNo.No.Value,new {id="OtherDebtNo",iconClass="other-debt", Class="radio no"})%>
					            <label for="OtherDebtNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="other-debt<%= NeedsAssessmentViewData.GetOtherDebt.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
	            <tr class="even">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.InterestedInDMPText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("InterestedInDMP", true, NeedsAssessmentViewData.GetInterestedInDMP.Value == NeedsAssessmentYesNo.Yes.Value,new {id="InterestedInDMPYes",iconClass="DMP", Class="radio yes"})%>
					            <label for="InterestedInDMPYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("InterestedInDMP", false, NeedsAssessmentViewData.GetInterestedInDMP.Value == NeedsAssessmentYesNo.No.Value,new {id="InterestedInDMPNo",iconClass="DMP", Class="radio no"})%>
					            <label for="InterestedInDMPNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="DMP"></span></td>
	            </tr>
	            <tr class="odd">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.HomePurchaseText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("HomePurchase", true, NeedsAssessmentViewData.GetHomePurchase.Value == NeedsAssessmentYesNo.Yes.Value,new {id="HomePurchaseYes",iconClass="home-purchase", Class="radio yes"})%>    
					            <label for="HomePurchaseYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("HomePurchase", false, NeedsAssessmentViewData.GetHomePurchase.Value == NeedsAssessmentYesNo.No.Value,new {id="HomePurchaseNo",iconClass="home-purchase", Class="radio no"})%>
					            <label for="HomePurchaseNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="home-purchase<%= NeedsAssessmentViewData.GetHomePurchase.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
	            <tr class="even">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.InterestedInReverseMortgageText %></p>
			            </td>
			            <td width="160" class="radio">						
                                <%= Html.RadioButton("InterestedInReverseMortgage", true, NeedsAssessmentViewData.GetInterestedInReverseMortgage.Value == NeedsAssessmentYesNo.Yes.Value,new {id="InterestedInReverseMortgageYes",iconClass="reverse-mortgage", Class="radio yes"})%>
					            <label for="InterestedInReverseMortgageYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("InterestedInReverseMortgage", false, NeedsAssessmentViewData.GetInterestedInReverseMortgage.Value == NeedsAssessmentYesNo.No.Value,new {id="InterestedInReverseMortgageNo",iconClass="reverse-mortgage", Class="radio no"})%>
					            <label for="InterestedInReverseMortgageNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="reverse-mortgage<%= NeedsAssessmentViewData.GetInterestedInReverseMortgage.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
	            <tr class="odd">
			            <td width="660" height="60" class="selection">
					            <p><%= NeedsAssessmentViewData.BankruptcyFilingProcessText %></p>
			            </td>
			            <td width="160" class="radio">
					            <%= Html.RadioButton("BankruptcyFilingProcess", true, NeedsAssessmentViewData.GetBankruptcyFilingProcess.Value == NeedsAssessmentYesNo.Yes.Value,new {id="BankruptcyFilingProcessYes",iconClass="bankruptcy", Class="radio yes"})%>
					            <label for="BankruptcyFilingProcessYes"><%= Resources.Website.NeedAssessment_Yes %></label>
					            <%= Html.RadioButton("BankruptcyFilingProcess", false, NeedsAssessmentViewData.GetBankruptcyFilingProcess.Value == NeedsAssessmentYesNo.No.Value,new {id="BankruptcyFilingProcessNo",iconClass="bankruptcy", Class="radio no"})%>
					            <label for="BankruptcyFilingProcessNo"><%= Resources.Website.NeedAssessment_No %></label>
			            </td>
			            <td width="130" class="icon"><span class="bankruptcy<%= NeedsAssessmentViewData.GetBankruptcyFilingProcess.Value == NeedsAssessmentYesNo.Yes.Value?" active":""%>"></span></td>
	            </tr>
            </table>
		</div>  
    
    
    <div class="previousNextButtons"><span class="submitNextButton"><%= Html.SubmitButton("GoToNextStep", NeedsAssessmentViewData.NextButtonText)%></span></div>
<% } %>
<script>
    var YESNOInit = true;
    var WarningMessage = "<%=Resources.Website.NeedAssessment_AllYesAllNo %>";
</script>