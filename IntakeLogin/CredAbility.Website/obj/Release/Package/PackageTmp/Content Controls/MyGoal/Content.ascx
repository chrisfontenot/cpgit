﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="CredAbility.Core.MyGoal" %>
<%@ Import Namespace="CredAbility.Web.UI.ToStringHelpers" %>
<%@ Import Namespace="System.Collections.Generic"%>

<% var Goals = Model as List<Goal>; %>
<div class="goals">
	<% foreach (var goal in Goals){ %>
	<div class="goal-item">
		<div class="name"><%= goal.Name %> <span class="amount-due">(<%= goal.TotalAmount.ConvertToCurrency() %>)</span></div>
		<div class="link-buttons"><a onclick="Goal.AddHistory(<%= goal.Id %>);"><%= Resources.Website.Goal_UpdateDelete %></a></div>
		<div class="clearboth"></div>
		<div class="amount">
			<div class="total-amount<%= goal.AmountUptoDate < 0?" red":"" %>" style="width:<%= goal.AmountUptoDate.ConvertToCurrency().Length > ((((goal.AmountUptoDate/goal.TotalAmount)* 100) - 1) /2) ? "" : (((goal.AmountUptoDate/goal.TotalAmount)* 100) - 1).ToString() %>%"><%= goal.AmountUptoDate.ConvertToCurrency() %></div>
		</div>
	</div>
	<% } %>
</div>
