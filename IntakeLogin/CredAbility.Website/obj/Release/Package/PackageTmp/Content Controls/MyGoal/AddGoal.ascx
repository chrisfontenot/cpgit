﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="AddGoal">
	<form id="AddGoalForm">
	
		<div class="Title"><%= Resources.Website.Goal_AddAGoal %></div>
		<div class="resultMessage"><%= ViewData["resultMessages"] != null?((ResultMessages)ViewData["resultMessages"]).RenderControl():"" %></div>
		<div class="Name"><%= Resources.Website.Goal_Name %><%= Html.TextBox("Name") %></div>
		<div class="Want"><%= Resources.Website.Goal_IWant %> <%= Html.DropDownList("Type") %> $<%= Html.TextBox("TotalAmount") %><%= Resources.Website.Goal_By %><%= Html.TextBox("DueDate","", new {Class = "CalendarBox", disabled="disabled"}) %><a class="calendarIcon" onclick="Goal.showCalendar();"></a></div>
		
		<div class="buttons">
			<span class="submitNextButton"><a onclick="Goal.SaveGoal();"><%= Resources.Website.Goal_Add %></a></span>
			<span class="submitNextButton"><a onclick="Goal.overlay.hide();"><%= Resources.Website.Goal_Cancel %></a></span>
		</div>
	
	</form>

</div>