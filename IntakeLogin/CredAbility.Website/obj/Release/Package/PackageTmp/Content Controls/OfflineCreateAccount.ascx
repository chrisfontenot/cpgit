﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="OfflineCreateAccount.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.OfflineCreateAccount" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div id="offline-create-account" style="margin-bottom: 50px;">        
     <div class="form-block">
      <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
      <ca:ResultMessages ID="ResultMessages" runat="server"></ca:ResultMessages>
      
        <div class="field-block <%= GetCssClassIfMessageCodeExist("CLIENTID-REQUIRED", "field-block-error") %>">
                <asp:Label runat="server" Text="<%$Resources:Website, OfflineProfile_ClientID%>" AssociatedControlID="ClientIDTextBox"></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="ClientIDTextBox" runat="server" Columns="40" ></asp:TextBox>
                     <a href="#" id="show1" class="CreateAccountTextLink" title="Client ID"><img src="/static/images/layout/ico-questionmark.gif"/></a> 
                    <div class="clientIDInfo" id="myPanel" style="visiblity:hidden;display:none">
                    <div id="panel1">
                        <div class="hd">Client ID</div>
                        <div class="bd">
                           <iframe src="/global/clientid-info.aspx" frameborder="0"></iframe>
                        </div>
                    </div>
                    </div>
                    <div class="field-message"><%= GetMessage("CLIENTID-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                <div class="clearboth"></div>
            </div>
            
            <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"SSN-REQUIRED","SSN-WRONGFORMAT"}, "field-block-error") %>">
                <asp:Label ID="Label1" runat="server" Text="<%$Resources:Website, OfflineProfile_SSN%>" AssociatedControlID="SSNTextBox1"></asp:Label>
                
                <div class="field-element">
                    <asp:TextBox  class="ssn1" ID="SSNTextBox1"  runat="server" Columns="3" MaxLength="3" ></asp:TextBox>
                    -
                    <asp:TextBox class="ssn2" ID="SSNTextBox2"  runat="server" Columns="2" MaxLength="2" ></asp:TextBox>
                    -
                    <asp:TextBox class="ssn3" ID="SSNTextBox3"  runat="server" Columns="4" MaxLength="4" ></asp:TextBox>
                    <div class="field-message"><%= GetMessage(new[] { "SSN-REQUIRED", "SSN-WRONGFORMAT" }, Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                <div class="clearboth"></div>
            </div>
     
        <div class="form-action">
                <span class="submitNextButton"><asp:Button ID="Button1" runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:Website, Global_Next%>"></asp:Button></span>
        </div>
    </div>
    
    
    <div>
        
       
    </div>
</div>
    
    
    <script>
var ClientIDInit = true;
</script>
    
    
    
   
        