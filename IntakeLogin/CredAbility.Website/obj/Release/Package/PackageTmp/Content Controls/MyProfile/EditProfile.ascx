﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditProfile.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.EditProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:ScriptManager ID="ScriptManager" runat="server">
</asp:ScriptManager>
    <div>
        
        <ca:ResultMessages ID="ResultMessages" runat="server" CssClass="AccountFormErrorMessage"></ca:ResultMessages>
    </div>

<div id="edit-profile">
    <div class="button">
            <div class="submitNextButton"><a href="/my-account/my-profile/default.aspx"><%=Resources.MyAccount.Profile_CancelButton%></a></div><div class="submitNextButton"><asp:LinkButton ID="LinkButton3" OnClick="SaveProfileButton_Click" runat="server"><%=Resources.MyAccount.Profile_SaveProfileButton%></asp:LinkButton></div>
    </div>
        
	<div class="account-info clearfix">
		<h3><%= Resources.MyAccount.Profile_AccountInformation%></h3>
		
		<div class="avatar"> 
		    <asp:Image ID="AvatarImage" runat="server" AlternateText="Avatar" /> <asp:LinkButton id="ChangeAvatarButton" runat="server"><%=Resources.MyAccount.Profile_ChangeAvatar%></asp:LinkButton> 
		</div>
		
		<div class="personal-info">
		
		    <div class="item user-name">
		            <label><%= Resources.MyAccount.Profile_Username %></label>
					
				    <asp:Label ID="UsernameLabel" runat="server"></asp:Label>
					
		    </div>
		    <div class="item first-name float">
				    <label><%=Resources.MyAccount.Profile_FirstName %></label>
				    <asp:TextBox ID="FirstNameTextBox" runat="server" ReadOnly="true"></asp:TextBox>
				    
		    </div>
		    <div class="item float middle-initial">
				    <label><%=Resources.MyAccount.Profile_MiddleInitial %></label>
				    <asp:TextBox class="edit-middle-initial" ID="MiddleInitialTextBox"  ReadOnly="true" runat="server"></asp:TextBox>
		    </div>
		    <div class="item last-name float">
				    <label><%=Resources.MyAccount.Profile_LastName %></label>
				    <asp:TextBox ID="LastNameTextBox" runat="server"  ReadOnly="true"></asp:TextBox>
		    </div>
		    
		    <div class="item long">
	            <label><%=Resources.MyAccount.Profile_SSN%></label>
			        <asp:TextBox  class="ssn1" ID="SSNTextBox1" runat="server" MaxLength="3"  ReadOnly="true"></asp:TextBox>
                    -
                    <asp:TextBox class="ssn2" ID="SSNTextBox2" runat="server" MaxLength="2"  ReadOnly="true"></asp:TextBox>
                    -
                    <asp:TextBox class="ssn3" ID="SSNTextBox3" runat="server" MaxLength="4"  ReadOnly="true"></asp:TextBox>
		    </div>
		    
		    
		    <div class="item long">
	            <label><%=Resources.MyAccount.Profile_MaritalStatus%></label>
			        <asp:DropDownList ID="MaritalStatusDDL" runat="server"></asp:DropDownList>
		    </div>
		    
		     <div class="item long">
	            <label><%=Resources.MyAccount.Profile_LanguagePreference%></label>
	            
			        <asp:DropDownList ID="DDLLanguage" runat="server">
                    </asp:DropDownList>
                
		    </div>
		</div>
		
		
		
	</div>
	<div class="mail-password clearfix">
	    <h3><%= Resources.MyAccount.Profile_EmailAndPassword%></h3>
	    
	        <div class="clear">
    	    <div class="item mail-view">
    	         <label><%=Resources.MyAccount.Profile_EmailAddress%></label>
    	        <span><%= this.userDetail == null ? string.Empty : this.userDetail.Email%></span>
    	    </div>
        	
    	    <div class="submitNextButton save"><a href="change-password.aspx"><%=Resources.MyAccount.Profile_ChangePassword %></a></div>
    	</div>    	
    	
    	
    	<div class="clear">
	        <div class="item mail-edit">
			        <label><%=Resources.MyAccount.Profile_NewEmail%></label>
			        <asp:TextBox ID="NewEmailTextBox" runat="server"></asp:TextBox>
	        </div>
        	
        	
	        <div class="item mail-edit">
	            <label><%=Resources.MyAccount.Profile_ConfirmNewEmail%></label>
                <asp:TextBox ID="ConfirmNewEmailTextBox" runat="server"></asp:TextBox>
            </div>
        </div>
    	
    	
    	<div class="clear">
	        <div class="item question-edit">
	            <label><%=Resources.MyAccount.Profile_EditQuestion1%></label>
	            <div class="selectPadding">
                    <asp:DropDownList ID="Question1DDL" runat="server"></asp:DropDownList>
                </div>

                <asp:TextBox ID="Question1AnswerTextBox" runat="server"></asp:TextBox>
            </div>
        </div>
	</div>
	
	<div class="address-phone clearfix">
		<h3><%=Resources.MyAccount.Profile_MailingAddressAndPhone%></h3>
		<div class="clear">
		    <div class="item home">
					    <label><%=Resources.MyAccount.Profile_HomePhone%></label>
					    <asp:TextBox ID="HomePhoneTextBox" runat="server"></asp:TextBox>
		    </div>
		    <div class="item work">
					    <label><%=Resources.MyAccount.Profile_WorkPhone%></label>
					    <asp:TextBox ID="WorkPhoneTextBox" runat="server"></asp:TextBox>
    					
		    </div><div class="item cell">
					    <label><%=Resources.MyAccount.Profile_CellPhone%></label>
					    <asp:TextBox ID="CellPhoneTextBox" runat="server"></asp:TextBox>
		    </div>
		
		</div>
		
		<div class="address">
		
		    <div class="clear">
		        <div class="item address">
					        <label><%=Resources.MyAccount.Profile_MailingAddress%></label>
					        <asp:TextBox ID="MailingAddressLine1TextBox" runat="server"></asp:TextBox>
		        </div>
		        <div class="item address2">
					        <label><%=Resources.MyAccount.Profile_MailingAddressLine2%></label>
					        <asp:TextBox ID="MailingAddressLine2TextBox" runat="server"></asp:TextBox>
		        </div>
		    
		    </div>
		    
		    <div class="clear">
		        <div class="item city">
					        <label><%=Resources.MyAccount.Profile_City%></label>
					        <asp:TextBox ID="CityTextBox" runat="server"></asp:TextBox>
		        </div>
		        <div class="item state">
					        <label><%=Resources.MyAccount.Profile_State%></label>
					        <asp:DropDownList ID="StateDDL" runat="server"></asp:DropDownList>
		        </div>
		        <div class="item zip">
					        <label><%=Resources.MyAccount.Profile_Zip%></label>
					        <asp:TextBox ID="ZipTextBox" runat="server"></asp:TextBox>
		        </div>
		    </div>
		</div>
	</div>
	
	<div class="button">
		<div class="submitNextButton"><a href="default.aspx"><%=Resources.MyAccount.Profile_CancelButton%></a></div><div class="submitNextButton"><asp:LinkButton ID="LinkButton2" OnClick="SaveProfileButton_Click" runat="server"><%=Resources.MyAccount.Profile_SaveProfileButton%></asp:LinkButton></div>
	</div>
	
</div>
				
				
<asp:Panel ID="AvatarModalPanel" runat="server" CssClass="AccountModalPopup">
<asp:Panel ID="AvatarPanel" runat="server"></asp:Panel>
</asp:Panel>
<ajaxtoolkit:ModalPopupExtender ID="AvatarModal" runat="server" TargetControlID="ChangeAvatarButton"
    PopupControlID="AvatarModalPanel" BackgroundCssClass="AccountModalBackground"/>


