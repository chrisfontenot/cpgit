﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ForgotPassword.ascx.cs"
    Inherits="CredAbility.Website.Content_Controls.ForgotPassword" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>

<div id="forgot-password" style="margin-bottom: 50px;">        
    
    <asp:Panel ID="GetUsernamePanel" runat="server">
        <div class="form-block">
            <ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow1" />
            <ca:ResultMessages ID="ResultMessagesStep1" runat="server"></ca:ResultMessages>
           <div class="field-block <%= GetCssClassIfMessageCodeExist("USERNAME-REQUIRED", "field-block-error") %>">
                <asp:Label ID="Username" runat="server" Text="<%$Resources:MyAccount, ForgotPassword_Username%>" AssociatedControlID="UsernameTextBox"></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="UsernameTextBox" runat="server" Columns="40" ></asp:TextBox>
                    <div class="field-message"><%= GetMessage("USERNAME-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                <div class="clearboth"></div>
            </div>
            
            
            <div class="form-action">
                <span class="submitNextButton"><asp:Button ID="Button1" runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:MyAccount, ForgotPassword_Submit%>"></asp:Button></span>
            </div>
        </div>
    </asp:Panel>
    
    
    
    
    
    <asp:Panel ID="ContactCustomerServicePanel" runat="server">
        <div class="form-block">
            <%

                
                if(_displayAccountLockedMsg)
                {
                    Response.Write(Resources.MyAccount.ForgotUsername_UserLocked);
                }
            else
                {
                    Response.Write(Resources.MyAccount.ForgotPassword_ContactCustomerService);
                }
            %>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="SecurityQuestionsPanel" runat="server">
        <div  class="form-block">
            <ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow2" />
            <ca:ResultMessages ID="ResultMessagesStep2" runat="server"></ca:ResultMessages>
        
            <div class="field-block">
                <label><%=Resources.MyAccount.ForgotPassword_SecurityQuestion1%></label>
                <div class="field-element" style="font-weight:bold">
                    <asp:Literal ID="Question1Label" runat="server"/>
                </div>
                <div class="clearboth"></div>
            </div>     
            <div class="field-block <%= GetCssClassIfMessageCodeExist("ANSWER1-REQUIRED", "field-block-error") %>">
                <asp:Label ID="Label1" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_SecurityAnswer1%> " AssociatedControlID="Question1AnswerTextBox" ></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="Question1AnswerTextBox" runat="server" Columns="40"></asp:TextBox>
                    <div class="field-message"><%= GetMessage("ANSWER1-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                 <div class="clearboth"></div>
            </div>
                        
             <div class="form-action">
                <span class="submitNextButton">
                    <asp:Button ID="Button2"  runat="server" OnClick="SecurityQuestionsSubmitButton_Click" Text="<%$Resources:MyAccount, ForgotPassword_Submit%>"></asp:Button>
                </span>
            </div>
        </div>
    </asp:Panel>
     <asp:Panel ID="ResetPasswordPanel" runat="server">
        <div  class="form-block">
        <ca:ErrorSeeBelow runat="server" ID="ErrorSeeBelow3" />
            <ca:ResultMessages ID="ResultMessagesStep3" runat="server"></ca:ResultMessages>
        
            <div class="field-block"><asp:Literal runat="server" Text="<%$Resources:MyAccount,ForgotPassword_HeaderStep3 %>"/></div>
        
            <div class="field-block">
                <asp:Label ID="Label3" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_NewPassword%> " AssociatedControlID="NewPassswordTextBox" ></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="NewPassswordTextBox" runat="server" TextMode="Password" Columns="40"></asp:TextBox>
                     <a href="#" id="show1" class="CreateAccountTextLink" title="<%= Resources.MyAccount.Global_PasswordRules %>"><img src="/static/images/layout/ico-questionmark.gif"/></a> 
                     <div class="passwordRules" id="myPanel" style="visiblity:hidden;display:none">
                        <div id="panel1">
                            <div class="hd"><%= Resources.MyAccount.Global_PasswordRules %></div>
                            <div class="bd">
                                <iframe src="/global/password-rules.aspx" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                     <div class="field-message"><%= GetMessage("NEWPWD-REQUIRED", Resources.Website.ContactUs_RequiredField)%></div>
                </div>
            
                <div class="clearboth"></div>
            </div>
            <div class="field-block <%= GetCssClassIfMessageCodeExist(new []{"CONFIRMPWD-REQUIRED","CONFIRMPWD-NOMATCH"}, "field-block-error") %>">
                <asp:Label ID="Label4" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_RetypePassword%> " AssociatedControlID="RetypeNewPasswordTextBox" ></asp:Label>
                <div class="field-element">
                    <asp:TextBox ID="RetypeNewPasswordTextBox" runat="server" TextMode="Password" Columns="40"></asp:TextBox>
                     <div class="field-message"><%= GetMessage(new []{"CONFIRMPWD-REQUIRED","CONFIRMPWD-NOMATCH"}, Resources.Website.ContactUs_RequiredField)%></div>
                </div>
                <div class="clearboth"></div>
            </div>
       
        
           <div class="form-action">
                <span class="submitNextButton">
                    <asp:Button   runat="server" OnClick="ResetPasswordButton_Click" Text="<%$Resources:MyAccount, ForgotPassword_Submit%>"></asp:Button>
                </span>
            </div>
        </div>
        
        
    </asp:Panel>
    
</div>
<script>
var PasswordRuleInit = true;
</script>
        