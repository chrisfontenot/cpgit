﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MustChangeUsername.ascx.cs" Inherits="CredAbility.Website.Content_Controls.Authentification.MustChangeUsername" %>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>

<div id="change-username">

    
     <ca:ResultMessages ID="ResultMessages"  runat="server" CssClass="AccountFormErrorMessage"></ca:ResultMessages>
    
    <div class="username-info">
        <asp:UpdatePanel runat="server" ID="CheckUsernameUpdatePanel">
            <ContentTemplate>
                <div class="field-block">
                    <asp:Label ID="UsernameLabel" runat="server" CssClass="AccountFormLabel" Text="<%$Resources:MyAccount, ChangeUsername_CreateUsername%>" AssociatedControlID="UsernameTextBox"></asp:Label>
                    <div class="field-element">
                        <asp:TextBox ID="UsernameTextBox" runat="server" CssClass="AccountFormTextBox" AutoPostBack="true" OnTextChanged="Check_Username"></asp:TextBox>
                            
                    
                        <asp:Label ID="UsernameAvailableLabel" runat="server"></asp:Label>
                    </div>
                    
                </div>
                
                <div class="field-block">
                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_NewPassword%> " AssociatedControlID="NewPassswordTextBox" ></asp:Label>
                    <div class="field-element">
                        <asp:TextBox ID="NewPassswordTextBox" runat="server" TextMode="Password" Columns="40"></asp:TextBox>
                         <a href="#" id="show1" class="CreateAccountTextLink" title="Password Rules"><img src="/static/images/layout/ico-questionmark.gif"/></a> 
            
                       <div class="passwordRules" id="myPanel" style="visiblity:hidden;display:none">
                            <div id="panel1">
                                <div class="hd">Password rules</div>
                                <div class="bd">
                                    <iframe src="/global/password-rules.aspx" frameborder="0"></iframe>
                                </div>
                            </div>

                        </div>
                     </div>
                </div>
                <div class="field-block">
                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:MyAccount,ForgotPassword_RetypePassword%> " AssociatedControlID="RetypeNewPasswordTextBox" ></asp:Label>
                    <div class="field-element">
                        <asp:TextBox ID="RetypeNewPasswordTextBox" runat="server" TextMode="Password" Columns="40"></asp:TextBox>
                       
                    </div>
                </div>
                
                
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <div class="clear">
	        <div class="item question-edit">
                <asp:Label ID="EditChallengeQuestion1Label" runat="server" Text="<%$Resources:MyAccount, Profile_EditQuestion1%>"   AssociatedControlID="Question1DDL"></asp:Label>
                
                <div class="selectPadding">
                    <asp:DropDownList ID="Question1DDL" runat="server"></asp:DropDownList>
                </div>
            
                
                <label><%  =Resources.MyAccount.ChangeUsername_Answer %></label>
                
                <asp:TextBox ID="Question1AnswerTextBox" runat="server"></asp:TextBox>
                
            </div>
        </div>
    
    </div>
    <div class="button">
        <div class="submitNextButton"><asp:LinkButton ID="SaveProfileButton" runat="server" CssClass="rounded_button" BorderStyle="None" OnClick="SaveProfileButton_Click"><%  =Resources.MyAccount.ChangeUsername_SaveProfile %></asp:LinkButton></div>
    </div>    


</div>
<script>
var PasswordRuleInit = true;
</script>
