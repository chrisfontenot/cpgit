﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NeedZip.ascx.cs" Inherits="CredAbility.Website.Content_Controls.NeedZip" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div class="form-block">
    <ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
    <ca:ResultMessages id="ResultMessages" runat="server"/>
    <div class="field-block <%= GetCssClassIfMessageCodeExist("BAD-ZIP", "field-block-error") %>">
        <asp:Label runat="server" AssociatedControlID="ZipCode" Text="<%$Resources:Website, CredU_Zipcode%>"/>
        <div class="field-element">
            <asp:TextBox TabIndex="1"  ID="ZipCode" runat="server" Columns="5"/>
            <div class="field-message"><%= GetMessage("BAD-ZIP", Resources.Website.ContactUs_RequiredField)%></div>
       </div>
       <div class="clearboth"></div>
    </div>
    <div class="form-action"  >
        <span class="submitNextButton"><asp:Button runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:Website,CredU_Submit %>" /></span>
    </div>
</div>