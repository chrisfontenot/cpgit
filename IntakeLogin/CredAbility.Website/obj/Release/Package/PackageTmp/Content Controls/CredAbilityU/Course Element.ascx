﻿<%@ Import Namespace="Sitecore.Links"%>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.Item.Interfaces"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Course Element.ascx.cs" Inherits="CredAbility.Website.Content_Controls.CredAbilityU.Course_Element" %>
<div class="course-element">
            <!--<div class="category">[Category]</div>-->
            <div class="visual">
                <a href="<%=LinkManager.GetItemUrl(CourseItem)%>"><%= CourseItem.Thumbnail.ImageHtmlTag()%></a>
                
                <% if (CourseItem is IMediaCourseItem)
                   { %>
                    <div class="runtime"><%= ((IMediaCourseItem)CourseItem).Runtime%></div>
                <%} %>
            </div>
            <div class="detail">
                <div class="name"><%= CourseItem.Subhead%></div>
                <div class="summary"><%= CourseItem.Summary%></div>
            </div>
            
            <div class="clearboth"></div>
        </div>