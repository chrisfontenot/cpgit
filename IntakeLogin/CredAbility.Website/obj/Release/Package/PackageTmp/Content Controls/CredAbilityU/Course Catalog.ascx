﻿<%@ Import Namespace="CredAbility.Web.SitecoreLib.Item.Interfaces"%>
<%@ Import Namespace="Sitecore.Links"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Course Catalog.ascx.cs" Inherits="CredAbility.Website.Content_Controls.CredAbilityU.Course_Catalog" %>
<div class="courses">
<% if(Courses.HasFeaturedCourses){ %>
<div class="featured-courses">
    <h2><%= Resources.Website.CourseCatalog_FeaturedCourses %></h2>
    <div class="course-elements">
        
        <% foreach (var courseItem in Courses.GetFeatured()) { %>
        <div class="course-element">
            <!-- <div class="category">[Category]</div> -->
            <div class="visual">
                <a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Thumbnail.ImageHtmlTag() %></a>
                
                <% if (courseItem is IMediaCourseItem){ %>
                    <div class="runtime"><%= ((IMediaCourseItem)courseItem).Runtime %></div>
                <%} %>
            </div>
            <div class="detail">
               <div class="name"><a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Subhead%></a></div>
                <div class="rated-torename">
                    <%-- ItemRate1.ItemID = courseItem.ID.ToString();--%>
                    <%--<ca:ItemRate ID="ItemRate1" runat="server"/>--%>
                    <div class="clearboth"></div>
                </div>
                <div class="summary"><%= courseItem.Summary %></div>
            </div>
            
            <div class="clearboth"></div>
        </div>
        <%} %>
    </div>
</div>
<%} %>

        <div class="course-category">
            
            <div class="course-elements">
            <% foreach (var courseItem in Courses)
               {%>
                <div class="course-element">
                     <div class="visual">                    
                        
                        
                        <a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Thumbnail.ImageHtmlTag()%></a>
                         <% if (courseItem is IMediaCourseItem)
                            { %>
                            <div class="runtime"><%= ((IMediaCourseItem)courseItem).Runtime%></div>
                        <%} %>
                    </div>
                    <div class="detail">
                        <div class="name"><a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Subhead%></a></div>
                        <div class="rated-torename">
                            <%-- ItemRate2.ItemID = courseItem.ID.ToString();--%>
                            <%--<ca:ItemRate ID="ItemRate2" runat="server"/>--%>
                            <div class="clearboth"></div>
                        </div>
                        <div class="summary"><%= courseItem.Summary%></div>
                    </div>
                    
                    <div class="clearboth"></div>
                </div>
            <%} %>
            </div>
            
        </div>
 

</div>
<div class="right-rail">
<div class="rr-block show-courses-filter">
        <h3><%= Resources.Website.CredU_ListItems %></h3>
        <ca:CourseSortings ID="CourseFilters2" runat="server"/>
    </div>
<div class="rr-block show-courses-filter">
        <h3><%= Resources.Website.CredU_MediaTypes %></h3>
        <ca:CourseFilters ID="CourseFilters1" runat="server"/>
    </div>
<div class="clearboth"></div>


</div>