﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"%>




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 

<html>
<head>
    <title>CredAbility.org: Down for Maintenance</title>
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/combo?3.0.0/build/cssreset/reset-min.css&3.0.0/build/cssfonts/fonts-min.css">
     <link rel="stylesheet" type="text/css" href="~/minify/combo.ashx?f=/static/css/base.css,/static/css/menu.css,/static/css/public.css&m=css&ct=text/css"/>
</head>
<body>
    <div class="wrapper">
        <div class="header">
    <div class="logo"><a href="#" class="en"><strong>CredAbility</strong> Nonprofit Credit Counseling & Education</a></div>
   
</div>
	
       
         <div class="headline" style="background-image:url(/static/images/layout/primary_maintenance.jpg);">
 
</div>
        <div class="body">
       
             <div class="c-body c-large">
        <h1>Sorry, CredAbility.org is temporarily closed due to planned maintenance.</h1>
        
        
        
        <p>Online Counseling and Client Access (DMP) services will not be available during this maintenance period (including bankruptcy pre-filing and debtor education services).</p>
        <p>We sincerely apologize for this disruption, and any inconvenience it may cause. We will continue to provide support throughout this period by telephone at 800.251.2227, or by email at <a href="info@CredAbility.org">info@CredAbility.org</a>.</p>
        
        
        
        <h1 style="margin-top: 10px; padding-top: 10px; border-top: 1px solid #ECECEC">Lo sentimos, CredAbility.org está temporalmente cerrado debido a tareas de mantenimiento previstas.</h1>
        
        <p>El acceso a los servicios de asesoría por Internet y acceso al cliente (DMP) no estarán disponibles durante el período de mantenimiento (incluyendo los servicios de asesoría previa a la declaración de bancarrota y la educación para el deudor de bancarrota).</p>
        <p>
Nos disculpamos sinceramente por esta interrupción y por cualquier inconveniencia que pueda causar. Continuaremos proporcionando nuestros servicios durante este período por teléfono al 800.251.2227, o por correo electrónico a <a href="info@CredAbility.org">info@CredAbility.org</a>.</p>
    </div>
            <div class="clearboth"></div>
        </div>

      <div class="footer">
        </div>
    </div>

    </div>
       

</body>
</html>
