﻿<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Service.Master" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="PageTitle">
 <div class="headline" style="background-image:url(/static/images/layout/headline-404-en.jpg);">
 
</div>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="PageBody">


    <div class="c-body c-large">
        <h1>Sorry, the page you were looking for could not be found</h1>
        
        Here's what you can do next:
    <ul>
        <li>Check the spelling of the web address.</li>
        <li>Try using the search box above to find what you're looking for.</li>
        <li>Click the "<a href="javascript:window.history.back()">Back</a>" button on your browser or go to our home page</li>
    </ul>
    </div>
</asp:Content>
