﻿<%@ Page Language="C#" %>


<%
var value = Request.QueryString["action"];
if(!string.IsNullOrEmpty(value))
{
    
    System.Web.HttpContext.Current.Cache.Remove(Server.UrlDecode(value));  
}


 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>CredAbility Cache Manager</title>
</head>
<body>
    <h1>CredAbility Cache Manager</h1>
    <form method="get" action="cache.aspx">
    
    
    <input type="hidden" name="action" />

    <hr />
    
    
    <table>
    
<%
        var list = System.Web.HttpContext.Current.Cache.GetEnumerator();
        while (list.MoveNext()){
%>
            <tr>
                <td><%=list.Entry.Key %></td>
                <td><input type="submit" value="Flush -->" onclick="setAction('<%=Server.UrlEncode(list.Entry.Key.ToString()) %>')"/></td>
            </tr>    
<%
      
        }
%>
    
    </table>
    
    </form>
    
    <script>
        var setAction = function(value) {
            document.forms[0].elements['action'].value = value;
        }
    </script>
</body>
</html>
