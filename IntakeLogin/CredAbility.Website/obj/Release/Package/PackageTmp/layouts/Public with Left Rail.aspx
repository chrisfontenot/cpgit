<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Main.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="PageBody">
    <div class="left-rail">
        <sc:Placeholder runat="server" Key="left-rail" />
    </div>
    <div class="content">
        <sc:Placeholder ID="content" runat="server" Key="content" />
    </div>
</asp:Content>
