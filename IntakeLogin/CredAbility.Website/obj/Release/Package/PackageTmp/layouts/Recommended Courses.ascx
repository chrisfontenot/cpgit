﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Recommended Courses.ascx.cs" Inherits="CredAbility.Website.layouts.Recommended_Courses" %>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.Item.Interfaces"%>
<%@ Import Namespace="Sitecore.Links"%>

<div class="recommended-courses">
<div>
	<div class="title">Recommended Courses</div>
	<div class="more-courses"><span class="prev"><</span><span>More</span><span class="next">></span></div>
	<div class="clearboth"></div>
</div>
<div class="courses-box">	
	<% foreach (var courseItem in Courses){ %>
		<div class="course-element">
                     <div class="visual">                    
                        
                        
                        <a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Thumbnail.ImageHtmlTag()%></a>
                         <% if (courseItem is IMediaCourseItem)
                            { %>
                            <div class="runtime"><%= ((IMediaCourseItem)courseItem).Runtime%></div>
                        <%} %>
                    </div>
                    <div class="detail">
                        <div class="name"><a href="<%=LinkManager.GetItemUrl(courseItem)%>"><%= courseItem.Subhead%></a></div>
                        <div class="rated">
                            <% ItemRate2.ItemID = courseItem.ID.ToString();%>
                            <ca:ItemRate ID="ItemRate2" runat="server"/>
                            <div class="clearboth"></div>
                        </div>
                        <div class="summary"><%= courseItem.Summary%></div>
                    </div>
                    
                    <div class="clearboth"></div>
                </div>
	<% } %>
</div>
</div>
<script>
var recommendedCoursesScroll = true;
</script>