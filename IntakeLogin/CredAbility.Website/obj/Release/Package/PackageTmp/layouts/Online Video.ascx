﻿<%@ Control Language="c#" AutoEventWireup="true" Inherits="CredAbility.Web.UI.UserControls.CourseBaseControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<h1>
            <sc:text ID="Text1" field="Subhead" runat="server"/>
            
        </h1>
        <script type="text/javascript" src="/JavaScript/Education.js"></script>

         <div class="course-info article-info">
            <div class="posted">
                <asp:Literal ID="Literal1" runat="server" Text="<%$Resources:Website, Course_Posted%>"/> <sc:date ID="Date1" field="Posted" format="D"  runat="server"/>
            </div>
            
            <%--<ca:ItemRate ID="ItemRate1" runat="server"/>--%>
            <div class="clearboth"></div>
        </div>
        
        <div class="media-embed">
        <sc:text ID="Text3" field="Media Embed"  runat="server"/>
        </div>
        <sc:text ID="Text2" field="Content"  runat="server"/>

        <%--<div class="rate-this">
            <div><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Website, Course_RateThisCourse%>"/></div> <ca:ItemRater ID="ItemRater1" runat="server"/>
        </div>--%>
