<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"  MasterPageFile="~/MasterPages/MainView.Master" inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Register src="/layouts/ProfileInfo.ascx" tagname="profile" tagprefix="uc" %>
<%@ Register src="/layouts/Section Navigation.ascx" tagname="nav" tagprefix="uc" %>
<%@ Register src="/layouts/My Account Headline.ascx" tagname="headline" tagprefix="uc" %>

<asp:Content runat="server" ContentPlaceHolderID="PageBody">
<div class="left-rail">
    	<uc:profile runat="server"/>
	<uc:nav runat="server"/>
</div>
<div class="content yui-skin-sam">
    <div id="my-goal" class="c-body c-large">
        <div>
		<div><sc:Placeholder runat="server" Key="content-body" /></div>
		<div class="add-goal-link"><span class="submitNextButton"><a onclick="if (typeof(Goal.AddGoal) != 'undefined'){ Goal.AddGoal();}"><%= Resources.Website.Goal_AddGoal %></a></span></div>
	</div>
	<div class="clearboth"></div>
	
	<div class="contentBox"><% Html.RenderAction("UpdateContent", "MyGoal",new {culture = ViewData["currentCulture"]}); %></div></br><br/>
	

	<div id="overlay" class="yui3-overlay-loading">
    		<div class="yui3-widget-hd"></div>
    		<div class="yui3-widget-bd lightBox"></div>
    		<div class="yui3-widget-ft"></div>	
	</div>

	<div id="overlayCalendar" class="yui3-overlay-loading">
    		<div class="yui3-widget-hd"></div>
    		<div class="yui3-widget-bd"><div id="calendar"></div></div>
    		<div class="yui3-widget-ft"></div>	
	</div>

	
    </div>

    <div><sc:Placeholder runat="server" Key="recommended-content" /></div>
</div>
<script>var mygoal = true;var culture = '<%= ViewData["currentCulture"]%>';</script>


</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="Headline">
<form id="form1">
        <uc:headline runat="server"/>
</form>
</asp:Content>