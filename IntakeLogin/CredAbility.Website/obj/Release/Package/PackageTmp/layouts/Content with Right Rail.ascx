﻿<%@ Control Language="c#" AutoEventWireup="true" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
    <div class="right-rail">
         <sc:Placeholder ID="rightrail" runat="server" Key="right-rail" />
    </div>
    <div class="c-body c-small">
        <sc:Placeholder ID="contentBody" runat="server" Key="content-body" />
    </div>
    <div><sc:Placeholder runat="server" Key="recommended-content" /></div>   
    <div class="clearboth"></div>