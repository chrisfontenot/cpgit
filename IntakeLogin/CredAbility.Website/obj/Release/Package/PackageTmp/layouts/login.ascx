﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="login.ascx.cs" Inherits="CredAbility.Website.layouts.login" %>
<%@ Register TagName="ErrorSeeBelow" TagPrefix="ca" Src="~/Content Controls/ErrorSeeBelow.ascx" %>
<div class="form-block">
<ca:ErrorSeeBelow runat="server" id="ErrorSeeBelow"/>
<ca:ResultMessages id="ResultMessages" runat="server"/>

<p><%=Resources.MyAccount.Login_Already %></p>

 <div class="field-block <%= GetCssClassIfMessageCodeExist("USERNAME-REQUIRED", "field-block-error") %>">
    <asp:Label ID="Label1" runat="server" AssociatedControlID="Username" Text="<%$Resources:MyAccount, Login_Username%>"/>
    <div class="field-element">
        <asp:TextBox ID="Username" runat="server" Columns="20" />
     <div class="field-message"><%= GetMessage("USERNAME-REQUIRED")%></div>
        </div>
        <div class="clearboth"></div>
</div>
<div class="field-block <%= GetCssClassIfMessageCodeExist("PASSWORD-REQUIRED", "field-block-error") %>">
    <asp:Label ID="Label2" runat="server" AssociatedControlID="Password" Text="<%$Resources:MyAccount, Login_Password%>"/>
    <div class="field-element">
    <asp:TextBox ID="Password" TextMode="Password" runat="server" Columns="20"/>
     <div class="field-message"><%= GetMessage("PASSWORD-REQUIRED")%></div>
        </div>
        <div class="clearboth"></div>
</div>

<div class="form-action">
    <span class="submitNextButton"><asp:Button ID="Button1" runat="server" OnClick="SubmitButton_Click" Text="<%$Resources:MyAccount,Login_LoginButton %>" /></span>
</div>
</div>