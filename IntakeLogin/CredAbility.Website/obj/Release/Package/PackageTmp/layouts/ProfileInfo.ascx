﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProfileInfo.ascx.cs" Inherits="CredAbility.Website.layouts.ProfileInfo" %>

<div class="Profile-Info">
    <div class="Avatar"><img src="<%= ThumbnailUrl %>" alt="<%= ClientProfile.Username %>-avatar" /></div>
    <div class="UserName" style="padding-top: 15px;"><%= ClientProfile.Username %></div>
    <div class="ClientID"><%= Resources.MyAccount.ProfileInfoBox_ClientID %>&nbsp;<% =ClientProfile.DisplayId %></div>
</div>