﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer.ascx.cs" Inherits="CredAbility.Website.MasterPages.controls.footer" %>

<div class="footer">
            <div class="footer-links">
                <ca:FooterLinks ID="FooterLinks1" runat="server"/>
            </div>
            
            <div class="clearboth"></div>
             <div class="partners-logos">
                <ca:PartnersLogo ID="PartnersLogo1" runat="server"/>
            </div>
<div class="clearboth"></div>
<div class="copyright">
                <ca:Copyright ID="Copyright1" runat="server"/>
            </div>
            <ca:DartAnalytics ID="DartAnalyticsTag" runat="server" />
        </div>