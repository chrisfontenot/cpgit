﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="CredAbility.Website.MasterPages.controls.header" %>
<%@ Import Namespace="System.Threading"%>
<div class="header">
    <div class="logo">
<a class="<%= Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName %>" href="/" title="Return to CredAbility's home page"><strong>CredAbility</strong> Nonprofit Credit Counseling & Education</a></div>
	<div class="function-bar">
<table cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; float: left; margin: 0; margin-left: 300px;"><tr>
<td><a href="http://www.facebook.com/CredAbility" target="_blank"><img src="/static/images/icons/facebook.png" height="30" hspace="2" border="0" /></a></td>
<td><a href="http://www.twitter.com/credability" target="_blank"><img src="/static/images/icons/twitter.png" height="30" hspace="2" border="0" /></a></td></tr></table>
			<div class="login-bar">

			<ca:AuthenticateLink runat="server"/>
			</div>
			<div class="function-nav">
				<ca:SubHeaderLinks ID="SubHeaderLinks1" runat="server"/>
			</div>
			
	
            <div class="search"><ul class="language"><ca:LanguageLinks runat="server"/></ul> 
		<form action="/search.aspx" method="get">
    						<input type="text" name="q" class="keyword" value=""/>
							<select name="category" id="services">
									<option value="All"><%=DDTxtAllCredAbility%></option>
									<%foreach (var listItem in SearchCategories){ %>
									<option value="<%= listItem.Value%>"><%=listItem.Text%></option>
									<% } %>
									<option value="FAQ"><%=DDTxtFAQ%></option>
							</select>
							
							<input name="submit" hspace="5" type="image" class="submit" src="/static/images/layout/btn-go.gif" />
		</form>
			
			</div>
	</div>
	<div class="menu">
		<ca:HeaderLinks ID="HeaderLinks1" runat="server"/>
	</div>

    
</div>