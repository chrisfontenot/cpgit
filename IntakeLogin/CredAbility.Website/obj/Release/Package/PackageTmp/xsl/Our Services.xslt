﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!-- Uncomment one of the following lines if you need a "home" variable in you code -->
<!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
<!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
<!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


  <!-- entry point -->
  <xsl:template match="*">
    <xsl:apply-templates select="$sc_item/item[@template = 'right-rail block']" mode="main"/>
  </xsl:template>



  <xsl:template match="*" mode="main">
    <sc:disableSecurity>
      <div class="rr-block related">
        <h3>
          <sc:text field="Text"/>
        </h3>
        <ul>
          <xsl:apply-templates select="item" mode="main2"/>
        </ul>
      </div>
    </sc:disableSecurity>
  </xsl:template>

  <!--==============================================================-->
  <!-- main                                                         -->
  <!--==============================================================-->
  <xsl:template match="*" mode="main2">
    <li>
      <a>

        <xsl:attribute name="href">
          <xsl:if test="@template = 'internal link'">
            <xsl:value-of select="sc:path(sc:item(sc:fld('Linked Item',current()),.))"/>
          </xsl:if>


          <xsl:if test="@template = 'external link'">
            <xsl:value-of select="sc:fld('Url',current())"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:if test="@template = 'external link'">
          <xsl:attribute name="target">_blank</xsl:attribute>
          <xsl:if test="sc:fld('nofollow',current()) = 1">
            <xsl:attribute name="rel">nofollow</xsl:attribute>
          </xsl:if>
        </xsl:if>

        <sc:text field="Text"/>&#160;<img src="/static/images/layout/blue-arrow.gif" align="absmiddle"/>
      </a>
    </li>
  </xsl:template>

</xsl:stylesheet>