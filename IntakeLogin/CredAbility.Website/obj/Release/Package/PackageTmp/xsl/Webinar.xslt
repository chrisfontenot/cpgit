﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!-- Uncomment one of the following lines if you need a "home" variable in you code -->
<!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
<!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
<!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


    <!-- entry point -->
    <xsl:template match="*">
        <xsl:apply-templates select="$sc_item" mode="main"/>
    </xsl:template>

    <!--==============================================================-->
    <!-- main                                                         -->
    <!--==============================================================-->
    <xsl:template match="*" mode="main">
        <h1>
            <sc:text field="Subhead"/>
            
        </h1>

        <div class="course-info article-info">
            <div class="posted">
                Posted: <sc:date field="Posted" format="D"/>
            </div>
            <div class="rated">
            ***** <span class="rated-by">(rated by 7 users)</span>
            </div>

            <div class="runtime">
                Runtime: 2:45
            </div>
        </div>
        
        <sc:text field="Content" />


        <div class="rate-this">
            Rate this course *****
        </div>

        <div class="share-this">
            <!-- AddThis Button BEGIN -->
            <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4bba8a9e575eb1c0">
                <img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/>
            </a>
            <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4bba8a9e575eb1c0"></script>
            <!-- AddThis Button END -->
        </div>

    </xsl:template>

</xsl:stylesheet>