﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!-- Uncomment one of the following lines if you need a "home" variable in you code -->
<!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
<!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
<!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


<!-- entry point -->
<xsl:template match="*">
  <table width="700" border="0" cellpadding="0" cellspacing="0">
    <tr>
  <xsl:apply-templates select="following-sibling::*[@template = 'sub-section folder']" mode="main"/>
    </tr>
  </table>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
    <td width="350" valign="top" style="padding:10px">
      <h2><sc:text field="Text"/></h2>
      <xsl:apply-templates select="item[@template = 'news']" mode="news"/>
      <div style="font-weight:bold;text-align:right; padding-right:10px; vertical-align:bottom">
        <h2><a href="{sc:path(sc:item(sc:fld('Landing Page',current()),.))}">

          <xsl:variable name="moreInfo" select="sc:GetResources('PressRelease_SeeAll')" />

          <xsl:value-of select="$moreInfo"/>
          
          
          
          
          <span style="text-transform:lowercase">
            <sc:text field="Text"/>
          </span>
        </a>
        </h2>
      </div>
    </td>
</xsl:template>

    <xsl:template match="*" mode="news">
      <xsl:if test="position() &lt; 6">
        <p>
            <strong>
                <a href="{sc:path(current())}">
                    <sc:text field="Subhead"/>
                </a>
            </strong>
            <em>, <sc:date field="News Date" format="MM/dd/yyyy"/>
            </em>
        </p>
      </xsl:if>
    </xsl:template>
</xsl:stylesheet>