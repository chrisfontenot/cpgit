<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sc="http://www.sitecore.net/sc"
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

    <!-- output directives -->
    <xsl:output method="html" indent="no" encoding="UTF-8" />

    <!-- parameters -->
    <xsl:param name="lang" select="'en'"/>
    <xsl:param name="id" select="''"/>
    <xsl:param name="sc_item"/>
    <xsl:param name="sc_currentitem"/>

    <!-- variables -->
    <!-- Uncomment one of the following lines if you need a "home" variable in you code -->
    <!--<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />-->
    <!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
    <!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


    <!-- entry point -->
    <xsl:template match="*">
        <div class="hero-line-of-service">
            <div class="default">

                <xsl:apply-templates select="$sc_item" mode="main"/>
            </div>
            <div class="line-of-services">
<!-- <div class="intro" style="margin-bottom: 0;"><xsl:choose>
                    <xsl:when test="$lang = 'es'">TESTIMONIOS</xsl:when>
                    <xsl:otherwise>
                      TESTIMONIALS</xsl:otherwise>                  </xsl:choose></div> -->
<a href="/chat/chat-window.aspx" style="text-decoration: none;"> 
                <div style="background: url(/static/images/layout/bg-line-of-service-current.png) repeat-y bottom left;"><div class="nav"><xsl:choose>
                    <xsl:when test="$lang = 'es'">
						¿No sabe cómo empezar?<br /> ¡Charle con nosotros!
					</xsl:when>
                    <xsl:otherwise>
                      Have Questions?<br />Click here to chat!</xsl:otherwise>                  </xsl:choose></div></div></a>

<div class="intro" style="margin-bottom: 0; margin-top: 25px;"><xsl:choose>
                    <xsl:when test="$lang = 'es'">COMIENCE AHORA</xsl:when>
                    <xsl:otherwise>
                      GET STARTED NOW</xsl:otherwise>                  </xsl:choose>



                                      
                </div>                 <xsl:apply-templates select="$sc_item/item[@template = 'line of service']" mode="main"/>

                <div class="loading"></div>
            </div>
            <xsl:apply-templates select="$sc_item" mode="link"/>
          <script>
            var HeroHeadlineInit = true;
          </script>
        </div>
    </xsl:template>

    <!--==============================================================-->
    <!-- main                                                         -->
    <!--==============================================================-->
    <xsl:template match="*" mode="main">
        <div class="line-of-service">

            <xsl:choose>
                <xsl:when test="last() = 1">
                    <xsl:attribute name="class">line-of-service current</xsl:attribute>
                    <h1 class="nav">
                      <a href="/our-services/mortgage-assistance-programs/default.aspx" target="_blank">                                                 <sc:text field="Link Text"/>
                                           </a>                     </h1>
                </xsl:when>
                <xsl:otherwise>
                    <h2 class="nav" onclick="window.location = '{sc:path(sc:item(sc:fld('Linked Item',.),.))}'">                        
                            <sc:text field="Link Text"/>                        
                    </h2>
                   
                </xsl:otherwise>
            </xsl:choose>
            <div class="headline">
                <sc:text field="HeadLine"/>
            </div>

            <div class="summary">
                <sc:text field="Summary"/>
            </div>

            <div class="linked-item">
                <a href="{sc:path(sc:item(sc:fld('Linked Item',.),.))}">
                    <sc:text field="Link Text"/>
                </a>
            </div>

            <div class="image">
                <xsl:element name="img">
                    <xsl:attribute name="id">
                        LOSImage<xsl:value-of select="position()"/>
                    </xsl:attribute>
                    <xsl:attribute name="url">
                        <xsl:value-of select="sc:GetMediaUrl(sc:item(sc:fld('Image',.,'mediaid'),.))"></xsl:value-of>
                    </xsl:attribute>
                </xsl:element>
            </div>

        </div>


    </xsl:template>
    <xsl:template match="*" mode="link">
        <a class="LOS-link" href="{sc:path(sc:item(sc:fld('Linked Item',.),.))}"></a>
    </xsl:template>
</xsl:stylesheet>