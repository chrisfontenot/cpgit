﻿<?xml version="1.0" encoding="UTF-8"?>

<!--=============================================================
    File: Related Resources.xslt                                                   
    Created by: sitecore\PatrickMeunier                                       
    Created: 2010-03-03 13:42:19                                               
    Copyright notice at bottom of file
==============================================================-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:sc="http://www.sitecore.net/sc" 
  xmlns:dot="http://www.sitecore.net/dot"
  exclude-result-prefixes="dot sc">

<!-- output directives -->
<xsl:output method="html" indent="no" encoding="UTF-8" />

<!-- parameters -->
<xsl:param name="lang" select="'en'"/>
<xsl:param name="id" select="''"/>
<xsl:param name="sc_item"/>
<xsl:param name="sc_currentitem"/>

<!-- variables -->
<!-- Uncomment one of the following lines if you need a "home" variable in you code -->
<xsl:variable name="home" select="sc:item('/sitecore/content/home',.)" />
<!--<xsl:variable name="home" select="/*/item[@key='content']/item[@key='home']" />-->
<!--<xsl:variable name="home" select="$sc_currentitem/ancestor-or-self::item[@template='site root']" />-->


<!-- entry point -->
<xsl:template match="*">
    <ul>
        <xsl:apply-templates select="$home/item[@template='section folder']" mode="main"/>
    </ul>
</xsl:template>

<!--==============================================================-->
<!-- main                                                         -->
<!--==============================================================-->
<xsl:template match="*" mode="main">
  <li>
    <xsl:variable name="active" select="sc:fld('Sitemap', .)"/>
      <xsl:if test="$active = '1'">
          <xsl:if test="sc:fld('Text',.) = 'global'">
          <ul>
            <xsl:apply-templates select="item[@template = 'sub-section folder']" mode="main"/>
            <xsl:apply-templates select="item[@template = 'content']" mode="main2"/>
           <xsl:apply-templates select="item[@template = 'user control content']" mode="main2"/>
         </ul>
        </xsl:if>
    
      <a href="{sc:path(sc:item(sc:fld('Landing Page',current()),.))}">
          <strong>
              <sc:text field="Text"/>
          </strong>
      </a>
      <ul>       
        <xsl:apply-templates select="item[@template = 'sub-section folder']" mode="main"/>
        <xsl:apply-templates select="item[@template = 'content']" mode="main2"/>
        <xsl:apply-templates select="item[@template = 'user control content']" mode="main2"/>
      </ul>
    </xsl:if>
  </li>
</xsl:template>
  
  <xsl:template match="*" mode="main2">
    <xsl:if test="sc:fld('Sitemap', .) = '1'">
        <li><a href="{sc:path(current())}">
          <sc:text field="Text"/>
        </a>
        </li>
      
    </xsl:if>
    
  </xsl:template>
</xsl:stylesheet>