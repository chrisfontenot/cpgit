﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using CredAbility.Web.Configuration;
using Sitecore.Web.UI.WebControls;

namespace CredAbility.Website.MasterPages.controls
{
    public partial class PartnerLogos : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            InitLogos();
        }

        protected void InitLogos()
        {
            Item logoRoot = Sitecore.Context.Database.GetItem(ConfigReader.ConfigSection.PartnersLogo.SitecorePath);
            Logos.DataSource = logoRoot.GetChildren();
            Logos.DataBind();
        }

        protected void Logos_DataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Literal lit = e.Item.FindControl("Logo") as Literal;
            Item logoItem = e.Item.DataItem as Item;

            if (logoItem.TemplateName.ToLower() == "logo custom")
            {
                lit.Text = logoItem["Code"];
            }
            else if (logoItem.TemplateName.ToLower() == "logo")
            {
                lit.Text = FieldRenderer.Render(logoItem, "Image");
            }
        }
    }
}