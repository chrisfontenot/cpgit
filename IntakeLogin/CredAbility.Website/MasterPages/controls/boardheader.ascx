﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="CredAbility.Website.MasterPages.controls.header" %>
<%@ Import Namespace="System.Threading"%>
<div class="header">
    <div class="logo"><a class="<%= Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName %>" href="/" title="Return to CredAbility's home page"><strong>CredAbility</strong> Nonprofit Credit Counseling & Education</a></div>
	<div class="function-bar">
			<!-- <div class="login-bar">
			<ca:AuthenticateLink runat="server"/>
			</div> -->
			<div class="function-nav">
				<!-- <ca:SubHeaderLinks ID="SubHeaderLinks1" runat="server"/> -->
			</div>
			
	
            <div class="search"><!-- <ul class="language"><ca:LanguageLinks runat="server"/></ul> -->
		<!-- <form action="/search.aspx" method="get">
    						<input type="text" name="q" class="keyword" value=""/>
							<select name="category" id="services">
									<option value="All"><%=DDTxtAllCredAbility%></option>
									<%foreach (var listItem in SearchCategories){ %>
									<option value="<%= listItem.Value%>"><%=listItem.Text%></option>
									<% } %>
									<option value="FAQ"><%=DDTxtFAQ%></option>
							</select>
							
							<input name="submit" hspace="5" type="image" class="submit" src="/_static/layout/images/btn-go.gif" />
		</form> -->
			
			</div>
	</div>
	<div class="menu">
		<div class="yui3-menu yui3-menu-horizontal">
	<div class="yui3-menu-content">
		<ul>
<li class="first yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/default.aspx">Home</a></span></li>
			<li class="yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/default.aspx">About CredAbility</a></span><div class="yui3-menu yui3-menu-horizontal">
				<div class="yui3-menu-content">
					<ul>
				<li class="first  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/mission-statement.aspx">Mission Statement</a></span></li><li class="current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/history.aspx">History</a></span></li><li class="current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/annual-reports.aspx">Annual Reports</a></span></li><li class="current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/services.aspx">Services - What we do</a></span></li><li class="current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/management/default.aspx">Management</a></span></li><li class="current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/awards.aspx">Awards</a></span></li><li class="last current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/partnerships.aspx">Partnerships</a></span></li><li class="last current  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/about-credability/research.aspx">Research</a></span></li>
					</ul>
				</div>
			</div></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/default.aspx">Governance</a></span><div class="yui3-menu yui3-menu-horizontal">
				<div class="yui3-menu-content">
					<ul>
						<li class="first  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/board-members/default.aspx">Board Members</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/committees/executive-committee.aspx">Committees</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/directors-emeritus/default.aspx">Directors Emeritus</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/Roles/Board-Principals-and-Responsibilities.aspx">Roles</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/policies.aspx">Policies</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/compliance.aspx">Compliance</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/documents.aspx">Corporate Documents</a></span></li>
					</ul>
				</div>
			</div></li>
            <li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/financial-information/default.aspx">Financial Information</a></span><div class="yui3-menu yui3-menu-horizontal">
				<div class="yui3-menu-content">
					<ul>
						<li class="first  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/financial-information/audit-reports.aspx">Audit Reports</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/financial-information/form-990.aspx">Form 990</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/financial-information/financial-reports.aspx">Financial Reports</a></span></li>
                        </ul>
				</div>
			</div></li>
            <li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/board-members/meetings-calendar/default.aspx">Board/Committee Meetings</a></span><div class="yui3-menu yui3-menu-horizontal">
				<div class="yui3-menu-content">
					<ul>
						<li class="first  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/board-members/meetings-calendar/default.aspx">Board Meetings</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/governance/committees/executive-committee.aspx">Committee Meetings</a></span></li>
                        </ul>
				</div>
			</div></li>
            <li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/default.aspx">Keeping Current</a></span><div class="yui3-menu yui3-menu-horizontal">
				<div class="yui3-menu-content">
					<ul>
						<li class="first  yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/client-profile.aspx">Client Profile</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/media-links.aspx">Media Links</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/message-from-president.aspx">Messages from President</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/top-10-accomplishments.aspx">Top 10 Accomplishments</a></span></li><li class=" yui3-menuitem"><span class="yui3-menu-label"><a href="/en/Board/keeping-current/referral-program.aspx">Referral Program</a></span></li>
                        </ul>
				</div>
			</div></li>
            </ul></div></div>
	</div>
    </div>