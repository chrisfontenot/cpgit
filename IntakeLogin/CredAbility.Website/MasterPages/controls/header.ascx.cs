﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using CredAbility.Web;
using CredAbility.Web.Configuration;
using Sitecore.Data.Items;
using CredAbility.Web.SitecoreLib.Item;

namespace CredAbility.Website.MasterPages.controls
{
    public partial class header : System.Web.UI.UserControl
    {
        protected string DDTxtAllCredAbility { get { return Resources.Website.SearchHeader_AllCredAbility; } }
        protected string DDTxtFAQ { get { return Resources.Website.SearchHeader_FAQ; } }
        protected string DDTxtCredAbilityU { get { return Resources.Website.SearchHeader_CredAbilityU; } }



        private List<ListItem> _searchCategories = new List<ListItem>();
        protected List<ListItem> SearchCategories
        {
            get { return _searchCategories; }
            set { _searchCategories = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (SectionFolderItem item in GetSearchableSectionFolders())
            {
                SearchCategories.Add(new ListItem(item.FolderName, item.Name));
            }
        }

        private static IEnumerable<Item> GetSearchableSectionFolders()
        {
            var contextDb = Sitecore.Context.Database;

            var sectionFolders =
                contextDb.GetItem(ConfigReader.ConfigSection.SitecoreHome.SitecorePath).Children.InnerChildren.Where(
                    c => c.TemplateName == "Section Folder");
            var searchableSectionFolders =  sectionFolders.Where(sf => ((Sitecore.Data.Fields.CheckboxField) sf.Fields["Searchable"]).Checked);

            var result = new List<Item>();
            foreach (var item in searchableSectionFolders)
            {
                result.Add(new SectionFolderItem(item));
            }
            return result;
        }
    }
}