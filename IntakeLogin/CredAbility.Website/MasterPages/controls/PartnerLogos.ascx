﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartnerLogos.ascx.cs" Inherits="CredAbility.Website.MasterPages.controls.PartnerLogos" %>
<asp:Repeater ID="Logos" OnItemDataBound="Logos_DataBound" runat="server">
    <HeaderTemplate><ul></HeaderTemplate>
    <ItemTemplate>
        <li><asp:Literal ID="Logo" runat="server" /></li>
    </ItemTemplate>
    <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>