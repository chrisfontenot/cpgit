﻿using System;
using System.Web;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using Sitecore.Web;


namespace CredAbility.Website.MasterPages.Clearpoint
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected int HeroRotationTime { get { return ConfigReader.ConfigSection.HeroShotRotationTime.Timer; } }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

		public void SetTitle(string PageTitle)
		{
			TrueTitle.InnerText = PageTitle + " | Credabilty.org";
		}
    }
}
