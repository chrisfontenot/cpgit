﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="header.ascx.cs" Inherits="CredAbility.Website.MasterPages.Clearpoint.controls.header" %>
<%@ Import Namespace="System.Threading"%>
<div class="header" style="position: relative;">
    <div class="logo">
		
        <a href="http://www.clearpointccs.org" class="<%= Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName %>" title="ClearPoint Credit Counseling Solutions">
            <strong>ClearPoint</strong> Credit Counseling Solutions
        </a>
    </div>
    <div class="utility">
        <ca:AuthenticateLink runat="server"/>
         &nbsp;|&nbsp;
	<% if (Sitecore.Context.Language.Name == "es") { %>
        	<a href="javascript:void(window.open('https://server.iad.liveperson.net/hc/29009926/?visitor=&msessionkey=&cmd=file&file=visitorWantsToChat&site=29009926&SESSIONVAR%21skill=Helpdesk&referrer='+escape(document.location),'chat'))">Charla en vivo</a>
        	&nbsp;|&nbsp;
        	<a href="http://www.credability.org/about-us/contact-us"><span>Contáctenos</span>

	<% } else { %>
        	<a href="javascript:void(window.open('https://server.iad.liveperson.net/hc/29009926/?visitor=&msessionkey=&cmd=file&file=visitorWantsToChat&site=29009926&SESSIONVAR%21skill=Helpdesk&referrer='+escape(document.location),'chat'))"><%=LiveChatText %></a>
        	&nbsp;|&nbsp;
        	<a href="http://www.credability.org/about-us/contact-us"><span><%=ContactUsText %></span>
	<% } %>
        &nbsp;<img src="/static/images/clearpoint/email-icon.png" alt="email" /></a>
        &nbsp;    
        |
        <strong>1.800.251.CCCS (2227)</strong>
    </div>
</div>