﻿YUI.add('gallery-watermark', function(Y) {

    Y.Watermark = function(inputID, cssBlur, cssFocus, text) {
        var input = Y.one(inputID);
        this.text = text;
        this.input = input;
        


        input.setAttribute("value", this.text);

        var _onFocus = function(e, args) {
            if (e.currentTarget.getAttribute("value") == args.text) {
                e.currentTarget.setAttribute("value", "");
            }
        }

        var _onBlur = function(e, args) {

            console.log(e)
            if (e.currentTarget.getAttribute("value") == "") {
                e.currentTarget.setAttribute("value", args.text);
            }
        }

        input.on('focus', _onFocus, null, this);
        input.on('blur', _onBlur, null, this);
    };

    Y.Watermark.prototype = {
        text: "",
        input: null
    }




}, 'gallery-watermark-01');