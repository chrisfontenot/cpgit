﻿
YUI.add("Rating", function(Y) {

function curry(fn, scope) {
    var scope = scope || window;
    var args = Array.prototype.slice.call(arguments, 2) || [];

    return function() {
        fn.apply(scope, args);
    };
}
    Y.Rating = function(id, value) {
        this.construct.apply(this, arguments);
    };

    Y.Rating.prototype = {
        construct: function(id, stars, value, rerate, itemID) {
            this.id = id;
            this.imageOff = 'blank';
            this.imageOn = 'mouseOn';
            this.imageOut = 'mouseOff';
            this.stars = stars;
            this.rerate = rerate;
            this.itemID = itemID;

            this.setValue(value);

            this.el = document.getElementById(id);

            for (i = 1; i <= this.stars; i++) {
                var starID = 'star' + i;
                var newstar = document.createElement('span');
                newstar.id = starID;
                newstar.className = this.imageOff;

                this.el.appendChild(newstar);

                this.addListener(document.getElementById(starID), "mouseover", curry(this.mouseOver, this, i));
                this.addListener(document.getElementById(starID), "click", curry(this.clickMethod, this, i));
            }

            this.addListener(this.el, "mouseout", curry(this.mouseOut, this));

            this.renderStars(this.value, false);
        },

        addListener: function(element, type, expression, bubbling) {
            bubbling = bubbling || false;

            if (window.addEventListener) {
                element.addEventListener(type, expression, bubbling);
                return true;
            }
            else if (window.attachEvent) {
                element.attachEvent('on' + type, expression);
                return true;
            }
            else {
                return false;
            }
        },

        removeListener: function(element, type, expression, bubbling) {
            bubbling = bubbling || false;

            if (window.removeEventListener) {
                element.removeEventListener(type, expression, bubbling);
                return true;
            }
            else if (window.removeEvent) {
                element.removeEvent('on' + type, expression);
                return true;
            }
            else {
                return false;
            }
        },

        mouseOver: function(rating) {
            if (this.rerate) {
                this.renderStars(rating, true);
            }
        },

        clickMethod: function(rating) {
            this.onClick(rating);
        },

        mouseOut: function() {
            if (this.rerate) {
                if (this.value == 0 || this.value == '') {
                    this.renderStars(0, false);
                }
                else {
                    this.renderStars(this.value, false);
                }
            }
        },

        renderStars: function(units, startColor) {
            if (units > 0) {
                for (var i = 1; i <= units; i++) {
                    if (startColor == true) {
                        document.getElementById("star" + i).className = this.imageOn;
                    }
                    else {
                        document.getElementById("star" + i).className = this.imageOut;
                    }
                }

                for (i = parseInt(units) + 1; i <= this.stars; i++) {
                    document.getElementById("star" + i).className = this.imageOff;
                }
            }
            else {
                for (i = 1; i <= this.stars; i++) {
                    document.getElementById("star" + i).className = this.imageOff;
                }
            }
        },

        onClick: function(value) {
            this.setValue(value);

            var cfg = {
                on: {
                    success: function(id, o, args) {
                        var obj = eval("(" + o.responseText + ")");
                        var totalRate = this.one(".item-rate #total-rate");

                        if (totalRate) {
                            this.one(".item-rate .stars").setAttribute("class", "stars rate-" + obj.average.replace(",", ""))
                            totalRate.setContent(obj.totalRate);
                        }
                        this.one(".rate-this").setContent("");
                    },
                    failure: function(id, o, args) { }
                },
                context: Y
            };

            Y.io("/~/itemrater/itemrater.ashx?i=" + this.itemID + "&r=" + this.getValue() + "&d=" + new Date(), cfg);

            if (!this.rerate) {
                for (i = 1; i <= this.stars; i++) {
                    this.removeListener(document.getElementById('star' + i), "mouseover");
                    this.removeListener(document.getElementById('star' + i), "click");
                }

                this.removeListener(document.getElementById(this.id), "mouseout");

                this.renderStars(this.value, false);

            }
        },

        setValue: function(value) {
            this.value = value;
        },

        getValue: function() {
            return this.value;
        }
    }
});


