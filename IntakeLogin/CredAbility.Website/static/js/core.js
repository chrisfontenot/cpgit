﻿YUI().use("node-menunav", function(Y) {
    var menu = Y.one(".menu .yui3-menu");
    menu.plug(Y.Plugin.NodeMenuNav, {useARIA: false});
    menu.get("ownerDocument").get("documentElement").removeClass("yui-loading");
});



/*YUI({
modules: {
'gallery-watermark': {
fullpath: '/static/js/watermark.js',
requires: ['node'],
optional: [],
supersedes: []
}
}
}).use('gallery-watermark', function(Y) {
var watermark = new Y.Watermark(".keyword", "","","asdad");
});*/

        
if (typeof (HeroHeadlineInit) != "undefined") {

    YUI().use("node", "event", "imageloader", "async-queue", function(Y) {
        var LineOfServicesNav = Y.all('.hero-line-of-service .line-of-service');
        if (LineOfServicesNav._nodes.length === 0) {
            return;
        }
        var images = new Y.ImgLoadGroup({ timeLimit: 2 });
        images.addTrigger('body', 'onload');
        LineOfServicesNav.each(function(node) {
            var imageTag = node.one('.image img');
            var id = imageTag.getAttribute('id');
            var url = imageTag.getAttribute('url')
            images.registerImage({
                domId: id,
                srcUrl: url
            });
        });
        var headline = Y.one('.headline');
        var LOSLink = Y.one('.hero-line-of-service .LOS-link');
        var ChangeHeroHeadlineImage = function(targetNode) {
            var ancestorNode = targetNode.ancestor();
            var imageNode = ancestorNode.one('.image img');
            var imageSRC = imageNode.getAttribute('url');
            headline.setStyle('backgroundImage', 'url(' + imageSRC + ')');
            var linkedItemUrl = ancestorNode.one('.linked-item a').getAttribute('href');
            LOSLink.set('href', linkedItemUrl);
            LineOfServicesNav.removeClass('current');
            ancestorNode.addClass('current');
        }
        

        var HeroHeadlineMouseOver = function(e) {
            var targetNode = Y.one(e.target._node);
            ChangeHeroHeadlineImage(targetNode);
            //jb - added clearInterval for CREDA-2073
            clearInterval(si);
        }
        
        //jb - created for Issue CREDA-2073
        var HeroHeadlineMouseOut = function(e) {
            var targetNode = Y.one(e.target._node);
            si = setInterval(rotate, HeroRotationTime);
        }
        
        //jb - moved function into a name for CREDA-2073
        var rotate = function() {
            var currentLineOfService = Y.one('.hero-line-of-service .line-of-service.current');
		    var nextLineOfService = currentLineOfService.next();
		    if (nextLineOfService != null && nextLineOfService.hasClass('line-of-service')){
			    ChangeHeroHeadlineImage(nextLineOfService.one(".nav"));
		    }else{
			    ChangeHeroHeadlineImage(Y.one('.hero-line-of-service .line-of-services .line-of-service .nav'));
		    }
	    }
	
	Y.on('mouseover', HeroHeadlineMouseOver, LineOfServicesNav);
    //jb - added mouseout for CREDA-2073
    Y.on('mouseout',HeroHeadlineMouseOut, LineOfServicesNav);    
	
	var si = setInterval(rotate, HeroRotationTime);

    });
}

/* Item Rater*/
if (typeof(itemRater) != "undefined") {

    YUI({
        modules: {
            'Rating': {
                fullpath: '~/minify/combo.ashx?f=/static/js/rater.js&m=js',
                requires: ['node', "io-base"]
            }
        }
    }).use('Rating', function(Y) {
        var r = new Y.Rating('stars', 5, itemRater.defaultAverage || 0, true, itemRater.itemId);
    });
}

/* Needs Assessment sliders*/
if (typeof (slidersInit) != "undefined") {

    YUI().use('slider', 'node', function(Y) {

        var sliderInput = Y.all('.slider')._nodes;
        var sliders = new Array();
        for (input = 0; input < sliderInput.length; input++) {
            var sliderSpan = Y.one(sliderInput[input]).ancestor().one('span.slider-label');
            if (sliderSpan != null) {
                sliderSpan.set("innerHTML", formatCurrency(sliderInput[input].getAttribute("value")));
            }
            var inputID = sliderInput[input].id;
            var inputMax = sliderInput[input].getAttribute("max");
            var inputMin = sliderInput[input].getAttribute("min");
            if (inputMin == null) { inputMin = 1; }
            var slider = new Y.Slider({
                axis: 'x',
                min: parseInt(inputMin),      // min is the value at the top
                max: parseInt(inputMax),     // max is the value at the bottom
                value: 1,       // initial value
                length: '850px',  // rail extended to afford all values
                // construction-time event subscription
                after: {
                    valueChange: Y.bind(updateInput, sliderInput[input], sliderSpan)
                }
            }).render('#' + inputID + "-slider") // render returns the Slider

            slider.setValue(sliderInput[input].getAttribute("value"));
            slider.on('mouseup', function(e) { e.target.syncUI(); })
            sliders.push({ slider: slider })
        }

        // Function to update the input value from the Slider value
        function updateInput(spanNode, e) {
            this.setAttribute("value", e.newVal);
            if (spanNode != null) {
                spanNode.set("innerHTML", formatCurrency(e.newVal));
            }
        }

        if (typeof (CurrencyConvert) != "undefined") {
            var submitButton = Y.one("#GoToNextStep");
            submitButton.on("click", function(e) {

                if ((Y.one('input#TotalMonthlyPayment') != 'undefined') && (Y.one('input#TotalMonthlyPayment').get('value') == 0)) {

                    var answer = confirm(monthlyPaymentConfirm);

                    if (!answer) {

                        e.preventDefault();

                    }
                }
                if ((Y.one('input#TotalDebtAmount') != 'undefined') && (Y.one('input#TotalDebtAmount').get('value') == 0)) {

                    var answer = confirm(totalDebtAmountConfirm);

                    if (!answer) {

                        e.preventDefault();

                    }
                }

            });
        }

    });
    
    

    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
            num.substring(num.length - (4 * i + 3));
        }
        return (((sign) ? '' : '-') + num + '.' + cents);
    }
}

/*Needs Assessment step 1 YES/NO */


if (typeof (YESNOInit) != "undefined") {
    YUI().use('node', 'event', function(Y) {

        var radioButtons = Y.all('input.radio');
        radioButtons.each(function(radio) {
            radio.on('click', function(e) {
                var iconclass = radio.getAttribute('iconClass');
                if (radio.hasClass('yes')) {
                    Y.one('.' + iconclass).addClass('active');
                } else {
                    Y.one('.' + iconclass).removeClass('active');
                }
            });
        });
        var submitButton = Y.one("#GoToNextStep");
        submitButton.on("click", function(e) {



            var allYes = true;
            var allNo = true;

            var radioButtons = Y.all('input.radio');
            for (var i = 0; i < radioButtons._nodes.length; i++) {
                if (radioButtons._nodes[i].checked == true && radioButtons._nodes[i].className == 'radio yes') {
                    allNo = false;
                }

                if (radioButtons._nodes[i].checked == true && radioButtons._nodes[i].className == 'radio no') {
                    allYes = false;
                }
            }


            if (allYes || allNo) {
                //See Step1.ascx for WarningMessage
                var answer = confirm(WarningMessage);

                if (!answer) {

                    e.preventDefault();

                }
            }

        });
    });

}



/* My Counseling Action Plan Checkboxes */
if (typeof (ActionPlanCheckboxesInit) != "undefined") {

    YUI().use('node', 'event', 'yui2-button', 'io-base', function(Y) {

        var Ytwo = Y.YUI2;

        var checkboxesId = new Array();
        var checkboxes = Y.all('#action-plan .item .content input');
        checkboxes.each(function(node, index, list) {
            checkboxesId.push(node.get('id'));
        });

        for (i = 0; i < checkboxesId.length; i++) {
            var oCheckButton1 = new Ytwo.widget.Button(checkboxesId[i], { label: "" });
            oCheckButton1.addListener('click', CheckAndSend, oCheckButton1);
        }

        function CheckAndSend(e, button) {
            var buttonSpan;
            var targetNode = Y.one(e.target);
            if (targetNode.hasClass('yui-checkbox-button')) {
                buttonSpan = targetNode;
            }
            else {
                buttonSpan = targetNode.ancestor('.yui-checkbox-button');
            }
            if (buttonSpan.hasClass('yui-checkbox-button-checked')) {
                Y.on('io:success', function(id, o, args) {
                    var targetNode = args[0];
                    var button = args[1];
                    button._setChecked(true);
                    button.set('checked', true);
                    var buttonAncestor = targetNode.ancestor('.completeCheckbox');
                    var completeTextNode = buttonAncestor.one('span.undone');
                    completeTextNode.set('innerHTML', o.responseText);
                }, Y, [targetNode, button]);

                Y.on('io:failure', function(id, o, button) {
                    button._setChecked(false);
                    button.set('checked', false);
                }, Y, button);
                var buttonSpanId = buttonSpan.get('id');
                var actionNode = buttonSpan.ancestor('.content');
                var actionTitle = actionNode.one('h3').get('innerHTML');
                var request = Y.io("~/actionplan/actionplancheckbox.ashx?actionid=" + buttonSpanId + "&clientid=" + actionPlanClientId + "&title=" + escape(actionTitle));

                button._setChecked(true);
                button.set('checked', true);
            } else {
                button._setChecked(true);
                button.set('checked', true);
            }
        }


    });
}

//My Credit Profile Tab
//Start
if (typeof (CreditProfileTabInit) != "undefined") {

    YUI().use('tabview', function(Y) {
        var tabview = new Y.TabView({
            srcNode: '#demo'
        });

        tabview.render();
    });
    //End
}


//Chart
//Start
if (typeof (CreditProfileChartInit) != "undefined") {

    var YAHOO;

    YUI().use('yui2-charts', 'yui2-dom', 'yu2-datasource', 'yu2-json', 'yui2-element', 'yui2-swf', 'node', function(Y) {

        YAHOO = Y.YUI2;

        YAHOO.widget.Chart.SWFURL = "/static/js/yui3/2in3.1/2.8.0/build/yui2-charts/charts.swf";


        //Income & Expenses Pie Chart
        //Start
        var invalidChartNode = Y.all('.invalidTag .PieChart');
        var chartNode = Y.all('.PieChart');

        for (var i = 0; i < chartNode._nodes.length; i++) {
            var isFound = false;

            for (var j = 0; j < invalidChartNode._nodes.length; j++) {
                if (chartNode._nodes[i] == invalidChartNode._nodes[j]) {

                    isFound = true;
                    break;
                }
            }

            if (isFound) {
                chartNode._nodes[i].setAttribute('id', 'id' + i);
                continue;
            }


            var chartValue = chartNode._nodes[i].getAttribute('chartValue');

            var valueToAdd = chartValue.split('£');


            var thedata = [];

            for (var k = 0; k < valueToAdd.length; k++) {

                var info = valueToAdd[k].split('|');

                thedata.push({ response: info[0], count: info[1] });
            }






            var ds = new YAHOO.util.DataSource(thedata);
            ds.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
            ds.responseSchema = { fields: ["response", "count"] };
            var id = chartNode._nodes[i].getAttribute('id');




            //--- chart
            var mychart = new YAHOO.widget.PieChart(id, ds,
                {
                    dataField: "count",
                    categoryField: "response",
                    style: { legend: { display: "bottom" }, background: { color: "#eaf9c7"} },
                    series: [{ style: { colors: [0x114400, 0x336600, 0x558800, 0x77AA00, 0x99CC00, 0xAADD00, 0xDDFF00, 0xDDFF00]}}],
                    wmode: "transparent"
                });

        }
        //End


        //Income & Expenses Pie Chart
        //Start
        var validColunmChartNode = Y.all('.validTag .ColumnChart');
        var colunmChartNode = Y.all('.ColumnChart');

        for (var i = 0; i < colunmChartNode._nodes.length; i++) {
            var isFound = false;

            for (var j = 0; j < validColunmChartNode._nodes.length; j++) {
                if (colunmChartNode._nodes[i] == validColunmChartNode._nodes[j]) {

                    isFound = true;
                    break;
                }
            }

            if (!isFound) {
                colunmChartNode._nodes[i].setAttribute('id', 'NetWorthChar' + i);
                var divContainerId = colunmChartNode._nodes[i].getAttribute('id');
                var assets = colunmChartNode._nodes[i].getAttribute('assets');
                var liabilities = colunmChartNode._nodes[i].getAttribute('liabilities');
                break;
            }
        }




        //Income and Expense Column Chart
        //Start
        var thedata = [];
        thedata.push({ name: "", assets: assets, liabilities: liabilities });






        var myDataSource = new YAHOO.util.DataSource(thedata);
        myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
        myDataSource.responseSchema = {
            fields: ["name", "assets", "liabilities"]
        };



        //format currency
        YAHOO.example.formatCurrencyAxisLabel = function(value) {
            return YAHOO.util.Number.format(value,
	            {
	                prefix: "$",
	                thousandsSeparator: ","
	            });
        }



        var currencyAxis = new YAHOO.widget.NumericAxis();
        currencyAxis.labelFunction = YAHOO.example.formatCurrencyAxisLabel;


        //For the LiabilitiesDisplayName and AssetsDisplayName see CreditProfile.ascx
        var seriesDef = [
                        {
                            
                            displayName: AssetsDisplayName,
                            yField: "assets",
                            style: {
                                color: "#578600",
                                size: 120
                            }
                        },

                        {
                            displayName: LiabilitiesDisplayName,
                            yField: "liabilities",
                            style: {
                                color: "#5a0047",
                                size: 120

                            }
                        }


                    ];


        //DataTip function for the chart
        YAHOO.example.getDataTipText = function(item, index, series) {

            var toolTipText = series.displayName;
            if (series.yField == "liabilities") {

                toolTipText += "\n" + "-" + YAHOO.example.formatCurrencyAxisLabel(item[series.yField]);

            } else {
                toolTipText += "\n" + YAHOO.example.formatCurrencyAxisLabel(item[series.yField]);
            }


            return toolTipText;
        }


        if (divContainerId != null) {
            var myChart = new YAHOO.widget.ColumnChart(divContainerId, myDataSource, {
                xField: "name",
                yAxis: currencyAxis,
                version: "9.0.115",
                style: { background: { color: "#eaf9c7"} },
                series: seriesDef,
                dataTipFunction: YAHOO.example.getDataTipText,
                wmode: "transparent"

            });
        }


    });
}



if (typeof (PasswordRuleInit) != "undefined") {

    YUI().use('yui2-dom', 'yui2-dom', 'yui2-event', 'yui2-container', 'yui2-dragdrop', function(Y) {

        YAHOO = Y.YUI2;

        YAHOO.namespace("example.container");





        var handleShow = function(evt) {
            YAHOO.util.Event.preventDefault(evt);

            this.show();



        }



        // Instantiate a Panel from script
        YAHOO.example.container.panel1 = new YAHOO.widget.Panel("panel1", {visible: false, constraintoviewport: true, draggable: false, close: true, zIndex: 1000 });
        YAHOO.example.container.panel1.render();


        YAHOO.util.Event.addListener("show1", "click", handleShow, YAHOO.example.container.panel1, true);


        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get("myPanel"), 'display', 'block');





    });


}

if (typeof (ClientIDInit) != "undefined") {

    YUI().use('yui2-dom', 'yui2-dom', 'yui2-event', 'yui2-container', 'yui2-dragdrop', function(Y) {

        YAHOO = Y.YUI2;

        YAHOO.namespace("example.container");





        var handleShow = function(evt) {
            YAHOO.util.Event.preventDefault(evt);

            this.show();



        }



        // Instantiate a Panel from script
        YAHOO.example.container.panel1 = new YAHOO.widget.Panel("panel1", { visible: false, constraintoviewport: true, draggable: false, close: true, zIndex: 1000 });
        YAHOO.example.container.panel1.render();


        YAHOO.util.Event.addListener("show1", "click", handleShow, YAHOO.example.container.panel1, true);
        YAHOO.util.Event.addListener("show2", "click", handleShow, YAHOO.example.container.panel1, true);


        YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get("myPanel"), 'display', 'block');





    });


}

/* Recommended Courses control scroll */

if (typeof (recommendedCoursesScroll) != "undefined") {

    YUI().use('node', 'anim', function(Y) {



        var moreButtonNext = Y.one('.more-courses .next');
        var moreButtonPrev = Y.one('.more-courses .prev');
        var coursesBox = Y.one('.recommended-courses .courses-box');

        moreButtonNext.on('click', ScrollCoursesBox);
        moreButtonPrev.on('click', ScrollCoursesBox);

        var animReady = true;

        function ScrollCoursesBox(e) {
            if (animReady == true) {
                var scrollTopValue = coursesBox.get('scrollTop');
                var scrollValue = 0;
                if (e.target.hasClass('prev'))
                { scrollValue = scrollTopValue - 116; }
                else if (e.target.hasClass('next'))
                { scrollValue = scrollTopValue + 116; }

                var coursesBoxAnim = new Y.Anim({
                    node: coursesBox,
                    to: { scroll: [0, scrollValue] },
                    duration: 1,
                    easing: Y.Easing.backOut
                });
                coursesBoxAnim.run();
                coursesBoxAnim.on('end', function() { animReady = true; });
                animReady = false;
            }
        };
    });

}



/* My goal page */
if (typeof (mygoal) != "undefined") {



var Goal = {};
YUI().use('node', 'overlay', 'io', 'yui2-calendar', 'event', function(Y) {

    Goal.overlay = new Y.Overlay({
        srcNode: "#overlay"
    });
    Goal.overlay.set("centered", true);
    Goal.overlay.render();
    Goal.overlay.hide();



    function updateContentComplete(id, o, args) {
        var contentBox = Y.one('#my-goal .contentBox');
        contentBox.set('innerHTML', o.responseText);
    };

    function updateLightBoxComplete(id, o, args) {

        if (o.responseText == 'Successful') {
            Goal.overlay.hide();
        }
        else {
            var contentBox = Y.one('#my-goal .lightBox');
            contentBox.set('innerHTML', o.responseText);
            Goal.overlay.show();
            Goal.overlay.set("centered", true);
        }
    };

    function updateSaveLightBoxComplete(id, o, args) {
        updateLightBoxComplete(id, o, args);
        Goal.UpdateContent();
    };



    Goal.UpdateContent = function() {
    var uri = "/goal" + "/" + culture + "/update.aspx";
        var cfg = { on: { complete: updateContentComplete} }
        var request = Y.io(uri, cfg);
    };

    Goal.AddGoal = function() {
    var uri = "/goal" + "/" + culture + "/add.aspx";
        var cfg = { on: { complete: updateLightBoxComplete} }
        var request = Y.io(uri, cfg);
    };

    Goal.SaveGoal = function() {
        var form = Y.one('#AddGoalForm');
        var uri = "/goal" + "/" + culture + "/save.aspx";
        var cfg = { on: { complete: updateSaveLightBoxComplete },
            form: { id: form, useDisabled: true },
            method: 'POST'
        }
        var request = Y.io(uri, cfg);
    };

    Goal.AddHistory = function(id) {
    var uri = "/goal" + "/" + culture + "/history/add.aspx/" + id;
        var cfg = { on: { complete: updateLightBoxComplete} }
        var request = Y.io(uri, cfg);
    };

    Goal.SaveHistory = function() {
        var form = Y.one('#AddHistoryForm');
        var deleteCheckBox = Y.one('#AddHistoryForm input.delete');
        if (deleteCheckBox.get('checked')) {
            var uri = "/goal" + "/" + culture + "/delete.aspx";
        } else {
        var uri = "/goal" + "/" + culture + "/history/save.aspx";
        }
        var cfg = { on: { complete: updateSaveLightBoxComplete },
            form: { id: form, useDisabled: true },
            method: 'POST'

        }
        var request = Y.io(uri, cfg);
    };

    Goal.DeleteGoal = function() {
    var uri = "/goal" + "/" + culture + "/delete.aspx";
        var cfg = { on: { complete: updateSaveLightBoxComplete} }
        var request = Y.io(uri, cfg);
    };

    function setDate(e, args) {
        var dateBox = Y.one('.CalendarBox');
        var dates = Goal.calendar.getSelectedDates();
        var date = new Date(dates[0]);
        dateBox.set('value', date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear());
        Goal.calendarOverlay.hide();
    };

    var YAHOO = Y.YUI2;

    var calCfg = { mindate: '1/1/1800', maxdate: '1/1/9999', close: true, navigator: true };
    Goal.calendar = new YAHOO.widget.Calendar('cal1', 'calendar', calCfg);
    Goal.calendar.render();

    Goal.calendar.selectEvent.subscribe(setDate, Goal.calendar, true);

    Goal.calendarOverlay = new Y.Overlay({
        srcNode: "#overlayCalendar"
    });
    Goal.calendarOverlay.render();
    Goal.calendarOverlay.set("centered", true);
    Goal.calendarOverlay.hide();

    Goal.showCalendar = function() {
        Y.one('#calendar').setStyle('display','block');
        Goal.calendarOverlay.show();
        Goal.calendarOverlay.set("centered", true);
    }
});
}



//Chart
//Start
if (typeof (CscChartInit) != "undefined") {

    var YAHOO;

    YUI().use('yui2-charts', 'yui2-dom', 'yu2-datasource', 'yu2-json', 'yui2-element', 'yui2-swf', 'node', function(Y) {

        YAHOO = Y.YUI2;

        YAHOO.widget.Chart.SWFURL = "/static/js/yui3/2in3.1/2.8.0/build/yui2-charts/charts.swf";

        var divContainerId = document.getElementById("NetWorthChart");

        var myDataSource = new YAHOO.util.DataSource(datasource);
        myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
        myDataSource.responseSchema = {
            fields: ["score", "rank", "pourcentage", "date", "direction"]
        };



        var yAxis = new YAHOO.widget.NumericAxis();
        yAxis.labelFunction = function(value) { return value; };

        var seriesDef = [
                        {
                            displayName: "Score",
                            yField: "score",
                            style: {
                                color: "#578600",
                                size: 50
                            }
                        }
                        ];


        //DataTip function for the chart
        YAHOO.example.getDataTipText = function(item, index, series) {

            var tooltips = tooltipsTemplate;
            tooltips = tooltips.replace("{SCORE}", item.score);
            tooltips = tooltips.replace("{RANK}", item.rank);
            tooltips = tooltips.replace("{POURCENTAGE}", item.pourcentage);
            
            if (item.direction.toLowerCase() == "increase") {
                tooltips = tooltips.replace("{DIRECTION}", direction[0]);
            }
            else {
                tooltips = tooltips.replace("{DIRECTION}", direction[1]);
            }

            return tooltips;
        }


        if (divContainerId != null) {
            var myChart = new YAHOO.widget.ColumnChart(divContainerId, myDataSource, {
                xField: "date",
                yAxis: yAxis,
                version: "9.0.115",
                style: { background: { color: "#eaf9c7"} },
                series: seriesDef,
                dataTipFunction: YAHOO.example.getDataTipText,
                wmode: "transparent"

            });
        }


    });
}