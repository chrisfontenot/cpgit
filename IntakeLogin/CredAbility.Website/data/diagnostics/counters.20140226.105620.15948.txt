<report date='20140226T105620'>
  <category name='Sitecore.Caching'>
    <counter name='CacheClearings' value='90' category='Sitecore.Caching'/>
    <counter name='CacheHits' value='611274' category='Sitecore.Caching'/>
    <counter name='CacheMisses' value='12746' category='Sitecore.Caching'/>
    <counter name='DataCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='DataCacheHits' value='284072' category='Sitecore.Caching'/>
    <counter name='DataCacheMisses' value='9179' category='Sitecore.Caching'/>
    <counter name='HtmlCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheHits' value='0' category='Sitecore.Caching'/>
    <counter name='HtmlCacheMisses' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='PathCacheHits' value='4688' category='Sitecore.Caching'/>
    <counter name='PathCacheMisses' value='84' category='Sitecore.Caching'/>
    <counter name='RegistryCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='RegistryCacheHits' value='35818' category='Sitecore.Caching'/>
    <counter name='RegistryCacheMisses' value='43' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheHits' value='263' category='Sitecore.Caching'/>
    <counter name='ViewStateCacheMisses' value='120' category='Sitecore.Caching'/>
    <counter name='XslCacheClearings' value='0' category='Sitecore.Caching'/>
    <counter name='XslCacheHits' value='4' category='Sitecore.Caching'/>
    <counter name='XslCacheMisses' value='4' category='Sitecore.Caching'/>
  </category>
  <category name='Sitecore.Data'>
    <counter name='Data.ClientDataReads' value='3' category='Sitecore.Data'/>
    <counter name='Data.ClientDataWrites' value='2' category='Sitecore.Data'/>
    <counter name='Data.ItemsAccessed' value='195404' category='Sitecore.Data'/>
    <counter name='Data.PhysicalReads' value='188773' category='Sitecore.Data'/>
    <counter name='Data.PhysicalWrites' value='1150' category='Sitecore.Data'/>
    <counter name='Globalization.TextsTranslated' value='0' category='Sitecore.Data'/>
    <counter name='Globalization.TranslateFailed' value='27136' category='Sitecore.Data'/>
    <counter name='Links.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Links.DataUpdated' value='859' category='Sitecore.Data'/>
    <counter name='Links.FieldsExamined' value='2697' category='Sitecore.Data'/>
    <counter name='Shadows.DataRead' value='0' category='Sitecore.Data'/>
    <counter name='Shadows.DataUpdated' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.ActionsExecuted' value='0' category='Sitecore.Data'/>
    <counter name='Workflow.SecurityResolved' value='71' category='Sitecore.Data'/>
    <counter name='Workflow.StateChanges' value='0' category='Sitecore.Data'/>
  </category>
  <category name='Sitecore.Jobs'>
    <counter name='Jobs.JobsExecuted' value='94' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesAborted' value='289' category='Sitecore.Jobs'/>
    <counter name='Pipelines.PipelinesExecuted' value='11809' category='Sitecore.Jobs'/>
    <counter name='Pipelines.ProcessorsExecuted' value='30952' category='Sitecore.Jobs'/>
    <counter name='Publishing.FullPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.IncrementalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsExamined' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsPublished' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.ItemsQueued' value='36' category='Sitecore.Jobs'/>
    <counter name='Publishing.Replacements' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.SmartPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Publishing.TotalPublishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.FileCleanups' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.HtmlCacheClearings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.ItemsArchived' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.Publishings' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.RemindersSent' value='0' category='Sitecore.Jobs'/>
    <counter name='Tasks.TasksExecuted' value='0' category='Sitecore.Jobs'/>
  </category>
  <category name='Sitecore.Presentation'>
    <counter name='Context.SiteChanged' value='95' category='Sitecore.Presentation'/>
    <counter name='Context.ThreadDataAccessed' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.FastMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheHits' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Media.StandardMediaCacheMisses' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRendered' value='19107' category='Sitecore.Presentation'/>
    <counter name='Rendering.ControlsRenderedFromCache' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.PagesGenerated' value='745' category='Sitecore.Presentation'/>
    <counter name='Rendering.RenderingsRendered' value='0' category='Sitecore.Presentation'/>
    <counter name='Rendering.SublayoutsRendered' value='4' category='Sitecore.Presentation'/>
    <counter name='Rendering.XamlControlsCompiled' value='3' category='Sitecore.Presentation'/>
    <counter name='Rendering.XmlControlsCompiled' value='10' category='Sitecore.Presentation'/>
    <counter name='Rendering.XslTransformsRendered' value='8' category='Sitecore.Presentation'/>
  </category>
  <category name='Sitecore.Security'>
    <counter name='AccessDenied' value='0' category='Sitecore.Security'/>
    <counter name='AccessGranted' value='78569' category='Sitecore.Security'/>
    <counter name='AccessResolved' value='78571' category='Sitecore.Security'/>
    <counter name='ModifyRequests' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticDisabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticEnabling' value='0' category='Sitecore.Security'/>
    <counter name='ProgrammaticUserSwitch' value='0' category='Sitecore.Security'/>
    <counter name='ReadRequests' value='0' category='Sitecore.Security'/>
    <counter name='RolesResolved' value='0' category='Sitecore.Security'/>
    <counter name='UsersResolved' value='0' category='Sitecore.Security'/>
    <counter name='VirtualUsersBuilt' value='0' category='Sitecore.Security'/>
  </category>
  <category name='Sitecore.System'>
    <counter name='Events.EventsRaised' value='107' category='Sitecore.System'/>
    <counter name='Exceptions.ExceptionsThrown' value='0' category='Sitecore.System'/>
    <counter name='IO.FileWatcherEvents' value='0' category='Sitecore.System'/>
    <counter name='Logging.AuditsLogged' value='35' category='Sitecore.System'/>
    <counter name='Logging.ErrorsLogged' value='6' category='Sitecore.System'/>
    <counter name='Logging.FatalsLogged' value='0' category='Sitecore.System'/>
    <counter name='Logging.InformationsLogged' value='317' category='Sitecore.System'/>
    <counter name='Logging.WarningsLogged' value='112' category='Sitecore.System'/>
    <counter name='Reflection.MethodsInvoked' value='13202' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsCreated' value='76142' category='Sitecore.System'/>
    <counter name='Reflection.ObjectsNotCreated' value='0' category='Sitecore.System'/>
    <counter name='Reflection.TypesNotResolved' value='263' category='Sitecore.System'/>
    <counter name='Reflection.TypesResolved' value='4770' category='Sitecore.System'/>
    <counter name='Threading.BackgroundThreadsStarted' value='102' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataModified' value='0' category='Sitecore.System'/>
    <counter name='Threading.ThreadDataRead' value='0' category='Sitecore.System'/>
    <counter name='Xml.ItemNavigatorsCreated' value='296' category='Sitecore.System'/>
    <counter name='Xml.PacketsCreated' value='12' category='Sitecore.System'/>
  </category>
</report>

