﻿<%@ Page Language="C#" AutoEventWireup="true" Debug="true" %>
<%@ Import Namespace="System.Linq" %>
<%-- 
#############################################################
Author: Sean Kearney
Date: 11/13/09
Description: Find items that have 'standard value' for a specific field
#############################################################
--%>

<script runat="server">
    protected string StartingPath
    {
        get
        {
            return txtStartPath.Text;
        }
    }

    protected string FieldName
    {
        get
        {
            return txtFieldName.Text;
        }
    }

    protected string[] ReportFields
    {
        get
        {
            return txtReportFieldNames.Text.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
    
    protected void btnReport_Click(object sender, EventArgs e)
    {
        Execute(false);
    }

    protected void Execute(bool performAction)
    {
        if (string.IsNullOrEmpty(StartingPath))
            throw new Exception("Starting Path Not Set");

        Sitecore.Data.Database master = Sitecore.Configuration.Factory.GetDatabase("master");

        Response.Write("<html><head><style type='text/css'>*{font-family:monospace;font-size:10pt;}</style></head><body><table border=1>");
        Response.Write("<tr><th>Content Path</th>");
        foreach (string field in ReportFields)
        {
            Response.Write("<th>" + field + "</th>");
        }
        Response.Write("</tr>");
        
        using (new Sitecore.SecurityModel.SecurityDisabler())
        {
            Recurse(master.GetItem(StartingPath), performAction);
        }
        

        Response.Write("</table></body></html>");
        Response.End();
    }

    private void Recurse(Sitecore.Data.Items.Item item, bool performAction)
    {
        foreach (Sitecore.Data.Items.Item child in item.Children)
        {
            if (child.Fields[FieldName] != null && child.Fields[FieldName].ContainsStandardValue)
            {
                Response.Write(string.Format("<tr><td nowrap='nowrap'><a href='{0}.aspx' target='_blank'>{0}</a></td>", child.Paths.ContentPath));

                foreach (string field in ReportFields)
                {
                    Response.Write("<td>" + child[field] + "</td>");
                }
                
                Response.Write("</tr>");

                //if (performAction)
                //{
                //    using (new Sitecore.Data.Items.EditContext(child))
                //    {
                //        child.Fields[FieldName].Value = NewValue;
                //    }
                //}
            }
            
            // Keep going
            Recurse(child, performAction);
        }
    }
    
</script>

<html>
<body>
    <form id="form1" runat="server">
    <fieldset>
        Starting Path: <asp:TextBox ID="txtStartPath" runat="server" Text="/sitecore/content/home" /> <br />
        Field Name to Check:  <asp:TextBox ID="txtFieldName" runat="server" Text="ProductMediaImages" /> <br />
        Fields to show in report (separate fields by a comma):  <asp:TextBox ID="txtReportFieldNames" runat="server" Text="ProductNumber" /> <br />
        <asp:Button ID="btnReport" runat="server" OnClick="btnReport_Click" Text="Run Report" />
    </fieldset>
    </form>
</body>
</html>
