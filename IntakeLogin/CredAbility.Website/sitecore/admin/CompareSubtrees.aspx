﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompareSubtrees.aspx.cs" Inherits="Sitecore.sitecore.admin.CompareSubtrees" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Comparing subtrees</title>
</head>
<body>
    <form id="form" runat="server">
    <div>
        <table>
            <tr>
                <td>First item. Database:</td>
                <td><asp:DropDownList ID="lstDatabase1" runat="server" Width="150" /></td>
                <td>ItemID</td>
                <td><asp:TextBox ID="txtId1" runat="server" Width="150" /> </td>
            </tr>
            <tr>
                <td>Second item. Database:</td>
                <td><asp:DropDownList ID="lstDatabase2" runat="server" Width="150" /></td>
                <td>ItemID</td>
                <td><asp:TextBox ID="txtId2" runat="server" Width="150" /> </td>
            </tr>            
        </table>
        <asp:Button ID="btnGo" runat="server" OnClick="Go" Text="Compare"/>
        <asp:Panel runat="server" ID="pnlResult" Visible="false">
            <br /><br />
            <table id="tblResult" runat="server" border="1">
            <tr>
            <td>Item</td>
            <td>Difference type</td>
            <td>Full description</td>
            </tr>
            </table>
        </asp:Panel>
        <br />
    </div>
    </form>
</body>
</html>
