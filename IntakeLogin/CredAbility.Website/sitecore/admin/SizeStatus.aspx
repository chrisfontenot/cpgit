<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SizeStatus.aspx.cs" Inherits="Sitecore.layouts.SizeStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Size status</title>
</head>
<body>
  <form id="form1" runat="server">
    <div>
      <div style="font:bold 13px verdana; margin-bottom:8px;">
        Statistics in bytes (all data)
      </div>
      <table border="1" cellspacing="0" cellpadding="4" style="border-collapse:collapse; font-family:verdana; font-size:11px;">
        <tr>
          <td>Space limit:</td>
          <td align="right"><asp:label ID="c_limit" runat="server" text="[limit]" /></td>
        </tr>
        <tr>
          <td>Space used:</td>
          <td align="right"><asp:label id="c_used" runat="server" text="[used]" /></td>
        </tr>
        <tr>
          <td>Space remaining:</td>
          <td align="right"><asp:label ID="c_remaining" runat="server" text="[remaining]" /></td>
        </tr>
        <tr>
          <td>Base size in source code:</td>
          <td align="right"><asp:label ID="c_base" runat="server" text="[base]" /></td>
        </tr>
        <tr>
          <td>Calculated base size:</td>
          <td align="right"><asp:label id="c_baseUsed" runat="server" text="[base used]" /></td>
        </tr>
        <tr>
          <td>Min. entity size:</td>
          <td align="right"><asp:label ID="c_minEntity" runat="server" text="[minEntity]" /></td>
        </tr>
        <tr>
          <td>Max. entity size:</td>
          <td align="right"><asp:label ID="c_maxEntity" runat="server" text="[maxEntity]" /></td>
        </tr>
        <tr>
          <td>Min. dictionary entries:</td>
          <td align="right"><asp:label ID="c_minDictionaryEntries" runat="server" text="[minDictionaryEntries]" /></td>
        </tr>
        <tr>
          <td>Actual dictionary entries:</td>
          <td align="right"><asp:label ID="c_dictionaryEntries" runat="server" text="[dictionaryEntries]" /></td>
        </tr>
      </table>
    </div>
    <div>
      <div style="font:bold 13px verdana; margin-top:20px; margin-bottom:8px;">
        Statistics in percent (all data)
      </div>
      <table border="1" cellspacing="0" cellpadding="4" style="border-collapse:collapse; font-family:verdana; font-size:11px;">
        <tr>
          <td>Space used:</td>
          <td align="right"><asp:label id="c_usedPct" runat="server" text="[used]" /></td>
        </tr>
        <tr>
          <td>Space remaining:</td>
          <td align="right"><asp:label ID="c_remainingPct" runat="server" text="[remaining]" /></td>
        </tr>
      </table>
    </div>
    <div>
      <div style="font:bold 13px verdana; margin-top:20px; margin-bottom:8px;">
        Statistics in bytes (user data)
      </div>
      <table border="1" cellspacing="0" cellpadding="4" style="border-collapse:collapse; font-family:verdana; font-size:11px;">
        <tr>
          <td>Space limit:</td>
          <td align="right"><asp:label ID="c_limitUser" runat="server" text="[limit]" /></td>
        </tr>
        <tr>
          <td>Space used:</td>
          <td align="right"><asp:label id="c_usedUser" runat="server" text="[used]" /></td>
        </tr>
        <tr>
          <td>Space remaining:</td>
          <td align="right"><asp:label ID="c_remainingUser" runat="server" text="[remaining]" /></td>
        </tr>
      </table>
    </div>
    <div>
      <div style="font:bold 13px verdana; margin-top:20px; margin-bottom:8px;">
        Statistics in percent (user data)
      </div>
      <table border="1" cellspacing="0" cellpadding="4" style="border-collapse:collapse; font-family:verdana; font-size:11px;">
        <tr>
          <td>Space used:</td>
          <td align="right"><asp:label id="c_usedUserPct" runat="server" text="[used]" /></td>
        </tr>
        <tr>
          <td>Space remaining:</td>
          <td align="right"><asp:label ID="c_remainingUserPct" runat="server" text="[remaining]" /></td>
        </tr>
      </table>
    </div>
    <div>
      <div style="font:bold 13px verdana; margin-top:20px; margin-bottom:8px;">
        Details
      </div>
      <asp:placeholder id="c_details" runat="server"/>
    </div>
  </form>
</body>
</html>
