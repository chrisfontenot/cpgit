<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import namespace="Sitecore.Collections"%>
<%@ Import namespace="Sitecore.Data.Items"%>
<%@ Import namespace="Sitecore.Web"%>
<%@ Import namespace="System.Data.SqlClient"%>
<%@ Import namespace="Sitecore.IO"%>
<%@ Import namespace="System.Xml"%>
<%@ Import namespace="Sitecore"%>
<%@ Import namespace="Sitecore.Configuration"%>
<%@ Import namespace="System.Collections.Generic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script language="C#" runat="server">

  /// <summary>Page load handler</summary>
  void Page_Load() {
    if(Request.Form.Count > 0){
      HtmlTable table = InitializeTable();

      ShowSizes(table);

      Controls.Add(table);
    }
  }

  /// <summary></summary>
  HtmlTable InitializeTable() {
    HtmlTable table = new HtmlTable();

    table.CellPadding = 4;
    table.CellSpacing = 0;
    table.Border = 1;
    table.Style["border-collapse"] = "collapse";
    table.Attributes["class"] = "text";

    HtmlTableRow row = HtmlUtil.AddRow(table, "Database", "Size");

    for(int n = 0; n < row.Cells.Count; n++) {
      row.Cells[n].Style["font-weight"] = "bold";
    }

    return table;
  }

  /// <summary></summary>
  void ShowSizes(HtmlTable table) {
    long totalSize = 0;
    
    SafeDictionary<string, string> connections = GetConnections();

    foreach(string key in connections.Keys){
      long size = GetSize(connections[key]);

      ShowSize(key, size, table);

      totalSize += size;
    }

    ShowTotalSize(totalSize, table);
  }

  /// <summary></summary>
  SafeDictionary<string, string> GetConnections() {
    SafeDictionary<string, string> result = new SafeDictionary<string, string>();
    
    XmlNodeList nodes = Factory.GetConfigNodes("connections/node()");

    foreach(XmlNode node in nodes){
      string connection = Factory.CreateObject(node, true).ToString();

      result[node.Name] = MapFileNames(connection);
    }

    return result;
  }

  /// <summary></summary>
  long GetSize(string connectionString) {
    string sql = GetSizeSql();

    string prefix = IsItemBased() ? "item" : "field";

    int minSize = MainUtil.GetInt(WebUtil.GetFormValue(prefix + "Min"), 0);
    int maxSize = MainUtil.GetInt(WebUtil.GetFormValue(prefix + "Max"), 0);

    using(SqlConnection connection = new SqlConnection(connectionString)){
      connection.Open();

      using(SqlCommand command = new SqlCommand(sql, connection)){
        command.Parameters.AddWithValue("@minimum", minSize);
        command.Parameters.AddWithValue("@maximum", maxSize);
        
        using(SqlDataReader reader = command.ExecuteReader()){
          if(reader.Read()){
            return reader.GetInt64(0);
          }
        }
      }
    }

    return 0;
  }

  /// <summary></summary>
  string GetSizeSql() {
    if(IsItemBased()) {
      return GetSizeSqlItemBased();
    }

    return GetSizeSqlFieldBased();
  }

  /// <summary></summary>
  string GetSizeSqlFieldBased() {
    return " SELECT SUM(" +
           "   CASE" +
           "     WHEN [Length] < @minimum THEN @minimum" +
           "     WHEN @maximum > 0 AND [Length] > @maximum THEN @maximum" +
           "     ELSE [Length]" +
           "   END" +
           " ) AS TotalSize" +
           " FROM (" +
           "   SELECT LEN([Value]) AS Length" +
           "   FROM [VersionedFields]" +
           "   UNION ALL" +
           "   SELECT LEN([Value]) AS Length" +
           "   FROM [UnversionedFields]" +
           "   UNION ALL" +
           "   SELECT LEN([Value]) AS Length" +
           "   FROM [SharedFields]" +
           " ) AS Lengths";
  }

  /// <summary></summary>
  string GetSizeSqlItemBased() {
    return " SELECT SUM(" +
           "   CASE" +
           "     WHEN [Length] < @minimum THEN @minimum" +
           "     WHEN @maximum > 0 AND [Length] > @maximum THEN @maximum" +
           "     ELSE [Length]" +
           "   END" +
           " ) AS TotalSize" +
           " FROM (" +
           "   SELECT SUM(LEN([Value])) AS Length FROM (" +
           "     SELECT [ItemId], [Value]" +
           "     FROM [VersionedFields]" +
           "     UNION ALL" +
           "     SELECT [ItemId], [Value]" +
           "     FROM [UnversionedFields]" +
           "     UNION ALL" +
           "     SELECT [ItemId], [Value]" +
           "     FROM [SharedFields]" +
           "   ) AS [Values]" +
           "   GROUP BY [ItemId]" +
           " ) AS Lengths";
  }

  /// <summary></summary>
  bool IsItemBased() {
    return !string.IsNullOrEmpty(Request.Form["itemBased"]);
  }

  /// <summary>
  /// Maps the file names embedded in a connection string.
  /// </summary>
  /// <param name="connectionString">The connection string.</param>
  /// <returns></returns>
  public static string MapFileNames(string connectionString) {
    string marker = "attachdbfilename=";

    int index = connectionString.IndexOf(marker, StringComparison.OrdinalIgnoreCase);

    if(index >= 0) {
      int startPos = index + marker.Length;
      int endPos = connectionString.IndexOf(';', startPos);

      if(endPos < 0) {
        endPos = connectionString.Length;
      }

      int length = endPos-startPos;

      string file = FileUtil.MapPath(connectionString.Substring(startPos, length));

      return StringUtil.Left(connectionString, startPos) + file + StringUtil.Mid(connectionString, endPos);
    }

    return connectionString;
  }

  /// <summary></summary>
  void ShowSize(string databaseName, long size, HtmlTable table) {
    HtmlTableRow row = HtmlUtil.AddRow(table, databaseName, size.ToString("0,0"));

    row.Cells[1].Align = "right";
  }

  /// <summary></summary>
  void ShowTotalSize(long totalSize, HtmlTable table) {
    HtmlTableRow row = HtmlUtil.AddRow(table, "Total", totalSize.ToString("0,0"));

    row.Cells[1].Align = "right";

    for(int n = 0; n < row.Cells.Count; n++) {
      row.Cells[n].Style["font-weight"] = "bold";
    }
  }

  </script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <title>Size tester</title>
  <style>
    table{
      border-width:1px;
      border-collapse:collapse;
    }
    div.divider{
      margin-top:40px;
    }
    .text,
    .header{
      font-family: verdana;
      font-size: 11px;
    }
    .header{
      font-weight:bold;
      font-size:larger;
    }
    .itemForm{
      background-color:#CCCCCC;
    }
    .fieldForm{
      background-color:#888888;
    }
  </style>
</head>
<body>
    <form id="form1" runat="server">
      <table border="1" cellpadding="4" cellspacing="0" class="text itemForm">
        <tr>
          <td colspan="2" class="header">Item based</td>
        </tr>
        <tr>
          <td>Minimum size:</td>
          <td><input type="text" name="itemMin" value="<%=Request.Form["itemMin"] ?? "0"%>"/></td>
        </tr>
        <tr>
          <td>Maximum size:</td>
          <td><input type="text" name="itemMax" value="<%=Request.Form["itemMax"] ?? "0"%>"/></td>
        </tr>
        <tr>
          <td colspan="2" align="right"><input type="submit" value="Submit" name="itemBased"/></td>
        </tr>
      </table>
      <div class="divider"></div>
      <table border="1" cellpadding="4" cellspacing="0" class="text fieldForm">
        <tr>
          <td colspan="2" class="header">Field based</td>
        </tr>
        <tr>
          <td>Minimum size:</td>
          <td><input type="text" name="fieldMin" value="<%=Request.Form["fieldMin"] ?? "0"%>"/></td>
        </tr>
        <tr>
          <td>Maximum size:</td>
          <td><input type="text" name="fieldMax" value="<%=Request.Form["fieldMax"] ?? "0"%>"/></td>
        </tr>
        <tr>
          <td colspan="2" align="right"><input type="submit" value="Submit" name="fieldBased"/></td>
        </tr>
      </table>
      <div class="divider"></div>
    </form>
</body>
</html>
