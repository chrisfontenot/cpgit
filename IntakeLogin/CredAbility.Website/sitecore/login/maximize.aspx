﻿<%@ Page Language="C#" %>
<%@ Import namespace="Sitecore"%>

<script runat="server">
  string GetFullscreen() {
    bool fullscreen = Sitecore.Web.WebUtil.GetQueryString("fu") == "1";

    Sitecore.Configuration.State.Client.IsFullscreen = fullscreen;
    
    return fullscreen ? ",fullscreen=yes" : String.Empty;
  }
</script>

<script type="text/javascript" language="javascript">
  window.open("<% =System.Web.HttpUtility.UrlDecode(Sitecore.Web.WebUtil.GetQueryString("re")) %>", "_blank", 
    "location=no,menubar=no,status=no,titlebar=no,toolbar=no,resizable=yes<% =GetFullscreen() %>");
</script>

<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Sitecore</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
  <body>
     <div align="center" style="padding:64px 0px 0px 0px">
       <h1>This window is no longer used.</h1>
       <h2>You can safely close it...</h2>

       <% if (!UIUtil.IsFirefox()) { %>
       <div align="center" style="padding:16px 0px 16px 0px">
          <button onclick="javascript:window.close()">Close Window</button>
       </div>
       <% } %>

       <h2>or</h2>

       <div align="center" style="padding:16px 0px 0px 0px">
         <button onclick="javascript:location.href='/sitecore/login'">Login to Sitecore</button>
       </div>
     </div>
  </body>
</html>

