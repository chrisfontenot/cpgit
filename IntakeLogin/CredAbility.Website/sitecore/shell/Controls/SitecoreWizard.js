function scKeyDown(evt) {
  evt = window.event;

  if (evt.keyCode == 27) {
    var ctl = scForm.browser.getControl("CancelButton");
    if (ctl != null) {
      ctl.click();
    }
  }
  
  if (evt.keyCode == 13 && evt.srcElement.tagName != "TEXTAREA") {
    var ctl = scForm.browser.getControl("NextButton");
    
    if (ctl != null && window.event.srcElement != ctl) {
      ctl.click();
    }
  }
}

function scWizardInitialize() {
  scForm.browser.attachEvent(document, "onkeydown", scKeyDown);
}

scForm.browser.attachEvent(window, "onload", scWizardInitialize);

