function scSitecoreSidebar() {
  this.timer = null;
}

scSitecoreSidebar.prototype.expand = function() {
  var frame = scForm.browser.getFrameElement(window);
  
  if (this.isIE()) {
    frame.style.removeExpression("left");
    
    frame.style.setExpression("left", "parentNode.clientWidth-250");
    
    frame.style.width = "250px";
  }
  else {
    frame.style.width = "250px";
  }
}

scSitecoreSidebar.prototype.collapse = function() {
  var frame = scForm.browser.getFrameElement(window);
  
  if (this.isIE()) {
    frame.style.removeExpression("left");
    
    frame.style.width = "6";
    
    frame.style.setExpression("left", "parentNode.clientWidth-6");
  }
  else {
    frame.style.width = "6";
  }
}

scSitecoreSidebar.prototype.isIE = function() {
  var agent = navigator.userAgent.toLowerCase();
  
  return (agent.indexOf("msie") >= 0 && agent.indexOf("opera") < 0);
}

scSitecoreSidebar.prototype.mouseLeave = function() {
  if (this.timer != null) {
    clearTimeout(this.timer);
    this.timer = null;
  }
  
  this.timer = setTimeout("scSidebar.collapse()", 750);
}

scSitecoreSidebar.prototype.mouseEnter = function() {
  if (this.timer != null) {
    clearTimeout(this.timer);
    this.timer = null;
  }
  
  this.expand();
}

function scSidebarMouseEnter() {
  scSidebar.mouseEnter();
}

function scSidebarMouseLeave() {
  scSidebar.mouseLeave();
}

function scSidebarInitialize() {
  scSidebar = new scSitecoreSidebar();

  if (scSidebar.isIE()) {
    scForm.browser.attachEvent(window.document.body, "onmouseenter", scSidebarMouseEnter);
    scForm.browser.attachEvent(window.document.body, "onmouseleave", scSidebarMouseLeave);
  }
  else {
    scForm.browser.attachEvent(window.document.body, "onmouseover", scSidebarMouseEnter);
    scForm.browser.attachEvent(window.document.body, "onmouseout", scSidebarMouseLeave);
  }
}

scForm.browser.attachEvent(window, "onload", scSidebarInitialize);
