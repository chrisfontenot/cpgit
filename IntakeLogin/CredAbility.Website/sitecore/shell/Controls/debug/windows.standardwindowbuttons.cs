using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class StandardWindowButtons_a_80 : Sitecore.Shell.Controls.Windows.StandardWindowButtonsXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Align", "right", "Style", "position:relative;top:-1px") as Sitecore.Web.UI.HtmlControls.Border;
      MinimizeButton = AddControl("WindowButton", "", border1, "", "Src", "Images/Window Management/Minimize.png", "Click", "javascript:scWin.minimizeWindow()", "Width", "24", "Height", "13", "Margin", "0px 1px 0px 0px", "ToolTip", "Minimize") as Sitecore.Web.UI.XmlControls.XmlControl;
      MaximizeButton = AddControl("WindowButton", "", border1, "", "Src", "Images/Window Management/Maximize.png", "Click", "javascript:scWin.maximizeWindow()", "Width", "24", "Height", "13", "Margin", "0px 1px 0px 0px", "ToolTip", "Maximize/Restore") as Sitecore.Web.UI.XmlControls.XmlControl;
      CloseButton = AddControl("WindowButton", "", border1, "", "Src", "Images/Window Management/Close_red.png", "Click", "javascript:scWin.closeWindow()", "Width", "38", "Height", "13", "Margin", "0px 4px 0px 0px", "ToolTip", "Close") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

