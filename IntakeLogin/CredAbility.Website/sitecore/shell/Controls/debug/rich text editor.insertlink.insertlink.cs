using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class RichText_InsertLink_a_124 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public System.Web.UI.Control script2;
    public System.Web.UI.Control text3;
    public System.Web.UI.Control script4;
    public System.Web.UI.Control text5;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside6;
    public Sitecore.Web.UI.HtmlControls.DataContext InternalLinkDataContext;
    public Sitecore.Web.UI.HtmlControls.DataContext MediaDataContext;
    public Sitecore.Web.UI.HtmlControls.Tabstrip Tabs;
    public Sitecore.Web.UI.HtmlControls.Tab InternalLinkTab;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox8;
    public Sitecore.Web.UI.WebControls.TreeviewEx InternalLinkTreeview;
    public Sitecore.Web.UI.HtmlControls.Tab MediaTab;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox10;
    public Sitecore.Web.UI.WebControls.TreeviewEx MediaTreeview;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.HtmlControls.Button button12;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/link.png", "Header", "Insert a Link", "Text", "Select the item or media that you want to create a link to.", "OKButton", "Link") as Sitecore.Web.UI.XmlControls.XmlControl;
      script2 = AddControl("script", "", formdialog1, "", "Type", "text/javascript", "Language", "javascript", "Src", "/sitecore/shell/RadControls/Editor/Scripts/7_2_0/RadWindow.js");
      text3 = AddLiteral(".", "", script2, "");
      script4 = AddControl("script", "", formdialog1, "", "Type", "text/javascript", "Language", "javascript", "Src", "/sitecore/shell/Controls/Rich Text Editor/InsertLink/InsertLink.js");
      text5 = AddLiteral(".", "", script4, "");
      codebeside6 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Controls.RichTextEditor.InsertLink.InsertLinkForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      InternalLinkDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("InternalLinkDataContext")) as Sitecore.Web.UI.HtmlControls.DataContext;
      MediaDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("MediaDataContext"), "Root", "{3D6658D8-A0BF-4E75-B3E2-D050FABCF4E1}") as Sitecore.Web.UI.HtmlControls.DataContext;
      Tabs = AddControl(new Sitecore.Web.UI.HtmlControls.Tabstrip(), formdialog1, "", "ID", idref("Tabs"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Tabstrip;
      InternalLinkTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("InternalLinkTab"), "Header", "Internal Link") as Sitecore.Web.UI.HtmlControls.Tab;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), InternalLinkTab, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox8 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel7, "", "Width", "100%", "Height", "100%", "GridPanel.VAlign", "top", "Background", "white", "Border", "none", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      InternalLinkTreeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox8, "", "ID", idref("InternalLinkTreeview"), "DataContext", "InternalLinkDataContext", "Root", "true") as Sitecore.Web.UI.WebControls.TreeviewEx;
      MediaTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("MediaTab"), "Header", "Media Items") as Sitecore.Web.UI.HtmlControls.Tab;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), MediaTab, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox10 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "none", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      MediaTreeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox10, "", "ID", idref("MediaTreeview"), "DataContext", "MediaDataContext", "Root", "true") as Sitecore.Web.UI.WebControls.TreeviewEx;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel9, "", "Align", "right", "Style", "border-top:1px solid #919b9c", "Padding", "4px 8px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button12 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border11, "", "Header", "Upload", "Click", "media:upload(edit=1,load=1)") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(InternalLinkDataContext, "ID", idref("InternalLinkDataContext"));
      SetProperty(MediaDataContext, "ID", idref("MediaDataContext"));
      SetProperty(Tabs, "ID", idref("Tabs"));
      SetProperty(InternalLinkTab, "ID", idref("InternalLinkTab"));
      SetProperty(InternalLinkTreeview, "ID", idref("InternalLinkTreeview"));
      SetProperty(MediaTab, "ID", idref("MediaTab"));
      SetProperty(MediaTreeview, "ID", idref("MediaTreeview"));
    }
  }
}

