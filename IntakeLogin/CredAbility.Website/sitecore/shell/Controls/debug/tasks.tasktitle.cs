using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TaskTitle_a_101 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Space space4;
    public System.Web.UI.Control placeholder5;
    public Sitecore.Web.UI.HtmlControls.Border Home;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    
    public string m_Icon;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage1, "Src", Icon);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      themedimage1 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), this, "", "Src", Icon, "Width", "32", "Height", "32", "Style", "position:absolute;left:8px;top:4px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", "scTaskGradient") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border2, "", "Columns", "3", "Width", "100%", "class", "scTaskTitle") as Sitecore.Web.UI.WebControls.GridPanel;
      space4 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel3, "", "Width", "48", "Height", "28") as Sitecore.Web.UI.HtmlControls.Space;
      placeholder5 = AddPlaceholder("", gridpanel3, "", "GridPanel.Width", "100%");
      Home = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Class", "scRollOver", "RollOver", "true", "ID", idref("Home"), "Padding", "0px 8px 0px 0px", "GridPanel.NoWrap", "true", "GridPanel.Align", "right", "Click", "#") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), Home, "", "Src", "Network/24x24/home.png", "Width", "24", "Height", "24", "Align", "absmiddle", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Home, "", "Text", "Home") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Home, "ID", idref("Home"));
    }
  }
}

