using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class FormPage_a_51 : Sitecore.Shell.Controls.Standard.FormPageXmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl webpanel1;
    public System.Web.UI.Control placeholder2;
    public Sitecore.Web.UI.XmlControls.XmlControl globalkeys3;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      webpanel1 = AddControl("WebPanel", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Body = AddControl("body", "", webpanel1, "");
      Form = AddControl("html:HtmlForm", "", Body, "", "Style", "width:100%;height:100%");
      placeholder2 = AddPlaceholder("", Form, "");
      globalkeys3 = AddControl("GlobalKeys", "", Body, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

