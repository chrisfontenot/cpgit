using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TemplateDataContext_a_111 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.DataContext datacontext1;
    
    public string m_ID;
    public string m_Folder;
    
    // properties
    public new string ID {
      get {
        return StringUtil.GetString(m_ID);
      }
      set {
        m_ID = value;
        
        SetProperty(datacontext1, "ID", ID);
      }
    }
    
    public string Folder {
      get {
        return StringUtil.GetString(m_Folder);
      }
      set {
        m_Folder = value;
        
        SetProperty(datacontext1, "Folder", Folder);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      datacontext1 = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), this, "", "ID", ID, "DataViewName", "Master", "Folder", Folder, "Root", "{3C1715FE-6A13-4FCF-845F-DE308BA9741D}", "Filter", "not(@@key='__standard values') and Contains('{E3E2D58C-DF95-4230-ADC9-279924CECE84},{A87A00B1-E6DB-45AB-8B54-636FEC3B5523},{AB86861A-6030-46C5-B394-E8F99E8B87DB},{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B},{0437FEE2-44C9-46A6-ABE9-28858D9FEE8C},{35E75C72-4985-4E09-88C3-0EAC6CD1E64F},{85ADBF5B-E836-4932-A333-FE0F9FA1ED1E}', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      
      _Mode = "";
    }
  }
}

