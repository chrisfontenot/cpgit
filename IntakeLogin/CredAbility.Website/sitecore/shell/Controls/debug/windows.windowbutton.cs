using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WindowButton_a_81 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.ImageButton imagebutton1;
    
    public string m_Src;
    public string m_Width;
    public string m_Height;
    public string m_ToolTip;
    public string m_Margin;
    public string m_Align;
    public string m_Click;
    
    // properties
    public string Src {
      get {
        return StringUtil.GetString(m_Src);
      }
      set {
        m_Src = value;
        
        SetProperty(imagebutton1, "Src", Src);
      }
    }
    
    public new string Width {
      get {
        return StringUtil.GetString(m_Width);
      }
      set {
        m_Width = value;
        
        SetProperty(imagebutton1, "Width", Width);
      }
    }
    
    public new string Height {
      get {
        return StringUtil.GetString(m_Height);
      }
      set {
        m_Height = value;
        
        SetProperty(imagebutton1, "Height", Height);
      }
    }
    
    public new string ToolTip {
      get {
        return StringUtil.GetString(m_ToolTip);
      }
      set {
        m_ToolTip = value;
        
        SetProperty(imagebutton1, "Alt", ToolTip);
      }
    }
    
    public new string Margin {
      get {
        return StringUtil.GetString(m_Margin);
      }
      set {
        m_Margin = value;
        
        SetProperty(imagebutton1, "Margin", Margin);
      }
    }
    
    public string Align {
      get {
        return StringUtil.GetString(m_Align);
      }
      set {
        m_Align = value;
        
        SetProperty(imagebutton1, "Align", Align);
      }
    }
    
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(imagebutton1, "Click", Click);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      imagebutton1 = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), this, "", "Src", Src, "Width", Width, "Height", Height, "Alt", ToolTip, "Margin", Margin, "Align", Align, "Style", "cursor:default", "Click", Click) as Sitecore.Web.UI.HtmlControls.ImageButton;
      
      _Mode = "";
    }
  }
}

