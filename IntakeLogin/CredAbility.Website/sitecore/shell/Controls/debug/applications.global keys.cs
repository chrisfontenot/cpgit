using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class GlobalKeys_a_53 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.RegisterKey registerkey1;
    public Sitecore.Web.UI.HtmlControls.KeyMap keymap2;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      registerkey1 = AddControl(new Sitecore.Web.UI.HtmlControls.RegisterKey(), this, "", "KeyCode", "120", "Click", "system:publish") as Sitecore.Web.UI.HtmlControls.RegisterKey;
      keymap2 = AddControl(new Sitecore.Web.UI.HtmlControls.KeyMap(), this, "") as Sitecore.Web.UI.HtmlControls.KeyMap;
      
      _Mode = "";
    }
  }
}

