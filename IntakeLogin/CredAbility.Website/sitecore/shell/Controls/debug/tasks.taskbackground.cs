using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TaskBackground_a_100 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox2;
    public System.Web.UI.Control placeholder3;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Width", "100%", "Height", "100%", "Style", "filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#6375D6', EndColorStr='#6375D6')") as Sitecore.Web.UI.HtmlControls.Border;
      scrollbox2 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), border1, "", "Width", "100%", "Height", "100%", "Border", "none", "Padding", "0px", "Background", "#6375D6 url(/sitecore/shell/themes/standard/images/controlpanel.png) no-repeat bottom right") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      placeholder3 = AddPlaceholder("", scrollbox2, "");
      
      _Mode = "";
    }
  }
}

