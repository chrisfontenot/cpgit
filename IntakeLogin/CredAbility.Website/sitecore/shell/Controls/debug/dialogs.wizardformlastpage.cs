using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WizardFormLastPage_a_95 : Sitecore.Web.UI.Pages.WizardDialogBaseXmlControl   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public System.Web.UI.Control placeholder8;
    
    public string m_Icon;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage4, "Src", Icon);
        SetProperty(themedimage6, "Src", Icon);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Height", "100%", "Columns", "2", "Style", "display:none") as Sitecore.Web.UI.WebControls.GridPanel;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "Width", "164", "GridPanel.Style", "background:#123960; filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#123960', EndColorStr='#405E7A')", "GridPanel.Height", "100%", "GridPanel.Width", "164", "GridPanel.VAlign", "bottom", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "", "Align", "center", "Border", "2px solid white", "Style", "position:absolute;top:8px; left:102px; width:58px; height:58px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage4 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border3, "", "Src", Icon, "Height", "48", "Width", "48") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "", "Align", "right", "Width", "128px", "Height", "128px", "Style", "overflow:hidden; filter:progid:DXImageTransform.Microsoft.BasicImage(opacity=0.20, grayscale=1);-moz-opacity:0.2; clip: rect(0px 128px 128px 0px)") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border5, "", "Src", Icon, "Width", "196", "Height", "196") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel1, "", "CellPadding", "8px", "Width", "100%", "Height", "100%", "GridPanel.Class", "scWizardWelcomePanel", "GridPanel.VAlign", "top", "GridPanel.Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder8 = AddPlaceholder("", gridpanel7, "", "GridPanel.VAlign", "top");
      
      _Mode = "";
    }
  }
}

