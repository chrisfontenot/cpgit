using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WizardFormPage_a_92 : Sitecore.Web.UI.Pages.WizardDialogBaseXmlControl   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage8;
    public Sitecore.Web.UI.HtmlControls.Space space9;
    public Sitecore.Web.UI.HtmlControls.Space space10;
    public System.Web.UI.Control placeholder11;
    
    public string m_Header;
    public string m_Text;
    public string m_Icon;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal5, "Text", Header);
      }
    }
    
    public string Text {
      get {
        return StringUtil.GetString(m_Text);
      }
      set {
        m_Text = value;
        
        SetProperty(literal7, "Text", Text);
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage8, "Src", Icon);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Height", "100%", "Style", "display:none") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel1, "", "Columns", "2", "Height", "64px", "GridPanel.Class", "scWizardHeader") as Sitecore.Web.UI.WebControls.GridPanel;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border3, "", "Class", "scWizardTitle") as Sitecore.Web.UI.HtmlControls.Border;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border4, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border3, "", "Class", "scWizardText") as Sitecore.Web.UI.HtmlControls.Border;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "Text", Text) as Sitecore.Web.UI.HtmlControls.Literal;
      themedimage8 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel2, "", "Src", Icon, "Width", "32", "Height", "32", "Margin", "0px 8px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space9 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel1, "", "GridPanel.Class", "scBottomEdge") as Sitecore.Web.UI.HtmlControls.Space;
      space10 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel1, "", "GridPanel.Class", "scTopEdge") as Sitecore.Web.UI.HtmlControls.Space;
      placeholder11 = AddPlaceholder("", gridpanel1, "", "GridPanel.Height", "100%", "GridPanel.VAlign", "top");
      
      _Mode = "";
    }
  }
}

