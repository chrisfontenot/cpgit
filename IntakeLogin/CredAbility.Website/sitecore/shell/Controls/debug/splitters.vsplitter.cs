using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class VSplitter_a_84 : Sitecore.Shell.Controls.Splitters.VSplitterXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreVSplitter.js") as Sitecore.Web.UI.HtmlControls.Script;
      Image = AddControl(new Sitecore.Web.UI.HtmlControls.Image(), this, "", "Src", "/sitecore/images/blank.gif", "Cursor", "move", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Image;
      
      _Mode = "";
    }
  }
}

