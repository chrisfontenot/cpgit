using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class FormDialog_a_97 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.XmlControls.XmlControl formpage4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Literal literal11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Space space15;
    public System.Web.UI.Control placeholder16;
    public Sitecore.Web.UI.HtmlControls.Space space17;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox18;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel19;
    public System.Web.UI.Control placeholder20;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.Space space23;
    public Sitecore.Web.UI.HtmlControls.Button OK;
    public Sitecore.Web.UI.HtmlControls.Literal literal24;
    public Sitecore.Web.UI.HtmlControls.Space CancelSpace;
    public Sitecore.Web.UI.HtmlControls.Button Cancel;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public System.Web.UI.Control placeholder26;
    
    public string m_CancelButton;
    public string m_Application;
    public string m_FormTarget;
    public string m_Enctype;
    public string m_Submittable;
    public string m_Icon;
    public string m_Header;
    public string m_Text;
    public string m_ClientBackground;
    public string m_ClientBorder;
    public string m_OKButton;
    
    // properties
    public string CancelButton {
      get {
        return StringUtil.GetString(m_CancelButton);
      }
      set {
        m_CancelButton = value;
      }
    }
    
    public string Application {
      get {
        return StringUtil.GetString(m_Application);
      }
      set {
        m_Application = value;
        
        SetProperty(formpage4, "Application", Application);
      }
    }
    
    public string FormTarget {
      get {
        return StringUtil.GetString(m_FormTarget);
      }
      set {
        m_FormTarget = value;
        
        SetProperty(formpage4, "FormTarget", FormTarget);
      }
    }
    
    public string Enctype {
      get {
        return StringUtil.GetString(m_Enctype);
      }
      set {
        m_Enctype = value;
        
        SetProperty(formpage4, "Enctype", Enctype);
      }
    }
    
    public string Submittable {
      get {
        return StringUtil.GetString(m_Submittable);
      }
      set {
        m_Submittable = value;
        
        SetProperty(formpage4, "Submittable", MainUtil.GetString(Submittable,"true"));
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage8, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal11, "Text", Header);
      }
    }
    
    public string Text {
      get {
        return StringUtil.GetString(m_Text);
      }
      set {
        m_Text = value;
        
        SetProperty(literal13, "Text", Text);
      }
    }
    
    public string ClientBackground {
      get {
        return StringUtil.GetString(m_ClientBackground);
      }
      set {
        m_ClientBackground = value;
        
        SetProperty(scrollbox18, "Background", StringUtil.GetString(ClientBackground, "transparent"));
      }
    }
    
    public string ClientBorder {
      get {
        return StringUtil.GetString(m_ClientBorder);
      }
      set {
        m_ClientBorder = value;
        
        SetProperty(scrollbox18, "Border", StringUtil.GetString(ClientBorder, "none"));
      }
    }
    
    public string OKButton {
      get {
        return StringUtil.GetString(m_OKButton);
      }
      set {
        m_OKButton = value;
        
        SetProperty(literal24, "Text", StringUtil.GetString(OKButton, "OK"));
      }
    }
    
    // custom code
    
    
    protected override void OnLoad(EventArgs e) {
      if (CancelButton == "false") {
        CancelSpace.Visible = false;
        Cancel.Visible = false;
      }
    }
    
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      stylesheet1 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Dialogs.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "Key", "FormDialog") as Sitecore.Web.UI.HtmlControls.Script;
      text3 = AddLiteral("      scForm.browser.attachEvent(window, \"onload\", scLoad);      function scLoad() {      scForm.browser.attachEvent(document.forms[0], \"onsubmit\", scSubmit);      }      function scSubmit() {      var ctl = scForm.browser.getControl(\"OK\");      if (ctl != null) {      ctl.click();      }      return false;      }    ", "", script2, "");
      formpage4 = AddControl("FormPage", "", this, "", "Application", Application, "Background", "#e9e9e9", "Overflow", "true", "FormTarget", FormTarget, "Enctype", Enctype, "Submittable", MainUtil.GetString(Submittable,"true")) as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage4, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "Columns", "2", "Width", "100%", "Background", "white") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage8 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel7, "", "Src", Icon, "Width", "32", "Height", "32", "Margin", "4px 8px 4px 8px", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "Padding", "2px 16px 0px 0px", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Foreground", "black", "Padding", "0px 0px 4px 0px", "FontSize", "9pt", "FontBold", "true", "FontName", "tahoma") as Sitecore.Web.UI.HtmlControls.Border;
      literal11 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border10, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Foreground", "#333333") as Sitecore.Web.UI.HtmlControls.Border;
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border12, "", "Text", Text) as Sitecore.Web.UI.HtmlControls.Literal;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "Background", "#dbdbdb", "GridPanel.ColSpan", "2") as Sitecore.Web.UI.HtmlControls.Border;
      space15 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "") as Sitecore.Web.UI.HtmlControls.Space;
      placeholder16 = AddPlaceholder("Toolbar", gridpanel5, "", "name", "Toolbar");
      space17 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "Height", "4px", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      scrollbox18 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Height", "100%", "Background", StringUtil.GetString(ClientBackground, "transparent"), "Border", StringUtil.GetString(ClientBorder, "none"), "Padding", "0px", "GridPanel.VAlign", "top", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel19 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox18, "", "Width", "100%", "Height", "100%", "Class", "scDialogContentContainer") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder20 = AddPlaceholder("", gridpanel19, "", "GridPanel.VAlign", "top", "GridPanel.Height", "100%");
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Padding", "4px 16px 0px 16px", "GridPanel.Height", "32px") as Sitecore.Web.UI.HtmlControls.Border;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border21, "", "Float", "right", "Padding", "0px 0px 0px 8px", "NoWrap", "NoWrap", "Height", "34") as Sitecore.Web.UI.HtmlControls.Border;
      space23 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border22, "", "Width", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      OK = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border22, "", "ID", idref("OK"), "Width", "76px", "Height", "23px", "Click", "#", "KeyCode", "13") as Sitecore.Web.UI.HtmlControls.Button;
      literal24 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), OK, "", "Text", StringUtil.GetString(OKButton, "OK")) as Sitecore.Web.UI.HtmlControls.Literal;
      CancelSpace = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border22, "", "Width", "4") as Sitecore.Web.UI.HtmlControls.Space;
      Cancel = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border22, "", "ID", idref("Cancel"), "Width", "76px", "Height", "23px", "Click", "#", "KeyCode", "27") as Sitecore.Web.UI.HtmlControls.Button;
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Cancel, "", "Text", "Cancel") as Sitecore.Web.UI.HtmlControls.Literal;
      placeholder26 = AddPlaceholder("Buttons", border21, "", "name", "Buttons");
      
      CancelButton = "";
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(OK, "ID", idref("OK"));
      SetProperty(Cancel, "ID", idref("Cancel"));
    }
  }
}

