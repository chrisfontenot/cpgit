using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class RichText_InsertImage_a_141 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control script3;
    public System.Web.UI.Control text4;
    public System.Web.UI.Control script5;
    public System.Web.UI.Control text6;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside7;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox10;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl InsertMediaLeft;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Listview;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Edit Filename;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Button Upload;
    public Sitecore.Web.UI.HtmlControls.Button button16;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/photo_scenery.png", "Header", "Insert Media Item", "Text", "You can insert a media item here.", "OKButton", "Insert") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Src", "MediaBrowser.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script3 = AddControl("script", "", formdialog1, "", "Type", "text/javascript", "Language", "javascript", "Src", "/sitecore/shell/RadControls/Editor/Scripts/7_2_0/RadWindow.js");
      text4 = AddLiteral(".", "", script3, "");
      script5 = AddControl("script", "", formdialog1, "", "Type", "text/javascript", "Language", "javascript", "Src", "/sitecore/shell/Controls/Rich Text Editor/InsertImage/InsertImage.js");
      text6 = AddLiteral(".", "", script5, "");
      codebeside7 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Controls.RichTextEditor.InsertImage.InsertImageForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("DataContext"), "Root", "{3D6658D8-A0BF-4E75-B3E2-D050FABCF4E1}") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel8, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed", "Columns", "3", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox10 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Class", "scScrollbox scFixSize", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%", "GridPanel.Width", "50%", "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox10, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Click", "SelectTreeNode", "ShowRoot", "true", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      InsertMediaLeft = AddControl("VSplitter", "", gridpanel9, "", "ID", idref("InsertMediaLeft"), "GridPanel.Width", "4", "Target", "left") as Sitecore.Web.UI.XmlControls.XmlControl;
      Listview = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Class", "scScrollbox scFixSize", "ID", idref("Listview"), "Width", "100%", "Height", "100%", "Background", "white", "Padding", "0px", "GridPanel.Height", "100%", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "Padding", "4px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border11, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Padding", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", "Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filename = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel12, "", "ID", idref("Filename"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "Buttons") as Sitecore.Web.UI.HtmlControls.Border;
      Upload = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border15, "", "ID", idref("Upload"), "Header", "Upload", "Click", "media:upload(edit=1,load=1)") as Sitecore.Web.UI.HtmlControls.Button;
      button16 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border15, "", "Header", "Edit", "Click", "Edit") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(InsertMediaLeft, "ID", idref("InsertMediaLeft"));
      SetProperty(Listview, "ID", idref("Listview"));
      SetProperty(Filename, "ID", idref("Filename"));
      SetProperty(Upload, "ID", idref("Upload"));
    }
  }
}

