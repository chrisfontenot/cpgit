using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WindowChrome_a_78 : Sitecore.Shell.Controls.Windows.WindowChromeXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Space space6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public System.Web.UI.Control placeholder8;
    
    public string m_Icon;
    public string m_Header;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(Caption, "Icon", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(Caption, "Header", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      stylesheet1 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Windows.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreWindow.js") as Sitecore.Web.UI.HtmlControls.Script;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Height", "100%", "Class", "scWindowFrame") as Sitecore.Web.UI.HtmlControls.Border;
      WindowHandle = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border4, "", "Width", "100%", "Height", "100%", "Class", "scWindowHandle", "Fixed", "true", "onmousedown", "scWin.mouseDown(this, event)", "onmousemove", "scWin.mouseMove(this, event)", "onmouseup", "scWin.mouseUp(this, event)", "ondblclick", "scWin.maximizeWindow()", "onactivate", "scWin.activate(this, event)") as Sitecore.Web.UI.WebControls.GridPanel;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), WindowHandle, "", "Class", "scWindowFrameTop", "GridPanel.Height", "1") as Sitecore.Web.UI.HtmlControls.Border;
      space6 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border5, "") as Sitecore.Web.UI.HtmlControls.Space;
      Caption = AddControl("StandardWindowCaption", "", WindowHandle, "", "ID", idref("Caption"), "Icon", Icon, "Header", Header) as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), WindowHandle, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "Fixed", "true", "CellPadding", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder8 = AddPlaceholder("", gridpanel7, "", "GridPanel.Height", "100%", "GridPanel.Style", "padding-top:0px");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Caption, "ID", idref("Caption"));
    }
  }
}

