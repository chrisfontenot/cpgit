using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TaskOptions_a_102 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.XmlControls.XmlControl taskheadline2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control placeholder4;
    
    public string m_Header;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal3, "Text", StringUtil.GetString(Header, "Pick a task..."));
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Padding", "16px", "Background", "transparent") as Sitecore.Web.UI.HtmlControls.Border;
      taskheadline2 = AddControl("TaskHeadline", "", border1, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), taskheadline2, "", "Text", StringUtil.GetString(Header, "Pick a task...")) as Sitecore.Web.UI.HtmlControls.Literal;
      placeholder4 = AddPlaceholder("", border1, "");
      
      _Mode = "";
    }
  }
}

