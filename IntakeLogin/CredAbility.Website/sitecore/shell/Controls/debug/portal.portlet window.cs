using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class PortletWindow_a_123 : Sitecore.Shell.Controls.Portal.PortletWindowXmlControl   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.HtmlControls.Image image2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Image image6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public System.Web.UI.Control placeholder8;
    
    public string m_Icon;
    public string m_Header;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage4, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal5, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Class", "PortletWindow", "Columns", "3", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      image2 = AddControl(new Sitecore.Web.UI.HtmlControls.Image(), gridpanel1, "", "Src", "/sitecore/portal/tech/cap_l.gif", "Width", "5", "Height", "22", "GridPanel.Width", "5") as Sitecore.Web.UI.HtmlControls.Image;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "GridPanel.Class", "PortletCaption") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage4 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border3, "", "Src", Icon, "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border3, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      Menu = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), border3, "", "Src", "/sitecore/portal/tech/menu.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 0px 0px 8px") as Sitecore.Web.UI.HtmlControls.ImageButton;
      image6 = AddControl(new Sitecore.Web.UI.HtmlControls.Image(), gridpanel1, "", "Src", "/sitecore/portal/tech/cap_r.gif", "Width", "25", "Height", "22", "GridPanel.Width", "25") as Sitecore.Web.UI.HtmlControls.Image;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "GridPanel.ColSpan", "3", "GridPanel.Class", "PortletBody") as Sitecore.Web.UI.HtmlControls.Border;
      placeholder8 = AddPlaceholder("", border7, "");
      
      _Mode = "";
    }
  }
}

