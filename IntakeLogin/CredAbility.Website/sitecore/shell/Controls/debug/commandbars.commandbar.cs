using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Commandbar_a_118 : Sitecore.Shell.Controls.Commandbars.CommandbarXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Details;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox7;
    public System.Web.UI.Control placeholder8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Space space10;
    
    public string m_Background;
    public string m_Glyph;
    public string m_Icon;
    public string m_Title;
    public string m_Description;
    
    // properties
    public new string Background {
      get {
        return StringUtil.GetString(m_Background);
      }
      set {
        m_Background = value;
        
        SetProperty(Toolbar, "Class", StringUtil.GetString(Background, "scGreenGradient"));
      }
    }
    
    public string Glyph {
      get {
        return StringUtil.GetString(m_Glyph);
      }
      set {
        m_Glyph = value;
        
        SetProperty(ToolbarIcon, "Src", StringUtil.GetString(Glyph, Icon));
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(ToolbarIcon, "Src", StringUtil.GetString(Glyph, Icon));
        SetProperty(CommandbarIcon, "Src", Icon);
      }
    }
    
    public string Title {
      get {
        return StringUtil.GetString(m_Title);
      }
      set {
        m_Title = value;
        
        SetProperty(CommandbarTitle, "Text", Title);
      }
    }
    
    public string Description {
      get {
        return StringUtil.GetString(m_Description);
      }
      set {
        m_Description = value;
        
        SetProperty(CommandbarDescription, "Text", Description);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Toolbar = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", StringUtil.GetString(Background, "scGreenGradient"), "Height", "120", "Style", "overflow:hidden") as Sitecore.Web.UI.HtmlControls.Border;
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Toolbar, "", "Width", "196", "Height", "196", "Style", "position:absolute; z-index:-1; overflow:hidden; filter:progid:DXImageTransform.Microsoft.BasicImage(opacity=0.20,grayscale=1)") as Sitecore.Web.UI.HtmlControls.Border;
      ToolbarIcon = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border1, "", "Src", StringUtil.GetString(Glyph, Icon), "Width", "196", "Height", "196", "Style", "position:absolute;z-index:-1;-moz-opacity:0.2; clip: rect(0px 196px 120px 0px)") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Toolbar, "", "Columns", "2", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      Details = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "Width", "100%", "Height", "100%", "Border", "none", "Foreground", "white", "Background", "transparent", "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Details, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      PreviewPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "GridPanel.Width", "56", "GridPanel.Valign", "top", "Padding", "0px 8px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      CommandbarIcon = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), PreviewPane, "", "Src", Icon, "Height", "48", "Width", "48") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Foreground", "white") as Sitecore.Web.UI.HtmlControls.Border;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "", "Class", "scLargeFont") as Sitecore.Web.UI.HtmlControls.Border;
      CommandbarTitle = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border5, "", "Text", Title) as Sitecore.Web.UI.HtmlControls.Literal;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "") as Sitecore.Web.UI.HtmlControls.Border;
      CommandbarDescription = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "Text", Description) as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox7 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "Width", "100%", "Height", "100%", "Border", "none", "Background", "transparent", "Padding", "8px 12px 0px 12px", "GridPanel.Width", "65%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Tasks = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox7, "", "Foreground", "white", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder8 = AddPlaceholder("", scrollbox7, "");
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Background", "#212424", "Style", "border-left:2px solid #4E4E4E; border-right:2px solid #4E4E4E") as Sitecore.Web.UI.HtmlControls.Border;
      space10 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border9, "") as Sitecore.Web.UI.HtmlControls.Space;
      
      _Mode = "";
    }
  }
}

