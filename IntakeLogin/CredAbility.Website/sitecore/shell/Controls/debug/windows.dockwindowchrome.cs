using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class DockWindowChrome_a_110 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.WebControls.GridPanel WindowHandle;
    public Sitecore.Web.UI.WebControls.GridPanel WindowFrameTop;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.Literal Caption;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.XmlControls.XmlControl CloseButton;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Space space6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public System.Web.UI.Control placeholder8;
    public Sitecore.Web.UI.HtmlControls.Space space9;
    public Sitecore.Web.UI.HtmlControls.Space space10;
    
    public string m_Header;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(Caption, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreWindow.js") as Sitecore.Web.UI.HtmlControls.Script;
      WindowHandle = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Height", "100%", "Class", "scWindowHandle", "Fixed", "true", "Background", "threedface", "onmousedown", "javascript:scWin.mouseDown(this, event)", "onmousemove", "javascript:scWin.mouseMove(this, event)", "onmouseup", "javascript:scWin.mouseUp(this, event)", "onactivate", "javascript:scWin.activate(this, event)") as Sitecore.Web.UI.WebControls.GridPanel;
      WindowFrameTop = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), WindowHandle, "", "ID", idref("WindowFrameTop"), "Foreground", "captiontext", "Height", "22", "Width", "100%", "Columns", "2", "Style", "table-layout:fixed", "Background", "activecaption url(/sitecore/shell/themes/standard/images/toolwindow.gif) repeat-x") as Sitecore.Web.UI.WebControls.GridPanel;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), WindowFrameTop, "", "GridPanel.Height", "16", "Style", "font:caption", "Cursor", "default", "Padding", "2px 0px 1px 4px", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      Caption = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border3, "", "ID", idref("Caption"), "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), WindowFrameTop, "", "Align", "right", "Padding", "3px 2px 0px 0px", "GridPanel.Width", "20") as Sitecore.Web.UI.HtmlControls.Border;
      CloseButton = AddControl("WindowButton", "", border4, "", "Src", "Images/CloseWindow16x16.png", "Click", "javascript:scWin.hideWindow()", "Width", "16", "Height", "16", "Margin", "1 1", "ToolTip", "Close") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), WindowHandle, "", "Height", "100%", "Width", "100%", "Columns", "3", "Style", "table-layout:fixed", "GridPanel.Width", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space6 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "GridPanel.ID", "WindowFrameLeft", "Width", "3", "GridPanel.Style", "background:activecaption", "GridPanel.Width", "3") as Sitecore.Web.UI.HtmlControls.Space;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Height", "100%", "GridPanel.Height", "100%", "onmousedown", "javascript:scForm.browser.clearEvent(event, true, false)", "onmousemove", "javascript:scForm.browser.clearEvent(event, true, false)", "onmouseup", "javascript:scForm.browser.clearEvent(event, true, false)", "onactivate", "javascript:scForm.browser.clearEvent(event, true, false)") as Sitecore.Web.UI.HtmlControls.Border;
      placeholder8 = AddPlaceholder("", border7, "");
      space9 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "GridPanel.ID", "WindowFrameRight", "Width", "3", "GridPanel.Style", "background:activecaption", "GridPanel.Width", "3") as Sitecore.Web.UI.HtmlControls.Space;
      space10 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "GridPanel.ID", "WindowFrameBottom", "Height", "3", "GridPanel.ColSpan", "3", "GridPanel.Style", "background:activecaption", "GridPanel.Height", "3") as Sitecore.Web.UI.HtmlControls.Space;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(WindowFrameTop, "ID", idref("WindowFrameTop"));
      SetProperty(Caption, "ID", idref("Caption"));
    }
  }
}

