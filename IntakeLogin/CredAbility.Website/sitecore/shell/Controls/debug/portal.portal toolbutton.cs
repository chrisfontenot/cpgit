using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class PortalToolbutton_a_119 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.Image image2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    
    public string m_Click;
    public string m_ToolTip;
    public string m_Header;
    
    // properties
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(border1, "Click", Click);
      }
    }
    
    public new string ToolTip {
      get {
        return StringUtil.GetString(m_ToolTip);
      }
      set {
        m_ToolTip = value;
        
        SetProperty(border1, "ToolTip", ToolTip);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal3, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", "PortalToolbutton", "Click", Click, "RollOver", "true", "ToolTip", ToolTip) as Sitecore.Web.UI.HtmlControls.Border;
      image2 = AddControl(new Sitecore.Web.UI.HtmlControls.Image(), border1, "", "Src", "/sitecore/portal/tech/go_s.gif", "Width", "14", "Height", "9", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.Image;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border1, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

