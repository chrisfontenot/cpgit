using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Pane_a_81 : Sitecore.Shell.Controls.Panes.PaneXmlControl   {
    
    // variables
    public System.Web.UI.Control table1;
    public Sitecore.Web.UI.HtmlControls.RenderLiteral renderliteral2;
    public System.Web.UI.Control tr3;
    public System.Web.UI.Control td4;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage5;
    public System.Web.UI.Control td6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Space space8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Space space10;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public System.Web.UI.Control td16;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage17;
    public Sitecore.Web.UI.HtmlControls.RenderLiteral renderliteral18;
    public System.Web.UI.Control tr19;
    public System.Web.UI.Control td20;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Space space22;
    public System.Web.UI.Control td23;
    public System.Web.UI.Control td24;
    public System.Web.UI.Control placeholder25;
    public Sitecore.Web.UI.HtmlControls.RenderLiteral renderliteral26;
    public System.Web.UI.Control tr27;
    public System.Web.UI.Control td28;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage29;
    public System.Web.UI.Control td30;
    public Sitecore.Web.UI.HtmlControls.Space space31;
    public System.Web.UI.Control td32;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage33;
    
    public string m_PaneID;
    public string m_Height;
    public string m_Icon;
    public string m_Header;
    public string m_BodyPadding;
    public string m_BackgroundColor;
    public string m_BackgroundClass;
    
    // properties
    public string PaneID {
      get {
        return StringUtil.GetString(m_PaneID);
      }
      set {
        m_PaneID = value;
        
        SetProperty(Control, "ID", PaneID);
      }
    }
    
    public new string Height {
      get {
        return StringUtil.GetString(m_Height);
      }
      set {
        m_Height = value;
        
        SetProperty(Control, "Height", Height);
        SetProperty(table1, "style", "height:" + Height + ";table-layout:fixed; margin:0px 0px 24px 0px; background:white");
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage11, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal14, "Text", Header);
      }
    }
    
    public string BodyPadding {
      get {
        return StringUtil.GetString(m_BodyPadding);
      }
      set {
        m_BodyPadding = value;
        
        SetProperty(td24, "style", "padding:" + StringUtil.GetString(BodyPadding, "20px 12px 0px 12px"));
      }
    }
    
    public string BackgroundColor {
      get {
        return StringUtil.GetString(m_BackgroundColor);
      }
      set {
        m_BackgroundColor = value;
        
        SetProperty(td28, "style", "background:" + StringUtil.GetString(BackgroundColor, "transparent"));
        SetProperty(td32, "style", "background:" + StringUtil.GetString(BackgroundColor, "transparent"));
      }
    }
    
    public string BackgroundClass {
      get {
        return StringUtil.GetString(m_BackgroundClass);
      }
      set {
        m_BackgroundClass = value;
        
        SetProperty(td30, "class", BackgroundClass);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Control = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "ID", PaneID, "Height", Height) as Sitecore.Web.UI.HtmlControls.Border;
      table1 = AddControl("table", "", Control, "", "border", "0", "cellpadding", "0", "cellspacing", "0", "width", "100%", "style", "height:" + Height + ";table-layout:fixed; margin:0px 0px 24px 0px; background:white");
      renderliteral2 = AddControl(new Sitecore.Web.UI.HtmlControls.RenderLiteral(), table1, "") as Sitecore.Web.UI.HtmlControls.RenderLiteral;
      tr3 = AddControl("tr", "", renderliteral2, "");
      td4 = AddControl("td", "", tr3, "", "width", "8", "style", "background:#4E4E4E", "valign", "top");
      themedimage5 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), td4, "", "Src", "images/contentsectioncorner1.gif", "Width", "8", "Height", "8") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      td6 = AddControl("td", "", tr3, "", "width", "100%", "height", "26", "class", "scGrayGradient");
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), td6, "", "Background", "black") as Sitecore.Web.UI.HtmlControls.Border;
      space8 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border7, "") as Sitecore.Web.UI.HtmlControls.Space;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), td6, "", "Class", "scGrayGradientLightShadow") as Sitecore.Web.UI.HtmlControls.Border;
      space10 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border9, "") as Sitecore.Web.UI.HtmlControls.Space;
      Caption = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), td6, "") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage11 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), Caption, "", "Src", Icon, "Width", "48", "Height", "48", "Style", "position:absolute;margin:-8px 0px 0px 0px", "GridPanel.Height", "24") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Caption, "", "Columns", "2", "Width", "100%", "Height", "24") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "FontSize", "10pt", "FontName", "verdana", "FontBold", "true", "Padding", "4px 16px 0px 60px", "Foreground", "white", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.Align", "right", "GridPanel.VAlign", "top", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      CollapseButton = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), border15, "", "Src", "images/contentsectionbutton2.png", "Width", "16", "Height", "16", "Alt", "Expand/Collapse") as Sitecore.Web.UI.HtmlControls.ImageButton;
      CloseButton = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), border15, "", "Src", "images/contentsectionbutton3.png", "Width", "16", "Height", "16", "Margin", "4px 0px 0px 0px", "Alt", "Close") as Sitecore.Web.UI.HtmlControls.ImageButton;
      td16 = AddControl("td", "", tr3, "", "width", "8", "style", "background:#4E4E4E", "valign", "top");
      themedimage17 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), td16, "", "Src", "images/contentsectioncorner2.gif", "Width", "8", "Height", "8") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      renderliteral18 = AddControl(new Sitecore.Web.UI.HtmlControls.RenderLiteral(), table1, "") as Sitecore.Web.UI.HtmlControls.RenderLiteral;
      tr19 = AddControl("tr", "", renderliteral18, "");
      td20 = AddControl("td", "", tr19, "", "colspan", "3");
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), td20, "", "Background", "#212424", "Style", "border-left:2px solid #4E4E4E; border-right:2px solid #4E4E4E") as Sitecore.Web.UI.HtmlControls.Border;
      space22 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border21, "") as Sitecore.Web.UI.HtmlControls.Space;
      PaneContent = AddControl("tbody", "", table1, "");
      Toolbar = AddControl("tr", "", PaneContent, "");
      td23 = AddControl("td", "", Toolbar, "", "colspan", "3");
      ToolbarPlaceholder = AddPlaceholder("Toolbar", td23, "", "name", "Toolbar");
      Body = AddControl("tr", "", PaneContent, "");
      td24 = AddControl("td", "", Body, "", "colspan", "3", "style", "padding:" + StringUtil.GetString(BodyPadding, "20px 12px 0px 12px"), "height", "100%", "valign", "top");
      placeholder25 = AddPlaceholder("", td24, "");
      renderliteral26 = AddControl(new Sitecore.Web.UI.HtmlControls.RenderLiteral(), table1, "") as Sitecore.Web.UI.HtmlControls.RenderLiteral;
      tr27 = AddControl("tr", "", renderliteral26, "");
      td28 = AddControl("td", "", tr27, "", "style", "background:" + StringUtil.GetString(BackgroundColor, "transparent"));
      themedimage29 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), td28, "", "Src", "images/contentsectioncorner3.gif", "Width", "8", "Height", "8") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      td30 = AddControl("td", "", tr27, "", "class", BackgroundClass);
      space31 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), td30, "", "Width", "1", "Height", "1") as Sitecore.Web.UI.HtmlControls.Space;
      td32 = AddControl("td", "", tr27, "", "style", "background:" + StringUtil.GetString(BackgroundColor, "transparent"));
      themedimage33 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), td32, "", "Src", "images/contentsectioncorner4.gif", "Width", "8", "Height", "8") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      
      _Mode = "";
    }
  }
}

