using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class ListviewViewsMenuItems_a_98 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem1;
    
    public string m_Listview;
    
    // properties
    public string Listview {
      get {
        return StringUtil.GetString(m_Listview);
      }
      set {
        m_Listview = value;
        
        SetProperty(menuitem1, "Click", Listview + ".GetViewsMenu(\"right\")");
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      menuitem1 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), this, "", "Header", "View", "Type", "Submenu", "Click", Listview + ".GetViewsMenu(\"right\")") as Sitecore.Web.UI.HtmlControls.MenuItem;
      
      _Mode = "";
    }
  }
}

