using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class StandardWindowCaption_a_79 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.Inline WindowCaption;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.XmlControls.XmlControl standardwindowbuttons5;
    
    public string m_Icon;
    public string m_Header;
    public string m_Maximize;
    public string m_Minimize;
    public string m_Close;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage2, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal4, "Text", Header);
      }
    }
    
    public string Maximize {
      get {
        return StringUtil.GetString(m_Maximize);
      }
      set {
        m_Maximize = value;
        
        SetProperty(standardwindowbuttons5, "Maximize", Maximize);
      }
    }
    
    public string Minimize {
      get {
        return StringUtil.GetString(m_Minimize);
      }
      set {
        m_Minimize = value;
        
        SetProperty(standardwindowbuttons5, "Minimize", Minimize);
      }
    }
    
    public string Close {
      get {
        return StringUtil.GetString(m_Close);
      }
      set {
        m_Close = value;
        
        SetProperty(standardwindowbuttons5, "Close", Close);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Columns", "3", "Width", "100%", "Height", "20") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage2 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel1, "", "Src", Icon, "Width", "16", "Height", "16", "Margin", "0px 4px 0px 4px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "Class", "scWindowCaption", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      WindowCaption = AddControl(new Sitecore.Web.UI.HtmlControls.Inline(), border3, "", "ID", idref("WindowCaption")) as Sitecore.Web.UI.HtmlControls.Inline;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border3, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      standardwindowbuttons5 = AddControl("StandardWindowButtons", "", gridpanel1, "", "Maximize", Maximize, "Minimize", Minimize, "Close", Close, "GridPanel.NoWrap", "true", "GridPanel.Valign", "top") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(WindowCaption, "ID", idref("WindowCaption"));
    }
  }
}

