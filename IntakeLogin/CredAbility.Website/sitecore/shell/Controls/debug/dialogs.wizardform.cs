using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WizardForm_a_90 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet1;
    public Sitecore.Web.UI.XmlControls.XmlControl formpage2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public System.Web.UI.Control placeholder6;
    public Sitecore.Web.UI.HtmlControls.Space space7;
    public Sitecore.Web.UI.HtmlControls.Space space8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Button BackButton;
    public Sitecore.Web.UI.HtmlControls.Button NextButton;
    public System.Web.UI.Control text10;
    public Sitecore.Web.UI.HtmlControls.Button CancelButton;
    public System.Web.UI.Control text11;
    
    public string m_Application;
    public string m_FormTarget;
    public string m_Enctype;
    public string m_Submittable;
    public string m_CodeBeside;
    
    // properties
    public string Application {
      get {
        return StringUtil.GetString(m_Application);
      }
      set {
        m_Application = value;
        
        SetProperty(formpage2, "Application", Application);
      }
    }
    
    public string FormTarget {
      get {
        return StringUtil.GetString(m_FormTarget);
      }
      set {
        m_FormTarget = value;
        
        SetProperty(formpage2, "FormTarget", FormTarget);
      }
    }
    
    public string Enctype {
      get {
        return StringUtil.GetString(m_Enctype);
      }
      set {
        m_Enctype = value;
        
        SetProperty(formpage2, "Enctype", Enctype);
      }
    }
    
    public string Submittable {
      get {
        return StringUtil.GetString(m_Submittable);
      }
      set {
        m_Submittable = value;
        
        SetProperty(formpage2, "Submittable", MainUtil.GetString(Submittable,"true"));
      }
    }
    
    public string CodeBeside {
      get {
        return StringUtil.GetString(m_CodeBeside);
      }
      set {
        m_CodeBeside = value;
        
        SetProperty(codebeside3, "Type", CodeBeside);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      stylesheet1 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Dialogs.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      formpage2 = AddControl("FormPage", "", this, "", "Application", Application, "FormTarget", FormTarget, "Enctype", Enctype, "Submittable", MainUtil.GetString(Submittable,"true"), "Overflow", "true") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage2, "", "Type", CodeBeside) as Sitecore.Web.UI.HtmlControls.CodeBeside;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage2, "", "Type", "text/JavaScript", "Language", "javascript", "Src", "/sitecore/shell/controls/SitecoreWizard.js") as Sitecore.Web.UI.HtmlControls.Script;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage2, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder6 = AddPlaceholder("", gridpanel5, "", "GridPanel.Height", "100%", "GridPanel.Style", "background:#e9e9e9", "GridPanel.VAlign", "top");
      space7 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "GridPanel.Class", "scBottomEdge") as Sitecore.Web.UI.HtmlControls.Space;
      space8 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "GridPanel.Class", "scTopEdge") as Sitecore.Web.UI.HtmlControls.Space;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Height", "25", "Align", "right", "GridPanel.Height", "36", "GridPanel.Style", "background:#e9e9e9; white-space:nowrap") as Sitecore.Web.UI.HtmlControls.Border;
      BackButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border9, "", "ID", idref("BackButton"), "Class", "scButton", "Disabled", "true", "Click", "#", "Header", "< Back") as Sitecore.Web.UI.HtmlControls.Button;
      NextButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border9, "", "ID", idref("NextButton"), "Class", "scButton", "Click", "#", "Header", "Next >") as Sitecore.Web.UI.HtmlControls.Button;
      text10 = AddLiteral("             ", "", border9, "");
      CancelButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border9, "", "ID", idref("CancelButton"), "Class", "scButton", "Click", "#", "Header", "Cancel") as Sitecore.Web.UI.HtmlControls.Button;
      text11 = AddLiteral("           ", "", border9, "");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(BackButton, "ID", idref("BackButton"));
      SetProperty(NextButton, "ID", idref("NextButton"));
      SetProperty(CancelButton, "ID", idref("CancelButton"));
    }
  }
}

