using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class BorderWindowChrome_a_184 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.WebControls.GridPanel WindowHandle;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Space space8;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage9;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel10;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage14;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel17;
    public Sitecore.Web.UI.HtmlControls.Space space18;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel19;
    public Sitecore.Web.UI.HtmlControls.Space space20;
    public System.Web.UI.Control placeholder21;
    public Sitecore.Web.UI.HtmlControls.Space space22;
    public Sitecore.Web.UI.HtmlControls.Space space23;
    public Sitecore.Web.UI.HtmlControls.Space space24;
    public Sitecore.Web.UI.HtmlControls.Space space25;
    public Sitecore.Web.UI.HtmlControls.Space space26;
    public Sitecore.Web.UI.HtmlControls.Space space27;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreWindow.js") as Sitecore.Web.UI.HtmlControls.Script;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Height", "100%", "Class", "scGrayGradient") as Sitecore.Web.UI.HtmlControls.Border;
      WindowHandle = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border3, "", "Width", "100%", "Height", "100%", "Class", "scWindowHandle", "Fixed", "true", "onmousedown", "scWin.mouseDown(this, event)", "onmousemove", "scWin.mouseMove(this, event)", "onmouseup", "scWin.mouseUp(this, event)", "ondblclick", "scWin.maximizeWindow()", "onactivate", "scWin.activate(this, event)") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), WindowHandle, "", "Height", "4", "Width", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel4, "", "Columns", "3", "Width", "100%", "Height", "2", "GridPanel.Height", "2", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel5, "", "Src", "Images/windowtopleft14x2.png", "Width", "4", "Height", "2", "GridPanel.Width", "4") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Height", UIUtil.IsIE() ? "2" : "1", "Class", "scGrayGradientLightShadow", "Style", "border-top:1px solid #232323", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      space8 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border7, "") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage9 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel5, "", "Src", "Images/windowtopright14x2.png", "Width", "4", "Height", "2", "GridPanel.Width", "4") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      gridpanel10 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel4, "", "Columns", "3", "Width", "100%", "Height", "2", "GridPanel.Height", "2", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage11 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel10, "", "Src", "Images/windowtopleft22x2.png", "Width", "2", "Height", "2", "GridPanel.Width", "2") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel10, "", "Height", "2") as Sitecore.Web.UI.HtmlControls.Border;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border12, "", "Height", "2") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage14 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel10, "", "Src", "Images/windowtopright22x2.png", "Width", "2", "Height", "2", "GridPanel.Width", "2") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      gridpanel15 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), WindowHandle, "", "Width", "100%", "Height", "100%", "Columns", "3", "GridPanel.Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "GridPanel.Style", "background:#232323", "GridPanel.Width", "1") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel17 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel15, "", "Height", "100%", "Width", "100%", "Columns", "3", "Style", "table-layout:fixed", "GridPanel.Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space18 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel17, "", "GridPanel.Style", "background:#848484", "GridPanel.Width", "1") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel19 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel17, "", "Height", "100%", "Width", "100%", "Columns", "3", "Style", "table-layout:fixed", "GridPanel.Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space20 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel19, "", "Width", "2", "GridPanel.Style", "background:#4E4E4E", "GridPanel.Width", "2") as Sitecore.Web.UI.HtmlControls.Space;
      placeholder21 = AddPlaceholder("", gridpanel19, "", "GridPanel.Height", "100%");
      space22 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel19, "", "Width", "2", "GridPanel.Style", "background:#4E4E4E", "GridPanel.Width", "2") as Sitecore.Web.UI.HtmlControls.Space;
      space23 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel19, "", "Height", "2", "GridPanel.ColSpan", "3", "GridPanel.Style", "background:#4E4E4E", "GridPanel.Height", "2") as Sitecore.Web.UI.HtmlControls.Space;
      space24 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel17, "", "GridPanel.Style", "background:#848484", "GridPanel.Width", "1") as Sitecore.Web.UI.HtmlControls.Space;
      space25 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel17, "", "GridPanel.ColSpan", "3", "GridPanel.Style", "background:#848484", "GridPanel.Height", "1") as Sitecore.Web.UI.HtmlControls.Space;
      space26 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "GridPanel.Style", "background:#232323", "GridPanel.Width", "1") as Sitecore.Web.UI.HtmlControls.Space;
      space27 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "GridPanel.ColSpan", "3", "GridPanel.Style", "background:#232323", "GridPanel.Height", "1") as Sitecore.Web.UI.HtmlControls.Space;
      
      _Mode = "";
    }
  }
}

