using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SidebarTileHeader_a_132 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    
    public string m_Icon;
    public string m_Header;
    
    // properties
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage2, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal3, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Padding", "0px 0px 4px 0px", "Class", "scRollOver") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage2 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border1, "", "Src", Icon, "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 0px 0px 4px", "Float", "right") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border1, "", "Class", "scClickFont", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

