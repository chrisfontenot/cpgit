using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WebPanel_a_52 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Literal literal1;
    public System.Web.UI.Control html2;
    public System.Web.UI.Control htmlhead3;
    public System.Web.UI.Control htmltitle4;
    public System.Web.UI.Control text5;
    public System.Web.UI.Control meta6;
    public System.Web.UI.Control meta7;
    public System.Web.UI.Control meta8;
    public System.Web.UI.Control placeholder9;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      literal1 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), this, "", "Text", Sitecore.Context.Device.Capabilities.GetDefaultDocumentType()) as Sitecore.Web.UI.HtmlControls.Literal;
      html2 = AddControl("html", "", this, "");
      htmlhead3 = AddControl("html:HtmlHead", "", html2, "");
      htmltitle4 = AddControl("html:HtmlTitle", "", htmlhead3, "");
      text5 = AddLiteral("Sitecore", "", htmltitle4, "");
      meta6 = AddControl("meta", "", htmlhead3, "", "name", "GENERATOR", "content", "Sitecore");
      meta7 = AddControl("meta", "", htmlhead3, "", "http-equiv", "imagetoolbar", "content", "no");
      meta8 = AddControl("meta", "", htmlhead3, "", "http-equiv", "imagetoolbar", "content", "false");
      placeholder9 = AddPlaceholder("", html2, "");
      
      _Mode = "";
    }
  }
}

