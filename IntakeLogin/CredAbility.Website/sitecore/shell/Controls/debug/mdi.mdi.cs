using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class MDI_a_108 : Sitecore.Shell.Controls.MDI.MDIXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public System.Web.UI.Control text2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Space space5;
    public Sitecore.Web.UI.HtmlControls.Scroller MDIScroller;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "Type", "text/javascript") as Sitecore.Web.UI.HtmlControls.Script;
      text2 = AddLiteral("      function resizeFrames() {        $$(\"#MDIFrames iframe\").each(function(frame) {           if (frame.style.display != 'none') {            frame.style.display = 'none';            frame.scWasActive = true;          }        });              var width = $(\"MDIFrames\").getWidth();                $$(\"#MDIFrames iframe\").each(function(frame) {          frame.width = width;          if (frame.scWasActive) {            frame.show();            frame.scWasActive = false;          }        });      }            window.onresize = resizeFrames;    ", "", script1, "");
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "MDI.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      MDIWorkspace = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "ID", idref("MDIWorkspace"), "Height", "100%", "Style", "display:none") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), MDIWorkspace, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      TopSeparator = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "ID", idref("TopSeparator"), "Background", "threedlightshadow") as Sitecore.Web.UI.HtmlControls.Border;
      space5 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), TopSeparator, "", "Height", "3") as Sitecore.Web.UI.HtmlControls.Space;
      Header = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel4, "", "ID", idref("Header"), "Width", "100%", "Columns", "3", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      MDIScroller = AddControl(new Sitecore.Web.UI.HtmlControls.Scroller(), Header, "", "ID", idref("MDIScroller"), "GridPanel.Width", "100%", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scroller;
      MDITabs = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), MDIScroller, "", "ID", idref("MDITabs"), "Background", "threedlightshadow") as Sitecore.Web.UI.HtmlControls.Border;
      ListButton = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), Header, "", "Src", "Images/down.png", "Width", "16", "Height", "16", "GridPanel.Style", "background: threedlightshadow url(/sitecore/shell/themes/standard/images/tab_filler.png) repeat-x top", "GridPanel.Width", "16") as Sitecore.Web.UI.HtmlControls.ImageButton;
      CloseButton = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), Header, "", "Src", "Images/close.png", "Width", "16", "Height", "16", "GridPanel.Style", "background: threedlightshadow url(/sitecore/shell/themes/standard/images/tab_filler.png) repeat-x top", "GridPanel.Width", "16") as Sitecore.Web.UI.HtmlControls.ImageButton;
      MDIFrames = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "ID", idref("MDIFrames"), "Height", "100%", "GridPanel.Height", "100%", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(MDIWorkspace, "ID", idref("MDIWorkspace"));
      SetProperty(TopSeparator, "ID", idref("TopSeparator"));
      SetProperty(Header, "ID", idref("Header"));
      SetProperty(MDIScroller, "ID", idref("MDIScroller"));
      SetProperty(MDITabs, "ID", idref("MDITabs"));
      SetProperty(MDIFrames, "ID", idref("MDIFrames"));
    }
  }
}

