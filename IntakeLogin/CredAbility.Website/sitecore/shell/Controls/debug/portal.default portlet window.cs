using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class DefaultPortletWindow_a_117 : Sitecore.Shell.Controls.Portal.DefaultPortletWindowXmlControl   {
    
    // variables
    public System.Web.UI.Control placeholder1;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      placeholder1 = AddPlaceholder("", this, "");
      
      _Mode = "";
    }
  }
}

