using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SidebarPortal_a_127 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.Portal.Portal portal3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.Portal.PortalZone DefaultZone;
    public System.Web.UI.Control placeholder5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.Portal.PortalZone Footer;
    
    public string m_PortalID;
    public string m_PortalDataSource;
    
    // properties
    public string PortalID {
      get {
        return StringUtil.GetString(m_PortalID);
      }
      set {
        m_PortalID = value;
        
        SetProperty(portal3, "ID", PortalID);
      }
    }
    
    public string PortalDataSource {
      get {
        return StringUtil.GetString(m_PortalDataSource);
      }
      set {
        m_PortalDataSource = value;
        
        SetProperty(portal3, "DataSource", PortalDataSource);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      scrollbox1 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), this, "", "Background", "transparent", "Height", "100%", "Border", "none", "Padding", "0px", "Style", "overflow:expression(document.body.offsetWidth > 32 ? 'auto' : 'hidden')") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox1, "", "Height", "100%", "Width", "100%", "Class", "scSidebar") as Sitecore.Web.UI.WebControls.GridPanel;
      portal3 = AddControl(new Sitecore.Web.UI.Portal.Portal(), gridpanel2, "", "ID", PortalID, "Class", "Portal", "DataSource", PortalDataSource, "GridPanel.VAlign", "top", "GridPanel.Height", "100%", "Background", "transparent") as Sitecore.Web.UI.Portal.Portal;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), portal3, "", "Foreground", "white", "Height", "100%", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      DefaultZone = AddControl(new Sitecore.Web.UI.Portal.PortalZone(), gridpanel4, "", "ID", idref("DefaultZone"), "GridPanel.VAlign", "top", "GridPanel.Height", "100%") as Sitecore.Web.UI.Portal.PortalZone;
      placeholder5 = AddPlaceholder("", DefaultZone, "");
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "GridPanel.VAlign", "bottom") as Sitecore.Web.UI.HtmlControls.Border;
      Footer = AddControl(new Sitecore.Web.UI.Portal.PortalZone(), border6, "", "ID", idref("Footer")) as Sitecore.Web.UI.Portal.PortalZone;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DefaultZone, "ID", idref("DefaultZone"));
      SetProperty(Footer, "ID", idref("Footer"));
    }
  }
}

