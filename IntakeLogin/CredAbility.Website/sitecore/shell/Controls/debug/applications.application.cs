using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Application_a_77 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl FormPage;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.Border Body;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      FormPage = AddControl("FormPage", "", this, "", "ID", idref("FormPage"), "Background", "#999999") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), FormPage, "", "Type", "Sitecore.Shell.Controls.Applications.ApplicationForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      Body = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FormPage, "", "ID", idref("Body"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FormPage, "ID", idref("FormPage"));
      SetProperty(Body, "ID", idref("Body"));
    }
  }
}

