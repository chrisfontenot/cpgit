using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SidebarTile_a_130 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public System.Web.UI.Control placeholder2;
    public Sitecore.Web.UI.XmlControls.XmlControl sidebardivider3;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", "scSidebarTile") as Sitecore.Web.UI.HtmlControls.Border;
      placeholder2 = AddPlaceholder("", border1, "");
      sidebardivider3 = AddControl("SidebarDivider", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

