using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SidebarPortletWindow_a_129 : Sitecore.Shell.Controls.Portal.PortletWindowXmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl sidebartile1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.XmlControls.XmlControl sidebartileheader3;
    public System.Web.UI.Control placeholder4;
    
    public string m_Header;
    public string m_Icon;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(sidebartileheader3, "Header", Header);
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(sidebartileheader3, "Icon", Icon);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      sidebartile1 = AddControl("SidebarTile", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Menu = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), sidebartile1, "", "Src", "/sitecore/portal/tech/menu.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 0px 0px 8px", "Visible", "false") as Sitecore.Web.UI.HtmlControls.ImageButton;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), sidebartile1, "", "Foreground", "white") as Sitecore.Web.UI.HtmlControls.Border;
      sidebartileheader3 = AddControl("SidebarTileHeader", "", border2, "", "Header", Header, "Icon", Icon) as Sitecore.Web.UI.XmlControls.XmlControl;
      placeholder4 = AddPlaceholder("", border2, "");
      
      _Mode = "";
    }
  }
}

