scForm.browser.attachEvent(window, "onbeforeunload", scBeforeUnload);

function scBeforeUnload() {
  if (scForm.modified) {
    window.event.returnValue = "There are unsaved changes.";
  }
}
