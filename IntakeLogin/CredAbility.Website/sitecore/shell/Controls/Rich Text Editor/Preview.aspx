<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview.aspx.cs" Inherits="Sitecore.Shell.Controls.RADEditor.Preview" %>
<%@ Import namespace="Sitecore"%>
<%@ Import namespace="Sitecore.Web"%>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sitecore</title>
    <asp:PlaceHolder id="Stylesheets" runat="server" />
    <style>
      html { <% if (UIUtil.IsIE()) { %> height:100%; <% } %> width:100% }
      body { height:100%; width:100%; padding: 0; margin: 0; position: relative; }
      form { height:100%; width:100% }
      #ContentWrapper {margin-left: 4px;}
    </style>

    <script type="text/javascript" language="javascript">
    
      var scModified = false;
      var scDisabled = "<asp:PlaceHolder id="DisabledFlag" runat="server" />";
      var scValue = <asp:PlaceHolder id="InitialValue" runat="server" />;
      var scOldValue = "";
   
      function scGetForm() {
        return window.parent.scForm;
      }
      
      function scGetFrameValue(value, request) {
        if (scOldValue.indexOf("__#!$No value$!#__") >= 0) {
          return "__#!$No value$!#__";
        }
        
        var html = scValue;
        
        if (scModified) {
          var form = scGetForm()
          
          if (form != null) {
            form.setModified(true);
          }
          
          if (request != null) {
            request.form += "&EditorChanged=" + encodeURIComponent("1");
          }
        }
        
        return html;
      }
    
      function scSetText(text) {
        scModified = scValue != text;
        
        scValue = text;
              
        if (scValue.indexOf("__#!$No value$!#__") >= 0) {
          document.getElementById("ContentWrapper").innerHTML = "";
        }
        else {
          document.getElementById("ContentWrapper").innerHTML = scValue;
        }
        
        scDisableLinks();
      }
      
      function scOnLoad() {
        scOldValue = scValue;
        
        if (typeof(document.addEventListener) != "undefined") {
          document.addEventListener("keydown", scKeyDown, false);
        }
        
        if (scValue.indexOf("__#!$No value$!#__") >= 0) {
          document.getElementById("ContentWrapper").innerHTML = "";
        }
        
        scDisableLinks();
      }
      
      function scDisableLinks() {
        var form = scGetForm();
        
        var list = document.getElementsByTagName("A");
        for(var n = 0; n < list.length; n++) {
          form.browser.attachEvent(list[n], "onclick", scDoCancel);
        }
      }
      
      function scEdit() {
        if (scDisabled == "1") {
          return;
        }
        
        var form = scGetForm();
        
        if (form != null) {
          var id = location.href;
          
          var n = id.indexOf("&ed=");
          if (n >= 0) {
            id = id.substr(n + 4);
            
            n = id.indexOf("&");
            
            if (n >= 0) {
              id = id.substr(0, n);
            }
          
            form.invoke("richtext:edit(id=" + id + ")");
          }
        }
      }

      function scDoCancel() {
        var evt = window.event;

        if (evt != null) {
          evt.returnValue = false;
          evt.cancelBubble = true;
        }
        
        return false;
      }

      function scKeyDown(e) {
        if (typeof(document.addEventListener) != "undefined" && !e) {
          return;
        }
      
        if (scDisabled == "1") {
          return;
        }
        
        var form = scGetForm();
        var evt = window.event || e;
        
        if (form != null) {
          if (evt.ctrlKey && evt.keyCode == 83) {
            form.postEvent(evt.srcElement, evt, "item:save");
          }
          else {
            form.handleKey(document.body, evt, null, null, true);
          }
          
          form.browser.clearEvent(evt, true, false, 0);
        }
      }
      
    </script>
</head>
<body onload="javascript:scOnLoad()" ondblclick="javascript:scEdit()" onkeydown="javascript:scKeyDown()" title='<asp:PlaceHolder id="HelpText" runat="server" />'>
<div id="ContentWrapper">
  <asp:PlaceHolder id="Content" runat="server" runat="server"/>
</div>
</body>
</html>
