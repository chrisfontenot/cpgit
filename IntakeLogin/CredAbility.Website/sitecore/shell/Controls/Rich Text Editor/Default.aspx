<%@ Page Language="c#" Inherits="Sitecore.Shell.Controls.RADEditor.RADEditor" AutoEventWireup="true" %>
<%@ Register TagPrefix="rad" Namespace="Telerik.WebControls" Assembly="RadEditor.Net2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html style="width: 100%; height: 100%">
  <head>
    <title>Sitecore</title>
    
    <style>
      .scRadEditor {
        width:100%;
        height:100%;
        overflow:auto;
      }
    </style>
    
    <script type="text/javascript" language="javascript">
     var scClientID = "<%=Editor.ClientID%>"
     var scItemID = "<asp:placeholder id="ItemID" runat="server"/>"
     var scLanguage = "<asp:placeholder id="Language" runat="server"/>"
     var scSpecificLanguage = "<asp:placeholder id="SpecificLanguage" runat="server"/>"
    
      function scGetEditor() {
        if (typeof(GetRadEditor)=="function") {
          return GetRadEditor(scClientID);
        }
        
        return null;
      }
      
      function scGetMode() {
        return '<%=Sitecore.Web.WebUtil.GetQueryString("mo")%>';
      }

      function HideToolbars(editor) {
        var holder = document.getElementById("Top" + editor.Id);
        holder.style.display = "none";
        OnClientLoad(editor);
      }
    </script>
    
    <script type="text/javascript" language="javascript" src="/sitecore/shell/Controls/Rich Text Editor/RichText.js"></script>
  </head>
  <body style="overflow:auto;width:100%;height:100%">
    <form runat="server" id="mainForm" method="post" style="width:100%; height:100%">
    
      <table style="width:100%; height:100%; table-layout:fixed" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
            <div style="height:100%;overflow:auto">
      <rad:RadEditor ID="Editor" Runat="server" 
        Width="100%"
        Height="100%"
        
        CssClass="scRadEditor"
        
        HasPermission="True"
        Editable="True"
        
        AllowCustomColors="true"
        AllowThumbGeneration="false"
        AllowScripts="true"
        ConvertToXHtml="true"      
        ConvertFontToSpan="true"
        ConvertTagsToLower="true"
        EnableClientSerialize="true"
        EnableContextMenus="true"
        EnableDocking="true"
        EnableEnhancedEdit="true"
        EnableHtmlIndentation="true"
        EnableServerSideRendering="true"
        EnableTab="true"
        FocusOnLoad="false"
        ShowSubmitCancelButtons="false"
        ShowHtmlMode="false"
        ShowPreviewMode="false"
        StripAbsoluteAnchorPaths="true"
        StripAbsoluteImagesPaths="true"
        UseClassicDialogs="true"
        StripFormattingOnPaste="NoneSupressCleanMessage"

        Skin="Default"
        ToolsFile="~/sitecore/shell/Controls/Rich Text Editor/ToolsFile.xml"
        RadControlsDir="~/sitecore/shell/RadControls"

        ImagesPaths="/media library"
        UploadImagesPaths="/media library"
        DeleteImagesPaths="/media library"
        
        FlashPaths="/media library"
        UploadFlashPaths="/media library"
        DeleteFlashPaths="/media library"
        
        MediaPaths="/media library"
        UploadMediaPaths="/media library"
        DeleteMediaPaths="/media library"

        DocumentsPaths="/media library"
        UploadDocumentsPaths="/media library"
        DeleteDocumentsPaths="/media library"

        TemplatePaths="/media library"
        UploadTemplatePaths="/media library"
        DeleteTemplatePaths="/media library"

        ThumbSuffix="thumb"
        
        OnClientLoad="OnClientLoad"
        OnClientModeChange="OnClientModeChange"
        OnClientCommandExecuted="OnClientCommandExecuted" />
            </div>
          </td>
        </tr>
      </table>

      <script type="text/javascript" language="javascript" src="/sitecore/shell/Controls/Rich Text Editor/RichText Commands.js"></script>
      
      <asp:placeholder id="EditorClientScripts" runat="server"/>
    </form>
  </body>
</html>