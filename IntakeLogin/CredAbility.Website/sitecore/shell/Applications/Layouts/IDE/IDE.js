function scOnIDELoad() {
  var agt = navigator.userAgent.toLowerCase();

  var is_major = parseInt(navigator.appVersion);
  var is_minor = parseFloat(navigator.appVersion);

  var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
  var is_ie3    = (is_ie && (is_major < 4));
  var is_ie4    = (is_ie && (is_major == 4) && (agt.indexOf("msie 4")!=-1) );
  var is_ie4up  = (is_ie && (is_major >= 4));
  var is_ie5    = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
  var is_ie5_5  = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.5") !=-1));
  var is_ie5up  = (is_ie && !is_ie3 && !is_ie4);
  var is_ie5_5up =(is_ie && !is_ie3 && !is_ie4 && !is_ie5);
  var is_ie6    = (is_ie && (is_major == 4) && (agt.indexOf("msie 6.")!=-1) );
  var is_ie6up  = (is_ie && !is_ie3 && !is_ie4 && !is_ie5 && !is_ie5_5);
  
  if (!is_ie6up) {
    alert("Your are currently using " + navigator.userAgent + " (" + is_major + "." + is_minor + ") as your browser.\n\n" +
      "The Sitecore IDE requires Internet Explorer 6.0 or later.\n\n" +
      "You will probably not be able to run the Sitecore IDE.");
  }
}

function scCheckModified(text) {
  var modified = false;
  var result = "yes";
  
  for(var e = scForm.browser.getEnumerator(document.getElementsByTagName("IFRAME")); !e.atEnd(); e.moveNext()) {
    var iframe = e.item();
    
    if (iframe.contentWindow.scForm != null) {
      if (iframe.contentWindow.scForm.modified) {
        modified = true;
        break;
      }
    }
  }
  
  if (modified) {
    if (!confirm(text)) {
      result = "no";
    }
  }
  
  return result;
}
