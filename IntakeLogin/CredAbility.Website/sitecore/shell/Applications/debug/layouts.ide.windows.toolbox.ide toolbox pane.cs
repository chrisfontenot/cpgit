using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDEToolboxPane_a_113 : Sitecore.Shell.Applications.Layouts.IDE.Windows.Toolbox.IDEToolboxPaneXmlControl   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      Caption = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "Class", "scToolboxPaneHeader") as Sitecore.Web.UI.HtmlControls.Border;
      Toggle = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), Caption, "", "Src", "Images/collapse9x9.gif", "Width", "9", "Height", "9", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      Header = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Caption, "") as Sitecore.Web.UI.HtmlControls.Literal;
      ItemsHolder = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "Class", "scToolboxPaneItems") as Sitecore.Web.UI.HtmlControls.Border;
      Items = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ItemsHolder, "") as Sitecore.Web.UI.HtmlControls.Listview;
      
      _Mode = "";
    }
  }
}

