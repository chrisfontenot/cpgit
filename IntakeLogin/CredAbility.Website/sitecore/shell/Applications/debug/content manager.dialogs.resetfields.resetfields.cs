using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class ResetFields_a_102 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public System.Web.UI.Control scSections;
    public Sitecore.Web.UI.HtmlControls.Border Fields;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Control/32x32/edit_b.png", "Header", "Reset Fields", "Text", "You can reset fields to their standard values. The left-hand column contains the current values. The right-hand column contains the standard values.", "OKButton", "Reset") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.ResetFields.ResetFieldsForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formdialog1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      scSections = AddControl("input", "", formdialog1, "", "type", "hidden", "id", idref("scSections"), "name", "scSections");
      Fields = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "", "ID", idref("Fields"), "Background", "white") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(scSections, "id", idref("scSections"));
      SetProperty(Fields, "ID", idref("Fields"));
    }
  }
}

