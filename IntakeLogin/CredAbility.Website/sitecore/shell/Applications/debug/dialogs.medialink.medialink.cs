using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class MediaLink_a_100 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext MediaLinkDataContext;
    public Sitecore.Web.UI.HtmlControls.UpAction UpAction;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox4;
    public Sitecore.Web.UI.WebControls.TreeviewEx MediaLinkTreeview;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border Preview;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Edit Text;
    public Sitecore.Web.UI.HtmlControls.Label label9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Combobox Target;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem11;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem12;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem13;
    public Sitecore.Web.UI.HtmlControls.Panel CustomLabel;
    public Sitecore.Web.UI.HtmlControls.Label label14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Edit CustomTarget;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Edit Class;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Edit Title;
    public Sitecore.Web.UI.HtmlControls.Button button18;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/photo_scenery.png", "Header", "Insert a media library link", "Text", "Please select an item from the media library and specify any additional properties. When done click the OK button.", "OKButton", "OK") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.MediaLink.MediaLinkForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      MediaLinkDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("MediaLinkDataContext"), "Root", "{3D6658D8-A0BF-4E75-B3E2-D050FABCF4E1}") as Sitecore.Web.UI.HtmlControls.DataContext;
      UpAction = AddControl(new Sitecore.Web.UI.HtmlControls.UpAction(), formdialog1, "", "ID", idref("UpAction"), "DataContext", "MediaLinkDataContext") as Sitecore.Web.UI.HtmlControls.UpAction;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Columns", "3", "Width", "100%", "Height", "100%", "CellPadding", "4", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox4 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize", "Background", "window", "Padding", "0", "Border", "1px solid #CFCFCF", "GridPanel.VAlign", "top", "GridPanel.Height", "100%", "GridPanel.Width", "50%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      MediaLinkTreeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox4, "", "ID", idref("MediaLinkTreeview"), "DataContext", "MediaLinkDataContext", "Click", "SelectTreeNode", "DblClick", "OnOpen", "ContextMenu", "MediaLinkTreeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.Width", "50%", "GridPanel.Align", "top", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      Preview = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "ID", idref("Preview"), "Align", "Center", "Height", "136px", "Padding", "4px", "Background", "white", "Class", "scInsetBorder", "GridPanel.Height", "136px") as Sitecore.Web.UI.HtmlControls.Border;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Background", "transparent", "Border", "none", "GridPanel.VAlign", "top", "Padding", "4px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "CellPadding", "2", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Link description:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel7, "", "ID", idref("Text"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      label9 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel7, "", "for", "Target", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label9, "", "Text", "Target window:") as Sitecore.Web.UI.HtmlControls.Literal;
      Target = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel7, "", "ID", idref("Target"), "Width", "100%", "Change", "OnListboxChanged") as Sitecore.Web.UI.HtmlControls.Combobox;
      listitem11 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Self", "Header", "Active browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem12 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "New", "Header", "New browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem13 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Custom", "Header", "Custom") as Sitecore.Web.UI.HtmlControls.ListItem;
      CustomLabel = AddControl(new Sitecore.Web.UI.HtmlControls.Panel(), gridpanel7, "", "ID", idref("CustomLabel"), "Disabled", "true", "Background", "transparent", "Border", "none", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Panel;
      label14 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), CustomLabel, "", "For", "CustomTarget") as Sitecore.Web.UI.HtmlControls.Label;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label14, "", "Text", "Custom:") as Sitecore.Web.UI.HtmlControls.Literal;
      CustomTarget = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel7, "", "ID", idref("CustomTarget"), "Width", "100%", "Disabled", "true") as Sitecore.Web.UI.HtmlControls.Edit;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Style class:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Class = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel7, "", "ID", idref("Class"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Alternate text:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Title = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel7, "", "ID", idref("Title"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      button18 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), formdialog1, "Buttons", "Header", "Upload", "Click", "UploadImage") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(MediaLinkDataContext, "ID", idref("MediaLinkDataContext"));
      SetProperty(UpAction, "ID", idref("UpAction"));
      SetProperty(MediaLinkTreeview, "ID", idref("MediaLinkTreeview"));
      SetProperty(Preview, "ID", idref("Preview"));
      SetProperty(Text, "ID", idref("Text"));
      SetProperty(Target, "ID", idref("Target"));
      SetProperty(CustomLabel, "ID", idref("CustomLabel"));
      SetProperty(CustomTarget, "ID", idref("CustomTarget"));
      SetProperty(Class, "ID", idref("Class"));
      SetProperty(Title, "ID", idref("Title"));
    }
  }
}

