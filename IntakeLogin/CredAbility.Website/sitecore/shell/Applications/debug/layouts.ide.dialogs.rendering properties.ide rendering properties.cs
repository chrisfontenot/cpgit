using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_RenderingProperties_a_145 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Edit Rendering;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Literal ItemName;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Edit Placeholder;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel10;
    public Sitecore.Web.UI.HtmlControls.Edit Datasource;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.HtmlControls.Button BrowseDatasource;
    public Sitecore.Web.UI.HtmlControls.Tabstrip Tabs;
    public Sitecore.Web.UI.HtmlControls.Tab AttributesTab;
    public Sitecore.Web.UI.HtmlControls.Border Attributes;
    public Sitecore.Web.UI.HtmlControls.Tab tab12;
    public Sitecore.Web.UI.HtmlControls.Border Parameters;
    public Sitecore.Web.UI.HtmlControls.Tab tab13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Checkbox Cacheable;
    public System.Web.UI.Control br15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByData;
    public System.Web.UI.Control br17;
    public Sitecore.Web.UI.HtmlControls.Space space18;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByDevice;
    public System.Web.UI.Control br19;
    public Sitecore.Web.UI.HtmlControls.Space space20;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByLogin;
    public System.Web.UI.Control br21;
    public Sitecore.Web.UI.HtmlControls.Space space22;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByParameters;
    public System.Web.UI.Control br23;
    public Sitecore.Web.UI.HtmlControls.Space space24;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByQueryString;
    public System.Web.UI.Control br25;
    public Sitecore.Web.UI.HtmlControls.Space space26;
    public Sitecore.Web.UI.HtmlControls.Checkbox VaryByUser;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Software/32x32/element.png", "Header", "Rendering", "Text", "Please configure the rendering here.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "SetBaseTemplates") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scContentControl {        height:100%;        }        .scContentControlMultilistBox {        height:100%;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Dialogs.RenderingProperties.IDERenderingPropertiesForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Columns", "2", "CellPadding", "4", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "Rendering:", "GridPanel.NoWrap", "true", "GridPanel.Width", "100") as Sitecore.Web.UI.HtmlControls.Literal;
      Rendering = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Rendering"), "ReadOnly", "true", "Background", "Transparent", "Border", "none", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "Item:", "GridPanel.NoWrap", "true", "GridPanel.Width", "100") as Sitecore.Web.UI.HtmlControls.Literal;
      ItemName = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "ID", idref("ItemName"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Literal;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "Placeholder:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Placeholder = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Placeholder"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "Data source:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel10 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel5, "", "Columns", "3", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Datasource = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel10, "", "ID", idref("Datasource"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel10, "", "Width", "2") as Sitecore.Web.UI.HtmlControls.Space;
      BrowseDatasource = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel10, "", "ID", idref("BrowseDatasource"), "Header", "Browse", "Click", "rendering:browse") as Sitecore.Web.UI.HtmlControls.Button;
      Tabs = AddControl(new Sitecore.Web.UI.HtmlControls.Tabstrip(), gridpanel5, "", "ID", idref("Tabs"), "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.ColSpan", "2") as Sitecore.Web.UI.HtmlControls.Tabstrip;
      AttributesTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("AttributesTab"), "Header", "Attributes") as Sitecore.Web.UI.HtmlControls.Tab;
      Attributes = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), AttributesTab, "", "ID", idref("Attributes"), "Width", "100%", "Height", "100%", "Border", "none") as Sitecore.Web.UI.HtmlControls.Border;
      tab12 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Parameters") as Sitecore.Web.UI.HtmlControls.Tab;
      Parameters = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), tab12, "", "ID", idref("Parameters"), "Width", "100%", "Height", "100%", "Border", "none") as Sitecore.Web.UI.HtmlControls.Border;
      tab13 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Caching") as Sitecore.Web.UI.HtmlControls.Tab;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), tab13, "", "Padding", "4") as Sitecore.Web.UI.HtmlControls.Border;
      Cacheable = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("Cacheable"), "Header", "Cacheable", "Click", "SetCacheable") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br15 = AddControl("br", "", border14, "");
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByData = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByData"), "Header", "Vary by data") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br17 = AddControl("br", "", border14, "");
      space18 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByDevice = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByDevice"), "Header", "Vary by device") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br19 = AddControl("br", "", border14, "");
      space20 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByLogin = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByLogin"), "Header", "Vary by login") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br21 = AddControl("br", "", border14, "");
      space22 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByParameters = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByParameters"), "Header", "Vary by parameters") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br23 = AddControl("br", "", border14, "");
      space24 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByQueryString = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByQueryString"), "Header", "Vary by query string") as Sitecore.Web.UI.HtmlControls.Checkbox;
      br25 = AddControl("br", "", border14, "");
      space26 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "16") as Sitecore.Web.UI.HtmlControls.Space;
      VaryByUser = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border14, "", "ID", idref("VaryByUser"), "Header", "Vary by user") as Sitecore.Web.UI.HtmlControls.Checkbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Rendering, "ID", idref("Rendering"));
      SetProperty(ItemName, "ID", idref("ItemName"));
      SetProperty(Placeholder, "ID", idref("Placeholder"));
      SetProperty(Datasource, "ID", idref("Datasource"));
      SetProperty(BrowseDatasource, "ID", idref("BrowseDatasource"));
      SetProperty(Tabs, "ID", idref("Tabs"));
      SetProperty(AttributesTab, "ID", idref("AttributesTab"));
      SetProperty(Attributes, "ID", idref("Attributes"));
      SetProperty(Parameters, "ID", idref("Parameters"));
      SetProperty(Cacheable, "ID", idref("Cacheable"));
      SetProperty(VaryByData, "ID", idref("VaryByData"));
      SetProperty(VaryByDevice, "ID", idref("VaryByDevice"));
      SetProperty(VaryByLogin, "ID", idref("VaryByLogin"));
      SetProperty(VaryByParameters, "ID", idref("VaryByParameters"));
      SetProperty(VaryByQueryString, "ID", idref("VaryByQueryString"));
      SetProperty(VaryByUser, "ID", idref("VaryByUser"));
    }
  }
}

