using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WorkboxCommand_a_140 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Inline inline1;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    
    public string m_Command;
    public string m_Icon;
    public string m_Header;
    
    // properties
    public string Command {
      get {
        return StringUtil.GetString(m_Command);
      }
      set {
        m_Command = value;
        
        SetProperty(inline1, "Click", Command);
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage2, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal3, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      inline1 = AddControl(new Sitecore.Web.UI.HtmlControls.Inline(), this, "", "RollOver", "true", "Class", "scRollover", "Click", Command, "Padding", "0px 8px 0px 0px") as Sitecore.Web.UI.HtmlControls.Inline;
      themedimage2 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), inline1, "", "Src", Icon, "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), inline1, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

