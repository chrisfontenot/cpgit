using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Workbox_a_77 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet7;
    public System.Web.UI.Control text8;
    public Sitecore.Web.UI.XmlControls.XmlControl formpage9;
    public System.Web.UI.Control scActiveRibbonStrip;
    public Sitecore.Web.UI.WebControls.GridPanel Grid;
    public Sitecore.Web.UI.HtmlControls.Border RibbonPanel;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox10;
    public Sitecore.Web.UI.WebControls.GridPanel Grid2;
    public Sitecore.Web.UI.HtmlControls.Border States;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), this, "", "Type", "Sitecore.Shell.Applications.Workbox.WorkboxForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Workbox.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet5 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "/sitecore/shell/themes/navigator.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet7 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Key", "Workbox") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text8 = AddLiteral("      html { width:100%; height:100% }      body { width:100%; height:100%; overflow:auto }    ", "", stylesheet7, "");
      formpage9 = AddControl("FormPage", "", this, "", "Application", "Workbox") as Sitecore.Web.UI.XmlControls.XmlControl;
      scActiveRibbonStrip = AddControl("input", "", formpage9, "", "type", "hidden", "id", idref("scActiveRibbonStrip"), "name", "scActiveRibbonStrip");
      Grid = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage9, "", "ID", idref("Grid"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      RibbonPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Grid, "", "ID", idref("RibbonPanel")) as Sitecore.Web.UI.HtmlControls.Border;
      scrollbox10 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Grid, "", "Padding", "0px", "Background", "transparent", "Border", "none", "GridPanel.Class", "scLightBlueGradient", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.NoWrap", "true", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Grid2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox10, "", "ID", idref("Grid2"), "Width", "100%", "CellPadding", "8") as Sitecore.Web.UI.WebControls.GridPanel;
      States = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Grid2, "", "ID", idref("States")) as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(scActiveRibbonStrip, "id", idref("scActiveRibbonStrip"));
      SetProperty(Grid, "ID", idref("Grid"));
      SetProperty(RibbonPanel, "ID", idref("RibbonPanel"));
      SetProperty(Grid2, "ID", idref("Grid2"));
      SetProperty(States, "ID", idref("States"));
    }
  }
}

