using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class ExternalLink_a_106 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Label label6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Edit Text;
    public Sitecore.Web.UI.HtmlControls.Label label8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Edit Url;
    public Sitecore.Web.UI.HtmlControls.Button button12;
    public Sitecore.Web.UI.HtmlControls.Label label13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Combobox Target;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem15;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem16;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem17;
    public Sitecore.Web.UI.HtmlControls.Panel CustomLabel;
    public Sitecore.Web.UI.HtmlControls.Label label18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.Edit CustomTarget;
    public Sitecore.Web.UI.HtmlControls.Label label20;
    public Sitecore.Web.UI.HtmlControls.Literal literal21;
    public Sitecore.Web.UI.HtmlControls.Edit Class;
    public Sitecore.Web.UI.HtmlControls.Label label22;
    public Sitecore.Web.UI.HtmlControls.Literal literal23;
    public Sitecore.Web.UI.HtmlControls.Edit Title;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/link.png", "Header", "Insert an external link", "Text", "Please specify the URL, e.g. http://www.sitecore.net and any additional properties. When done click the OK button.", "OKButton", "OK") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "Style") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .ff .test {          margin: 0 -4px 0 4px;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.ExternalLink.ExternalLinkForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "CellPadding", "2", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      label6 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel5, "", "For", "Text", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label6, "", "Text", "Link description:") as Sitecore.Web.UI.HtmlControls.Literal;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Text"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      label8 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel5, "", "For", "Url", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label8, "", "Text", "URL:") as Sitecore.Web.UI.HtmlControls.Literal;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border10, "", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Url = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel11, "", "ID", idref("Url"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      button12 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel11, "", "Header", "Test", "Click", "OnTest") as Sitecore.Web.UI.HtmlControls.Button;
      label13 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel5, "", "for", "Target", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label13, "", "Text", "Target window:") as Sitecore.Web.UI.HtmlControls.Literal;
      Target = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel5, "", "ID", idref("Target"), "GridPanel.Width", "100%", "Width", "100%", "Change", "OnListboxChanged") as Sitecore.Web.UI.HtmlControls.Combobox;
      listitem15 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Self", "Header", "Active browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem16 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "New", "Header", "New browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Custom", "Header", "Custom") as Sitecore.Web.UI.HtmlControls.ListItem;
      CustomLabel = AddControl(new Sitecore.Web.UI.HtmlControls.Panel(), gridpanel5, "", "ID", idref("CustomLabel"), "Disabled", "true", "Background", "transparent", "Border", "none", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Panel;
      label18 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), CustomLabel, "", "For", "CustomTarget") as Sitecore.Web.UI.HtmlControls.Label;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label18, "", "Text", "Custom:") as Sitecore.Web.UI.HtmlControls.Literal;
      CustomTarget = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("CustomTarget"), "Width", "100%", "Disabled", "true") as Sitecore.Web.UI.HtmlControls.Edit;
      label20 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel5, "", "For", "Class", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal21 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label20, "", "Text", "Style class:") as Sitecore.Web.UI.HtmlControls.Literal;
      Class = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Class"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      label22 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel5, "", "for", "Title", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal23 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label22, "", "Text", "Alternate text:") as Sitecore.Web.UI.HtmlControls.Literal;
      Title = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Title"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Text, "ID", idref("Text"));
      SetProperty(Url, "ID", idref("Url"));
      SetProperty(Target, "ID", idref("Target"));
      SetProperty(CustomLabel, "ID", idref("CustomLabel"));
      SetProperty(CustomTarget, "ID", idref("CustomTarget"));
      SetProperty(Class, "ID", idref("Class"));
      SetProperty(Title, "ID", idref("Title"));
    }
  }
}

