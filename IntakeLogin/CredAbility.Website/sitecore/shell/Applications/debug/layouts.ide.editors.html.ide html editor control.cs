using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDEHtmlEditorControl_a_145 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public System.Web.UI.Control text1;
    public System.Web.UI.Control html2;
    public System.Web.UI.Control head3;
    public System.Web.UI.Control title4;
    public System.Web.UI.Control text5;
    public System.Web.UI.Control meta6;
    public System.Web.UI.Control meta7;
    public System.Web.UI.Control meta8;
    public Sitecore.Web.UI.HtmlControls.Script script9;
    public Sitecore.Web.UI.HtmlControls.Script script10;
    public Sitecore.Web.UI.HtmlControls.Script script11;
    public System.Web.UI.Control body12;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside13;
    public System.Web.UI.Control Form;
    public Sitecore.Web.UI.HtmlControls.Border CustomizePage;
    public Sitecore.Web.UI.HtmlControls.Border Control;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      text1 = AddLiteral("    <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">    ", "", this, "");
      html2 = AddControl("html", "", this, "");
      head3 = AddControl("head", "", html2, "");
      title4 = AddControl("title", "", head3, "");
      text5 = AddLiteral("Sitecore", "", title4, "");
      meta6 = AddControl("meta", "", head3, "", "name", "GENERATOR", "content", "Sitecore");
      meta7 = AddControl("meta", "", head3, "", "http-equiv", "imagetoolbar", "content", "no");
      meta8 = AddControl("meta", "", head3, "", "http-equiv", "imagetoolbar", "content", "false");
      script9 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), head3, "", "src", "/sitecore/shell/controls/InternetExplorer.js") as Sitecore.Web.UI.HtmlControls.Script;
      script10 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), head3, "", "src", "/sitecore/shell/controls/Sitecore.js") as Sitecore.Web.UI.HtmlControls.Script;
      script11 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), head3, "", "Src", "/sitecore/shell/Applications/Layouts/IDE/Editors/Html/IDE Html Editor Control.js") as Sitecore.Web.UI.HtmlControls.Script;
      body12 = AddControl("body", "", html2, "", "scroll", "no", "style", "margin:0px", "onload", "javascript:scOnLoad()", "onmouseover", "javascript:this.bg=this.style.background;this.style.background='#EBEBEE'", "onmouseout", "javascript:this.style.background=this.bg", "ondblclick", "javascript:scDblClick()", "onclick", "javascript:scClick()");
      codebeside13 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), body12, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Editors.Html.IDEHtmlEditorControlControlForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      Form = AddControl("html:HtmlForm", "", body12, "", "style", "margin:0px");
      CustomizePage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Form, "", "ID", idref("CustomizePage"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      Control = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Form, "", "ID", idref("Control"), "Style", "display:inline;cursor:default") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(CustomizePage, "ID", idref("CustomizePage"));
      SetProperty(Control, "ID", idref("Control"));
    }
  }
}

