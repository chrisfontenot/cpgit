using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class RichTextEditor_a_123 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public System.Web.UI.Control text5;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside6;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Border HtmlEditorPane;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Memo HtmlEditor;
    public Sitecore.Web.UI.HtmlControls.Frame Editor;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar10;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.HtmlControls.Button button12;
    public Sitecore.Web.UI.HtmlControls.Button button13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Toolbutton DesignButton;
    public Sitecore.Web.UI.HtmlControls.Toolbutton HtmlButton;
    public Sitecore.Web.UI.HtmlControls.Space space15;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/SitecoreKeyboard.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Key", "RichTextEditor") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text5 = AddLiteral("        html { width:100%; height:100% }        body { width:100%; height:100% }        form { width:100%; height:100% }                .ff .scToolbar {          background-image: url(/sitecore/shell/themes/standard/Images/Toolbar/ToolbarBackground.png);        }                .ff .scModeButtons {          overflow: auto;        }                .ff .scToolbutton, .ff .scToolbutton_Hover, .ff .scToolbutton_Down, .ff .scToolbutton_Down_Hover {          padding: 1px 2px 2px 2px;        }                .ff .scToolbutton_Hover {          padding: 0px 1px 1px 1px;          height: 19px !important;        }                .ff .scModeButtons span {          height: 17px;          line-height: 20px;        }                .ff .scModeButtons img {          vertical-align: middle;        }                .ff .scCommandButtons {          margin-top: -1px;        }      ", "", stylesheet4, "");
      codebeside6 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.ContentManager.RichText.RichTextEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext"), "Root", "/sitecore/content", "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "Height", "100%", "GridPanel.Width", "100%", "GridPanel.Height", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      HtmlEditorPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border8, "", "ID", idref("HtmlEditorPane"), "Visible", "false", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), HtmlEditorPane, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      HtmlEditor = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel9, "", "ID", idref("HtmlEditor"), "Style", "width:100%;height:100%;border:none;margin:0px", "Wrap", "soft", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      Editor = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), border8, "", "ID", idref("Editor"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Frame;
      toolbar10 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), gridpanel7, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), toolbar10, "", "Class", "scCommandButtons", "Float", "right") as Sitecore.Web.UI.HtmlControls.Border;
      button12 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border11, "", "Header", "Accept", "Click", "Accept_Click") as Sitecore.Web.UI.HtmlControls.Button;
      button13 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border11, "", "Header", "Reject", "Click", "Reject_Click") as Sitecore.Web.UI.HtmlControls.Button;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), toolbar10, "", "Class", "scModeButtons", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      DesignButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border14, "", "ID", idref("DesignButton"), "Header", "Design", "Icon", "Applications/16x16/text_view.png", "IconSize", "id16x16", "Down", "true", "Click", "ShowDesign") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      HtmlButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border14, "", "ID", idref("HtmlButton"), "Header", "HTML", "Icon", "Software/16x16/text_code.png", "IconSize", "id16x16", "Click", "ShowHtml") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space15 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border14, "", "Width", "4") as Sitecore.Web.UI.HtmlControls.Space;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(HtmlEditorPane, "ID", idref("HtmlEditorPane"));
      SetProperty(HtmlEditor, "ID", idref("HtmlEditor"));
      SetProperty(Editor, "ID", idref("Editor"));
      SetProperty(DesignButton, "ID", idref("DesignButton"));
      SetProperty(HtmlButton, "ID", idref("HtmlButton"));
    }
  }
}

