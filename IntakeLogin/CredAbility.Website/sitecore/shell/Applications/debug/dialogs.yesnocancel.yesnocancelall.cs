using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class YesNoCancelAll_a_94 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Literal Text;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Button button8;
    public Sitecore.Web.UI.HtmlControls.Button button9;
    public Sitecore.Web.UI.HtmlControls.Button button10;
    public Sitecore.Web.UI.HtmlControls.Button button11;
    public Sitecore.Web.UI.HtmlControls.Button button12;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "threedface") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Dialogs.YesNoCancelAll.YesNoCancelAllForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "2", "Width", "100%", "GridPanel.Height", "100%", "GridPanel.Valign", "top") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage5 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel4, "", "Src", "Applications/32x32/Warning.png", "Width", "32", "Height", "32", "Margin", "8px 0px 8px 16px", "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Padding", "8px 16px 8px 8px", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "ID", idref("Text")) as Sitecore.Web.UI.HtmlControls.Literal;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Align", "Center", "Padding", "16px 8px 8px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      button8 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Click", "Yes", "Header", "Yes", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Button;
      button9 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Click", "YesToAll", "Header", "Yes to all", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Button;
      button10 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Click", "No", "Header", "No", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Button;
      button11 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Click", "NoToAll", "Header", "No to all", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Button;
      button12 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Click", "Cancel", "Header", "Cancel", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Text, "ID", idref("Text"));
    }
  }
}

