using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class UploadPackage_a_93 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.DataContext UploadDataContext;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public System.Web.UI.Control ul5;
    public System.Web.UI.Control li6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public System.Web.UI.Control li8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.XmlControls.XmlControl Files;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent10;
    public System.Web.UI.Control Item;
    public System.Web.UI.Control Language;
    public System.Web.UI.Control Path;
    public System.Web.UI.Control Unzip;
    public System.Web.UI.Control Overwrite;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Scrollbox FileList;
    public Sitecore.Web.UI.XmlControls.XmlControl Settings;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent12;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Checkbox OverwriteCheck;
    public Sitecore.Web.UI.XmlControls.XmlControl Uploading;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage17;
    public Sitecore.Web.UI.HtmlControls.Space space18;
    public Sitecore.Web.UI.HtmlControls.Border border19;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage20;
    public Sitecore.Web.UI.HtmlControls.Space space21;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.Literal literal23;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Literal literal24;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Install.Dialogs.UploadPackageForm,Sitecore.Client", "FormTarget", "sitecoreupload", "Enctype", "multipart/form-data", "GridPanel.Height", "100%") as Sitecore.Web.UI.XmlControls.XmlControl;
      UploadDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), wizardform1, "", "ID", idref("UploadDataContext"), "DataViewName", "Master", "Root", "{3D6658D8-A0BF-4E75-B3E2-D050FABCF4E1}", "Filter", "Contains('{FE5DD826-48C6-436D-B87A-7C4210C7413B}', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Applications/32x32/Export1.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "Text", "Welcome to the Upload wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul5 = AddControl("ul", "", FirstPage, "");
      li6 = AddControl("li", "", ul5, "", "class", "scWizardBullet");
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li6, "", "Text", "Select a number of files to upload.") as Sitecore.Web.UI.HtmlControls.Literal;
      li8 = AddControl("li", "", ul5, "", "class", "scWizardBullet");
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li8, "", "Text", "Upload the selected files to the server.") as Sitecore.Web.UI.HtmlControls.Literal;
      Files = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Files"), "Header", "Select Files", "Text", "Select the files to upload. Click Next when done.", "Icon", "Applications/32x32/Export1.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent10 = AddControl("WizardFormIndent", "", Files, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Item = AddControl("input", "", wizardformindent10, "", "id", idref("Item"), "name", "Item", "type", "hidden", "value", "");
      Language = AddControl("input", "", wizardformindent10, "", "id", idref("Language"), "name", "Language", "type", "hidden", "value", "");
      Path = AddControl("input", "", wizardformindent10, "", "id", idref("Path"), "name", "Path", "type", "hidden", "value", "");
      Unzip = AddControl("input", "", wizardformindent10, "", "id", idref("Unzip"), "name", "Unzip", "type", "hidden", "value", "0");
      Overwrite = AddControl("input", "", wizardformindent10, "", "id", idref("Overwrite"), "name", "Overwrite", "type", "hidden", "value", "0");
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent10, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      FileList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel11, "", "ID", idref("FileList"), "Width", "100%", "Height", "100%", "Background", "transparent", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Settings = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Settings"), "Header", "Settings", "Text", "Please select settings for uploading the files. When ready, click Next to upload the files.", "Icon", "Applications/32x32/Export1.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent12 = AddControl("WizardFormIndent", "", Settings, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel13 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent12, "", "Columns", "2", "CellPadding", "4") as Sitecore.Web.UI.WebControls.GridPanel;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel13, "", "Text", "Overwrite existing files:") as Sitecore.Web.UI.HtmlControls.Literal;
      OverwriteCheck = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), gridpanel13, "", "ID", idref("OverwriteCheck"), "Change", "upload:overwriteclicked") as Sitecore.Web.UI.HtmlControls.Checkbox;
      Uploading = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Uploading"), "Header", "Uploading", "Text", "Please wait while the file(s) are being uploaded...", "Icon", "Applications/32x32/Export1.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel15 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Uploading, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage17 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel15, "", "Src", "Applications/48x48/Export1.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space18 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border19 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel15, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage20 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border19, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space21 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel15, "", "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal23 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border22, "", "Text", "Uploading...") as Sitecore.Web.UI.HtmlControls.Literal;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Applications/32x32/Export1.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      literal24 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), LastPage, "", "Text", "The wizard has completed. Click the Finish button to close the Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(UploadDataContext, "ID", idref("UploadDataContext"));
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Files, "ID", idref("Files"));
      SetProperty(Item, "id", idref("Item"));
      SetProperty(Language, "id", idref("Language"));
      SetProperty(Path, "id", idref("Path"));
      SetProperty(Unzip, "id", idref("Unzip"));
      SetProperty(Overwrite, "id", idref("Overwrite"));
      SetProperty(FileList, "ID", idref("FileList"));
      SetProperty(Settings, "ID", idref("Settings"));
      SetProperty(OverwriteCheck, "ID", idref("OverwriteCheck"));
      SetProperty(Uploading, "ID", idref("Uploading"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

