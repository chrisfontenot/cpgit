using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_XPath_Builder_a_133 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public System.Web.UI.Control text4;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside5;
    public Sitecore.Web.UI.HtmlControls.RegisterKey registerkey6;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.TreePicker DataSource;
    public Sitecore.Web.UI.HtmlControls.Combobox Databases;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Memo Editor;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Radiobutton Sitecore;
    public Sitecore.Web.UI.HtmlControls.Radiobutton Real;
    public Sitecore.Web.UI.HtmlControls.Button button16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Result;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "threedface") as Sitecore.Web.UI.XmlControls.XmlControl;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Layouts/IDE/IDE.js") as Sitecore.Web.UI.HtmlControls.Script;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Key", "XPathBuilder") as Sitecore.Web.UI.HtmlControls.Script;
      text4 = AddLiteral("        scSitecore.prototype.setModified = function(value) {          this.modified = false;        }      ", "", script3, "");
      codebeside5 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Editors.XPathBuilder.IDEXPathBuilderForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      registerkey6 = AddControl(new Sitecore.Web.UI.HtmlControls.RegisterKey(), formpage1, "", "KeyCode", "c69", "Click", "Evaluate") as Sitecore.Web.UI.HtmlControls.RegisterKey;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext"), "Root", "/sitecore", "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "CellPadding", "4") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel7, "", "Columns", "2", "Width", "100%", "Height", "100%", "CellPadding", "4") as Sitecore.Web.UI.WebControls.GridPanel;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Context Node:") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel8, "", "Width", "100%", "Columns", "2", "GridPanel.Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      DataSource = AddControl(new Sitecore.Web.UI.HtmlControls.TreePicker(), gridpanel11, "", "ID", idref("DataSource"), "DataContext", "DataContext", "SelectOnly", "true", "GridPanel.Width", "100%", "Padding", "1") as Sitecore.Web.UI.HtmlControls.TreePicker;
      Databases = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel11, "", "ID", idref("Databases"), "Change", "ChangeDatabase", "GridPanel.Style", "padding:0px 0px 0px 4px") as Sitecore.Web.UI.HtmlControls.Combobox;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "XPath Expression:", "GridPanel.NoWrap", "true", "GridPanel.VAlign", "top", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Editor = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel8, "", "ID", idref("Editor"), "Width", "100%", "Height", "100", "Wrap", "off", "Value", "/sitecore/content/*", "GridPanel.Height", "100", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel8, "", "GridPanel.Style", "padding:0px 4px 16px 4px") as Sitecore.Web.UI.HtmlControls.Space;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "GridPanel.Style", "padding:0px 4px 16px 4px") as Sitecore.Web.UI.HtmlControls.Border;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border14, "", "NoWrap", "true", "Float", "right") as Sitecore.Web.UI.HtmlControls.Border;
      Sitecore = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border15, "", "ID", idref("Sitecore"), "Name", "Sitecore", "Value", "Sitecore", "Header", "Sitecore Query Notation", "Checked", "true", "Tooltip", "E.g. /sitecore/content/Home") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      Real = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border15, "", "ID", idref("Real"), "Name", "Sitecore", "Value", "Real", "Header", "XPath", "Tooltip", "E.g. /item[@key='sitecore']/item[@key='content']/item[@key='Home']") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      button16 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border14, "", "Header", "Evaluate", "Click", "Evaluate") as Sitecore.Web.UI.HtmlControls.Button;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Result:", "GridPanel.NoWrap", "true", "GridPanel.VAlign", "top", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Result = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel8, "", "ID", idref("Result"), "Height", "100%", "GridPanel.Height", "100%", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Result, "", "Align", "Center", "Padding", "32px 0px 0px 0px", "Foreground", "#999999") as Sitecore.Web.UI.HtmlControls.Border;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border18, "", "Text", "Enter an expression in the XPath Expression field.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(DataSource, "ID", idref("DataSource"));
      SetProperty(Databases, "ID", idref("Databases"));
      SetProperty(Editor, "ID", idref("Editor"));
      SetProperty(Sitecore, "ID", idref("Sitecore"));
      SetProperty(Real, "ID", idref("Real"));
      SetProperty(Result, "ID", idref("Result"));
    }
  }
}

