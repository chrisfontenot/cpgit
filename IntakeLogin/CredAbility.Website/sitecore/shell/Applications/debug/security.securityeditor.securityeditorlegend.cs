using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SecurityEditorLegend_a_92 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Space space3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage10;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage14;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage15;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Border border17;
    public System.Web.UI.Control div18;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage19;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage20;
    public System.Web.UI.Control br21;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage22;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage23;
    public Sitecore.Web.UI.HtmlControls.Space space24;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public Sitecore.Web.UI.HtmlControls.Border border26;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage27;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage28;
    public Sitecore.Web.UI.HtmlControls.Literal literal29;
    public Sitecore.Web.UI.HtmlControls.Border border30;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage31;
    public Sitecore.Web.UI.HtmlControls.Literal literal32;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Background", "#e9e9e9", "Height", "24px", "Style", "border-top:2px groove", "Padding", "2px 4px 2px 4px") as Sitecore.Web.UI.HtmlControls.Border;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      space3 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border2, "", "Width", "1", "Height", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.Space;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "Text", "Key:") as Sitecore.Web.UI.HtmlControls.Literal;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border5, "", "Src", "Images/Security/large_allow_disabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage7 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border5, "", "Src", "Images/Security/large_deny_disabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border5, "", "Text", "Inherited") as Sitecore.Web.UI.HtmlControls.Literal;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage10 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border9, "", "Src", "Images/Security/large_allow_enabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage11 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border9, "", "Src", "Images/Security/large_deny_disabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Allowed") as Sitecore.Web.UI.HtmlControls.Literal;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage14 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border13, "", "Src", "Images/Security/large_allow_disabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage15 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border13, "", "Src", "Images/Security/large_deny_enabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", "Denied") as Sitecore.Web.UI.HtmlControls.Literal;
      border17 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      div18 = AddControl("div", "", border17, "", "style", "position:absolute");
      themedimage19 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), div18, "", "Src", "Images/Security/small_allow_disabled.gif", "Width", "16", "Height", "8", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage20 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), div18, "", "Src", "Images/Security/small_deny_disabled.gif", "Width", "16", "Height", "8", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      br21 = AddControl("br", "", div18, "");
      themedimage22 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), div18, "", "Src", "Images/Security/small_allow_enabled.gif", "Width", "16", "Height", "8", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage23 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), div18, "", "Src", "Images/Security/small_deny_disabled.gif", "Width", "16", "Height", "8", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space24 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border17, "", "Width", "34", "HEight", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.Space;
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border17, "", "Text", "Item vs. Descendant Right") as Sitecore.Web.UI.HtmlControls.Literal;
      border26 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage27 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border26, "", "Src", "Images/Security/large_allow_enabled_d.gif", "Width", "16", "Height", "16", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      themedimage28 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border26, "", "Src", "Images/Security/large_deny_disabled.gif", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal29 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border26, "", "Text", "Protected") as Sitecore.Web.UI.HtmlControls.Literal;
      border30 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "", "Float", "left", "FontBold", "true", "Margin", "0px 0px 0px 12px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage31 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border30, "", "Src", "Images/Security/NA.gif", "Width", "32", "Height", "16", "Align", "absmiddle", "Margin", "0px 2px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal32 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border30, "", "Text", "Not Applicable") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

