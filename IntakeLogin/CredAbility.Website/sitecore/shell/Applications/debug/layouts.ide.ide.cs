using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_a_107 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside6;
    public System.Web.UI.Control __FRAMENAME;
    public Sitecore.Web.UI.HtmlControls.ContextMenu ContextMenu;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem7;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem8;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem9;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.RenderItem Menu;
    public Sitecore.Web.UI.HtmlControls.Border Workspace;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.XmlControls.XmlControl MDI;
    public Sitecore.Web.UI.HtmlControls.Border border16;
    public Sitecore.Web.UI.HtmlControls.Border border17;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Layouts/IDE", "Submittable", "false", "OnLoadEvent", "scOnIDELoad()") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreWindowManager.js") as Sitecore.Web.UI.HtmlControls.Script;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "src", "/sitecore/shell/Applications/Layouts/IDE/IDE.js") as Sitecore.Web.UI.HtmlControls.Script;
      codebeside6 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.IDEForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      __FRAMENAME = AddControl("input", "", formpage1, "", "id", idref("__FRAMENAME"), "type", "hidden", "value", "IDE");
      ContextMenu = AddControl(new Sitecore.Web.UI.HtmlControls.ContextMenu(), formpage1, "", "ID", idref("ContextMenu")) as Sitecore.Web.UI.HtmlControls.ContextMenu;
      menuitem7 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Icon", "Applications/16x16/disk_blue.png", "Header", "Save", "Click", "SaveTab") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menuitem8 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Header", "Close", "Click", "CloseTab") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menuitem9 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Header", "Close All But This", "Click", "CloseAllButThisTab") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menuitem10 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Header", "Close All", "Click", "CloseAll") as Sitecore.Web.UI.HtmlControls.MenuItem;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      Menu = AddControl(new Sitecore.Web.UI.HtmlControls.RenderItem(), gridpanel11, "", "ID", idref("Menu"), "DataSource", "/sitecore/content/Applications/Layouts/IDE/Toolbar") as Sitecore.Web.UI.HtmlControls.RenderItem;
      Workspace = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel11, "", "ID", idref("Workspace"), "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Workspace, "", "Width", "100%", "Columns", "3", "Height", "100%", "Background", "appworkspace") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Height", "1", "Width", "100%", "Class", "scTopDockZone", "GridPanel.ColSpan", "3", "GridPanel.Height", "1", "GridPanel.Style", "font:1px tahoma") as Sitecore.Web.UI.HtmlControls.Border;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Width", "1", "Height", "100%", "Class", "scLeftDockZone", "GridPanel.Width", "16") as Sitecore.Web.UI.HtmlControls.Border;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.Width", "100%", "GridPanel.Height", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      MDI = AddControl("MDI", "", border15, "", "ID", idref("MDI"), "ContextMenu", "show:ContextMenu") as Sitecore.Web.UI.XmlControls.XmlControl;
      border16 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Width", "1", "Height", "100%", "Class", "scRightDockZone", "GridPanel.Width", "16", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      border17 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Height", "1", "Width", "100%", "Class", "scBottomDockZone", "GridPanel.ColSpan", "3", "GridPanel.Height", "1", "GridPanel.Style", "font:1px tahoma") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(__FRAMENAME, "id", idref("__FRAMENAME"));
      SetProperty(ContextMenu, "ID", idref("ContextMenu"));
      SetProperty(Menu, "ID", idref("Menu"));
      SetProperty(Workspace, "ID", idref("Workspace"));
      SetProperty(MDI, "ID", idref("MDI"));
    }
  }
}

