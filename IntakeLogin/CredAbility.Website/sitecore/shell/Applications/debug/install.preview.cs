using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_Preview_a_78 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public System.Web.UI.Control script5;
    public System.Web.UI.Control text6;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside7;
    public System.Web.UI.Control __ActiveRibbonStrip;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Border RibbonFrame;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Border SplashFrame;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel10;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage12;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.HtmlControls.Border PublishingText;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Border ViewerFrame;
    public Sitecore.Web.UI.XmlControls.XmlControl Viewer;
    public Sitecore.Web.UI.WebControls.GridPanel StatusBar;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Literal StatusText;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      script5 = AddControl("script", "", formpage1, "", "language", "javascript");
      text6 = AddLiteral("        scForm.browser.attachEvent(window, 'onload', function() { scForm.invoke('preview:prepare'); });      ", "", script5, "");
      codebeside7 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Install.Preview, Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      __ActiveRibbonStrip = AddControl("input", "", formpage1, "", "type", "hidden", "id", idref("__ActiveRibbonStrip"), "name", "__ActiveRibbonStrip");
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Background", "white", "CellPadding", "0", "CellSpacing", "0") as Sitecore.Web.UI.WebControls.GridPanel;
      RibbonFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "GridPanel.Row.Height", "20px", "ID", idref("RibbonFrame"), "GridPanel.Height", "0px", "GridPanel.Style", "border:solid 1px black") as Sitecore.Web.UI.HtmlControls.Border;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      SplashFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "ID", idref("SplashFrame"), "GridPanel.Style", "padding:1px;", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel10 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), SplashFrame, "", "Width", "100%", "Style", "vertical-align:middle") as Sitecore.Web.UI.WebControls.GridPanel;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel10, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage12 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel10, "", "Src", "People/48x48/Box_View.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel10, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel10, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage15 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border14, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel10, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      PublishingText = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel10, "", "ID", idref("PublishingText"), "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), PublishingText, "", "Text", "Generating preview information...") as Sitecore.Web.UI.HtmlControls.Literal;
      ViewerFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "ID", idref("ViewerFrame"), "Style", "display:none", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      Viewer = AddControl("Installer.PackageViewer", "", ViewerFrame, "", "ID", idref("Viewer")) as Sitecore.Web.UI.XmlControls.XmlControl;
      StatusBar = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel8, "", "ID", idref("StatusBar"), "Width", "100%", "GridPanel.Row.Height", "20px", "Background", "white", "Columns", "2", "CellPadding", "1") as Sitecore.Web.UI.WebControls.GridPanel;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), StatusBar, "", "Border", "solid 1px silver") as Sitecore.Web.UI.HtmlControls.Border;
      StatusText = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border18, "", "ID", idref("StatusText"), "Text", "", "Style", "padding:1px") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(__ActiveRibbonStrip, "id", idref("__ActiveRibbonStrip"));
      SetProperty(RibbonFrame, "ID", idref("RibbonFrame"));
      SetProperty(SplashFrame, "ID", idref("SplashFrame"));
      SetProperty(PublishingText, "ID", idref("PublishingText"));
      SetProperty(ViewerFrame, "ID", idref("ViewerFrame"));
      SetProperty(Viewer, "ID", idref("Viewer"));
      SetProperty(StatusBar, "ID", idref("StatusBar"));
      SetProperty(StatusText, "ID", idref("StatusText"));
    }
  }
}

