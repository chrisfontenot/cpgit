using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Sitecore_Shell_Applications_Dialogs_SelectItem_a_124 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Items;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "Core/32x32/open_document.png", "Header", "Open Item", "Text", "Select the item that you wish to open. Then click the Open button.", "OKButton", "Open") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Dialogs.SelectItem.SelectItemForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), Dialog, "", "ID", idref("DataContext"), "Root", "/") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      Items = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "ID", idref("Items"), "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), Items, "", "ID", idref("Treeview"), "DataContext", "DataContext", "ShowRoot", "true", "DblClick", "OK_Click", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Items, "ID", idref("Items"));
      SetProperty(Treeview, "ID", idref("Treeview"));
    }
  }
}

