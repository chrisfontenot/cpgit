using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TemplateBuilder_a_122 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.Script script7;
    public Sitecore.Web.UI.HtmlControls.Script script8;
    public Sitecore.Web.UI.HtmlControls.Script script9;
    public Sitecore.Web.UI.HtmlControls.Script script10;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside11;
    public System.Web.UI.Control scActiveRibbonStrip;
    public System.Web.UI.Control Structure;
    public System.Web.UI.Control AddNewSectionText;
    public System.Web.UI.Control AddNewFieldText;
    public System.Web.UI.Control FieldTypes;
    public System.Web.UI.Control TemplateID;
    public System.Web.UI.Control Caption;
    public System.Web.UI.Control Active;
    public Sitecore.Web.UI.HtmlControls.Border RibbonPanel;
    public Sitecore.Web.UI.HtmlControls.Border Ribbon;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Scrollbox TemplatePanel;
    public Sitecore.Web.UI.HtmlControls.RegisterKey registerkey13;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Templates/Template Builder") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "TemplateBuilder.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/controls/SitecoreVSplitter.js") as Sitecore.Web.UI.HtmlControls.Script;
      script7 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/controls/SitecoreKeyboard.js") as Sitecore.Web.UI.HtmlControls.Script;
      script8 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/Sitecore.Runtime.js") as Sitecore.Web.UI.HtmlControls.Script;
      script9 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Templates/TemplateBuilder/TemplateBuilder.xml.js") as Sitecore.Web.UI.HtmlControls.Script;
      script10 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      codebeside11 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Templates.TemplateBuilder.TemplateBuilderForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      scActiveRibbonStrip = AddControl("input", "", formpage1, "", "id", idref("scActiveRibbonStrip"), "name", "scActiveRibbonStrip", "type", "hidden");
      Structure = AddControl("input", "", formpage1, "", "id", idref("Structure"), "type", "hidden");
      AddNewSectionText = AddControl("input", "", formpage1, "", "id", idref("AddNewSectionText"), "type", "hidden");
      AddNewFieldText = AddControl("input", "", formpage1, "", "id", idref("AddNewFieldText"), "type", "hidden");
      FieldTypes = AddControl("input", "", formpage1, "", "id", idref("FieldTypes"), "type", "hidden");
      TemplateID = AddControl("input", "", formpage1, "", "id", idref("TemplateID"), "type", "hidden");
      Caption = AddControl("input", "", formpage1, "", "id", idref("Caption"), "type", "hidden");
      Active = AddControl("input", "", formpage1, "", "id", idref("Active"), "type", "hidden");
      RibbonPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "ID", idref("RibbonPanel"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      Ribbon = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), RibbonPanel, "", "ID", idref("Ribbon")) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      TemplatePanel = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel12, "", "ID", idref("TemplatePanel"), "Width", "100%", "Height", "100%", "Background", "white", "Padding", "0px", "Border", "none", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      registerkey13 = AddControl(new Sitecore.Web.UI.HtmlControls.RegisterKey(), formpage1, "", "KeyCode", "c83", "Click", "templatebuilder:save") as Sitecore.Web.UI.HtmlControls.RegisterKey;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(scActiveRibbonStrip, "id", idref("scActiveRibbonStrip"));
      SetProperty(Structure, "id", idref("Structure"));
      SetProperty(AddNewSectionText, "id", idref("AddNewSectionText"));
      SetProperty(AddNewFieldText, "id", idref("AddNewFieldText"));
      SetProperty(FieldTypes, "id", idref("FieldTypes"));
      SetProperty(TemplateID, "id", idref("TemplateID"));
      SetProperty(Caption, "id", idref("Caption"));
      SetProperty(Active, "id", idref("Active"));
      SetProperty(RibbonPanel, "ID", idref("RibbonPanel"));
      SetProperty(Ribbon, "ID", idref("Ribbon"));
      SetProperty(TemplatePanel, "ID", idref("TemplatePanel"));
    }
  }
}

