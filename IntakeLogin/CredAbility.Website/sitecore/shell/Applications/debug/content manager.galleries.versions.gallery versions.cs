using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_Versions_a_95 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl gallery1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public System.Web.UI.Control text4;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet5;
    public System.Web.UI.Control text6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.GalleryMenu Options;
    public Sitecore.Web.UI.HtmlControls.MenuPanel menupanel8;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Versions;
    public Sitecore.Web.UI.XmlControls.XmlControl gallery_grip9;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gallery1 = AddControl("Gallery", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), gallery1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Galleries.Versions.GalleryVersionsForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), gallery1, "") as Sitecore.Web.UI.HtmlControls.Script;
      text4 = AddLiteral("        if (Prototype.Browser.Gecko) {          scForm = window.parent.scForm;        }      ", "", script3, "");
      stylesheet5 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), gallery1, "", "Key", "GalleryLanguages") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text6 = AddLiteral("        .scMenuPanelItem, .scMenuPanelItem_Hover, .scMenuPanelItemSelected_Hover, .scMenuPanelItemSelected {        padding-left: 0;        padding-right: 0;        }      ", "", stylesheet5, "");
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gallery1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Options = AddControl(new Sitecore.Web.UI.HtmlControls.GalleryMenu(), gridpanel7, "", "ID", idref("Options"), "GridPanel.Height", "100%", "GridPanel.VAlign", "top", "Style", "overflow:hidden") as Sitecore.Web.UI.HtmlControls.GalleryMenu;
      menupanel8 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuPanel(), Options, "", "Height", "100%") as Sitecore.Web.UI.HtmlControls.MenuPanel;
      Versions = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), menupanel8, "", "ID", idref("Versions"), "Height", "100%", "Background", "#F4F4F5", "Padding", "0", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gallery_grip9 = AddControl("Gallery.Grip", "", gridpanel7, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Options, "ID", idref("Options"));
      SetProperty(Versions, "ID", idref("Versions"));
    }
  }
}

