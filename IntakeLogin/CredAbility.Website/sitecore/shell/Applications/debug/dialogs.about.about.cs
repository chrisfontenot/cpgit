using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class AboutSitecore_a_107 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Border VersionInfo;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Literal LicensedTo;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Literal CompanyNo;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Literal Address;
    public Sitecore.Web.UI.HtmlControls.Literal literal11;
    public Sitecore.Web.UI.HtmlControls.Literal Version;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Literal Expiration;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public Sitecore.Web.UI.HtmlControls.Literal Purpose;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Literal ForUseAt;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Literal Reseller;
    public Sitecore.Web.UI.HtmlControls.Border LicenseAgreement;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Copyright;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Literal literal18;
    public Sitecore.Web.UI.HtmlControls.Scrollbox AdditionalInfoContainer;
    public Sitecore.Web.UI.HtmlControls.Literal AdditionalInfo;
    public Sitecore.Web.UI.HtmlControls.Border border19;
    public Sitecore.Web.UI.HtmlControls.Literal literal20;
    public System.Web.UI.Control a21;
    public System.Web.UI.Control text22;
    public System.Web.UI.Control text23;
    public Sitecore.Web.UI.HtmlControls.Border border24;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public Sitecore.Web.UI.HtmlControls.Border border26;
    public System.Web.UI.Control hr27;
    public Sitecore.Web.UI.HtmlControls.Border border28;
    public Sitecore.Web.UI.HtmlControls.Border border29;
    public Sitecore.Web.UI.HtmlControls.Button OK;
    public Sitecore.Web.UI.HtmlControls.Literal literal30;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "#e9e9e9") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Dialogs.About.AboutForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Style", "padding:8px 16px 0px 16px", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      VersionInfo = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "ID", idref("VersionInfo"), "GridPanel.VAlign", "top", "Style", "font-weight:bold", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "License:") as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox6 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel4, "", "GridPanel.VAlign", "top", "Border", "1px inset", "GridPanel.Height", "80") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox6, "", "CellPadding", "2", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Licensed to:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      LicensedTo = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("LicensedTo"), "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Literal;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Company no:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      CompanyNo = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("CompanyNo")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Address:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Address = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("Address")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal11 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Version:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Version = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("Version")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Expiration:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Expiration = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("Expiration")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Purpose:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Purpose = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("Purpose")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "For use at:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      ForUseAt = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("ForUseAt")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Reseller:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Reseller = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "ID", idref("Reseller")) as Sitecore.Web.UI.HtmlControls.Literal;
      LicenseAgreement = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "ID", idref("LicenseAgreement"), "GridPanel.ColSpan", "2") as Sitecore.Web.UI.HtmlControls.Border;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Copyright:", "GridPanel.Style", "padding:8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Literal;
      Copyright = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel4, "", "ID", idref("Copyright"), "GridPanel.VAlign", "top", "Border", "1px inset", "GridPanel.Height", "80") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Copyright, "", "Text", "Portions of #ziplib by Mike Krueger (c) 2000-2002. All rights reserved. CSS Editor by Hsun-Cheng Hu (http://home.kimo.com.tw/huchengtw/en/index.html). R.A.D. HTML Editor by Telerik (c) Copyright 2002 - 2006.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal18 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Additional Information:", "GridPanel.Style", "padding:8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Literal;
      AdditionalInfoContainer = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel4, "", "ID", idref("AdditionalInfoContainer"), "GridPanel.VAlign", "top", "Border", "1px inset", "GridPanel.Height", "50") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      AdditionalInfo = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), AdditionalInfoContainer, "", "ID", idref("AdditionalInfo")) as Sitecore.Web.UI.HtmlControls.Literal;
      border19 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Padding", "8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal20 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border19, "", "Text", "Visit the Sitecore homepage at", "Padding", "0px 0.5em 0px 0px") as Sitecore.Web.UI.HtmlControls.Literal;
      a21 = AddControl("a", "", border19, "", "href", "http://www.sitecore.net", "target", "_blank");
      text22 = AddLiteral("www.sitecore.net", "", a21, "");
      text23 = AddLiteral(".          ", "", border19, "");
      border24 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Padding", "8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border24, "", "Text", "Warning: This computer program is protected by copyright law and international   treaties. Unauthorized reproduction or distribution of this program, or any   portion of it, may result in severe civil and criminal penalties, and will be   prosecuted to the maximum extent possible under the law.") as Sitecore.Web.UI.HtmlControls.Literal;
      border26 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "0px 16px 0px 16px") as Sitecore.Web.UI.HtmlControls.Border;
      hr27 = AddControl("hr", "", border26, "");
      border28 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "0px 16px 0px 16px") as Sitecore.Web.UI.HtmlControls.Border;
      border29 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border28, "", "Align", "right", "Padding", "0px 0px 4px 8px", "NoWrap", "NoWrap", "Height", "34") as Sitecore.Web.UI.HtmlControls.Border;
      OK = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border29, "", "ID", idref("OK"), "Width", "80", "Height", "25", "Click", "#") as Sitecore.Web.UI.HtmlControls.Button;
      literal30 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), OK, "", "Text", "Close") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(VersionInfo, "ID", idref("VersionInfo"));
      SetProperty(LicensedTo, "ID", idref("LicensedTo"));
      SetProperty(CompanyNo, "ID", idref("CompanyNo"));
      SetProperty(Address, "ID", idref("Address"));
      SetProperty(Version, "ID", idref("Version"));
      SetProperty(Expiration, "ID", idref("Expiration"));
      SetProperty(Purpose, "ID", idref("Purpose"));
      SetProperty(ForUseAt, "ID", idref("ForUseAt"));
      SetProperty(Reseller, "ID", idref("Reseller"));
      SetProperty(LicenseAgreement, "ID", idref("LicenseAgreement"));
      SetProperty(Copyright, "ID", idref("Copyright"));
      SetProperty(AdditionalInfoContainer, "ID", idref("AdditionalInfoContainer"));
      SetProperty(AdditionalInfo, "ID", idref("AdditionalInfo"));
      SetProperty(OK, "ID", idref("OK"));
    }
  }
}

