using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_MetadataEditor_a_86 : Sitecore.Shell.Applications.Install.Controls.MetadataEditor   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl installer_fieldset1;
    public Sitecore.Web.UI.HtmlControls.Space space2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public Sitecore.Web.UI.HtmlControls.Space space4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Space space6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_fieldset8;
    public Sitecore.Web.UI.HtmlControls.Space space9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton14;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox15;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_fieldset16;
    public Sitecore.Web.UI.HtmlControls.Space space17;
    public Sitecore.Web.UI.HtmlControls.Literal literal18;
    public Sitecore.Web.UI.HtmlControls.Space space19;
    public Sitecore.Web.UI.HtmlControls.Border border20;
    public Sitecore.Web.UI.HtmlControls.Literal literal21;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton22;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_fieldset23;
    public Sitecore.Web.UI.HtmlControls.Space space24;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public Sitecore.Web.UI.HtmlControls.Space space26;
    public Sitecore.Web.UI.HtmlControls.Literal literal27;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox28;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      installer_fieldset1 = AddControl("Installer.FieldSet", "", this, "", "Header", "General Info") as Sitecore.Web.UI.XmlControls.XmlControl;
      space2 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset1, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset1, "", "Text", "Package Name") as Sitecore.Web.UI.HtmlControls.Literal;
      PackageName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), installer_fieldset1, "", "Width", "100%", "class", "scInstallerField", "maxlength", "50") as Sitecore.Web.UI.HtmlControls.Edit;
      space4 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset1, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset1, "", "Text", "Author") as Sitecore.Web.UI.HtmlControls.Literal;
      Author = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), installer_fieldset1, "", "Width", "100%", "Class", "scInstallerField") as Sitecore.Web.UI.HtmlControls.Edit;
      space6 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset1, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset1, "", "Text", "Version") as Sitecore.Web.UI.HtmlControls.Literal;
      Version = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), installer_fieldset1, "", "Width", "100%", "Class", "scInstallerField", "maxlength", "50") as Sitecore.Web.UI.HtmlControls.Edit;
      installer_fieldset8 = AddControl("Installer.FieldSet", "", this, "", "Header", "Publishing") as Sitecore.Web.UI.XmlControls.XmlControl;
      space9 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset8, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset8, "", "Text", "Publisher") as Sitecore.Web.UI.HtmlControls.Literal;
      Publisher = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), installer_fieldset8, "", "Width", "100%", "Class", "scInstallerField") as Sitecore.Web.UI.HtmlControls.Edit;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset8, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), installer_fieldset8, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border12, "", "Text", "License") as Sitecore.Web.UI.HtmlControls.Literal;
      toolbutton14 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border12, "", "Icon", "Applications/16x16/edit.png", "IconSize", "id16x16", "Tooltip", "Modify...", "Click", "installer:modifylicense") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      scrollbox15 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), installer_fieldset8, "", "Width", "100%", "Height", "200px", "Border", "solid 1px #dcdcdc") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      License = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), scrollbox15, "", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Literal;
      installer_fieldset16 = AddControl("Installer.FieldSet", "", this, "", "Header", "Documentation") as Sitecore.Web.UI.XmlControls.XmlControl;
      space17 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset16, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal18 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset16, "", "Text", "Comment") as Sitecore.Web.UI.HtmlControls.Literal;
      Comment = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), installer_fieldset16, "", "Width", "100%", "Height", "100px", "Class", "scInstallerField") as Sitecore.Web.UI.HtmlControls.Memo;
      space19 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset16, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      border20 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), installer_fieldset16, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal21 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border20, "", "Text", "Read me") as Sitecore.Web.UI.HtmlControls.Literal;
      toolbutton22 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border20, "", "Icon", "Applications/16x16/edit.png", "IconSize", "id16x16", "Tooltip", "Modify...", "Click", "installer:modifyreadme") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      Readme = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), installer_fieldset16, "", "ReadOnly", "true", "Class", "scInstallerField", "Width", "100%", "Height", "200px", "Style", "scroll:auto") as Sitecore.Web.UI.HtmlControls.Memo;
      installer_fieldset23 = AddControl("Installer.FieldSet", "", this, "", "Header", "System") as Sitecore.Web.UI.XmlControls.XmlControl;
      space24 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset23, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset23, "", "Text", "Post Step", "Margin", "0 0 0 2px") as Sitecore.Web.UI.HtmlControls.Literal;
      PostStep = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), installer_fieldset23, "", "Width", "100%", "Class", "scInstallerField", "Margin", "0px 2px") as Sitecore.Web.UI.HtmlControls.Edit;
      space26 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), installer_fieldset23, "", "Height", "5px") as Sitecore.Web.UI.HtmlControls.Space;
      literal27 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), installer_fieldset23, "", "Text", "Custom Attributes", "Margin", "0 0 0 2px") as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox28 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), installer_fieldset23, "", "Width", "100%", "Height", "200px", "Border", "none", "Padding", "0px") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      CustomAttributes = AddControl("Installer.AttributesEditor", "", scrollbox28, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

