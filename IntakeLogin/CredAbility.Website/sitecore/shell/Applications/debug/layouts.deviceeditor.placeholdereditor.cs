using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class PlaceholderEditor_a_121 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.HtmlControls.Edit PlaceholderName;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Edit MetaDataItem;
    public Sitecore.Web.UI.HtmlControls.Space space7;
    public Sitecore.Web.UI.HtmlControls.Button BrowseButton;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Imaging/32x32/layer_blend.png", "Header", "Placeholder Settings", "Text", "Configure the placeholder settings here.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Layouts.DeviceEditor.PlaceholderEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Columns", "2", "CellPadding", "4") as Sitecore.Web.UI.WebControls.GridPanel;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "Text", "Key:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      PlaceholderName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel3, "", "ID", idref("PlaceholderName"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "Text", "Settings Item:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "3", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      MetaDataItem = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel6, "", "ID", idref("MetaDataItem"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space7 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel6, "", "Width", "2", "GridPanel.Width", "2px") as Sitecore.Web.UI.HtmlControls.Space;
      BrowseButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel6, "", "ID", idref("BrowseButton"), "Header", "Browse", "Click", "BrowseMetaDataItem", "Margin", "0px 0px 0px 4px") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(PlaceholderName, "ID", idref("PlaceholderName"));
      SetProperty(MetaDataItem, "ID", idref("MetaDataItem"));
      SetProperty(BrowseButton, "ID", idref("BrowseButton"));
    }
  }
}

