using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class LogViewerDetails_a_120 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Border TextPanel;
    public System.Web.UI.Control i3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.HtmlControls.Scrollbox LogViewer;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Reports.LogViewer.LogViewerDetailsForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      TextPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "ID", idref("TextPanel"), "Foreground", "#999999", "Align", "center", "Padding", "16px 0px 16px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      i3 = AddControl("i", "", TextPanel, "");
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), i3, "", "Text", "Open a log file.") as Sitecore.Web.UI.HtmlControls.Literal;
      LogViewer = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), formpage1, "", "ID", idref("LogViewer"), "Padding", "4", "FontSize", "9pt", "FontName", "courier new", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(TextPanel, "ID", idref("TextPanel"));
      SetProperty(LogViewer, "ID", idref("LogViewer"));
    }
  }
}

