using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_SourceOutputEditor_a_113 : Sitecore.Shell.Applications.Install.Controls.SourceOutputEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    
    public string m_MergeOption;
    
    // properties
    public string MergeOption {
      get {
        return StringUtil.GetString(m_MergeOption);
      }
      set {
        m_MergeOption = value;
        
        SetProperty(Options, "MergeOption", StringUtil.GetString(MergeOption,"true"));
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Padding", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border1, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "Text", "How should the installer behave if the package contains items that already exist:", "Style", "font-weight:bold;") as Sitecore.Web.UI.HtmlControls.Literal;
      Options = AddControl("Installer.BehaviourOptionEditor", "", border1, "", "User", "Developer", "MergeOption", StringUtil.GetString(MergeOption,"true")) as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

