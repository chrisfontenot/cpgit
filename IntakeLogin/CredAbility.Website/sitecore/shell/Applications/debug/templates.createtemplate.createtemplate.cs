using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class CreateTemplate_a_87 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl CreateTemplatePicker;
    public Sitecore.Web.UI.HtmlControls.DataContext LocationDataContext;
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public System.Web.UI.Control text2;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform3;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control ul7;
    public System.Web.UI.Control li8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public System.Web.UI.Control li10;
    public Sitecore.Web.UI.HtmlControls.Literal literal11;
    public System.Web.UI.Control li12;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public System.Web.UI.Control li14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.XmlControls.XmlControl Select;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent16;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel17;
    public Sitecore.Web.UI.HtmlControls.Literal literal18;
    public Sitecore.Web.UI.HtmlControls.Edit TemplateName;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.TreePicker BaseTemplate;
    public Sitecore.Web.UI.XmlControls.XmlControl LocationPage;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent20;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox21;
    public Sitecore.Web.UI.WebControls.TreeviewEx Location;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.Literal literal23;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      CreateTemplatePicker = AddControl("TemplateDataContext", "", this, "", "ID", idref("CreateTemplatePicker")) as Sitecore.Web.UI.XmlControls.XmlControl;
      LocationDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), this, "", "ID", idref("LocationDataContext"), "DataViewName", "Master", "Root", "{3C1715FE-6A13-4FCF-845F-DE308BA9741D}", "Filter", "Contains('{E3E2D58C-DF95-4230-ADC9-279924CECE84},{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B},{A87A00B1-E6DB-45AB-8B54-636FEC3B5523},{0437FEE2-44C9-46A6-ABE9-28858D9FEE8C}', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "") as Sitecore.Web.UI.HtmlControls.Script;
      text2 = AddLiteral("      Event.observe(window, \"load\", function() {        $(\"TemplateName\").select();      });    ", "", script1, "");
      wizardform3 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Templates.CreateTemplate.CreateTemplateForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform3, "", "ID", idref("FirstPage"), "Icon", "Applications/32x32/form_blue.png?overlay=People/16x16/sun.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border4, "", "Text", "Welcome to the Create Template wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul7 = AddControl("ul", "", FirstPage, "");
      li8 = AddControl("li", "", ul7, "", "class", "scWizardBullet");
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li8, "", "Text", "Enter a name for the new template.") as Sitecore.Web.UI.HtmlControls.Literal;
      li10 = AddControl("li", "", ul7, "", "class", "scWizardBullet");
      literal11 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li10, "", "Text", "Select a base template.") as Sitecore.Web.UI.HtmlControls.Literal;
      li12 = AddControl("li", "", ul7, "", "class", "scWizardBullet");
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li12, "", "Text", "Select a location.") as Sitecore.Web.UI.HtmlControls.Literal;
      li14 = AddControl("li", "", ul7, "", "class", "scWizardBullet");
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li14, "", "Text", "Create the template.") as Sitecore.Web.UI.HtmlControls.Literal;
      Select = AddControl("WizardFormPage", "", wizardform3, "", "ID", idref("Select"), "Header", "Select name", "Text", "Enter a name for the new template. Select a base template. Click Next to continue.", "Icon", "Applications/32x32/form_blue.png?overlay=People/16x16/sun.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent16 = AddControl("WizardFormIndent", "", Select, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel17 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent16, "", "Width", "100%", "Columns", "2", "CellPadding", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal18 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel17, "", "Text", "Name:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      TemplateName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel17, "", "ID", idref("TemplateName"), "Value", "New template", "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel17, "", "Text", "Base template:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      BaseTemplate = AddControl(new Sitecore.Web.UI.HtmlControls.TreePicker(), gridpanel17, "", "ID", idref("BaseTemplate"), "DataContext", "CreateTemplatePicker", "SelectOnly", "True", "Width", "100%", "GridPanel.Width", "100%", "Value", "{1930BBEB-7805-471A-A3BE-4858AC7CF696}", "AllowNone", "true") as Sitecore.Web.UI.HtmlControls.TreePicker;
      LocationPage = AddControl("WizardFormPage", "", wizardform3, "", "ID", idref("LocationPage"), "Header", "Location", "Text", "Select the location where you want to store the template. Click Next to create the template.", "Icon", "Applications/32x32/form_blue.png?overlay=People/16x16/sun.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent20 = AddControl("WizardFormIndent", "", LocationPage, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox21 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), wizardformindent20, "", "Padding", "0", "Class", "scScrollbox scFixSize", "ContextMenu", "Location.GetContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Location = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox21, "", "ID", idref("Location"), "DataContext", "LocationDataContext") as Sitecore.Web.UI.WebControls.TreeviewEx;
      LastPage = AddControl("WizardFormLastPage", "", wizardform3, "", "ID", idref("LastPage"), "Icon", "Applications/32x32/form_blue.png?overlay=People/16x16/sun.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal23 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border22, "", "Text", "The template has been created successfully. Click Finish to close the Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(CreateTemplatePicker, "ID", idref("CreateTemplatePicker"));
      SetProperty(LocationDataContext, "ID", idref("LocationDataContext"));
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Select, "ID", idref("Select"));
      SetProperty(TemplateName, "ID", idref("TemplateName"));
      SetProperty(BaseTemplate, "ID", idref("BaseTemplate"));
      SetProperty(LocationPage, "ID", idref("LocationPage"));
      SetProperty(Location, "ID", idref("Location"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

