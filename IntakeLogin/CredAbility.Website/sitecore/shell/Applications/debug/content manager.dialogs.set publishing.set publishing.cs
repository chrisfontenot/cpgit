using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SetPublishing_a_128 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public System.Web.UI.Control text4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border Warning;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Tabstrip Tabs;
    public Sitecore.Web.UI.HtmlControls.Tab VersionsTab;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Versions;
    public Sitecore.Web.UI.HtmlControls.Tab ItemTab;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Checkbox NeverPublish;
    public Sitecore.Web.UI.HtmlControls.Border PublishPanel;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel14;
    public Sitecore.Web.UI.HtmlControls.Space space15;
    public Sitecore.Web.UI.HtmlControls.Border border16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.DateTimePicker Publish;
    public Sitecore.Web.UI.HtmlControls.Space space18;
    public Sitecore.Web.UI.HtmlControls.Border border19;
    public Sitecore.Web.UI.HtmlControls.Literal literal20;
    public Sitecore.Web.UI.HtmlControls.DateTimePicker Unpublish;
    public Sitecore.Web.UI.HtmlControls.Tab TargetsTab;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Literal literal22;
    public Sitecore.Web.UI.HtmlControls.Border border23;
    public Sitecore.Web.UI.HtmlControls.Groupbox PublishingTargetsPanel;
    public Sitecore.Web.UI.HtmlControls.Border PublishingTargets;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/earth_time.png", "Header", "Publishing Settings", "Text", "Configure how the current item is published.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.SetPublishing.SetPublishingForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "Warning") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text4 = AddLiteral("        .scWarning {          background: #ffffe4;          border: 1px solid #c9c9c9;          border-left:none;          border-right:none;          padding: 4px 2px 4px 4px;          margin: 4px 0px 12px 0px;          font-weight: bold;        }                #Versions {          padding: 4px;        }                .ff #Versions {          padding: 0;        }                .ff #Versions > table {          margin: 4px;          width: auto;        }      ", "", stylesheet3, "");
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      Warning = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "ID", idref("Warning"), "Visible", "false", "class", "scWarning") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), Warning, "", "Height", "16", "Width", "16", "style", "vertical-align:middle; margin-right: 4px", "Src", "Applications/16x16/warning.png") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Warning, "", "Text", "You cannot change publish restrictions as you cannot modify the item.") as Sitecore.Web.UI.HtmlControls.Literal;
      Tabs = AddControl(new Sitecore.Web.UI.HtmlControls.Tabstrip(), gridpanel5, "", "ID", idref("Tabs"), "Height", "100%", "Width", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Tabstrip;
      VersionsTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("VersionsTab"), "Header", "Versions", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Tab;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), VersionsTab, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "Padding", "8px 8px 4px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Restrict the publication of one or more versions of the current item. This only affects the current language.") as Sitecore.Web.UI.HtmlControls.Literal;
      Versions = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel8, "", "ID", idref("Versions"), "Height", "100%", "Border", "none", "Padding", "", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ItemTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("ItemTab"), "Header", "Item") as Sitecore.Web.UI.HtmlControls.Tab;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), ItemTab, "", "Padding", "8px 8px 4px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border11, "", "Text", "Restrict the publication of the current item. This affects every version in every language.") as Sitecore.Web.UI.HtmlControls.Literal;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), ItemTab, "", "Padding", "8px 8px 4px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      NeverPublish = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border13, "", "ID", idref("NeverPublish"), "Header", "Publishable", "Click", "SetNeverPublish") as Sitecore.Web.UI.HtmlControls.Checkbox;
      PublishPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), ItemTab, "", "ID", idref("PublishPanel")) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel14 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), PublishPanel, "", "Width", "100%", "Columns", "3", "CellPadding", "4") as Sitecore.Web.UI.WebControls.GridPanel;
      space15 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel14, "", "Width", "32") as Sitecore.Web.UI.HtmlControls.Space;
      border16 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel14, "", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border16, "", "Text", "Publishable From:") as Sitecore.Web.UI.HtmlControls.Literal;
      Publish = AddControl(new Sitecore.Web.UI.HtmlControls.DateTimePicker(), gridpanel14, "", "ID", idref("Publish"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.DateTimePicker;
      space18 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel14, "", "Width", "32") as Sitecore.Web.UI.HtmlControls.Space;
      border19 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel14, "", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      literal20 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border19, "", "Text", "Publishable To:") as Sitecore.Web.UI.HtmlControls.Literal;
      Unpublish = AddControl(new Sitecore.Web.UI.HtmlControls.DateTimePicker(), gridpanel14, "", "ID", idref("Unpublish"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.DateTimePicker;
      TargetsTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "ID", idref("TargetsTab"), "Header", "Targets") as Sitecore.Web.UI.HtmlControls.Tab;
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), TargetsTab, "", "Padding", "8px 8px 4px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      literal22 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border21, "", "Text", "Indicate the publication destinations for the current item. This affects every version in every language.") as Sitecore.Web.UI.HtmlControls.Literal;
      border23 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), TargetsTab, "", "Padding", "8px") as Sitecore.Web.UI.HtmlControls.Border;
      PublishingTargetsPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), border23, "", "ID", idref("PublishingTargetsPanel"), "Header", "Publishing targets") as Sitecore.Web.UI.HtmlControls.Groupbox;
      PublishingTargets = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), PublishingTargetsPanel, "", "ID", idref("PublishingTargets"), "Padding", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Warning, "ID", idref("Warning"));
      SetProperty(Tabs, "ID", idref("Tabs"));
      SetProperty(VersionsTab, "ID", idref("VersionsTab"));
      SetProperty(Versions, "ID", idref("Versions"));
      SetProperty(ItemTab, "ID", idref("ItemTab"));
      SetProperty(NeverPublish, "ID", idref("NeverPublish"));
      SetProperty(PublishPanel, "ID", idref("PublishPanel"));
      SetProperty(Publish, "ID", idref("Publish"));
      SetProperty(Unpublish, "ID", idref("Unpublish"));
      SetProperty(TargetsTab, "ID", idref("TargetsTab"));
      SetProperty(PublishingTargetsPanel, "ID", idref("PublishingTargetsPanel"));
      SetProperty(PublishingTargets, "ID", idref("PublishingTargets"));
    }
  }
}

