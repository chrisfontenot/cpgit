using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Run_a_125 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage5;
    public Sitecore.Web.UI.HtmlControls.Space space6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.SubmittableEdit Program;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.Button button14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Button button16;
    public Sitecore.Web.UI.HtmlControls.Button button17;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Submittable", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Tools.Run.RunForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Class", "scBackground", "Width", "100%", "Height", "100%", "CellPadding", "8") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "3") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage5 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel4, "", "Src", "Software/32x32/application_run.png", "Width", "32", "Height", "32") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space6 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel4, "", "Width", "8") as Sitecore.Web.UI.HtmlControls.Space;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", "Type the name of a application, folder, document or Internet resource, and Sitecore will open it for you.") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "5", "GridPanel.Valign", "top") as Sitecore.Web.UI.WebControls.GridPanel;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel9, "", "Text", "Open:") as Sitecore.Web.UI.HtmlControls.Literal;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel9, "", "Width", "8") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel9, "", "GridPanel.Width", "100%", "Fixed", "true", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Program = AddControl(new Sitecore.Web.UI.HtmlControls.SubmittableEdit(), gridpanel12, "", "ID", idref("Program"), "Width", "100%", "Submit", "run:ok") as Sitecore.Web.UI.HtmlControls.SubmittableEdit;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel9, "", "Width", "2") as Sitecore.Web.UI.HtmlControls.Space;
      button14 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel9, "", "Header", "Browse", "Click", "run:browse", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Button;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      button16 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border15, "", "Header", "OK", "Click", "run:ok") as Sitecore.Web.UI.HtmlControls.Button;
      button17 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border15, "", "Header", "Cancel", "Click", "run:cancel") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Program, "ID", idref("Program"));
    }
  }
}

