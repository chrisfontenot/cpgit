using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SetMasters_a_90 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.VerticalTabstrip verticaltabstrip6;
    public Sitecore.Web.UI.HtmlControls.TabSection tabsection7;
    public Sitecore.Web.UI.HtmlControls.Tab Templates;
    public System.Web.UI.Control TreeList;
    public Sitecore.Web.UI.HtmlControls.Tab Insertion;
    public System.Web.UI.Control InsertRules;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/form_yellow_edit.png", "Header", "Insert Options", "Text", "Assign the insert options for the current item.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "SetBaseTemplates") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scContentControl {          height:100%;        }        .scContentControlMultilistBox {          height:100%;        }        .scDialogContentContainer {          padding: 0px;        }              ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.SetMasters.SetMastersForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Cellpadding", "0", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      verticaltabstrip6 = AddControl(new Sitecore.Web.UI.HtmlControls.VerticalTabstrip(), gridpanel5, "", "GridPanel.Height", "100%", "Height", "100%", "FixSize", "true") as Sitecore.Web.UI.HtmlControls.VerticalTabstrip;
      tabsection7 = AddControl(new Sitecore.Web.UI.HtmlControls.TabSection(), verticaltabstrip6, "", "Header", "Options") as Sitecore.Web.UI.HtmlControls.TabSection;
      Templates = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), verticaltabstrip6, "", "ID", idref("Templates"), "Header", "Templates") as Sitecore.Web.UI.HtmlControls.Tab;
      TreeList = AddControl("content:TreeList", "", Templates, "", "ID", idref("TreeList"), "Source", "/sitecore/templates", "Activation", "false", "Style", "background:#e9e9e9;border:none;padding: 0px 4px 4px 4px;font:8pt tahoma", "IncludeTemplatesForSelection", "Template,Master,Command Template,Branch", "IncludeTemplatesForDisplay", "Master folder,Master,Template folder,Template,Folder,Node,Main section,Command Template,Branch,Branch Folder", "ExcludeItemsForDisplay", "{FC118F80-8638-483C-A810-214E91F69CA4},{0DE95AE4-41AB-4D01-9EB0-67441B7C2450},{EB2E4FFD-2761-4653-B052-26A64D385227},{3D6658D8-A0BF-4E75-B3E2-D050FABCF4E1},{13D6D6C6-C50B-4BBD-B331-2B04F1A58F21}");
      Insertion = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), verticaltabstrip6, "", "ID", idref("Insertion"), "Header", "Insert Rules") as Sitecore.Web.UI.HtmlControls.Tab;
      InsertRules = AddControl("content:TreeList", "", Insertion, "", "ID", idref("InsertRules"), "Source", "/sitecore/system/Settings/Insert Rules", "Activation", "false", "Style", "background:#e9e9e9;border:none;padding: 0px 4px 4px 4px;font:8pt tahoma", "IncludeTemplatesForSelection", "Insert Rule", "IncludeTemplatesForDisplay", "Folder,Insert Rule", "GridPanel.Height", "34%");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Templates, "ID", idref("Templates"));
      SetProperty(TreeList, "ID", idref("TreeList"));
      SetProperty(Insertion, "ID", idref("Insertion"));
      SetProperty(InsertRules, "ID", idref("InsertRules"));
    }
  }
}

