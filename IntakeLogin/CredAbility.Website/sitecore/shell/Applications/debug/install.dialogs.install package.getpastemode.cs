using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_GetPasteMode_a_95 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.WebControls.GridPanel PnlCollision;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public System.Web.UI.Control br8;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Edit FldItemID;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Edit FldItemPath;
    public System.Web.UI.Control br13;
    public System.Web.UI.Control br14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public System.Web.UI.Control br16;
    public System.Web.UI.Control br17;
    public Sitecore.Web.UI.XmlControls.XmlControl OptionEditor;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Button button19;
    public Sitecore.Web.UI.HtmlControls.Button button20;
    public Sitecore.Web.UI.HtmlControls.Button button21;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "threedface") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Install.Dialogs.InstallPackage.GetPasteModeForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "2", "Width", "100%", "GridPanel.Height", "100%", "GridPanel.Valign", "top") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage5 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel4, "", "Src", "Applications/32x32/Warning.png", "Width", "32", "Height", "32", "Margin", "8 0 8 16", "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Padding", "8 16 8 8", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      PnlCollision = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "ID", idref("PnlCollision")) as Sitecore.Web.UI.WebControls.GridPanel;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), PnlCollision, "", "Text", "Item being installed already exists in database.") as Sitecore.Web.UI.HtmlControls.Literal;
      br8 = AddControl("br", "", PnlCollision, "");
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), PnlCollision, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel9, "", "Text", "ID: ") as Sitecore.Web.UI.HtmlControls.Literal;
      FldItemID = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel9, "", "ID", idref("FldItemID"), "ReadOnly", "true", "Disabled", "False", "HideFocus", "hidefocus", "Style", "border-style:none;background:none;width:300px") as Sitecore.Web.UI.HtmlControls.Edit;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel11, "", "Text", "Path:") as Sitecore.Web.UI.HtmlControls.Literal;
      FldItemPath = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel11, "", "ID", idref("FldItemPath"), "ReadOnly", "true", "Disabled", "False", "HideFocus", "hidefocus", "Style", "border-style:none;background:none;width:300px") as Sitecore.Web.UI.HtmlControls.Edit;
      br13 = AddControl("br", "", border6, "");
      br14 = AddControl("br", "", border6, "");
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "Text", "Please choose one of the following options:") as Sitecore.Web.UI.HtmlControls.Literal;
      br16 = AddControl("br", "", border6, "");
      br17 = AddControl("br", "", border6, "");
      OptionEditor = AddControl("Installer.BehaviourOptionEditor", "", border6, "", "ID", idref("OptionEditor"), "User", "EndUser") as Sitecore.Web.UI.XmlControls.XmlControl;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Align", "Center", "Padding", "16 8 8 8") as Sitecore.Web.UI.HtmlControls.Border;
      button19 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Click", "Apply", "Header", "Apply", "Margin", "0 4 0 0") as Sitecore.Web.UI.HtmlControls.Button;
      button20 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Click", "ApplyToAll", "Header", "Apply to all", "Margin", "0 4 0 0") as Sitecore.Web.UI.HtmlControls.Button;
      button21 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Click", "Cancel", "Header", "Abort", "Margin", "0 4 0 0") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(PnlCollision, "ID", idref("PnlCollision"));
      SetProperty(FldItemID, "ID", idref("FldItemID"));
      SetProperty(FldItemPath, "ID", idref("FldItemPath"));
      SetProperty(OptionEditor, "ID", idref("OptionEditor"));
    }
  }
}

