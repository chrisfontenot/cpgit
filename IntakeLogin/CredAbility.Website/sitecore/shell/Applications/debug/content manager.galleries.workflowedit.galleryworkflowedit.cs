using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_WorkflowEdit_a_112 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet1;
    public System.Web.UI.Control text2;
    public Sitecore.Web.UI.XmlControls.XmlControl gallery3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.HtmlControls.Menu Options;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      stylesheet1 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Key", "Gallery.WorkflowEdit") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text2 = AddLiteral("      body {      height:auto;      }    ", "", stylesheet1, "");
      gallery3 = AddControl("Gallery", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), gallery3, "", "Type", "Sitecore.Shell.Applications.ContentManager.Galleries.WorkflowEdit.GalleryWorkflowEditForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      Options = AddControl(new Sitecore.Web.UI.HtmlControls.Menu(), gallery3, "", "ID", idref("Options"), "Border", "none") as Sitecore.Web.UI.HtmlControls.Menu;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Options, "ID", idref("Options"));
    }
  }
}

