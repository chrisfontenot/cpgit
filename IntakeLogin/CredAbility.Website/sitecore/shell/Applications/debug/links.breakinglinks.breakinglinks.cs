using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class BreakingLinks_a_108 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Border SelectActionPage;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Radiobutton RemoveButton;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Radiobutton RelinkButton;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Radiobutton BreakButton;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Button button8;
    public Sitecore.Web.UI.HtmlControls.Border SelectItemPage;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox11;
    public Sitecore.Web.UI.WebControls.TreeviewEx Link;
    public Sitecore.Web.UI.HtmlControls.Border ExecutingPage;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage14;
    public Sitecore.Web.UI.HtmlControls.Space space15;
    public Sitecore.Web.UI.HtmlControls.Border border16;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage17;
    public Sitecore.Web.UI.HtmlControls.Space space18;
    public Sitecore.Web.UI.HtmlControls.Border Text;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.Space space20;
    public Sitecore.Web.UI.HtmlControls.Border Status;
    public System.Web.UI.Control text21;
    public Sitecore.Web.UI.HtmlControls.Border FailedPage;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel22;
    public Sitecore.Web.UI.HtmlControls.Memo ErrorText;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/link_delete.png", "Header", "Breaking Links", "Text", "Other items link to this item or subitems. The links should be updated to avoid broken links.", "OKButton", "Continue") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.BreakingLinks.BreakingLinksForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      SelectActionPage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "", "ID", idref("SelectActionPage")) as Sitecore.Web.UI.HtmlControls.Border;
      groupbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), SelectActionPage, "", "Header", "Action", "Padding", "2px") as Sitecore.Web.UI.HtmlControls.Groupbox;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), groupbox3, "") as Sitecore.Web.UI.HtmlControls.Border;
      RemoveButton = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border4, "", "ID", idref("RemoveButton"), "Name", "Action", "Value", "Remove", "Header", "Remove Links", "Checked", "true") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), groupbox3, "") as Sitecore.Web.UI.HtmlControls.Border;
      RelinkButton = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border5, "", "ID", idref("RelinkButton"), "Name", "Action", "Value", "Relink", "Header", "Link to Another Item") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), groupbox3, "") as Sitecore.Web.UI.HtmlControls.Border;
      BreakButton = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border6, "", "ID", idref("BreakButton"), "Name", "Action", "Value", "Break", "Header", "Leave Links - the Links Will Appear in the Broken Links Report") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), SelectActionPage, "", "Margin", "8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button8 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "Header", "Edit Links", "Click", "EditLinks") as Sitecore.Web.UI.HtmlControls.Button;
      SelectItemPage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "", "ID", idref("SelectItemPage"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), SelectItemPage, "", "ID", idref("DataContext"), "Root", "/", "Folder", "/sitecore/content", "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), SelectItemPage, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel9, "", "Text", "Select Another Item.") as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox11 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "Padding", "0px") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Link = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox11, "", "ID", idref("Link"), "DataContext", "DataContext") as Sitecore.Web.UI.WebControls.TreeviewEx;
      ExecutingPage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "", "ID", idref("ExecutingPage"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), ExecutingPage, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel12, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage14 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel12, "", "Src", "Network/48x48/Link_new.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space15 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel12, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border16 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage17 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border16, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space18 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel12, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "ID", idref("Text"), "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Text, "", "Text", "Updating Links...") as Sitecore.Web.UI.HtmlControls.Literal;
      space20 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel12, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      Status = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "ID", idref("Status"), "Align", "center", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      text21 = AddLiteral("                       ", "", Status, "");
      FailedPage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formdialog1, "", "ID", idref("FailedPage"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel22 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), FailedPage, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      ErrorText = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel22, "", "ID", idref("ErrorText"), "ReadOnly", "true", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(SelectActionPage, "ID", idref("SelectActionPage"));
      SetProperty(RemoveButton, "ID", idref("RemoveButton"));
      SetProperty(RelinkButton, "ID", idref("RelinkButton"));
      SetProperty(BreakButton, "ID", idref("BreakButton"));
      SetProperty(SelectItemPage, "ID", idref("SelectItemPage"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Link, "ID", idref("Link"));
      SetProperty(ExecutingPage, "ID", idref("ExecutingPage"));
      SetProperty(Text, "ID", idref("Text"));
      SetProperty(Status, "ID", idref("Status"));
      SetProperty(FailedPage, "ID", idref("FailedPage"));
      SetProperty(ErrorText, "ID", idref("ErrorText"));
    }
  }
}

