using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Sitecore_Shell_Applications_Dialogs_SelectRendering_a_121 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Items;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.HtmlControls.Border Help;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public System.Web.UI.Control i4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Border PlaceHolderNameBorder;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Space space8;
    public Sitecore.Web.UI.HtmlControls.Edit PlaceholderName;
    public Sitecore.Web.UI.HtmlControls.Border OpenPropertiesBorder;
    public Sitecore.Web.UI.HtmlControls.Checkbox OpenProperties;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "Core/32x32/open_document.png", "Header", "Open Item", "Text", "Select the item that you wish to open. Then click the Open button.", "OKButton", "Open") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Dialogs.SelectRendering.SelectRenderingForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), Dialog, "", "ID", idref("DataContext"), "Root", "/") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      Items = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "ID", idref("Items"), "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), Items, "", "ID", idref("Treeview"), "DataContext", "DataContext", "ShowRoot", "true", "DblClick", "OK_Click", "Click", "Treeview_Click", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      Help = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "ID", idref("Help"), "Height", "48px", "style", "border:1px inset", "Background", "white", "Margin", "4px 0px 0px 0px", "Padding", "2px") as Sitecore.Web.UI.HtmlControls.Border;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Help, "", "style", "color:#999999", "align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      i4 = AddControl("i", "", border3, "");
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), i4, "", "Text", "Please select a rendering.") as Sitecore.Web.UI.HtmlControls.Literal;
      PlaceHolderNameBorder = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "ID", idref("PlaceHolderNameBorder"), "Visible", "false", "style", "padding:12px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), PlaceHolderNameBorder, "", "Columns", "3", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel6, "", "Text", "Add to Placeholder: ", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      space8 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel6, "", "Width", "4") as Sitecore.Web.UI.HtmlControls.Space;
      PlaceholderName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel6, "", "ID", idref("PlaceholderName"), "Name", "PlaceholderName", "GridPanel.Width", "100%", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      OpenPropertiesBorder = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "ID", idref("OpenPropertiesBorder"), "Visible", "false", "style", "padding:8px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      OpenProperties = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), OpenPropertiesBorder, "", "ID", idref("OpenProperties"), "Header", "Open Properties after this dialog closes") as Sitecore.Web.UI.HtmlControls.Checkbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Items, "ID", idref("Items"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(Help, "ID", idref("Help"));
      SetProperty(PlaceHolderNameBorder, "ID", idref("PlaceHolderNameBorder"));
      SetProperty(PlaceholderName, "ID", idref("PlaceholderName"));
      SetProperty(OpenPropertiesBorder, "ID", idref("OpenPropertiesBorder"));
      SetProperty(OpenProperties, "ID", idref("OpenProperties"));
    }
  }
}

