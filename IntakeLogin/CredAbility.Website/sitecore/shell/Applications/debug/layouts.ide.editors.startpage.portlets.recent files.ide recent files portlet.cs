using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_RecentFilesPortlet_a_116 : Sitecore.Shell.Applications.Layouts.IDE.Editors.Startpage.Portlets.RecentFiles.IDERecentFilePortletXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.XmlControls.XmlControl RecentFilesOpen;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Portlet = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "") as Sitecore.Web.UI.HtmlControls.Border;
      Window = AddControl("DefaultPortletWindow", "", Portlet, "", "Header", "Recent Files", "Icon", "Applications/16x16/document_time.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      Body = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Window, "") as Sitecore.Web.UI.HtmlControls.Border;
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Window, "", "Style", "border-top:1px solid #b6b8cb; margin:4px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      RecentFilesOpen = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("RecentFilesOpen"), "Header", "Open", "Click", "RecentFilesPortlet.Click(\"ide:open\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Window, "", "Class", "PortletOptions") as Sitecore.Web.UI.HtmlControls.Border;
      ClearButton = AddControl("PortalToolbutton", "", border2, "", "Header", "Clear") as Sitecore.Web.UI.XmlControls.XmlControl;
      RefreshButton = AddControl("PortalToolbutton", "", border2, "", "Header", "Refresh") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(RecentFilesOpen, "ID", idref("RecentFilesOpen"));
    }
  }
}

