using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_Versions_Option_a_101 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    
    public string m_Click;
    public string m_Number;
    public string m_Header;
    
    // properties
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(border1, "Click", Click);
      }
    }
    
    public string Number {
      get {
        return StringUtil.GetString(m_Number);
      }
      set {
        m_Number = value;
        
        SetProperty(literal4, "Text", Number);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal7, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "RollOver", "true", "Class", "scMenuPanelItem", "Click", Click) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Class", "scClickFont", "NoWrap", "true", "FontName", "Ariel", "FontSize", "16px", "FontBold", "true", "Height", "25px", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border3, "", "Text", Number) as Sitecore.Web.UI.HtmlControls.Literal;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border5, "", "Padding", "0px 0px 0px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

