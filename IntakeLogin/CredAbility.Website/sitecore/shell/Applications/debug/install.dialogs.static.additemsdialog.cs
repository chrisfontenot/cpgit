using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_AddItemsDialog_a_116 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Combobox Databases;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox8;
    public Sitecore.Web.UI.HtmlControls.DataTreeview Treeview;
    public Sitecore.Web.UI.HtmlControls.TreeHeader treeheader9;
    public Sitecore.Web.UI.HtmlControls.TreeHeaderItem treeheaderitem10;
    public Sitecore.Web.UI.HtmlControls.TreeHeaderItem treeheaderitem11;
    public Sitecore.Web.UI.HtmlControls.Space space12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton14;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton15;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton16;
    public Sitecore.Web.UI.HtmlControls.Space space17;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ItemListBox;
    public Sitecore.Web.UI.HtmlControls.Listview ItemList;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader18;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem19;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/24x24/folder_cubes.png", "Header", "Add Items", "Text", "Add the items that you want to include in the package.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Install.Dialogs.AddItemsDialog,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("DataContext"), "Root", "{11111111-1111-1111-1111-111111111111}", "filter", "@@virtual=0") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "0 0 8 0", "GridPanel.Height", "2em") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border4, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "Database: ") as Sitecore.Web.UI.HtmlControls.Literal;
      Databases = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel5, "", "ID", idref("Databases"), "Change", "ChangeDatabase", "Width", "100%", "Padding", "0 0 0 4", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Combobox;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox8 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel7, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.HtmlControls.DataTreeview(), scrollbox8, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Root", "true") as Sitecore.Web.UI.HtmlControls.DataTreeview;
      treeheader9 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeader(), Treeview, "") as Sitecore.Web.UI.HtmlControls.TreeHeader;
      treeheaderitem10 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeaderItem(), treeheader9, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.TreeHeaderItem;
      treeheaderitem11 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeaderItem(), treeheader9, "", "Name", "__Short description", "Header", "Description") as Sitecore.Web.UI.HtmlControls.TreeHeaderItem;
      space12 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel7, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      toolbutton14 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Style", "float:right", "Icon", "Applications/24x24/delete2.png", "Header", "Remove", "ToolTip", "Removes entry from selection list", "Click", "Remove(\"\")") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton15 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Icon", "Software/24x24/branch.png", "Header", "Add with Subitems", "ToolTip", "Adds item with subitems", "Click", "AddTree") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton16 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Icon", "Software/24x24/element.png", "Header", "Add Item", "ToolTip", "Add Item", "Click", "AddItem") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space17 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel7, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      ItemListBox = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel7, "", "ID", idref("ItemListBox"), "Width", "100%", "Height", "150px", "ContextMenu", "ListContextMenu()") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ItemList = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ItemListBox, "", "ID", idref("ItemList"), "Width", "100%", "View", "Details") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), ItemList, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem19 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader18, "", "Header", "Selected items:") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Databases, "ID", idref("Databases"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(ItemListBox, "ID", idref("ItemListBox"));
      SetProperty(ItemList, "ID", idref("ItemList"));
    }
  }
}

