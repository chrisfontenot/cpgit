using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class DeviceRendering_a_142 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public System.Web.UI.Control b5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control br7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    
    public string m_ID;
    public string m_Click;
    public string m_DblClick;
    public string m_Background;
    public string m_Icon;
    public string m_Header;
    public string m_Placeholder;
    
    // properties
    public new string ID {
      get {
        return StringUtil.GetString(m_ID);
      }
      set {
        m_ID = value;
        
        SetProperty(border1, "ID", ID);
      }
    }
    
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(border1, "Click", Click);
      }
    }
    
    public string DblClick {
      get {
        return StringUtil.GetString(m_DblClick);
      }
      set {
        m_DblClick = value;
        
        SetProperty(border1, "DblClick", DblClick);
      }
    }
    
    public new string Background {
      get {
        return StringUtil.GetString(m_Background);
      }
      set {
        m_Background = value;
        
        SetProperty(border1, "Background", MainUtil.GetString(Background, "transparent"));
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage3, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal6, "Text", Header);
      }
    }
    
    public string Placeholder {
      get {
        return StringUtil.GetString(m_Placeholder);
      }
      set {
        m_Placeholder = value;
        
        SetProperty(literal8, "Text", Placeholder);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "ID", ID, "Click", Click, "DblClick", DblClick, "Background", MainUtil.GetString(Background, "transparent")) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage3 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel2, "", "Src", Icon, "Width", "32", "Height", "32", "GridPanel.Style", "padding:2px 0px 2px 2px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "GridPanel.Width", "100%", "GridPanel.Style", "padding:2px 2px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      b5 = AddControl("b", "", border4, "");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b5, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      br7 = AddControl("br", "", border4, "");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border4, "", "Text", Placeholder) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

