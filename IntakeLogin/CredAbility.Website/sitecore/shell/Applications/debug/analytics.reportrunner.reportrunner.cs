using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Sitecore_Shell_Applications_Analytics_ReportRunner_a_89 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public System.Web.UI.Control script3;
    public System.Web.UI.Control text4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox6;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl AnalyticsReportsLeft;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Border Loading;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public Sitecore.Web.UI.HtmlControls.Frame ReportFrame;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Analytics.ReportRunner.ReportRunnerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext"), "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      script3 = AddControl("script", "", formpage1, "", "type", "text/javascript");
      text4 = AddLiteral("        function scDesign(enable) {        var display = enable ? \"none\" : \"\";        $('TreeViewPanel').style.display = display;        $('SplitterPanel').style.display = display;        }        function scLoaded() {        $('Loading').hide();        }        function scLoading() {        $('Loading').show();        }      ", "", script3, "");
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Columns", "3", "CellPadding", "0", "Style", "table-layout:fixed;background:white") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox6 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "GridPanel.ID", "TreeViewPanel", "Width", "100%", "Height", "100%", "GridPanel.Width", "250px", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox6, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Click", "Refresh", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      AnalyticsReportsLeft = AddControl("VSplitter", "", gridpanel5, "", "ID", idref("AnalyticsReportsLeft"), "GridPanel.Width", "4", "Target", "left", "GridPanel.Style", "background:#d2d5da;", "GridPanel.ID", "SplitterPanel") as Sitecore.Web.UI.XmlControls.XmlControl;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      Loading = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "id", idref("Loading"), "Style", "position:absolute;width:100%;height:100%;background:white;z-index:1;padding:64px 0px 0px 0px", "align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Loading, "") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage9 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border8, "", "Src", "Business/48x48/column-chart.png", "Width", "48", "Height", "48") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Loading, "", "Padding", "16px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage11 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border10, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Loading, "", "Padding", "16px 0px 0px 0px;font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border12, "", "Text", "Processing Report...") as Sitecore.Web.UI.HtmlControls.Literal;
      ReportFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), border7, "", "id", idref("ReportFrame"), "SourceUri", "/sitecore/shell/~/xaml/Sitecore.Shell.Applications.Analytics.ReportRunner.NoReport.aspx", "Width", "100%", "Height", "100%", "OnLoad", "javascript:scLoaded()") as Sitecore.Web.UI.HtmlControls.Frame;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(AnalyticsReportsLeft, "ID", idref("AnalyticsReportsLeft"));
      SetProperty(Loading, "id", idref("Loading"));
      SetProperty(ReportFrame, "id", idref("ReportFrame"));
    }
  }
}

