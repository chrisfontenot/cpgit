using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_SolutionExplorer_a_125 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.XmlControls.XmlControl dockwindowchrome4;
    public Sitecore.Web.UI.HtmlControls.DataContext ContentEditorDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton8;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox12;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Submittable", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Windows.SolutionExplorer.SolutionExplorerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      dockwindowchrome4 = AddControl("DockWindowChrome", "", formpage1, "", "Header", "Solution Explorer") as Sitecore.Web.UI.XmlControls.XmlControl;
      ContentEditorDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), dockwindowchrome4, "", "ID", idref("ContentEditorDataContext"), "DataViewName", "Master", "Root", "/sitecore/layout") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), dockwindowchrome4, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "") as Sitecore.Web.UI.HtmlControls.Border;
      toolbar7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), border6, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      toolbutton8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "Icon", "Core3/16x16/open_document.png", "IconSize", "id16x16", "Click", "Open", "ToolTip", "Open the selected item") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton9 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "Icon", "Applications/16x16/refresh.png", "IconSize", "id16x16", "Click", "Refresh", "ToolTip", "Refresh") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border6, "", "Background", "#aca899") as Sitecore.Web.UI.HtmlControls.Border;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border10, "") as Sitecore.Web.UI.HtmlControls.Space;
      scrollbox12 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Width", "100%", "Height", "100%", "Padding", "0", "ContextMenu", "Treeview.GetContextMenu", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox12, "", "ID", idref("Treeview"), "DataContext", "ContentEditorDataContext", "ShowRoot", "false", "DblClick", "open") as Sitecore.Web.UI.WebControls.TreeviewEx;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(ContentEditorDataContext, "ID", idref("ContentEditorDataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
    }
  }
}

