using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class RebuildSearchIndex_a_68 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public System.Web.UI.Control ul5;
    public System.Web.UI.Control li6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public System.Web.UI.Control li8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.XmlControls.XmlControl Database;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformpadding10;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox11;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox12;
    public Sitecore.Web.UI.HtmlControls.Border Indexes;
    public Sitecore.Web.UI.XmlControls.XmlControl Rebuilding;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel13;
    public Sitecore.Web.UI.HtmlControls.Space space14;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.HtmlControls.Border border17;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage18;
    public Sitecore.Web.UI.HtmlControls.Space space19;
    public Sitecore.Web.UI.HtmlControls.Border Text;
    public Sitecore.Web.UI.HtmlControls.Literal literal20;
    public Sitecore.Web.UI.HtmlControls.Space space21;
    public Sitecore.Web.UI.HtmlControls.Border Status;
    public System.Web.UI.Control text22;
    public Sitecore.Web.UI.XmlControls.XmlControl Retry;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformpadding23;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel24;
    public Sitecore.Web.UI.HtmlControls.Memo ErrorText;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel25;
    public Sitecore.Web.UI.HtmlControls.Border border26;
    public Sitecore.Web.UI.HtmlControls.Literal literal27;
    public Sitecore.Web.UI.HtmlControls.Border border28;
    public Sitecore.Web.UI.HtmlControls.Literal literal29;
    public Sitecore.Web.UI.HtmlControls.Memo ResultText;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "Application", "Search/RebuildSearchIndex", "CodeBeside", "Sitecore.Shell.Applications.Search.RebuildSearchIndex.RebuildSearchIndexForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Applications/32x32/find.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "Text", "Welcome to the Rebuild Search Index Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul5 = AddControl("ul", "", FirstPage, "");
      li6 = AddControl("li", "", ul5, "", "class", "scWizardBullet");
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li6, "", "Text", "Select a search index.") as Sitecore.Web.UI.HtmlControls.Literal;
      li8 = AddControl("li", "", ul5, "", "class", "scWizardBullet");
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li8, "", "Text", "Rebuild the search index.") as Sitecore.Web.UI.HtmlControls.Literal;
      Database = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Database"), "Header", "Select Search Index", "Text", "Select the search index that you want to rebuild. You can rebuild more than one index at a time. Click Rebuild to rebuild the search index.", "Icon", "Applications/32x32/find.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformpadding10 = AddControl("WizardFormPadding", "", Database, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox11 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), wizardformpadding10, "", "Border", "none", "Background", "transparent") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      groupbox12 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), scrollbox11, "", "Header", "Rebuild Search Index", "Margin", "0px 0px 16px 0px") as Sitecore.Web.UI.HtmlControls.Groupbox;
      Indexes = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), groupbox12, "", "ID", idref("Indexes"), "Padding", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      Rebuilding = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Rebuilding"), "Header", "Rebuilding", "Text", "Please wait while rebuilding...", "Icon", "Applications/32x32/find.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel13 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Rebuilding, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space14 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel13, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage15 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel13, "", "Src", "Applications/48x48/find.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel13, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border17 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel13, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage18 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border17, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space19 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel13, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel13, "", "ID", idref("Text"), "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal20 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Text, "", "Text", "Rebuilding...") as Sitecore.Web.UI.HtmlControls.Literal;
      space21 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel13, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      Status = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel13, "", "ID", idref("Status"), "Align", "center", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      text22 = AddLiteral("                       ", "", Status, "");
      Retry = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Retry"), "Header", "Error", "Text", "An error occurred while rebuilding the search index.", "Icon", "Applications/32x32/find.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformpadding23 = AddControl("WizardFormPadding", "", Retry, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel24 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformpadding23, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      ErrorText = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel24, "", "ID", idref("ErrorText"), "ReadOnly", "true", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Applications/32x32/find.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel25 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), LastPage, "", "Cellpadding", "2", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border26 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel25, "", "GridPanel.Style", "padding:0px 0px 16px 0px; font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal27 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border26, "", "Text", "The wizard has completed. Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      border28 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel25, "", "GridPanel.Style", "padding:0px 0px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal29 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border28, "", "Text", "Result:") as Sitecore.Web.UI.HtmlControls.Literal;
      ResultText = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel25, "", "ID", idref("ResultText"), "ReadOnly", "true", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Database, "ID", idref("Database"));
      SetProperty(Indexes, "ID", idref("Indexes"));
      SetProperty(Rebuilding, "ID", idref("Rebuilding"));
      SetProperty(Text, "ID", idref("Text"));
      SetProperty(Status, "ID", idref("Status"));
      SetProperty(Retry, "ID", idref("Retry"));
      SetProperty(ErrorText, "ID", idref("ErrorText"));
      SetProperty(LastPage, "ID", idref("LastPage"));
      SetProperty(ResultText, "ID", idref("ResultText"));
    }
  }
}

