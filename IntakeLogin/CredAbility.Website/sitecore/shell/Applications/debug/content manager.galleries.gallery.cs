using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_a_50 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public System.Web.UI.Control __IGNOREMESSAGES;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public System.Web.UI.Control placeholder9;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "White") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Gallery.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Type", "text/JavaScript", "Language", "javascript", "Src", "/sitecore/shell/Controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Type", "text/JavaScript", "Language", "javascript", "Src", "/sitecore/shell/Applications/Content Manager/Galleries/Gallery.js") as Sitecore.Web.UI.HtmlControls.Script;
      __IGNOREMESSAGES = AddControl("input", "", formpage1, "", "type", "hidden", "id", idref("__IGNOREMESSAGES"), "value", "1");
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "Class", "scGalleryOuterFrame") as Sitecore.Web.UI.HtmlControls.Border;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Class", "scGalleryInnerFrame") as Sitecore.Web.UI.HtmlControls.Border;
      placeholder9 = AddPlaceholder("", border8, "");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(__IGNOREMESSAGES, "id", idref("__IGNOREMESSAGES"));
    }
  }
}

