using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_BehaviourOptionEditor_a_114 : Sitecore.Shell.Applications.Install.Controls.BehaviourOptionEditor   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.ListItem ClearVersions;
    public Sitecore.Web.UI.HtmlControls.ListItem AppendVersions;
    public Sitecore.Web.UI.HtmlControls.ListItem MergeVersions;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox9;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "") as Sitecore.Web.UI.HtmlControls.Border;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "") as Sitecore.Web.UI.HtmlControls.Border;
      OverwriteItems = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border3, "", "Header", "Overwrite", "Width", "100%", "Value", "1") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "") as Sitecore.Web.UI.HtmlControls.Border;
      MergeItems = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border4, "", "Name", "BehaviourOptions", "Header", "Merge", "Width", "100%", "Value", "2") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      MergeOptions = AddControl(new Sitecore.Web.UI.HtmlControls.Listbox(), border4, "", "Style", "display:block", "margin", "0 0 0 20") as Sitecore.Web.UI.HtmlControls.Listbox;
      ClearVersions = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), MergeOptions, "", "Header", "Clear", "Value", "1", "Selected", "true") as Sitecore.Web.UI.HtmlControls.ListItem;
      AppendVersions = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), MergeOptions, "", "Header", "Append", "Value", "2") as Sitecore.Web.UI.HtmlControls.ListItem;
      MergeVersions = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), MergeOptions, "", "Header", "Merge", "Value", "3") as Sitecore.Web.UI.HtmlControls.ListItem;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "") as Sitecore.Web.UI.HtmlControls.Border;
      SideBySideItems = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border5, "", "Header", "Side-by-Side", "Width", "100%", "Value", "4") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "") as Sitecore.Web.UI.HtmlControls.Border;
      SkipItems = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border6, "", "Header", "Skip", "Width", "100%", "Value", "3") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border2, "") as Sitecore.Web.UI.HtmlControls.Border;
      AskUser = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border7, "", "Header", "Ask User", "Width", "100%", "Value", "0") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel1, "", "GridPanel.Valign", "top", "Padding", "8 4 0 4") as Sitecore.Web.UI.HtmlControls.Border;
      groupbox9 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), border8, "", "Padding", "4 8 4 8", "Style", "width:260;height:80;background:f7f7f7") as Sitecore.Web.UI.HtmlControls.Groupbox;
      BehaviorOptionText = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), groupbox9, "") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

