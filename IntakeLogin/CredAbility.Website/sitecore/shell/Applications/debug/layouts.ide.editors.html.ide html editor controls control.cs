using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_HtmlEditor_Controls_Control_a_113 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    
    public string m_Details;
    
    // properties
    public string Details {
      get {
        return StringUtil.GetString(m_Details);
      }
      set {
        m_Details = value;
        
        SetProperty(literal4, "Text", Details);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Background", "#F8EED0", "Border", "1px solid #F0CCA5", "FontName", "tahoma", "FontSize", "8pt", "Padding", "2", "Margin", "2") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage3 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel2, "", "Src", "Software/16x16/element.png", "Width", "16", "Height", "16", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel2, "", "Text", Details) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

