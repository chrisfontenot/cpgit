using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_HtmlEditor_Controls_Placeholder_a_146 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public System.Web.UI.Control b6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    
    public string m_Header;
    public string m_Details;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal7, "Text", Header);
      }
    }
    
    public string Details {
      get {
        return StringUtil.GetString(m_Details);
      }
      set {
        m_Details = value;
        
        SetProperty(literal9, "Text", Details);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Background", "#F8EED0", "Border", "1px solid #F0CCA5", "FontName", "tahoma", "FontSize", "8pt", "Padding", "2", "Margin", "2") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage3 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel2, "", "Src", "Software/16x16/element_selection.png", "Width", "16", "Height", "16", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "") as Sitecore.Web.UI.HtmlControls.Border;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      b6 = AddControl("b", "", border5, "");
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b6, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "", "Foreground", "#666666") as Sitecore.Web.UI.HtmlControls.Border;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "Text", Details) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

