using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_NameEditor_a_94 : Sitecore.Shell.Applications.Install.Controls.NameEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    
    public string m_Padding;
    public string m_Style;
    
    // properties
    public new string Padding {
      get {
        return StringUtil.GetString(m_Padding);
      }
      set {
        m_Padding = value;
        
        SetProperty(border1, "Padding", Padding);
      }
    }
    
    public new string Style {
      get {
        return StringUtil.GetString(m_Style);
      }
      set {
        m_Style = value;
        
        SetProperty(border1, "Style", Style);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Width", "100%", "Padding", Padding, "Style", Style) as Sitecore.Web.UI.HtmlControls.Border;
      NameLabel = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), border1, "", "Header", "Source name:") as Sitecore.Web.UI.HtmlControls.Label;
      NameEdit = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border1, "", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
  }
}

