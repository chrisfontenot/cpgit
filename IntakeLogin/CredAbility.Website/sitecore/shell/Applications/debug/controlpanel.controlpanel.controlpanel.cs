using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class ControlPanel_a_99 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ControlPanel;
    public Sitecore.Web.UI.XmlControls.XmlControl taskbackground3;
    public Sitecore.Web.UI.XmlControls.XmlControl Title;
    public Sitecore.Web.UI.HtmlControls.Literal TitleText;
    public Sitecore.Web.UI.XmlControls.XmlControl OptionsSection;
    public Sitecore.Web.UI.HtmlControls.Border Options;
    public Sitecore.Web.UI.XmlControls.XmlControl LinksSection;
    public Sitecore.Web.UI.HtmlControls.Border Links;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Control Panel") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.ControlPanel.ControlPanel.ControlPanelForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      ControlPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), formpage1, "", "ID", idref("ControlPanel"), "Border", "none", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      taskbackground3 = AddControl("TaskBackground", "", ControlPanel, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Title = AddControl("TaskTitle", "", taskbackground3, "", "ID", idref("Title")) as Sitecore.Web.UI.XmlControls.XmlControl;
      TitleText = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Title, "", "ID", idref("TitleText")) as Sitecore.Web.UI.HtmlControls.Literal;
      OptionsSection = AddControl("TaskOptions", "", taskbackground3, "", "ID", idref("OptionsSection")) as Sitecore.Web.UI.XmlControls.XmlControl;
      Options = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), OptionsSection, "", "ID", idref("Options")) as Sitecore.Web.UI.HtmlControls.Border;
      LinksSection = AddControl("TaskLinks", "", taskbackground3, "", "ID", idref("LinksSection")) as Sitecore.Web.UI.XmlControls.XmlControl;
      Links = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LinksSection, "", "ID", idref("Links")) as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(ControlPanel, "ID", idref("ControlPanel"));
      SetProperty(Title, "ID", idref("Title"));
      SetProperty(TitleText, "ID", idref("TitleText"));
      SetProperty(OptionsSection, "ID", idref("OptionsSection"));
      SetProperty(Options, "ID", idref("Options"));
      SetProperty(LinksSection, "ID", idref("LinksSection"));
      SetProperty(Links, "ID", idref("Links"));
    }
  }
}

