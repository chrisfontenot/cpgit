using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_Grip_a_54 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage2;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", "scGalleryGrip", "onmousedown", "javascript:scGallery.mouseDown(this, event)", "onmousemove", "javascript:scGallery.mouseMove(this, event)", "onmouseup", "javascript:scGallery.mouseUp(this, event)") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage2 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border1, "", "Src", "Images/Galleries/Grip.png", "Class", "scGalleryGripIcon") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      
      _Mode = "";
    }
  }
}

