using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_Languages_Option_a_103 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public System.Web.UI.Control b6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    
    public string m_ClassName;
    public string m_Click;
    public string m_Icon;
    public string m_Header;
    public string m_Description;
    
    // properties
    public string ClassName {
      get {
        return StringUtil.GetString(m_ClassName);
      }
      set {
        m_ClassName = value;
        
        SetProperty(border1, "Class", ClassName);
      }
    }
    
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(border1, "Click", Click);
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage3, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal7, "Text", Header);
      }
    }
    
    public string Description {
      get {
        return StringUtil.GetString(m_Description);
      }
      set {
        m_Description = value;
        
        SetProperty(literal9, "Text", Description);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "RollOver", "true", "Class", ClassName, "Click", Click) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage3 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel2, "", "Src", Icon, "Height", "32", "Width", "32", "Margin", "0px 4px 0px 4px", "GridPanel.Style", "padding: 0px 4px", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "GridPanel.VAlign", "top", "Padding", "2px 4px 2px 0px", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "", "Padding", "0px 0px 4px 0px", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      b6 = AddControl("b", "", border5, "");
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b6, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border4, "", "NoWrap", "true", "Foreground", "#666666") as Sitecore.Web.UI.HtmlControls.Border;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "Text", Description) as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
  }
}

