using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FileDateFilterEditor_a_77 : Sitecore.Shell.Applications.Install.Controls.FileDateFilterEditor   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Columns", "2", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel1, "", "Columns", "1", "GridPanel.Width", "180", "GridPanel.Valign", "Top") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel2, "", "Columns", "3", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      PastDays = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), gridpanel3, "", "Header", "Within the past ", "Width", "100%", "GridPanel.Width", "100", "Value", "PastDays") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      PastDaysEdit = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel3, "", "Width", "40", "GridPanel.Width", "45") as Sitecore.Web.UI.HtmlControls.Edit;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "Text", "days") as Sitecore.Web.UI.HtmlControls.Literal;
      SpecifyRange = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), gridpanel2, "", "Width", "100%", "GridPanel.Width", "100%", "Header", "Specify dates", "Value", "SpecifyRange") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      Dates = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dates, "", "Columns", "2", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "From", "Padding", "0 0 0 21", "GridPanel.Width", "55") as Sitecore.Web.UI.HtmlControls.Literal;
      FromDate = AddControl(new Sitecore.Web.UI.HtmlControls.DatePicker(), gridpanel5, "", "GridPanel.Width", "100") as Sitecore.Web.UI.HtmlControls.DatePicker;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel5, "", "Text", "To", "Padding", "0 0 0 21") as Sitecore.Web.UI.HtmlControls.Literal;
      ToDate = AddControl(new Sitecore.Web.UI.HtmlControls.DatePicker(), gridpanel5, "") as Sitecore.Web.UI.HtmlControls.DatePicker;
      ClearButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel1, "", "Header", "Clear Filter", "GridPanel.VAlign", "Top") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
  }
}

