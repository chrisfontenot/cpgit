using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDENewFileWizard_a_78 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public System.Web.UI.Control li11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.XmlControls.XmlControl Name;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent13;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Edit FileName;
    public Sitecore.Web.UI.XmlControls.XmlControl Location;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent16;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox17;
    public Sitecore.Web.UI.WebControls.TreeviewEx LocationTreeview;
    public Sitecore.Web.UI.XmlControls.XmlControl FileLocation;
    public Sitecore.Web.UI.HtmlControls.DataContext FileDataContext;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent18;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel19;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox20;
    public Sitecore.Web.UI.WebControls.TreeviewEx FileLocationTreeview;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Checkbox CreateCodebehind;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Literal literal22;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "Background", "#e9e9e9", "CodeBeside", "Sitecore.Shell.Applications.Layouts.IDE.Wizards.NewFileWizard.IDENewFileWizardForm,Sitecore.Client", "Submittable", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Software/32x32/text_code.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Create File Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Give a name to the new layout.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Select a location.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Select a file location.") as Sitecore.Web.UI.HtmlControls.Literal;
      li11 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li11, "", "Text", "Create the layout.") as Sitecore.Web.UI.HtmlControls.Literal;
      Name = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Name"), "Header", "Name", "Text", "Enter a name for the new file. Click Next to continue.", "Icon", "Software/32x32/text_code.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent13 = AddControl("WizardFormIndent", "", Name, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel14 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent13, "", "Width", "100%", "CellPadding", "2", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel14, "", "Text", "Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      FileName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel14, "", "ID", idref("FileName"), "Value", "New File", "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      Location = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Location"), "Header", "Location", "Text", "Select the location where the item representing the new file should be stored in the Sitecore content tree. Click Next to continue.", "Icon", "Software/32x32/text_code.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), Location, "", "ID", idref("DataContext"), "DataViewName", "Master", "Root", "{32566F0E-7686-45F1-A12F-D7260BD78BC3}", "Filter", "Contains('{A87A00B1-E6DB-45AB-8B54-636FEC3B5523},{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B},{93227C5D-4FEF-474D-94C0-F252EC8E8219},{7EE0975B-0698-493E-B3A2-0B2EF33D0522},{3BAA73E5-6BA9-4462-BF72-C106F8801B11}', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      wizardformindent16 = AddControl("WizardFormIndent", "", Location, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox17 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), wizardformindent16, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "ContextMenu", "LocationTreeview.GetContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      LocationTreeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox17, "", "ID", idref("LocationTreeview"), "DataContext", "DataContext") as Sitecore.Web.UI.WebControls.TreeviewEx;
      FileLocation = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("FileLocation"), "Header", "File Location", "Text", "Select the location where the new file should be stored in the Sitecore webroot file structure. Click Create to create the file.", "Icon", "Software/32x32/text_code.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      FileDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), FileLocation, "", "ID", idref("FileDataContext"), "DataViewName", "FileSystem", "Root", "/", "Folder", "/", "Filter", "Contains('" + Sitecore.Data.DataProviders.FileSystemDataProvider.FolderID.ToString() + "', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      wizardformindent18 = AddControl("WizardFormIndent", "", FileLocation, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel19 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent18, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox20 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel19, "", "GridPanel.Height", "100%", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "ContextMenu", "GetFileContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileLocationTreeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox20, "", "ID", idref("FileLocationTreeview"), "DataContext", "FileDataContext", "Root", "false", "Width", "100%") as Sitecore.Web.UI.WebControls.TreeviewEx;
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel19, "", "Padding", "4px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      CreateCodebehind = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), border21, "", "ID", idref("CreateCodebehind"), "Checked", "false", "Visible", "false", "Header", "Create Associated C# Code Files") as Sitecore.Web.UI.HtmlControls.Checkbox;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Software/32x32/text_code.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      literal22 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), LastPage, "", "Text", "The wizard has completed. Click Finish to close the Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(Name, "ID", idref("Name"));
      SetProperty(FileName, "ID", idref("FileName"));
      SetProperty(Location, "ID", idref("Location"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(LocationTreeview, "ID", idref("LocationTreeview"));
      SetProperty(FileLocation, "ID", idref("FileLocation"));
      SetProperty(FileDataContext, "ID", idref("FileDataContext"));
      SetProperty(FileLocationTreeview, "ID", idref("FileLocationTreeview"));
      SetProperty(CreateCodebehind, "ID", idref("CreateCodebehind"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

