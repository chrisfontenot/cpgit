using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class AddFromTemplate_a_101 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.XmlControls.XmlControl TemplatesDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox4;
    public Sitecore.Web.UI.WebControls.TreeviewEx Templates;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Edit TemplateName;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Edit ItemName;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "Applications/32x32/form_blue_add.png", "Header", "Insert from Template", "Text", "Select a template and enter a name for the new item.", "OKButton", "Insert") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Templates.AddFromTemplate.AddFromTemplateForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), Dialog, "", "Src", "/sitecore/shell/applications/Templates/AddFromTemplate/AddFromTemplate.xml.js") as Sitecore.Web.UI.HtmlControls.Script;
      TemplatesDataContext = AddControl("TemplateDataContext", "", Dialog, "", "ID", idref("TemplatesDataContext")) as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox4 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "Background", "white", "Class", "scScrollbox scFixSize scInsetBorder", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Templates = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox4, "", "ID", idref("Templates"), "Click", "SelectTreeNode", "DblClick", "DoOK", "DataContext", "TemplatesDataContext") as Sitecore.Web.UI.WebControls.TreeviewEx;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "4px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border5, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 4px 0px 0px", "GridPanel.Align", "right", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", "Template:") as Sitecore.Web.UI.HtmlControls.Literal;
      TemplateName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel6, "", "ID", idref("TemplateName"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 4px 0px 0px", "GridPanel.Align", "right", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Item Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      ItemName = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel6, "", "ID", idref("ItemName"), "Width", "100%", "GridPanel.Width", "100%", "Class", "not-dirty") as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(TemplatesDataContext, "ID", idref("TemplatesDataContext"));
      SetProperty(Templates, "ID", idref("Templates"));
      SetProperty(TemplateName, "ID", idref("TemplateName"));
      SetProperty(ItemName, "ID", idref("ItemName"));
    }
  }
}

