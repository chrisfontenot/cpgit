using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class ChangeTemplate_a_92 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public System.Web.UI.Control ul6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.XmlControls.XmlControl Template;
    public Sitecore.Web.UI.XmlControls.XmlControl TemplatesDataContext;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent9;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox10;
    public Sitecore.Web.UI.WebControls.TreeviewEx TemplateLister;
    public Sitecore.Web.UI.XmlControls.XmlControl Confirm;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public System.Web.UI.Control li14;
    public System.Web.UI.Control b15;
    public Sitecore.Web.UI.HtmlControls.Literal OldTemplateName;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Border border17;
    public System.Web.UI.Control li18;
    public System.Web.UI.Control b19;
    public Sitecore.Web.UI.HtmlControls.Literal TemplateName;
    public Sitecore.Web.UI.HtmlControls.Border Warning;
    public Sitecore.Web.UI.HtmlControls.Border border20;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage21;
    public Sitecore.Web.UI.HtmlControls.Literal literal22;
    public Sitecore.Web.UI.HtmlControls.Border LostFields;
    public System.Web.UI.Control ul23;
    public System.Web.UI.Control li24;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public System.Web.UI.Control li26;
    public Sitecore.Web.UI.HtmlControls.Literal literal27;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Literal literal28;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "Application", "Templates/Change Template", "Background", "#e9e9e9", "CodeBeside", "Sitecore.Shell.Applications.Templates.ChangeTemplate.ChangeTemplateForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), wizardform1, "", "Key", "Warning") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scWarning {          background: #ffffe4;          border: 1px solid #c9c9c9;          border-left:none;          border-right:none;          padding: 4px 2px 4px 4px;          margin: 24px 0px 4px 0px;          font-weight: bold;        }                #LostFields ul {          margin-top: 0;           margin-left: 32px;        }      ", "", stylesheet2, "");
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Applications/32x32/form_blue.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border4, "", "ID", idref("Welcome"), "Text", "Welcome to the Change Template Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul6 = AddControl("ul", "", FirstPage, "");
      li7 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Select a new template to use with the current item.") as Sitecore.Web.UI.HtmlControls.Literal;
      Template = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Template"), "Header", "Select the Template", "Text", "Select the template that you want to use. Click Next to continue", "Icon", "Applications/32x32/form_blue.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      TemplatesDataContext = AddControl("TemplateDataContext", "", Template, "", "ID", idref("TemplatesDataContext")) as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent9 = AddControl("WizardFormIndent", "", Template, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox10 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), wizardformindent9, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      TemplateLister = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox10, "", "ID", idref("TemplateLister"), "DataContext", "TemplatesDataContext") as Sitecore.Web.UI.WebControls.TreeviewEx;
      Confirm = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Confirm"), "Header", "Change", "Text", "Are you sure you want to change the template? Click Next to change the template.", "Icon", "Applications/32x32/form_blue.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent11 = AddControl("WizardFormIndent", "", Confirm, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), wizardformindent11, "", "Text", "The template will be changed from:") as Sitecore.Web.UI.HtmlControls.Literal;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformindent11, "", "Padding", "16px 0px 16px 16px") as Sitecore.Web.UI.HtmlControls.Border;
      li14 = AddControl("li", "", border13, "");
      b15 = AddControl("b", "", li14, "");
      OldTemplateName = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b15, "", "ID", idref("OldTemplateName")) as Sitecore.Web.UI.HtmlControls.Literal;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), wizardformindent11, "", "Text", "to:") as Sitecore.Web.UI.HtmlControls.Literal;
      border17 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformindent11, "", "Padding", "16px 0px 0px 16px") as Sitecore.Web.UI.HtmlControls.Border;
      li18 = AddControl("li", "", border17, "");
      b19 = AddControl("b", "", li18, "");
      TemplateName = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b19, "", "ID", idref("TemplateName")) as Sitecore.Web.UI.HtmlControls.Literal;
      Warning = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformindent11, "", "ID", idref("Warning"), "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      border20 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Warning, "", "class", "scWarning") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage21 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border20, "", "Height", "16", "Width", "16", "style", "vertical-align:middle; margin-right: 4px", "Src", "Applications/16x16/warning.png") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal22 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border20, "", "Text", "If you change the template, the data in the following fields may be lost:") as Sitecore.Web.UI.HtmlControls.Literal;
      LostFields = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Warning, "", "ID", idref("LostFields")) as Sitecore.Web.UI.HtmlControls.Border;
      ul23 = AddControl("ul", "", LostFields, "");
      li24 = AddControl("li", "", ul23, "");
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li24, "", "Text", "Text") as Sitecore.Web.UI.HtmlControls.Literal;
      li26 = AddControl("li", "", ul23, "");
      literal27 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li26, "", "Text", "Description") as Sitecore.Web.UI.HtmlControls.Literal;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Applications/32x32/form_blue.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      literal28 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), LastPage, "", "Text", "The template has been changed successfully. Click Finish to close the Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(Template, "ID", idref("Template"));
      SetProperty(TemplatesDataContext, "ID", idref("TemplatesDataContext"));
      SetProperty(TemplateLister, "ID", idref("TemplateLister"));
      SetProperty(Confirm, "ID", idref("Confirm"));
      SetProperty(OldTemplateName, "ID", idref("OldTemplateName"));
      SetProperty(TemplateName, "ID", idref("TemplateName"));
      SetProperty(Warning, "ID", idref("Warning"));
      SetProperty(LostFields, "ID", idref("LostFields"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

