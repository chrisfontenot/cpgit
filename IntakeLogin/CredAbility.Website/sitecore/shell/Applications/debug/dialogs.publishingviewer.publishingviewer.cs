using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class PublishingViewer_a_117 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public System.Web.UI.Control text4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Space space9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.DateCalendar StartDate;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.HtmlControls.DateCalendar EndDate;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox12;
    public Sitecore.Web.UI.WebControls.GridPanel Viewer;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/48x48/Earth_Time.png", "Header", "Publishing Viewer", "Text", "When the different versions of the current item are active on the Web site.", "OKButton", "Close", "CancelButton", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.PublishingViewer.PublishingViewerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "inline") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text4 = AddLiteral("        .ie .scBar {          display: inline;        }                .ff .scBar {          float: left;        }                .ff .scDates {          display: none;        }      ", "", stylesheet3, "");
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Align", "Center", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "Columns", "3") as Sitecore.Web.UI.WebControls.GridPanel;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "Start date:", "Click", "Refresh", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Literal;
      space9 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel7, "", "Width", "32") as Sitecore.Web.UI.HtmlControls.Space;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel7, "", "Text", "End date:", "Click", "Refresh", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Literal;
      StartDate = AddControl(new Sitecore.Web.UI.HtmlControls.DateCalendar(), gridpanel7, "", "ID", idref("StartDate")) as Sitecore.Web.UI.HtmlControls.DateCalendar;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel7, "", "Width", "32") as Sitecore.Web.UI.HtmlControls.Space;
      EndDate = AddControl(new Sitecore.Web.UI.HtmlControls.DateCalendar(), gridpanel7, "", "ID", idref("EndDate")) as Sitecore.Web.UI.HtmlControls.DateCalendar;
      scrollbox12 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Height", "100%", "Padding", "0px", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Viewer = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox12, "", "ID", idref("Viewer"), "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(StartDate, "ID", idref("StartDate"));
      SetProperty(EndDate, "ID", idref("EndDate"));
      SetProperty(Viewer, "ID", idref("Viewer"));
    }
  }
}

