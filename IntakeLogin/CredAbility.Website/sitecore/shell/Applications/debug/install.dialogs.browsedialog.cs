using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_Browse_a_96 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.DataContext FileExplorerDataContext;
    public Sitecore.Web.UI.HtmlControls.ContextMenu ContextMenu;
    public Sitecore.Web.UI.XmlControls.XmlControl listviewviewsmenuitems2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar4;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton5;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton6;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton8;
    public Sitecore.Web.UI.WebControls.GridPanel Body;
    public Sitecore.Web.UI.HtmlControls.Scrollbox FileExplorerFrame;
    public Sitecore.Web.UI.HtmlControls.Listview FileExplorer;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader9;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem10;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem11;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Edit Filename;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "People/16x16/box.png", "Header", "Save Package As", "Text", "Save changes") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Install.Dialogs.BrowseDialog,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      FileExplorerDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), Dialog, "", "ID", idref("FileExplorerDataContext")) as Sitecore.Web.UI.HtmlControls.DataContext;
      ContextMenu = AddControl(new Sitecore.Web.UI.HtmlControls.ContextMenu(), Dialog, "", "ID", idref("ContextMenu")) as Sitecore.Web.UI.HtmlControls.ContextMenu;
      listviewviewsmenuitems2 = AddControl("ListviewViewsMenuItems", "", ContextMenu, "", "Listview", "FileExplorer") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      toolbar4 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), gridpanel3, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      toolbutton5 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar4, "", "Icon", "Applications/16x16/refresh.png", "Tooltip", "Refresh the files view", "Header", "Refresh", "Click", "dialog:refresh") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar4, "", "Icon", "Applications/16x16/folder_up.png", "Tooltip", "Upload a file", "Header", "Upload", "Click", "dialog:upload") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar4, "", "Icon", "Network/16x16/download.png", "Tooltip", "Download the selected file", "Header", "Download", "Click", "dialog:download", "Visible", Sitecore.Context.IsAdministrator.ToString()) as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar4, "", "Icon", "Applications/16x16/delete2.png", "Tooltip", "Delete the selected file", "Header", "Delete", "Click", "dialog:delete") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      Body = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "ID", idref("Body"), "Width", "100%", "Height", "100%", "Fixed", "true", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      FileExplorerFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Body, "", "ID", idref("FileExplorerFrame"), "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "ContextMenu", "show:ContextMenu", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileExplorer = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), FileExplorerFrame, "", "ID", idref("FileExplorer"), "Click", "SelectListItem", "DblClick", "OK.ClickButton", "DataContext", "FileExplorerDataContext") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader9 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), FileExplorer, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem10 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader9, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem11 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader9, "", "Name", "size", "Header", "Size") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem12 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader9, "", "Name", "modified", "Header", "Date Modified") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "2px 0px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel14 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border13, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel14, "", "Padding", "0px 4px 0px 0px", "Width", "5em") as Sitecore.Web.UI.HtmlControls.Border;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border15, "", "Text", "File name:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filename = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel14, "", "ID", idref("Filename"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(FileExplorerDataContext, "ID", idref("FileExplorerDataContext"));
      SetProperty(ContextMenu, "ID", idref("ContextMenu"));
      SetProperty(Body, "ID", idref("Body"));
      SetProperty(FileExplorerFrame, "ID", idref("FileExplorerFrame"));
      SetProperty(FileExplorer, "ID", idref("FileExplorer"));
      SetProperty(Filename, "ID", idref("Filename"));
    }
  }
}

