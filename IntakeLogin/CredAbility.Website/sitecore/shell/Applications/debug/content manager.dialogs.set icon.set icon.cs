using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SetIcon_a_100 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public System.Web.UI.Control text5;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Edit IconFile;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Literal literal11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Combobox Selector;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem13;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem14;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem15;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem16;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem17;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem18;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem19;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem20;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem21;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem22;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem23;
    public System.Web.UI.Control text24;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem25;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem26;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem27;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem28;
    public Sitecore.Web.UI.HtmlControls.Literal literal29;
    public Sitecore.Web.UI.HtmlControls.Border List;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ApplicationsList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox BusinessList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ControlsList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Core1List;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Core2List;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Core3List;
    public Sitecore.Web.UI.HtmlControls.Scrollbox DatabaseList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox FlagsList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ImagingList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox MultimediaList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox NetworkList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox OtherList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox PeopleList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox SoftwareList;
    public Sitecore.Web.UI.HtmlControls.Scrollbox WordProcessingList;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Core2/32x32/smiley_face2.png", "Header", "Icon", "Submittable", "false", "Text", "Assign an icon to the item.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.SetIcon.SetIconForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formdialog1, "", "Src", "/sitecore/shell/Applications/Content Manager/Dialogs/Set Icon/SetIcon.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "SetIcons") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text5 = AddLiteral("        #List img {          margin:4px;          width:32px;          height:32px;        }      ", "", stylesheet4, "");
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 0px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", "Enter the file name of the icon:") as Sitecore.Web.UI.HtmlControls.Literal;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 0px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      IconFile = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border9, "", "ID", idref("IconFile"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 0px 24px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal11 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border10, "", "Text", "For example: 'Applications/16x16/about.png'.") as Sitecore.Web.UI.HtmlControls.Literal;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      Selector = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), border12, "", "ID", idref("Selector"), "Float", "right", "Style", "font:8pt tahoma", "Change", "javascript:scChange(this,event)") as Sitecore.Web.UI.HtmlControls.Combobox;
      listitem13 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Applications", "Value", "Applications") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem14 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Business", "Value", "Business") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem15 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Controls", "Value", "Controls") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem16 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Core 1", "Value", "Core1") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Core 2", "Value", "Core2") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Core 3", "Value", "Core3") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem19 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Database", "Value", "Database") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem20 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Flags", "Value", "Flags") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem21 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Imaging", "Value", "Imaging") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem22 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Multimedia", "Value", "Multimedia") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem23 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Network", "Value", "Network") as Sitecore.Web.UI.HtmlControls.ListItem;
      text24 = AddLiteral("\"            ", "", Selector, "");
      listitem25 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Other", "Value", "Other") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem26 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "People", "Value", "People") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem27 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Software", "Value", "Software") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem28 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Selector, "", "Header", "Word Processing", "Value", "WordProcessing") as Sitecore.Web.UI.HtmlControls.ListItem;
      literal29 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border12, "", "Text", "You can also select an icon:") as Sitecore.Web.UI.HtmlControls.Literal;
      List = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "ID", idref("List"), "Height", "100%", "GridPanel.Height", "100%", "Click", "javascript:scClick(this,event)") as Sitecore.Web.UI.HtmlControls.Border;
      ApplicationsList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("ApplicationsList"), "Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      BusinessList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("BusinessList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ControlsList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("ControlsList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Core1List = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("Core1List"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Core2List = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("Core2List"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Core3List = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("Core3List"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      DatabaseList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("DatabaseList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FlagsList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("FlagsList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ImagingList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("ImagingList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      MultimediaList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("MultimediaList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      NetworkList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("NetworkList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      OtherList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("OtherList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      PeopleList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("PeopleList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      SoftwareList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("SoftwareList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      WordProcessingList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), List, "", "ID", idref("WordProcessingList"), "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(IconFile, "ID", idref("IconFile"));
      SetProperty(Selector, "ID", idref("Selector"));
      SetProperty(List, "ID", idref("List"));
      SetProperty(ApplicationsList, "ID", idref("ApplicationsList"));
      SetProperty(BusinessList, "ID", idref("BusinessList"));
      SetProperty(ControlsList, "ID", idref("ControlsList"));
      SetProperty(Core1List, "ID", idref("Core1List"));
      SetProperty(Core2List, "ID", idref("Core2List"));
      SetProperty(Core3List, "ID", idref("Core3List"));
      SetProperty(DatabaseList, "ID", idref("DatabaseList"));
      SetProperty(FlagsList, "ID", idref("FlagsList"));
      SetProperty(ImagingList, "ID", idref("ImagingList"));
      SetProperty(MultimediaList, "ID", idref("MultimediaList"));
      SetProperty(NetworkList, "ID", idref("NetworkList"));
      SetProperty(OtherList, "ID", idref("OtherList"));
      SetProperty(PeopleList, "ID", idref("PeopleList"));
      SetProperty(SoftwareList, "ID", idref("SoftwareList"));
      SetProperty(WordProcessingList, "ID", idref("WordProcessingList"));
    }
  }
}

