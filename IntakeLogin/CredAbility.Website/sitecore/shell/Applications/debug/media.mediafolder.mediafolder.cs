using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class MediaFolder_a_101 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Border SettingsContainer;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.Script script7;
    public Sitecore.Web.UI.HtmlControls.Script script8;
    public Sitecore.Web.UI.HtmlControls.Script script9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Scrollbox FileList;
    public System.Web.UI.Control UploadPanel;
    public System.Web.UI.Control UploadUI;
    public Sitecore.Web.UI.HtmlControls.Literal Header;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Scrollbox;
    public System.Web.UI.Control queue;
    public System.Web.UI.Control thead12;
    public System.Web.UI.Control tr13;
    public System.Web.UI.Control th14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public System.Web.UI.Control th16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public System.Web.UI.Control th18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public System.Web.UI.Control th20;
    public System.Web.UI.Control tbody21;
    public Sitecore.Web.UI.WebControls.GridPanel AdvancedOptions;
    public Sitecore.Web.UI.HtmlControls.Checkbox Unpack;
    public Sitecore.Web.UI.HtmlControls.Checkbox Versioned;
    public Sitecore.Web.UI.HtmlControls.Checkbox Overwrite;
    public Sitecore.Web.UI.HtmlControls.Checkbox AsFiles;
    public System.Web.UI.Control buttons;
    public System.Web.UI.Control img22;
    public Sitecore.Web.UI.HtmlControls.Button UploadButton;
    public Sitecore.Web.UI.HtmlControls.Button CancelButton;
    public Sitecore.Web.UI.HtmlControls.Button CloseButton;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Media.MediaFolder.MediaFolderForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Media Folder Viewer.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      SettingsContainer = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "id", idref("SettingsContainer"), "style", "display:none") as Sitecore.Web.UI.HtmlControls.Border;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "runat", "server", "Src", "/sitecore/shell/controls/lib/YUIupload/yahoo-dom-event/yahoo-dom-event.js") as Sitecore.Web.UI.HtmlControls.Script;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "runat", "server", "Src", "/sitecore/shell/controls/lib/YUIupload/element/element-beta-min.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "runat", "server", "Src", "/sitecore/shell/controls/lib/YUIupload/uploader/uploader-experimental-min.js") as Sitecore.Web.UI.HtmlControls.Script;
      script7 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "src", "/sitecore/shell/controls/lib/scriptaculous/scriptaculous.js?load=effects") as Sitecore.Web.UI.HtmlControls.Script;
      script8 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "Src", "/sitecore/shell/controls/SitecoreLightbox.js") as Sitecore.Web.UI.HtmlControls.Script;
      script9 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "language", "javascript", "Src", "/sitecore/shell/applications/media/mediafolder/mediafolder.js") as Sitecore.Web.UI.HtmlControls.Script;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "Class", "scBackground") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border10, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      FileList = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel11, "", "ID", idref("FileList"), "Width", "100%", "Height", "100%", "Background", "Transparent", "Border", "none", "Padding", "0", "GridPanel.Height", "100%", "ContextMenu", "FileList_ContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      UploadPanel = AddControl("div", "", formpage1, "", "id", idref("UploadPanel"), "style", "width: 684px; display:none; background: white");
      UploadUI = AddControl("div", "", UploadPanel, "", "id", idref("UploadUI"));
      Header = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), UploadUI, "", "Text", "These files are ready for uploading:", "style", "display: none; padding: 0px 0px 12px 4px; font-weight: 700", "ID", idref("Header"), "runat", "server") as Sitecore.Web.UI.HtmlControls.Literal;
      Scrollbox = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), UploadUI, "", "ID", idref("Scrollbox"), "Style", "border:none; padding:0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      queue = AddControl("table", "", Scrollbox, "", "style", "display:none;", "id", idref("queue"), "cellpadding", "0", "cellspacing", "0");
      thead12 = AddControl("thead", "", queue, "");
      tr13 = AddControl("tr", "", thead12, "");
      th14 = AddControl("th", "", tr13, "", "class", "filename");
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), th14, "", "runat", "server", "Text", "Filename") as Sitecore.Web.UI.HtmlControls.Literal;
      th16 = AddControl("th", "", tr13, "", "class", "size");
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), th16, "", "runat", "server", "Text", "Size") as Sitecore.Web.UI.HtmlControls.Literal;
      th18 = AddControl("th", "", tr13, "", "class", "alt");
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), th18, "", "runat", "server", "Text", "Alternate Text") as Sitecore.Web.UI.HtmlControls.Literal;
      th20 = AddControl("th", "", tr13, "");
      tbody21 = AddControl("tbody", "", queue, "");
      AdvancedOptions = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), UploadPanel, "", "id", idref("AdvancedOptions"), "class", "options", "runat", "server", "Columns", "2", "Style", "display:none") as Sitecore.Web.UI.WebControls.GridPanel;
      Unpack = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), AdvancedOptions, "", "Header", "Unpack ZIP Archives", "runat", "server", "ID", idref("Unpack")) as Sitecore.Web.UI.HtmlControls.Checkbox;
      Versioned = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), AdvancedOptions, "", "Header", "Make Uploaded Media Items Versionable", "runat", "server", "ID", idref("Versioned")) as Sitecore.Web.UI.HtmlControls.Checkbox;
      Overwrite = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), AdvancedOptions, "", "Header", "Overwrite Existing Media Items", "runat", "server", "ID", idref("Overwrite")) as Sitecore.Web.UI.HtmlControls.Checkbox;
      AsFiles = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), AdvancedOptions, "", "Header", "Upload as Files", "runat", "server", "ID", idref("AsFiles")) as Sitecore.Web.UI.HtmlControls.Checkbox;
      buttons = AddControl("div", "", UploadPanel, "", "id", idref("buttons"), "style", "display:none");
      img22 = AddControl("img", "", buttons, "", "src", "/sitecore/shell/themes/standard/images/loading15x15.gif", "Class", "closeProgress", "style", "display:none; margin-left: 8px");
      UploadButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), buttons, "", "id", idref("UploadButton"), "runat", "server", "Click", "OnStart", "Header", "Upload", "style", "display:none") as Sitecore.Web.UI.HtmlControls.Button;
      CancelButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), buttons, "", "id", idref("CancelButton"), "runat", "server", "Click", "OnCancel", "Header", "Cancel") as Sitecore.Web.UI.HtmlControls.Button;
      CloseButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), buttons, "", "id", idref("CloseButton"), "runat", "server", "Click", "", "onclick", "javascript:scMediaFolder.activeUploader.close()", "Header", "Close", "style", "display:none") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(SettingsContainer, "id", idref("SettingsContainer"));
      SetProperty(FileList, "ID", idref("FileList"));
      SetProperty(UploadPanel, "id", idref("UploadPanel"));
      SetProperty(UploadUI, "id", idref("UploadUI"));
      SetProperty(Header, "ID", idref("Header"));
      SetProperty(Scrollbox, "ID", idref("Scrollbox"));
      SetProperty(queue, "id", idref("queue"));
      SetProperty(AdvancedOptions, "id", idref("AdvancedOptions"));
      SetProperty(Unpack, "ID", idref("Unpack"));
      SetProperty(Versioned, "ID", idref("Versioned"));
      SetProperty(Overwrite, "ID", idref("Overwrite"));
      SetProperty(AsFiles, "ID", idref("AsFiles"));
      SetProperty(buttons, "id", idref("buttons"));
      SetProperty(UploadButton, "id", idref("UploadButton"));
      SetProperty(CancelButton, "id", idref("CancelButton"));
      SetProperty(CloseButton, "id", idref("CloseButton"));
    }
  }
}

