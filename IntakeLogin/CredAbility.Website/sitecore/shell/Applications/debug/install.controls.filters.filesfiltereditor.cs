using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FilesFilterEditor_a_75 : Sitecore.Shell.Applications.Install.Controls.FilesFilterEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox7;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Groupbox groupbox15;
    
    public string m_Padding;
    public string m_Style;
    
    // properties
    public new string Padding {
      get {
        return StringUtil.GetString(m_Padding);
      }
      set {
        m_Padding = value;
        
        SetProperty(border1, "Padding", Padding);
      }
    }
    
    public new string Style {
      get {
        return StringUtil.GetString(m_Style);
      }
      set {
        m_Style = value;
        
        SetProperty(border1, "Style", Style);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Width", "100%", "Height", "100%", "Padding", Padding, "Style", Style) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Width", "100%", "Columns", "1", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.Width", "100%", "Background", "transparent", "Border", "none", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox3, "", "Width", "100%", "Columns", "1", "Height", "50") as Sitecore.Web.UI.WebControls.GridPanel;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Padding", "0 0 20 0") as Sitecore.Web.UI.HtmlControls.Border;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border5, "", "Text", "File name filter") as Sitecore.Web.UI.HtmlControls.Literal;
      groupbox7 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), border5, "", "Padding", "5 0 5 0") as Sitecore.Web.UI.HtmlControls.Groupbox;
      NameFilter = AddControl("Installer.FileNameFilterEditor", "", groupbox7, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox3, "", "Width", "100%", "Columns", "1", "Height", "80") as Sitecore.Web.UI.WebControls.GridPanel;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "Padding", "0 0 20 0") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Creation date filter") as Sitecore.Web.UI.HtmlControls.Literal;
      groupbox11 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), border9, "", "Padding", "5 0 5 0") as Sitecore.Web.UI.HtmlControls.Groupbox;
      CreationDateFilter = AddControl("Installer.FileDateFilterEditor", "", groupbox11, "", "DateFilterType", "CreatedFilter") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox3, "", "Width", "100%", "Columns", "1", "Height", "80") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Padding", "0 0 20 0") as Sitecore.Web.UI.HtmlControls.Border;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", "Modification date filter") as Sitecore.Web.UI.HtmlControls.Literal;
      groupbox15 = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), border13, "", "Padding", "5 0 5 0") as Sitecore.Web.UI.HtmlControls.Groupbox;
      ModificationDateFilter = AddControl("Installer.FileDateFilterEditor", "", groupbox15, "", "DateFilterType", "ModifiedFilter") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

