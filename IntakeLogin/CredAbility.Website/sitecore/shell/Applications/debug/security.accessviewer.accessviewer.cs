using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class AccessViewer_a_85 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside7;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public System.Web.UI.Control scActiveRibbonStrip;
    public Sitecore.Web.UI.WebControls.GridPanel Grid;
    public Sitecore.Web.UI.HtmlControls.Border RibbonPanel;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox9;
    public System.Web.UI.Control Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl AccessViewerLeft;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Explanation;
    public Sitecore.Web.UI.XmlControls.XmlControl accessviewerlegend10;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Security/Access Viewer") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Security editor.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      codebeside7 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Security.AccessViewer.AccessViewerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext")) as Sitecore.Web.UI.HtmlControls.DataContext;
      scActiveRibbonStrip = AddControl("input", "", formpage1, "", "type", "hidden", "id", idref("scActiveRibbonStrip"), "name", "scActiveRibbonStrip");
      Grid = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "ID", idref("Grid"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      RibbonPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Grid, "", "ID", idref("RibbonPanel")) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Grid, "", "Columns", "3", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox9 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel8, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "none", "Padding", "0px", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl("shell:AccessViewerTreeview", "", scrollbox9, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Root", "true");
      AccessViewerLeft = AddControl("VSplitter", "", gridpanel8, "", "ID", idref("AccessViewerLeft"), "GridPanel.Width", "4", "Target", "right", "GridPanel.Style", "background:#8595a4; display:expression(previousSibling.style.display)") as Sitecore.Web.UI.XmlControls.XmlControl;
      Explanation = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel8, "", "ID", idref("Explanation"), "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0px", "GridPanel.Width", "200") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      accessviewerlegend10 = AddControl("AccessViewerLegend", "", Grid, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(scActiveRibbonStrip, "id", idref("scActiveRibbonStrip"));
      SetProperty(Grid, "ID", idref("Grid"));
      SetProperty(RibbonPanel, "ID", idref("RibbonPanel"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(AccessViewerLeft, "ID", idref("AccessViewerLeft"));
      SetProperty(Explanation, "ID", idref("Explanation"));
    }
  }
}

