using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_Icons_a_49 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl gallery1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.WebControls.GridPanel Grid;
    public Sitecore.Web.UI.HtmlControls.GalleryMenu gallerymenu4;
    public Sitecore.Web.UI.HtmlControls.MenuPanel menupanel5;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Icons;
    public Sitecore.Web.UI.HtmlControls.MenuLine menuline6;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem7;
    public Sitecore.Web.UI.HtmlControls.MenuLine menuline8;
    public Sitecore.Web.UI.XmlControls.XmlControl gallery_grip9;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gallery1 = AddControl("Gallery", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), gallery1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), gallery1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Galleries.Icons.GalleryIconsForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      Grid = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gallery1, "", "ID", idref("Grid"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gallerymenu4 = AddControl(new Sitecore.Web.UI.HtmlControls.GalleryMenu(), Grid, "", "GridPanel.Height", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.GalleryMenu;
      menupanel5 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuPanel(), gallerymenu4, "", "Height", "100%") as Sitecore.Web.UI.HtmlControls.MenuPanel;
      Icons = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), menupanel5, "", "ID", idref("Icons"), "Height", "100%", "Background", "#F4F4F5", "Padding", "0", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      menuline6 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuLine(), gallerymenu4, "") as Sitecore.Web.UI.HtmlControls.MenuLine;
      menuitem7 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), gallerymenu4, "", "Header", "More Icons", "Icon", "Core2/16x16/smiley_face2.png", "Click", "item:selecticon") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menuline8 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuLine(), Grid, "") as Sitecore.Web.UI.HtmlControls.MenuLine;
      gallery_grip9 = AddControl("Gallery.Grip", "", Grid, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Grid, "ID", idref("Grid"));
      SetProperty(Icons, "ID", idref("Icons"));
    }
  }
}

