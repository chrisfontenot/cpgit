using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FileNameFilterEditor_a_76 : Sitecore.Shell.Applications.Install.Controls.FileNameFilterEditor   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.HtmlControls.Literal literal2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Columns", "1", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal2 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel1, "", "Text", "All or part of the file name:") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel1, "", "Width", "100%", "Columns", "2", "Height", "100%", "GridPanel.Valign", "Top") as Sitecore.Web.UI.WebControls.GridPanel;
      MaskFile = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel3, "", "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      ClearButton = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), gridpanel3, "", "Header", "Clear Filter") as Sitecore.Web.UI.HtmlControls.Button;
      IncludeAllDirectories = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), gridpanel3, "", "Header", "Ignore filter for directory entries", "GridPanel.Colspan", "2") as Sitecore.Web.UI.HtmlControls.Checkbox;
      
      _Mode = "";
    }
  }
}

