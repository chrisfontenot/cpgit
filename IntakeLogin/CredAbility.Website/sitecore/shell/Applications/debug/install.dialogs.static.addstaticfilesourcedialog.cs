using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_AddStaticFileSource_a_71 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.Border Internals;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.XmlControls.XmlControl LoadFileSource;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent11;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.HtmlControls.DataContext TreeDataContext;
    public Sitecore.Web.UI.HtmlControls.UpAction UpAction;
    public Sitecore.Web.UI.HtmlControls.ContextMenu ContextMenu;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem12;
    public Sitecore.Web.UI.HtmlControls.MenuDivider menudivider13;
    public Sitecore.Web.UI.XmlControls.XmlControl listviewviewsmenuitems14;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel15;
    public Sitecore.Web.UI.WebControls.GridPanel Body;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Folders;
    public Sitecore.Web.UI.HtmlControls.DataTreeview FileTreeview;
    public Sitecore.Web.UI.XmlControls.XmlControl FileExplorerLeft;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox16;
    public Sitecore.Web.UI.HtmlControls.DataListview FileExplorer;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader17;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem18;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem19;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem20;
    public Sitecore.Web.UI.HtmlControls.Space space21;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton23;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton24;
    public Sitecore.Web.UI.HtmlControls.Space space25;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ItemListBox;
    public Sitecore.Web.UI.HtmlControls.Listview ItemList;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader26;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem27;
    public Sitecore.Web.UI.XmlControls.XmlControl SetName;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent28;
    public Sitecore.Web.UI.XmlControls.XmlControl Name;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border border29;
    public Sitecore.Web.UI.HtmlControls.Literal literal30;
    public Sitecore.Web.UI.HtmlControls.Border border31;
    public Sitecore.Web.UI.HtmlControls.Literal literal32;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Install.Dialogs.AddStaticFileSourceDialog,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      Internals = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardform1, "", "ID", idref("Internals")) as Sitecore.Web.UI.HtmlControls.Border;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Add Static File Source Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Select the directories and files to include in the source.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Specify the name of the source.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Add the static file source.") as Sitecore.Web.UI.HtmlControls.Literal;
      LoadFileSource = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("LoadFileSource"), "Header", "Select Root Folder", "Text", "Select the folder where you want the search to start. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent11 = AddControl("WizardFormIndent", "", LoadFileSource, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), wizardformindent11, "", "ID", idref("DataContext"), "DataContext", "TreeDataContext", "DefaultItem", "/") as Sitecore.Web.UI.HtmlControls.DataContext;
      TreeDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), wizardformindent11, "", "ID", idref("TreeDataContext"), "DataContext", "DataContext", "DefaultItem", "/", "Filter", "Contains('" + Sitecore.Data.DataProviders.FileSystemDataProvider.FolderID.ToString() + "', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      UpAction = AddControl(new Sitecore.Web.UI.HtmlControls.UpAction(), wizardformindent11, "", "ID", idref("UpAction"), "DataContext", "DataContext") as Sitecore.Web.UI.HtmlControls.UpAction;
      ContextMenu = AddControl(new Sitecore.Web.UI.HtmlControls.ContextMenu(), wizardformindent11, "", "ID", idref("ContextMenu")) as Sitecore.Web.UI.HtmlControls.ContextMenu;
      menuitem12 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Icon", "Applications/16x16/folder_up.png", "Header", "Up", "Action", "UpAction", "Click", "DataContext.Up") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menudivider13 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuDivider(), ContextMenu, "") as Sitecore.Web.UI.HtmlControls.MenuDivider;
      listviewviewsmenuitems14 = AddControl("ListviewViewsMenuItems", "", ContextMenu, "", "Listview", "FileExplorer") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel15 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent11, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Body = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel15, "", "ID", idref("Body"), "Columns", "3", "Width", "100%", "Height", "100%", "Fixed", "true", "Border", "1px inset") as Sitecore.Web.UI.WebControls.GridPanel;
      Folders = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Body, "", "ID", idref("Folders"), "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "Style", "filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1, StartColorStr='#ffffff', EndColorStr='#DFE6EC')", "GridPanel.ID", "FoldersPane", "GridPanel.Width", "200") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileTreeview = AddControl(new Sitecore.Web.UI.HtmlControls.DataTreeview(), Folders, "", "ID", idref("FileTreeview"), "DataContext", "TreeDataContext", "MultiSelect", "false", "Root", "true") as Sitecore.Web.UI.HtmlControls.DataTreeview;
      FileExplorerLeft = AddControl("VSplitter", "", Body, "", "ID", idref("FileExplorerLeft"), "GridPanel.Width", "4", "Target", "left", "GridPanel.Style", "background:#e9e9e9; display:expression(previousSibling.style.display)") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox16 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Body, "", "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "ContextMenu", "show:ContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileExplorer = AddControl(new Sitecore.Web.UI.HtmlControls.DataListview(), scrollbox16, "", "ID", idref("FileExplorer"), "DataContext", "DataContext", "DblClick", "AddFile") as Sitecore.Web.UI.HtmlControls.DataListview;
      listviewheader17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), FileExplorer, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader17, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem19 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader17, "", "Name", "size", "Header", "Size") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem20 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader17, "", "Name", "modified", "Header", "Date Modified") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      space21 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel15, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      toolbutton23 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border22, "", "Style", "float:right", "Icon", "Applications/24x24/delete2.png", "Header", "Remove", "ToolTip", "Remove entry from selection list.", "Click", "Remove(\"\")") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton24 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border22, "", "Icon", "Applications/24x24/add.png", "Header", "Add path", "ToolTip", "Adds a path to selection list to include into the package.", "Click", "AddFile") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space25 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel15, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      ItemListBox = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel15, "", "ID", idref("ItemListBox"), "Width", "100%", "GridPanel.Height", "150px", "Height", "100%", "ContextMenu", "ListContextMenu()") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ItemList = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ItemListBox, "", "ID", idref("ItemList"), "Width", "100%", "View", "Details", "MultiSelect", "true") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader26 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), ItemList, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem27 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader26, "", "Header", "Selected paths:") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      SetName = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("SetName"), "Header", "Source Name", "Text", "Enter a name for the source. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent28 = AddControl("WizardFormIndent", "", SetName, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Name = AddControl("Installer.NameEditor", "", wizardformindent28, "", "ID", idref("Name")) as Sitecore.Web.UI.XmlControls.XmlControl;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border29 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal30 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border29, "", "Text", "The static file source has been added successfully.") as Sitecore.Web.UI.HtmlControls.Literal;
      border31 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal32 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border31, "", "Text", "Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Internals, "ID", idref("Internals"));
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(LoadFileSource, "ID", idref("LoadFileSource"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(TreeDataContext, "ID", idref("TreeDataContext"));
      SetProperty(UpAction, "ID", idref("UpAction"));
      SetProperty(ContextMenu, "ID", idref("ContextMenu"));
      SetProperty(Body, "ID", idref("Body"));
      SetProperty(Folders, "ID", idref("Folders"));
      SetProperty(FileTreeview, "ID", idref("FileTreeview"));
      SetProperty(FileExplorerLeft, "ID", idref("FileExplorerLeft"));
      SetProperty(FileExplorer, "ID", idref("FileExplorer"));
      SetProperty(ItemListBox, "ID", idref("ItemListBox"));
      SetProperty(ItemList, "ID", idref("ItemList"));
      SetProperty(SetName, "ID", idref("SetName"));
      SetProperty(Name, "ID", idref("Name"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

