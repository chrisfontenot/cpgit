using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FileSourceEditor_a_78 : Sitecore.Shell.Applications.Install.Controls.FileSourceEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Tab tab1;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_filesourcerooteditor2;
    public Sitecore.Web.UI.HtmlControls.Tab tab3;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_filesfiltereditor4;
    public Sitecore.Web.UI.HtmlControls.Tab tab5;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_sourceoutputeditor6;
    public Sitecore.Web.UI.HtmlControls.Tab tab7;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_sourceviewer8;
    public Sitecore.Web.UI.HtmlControls.Tab tab9;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_nameeditor10;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Tabs = AddControl(new Sitecore.Web.UI.HtmlControls.Tabstrip(), this, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Tabstrip;
      tab1 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Search Root") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_filesourcerooteditor2 = AddControl("Installer.FileSourceRootEditor", "", tab1, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab3 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Filters") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_filesfiltereditor4 = AddControl("Installer.FilesFilterEditor", "", tab3, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab5 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Installation options") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_sourceoutputeditor6 = AddControl("Installer.SourceOutputEditor", "", tab5, "", "Padding", "4px", "MergeOption", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab7 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Preview") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_sourceviewer8 = AddControl("Installer.SourceViewer", "", tab7, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab9 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Name") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_nameeditor10 = AddControl("Installer.NameEditor", "", tab9, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

