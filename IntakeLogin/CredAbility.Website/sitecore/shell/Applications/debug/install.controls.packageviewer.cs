using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_PackageViewer_a_79 : Sitecore.Shell.Applications.Install.Controls.SourceViewer   {
    
    // variables
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader4;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem5;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem6;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem7;
    
    public string m_Padding;
    public string m_Style;
    
    // properties
    public new string Padding {
      get {
        return StringUtil.GetString(m_Padding);
      }
      set {
        m_Padding = value;
        
        SetProperty(gridpanel1, "Padding", Padding);
      }
    }
    
    public new string Style {
      get {
        return StringUtil.GetString(m_Style);
      }
      set {
        m_Style = value;
        
        SetProperty(gridpanel1, "Style", Style);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gridpanel1 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Width", "100%", "Height", "100%", "Padding", Padding, "Style", Style) as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel1, "", "Width", "100%", "Columns", "3", "GridPanel.Height", "20px") as Sitecore.Web.UI.WebControls.GridPanel;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel2, "", "Text", "Search for key:", "Style", "font-weight:bold", "GridPanel.Width", "100px") as Sitecore.Web.UI.HtmlControls.Literal;
      Filter = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel2, "", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      ButtonFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "GridPanel.Width", "50px") as Sitecore.Web.UI.HtmlControls.Border;
      ViewScroll = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel1, "", "Width", "100%", "Height", "100%", "Border", "solid 1px silver") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      View = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ViewScroll, "", "View", "Details") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader4 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), View, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem5 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader4, "", "Header", "Entry Key") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem6 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader4, "", "Header", "Source", "Name", "source") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem7 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader4, "", "Header", "Installation options", "Name", "options") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
  }
}

