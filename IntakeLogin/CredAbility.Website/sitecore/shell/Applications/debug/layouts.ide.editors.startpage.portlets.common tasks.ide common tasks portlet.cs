using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_CommonTasksPortlet_a_121 : Sitecore.Shell.Applications.Layouts.IDE.Editors.Startpage.Portlets.CommonTasks.IDECommonTasksPortletXmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl SDN5;
    public Sitecore.Web.UI.XmlControls.XmlControl RestoreWorkspace;
    public Sitecore.Web.UI.XmlControls.XmlControl StartDebugger;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Portlet = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "") as Sitecore.Web.UI.HtmlControls.Border;
      Window = AddControl("DefaultPortletWindow", "", Portlet, "", "Header", "Common Tasks", "Icon", "Applications/16x16/nav_right_green.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      SDN5 = AddControl("IDE.TaskOption", "", Window, "", "ID", idref("SDN5"), "Header", "Open the SDN5 Website", "Click", "CommonTasksPortlet.OpenSDN5") as Sitecore.Web.UI.XmlControls.XmlControl;
      RestoreWorkspace = AddControl("IDE.TaskOption", "", Window, "", "ID", idref("RestoreWorkspace"), "Header", "Restore Workspace", "Click", "CommonTasksPortlet.Click(\"ide:restoreworkspace\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      StartDebugger = AddControl("IDE.TaskOption", "", Window, "", "ID", idref("StartDebugger"), "Header", "Start Debugger", "Click", "system:debug") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(SDN5, "ID", idref("SDN5"));
      SetProperty(RestoreWorkspace, "ID", idref("RestoreWorkspace"));
      SetProperty(StartDebugger, "ID", idref("StartDebugger"));
    }
  }
}

