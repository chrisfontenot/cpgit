using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class LogViewer_a_117 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Action HasFile;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.XmlControls.XmlControl Commandbar;
    public Sitecore.Web.UI.HtmlControls.Frame Document;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Reports/LogViewer") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Reports.LogViewer.LogViewerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      HasFile = AddControl(new Sitecore.Web.UI.HtmlControls.Action(), formpage1, "", "ID", idref("HasFile"), "Disabled", "true") as Sitecore.Web.UI.HtmlControls.Action;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Commandbar = AddControl("Commandbar", "", gridpanel3, "", "ID", idref("Commandbar"), "Icon", "Software/32x32/text_code_colored.png", "Title", "Log files", "Description", "This tool displays the content of log files.", "DataSource", "/sitecore/content/Applications/Reports/LogViewer/Commands") as Sitecore.Web.UI.XmlControls.XmlControl;
      Document = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), gridpanel3, "", "ID", idref("Document"), "XmlControl", "LogViewerDetails", "Height", "100%", "Width", "100%", "GridPanel.Width", "100%", "GridPanel.Height", "100%", "Background", "window") as Sitecore.Web.UI.HtmlControls.Frame;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(HasFile, "ID", idref("HasFile"));
      SetProperty(Commandbar, "ID", idref("Commandbar"));
      SetProperty(Document, "ID", idref("Document"));
    }
  }
}

