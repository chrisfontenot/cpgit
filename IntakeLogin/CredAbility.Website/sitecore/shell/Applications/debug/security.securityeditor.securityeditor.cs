using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SecurityEditor_a_91 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.Script script7;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside8;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public System.Web.UI.Control scActiveRibbonStrip;
    public Sitecore.Web.UI.WebControls.GridPanel Grid;
    public Sitecore.Web.UI.HtmlControls.Border RibbonPanel;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox10;
    public System.Web.UI.Control Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl securityeditorlegend11;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Security/Security Editor") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Security editor.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      script7 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Security/SecurityEditor/SecurityEditor.js") as Sitecore.Web.UI.HtmlControls.Script;
      codebeside8 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Security.SecurityEditor.SecurityEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext"), "Root", "{11111111-1111-1111-1111-111111111111}") as Sitecore.Web.UI.HtmlControls.DataContext;
      scActiveRibbonStrip = AddControl("input", "", formpage1, "", "type", "hidden", "id", idref("scActiveRibbonStrip"), "name", "scActiveRibbonStrip");
      Grid = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "ID", idref("Grid"), "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      RibbonPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Grid, "", "ID", idref("RibbonPanel"), "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Grid, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.Width", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox10 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Width", "100%", "Height", "100%", "Border", "none", "Padding", "0px", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl("shell:SecurityTreeview", "", scrollbox10, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Root", "true", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")", "AllowDragging", "true");
      securityeditorlegend11 = AddControl("SecurityEditorLegend", "", Grid, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(scActiveRibbonStrip, "id", idref("scActiveRibbonStrip"));
      SetProperty(Grid, "ID", idref("Grid"));
      SetProperty(RibbonPanel, "ID", idref("RibbonPanel"));
      SetProperty(Treeview, "ID", idref("Treeview"));
    }
  }
}

