using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDEHtmlEditor_a_138 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside5;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Frame GridDesigner;
    public Sitecore.Web.UI.HtmlControls.Frame Editor;
    public Sitecore.Web.UI.HtmlControls.Border HtmlEditorPane;
    public Sitecore.Web.UI.HtmlControls.Memo HtmlEditor;
    public Sitecore.Web.UI.HtmlControls.Border CodeEditorPane;
    public Sitecore.Web.UI.HtmlControls.Memo CodeEditor;
    public Sitecore.Web.UI.HtmlControls.Frame ContentEditor;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar8;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Toolbutton GridButton;
    public Sitecore.Web.UI.HtmlControls.Toolbutton DesignButton;
    public Sitecore.Web.UI.HtmlControls.Toolbutton HtmlButton;
    public Sitecore.Web.UI.HtmlControls.Toolbutton CodeButton;
    public Sitecore.Web.UI.HtmlControls.Toolbutton ContentButton;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage11;
    public Sitecore.Web.UI.HtmlControls.Toolbutton LiveButton;
    public Sitecore.Web.UI.HtmlControls.Space space12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.TreePicker DataSource;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton14;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/SitecoreKeyboard.js") as Sitecore.Web.UI.HtmlControls.Script;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/controls/SitecoreModifiedHandling.js") as Sitecore.Web.UI.HtmlControls.Script;
      codebeside5 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Editors.HTML.IDEHtmlEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("DataContext"), "Root", "/sitecore/content", "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Height", "100%", "GridPanel.Width", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      GridDesigner = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), border7, "", "ID", idref("GridDesigner"), "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Frame;
      Editor = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), border7, "", "ID", idref("Editor"), "Width", "100%", "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Frame;
      HtmlEditorPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "ID", idref("HtmlEditorPane"), "Visible", "false", "Height", "100%", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      HtmlEditor = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), HtmlEditorPane, "", "ID", idref("HtmlEditor"), "Width", "100%", "Height", "100%", "Border", "none", "Wrap", "off") as Sitecore.Web.UI.HtmlControls.Memo;
      CodeEditorPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "ID", idref("CodeEditorPane"), "Visible", "false", "Height", "100%", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      CodeEditor = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), CodeEditorPane, "", "ID", idref("CodeEditor"), "Width", "100%", "Height", "100%", "Border", "none", "Wrap", "off") as Sitecore.Web.UI.HtmlControls.Memo;
      ContentEditor = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), border7, "", "ID", idref("ContentEditor"), "Width", "100%", "Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Frame;
      toolbar8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), gridpanel6, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), toolbar8, "", "Height", "21", "Columns", "3") as Sitecore.Web.UI.WebControls.GridPanel;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel9, "", "NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Border;
      GridButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("GridButton"), "Header", "Grid", "Icon", "Business/16x16/table_edit.png", "IconSize", "id16x16", "Down", "true", "Click", "ShowGrid", "ToolTip", "Show Grid Editor Mode") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      DesignButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("DesignButton"), "Header", "Design", "Icon", "Applications/16x16/text_view.png", "IconSize", "id16x16", "Down", "false", "Click", "ShowDesign", "ToolTip", "Show Design Mode") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      HtmlButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("HtmlButton"), "Header", "HTML", "Icon", "Software/16x16/text_code.png", "IconSize", "id16x16", "Click", "ShowHtml", "ToolTip", "Show HTML Codes") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      CodeButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("CodeButton"), "Header", "Code", "Icon", "Software/16x16/text_code_csharp.png", "IconSize", "id16x16", "Click", "ShowCode", "ToolTip", "Show Code Behind") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      ContentButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("ContentButton"), "Header", "Content", "Icon", "People/16x16/cube_blue.png", "IconSize", "id16x16", "Click", "ShowContent", "ToolTip", "Show Content Item") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      themedimage11 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border10, "", "Src", "Images/draghandle9x15.png", "Width", "9", "Height", "15", "Align", "Bottom") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      LiveButton = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border10, "", "ID", idref("LiveButton"), "Icon", "Network/16x16/earth_view.png", "IconSize", "id16x16", "Click", "ShowLiveMode", "ToolTip", "Toggle Live Mode") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space12 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border10, "", "Width", "4") as Sitecore.Web.UI.HtmlControls.Space;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel9, "", "Padding", "1px 0px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      DataSource = AddControl(new Sitecore.Web.UI.HtmlControls.TreePicker(), border13, "", "ID", idref("DataSource"), "DataContext", "DataContext", "Width", "256px", "Changed", "SetDataSource", "SelectOnly", "true") as Sitecore.Web.UI.HtmlControls.TreePicker;
      toolbutton14 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel9, "", "Icon", "Applications/24x24/refresh.png", "IconSize", "id16x16", "Click", "SetDataSource") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(GridDesigner, "ID", idref("GridDesigner"));
      SetProperty(Editor, "ID", idref("Editor"));
      SetProperty(HtmlEditorPane, "ID", idref("HtmlEditorPane"));
      SetProperty(HtmlEditor, "ID", idref("HtmlEditor"));
      SetProperty(CodeEditorPane, "ID", idref("CodeEditorPane"));
      SetProperty(CodeEditor, "ID", idref("CodeEditor"));
      SetProperty(ContentEditor, "ID", idref("ContentEditor"));
      SetProperty(GridButton, "ID", idref("GridButton"));
      SetProperty(DesignButton, "ID", idref("DesignButton"));
      SetProperty(HtmlButton, "ID", idref("HtmlButton"));
      SetProperty(CodeButton, "ID", idref("CodeButton"));
      SetProperty(ContentButton, "ID", idref("ContentButton"));
      SetProperty(LiveButton, "ID", idref("LiveButton"));
      SetProperty(DataSource, "ID", idref("DataSource"));
    }
  }
}

