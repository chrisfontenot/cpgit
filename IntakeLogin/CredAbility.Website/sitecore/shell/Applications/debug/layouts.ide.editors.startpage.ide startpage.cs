using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_Startpage_a_115 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.Portal.Portal IDEStartpagePortal;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.HtmlControls.Space space12;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel13;
    public Sitecore.Web.UI.Portal.PortalZone Left;
    public Sitecore.Web.UI.Portal.PortalZone Mid;
    public Sitecore.Web.UI.HtmlControls.Frame Sidebar;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Scroll", "yes") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Editors.Startpage.IDEStartpageForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "/sitecore/portal/ide/ide.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      IDEStartpagePortal = AddControl(new Sitecore.Web.UI.Portal.Portal(), formpage1, "", "ID", idref("IDEStartpagePortal"), "DataSource", "/sitecore/content/Applications/Layouts/IDE/Portal", "RefreshPageOnRedraw", "true") as Sitecore.Web.UI.Portal.Portal;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), IDEStartpagePortal, "", "Class", "Portal", "Background", "#ebf1fe") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border4, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Background", "#7c94c8", "Padding", "8", "style", "filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#9bacd5', EndColorStr='#7189bd');", "Width", "100%", "Height", "56") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border6, "", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage8 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel7, "", "Src", "People/48x48/cubes.png", "Width", "48", "Height", "48", "Margin", "0px 8px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "FontName", "Franklin Gothic Medium", "FontSize", "20", "Foreground", "white", "Padding", "0px", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Developer Center") as Sitecore.Web.UI.HtmlControls.Literal;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "") as Sitecore.Web.UI.HtmlControls.Border;
      space12 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border11, "") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel13 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel5, "", "Columns", "2", "Width", "100%", "Height", "100%", "CellSpacing", "4", "CellPadding", "4", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Left = AddControl(new Sitecore.Web.UI.Portal.PortalZone(), gridpanel13, "", "ID", idref("Left"), "GridPanel.Width", "33%", "GridPanel.Class", "PortalColumn", "GridPanel.VAlign", "top", "GridPanel.Height", "100%") as Sitecore.Web.UI.Portal.PortalZone;
      Mid = AddControl(new Sitecore.Web.UI.Portal.PortalZone(), gridpanel13, "", "ID", idref("Mid"), "GridPanel.Width", "66%", "GridPanel.Class", "PortalColumn", "GridPanel.VAlign", "top") as Sitecore.Web.UI.Portal.PortalZone;
      Sidebar = AddControl(new Sitecore.Web.UI.HtmlControls.Frame(), formpage1, "", "ID", idref("Sidebar"), "Width", "6", "Height", "100%", "Style", "position:absolute; left:expression(parentNode.clientWidth-6); top:0") as Sitecore.Web.UI.HtmlControls.Frame;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(IDEStartpagePortal, "ID", idref("IDEStartpagePortal"));
      SetProperty(Left, "ID", idref("Left"));
      SetProperty(Mid, "ID", idref("Mid"));
      SetProperty(Sidebar, "ID", idref("Sidebar"));
    }
  }
}

