using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class TreeListExEditor_a_46 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet1;
    public System.Web.UI.Control text2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public System.Web.UI.Control TreeList;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "Control/32x32/treeview.png", "Header", "Select Items", "Text", "Please select the items that you wish to use.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet1 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), Dialog, "", "Key", "TreeListExEditor") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text2 = AddLiteral("        .scContentControl {        height:100%;        }        .scContentControlMultilistBox {        height:100%;        }      ", "", stylesheet1, "");
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.ContentEditor.Dialogs.TreeListExEditor.TreeListExEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Cellpadding", "0", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      TreeList = AddControl("content:TreeList", "", gridpanel4, "", "ID", idref("TreeList"), "Activation", "false", "Style", "background:#e9e9e9;border:none;padding: 0px 4px 4px 4px");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(TreeList, "ID", idref("TreeList"));
    }
  }
}

