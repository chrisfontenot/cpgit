using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class CopyTo_a_93 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox4;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Edit Filename;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Core3/24x24/Copy_To_Folder.png", "Header", "Copy Item To", "Text", "Select the location where you want to copy the item to.", "OKButton", "Copy") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.CopyTo.CopyToForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("DataContext"), "Root", "/") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox4 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize scInsetBorder", "Background", "white", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox4, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Click", "SelectTreeNode", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Padding", "4px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border5, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "Padding", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", "Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filename = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel6, "", "ID", idref("Filename"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(Filename, "ID", idref("Filename"));
    }
  }
}

