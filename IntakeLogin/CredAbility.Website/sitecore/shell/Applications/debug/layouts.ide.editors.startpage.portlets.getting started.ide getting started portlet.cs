using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_GettingStartedPortlet_a_120 : Sitecore.Shell.Applications.Layouts.IDE.Editors.Startpage.Portlets.GettingStarted.IDEGettingStartedPortletXmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.XmlControls.XmlControl NewLayout;
    public Sitecore.Web.UI.XmlControls.XmlControl NewXSLT;
    public Sitecore.Web.UI.XmlControls.XmlControl NewSublayout;
    public Sitecore.Web.UI.XmlControls.XmlControl NewFile;
    public Sitecore.Web.UI.XmlControls.XmlControl Open;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Portlet = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "") as Sitecore.Web.UI.HtmlControls.Border;
      Window = AddControl("DefaultPortletWindow", "", Portlet, "", "Header", "Getting Started", "Icon", "People/16x16/cubes.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Window, "") as Sitecore.Web.UI.HtmlControls.Border;
      NewLayout = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("NewLayout"), "Header", "Create a New Layout", "Click", "GettingStartedPortlet.Click(\"ide:startwizard(itemid={6F8DE850-B12E-4715-9E27-EF5F49E354E9})\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      NewXSLT = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("NewXSLT"), "Header", "Create a New XSLT Rendering", "Click", "GettingStartedPortlet.Click(\"ide:startwizard(itemid={EB940DD0-30AF-420A-9258-809723520DA7})\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      NewSublayout = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("NewSublayout"), "Header", "Create a New Sublayout", "Click", "GettingStartedPortlet.Click(\"ide:startwizard(itemid={85BA99CF-611E-413D-97C2-C71AA8A4063C})\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      NewFile = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("NewFile"), "Header", "Create a File", "Click", "GettingStartedPortlet.Click(\"ide:newfile\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      Open = AddControl("IDE.TaskOption", "", border1, "", "ID", idref("Open"), "Header", "Open an Existing Solution Item", "Click", "GettingStartedPortlet.Click(\"ide:open\")") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(NewLayout, "ID", idref("NewLayout"));
      SetProperty(NewXSLT, "ID", idref("NewXSLT"));
      SetProperty(NewSublayout, "ID", idref("NewSublayout"));
      SetProperty(NewFile, "ID", idref("NewFile"));
      SetProperty(Open, "ID", idref("Open"));
    }
  }
}

