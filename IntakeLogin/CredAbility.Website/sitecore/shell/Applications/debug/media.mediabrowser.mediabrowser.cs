using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Sitecore_Shell_Applications_Media_MediaBrowser_a_87 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.HtmlControls.DataContext MediaDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox6;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl MediaBrowserLeft;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Listview;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Edit Filename;
    public Sitecore.Web.UI.HtmlControls.Button button11;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/photo_scenery.png", "Header", "Media Browser", "Submittable", "false", "Text", "Select the image or file that you want to open.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Src", "MediaBrowser.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Media.MediaBrowser.MediaBrowserForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      MediaDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("MediaDataContext"), "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel4, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed", "Columns", "3", "GridPanel.Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox6 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize", "Background", "white", "Padding", "0px", "GridPanel.Height", "100%", "GridPanel.Width", "200", "GridPanel.VAlign", "top", "Style", "overflow:auto") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox6, "", "ID", idref("Treeview"), "DataContext", "MediaDataContext", "Click", "SelectTreeNode", "ShowRoot", "true", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")", "Width", "100%") as Sitecore.Web.UI.WebControls.TreeviewEx;
      MediaBrowserLeft = AddControl("VSplitter", "", gridpanel5, "", "ID", idref("MediaBrowserLeft"), "Target", "left") as Sitecore.Web.UI.XmlControls.XmlControl;
      Listview = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "ID", idref("Listview"), "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize", "Background", "white", "Padding", "0px", "GridPanel.Height", "100%", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "", "Class", "filenameContainer") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border7, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel8, "", "Class", "name") as Sitecore.Web.UI.HtmlControls.Border;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border9, "", "Text", "Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filename = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Filename"), "Class", "filename", "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      button11 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), formdialog1, "Buttons", "Header", "Upload", "Click", "UploadImage") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(MediaDataContext, "ID", idref("MediaDataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(MediaBrowserLeft, "ID", idref("MediaBrowserLeft"));
      SetProperty(Listview, "ID", idref("Listview"));
      SetProperty(Filename, "ID", idref("Filename"));
    }
  }
}

