using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class SetBaseTemplates_a_81 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public System.Web.UI.Control TreeList;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Software/32x32/components.png", "Header", "Base Templates", "Text", "Select the base template for the current template.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "SetBaseTemplates") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scContentControl {        height:100%;        }        .scContentControlMultilistBox {        height:100%;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Templates.SetBaseTemplates.SetBaseTemplatesForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      TreeList = AddControl("content:TreeList", "", formdialog1, "", "ID", idref("TreeList"), "Source", "/sitecore/templates", "Activation", "false", "Style", "background:#e9e9e9;border:none", "IncludeTemplatesForSelection", "Template", "IncludeTemplatesForDisplay", "Template,Template Folder,Folder,Node");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(TreeList, "ID", idref("TreeList"));
    }
  }
}

