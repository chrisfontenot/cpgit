using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_AddFilesDialog_a_83 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.HtmlControls.DataContext TreeDataContext;
    public Sitecore.Web.UI.HtmlControls.UpAction UpAction;
    public Sitecore.Web.UI.HtmlControls.ContextMenu ContextMenu;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem3;
    public Sitecore.Web.UI.HtmlControls.MenuDivider menudivider4;
    public Sitecore.Web.UI.XmlControls.XmlControl listviewviewsmenuitems5;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel6;
    public Sitecore.Web.UI.WebControls.GridPanel Body;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Folders;
    public Sitecore.Web.UI.HtmlControls.DataTreeview FileTreeview;
    public Sitecore.Web.UI.XmlControls.XmlControl FileExplorerLeft;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox7;
    public Sitecore.Web.UI.HtmlControls.DataListview FileExplorer;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader8;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem9;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem10;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem11;
    public Sitecore.Web.UI.HtmlControls.Space space12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton14;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton15;
    public Sitecore.Web.UI.HtmlControls.Space space16;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ItemListBox;
    public Sitecore.Web.UI.HtmlControls.Listview ItemList;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader17;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem18;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/24x24/folder_view.png", "Header", "Add Files", "Text", "Pick the files and folders that you want to include in the package.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Install.Dialogs.AddFilesDialog,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("DataContext"), "DataContext", "TreeDataContext", "DefaultItem", "/") as Sitecore.Web.UI.HtmlControls.DataContext;
      TreeDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("TreeDataContext"), "DataContext", "DataContext", "DefaultItem", "/", "Filter", "Contains('" + Sitecore.Data.DataProviders.FileSystemDataProvider.FolderID.ToString() + "', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      UpAction = AddControl(new Sitecore.Web.UI.HtmlControls.UpAction(), formdialog1, "", "ID", idref("UpAction"), "DataContext", "DataContext") as Sitecore.Web.UI.HtmlControls.UpAction;
      ContextMenu = AddControl(new Sitecore.Web.UI.HtmlControls.ContextMenu(), formdialog1, "", "ID", idref("ContextMenu")) as Sitecore.Web.UI.HtmlControls.ContextMenu;
      menuitem3 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Icon", "Applications/16x16/folder_up.png", "Header", "Up", "Action", "UpAction", "Click", "DataContext.Up") as Sitecore.Web.UI.HtmlControls.MenuItem;
      menudivider4 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuDivider(), ContextMenu, "") as Sitecore.Web.UI.HtmlControls.MenuDivider;
      listviewviewsmenuitems5 = AddControl("ListviewViewsMenuItems", "", ContextMenu, "", "Listview", "FileExplorer") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel6 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Body = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel6, "", "ID", idref("Body"), "Columns", "3", "Width", "100%", "Height", "100%", "Fixed", "true", "Border", "1px inset") as Sitecore.Web.UI.WebControls.GridPanel;
      Folders = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Body, "", "ID", idref("Folders"), "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "Style", "filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1, StartColorStr='#ffffff', EndColorStr='#DFE6EC')", "GridPanel.ID", "FoldersPane", "GridPanel.Width", "200") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileTreeview = AddControl(new Sitecore.Web.UI.HtmlControls.DataTreeview(), Folders, "", "ID", idref("FileTreeview"), "DataContext", "TreeDataContext", "MultiSelect", "false", "Root", "true") as Sitecore.Web.UI.HtmlControls.DataTreeview;
      FileExplorerLeft = AddControl("VSplitter", "", Body, "", "ID", idref("FileExplorerLeft"), "GridPanel.Width", "4", "Target", "left", "GridPanel.Style", "background:#e9e9e9; display:expression(previousSibling.style.display)") as Sitecore.Web.UI.XmlControls.XmlControl;
      scrollbox7 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), Body, "", "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "ContextMenu", "show:ContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      FileExplorer = AddControl(new Sitecore.Web.UI.HtmlControls.DataListview(), scrollbox7, "", "ID", idref("FileExplorer"), "DataContext", "DataContext", "DblClick", "AddFile") as Sitecore.Web.UI.HtmlControls.DataListview;
      listviewheader8 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), FileExplorer, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem9 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader8, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem10 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader8, "", "Name", "size", "Header", "Size") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem11 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader8, "", "Name", "modified", "Header", "Date Modified") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      space12 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel6, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel6, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      toolbutton14 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Style", "float:right", "Icon", "Applications/24x24/delete2.png", "Header", "Remove", "ToolTip", "Remove entry from selection list.", "Click", "Remove(\"\")") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton15 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Icon", "Applications/24x24/add.png", "Header", "Add File", "ToolTip", "Adds a file to the package.", "Click", "AddFile") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space16 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel6, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      ItemListBox = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel6, "", "ID", idref("ItemListBox"), "Width", "100%", "GridPanel.Height", "150px", "Height", "100%", "ContextMenu", "ListContextMenu()") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ItemList = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ItemListBox, "", "ID", idref("ItemList"), "Width", "100%", "View", "Details") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), ItemList, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader17, "", "Header", "Selected paths:") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(TreeDataContext, "ID", idref("TreeDataContext"));
      SetProperty(UpAction, "ID", idref("UpAction"));
      SetProperty(ContextMenu, "ID", idref("ContextMenu"));
      SetProperty(Body, "ID", idref("Body"));
      SetProperty(Folders, "ID", idref("Folders"));
      SetProperty(FileTreeview, "ID", idref("FileTreeview"));
      SetProperty(FileExplorerLeft, "ID", idref("FileExplorerLeft"));
      SetProperty(FileExplorer, "ID", idref("FileExplorer"));
      SetProperty(ItemListBox, "ID", idref("ItemListBox"));
      SetProperty(ItemList, "ID", idref("ItemList"));
    }
  }
}

