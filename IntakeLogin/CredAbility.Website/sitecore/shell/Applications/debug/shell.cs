using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Shell_a_50 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public Sitecore.Web.UI.HtmlControls.Script script6;
    public Sitecore.Web.UI.HtmlControls.Script script7;
    public System.Web.UI.Control text8;
    public Sitecore.Web.UI.XmlControls.XmlControl formpage9;
    public System.Web.UI.Control Wallpaper;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Border Desktop;
    public Sitecore.Web.UI.HtmlControls.Border Links;
    public Sitecore.Web.UI.XmlControls.XmlControl startbar12;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), this, "", "Type", "Sitecore.Shell.Applications.ShellForm,Sitecore.Client", "FrameName", "Shell") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Shell.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "Startbar.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreObjects.js") as Sitecore.Web.UI.HtmlControls.Script;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/SitecoreWindowManager.js") as Sitecore.Web.UI.HtmlControls.Script;
      script6 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/controls/lib/scriptaculous/scriptaculous.js?load=effects") as Sitecore.Web.UI.HtmlControls.Script;
      script7 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "key", "TrackModified") as Sitecore.Web.UI.HtmlControls.Script;
      text8 = AddLiteral("      scSitecore.prototype.setModified = function(value) {        this.modified = false;      }    ", "", script7, "");
      formpage9 = AddControl("FormPage", "", this, "", "Submittable", "false", "TrackModified", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      Wallpaper = AddControl("img", "", formpage9, "", "ID", idref("Wallpaper"), "src", "/sitecore/shell/Themes/Backgrounds/working.jpg", "alt", "", "border", "0");
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage9, "", "KeyDown", "ShowStartMenu", "KeyFilter", "c91") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border10, "", "Class", "scFill scFixed") as Sitecore.Web.UI.WebControls.GridPanel;
      Desktop = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel11, "", "ID", idref("Desktop"), "GridPanel.Class", "scFill", "ContextMenu", "ShowContextMenu") as Sitecore.Web.UI.HtmlControls.Border;
      Links = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Desktop, "", "ID", idref("Links"), "DblClick", "Launch") as Sitecore.Web.UI.HtmlControls.Border;
      startbar12 = AddControl("Startbar", "", gridpanel11, "", "GridPanel.Class", "scStartbar") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Wallpaper, "ID", idref("Wallpaper"));
      SetProperty(Desktop, "ID", idref("Desktop"));
      SetProperty(Links, "ID", idref("Links"));
    }
  }
}

