using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Aliases_a_123 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Edit NewAlias;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Button Add;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Listbox List;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Button Remove;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/24x24/spy.png", "Header", "Aliases", "Text", "Add or remove URL aliases for the current item.", "OKButton", "Close", "CancelButton", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.Aliases.AliasesForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Columns", "2", "CellPadding", "4", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      NewAlias = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border4, "", "ID", idref("NewAlias"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "") as Sitecore.Web.UI.HtmlControls.Border;
      Add = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border5, "", "ID", idref("Add"), "Header", "Add", "Click", "Add_Click") as Sitecore.Web.UI.HtmlControls.Button;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.VAlign", "top", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      List = AddControl(new Sitecore.Web.UI.HtmlControls.Listbox(), border6, "", "ID", idref("List"), "Height", "100%", "Width", "100%", "Size", "8", "Multiple", "true") as Sitecore.Web.UI.HtmlControls.Listbox;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "Height", "100%", "GridPanel.Height", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      Remove = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border7, "", "ID", idref("Remove"), "Header", "Remove", "Click", "Remove_Click") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(NewAlias, "ID", idref("NewAlias"));
      SetProperty(Add, "ID", idref("Add"));
      SetProperty(List, "ID", idref("List"));
      SetProperty(Remove, "ID", idref("Remove"));
    }
  }
}

