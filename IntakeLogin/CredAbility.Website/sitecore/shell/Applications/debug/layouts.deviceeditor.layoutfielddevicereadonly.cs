using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class LayoutFieldDeviceReadOnly_a_74 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Border border3;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage4;
    public System.Web.UI.Control br5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Space space11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.HtmlControls.Border ControlsPane;
    public Sitecore.Web.UI.HtmlControls.Border PlaceholdersPane;
    
    public string m_DblClick;
    public string m_DeviceIcon;
    public string m_DeviceName;
    public string m_LayoutName;
    
    // properties
    public string DblClick {
      get {
        return StringUtil.GetString(m_DblClick);
      }
      set {
        m_DblClick = value;
        
        SetProperty(border3, "DblClick", DblClick);
      }
    }
    
    public string DeviceIcon {
      get {
        return StringUtil.GetString(m_DeviceIcon);
      }
      set {
        m_DeviceIcon = value;
        
        SetProperty(themedimage4, "Src", DeviceIcon);
      }
    }
    
    public string DeviceName {
      get {
        return StringUtil.GetString(m_DeviceName);
      }
      set {
        m_DeviceName = value;
        
        SetProperty(literal6, "Text", DeviceName);
      }
    }
    
    public string LayoutName {
      get {
        return StringUtil.GetString(m_LayoutName);
      }
      set {
        m_LayoutName = value;
        
        SetProperty(literal9, "Text", LayoutName);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Class", "scContentControlLayoutDevice", "background", "#e9e9e9") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Width", "100%", "Height", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border3 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Align", "center", "Class", "scContentControlLayoutDeviceName", "DblClick", DblClick, "GridPanel.NoWrap", "true", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage4 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border3, "", "Src", DeviceIcon, "Height", "32", "Width", "32", "Disabled", "true") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      br5 = AddControl("br", "", border3, "");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border3, "", "Text", DeviceName) as Sitecore.Web.UI.HtmlControls.Literal;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Class", "scContentControlLayoutDeviceRenderings", "GridPanel.Width", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Class", "scRollOver", "Foreground", "#666666") as Sitecore.Web.UI.HtmlControls.Border;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "Class", "scClickFont", "Style", "font-weight:bold", "Text", LayoutName) as Sitecore.Web.UI.HtmlControls.Literal;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Background", "#dddddd", "Margin", "4px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      space11 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border10, "") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border7, "", "Width", "100%", "Columns", "2", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.Width", "50%") as Sitecore.Web.UI.HtmlControls.Border;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", "Controls") as Sitecore.Web.UI.HtmlControls.Literal;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.Width", "50%") as Sitecore.Web.UI.HtmlControls.Border;
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border15, "", "Text", "Placeholders") as Sitecore.Web.UI.HtmlControls.Literal;
      ControlsPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      PlaceholdersPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
  }
}

