using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Publish_a_107 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.XmlControls.XmlControl Settings;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformpadding11;
    public Sitecore.Web.UI.HtmlControls.Scrollbox SettingsPane;
    public Sitecore.Web.UI.HtmlControls.Groupbox PublishingPanel;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Border IncrementalPublishPane;
    public Sitecore.Web.UI.HtmlControls.Radiobutton IncrementalPublish;
    public System.Web.UI.Control br13;
    public Sitecore.Web.UI.HtmlControls.Radiobutton SmartPublish;
    public System.Web.UI.Control br14;
    public Sitecore.Web.UI.HtmlControls.Radiobutton Republish;
    public Sitecore.Web.UI.HtmlControls.Border PublishChildrenPane;
    public Sitecore.Web.UI.HtmlControls.Checkbox PublishChildren;
    public Sitecore.Web.UI.HtmlControls.Groupbox LanguagesPanel;
    public Sitecore.Web.UI.HtmlControls.Border Languages;
    public Sitecore.Web.UI.HtmlControls.Groupbox PublishingTargetsPanel;
    public Sitecore.Web.UI.HtmlControls.Border PublishingTargets;
    public Sitecore.Web.UI.HtmlControls.Border NoPublishingTarget;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage15;
    public System.Web.UI.Control b16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.XmlControls.XmlControl Publishing;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel18;
    public Sitecore.Web.UI.HtmlControls.Space space19;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage20;
    public Sitecore.Web.UI.HtmlControls.Space space21;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage23;
    public Sitecore.Web.UI.HtmlControls.Space space24;
    public Sitecore.Web.UI.HtmlControls.Border PublishingText;
    public Sitecore.Web.UI.HtmlControls.Literal literal25;
    public Sitecore.Web.UI.HtmlControls.Space space26;
    public Sitecore.Web.UI.HtmlControls.Border PublishingTarget;
    public System.Web.UI.Control text27;
    public Sitecore.Web.UI.XmlControls.XmlControl Retry;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformpadding28;
    public Sitecore.Web.UI.HtmlControls.Memo ErrorText;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel29;
    public Sitecore.Web.UI.HtmlControls.Border border30;
    public Sitecore.Web.UI.HtmlControls.Literal literal31;
    public Sitecore.Web.UI.HtmlControls.Border border32;
    public Sitecore.Web.UI.HtmlControls.Literal Status;
    public Sitecore.Web.UI.HtmlControls.Border ShowResultPane;
    public Sitecore.Web.UI.HtmlControls.Literal literal33;
    public Sitecore.Web.UI.HtmlControls.Border ResultLabel;
    public Sitecore.Web.UI.HtmlControls.Literal literal34;
    public Sitecore.Web.UI.HtmlControls.Memo ResultText;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Dialogs.Publish.PublishForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Custom/32x32/Publish.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Publish Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Choose publishing mode.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Select the languages to publish.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Select the publishing targets.") as Sitecore.Web.UI.HtmlControls.Literal;
      Settings = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Settings"), "Header", "Settings", "Text", "Select the publishing settings. Click Publish to publish.", "Icon", "Custom/32x32/Publish.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformpadding11 = AddControl("WizardFormPadding", "", Settings, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      SettingsPane = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), wizardformpadding11, "", "ID", idref("SettingsPane"), "Border", "none", "Background", "transparent") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      PublishingPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), SettingsPane, "", "ID", idref("PublishingPanel"), "Header", "Publishing", "Margin", "0px 0px 16px 0px") as Sitecore.Web.UI.HtmlControls.Groupbox;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), PublishingPanel, "", "Padding", "4") as Sitecore.Web.UI.HtmlControls.Border;
      IncrementalPublishPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border12, "", "ID", idref("IncrementalPublishPane")) as Sitecore.Web.UI.HtmlControls.Border;
      IncrementalPublish = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), IncrementalPublishPane, "", "ID", idref("IncrementalPublish"), "Name", "PublishMode", "Header", "Incremental Publish - Publish Only Changed Items.", "Value", "IncrementalPublish") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      br13 = AddControl("br", "", IncrementalPublishPane, "");
      SmartPublish = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border12, "", "ID", idref("SmartPublish"), "Name", "PublishMode", "Header", "Smart publish - Publish Differences Between Source and Target Database.", "Value", "SmartPublish", "Checked", "true") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      br14 = AddControl("br", "", border12, "");
      Republish = AddControl(new Sitecore.Web.UI.HtmlControls.Radiobutton(), border12, "", "ID", idref("Republish"), "Name", "PublishMode", "Header", "Republish - Publish Everything.", "Value", "Republish") as Sitecore.Web.UI.HtmlControls.Radiobutton;
      PublishChildrenPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border12, "", "ID", idref("PublishChildrenPane")) as Sitecore.Web.UI.HtmlControls.Border;
      PublishChildren = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), PublishChildrenPane, "", "ID", idref("PublishChildren"), "Header", "Publish Subitems") as Sitecore.Web.UI.HtmlControls.Checkbox;
      LanguagesPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), SettingsPane, "", "ID", idref("LanguagesPanel"), "Header", "Publish Language", "Margin", "0px 0px 16px 0px") as Sitecore.Web.UI.HtmlControls.Groupbox;
      Languages = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LanguagesPanel, "", "Padding", "4", "ID", idref("Languages")) as Sitecore.Web.UI.HtmlControls.Border;
      PublishingTargetsPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Groupbox(), SettingsPane, "", "ID", idref("PublishingTargetsPanel"), "Header", "Publishing Targets") as Sitecore.Web.UI.HtmlControls.Groupbox;
      PublishingTargets = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), PublishingTargetsPanel, "", "Padding", "4", "ID", idref("PublishingTargets")) as Sitecore.Web.UI.HtmlControls.Border;
      NoPublishingTarget = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformpadding11, "", "ID", idref("NoPublishingTarget"), "Visible", "false", "Align", "center", "Padding", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage15 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), NoPublishingTarget, "", "Src", "Applications/24x24/error.png", "Width", "16", "Height", "16", "Align", "absmiddle", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      b16 = AddControl("b", "", NoPublishingTarget, "");
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), b16, "", "Text", "You cannot publish as no publishing targets are defined in this database.") as Sitecore.Web.UI.HtmlControls.Literal;
      Publishing = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Publishing"), "Header", "Publishing", "Text", "Please wait while publishing...", "Icon", "Custom/32x32/Publish.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel18 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Publishing, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space19 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel18, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage20 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel18, "", "Src", "Custom/48x48/Publish.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space21 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel18, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel18, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage23 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border22, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space24 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel18, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      PublishingText = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel18, "", "ID", idref("PublishingText"), "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal25 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), PublishingText, "", "Text", "Publishing...") as Sitecore.Web.UI.HtmlControls.Literal;
      space26 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel18, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      PublishingTarget = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel18, "", "ID", idref("PublishingTarget"), "Align", "center", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      text27 = AddLiteral("                       ", "", PublishingTarget, "");
      Retry = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Retry"), "Header", "An error occurred", "Text", "An error occurred while publishing.", "Icon", "Custom/32x32/Publish.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformpadding28 = AddControl("WizardFormPadding", "", Retry, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      ErrorText = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), wizardformpadding28, "", "ID", idref("ErrorText"), "ReadOnly", "true", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Memo;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Custom/32x32/Publish.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel29 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), LastPage, "", "Cellpadding", "2", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border30 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel29, "", "GridPanel.Style", "padding:0px 0px 16px 0px; font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal31 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border30, "", "Text", "The wizard has completed. Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      border32 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel29, "", "GridPanel.Style", "padding:0px 0px 2px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      Status = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border32, "", "ID", idref("Status")) as Sitecore.Web.UI.HtmlControls.Literal;
      ShowResultPane = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel29, "", "ID", idref("ShowResultPane"), "GridPanel.Style", "padding:8px 0px 2px 0px", "Click", "ShowResult", "RollOver", "true", "Class", "scRollOver") as Sitecore.Web.UI.HtmlControls.Border;
      literal33 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), ShowResultPane, "", "Text", "Click here to see additional information.") as Sitecore.Web.UI.HtmlControls.Literal;
      ResultLabel = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel29, "", "ID", idref("ResultLabel"), "GridPanel.Style", "padding:8px 0px 2px 0px", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Border;
      literal34 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), ResultLabel, "", "Text", "Result:") as Sitecore.Web.UI.HtmlControls.Literal;
      ResultText = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel29, "", "ID", idref("ResultText"), "ReadOnly", "true", "Width", "100%", "Height", "100%", "GridPanel.Height", "100%", "Visible", "false") as Sitecore.Web.UI.HtmlControls.Memo;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(Settings, "ID", idref("Settings"));
      SetProperty(SettingsPane, "ID", idref("SettingsPane"));
      SetProperty(PublishingPanel, "ID", idref("PublishingPanel"));
      SetProperty(IncrementalPublishPane, "ID", idref("IncrementalPublishPane"));
      SetProperty(IncrementalPublish, "ID", idref("IncrementalPublish"));
      SetProperty(SmartPublish, "ID", idref("SmartPublish"));
      SetProperty(Republish, "ID", idref("Republish"));
      SetProperty(PublishChildrenPane, "ID", idref("PublishChildrenPane"));
      SetProperty(PublishChildren, "ID", idref("PublishChildren"));
      SetProperty(LanguagesPanel, "ID", idref("LanguagesPanel"));
      SetProperty(Languages, "ID", idref("Languages"));
      SetProperty(PublishingTargetsPanel, "ID", idref("PublishingTargetsPanel"));
      SetProperty(PublishingTargets, "ID", idref("PublishingTargets"));
      SetProperty(NoPublishingTarget, "ID", idref("NoPublishingTarget"));
      SetProperty(Publishing, "ID", idref("Publishing"));
      SetProperty(PublishingText, "ID", idref("PublishingText"));
      SetProperty(PublishingTarget, "ID", idref("PublishingTarget"));
      SetProperty(Retry, "ID", idref("Retry"));
      SetProperty(ErrorText, "ID", idref("ErrorText"));
      SetProperty(LastPage, "ID", idref("LastPage"));
      SetProperty(Status, "ID", idref("Status"));
      SetProperty(ShowResultPane, "ID", idref("ShowResultPane"));
      SetProperty(ResultLabel, "ID", idref("ResultLabel"));
      SetProperty(ResultText, "ID", idref("ResultText"));
    }
  }
}

