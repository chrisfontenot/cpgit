using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_MediaLibrary_a_124 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.XmlControls.XmlControl dockwindowchrome4;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton Databases;
    public System.Web.UI.Control toolbarseperator8;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton9;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton10;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox14;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Submittable", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Windows.MediaLibrary.MediaLibraryForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      dockwindowchrome4 = AddControl("DockWindowChrome", "", formpage1, "", "Header", "Media Library") as Sitecore.Web.UI.XmlControls.XmlControl;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), dockwindowchrome4, "", "ID", idref("DataContext"), "Root", "/sitecore/media library", "DataViewName", "Master") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), dockwindowchrome4, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "") as Sitecore.Web.UI.HtmlControls.Border;
      toolbar7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), border6, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      Databases = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "ID", idref("Databases"), "Icon", "Business/16x16/data.png", "IconSize", "id16x16", "Header", "Master", "Glyph", "images/menudropdown_black9x8.png", "Click", "ShowDatabases", "ToolTip", "Select another database") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbarseperator8 = AddControl("ToolbarSeperator", "", toolbar7, "");
      toolbutton9 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "Icon", "Core3/16x16/open_document.png", "IconSize", "id16x16", "Click", "Open", "ToolTip", "Open the selected item") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton10 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "Icon", "Applications/32x32/Export1.png", "IconSize", "id16x16", "Click", "Upload_Click", "ToolTip", "Upload a file") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton11 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar7, "", "Icon", "Applications/32x32/Refresh.png", "IconSize", "id16x16", "Click", "Refresh", "ToolTip", "Refresh") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border6, "", "Background", "#aca899") as Sitecore.Web.UI.HtmlControls.Border;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border12, "") as Sitecore.Web.UI.HtmlControls.Space;
      scrollbox14 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Width", "100%", "Height", "100%", "Padding", "0", "ContextMenu", "Treeview.GetContextMenu", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox14, "", "ID", idref("Treeview"), "DataContext", "DataContext", "DblClick", "Open") as Sitecore.Web.UI.WebControls.TreeviewEx;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Databases, "ID", idref("Databases"));
      SetProperty(Treeview, "ID", idref("Treeview"));
    }
  }
}

