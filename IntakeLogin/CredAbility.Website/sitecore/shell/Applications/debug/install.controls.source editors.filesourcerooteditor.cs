using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FileSourceRootEditor_a_74 : Sitecore.Shell.Applications.Install.Controls.FileSourceRootEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox3;
    
    public string m_Padding;
    public string m_Style;
    
    // properties
    public new string Padding {
      get {
        return StringUtil.GetString(m_Padding);
      }
      set {
        m_Padding = value;
        
        SetProperty(border1, "Padding", Padding);
      }
    }
    
    public new string Style {
      get {
        return StringUtil.GetString(m_Style);
      }
      set {
        m_Style = value;
        
        SetProperty(border1, "Style", Style);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Width", "100%", "Height", "100%", "Padding", Padding, "Style", Style) as Sitecore.Web.UI.HtmlControls.Border;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), border1, "", "DefaultItem", "/", "Filter", "Contains('" + Sitecore.Data.DataProviders.FileSystemDataProvider.FolderID.ToString() + "', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "1", "Width", "100%", "Height", "100%", "Fixed", "true", "Border", "1px inset") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "Width", "100%", "Height", "100%", "Background", "window", "Border", "none", "Padding", "0", "Style", "filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=1, StartColorStr='#ffffff', EndColorStr='#DFE6EC')", "GridPanel.ID", "FoldersPane") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.HtmlControls.DataTreeview(), scrollbox3, "", "MultiSelect", "false", "Root", "true") as Sitecore.Web.UI.HtmlControls.DataTreeview;
      
      _Mode = "";
    }
  }
}

