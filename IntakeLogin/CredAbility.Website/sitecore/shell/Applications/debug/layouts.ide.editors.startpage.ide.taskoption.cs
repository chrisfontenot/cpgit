using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_TaskOption_a_118 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.HtmlControls.Inline inline2;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage3;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public System.Web.UI.Control placeholder5;
    
    public string m_ID;
    public string m_Click;
    public string m_Header;
    
    // properties
    public new string ID {
      get {
        return StringUtil.GetString(m_ID);
      }
      set {
        m_ID = value;
        
        SetProperty(border1, "ID", ID);
      }
    }
    
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(inline2, "Click", Click);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal4, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "ID", ID, "Padding", "6 0") as Sitecore.Web.UI.HtmlControls.Border;
      inline2 = AddControl(new Sitecore.Web.UI.HtmlControls.Inline(), border1, "", "RollOver", "true", "Class", "scRollOver", "Click", Click) as Sitecore.Web.UI.HtmlControls.Inline;
      themedimage3 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), inline2, "", "Src", "Images/Go16x16.png", "Width", "16", "Height", "16", "Margin", "0px 6px 0px 6px", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), inline2, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      placeholder5 = AddPlaceholder("", inline2, "");
      
      _Mode = "";
    }
  }
}

