using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_ProjectView_a_83 : Sitecore.Shell.Applications.Install.Controls.ProjectView   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox4;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "language", "javascript", "src", "/sitecore/shell/applications/install/Controls/Widgets/TreeView.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "/sitecore/shell/applications/install/installer.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), this, "", "Columns", "3", "Height", "100%", "Style", "margin:0px;", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      SideFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "GridPanel.Width", "120px", "Width", "100%", "Border", "none", "Padding", "0px") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      SideFrameContent = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), SideFrame, "") as Sitecore.Web.UI.HtmlControls.Border;
      Splitter = AddControl("VSplitter", "", gridpanel3, "", "GridPanel.Width", "5px", "Target", "left", "GridPanel.Style", "background:#f8f8f8") as Sitecore.Web.UI.XmlControls.XmlControl;
      MainPane = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%", "Style", "margin:0px;") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox4 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), MainPane, "", "Width", "100%", "Height", "100%", "Border", "none") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      EditorFrame = AddControl("Installer.EditorHost", "", scrollbox4, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

