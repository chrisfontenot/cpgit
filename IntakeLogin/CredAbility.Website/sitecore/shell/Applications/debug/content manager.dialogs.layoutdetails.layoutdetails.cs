using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class LayoutDetails_a_138 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.HtmlControls.Scrollbox LayoutPanel;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/window_view.png", "Header", "Layout Details", "Text", "The details of the assigned layouts, controls and placeholders.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        #LayoutPanel {          padding: 4px;        }                .ff #LayoutPanel {          padding: 0;        }                .scContentControlLayoutDevice, .scContentControlLayoutDevice_Active {          padding-bottom: 6px;        }                .ff .scDeviceCommands span:hover {          text-decoration: underline;          cursor: pointer;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Dialogs.LayoutDetails.LayoutDetailsForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      LayoutPanel = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), formdialog1, "", "ID", idref("LayoutPanel"), "Padding", "0", "Height", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(LayoutPanel, "ID", idref("LayoutPanel"));
    }
  }
}

