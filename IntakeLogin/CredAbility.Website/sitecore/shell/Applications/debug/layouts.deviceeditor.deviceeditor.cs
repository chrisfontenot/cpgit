using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class DeviceEditor_a_141 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.HtmlControls.DataContext LayoutDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.VerticalTabstrip verticaltabstrip6;
    public Sitecore.Web.UI.HtmlControls.Tab LayoutTab;
    public Sitecore.Web.UI.HtmlControls.TreePicker Layout;
    public Sitecore.Web.UI.HtmlControls.Tab ControlsTab;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Renderings;
    public Sitecore.Web.UI.HtmlControls.Space space8;
    public Sitecore.Web.UI.HtmlControls.Border border9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.HtmlControls.Button button11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Button button13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Button button15;
    public Sitecore.Web.UI.HtmlControls.Border border16;
    public Sitecore.Web.UI.HtmlControls.Button button17;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Button button19;
    public Sitecore.Web.UI.HtmlControls.Border border20;
    public Sitecore.Web.UI.HtmlControls.Button button21;
    public Sitecore.Web.UI.HtmlControls.Tab PlaceholdersTab;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel22;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Placeholders;
    public Sitecore.Web.UI.HtmlControls.Space space23;
    public Sitecore.Web.UI.HtmlControls.Border border24;
    public Sitecore.Web.UI.HtmlControls.Border border25;
    public Sitecore.Web.UI.HtmlControls.Button button26;
    public Sitecore.Web.UI.HtmlControls.Border border27;
    public Sitecore.Web.UI.HtmlControls.Button button28;
    public Sitecore.Web.UI.HtmlControls.Border border29;
    public Sitecore.Web.UI.HtmlControls.Button button30;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "People/24x24/pda.png", "Header", "Device Editor", "Text", "Set the layouts, controls and placeholders for this device.", "OKButton", "OK") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scVerticalTabstrip .scTabContent {        background: transparent;        border: none;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Layouts.DeviceEditor.DeviceEditorForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      LayoutDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("LayoutDataContext"), "DataViewName", "Master", "Root", "{75CC5CE4-8979-4008-9D3C-806477D57619}", "Filter", "Contains('{A87A00B1-E6DB-45AB-8B54-636FEC3B5523},{1163DA83-B2EF-4381-BF09-B2FF714B1B3F},{3A45A723-64EE-4919-9D41-02FD40FD1466},{A87A00B1-E6DB-45AB-8B54-636FEC3B5523},{239F9CF4-E5A0-44E0-B342-0F32CD4C6D8B},{93227C5D-4FEF-474D-94C0-F252EC8E8219}', @@templateid)") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "Height", "100%", "Cellpadding", "0", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      verticaltabstrip6 = AddControl(new Sitecore.Web.UI.HtmlControls.VerticalTabstrip(), gridpanel5, "", "GridPanel.Height", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.VerticalTabstrip;
      LayoutTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), verticaltabstrip6, "", "ID", idref("LayoutTab"), "Header", "Layout") as Sitecore.Web.UI.HtmlControls.Tab;
      Layout = AddControl(new Sitecore.Web.UI.HtmlControls.TreePicker(), LayoutTab, "", "ID", idref("Layout"), "DataContext", "LayoutDataContext", "SelectOnly", "true", "AllowNone", "true", "Width", "100%") as Sitecore.Web.UI.HtmlControls.TreePicker;
      ControlsTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), verticaltabstrip6, "", "ID", idref("ControlsTab"), "Header", "Controls") as Sitecore.Web.UI.HtmlControls.Tab;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), ControlsTab, "", "Columns", "3", "Width", "100%", "Height", "100%", "GridPanel.Height", "50%") as Sitecore.Web.UI.WebControls.GridPanel;
      Renderings = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel7, "", "ID", idref("Renderings"), "Width", "100%", "Height", "100%", "Padding", "0px", "GridPanel.Width", "100%", "GridPanel.Height", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      space8 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel7, "", "Width", "4", "GridPanel.Width", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border9 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button11 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border10, "", "Header", "Add", "Click", "device:add") as Sitecore.Web.UI.HtmlControls.Button;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button13 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border12, "", "Header", "Edit", "Click", "device:edit") as Sitecore.Web.UI.HtmlControls.Button;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button15 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border14, "", "Header", "Change", "Click", "device:change") as Sitecore.Web.UI.HtmlControls.Button;
      border16 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 12px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button17 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border16, "", "Header", "Remove", "Click", "device:remove") as Sitecore.Web.UI.HtmlControls.Button;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button19 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Header", "Move Up", "Click", "device:sortup") as Sitecore.Web.UI.HtmlControls.Button;
      border20 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border9, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button21 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border20, "", "Header", "Move Down", "Click", "device:sortdown") as Sitecore.Web.UI.HtmlControls.Button;
      PlaceholdersTab = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), verticaltabstrip6, "", "ID", idref("PlaceholdersTab"), "Header", "Placeholder Settings") as Sitecore.Web.UI.HtmlControls.Tab;
      gridpanel22 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), PlaceholdersTab, "", "Columns", "3", "Width", "100%", "Height", "100%", "GridPanel.Height", "50%") as Sitecore.Web.UI.WebControls.GridPanel;
      Placeholders = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel22, "", "ID", idref("Placeholders"), "Width", "100%", "Height", "100%", "Padding", "0px", "GridPanel.Width", "100%", "GridPanel.Height", "100%", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      space23 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel22, "", "Width", "4", "GridPanel.Width", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border24 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel22, "", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border25 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border24, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button26 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border25, "", "Header", "Add", "Click", "device:addplaceholder") as Sitecore.Web.UI.HtmlControls.Button;
      border27 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border24, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button28 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border27, "", "Header", "Edit", "Click", "device:editplaceholder") as Sitecore.Web.UI.HtmlControls.Button;
      border29 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border24, "", "Padding", "0px 0px 12px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      button30 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border29, "", "Header", "Remove", "Click", "device:removeplaceholder") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(LayoutDataContext, "ID", idref("LayoutDataContext"));
      SetProperty(LayoutTab, "ID", idref("LayoutTab"));
      SetProperty(Layout, "ID", idref("Layout"));
      SetProperty(ControlsTab, "ID", idref("ControlsTab"));
      SetProperty(Renderings, "ID", idref("Renderings"));
      SetProperty(PlaceholdersTab, "ID", idref("PlaceholdersTab"));
      SetProperty(Placeholders, "ID", idref("Placeholders"));
    }
  }
}

