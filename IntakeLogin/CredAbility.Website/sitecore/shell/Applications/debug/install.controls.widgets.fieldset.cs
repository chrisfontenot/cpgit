using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_FieldSet_a_87 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public System.Web.UI.Control fieldset1;
    public System.Web.UI.Control legend2;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public System.Web.UI.Control placeholder5;
    
    public string m_Header;
    
    // properties
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal3, "Text", Header);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      fieldset1 = AddControl("fieldset", "", this, "", "class", "scGroup");
      legend2 = AddControl("legend", "", fieldset1, "");
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), legend2, "", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), fieldset1, "", "Width", "100%", "Fixed", "true", "Style", "margin-top:5px") as Sitecore.Web.UI.WebControls.GridPanel;
      placeholder5 = AddPlaceholder("", gridpanel4, "", "GridPanel.Container", "true");
      
      _Mode = "";
    }
  }
}

