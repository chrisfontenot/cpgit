using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDENewFile_a_156 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.HtmlControls.DataContext FilteredDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Border border5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Space space7;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox8;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.XmlControls.XmlControl IDENewFileLeft;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel9;
    public Sitecore.Web.UI.HtmlControls.Border border10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton14;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton15;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox16;
    public Sitecore.Web.UI.HtmlControls.DataListview Listview;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader17;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem18;
    public Sitecore.Web.UI.HtmlControls.Border Help;
    public System.Web.UI.Control text19;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Applications/32x32/document_plain.png", "Header", "New File", "Text", "Select the type of file that you want to create.", "OKButton", "Create") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Dialogs.NewFile.NewFileForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("DataContext"), "DataContext", "FilteredDataContext", "Root", "/sitecore/content/Applications/Layouts/IDE/New File", "DataViewName", "Master", "Parameters", "databasename=core", "Filter", "Contains('{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}', @@templateid)", "Folder", "/sitecore/content/Applications/Layouts/IDE/New File/Renderings") as Sitecore.Web.UI.HtmlControls.DataContext;
      FilteredDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("FilteredDataContext"), "DataContext", "DataContext", "Root", "/sitecore/content/Applications/Layouts/IDE/New File", "DataViewName", "Master", "Parameters", "databasename=core", "Filter", "not(Contains('{A87A00B1-E6DB-45AB-8B54-636FEC3B5523}', @@templateid))", "Folder", "/sitecore/content/Applications/Layouts/IDE/New File/Renderings") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Columns", "3", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Width", "33%", "GridPanel.Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border5 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel4, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border5, "", "Text", "Categories:", "Style", "height:16px") as Sitecore.Web.UI.HtmlControls.Literal;
      space7 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border5, "", "Height", "22") as Sitecore.Web.UI.HtmlControls.Space;
      scrollbox8 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel4, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px solid #7f9db9", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox8, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Root", "false", "Click", "Treeview_Click", "Width", "100%") as Sitecore.Web.UI.WebControls.TreeviewEx;
      IDENewFileLeft = AddControl("VSplitter", "", gridpanel3, "", "ID", idref("IDENewFileLeft"), "GridPanel.Width", "4", "Target", "left") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel9 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border10 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel9, "") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border10, "", "Columns", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel11, "", "Text", "Templates:", "Style", "height:16px") as Sitecore.Web.UI.HtmlControls.Literal;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel11, "", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      toolbutton14 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Icon", "Core3/16x16/views.png", "IconSize", "id16x16", "Click", "SetViewIcons") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton15 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border13, "", "Icon", "Core/16x16/list.png", "IconSize", "id16x16", "Click", "SetViewList") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      scrollbox16 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel9, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px solid #7f9db9", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Listview = AddControl(new Sitecore.Web.UI.HtmlControls.DataListview(), scrollbox16, "", "ID", idref("Listview"), "DataContext", "FilteredDataContext", "Click", "Select", "DblClick", "Open", "AllowDragging", "false") as Sitecore.Web.UI.HtmlControls.DataListview;
      listviewheader17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), Listview, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader17, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      Help = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "ID", idref("Help"), "GridPanel.ColSpan", "3", "Margin", "4px 0px 0px 0px", "Padding", "2", "Border", "1px solid #7f9db9") as Sitecore.Web.UI.HtmlControls.Border;
      text19 = AddLiteral("         ", "", Help, "");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(FilteredDataContext, "ID", idref("FilteredDataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(IDENewFileLeft, "ID", idref("IDENewFileLeft"));
      SetProperty(Listview, "ID", idref("Listview"));
      SetProperty(Help, "ID", idref("Help"));
    }
  }
}

