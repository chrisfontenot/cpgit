using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class FileBrowser_a_127 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.DataContext FileDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox3;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Border border6;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Edit Filename;
    public Sitecore.Web.UI.HtmlControls.Button button8;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "Application", "Files/FileBrowser", "ID", idref("Dialog"), "Icon", "Applications/48x48/folder.png", "Header", "File browser", "Text", "Please select a file.") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Files.FileBrowser.FileBrowserForm,Sitecore.Client", "Submittable", "false") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      FileDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), Dialog, "", "ID", idref("FileDataContext"), "DataViewName", "FileSystem") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Dialog, "", "Width", "100%", "Height", "100%", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel2, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox3, "", "ID", idref("Treeview"), "DataContext", "FileDataContext", "Click", "SelectTreeNode", "DblClick", "Treeview_DblClick", "ShowRoot", "false", "ContextMenu", "Treeview.GetContextMenu(\"contextmenu\")") as Sitecore.Web.UI.WebControls.TreeviewEx;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Padding", "4px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border4, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border6 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel5, "", "Padding", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border6, "", "Text", "Name:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filename = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel5, "", "ID", idref("Filename"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      button8 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), Dialog, "Buttons", "Header", "Upload", "Click", "filebrowser:upload") as Sitecore.Web.UI.HtmlControls.Button;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(FileDataContext, "ID", idref("FileDataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(Filename, "ID", idref("Filename"));
    }
  }
}

