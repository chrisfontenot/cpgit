using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_AddFileSource_a_73 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.Border Internals;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.XmlControls.XmlControl LoadFileSource;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent11;
    public Sitecore.Web.UI.XmlControls.XmlControl Root;
    public Sitecore.Web.UI.XmlControls.XmlControl ApplyFilters;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent12;
    public Sitecore.Web.UI.XmlControls.XmlControl Filters;
    public Sitecore.Web.UI.XmlControls.XmlControl SetName;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent13;
    public Sitecore.Web.UI.XmlControls.XmlControl Name;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Border border16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Install.Dialogs.AddFileSourceForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      Internals = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardform1, "", "ID", idref("Internals")) as Sitecore.Web.UI.HtmlControls.Border;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Add Dynamic File Source Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Select the root folder for the search.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Specify the search criteria.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Add the dynamic file source.") as Sitecore.Web.UI.HtmlControls.Literal;
      LoadFileSource = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("LoadFileSource"), "Header", "Select Root Folder", "Text", "Select the folder where you want to start the search. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent11 = AddControl("WizardFormIndent", "", LoadFileSource, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Root = AddControl("Installer.FileSourceRootEditor", "", wizardformindent11, "", "ID", idref("Root")) as Sitecore.Web.UI.XmlControls.XmlControl;
      ApplyFilters = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("ApplyFilters"), "Header", "Specify Source Filters", "Text", "Specify the filters that you want to apply to the source. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent12 = AddControl("WizardFormIndent", "", ApplyFilters, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Filters = AddControl("Installer.FilesFilterEditor", "", wizardformindent12, "", "ID", idref("Filters")) as Sitecore.Web.UI.XmlControls.XmlControl;
      SetName = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("SetName"), "Header", "Source Name", "Text", "Enter a name for the source. Click Add to add the dynamic file source.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent13 = AddControl("WizardFormIndent", "", SetName, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Name = AddControl("Installer.NameEditor", "", wizardformindent13, "", "ID", idref("Name")) as Sitecore.Web.UI.XmlControls.XmlControl;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border14, "", "Text", "The dynamic file source has been added successfully.") as Sitecore.Web.UI.HtmlControls.Literal;
      border16 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border16, "", "Text", "Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Internals, "ID", idref("Internals"));
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(LoadFileSource, "ID", idref("LoadFileSource"));
      SetProperty(Root, "ID", idref("Root"));
      SetProperty(ApplyFilters, "ID", idref("ApplyFilters"));
      SetProperty(Filters, "ID", idref("Filters"));
      SetProperty(SetName, "ID", idref("SetName"));
      SetProperty(Name, "ID", idref("Name"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

