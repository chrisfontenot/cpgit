using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_AddStaticItemSource_a_89 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.Border Internals;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.XmlControls.XmlControl LoadItemSource;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent11;
    public Sitecore.Web.UI.HtmlControls.DataContext DataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel14;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Combobox Databases;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel16;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox17;
    public Sitecore.Web.UI.HtmlControls.DataTreeview Treeview;
    public Sitecore.Web.UI.HtmlControls.TreeHeader treeheader18;
    public Sitecore.Web.UI.HtmlControls.TreeHeaderItem treeheaderitem19;
    public Sitecore.Web.UI.HtmlControls.TreeHeaderItem treeheaderitem20;
    public Sitecore.Web.UI.HtmlControls.Space space21;
    public Sitecore.Web.UI.HtmlControls.Border border22;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton23;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton24;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton25;
    public Sitecore.Web.UI.HtmlControls.Space space26;
    public Sitecore.Web.UI.HtmlControls.Scrollbox ItemListBox;
    public Sitecore.Web.UI.HtmlControls.Listview ItemList;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader27;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem28;
    public Sitecore.Web.UI.XmlControls.XmlControl SetName;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent29;
    public Sitecore.Web.UI.XmlControls.XmlControl Name;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border border30;
    public Sitecore.Web.UI.HtmlControls.Literal literal31;
    public Sitecore.Web.UI.HtmlControls.Border border32;
    public Sitecore.Web.UI.HtmlControls.Literal literal33;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Install.Dialogs.AddStaticItemSourceDialog,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      Internals = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardform1, "", "ID", idref("Internals")) as Sitecore.Web.UI.HtmlControls.Border;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Add Static Item Source Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Select the items and subtrees to include in the source.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Specify a name for the source.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Add the static item source.") as Sitecore.Web.UI.HtmlControls.Literal;
      LoadItemSource = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("LoadItemSource"), "Header", "Select Items", "Text", "Select the database and the items and subtrees that you want to include. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent11 = AddControl("WizardFormIndent", "", LoadItemSource, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      DataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), wizardformindent11, "", "ID", idref("DataContext"), "Root", "{11111111-1111-1111-1111-111111111111}", "filter", "@@virtual=0") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent11, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Padding", "0 0 8 0", "GridPanel.Height", "2em") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel14 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border13, "", "Width", "100%", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel14, "", "Text", "Database: ") as Sitecore.Web.UI.HtmlControls.Literal;
      Databases = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel14, "", "ID", idref("Databases"), "Change", "ChangeDatabase", "Width", "100%", "Padding", "0 0 0 4", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Combobox;
      gridpanel16 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel12, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox17 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel16, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.HtmlControls.DataTreeview(), scrollbox17, "", "ID", idref("Treeview"), "DataContext", "DataContext", "Root", "true") as Sitecore.Web.UI.HtmlControls.DataTreeview;
      treeheader18 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeader(), Treeview, "") as Sitecore.Web.UI.HtmlControls.TreeHeader;
      treeheaderitem19 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeaderItem(), treeheader18, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.TreeHeaderItem;
      treeheaderitem20 = AddControl(new Sitecore.Web.UI.HtmlControls.TreeHeaderItem(), treeheader18, "", "Name", "__Short description", "Header", "Description") as Sitecore.Web.UI.HtmlControls.TreeHeaderItem;
      space21 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel16, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      border22 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel16, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      toolbutton23 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border22, "", "Style", "float:right", "Icon", "Applications/24x24/delete2.png", "Header", "Remove", "ToolTip", "Removes entry from selection list", "Click", "Remove(\"\")") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton24 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border22, "", "Icon", "Software/24x24/branch.png", "Header", "Add with Subitems", "ToolTip", "Adds item with subitems", "Click", "AddTree") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton25 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border22, "", "Icon", "Software/24x24/element.png", "Header", "Add Item", "ToolTip", "Add Item", "Click", "AddItem") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space26 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel16, "", "GridPanel.Height", "4px") as Sitecore.Web.UI.HtmlControls.Space;
      ItemListBox = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel16, "", "ID", idref("ItemListBox"), "Width", "100%", "Height", "150px", "ContextMenu", "ListContextMenu()") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      ItemList = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), ItemListBox, "", "ID", idref("ItemList"), "Width", "100%", "View", "Details", "MultiSelect", "true") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader27 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), ItemList, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem28 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader27, "", "Header", "Selected items:") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      SetName = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("SetName"), "Header", "Source Name", "Text", "Enter a name for the source. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent29 = AddControl("WizardFormIndent", "", SetName, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Name = AddControl("Installer.NameEditor", "", wizardformindent29, "", "ID", idref("Name")) as Sitecore.Web.UI.XmlControls.XmlControl;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border30 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal31 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border30, "", "Text", "The static item source has been added successfully.") as Sitecore.Web.UI.HtmlControls.Literal;
      border32 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "") as Sitecore.Web.UI.HtmlControls.Border;
      literal33 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border32, "", "Text", "Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Internals, "ID", idref("Internals"));
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(LoadItemSource, "ID", idref("LoadItemSource"));
      SetProperty(DataContext, "ID", idref("DataContext"));
      SetProperty(Databases, "ID", idref("Databases"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(ItemListBox, "ID", idref("ItemListBox"));
      SetProperty(ItemList, "ID", idref("ItemList"));
      SetProperty(SetName, "ID", idref("SetName"));
      SetProperty(Name, "ID", idref("Name"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

