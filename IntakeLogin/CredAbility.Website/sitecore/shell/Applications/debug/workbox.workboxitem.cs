using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class WorkboxItem_a_82 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Border border1;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel2;
    public Sitecore.Web.UI.HtmlControls.Checkbox checkbox3;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public System.Web.UI.Control input5;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage6;
    public Sitecore.Web.UI.HtmlControls.Border border7;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Literal literal11;
    public Sitecore.Web.UI.HtmlControls.Border border12;
    public Sitecore.Web.UI.HtmlControls.Space space13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public System.Web.UI.Control text15;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public System.Web.UI.Control text17;
    public Sitecore.Web.UI.HtmlControls.Inline inline18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage20;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Space space22;
    public Sitecore.Web.UI.XmlControls.XmlControl workboxcommand23;
    public Sitecore.Web.UI.XmlControls.XmlControl workboxcommand24;
    public Sitecore.Web.UI.XmlControls.XmlControl workboxcommand25;
    public System.Web.UI.Control placeholder26;
    
    public string m_CheckID;
    public string m_Click;
    public string m_HiddenID;
    public string m_CheckValue;
    public string m_Icon;
    public string m_Header;
    public string m_Details;
    public string m_ShortDescription;
    public string m_History;
    public string m_HistoryMoreID;
    public string m_HistoryClick;
    public string m_PreviewClick;
    public string m_DiffClick;
    
    // properties
    public string CheckID {
      get {
        return StringUtil.GetString(m_CheckID);
      }
      set {
        m_CheckID = value;
        
        SetProperty(checkbox3, "ID", CheckID);
      }
    }
    
    public string Click {
      get {
        return StringUtil.GetString(m_Click);
      }
      set {
        m_Click = value;
        
        SetProperty(border4, "Click", Click);
        SetProperty(border8, "Click", Click);
        SetProperty(workboxcommand24, "Command", Click);
      }
    }
    
    public string HiddenID {
      get {
        return StringUtil.GetString(m_HiddenID);
      }
      set {
        m_HiddenID = value;
        
        SetProperty(input5, "name", HiddenID);
      }
    }
    
    public string CheckValue {
      get {
        return StringUtil.GetString(m_CheckValue);
      }
      set {
        m_CheckValue = value;
        
        SetProperty(input5, "value", CheckValue);
      }
    }
    
    public string Icon {
      get {
        return StringUtil.GetString(m_Icon);
      }
      set {
        m_Icon = value;
        
        SetProperty(themedimage6, "Src", Icon);
      }
    }
    
    public string Header {
      get {
        return StringUtil.GetString(m_Header);
      }
      set {
        m_Header = value;
        
        SetProperty(literal9, "Text", Header);
      }
    }
    
    public string Details {
      get {
        return StringUtil.GetString(m_Details);
      }
      set {
        m_Details = value;
        
        SetProperty(literal10, "Text", Details);
      }
    }
    
    public string ShortDescription {
      get {
        return StringUtil.GetString(m_ShortDescription);
      }
      set {
        m_ShortDescription = value;
        
        SetProperty(literal11, "Text", ShortDescription);
      }
    }
    
    public string History {
      get {
        return StringUtil.GetString(m_History);
      }
      set {
        m_History = value;
        
        SetProperty(literal16, "Text", History);
      }
    }
    
    public string HistoryMoreID {
      get {
        return StringUtil.GetString(m_HistoryMoreID);
      }
      set {
        m_HistoryMoreID = value;
        
        SetProperty(inline18, "ID", HistoryMoreID);
      }
    }
    
    public string HistoryClick {
      get {
        return StringUtil.GetString(m_HistoryClick);
      }
      set {
        m_HistoryClick = value;
        
        SetProperty(inline18, "Click", HistoryClick);
      }
    }
    
    public string PreviewClick {
      get {
        return StringUtil.GetString(m_PreviewClick);
      }
      set {
        m_PreviewClick = value;
        
        SetProperty(workboxcommand23, "Command", PreviewClick);
      }
    }
    
    public string DiffClick {
      get {
        return StringUtil.GetString(m_DiffClick);
      }
      set {
        m_DiffClick = value;
        
        SetProperty(workboxcommand25, "Command", DiffClick);
      }
    }
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      border1 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "Style", "border-bottom:1px solid #999999", "Margin", "8px 4px 8px 16px", "Padding", "2px 8px 8px 8px") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel2 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), border1, "", "Columns", "3") as Sitecore.Web.UI.WebControls.GridPanel;
      checkbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), gridpanel2, "", "GridPanel.Valign", "top", "ID", CheckID, "Click", "javascript:true") as Sitecore.Web.UI.HtmlControls.Checkbox;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Click", Click, "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      input5 = AddControl("input", "", border4, "", "type", "hidden", "name", HiddenID, "value", CheckValue);
      themedimage6 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border4, "", "Src", Icon, "Width", "24", "Height", "24", "Margin", "0px 4px 0px 0px") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border7 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel2, "", "Cursor", "default", "GridPanel.Width", "100%", "GridPanel.Valign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Class", "scRollOver", "RollOver", "true", "Click", Click, "Foreground", "#072D6B") as Sitecore.Web.UI.HtmlControls.Border;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "Class", "scClickFont", "Style", "font-weight:bold", "Text", Header) as Sitecore.Web.UI.HtmlControls.Literal;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "Class", "scFont", "Text", Details) as Sitecore.Web.UI.HtmlControls.Literal;
      literal11 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", ShortDescription) as Sitecore.Web.UI.HtmlControls.Literal;
      border12 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Background", "#dddddd", "Margin", "2px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      space13 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border12, "") as Sitecore.Web.UI.HtmlControls.Space;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Foreground", "#999999", "Text", "Last change:") as Sitecore.Web.UI.HtmlControls.Literal;
      text15 = AddLiteral(" ", "", border7, "");
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border7, "", "Text", History) as Sitecore.Web.UI.HtmlControls.Literal;
      text17 = AddLiteral("           ", "", border7, "");
      inline18 = AddControl(new Sitecore.Web.UI.HtmlControls.Inline(), border7, "", "ID", HistoryMoreID, "Class", "scRollover", "RollOver", "true", "Click", HistoryClick) as Sitecore.Web.UI.HtmlControls.Inline;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), inline18, "", "Text", "More") as Sitecore.Web.UI.HtmlControls.Literal;
      themedimage20 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), inline18, "", "Src", "Images/SortDown9x5.png", "Width", "9", "Height", "5", "Margin", "0px 0px 0px 2px", "Align", "absmiddle") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border7, "", "Background", "#dddddd", "Margin", "8px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      space22 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), border21, "") as Sitecore.Web.UI.HtmlControls.Space;
      workboxcommand23 = AddControl("WorkboxCommand", "", border7, "", "Icon", "Applications/16x16/document_time.png", "Header", "Preview", "Command", PreviewClick) as Sitecore.Web.UI.XmlControls.XmlControl;
      workboxcommand24 = AddControl("WorkboxCommand", "", border7, "", "Icon", "Applications/16x16/document_view.png", "Header", "Open", "Command", Click) as Sitecore.Web.UI.XmlControls.XmlControl;
      workboxcommand25 = AddControl("WorkboxCommand", "", border7, "", "Icon", "Applications/16x16/Document_exchange.png", "Header", "Diff", "Command", DiffClick) as Sitecore.Web.UI.XmlControls.XmlControl;
      placeholder26 = AddPlaceholder("", border7, "");
      
      _Mode = "";
    }
  }
}

