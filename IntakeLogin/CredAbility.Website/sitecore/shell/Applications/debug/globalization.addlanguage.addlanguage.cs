using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class AddLanguage_a_74 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border4;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public System.Web.UI.Control ul6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public System.Web.UI.Control li11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public System.Web.UI.Control li13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public System.Web.UI.Control li15;
    public Sitecore.Web.UI.HtmlControls.Literal literal16;
    public Sitecore.Web.UI.XmlControls.XmlControl Codes;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent17;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.Combobox Predefined;
    public System.Web.UI.Control hr20;
    public Sitecore.Web.UI.HtmlControls.Border border21;
    public Sitecore.Web.UI.HtmlControls.Literal literal22;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel23;
    public Sitecore.Web.UI.HtmlControls.Literal literal24;
    public Sitecore.Web.UI.HtmlControls.Edit Language;
    public Sitecore.Web.UI.HtmlControls.Space space25;
    public Sitecore.Web.UI.HtmlControls.Border border26;
    public Sitecore.Web.UI.HtmlControls.Literal literal27;
    public Sitecore.Web.UI.HtmlControls.Literal literal28;
    public Sitecore.Web.UI.HtmlControls.Edit Region;
    public Sitecore.Web.UI.HtmlControls.Space space29;
    public Sitecore.Web.UI.HtmlControls.Border border30;
    public Sitecore.Web.UI.HtmlControls.Literal literal31;
    public Sitecore.Web.UI.HtmlControls.Literal literal32;
    public Sitecore.Web.UI.HtmlControls.Edit CustomCode;
    public Sitecore.Web.UI.HtmlControls.Border border33;
    public System.Web.UI.Control hr34;
    public Sitecore.Web.UI.HtmlControls.Literal literal35;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel36;
    public Sitecore.Web.UI.HtmlControls.Border border37;
    public Sitecore.Web.UI.HtmlControls.Edit Flag;
    public Sitecore.Web.UI.HtmlControls.Border border38;
    public Sitecore.Web.UI.HtmlControls.Border border39;
    public Sitecore.Web.UI.HtmlControls.ThemedImage FlagImage;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Flags;
    public Sitecore.Web.UI.XmlControls.XmlControl EncodingPage;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent40;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel41;
    public Sitecore.Web.UI.HtmlControls.Literal literal42;
    public Sitecore.Web.UI.HtmlControls.Edit Codepage;
    public Sitecore.Web.UI.HtmlControls.Space space43;
    public Sitecore.Web.UI.HtmlControls.Border border44;
    public Sitecore.Web.UI.HtmlControls.Literal literal45;
    public Sitecore.Web.UI.HtmlControls.Literal literal46;
    public Sitecore.Web.UI.HtmlControls.Edit Encoding;
    public Sitecore.Web.UI.HtmlControls.Space space47;
    public Sitecore.Web.UI.HtmlControls.Border border48;
    public Sitecore.Web.UI.HtmlControls.Literal literal49;
    public Sitecore.Web.UI.HtmlControls.Literal literal50;
    public Sitecore.Web.UI.HtmlControls.Edit Charset;
    public Sitecore.Web.UI.HtmlControls.Space space51;
    public Sitecore.Web.UI.HtmlControls.Border border52;
    public Sitecore.Web.UI.HtmlControls.Literal literal53;
    public Sitecore.Web.UI.XmlControls.XmlControl SpellcheckerPage;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent54;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel55;
    public Sitecore.Web.UI.HtmlControls.Literal literal56;
    public Sitecore.Web.UI.HtmlControls.Edit Spellchecker;
    public Sitecore.Web.UI.HtmlControls.Space space57;
    public Sitecore.Web.UI.HtmlControls.Border border58;
    public Sitecore.Web.UI.HtmlControls.Literal literal59;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border border60;
    public Sitecore.Web.UI.HtmlControls.Literal literal61;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Globalization.AddLanguage.AddLanguageForm,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), wizardform1, "", "Key", "Flags") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        a.scFlag,        a.scFlag:link,        a.scFlag:visited,        a.scFlag:hover,        a.scFlag:active {          color: #303030;          cursor: default;          text-decoration: none;          padding: 3px 6px 4px 6px;          text-align: center;          float: left;        }                a.scFlag:active {          border:1px solid;          border-color: #ddcf9b #c1a877 #dfd8bd #c1a877;          background: #ffe594 url(/sitecore/shell/Themes/Standard/Images/Ribbon/LargeButtonActive.png) repeat-x;          padding: 2px 5px 3px 5px;        }        a.scFlag:hover {          border:1px solid;          border-color: #ddcf9b #c1a877 #dfd8bd #c1a877;          background: #ffe695 url(/sitecore/shell/Themes/Standard/Images/RibbonLargeButtonHover.png) repeat-x;          padding: 2px 5px 3px 5px;          text-decoration: underline;        }        a.scFlagIcon {        }      ", "", stylesheet2, "");
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "Flags/32x32/Flag_generic.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border4 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border4, "", "ID", idref("Welcome"), "Text", "Welcome to the Add Language Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul6 = AddControl("ul", "", FirstPage, "");
      li7 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Select the name of the new language.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Select the language and country/region codes.") as Sitecore.Web.UI.HtmlControls.Literal;
      li11 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li11, "", "Text", "Select the codepage and encoding to use for Web pages.") as Sitecore.Web.UI.HtmlControls.Literal;
      li13 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li13, "", "Text", "Select a spell checker dictionary.") as Sitecore.Web.UI.HtmlControls.Literal;
      li15 = AddControl("li", "", ul6, "", "class", "scWizardBullet");
      literal16 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li15, "", "Text", "Add the language.") as Sitecore.Web.UI.HtmlControls.Literal;
      Codes = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Codes"), "Header", "Language Codes", "Text", "Enter the language codes. Click Next to continue.", "Icon", "Flags/32x32/Flag_generic.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent17 = AddControl("WizardFormIndent", "", Codes, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformindent17, "", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border18, "", "Text", "You may select a predefined language here:") as Sitecore.Web.UI.HtmlControls.Literal;
      Predefined = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), wizardformindent17, "", "ID", idref("Predefined"), "Change", "Predefined_Changed", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Combobox;
      hr20 = AddControl("hr", "", wizardformindent17, "");
      border21 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), wizardformindent17, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal22 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border21, "", "Text", "The language code consists of a language identifier, for example, \"en\" for English and a country/region code, for example, \"US\" for United States. The language code is then \"en-US\".") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel23 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent17, "", "Columns", "2", "CellPadding", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal24 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel23, "", "Text", "Language:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Language = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel23, "", "ID", idref("Language"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space25 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel23, "") as Sitecore.Web.UI.HtmlControls.Space;
      border26 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel23, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal27 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border26, "", "Text", "E.g. \"en\".") as Sitecore.Web.UI.HtmlControls.Literal;
      literal28 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel23, "", "Text", "Country/Region Code:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Region = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel23, "", "ID", idref("Region"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space29 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel23, "") as Sitecore.Web.UI.HtmlControls.Space;
      border30 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel23, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal31 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border30, "", "Text", "E.g. \"US\".") as Sitecore.Web.UI.HtmlControls.Literal;
      literal32 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel23, "", "Text", "Custom Code:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      CustomCode = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel23, "", "ID", idref("CustomCode"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      border33 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel23, "", "GridPanel.ColSpan", "2") as Sitecore.Web.UI.HtmlControls.Border;
      hr34 = AddControl("hr", "", border33, "");
      literal35 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel23, "", "Text", "Icon:", "GridPanel.Align", "right", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel36 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel23, "", "Columns", "2", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      border37 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel36, "", "GridPanel.ColSpan", "2", "Padding", "0px 0px 4px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      Flag = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border37, "", "ID", idref("Flag"), "runat", "server", "Width", "100%", "Value", "Flags/48x48/Flag_generic.png", "Change", "Flag_Changed") as Sitecore.Web.UI.HtmlControls.Edit;
      border38 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel36, "", "Border", "1px inset", "Margin", "0px 16px 0px 0px", "GridPanel.VAlign", "top") as Sitecore.Web.UI.HtmlControls.Border;
      border39 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), border38, "", "Padding", "4px") as Sitecore.Web.UI.HtmlControls.Border;
      FlagImage = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border39, "", "ID", idref("FlagImage"), "Src", "Flags/48x48/Flag_generic.png", "Height", "48", "Width", "48", "runat", "server") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      Flags = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel36, "", "ID", idref("Flags"), "Width", "100%", "Height", "128", "GridPanel.VAlign", "top", "GridPanel.Width", "100%", "GridPanel.Height", "128") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      EncodingPage = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("EncodingPage"), "Header", "Codepage and Encoding", "Text", "Enter the codepage, encoding and charset to use for displaying HTML pages in this language. Click Next to continue.", "Icon", "Flags/32x32/Flag_generic.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent40 = AddControl("WizardFormIndent", "", EncodingPage, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel41 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent40, "", "Columns", "2", "CellPadding", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal42 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel41, "", "Text", "Codepage:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Codepage = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel41, "", "ID", idref("Codepage"), "Value", "65001", "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space43 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel41, "") as Sitecore.Web.UI.HtmlControls.Space;
      border44 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel41, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal45 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border44, "", "Text", "E.g. \"65001\".") as Sitecore.Web.UI.HtmlControls.Literal;
      literal46 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel41, "", "Text", "Encoding:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Encoding = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel41, "", "ID", idref("Encoding"), "Value", "utf-8", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space47 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel41, "") as Sitecore.Web.UI.HtmlControls.Space;
      border48 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel41, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal49 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border48, "", "Text", "E.g. \"utf-8\".") as Sitecore.Web.UI.HtmlControls.Literal;
      literal50 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel41, "", "Text", "Charset:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Charset = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel41, "", "ID", idref("Charset"), "Value", "iso-8859-1", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space51 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel41, "") as Sitecore.Web.UI.HtmlControls.Space;
      border52 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel41, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal53 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border52, "", "Text", "E.g. \"iso-8859-1\".") as Sitecore.Web.UI.HtmlControls.Literal;
      SpellcheckerPage = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("SpellcheckerPage"), "Header", "Checker Dictionary", "Text", "Enter the dictionary to use for spell checking. Click Next to continue.", "Icon", "Flags/32x32/Flag_generic.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent54 = AddControl("WizardFormIndent", "", SpellcheckerPage, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel55 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent54, "", "Columns", "2", "CellPadding", "2", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal56 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel55, "", "Text", "Spellchecker file path:", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Spellchecker = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel55, "", "ID", idref("Spellchecker"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      space57 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel55, "") as Sitecore.Web.UI.HtmlControls.Space;
      border58 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel55, "", "Padding", "0px 0px 8px 0px") as Sitecore.Web.UI.HtmlControls.Border;
      literal59 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border58, "", "Text", "E.g. \"en-UK.tdf\".") as Sitecore.Web.UI.HtmlControls.Literal;
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "Flags/32x32/Flag_generic.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border60 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "", "GridPanel.Style", "padding:0px 0px 16px 0px; font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal61 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border60, "", "Text", "The wizard has completed. Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(Codes, "ID", idref("Codes"));
      SetProperty(Predefined, "ID", idref("Predefined"));
      SetProperty(Language, "ID", idref("Language"));
      SetProperty(Region, "ID", idref("Region"));
      SetProperty(CustomCode, "ID", idref("CustomCode"));
      SetProperty(Flag, "ID", idref("Flag"));
      SetProperty(FlagImage, "ID", idref("FlagImage"));
      SetProperty(Flags, "ID", idref("Flags"));
      SetProperty(EncodingPage, "ID", idref("EncodingPage"));
      SetProperty(Codepage, "ID", idref("Codepage"));
      SetProperty(Encoding, "ID", idref("Encoding"));
      SetProperty(Charset, "ID", idref("Charset"));
      SetProperty(SpellcheckerPage, "ID", idref("SpellcheckerPage"));
      SetProperty(Spellchecker, "ID", idref("Spellchecker"));
      SetProperty(LastPage, "ID", idref("LastPage"));
    }
  }
}

