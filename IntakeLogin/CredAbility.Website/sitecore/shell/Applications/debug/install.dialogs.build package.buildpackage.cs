using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_BuildPackage_a_99 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl wizardform1;
    public Sitecore.Web.UI.XmlControls.XmlControl FirstPage;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Literal Welcome;
    public Sitecore.Web.UI.HtmlControls.Literal literal3;
    public System.Web.UI.Control ul4;
    public System.Web.UI.Control li5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public System.Web.UI.Control li7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public System.Web.UI.Control li9;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.XmlControls.XmlControl SetName;
    public Sitecore.Web.UI.XmlControls.XmlControl wizardformindent11;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel12;
    public Sitecore.Web.UI.HtmlControls.Border border13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Border border15;
    public Sitecore.Web.UI.HtmlControls.Edit PackageFile;
    public Sitecore.Web.UI.XmlControls.XmlControl Building;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel16;
    public Sitecore.Web.UI.HtmlControls.Space space17;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage18;
    public Sitecore.Web.UI.HtmlControls.Space space19;
    public Sitecore.Web.UI.HtmlControls.Border border20;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage21;
    public Sitecore.Web.UI.HtmlControls.Space space22;
    public Sitecore.Web.UI.HtmlControls.Border StatusText;
    public Sitecore.Web.UI.HtmlControls.Literal literal23;
    public System.Web.UI.Control Monitor;
    public Sitecore.Web.UI.XmlControls.XmlControl LastPage;
    public Sitecore.Web.UI.HtmlControls.Border SuccessMessage;
    public Sitecore.Web.UI.HtmlControls.Literal literal24;
    public System.Web.UI.Control br25;
    public Sitecore.Web.UI.HtmlControls.Literal literal26;
    public Sitecore.Web.UI.HtmlControls.Border border27;
    public Sitecore.Web.UI.HtmlControls.Literal literal28;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton29;
    public Sitecore.Web.UI.HtmlControls.Border FailureMessage;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      wizardform1 = AddControl("WizardForm", "", this, "", "CodeBeside", "Sitecore.Shell.Applications.Install.Dialogs.BuildPackage,Sitecore.Client") as Sitecore.Web.UI.XmlControls.XmlControl;
      FirstPage = AddControl("WizardFormFirstPage", "", wizardform1, "", "ID", idref("FirstPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), FirstPage, "", "Class", "scWizardWelcomeTitle") as Sitecore.Web.UI.HtmlControls.Border;
      Welcome = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border2, "", "ID", idref("Welcome"), "Text", "Welcome to the Build Package Wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      literal3 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), FirstPage, "", "Text", "This wizard helps you:") as Sitecore.Web.UI.HtmlControls.Literal;
      ul4 = AddControl("ul", "", FirstPage, "");
      li5 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li5, "", "Text", "Enter a file name for the package.") as Sitecore.Web.UI.HtmlControls.Literal;
      li7 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li7, "", "Text", "Build the package.") as Sitecore.Web.UI.HtmlControls.Literal;
      li9 = AddControl("li", "", ul4, "", "class", "scWizardBullet");
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), li9, "", "Text", "Download the package file.") as Sitecore.Web.UI.HtmlControls.Literal;
      SetName = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("SetName"), "Header", "Package Name", "Text", "Enter a name for the package. Click Next to continue.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      wizardformindent11 = AddControl("WizardFormIndent", "", SetName, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel12 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), wizardformindent11, "", "Width", "100%", "Border", "1px", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border13 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Padding", "0 0 2 0") as Sitecore.Web.UI.HtmlControls.Border;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border13, "", "Text", "Package name:") as Sitecore.Web.UI.HtmlControls.Literal;
      border15 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel12, "", "Padding", "0 0 16 0") as Sitecore.Web.UI.HtmlControls.Border;
      PackageFile = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border15, "", "ID", idref("PackageFile"), "Width", "100%", "maxlength", "200") as Sitecore.Web.UI.HtmlControls.Edit;
      Building = AddControl("WizardFormPage", "", wizardform1, "", "ID", idref("Building"), "Header", "Building", "Text", "This may take a few minutes.", "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel16 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Building, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      space17 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel16, "", "Height", "64") as Sitecore.Web.UI.HtmlControls.Space;
      themedimage18 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel16, "", "Src", "People/48x48/Box_Software.png", "Width", "48", "Height", "48", "GridPanel.Align", "center") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space19 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel16, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      border20 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel16, "", "Align", "center") as Sitecore.Web.UI.HtmlControls.Border;
      themedimage21 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), border20, "", "Src", "Images/progress.gif", "Width", "94", "Height", "17") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      space22 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel16, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      StatusText = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel16, "", "ID", idref("StatusText"), "Align", "center", "GridPanel.Align", "center", "GridPanel.Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Border;
      literal23 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), StatusText, "", "Text", "Generating...") as Sitecore.Web.UI.HtmlControls.Literal;
      Monitor = AddControl("TaskMonitor", "", Building, "", "ID", idref("Monitor"));
      LastPage = AddControl("WizardFormLastPage", "", wizardform1, "", "ID", idref("LastPage"), "Icon", "People/32x32/Box_Software.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      SuccessMessage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "", "ID", idref("SuccessMessage")) as Sitecore.Web.UI.HtmlControls.Border;
      literal24 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), SuccessMessage, "", "Text", "The package has been built successfully.") as Sitecore.Web.UI.HtmlControls.Literal;
      br25 = AddControl("br", "", SuccessMessage, "");
      literal26 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), SuccessMessage, "", "Text", "Click Finish to close the wizard.") as Sitecore.Web.UI.HtmlControls.Literal;
      border27 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), SuccessMessage, "", "Padding", "64 0 0 0") as Sitecore.Web.UI.HtmlControls.Border;
      literal28 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border27, "", "Text", "Click this button to download the package:") as Sitecore.Web.UI.HtmlControls.Literal;
      toolbutton29 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), border27, "", "Icon", "Network/16x16/download.png", "Tooltip", "Download the package", "Click", "buildpackage:download") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      FailureMessage = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), LastPage, "", "ID", idref("FailureMessage"), "Style", "color:red;font-weight:bold;display:none") as Sitecore.Web.UI.HtmlControls.Border;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(FirstPage, "ID", idref("FirstPage"));
      SetProperty(Welcome, "ID", idref("Welcome"));
      SetProperty(SetName, "ID", idref("SetName"));
      SetProperty(PackageFile, "ID", idref("PackageFile"));
      SetProperty(Building, "ID", idref("Building"));
      SetProperty(StatusText, "ID", idref("StatusText"));
      SetProperty(Monitor, "ID", idref("Monitor"));
      SetProperty(LastPage, "ID", idref("LastPage"));
      SetProperty(SuccessMessage, "ID", idref("SuccessMessage"));
      SetProperty(FailureMessage, "ID", idref("FailureMessage"));
    }
  }
}

