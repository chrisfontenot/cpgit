using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class CustomizeRibbon_a_75 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public System.Web.UI.Control TreeList;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Control/32x32/toolbar.png", "Header", "Customize My Toolbar", "Text", "Add or remove commands from My Toolbar.") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "CustomizeRibbon") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .scContentControl {        height:100%;        }        .scContentControlMultilistBox {        height:100%;        }      ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.CustomizeRibbon.CustomizeRibbonForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      TreeList = AddControl("content:TreeList", "", formdialog1, "", "ID", idref("TreeList"), "Source", "/sitecore/content/Applications/Content Editor/Ribbons/Chunks", "Activation", "false", "Style", "background:#e9e9e9;border:none", "DatabaseName", "core", "ExcludeTemplatesForSelection", "Chunk", "AllowMultipleSelection", "true");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(TreeList, "ID", idref("TreeList"));
    }
  }
}

