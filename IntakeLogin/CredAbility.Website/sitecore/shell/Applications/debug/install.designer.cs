using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_Designer_a_82 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.HtmlControls.Script script3;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet4;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet5;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside6;
    public System.Web.UI.Control __ActiveRibbonStrip;
    public Sitecore.Web.UI.HtmlControls.Literal FrameName;
    public Sitecore.Web.UI.HtmlControls.Border Internals;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel7;
    public Sitecore.Web.UI.HtmlControls.Border RibbonFrame;
    public Sitecore.Web.UI.HtmlControls.Border ViewFrame;
    public Sitecore.Web.UI.XmlControls.XmlControl View;
    public Sitecore.Web.UI.WebControls.GridPanel StatusBar;
    public Sitecore.Web.UI.HtmlControls.Border border8;
    public Sitecore.Web.UI.HtmlControls.Literal StatusText;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Application", "Tools/Installer/Designer") as Sitecore.Web.UI.XmlControls.XmlControl;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Content Manager/Content Editor.js") as Sitecore.Web.UI.HtmlControls.Script;
      script3 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Controls/SitecoreKeyboard.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet4 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Content Manager.css", "DeviceDependant", "true", "runat", "server") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet5 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "Ribbon.css", "DeviceDependant", "true", "runat", "server") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside6 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Install.Designer, Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      __ActiveRibbonStrip = AddControl("input", "", formpage1, "", "type", "hidden", "id", idref("__ActiveRibbonStrip"), "name", "__ActiveRibbonStrip");
      FrameName = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), formpage1, "", "ID", idref("FrameName")) as Sitecore.Web.UI.HtmlControls.Literal;
      Internals = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "ID", idref("Internals")) as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel7 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "Height", "100%", "Background", "white", "CellPadding", "0", "CellSpacing", "0") as Sitecore.Web.UI.WebControls.GridPanel;
      RibbonFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "GridPanel.Row.Height", "20px", "ID", idref("RibbonFrame"), "GridPanel.Height", "0px", "Class", "scCaption") as Sitecore.Web.UI.HtmlControls.Border;
      ViewFrame = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel7, "", "ID", idref("ViewFrame"), "GridPanel.Style", "padding:1px", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      View = AddControl("Installer.ProjectView", "", ViewFrame, "", "ID", idref("View")) as Sitecore.Web.UI.XmlControls.XmlControl;
      StatusBar = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel7, "", "ID", idref("StatusBar"), "Width", "100%", "GridPanel.Row.Height", "20px", "Background", "white", "Columns", "2", "CellPadding", "1") as Sitecore.Web.UI.WebControls.GridPanel;
      border8 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), StatusBar, "", "Border", "solid 1px silver") as Sitecore.Web.UI.HtmlControls.Border;
      StatusText = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), border8, "", "ID", idref("StatusText"), "Text", "", "Style", "padding:1px") as Sitecore.Web.UI.HtmlControls.Literal;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(__ActiveRibbonStrip, "id", idref("__ActiveRibbonStrip"));
      SetProperty(FrameName, "ID", idref("FrameName"));
      SetProperty(Internals, "ID", idref("Internals"));
      SetProperty(RibbonFrame, "ID", idref("RibbonFrame"));
      SetProperty(ViewFrame, "ID", idref("ViewFrame"));
      SetProperty(View, "ID", idref("View"));
      SetProperty(StatusBar, "ID", idref("StatusBar"));
      SetProperty(StatusText, "ID", idref("StatusText"));
    }
  }
}

