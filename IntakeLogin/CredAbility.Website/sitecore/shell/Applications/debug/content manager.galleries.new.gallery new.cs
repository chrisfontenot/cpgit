using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Gallery_New_a_99 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl gallery1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.GalleryMenu Options;
    public Sitecore.Web.UI.HtmlControls.MenuPanel menupanel4;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Masters;
    public Sitecore.Web.UI.XmlControls.XmlControl gallery_grip5;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      gallery1 = AddControl("Gallery", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), gallery1, "", "Type", "Sitecore.Shell.Applications.ContentManager.Galleries.New.GalleryNewForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gallery1, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      Options = AddControl(new Sitecore.Web.UI.HtmlControls.GalleryMenu(), gridpanel3, "", "ID", idref("Options"), "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.GalleryMenu;
      menupanel4 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuPanel(), Options, "", "Height", "100%") as Sitecore.Web.UI.HtmlControls.MenuPanel;
      Masters = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), menupanel4, "", "ID", idref("Masters"), "Padding", "0px", "Border", "none", "Background", "transparent") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gallery_grip5 = AddControl("Gallery.Grip", "", gridpanel3, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Options, "ID", idref("Options"));
      SetProperty(Masters, "ID", idref("Masters"));
    }
  }
}

