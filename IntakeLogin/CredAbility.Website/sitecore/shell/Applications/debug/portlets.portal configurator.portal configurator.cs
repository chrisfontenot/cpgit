using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class PortalConfiguratorPortlet_a_128 : Sitecore.Shell.Applications.Today.Portlets.PortalConfiguratorPortletXmlControl   {
    
    // variables
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Portlet = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "") as Sitecore.Web.UI.HtmlControls.Border;
      Window = AddControl("DefaultPortletWindow", "", Portlet, "", "Header", "Portal", "Icon", "Applications/16x16/flash.png") as Sitecore.Web.UI.XmlControls.XmlControl;
      AddContent = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), Window, "", "ID", idref("AddContent"), "Icon", "Applications/24x24/add.png", "Header", "Add content", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(AddContent, "ID", idref("AddContent"));
    }
  }
}

