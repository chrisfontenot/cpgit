using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Installer_ExplicitItemSourceEditor_a_111 : Sitecore.Shell.Applications.Install.Controls.BaseSourceEditor   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Tab tab1;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_listsourceeditor2;
    public Sitecore.Web.UI.HtmlControls.Tab tab3;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_sourceoutputeditor4;
    public Sitecore.Web.UI.HtmlControls.Tab tab5;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_sourceviewer6;
    public Sitecore.Web.UI.HtmlControls.Tab tab7;
    public Sitecore.Web.UI.XmlControls.XmlControl installer_nameeditor8;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Tabs = AddControl(new Sitecore.Web.UI.HtmlControls.Tabstrip(), this, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Tabstrip;
      tab1 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Entries") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_listsourceeditor2 = AddControl("Installer.ListSourceEditor", "", tab1, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab3 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Installation options") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_sourceoutputeditor4 = AddControl("Installer.SourceOutputEditor", "", tab3, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab5 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Preview") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_sourceviewer6 = AddControl("Installer.SourceViewer", "", tab5, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      tab7 = AddControl(new Sitecore.Web.UI.HtmlControls.Tab(), Tabs, "", "Header", "Name") as Sitecore.Web.UI.HtmlControls.Tab;
      installer_nameeditor8 = AddControl("Installer.NameEditor", "", tab7, "", "Padding", "4px") as Sitecore.Web.UI.XmlControls.XmlControl;
      
      _Mode = "";
    }
  }
}

