using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class IDE_Toolbox_a_112 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public Sitecore.Web.UI.HtmlControls.Script script4;
    public Sitecore.Web.UI.HtmlControls.Script script5;
    public System.Web.UI.Control text6;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside7;
    public Sitecore.Web.UI.XmlControls.XmlControl dockwindowchrome8;
    public Sitecore.Web.UI.HtmlControls.ContextMenu ContextMenu;
    public Sitecore.Web.UI.HtmlControls.MenuItem menuitem9;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel10;
    public Sitecore.Web.UI.HtmlControls.Border border11;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar12;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton13;
    public Sitecore.Web.UI.HtmlControls.Scrollbox Panes;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Submittable", "false") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "IDE.css", "DeviceDependant", "true") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "Src", "/sitecore/shell/Applications/Layouts/IDE/Windows/Toolbox/IDE Toolbox.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      script4 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Src", "/sitecore/shell/Applications/Layouts/IDE/IDE.js") as Sitecore.Web.UI.HtmlControls.Script;
      script5 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), formpage1, "", "Key", "Script") as Sitecore.Web.UI.HtmlControls.Script;
      text6 = AddLiteral("        function onSelectionChanged(controlid) {          controlid = '\"' + controlid + '\"';          scForm.postRequest(\"\", \"\", \"\", \"OnSelectionChanged(\" + controlid + \")\");        }      ", "", script5, "");
      codebeside7 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Shell.Applications.Layouts.IDE.Windows.Toolbox.IDEToolboxForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      dockwindowchrome8 = AddControl("DockWindowChrome", "", formpage1, "", "Header", "Toolbox") as Sitecore.Web.UI.XmlControls.XmlControl;
      ContextMenu = AddControl(new Sitecore.Web.UI.HtmlControls.ContextMenu(), dockwindowchrome8, "", "ID", idref("ContextMenu")) as Sitecore.Web.UI.HtmlControls.ContextMenu;
      menuitem9 = AddControl(new Sitecore.Web.UI.HtmlControls.MenuItem(), ContextMenu, "", "Icon", "Applications/16x16/refresh.png", "Header", "Refresh", "Click", "Refresh") as Sitecore.Web.UI.HtmlControls.MenuItem;
      gridpanel10 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), dockwindowchrome8, "", "Width", "100%", "Height", "100%", "Fixed", "true") as Sitecore.Web.UI.WebControls.GridPanel;
      border11 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel10, "") as Sitecore.Web.UI.HtmlControls.Border;
      toolbar12 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), border11, "") as Sitecore.Web.UI.HtmlControls.Toolbar;
      toolbutton13 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar12, "", "Icon", "Applications/16x16/refresh.png", "IconSize", "id16x16", "Header", "Refresh", "Click", "javascript:location.reload(true)", "ToolTip", "Refresh") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      Panes = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel10, "", "ID", idref("Panes"), "Width", "100%", "Height", "100%", "Background", "threedface", "Padding", "0", "ContextMenu", "show:ContextMenu", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(ContextMenu, "ID", idref("ContextMenu"));
      SetProperty(Panes, "ID", idref("Panes"));
    }
  }
}

