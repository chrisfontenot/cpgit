using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class Startbar_a_54 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.HtmlControls.Script script1;
    public Sitecore.Web.UI.HtmlControls.Script script2;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet3;
    public System.Web.UI.Control startmenu4;
    public Sitecore.Web.UI.HtmlControls.Border Startbar;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.ImageButton StartButton;
    public Sitecore.Web.UI.HtmlControls.Space space6;
    public Sitecore.Web.UI.HtmlControls.Scroller StartbarScroller;
    public System.Web.UI.Control StartbarApplications;
    public System.Web.UI.Control tr7;
    public System.Web.UI.Control StartbarTray;
    public System.Web.UI.Control tr8;
    public System.Web.UI.Control td9;
    public Sitecore.Web.UI.HtmlControls.SubmittableEdit SearchBox;
    public System.Web.UI.Control td10;
    public Sitecore.Web.UI.HtmlControls.Border DatabaseName;
    public System.Web.UI.Control Tray;
    public System.Web.UI.Control Clock;
    public Sitecore.Web.UI.HtmlControls.DataContextMenu StartbarMenu;
    public Sitecore.Web.UI.HtmlControls.DataContextMenu StartButtonMenu;
    public Sitecore.Web.UI.HtmlControls.DataContextMenu ApplicationMenu;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      script1 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "Src", "/sitecore/shell/applications/Startbar/Clock.js") as Sitecore.Web.UI.HtmlControls.Script;
      script2 = AddControl(new Sitecore.Web.UI.HtmlControls.Script(), this, "", "Src", "/sitecore/shell/applications/search/Instant/instantsearch.js") as Sitecore.Web.UI.HtmlControls.Script;
      stylesheet3 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), this, "", "Src", "/sitecore/shell/applications/search/Instant/InstantSearch.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      startmenu4 = AddControl("shell:StartMenu", "", this, "");
      Startbar = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), this, "", "ID", idref("Startbar"), "Height", "29", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Startbar, "", "Columns", "4", "Width", "100%", "Height", "29", "Style", "background:url(/sitecore/shell/themes/standard/Images/Startbar/StartbarBackground.png) repeat-x;") as Sitecore.Web.UI.WebControls.GridPanel;
      StartButton = AddControl(new Sitecore.Web.UI.HtmlControls.ImageButton(), gridpanel5, "", "ID", idref("StartButton"), "Src", "Images/Startbar/StartButton.png", "Width", "79", "Height", "29", "Click", "javascript:scForm.showPopup(null, \"StartButton\", \"StartMenu\", \"above\")", "Alt", "Click here to begin.") as Sitecore.Web.UI.HtmlControls.ImageButton;
      space6 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel5, "", "Width", "8") as Sitecore.Web.UI.HtmlControls.Space;
      StartbarScroller = AddControl(new Sitecore.Web.UI.HtmlControls.Scroller(), gridpanel5, "", "ID", idref("StartbarScroller"), "Height", "27", "Width", "100%", "Border", "none", "ContextMenu", "StartbarMenu.Show", "GridPanel.Width", "100%", "GridPanel.VAlign", "bottom") as Sitecore.Web.UI.HtmlControls.Scroller;
      StartbarApplications = AddControl("table", "", StartbarScroller, "", "id", idref("StartbarApplications"), "style", "color:white; font:8pt tahoma", "border", "0", "cellpadding", "0", "cellspacing", "0", "Height", "27");
      tr7 = AddControl("tr", "", StartbarApplications, "");
      StartbarTray = AddControl("table", "", gridpanel5, "", "id", idref("StartbarTray"), "style", "color:white; font:8pt tahoma", "border", "0", "cellpadding", "0", "cellspacing", "0", "height", "27", "GridPanel.Width", "200");
      tr8 = AddControl("tr", "", StartbarTray, "");
      td9 = AddControl("td", "", tr8, "");
      SearchBox = AddControl(new Sitecore.Web.UI.HtmlControls.SubmittableEdit(), td9, "", "ID", idref("SearchBox"), "Submit", "Search") as Sitecore.Web.UI.HtmlControls.SubmittableEdit;
      td10 = AddControl("td", "", tr8, "");
      DatabaseName = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), td10, "", "ID", idref("DatabaseName"), "Class", "scDatabaseName", "onmove", "javascript:this.style.left=(scForm.browser.getControl('Startbar').offsetWidth-this.offsetWidth-4)+'px'") as Sitecore.Web.UI.HtmlControls.Border;
      Tray = AddControl("td", "", tr8, "", "ID", idref("Tray"), "style", "white-space:nowrap");
      Clock = AddControl("td", "", tr8, "", "ID", idref("Clock"), "style", "white-space:nowrap");
      StartbarMenu = AddControl(new Sitecore.Web.UI.HtmlControls.DataContextMenu(), this, "", "ID", idref("StartbarMenu"), "DataSource", "/sitecore/content/Applications/Desktop/Context Menues/Startbar", "RenderAs", "WebControl") as Sitecore.Web.UI.HtmlControls.DataContextMenu;
      StartButtonMenu = AddControl(new Sitecore.Web.UI.HtmlControls.DataContextMenu(), this, "", "ID", idref("StartButtonMenu"), "DataSource", "/sitecore/content/Applications/Desktop/Context Menues/Startbutton", "RenderAs", "WebControl") as Sitecore.Web.UI.HtmlControls.DataContextMenu;
      ApplicationMenu = AddControl(new Sitecore.Web.UI.HtmlControls.DataContextMenu(), this, "", "ID", idref("ApplicationMenu"), "DataSource", "/sitecore/content/Applications/Desktop/Context Menues/ApplicationTab", "RenderAs", "WebControl") as Sitecore.Web.UI.HtmlControls.DataContextMenu;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Startbar, "ID", idref("Startbar"));
      SetProperty(StartButton, "ID", idref("StartButton"));
      SetProperty(StartbarScroller, "ID", idref("StartbarScroller"));
      SetProperty(StartbarApplications, "id", idref("StartbarApplications"));
      SetProperty(StartbarTray, "id", idref("StartbarTray"));
      SetProperty(SearchBox, "ID", idref("SearchBox"));
      SetProperty(DatabaseName, "ID", idref("DatabaseName"));
      SetProperty(Tray, "ID", idref("Tray"));
      SetProperty(Clock, "ID", idref("Clock"));
      SetProperty(StartbarMenu, "ID", idref("StartbarMenu"));
      SetProperty(StartButtonMenu, "ID", idref("StartButtonMenu"));
      SetProperty(ApplicationMenu, "ID", idref("ApplicationMenu"));
    }
  }
}

