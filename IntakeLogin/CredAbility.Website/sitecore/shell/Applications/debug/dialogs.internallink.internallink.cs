using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class InternalLink_a_118 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet stylesheet2;
    public System.Web.UI.Control text3;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside4;
    public Sitecore.Web.UI.HtmlControls.DataContext InternalLinkDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel5;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox6;
    public Sitecore.Web.UI.WebControls.TreeviewEx Treeview;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox7;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Edit Text;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Edit Anchor;
    public Sitecore.Web.UI.HtmlControls.Label label11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Combobox Target;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem13;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem14;
    public Sitecore.Web.UI.HtmlControls.ListItem listitem15;
    public Sitecore.Web.UI.HtmlControls.Panel CustomLabel;
    public Sitecore.Web.UI.HtmlControls.Label label16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Edit CustomTarget;
    public Sitecore.Web.UI.HtmlControls.Literal literal18;
    public Sitecore.Web.UI.HtmlControls.Edit Class;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.Edit Title;
    public Sitecore.Web.UI.HtmlControls.Literal literal20;
    public Sitecore.Web.UI.HtmlControls.Edit Querystring;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/link.png", "Header", "Internal Link", "Text", "Select the item that you want to create a link to and specify the appropriate properties.", "OKButton", "OK") as Sitecore.Web.UI.XmlControls.XmlControl;
      stylesheet2 = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formdialog1, "", "Key", "Style") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      text3 = AddLiteral("        .ff input {           width: 160px;        }              ", "", stylesheet2, "");
      codebeside4 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Shell.Applications.Dialogs.InternalLink.InternalLinkForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      InternalLinkDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formdialog1, "", "ID", idref("InternalLinkDataContext")) as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel5 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Columns", "2", "Width", "100%", "Height", "100%", "CellPadding", "4", "Style", "table-layout:fixed") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox6 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Width", "100%", "Height", "100%", "Class", "scScrollbox scFixSize", "Background", "window", "Padding", "0", "Border", "1px solid #CFCFCF", "GridPanel.VAlign", "top", "GridPanel.Width", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Treeview = AddControl(new Sitecore.Web.UI.WebControls.TreeviewEx(), scrollbox6, "", "ID", idref("Treeview"), "DataContext", "InternalLinkDataContext", "MultiSelect", "False", "Width", "100%") as Sitecore.Web.UI.WebControls.TreeviewEx;
      scrollbox7 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel5, "", "Width", "256", "Height", "100%", "Background", "transparent", "Border", "none", "GridPanel.VAlign", "top", "GridPanel.Width", "256") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox7, "", "CellPadding", "2", "Columns", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Link Description:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Text = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Text")) as Sitecore.Web.UI.HtmlControls.Edit;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Anchor:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Anchor = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Anchor")) as Sitecore.Web.UI.HtmlControls.Edit;
      label11 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), gridpanel8, "", "for", "Target", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Label;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label11, "", "Text", "Target Window:") as Sitecore.Web.UI.HtmlControls.Literal;
      Target = AddControl(new Sitecore.Web.UI.HtmlControls.Combobox(), gridpanel8, "", "ID", idref("Target"), "Width", "100%", "Change", "OnListboxChanged") as Sitecore.Web.UI.HtmlControls.Combobox;
      listitem13 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Self", "Header", "Active browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem14 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "New", "Header", "New browser") as Sitecore.Web.UI.HtmlControls.ListItem;
      listitem15 = AddControl(new Sitecore.Web.UI.HtmlControls.ListItem(), Target, "", "Value", "Custom", "Header", "Custom") as Sitecore.Web.UI.HtmlControls.ListItem;
      CustomLabel = AddControl(new Sitecore.Web.UI.HtmlControls.Panel(), gridpanel8, "", "ID", idref("CustomLabel"), "Background", "transparent", "Border", "none", "GridPanel.NoWrap", "true", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Panel;
      label16 = AddControl(new Sitecore.Web.UI.HtmlControls.Label(), CustomLabel, "", "For", "CustomTarget") as Sitecore.Web.UI.HtmlControls.Label;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), label16, "", "Text", "Custom:") as Sitecore.Web.UI.HtmlControls.Literal;
      CustomTarget = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("CustomTarget")) as Sitecore.Web.UI.HtmlControls.Edit;
      literal18 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Style Class:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Class = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Class")) as Sitecore.Web.UI.HtmlControls.Edit;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Alternate Text:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Title = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Title")) as Sitecore.Web.UI.HtmlControls.Edit;
      literal20 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel8, "", "Text", "Query String:", "GridPanel.NoWrap", "true") as Sitecore.Web.UI.HtmlControls.Literal;
      Querystring = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel8, "", "ID", idref("Querystring")) as Sitecore.Web.UI.HtmlControls.Edit;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(InternalLinkDataContext, "ID", idref("InternalLinkDataContext"));
      SetProperty(Treeview, "ID", idref("Treeview"));
      SetProperty(Text, "ID", idref("Text"));
      SetProperty(Anchor, "ID", idref("Anchor"));
      SetProperty(Target, "ID", idref("Target"));
      SetProperty(CustomLabel, "ID", idref("CustomLabel"));
      SetProperty(CustomTarget, "ID", idref("CustomTarget"));
      SetProperty(Class, "ID", idref("Class"));
      SetProperty(Title, "ID", idref("Title"));
      SetProperty(Querystring, "ID", idref("Querystring"));
    }
  }
}

