using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Web.UI.XmlControls {
  
  // control class
  public class FileLister_a_121 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl Dialog;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside1;
    public Sitecore.Web.UI.HtmlControls.Border border2;
    public Sitecore.Web.UI.HtmlControls.Listview FileLister;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader3;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem4;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem5;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem6;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      Dialog = AddControl("FormDialog", "", this, "", "ID", idref("Dialog"), "Icon", "Applications/32x32/document_out.png", "Header", "Move item to", "Text", "Select the location where you want to move the item. Then click the Move button.", "OKButton", "Move") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside1 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), Dialog, "", "Type", "Sitecore.Shell.Applications.Files.FileLister.FileListerForm,Sitecore.Client") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      border2 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Dialog, "", "Background", "white", "Border", "1px inset", "Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      FileLister = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), border2, "", "ID", idref("FileLister"), "DblClick", "OnFileListerDblClick", "View", "Details") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader3 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), FileLister, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem4 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader3, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem5 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader3, "", "Name", "size", "Header", "Size") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem6 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader3, "", "Name", "modified", "Header", "Date modified") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Dialog, "ID", idref("Dialog"));
      SetProperty(FileLister, "ID", idref("FileLister"));
    }
  }
}

