﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SitecoreClientTranslation.aspx.cs" Inherits="Sitecore.Shell.Applications.Globalization.SitecoreClientTranslation.SitecoreClientTranslation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
  <form id="form1" runat="server">
    <h2 style="margin: 0; padding: 0">Select copy-paste languages</h2> 
    <p>If checked, Sitecore will look for the duplicate phrases in items and translate all of them if it knows the translation (using dictionary) <br />
       If unchecked, duplicate phrases in different items will have to be translated manually.
    </p>
    <div style="margin-bottom: 2em">
      <asp:PlaceHolder ID="CopyPasteEditingControls" runat=server />
    </div>
    
  
    <input type="submit" value="Go" style="width: 85px"/>
  </form>
</body>
</html>
