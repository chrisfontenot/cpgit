﻿<%@ Page language="c#" %>
<%@ Register TagPrefix="ces" Namespace="Coveo.CES.Web.Search.Controls" Assembly="Coveo.CES.Web.Search, Version=6.0.0.0, Culture=neutral, PublicKeyToken=44110d16825221f2" %>
<%@ Assembly Name="Coveo.CNL, Version=6.0.0.0, Culture=neutral, PublicKeyToken=44110d16825221f2" %>
<%@ Assembly Name="Coveo.CNL.Web, Version=6.0.0.0, Culture=neutral, PublicKeyToken=44110d16825221f2" %>
<%@ Assembly Name="Coveo.CES.Interops, Version=6.0.0.0, Culture=neutral, PublicKeyToken=44110d16825221f2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
void c_OverrideUser(object p_Sender, OverrideUserEventArgs p_Args) 
{
    /* UNCOMMENT THIS SECTION BEFORE FIRST USE
    // The name of the security provider defined under the Admin Tool
    string securityProviderName = "Sitecore Security Provider";

    Coveo.CES.Interops.CESSearch.UserIDFactory factory = new Coveo.CES.Interops.CESSearch.UserIDFactory();

    // Get the username from SitecoreAPI
    string userName = Sitecore.Context.GetUserName().ToLower();

    // Add to the collection of identities
    Coveo.CES.Web.Search.Providers.COM.COMUserIdentity user = new Coveo.CES.Web.Search.Providers.COM.COMUserIdentity(factory.CreateExternalUserID(userName, securityProviderName, false, null));
    p_Args.AdditionalIdentities.Add(user);
    */
}
    
protected override void OnInit(EventArgs p_Args)
{
    /* UNCOMMENT THIS SECTION BEFORE FIRST USE
    SearchBinding.MainSearchObject.SetupSearchBuilder += this.Search_SetupSearchBuilder;
    */
    base.OnInit(p_Args);
}

void Search_SetupSearchBuilder(object p_Sender, SetupSearchBuilderEventArgs p_Args)
{ 
    /* UNCOMMENT THIS SECTION BEFORE FIRST USE
    p_Args.Builder.AddConstantExpression("@Source=MySourceName");
    */
}
</script>
<script src="/sitecore/shell/Applications/Coveo/CoveoSheerUI.js" type="text/javascript"></script>

<html>
<head id="Head1" runat="server">
    <link href="/default.css" rel="stylesheet" />
</head>
  <body style="background-color: #FFFFFF">
    <form id="f" runat="server">
        <iframe name="ItemHandler" src="/sitecore/shell/applications/coveo/CoveoSearchFrame.aspx" height="1px" width="1px" style="visibility:hidden"></iframe>
        <ces:SearchInterface id="c" runat="server" OnOverrideUser="c_OverrideUser" />
    </form>
  </body>
</html>

