﻿//****************************************************************************
// Copyright (c) 2009, Coveo Solutions Inc.
//****************************************************************************

//****************************************************************************
// Posts a click event that will trigger the OpenItem method handler. The 
// OpenItem method is defined in the SheerUIHandler class that is specified
// in the CoveoSearchFrame.aspx under the CodeBeside control.
//****************************************************************************
OpenItem = function(p_ItemID, p_Language, p_Version) 
{
    scForm.postEvent(this,'click','OpenItem(&quot;' + p_ItemID + '&quot;, &quot;' + p_Language + '&quot;, &quot;' + p_Version + '&quot;)');
}

//****************************************************************************
// Calls the javascript OpenItem method contained on the IFrame. For technical
// reasons, the call should be called from an IFrame to bypass the AjaxManager
// mechanism.
//****************************************************************************
callItemHandler = function(p_ItemID, p_Language, p_Version) 
{
  this.parent.frames[0].frames[1].OpenItem(p_ItemID, p_Language, p_Version);
}