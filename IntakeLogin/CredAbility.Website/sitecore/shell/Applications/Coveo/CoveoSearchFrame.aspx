﻿<%@ Page language="c#" Inherits="Sitecore.Web.UI.Sheer.ClientPage" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.HtmlControls" Assembly="Sitecore.Kernel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script src="/sitecore/shell/Applications/Coveo/CoveoSheerUI.js" type="text/javascript"></script>

<html>
  <body style="background-color: #FFFFFF">
    <form id="f" runat="server">
        <sc:CodeBeside runat="server" Type="Coveo.CES.CustomCrawlers.Sitecore60.SheerUI.SheerUIHandler" />
    </form>
  </body>
</html>