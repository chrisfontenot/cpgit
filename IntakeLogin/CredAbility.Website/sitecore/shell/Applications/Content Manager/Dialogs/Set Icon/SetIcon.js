function scClick(element, evt) {
  var icon = evt.srcElement;
  
  var edit = scForm.browser.getControl("IconFile");
  
  var src = scForm.browser.getImageSrc(icon);
  
  if (src == null) {
    return;
  }
  
  var n = src.indexOf("/~/icon/");
  
  if (n >= 0) {
    src = src.substr(n + 8); 
  }
  else if (src.substr(0, 32) == "/sitecore/shell/themes/standard/") {
    src = src.substr(32); 
  }
  
  if (src.substr(src.length - 5, 5) == ".aspx") {
    src = src.substr(0, src.length - 5);
  }
  
  edit.value = src;
}

function scChange(element, evt) {
  var element = scForm.browser.getControl("Selector");
  
  var id = element.options[element.selectedIndex].value + "List";
  
  var list = scForm.browser.getControl("List");
  
  var childNodes = list.childNodes;
  
  for(var n = 0; n < childNodes.length; n++) {
    var element = childNodes[n];
    
    element.style.display = (element.id == id ? "" : "none");
  }
}