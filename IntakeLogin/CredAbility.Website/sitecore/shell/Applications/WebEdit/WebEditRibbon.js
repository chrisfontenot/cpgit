function scSave(postaction) {
  var command = 'webedit:save';
  
  if (postaction != null) {
    command += "(postaction=" + postaction + ")";
  }

  scForm.postRequest('','','',command);
}

function scBeginEdit() {
  scForm.postRequest('','','','webedit:beginedit');
}

function scNew() {
  scForm.postRequest('','','','webedit:new');
}

function scDelete(id) {
  scForm.postRequest('','','','webedit:delete(id=' + id + ')');
}

function scSetLayout() {
  scForm.postRequest('','','','webedit:changelayout');
}

function scCancel() {
  scForm.setCookie("sitecore_webedit_editing", "");
  window.top.location.href = scSetDesigning(false);
}

function scClose() {
  scForm.postRequest('','','','webedit:close');
}

function scNavigate(href) {
  window.top.location.href = href;
}

function scSetHtmlValue(controlid, preserveInnerContent) {
  var ctl = $("scHtmlValue");
  if (ctl == null) {
    return;
  }
  
  var plainValueControl = $("scPlainValue");
  if (plainValueControl == null) {
    return;
  }
  
  var value = ctl.value;
  var plainValue = plainValueControl.value;
  
  ctl.value = "";
  plainValueControl.value = "";
  
  var win = window.parent;
  if (win == null) {
    return;
  }
 
  var doc = win.document;
  
  ctl = doc.getElementById(controlid); 
  
  if (ctl != null) {
    if (plainValue) {
      ctl.value = plainValue;
    }
    else {
      ctl.value = value;
    }
  }
 
  ctl = doc.getElementById(controlid + "_edit"); 
  if (ctl == null) {
    alert("control '" + controlid + "_edit" + "' not found");
    return;
  }
    
  if (!preserveInnerContent) {
    ctl.innerHTML = value;
  }
  else {
    //Instead of setting whole innerHtml only setting needed attributes
    //Useful when replacing tags that contain children, like links.
    var targetCtl = ctl.firstChild;
    for (i = 0; i < targetCtl.attributes.length; i++) {
      if (targetCtl.attributes.item(i).specified) {
        targetCtl.attributes.removeNamedItem(targetCtl.attributes.item(i).name);
      }
    }

    attributesString = value.substring(1, value.indexOf(">"))
    var attributesArray = attributesString.split(" ");
    attributesArray.each(function(attribute) {
      var nameValuePair = attribute.split("=");
      if (nameValuePair.length == 2) {
        targetCtl[nameValuePair[0]] = nameValuePair[1].substring(1, nameValuePair[1].length - 1);
      }
    });
  }
  
  win.Sitecore.WebEdit.setModified(controlid);
}

function scShowRibbon() {
  var ribbon = $("RibbonPanel");
  
  ribbon.toggle();
  
  scAdjustSize();
  
  scForm.setCookie("sitecore_webedit_ribbon", ribbon.visible() ? "1" : "0");
  
  var sitecore = window.top.Sitecore
  if (typeof sitecore != "undefined") {
    sitecore.WebEdit.hide();
  }
}

function scShowPalette() {
  window.top.location.href = scSetDesigning(true);
}

function scOnLoad() {
  scAdjustSize();
}

function scAdjustSize() {
  var frame = scForm.browser.getFrameElement(window);    
  
  frame.style.height = "" + document.body.scrollHeight + "px";
}

scRequest.prototype.buildFieldsEx = scRequest.prototype.buildFields;

scRequest.prototype.buildFields = function(doc) {
  this.buildFieldsEx(parent.document);
  
  this.form = this.form.replace("__VIEWSTATE=", "__VIEWSTATE2=")
  
  this.buildFieldsEx();
}

scContentEditor.prototype.setActiveStripEx = scContentEditor.prototype.setActiveStrip;

scContentEditor.prototype.setActiveStrip = function(id, toggleRibbon) {
  scContentEditor.prototype.setActiveStripEx(id, toggleRibbon);
  
  var ctl = scForm.browser.getControl("scActiveRibbonStrip");
  
  if (ctl != null) {
    scForm.setCookie("sitecore_webedit_activestrip", ctl.value);
  }
}

function scSetDesigning(enabled) {
  var page = window.top.location.href;
  
  var params = page.toQueryParams();
  
  if (enabled) {
    params["sc_de"] = "1";
  }
  else {
    delete params["sc_de"];
  }
  
  var n = page.indexOf("?");
  if (n >= 0) {
    page = page.substr(0, n + 1);
  }
  else {
    page += "?";
  }

  return page + Object.toQueryString(params);
}

function scSetModified(modified) {
  scForm.browser.getParentWindow(scForm.browser.getFrameElement(window).ownerDocument).Sitecore.WebEdit.modified = false;
}