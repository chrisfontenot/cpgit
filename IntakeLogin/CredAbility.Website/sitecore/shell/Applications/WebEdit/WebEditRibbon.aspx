<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebEditRibbon.aspx.cs" Inherits="Sitecore.Shell.Applications.WebEdit.WebEditRibbon" %>
<%@ Import Namespace="Sitecore"%>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.HtmlControls" Assembly="Sitecore.Kernel" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
  <title>Sitecore</title>
  <sc:Stylesheet runat="server" Src="Content Manager.css" DeviceDependant="true"/><asp:placeholder id="Stylesheets" runat="server" />
  <sc:Stylesheet runat="server" Src="Ribbon.css" DeviceDependant="true" />

  <sc:Stylesheet runat="server" Src="/sitecore/shell/Applications/WebEdit/WebEditRibbon.css"/>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/controls/SitecoreObjects.js"></script>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/controls/SitecoreKeyboard.js"></script>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/controls/SitecoreModifiedHandling.js"></script>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/controls/SitecoreVSplitter.js"></script>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/controls/SitecoreWindow.js"></script>
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/Applications/Content Manager/Content Editor.js"></script>  
  <script type="text/JavaScript" language="javascript" src="/sitecore/shell/Applications/WebEdit/WebEditRibbon.js"></script>  
  
  <style type="text/css">
    #Buttons 
    {
      /* background: #4E4E4E url(/sitecore/shell/themes/standard/gradients/gray1.gif) 50% 0 repeat-y; */
    }
    #Buttons button {
      margin:2px;
      width:85px;
      height: 19px;
      color:black;
      font:8pt tahoma;
      background:#d9dbe1 url(/sitecore/shell/themes/standard/images/RibbonNavigatorButtonActive.png) repeat-x;
      border:1px solid #bebebe;
    }
    
    .scButtonDown 
    {
      background:#606060;
    }
    
    .scRibbonNavigator {
    }
  </style>
  
</head>
<body onload="javascript:scOnLoad()">
  <input type="hidden" id="scActiveRibbonStrip" name="scActiveRibbonStrip" />
  <input type="hidden" id="scHtmlValue" name="scHtmlValue" />
  <input type="hidden" id="scPlainValue" name="scPlainValue" />
  <input type="hidden" id="scLayoutDefinition" name="scLayoutDefinition" />
  <input type="hidden" id="scRendering" name="scRendering" />
  <input type="hidden" id="scOpenProperties" name="scOpenProperties" />
  
  <sc:CodeBeside runat="server" Type="Sitecore.Shell.Applications.WebEdit.WebEditRibbonForm,Sitecore.Client"/>
  
  <form id="RibbonForm" runat="server">
    <sc:Border ID="Buttons" runat="server" />

    <sc:Border id="RibbonPanel" Class="scRibbonPanel" runat="server">
      <sc:Border ID="Ribbon" runat="server"/>
   
      <sc:Border id="TreecrumbPane" class="scTreecrumb" runat="server">
        <sc:Border ID="Treecrumb" runat="server" Class="scTreecrumbBar" />
        <div class="scTreecrumbBottomLine">
        </div>
      </sc:Border>
    </sc:Border>
    
    <input id="__FRAMENAME" type="hidden" value="Shell"/>
  </form>
</body>
</html>
