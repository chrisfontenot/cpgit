﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Routing;
using CredAbility.Core.Client;
using CredAbility.Data.Identity;
using CredAbility.Web.API;
using CredAbility.Web.API.Processors;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.CreditScoreChallenge;
using Sitecore.Links;
using Sitecore.Web;
using StructureMap;


namespace CredAbility.Website
{
    public partial class MvcApplication : HttpApplication
    {
        /*
                * Application_BeginRequest
                * Application_AuthenticateRequest
                * Application_AuthorizeRequest
                * Application_ResolveRequestCache
                * Application_AcquireRequestState
                * Application_PreRequestHandlerExecute
                * Application_PreSendRequestHeaders
                * Application_PreSendRequestContent
                * <<code is executed>>
                * Application_PostRequestHandlerExecute
                * Application_ReleaseRequestState
                * Application_UpdateRequestCache
                * Application_EndRequest
         
         */

        protected void Application_Start()
        {
            RouteRegistrar.RegisterRoutesTo(RouteTable.Routes);
			ConfigureDependencyInjection();
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {

//            ClientProfileManager profileManager = new ClientProfileManager();
//
//
//            AuthentificationContext context = new AuthentificationContext();
//
//
//
//
//            //Check if we have a redirection to do. (Single Sign On)
//            var losCode = WebUtil.GetQueryString("LOS");
//
//            if (!string.IsNullOrEmpty(losCode))
//            {
//                context.LosCode = losCode;
//            }
//
//            //If the user is connected and we have a redirect command (LOS) then we redirect.
//            //Else, the user will be transfert to the page he ask (example: login, create account, forgot password ...)
//            if (Sitecore.Context.User.IsAuthenticated && !string.IsNullOrEmpty(losCode) && context.IsActiveLosCodeRedirection)
//            {
//                profileManager.RedirectBack();
//            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //TODO : CHECK REFCODE
            if (Sitecore.Context.Domain.Name == "extranet")
            {
                //Get the Ref Code
                var refCode = WebUtil.GetQueryString("RefCode");

                if (!string.IsNullOrEmpty(refCode))
                {
                    new AuthentificationContext {ReferralCode = refCode};
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Context != null)
            {
                Exception ex = Context.Error;
                Sitecore.Diagnostics.Log.Error("The website cause an error", ex);
                if (ex is TimeoutException)
                {
                    Response.Redirect("/service/error.aspx");
                }
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (Sitecore.Context.IsLoggedIn)
            {
                Sitecore.Security.Authentication.AuthenticationManager.Logout();
            }
        }

		private void ConfigureDependencyInjection()
		{
			ObjectFactory.Initialize(r =>
			{
				r.AddRegistry<RegisterServices>();
			});
		}
	}
}