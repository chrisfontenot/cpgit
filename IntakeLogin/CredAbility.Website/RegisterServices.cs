﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using StructureMap.Configuration.DSL;
using CredAbility.Data.Document;

namespace CredAbility.Website
{
	public class RegisterServices:Registry
	{
		public RegisterServices()
		{
			this.For<IPrefilingDocResolver>().AddInstances(i => 
			{
				i.OfConcreteType<CredAbilityRepository>().Named("CredAbilityDocs");
				i.OfConcreteType<OmiRepository>().Named("OmiDocs");
			});
		}
	}
}
