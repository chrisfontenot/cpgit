<%@ Page Language="C#" AutoEventWireup="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 

<html>
<head>
    <link rel="stylesheet" type="text/css" href="~/minify/combo.ashx?f=/static/js/yui3/cssreset/reset-min.css,/static/js/yui3/cssfonts/fonts-min.css&m=css&ct=text/css">
    <link rel="stylesheet" type="text/css" href="~/minify/combo.ashx?f=/static/css/base.css,/static/css/public.css&m=css&ct=text/css"/>
    <script type="text/javascript">
        document.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"~/minify/combo.ashx?f=/static/css/initiate.css&m=css&ct=text/css\" media=\"screen\"/>");
    </script>
    
    <style>
    .body {padding:0} .wrapper{width:auto;}
    </style>
</head>
<body>
		<div class="wrapper" id="ctl1">
	<form id="form1" runat="server">
        <div class="body" id="ctl2">
           <div class="content-full" id="ctl3">
            <sc:placeholder runat="server" id="content" key="content-body"></sc:placeholder>
            </div> 
            <div class="clearboth" id="ctl6"></div>
        </div>
	</form>
       
    </div>
</body>
</html>
