﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Web;
using CredAbility.Web.Authentification;

namespace CredAbility.Website.layouts
{
    public partial class ProfileInfo : UserControl
    {
        protected ClientProfile ClientProfile { get; set; }
        protected string displayId { get; set; }

        protected string ThumbnailUrl
        {
            get
            {
                return AvatarHelper.GetThumbnailFilePath((this.ClientProfile.Avatar));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClientProfile = ClientProfileManager.CurrentClientProfile;
        }
    }
}