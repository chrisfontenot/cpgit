﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Authentification;
using CredAbility.Web.SitecoreLib.Data;
using CredAbility.Web.SitecoreLib.Item.Interfaces;

namespace CredAbility.Website.layouts
{
    public partial class Recommended_Courses : System.Web.UI.UserControl
    {
        public string LinkText = "See our recommended Podcast offerings";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Sitecore.Context.Site.Name.ToLower().Contains("es"))
            {
                LinkText = "Vea nuestras recomendaciones de Podcast";
            }
        }
    }
}