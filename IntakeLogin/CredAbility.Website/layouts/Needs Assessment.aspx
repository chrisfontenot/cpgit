<%@ Page language="c#" Codepage="65001" AutoEventWireup="true"  MasterPageFile="~/MasterPages/MainView.Master" inherits="System.Web.Mvc.ViewPage" %>
<%@ Import Namespace="CredAbility.Web.SitecoreLib.ViewData"%>

<asp:Content runat="server" ContentPlaceHolderID="PageBody">    
    <div class="needs-assessment">
        <div class="c-body">
	        <div id="assessment-selection" class="<%= (ViewData.Model as NeedsAssessmentViewData).GetCurrentStep.ClassName %>">
		        <div class="steps">
		        
		              <%if (((ViewData.Model as NeedsAssessmentViewData).CurrentStep) != 1)
                                                                                   {%>
		        
		                <%= string.Format(Resources.Website.NeedAssessment_CurrentStepOf, (ViewData.Model as NeedsAssessmentViewData).CurrentPage, (ViewData.Model as NeedsAssessmentViewData).MaxPage)%> 
		        
		                
		            <%}else{%> 
		            
		                <%= string.Format(Resources.Website.NeedAssessment_CurrentStep, (ViewData.Model as NeedsAssessmentViewData).CurrentPage)%>
		            
		            
		            <% }%>
		        
		        </div>
		        <div class="HeadTitle">
				<span class="stepIcon"></span><h1><%= (ViewData.Model as NeedsAssessmentViewData).GetCurrentStep.Text %></h1>
                    		<% Html.RenderAction((ViewData.Model as NeedsAssessmentViewData).GetCurrentStep.ActionName, "NeedsAssessment", new { VD = ViewData.Model }); %>
			</div>
            </div>
        </div>
	<script>
		var slidersInit = true;
	</script>
    </div>
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="Headline">
<form id="form2">
	<ca:RandomHeadline runat="server" />
</form>
</asp:Content>