﻿using System;
using CredAbility.Core.Client;
using CredAbility.Web;
using CredAbility.Web.Authentification;

namespace CredAbility.Website.layouts
{
    public class My_Account_Headline : System.Web.UI.UserControl
    {
        protected ClientProfile ClientProfile { get; set; }
        protected string HeadlineUrl { 
            get{
                return AvatarHelper.GetHeadlineFilePath(this.ClientProfile.Avatar);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClientProfile = ClientProfileManager.CurrentClientProfile;
        }
    }
}
