﻿    <%@ Control Language="c#" AutoEventWireup="true"  CodeBehind="My_Account_Headline.ascx.cs" Inherits="CredAbility.Website.layouts.My_Account_Headline" %>
 <div class="headline" style="background-image:url(<%= HeadlineUrl %>)">
 	<span class="welcome"><% if(ClientProfile.IsFirstTime)
 {%>
     <%=string.Format(Resources.MyAccount.Headline_FirstWelcome, ClientProfile.FirstName)%>
 <%}else
 {%>
    <%= string.Format(Resources.MyAccount.Headline_Welcome, ClientProfile.FirstName)%>
<% } %></span>
 </div>
    