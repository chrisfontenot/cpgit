﻿<%@ Control Language="c#" AutoEventWireup="true" Inherits="CredAbility.Web.UI.UserControls.CourseBaseControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<h1>
    <sc:text field="Subhead" runat="server"/>
</h1>

        <script type="text/javascript" src="/JavaScript/Education.js"></script>

<div class="course-info article-info">
    <div class="posted">
        <asp:Literal runat="server" Text="<%$Resources:Website, Course_Posted%>"/> <sc:date field="Posted" format="D"  runat="server"/>
    </div>
    
   <%-- <ca:ItemRate runat="server"/>--%>
    <div class="clearboth"></div>
</div>

<sc:text field="Content"  runat="server"/>


<%--<div class="rate-this">
    <div><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:Website, Course_RateThisCourse%>"/></div> <ca:ItemRater runat="server"/>
</div>--%>

