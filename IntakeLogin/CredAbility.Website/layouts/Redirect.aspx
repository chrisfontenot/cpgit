<%@ Page language="c#" Codepage="65001" AutoEventWireup="true" %>
<%@ Import Namespace="Sitecore.Shell.Applications.ContentManager"%>
<%@ Import Namespace="Sitecore.Links"%>
<%
    var url = "";
    if(Sitecore.Context.Item.TemplateName == "External Redirect")
    {
        url = Sitecore.Context.Item.Fields["Url"].Value;
    }
    else
    {
        url = LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(Sitecore.Context.Item.Fields["Linked Item"].Value));
    }

    Response.Redirect(url); 
 %>