﻿using System;
using System.Web;
using CredAbility.Core.Common;
using CredAbility.Web.Authentification;
using CredAbility.Web.Configuration;
using CredAbility.Web.UI.UserControls;


namespace CredAbility.Website.layouts
{
    public partial class login : FormBaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

            ErrorSeeBelow.Visible = false;


        }
        
        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            var clientProfileManager = new ClientProfileManager();
            ActionResult actionResult = null;

            AuthentificationContext.Current.IsActiveLosCodeRedirection = false;

            actionResult = clientProfileManager.Login(this.Username.Text, this.Password.Text, false, AuthentificationContext.Current.HasLosCode, false);



            if (Sitecore.Context.User.IsAuthenticated || AuthentificationContext.Current.IsLogged)
            {
                HttpContext.Current.Response.Redirect(ConfigReader.ConfigSection.Url.MyActivity.Value);
            }


            if (!actionResult.IsSuccessful && !actionResult.Messages.HasMessage(string.Empty))
            {
                ErrorSeeBelow.Visible = true;
            }
            else if (actionResult.Messages.HasMessage(string.Empty))
            {
                ErrorSeeBelow.Visible = false;
                this.ResultMessages.Messages = actionResult.Messages;
            }
            this.Messages = actionResult.Messages;

        }
    }
}