﻿using System;
using System.Web.UI;
using System.Threading;
using System.Globalization;

namespace CredAbility.Website.layouts
{
    public partial class User_Control : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.UI.Control control = null;
            if (Sitecore.Context.Item.Fields["User Control Path"] == null || string.IsNullOrEmpty(Sitecore.Context.Item.Fields["User Control Path"].Value))
            {
                foreach (Sitecore.Globalization.Language compare in Sitecore.Context.Item.Database.Languages)
                {
                    if (compare.Name == "en")
                    {
                        control = LoadControl(Sitecore.Context.Item.Database.GetItem(Sitecore.Context.Item.ID, compare).Fields["User Control Path"].Value);
                    }
                }
            }
            else
            {
                control = LoadControl(Sitecore.Context.Item.Fields["User Control Path"].Value);
            }
            PlaceHolder.Controls.Add(control); 
        }
    }
}