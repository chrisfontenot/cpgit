onunload = function()
{
	EndClass();
};

// Get Requist Objict
function GetReqObj()
{
	var xmlHttp;
	try
	{
// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				xmlHttp = null;
			}
		}
	}
	return xmlHttp;
}
// EndClass
function EndClass()
{
	var xmlHttp;
	xmlHttp = GetReqObj();
	if(xmlHttp != null)
	{
		xmlHttp.open("GET","/CloseClass.aspx",false);
		xmlHttp.send(null);
	}
}
