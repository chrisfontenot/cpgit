using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class SentPage_a_136 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext LocalDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton5;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton6;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton8;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox9;
    public Sitecore.Web.UI.HtmlControls.DataListview Mails;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "silver") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.SentPage,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      LocalDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("LocalDataContext"), "DataViewName", "Master", "Folder", "/sitecore/content/Modules/Mailing List/Sent") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Columns", "2", "CellPadding", "5", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "GridPanel.VAlign", "top", "GridPanel.Width", "120") as Sitecore.Web.UI.WebControls.GridPanel;
      toolbutton5 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail.png", "Click", "mailinglist:open", "Header", "Open") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail_exchange.png", "Click", "mailinglist:copy", "Header", "Copy to outbox") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail_out.png", "Click", "mailinglist:sendqueued", "Header", "Send queued mails") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail_delete.png", "Click", "mailinglist:cancelqueued", "Header", "Cancel queued mails") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      scrollbox9 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "ContextMenu", "GetContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Mails = AddControl(new Sitecore.Web.UI.HtmlControls.DataListview(), scrollbox9, "", "ID", idref("Mails"), "DataContext", "LocalDataContext", "DblClick", "mailinglist:open") as Sitecore.Web.UI.HtmlControls.DataListview;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(LocalDataContext, "ID", idref("LocalDataContext"));
      SetProperty(Mails, "ID", idref("Mails"));
    }
  }
}

