using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class OutboxPage_a_120 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.DataContext LocalDataContext;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton5;
    public Sitecore.Web.UI.HtmlControls.Toolbutton TBtnNewMail;
    public Sitecore.Web.UI.HtmlControls.Toolbutton TBtnDelete;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton6;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox7;
    public Sitecore.Web.UI.HtmlControls.DataListview Mails;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "silver") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.OutboxPage,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      LocalDataContext = AddControl(new Sitecore.Web.UI.HtmlControls.DataContext(), formpage1, "", "ID", idref("LocalDataContext"), "DataViewName", "Master", "Folder", "/sitecore/content/Modules/Mailing List/Outbox") as Sitecore.Web.UI.HtmlControls.DataContext;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Columns", "2", "CellPadding", "5", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "GridPanel.VAlign", "top", "GridPanel.Width", "120") as Sitecore.Web.UI.WebControls.GridPanel;
      toolbutton5 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail.png", "Click", "mailinglist:open", "Header", "Edit selected letter") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      TBtnNewMail = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "ID", idref("TBtnNewMail"), "Icon", "Network/32x32/mail_write.png", "Click", "mailinglist:newmail", "Header", "Create a new letter") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      TBtnDelete = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "ID", idref("TBtnDelete"), "Icon", "Network/32x32/mail_delete.png", "Click", "mailinglist:delete", "Header", "Delete selected letter") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Network/32x32/mail_out.png", "Click", "mailinglist:send", "Header", "Send out mail") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      scrollbox7 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "ContextMenu", "GetContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Mails = AddControl(new Sitecore.Web.UI.HtmlControls.DataListview(), scrollbox7, "", "ID", idref("Mails"), "DataContext", "LocalDataContext", "DblClick", "mailinglist:open") as Sitecore.Web.UI.HtmlControls.DataListview;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(LocalDataContext, "ID", idref("LocalDataContext"));
      SetProperty(TBtnNewMail, "ID", idref("TBtnNewMail"));
      SetProperty(TBtnDelete, "ID", idref("TBtnDelete"));
      SetProperty(Mails, "ID", idref("Mails"));
    }
  }
}

