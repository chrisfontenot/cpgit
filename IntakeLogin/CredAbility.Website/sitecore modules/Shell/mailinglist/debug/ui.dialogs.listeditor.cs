using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class ListEditor_a_135 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.Literal MailingListName;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Memo Description;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/Mail.png", "Header", "Edit mailing list", "Text", "Change mailing list settings as required and click Save button.", "OKButton", "Save") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Modules.MailingList.UI.Dialogs.ListEditor,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      MailingListName = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "ID", idref("MailingListName"), "GridPanel.Height", "20") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "2", "CellPadding", "5", "Width", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Description:", "GridPanel.VAlign", "top", "GridPanel.Width", "100", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Description = AddControl(new Sitecore.Web.UI.HtmlControls.Memo(), gridpanel4, "", "ID", idref("Description"), "Width", "100%", "Height", "200") as Sitecore.Web.UI.HtmlControls.Memo;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(MailingListName, "ID", idref("MailingListName"));
      SetProperty(Description, "ID", idref("Description"));
    }
  }
}

