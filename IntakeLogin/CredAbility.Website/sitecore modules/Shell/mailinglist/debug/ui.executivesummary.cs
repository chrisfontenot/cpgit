using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class ExecutiveSummary_a_131 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel Caption;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage4;
    public Sitecore.Web.UI.HtmlControls.Border Title;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Toolbar Toolbar;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Listbox Start;
    public Sitecore.Web.UI.HtmlControls.Space space7;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Listbox End;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider9;
    public Sitecore.Web.UI.HtmlControls.Toolbutton btnProcessRange;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider10;
    public Sitecore.Web.UI.HtmlControls.Toolbutton btnPrint;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider11;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox12;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel13;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scMails;
    public Sitecore.Web.UI.HtmlControls.Literal literal15;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scLists;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "#dcdcdc") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.ExecutiveSummary,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "height", "100%", "fixed", "true", "colums", "1") as Sitecore.Web.UI.WebControls.GridPanel;
      Caption = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "ID", idref("Caption"), "Style", "border-bottom: #ffffff solid 1px", "Width", "100%", "Columns", "2", "CellSpacing", "2px", "GridPanel.Height", "0", "Background", "#5A6B7D url(/sitecore/shell/themes/standard/gradients/blue1.gif) 50% 0 repeat-y;") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage4 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), Caption, "", "Src", "Business/48x48/chart.png", "Width", "48", "Height", "48", "GridPanel.Width", "48") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      Title = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), Caption, "", "ID", idref("Title"), "Class", "scLargeFont", "Foreground", "white", "Width", "100%", "Style", "padding-left:4px; overflow:hidden; text-overflow:ellipsis; filter:progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color='#333333', Positive='true')", "nowrap", "nowrap") as Sitecore.Web.UI.HtmlControls.Border;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Title, "", "Width", "100%", "GridPanel.Aligh", "left", "Text", "Executive Summary") as Sitecore.Web.UI.HtmlControls.Literal;
      Toolbar = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), gridpanel3, "", "ID", idref("Toolbar"), "Style", "BORDER-BOTTOM: #ffffff 1px solid;", "GridPanel.Height", "0") as Sitecore.Web.UI.HtmlControls.Toolbar;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Toolbar, "", "Text", "Start:") as Sitecore.Web.UI.HtmlControls.Literal;
      Start = AddControl(new Sitecore.Web.UI.HtmlControls.Listbox(), Toolbar, "", "ID", idref("Start"), "Width", "100px", "Style", "Font-size:11px;", "Change", "mailinglistsummary:datechange") as Sitecore.Web.UI.HtmlControls.Listbox;
      space7 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), Toolbar, "", "Width", "10px") as Sitecore.Web.UI.HtmlControls.Space;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Toolbar, "", "Text", "End:") as Sitecore.Web.UI.HtmlControls.Literal;
      End = AddControl(new Sitecore.Web.UI.HtmlControls.Listbox(), Toolbar, "", "ID", idref("End"), "Width", "100px", "Style", "Font-size:11px;", "Change", "mailinglistsummary:datechange") as Sitecore.Web.UI.HtmlControls.Listbox;
      tooldivider9 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), Toolbar, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      btnProcessRange = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), Toolbar, "", "id", idref("btnProcessRange"), "Disabled", "true", "Click", "mailinglistsummary:processrange", "Icon", "Applications/16x16/nav_right_green.png") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider10 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), Toolbar, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      btnPrint = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), Toolbar, "", "id", idref("btnPrint"), "Disabled", "true", "Click", "mailinglistsummary:print", "Icon", "People/16x16/printer.png") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider11 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), Toolbar, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      scrollbox12 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "height", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel13 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), scrollbox12, "", "Width", "100%", "VAlign", "top", "CellSpacing", "5px") as Sitecore.Web.UI.WebControls.GridPanel;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel13, "", "FontSize", "12px", "FontName", "Tahoma", "FontBold", "true", "Foreground", "#000000", "Text", "Mails") as Sitecore.Web.UI.HtmlControls.Literal;
      scMails = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel13, "", "id", idref("scMails"), "Padding", "10", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      literal15 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel13, "", "FontSize", "12px", "FontName", "Tahoma", "FontBold", "true", "Foreground", "#000000", "Text", "Mailing Lists") as Sitecore.Web.UI.HtmlControls.Literal;
      scLists = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel13, "", "id", idref("scLists"), "Padding", "10", "Width", "100%", "runat", "server") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Caption, "ID", idref("Caption"));
      SetProperty(Title, "ID", idref("Title"));
      SetProperty(Toolbar, "ID", idref("Toolbar"));
      SetProperty(Start, "ID", idref("Start"));
      SetProperty(End, "ID", idref("End"));
      SetProperty(btnProcessRange, "id", idref("btnProcessRange"));
      SetProperty(btnPrint, "id", idref("btnPrint"));
      SetProperty(scMails, "id", idref("scMails"));
      SetProperty(scLists, "id", idref("scLists"));
    }
  }
}

