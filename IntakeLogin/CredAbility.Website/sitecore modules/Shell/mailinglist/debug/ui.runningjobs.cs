using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class RunningJobs_a_132 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.HtmlControls.ThemedImage themedimage4;
    public Sitecore.Web.UI.HtmlControls.Border Title;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Toolbar toolbar6;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton7;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider8;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton9;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider10;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton11;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider12;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton13;
    public Sitecore.Web.UI.HtmlControls.Tooldivider tooldivider14;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton15;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel16;
    public Sitecore.Web.UI.HtmlControls.Literal literal17;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scMails;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel18;
    public Sitecore.Web.UI.HtmlControls.Literal literal19;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scJobs;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "#dcdcdc;", "Scroll", "yes") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.RunningJobs,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Style", "border-bottom: #ffffff solid 1px", "Width", "100%", "Columns", "2", "CellSpacing", "2px", "Background", "#5A6B7D url(/sitecore/shell/themes/standard/gradients/blue1.gif) 50% 0 repeat-y;") as Sitecore.Web.UI.WebControls.GridPanel;
      themedimage4 = AddControl(new Sitecore.Web.UI.HtmlControls.ThemedImage(), gridpanel3, "", "Src", "Software/32x32/branch_element.png", "Width", "48", "Height", "48", "GridPanel.Width", "48") as Sitecore.Web.UI.HtmlControls.ThemedImage;
      Title = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel3, "", "ID", idref("Title"), "Class", "scLargeFont", "Foreground", "white", "Width", "100%", "Style", "padding-left:4px; overflow:hidden; text-overflow:ellipsis; filter:progid:DXImageTransform.Microsoft.dropshadow(OffX=2, OffY=2, Color='#333333', Positive='true')", "nowrap", "nowrap") as Sitecore.Web.UI.HtmlControls.Border;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), Title, "", "Width", "100%", "GridPanel.Aligh", "left", "Text", "Running Jobs") as Sitecore.Web.UI.HtmlControls.Literal;
      toolbar6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbar(), formpage1, "", "Style", "BORDER-BOTTOM: #ffffff 1px solid;") as Sitecore.Web.UI.HtmlControls.Toolbar;
      toolbutton7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar6, "", "Click", "mailinglist:sendqueued", "Icon", "Network/32x32/mail_out.png", "Header", "Send Queued Mails") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider8 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), toolbar6, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      toolbutton9 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar6, "", "Click", "mailinglist:cancelqueued", "Icon", "Network/32x32/mail_delete.png", "Header", "Cancel Queued Mails") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider10 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), toolbar6, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      toolbutton11 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar6, "", "Click", "mailinglistrunningjobs:refresh", "Icon", "Applications/32x32/refresh.png", "Header", "Refresh") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider12 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), toolbar6, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      toolbutton13 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar6, "", "Click", "mailinglist:cancelalljobs", "Icon", "Applications/32x32/delete2.png", "Header", "Cancel All Jobs") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      tooldivider14 = AddControl(new Sitecore.Web.UI.HtmlControls.Tooldivider(), toolbar6, "") as Sitecore.Web.UI.HtmlControls.Tooldivider;
      toolbutton15 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), toolbar6, "", "Click", "mailinglist:clearfinishedjobs", "Icon", "Applications/32x32/delete.png", "Header", "Clear Finished Jobs") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      gridpanel16 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "VAlign", "top", "CellSpacing", "5px") as Sitecore.Web.UI.WebControls.GridPanel;
      literal17 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel16, "", "FontSize", "12px", "FontName", "Tahoma", "FontBold", "true", "Foreground", "#000000", "Text", "Mails") as Sitecore.Web.UI.HtmlControls.Literal;
      scMails = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel16, "", "id", idref("scMails"), "Padding", "10", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      gridpanel18 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Width", "100%", "VAlign", "top", "CellSpacing", "5px") as Sitecore.Web.UI.WebControls.GridPanel;
      literal19 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel18, "", "FontSize", "12px", "FontName", "Tahoma", "FontBold", "true", "Foreground", "#000000", "Text", "Background Jobs") as Sitecore.Web.UI.HtmlControls.Literal;
      scJobs = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel18, "", "id", idref("scJobs"), "Padding", "10", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Title, "ID", idref("Title"));
      SetProperty(scMails, "id", idref("scMails"));
      SetProperty(scJobs, "id", idref("scJobs"));
    }
  }
}

