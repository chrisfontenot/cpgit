using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class MailTypeSelectDialog_a_140 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox3;
    public Sitecore.Web.UI.HtmlControls.Listview MailTypesView;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader4;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem5;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "Network/32x32/mail_write.png", "Header", "Create a new mail", "Text", "Please, select a mail type. Then click the Create button.", "OKButton", "Create") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Modules.MailingList.UI.Dialogs.MailTypeSelectDialog,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      scrollbox3 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), formdialog1, "", "Width", "100%", "Height", "100%", "Background", "white", "Border", "1px inset", "Padding", "0") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      MailTypesView = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox3, "", "ID", idref("MailTypesView"), "DblClick", "OnMailTypeDbClick") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader4 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), MailTypesView, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem5 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader4, "", "Name", "name", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(MailTypesView, "ID", idref("MailTypesView"));
    }
  }
}

