using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class ListsPage_a_124 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton5;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton6;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton8;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox9;
    public Sitecore.Web.UI.HtmlControls.Listview Lists;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader10;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem11;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem12;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "silver") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.ListsPage,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Columns", "2", "CellPadding", "5", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "GridPanel.VAlign", "top", "GridPanel.Width", "120") as Sitecore.Web.UI.WebControls.GridPanel;
      toolbutton5 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/book_blue_add.png", "Click", "mailinglist:new", "Header", "New list") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/book_blue_view.png", "Click", "mailinglist:edit", "Header", "Edit list") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/book_green.png", "Click", "mailinglist:rename", "Header", "Rename list") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/book_blue_delete.png", "Click", "mailinglist:delete", "Header", "Delete list") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      scrollbox9 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "ContextMenu", "GetContextMenu") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Lists = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox9, "", "ID", idref("Lists"), "DblClick", "mailinglist:edit") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader10 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), Lists, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem11 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader10, "", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem12 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader10, "", "Name", "description", "Header", "Description") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Lists, "ID", idref("Lists"));
    }
  }
}

