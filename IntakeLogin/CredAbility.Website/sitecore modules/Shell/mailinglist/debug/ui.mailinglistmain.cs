using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class MailingListMain_a_93 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public System.Web.UI.Control meta2;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside3;
    public Sitecore.Web.UI.HtmlControls.Border borderDbError;
    public Sitecore.Web.UI.HtmlControls.Literal literal4;
    public System.Web.UI.Control br5;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Border mainTasks;
    public Sitecore.Web.UI.XmlControls.XmlControl taskbackground7;
    public Sitecore.Web.UI.XmlControls.XmlControl Links;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel8;
    public System.Web.UI.Control mltaskoption9;
    public System.Web.UI.Control mltaskoption10;
    public System.Web.UI.Control mltaskoption11;
    public System.Web.UI.Control mltaskoption12;
    public System.Web.UI.Control mltaskoption13;
    public System.Web.UI.Control mltaskoption14;
    public System.Web.UI.Control mltaskoption15;
    public System.Web.UI.Control mltaskoption16;
    public System.Web.UI.Control mltaskoption17;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      meta2 = AddControl("meta", "", formpage1, "", "http-equiv", "Page-Exit", "CONTENT", "progid:DXImageTransform.Microsoft.Fade(Duration=0.3)");
      codebeside3 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.MailingListMain,Sitecore.MailingList.dll") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      borderDbError = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "Style", "background:#EFEFD0;color:red;font-size:medium", "Width", "100%", "ID", idref("borderDbError")) as Sitecore.Web.UI.HtmlControls.Border;
      literal4 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), borderDbError, "", "Text", "Couldn't find MailingList database.") as Sitecore.Web.UI.HtmlControls.Literal;
      br5 = AddControl("br", "", borderDbError, "");
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), borderDbError, "", "Text", "Please check database settings.") as Sitecore.Web.UI.HtmlControls.Literal;
      mainTasks = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), formpage1, "", "ID", idref("mainTasks"), "height", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.HtmlControls.Border;
      taskbackground7 = AddControl("TaskBackground", "", mainTasks, "") as Sitecore.Web.UI.XmlControls.XmlControl;
      Links = AddControl("TaskOptions", "", taskbackground7, "", "ID", idref("Links"), "Header", "Pick a task", "height", "100%", "GridPanel.Height", "100%") as Sitecore.Web.UI.XmlControls.XmlControl;
      gridpanel8 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), Links, "", "Columns", "2", "CellSpacing", "10px", "CellPadding", "5px") as Sitecore.Web.UI.WebControls.GridPanel;
      mltaskoption9 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:newmail", "Header", "Write a new mail", "Icon", "Network/48x48/mail_write.png");
      mltaskoption10 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:subscribers", "Header", "Manage subscribers", "Icon", "Business/48x48/businessmen.png");
      mltaskoption11 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:send", "Header", "Send Mail", "Icon", "Network/48x48/mail_out.png");
      mltaskoption12 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:lists", "Header", "Manage mailing lists", "Icon", "People/48x48/books.png");
      mltaskoption13 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:sendqueued", "Header", "Send Queued Mails", "Icon", "Network/48x48/mail_out.png");
      mltaskoption14 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:summary", "Header", "Executive Summary", "Icon", "Business/48x48/chart.png");
      mltaskoption15 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:outbox", "Header", "Manage Outbox", "Icon", "Network/48x48/outbox.png");
      mltaskoption16 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:sent", "Header", "Manage sent mails", "Icon", "Network/48x48/outbox_out.png");
      mltaskoption17 = AddControl("mailinglist:MLTaskOption", "", gridpanel8, "", "Click", "mailinglist:jobs", "Header", "Running Jobs", "Icon", "Software/48x48/branch_element.png");
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(borderDbError, "ID", idref("borderDbError"));
      SetProperty(mainTasks, "ID", idref("mainTasks"));
      SetProperty(Links, "ID", idref("Links"));
    }
  }
}

