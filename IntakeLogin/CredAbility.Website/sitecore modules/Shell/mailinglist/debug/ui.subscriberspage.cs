using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class SubscribersPage_a_125 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formpage1;
    public Sitecore.Web.UI.HtmlControls.Stylesheet Navigator;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton5;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton6;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton7;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton8;
    public Sitecore.Web.UI.HtmlControls.Toolbutton toolbutton9;
    public Sitecore.Web.UI.HtmlControls.Space space10;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel11;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.HtmlControls.Literal literal13;
    public Sitecore.Web.UI.HtmlControls.Edit Filter;
    public Sitecore.Web.UI.HtmlControls.Literal literal14;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox15;
    public Sitecore.Web.UI.HtmlControls.Listview Lists;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader16;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem17;
    public Sitecore.Web.UI.HtmlControls.Border border18;
    public Sitecore.Web.UI.HtmlControls.Button button19;
    public System.Web.UI.Control text20;
    public System.Web.UI.Control text21;
    public Sitecore.Web.UI.HtmlControls.Button button22;
    public System.Web.UI.Control text23;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel24;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox25;
    public Sitecore.Web.UI.HtmlControls.Listview Subscribers;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader26;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem27;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem28;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem29;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem30;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel31;
    public Sitecore.Web.UI.HtmlControls.Literal Summary;
    public Sitecore.Web.UI.HtmlControls.Scroller ListNavigatorScroller;
    public Sitecore.Web.UI.HtmlControls.Navigator ListNavigator;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formpage1 = AddControl("FormPage", "", this, "", "Background", "silver") as Sitecore.Web.UI.XmlControls.XmlControl;
      Navigator = AddControl(new Sitecore.Web.UI.HtmlControls.Stylesheet(), formpage1, "", "ID", idref("Navigator"), "Src", "/sitecore/shell/themes/navigator.css") as Sitecore.Web.UI.HtmlControls.Stylesheet;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formpage1, "", "Type", "Sitecore.Modules.MailingList.UI.SubscribersPage,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formpage1, "", "Columns", "2", "CellSpacing", "5", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "GridPanel.VAlign", "top", "GridPanel.Width", "180") as Sitecore.Web.UI.WebControls.GridPanel;
      toolbutton5 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/user1_view.png", "Click", "mailinglist:edit", "Header", "Edit the current subscriber") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton6 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/user1_add.png", "Click", "mailinglist:new", "Header", "Create a new subscriber") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton7 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "People/32x32/user1_delete.png", "Click", "mailinglist:delete", "Header", "Delete the current subscriber") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton8 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Applications/32x32/scroll_refresh.png", "Click", "mailinglist:auditlog", "Header", "View audit log") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      toolbutton9 = AddControl(new Sitecore.Web.UI.HtmlControls.Toolbutton(), gridpanel4, "", "Icon", "Applications/32x32/scroll_replace.png", "Click", "mailinglist:maillog", "Header", "View mail log") as Sitecore.Web.UI.HtmlControls.Toolbutton;
      space10 = AddControl(new Sitecore.Web.UI.HtmlControls.Space(), gridpanel4, "", "Height", "16") as Sitecore.Web.UI.HtmlControls.Space;
      gridpanel11 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel4, "", "Width", "100%", "CellSpacing", "5") as Sitecore.Web.UI.WebControls.GridPanel;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel11, "", "Text", "Search options", "Style", "font-weight:bold") as Sitecore.Web.UI.HtmlControls.Literal;
      literal13 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel11, "", "Text", "Filter:") as Sitecore.Web.UI.HtmlControls.Literal;
      Filter = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel11, "", "ID", idref("Filter"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal14 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel11, "", "Text", "Mailing lists:") as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox15 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel11, "", "GridPanel.Height", "180", "ContextMenu", "mailinglist:contextmenu(id=Lists)") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Lists = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox15, "", "ID", idref("Lists"), "ShowCheckboxes", "true") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader16 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), Lists, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem17 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader16, "", "Header", "List") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      border18 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel11, "", "GridPanel.NoWrap", "", "Align", "right") as Sitecore.Web.UI.HtmlControls.Border;
      button19 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Click", "mailinglist:clearfilter") as Sitecore.Web.UI.HtmlControls.Button;
      text20 = AddLiteral("Clear", "", button19, "");
      text21 = AddLiteral("              &nbsp;              ", "", border18, "");
      button22 = AddControl(new Sitecore.Web.UI.HtmlControls.Button(), border18, "", "Click", "mailinglist:updatefilter") as Sitecore.Web.UI.HtmlControls.Button;
      text23 = AddLiteral("Search", "", button22, "");
      gridpanel24 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Width", "100%", "Height", "100%") as Sitecore.Web.UI.WebControls.GridPanel;
      scrollbox25 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel24, "", "ContextMenu", "mailinglist:contextmenu(id=Subscribers)") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Subscribers = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox25, "", "ID", idref("Subscribers"), "DblClick", "mailinglist:edit") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader26 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), Subscribers, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem27 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader26, "", "Header", "E-mail") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem28 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader26, "", "Name", "name", "Header", "Full Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem29 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader26, "", "Name", "company", "Header", "Company") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem30 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader26, "", "Name", "country", "Header", "Country") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      gridpanel31 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel24, "", "Columns", "2", "CellSpacing", "2", "GridPanel.Height", "1") as Sitecore.Web.UI.WebControls.GridPanel;
      Summary = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel31, "", "ID", idref("Summary"), "Text", "Ready", "GridPanel.NoWrap", "") as Sitecore.Web.UI.HtmlControls.Literal;
      ListNavigatorScroller = AddControl(new Sitecore.Web.UI.HtmlControls.Scroller(), gridpanel31, "", "ID", idref("ListNavigatorScroller"), "Style", "width:400", "GridPanel.Align", "center", "GridPanel.Width", "100%", "Border", "None") as Sitecore.Web.UI.HtmlControls.Scroller;
      ListNavigator = AddControl(new Sitecore.Web.UI.HtmlControls.Navigator(), ListNavigatorScroller, "", "ID", idref("ListNavigator"), "Count", "0", "PageSize", "100", "Offset", "0", "Click", "mailinglist:refresh(id=Subscribers)") as Sitecore.Web.UI.HtmlControls.Navigator;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Navigator, "ID", idref("Navigator"));
      SetProperty(Filter, "ID", idref("Filter"));
      SetProperty(Lists, "ID", idref("Lists"));
      SetProperty(Subscribers, "ID", idref("Subscribers"));
      SetProperty(Summary, "ID", idref("Summary"));
      SetProperty(ListNavigatorScroller, "ID", idref("ListNavigatorScroller"));
      SetProperty(ListNavigator, "ID", idref("ListNavigator"));
    }
  }
}

