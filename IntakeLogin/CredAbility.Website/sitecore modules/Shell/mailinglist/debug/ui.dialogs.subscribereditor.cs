using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using Sitecore.Globalization;
using Sitecore.Sites;
using Sitecore.Web.UI;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.WebControls;
using Sitecore.Xml;

namespace Sitecore.Modules.MailingList.XmlControls {
  
  // control class
  public class SubscriberEditor_a_133 : Sitecore.Web.UI.XmlControls.XmlControl   {
    
    // variables
    public Sitecore.Web.UI.XmlControls.XmlControl formdialog1;
    public Sitecore.Web.UI.HtmlControls.CodeBeside codebeside2;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel3;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel4;
    public Sitecore.Web.UI.HtmlControls.Literal literal5;
    public Sitecore.Web.UI.HtmlControls.Edit Name;
    public Sitecore.Web.UI.HtmlControls.Literal literal6;
    public Sitecore.Web.UI.HtmlControls.Edit Email;
    public Sitecore.Web.UI.HtmlControls.Literal literal7;
    public Sitecore.Web.UI.HtmlControls.Edit Company;
    public Sitecore.Web.UI.HtmlControls.Literal literal8;
    public Sitecore.Web.UI.HtmlControls.Edit Country;
    public Sitecore.Web.UI.HtmlControls.Literal literal9;
    public Sitecore.Web.UI.HtmlControls.Checkbox Active;
    public Sitecore.Web.UI.HtmlControls.Literal literal10;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox11;
    public Sitecore.Web.UI.HtmlControls.Listview Lists;
    public Sitecore.Web.UI.HtmlControls.Literal literal12;
    public Sitecore.Web.UI.WebControls.GridPanel gridpanel13;
    public Sitecore.Web.UI.HtmlControls.Border border14;
    public Sitecore.Web.UI.HtmlControls.Edit Key;
    public Sitecore.Web.UI.HtmlControls.Edit Value;
    public System.Web.UI.Control themedimagebutton15;
    public System.Web.UI.Control themedimagebutton16;
    public Sitecore.Web.UI.HtmlControls.Scrollbox scrollbox17;
    public Sitecore.Web.UI.HtmlControls.Listview CustomFields;
    public Sitecore.Web.UI.HtmlControls.ListviewHeader listviewheader18;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem19;
    public Sitecore.Web.UI.HtmlControls.ListviewHeaderItem listviewheaderitem20;
    
    
    // initializer
    public override void Initialize() {
      _NamespacePrefix = "";
      
      formdialog1 = AddControl("FormDialog", "", this, "", "Icon", "People/16x16/user1_preferences.png", "Header", "Subscriber information", "Text", "Update subscriber data as required and click Save button.", "OKButton", "Save") as Sitecore.Web.UI.XmlControls.XmlControl;
      codebeside2 = AddControl(new Sitecore.Web.UI.HtmlControls.CodeBeside(), formdialog1, "", "Type", "Sitecore.Modules.MailingList.UI.Dialogs.SubscriberEditor,Sitecore.MailingList") as Sitecore.Web.UI.HtmlControls.CodeBeside;
      gridpanel3 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), formdialog1, "", "Width", "100%", "CellSpacing", "5") as Sitecore.Web.UI.WebControls.GridPanel;
      gridpanel4 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "2", "Width", "100%", "CellSpacing", "5") as Sitecore.Web.UI.WebControls.GridPanel;
      literal5 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Name:", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Name = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel4, "", "ID", idref("Name"), "Width", "100%", "GridPanel.Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal6 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "E-mail:", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Email = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel4, "", "ID", idref("Email"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal7 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Company:", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Company = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel4, "", "ID", idref("Company"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal8 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Country:", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Country = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), gridpanel4, "", "ID", idref("Country"), "Width", "100%") as Sitecore.Web.UI.HtmlControls.Edit;
      literal9 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel4, "", "Text", "Active:", "GridPanel.Align", "right") as Sitecore.Web.UI.HtmlControls.Literal;
      Active = AddControl(new Sitecore.Web.UI.HtmlControls.Checkbox(), gridpanel4, "", "ID", idref("Active"), "Checked", "true") as Sitecore.Web.UI.HtmlControls.Checkbox;
      literal10 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "Text", "Subscriptions:", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Literal;
      scrollbox11 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "180") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      Lists = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox11, "", "ID", idref("Lists"), "ShowCheckboxes", "true") as Sitecore.Web.UI.HtmlControls.Listview;
      literal12 = AddControl(new Sitecore.Web.UI.HtmlControls.Literal(), gridpanel3, "", "Text", "Custom fields:", "Width", "100%") as Sitecore.Web.UI.HtmlControls.Literal;
      gridpanel13 = AddControl(new Sitecore.Web.UI.WebControls.GridPanel(), gridpanel3, "", "Columns", "3", "CellSpacing", "2") as Sitecore.Web.UI.WebControls.GridPanel;
      border14 = AddControl(new Sitecore.Web.UI.HtmlControls.Border(), gridpanel13, "", "GridPanel.Width", "100%", "GridPanel.NoWrap", "") as Sitecore.Web.UI.HtmlControls.Border;
      Key = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border14, "", "ID", idref("Key"), "Width", "50%") as Sitecore.Web.UI.HtmlControls.Edit;
      Value = AddControl(new Sitecore.Web.UI.HtmlControls.Edit(), border14, "", "ID", idref("Value"), "Width", "50%") as Sitecore.Web.UI.HtmlControls.Edit;
      themedimagebutton15 = AddControl("mailinglist:ThemedImageButton", "", gridpanel13, "", "Click", "customfield:add", "Src", "Applications/16x16/add2.png");
      themedimagebutton16 = AddControl("mailinglist:ThemedImageButton", "", gridpanel13, "", "Click", "customfield:remove", "Src", "Applications/16x16/delete2.png");
      scrollbox17 = AddControl(new Sitecore.Web.UI.HtmlControls.Scrollbox(), gridpanel3, "", "Width", "100%", "Height", "100%", "GridPanel.Height", "160") as Sitecore.Web.UI.HtmlControls.Scrollbox;
      CustomFields = AddControl(new Sitecore.Web.UI.HtmlControls.Listview(), scrollbox17, "", "ID", idref("CustomFields"), "View", "Details") as Sitecore.Web.UI.HtmlControls.Listview;
      listviewheader18 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeader(), CustomFields, "") as Sitecore.Web.UI.HtmlControls.ListviewHeader;
      listviewheaderitem19 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader18, "", "Header", "Name") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      listviewheaderitem20 = AddControl(new Sitecore.Web.UI.HtmlControls.ListviewHeaderItem(), listviewheader18, "", "Name", "value", "Header", "Value") as Sitecore.Web.UI.HtmlControls.ListviewHeaderItem;
      
      _Mode = "";
    }
    
    // update id references
    public override void UpdateIDRefs() {
      SetProperty(Name, "ID", idref("Name"));
      SetProperty(Email, "ID", idref("Email"));
      SetProperty(Company, "ID", idref("Company"));
      SetProperty(Country, "ID", idref("Country"));
      SetProperty(Active, "ID", idref("Active"));
      SetProperty(Lists, "ID", idref("Lists"));
      SetProperty(Key, "ID", idref("Key"));
      SetProperty(Value, "ID", idref("Value"));
      SetProperty(CustomFields, "ID", idref("CustomFields"));
    }
  }
}

