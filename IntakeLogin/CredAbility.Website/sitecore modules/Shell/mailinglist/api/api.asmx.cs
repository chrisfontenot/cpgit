using System.ComponentModel;
using System.Web.Services;
using Sitecore.Web.Services;
using Sitecore.Modules.MailingList.Core;

namespace Sitecore.Modules.MailingList.API
{
   using MailingList = Sitecore.Modules.MailingList.Core.MailingList;

   /// <summary>
   /// Summary description for api.
   /// </summary>
   [WebService(Name="Sitecore Mailing List API",Namespace="http://sitecore.net/modules/mailinglist/api/")]
   [ToolboxItem(false)]
   public class api : System.Web.Services.WebService
   {
      //-------------------------------------------------------------------------
      /// <summary>Constructor for api class</summary>
      //-------------------------------------------------------------------------
      public api()
      {
         InitializeComponent();
         Sitecore.Context.SetActiveSite("shell");
      }

      //-------------------------------------------------------------------------
      /// <summary>Sends a test mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Send a test mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string SendTestMail(string mailID, string Email, string fromEmail, string fromName) 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.SendTestMail(mailID, Email, fromEmail, fromName);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Sends a mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Send a mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string SendMail(string mailID, string mailingLists, string recipients, string fromEmail, string fromName, bool individualMails) 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.SendMail(mailID, mailingLists, recipients, fromEmail, fromName, individualMails);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Queue mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Queue a mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string QueueMail(string mailItemID, string mailingLists, string recipients, string fromEmail, string fromName, bool individualMails) 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.QueueMail(mailItemID, mailingLists, recipients, fromEmail, fromName, individualMails);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Sends a queued mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Sends a queued mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string SendQueuedMail(string mailID) 
      {
         SendHelper sh = new SendHelper();
         return sh.Send(mailID);
         //using (MailingList ml = new MailingList()) 
         //{
         //   return ml.SendQueuedMail(mailID);
         //}
      }

      //-------------------------------------------------------------------------
      /// <summary>Sends queued mails.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Sends queued mails", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string SendQueuedMails() 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.SendQueuedMails();
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Fails queued mails.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Sets action on queued mails", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void SetQueuedMails(string mails, string action) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.SetQueuedMails(mails, action);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Cancel queued mails.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Cancel queued mails", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void FailAllQueuedMails() 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.FailAllQueuedMails();
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Publish text or html mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Publish text/html mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string PublishMailItem(string mailID) 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.PublishMailItem(mailID);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Get the url of a URL-type mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Get message url", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public string GetMessageUrl(string mailID) 
      {
         using (MailingList ml = new MailingList()) 
         {
            return ml.GetMessageUrl(mailID);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Deletes a subscriber.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Delete a subscriber", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void DeleteSubscriber(string subscriberEmail) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.DeleteSubscriber(subscriberEmail);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Updates a mailing list.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Updates a mailing list", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void PutMailingList(string mailingList, string name) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.PutMailingList(mailingList, name);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Deletes a mailing list.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Delete a mailing list", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void DeleteMailingList(string mailingList) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.DeleteMailingList(mailingList);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Deletes a mail.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Delete a mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void DeleteMail(string mailID) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.DeleteMail(mailID);
         }
      }

      //-------------------------------------------------------------------------
      /// <summary>Drops a mail from database by removing any entries regarding it.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Drop a mail", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      public void DropMail(string mailID) 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.DropMail(mailID);
         }
      }


      //-------------------------------------------------------------------------
      /// <summary>Synchronizes mailing list in Sitecore and the database.</summary>
      //-------------------------------------------------------------------------
      [WebMethod(Description="Synchronizes mailing list in Sitecore and the database", EnableSession=true),
      ApiExtensionAttribute(RequireLogin=true)]
      [System.Obsolete]
      public void SynchronizeMailingLists() 
      {
         using (MailingList ml = new MailingList()) 
         {
            ml.SynchronizeMailingLists();
         }
      }

      #region Component Designer generated code
    
      //Required by the Web Services Designer 
      private IContainer components = null;
        
      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
      }

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      protected override void Dispose( bool disposing )
      {
         if(disposing && components != null)
         {
            components.Dispose();
         }
         base.Dispose(disposing);    
      }
    
      #endregion

   }
}
