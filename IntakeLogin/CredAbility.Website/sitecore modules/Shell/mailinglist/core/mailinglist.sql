if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ListLog_Lists]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ListLog] DROP CONSTRAINT FK_ListLog_Lists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Subscriptions_Lists]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Subscriptions] DROP CONSTRAINT FK_Subscriptions_Lists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_UserAudit_Lists]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[UserAudit] DROP CONSTRAINT FK_UserAudit_Lists
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ListLog_MailLog]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ListLog] DROP CONSTRAINT FK_ListLog_MailLog
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailResults_MailLog]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MailResults] DROP CONSTRAINT FK_MailResults_MailLog
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailStatistics_MailLog]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MailStatistics] DROP CONSTRAINT FK_MailStatistics_MailLog
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MailStatistics_Subscribers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MailStatistics] DROP CONSTRAINT FK_MailStatistics_Subscribers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_SubscriberFields_Subscribers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[SubscriberFields] DROP CONSTRAINT FK_SubscriberFields_Subscribers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Subscriptions_Subscribers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Subscriptions] DROP CONSTRAINT FK_Subscriptions_Subscribers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_UserAudit_Subscribers]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[UserAudit] DROP CONSTRAINT FK_UserAudit_Subscribers
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_UpdateRawHeaders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_UpdateRawHeaders]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_UpdateRecipients]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_UpdateRecipients]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_UpdateWarnings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_UpdateWarnings]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ListReadersDistinct]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ListReadersDistinct]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ActionMap]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ActionMap]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ListLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ListLog]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Lists]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Lists]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MailLog]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailResults]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MailResults]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MailStatistics]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MailStatistics]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SubscriberFields]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SubscriberFields]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Subscribers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Subscribers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Subscriptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Subscriptions]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserAudit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[UserAudit]
GO

CREATE TABLE [dbo].[ActionMap] (
        [ID] [uniqueidentifier] NULL ,
        [Action] [int] NOT NULL ,
        [Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ListLog] (
        [ID] [uniqueidentifier] NOT NULL ,
        [Created] [smalldatetime] NULL ,
        [fk_Mail] [uniqueidentifier] NOT NULL ,
        [fk_List] [uniqueidentifier] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Lists] (
        [ID] [uniqueidentifier] NOT NULL ,
        [Created] [smalldatetime] NULL ,
        [Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Description] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Footer] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Disabled] [int] NULL,
        [Deleted] [int] NULL DEFAULT (0)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[MailLog] (
        [ID] [uniqueidentifier] NOT NULL ,
        [Created] [smalldatetime] NULL ,
        [Subject] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [From] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [FromName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Message] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [AltBody] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Format] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [SentTo] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Lists] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [SendTime] [smalldatetime] NULL ,
        [SendSchedule] [int] NULL ,
        [LastSent] [smalldatetime] NULL ,
        [CustomFunction] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Deleted] [int] NULL ,
        [IndividualMails] [int] NULL ,
        [MailItemID] [uniqueidentifier] NULL,
        [LastAction] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[MailResults] (
        [ID] [uniqueidentifier] NOT NULL ,
        [fk_Mail] [uniqueidentifier] NOT NULL ,
        [RawHeaders] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Warnings] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Recipients] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[MailStatistics] (
        [ID] [uniqueidentifier] NOT NULL ,
        [fk_Mail] [uniqueidentifier] NOT NULL ,
        [fk_Subscriber] [uniqueidentifier] NOT NULL ,
        [Action] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
        [AuditDate] [smalldatetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SubscriberFields] (
        [ID] [uniqueidentifier] NOT NULL ,
        [fk_Subscriber] [uniqueidentifier] NOT NULL ,
        [FieldName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
        [FieldValue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Subscribers] (
        [ID] [uniqueidentifier] NOT NULL ,
        [Created] [smalldatetime] NULL ,
        [Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
        [Company] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
        [Country] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
        [Disabled] [int] NULL ,
        [Deleted] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Subscriptions] (
        [ID] [uniqueidentifier] NOT NULL ,
        [Created] [smalldatetime] NULL ,
        [fk_List] [uniqueidentifier] NOT NULL ,
        [fk_Subscriber] [uniqueidentifier] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[UserAudit] (
        [ID] [uniqueidentifier] NOT NULL ,
        [fk_Subscriber] [uniqueidentifier] NOT NULL ,
        [fk_Lists] [uniqueidentifier] NULL ,
        [AuditDate] [smalldatetime] NOT NULL ,
        [Action] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ListLog] WITH NOCHECK ADD 
        CONSTRAINT [PK_ListLog] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Lists] WITH NOCHECK ADD 
        CONSTRAINT [PK_Lists] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MailLog] WITH NOCHECK ADD 
        CONSTRAINT [PK_MailLog] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MailResults] WITH NOCHECK ADD 
        CONSTRAINT [PK_MailResults] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MailStatistics] WITH NOCHECK ADD 
        CONSTRAINT [PK_MailStatistics] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[SubscriberFields] WITH NOCHECK ADD 
        CONSTRAINT [PK_SubscriberFields] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Subscribers] WITH NOCHECK ADD 
        CONSTRAINT [PK_Subscribers] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Subscriptions] WITH NOCHECK ADD 
        CONSTRAINT [PK_Subscriptions] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[UserAudit] WITH NOCHECK ADD 
        CONSTRAINT [PK_UserAudit] PRIMARY KEY  CLUSTERED 
        (
                [ID]
        )  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ListLog] ADD 
        CONSTRAINT [DF_ListLog_ID] DEFAULT (newid()) FOR [ID],
        CONSTRAINT [DF_ListLog_Created] DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [dbo].[Lists] ADD 
        CONSTRAINT [DF_Lists_Created] DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [dbo].[MailLog] ADD 
        CONSTRAINT [DF_MailLog_Created] DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [dbo].[MailStatistics] ADD 
        CONSTRAINT [DF_MailStatistics_ID] DEFAULT (newid()) FOR [ID],
        CONSTRAINT [DF_MailStatistics_AuditDate] DEFAULT (getdate()) FOR [AuditDate]
GO

ALTER TABLE [dbo].[Subscribers] ADD 
        CONSTRAINT [DF_Subscribers_Created] DEFAULT (getdate()) FOR [Created],
        CONSTRAINT [DF_Subscribers_Deleted] DEFAULT (0) FOR [Deleted]
GO

 CREATE  UNIQUE  INDEX [eMail] ON [dbo].[Subscribers]([Email]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Subscriptions] ADD 
        CONSTRAINT [DF_Subscriptions_ID] DEFAULT (newid()) FOR [ID],
        CONSTRAINT [DF_Subscriptions_Created] DEFAULT (getdate()) FOR [Created]
GO

ALTER TABLE [dbo].[UserAudit] ADD 
        CONSTRAINT [DF_UserAudit_ID] DEFAULT (newid()) FOR [ID],
        CONSTRAINT [DF_UserAudit_AuditDate] DEFAULT (getdate()) FOR [AuditDate]
GO

ALTER TABLE [dbo].[ListLog] ADD 
        CONSTRAINT [FK_ListLog_Lists] FOREIGN KEY 
        (
                [fk_List]
        ) REFERENCES [dbo].[Lists] (
                [ID]
        ),
        CONSTRAINT [FK_ListLog_MailLog] FOREIGN KEY 
        (
                [fk_Mail]
        ) REFERENCES [dbo].[MailLog] (
                [ID]
        )
GO

ALTER TABLE [dbo].[MailResults] ADD 
        CONSTRAINT [FK_MailResults_MailLog] FOREIGN KEY 
        (
                [fk_Mail]
        ) REFERENCES [dbo].[MailLog] (
                [ID]
        )
GO

ALTER TABLE [dbo].[MailStatistics] ADD 
        CONSTRAINT [FK_MailStatistics_MailLog] FOREIGN KEY 
        (
                [fk_Mail]
        ) REFERENCES [dbo].[MailLog] (
                [ID]
        ),
        CONSTRAINT [FK_MailStatistics_Subscribers] FOREIGN KEY 
        (
                [fk_Subscriber]
        ) REFERENCES [dbo].[Subscribers] (
                [ID]
        )
GO

ALTER TABLE [dbo].[SubscriberFields] ADD 
        CONSTRAINT [FK_SubscriberFields_Subscribers] FOREIGN KEY 
        (
                [fk_Subscriber]
        ) REFERENCES [dbo].[Subscribers] (
                [ID]
        )
GO

ALTER TABLE [dbo].[Subscriptions] ADD 
        CONSTRAINT [FK_Subscriptions_Lists] FOREIGN KEY 
        (
                [fk_List]
        ) REFERENCES [dbo].[Lists] (
                [ID]
        ),
        CONSTRAINT [FK_Subscriptions_Subscribers] FOREIGN KEY 
        (
                [fk_Subscriber]
        ) REFERENCES [dbo].[Subscribers] (
                [ID]
        )
GO

ALTER TABLE [dbo].[UserAudit] ADD 
        CONSTRAINT [FK_UserAudit_Lists] FOREIGN KEY 
        (
                [fk_Lists]
        ) REFERENCES [dbo].[Lists] (
                [ID]
        ),
        CONSTRAINT [FK_UserAudit_Subscribers] FOREIGN KEY 
        (
                [fk_Subscriber]
        ) REFERENCES [dbo].[Subscribers] (
                [ID]
        )
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW ListReadersDistinct AS
SELECT DISTINCT fk_Mail, fk_Subscriber
FROM MailStatistics
WHERE Action='open'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.sp_UpdateRawHeaders
        (
                @id  nvarchar(38),
                @appendtext ntext
        )
AS
  
DECLARE @ptrval binary(16)
SELECT @ptrval = TEXTPTR(rawheaders) 
   FROM mailresults mr
      WHERE fk_mail = @id
UPDATETEXT mailresults.rawheaders @ptrval null 0 @appendtext
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.sp_UpdateRecipients
        (
                @id  nvarchar(38),
                @appendtext ntext
        )
AS
  
DECLARE @ptrval binary(16)
SELECT @ptrval = TEXTPTR(recipients) 
   FROM mailresults mr
      WHERE fk_mail = @id
UPDATETEXT mailresults.recipients @ptrval null 0 @appendtext
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.sp_UpdateWarnings
        (
                @id  nvarchar(38),
                @appendtext ntext
        )
AS
  
DECLARE @ptrval binary(16)
SELECT @ptrval = TEXTPTR(warnings) 
   FROM mailresults mr
      WHERE fk_mail = @id
UPDATETEXT mailresults.warnings @ptrval null 0 @appendtext
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

