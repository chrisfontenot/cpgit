﻿<%@ Import Namespace="System.Linq"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentsOverview.ascx.cs" Inherits="IndexViewer.sitecore_modules.Shell.IndexViewer.DocumentsOverview" %>
<div class="DocumentsOverview">
  <div class="DocumentSelectorByNumber">
    <div class="Title">
      Browse by document number:
      
    </div>
    <div class="NumberSelector">
      
      Doc. #. 0 
      <asp:Button CssClass="IterateButton" ID="SelectPreviousDocNumberButton" runat="server" 
        onclick="SelectPreviousDocNumberButton_Click" Text="&lt;" />
      <asp:TextBox ID="SelectedNumberTextBox" runat="server" >0</asp:TextBox>
      <asp:Button CssClass="IterateButton" ID="SelectNextDocNumberButton" runat="server" 
        CausesValidation="False" onclick="SelectNextDocNumberButton_Click" 
        Text="&gt;" />
      <asp:Label ID="DocSelectorLastIndexLabel" runat="server" />
      <asp:Button ID="GoButton" CssClass="GoButton" runat="server" onclick="GoButton_Click" Text="Go" />
    </div>
    
    <div class="DocumentContent">
      <div class="Title">
        Doc #: <asp:Label ID="SelectedDocumentNumber" runat="server" />
      </div>
      <div class="DocumentFields">
        <table class="DocumentFieldTable" cellpadding="0px" cellspacing="0px">
          <asp:Repeater ID="FieldRepeater" runat="server">
            <HeaderTemplate>
              <thead>
                <tr>
                  <th class="Index">No</th>
                  <th class="FieldName">Field Name</th>
                  <th>Field Value</th>
                </tr>
              </thead>
            </HeaderTemplate>
            <ItemTemplate>
              <tr>
                <td class="Index"><%# Container.ItemIndex %></td>
                <td class="FieldName"><%# (Container.DataItem as Lucene.Net.Documents.Field).Name()%></td>
                <td class="FieldValue"><%# (Container.DataItem as Lucene.Net.Documents.Field).StringValue()%></td>
              </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
      </div>
    </div>
  </div>
</div>