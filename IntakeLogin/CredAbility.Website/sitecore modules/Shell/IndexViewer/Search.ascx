﻿<%@ Import Namespace="IndexViewer"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Lucene.Net.Documents"%>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="IndexViewer.Search" %>
<div class="SearchContent">
  <div class="TopContent">
    
    <div class="SearchFieldSection">
      <div class="TopSection">
        <div class="SearchTermText">
          Enter search expression here:
        </div>
        <div class="SearchTextField">
          Fieldname: <asp:TextBox ID="FieldNameTextBox" runat="server"></asp:TextBox>
          Search word: <asp:TextBox ID="SearchWordTextBox" runat="server"></asp:TextBox>
          Query to use: 
          <asp:DropDownList ID="QuerySelector" runat="server">
            <asp:ListItem Selected="True" Text="QueryParser" Value="QueryParser"></asp:ListItem>
            <asp:ListItem Text="TermQuery" Value="TermQuery"></asp:ListItem>
            <asp:ListItem Text="PrefixQuery" Value="PrefixQuery"></asp:ListItem>
            <asp:ListItem Text="WildCardQuery" Value="WildCardQuery"></asp:ListItem>
          </asp:DropDownList>
        </div>
        <div class="SearchButtons">
          <asp:Button ID="SearchFieldButton" runat="server" Text="Search" 
            onclick="SearchFieldButton_Click" />
        </div>
      </div>
    </div>
  </div>
  <div class="BottomContent">
    <div class="SearchResultInfo">
      <div class="SearchResultInfoEntity">
        Time elapsed for search: <asp:Label ID="TimeElapsedLabel" CssClass="SearchResultInfoValue" runat="server"></asp:Label>
      </div>
      <div class="SearchResultInfoEntity">
        Total hits: <asp:Label ID="TotalHitsLabel" CssClass="SearchResultInfoValue" runat="server"></asp:Label>
      </div>
    </div>
    <div class="SearchResults">
      <asp:Panel ID="SearchResultPanel" CssClass="SearchResultPanel" runat="server" ScrollBars="Horizontal">
        <asp:GridView ID="SearchResultGrid" runat="server" 
  CssClass="SearchResultTable" AllowPaging="True" 
          onpageindexchanging="SearchResultGrid_PageIndexChanging" PageSize="15">
          
          <PagerStyle CssClass="PagerLinks" />
          
        </asp:GridView>
      </asp:Panel>
    </div>
  </div>
</div>
