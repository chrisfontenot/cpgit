﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainForm.aspx.cs" Inherits="IndexViewer.MainForm" %>
<%@ Register src="IndexOverview.ascx" tagname="IndexOverview" tagprefix="IndexViewer" %>
<%@ Register src="DocumentsOverview.ascx" tagname="DocumentsOverview" tagprefix="IndexViewer" %>
<%@ Register src="Search.ascx" tagname="Search" tagprefix="IndexViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    
</head>
<body>
    <form id="form1" runat="server">
    <link rel="Stylesheet" type="text/css" href="/sitecore modules/Shell/IndexViewer/css/IndexViewerDefault.css" />
    <div class="IndexViewer">
      <div class="Header">
        <div class="HeaderIndexSelector">
          <div class="SelectorEntity">
            <div class="SelectLabel">Select Database:</div>
            <asp:DropDownList CssClass="Selector" ID="DatabaseSelector" runat="server" AutoPostBack="True" 
              onselectedindexchanged="DatabaseSelector_SelectedIndexChanged">
            </asp:DropDownList>
          </div>
          <div class="SelectorEntity">
            <div class="SelectLabel">Select Index:</div>
            <asp:dropdownlist CssClass="Selector" ID="IndexSelector" runat="server" AutoPostBack="True" 
              onselectedindexchanged="IndexSelector_SelectedIndexChanged"></asp:dropdownlist>      
          </div>
        </div>
        <div class="clearfloat"></div>
        <div class="HeaderViewSelector">
          <asp:Button id="OverviewButton" CssClass="ButtonSelector" runat="server" 
            Text="Overview" onclick="OverviewButton_Click">
          </asp:Button>
          <asp:Button id="DocumentsButton" CssClass="ButtonSelector" runat="server" 
            Text="Documents" onclick="DocumentsButton_Click">
          </asp:Button>
          <asp:Button id="SearchButton" CssClass="ButtonSelector" runat="server" 
            Text="Search" onclick="SearchButton_Click">
          </asp:Button>
        </div>
      </div>
      <IndexViewer:IndexOverview ID="IndexOverview" runat="server" Visible="false" /> 
      <IndexViewer:DocumentsOverview ID="DocumentsOverviewControl" runat="server" Visible="false" />
      <IndexViewer:Search ID="SearchControl" runat="server" Visible="false" />
      <div class="content">
        <asp:Panel ID="WelcomeMessagePanel" runat="server">
          <div class="WelcomeMessage">
            <div class="Information">
              <p>
                Welcome to the IndexViewer
              </p>
              <p>
              Please select a database and an Index
              </p>              
            </div>
            <div class="ContactInformation">
              If you have any problems or feature request contact me at:<br /><br />
              Jens Mikkelsen<br />
              Pentia A/S<br />
              <a href="mailto:jm@pentia.dk">jm@pentia.dk</a><br /><br />
            </div>
          </div>
        </asp:Panel>
        <asp:PlaceHolder ID="ContentPlaceholder" runat="server" />
      </div>
    </div>
    </form>
</body>
</html>
