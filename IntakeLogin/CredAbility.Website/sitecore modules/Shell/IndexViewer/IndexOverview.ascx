﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IndexOverview.ascx.cs" Inherits="IndexViewer.IndexOverview" %>
<div class="IndexOverview">
  <div class="SimpleInformation">
    <div class="OverviewEntity">
      <div class="OverviewLabel">
        Index name:
      </div>
      <div class="OverviewValue">
        <asp:Label ID="IndexNameValue" runat="server" />
      </div>
    </div>
    <div class="OverviewEntity">
      <div class="OverviewLabel">
        Index directory:
      </div>
      <div class="OverviewValue">
        <asp:Label ID="IndexDirectoryValue" runat="server" />
      </div>
    </div>
    <div class="OverviewEntity">
      <div class="OverviewLabel">
        Number of documents:
      </div>
      <div class="OverviewValue">
        <asp:Label ID="NumberOfDocumentsValue" runat="server" />
      </div>
    </div>
    <div class="OverviewEntity">
      <div class="OverviewLabel">
        Last modified:
      </div>
      <div class="OverviewValue">
        <asp:Label ID="LastModifiedValue" runat="server" />
      </div>
    </div>
  </div>
  <div class="Fields">
    <div class="FieldTitle">Available Fields:</div>
    <div class="FieldValues">
      <ul ID="FieldList" class="FieldList" runat="server">
        <asp:Repeater ID="FieldsRepeater" runat="server">
          <ItemTemplate>
            <li><%# Container.DataItem %></li>
          </ItemTemplate>
        </asp:Repeater>
      </ul>
    </div>
  </div>
</div>
