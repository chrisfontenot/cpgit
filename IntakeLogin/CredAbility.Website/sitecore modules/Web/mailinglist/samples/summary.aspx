<%@ Page language="c#" Codebehind="summary.aspx.cs" AutoEventWireup="false" Inherits="Sitecore.Modules.MailingList.SummarySample.Summary" %>
<%@ register TagPrefix="sc" Namespace="sitecore.client" Assembly="sitecore" %>
<%@ register TagPrefix="ml" Namespace="sitecore.client" Assembly="mailinglist" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>SiteCore Mailing List Summary</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="/sitecore/client/themes/standard/client.css" rel="stylesheet">
    <link href="/sitecore modules/mailinglist/themes/standard/client.css" rel="stylesheet">
    <script language="javascript" src="/sitecore/client/script/utils.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/script/ui.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/script/msgs.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/script/server.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/script/packet.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/script/systemkeys.js" type="text/JavaScript"></script>
    <script language="vbscript" src="/sitecore/client/script/utils.vbs" type="text/vbscript"></script>
    <script language="javascript" src="/sitecore/client/controls/menu/menu.js" type="text/JavaScript"></script>
    <script language="javascript" src="/sitecore/client/controls/toolbar/toolbar.js" type="text/JavaScript"></script>
    <script language="javascript" type="text/JavaScript">
    
    //---------------------------------------
    // Summary behavior
    //---------------------------------------
    function scSummary() {
    }
    
    scSummary.prototype.MessageMap = new Array(
      "summary:print", "OnPrint"
    );
    
    scSummary.prototype.OnPrint = function(msg) {
      window.open("/sitecore modules/mailinglist/summaryprint.aspx?<asp:placeholder id="idPrintDates" runat="server"/>", "_blank");
      msg.clear();
    }

    </script>
  </head>
  <body class="sc_bg_main sc_body sc_font" leftmargin="0" topmargin="0" scroll="no" sc_class="scSummary">
    <form id="summary" method="post" runat="server">
      <ml:tooltaskpanel id="Tooltaskpanel1" runat="server" caption="SiteCore Mailing List - Executive Summary" icon="/sitecore modules/mailinglist/themes/standard/mailinglist32x32.gif" fillbottom="false">
        <table class="sc_font" cellspacing="0" cellpadding="4" width="100%" height="100%" border="0">
          <tr>
            <td style="padding:0px">
            <sc:toolbar id="Toolbar2" runat="server">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start:
                <select class="sc_font" id="idStart" name="idStart" runat="server">
                </select>&nbsp;&nbsp;&nbsp;End:
                <select class="sc_font" id="idEnd" name="idEnd" runat="server">
                </select>&nbsp;&nbsp;&nbsp;<button id="btnSubmit" hidefocus style="BORDER-RIGHT: 0px; BORDER-TOP: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; BORDER-LEFT: 0px; BORDER-BOTTOM: 0px" name="btnSubmit" type="submit" runat="server"><img height="16" src="/sitecore/client/themes/standard/go.gif" width="16" align="absMiddle" border="0" title="Search"></button>
                <sc:toolbarseparator id="Toolbarbutton2" runat="server"></sc:toolbarseparator>
                <sc:toolbarbutton id="Toolbarbutton3" runat="server" alt="Print" icon="/sitecore modules/mailinglist/themes/standard/print20x20.gif" text="Print" command="summary:print"></sc:toolbarbutton>
                <sc:toolbarbutton id="Toolbarbutton1" runat="server" alt="Go to Mailing list tasks" icon="/sitecore/client/themes/standard/tasks20x20.gif" text="Tasks" command="item:load" parameters="id=/sitecore/content/modules/mailing list"></sc:toolbarbutton>
              </sc:toolbar>
            </td>
          </tr>
          <tr>
            <td style="padding-top:0px; padding-left:12px" height="100%">
              <div style="width:100%; height:100%; overflow:auto">
                <table class="sc_font" cellspacing="0" cellpadding="4" width="100%" border="0">
                  <tr>
                    <td><b>Leads</b></td>
                  </tr>
                  <td>News leads: <span id="idLeads" runat="server"></span></td>
                  <tr>
                    <td><b>Mails</b></td>
                  </tr>
                  <tr>
                    <td>
                      <table id="idMails" cellspacing="0" cellpadding="4" width="100%" border="0" runat="server" class="sc_font" style="border:1px inset; background:window;">
                        <tr>
                          <td style="background:threedface; border:1px outset;">Date</td>
                          <td style="background:threedface; border:1px outset;">Subject</td>
                          <td style="background:threedface; border:1px outset;">Recipients</td>
                          <td style="background:threedface; border:1px outset;">Opened</td>
                          <td style="background:threedface; border:1px outset;">Clicked</td>
                          <td style="background:threedface; border:1px outset;">Opened count</td>
                          <td style="background:threedface; border:1px outset;">Clicked count</td>
                          <td style="background:threedface; border:1px outset;">Queued</td>
                          <td style="background:threedface; border:1px outset;">Failed</td>
                          <td style="background:threedface; border:1px outset;">Unsent</td>
                        </tr>
                      </table>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td><b>Mailing lists</b></td>
                  </tr>
                  <tr>
                    <td>
                      <table id="idMailingLists" cellspacing="0" cellpadding="4" width="100%" border="0" runat="server" class="sc_font" style="border:1px inset; background:window;" rules="groups">
                        <tr>
                          <td style="background:threedface; border:1px outset;">Mailing list</td>
                          <td style="background:threedface; border:1px outset;">New subscribers</td>
                          <td style="background:threedface; border:1px outset;">Lost subscribers</td>
                          <td style="background:threedface; border:1px outset;">Before periode</td>
                          <td style="background:threedface; border:1px outset;">After periode</td>
                          <td style="background:threedface; border:1px outset;">Difference</td>
                          <td style="background:threedface; border:1px outset;">Rate</td>
                        </tr>
                      </table>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </ml:tooltaskpanel></form>
  </body>
</html>
