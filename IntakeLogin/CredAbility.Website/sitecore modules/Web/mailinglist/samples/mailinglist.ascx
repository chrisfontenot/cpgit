<%@ Control Language="c#" AutoEventWireup="true" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Import Namespace="System.Xml.XPath" %>
<%@ Import Namespace="Sitecore.Modules.MailingList.Core" %>
<!-- ********************************************************************* *
 * File   : MailingList.ascx                              Part of SiteCore *
 * Version: 1.00                                          www.sitecore.net *
 *                                                                         *
 * Purpose: Mailing list sample                                            *
 *                                                                         *
 * Bugs   : None known.                                                    *
 *                                                                         *
 * Status : Published.                                                     *
 *                                                                         *
 * Copyright (C) 1999-2003 by SiteCore A/S. All rights reserved.           *
 *                                                                         *
 * This work is the property of:                                           *
 *                                                                         *
 *        SiteCore A/S                                                     *
 *        Tornebuskegade 1                                                 *
 *        1131 Copenhagen K.                                               *
 *        Denmark                                                          *
 *                                                                         *
 * This is a SiteCore published work under SiteCore's                      *
 * shared source license.                                                  *
 *                                                                         *
 * ********************************************************************* -->
<script runat="server" language="C#">

  void Page_Load(object sender, System.EventArgs e) {
    if(!IsPostBack) 
    {
      // make a list of all defined mailing lists
      lists.Items.Clear();
      
      using (MailingList maillist = new MailingList()) {
        XPathNavigator nav = maillist.GetMailingLists();

        XPathNodeIterator node = nav.Select("/sitecore/mailinglist");
        
        while (node.MoveNext()) {
          string name = node.Current.Evaluate("string(name)").ToString();
          string id = node.Current.GetAttribute("id", "");
        
          ListItem item = new ListItem(name, id);
          lists.Items.Add(item);
        }
      }
    }
  }

  public bool ValidateEmail() {
    if (email.Text == "") {
      msg.Controls.Add(new LiteralControl("Please enter an Email address."));
      return false;
    }
    return true;
  }

  public void btnSubscribe_Click(object sender, System.EventArgs e) {

    // build a list of lists to subscribe to

    if (ValidateEmail()) {
      string subscriptionBuilder = "";
      string text = "";
      
      foreach(ListItem item in lists.Items) {
        if (item.Selected) {
          subscriptionBuilder += (subscriptionBuilder != "" ? "," : "") + item.Value;
          text += (text != "" ? ", " : "") + item.Text;
        }
      }
      
      string[] subscriptions = subscriptionBuilder.Split(',');

      // subscribe
      using (MailingList maillist = new MailingList()) {
        maillist.Subscribe(name.Text, email.Text, company.Text, country.Text, subscriptions);

        maillist.PutSubscriberField(email.Text, "customfield", customfield.Text);
        
        // send out welcome mails
        foreach(string subscription in subscriptions) {
          maillist.SendSubscribeMail(subscription, email.Text);
        }
      }
      
      msg.Controls.Add(new LiteralControl("You have subscribed to " + text + "."));
    }
  }
  
  public void btnUnsubscribe_Click(object sender, System.EventArgs e) {

    // unsubscribe from all mailing lists

    if (ValidateEmail()) {

      using (MailingList maillist = new MailingList()) {
        maillist.UnsubscribeAll(email.Text);
      }
      
      msg.Controls.Add(new LiteralControl("You have been unsubscribed."));
    }
  }

  public void btnSubscriptions_Click(object sender, System.EventArgs e) {
  
    // list all of a user subscriptions and his/her custom field
  
    if (ValidateEmail()) {
      string text = "";
      string customField = "";
    
      using (MailingList maillist = new MailingList()) {
        XPathNavigator nav = maillist.GetSubscriptions(email.Text);

        if (nav != null) {
              XPathNodeIterator node = nav.Select("/sitecore/mailinglist");
              
              while (node.MoveNext()) {
                string name = node.Current.Evaluate("string(name)").ToString();
                
                text += name + ", ";
              }
  
        }


        
        
        customField = maillist.GetSubscriberField(email.Text, "customfield");
      }

      text = (text != "" ? text.Substring(0, text.Length - 2) : "nothing");

      msg.Controls.Add(new LiteralControl("You are subscribing to " + text + 
        " and your custom field is '" + customField + "'."));
    }
  }

</script>

<div style="background:white; border:1px solid silver">

<asp:placeholder id="msg" runat="server"/><br/>

<table cellspacing="0" cellpadding="4" border="0">
  <tr>
    <td>Name:</td>
    <td><asp:textbox id="name" runat="server"/></td>
  </tr>
  <tr>
    <td>E-mail:</td>
    <td><asp:textbox id="email" runat="server"/></td>
  </tr>
  <tr>
    <td>Company:</td>
    <td><asp:textbox id="company" runat="server"/></td>
  </tr>
  <tr>
    <td>Country:</td>
    <td><asp:textbox id="country" runat="server"/></td>
  </tr>
  <tr>
    <td>Custom field:</td>
    <td><asp:textbox id="customfield" runat="server"/></td>
  </tr>
  <tr>
    <td colspan="2">
      <asp:checkboxlist id="lists" runat="server"/>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <asp:button id="btnSubscribe" runat="server" text="Subscribe" onclick="btnSubscribe_Click"></asp:button>
      <asp:button id="btnUnsubscribe" runat="server" text="Unsubscribe" onclick="btnUnsubscribe_Click"></asp:button>
      <asp:button id="btnSubscriptions" runat="server" text="List subscriptions" onclick="btnSubscriptions_Click"></asp:button>
    </td>
  </tr>
</table>

</div>
