/* ********************************************************************** *
* File   : MailHandler.cs                                Part of Sitecore *
* Version: 5.1.0                                         www.sitecore.net *
*                                                                         *
* Purpose: Mail handler sample                                            *
*                                                                         *
* Bugs   : None known.                                                    *
*                                                                         *
* Status : Published.                                                     *
*                                                                         *
* Copyright (C) 1999-2006 by Sitecore A/S. All rights reserved.           *
*                                                                         *
* This work is the property of:                                           *
*                                                                         *
*        Sitecore A/S                                                     *
*        Tornebuskegade 1                                                 *
*        1131 Copenhagen K.                                               *
*        Denmark                                                          *
*                                                                         *
* This is a Sitecore published work under Sitecore's                      *
* shared source license.                                                  *
*                                                                         *
* *********************************************************************** */

using System;
using Sitecore;

namespace Sitecore.Modules.MailingList 
{
   public class MyMailHandler: IMailHandler 
   {
      public bool Filter(IMasterItem MailItem, string mailID, string subscriber) 
      {
         return false;
      }

      public void Personalize(string mailID, string subscriber, 
         ref string subject, ref string fromEmail, ref string fromName, ref string htmlBody, 
         ref string alternateText, object email) 
      {

         subject = "MailHandler says Hello.";
      }
   }
}
