/* ********************************************************************** *
* File   : Summary.cs                                    Part of Sitecore *
* Version: 5.1.0                                         www.sitecore.net *
*                                                                         *
* Purpose: Mailing list summary sample                                    *
*                                                                         *
* Bugs   : None known.                                                    *
*                                                                         *
* Status : Published.                                                     *
*                                                                         *
* Copyright (C) 1999-2006 by Sitecore A/S. All rights reserved.           *
*                                                                         *
* This work is the property of:                                           *
*                                                                         *
*        Sitecore A/S                                                     *
*        Tornebuskegade 1                                                 *
*        1131 Copenhagen K.                                               *
*        Denmark                                                          *
*                                                                         *
* This is a Sitecore published work under Sitecore's                      *
* shared source license.                                                  *
*                                                                         *
* *********************************************************************** */
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Sitecore.Master;
using Sitecore.Utilities;

namespace Sitecore.Modules.MailingList.SummarySample
{
   // ------------------------------------------------------------------------
   /// Data holder structs
   // ------------------------------------------------------------------------
   public struct MailStat 
   {
      public string MailID;
      public DateTime Created;
      public string Subject;
      public string SentTo;
      public string MailingLists;
      public int Recipients;
      public int Opened;
      public int Clicked;
      public int OpenedCount;
      public int ClickedCount;
      public int Queued;
      public int Failed;
      public int Unsent;
      public int MuClick;
   }
 
   public struct MailingListStat 
   {
      public string ListID;
      public string Name;
      public int BeforeSubscribers;
      public int AfterSubscribers;
      public int DiffSubscribers;
      public int NewSubscribers;
      public int LostSubscribers;
      public string Rate;
   }

   // ------------------------------------------------------------------------
   /// Implements the summary page
   // ------------------------------------------------------------------------
   public class Summary : System.Web.UI.Page 
   {
      protected sitecore.client.ToolTaskPanel Tooltaskpanel1;
      protected System.Web.UI.HtmlControls.HtmlSelect idStart;
      protected System.Web.UI.HtmlControls.HtmlSelect idEnd;
      protected System.Web.UI.HtmlControls.HtmlTable idMails;
      protected System.Web.UI.HtmlControls.HtmlTable idMailingLists;
      protected System.Web.UI.HtmlControls.HtmlContainerControl idLeads;
      protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;
      protected PlaceHolder idPrintDates;
  
      private void Page_Load(object sender, System.EventArgs e) 
      {
         using (MailingListStatistics mailingList = new MailingListStatistics()) 
         {
            WriteRange(mailingList);

            // validate range
            if (idEnd.SelectedIndex < idStart.SelectedIndex) 
            {
               idEnd.SelectedIndex = idStart.SelectedIndex;
            }

            if (idStart.SelectedIndex >= 0 && idStart.Items.Count > 0) 
            {
               DateTime startDate = DateTime.Parse(idStart.Items[idStart.SelectedIndex].Value);
               DateTime endDate = DateTime.Parse(idEnd.Items[idEnd.SelectedIndex].Value);

               idPrintDates.Controls.Add(new LiteralControl("startdate=" + startDate.ToString("dd-MMM-yyyy") + "&enddate=" + endDate.ToString("dd-MMM-yyyy")));

               Statistics stat = new Statistics();
               stat.Output(mailingList, idMails, idMailingLists, startDate, endDate);

               idLeads.Controls.Add(new LiteralControl(mailingList.QueryMailStatistics(startDate, endDate, "'lead'").ToString()));
            }
            else 
            {
               idLeads.Controls.Add(new LiteralControl("0"));
            }
         }
      }

      // ------------------------------------------------------------------------
      /// <summary>Writes two select boxes with the range of mails.</summary>
      // ------------------------------------------------------------------------
      private void WriteRange(MailingListStatistics mailingList) 
      {
         if (idStart.Items.Count == 0) 
         {
            OleDbDataReader reader = mailingList.QueryMailLogRange();
        
            if (reader.Read()) 
            {                   
               if (reader["startdate"].ToString() != "") 
               {

                  DateTime startDate = (DateTime)reader["startdate"];
                  DateTime endDate = (DateTime)reader["enddate"];
                  startDate = startDate.AddDays(-startDate.Day + 1);
                  endDate = endDate.AddDays(-endDate.Day).AddMonths(1);

                  while(startDate < endDate) 
                  {

                     ListItem item = new ListItem(startDate.ToString("MMMM yyyy"), startDate.ToString("1-MMM-yyyy"));
                     idStart.Items.Add(item);
            
                     item = new ListItem(startDate.ToString("MMMM yyyy"), startDate.AddMonths(1).ToString("dd-MMM-yyyy"));
                     idEnd.Items.Add(item);

                     startDate = startDate.AddMonths(1);
                  }
               }
            }
            reader.Close();
         }
      }

      
      #region Web Form Designer generated code
      override protected void OnInit(EventArgs e) 
      {
         //
         // CODEGEN: This call is required by the ASP.NET Web Form Designer.
         //
         InitializeComponent();
         base.OnInit(e);
      }
		
      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() 
      {    
         this.Load += new System.EventHandler(this.Page_Load);

      }
      #endregion
   }

   public class Statistics 
   {

      public void Output(MailingListStatistics mailingList, HtmlTable idMails, HtmlTable idMailingLists, DateTime startDate, DateTime endDate) 
      {
         ArrayList mails = new ArrayList();
         CollectMails(mailingList, mails, startDate, endDate);
         WriteMails(idMails, mails);

         ArrayList mailingLists = new ArrayList();
         CollectMailingLists(mailingList, mailingLists, startDate, endDate);
         WriteMailingLists(idMailingLists, mailingLists);
      }

      // ------------------------------------------------------------------------
      /// Collects all mails and statistics in the start/end range
      // ------------------------------------------------------------------------
      private void CollectMails(MailingListStatistics mailingList, ArrayList mails, DateTime startDate, DateTime endDate) 
      {

         // loop all mails in the range
         OleDbDataReader reader = mailingList.QueryMailLog(startDate, endDate);
         while(reader.Read()) 
         {
            MailStat mail = new MailStat();
            mail.MailID = Helper.NormalizeGuid(reader["id"].ToString());
            mail.Created = (DateTime)reader["created"];
            mail.Subject = Helper.GetString(reader["subject"]);
            mail.SentTo = Helper.GetString(reader["sentto"]);
            mail.MailingLists = Helper.GetString(reader["lists"]);
            mails.Add(mail);
         }
         reader.Close();

         // recipients
         reader = mailingList.QueryMailStatistics("'sent','bulk'", true);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Recipients = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // open
         reader = mailingList.QueryMailStatistics("'open'", true);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Opened = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // click
         reader = mailingList.QueryMailStatistics("'click'", true);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Clicked = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // open count
         reader = mailingList.QueryMailStatistics("'open'", false);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.OpenedCount = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // click count
         reader = mailingList.QueryMailStatistics("'click'", false);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.ClickedCount = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // queued
         reader = mailingList.QueryMailStatistics("'queued'", false);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Queued = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // fail
         reader = mailingList.QueryMailStatistics("'fail'", false);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Failed = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();

         // unset
         reader = mailingList.QueryMailStatistics("'unsent'", false);
         while(reader.Read()) 
         {
            string mailID = Helper.NormalizeGuid(reader["mailid"].ToString());
            for(int n = 0; n < mails.Count; n++) 
            {
               MailStat mail = (MailStat)mails[n];
               if (mail.MailID == mailID) 
               {
                  mail.Unsent = (int)reader["readers"];
               }
               mails[n] = mail;
            }
         }
         reader.Close();
      }

      // ------------------------------------------------------------------------
      /// Outputs all mail statistics
      // ------------------------------------------------------------------------
      private void WriteMails(HtmlTable idMails, ArrayList mails) 
      {
         for(int n = 0; n < mails.Count; n++) 
         {
            MailStat mail = (MailStat)mails[n];

            HtmlTableRow row = new HtmlTableRow();

            HtmlTableCell cell = new HtmlTableCell();
            cell.InnerText = mail.Created.ToString();
            cell.VAlign = "top";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = mail.Subject;
            cell.VAlign = "top";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Recipients.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Opened.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Clicked.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.OpenedCount.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.ClickedCount.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Queued.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Failed.ToString();
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.Align = "right";
            cell.VAlign = "top";
            cell.InnerText = mail.Unsent.ToString();
            row.Cells.Add(cell);

            idMails.Rows.Add(row);
         }
      }
      
      // ------------------------------------------------------------------------
      /// Collect mailing list statistics
      // ------------------------------------------------------------------------
      private void CollectMailingLists(MailingListStatistics mailingList, ArrayList mailingLists, DateTime startDate, DateTime endDate) 
      {
         Item itm = Factory.Cache.GetItem("/sitecore/content/modules/mailing list/mailing lists");
      
         // loop all mailinglists
         DataIterator list = itm.ChildIterator;
         while (list.MoveNext()) 
         {
            Item child = list.CurrentItem;
            Item.ItemVersion data = child.GetLatestVersion(); 

            MailingListStat mailList = new MailingListStat();
            mailList.ListID = child.ID;
            mailList.Name = child.Name;

            mailingLists.Add(mailList);
         }                             

         // loop found mailing lists and get statistics
         for(int n = 0; n < mailingLists.Count; n++) 
         {
            MailingListStat mailList = (MailingListStat)mailingLists[n];
        
            mailList.NewSubscribers = mailingList.QueryUserAudit(mailList.ListID, startDate, endDate, 1);
            mailList.LostSubscribers = mailingList.QueryUserAudit(mailList.ListID, startDate, endDate, 2);
            mailList.AfterSubscribers = mailingList.GetMailingListSubscribersCount(mailList.ListID);
            mailList.BeforeSubscribers = mailList.AfterSubscribers - mailList.NewSubscribers + mailList.LostSubscribers;
            mailList.DiffSubscribers = mailList.AfterSubscribers - mailList.BeforeSubscribers;

            if (mailList.BeforeSubscribers <=  0) 
            {
               mailList.Rate = "-";
            }
            else 
            {
               mailList.Rate = ((float)mailList.DiffSubscribers / mailList.BeforeSubscribers * 100).ToString("n2") + "%";
            }

            mailingLists[n] = mailList;
         }
      }

      // ------------------------------------------------------------------------
      /// Output all mailing list statistics
      // ------------------------------------------------------------------------
      private void WriteMailingLists(HtmlTable idMailingLists, ArrayList mailingLists) 
      {
         for(int n = 0; n < mailingLists.Count; n++) 
         {
            MailingListStat mailList = (MailingListStat)mailingLists[n];

            HtmlTableRow row = new HtmlTableRow();

            HtmlTableCell cell = new HtmlTableCell();
            cell.InnerText = mailList.Name;
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = mailList.NewSubscribers.ToString();
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = mailList.LostSubscribers.ToString();
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = (mailList.BeforeSubscribers >= 0 ? mailList.BeforeSubscribers.ToString() : "N/A");
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = (mailList.AfterSubscribers >= 0 ? mailList.AfterSubscribers.ToString() : "N/A");
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = mailList.DiffSubscribers.ToString();
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);

            cell = new HtmlTableCell();
            cell.InnerText = mailList.Rate;
            cell.VAlign = "top";
            cell.Align = "right";
            row.Cells.Add(cell);
        
            idMailingLists.Rows.Add(row);
         }
      }
   }

   public class MailingListStatistics : MailingList 
   {
      //---------------------------------------------------------------
      /// <summary>Returns the start and end dates of mails.</summary>
      //---------------------------------------------------------------
      public OleDbDataReader QueryMailLogRange() 
      {
         string commandText = "select min(created) as startdate, max(created) as enddate from maillog";
         OleDbCommand command = CreateCommand(commandText);
         OleDbDataReader reader = command.ExecuteReader();
         return reader;
      }

      //---------------------------------------------------------------
      /// <summary>Returns the mail log.</summary>
      //---------------------------------------------------------------
      public OleDbDataReader QueryMailLog(DateTime startDate, DateTime endDate) 
      {
         string commandText = "select id, created, subject, sentto, lists from MailLog where created between #" + startDate.ToString("yyyy-MM-dd") + "# and #" + endDate.ToString("yyyy-MM-dd") + "# order by Created";
         OleDbCommand command = CreateCommand(commandText);
         OleDbDataReader reader = command.ExecuteReader();
         return reader;
      }

      //---------------------------------------------------------------
      /// <summary>Returns a count of the MailStatistics table.</summary>
      //---------------------------------------------------------------
      public OleDbDataReader QueryMailStatistics(string action, bool distinct) 
      {
         string commandText = "";
         if (distinct) 
         {
            commandText = 
               "select maillog.id as mailid, count(*) as readers " +
               "from maillog, " +
               "  (select distinct fk_mail, fk_subscriber " +
               "  from mailstatistics " +
               "  where action in (" + action + ")) as listreadersdistinct " +
               "where maillog.id = listreadersdistinct.fk_mail " +
               "group by maillog.id;";
         }
         else 
         {
            commandText = "select fk_mail as mailid, count(*) as readers from MailStatistics where [Action] in (" + action + ") group by fk_mail;";
         }
         OleDbCommand command = CreateCommand(commandText);
         OleDbDataReader reader = command.ExecuteReader();
         return reader;
      }
 
      //---------------------------------------------------------------
      /// <summary>Returns a count of the MailStatistics table.</summary>
      //---------------------------------------------------------------
      public int QueryMailStatistics(DateTime startDate, DateTime endDate, string action) 
      {
         string commandText = "select count(*) from MailStatistics where auditdate between #" + startDate.ToString("yyyy-MM-dd") + "# and #" + endDate.ToString("yyyy-MM-dd") + "# and [Action] in (" + action + ")";
         OleDbCommand command = CreateCommand(commandText);

         object count = command.ExecuteScalar();

         return (count != null ? (int)count : 0);
      }

      //---------------------------------------------------------------
      /// <summary>Returns a count of the UserAudit table.</summary>
      //---------------------------------------------------------------
      public int QueryUserAudit(string mailingListID, DateTime startDate, DateTime endDate, int action) 
      {
         string commandText = "select count(*) from UserAudit where fk_lists = '" + mailingListID + "' and AuditDate between #" + startDate.ToString("yyyy-MM-dd") + "# and #" + endDate.ToString("yyyy-MM-dd") + "# and [Action] = " + action.ToString();
         OleDbCommand command = CreateCommand(commandText);

         object count = command.ExecuteScalar();

         return (count != null ? (int)count : 0);
      }
   }
}
