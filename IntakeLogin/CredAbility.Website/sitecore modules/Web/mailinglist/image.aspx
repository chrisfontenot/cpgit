<%@ Page language="c#" AutoEventWireup="true" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<%@ Import namespace="Sitecore" %>
<%@ Import namespace="Sitecore.Modules.MailingList.Core" %>
<script language="C#" runat="server">

   private void Page_Load(object sender, System.EventArgs e)
   {
      string site = Sitecore.Context.Site.Name;

      Sitecore.Context.SetActiveSite("shell");

      try
      {
         string mailID = MainUtil.GetString(Request.QueryString["mid"]);
         string subscriberID = MainUtil.GetString(Request.QueryString["uid"]);

         using (new Sitecore.SecurityModel.SecurityDisabler())
         {
            using (MailingList ml = new MailingList())
            {
               ml.MailRead(mailID, subscriberID);
            }
         }
      }
      catch
      {
      }
      finally
      {
         Sitecore.Context.SetActiveSite(site);
      }

      Response.StatusCode = 404;
      Response.Write("<HTML><HEAD></HEAD><BODY>404 Page Not Found</BODY></HTML>");
   }

</script>
