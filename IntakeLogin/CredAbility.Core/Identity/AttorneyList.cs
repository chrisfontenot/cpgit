﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Identity
{
    public class AttorneyList : List<AttorneyInfo>
    {
        public AttorneyList() { }

        public string PrettyPrint()
        {
            string value = "";
            string endTag = "<BR>";
            foreach (AttorneyInfo att in this)
            {
                value += att.PrettyPrint();
                value += endTag + endTag;
            }
            return value;
        }
    }
}