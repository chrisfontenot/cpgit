namespace CredAbility.Core.Identity
{
    public class OfflineProfileIdentity
    {
        public string ClientId { get; set; }
        public string SocialSecurityNumber { get; set; }
    }
}