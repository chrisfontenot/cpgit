﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Identity
{
    public class AttorneyInfo
    {
        public AttorneyInfo() { }

        //<Attorneys>
        //<Attorney>
        //<FirmName>David L Holbrook</FirmName>
        //<FirmCode>2343</FirmCode>
        //<LocationPhone>(770) 717-8707</LocationPhone>
        //<MainPhone>(770) 717-8707</MainPhone>
        //<Url></Url>
        //<AddressLine1>P O Box 2452</AddressLine1>
        //<AddressLine2></AddressLine2>
        //<City>Lilburn</City>
        //<State>GA</State>
        //<ZipCode>30047</ZipCode>
        //<EstimatedDistance>16.0537026052138</EstimatedDistance>
        //</Attorney>

        private string _firmName;
        public string FirmName
        {
            get { return this._firmName; }
            set { this._firmName = value; }
        }

        private string _firmCode;
        public string FirmCode
        {
            get { return this._firmCode; }
            set { this._firmCode = value; }
        }

        private string _locationPhone;
        public string LocationPhone
        {
            get { return this._locationPhone; }
            set { this._locationPhone = value; }
        }

        private string _mainPhone;
        public string MainPhone
        {
            get { return this._mainPhone; }
            set { this._mainPhone = value; }
        }

        private string _url;
        public string Url
        {
            get { return this._url; }
            set { this._url = value; }
        }

        private string _addressLine1;
        public string AddressLine1
        {
            get { return this._addressLine1; }
            set { this._addressLine1 = value; }
        }

        private string _addressLine2;
        public string AddressLine2
        {
            get { return this._addressLine2; }
            set { this._addressLine2 = value; }
        }

        private string _city;
        public string City
        {
            get { return this._city; }
            set { this._city = value; }
        }

        private string _state;
        public string State
        {
            get { return this._state; }
            set { this._state = value; }
        }

        private string _zip;
        public string Zip
        {
            get { return this._zip; }
            set { this._zip = value; }
        }

        private string _estimatedDistance;
        public string EstimateDistance
        {
            get { return this._estimatedDistance; }
            set { this._estimatedDistance = value; }
        }

        public string PrettyPrint()
        {
            string value = "";

            value += "<table border=\"1\">";
            foreach (System.Reflection.PropertyInfo info in this.GetType().GetProperties())
            {
                if (info.CanRead)
                {
                    value += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", info.Name, info.GetValue(this, null));
                }
            }
            value += "</table>";

            return value;
        }
    }
}