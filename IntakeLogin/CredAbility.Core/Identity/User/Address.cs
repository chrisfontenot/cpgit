﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Address
/// The Address class holds all the attributes needed to define a single address for the user.
/// 
/// AddressList
/// This generic class allows us to manipulate a list of Address objects.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class Address
    {
        public Address() { }

        private long _id;
        public long ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        private string _addressType;
        public string AddressType
        {
            get { return this._addressType; }
            set { this._addressType = value; }
        }

        private string _addressLine1;
        public string AddressLine1
        {
            get { return this._addressLine1; }
            set { this._addressLine1 = value; }
        }

        private string _addressLine2;
        public string AddressLine2
        {
            get { return this._addressLine2; }
            set { this._addressLine2 = value; }
        }

        private string _city;
        public string City
        {
            get { return this._city; }
            set { this._city = value; }
        }

        private string _state;
        public string State
        {
            get { return this._state; }
            set { this._state = value; }
        }

        private string _zip;
        public string Zip
        {
            get { return this._zip; }
            set { this._zip = value; }
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }

        private string _modifiedBy;
        public string ModifiedBy
        {
            get { return this._modifiedBy; }
            set { this._modifiedBy = value; }
        }
    }

    public class AddressList : List<Address> { }
}
