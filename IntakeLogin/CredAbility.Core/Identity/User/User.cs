﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CredAbility.Core.Common;
using CredAbility.Core.Document;
using CredAbility.Core.Identity;

/// <summary>
/// User
/// The User class holds all the attributes needed to define the user.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class User
    {
        public User() { }

        public static string CacheName = "User";

        private long _id;
        public long ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        private string _username;
        public string Username
        {
            get { return this._username; }
            set { this._username = value; }
        }

        private string _password;
        public string Password
        {
            get { return this._password; }
            set { this._password = value; }
        }

        private string _status;
        public string Status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        private string _email;
        public string Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        private string _ssn;
        public string SSN
        {
            get { return this._ssn; }
            set { this._ssn = value; }
        }

        private DateTime _dateofbirth;
        public DateTime DateOfBirth
        {
            get { return this._dateofbirth; }
            set { this._dateofbirth = value; }
        }

        private string _firstName;
        public string FirstName
        {
            get { return this._firstName; }
            set { this._firstName = value; }
        }

        private string _lastName;
        public string LastName
        {
            get { return this._lastName; }
            set { this._lastName = value; }
        }

        private string _language;
        public string Language
        {
            get { return this._language; }
            set { this._language = value; }
        }

        private string _referringSite;
        public string ReferringSite
        {
            get { return this._referringSite; }
            set { this._referringSite = value; }
        }

        private AccountList _hostAccounts;
        public AccountList HostAccounts
        {
            get { return this._hostAccounts; }
            set { this._hostAccounts = value; }
        }

        private AddressList _addresses;
        public AddressList Addresses
        {
            get { return this._addresses; }
            set { this._addresses = value; }
        }

        private string _phoneCell;
        public string PhoneCell
        {
            get { return this._phoneCell; }
            set { this._phoneCell = value; }
        }

        private string _phoneHome;
        public string PhoneHome
        {
            get { return this._phoneHome; }
            set { this._phoneHome = value; }
        }

        private string _phoneWork;
        public string PhoneWork
        {
            get { return this._phoneWork; }
            set { this._phoneWork = value; }
        }

        private bool _passwordResetRequested;
        public bool PasswordResetRequested
        {
            get { return this._passwordResetRequested; }
            set { this._passwordResetRequested = value; }
        }

        private DateTime _passwordResetDate;
        public DateTime PasswordResetDate
        {
            get { return this._passwordResetDate; }
            set { this._passwordResetDate = value; }
        }

        private DateTime _loginAttempts;
        public DateTime LoginAttempts
        {
            get { return this._loginAttempts; }
            set { this._loginAttempts = value; }
        }

        private DateTime _lastLoginDate;
        public DateTime LastLoginDate
        {
            get { return this._lastLoginDate; }
            set { this._lastLoginDate = value; }
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }

        private string _modifiedBy;
        public string ModifiedBy
        {
            get { return this._modifiedBy; }
            set { this._modifiedBy = value; }
        }

        private DateTime _lockedDate;
        public DateTime LockedDate
        {
            get { return this._lockedDate; }
            set { this._lockedDate = value; }
        }

        private LineOfService _lineOfService;
        public LineOfService LineOfService
        {
            get { return this._lineOfService; }
            set { this._lineOfService = value; }
        }

        private string _documentTokenID;
        public string TokenID
        {
            get { return this._documentTokenID; }
            set { this._documentTokenID = value; }
        }

        private DocumentList _documentList;
        public DocumentList DocumentList
        {
            get { return this._documentList; }
            set { this._documentList = value; }
        }

        private DocumentDataTable _documentData;
        public DocumentDataTable DocumentData
        {
            get { return this._documentData; }
            set { this._documentData = value; }
        }

        public bool IsLoggedIn()
        {
            // add code here to check for login status

            return true;
        }

        //public CustomResponse GetDocumentTokenID()
        //{
        //    DocumentHelper dh = new DocumentHelper();
        //    CustomResponse response = dh.AuthenticateUser(this.ID.ToString());
        //    if (!response.HasError)
        //    {
        //        this.TokenID = response.ResponseStringValue;
        //    }
        //    return response;
        //}

        //public CustomResponse GetDocumentList()
        //{
        //    DocumentHelper dh = new DocumentHelper();
        //    CustomResponse response = dh.GetClientDocumentList(this.TokenID, this.ID.ToString());
        //    if (!response.HasError)
        //    {
        //        this.TokenID = response.ResponseStringValue;
        //    }
        //    return response;
        //}

        //public CustomResponse GetDocumentData()
        //{
        //    DocumentHelper dh = new DocumentHelper();
        //    CustomResponse response = dh.GetClientDocumentData(this.TokenID, this.ID.ToString(), "");
        //    if (!response.HasError)
        //    {
        //        this.TokenID = response.ResponseStringValue;
        //    }
        //    return response;
        //}
    }
}
