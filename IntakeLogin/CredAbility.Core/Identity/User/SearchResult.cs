﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// SearchResult
/// The SearchResult class holds all the searches a user has performed.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class SearchResult
    {
        public SearchResult() { }

        private int _totalCount;
        public int TotalCount
        {
            get { return this._totalCount; }
            set { this._totalCount = value; }
        }

        private UserSearchResult _results;
        public UserSearchResult Results
        {
            get { return this._results; }
            set { this._results = value; }
        }
    }
}
