﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Account
/// The Account class holds all the attributes needed to define a single account for the user.
/// 
/// AccountList
/// This generic class allows us to manipulate a list of Account objects.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class Account
    {
        public Account() { }

        private long _id;
        public long ID
        {
            get { return this._id; }
            set { this._id = value; }
        }

        private string _accountType;
        public string AccountType
        {
            get { return this._accountType; }
            set { this._accountType = value; }
        }

        private string _hostId;
        public string HostID
        {
            get { return this._hostId; }
            set { this._hostId = value; }
        }

        private string _status;
        public string Status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        private string _lastCompletedPage;
        public string LastCompletedPage
        {
            get { return this._lastCompletedPage; }
            set { this._lastCompletedPage = value; }
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        private DateTime _modifiedDate;
        public DateTime ModifiedDate
        {
            get { return this._modifiedDate; }
            set { this._modifiedDate = value; }
        }

        private string _modifiedBy;
        public string ModifiedBy
        {
            get { return this._modifiedBy; }
            set { this._modifiedBy = value; }
        }
    }

    public class AccountList : List<Account> { }
}
