﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// UserSearchResult
/// The UserSearchResult class holds all the attributes of the user performing the search.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class UserSearchResult
    {
        public UserSearchResult() { }

        private long _userID;
        public long UserID
        {
            get { return this._userID; }
            set { this._userID = value; }
        }

        private string _username;
        public string Username
        {
            get { return this._username; }
            set { this._username = value; }
        }

        private string _status;
        public string Status
        {
            get { return this._status; }
            set { this._status = value; }
        }

        private string _email;
        public string Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        private string _ssn;
        public string SSN
        {
            get { return this._ssn; }
            set { this._ssn = value; }
        }

        private DateTime _dateofbirth;
        public DateTime DateOfBirth
        {
            get { return this._dateofbirth; }
            set { this._dateofbirth = value; }
        }

        private string _firstName;
        public string FirstName
        {
            get { return this._firstName; }
            set { this._firstName = value; }
        }

        private string _lastName;
        public string LastName
        {
            get { return this._lastName; }
            set { this._lastName = value; }
        }
    }

}
