﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Identity
{
    public enum LineOfService
    {
        BankruptcyPreFilingCounseling,
        BankruptcyDebtorEducation,
        DebtManagementPlan,
        ForeclosurePrevention,
        ReverseMortgage,
        HousingPrePurchase,
        BudgetAndCreditCounseling,
        Education
    }
}
