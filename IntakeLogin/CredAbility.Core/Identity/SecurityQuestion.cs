﻿/// <summary>
/// SecurityQuestion
/// The SecurityQuestion class holds all the attributes for the possible questions that can be asked.
/// </summary>
namespace CredAbility.Core.Identity
{
    public class SecurityQuestion
    {
        public byte ID { get; set; }
        public string Text { get; set;}
    }
}
