﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Client;

namespace CredAbility.Core.CreditScoreChallenge
{
    public class Contestant
    {
        private readonly string _clientId;

        public Contestant(string clientId)
        {
            this._clientId = clientId;
            this.SubscriptionDate = DateTime.Now;
        }

        public string ClientId { get { return this._clientId; } }
        public string Fullname { get; set; }
        public SocialSecurityNumber SSN { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string SecondaryPhoneNumber { get; set; }
        public DateTime SubscriptionDate { get; set; }

        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PreferredLanguage { get; set; }
    }
}