﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.CreditScoreChallenge
{
    public enum ImprovmentDirection { Decrease, Increase}

    public class ChallengeHistoryItem
    {
        #region Constructors

        public ChallengeHistoryItem(string clientId)
        {
            this._clientId = clientId;
            this.Date = DateTime.Now;
        }


        #endregion
        
        #region Fields

        private readonly string _clientId;

        #endregion

        #region Properties

        public string ClientId
        {
           get { return this._clientId; }
        }

        public DateTime Date { get; set; }

        public string Rank { get; set; }
        public string PourcentageImprovement { get; set; }
        public string Score { get; set; }
        public ImprovmentDirection ImprovmentDirection { get; set; }

        #endregion
    }
}
