﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class ClientWebsiteStatus
    {
        public byte PercentComplete { get; set; }
        public DateTime CompletedDate { get; set; }
        public string WebsiteCode { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }


        public string WebsiteText
        {

            get
            {
                switch (this.WebsiteCode.Trim().ToUpper())
                {
                    case "BCH":
                        return Resources.WebsiteCode_BCH;
                    case "BKC":
                        return Resources.WebsiteCode_BKC;
                    case "BKDE":
                        return Resources.WebsiteCode_BKDE;
                    case "CA ":
                        return Resources.WebsiteCode_CA;
                    case "DMP":
                        return Resources.WebsiteCode_DMP;

                    case "HUD":
                        return Resources.WebsiteCode_HUD;
                        
                    case "PRP":
                        return Resources.WebsiteCode_PRP;
                    case "RVM":
                        return Resources.WebsiteCode_RVM;
                    default:
                        return string.Empty;
                }
            }

        }
    }
}
