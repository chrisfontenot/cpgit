﻿using System;
using System.Collections.Generic;

namespace CredAbility.Core.Client
{
    public class ClientProfile
    {
        //Warning: if you add value here, please be sure that the value is transfer correctly in EditProfile
        public string Username { get; set; }
        public string Password { get; set; }
        public string ClientID { get; set; }
        public string IdentityTokenID { get; set; }
        public List<string> Types { get; set; }
        public DateTime LastLogin { get; set; }
        public bool IsProfileUpdateRequired { get; set; }
        public DateTime PasswordResetDate { get; set; }
        public bool IsFirstTime { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        public string PreferredLanguage { get; set; }
        public string ReferringSite { get; set; }

        public List<ClientAccount> ClientAccounts { get; set; }
        public string DMPAccessID { get; set; }
        public string DocumentAccessID { get; set; }
        
        public string DocumentTokenID { get; set; }
        public string DocumentID { get; set; }
        private string _Avatar;

        public bool IsCreditScoreChallengeContestant { get; set; }

        public string Avatar
        {
            get
            {
                if (string.IsNullOrEmpty(this._Avatar))
                {
                    this._Avatar = "avatars-1";
                }

                return this._Avatar;
            }
            set { this._Avatar = value; }
        }

        public bool UpdateSecurityInfoRequired
        {
            get { return this.IsProfileUpdateRequired; }
        }

        public bool MustChangePassword
        {
            get { return (this.PasswordResetDate < DateTime.Now); }
        }

        public ClientProfile()
        {
            this.Types = new List<string>();
        }

        public string DisplayId
        {
            get
            {
                if (!string.IsNullOrEmpty(DMPAccessID))
                {
                    return this.DMPAccessID;
                }
                else if (!string.IsNullOrEmpty(DocumentAccessID))
                {
                    return this.DocumentAccessID;
                }
                else
                {
                    return this.ClientID;
                }
            }
        }
    }
}
