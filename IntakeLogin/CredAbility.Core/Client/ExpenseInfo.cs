﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace CredAbility.Core.Client
{
    public class ExpenseInfo
    {
        public List<ExpenseCategory> ExpenseCategories { get; set; }
        public decimal TotalIncome { get; set; }
        public DataTable IncomeAndExpenseRawData { get; set; }

        public ExpenseInfo()
        {
            ExpenseCategories = new List<ExpenseCategory>();
            IncomeAndExpenseRawData = new DataTable();
        }

        public decimal TotalExpenses
        {
            get
            {
                return CalculateCostOfAllExpenseCategories();
            }
        }

        private decimal CalculateCostOfAllExpenseCategories()
        {
            decimal cost = 0.00M;
            foreach (ExpenseCategory expenseCategory in this.ExpenseCategories)
            {
                cost += expenseCategory.TotalCategoryCost;
            }
            return cost;
        }
    }
}
