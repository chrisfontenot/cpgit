namespace CredAbility.Core.Client
{
    public class SocialSecurityNumber
    {
        public string FirstPart { get; set; }
        public string SecondPart { get; set; }
        public string ThirdPart { get; set; }

        
        public string SecuredSSN
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FirstPart) && !string.IsNullOrEmpty(this.SecondPart) && !string.IsNullOrEmpty(this.ThirdPart))
                {
                    return string.Format("***-**-{0}", this.ThirdPart);
                }

                return string.Empty;
            }
        }

        public string FormatedSSN
        {
            get
            {
                if (!string.IsNullOrEmpty(this.FirstPart) && !string.IsNullOrEmpty(this.SecondPart) && !string.IsNullOrEmpty(this.ThirdPart))
                {
                    return string.Format("{0}-{1}-{2}", this.FirstPart, this.SecondPart, this.ThirdPart);
                }

                return string.Empty;
            }
        }

        public SocialSecurityNumber(string ssn)
        {
            if (!string.IsNullOrEmpty(ssn))
            {
                string s = null;
                if (ssn.Contains("-") && ssn != "--")
                {
                    s = ssn.Replace("-", "");
                    
                }
                else if (ssn.Length == 9)
                {
                    s = ssn;
                }


                if (!string.IsNullOrEmpty(s))
                {
                    FirstPart = s.Substring(0, 3);
                    SecondPart = s.Substring(3, 2);
                    ThirdPart = s.Substring(5, 4);
                }

            }
        }

        public SocialSecurityNumber()
        {
        }

        public SocialSecurityNumber(string firstPart, string secondPart, string thirdPart)
        {
            FirstPart = firstPart;
            SecondPart = secondPart;
            ThirdPart = thirdPart;
        }


        public void AssignFullSSN(string ssn)
        {
            if (!string.IsNullOrEmpty(ssn) && ssn.Contains("-") && ssn != "--")
            {
                string s = ssn.Replace("-", "");
                FirstPart = s.Substring(0, 3);
                SecondPart = s.Substring(3, 2);
                ThirdPart = s.Substring(5, 4);
            }
        }
    }
}