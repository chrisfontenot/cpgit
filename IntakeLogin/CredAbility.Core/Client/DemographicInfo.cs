﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class DemographicInfo
    {
        public string PCPType { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string CityStateZip { get; set; }
        public string SSN { get; set; }
        public DateTime DocumentDate { get; set; }
        public int CreditScore { get; set; }
        public string CreditScoreText { get; set; }
    }
}
