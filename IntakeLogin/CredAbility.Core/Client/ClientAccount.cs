﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class ClientAccount
    {
        public string AccountName { get; set; }
        public long InternetId { get; set; }
        public string HostId { get; set; }

        public ClientAccount(string AccountName, long InternetId, string HostId)
        {
            this.AccountName = AccountName;
            this.InternetId = InternetId;
            this.HostId = HostId;
        }
    }
}
