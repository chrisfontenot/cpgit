﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public enum ClientRole
    {
        Default,
        BankruptcyPreFilingCounseling,
        BankruptcyEducation,
        DMPCounseling,
        HousingForeclosure,
        HousingReverseMortgage,
        HousingPrePurchase,
        BudgetAndCredit,
        ClientAccessDMP,
        NewUser
    }
}
