﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.ActionPlan;
using CredAbility.Core.Common;

namespace CredAbility.Core.Client
{
    public class ClientCounselingSummaryInfo
    {
        public DateTime AsOfDate { get; set; }
        public PersonalAssetsInfo PersonalAssetsInfo { get; set; }
        public ExpenseInfo ExpenseInfo { get; set; }
        public DemographicInfo Demographics { get; set; }
        public List<DebtInfo> DebtInfoList { get; set; }
        public List<ActionItem> ActionItems { get; set; }
        public bool IsSuccessful { get; set; }
        public List<ResultMessage> ResultMessages { get; set; }

        public ClientCounselingSummaryInfo()
        {
            AsOfDate = new DateTime();
            PersonalAssetsInfo = new PersonalAssetsInfo();
            ExpenseInfo = new ExpenseInfo();
            Demographics = new DemographicInfo();
            DebtInfoList = new List<DebtInfo>();
            ActionItems = new List<ActionItem>();
            ResultMessages = new List<ResultMessage>();
        }
    }
}
