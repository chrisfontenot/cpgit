﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class PersonalAssetsInfo
    {
        public decimal TotalIncome { get; set; }
        public decimal TotalExpenses { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal TotalLiabilities { get; set; }
        public decimal TotalDebtPayment { get; set; }
        public decimal CashAmountMonthEnd { get; set; }
        public decimal TotalNetWorth { get; set; }
    }
}
