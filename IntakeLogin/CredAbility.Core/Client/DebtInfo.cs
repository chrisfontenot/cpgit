﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class DebtInfo
    {
        public string Type { get; set; }
        public int Amount { get; set; }
    }
}
