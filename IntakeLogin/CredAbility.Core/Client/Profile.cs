﻿
namespace CredAbility.Core.Client
{
    public class Profile: ClientProfile
    {
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string CellPhone { get; set; }
        public string MailingAddressLine1 { get; set; }
        public string MailingAddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public SocialSecurityNumber SSN { get; set; }
        public string MaritalStatus { get; set; }
        public long MailingAddressId { get; set; }
        public string HostId { get; set; }
       

        public Profile()
        {
            this.SSN = new SocialSecurityNumber();
        }
    }
}
