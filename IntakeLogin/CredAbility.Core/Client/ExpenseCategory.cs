﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Client
{
    public class ExpenseCategory
    {
        //For the unique identification of the Category
        public string Id { get; set; }
        public string Name { get; set; }
        public List<ExpenseItem> ExpenseItems { get; set; }
        public decimal Percentage { get; set; }


        public ExpenseCategory()
        {
            this.ExpenseItems = new List<ExpenseItem>();
        }

        public decimal TotalCategoryCost
        {
            get
            {
                return CalculateCostOfAllItems();
            }
        }

        private decimal CalculateCostOfAllItems()
        {
            decimal cost = 0.00M;
            foreach (ExpenseItem item in this.ExpenseItems)
            {
                cost += item.Cost;
            }
            return cost;
        }
    }
}
