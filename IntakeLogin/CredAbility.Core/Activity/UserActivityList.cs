﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Activity
{
    public class UserActivityList : List<UserActivity>
    {
        public UserActivityList()
        {
        }

        public UserActivityList(int capacity) : base(capacity)
        {
        }

        public List<UserActivity> GetAlerts()
        {
            throw new NotImplementedException();
        }

        public Dictionary<DateTime, List<UserActivity>> GetActivitiesByDate()
        {
            var result = new Dictionary<DateTime, List<UserActivity>>();
            foreach (var activity in this)
            {

                if (!result.ContainsKey(activity.Date.Date))
                {
                    result.Add(activity.Date.Date, new List<UserActivity>());
                    
                }
            }
            foreach (var activity in this)
            {
                foreach (var keyValuePair in result)
                {
                    if(activity.Date.Date == keyValuePair.Key.Date)
                    {
                        keyValuePair.Value.Add(activity);
                    }
                }
            }
            return result;
        }
    }
}
