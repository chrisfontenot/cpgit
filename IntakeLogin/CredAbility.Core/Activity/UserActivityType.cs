﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Activity
{
    public enum UserActivityType
    {
        Updated,
        Watched,
        Listened,
        Read,
        Add,
        Login,
        Download,
        DMPPaymentMade,
        DMPProposalAccepted,
        DMPPaymentOverdue,
        Marked,
        CounselingSessionCompleted,
        CounselingSessionPartiallyCompleted,
        FeeWaiverApproved,
        FeeWaiverDenied,
        Completed
    }
}
