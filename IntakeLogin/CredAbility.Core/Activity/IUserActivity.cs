﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Activity
{
    public interface IUserActivity
    {
        string Text { get; set; }
        DateTime Date { get; set; }
        UserActivityType Type { get; set; }
    }
}
