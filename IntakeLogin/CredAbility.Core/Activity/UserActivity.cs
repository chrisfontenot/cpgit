﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Activity
{
    public class UserActivity: IUserActivity
    {
        public UserActivity(string userID)
        {
            this.ID = userID;
            this.Date = DateTime.Now;
        }
        public string ID { get; protected set; }
        public string Text{ get; set; }
        public DateTime Date { get; set; }
        public UserActivityType Type { get; set; }
    }
}
