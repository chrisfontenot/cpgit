﻿using System.Collections.Generic;

namespace CredAbility.Core.Common
{

    public class ActionResult<T> : ActionResult
    {
        public T Value { get; set; }
    }



    /// <summary>
    /// Class use when you make process and want to validate the result
    /// </summary>
    public class ActionResult
    {

        public bool IsSuccessful { get; set; }
        public ResultMessageList Messages { get; set; }

        public ActionResult()
        {
            this.Messages = new ResultMessageList();
        }

        public ActionResult(bool defaultSucessfulValue):this()
        {
            this.IsSuccessful = defaultSucessfulValue;
        }

        /// <summary>
        /// Merge this action result with another one
        /// </summary>
        /// <param name="result">ActionResult to merge</param>
        public void MergeActionResult(ActionResult result)
        {
            this.IsSuccessful &= result.IsSuccessful;
            foreach (var message in result.Messages)
            {
                if (!this.Messages.Contains(message))
                {
                    this.Messages.Add(message);
                }
            }
        }

    }
}
