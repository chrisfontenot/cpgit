﻿using System;

namespace CredAbility.Core.Common
{
    public class CustomResponse<T>
    {
        /* This helpful little class helps in those instances when you need to store
         * multiple things from a method call, like a string for a response message,
         * a bool for a response status, and maybe a duration for how long it took
         * to make the call.
        */

        // constructor
        public CustomResponse() { }

        // public members
        public T ResponseValue { get; set; }

        private bool _hasError = false; // set to false by default, until we explicitly have an error
        public bool HasError
        {
            get { return this._hasError; }
            set { this._hasError = value; }
        }

        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }

        // public methods
        public TimeSpan Duration
        {
            get { return StopTime.Subtract(StartTime); }
        }

        public void StartTimer()
        {
            this.StartTime = DateTime.Now;

            // we are assigning the start time to the stop time here,
            // that way if we get a 0 duration, we know something went wrong
            this.StopTime = this.StartTime;
        }

        public void StopTimer()
        {
            this.StopTime = DateTime.Now;
        }

        public string PrettyPrintDuration()
        {
            return Duration.TotalSeconds.ToString();
        }
    }
}