﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Common
{
    public class ResultMessageList : List<ResultMessage>
    {
        public bool HasMessageCodeStartingWith(string partialCode )
        {
            foreach (ResultMessage message in this)
            {
                if (message.Code.StartsWith(partialCode))
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasMessage(string code)
        {

            foreach (ResultMessage message in this)
            {
                if (message.Code == code)
                {
                    return true;
                }
            }
            return false;
        }

        public ResultMessage GetResultMessage(string code)
        {
            foreach (ResultMessage message in this)
            {
                if (message.Code == code)
                {
                    return message;
                }
            }
            return null;
        }

        public List<ResultMessage> GetByLevelType(ResultMessageLevelType levelType )
        {
            if(levelType == null)
            {
                throw new ArgumentNullException("levelType");
            }

            List<ResultMessage> result = new List<ResultMessage>();

            foreach (ResultMessage resultMessage in this)
            {
                if (levelType == resultMessage.LevelType)
                {
                    result.Add(resultMessage);
                }
            }

            return result;
        }

    }
}
