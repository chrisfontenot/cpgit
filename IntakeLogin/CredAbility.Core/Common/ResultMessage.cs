using System;

namespace CredAbility.Core.Common
{
    public class ResultMessage
    {
        public ResultMessageLevelType LevelType { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }

        public ResultMessage(ResultMessageLevelType levelType, string message, string code)
        {
            this.LevelType = levelType;
            this.Message = message;
            this.Code = code;

        }

        public ResultMessage(string message, string code): this(ResultMessageLevelType.Error, message, code)
        {
        }


        public override bool Equals(Object obj)
        {
            // If parameter cannot be cast to ResultMessage return false:
            var resultMessage = obj as ResultMessage;
            if (resultMessage == null)
            {
                return false;
            }

            // Return true if the fields match:
            return this.Code == resultMessage.Code;
        }

        public bool Equals(ResultMessage resultMessage)
        {
            // Return true if the fields match:
            return this.Code == resultMessage.Code;
        }

        public static bool operator ==(ResultMessage a, ResultMessage b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            // Return true if the fields match:
            return a.Code == b.Code;
        }

        public static bool operator !=(ResultMessage a, ResultMessage b)
        {
            return !(a == b);
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}