using System;

namespace CredAbility.Core.Common
{
    public class ResultMessageLevelType
    {

        public virtual string Code
        {
            get { throw new NotImplementedException("The code property should be override");}
        }

        public virtual string Description
        {
            get { throw new NotImplementedException("The description property should be override"); }
        }

        public static ResultMessageLevelType Error = new ResultMessageLevelTypeEnum.Error();
        public static ResultMessageLevelType Warning = new ResultMessageLevelTypeEnum.Warning();
        public static ResultMessageLevelType Info = new ResultMessageLevelTypeEnum.Info();
     
       

        public static class ResultMessageLevelTypeEnum
        {
            public class Error : ResultMessageLevelType
            {

                public override string Code
                {
                    get { return "Error"; }
                }

                public override string Description
                {
                    get { return "Error"; }
                }
            }

            public class Warning : ResultMessageLevelType
            {

                public override string Code
                {
                    get { return "Warning"; }
                }

                public override string Description
                {
                    get { return "Warning"; }
                }
            }

            public class Info : ResultMessageLevelType
            {

                public override string Code
                {
                    get { return "Info"; }
                }

                public override string Description
                {
                    get { return "Info"; }
                }
            }

           
        }

    }
}