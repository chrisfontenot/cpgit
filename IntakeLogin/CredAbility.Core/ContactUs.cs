﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core
{
    public class ContactUs
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PartnerProgram { get; set; }
        public string Comment { get; set; }
        public string Fullname
        {
            get { return this.Firstname+ " " + this.Lastname; }
        }
    }
}
