﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Core.Document
{
    public class DocumentDataTable : Dictionary<string, string>
    {
        public DocumentDataTable() { }

        public void PopulateViaCSVString(string documentData)
        {
            string[] data = documentData.Split(',');
            int i = 1;
            foreach (string s in data)
            {
                this.Add(i.ToString(), s);
                //string[] item = s.Split(':');
                //if (!this.Keys.Contains(item[0]))
                //{
                //    this.Add(i++.ToString(), item[0]item[1]);
                //}
                i++;
            }
        }
    }
}
