﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Document
{
    public class DocumentIndex
    {
        public DocumentIndex() { }

        public string ID { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string Url { get; set; }
    }
}
