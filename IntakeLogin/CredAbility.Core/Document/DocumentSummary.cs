﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Document
{
    public class DocumentSummary
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string DocumentUrl { get; set; }

        public DocumentSummary()
        {
            this.Date = DateTime.Now;
        }
    }
}
