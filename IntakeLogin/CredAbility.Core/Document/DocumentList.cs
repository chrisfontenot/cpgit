﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Document
{
    public class DocumentList : List<DocumentIndex>
    {
        public const string MYDEBTS_DOCUMENTTYPE = "mydebt.pdf";
        public const string MYCREDITSCORE_DOCUMENTTYPE = "mycreditscore.htm";
        public const string MYRESOURCES_DOCUMENTTYPE = "myresources.htm";
		public const string MYCERTIFICATE_DOCUMENTTYPE = "mycertificate.pdf";
		public const string MYACTIONPLAN_DOCUMENTTYPE = "myactionplan.pdf";

        private DocumentIndex GetDocumentIndex(string documentType)
        {
            return this.Find(DocIndex => DocIndex.Type.ToLower() == documentType);
        }

        public DocumentIndex GetLatestCounselingSummary()
        {
            // ignore all the ones that start with 'certificate', order by descending date, and get the first one
            return this.FindAll(DocIndex => !DocIndex.Type.ToLower().StartsWith("certificate")).OrderByDescending(DocIndex => DocIndex.Date).First();
        }

        public List<DocumentIndex> GetAllCounselingSummaries()
        {
            // ignore all the ones that start with 'certificate'
            return this.FindAll(DocIndex => !DocIndex.Type.ToLower().StartsWith("certificate"));
        }

        public List<DocumentIndex> GetAllCertificates()
        {
            // get only the ones that start with 'certificate'
            return this.FindAll(DocIndex => DocIndex.Type.ToLower().StartsWith(MYCERTIFICATE_DOCUMENTTYPE));
        }

        private DocumentSummary GetDocumentSummary(string documentType)
        {
            DocumentIndex di = this.GetDocumentIndex(documentType);
            DocumentSummary document = null; 
            if (di != null)
            {
                document = new DocumentSummary();
                document.Date = DateTime.Now.AddDays(-40);
                document.Description = di.Type;
                document.DocumentUrl = di.Url;
                document.Id = di.ID;
            }
            return document;
        }

        public DocumentSummary GetMyCreditScoreDocument()
        {
            return GetDocumentSummary(MYCREDITSCORE_DOCUMENTTYPE);
        }

        public DocumentSummary GetMyCertificateDocument()
        {
            return GetDocumentSummary(MYCERTIFICATE_DOCUMENTTYPE);
        }

        public DocumentSummary GetMyResourcesDocument()
        {
            return GetDocumentSummary(MYRESOURCES_DOCUMENTTYPE);
        }

        public DocumentSummary GetMyDebtsDocument()
        {
            return GetDocumentSummary(MYDEBTS_DOCUMENTTYPE);
        }
    }
}