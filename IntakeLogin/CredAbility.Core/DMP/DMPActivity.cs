﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.DMP
{
    public class DMPActivity
    {
        public DMPActivity() { }

        //<Transactions>
        //<Activity>
        //<ActivityDate>01/07/2010</ActivityDate>
        //<ActivityType>Disbursement</ActivityType>
        //<ActivityAmount>1109.00</ActivityAmount>
        //</Activity>
        //<Activity>
        //<ActivityDate>01/04/2010</ActivityDate>
        //<ActivityType>Autodraft</ActivityType>
        //<ActivityAmount>1109.00</ActivityAmount>
        //</Activity>
        //</Transactions>

        private DateTime _activityDate;
        public DateTime ActivityDate
        {
            get { return this._activityDate; }
            set { this._activityDate = value; }
        }

        private string _activityType;
        public string ActivityType
        {
            get { return this._activityType; }
            set { this._activityType = value; }
        }

        private decimal _activityAmount;
        public decimal ActivityAmount
        {
            get { return this._activityAmount; }
            set { this._activityAmount = value; }
        }

        public string PrettyPrint()
        {
            string value = "";

            value += "<table border=\"1\">";
            foreach (System.Reflection.PropertyInfo info in this.GetType().GetProperties())
            {
                if (info.CanRead)
                {
                    value += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", info.Name, info.GetValue(this, null));
                }
            }
            value += "</table>";
            
            return value;
        }
    }
}
