﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.DMP
{
    public class DMPStatus
    {
        public DMPStatus() { }

        //<DmpOrignalAmount>0</DmpOrignalAmount><DmpTotalPTD>0</DmpTotalPTD><DmpAutoPay></DmpAutoPay><DmpStartDate></DmpStartDate><DmpNextDueDate></DmpNextDueDate><DmpAmountDue></DmpAmountDue><Transactions />"

        public decimal DmpOriginalAmount { get; set; }

        public decimal DmpTotalPTD { get; set; }

        public string DmpAutoPay { get; set; }

        public DateTime DmpStartDate { get; set; }

        public DateTime? DmpNextDueDate { get; set; }

        public decimal DmpAmountDue { get; set; }

        public decimal TotalDebtAmount { get; set; }

        public int? DaysTilNextPaymentDue
        {
            get
            {
                if (DmpNextDueDate.HasValue)
                {
                    return Convert.ToInt32((DmpNextDueDate.Value - DateTime.Now).TotalDays);
                }else
                {
                    return null;
                }
            }
        }

        public string PrettyPrint()
        {
            string value = "";

            value += "<table border=\"1\">";
            foreach (System.Reflection.PropertyInfo info in this.GetType().GetProperties())
            {
                if (info.CanRead)
                {
                    value += string.Format("<tr><td>{0}</td><td>{1}</td></tr>", info.Name, info.GetValue(this, null));
                }
            }
            value += "</table>";
            
            return value;
        }
    }
}
