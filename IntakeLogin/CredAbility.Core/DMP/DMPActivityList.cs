﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.DMP
{
    public class DMPActivityList : List<DMPActivity>
    {
        public DMPActivityList() { }

        public string PrettyPrint()
        {
            string value = "";
            foreach (DMPActivity dmpActivity in this)
            {
                value += dmpActivity.PrettyPrint();
            }
            return value;
        }
    }
}
