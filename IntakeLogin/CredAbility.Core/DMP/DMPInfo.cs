﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Core.DMP
{
    public class DMPInfo
    {
        public DMPInfo() { }

        public DMPStatus DmpStatus { get; set; }

        public DMPActivityList DmpActivityList { get; set; }

        public bool IsSuccessful { get; set; }

        public List<ResultMessage> ResultMessages { get; set; }

        public string PrettyPrint()
        {
            string value = "";
            value += this.DmpStatus.PrettyPrint();
            value += "<BR><BR>";
            value += this.DmpActivityList.PrettyPrint();
            return value;
        }
    }
}
