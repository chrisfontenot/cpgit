﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.Rate
{
    public class ItemRate : IComparable
    {
        public ItemRate()
        {
            TotalRate = 0;
            RawAverage = 0;
            LastUpdated = DateTime.Now;
        }

        public Guid ItemID { get; set; }
        public int TotalRate { get; set; }
        public decimal RawAverage { get; set; }
        public DateTime LastUpdated { get; set; }
        public decimal Average { 
            get{
                var entier = Math.Floor(RawAverage); 
                var rest = RawAverage - entier; 

                if(rest >= 0.251m && rest < 0.750m)
                {
                    entier = entier + 0.5m;
                }
                else if(rest >= 0.751m)
                {
                    entier = entier + 1;
                }
                
                return entier;
            }
        }

        public void AddRate(string newRate)
        {
            AddRate(Convert.ToInt32(newRate));
        }

        public void AddRate(int newRate)
        {
            decimal score = (RawAverage*TotalRate) + newRate;
            
            RawAverage = score/(TotalRate + 1);
            TotalRate++;
        }

        #region Implementation of IComparable

        public int CompareTo(object obj)
        {
            if(typeof(ItemRate) != obj.GetType())
            {
                throw new ArgumentException("Object is not a ItemRate");
            }

            ItemRate casted = (ItemRate) obj;
            if (this.Average > casted.Average)
            {
                return 1;
            }
            if (this.Average < casted.Average)
            {
                return -1;
            }


            if(this.TotalRate > casted.TotalRate)
            {
                return 1;
            }
            if (this.TotalRate < casted.TotalRate)
            {
                return -1;
            }

            return 0;
        }

        #endregion
    }
}
