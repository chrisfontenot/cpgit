﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Core.MyGoal
{
    public class Goal
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public GoalType Type { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime DateCreated { get; set; }
        public List<GoalHistory> History { get; set; }

        public decimal AmountUptoDate
        {
            get
            {
                return History.Sum(h => h.Amount);
            }
        }

        public decimal RemainingAmount
        {
            get
            {
                return TotalAmount - History.Sum(h => h.Amount);
            }
        }
    }

    public enum GoalType
    {
        Save = 1,
        PayOff = 2
    }
}
