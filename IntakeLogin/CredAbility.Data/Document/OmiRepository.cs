﻿using System.Collections.Generic;
using CredAbility.Core.Document;
using CredAbility.Core.Client;
using System.Configuration;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;

namespace CredAbility.Data.Document
{
	public class OmiRepository:IPrefilingDocResolver
	{
		public IList<Certificate> GetCertificates(ClientProfile clientProfile)
		{
			return OmiDocuments.GetCertificates(clientProfile);
		}

		public string GetDocumentTokenId(ClientProfile clientProfile)
		{
			string documentTokenId = null;

			//todo : move that config to creadbility.config
			// authenticate user with the document web service
			var response = OmiDocuments.AuthenticateUser(clientProfile.DocumentAccessID
				, ConfigurationSettings.AppSettings["Services.Document.GUID"]);
			if(!response.HasError)
			{
				documentTokenId = response.ResponseValue;
			}
			return documentTokenId;
		}
		public DocumentList GetClientDocuments(ClientProfile clientProfile)
		{
			var documentList = new DocumentList();
			if(string.IsNullOrEmpty(clientProfile.DocumentAccessID))
				return documentList; 
			var documentTokenId = clientProfile.DocumentTokenID;
			if(documentTokenId == null)
			{
				//todo : move that config to creadbility.config
				// authenticate user with the document web service
				var response = OmiDocuments.AuthenticateUser(clientProfile.DocumentAccessID
					, ConfigurationSettings.AppSettings["Services.Document.GUID"]);
				if(response.HasError)
				{
					// error messaging
				}
				else
				{
					documentTokenId = response.ResponseValue;
					clientProfile.DocumentTokenID = documentTokenId;
				}
			}

			var docListresponse = OmiDocuments.GetClientDocumentList(
				documentTokenId, clientProfile.DocumentAccessID);

			if(docListresponse.HasError)
			{
				// error messaging
			}
			else
			{
				var docList = docListresponse.ResponseValue;
				foreach(var docIndex in docList)
				{
					var docSetsResponse = OmiDocuments.GetClientDocumentSets(documentTokenId, clientProfile.DocumentAccessID, docIndex.ID);

					if(docSetsResponse.HasError)
					{
						// error messaging
					}
					else
					{
						var docSetNames = docSetsResponse.ResponseValue.Split(',');
						foreach(var docSetName in docSetNames)
						{
							if(docSetName == "NONE") continue;
							var docResponse = OmiDocuments.GetClientDocument(documentTokenId, clientProfile.DocumentAccessID, docSetName, docIndex.ID);
							var cdi = docResponse.ResponseValue;
							var docURL = cdi.DocumentSetUrl + "?FileID=" + cdi.FileID;
							var di = new DocumentIndex
							{
								ID = docIndex.ID,
								Type = docSetName.ToLower(),
								Url = docURL,
								Date = docIndex.Date
							};
							documentList.Add(di);
						}
					}
				}
			}
			return documentList;
		}

		public ClientCounselingSummaryInfo GetClientCounselingSummaryInfo(ClientProfile clientProfile)
		{
			if(string.IsNullOrEmpty(clientProfile.DocumentAccessID))
			{
				return new ClientCounselingSummaryInfo { IsSuccessful = false };
			}

			var info = new ClientCounselingSummaryInfo { IsSuccessful = true };
			var documentTokenId = clientProfile.DocumentTokenID;

			if(string.IsNullOrEmpty(documentTokenId))
			{
				//todo : move that config to creadbility.config
				// authenticate user with the document web service
				var response = OmiDocuments.AuthenticateUser(clientProfile.DocumentAccessID
					, ConfigurationSettings.AppSettings["Services.Document.GUID"]);
				if(response.HasError)
				{
					info.IsSuccessful = false;
					info.ResultMessages.Add(new ResultMessage(response.ErrorMessage, response.ErrorCode));
				}
				else
				{
					documentTokenId = response.ResponseValue;
					clientProfile.DocumentTokenID = documentTokenId;
				}
			}

			var docId = clientProfile.DocumentID;
			if(string.IsNullOrEmpty(docId))
			{
				var docListResponse = OmiDocuments.GetClientDocumentList(documentTokenId
					, clientProfile.DocumentAccessID);

				if(docListResponse.HasError)
				{
					info.IsSuccessful = false;
					info.ResultMessages.Add(new ResultMessage(docListResponse.ErrorMessage, docListResponse.ErrorCode));
				}
				else
				{
					var docList = docListResponse.ResponseValue;
					if(docList != null)
					{
						DocumentIndex di = docList.GetLatestCounselingSummary();
						docId = di.ID;
						clientProfile.DocumentID = docId;
					}
				}
			}

			// get document data
			if(!string.IsNullOrEmpty(docId))
			{
				var docDataResponse = OmiDocuments.GetClientDocumentData(documentTokenId
					, clientProfile.DocumentAccessID, docId);

				var documentData = docDataResponse.ResponseValue;

				if(!string.IsNullOrEmpty(documentData))
				{
					var mapper = new CounselingSummaryMapper();
					info = mapper.Map(documentData, clientProfile.ClientID);
				}
				else
				{
					info.IsSuccessful = false;
				}
			}
			return info;
		}

		public List<DocumentSummary> GetPastSummaryList(ClientProfile clientProfile)
		{
			return OmiDocuments.GetPastSummaryList(clientProfile);
		}

		public string GetDocumentUrl(ClientProfile clientProfile, string docId)
		{
			return OmiDocuments.GetDocumentUrl(clientProfile, docId);
		}
	}
}