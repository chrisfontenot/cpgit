﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CredAbility.Core.Common;
using CredAbility.Core.Client;
using CredAbility.Core.Document;
using CredAbility.Data.Identity;
using CredAbility.Data.Services.Document;
using System.Configuration;

namespace CredAbility.Data.Document
{
	//NWORTH : TotalAssets - TotalLiabilities
	//TOTExpenses: HousingTotal + TransportationTotal + FoodTotal + OtherTotal
	//TOTDTPayment : Total Of DMP Payment
	//CASHMONEND: (BudgetIncome - TOTExpenses) - TOTDTPayment

	public static class OmiDocuments
	{
		private const string DOCUMENT_ENDPOINT_CONFIGURATION_NAME = "DocumentServicesSoap";

		//string GUID = "";

		//    if (ConfigurationSettings.AppSettings["Services.Document.GUID"] != null)
		//    {
		//        GUID = ConfigurationSettings.AppSettings["Services.Document.GUID"].ToString();
		//    }

		public static CustomResponse<string> AuthenticateUser(string clientID, string GUID)
		{
			var response = new CustomResponse<string>();

			//System.ServiceModel.BasicHttpBinding b = new System.ServiceModel.BasicHttpBinding();
			//System.ServiceModel.EndpointAddress ea = new System.ServiceModel.EndpointAddress("http://indevelop.statement2web.net/cccsv3a/SignOn.asmx");
			//SignOnSoapClient serviceClient = new SignOnSoapClient(b, ea);
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);

			//AuthenticateUserRequest1 request = new AuthenticateUserRequest1();
			//request.myClientID = "1111120";
			//request.myGUID = "testguidcredability";
			//request.myEmail = "support@outsourcemanagement.com";
			//request.mySessionID = "CredAbilitySessionId";
			//request.myRequestDateTime = "3/16/2010 12:00:00";
			//request.myReturnURL = "http://www.outsourcemanagement.com";
			//request.myErrorURL = "http://www.outsourcemanagement.com";
			//request.myExtendTimeURL = "http://www.outsourcemanagement.com";

			SSOReturn serviceResponse = serviceClient.AuthenticateUser(GUID, clientID
				, "support@outsourcemanagement.com", "CredAbilitySessionId"
				, DateTime.Now.ToString(), "http://www.outsourcemanagement.com"
				, "http://www.outsourcemanagement.com", "http://www.outsourcemanagement.com"
				, String.Empty, String.Empty, String.Empty);
			serviceClient.Close();
			response.HasError = serviceResponse.IsError;
			if(!response.HasError)
			{
				response.ResponseValue = serviceResponse.TokenID;
			}
			else
			{
				response.ErrorMessage = serviceResponse.ErrorMessage;
			}
			return response;
		}

		public static CustomResponse<DocumentList> GetClientDocumentList(string myTokenID, string myClientID)
		{
			var response = new CustomResponse<DocumentList>();
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);
			ClientDocListInfo docList = serviceClient.GetClientDocumentList(myTokenID, myClientID);
			serviceClient.Close();
			if((docList.ErrorCode == "0") && (docList.DocumentInfo != null) && (docList.DocumentInfo.Length > 0))
			{
				DocumentList documentList = new DocumentList();
				object[] docObject = docList.DocumentInfo;
				int i = 0;
				while(i < docObject.Length)
				{
					DocumentIndex di = new DocumentIndex();
					di.ID = docObject[i].ToString();
					di.Type = docObject[i + 1].ToString();
					di.Date = DateTime.Parse(docObject[i + 2].ToString(), new CultureInfo("en-us"));
					i = i + 3;
					documentList.Add(di);
				}
				response.ResponseValue = documentList;
			}
			else
			{
				response.HasError = true;
				response.ErrorCode = docList.ErrorCode;
				response.ErrorMessage = docList.ErrorMessage;
			}
			return response;
		}

		public static CustomResponse<string> GetClientDocumentData(string myTokenID, string myClientID, string myDocID)
		{
			var response = new CustomResponse<string>();
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);
			ClientDocDataInfo dataInfo = serviceClient.GetClientDocumentData(myTokenID, myClientID, myDocID);
			serviceClient.Close();
			if(dataInfo.ErrorCode == "0")
			{
				response.ResponseValue = dataInfo.ClientDocumentData;
			}
			else
			{
				response.HasError = true;
				response.ErrorCode = dataInfo.ErrorCode;
				response.ErrorMessage = dataInfo.ErrorMessage;
			}
			return response;
		}

		public static CustomResponse<ClientDocInfo> GetClientDocument(string myTokenID, string myClientID, string myDocumentSetName, string myDocumentID)
		{
			var response = new CustomResponse<ClientDocInfo>();
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);
			ClientDocInfo docInfo = serviceClient.GetClientDocument(myTokenID, myClientID, myDocumentSetName, myDocumentID);
			serviceClient.Close();
			if(docInfo.ErrorCode == "0")
			{
				response.ResponseValue = docInfo;
			}
			else
			{
				response.HasError = true;
				response.ErrorCode = docInfo.ErrorCode;
				response.ErrorMessage = docInfo.ErrorMessage;
			}
			return response;
		}

		public static CustomResponse<string> GetClientDocumentSets(string myTokenID, string myClientID, string myDocID)
		{
			var response = new CustomResponse<string>();
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);
			ClientDocSetInfo docSetInfo = serviceClient.GetClientDocumentSets(myTokenID, myClientID, myDocID);
			serviceClient.Close();
			if(docSetInfo.ErrorCode == "0")
			{
				string value = String.Empty;
				foreach(string s in docSetInfo.DocumentSets)
				{
					value += s;
				}
				response.ResponseValue = value;
			}
			else
			{
				response.HasError = true;
				response.ErrorCode = docSetInfo.ErrorCode;
				response.ErrorMessage = docSetInfo.ErrorMessage;
			}
			return response;
		}

		public static CustomResponse<string> SignOffUser(string myTokenID, string myClientID)
		{
			var response = new CustomResponse<string>();
			DocumentServicesSoapClient serviceClient = new DocumentServicesSoapClient(DOCUMENT_ENDPOINT_CONFIGURATION_NAME);
			ClientSignOffInfo signOffInfo = serviceClient.SignOffUser(myTokenID, myClientID);
			serviceClient.Close();
			if(signOffInfo.ErrorCode == "0")
			{
				response.ResponseValue = signOffInfo.ConfirmationMessage;
			}
			else
			{
				response.HasError = true;
				response.ErrorCode = signOffInfo.ErrorCode;
				response.ErrorMessage = signOffInfo.ErrorMessage;
			}
			return response;
		}

		public static string GetDocumentUrl(ClientProfile clientProfile, string docId)
		{
			var docListresponse = OmiDocuments.GetClientDocumentList(clientProfile.DocumentTokenID, clientProfile.DocumentAccessID);

			if(docListresponse.HasError)
			{
				// error messaging
			}
			else
			{
				var docList = docListresponse.ResponseValue;
				string docSetName = String.Empty;
				if(docList.Count > 0)
				{
					docSetName = docList[0].ID + ".pdf";
					var docSetResponse = OmiDocuments.GetClientDocument(clientProfile.DocumentTokenID, clientProfile.DocumentAccessID, docSetName, docList[0].ID);
					return docSetResponse.ResponseValue.DocumentSetUrl + "?FileId=" + docSetResponse.ResponseValue.FileID;
				}
			}
			return String.Empty;
		}

		public static List<DocumentSummary> GetPastSummaryList(ClientProfile clientProfile)
		{
			var documents = new List<DocumentSummary>();
			if(string.IsNullOrEmpty(clientProfile.DocumentTokenID))
			{
				clientProfile.DocumentTokenID = AuthenticateUser(clientProfile.DocumentAccessID
					, ConfigurationSettings.AppSettings["Services.Document.GUID"]).ResponseValue;
			}
			var docListresponse = OmiDocuments.GetClientDocumentList(clientProfile.DocumentTokenID, clientProfile.DocumentAccessID);

			if(docListresponse.HasError)
			{
				// error messaging
			}
			else
			{
				var docList = docListresponse.ResponseValue.GetAllCounselingSummaries();

				foreach(DocumentIndex di in docList)
				{
					var docSetName = di.ID + ".pdf";
					var docSetResponse = OmiDocuments.GetClientDocument(clientProfile.DocumentTokenID, clientProfile.DocumentAccessID, docSetName, di.ID);
					DocumentSummary docSummary = new DocumentSummary();
					docSummary.Id = di.ID;
					docSummary.Date = di.Date;
					docSummary.Description = di.Type;
					docSummary.DocumentUrl = docSetResponse.ResponseValue.DocumentSetUrl + "?FileId=" + docSetResponse.ResponseValue.FileID;
					documents.Add(docSummary);
				}
			}
			return documents;
		}

		//note : not sure this method is at the right place, should be in ClientProfileRepository?
		public static List<Certificate> GetCertificates(ClientProfile clientProfile)
		{
			var certificates = new List<Certificate>();
			OmiRepository OmiRep = new OmiRepository();
			foreach(DocumentIndex di in OmiRep.GetClientDocuments(clientProfile).GetAllCertificates())
			{
				Certificate c = new Certificate();
				c.Date = di.Date;
				c.Id = di.ID;
				c.Url = di.Url;
				c.HasPdfVersion = true;
				c.Name = "MyCertificate.PDF";
				certificates.Add(c);
			}
			return certificates;
		}
	}
}
