﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Document;
using CredAbility.Core.Client;
using CredAbility.Core.ActionPlan;
using CredAbility.Data.ActionPlan;
using System.Data;

namespace CredAbility.Data.Document
{
	public class CredAbilityRepository:IPrefilingDocResolver
	{
		public IList<Certificate> GetCertificates(CredAbility.Core.Client.ClientProfile clientProfile)
		{
			List<Certificate> CertList = new List<Certificate>();
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				var HoldCert = IdmSys.GetCertificates(UserId);
				foreach(CredAbility.Data.Services.Identity.Certificate CurCert in HoldCert)
				{
					Certificate MoveCert = new Certificate();
					MoveCert.Date = CurCert.Date;
					MoveCert.Description = String.Empty;//CurCert.Description;
					MoveCert.HasExpired = CurCert.HasExpired;
					MoveCert.HasPdfVersion = CurCert.HasPdfVersion;
					MoveCert.Id = CurCert.Id;
					MoveCert.Name = DocumentList.MYCERTIFICATE_DOCUMENTTYPE.Replace("myc","MyC");
					MoveCert.Url = String.Format("/my-account/DisplayDocument.aspx?Id={0}", CurCert.Id);
					CertList.Add(MoveCert);
				}
			}
			return CertList;
		}

		public DocumentList GetClientDocuments(CredAbility.Core.Client.ClientProfile clientProfile)
		{
			DocumentList DocList = new DocumentList();
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				var HoldDoc = IdmSys.GetClientDocuments(UserId);
				foreach(CredAbility.Data.Services.Identity.DocumentIndex CurDoc in HoldDoc)
				{
					DocumentIndex MoveDoc = new DocumentIndex();
					MoveDoc.Date = CurDoc.Date;
					MoveDoc.ID = CurDoc.ID;
					MoveDoc.Type = CurDoc.Type;
					MoveDoc.Url = String.Format("/my-account/DisplayDocument.aspx?Id={0}", CurDoc.ID);
					DocList.Add(MoveDoc);
				}
			}
			return DocList;
		}

		public ClientCounselingSummaryInfo GetClientCounselingSummaryInfo(CredAbility.Core.Client.ClientProfile clientProfile)
		{
			ClientCounselingSummaryInfo SumInfo = new ClientCounselingSummaryInfo();
			SumInfo.IsSuccessful = false;
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				CredAbility.Data.Services.Identity.Snapshot HoldDoc;
				long DocId;
				try
				{
					if(long.TryParse(clientProfile.DocumentID, out DocId))
					{
						HoldDoc = IdmSys.GetSnapshot(DocId);
					}
					else
					{
						HoldDoc = IdmSys.GetCurrentSnapshot(UserId);
					}
				}
				catch(Exception)
				{
					return SumInfo;
				}
				if(HoldDoc != null)
				{
					SumInfo.AsOfDate = HoldDoc.CourseCompleted;
					SumInfo.ActionItems = GenrateActionItems(clientProfile.ClientID);
					clientProfile.DocumentID = HoldDoc.PreFilingActionPlanId.ToString();

					SumInfo.PersonalAssetsInfo.TotalIncome = HoldDoc.IncomeAfterTaxes;
					SumInfo.PersonalAssetsInfo.TotalExpenses = HoldDoc.LivingExpenses;
					SumInfo.PersonalAssetsInfo.TotalDebtPayment = HoldDoc.DebtPayment;
					SumInfo.PersonalAssetsInfo.CashAmountMonthEnd = HoldDoc.CashAtMonthEnd;
					SumInfo.PersonalAssetsInfo.TotalAssets = HoldDoc.TotalAssets;
					SumInfo.PersonalAssetsInfo.TotalLiabilities = HoldDoc.TotalLiabilities;
					SumInfo.PersonalAssetsInfo.TotalNetWorth = HoldDoc.NetWorth;
					SumInfo.Demographics.CreditScore = HoldDoc.CreditScore;
					SumInfo.ExpenseInfo.TotalIncome = HoldDoc.IncomeAfterTaxes;
					SumInfo.PersonalAssetsInfo.TotalAssets = HoldDoc.TotalAssets;
					SumInfo.PersonalAssetsInfo.TotalLiabilities = HoldDoc.TotalLiabilities;
					SumInfo.PersonalAssetsInfo.TotalNetWorth = HoldDoc.NetWorth;

					SumInfo.ExpenseInfo.IncomeAndExpenseRawData = new DataTable();
					SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Columns.Add("TOTALNINCOME", typeof(string));
					SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETCODE", typeof(string));
					SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETLINE", typeof(string));
					SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETAMOUNT", typeof(string));
					SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETPERCENT", typeof(string));

					SumInfo.ExpenseInfo.ExpenseCategories = new List<ExpenseCategory>();
					foreach(var CurExpCat in HoldDoc.ExpenseCategories)
					{
						ExpenseCategory NewExpCat = new ExpenseCategory();
						NewExpCat.Id = CurExpCat.ExpenseCategoryId.ToString();
						NewExpCat.Name = CurExpCat.DisplayText;
						NewExpCat.Percentage = CurExpCat.PercentOfTotal;
						SumInfo.ExpenseInfo.ExpenseCategories.Add(NewExpCat);
					}
					
					foreach(var ExpCode in HoldDoc.ExpenseCodes)
					{
						ExpenseItem ExpItm = new ExpenseItem(ExpCode.DisplayText, ExpCode.SessionAmount);
						int i = SumInfo.ExpenseInfo.ExpenseCategories.FindIndex(Item => Item.Id == ExpCode.ExpenseCategoryId.ToString());
						if(i >= 0)
						{
							SumInfo.ExpenseInfo.ExpenseCategories[i].ExpenseItems.Add(ExpItm);
							SumInfo.ExpenseInfo.IncomeAndExpenseRawData.Rows.Add(HoldDoc.IncomeAfterTaxes
								, ExpCode.ExpenseCategoryId, ExpCode.DisplayText, ExpCode.SessionAmount
								, SumInfo.ExpenseInfo.ExpenseCategories[i].Percentage);
						}
					}
					SumInfo.IsSuccessful = true;
				}
			}
			return SumInfo;
		}

		public string GetDocumentTokenId(CredAbility.Core.Client.ClientProfile clientProfile)
		{
			return String.Empty;
		}

		public List<DocumentSummary> GetPastSummaryList(CredAbility.Core.Client.ClientProfile clientProfile)
		{
			List<DocumentSummary> DocList = new List<DocumentSummary>();
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				var HoldDoc = IdmSys.GetClientDocuments(UserId);
				if(HoldDoc != null)
				{
					var FilterDoc = HoldDoc.Where(ActPln => ActPln.Type.ToLower() == DocumentList.MYACTIONPLAN_DOCUMENTTYPE);
					foreach(CredAbility.Data.Services.Identity.DocumentIndex CurDoc in FilterDoc)
					{
						DocumentSummary MoveDoc = new DocumentSummary();
						MoveDoc.Id = CurDoc.ID;
						MoveDoc.Date = CurDoc.Date;
						MoveDoc.Description = String.Empty;
						MoveDoc.DocumentUrl = String.Format("/my-account/DisplayDocument.aspx?Id={0}", CurDoc.ID);
						DocList.Add(MoveDoc);
					}
				}
			}
			return DocList;
		}

		public string GetDocumentUrl(ClientProfile clientProfile, string docId)
		{
			string DocUrl = String.Empty;
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				var HoldDoc = IdmSys.GetClientDocuments(UserId);
				if(HoldDoc != null)
				{
					var CurDoc = HoldDoc.Where(Doc => Doc.ID == docId).FirstOrDefault();
					if(CurDoc != null)
					{
						DocUrl = String.Format("/my-account/DisplayDocument.aspx?Id={0}", CurDoc.ID);
					}
				}
			}
			return DocUrl;
		}

		public byte[] DisplayDocument(ClientProfile clientProfile, string DocId)
		{
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			long UserId;
			if(long.TryParse(clientProfile.ClientID, out UserId))
			{
				var HoldDoc = IdmSys.GetClientDocuments(UserId);
				var SelDoc = HoldDoc.Where(CurDoc => CurDoc.ID == DocId).FirstOrDefault();
				if(SelDoc != null)
				{
					switch(SelDoc.Type.ToLower())
					{
						case DocumentList.MYACTIONPLAN_DOCUMENTTYPE:
							string CertNo = GetCertNo(UserId, DocId);
							return IdmSys.DisplayActionPlan(CertNo);
							break;

						case DocumentList.MYCERTIFICATE_DOCUMENTTYPE:
							return IdmSys.DisplayCertificate(SelDoc.ID);
							break;

						case DocumentList.MYDEBTS_DOCUMENTTYPE:
							long ActPlanId;
							if(long.TryParse(DocId.Remove(0, 1), out ActPlanId))
							{
								return IdmSys.DisplayDebtAnalysis(ActPlanId);
							}
							break;
					}
				}
			}
			return null;
		}

		private List<ActionItem> GenrateActionItems(string UserId)
		{
			List<ActionItem> ActLst = new List<ActionItem>();
			//Builds a generic list of action plan items
			//will need to revisit this.
			ActionItem Item = new ActionItem();
			Item.Title = "Set Your Personal Goals";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,1);
			Item.Id = 1;
			Item.Text = "Complete the section on \"Your Goals\" in this package.";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Set Your Personal Goals";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,2);
			Item.Id = 2;
			Item.Text = "Find a prominent place to post your goals in your home as a reminder";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Set Your Personal Goals";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,3);
			Item.Id = 3;
			Item.Text = "Share your goals with a trusted friend and ask him or her to help you act consistently so that you can achieve them";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,4);
			Item.Id = 4;
			Item.Text = "Review your income and expenses carefully to find ways to get your budget in balance";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,5);
			Item.Id = 5;
			Item.Text = "Track your expenses on a daily basis to identify where you may be able to cut back";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,6);
			Item.Id = 6;
			Item.Text = "For good basic money management information, check out the Money Smarts section of our website at CredAbility.org";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,7);
			Item.Id = 7;
			Item.Text = "Remember that your mortgage or rent, car and utilities are priority debts. Create a budget that reflects these priorities.";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Protect Your Home";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,8);
			Item.Id = 8;
			Item.Text = "Your first and most important monthly payment is your house payment. Always pay your mortgage/rent first";
			ActLst.Add(Item);
			//Item = new ActionItem();
			//Item.Title = "Protect Your Home";
			//Item.Id = 9;
			//Item.Text = "If you have fallen behind on your mortgage payments, it is important to know that there are options available to help you save your home and avoid foreclosure. Contact the mortgage company to review your options and identify the one that will work best for you.";
			//ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,10);
			Item.Id = 10;
			Item.Text = "Ask an attorney any bankruptcy-related questions you may have. Your counselor is not a lawyer and cannot advise you about whether bankruptcy is the right solution to your financial problem";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,11);
			Item.Id = 11;
			Item.Text = "Remember that if you file for bankruptcy, a mandatory education course is required before your bankruptcy can be discharged. CredAbility provides this service. You may contact us at 866.672.2227 or at CredAbility.org";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,14);
			Item.Id = 14;
			Item.Text = "Contact your lender to explore options for catching up on past due car payments";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,15);
			Item.Id = 15;
			Item.Text = "Contact your creditors directly to request a hardship program or negotiate an alternate repayment plan";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,17);
			Item.Id = 17;
			Item.Text = "Until your finances are back on track, it is important that you stop using credit";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Understand Your Credit Score and Report";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,18);
			Item.Id = 18;
			Item.Text = "Obtain free copies of your credit report at www.annualcreditreport.com";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Understand Your Credit Score and Report";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,19);
			Item.Id = 19;
			Item.Text = "Review and make certain all your information is accurate";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Set Your Personal Goals";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,21);
			Item.Id = 21;
			Item.Text = "For good basic money management information, visit our online education center at www.credability.org";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Set Your Personal Goals";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,22);
			Item.Id = 22;
			Item.Text = "Register for the online class.. Where do I want to be in 5 years";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,23);
			Item.Id = 23;
			Item.Text = "Register for the online class.. Where does my money go";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,24);
			Item.Id = 24;
			Item.Text = "Register for the online class.. Living on 70 cents";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Build a Priority Budget";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,25);
			Item.Id = 25;
			Item.Text = "Register for the online class.. Financial success starts here";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Decide How to Tackle Your Unsecured Debts";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,27);
			Item.Id = 27;
			Item.Text = "Register for the online class.. How do I get out debt";
			ActLst.Add(Item);
			Item = new ActionItem();
			Item.Title = "Understand Your Credit Score and Report";
			Item.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId,28);
			Item.Id = 28;
			Item.Text = "Register for our online class.. Why am I a 678";
			ActLst.Add(Item);
			return ActLst;
		}

		private string GetCertNo(long UserId, string DocId)
		{
			Services.Identity.IdentityClient IdmSys = new Services.Identity.IdentityClient();
			var SelDoc = IdmSys.GetClientDocuments(UserId).Where(CurDoc => CurDoc.ID == DocId).FirstOrDefault();
			if(SelDoc != null)
			{
				return SelDoc.Url;
			}
			return String.Empty;
		}
	}
}
