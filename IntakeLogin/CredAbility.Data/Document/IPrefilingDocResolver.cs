﻿using System.Collections.Generic;
using CredAbility.Core.Client;
using CredAbility.Core.Document;

namespace CredAbility.Data.Document
{
	public interface IPrefilingDocResolver
	{
		IList<Certificate> GetCertificates(ClientProfile clientProfile);
		DocumentList GetClientDocuments(ClientProfile clientProfile);
		ClientCounselingSummaryInfo GetClientCounselingSummaryInfo(ClientProfile clientProfile);
		string GetDocumentTokenId(ClientProfile clientProfile);
		List<DocumentSummary> GetPastSummaryList(ClientProfile ClientProfile);
		string GetDocumentUrl(ClientProfile clientProfile, string docId);
	}
}
