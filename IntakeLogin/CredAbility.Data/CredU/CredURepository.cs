﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CredAbility.Data.CredU
{
	public class CredURepository
	{
		public static long AddClass(string ZipCode,long? UserID, string ClassName,string ClassType
			, string Language, string RunTime)
		{
			long Back = 0;
			using(CredAbilityDataContext DataContext = new CredAbilityDataContext())
			{
				CredUClass NewClass = new CredUClass();
				NewClass.ClassName = ClassName;
				NewClass.ClassType = ClassType;
				NewClass.StudentZip = ZipCode;
				NewClass.UserId = UserID;
				NewClass.ClassStartDTS = DateTime.Now;
				NewClass.Language = Language;
				NewClass.RunTime = RunTime;
				DataContext.CredUClasses.InsertOnSubmit(NewClass);
				DataContext.SubmitChanges();
				Back = NewClass.ClassTakenID;
			}
			return Back;
		}

		public static void EndClass(long ClassTakenID)
		{
			using(CredAbilityDataContext DataContext = new CredAbilityDataContext())
			{
				CredUClass ClassRec = null;
				try
				{
					ClassRec =
					 (
						 from Classes in DataContext.CredUClasses
						 where Classes.ClassTakenID == ClassTakenID
						 select Classes
						).FirstOrDefault<CredUClass>();

					if(ClassRec != null)
					{
						ClassRec.ClassEndDTS = DateTime.Now;
						DataContext.SubmitChanges();
					}
				}
				catch(Exception)// exception)
				{
				}
			}
		}

		public static bool CheckZip(string ZipCode)
		{
			bool Back = false;
			using(CccsDataContext DataContext = new CccsDataContext())
			{
				zipcode ZipRec = null;
				try
				{
					ZipRec = 
					(
					from Zip in DataContext.zipcodes
					where Zip.id == ZipCode.Trim()
					select Zip
					).First<zipcode>();
					Back = ZipRec != null;
				}
				catch
				{
				}
			}
			return Back;
		}
	}
}
