﻿using System;
using System.Linq;
using System.Text;
using CredAbility.Core.CreditScoreChallenge;

namespace CredAbility.Data.CreditScoreChallenge
{
    public class ContestantRepository
    {
        
        /// <summary>
        /// Save the specified contestant into persistence system.
        /// </summary>
        /// <param name="contestant">The contestant.</param>
        public static void Save(Contestant contestant)
        {
            if (contestant == null)
            {
                throw new ArgumentNullException("contestant");
            }
            var dataContext = new CredAbilityDataContext();
            var challengeContestantEntity = CredAbilityEntityFactory.GetCreditScoreChallengeContestantEntity(contestant);
          
            dataContext.CreditScoreChallengeContestantEntities.InsertOnSubmit(challengeContestantEntity);

            dataContext.SubmitChanges();
        }

        /// <summary>
        /// Get the contestant by id from persistence system.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns></returns>
        public static Contestant GetContestantById(string clientId)
        {
            try
            {
                var dataContext = new CredAbilityDataContext();
                CreditScoreChallengeContestantEntity contestant = dataContext.CreditScoreChallengeContestantEntities.Single(t => t.ClientID == clientId);
                return CredAbilityEntityFactory.GetContestant(contestant);
            }
            catch(Exception)
            {
                return null;
            }
        }

    }
}
