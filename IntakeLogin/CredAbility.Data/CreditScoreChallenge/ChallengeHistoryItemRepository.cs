using System;
using System.Collections.Generic;
using System.Linq;
using CredAbility.Core.CreditScoreChallenge;

namespace CredAbility.Data.CreditScoreChallenge
{
    public class ChallengeHistoryItemRepository
    {
        

        public static List<Core.CreditScoreChallenge.ChallengeHistoryItem> GetChallengeHistoryItemById(string clientId)
        {         
            try
            {
                CredAbilityDataContext _dataContext = new CredAbilityDataContext();
                List<ChallengeHistoryItem> result = new List<ChallengeHistoryItem>();
                IQueryable<CreditScoreChallengeHistoryEntity> historyItems = _dataContext.CreditScoreChallengeHistoryEntities.Where(t => t.ClientId == clientId);

                foreach (CreditScoreChallengeHistoryEntity item in historyItems)
                {
                    result.Add(CredAbilityEntityFactory.GetChallengeHistoryItem(item));
                }

                return result;
            }
            catch (Exception)
            {
                return new List<ChallengeHistoryItem>();
            }
        }
    }
}