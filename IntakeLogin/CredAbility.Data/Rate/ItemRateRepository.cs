﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using CredAbility.Core.Rate;

namespace CredAbility.Data.Rate
{
    public static class ItemRateRepository
    {
        public static void Save(ItemRate itemRate)
        {
            if(itemRate == null)
            {
                throw new NullReferenceException("You cannot save a ItemRate.");
            }
            
            var dataContext = new CredAbilityDataContext();
            var ratedItem = new RatedItemEntity();

            try
            {
                // protect agaisnt an invalid ratedItemId
                ratedItem = dataContext.RatedItemEntities.Single(r => r.ItemID == itemRate.ItemID);
            }
            catch(Exception)
            {
                dataContext.RatedItemEntities.InsertOnSubmit(ratedItem); 
                ratedItem.ItemID = itemRate.ItemID;
            }
            
            ratedItem.TotalRate = itemRate.TotalRate;
            ratedItem.AverageRate = (double) itemRate.RawAverage;
            ratedItem.LastUpdate = DateTime.Now;

            dataContext.SubmitChanges();
        }

        public static ItemRate Get(Guid itemID)
        {
            try
            {
                var dataContext = new CredAbilityDataContext();
                var itemRate = new ItemRate();

                var ratedItem = dataContext.RatedItemEntities.Single(r => r.ItemID == itemID);
                itemRate.ItemID = ratedItem.ItemID;
                itemRate.TotalRate = ratedItem.TotalRate;
                itemRate.RawAverage = (decimal)ratedItem.AverageRate;
                itemRate.LastUpdated = ratedItem.LastUpdate;

                return itemRate;
            }
            catch(Exception)
            {
                return null;
            }
            
        }
    }
}
