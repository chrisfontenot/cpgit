using System.Collections.Generic;
using CredAbility.Core.Client;
using CredAbility.Data.Services.DMP;

namespace CredAbility.Data.Services.DMP
{
    public static class ResultUserInfoHelpers
    {
        public static Profile ToProfile(this ResultUserInfo resultUserInfo)
        {
            Profile profile = new Profile();

            profile.HostId = resultUserInfo.ClientNo;

            profile.FirstName = resultUserInfo.FirstName;
            profile.MiddleName = resultUserInfo.MiddleName;
            profile.LastName = resultUserInfo.LastName;
            profile.EmailAddress = resultUserInfo.eMail;
            profile.CellPhone = resultUserInfo.PhoneCell;
            profile.City = resultUserInfo.City;
            profile.HomePhone = resultUserInfo.PhoneHome;
            profile.MailingAddressLine1 = resultUserInfo.Addr1;
            profile.MailingAddressLine2 = resultUserInfo.Addr2;
            profile.MaritalStatus = resultUserInfo.MaritalStatusCode;
            profile.SSN = new SocialSecurityNumber(resultUserInfo.SSN);
            profile.State = resultUserInfo.State;
            profile.WorkPhone = resultUserInfo.PhoneWork;
            profile.Zip = resultUserInfo.ZipCode;

            if (resultUserInfo.AccountType == "CM")
            {
                profile.DMPAccessID = profile.HostId;
            }

            if ((resultUserInfo.AccountType == "CAM") ||
                (resultUserInfo.AccountType == "BR.CAM") ||
                (resultUserInfo.AccountType == "WS.CAM"))
            {
                profile.DocumentAccessID = profile.HostId;
            }

            //Transform the account into code value to be transformed for Sitecore in Virtual User
            profile.Types.Add(resultUserInfo.AccountType);
            
            profile.ClientAccounts = new List<ClientAccount>();
            profile.ClientAccounts.Add(new ClientAccount(resultUserInfo.AccountType, 0, resultUserInfo.ClientNo));

            return profile;
        }
    }
}