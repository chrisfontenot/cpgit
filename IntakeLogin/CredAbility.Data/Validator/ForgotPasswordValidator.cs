﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class ForgotPasswordValidator : Validator<string>
    {
        public override ActionResult Validate(string obj)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            actionResult.MergeActionResult(this.ValidateStringRequired(obj,"USERNAME-REQUIRED", Resources.ForgotPasswordValidator_UsernameRequired));
            return actionResult;
        }
    }
}
