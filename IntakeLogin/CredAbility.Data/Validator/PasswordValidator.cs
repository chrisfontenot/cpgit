﻿using System.Text.RegularExpressions;
using CredAbility.Core.Client;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class PasswordValidator : Validator<ClientProfile>
    {
        public override ActionResult Validate(ClientProfile clientProfile)
        {
            var actionResult = new ActionResult(true);
            actionResult.MergeActionResult(this.ValidatePassword(clientProfile));
            

            return actionResult;
        }

        private ActionResult ValidatePassword(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult(true);
            actionResult.MergeActionResult(this.ValidateStringRequired(profile.Password, "PASSWORD-REQUIRED", Resources.PasswordValidator_PasswordRequired));

            if (actionResult.IsSuccessful)
            {
                if (!this.IsRespectingPasswordRules(profile))
                {
                    actionResult.Messages.Add(new ResultMessage(Resources.PasswordValidator_InvalidPasswordFormat, "PASSWORD-WRONGFORMAT"));
                    actionResult.IsSuccessful = false;
                }
            }


            return actionResult;
        }

       


        private bool IsRespectingPasswordRules(ClientProfile profile)
        {
            string password = profile.Password;

            

            //may not contain the users first name, last name, or userid/username
            if ((!string.IsNullOrEmpty(profile.ClientID) && password.Contains(profile.ClientID)) || (!string.IsNullOrEmpty(profile.FirstName) && password.Contains(profile.FirstName)) || (!string.IsNullOrEmpty(profile.LastName) && password.Contains(profile.LastName)) || (!string.IsNullOrEmpty(profile.Username) && password.Contains(profile.Username)))
            {
                return false;
            }

            //May not contain the word "password".  
            if (password.Contains("password"))
            {
                return false;
            }

            



            //require alphabetic characters and at least 1 number, no symbols. + //Minimum Length: 8 characters + //Maximum Lenght: 20 characters

            //RegexInfo : Validates a strong password. It must be between 8 and 10 characters, contain at least one digit and one alphabetic character, and must not contain special characters.
            //URL: http://msdn.microsoft.com/en-us/library/ms998267.aspx
            // (?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$

            Regex regex = new Regex("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$");
            if (!regex.Match(password).Success)
            {
                return false;
            }

            //May not contain a set of repeated character (ie. AAAAAAA1)

            for (int i = 0; i < password.Length-2; i++ )
            {
                if (password[i] == password[i + 1] && password[i + 1] == password[i + 2])
                {
                    return false;
                }                
            }

            return true;
        }
    }
}