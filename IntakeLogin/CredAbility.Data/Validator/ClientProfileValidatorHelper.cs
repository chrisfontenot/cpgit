using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;

namespace CredAbility.Data.Validator
{
    public static class ClientProfileValidatorHelper
    {
        public static ActionResult ValidateUsername(Profile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.Username))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_UsernameRequired, "USERNAME-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

            if (!IdentityRepository.ConfirmValidUsername(profile.Username))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_UsernameUnavailable, "USERNAME-UNAVAILABLE"));
                actionResult.IsSuccessful = false;
            }


            return actionResult;
        }

        public static ActionResult ValidateSecurityQuestion(Profile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;




            if (string.IsNullOrEmpty(profile.SecurityQuestion))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityQuestionRequired, "QUESTION1-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

            

            return actionResult;
        }



        public static ActionResult ValidateSecurityAnswer(Profile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.SecurityAnswer))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityAnswerRequired, "ANSWER1-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

           

            return actionResult;
        }

    }
}