﻿using System.Text.RegularExpressions;
using CredAbility.Core.Client;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class ProfileValidator: Validator<Profile>
    {
       
        private const string SSN_REG_EX = @"^(?!000)([0-6]\d{2}|7([0-6]\d|7[012]))([ -]?)(?!00)\d\d\3(?!0000)\d{4}$";
        private const string EMAIL_REG_EX = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-z0-9-]*[a-zA-Z0-9])?";

        public override ActionResult Validate(Profile profile)
        {
            ActionResult result = new ActionResult(true);
            

            
            //result.MergeActionResult(this.ValidateSSN(profile));
            result.MergeActionResult(this.ValidateHomePhone(profile));
            
            result.MergeActionResult(this.ValidateMailingAddressLine1(profile));
            result.MergeActionResult(this.ValidateCity(profile));
            result.MergeActionResult(this.ValidateState(profile));
            result.MergeActionResult(this.ValidateZip(profile));

            result.MergeActionResult(this.ValidateFirstName(profile));
            result.MergeActionResult(this.ValidateLastName(profile));
            result.MergeActionResult(this.ValidateEmail(profile));
            result.MergeActionResult(this.ValidateSecurityQuestion(profile));
            result.MergeActionResult(this.ValidateSecurityAnswer(profile));

            return result;

        }


        private ActionResult ValidateSSN(Profile profile)
        {
            ActionResult result = new ActionResult(true);
            


            Regex regex = new Regex(SSN_REG_EX);
            if (!regex.Match(profile.SSN.FormatedSSN).Success)
            {
                result.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_InvalidSSNFormat, "ISSNF"));
                result.IsSuccessful = false;
            }

            result.MergeActionResult(this.ValidateRequiredValue(profile.SSN.FormatedSSN, Resources.ClientProfileValidator_SSN, "SSNR"));

            return result;

        }


        private ActionResult ValidateHomePhone(Profile profile)
        {
            ActionResult result = new ActionResult(true);
            result.MergeActionResult(this.ValidateRequiredValue(profile.HomePhone, Resources.ClientProfileValidator_HomePhone, "HPR"));
      

            return result;
        }

      

        private ActionResult ValidateMailingAddressLine1(Profile profile)
        {
            return this.ValidateRequiredValue(profile.MailingAddressLine1, Resources.ClientProfileValidator_MailingAddress1, "MA1R");
        }

        private ActionResult ValidateCity(Profile profile)
        {
            return this.ValidateRequiredValue(profile.City, Resources.ClientProfileValidator_City, "CR");
        }

        private ActionResult ValidateState(Profile profile)
        {
            return this.ValidateRequiredValue(profile.State, Resources.ClientProfileValidator_State, "SR");
        }

        private ActionResult ValidateZip(Profile profile)
        {
            return this.ValidateRequiredValue(profile.Zip, Resources.ClientProfileValidator_Zip, "ZR");
        }



        private ActionResult ValidateRequiredValue(string value, string errorFieldName, string errorCode)
        {
            ActionResult actionResult = new ActionResult(true);
            if (string.IsNullOrEmpty(value))
            {
                actionResult.Messages.Add(new ResultMessage(string.Format(Resources.ClientProfileValidator_IsRequired, errorFieldName), errorCode));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        private ActionResult ValidateFirstName(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.FirstName))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_FirstNameRequired, "FNR"));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        private ActionResult ValidateLastName(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.LastName))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_LastNameRequired, "LNR"));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        private ActionResult ValidateEmail(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.EmailAddress))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_EmailAddressRequired, "ER"));
                actionResult.IsSuccessful = false;
            }
            else
            {
                Regex emailRegEx = new Regex(EMAIL_REG_EX);
                if (!emailRegEx.Match(profile.EmailAddress).Success)
                {
                    actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_InvalidEmailFormat, "IEF"));
                    actionResult.IsSuccessful = false;
                }
            }




            return actionResult;
        }


        private ActionResult ValidateSecurityQuestion(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;




            if (string.IsNullOrEmpty(profile.SecurityQuestion))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityQuestionRequired, "SQR"));
                actionResult.IsSuccessful = false;
            }

            

            return actionResult;
        }



        private ActionResult ValidateSecurityAnswer(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.SecurityAnswer))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityAnswerRequired, "SAR"));
                actionResult.IsSuccessful = false;
            }

            

            return actionResult;
        }


        

       

    }
}