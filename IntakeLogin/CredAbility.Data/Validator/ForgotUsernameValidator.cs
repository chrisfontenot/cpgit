﻿using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class ForgotUsernameValidator : Validator<string>
    {
        public override ActionResult Validate(string obj)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            actionResult.MergeActionResult(this.ValidateStringRequired(obj, "EMAIL-REQUIRED", Resources.ForgotUsernameValidator_EmailRequired));
            actionResult.MergeActionResult(this.ValidateEmailFormat(obj, "EMAIL-WRONGFORMAT", Resources.ForgotUsernameValidator_EmailWrongFormat));
            return actionResult;
        }
    }
}
