﻿using CredAbility.Core.Client;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class LoginValidator : Validator<ClientProfile>
    {
        public override ActionResult Validate(ClientProfile user)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;

            actionResult.MergeActionResult(this.ValidateStringRequired(user.Username, "USERNAME-REQUIRED", Resources.LoginValidator_UsernameRequired));
            actionResult.MergeActionResult(this.ValidateStringRequired(user.Password, "PASSWORD-REQUIRED", Resources.LoginValidator_PasswordRequired));
            
            return actionResult;
        }


    }

    
}
