﻿using System.Text.RegularExpressions;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class ValidatorBase
    {
        private const string SSN_REG_EX = @"^(?!000)([0-6]\d{2}|7([0-6]\d|7[012]))([ -]?)(?!00)\d\d\3(?!0000)\d{4}$";
        private const string EmailRegEx = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

        protected ActionResult ValidateRequired(object value, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };

            if (value == null || (typeof(string) ==  value.GetType() && string.IsNullOrEmpty(value.ToString())))
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }


        protected ActionResult ValidateStringRequired(string value, string code, string message)
        {

            var actionResult = new ActionResult { IsSuccessful = true };
            if (string.IsNullOrEmpty(value))
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        protected ActionResult ValidateEmailFormat(string value, string code, string message)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;

            Regex emailRegEx = new Regex(EmailRegEx);
            if (!string.IsNullOrEmpty(value) && !emailRegEx.Match(value.ToLower()).Success)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        protected ActionResult ValidateSsnFormat(string value, string code, string message)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;

            Regex regex = new Regex(SSN_REG_EX);
            if (!string.IsNullOrEmpty(value) && !regex.Match(value).Success)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }
    }
}
