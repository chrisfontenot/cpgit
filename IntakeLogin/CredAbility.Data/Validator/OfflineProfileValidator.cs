using System;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Validator;

namespace CredAbility.Data.Validator
{
    public class OfflineProfileValidator : OfflineProfileUpdateValidator
    {
        public override ActionResult Validate(Profile obj)
        {
            ActionResult result = base.Validate(obj);

            result.MergeActionResult(ClientProfileValidatorHelper.ValidateUsername(obj));

            return result;
        }

    }

    public class OfflineProfileUpdateValidator: Validator<Profile>{
        #region Overrides of Validator<Profile>

        public override ActionResult Validate(Profile obj)
        {
            ActionResult result = new ActionResult(true);
            
            result.MergeActionResult(this.ValidateStringRequired(obj.FirstName, "FIRSTNAME-REQUIRED", Resources.ClientProfileValidator_FirstNameRequired));
            result.MergeActionResult(this.ValidateStringRequired(obj.LastName, "LASTNAME-REQUIRED", Resources.ClientProfileValidator_LastNameRequired));
            result.MergeActionResult(this.ValidateStringRequired(obj.EmailAddress, "EMAIL-REQUIRED", Resources.ClientProfileValidator_EmailAddressRequired));
            result.MergeActionResult(this.ValidateEmailFormat(obj.EmailAddress, "EMAIL-WRONGFORMAT", Resources.ClientProfileValidator_InvalidEmailFormat));
            result.MergeActionResult(this.ValidateStringRequired(obj.HomePhone, "HOMEPHONE-REQUIRED", Resources.ClientProfileValidator_HomePhone));

            result.MergeActionResult(this.ValidateStringRequired(obj.MailingAddressLine1, "ADDRESS-REQUIRED", Resources.ClientProfileValidator_MailingAddress1));
            result.MergeActionResult(this.ValidateStringRequired(obj.City, "CITY-REQUIRED", Resources.ClientProfileValidator_City));
            result.MergeActionResult(this.ValidateStringRequired(obj.State, "STATE-REQUIRED", Resources.ClientProfileValidator_State));
            result.MergeActionResult(this.ValidateStringRequired(obj.Zip, "ZIPCODE-REQUIRED", Resources.ClientProfileValidator_Zip));

            result.MergeActionResult(ClientProfileValidatorHelper.ValidateSecurityQuestion(obj));
            result.MergeActionResult(ClientProfileValidatorHelper.ValidateSecurityAnswer(obj));

            PasswordValidator validator = new PasswordValidator();
            result.MergeActionResult(validator.Validate(obj));
            

            return result;
        }

        #endregion

        

        
    }
}