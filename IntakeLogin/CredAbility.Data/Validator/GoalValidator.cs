﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class GoalValidator : Validator<Core.MyGoal.Goal>
    {
        public override ActionResult Validate(Core.MyGoal.Goal goal)
        {
            var result = new ActionResult(true);
            result.MergeActionResult(ValidateStringRequired(goal.Name, "goalName", Resources.Goal_validation_NameRequired));
            result.MergeActionResult(ValidateNumberRequired(goal.TotalAmount, "goalAmount", Resources.Goal_validation_AmountRequired));
            result.MergeActionResult(ValidateNumberPositive(goal.TotalAmount, "goalAmountPositive", Resources.Goal_validation_Amountpositive));
            result.MergeActionResult(ValidateStringRequired(goal.Type.ToString(), "goalType", Resources.Goal_validation_TypeRequired));
            result.MergeActionResult(ValidateDate(goal.DueDate, "dueDate", Resources.Goal_validation_DateRequired));

            return result;
        }

        private static ActionResult ValidateNumberPositive(decimal amount, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (amount < 0)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }

        private static ActionResult ValidateDate(DateTime dueDate, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (dueDate<new DateTime(1800,1,1) || dueDate>new DateTime(9999,1,1))
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }

        private static ActionResult ValidateNumberRequired(decimal amount, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (amount == 0)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }
    }
}
