﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class SecurityAnswerValidator : ValidatorBase
    {
        public ActionResult Validate(string answer1)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            actionResult.MergeActionResult(this.ValidateStringRequired(answer1, "ANSWER1-REQUIRED", Resources.ForgotPassword_Answer1Required));
            return actionResult;
        }
        
    }
}
