﻿using System.Text.RegularExpressions;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Identity;

namespace CredAbility.Data.Validator
{
    public class ClientProfileValidator : Validator<ClientProfile>
    {
        private const string EMAIL_REG_EX = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-z0-9-]*[a-zA-Z0-9])?";


        public override ActionResult Validate(ClientProfile profile)
        {
            var actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            actionResult.MergeActionResult(this.ValidateFirstName(profile));
            actionResult.MergeActionResult(this.ValidateLastName(profile));
            actionResult.MergeActionResult(this.ValidateEmail(profile));
            actionResult.MergeActionResult(this.ValidateSecurityQuestion(profile));
            actionResult.MergeActionResult(this.ValidateSecurityAnswer(profile));
            actionResult.MergeActionResult(this.ValidateUsername(profile));

            PasswordValidator validator = new PasswordValidator();
            actionResult.MergeActionResult(validator.Validate(profile));
            
            return actionResult;
        }


        private ActionResult ValidateFirstName(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.FirstName))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_FirstNameRequired, "FIRSTNAME-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        private ActionResult ValidateLastName(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.LastName))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_LastNameRequired, "LASTNAME-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

            return actionResult;
        }

        private ActionResult ValidateEmail(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.EmailAddress))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_EmailAddressRequired, "EMAIL-REQUIRED"));
                actionResult.IsSuccessful = false;
            }
            else
            {
                Regex emailRegEx = new Regex(EMAIL_REG_EX);
                if (!emailRegEx.Match(profile.EmailAddress).Success)
                {
                    actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_InvalidEmailFormat,"EMAIL-WRONGFORMAT"));
                    actionResult.IsSuccessful = false;
                }
            }




            return actionResult;
        }


        private ActionResult ValidateSecurityQuestion(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;




            if (string.IsNullOrEmpty(profile.SecurityQuestion))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityQuestionRequired, "QUESTION1-REQUIRED"));
                actionResult.IsSuccessful = false;
            }
          
            

            return actionResult;
        }

    

        private ActionResult ValidateSecurityAnswer(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.SecurityAnswer))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_SecurityAnswerRequired, "ANSWER1-REQUIRED"));
                actionResult.IsSuccessful = false;
            }


            return actionResult;
        }


        private ActionResult ValidateUsername(ClientProfile profile)
        {
            ActionResult actionResult = new ActionResult();
            actionResult.IsSuccessful = true;
            if (string.IsNullOrEmpty(profile.Username))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_UsernameRequired, "USERNAME-REQUIRED"));
                actionResult.IsSuccessful = false;
            }

            if (!IdentityRepository.ConfirmValidUsername(profile.Username))
            {
                actionResult.Messages.Add(new ResultMessage(Resources.ClientProfileValidator_UsernameUnavailable, "USERNAME-UNAVAILABLE"));
                actionResult.IsSuccessful = false;
            }


            return actionResult;
        }

       

    }
}