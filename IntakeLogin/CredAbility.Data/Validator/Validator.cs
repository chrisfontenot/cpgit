﻿using System;
using System.Collections.Generic;
using System.Linq;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public abstract class Validator<T>: ValidatorBase
    {
        
        
        public abstract ActionResult Validate(T obj);

      
    }
}
