﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Common;

namespace CredAbility.Data.Validator
{
    public class GoalHistoryValidator : ValidatorBase
    {
        public ActionResult Validate(Core.MyGoal.GoalHistory goalHistory, Core.MyGoal.Goal goal)
        {
            var result = new ActionResult(true);
            result.MergeActionResult(ValidateNumberRequired(goalHistory.Amount, "goalAmount", Resources.Goal_validation_AmountRequired));
            result.MergeActionResult(ValidateDate(goalHistory.Date, "Date", Resources.Goal_validation_DateRequired));
            result.MergeActionResult(ValidateAmountIsSmallerThanTotalAmount(goal, goalHistory.Amount, "maxAmount", Resources.Goal_validation_AmountTooHigh));

            return result;
        }

        private static ActionResult ValidateAmountIsSmallerThanTotalAmount(Core.MyGoal.Goal goal, decimal amount, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (goal.History.Sum(h=>h.Amount) + amount > goal.TotalAmount)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }

        private static ActionResult ValidateDate(DateTime dueDate, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (dueDate < new DateTime(1800, 1, 1) || dueDate > new DateTime(9999, 1, 1))
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }

        private static ActionResult ValidateNumberRequired(decimal amount, string code, string message)
        {
            var actionResult = new ActionResult { IsSuccessful = true };
            if (amount == 0)
            {
                actionResult.Messages.Add(new ResultMessage(message, code));
                actionResult.IsSuccessful = false;
            }
            return actionResult;
        }
    }
}
