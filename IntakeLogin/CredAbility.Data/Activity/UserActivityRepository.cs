﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using CredAbility.Core.Activity;
using CredAbility.Core.DMP;
using CredAbility.Data.DMP;

namespace CredAbility.Data.Activity
{
	public static class UserActivityRepository
	{
		public static void Save(UserActivity userActivity)
		{
			if(userActivity == null)
			{
				throw new NullReferenceException("You cannot save a UserActivity.");
			}

			var dataContext = new CredAbilityDataContext();
			var activityEntity = CredAbilityEntityFactory.GetUserActivityEntity(userActivity);
			dataContext.UserActivityEntities.InsertOnSubmit(activityEntity);
			dataContext.SubmitChanges();
		}

		public static UserActivityList GetUserActivities(string userID, int maxResult, DMPInfo dmpInfo)
		{
			try
			{
				var dataContext = new CredAbilityDataContext();
				var activities = dataContext.UserActivityEntities.Where(ua => ua.ID == userID).ToList();
				if(dmpInfo != null)
				{
					NumberFormatInfo nfi = new NumberFormatInfo();
					nfi.CurrencyNegativePattern = 1;
					nfi.CurrencySymbol = "&#36;";

					foreach(DMPActivity activity in dmpInfo.DmpActivityList)
					{
						UserActivityEntity newActivity = new UserActivityEntity();
						newActivity.Date = activity.ActivityDate;
						newActivity.Text = activity.ActivityType + " of " + activity.ActivityAmount.ToString("C", nfi);
						newActivity.Type = (int) UserActivityType.DMPPaymentMade;
						activities.Add(newActivity);
					}
				}
				var userActivities = activities.OrderByDescending(ua => ua.Date).Take(maxResult);
				var result = new UserActivityList(userActivities.Count());
				foreach(var userActivityEntity in userActivities)
				{
					result.Add(CredAbilityEntityFactory.GetUserActivity(userActivityEntity));
				}
				return result;
			}
			catch(Exception)
			{
				return new UserActivityList();
			}
		}

		public static UserActivityList GetUserActivities(string userID, int maxResult, int currentPage, out int totalCount, DMPInfo dmpInfo)
		{
			try
			{
				var dataContext = new CredAbilityDataContext();
				var userActivities = dataContext.UserActivityEntities.Where(ua => ua.ID == userID).ToList();
				if(dmpInfo != null)
				{
					NumberFormatInfo nfi = new NumberFormatInfo();
					nfi.CurrencyNegativePattern = 1;
					nfi.CurrencySymbol = "&#36;";

					foreach(DMPActivity activity in dmpInfo.DmpActivityList)
					{
						UserActivityEntity newActivity = new UserActivityEntity();
						newActivity.Date = activity.ActivityDate;
						newActivity.Text = activity.ActivityType + " of " + activity.ActivityAmount.ToString("C", nfi);
						newActivity.Type = (int) UserActivityType.DMPPaymentMade;
						userActivities.Add(newActivity);
					}
				}
				totalCount = userActivities.Count();
				userActivities = userActivities.OrderByDescending(ua => ua.Date).Skip((currentPage - 1) * maxResult).Take(maxResult).ToList();
				var result = new UserActivityList(userActivities.Count());
				foreach(var userActivityEntity in userActivities)
				{
					result.Add(CredAbilityEntityFactory.GetUserActivity(userActivityEntity));
				}
				return result;
			}
			catch(Exception)
			{
				totalCount = 0;
				return new UserActivityList();
			}
		}
	}
}