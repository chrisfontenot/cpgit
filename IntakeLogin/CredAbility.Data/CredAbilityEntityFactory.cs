﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CredAbility.Core.Activity;
using CredAbility.Core.Client;
using CredAbility.Core.CreditScoreChallenge;
using CredAbility.Core.Document;

namespace CredAbility.Data
{
    public static class CredAbilityEntityFactory
    {
        public static UserActivity GetUserActivity(UserActivityEntity entity)
        {
            var userActivity = new UserActivity(entity.ID)
                                   {
                                       Date = entity.Date,
                                       Type = ((UserActivityType) entity.Type),
                                       Text = entity.Text
                                   };
            return userActivity;
        }

        public static UserActivityEntity GetUserActivityEntity(UserActivity userActivity)
        {
            var entity = new UserActivityEntity
                             {
                                 ID = userActivity.ID,
                Date = userActivity.Date,
                Type = userActivity.Type.GetHashCode(),
                Text = userActivity.Text
            };

            return entity;
        }


        public static Contestant GetContestant(CreditScoreChallengeContestantEntity entity)
        {

            var contestant = new Contestant(entity.ClientID);

            contestant.Fullname = entity.Fullname;
            contestant.PrimaryPhoneNumber = entity.PrimaryPhoneNumber;
            contestant.SecondaryPhoneNumber = entity.SecondaryPhoneNumber;
            contestant.State = entity.State;
            contestant.City = entity.City;
            contestant.StreetAddress = entity.StreetAddress;
            contestant.Zip = entity.ZipCode;
            contestant.PreferredLanguage = entity.PreferredLanguage;

            try
            {
                contestant.SSN = new SocialSecurityNumber(entity.SSN);
            }
            catch(Exception)
            {
                contestant.SSN = null;
            }

            if (entity.SubscriptionDate != null)
            {
                contestant.SubscriptionDate = (DateTime) entity.SubscriptionDate;
            }

            return contestant;
        }

        public static CreditScoreChallengeContestantEntity GetCreditScoreChallengeContestantEntity(Contestant contestant)
        {
            var entity = new CreditScoreChallengeContestantEntity
                             {
                                 ClientID = contestant.ClientId,
                                 Fullname = contestant.Fullname,
                                 PrimaryPhoneNumber = contestant.PrimaryPhoneNumber,
                                 SecondaryPhoneNumber = contestant.SecondaryPhoneNumber,
                                 SSN = (contestant.SSN != null) ? contestant.SSN.FormatedSSN : string.Empty,
                                 SubscriptionDate = contestant.SubscriptionDate,
                                 City = contestant.City,
                                 State = contestant.State,
                                 StreetAddress = contestant.StreetAddress,
                                 ZipCode = contestant.Zip,
                                 PreferredLanguage = contestant.PreferredLanguage
                             };

            return entity;
            
        }

        public static ChallengeHistoryItem GetChallengeHistoryItem(CreditScoreChallengeHistoryEntity entity)
        {
            var challengeHistoryItem = new ChallengeHistoryItem(entity.ClientId);

            challengeHistoryItem.PourcentageImprovement = entity.PourcentageImprovement;
            challengeHistoryItem.Rank = entity.Rank;
            challengeHistoryItem.Score = entity.Score;

            challengeHistoryItem.ImprovmentDirection = (ImprovmentDirection) Enum.Parse(typeof(ImprovmentDirection),entity.DirectionImprovement);

            if (entity.HistoryDate != null)
            {
                challengeHistoryItem.Date = (DateTime)entity.HistoryDate;
            }

            return challengeHistoryItem;
        }
    }
}
