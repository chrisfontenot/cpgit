﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using CredAbility.Core.Common;
using CredAbility.Core.Client;
using CredAbility.Data.Services.Identity;
using UserSearchResult = CredAbility.Data.Services.Identity.UserSearchResult;

namespace CredAbility.Data.Identity
{
	public static class IdentityRepository
	{
		public static bool IsExistingProfileByHostId(string hostId)
		{

			UserSearchResult result = IdmUserSearch(new UserSearchCriteria { HostId = hostId });

			if(result.IsSuccessful)
			{
				return result.TotalMatches > 0;
			}

			// if the web service UserSearch return IsSuccessful == false, we throw exception
			throw new IdentityClientException("There was an error during the UserSearch() web method call.");
		}

		private static UserSearchResult IdmUserSearch(UserSearchCriteria searchCriteria)
		{
			using(IdentityClient IdmSys = new IdentityClient())
			{
				return IdmSys.UserSearch(searchCriteria);
			}
		}

		//todo: Need refactor the code below by seperating each method in a right repository. IdentityRepository did too many things.
		public static bool Logout(ClientProfile clientProfile)
		{
			return Logout(long.Parse(clientProfile.ClientID));
		}

		public static bool Logout(string userID)
		{
			return Logout(long.Parse(userID));
		}

		public static bool Logout(long userID)
		{
			// sign off from Identity services
			IdentityClient identityClient = new IdentityClient();
			identityClient.UserLogout(userID);
			identityClient.Close();

			return true;
		}

		public static CustomResponse<ClientProfile> AuthenticateUserByToken(string userId, string tokenId)
		{
			var response = new CustomResponse<ClientProfile>();
			Services.Identity.IdentityClient identityClient = new IdentityClient();
			bool validatationResult = identityClient.UserSsoTokenValidate(long.Parse(userId), tokenId);
			identityClient.Close();
			if(validatationResult)
			{
				ClientProfile user = new ClientProfile();
				user.ClientID = userId;
				user.IdentityTokenID = tokenId;
				user.LastLogin = DateTime.MinValue;
				response.ResponseValue = user;
			}
			else
			{
				response.HasError = true;
				response.ErrorMessage = Resources.IdentityRepository_InvalidIdentification;
			}
			return response;
		}

		public static CustomResponse<ClientProfile> AuthenticateUser(string username, string password)
		{
			var response = new CustomResponse<ClientProfile>();
			Services.Identity.IdentityClient identityClient = new IdentityClient();
			UserAuthenticateResult userAuthenicateResult = identityClient.UserAuthenticate(username, password, "");
			identityClient.Close();
			if(userAuthenicateResult.IsSuccessful)
			{
				ClientProfile clientProfile = new ClientProfile();
				clientProfile.ClientID = userAuthenicateResult.UserId.Value.ToString();
				clientProfile.IdentityTokenID = userAuthenicateResult.SsoToken;

				if(userAuthenicateResult.LastLoginDate.HasValue)
				{
					clientProfile.LastLogin = userAuthenicateResult.LastLoginDate.Value;
				}

				response.ResponseValue = clientProfile;
			}
			else
			{
				response.HasError = true;

				if(userAuthenicateResult.IsUserLocked.HasValue && userAuthenicateResult.IsUserLocked.Value)
				{
					response.ErrorMessage = Resources.IdentityRepository_UserLocked;
				}
				else if((userAuthenicateResult.IsInvalidUsername.HasValue && userAuthenicateResult.IsInvalidUsername.Value) || (userAuthenicateResult.IsInvalidPassword.HasValue && userAuthenicateResult.IsInvalidPassword.Value))
				{
					response.ErrorMessage = Resources.IdentityRepository_InvalidIdentification;
				}
			}
			return response;
		}

		public static ActionResult ResetPassword(string userid, string newPassword, string confirmNewPassword)
		{
			var result = new ActionResult();
			IdentityClient identityClient = new IdentityClient();
			UserPasswordChangeResult userPasswordChangeResult = identityClient.UserPasswordReset(long.Parse(userid), newPassword, confirmNewPassword, "");
			identityClient.Close();
			result.IsSuccessful = userPasswordChangeResult.IsSuccessful;
			if(!userPasswordChangeResult.IsSuccessful)
			{
				result.Messages = CollectUserPasswordChangeResultErrors(userPasswordChangeResult);
			}
			return result;
		}

		public static ActionResult ChangePassword(ClientProfile clientProfile, string currentPassword, string newPassword, string retypedNewPassword)
		{
			var result = new ActionResult();
			IdentityClient identityClient = new IdentityClient();
			UserPasswordChangeResult userPasswordChangeResult = identityClient.UserPasswordChange(long.Parse(clientProfile.ClientID), currentPassword, newPassword, retypedNewPassword);
			identityClient.Close();
			result.IsSuccessful = userPasswordChangeResult.IsSuccessful;
			if(!userPasswordChangeResult.IsSuccessful)
			{
				result.Messages = CollectUserPasswordChangeResultErrors(userPasswordChangeResult);
			}

			//Refresh user
			CustomResponse<UserProfile> userProfileResponse = GetUserProfile(clientProfile.ClientID);
			UserProfile userProfile = userProfileResponse.ResponseValue;
			DataServicesFactory.GetClientProfile(userProfile, clientProfile);

			return result;
		}

		public static ActionResult ChangeUsername(ClientProfile clientProfile, Services.Identity.User user)
		{
			var result = new ActionResult();
			IdentityClient identityClient = new IdentityClient();
			UserSaveResult userSaveResult = identityClient.UserSave(user, clientProfile.Username);
			identityClient.Close();
			result.IsSuccessful = userSaveResult.IsSuccessful;
			if(!userSaveResult.IsSuccessful)
			{
				result.Messages.Add(new ResultMessage(userSaveResult.ExceptionStr, "USR"));
			}
			return result;
		}

		public static CustomResponse<ClientProfile> LoginByToken(string userId, string tokenId)
		{
			var response = new CustomResponse<ClientProfile>();

			var authResponse = AuthenticateUserByToken(userId, tokenId);

			if(authResponse.HasError)
			{
				response.HasError = true;
				response.ErrorMessage = authResponse.ErrorMessage;
			}
			else
			{
				ClientProfile user = authResponse.ResponseValue;

				CustomResponse<UserProfile> userProfileResponse = GetUserProfile(user.ClientID);


				//TAG USERID VALIDATION
				if(user.ClientID != userProfileResponse.ResponseValue.UserId.ToString())
				{
					response.HasError = true;
					response.ErrorMessage = Resources.IdentityRepository_UserIdValidation;
				}

				if(userProfileResponse.HasError)
				{
					response.HasError = true;
					response.ErrorMessage = userProfileResponse.ErrorMessage;
				}
				else
				{
					UserProfile userProfile = userProfileResponse.ResponseValue;

					ClientProfile clientProfile = DataServicesFactory.GetClientProfile(userProfile, user);

					response.ResponseValue = clientProfile;
				}
			}

			return response;
		}

		public static CustomResponse<ClientProfile> Login(string username, string password)
		{
			var response = new CustomResponse<ClientProfile>();

			var authResponse = AuthenticateUser(username, password);

			if(authResponse.HasError)
			{
				response.HasError = true;
				response.ErrorMessage = authResponse.ErrorMessage;
			}
			else
			{
				ClientProfile user = authResponse.ResponseValue;

				CustomResponse<UserProfile> userProfileResponse = GetUserProfile(user.ClientID);
				//TAG USERID VALIDATION
				if(user.ClientID != userProfileResponse.ResponseValue.UserId.ToString())
				{
					response.HasError = true;
					response.ErrorMessage = Resources.IdentityRepository_UserIdValidation;
				}

				if(userProfileResponse.HasError)
				{
					response.HasError = true;
					response.ErrorMessage = userProfileResponse.ErrorMessage;
				}
				else
				{
					UserProfile userProfile = userProfileResponse.ResponseValue;

					ClientProfile clientProfile = DataServicesFactory.GetClientProfile(userProfile, user);

					response.ResponseValue = clientProfile;
				}
			}

			return response;
		}

		public static bool ConfirmValidUsername(string username)
		{
			try
			{
				IdentityClient identityClient = new IdentityClient();
				var userid = identityClient.UserIdGetByUsername(username);
				identityClient.Close();
				return !userid.HasValue;
			}
			catch(FaultException)
			{
				return false;
			}
		}

		public static List<Core.Identity.SecurityQuestion> GetAllSecurityQuestions(string languageCode)
		{
			var identityClient = new IdentityClient();
			var securityQuestion = identityClient.SecurityQuestionsGet(languageCode);
			identityClient.Close();
			if(securityQuestion != null)
			{
				return DataServicesFactory.GetSecurityQuestions(securityQuestion);
			}

			return new List<Core.Identity.SecurityQuestion>();
		}

		public static Core.Identity.SecurityQuestion GetSecurityQuestionByUsername(string username)
		{
			var identityClient = new IdentityClient();
			var securityQuestion = identityClient.SecurityQuestionGetByUsername(username);
			identityClient.Close();

			if(securityQuestion != null)
			{
				return DataServicesFactory.GetSecurityQuestion(securityQuestion);
			}

			return null;
		}

		public static Core.Identity.SecurityQuestion GetSecurityQuestionByEmail(string emailAddress)
		{
			var identityClient = new IdentityClient();
			var securityQuestion = identityClient.SecurityQuestionGetByEmail(emailAddress);
			identityClient.Close();

			if(securityQuestion != null)
			{
				return DataServicesFactory.GetSecurityQuestion(securityQuestion);
			}

			return null;
		}

		public static CustomResponse<UsernameRecoverResult> ValidateSecurityQuestionChallengeByEmail(string emailAddress, string answer1)
		{
			var response = new CustomResponse<UsernameRecoverResult>();
			Services.Identity.IdentityClient identityClient = new IdentityClient();


			UsernameRecoverResult recoverResult = identityClient.UsernameRecover(emailAddress, answer1);
			identityClient.Close();
			response.HasError = !recoverResult.IsSuccessful;
			response.ResponseValue = recoverResult;
			return response;
		}

		public static CustomResponse<UserSecurityQuestionChallengeResult> ValidateSecurityQuestionChallengeByUsername(string username, string answer1)
		{
			var response = new CustomResponse<UserSecurityQuestionChallengeResult>();
			var identityClient = new IdentityClient();

			UserSecurityQuestionChallengeResult challengeResult = identityClient.UserSecurityQuestionChallenge(username, answer1);
			identityClient.Close();
			response.HasError = !challengeResult.IsSuccessful;
			response.ResponseValue = challengeResult;
			return response;
		}



		public static long? GetUserIdByUsername(string username)
		{
			IdentityClient identityClient = new IdentityClient();
			long? value = identityClient.UserIdGetByUsername(username);
			identityClient.Close();
			return value;
		}

		public static long[] GetUserIdByEmail(string emailAddress)
		{
			IdentityClient identityClient = new IdentityClient();
			long[] value = identityClient.UserIdsGetByEmail(emailAddress);
			identityClient.Close();
			return value;
		}


		public static CustomResponse<Data.Services.Identity.User> GetUser(string userid)
		{
			var response = new CustomResponse<Data.Services.Identity.User>();
			IdentityClient identityClient = new IdentityClient();
			Data.Services.Identity.User user = new Data.Services.Identity.User();
			Data.Services.Identity.UserDetail userdetail = new Data.Services.Identity.UserDetail();
			long useridlong = long.Parse(userid);
			user = identityClient.UserGet(useridlong);
			identityClient.Close();
			response.ResponseValue = user;
			return response;
		}

		public static CustomResponse<UserProfile> GetUserProfile(string userid)
		{
			var response = new CustomResponse<UserProfile>();
			var identityClient = new IdentityClient();
			var upo = new UserProfileOptions();
			upo.IncludeSecurityQuestionAnswers = true;
			upo.IncludeAddresses = true;
			upo.IncludeSecurityQuestionAnswers = true;
			upo.IncludeUserDetails = true;
			upo.IncludeAccounts = true;
			if(!string.IsNullOrEmpty(userid))
			{
				long useridlong = long.Parse(userid);

				var userProfile = identityClient.UserProfileGet(useridlong, upo);
				identityClient.Close();
				response.ResponseValue = userProfile;

				if(userProfile == null)
				{
					log4net.LogManager.GetLogger("LogFileAppender").Error("userProfile is null - userId = " + userid);
				}
			}
			return response;
		}

		public static CustomResponse<UserProfile> GetUserWebsites(string userid)
		{
			var response = new CustomResponse<UserProfile>();
			var identityClient = new IdentityClient();
			var upo = new UserProfileOptions();
			upo.IncludeUserWebsites = true;
			if(!string.IsNullOrEmpty(userid))
			{
				long useridlong = long.Parse(userid);

				var userProfile = identityClient.UserProfileGet(useridlong, upo);
				identityClient.Close();
				response.ResponseValue = userProfile;
			}
			return response;
		}

		public static CustomResponse<Website[]> GetAllWebsites()
		{
			var response = new CustomResponse<Website[]>();
			var identityClient = new IdentityClient();
			Website[] allWebsites = identityClient.WebsitesGet();
			identityClient.Close();
			response.ResponseValue = allWebsites;
			return response;
		}

		public static CustomResponse<CredAbility.Data.Services.Identity.UserDetail> GetUserDetail(string userid)
		{
			CredAbility.Data.Services.Identity.UserDetail userDetail = new CredAbility.Data.Services.Identity.UserDetail();
			var response = new CustomResponse<CredAbility.Data.Services.Identity.UserDetail>();
			var identityClient = new IdentityClient();
			var upo = new UserProfileOptions();

			if(!string.IsNullOrEmpty(userid))
			{
				long userIdLong = long.Parse(userid);
				userDetail = identityClient.UserDetailGet(userIdLong, true);
				identityClient.Close();
				response.ResponseValue = userDetail;
			}
			return response;
		}

		public static ActionResult SaveProfile(Profile profile)
		{
			return SaveProfile(profile, false, "");
		}

		public static ActionResult SaveProfile(Profile profile, bool AssignOfflineWebsiteCode, string offlineUserClientId)
		{
			CustomResponse<UserProfile> responseUserProfile = IdentityRepository.GetUserProfile(profile.ClientID);
			UserProfile userProfile = DataServicesFactory.AssignUserProfile(profile, responseUserProfile.ResponseValue);

			CustomResponse<CredAbility.Data.Services.Identity.UserDetail> responseUserDetail = IdentityRepository.GetUserDetail(profile.ClientID);
			userProfile.UserDetailPrimary = responseUserDetail.ResponseValue;

			if(AssignOfflineWebsiteCode)
			{
				UserWebsite uw = new UserWebsite();
				uw.WebsiteCode = "OFF";
				userProfile.UserWebsites = new UserWebsite[1];
				userProfile.UserWebsites[0] = uw;

				CredAbility.Data.Services.Identity.Account account = new CredAbility.Data.Services.Identity.Account();
				account.AccountTypeCode = profile.ClientAccounts[0].AccountName;

				account.HostId = offlineUserClientId;

				account.UserId = long.Parse(profile.ClientID);
				userProfile.Accounts = new CredAbility.Data.Services.Identity.Account[1];
				userProfile.Accounts[0] = account;
			}

			userProfile.UserDetailPrimary = DataServicesFactory.AssignUserDetail(profile, userProfile.UserDetailPrimary);
			var result = new ActionResult();
			var identityClient = new IdentityClient();
			UserProfileSaveResult userProfileSaveResult = identityClient.UserProfileSave(userProfile, profile.Username);
			identityClient.Close();
			result.IsSuccessful = userProfileSaveResult.IsSuccessful;
			if(!userProfileSaveResult.IsSuccessful)
			{
				result.Messages = CollectUserProfileSaveResultErrors(userProfileSaveResult);
			}
			return result;
		}

		public static ActionResult SaveUserDetail(ClientProfile clientProfile, CredAbility.Data.Services.Identity.UserDetail userDetail)
		{
			var result = new ActionResult();
			var identityClient = new IdentityClient();
			UserDetailSaveResult userDetailSaveResult = identityClient.UserDetailSave(userDetail, clientProfile.Username);
			identityClient.Close();
			result.IsSuccessful = userDetailSaveResult.IsSuccessful;
			if(!userDetailSaveResult.IsSuccessful)
			{
				result.Messages = CollectUserDetailSaveResultErrors(userDetailSaveResult);
			}
			return result;
		}

		public static ActionResult SaveUser(ClientProfile clientProfile, Data.Services.Identity.User user)
		{
			var result = new ActionResult();
			var identityClient = new IdentityClient();
			UserSaveResult userSaveResult = identityClient.UserSave(user, clientProfile.Username);
			identityClient.Close();
			result.IsSuccessful = userSaveResult.IsSuccessful;
			if(!userSaveResult.IsSuccessful)
			{
				result.Messages = CollectUserSaveResultErrors(userSaveResult);
			}


			//Refresh User
			CustomResponse<UserProfile> userProfileResponse = GetUserProfile(clientProfile.ClientID);
			UserProfile userProfile = userProfileResponse.ResponseValue;
			DataServicesFactory.GetClientProfile(userProfile, clientProfile);

			return result;
		}

		public static ActionResult SaveAvatar(ClientProfile clientProfile, string avatarKey)
		{
			CustomResponse<Data.Services.Identity.User> responseUser = IdentityRepository.GetUser(clientProfile.ClientID);
			Data.Services.Identity.User user = responseUser.ResponseValue;
			user.Avatar = avatarKey;
			return IdentityRepository.SaveUser(clientProfile, user);
		}

		public static ResultMessageList CollectUserProfileSaveResultErrors(UserProfileSaveResult result)
		{
			ResultMessageList l = new ResultMessageList();

			// 1. since UserProfileSaveResult inherits from UserSaveResult, go get any errors from the UserSaveResult aspect of this object
			// 2. this will also collect the NewPasswordErrors
			l.AddRange(CollectUserSaveResultErrors((UserSaveResult)result));

			// AccountSaveResult
			l.AddRange(CollectAccountSaveResultErrors(result.AccountSaveResults));

			// AddressSaveResult
			l.AddRange(CollectAddressSaveResultErrors(result.AddressSaveResults));

			// UserDetailSaveResultPrimary
			l.AddRange(CollectUserDetailSaveResultErrors(result.UserDetailSaveResultPrimary));

			// UserDetailSaveResultSecondary
			l.AddRange(CollectUserDetailSaveResultErrors(result.UserDetailSaveResultSecondary));

			// UserDetailSaveResultSecondary
			l.AddRange(CollectUserWebsiteSaveResultErrors(result.UserWebsiteSaveResults));

			return l;
		}

		public static ResultMessageList CollectNewPasswordErrors(NewPasswordErrors results)
		{
			ResultMessageList l = new ResultMessageList();

			if(results != null)
			{
				if(results.ContainsFirstName.HasValue && results.ContainsFirstName.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsFirstName, "WS-PasswordChange_ContainsFirstName"));
				}

				if(results.ContainsInvalidChar.HasValue && results.ContainsInvalidChar.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsInvalidChar, "WS-PasswordChange_ContainsInvalidChar"));
				}

				if(results.ContainsLastName.HasValue && results.ContainsLastName.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsLastName, "WS-PasswordChange_ContainsLastName"));
				}

				if(results.ContainsNoLetter.HasValue && results.ContainsNoLetter.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsNoLetter, "WS-PasswordChange_ContainsNoLetter"));
				}

				if(results.ContainsNoNumber.HasValue && results.ContainsNoNumber.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsNoNumber, "WS-PasswordChange_ContainsNoNumber"));
				}

				if(results.ContainsPassword.HasValue && results.ContainsPassword.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsPassword, "WS-PasswordChange_ContainsPassword"));
				}

				if(results.ContainsSequence.HasValue && results.ContainsSequence.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsSequence, "WS-PasswordChange_ContainsSequence"));
				}

				if(results.ContainsUsername.HasValue && results.ContainsUsername.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_ContainsUsername, "WS-PasswordChange_ContainsUsername"));
				}

				if(results.IsTooLong.HasValue && results.IsTooLong.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_IsTooLong, "WS-PasswordChange_IsTooLong"));
				}

				if(results.IsTooShort.HasValue && results.IsTooShort.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_IsTooShort, "WS-PasswordChange_IsTooShort"));
				}
			}

			return l;
		}

		public static ResultMessageList CollectUserWebsiteSaveResultErrors(UserWebsiteSaveResult[] results)
		{
			ResultMessageList l = new ResultMessageList();
			if(results != null)
			{
				foreach(UserWebsiteSaveResult r in results)
				{
					if(!r.IsSuccessful)
					{
						if(r.IsCompleteWithoutCompletedDate.HasValue && r.IsCompleteWithoutCompletedDate.Value)
						{
							l.Add(new ResultMessage(Resources.Website_IsCompleteWithoutCompletedDate, "WS-Website_IsCompleteWithoutCompletedDate"));
						}

						if(r.IsIncompleteWithCompletedDate.HasValue && r.IsIncompleteWithCompletedDate.Value)
						{
							l.Add(new ResultMessage(Resources.Website_IsIncompleteWithCompletedDate, "WS-Website_IsIncompleteWithCompletedDate"));
						}
					}
				}
			}
			return l;
		}

		public static List<ResultMessage> CollectAddressSaveResultErrors(AddressSaveResult[] results)
		{
			ResultMessageList l = new ResultMessageList();
			if(results != null)
			{
				foreach(AddressSaveResult asr in results)
				{
					if(!asr.IsSuccessful)
					{
						if(asr.IsInvalidZipCode.HasValue && asr.IsInvalidZipCode.Value)
						{
							l.Add(new ResultMessage(Resources.SaveProfile_IsInvalidZipCode, "WS-SaveProfile_IsInvalidZipCode"));
						}

						if(!string.IsNullOrEmpty(asr.StateExpectedForZip))
						{
							l.Add(new ResultMessage(string.Format(Resources.SaveProfile_StateExpectedForZip, asr.StateExpectedForZip), "WS-SaveProfile_StateExpectedForZip"));
						}
					}
				}
			}
			return l;
		}

		public static ResultMessageList CollectAccountSaveResultErrors(AccountSaveResult[] results)
		{
			ResultMessageList l = new ResultMessageList();
			if(results != null)
			{
				foreach(AccountSaveResult asr in results)
				{
					if(!asr.IsSuccessful)
					{
						if(asr.IsCompleteWithoutCompletedDate.HasValue && asr.IsCompleteWithoutCompletedDate.Value)
						{
							l.Add(new ResultMessage(Resources.Account_IsCompleteWithoutCompletedDate, "WS-Account_IsCompleteWithoutCompletedDate"));
						}

						if(asr.IsIncompleteWithCompletedDate.HasValue && asr.IsIncompleteWithCompletedDate.Value)
						{
							l.Add(new ResultMessage(Resources.Account_IsIncompleteWithCompletedDate, "WS-Account_IsIncompleteWithCompletedDate"));
						}
					}
				}
			}
			return l;
		}

		public static ResultMessageList CollectUserDetailSaveResultErrors(UserDetailSaveResult res)
		{
			ResultMessageList l = new ResultMessageList();

			if(res != null)
			{
				if(res.IsDuplicateEmail.HasValue && res.IsDuplicateEmail.Value)
				{
					l.Add(new ResultMessage(Resources.SaveProfile_IsDuplicateEmail, "WS-SaveProfile_IsDuplicateEmail"));
				}

				if(res.IsDuplicateSsn.HasValue && res.IsDuplicateSsn.Value)
				{
					l.Add(new ResultMessage(Resources.SaveProfile_IsDuplicateSsn, "WS-SaveProfile_IsDuplicateSsn"));
				}

				if(res.IsFirstNameMissing.HasValue && res.IsFirstNameMissing.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_FirstNameRequired, "WS-UserRegister_FirstNameRequired"));
				}

				if(res.IsLastNameMissing.HasValue && res.IsLastNameMissing.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_LastNameRequired, "WS-UserRegister_LastNameRequired"));
				}
			}

			return l;
		}

		public static ResultMessageList CollectUserPasswordChangeResultErrors(UserPasswordChangeResult upcr)
		{
			ResultMessageList l = new ResultMessageList();

			if(upcr.IsInvalidOldPassword.HasValue && upcr.IsInvalidOldPassword.Value)
			{
				l.Add(new ResultMessage(Resources.UserPasswordReset_InvalidOldPassword, "WS-PasswordChange_InvalidOldPassword"));
			}

			if(upcr.IsUnchangedNewPassword.HasValue && upcr.IsUnchangedNewPassword.Value)
			{
				l.Add(new ResultMessage(Resources.UserPasswordReset_UnchangedNewPassword, "WS-PasswordChange_UnchangedNewPassword"));
			}

			if(upcr.IsInvalidNewPasswordConfirm.HasValue && upcr.IsInvalidNewPasswordConfirm.Value)
			{
				l.Add(new ResultMessage(Resources.UserPasswordReset_InvalidNewPasswordConfirm, "WS-PasswordChange_InvalidNewPasswordConfirm"));
			}

			if(upcr.NewPasswordErrors != null)
			{
				l.AddRange(CollectNewPasswordErrors(upcr.NewPasswordErrors));
			}

			return l;
		}

		public static ResultMessageList CollectUserRegisterResultErrors(UserRegisterResult urr)
		{
			ResultMessageList l = new ResultMessageList();

			if(urr.IsDuplicateEmail.HasValue && urr.IsDuplicateEmail.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_DuplicateEmailAddress, "WS-UserRegister.DuplicateEmail"));
			}

			if(urr.IsDuplicateUsername.HasValue && urr.IsDuplicateUsername.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_DuplicateUsername, "WS-UserRegister.DuplicateUsername"));
			}

			if(urr.IsFirstNameMissing.HasValue && urr.IsFirstNameMissing.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_FirstNameRequired, "WS-UserRegister.FirstNameMissing"));
			}

			if(urr.IsLastNameMissing.HasValue && urr.IsLastNameMissing.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_LastNameRequired, "WS-UserRegister.LastNameMissing"));
			}

			if(urr.IsUsernameMissing.HasValue && urr.IsUsernameMissing.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_UsernameRequired, "WS-UserRegister.UsernameMissing"));
			}

			if(urr.IsInvalidUsername.HasValue && urr.IsInvalidUsername.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_InvalidUsername, "WS-UserRegister.UsernameInvalid"));
			}

			if(urr.IsInvalidOldPassword.HasValue && urr.IsInvalidOldPassword.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_InvalidPassword, "WS-UserRegister.InvalidOldPassword"));
			}

			if(urr.IsInvalidNewPasswordConfirm.HasValue && urr.IsInvalidNewPasswordConfirm.Value)
			{
				l.Add(new ResultMessage(Resources.UserRegister_InvalidNewPasswordConfirm, "WS-UserRegister.InvalidNewPasswordConfirm"));
			}

			if(urr.NewPasswordErrors != null)
			{
				l.AddRange(CollectNewPasswordErrors(urr.NewPasswordErrors));
			}

			return l;
		}

		public static ResultMessageList CollectUserSaveResultErrors(UserSaveResult res)
		{
			ResultMessageList l = new ResultMessageList();

			if(res != null)
			{

				if(res.IsDuplicateUsername.HasValue && res.IsDuplicateUsername.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_DuplicateUsername, "WS-UserRegister.DuplicateUsername"));
				}

				if(res.IsUsernameMissing.HasValue && res.IsUsernameMissing.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_UsernameRequired, "WS-UserRegister.UsernameMissing"));
				}

				if(res.IsInvalidUsername.HasValue && res.IsInvalidUsername.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_InvalidUsername, "WS-UserRegister.UsernameInvalid"));
				}

				if(res.IsInvalidOldPassword.HasValue && res.IsInvalidOldPassword.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_InvalidPassword, "WS-UserRegister.InvalidOldPassword"));
				}

				if(res.IsInvalidNewPasswordConfirm.HasValue && res.IsInvalidNewPasswordConfirm.Value)
				{
					l.Add(new ResultMessage(Resources.UserRegister_InvalidNewPasswordConfirm, "WS-UserRegister.InvalidNewPasswordConfirm"));
				}

				if(res.IsInvalidUserId.HasValue && res.IsInvalidUserId.Value)
				{
					l.Add(new ResultMessage(Resources.IdentityRepository_IsInvalidUserId, "WS-IdentityRepository_IsInvalidUserId"));
				}

				if(res.IsUnchangedNewPassword.HasValue && res.IsUnchangedNewPassword.Value)
				{
					l.Add(new ResultMessage(Resources.UserPasswordReset_UnchangedNewPassword, "WS-UserPasswordReset_UnchangedNewPassword"));
				}

				if(res.IsUserLocked.HasValue && res.IsUserLocked.Value)
				{
					l.Add(new ResultMessage(Resources.IdentityRepository_UserLocked, "WS-IdentityRepository_UserLocked"));
				}

				if(res.NewPasswordErrors != null)
				{
					l.AddRange(CollectNewPasswordErrors(res.NewPasswordErrors));
				}
			}
			return l;
		}
	}
}