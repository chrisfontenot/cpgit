using System;

namespace CredAbility.Data.Identity
{
    public class IdentityClientException : Exception
    {
        public IdentityClientException(string message): base(message){}
    }
}