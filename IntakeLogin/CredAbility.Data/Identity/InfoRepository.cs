﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CredAbility.Core.ActionPlan;
using CredAbility.Core.Common;
using CredAbility.Core.Document;
using CredAbility.Core.Identity;
using CredAbility.Core.Client;
using CredAbility.Data.ActionPlan;
using CredAbility.Data.Services.DMP;
using CredAbility.Data.Services.Identity;
using CredAbility.Data.Services.Document;
using CredAbility.Data.Services.Info;

namespace CredAbility.Data.Identity
{
    public static class InfoRepository
    {
        public static FeeWaiverStatus GetFeeWaiverStatus(ClientProfile clientProfile)
        {
            FeeWaiverStatus result = FeeWaiverStatus.Unknown;
            if ((clientProfile.ClientAccounts != null) && (clientProfile.ClientAccounts.Count > 0))
            {
                ClientAccount eligibleAccount = null;

                // get the first occurrence of a BR.CAM or WS.CAM account
                foreach (ClientAccount ca in clientProfile.ClientAccounts)
                {
                    if ((ca.AccountName == "BR.CAM") || (ca.AccountName == "WS.CAM"))
                    {
                        eligibleAccount = ca;
                        break;
                    }
                }

                if (eligibleAccount != null)
                {
                    long id = eligibleAccount.InternetId;
                    string accountType = eligibleAccount.AccountName;

                    Services.Info.CredabilityNurunClient client = new CredabilityNurunClient();
                    Services.Info.FeeWaiverInfo feeWaiverInfo = client.FeeWaiverInfoGet(id, accountType);
                    client.Close();

                    if ((feeWaiverInfo != null) && (feeWaiverInfo.IsApproved.HasValue))
                    {
                        if (feeWaiverInfo.IsApproved.Value)
                        {
                            result = FeeWaiverStatus.Approved;
                        }
                        else
                        {
                            result = FeeWaiverStatus.Denied;
                        }
                    }
                    else
                    {
                        result = FeeWaiverStatus.NeverApplied;
                    }
                }
            }
            return result;
        }
    }

    public enum FeeWaiverStatus
    {
        Unknown,
        NeverApplied,
        Approved,
        Denied
    }
}
