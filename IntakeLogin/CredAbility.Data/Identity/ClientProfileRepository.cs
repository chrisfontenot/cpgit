﻿using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Core.Document;
using CredAbility.Data.Document;
using CredAbility.Data.Services.Identity;
using StructureMap;

namespace CredAbility.Data.Identity
{
	public static class ClientProfileRepository
	{
		public static string GetDocumentTokenId(ClientProfile clientProfile)
		{
			string documentTokenId = null;
			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				string holdToken = Item.GetDocumentTokenId(clientProfile);
				if(!string.IsNullOrEmpty(holdToken) && string.IsNullOrEmpty(documentTokenId))
				{
					documentTokenId = holdToken;
				}
			}
			return documentTokenId;
		}

		public static DocumentList GetClientDocuments(ClientProfile clientProfile)
		{
			var documentList = new DocumentList();
			if(clientProfile.DocumentTokenID == null)
			{
				clientProfile.DocumentTokenID = GetDocumentTokenId(clientProfile);
			}

			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				var resultlist = Item.GetClientDocuments(clientProfile);
				if(resultlist.Count > 0)
					documentList.AddRange(resultlist);
			}
			return documentList;
		}

		public static ClientCounselingSummaryInfo GetClientCounselingSummaryInfo(ClientProfile clientProfile)
		{
			ClientCounselingSummaryInfo Back = null;
			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				var Hold = Item.GetClientCounselingSummaryInfo(clientProfile);
				if(Hold != null)
					if(Hold.IsSuccessful && Back == null)
						Back = Hold;
			}
			if(Back == null)
				Back = new ClientCounselingSummaryInfo { IsSuccessful = false };
			return Back;
		}

		public static ClientWebsiteStatus GetLastIncompleteWebsite(ClientProfile clientProfile)
		{
			ClientWebsiteStatus wi = new ClientWebsiteStatus();
			CustomResponse<UserProfile> response = IdentityRepository.GetUserWebsites(clientProfile.ClientID);
			if(!response.HasError)
			{
				UserProfile userProfile = response.ResponseValue;
				if((userProfile.UserWebsites != null) && (userProfile.UserWebsites.Length > 0) && (userProfile.UserWebsites[0].WebsiteCode != "OFF"))
				{
					foreach(UserWebsite uw in userProfile.UserWebsites)
					{
						//NuRun comented out this part.
						//if (uw.PercentComplete < 100)
						//{
						wi.WebsiteCode = uw.WebsiteCode;
						wi.PercentComplete = uw.PercentComplete;
						if(uw.CompletedDate.HasValue)
							wi.CompletedDate = uw.CompletedDate.Value;
						if(!string.IsNullOrEmpty(uw.LastCompletedPage))
						{
							wi.Url = uw.LastCompletedPage;
							if(wi.Url.Contains("?"))
							{
								int position = wi.Url.IndexOf("?");
								wi.Url = wi.Url.Substring(0, position);
							}
							wi.Url = wi.Url + string.Format("?u={0}&t={1}", clientProfile.ClientID, clientProfile.IdentityTokenID);
						}
						return wi;
						//}
					}
				}
			}
			return null;
		}

		public static ClientWebsiteStatus GetLastVisitedWebsite(ClientProfile clientProfile)
		{
			ClientWebsiteStatus wi = new ClientWebsiteStatus();
			CustomResponse<UserProfile> response = IdentityRepository.GetUserWebsites(clientProfile.ClientID);
			if(!response.HasError)
			{
				UserProfile userProfile = response.ResponseValue;
				if((userProfile.UserWebsites != null) && (userProfile.UserWebsites.Length > 0) && (userProfile.UserWebsites[0].WebsiteCode != "OFF"))
				{
					foreach(UserWebsite uw in userProfile.UserWebsites)
					{
						wi.WebsiteCode = uw.WebsiteCode;
						wi.PercentComplete = uw.PercentComplete;
						if(uw.CompletedDate.HasValue)
							wi.CompletedDate = uw.CompletedDate.Value;
						if(!string.IsNullOrEmpty(uw.LastCompletedPage))
						{
							wi.Url = uw.LastCompletedPage;
							if(wi.Url.Contains("?"))
							{
								int position = wi.Url.IndexOf("?");
								wi.Url = wi.Url.Substring(0, position);
							}
							wi.Url = wi.Url + string.Format("?u={0}&t={1}", clientProfile.ClientID, clientProfile.IdentityTokenID);
						}
						return wi;
					}
				}
			}
			return null;
		}

		public static string GetDocumentUrl(ClientProfile clientProfile, string docId)
		{
			string DocUrl = string.Empty, HoldUrl = string.Empty;
			var Repositors = ObjectFactory.GetAllInstances<IPrefilingDocResolver>();
			foreach(var Item in Repositors)
			{
				HoldUrl = Item.GetDocumentUrl(clientProfile, docId);
				if(!string.IsNullOrEmpty(HoldUrl) && string.IsNullOrEmpty(DocUrl))
					DocUrl = HoldUrl;
			}
			return DocUrl;
		}
	}
}
