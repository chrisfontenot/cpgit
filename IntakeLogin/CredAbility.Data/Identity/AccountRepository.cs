using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Data.Services.Identity;

namespace CredAbility.Data.Identity
{
    public static class AccountRepository
    {
        public static UserRegisterResult CreateAccount(ClientProfile profile, string confirmPassword)
        {
            ActionResult result = new ActionResult();

            Services.Identity.IdentityClient identityClient = new IdentityClient();
            Services.Identity.UserRegistration ur = new UserRegistration();

            ur.Username = profile.Username;
            ur.FirstName = profile.FirstName;
            ur.LastName = profile.LastName;
            ur.Password = profile.Password;
            ur.PasswordConfirm = confirmPassword;
            ur.LanguageCode = profile.PreferredLanguage;
            ur.Email = profile.EmailAddress;

            Services.Identity.SecurityQuestionAnswer sqa1 = new SecurityQuestionAnswer();
            sqa1.SecurityQuestionId = byte.Parse(profile.SecurityQuestion);
            sqa1.SecurityAnswer = profile.SecurityAnswer;

            ur.SecurityQuestionAnswer = sqa1;

            UserRegisterResult userRegisterResult = identityClient.UserRegister(ur);
            identityClient.Close();

            return userRegisterResult;
        }

        public static UserProfileSaveResult SaveProfile(Profile profile)
        {
            return SaveProfile(profile, string.Empty);
        }

        public static UserProfileSaveResult SaveProfile(Profile profile, string hostId)
        {
            CustomResponse<UserProfile> responseUserProfile = IdentityRepository.GetUserProfile(profile.ClientID);
            UserProfile userProfile = DataServicesFactory.AssignUserProfile(profile, responseUserProfile.ResponseValue);

			CustomResponse<CredAbility.Data.Services.Identity.UserDetail> responseUserDetail = IdentityRepository.GetUserDetail(profile.ClientID);
            userProfile.UserDetailPrimary = responseUserDetail.ResponseValue;

            if (!string.IsNullOrEmpty(hostId))
            {
                SetOfflineProfileInformation(userProfile, profile, hostId);
            }

            userProfile.UserDetailPrimary = DataServicesFactory.AssignUserDetail(profile, userProfile.UserDetailPrimary);
            var result = new ActionResult();
            var identityClient = new IdentityClient();
            UserProfileSaveResult userProfileSaveResult = identityClient.UserProfileSave(userProfile, profile.Username);
            identityClient.Close();

            return userProfileSaveResult;
        }

        public static void SetOfflineProfileInformation(UserProfile userProfile, Profile profile, string hostId)
        {
            UserWebsite uw = new UserWebsite();
            uw.WebsiteCode = "OFF";
            userProfile.UserWebsites = new UserWebsite[1];
            userProfile.UserWebsites[0] = uw;

            CredAbility.Data.Services.Identity.Account account = new CredAbility.Data.Services.Identity.Account();
            account.AccountTypeCode = profile.ClientAccounts[0].AccountName;

            account.HostId = hostId;

            account.UserId = long.Parse(profile.ClientID);
            userProfile.Accounts = new CredAbility.Data.Services.Identity.Account[1];
            userProfile.Accounts[0] = account;
        }
    }
}