﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Data;
using CredAbility.Core.ActionPlan;
using CredAbility.Core.Client;
using CredAbility.Data.ActionPlan;

namespace CredAbility.Data.Identity
{
	internal class CounselingSummaryMapper
	{
		private static readonly string[] DOCUMENT_DATA_SECTION_DELIMITER = new string[] { "^^" };
		private static readonly char[] DOCUMENT_DATA_PAIR_DELIMITER = new char[] { '~' };
		private static readonly char[] DOCUMENT_DATA_VALUE_DELIMITER = new char[] { ':' };
		private static readonly string[] DOCUMENT_DATA_RECORD_DELIMITER = new string[] { "!^" };
		private static readonly string[] DOCUMENT_DATA_ACTIONPLANITEM_DELIMITER = new string[] { "!*" };
		private static readonly string[] DOCUMENT_DATA_ACTIONPLANITEMCONTENT_DELIMITER = new string[] { "||" };

		public ClientCounselingSummaryInfo Map(string documentData, string UserId)
		{
			// this is a sample string that we need to parse
			//
			// yes, this is all coming to us in one giant string
			//
			//^^Demographics^^
			//PCPType:FCO~CLIENTEMAIL:user219@cccsinc.org~CLIENTNAME:MARY SMITH219~CLIENTADDRESS1:219 MAPLE LANE~CLIENTADDRESS2:~CLIENTCSZ:BUCHANAN, GA 30113~CLIENTSSN:924792766~DOCDATE:02-07-2008~CSCORE:~CSSCORE:~

			//^^Budget Information^^
			//TOTALNINCOME:1083~BUDGETCODE:1~BUDGETLINE:Mortgage or Rent Payments~BUDGETAMOUNT:700~BUDGETPERCENT:41\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:4~BUDGETLINE:Life and Medical Insurance~BUDGETAMOUNT:10~BUDGETPERCENT:1\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:2~BUDGETLINE:Car Insurance,Tags,Taxes~BUDGETAMOUNT:76~BUDGETPERCENT:4\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:1~BUDGETLINE:Utilities~BUDGETAMOUNT:250~BUDGETPERCENT:15\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:3~BUDGETLINE:Groceries and Household Items~BUDGETAMOUNT:400~BUDGETPERCENT:24\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:2~BUDGETLINE:Gas,Car Maintenance,Public Transportation~BUDGETAMOUNT:100~BUDGETPERCENT:6\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:4~BUDGETLINE:Child and Elder Care~BUDGETAMOUNT:300~BUDGETPERCENT:18\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:4~BUDGETLINE:Other Expenses~BUDGETAMOUNT:20~BUDGETPERCENT:1\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:1~BUDGETLINE:Home Maintenance,Security System, etc.~BUDGETAMOUNT:50~BUDGETPERCENT:3\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:4~BUDGETLINE:Clothing~BUDGETAMOUNT:50~BUDGETPERCENT:3\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:4~BUDGETLINE:Beauty and Barber~BUDGETAMOUNT:40~BUDGETPERCENT:2\n~!^
			//TOTALNINCOME:1083~BUDGETCODE:5~BUDGETLINE:Monthly Full-Time Income~BUDGETAMOUNT:1083.33~BUDGETPERCENT:0\n~

			//^^Debt Information^^
			//DMPPAYMENT:10~DMPPAYMENT:15~DMPPAYMENT:30~DMPPAYMENT:12~DMPPAYMENT:16~DMPPAYMENT:10~DMPPAYMENT:44~DMPPAYMENT:117~DMPPAYMENT:24~DMPPAYMENT:16~DMPPAYMENT:10~DMPPAYMENT:17~DMPPAYMENT:10~

			//^^Counselor Analyais^^
			//^^Client Goals^^

			//^^Recommendations^^
			//ACTIONPLANITEM:Contact Select Portfolio Servicing directly to discuss other workout options. We will be doing this during a second session.~

			//^^Recommendations^^
			//^^ACTIONPLANITEM:this is a test
			//~ACTIONTEXT:
			//Build a Priority Budget||||1||Complete the section on "Your Goals" in this package!*
			//Build a Priority Budget||||2||Find a prominent place to post your goals in your home as a reminder!*
			//Build a Priority Budget||||3||Share your goals with a trusted friend and ask him or her to help you act consistently so that you can achieve them!*
			//Protect Your Home-Test1||||4||Review your income and expenses carefully to find ways to get your budget in balance!*
			//Protect Your Home-Test1||||6||For good basic money management information, check out the Money Smarts section of our website at www.cccsinc.org!*
			//Protect Your Home-Test1||||7||Remember that your mortgage or rent, car and utilities are priority debts. Create a budget that reflects these priorities.!*
			//Protect Your Home-Test1||jhjh||8||Your first and most important monthly payment is your house payment. Always pay your mortgage/rent first!*
			//Protect Your Home-Test1||jhjh||9||If you have fallen behind on your mortgage payments, it is important to know that there are options available to help you save your home and avoid foreclosure. Contact the mortgage company to review your options and identify the one that will work best fo

			//^^Resource Sheets^^
			//^^Counselor Information^^

			//^^Personal Assets^^
			//TOTASSETS:117000~TOTLIABILITIES:96382\n~

			//^^Graph^^
			//^^Survey^^

			ClientCounselingSummaryInfo info = new ClientCounselingSummaryInfo();
			info.IsSuccessful = true;
			info.AsOfDate = DateTime.Now;

			List<string> dataCategories = new List<string> { "Demographics", "Budget Information", "Debt Information", "Counselor Analyais", "Client Goals", "Recommendations", "Resource Sheets", "Counselor Information", "Personal Assets", "Graph", "Survey" };

			// first, break out the sections
			string[] sections = documentData.Split(DOCUMENT_DATA_SECTION_DELIMITER, StringSplitOptions.None);

			// second, break each section down and extract the values
			for(int i = 0; i < sections.Length; i++)
			{
				if(!dataCategories.Contains(sections[i]))
				{
					if((sections[i] != "") && (sections[i - 1] != ""))
					{
						switch(sections[i - 1])
						{
							case "Demographics":
								info.Demographics = ProcessDemographics(sections[i]);
								break;
							case "Budget Information":
								info.ExpenseInfo = ProcessExpenses(sections[i]);
								break;
							case "Debt Information":
								info.DebtInfoList = ProcessDebtInfo(sections[i]);
								break;
							case "Counselor Analyais":
								ProcessCounselorAnalysisInfo(sections[i]);
								break;
							case "Client Goals":
								ProcessClientGoalsInfo(sections[i]);
								break;
							case "Recommendations":
								info.ActionItems = ProcessRecommendationsInfo(sections[i],UserId);
								break;
							case "Resource Sheets":
								ProcessResourceSheetsInfo(sections[i]);
								break;
							case "Counselor Information":
								ProcessCounselorInfo(sections[i]);
								break;
							case "Personal Assets":
								info.PersonalAssetsInfo = ProcessPersonalAssetsInfo(sections[i]);
								break;
							case "Graph":
								ProcessGraphInfo(sections[i]);
								break;
							case "Survey":
								ProcessSurveyInfo(sections[i]);
								break;
							default:
								break;
						}
					}
				}
			}

			// calculate personal assets info
			//TotalAssets: from web service
			//Total Liabilities: from web service
			//NWORTH : TotalAssets - TotalLiabilities
			//TOTExpenses: HousingTotal + TransportationTotal + FoodTotal + OtherTotal
			//TOTDTPayment : Total Of DMP Payment
			//CASHMONEND: (BudgetIncome - TOTExpenses) - TOTDTPayment
			info.PersonalAssetsInfo.TotalNetWorth = info.PersonalAssetsInfo.TotalAssets - info.PersonalAssetsInfo.TotalLiabilities;
			info.PersonalAssetsInfo.TotalIncome = info.ExpenseInfo.TotalIncome;
			info.PersonalAssetsInfo.TotalExpenses = info.ExpenseInfo.TotalExpenses; //info.ExpenseCategories info.PersonalAssetsInfo.TotalAssets - info.PersonalAssetsInfo.TotalLiabilities;

			decimal totalDebtPayment = 0.00M;
			foreach(DebtInfo debtInfo in info.DebtInfoList)
			{
				totalDebtPayment += debtInfo.Amount;
			}

			info.PersonalAssetsInfo.TotalDebtPayment = totalDebtPayment;
			info.PersonalAssetsInfo.CashAmountMonthEnd = info.PersonalAssetsInfo.TotalIncome - info.PersonalAssetsInfo.TotalExpenses - info.PersonalAssetsInfo.TotalDebtPayment;

			if(info.Demographics.DocumentDate != null)
			{
				info.AsOfDate = info.Demographics.DocumentDate;
			}

			return info;
		}


		private DemographicInfo ProcessDemographics(string data)
		{
			// sample
			//PCPType:FCO~CLIENTEMAIL:user219@cccsinc.org~CLIENTNAME:MARY SMITH219~CLIENTADDRESS1:219 MAPLE LANE~CLIENTADDRESS2:~CLIENTCSZ:BUCHANAN, GA 30113~CLIENTSSN:924792766~DOCDATE:02-07-2008~CSCORE:~CSSCORE:~

			DemographicInfo di = new DemographicInfo();
			string[] pairs = data.Split(DOCUMENT_DATA_PAIR_DELIMITER, StringSplitOptions.RemoveEmptyEntries);
			foreach(string token in pairs)
			{
				string[] pair = token.Split(DOCUMENT_DATA_VALUE_DELIMITER);
				switch(pair[0])
				{
					case "PCPType":
						di.PCPType = pair[1];
						break;
					case "CLIENTEMAIL":
						di.Email = pair[1];
						break;
					case "CLIENTNAME":
						di.Name = pair[1];
						break;
					case "CLIENTADDRESS1":
						di.Address1 = pair[1];
						break;
					case "CLIENTADDRESS2":
						di.Address2 = pair[1];
						break;
					case "DOCDATE":
						di.DocumentDate = DateTime.Parse(pair[1], new CultureInfo("en-US"));
						break;
					case "CLIENTSSN":
						di.SSN = pair[1];
						break;
					case "CSCORE":
						if(pair[1] != "")
							di.CreditScore = Int32.Parse(pair[1]);
						break;
					case "CSSCORE":
						di.CreditScoreText = pair[1];
						break;
					default:
						break;
				}
			}
			return di;
		}

		private ExpenseInfo ProcessExpenses(string data)
		{
			// sample
			//TOTALNINCOME:1083~BUDGETCODE:1~BUDGETLINE:Mortgage or Rent Payments~BUDGETAMOUNT:700~BUDGETPERCENT:41\n~!^

			ExpenseInfo expenseInfo = new ExpenseInfo();
			List<ExpenseCategory> expenseCategoryList = new List<ExpenseCategory>();
			decimal totalIncome = 0.00M;

			expenseInfo.IncomeAndExpenseRawData = new DataTable();
			expenseInfo.IncomeAndExpenseRawData.Columns.Add("TOTALNINCOME", typeof(string));
			expenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETCODE", typeof(string));
			expenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETLINE", typeof(string));
			expenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETAMOUNT", typeof(string));
			expenseInfo.IncomeAndExpenseRawData.Columns.Add("BUDGETPERCENT", typeof(string));

			string[] records = data.Split(DOCUMENT_DATA_RECORD_DELIMITER, StringSplitOptions.None);

			foreach(string record in records)
			{
				string[] pairs = record.Split(DOCUMENT_DATA_PAIR_DELIMITER, StringSplitOptions.RemoveEmptyEntries);

				decimal amount = 0.00M;
				string categoryId = "";
				string expenseItemName = "";
				decimal percentage = 0.00M;

				foreach(string token in pairs)
				{
					string[] pair = token.Split(DOCUMENT_DATA_VALUE_DELIMITER);

					switch(pair[0])
					{
						case "BUDGETAMOUNT":
							decimal.TryParse(pair[1], out amount);
							break;
						case "TOTALNINCOME":
							decimal.TryParse(pair[1], out totalIncome);
							break;
						case "BUDGETCODE":
							categoryId = pair.Length > 0 ? pair[1] : "";
							break;
						case "BUDGETLINE":
							expenseItemName = pair.Length > 0 ? pair[1] : "";
							break;
						case "BUDGETPERCENT":
							decimal.TryParse(pair[1], out percentage);
							break;
						default:
							break;
					}

                    if (pair[0] == "BUDGETPERCENT")
                    {
					expenseInfo.IncomeAndExpenseRawData.Rows.Add(totalIncome.ToString(), categoryId, expenseItemName, amount, percentage);
					ExpenseItem ei = new ExpenseItem(expenseItemName, amount);
					int itemIndex = expenseCategoryList.FindIndex(Item => Item.Id == categoryId);
					if(itemIndex < 0)
					{
						if(categoryId != "5")
						{
							ExpenseCategory ec = new ExpenseCategory();
							ec.Id = categoryId;
							switch(categoryId)
							{
								case "1":
									ec.Name = "Housing";
									break;
								case "2":
									ec.Name = "Transportation";
									break;
								case "3":
									ec.Name = "Food";
									break;
								default:
									ec.Name = "Other";
									break;
							}
							ec.Percentage = percentage;
							ec.ExpenseItems.Add(ei);
							expenseCategoryList.Add(ec);
						}
					}
					else
					{
						expenseCategoryList[itemIndex].ExpenseItems.Add(ei);
					}

                        // reset all values
                        amount = 0.00M;
                        categoryId = "";
                        expenseItemName = "";
                        percentage = 0.00M;
				}
			}
            }

			expenseInfo.ExpenseCategories = expenseCategoryList;
			expenseInfo.TotalIncome = totalIncome;
			return expenseInfo;
		}

		private List<DebtInfo> ProcessDebtInfo(string data)
		{
			// sample
			//DMPPAYMENT:10~DMPPAYMENT:15~DMPPAYMENT:30~DMPPAYMENT:12~DMPPAYMENT:16~DMPPAYMENT:10~DMPPAYMENT:44~DMPPAYMENT:117~DMPPAYMENT:24~DMPPAYMENT:16~DMPPAYMENT:10~DMPPAYMENT:17~DMPPAYMENT:10~

			List<DebtInfo> debtInfoList = new List<DebtInfo>();
			string[] pairs = data.Split(DOCUMENT_DATA_PAIR_DELIMITER, StringSplitOptions.RemoveEmptyEntries);
			foreach(string token in pairs)
			{
				string[] pair = token.Split(DOCUMENT_DATA_VALUE_DELIMITER);
				DebtInfo di = new DebtInfo();
				di.Type = pair[0];
				if(pair[1] != "")
				{
					di.Amount = Int32.Parse(pair[1]);
				}
				debtInfoList.Add(di);
			}
			return debtInfoList;
		}

		private void ProcessCounselorAnalysisInfo(string data)
		{

		}

		private void ProcessClientGoalsInfo(string data)
		{

		}

		private List<ActionItem> ProcessRecommendationsInfo(string data, string UserId)
		{
			// format
			//ACTIONPLANITEM:<>~ACTIONPLANCODE:MainTitle||SubTitle||Message!*MainTitle||SubTitle||Message

			// sample
			//^^ACTIONPLANITEM:this is a test
			//~ACTIONTEXT:
			//Build a Priority Budget||||1||Complete the section on "Your Goals" in this package!*
			//Build a Priority Budget||||2||Find a prominent place to post your goals in your home as a reminder!*
			//Build a Priority Budget||||3||Share your goals with a trusted friend and ask him or her to help you act consistently so that you can achieve them!*
			//Protect Your Home-Test1||||4||Review your income and expenses carefully to find ways to get your budget in balance!*
			//Protect Your Home-Test1||||6||For good basic money management information, check out the Money Smarts section of our website at www.cccsinc.org!*
			//Protect Your Home-Test1||||7||Remember that your mortgage or rent, car and utilities are priority debts. Create a budget that reflects these priorities.!*
			//Protect Your Home-Test1||jhjh||8||Your first and most important monthly payment is your house payment. Always pay your mortgage/rent first!*
			//Protect Your Home-Test1||jhjh||9||If you have fallen behind on your mortgage payments, it is important to know that there are options available to help you save your home and avoid foreclosure. Contact the mortgage company to review your options and identify the one that will work best fo

			// stub data because the live web service isn't ready yet
			//data = "ACTIONPLANITEM:this is a test~ACTIONTEXT:Build a Priority Budget||||1||Complete the section on \"Your Goals\" in this package!*Build a Priority Budget||||2||Find a prominent place to post your goals in your home as a reminder!*Build a Priority Budget||||3||Share your goals with a trusted friend and ask him or her to help you act consistently so that you can achieve them!*Protect Your Home-Test1||||4||Review your income and expenses carefully to find ways to get your budget in balance!*Protect Your Home-Test1||||6||For good basic money management information, check out the Money Smarts section of our website at www.cccsinc.org!*Protect Your Home-Test1||||7||Remember that your mortgage or rent, car and utilities are priority debts. Create a budget that reflects these priorities.!*Protect Your Home-Test1||jhjh||8||Your first and most important monthly payment is your house payment. Always pay your mortgage/rent first!*Protect Your Home-Test1||jhjh||9||If you have fallen behind on your mortgage payments, it is important to know that there are options available to help you save your home and avoid foreclosure. Contact the mortgage company to review your options and identify the one that will work best fo";

			List<ActionItem> actionItemList = new List<ActionItem>();

			string[] pairs = data.Split(DOCUMENT_DATA_PAIR_DELIMITER, StringSplitOptions.RemoveEmptyEntries);
			foreach(string token in pairs)
			{
				string[] pair = token.Split(DOCUMENT_DATA_VALUE_DELIMITER);

				if(pair[0] == "ACTIONTEXT")
				{
					string[] lineItemsArray = pair[1].Split(DOCUMENT_DATA_ACTIONPLANITEM_DELIMITER, StringSplitOptions.None);
					foreach(string lineItem in lineItemsArray)
					{
						string[] lineItemContents = lineItem.Split(DOCUMENT_DATA_ACTIONPLANITEMCONTENT_DELIMITER, StringSplitOptions.None);
						ActionItem ai = new ActionItem();
						ai.Title = lineItemContents[0];
						ai.Id = Int32.Parse(lineItemContents[2]);
						ai.Text = lineItemContents[3];
						ai.CompletionDate = ActionPlanRepository.GetActionItemCompletionDate(UserId, ai.Id);
						actionItemList.Add(ai);
					}
				}

			}
			return actionItemList;
		}

		private void ProcessResourceSheetsInfo(string data)
		{

		}

		private void ProcessCounselorInfo(string data)
		{

		}

		private PersonalAssetsInfo ProcessPersonalAssetsInfo(string data)
		{
			// sample
			//TOTASSETS:117000~TOTLIABILITIES:96382\n~

			PersonalAssetsInfo pai = new PersonalAssetsInfo();
			string[] pairs = data.Split(DOCUMENT_DATA_PAIR_DELIMITER, StringSplitOptions.RemoveEmptyEntries);
			foreach(string token in pairs)
			{
				string[] pair = token.Split(DOCUMENT_DATA_VALUE_DELIMITER);
				switch(pair[0])
				{
					case "TOTASSETS":
						pai.TotalAssets = decimal.Parse(pair[1]);
						break;
					case "TOTLIABILITIES":
						pai.TotalLiabilities = decimal.Parse(pair[1]);
						break;
					default:
						break;
				}
			}

			return pai;
		}

		private void ProcessGraphInfo(string data)
		{

		}

		private void ProcessSurveyInfo(string data)
		{

		}
	}
}