﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using CredAbility.Core.Common;
using CredAbility.Core.Identity;
using CredAbility.Core.DMP;
using CredAbility.Data.Services.DMP;

namespace CredAbility.Data.DMP
{
	public static class DMPRepository
	{
		public static CustomResponse<ResultUserInfo> GetUserInfo(string clientId, string ssn)
		{
			var response = new CustomResponse<ResultUserInfo>();
			MyAccntSoapClient client = new Services.DMP.MyAccntSoapClient();
			ResultUserInfo resultUserInfo = client.GetUserInfo(clientId, ssn);
			if(resultUserInfo.ReturnCode == 0)
			{
				response.ResponseValue = resultUserInfo;
			}
			else
			{
				response.HasError = true;
				response.ErrorMessage = resultUserInfo.ReturnMessage;
				response.ErrorCode = resultUserInfo.ReturnCode.ToString();
			}
			return response;
		}

		public static CustomResponse<AttorneyList> GetAttorneyList(string zipCode)
		{
			var response = new CustomResponse<AttorneyList>();
			Services.DMP.MyAccntSoapClient client = new Services.DMP.MyAccntSoapClient();

			XElement result = client.GetAtty(zipCode);
			client.Close();

			if(result.Element("ReturnCode").Value == "0")
			{
				AttorneyList attorneyList = BuildAttorneyList(result.Element("Attorneys").Elements("Attorney"));
				response.ResponseValue = attorneyList;
			}
			else
			{
				response.HasError = true;
				response.ErrorMessage = result.Element("ReturnCode").Value;
			}
			return response;
		}

		private static AttorneyList BuildAttorneyList(IEnumerable<XElement> list)
		{
			AttorneyList attorneyList = new AttorneyList();

			foreach(XElement xe in list)
			{
				AttorneyInfo attorney = new AttorneyInfo();
				attorney.FirmName = xe.Element("FirmName").Value;
				attorney.FirmCode = xe.Element("FirmCode").Value;
				attorney.LocationPhone = xe.Element("LocationPhone").Value;
				attorney.MainPhone = xe.Element("MainPhone").Value;
				attorney.Url = xe.Element("Url").Value;
				attorney.AddressLine1 = xe.Element("AddressLine1").Value;
				attorney.AddressLine2 = xe.Element("AddressLine2").Value;
				attorney.City = xe.Element("City").Value;
				attorney.State = xe.Element("State").Value;
				attorney.Zip = xe.Element("ZipCode").Value;
				attorney.EstimateDistance = xe.Element("EstimatedDistance").Value;
				attorneyList.Add(attorney);
			}
			return attorneyList;
		}

		public static CustomResponse<DMPInfo> GetDMPInfo(string dmpAccessId, DateTime startDate, DateTime endDate)
		{
			CustomResponse<DMPInfo> response = new CustomResponse<DMPInfo>();
			Services.DMP.MyAccntSoapClient client = new Services.DMP.MyAccntSoapClient();
			try
			{
				XElement result = client.GetDmp(dmpAccessId, startDate, endDate);
				client.Close();
				if(result.Element("ReturnCode").Value == "0")
				{
					DMPInfo dmpInfo = BuildDMPInfo(result);
					if(dmpInfo != null)
					{
						dmpInfo.IsSuccessful = true;
						response.ResponseValue = dmpInfo;
					}
					else
					{
						response.HasError = true;
						response.ErrorMessage = "Unable to get DMP information";
					}
				}
				else
				{
					response.HasError = true;
					response.ErrorMessage = result.Element("ReturnMessage").Value;
				}
			}
			catch(Exception ex)
			{
				response.HasError = true;
				response.ErrorMessage = ex.Message;
			}
			return response;
		}

		private static DMPInfo BuildDMPInfo(XElement element)
		{
			DMPInfo dmpInfo = new DMPInfo();
			DMPStatus dmpStatus = new DMPStatus();
			NumberFormatInfo nfi = new NumberFormatInfo();
			nfi.NumberDecimalDigits = 2;

			// get attributes
			if(!string.IsNullOrEmpty(element.Element("DmpOrignalAmount").Value))
			{
				dmpStatus.DmpOriginalAmount = decimal.Parse(element.Element("DmpOrignalAmount").Value, nfi);
			}
			else
			{
				dmpStatus.DmpOriginalAmount = 0;
			}

			if(!string.IsNullOrEmpty(element.Element("DmpAmountDue").Value))
			{
				dmpStatus.DmpAmountDue = decimal.Parse(element.Element("DmpAmountDue").Value, nfi);
			}
			else
			{
				dmpStatus.DmpAmountDue = 0;
			}

			if(!string.IsNullOrEmpty(element.Element("DmpTotalPTD").Value))
			{
				dmpStatus.DmpTotalPTD = decimal.Parse(element.Element("DmpTotalPTD").Value, nfi);
			}
			else
			{
				dmpStatus.DmpTotalPTD = 0;
			}

			if(!string.IsNullOrEmpty(element.Element("DmpAutoPay").Value))
			{
				dmpStatus.DmpAutoPay = element.Element("DmpAutoPay").Value;
			}
			else
			{
				dmpStatus.DmpAutoPay = "";
			}

			if(!string.IsNullOrEmpty(element.Element("DmpStartDate").Value))
			{
				dmpStatus.DmpStartDate = DateTime.Parse(element.Element("DmpStartDate").Value, new CultureInfo("en-US"));
			}

			if(!string.IsNullOrEmpty(element.Element("DmpNextDueDate").Value))
			{
				dmpStatus.DmpNextDueDate = DateTime.Parse(element.Element("DmpNextDueDate").Value, new CultureInfo("en-US"));
			}

			if(!string.IsNullOrEmpty(element.Element("TotalDebtAmount").Value))
			{
				dmpStatus.TotalDebtAmount = decimal.Parse(element.Element("TotalDebtAmount").Value, nfi);
			}
			else
			{
				dmpStatus.TotalDebtAmount = 0;
			}

			dmpInfo.DmpStatus = dmpStatus;
			// get activity
			IEnumerable<XElement> elementList = element.Elements("Transactions").Elements("Activity");
			if(elementList != null)
			{
				DMPActivityList dmpActivityList = new DMPActivityList();
				foreach(XElement e in elementList)
				{
					DMPActivity dmpActivity = new DMPActivity();
					dmpActivity.ActivityAmount = decimal.Parse(e.Element("ActivityAmount").Value, nfi);
					dmpActivity.ActivityDate = DateTime.Parse(e.Element("ActivityDate").Value, new CultureInfo("en-US"));
					dmpActivity.ActivityType = e.Element("ActivityType").Value;
					dmpActivityList.Add(dmpActivity);
				}
				dmpInfo.DmpActivityList = dmpActivityList;
			}
			return dmpInfo;
		}
	}
}