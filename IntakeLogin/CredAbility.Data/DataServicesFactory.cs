﻿using System;
using System.Configuration;
using System.Collections.Generic;
using CredAbility.Core.Common;
using CredAbility.Core.Document;
using CredAbility.Core.Client;
using CredAbility.Core.ActionPlan;
using CredAbility.Data.CreditScoreChallenge;
using CredAbility.Data.Document;
using CredAbility.Data.Services.Identity;
using CredAbility.Data.Services.Document;
using CredAbility.Data.DMP;
using CredAbility.Core.DMP;

namespace CredAbility.Data
{
    /// <summary>
    /// Factory class for Data to Logic Object
    /// </summary>
    public static class DataServicesFactory
    {
        public static List<Core.Identity.SecurityQuestion> GetSecurityQuestions(Data.Services.Identity.SecurityQuestion[] questions)
        {
            var result = new List<Core.Identity.SecurityQuestion>(questions.Length);

            foreach (var question in questions)
            {
                result.Add(GetSecurityQuestion(question));
            }
            return result;
        }

        public static Core.Identity.SecurityQuestion GetSecurityQuestion(Data.Services.Identity.SecurityQuestion question)
        {
            var result = new Core.Identity.SecurityQuestion();
            result.Text = question.Text;
            result.ID = question.SecurityQuestionId;
            return result;
        }

        public static ClientProfile GetClientProfile(UserProfile userProfile, ClientProfile clientProfile)
        {
            clientProfile.Username = userProfile.Username;
            clientProfile.FirstName = userProfile.UserDetailPrimary.FirstName;
            clientProfile.LastName = userProfile.UserDetailPrimary.LastName;
            clientProfile.Avatar = userProfile.Avatar;
            clientProfile.EmailAddress = userProfile.UserDetailPrimary.Email;
            clientProfile.PreferredLanguage = userProfile.LanguageCode;
            clientProfile.IsProfileUpdateRequired = userProfile.IsProfileUpdateRequired;

            if ((userProfile.LastLoginDate.HasValue) && (clientProfile.LastLogin == DateTime.MinValue))
            {
                clientProfile.LastLogin = userProfile.LastLoginDate.Value;
            }

            if (userProfile.PasswordResetDate.HasValue)
                clientProfile.PasswordResetDate = userProfile.PasswordResetDate.Value;

            if (userProfile.SecurityQuestionAnswer != null)
            {
                clientProfile.SecurityQuestion = userProfile.SecurityQuestionAnswer.Text;
                clientProfile.SecurityAnswer = userProfile.SecurityQuestionAnswer.SecurityAnswer;
            }

            if ((userProfile.Accounts != null) && (userProfile.Accounts.Length > 0))
            {
                //BR.CAM = bankruptcy counseling appt file
                //CM = client master file which contains active DMP clients or those who would use the Client Access site.
                //CAM = budget and credit, housing related appointment clients.
                //WS.CAM = Bankruptcy Education clients ( those using MIM)

                clientProfile.ClientAccounts = new List<ClientAccount>();
                foreach (Data.Services.Identity.Account account in userProfile.Accounts)
                {
                    //Transforme the account into code value to be transform for Sitecore in Virtual User
                    clientProfile.Types.Add(account.AccountTypeCode);
                    //account.InternetId is a nullable type and can have a null value
                    var internetId = account.InternetId.HasValue ? account.InternetId.Value : 0;
                    clientProfile.ClientAccounts.Add(new ClientAccount(account.AccountTypeCode, internetId, account.HostId));

                    if (!string.IsNullOrEmpty(account.HostId))
                    {
                        if (string.IsNullOrEmpty(clientProfile.DocumentAccessID)
                            && ((account.AccountTypeCode == "CAM") ||
                            (account.AccountTypeCode == "BR.CAM") ||
                            (account.AccountTypeCode == "WS.CAM")))
                        {
                            clientProfile.DocumentAccessID = account.HostId;
                        }

                        if (string.IsNullOrEmpty(clientProfile.DMPAccessID) && (account.AccountTypeCode == "CM"))
                        {
                            clientProfile.DMPAccessID = account.HostId;
                        }
                    }
                }
            }

            var contestant = ContestantRepository.GetContestantById(clientProfile.ClientID);
            if(contestant != null)
            {
                clientProfile.IsCreditScoreChallengeContestant = true;
            }

            return clientProfile;
        }

        public static UserProfile AssignUserProfile(Profile profile, UserProfile userProfile)
        {
            userProfile.Username = profile.Username;
            userProfile.LanguageCode = profile.PreferredLanguage;

            Data.Services.Identity.SecurityQuestionAnswer sqa1 = new SecurityQuestionAnswer();
            if (!string.IsNullOrEmpty(profile.SecurityQuestion))
            {
                sqa1.SecurityQuestionId = byte.Parse(profile.SecurityQuestion);
            }
            sqa1.SecurityAnswer = profile.SecurityAnswer;

            userProfile.SecurityQuestionAnswer = sqa1;

            Data.Services.Identity.Address address = new CredAbility.Data.Services.Identity.Address();
            address.StreetLine1 = profile.MailingAddressLine1;
            address.StreetLine2 = profile.MailingAddressLine2;
            address.City = profile.City;
            address.State = profile.State;
            address.Zip = profile.Zip;
            address.AddressType = "Primary";
            address.AddressId = profile.MailingAddressId;
            address.UserId = long.Parse(profile.ClientID);

            CredAbility.Data.Services.Identity.Address[] addresses = new CredAbility.Data.Services.Identity.Address[1];
            addresses[0] = address;
            userProfile.Addresses = addresses;

            userProfile.Avatar = profile.Avatar;

            return userProfile;
        }

		public static CredAbility.Data.Services.Identity.UserDetail AssignUserDetail(Profile profile, CredAbility.Data.Services.Identity.UserDetail userDetail)
        {
            userDetail.FirstName = profile.FirstName;
            userDetail.LastName = profile.LastName;
            userDetail.MiddleName = profile.MiddleName;

            if (!string.IsNullOrEmpty(profile.MaritalStatus))
            {
                MaritalStatus maritalStatus = new MaritalStatus();
                maritalStatus.MaritalStatusCode = profile.MaritalStatus[0];
                userDetail.MaritalStatus = maritalStatus;
            }
            //else
            //{
            //    maritalStatus.MaritalStatusCode = ' ';
            //}

            userDetail.Ssn = profile.SSN.FormatedSSN;
            userDetail.PhoneCell = profile.CellPhone;
            userDetail.PhoneHome = profile.HomePhone;
            userDetail.PhoneWork = profile.WorkPhone;
            userDetail.Email = profile.EmailAddress;

            return userDetail;
        }
    }
}