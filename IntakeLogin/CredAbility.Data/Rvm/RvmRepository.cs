﻿using System.Linq;
using System.Text;

namespace CredAbility.Data.Rvm
{
	public class StateNames
	{
		public string And { get; set; }
		public string Or { get; set; }
	}

	public class RvmRepository
	{
		public static StateNames RvmBlockedStatesGet()
		{
			StateNames BanedStates = new StateNames();
			RegulatedState[] RvmReg = null;
			using(CccsDataContext DataContext = new CccsDataContext())
			{
				try
				{
					RvmReg =
					(
					from RegSt in DataContext.RegulatedStates
					where RegSt.RvmBan == true
					select RegSt
					).ToArray<RegulatedState>();
				}
				catch
				{
				}
			}
			if(RvmReg != null)
			{
				StringBuilder CurAnd = new StringBuilder(), CurOr = new StringBuilder();
				string Con = string.Empty, LastOne = string.Empty;
				foreach(RegulatedState CurName in RvmReg)
				{
					CurAnd.Append(Con);
					CurOr.Append(Con);
					LastOne = CurName.State;
					CurAnd.Append(LastOne);
					CurOr.Append(LastOne);
					Con = Resources.ComaConjunction;
				}
				CurAnd.Replace(Resources.ComaConjunction + LastOne, Resources.AndConjunction + LastOne);
				CurOr.Replace(Resources.ComaConjunction + LastOne, Resources.OrConjunction + LastOne);
				BanedStates.And = CurAnd.ToString();
				BanedStates.Or = CurOr.ToString();
			}
			else
			{
				BanedStates.And = string.Empty;
				BanedStates.Or = string.Empty;
			}
			return BanedStates;
		}
	}
}
