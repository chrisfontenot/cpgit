﻿using System;
using System.Linq;

namespace CredAbility.Data.ActionPlan
{
	public static class ActionPlanRepository
	{
		public static readonly CredAbilityDataContext Context = new CredAbilityDataContext();

		public static DateTime? GetActionItemCompletionDate(string clientId, int actionId)
		{
			var actionItem = Context.CompletedActions.FirstOrDefault(ca => ca.ActionId == actionId && ca.ClientId == clientId);
			return actionItem == null ? (DateTime?)null : actionItem.Date;
		}

		public static bool SaveActionItemCompletion(string clientId, int actionId)
		{
			try
			{
				if(Context.CompletedActions.Any(ca => ca.ActionId == actionId && ca.ClientId == clientId))
				{
					Context.CompletedActions.FirstOrDefault(ca => ca.ActionId == actionId && ca.ClientId == clientId).Date = DateTime.Now;
				}
				else
				{
					var actionItem = new CompletedAction() { ActionId = actionId, ClientId = clientId, Date = DateTime.Now };
					Context.CompletedActions.InsertOnSubmit(actionItem);
				}
				Context.SubmitChanges();
			}
			catch(Exception)// e)
			{
				//TODO: do something with the exception
				return false;
			}
			return true;
		}
	}
}
