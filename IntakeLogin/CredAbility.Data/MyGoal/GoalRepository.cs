﻿using System;
using System.Collections.Generic;
using System.Linq;
using CredAbility.Core.Client;
using CredAbility.Core.MyGoal;

namespace CredAbility.Data.MyGoal
{
    public static class GoalRepository
    {
        private static readonly CredAbilityDataContext Context = new CredAbilityDataContext();

        public static List<Core.MyGoal.Goal> GetClientGoals(ClientProfile clientProfile)
        {
            var goalsResult = new List<Core.MyGoal.Goal>();
            var goals = Context.Goals.Where(g=>g.ClientId == clientProfile.ClientID);
            foreach (var goal in goals)
            {
                var goalHistoryResult = new List<Core.MyGoal.GoalHistory>();

                var goalId = goal.Id;
                var goalHistory = Context.GoalHistories.Where(gh => gh.GoalId == goalId);

                foreach (var history in goalHistory)
                {
                    goalHistoryResult.Add(new Core.MyGoal.GoalHistory(){Id=history.Id,Amount=history.Amount,Date=history.Date});
                }
                
                goalsResult.Add(new Core.MyGoal.Goal()
                                    {
                                        Id = goal.Id,
                                        Name = goal.Name,
                                        Type = (GoalType) goal.Type,
                                        TotalAmount = goal.Amount,
                                        DateCreated = goal.DateCreated,
                                        DueDate = goal.DueDate,
                                        History = goalHistoryResult
                                    });

            }

            return goalsResult;
        }

        public static void AddGoal(ClientProfile clientProfile,Core.MyGoal.Goal newGoal)
        {
            Context.Goals.InsertOnSubmit(new Goal
                                             {
                                                 Name = newGoal.Name,
                                                 ClientId = clientProfile.ClientID,
                                                 Type = (byte) newGoal.Type,
                                                 Amount = newGoal.TotalAmount,
                                                 DateCreated = DateTime.Now,
                                                 DueDate = newGoal.DueDate,
                                             });
            Context.SubmitChanges();
        }

        public static void AddHistory(ClientProfile clientProfile, Core.MyGoal.GoalHistory newGoalHistory, Core.MyGoal.Goal goal)
        {
            Context.GoalHistories.InsertOnSubmit(new GoalHistory
            {
                GoalId = goal.Id,
                Amount = newGoalHistory.Amount,
                Date = newGoalHistory.Date,
            });
            Context.SubmitChanges();
        }

        public static void DeleteGoal(ClientProfile clientProfile, Core.MyGoal.Goal goal)
        {
            var goalHistories = new List<GoalHistory>();
            foreach (var goalHistory in goal.History)
            {
                var goalHistoryId = goalHistory.Id;
                goalHistories.Add(Context.GoalHistories.FirstOrDefault(gh => gh.Id == goalHistoryId));
            }

            Context.GoalHistories.DeleteAllOnSubmit(goalHistories);

            Context.Goals.DeleteOnSubmit(Context.Goals.FirstOrDefault(g=>g.Id == goal.Id));

            Context.SubmitChanges();
        }
    }
}
