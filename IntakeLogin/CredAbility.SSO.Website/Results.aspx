﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="CredAbility.SSO.Website.Results" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Single Sign-On Results</h1>
    <hr />
    <p>To use single sign-on, just click a link below.</p>
    <ul>
        <li><a href="<%= string.Format("http://{3}/~/security2/sso.ashx?u={0}&t={1}&r={2}", this.UserID, this.TokenID, "/my-account/always-available-to-help.aspx", this.Request.Url.Host) %>">Goto a Secure Page</a></li>
        <li><a href="<%= string.Format("http://{3}/~/security2/sso.ashx?u={0}&t={1}&r={2}", this.UserID, this.TokenID, "/about-credability/default.aspx", this.Request.Url.Host) %>">Goto a Non-Secure Page</a></li>
    </ul>
    
    </div>
    </form>
</body>
</html>
