﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Web.Authentification;

namespace CredAbility.SSO.Website
{
    public partial class Results : System.Web.UI.Page
    {

        protected string UserID {
            get{
                return (string.IsNullOrEmpty(Request.QueryString["u"])) ? string.Empty : Request.QueryString["u"];
            }
        }

        protected string TokenID
        {
            get
            {
                return (string.IsNullOrEmpty(Request.QueryString["t"])) ? string.Empty : Request.QueryString["t"];
            }
        }        
    }
}
