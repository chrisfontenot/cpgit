﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CredAbility.SSO.Website._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h1>Single Sign-On Test Site</h1>
    <hr />
    
    <ca:ResultMessages id="ResultMessages" runat="server"/>
    
    <label>Username:</label>
    <asp:TextBox runat="server" ID="Username" />
    
    <label>Password:</label>
    <asp:TextBox runat="server" ID="Password" TextMode="Password" />
    
    <asp:Button runat="server" Text="Login" OnClick="OnClick_Login"/>
    
    </div>
    </form>
</body>
</html>
