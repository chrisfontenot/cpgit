﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CredAbility.Core.Client;
using CredAbility.Core.Common;
using CredAbility.Web.Authentification;

namespace CredAbility.SSO.Website
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnClick_Login(object sender, EventArgs e)
        {
            AuthentificationManager authentificationManager = new AuthentificationManager();

            ActionResult<ClientProfile> actionResult = authentificationManager.LoginWithThirdParty(this.Username.Text, this.Password.Text);

            if(actionResult.IsSuccessful)
            {
                this.Context.Response.Redirect(string.Format("Results.aspx?u={0}&t={1}", actionResult.Value.ClientID, actionResult.Value.IdentityTokenID));
            }
            else
            {
                ResultMessages.Messages = actionResult.Messages;
            }


        }
    }
}
