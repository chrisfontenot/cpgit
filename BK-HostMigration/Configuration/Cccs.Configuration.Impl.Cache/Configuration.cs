﻿using Microsoft.Practices.EnterpriseLibrary.Caching;

namespace Cccs.Configuration.Impl.Cache
{
	public class Configuration : IConfiguration
	{
		private IConfiguration m_configuration;
		private ICacheManager m_cache;

		public Configuration(IConfiguration configuration)
		{
			m_configuration = configuration;
			m_cache = CacheFactory.GetCacheManager();
		}

        public string ValueGet(string configuration_code)
		{
			string cache_key = ConfigurationCacheKey(configuration_code);

			string value = m_cache.GetData(cache_key) as string;

			if (value == null)	// Cache miss
			{
				value = m_configuration.ValueGet(configuration_code);

				if (value != null)	// Cache value
				{
					m_cache.Add(cache_key, value);
				}
			}

			return value;
		}

		#region Cache Keys

		private static string CONFIGURATION = "Configuration";

		private static string ConfigurationCacheKey(string configuration_code)
		{
			return string.Format("{0}_{1}", CONFIGURATION, configuration_code);
		}

		#endregion
	}
}
