﻿using System.Linq;
using Cccs.Configuration.Dal;

namespace Cccs.Configuration.Impl
{
	public class Configuration : IConfiguration
	{
		public string ValueGet(string configuration_code)
		{
			string value = null;

			using(ConfigurationDataContext entities = new ConfigurationDataContext())
			{
				value =
				(
					from entity in entities.Configurations
					where (string.Compare(entity.ConfigurationCode, configuration_code, true) == 0)
					select entity.Value
				).FirstOrDefault();
			}

			return value;
		}
	}
}