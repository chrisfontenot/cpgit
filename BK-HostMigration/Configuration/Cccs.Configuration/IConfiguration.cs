﻿using System.ServiceModel;

namespace Cccs.Configuration
{
	[ServiceContract]
	public interface IConfiguration
	{
		[OperationContract]
		string ValueGet(string configuration_code);
	}
}
