﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using MMI.FirstData;
using System.Configuration;
using MMI.Payment;
using MMI.FirstData.Dto;

namespace MMI.FirstData.Impl
{
	public class EPay : IEPay
	{
		string providerName;
        string accountType;
        IGatewayConfiguration config;
        private readonly NLog.Logger logger;

        public EPay()
		{
            this.logger = NLog.LogManager.GetCurrentClassLogger();

            LoadConfiguration();
        }

        private void LoadConfiguration()
        {
            var provider = ConfigurationManager.AppSettings["FirstDataProviderName"];
            var account = ConfigurationManager.AppSettings["FirstDataAccountType"];

            providerName = string.IsNullOrEmpty(provider) ? "first_data" : provider;
            accountType = string.IsNullOrEmpty(account) ? "first_data" : account;

            var configurationValues = ConfigurationManager.AppSettings.AllKeys
                                      .SelectMany(ConfigurationManager.AppSettings.GetValues, (k, v) => new { Key = k, Value = v });

            Dictionary<string, string> values = new Dictionary<string, string>();
            foreach (var configurationValue in configurationValues)
            {
                values.Add(configurationValue.Key, configurationValue.Value);
            }

            config = new AppSettingsConfiguration(values);
        }

        public IResponse AddTransaction(CardDetail card, decimal chargeAmount, string referenceCode, string memo)
        {
            try
            {
                var customer = new Customer();
                customer.FirstName = card.FirstName;
                customer.LastName = card.LastName;

                // Set the ID field as the reference Code since this is used as a reference field in MMI.Payment library
                customer.Id = referenceCode;

                customer.Street1 = card.Address1;
                customer.Street2 = card.Address2;
                customer.City = card.City;
                customer.State = card.State;
                customer.Zip = card.ZipCode;

                var debitCard = new MMI.Payment.DebitCard()
                {
                    CardNumber = card.AccountNumber,
                    ExpirationMonth = card.ExpirationMonth.ToString("D2"),
                    ExpirationYear = card.ExpirationYear.ToString(),
                    VerificationCode = card.VerificationCode,
                    Type = AccountType.DEBIT,
                    CardHolder = string.Format("{0} {1}", card.FirstName, card.LastName)
                };

                var paymentInfo = new PaymentInfo()
                {
                    Customer = customer,
                    Amount = chargeAmount,
                    PaymentMethod = debitCard
                };

                var gateway = new Gateway(config);

                logger.Debug("Processing Trasaction with reference code: " + referenceCode + " and memo: " + memo);

                var result = gateway.ProccessTransaction(providerName, accountType, TransactionType.SALE, paymentInfo, null, referenceCode, memo);

                if (result.StatusCode == Status.Code.SUCCESS)
                {
                    var response = new SuccessResult(result.ReferenceCode, result.TransactionId);
                    response.Message = "Transaction Successful";
                    logger.Warn("Transaction Successful for customer " + debitCard.CardHolder);

                    return response;
                }
                else
                {
                    var response = new FailError("Transaction failed due to " + " , reason : " + result.ErrorMessage + " status code:" + result.StatusCode);
                    logger.Error("Transaction failed for customer " + debitCard.CardHolder + " , reason : " + result.ErrorMessage + " status code:" + result.StatusCode);

                    return response;
                }
            }
            catch (Exception ex)
            {
                var response = new FailError(ex.Message);
                logger.Error("Exception occured while making payment, error detail: " + ex.Message);
                return response;
            }
        } 
	}
}
