﻿/*******************************************************************************
 *
 * Defines the class Template.
 *
 * File:        PaymentGateway/Library/Template.cs
 * Date:        Sat Nov  1 19:20:45 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Threading;
using System.Reflection;

namespace MMI.Payment
{
      /// <summary>
      /// Utility class used to repeat an operation multiple times until
      /// it succeeds until a maximum number of repetitions is reached, 
      /// using an exponential backoff strategy.
      /// </summary>
    class Repeater {

        /// <summary>
        /// The default maximum number of attempts.
        /// </summary>
        public static readonly int MaxAttempts = 10;

        /// <summary>
        /// The default initial sleep duration.
        /// </summary>
        public static readonly int InitialSleep = 100;

        /// <summary>
        /// The default base for exponentiation.
        /// </summary>
        public static readonly double Base = 1.5;

        /// <summary>
        /// The type of operation to be performed.
        /// </summary>
        public delegate Object Operation();

        /// <summary>
        /// Constructs a <see cref="Repeater"/>.
        /// </summary>
        /// <param name="log">A log in which to record
        /// failed attempts.</param>
        public Repeater(Log log)
            : this(log, MaxAttempts, InitialSleep, Base)
            { }

        /// <summary>
        /// Constructs a <see cref="Repeater"/>.
        /// </summary>
        /// <param name="log">A log in which to record
        /// failed attempts.</param>
        /// <param name="maxAttempts">The maximum number of attempts.</param>
        /// <param name="initialSleep">The initial sleep duration.</param>
        public Repeater(Log log, int maxAttempts, int initialSleep)
            : this(log, maxAttempts, initialSleep, Base)
            { }

        /// <summary>
        /// Constructs a <see cref="Repeater"/>.
        /// </summary>
        /// <param name="log">A log in which to record
        /// failed attempts.</param>
        /// <param name="maxAttempts">The maximum number of attempts.</param>
        /// <param name="initialSleep">The initial sleep duration.</param>
        /// <param name="Base">The base for exponentiation.</param>
        public Repeater(Log log, int maxAttempts, int initialSleep, double Base)
        {
            myLog = log;
            myMaxAttempts = maxAttempts;
            myInitialSleep = initialSleep;
            myBase = Base;
        }

        /// <summary>
        /// Repeats the specified operation until it succeeds or until 
        /// the maximum specified number of repetitions is reached.
        /// </summary>
        /// <param name="op">The operation to be repeated.</param>
        /// <returns>The result of performing the operation.</returns>
        public Object Repeat(Operation op)
        {
            if (myLog != null) {

                // This is not a missing parameter error; such errors are
                // reserved for errors by web service clients
                throw new PaymentException(
                              "Missing log message and status code",
                              Status.Code.INTERNAL_ERROR
                          );
            }
            return Repeat(op, "", Status.Code.SUCCESS);
        }

        /// <summary>
        /// Repeats the specified operation until it succeeds or until 
        /// the maximum specified number of repetitions is reached.
        /// </summary>
        /// <param name="op">The operation to be repeated.</param>
        /// <param name="message">The message.</param>
        /// <param name="status">The status.</param>
        /// <returns>The result of performing the operation.</returns>
        public Object Repeat(Operation op, string message, Status.Code status)
        {
            Log.Stream stream;
            for (int z = 0; z < myMaxAttempts; ++z) {
                try {
                    if (myLog != null) {
                        if (stream = myLog.GetStream(LogLevel.DEBUG)) {
                            string info = 
                                String.Format(
                                    "{0} - (attempt: {1} of {2}; " + 
                                        "sleep: {3}; base: {4})", 
                                    message, (z + 1).ToString(), 
                                    myMaxAttempts.ToString(),
                                    myInitialSleep.ToString(), 
                                    myBase.ToString()
                                );
                            stream.Write(MethodBase.GetCurrentMethod(), info);
                        } else if (stream = myLog.GetStream(LogLevel.VERBOSE)) {
                            string info = 
                                String.Format(
                                    "{0} - (attempt: {1} of {2})", 
                                    message, (z + 1).ToString(), 
                                    myMaxAttempts.ToString()
                                );
                            stream.Write(MethodBase.GetCurrentMethod(), info);
                        } else {
                            if ( z == 0 &&
                                 (stream = myLog.GetStream(LogLevel.INFO)) ) 
                            {
                                stream.Write(MethodBase.GetCurrentMethod(), message);
                            }
                        }
                    }
                    return op();
                } catch (Exception e) {
                    if (z < myMaxAttempts - 1) {
                        int sleep = 
                            (int) (myInitialSleep * Math.Pow(myBase, z));
                        if ( myLog != null &&
                             (stream = myLog.GetStream(LogLevel.ERROR)) ) 
                        {
                            string error = 
                                String.Format(
                                    "{0} failed: {1} Sleeping for {2} ms",
                                    message, e.Message, sleep.ToString()
                                );
                            stream.Write(MethodBase.GetCurrentMethod(), error, e);
                        }
                        Thread.Sleep(sleep);
                    } else {
                        string error = 
                            String.Format(
                                "{0} failed: {1}", message, e.Message
                            );
                        PaymentException pe = 
                            new PaymentException(
                                    error,
                                    status,
                                    e
                                );
                        if ( myLog != null &&
                             (stream = myLog.GetStream(LogLevel.FATAL_ERROR)) )
                        {
                            stream.Write(MethodBase.GetCurrentMethod(), String.Format("{0} failed", message), pe);
                            pe.SetLogged();
                        }
                        throw pe;
                    }
                }
            }
            return null; // Unreachable
        }

        private Log     myLog;
        private int     myMaxAttempts;
        private int     myInitialSleep;
        private double  myBase;
    }
}
