﻿/*******************************************************************************
 *
 * Defines the class Mmi.Payment.Test.Provider.
 *
 * File:        PaymentGateway/Library/TestProvider.cs
 * Date:        Sat Nov 15 11:48:35 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MMI.Payment
{
  namespace Test
  {

    using StringMap = Dictionary<string, string>;

      /// <summary>
      /// Used by the unit tests.
      /// </summary>
    public class ProviderTransDesc
    {
        public ProviderTransDesc( 
                   string type, StringMap inputParameters,
                   Status.Code status, ProviderStatus providerStatus,
                   StringMap outputParameters ) 
            : this( type, inputParameters, status, providerStatus, 
                    outputParameters, null )
            { }

        public ProviderTransDesc( 
                   string type, StringMap inputParameters,
                   Exception exception ) 
            : this( type, inputParameters, null, null, new StringMap(), 
                    exception )
            { }

        private ProviderTransDesc(
                    string type, StringMap inputParameters,
                    Status.Code? status, ProviderStatus providerStatus, 
                    StringMap outputParameters, Exception exception ) 
        {
            myType = type;
            myInputParameters = inputParameters;
            myStatus = status;
            myProviderStatus = providerStatus;
            myOutputParameters = outputParameters;
            myException = exception;
        }

        public string Type { get { return myType; } }

        public StringMap InputParameters { get { return myInputParameters; } }

        public Status.Code? Status { get { return myStatus; } }

        public ProviderStatus ProviderStatus 
        { 
            get { return myProviderStatus; } 
        }

        public StringMap OutputParameters { get { return myOutputParameters; } }

        public Exception Exception { get { return myException; } }

        public bool Test(ProviderTransaction trans)
        {
            if ( trans.Type != Type ||
                 trans.ProviderStatus != ProviderStatus ||
                 !DictionaryComparer.AreEqual(trans.InputParameters,
                       InputParameters) ||
                 !DictionaryComparer.AreEqual(trans.OutputParameters,
                       OutputParameters) )
            {
                return false;
            }
            if (myStatus.HasValue) {
                return myStatus.Value == trans.Status;
            } else if (myException is PaymentException) {
                return ((PaymentException) myException).Code == trans.Status;
            } else {
                return trans.Status == MMI.Payment.Status.Code.INTERNAL_ERROR;      
            }
        }

        private string          myType;
        private StringMap       myInputParameters;
        private Status.Code?    myStatus;
        private ProviderStatus  myProviderStatus;
        private StringMap       myOutputParameters;
        private Exception       myException;
    }

    /// <summary>
    /// Used by the unit tests.
    /// </summary>
    public class TransactionDesc {
        public TransactionDesc(
                    string provider, TransactionType type, 
                    PaymentInfo paymentInfo, Schedule schedule, string memo,
                    PaymentInfo outputPaymentInfo, Schedule outputSchedule )
            : this( provider, type, paymentInfo, schedule, memo,
                    MMI.Payment.Status.Code.SUCCESS, outputPaymentInfo,
                    outputSchedule, null )
            { }

        public TransactionDesc(
                    string provider, TransactionType type, 
                    PaymentInfo paymentInfo, Schedule schedule, 
                    string memo, Exception exception)
            : this( provider, type, paymentInfo, schedule, memo,
                    0, null, null, exception )
            { }

        private TransactionDesc(
                    string provider, TransactionType type, 
                    PaymentInfo paymentInfo, Schedule schedule, string memo,
                    Status.Code status, PaymentInfo outputPaymentInfo, 
                    Schedule outputSchedule, Exception exception)
        { 
            myProvider = provider;
            myType = type;
            myPaymentInfo = paymentInfo;
            mySchedule = schedule;
            myMemo = memo;
            myStatus = exception == null ?
                status :
                exception is PaymentException ?
                    ((PaymentException) exception).Code :
                    MMI.Payment.Status.Code.INTERNAL_ERROR;
            myReferenceCode = "";
            myOutputPaymentInfo = outputPaymentInfo;
            myOutputSchedule = outputSchedule;
            myException = exception;
            myTransactions = new List<ProviderTransDesc>();
            myReadOnlyTransactions = 
                new ReadOnlyCollection<ProviderTransDesc>(myTransactions);
        }

        public string Provider { get { return myProvider; } }

        public TransactionType Type { get { return myType; } }

        public PaymentInfo PaymentInfo 
        { 
            get { return myPaymentInfo; }
            set { myPaymentInfo = value; }
        }

        public PaymentInfo OutputPaymentInfo 
        { 
            get { return myOutputPaymentInfo; }
            set { myOutputPaymentInfo = value; }
        }

        public Schedule Schedule 
        { 
            get { return mySchedule; }
            set { mySchedule = value; }
        }

        public Schedule OutputSchedule 
        { 
            get { return myOutputSchedule; }
            set { myOutputSchedule = value; }
        }

        public IList<ProviderTransDesc> Transactions 
        { 
            get { return myReadOnlyTransactions; } 
        }

        public string Memo { get { return myMemo; } }

        public Status.Code Status { get { return myStatus; } }

        public string ReferenceCode { get { return myReferenceCode; } }

        public Exception Exception { get { return myException; } }

        public void AddTransaction(ProviderTransDesc desc) 
        {
            if ( myTransactions.Count > 0 && 
                 myTransactions.Last().Exception != null ) 
            { 
                throw new InvalidOperationException(
                              "Transaction sequence terminated with " +
                                  "an exception"
                          );
            }
            myTransactions.Add(desc);
        }

        public bool Test(GatewayTransaction trans)
        {
            if ( trans.Provider != Provider ||
                 trans.Type != myType ||
                 trans.Memo != Memo ||
                 trans.Status != Status ||
                 trans.ProviderTransactions.Count != Transactions.Count )
            {
                return false;
            }
            for (int z = 0, n = Transactions.Count; z < n; ++z)
                if (!Transactions[z].Test(trans.ProviderTransactions[z]))
                    return false;
            return true;
        }

        private string                                 myProvider;
        private TransactionType                        myType;
        private PaymentInfo                            myPaymentInfo;
        private PaymentInfo                            myOutputPaymentInfo;
        private Schedule                               mySchedule;
        private Schedule                               myOutputSchedule;
        private string                                 myMemo;
        private List<ProviderTransDesc>                myTransactions;
        private ReadOnlyCollection<ProviderTransDesc>  myReadOnlyTransactions;
        private Status.Code                            myStatus;
        private string                                 myReferenceCode;
        private Exception                              myException;
    }

    /// <summary>
    /// Used by the unit tests.
    /// </summary>
    public class Provider : MMI.Payment.Provider
    {
        public Provider(IGatewayConfiguration config, Log log)
            : base("test", log) 
            { }

        public override void ProcessSale(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            TransactionDesc desc = TransactionDescription;
            foreach (var d in desc.Transactions)
                ExecuteTransaction(transaction, d);
            if (desc.Exception != null) {
                throw desc.Exception;
            }
        }

        public static TransactionDesc TransactionDescription
        {
            get { return myDescription; }
            set { myDescription = value; }
        }

        private void ExecuteTransaction(GatewayTransaction trans,
            ProviderTransDesc desc)
        {
            ProviderTransaction providerTrans = desc.InputParameters != null ?
                trans.NewProviderTransaction(desc.Type, desc.InputParameters) :
                trans.NewProviderTransaction(desc.Type);
            if (desc.Exception != null) {
                throw desc.Exception;
            } else if (desc.OutputParameters != null) {
                providerTrans.Complete(
                    desc.Status.Value, 
                    desc.ProviderStatus, 
                    desc.OutputParameters
                );
            } else if (desc.ProviderStatus != null) {
                providerTrans.Complete(desc.Status.Value, desc.ProviderStatus);
            } else {
                providerTrans.Complete(desc.Status.Value);
            }
        }

        private static TransactionDesc myDescription;
    }

  }
}
