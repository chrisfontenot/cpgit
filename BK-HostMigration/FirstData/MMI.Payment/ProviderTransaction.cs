﻿/*******************************************************************************
 *
 * Defines the class ProviderTransaction.
 *
 * File:        PaymentGateway/Library/ProviderTransaction.cs
 * Date:        GatewayTransaction
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;
    using ReadOnlyStringMap = ReadOnlyDictionary<string, string>;

      /// <summary>
      /// Represents a single call to a payment services providers,
      /// typically via web services.
      /// </summary>
    public class ProviderTransaction : DataClient
    {
        /// <summary>
        /// Constructs a <see cref="ProviderTransaction"/>.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="id">The database record ID.</param>
        /// <param name="timestamp">A timestamp.</param>
        /// <param name="type">The transaction type.</param>
        /// <param name="status">The status code.</param>
        /// <param name="providerStatus">The status, if any, 
        /// returned by the provider.</param>
        /// <param name="inputParameters">The input parameters.</param>
        /// <param name="outputParameters">The output parameters.</param>
        protected ProviderTransaction(
            Connection connection, long id, DateTime timestamp, string type,
            Status.Code status, ProviderStatus providerStatus, 
            ReadOnlyStringMap inputParameters, 
            ReadOnlyStringMap outputParameters)
        {
            myConnection = connection;
            myId = id;
            myTimestamp = timestamp;
            myType = type;
            myStatus = status;
            myProviderStatus = providerStatus;
            myInputParameters = inputParameters;
            myOutputParameters = outputParameters;
            myFrozen = false;
        }

        /// <summary>
        /// Gets the database record ID.
        /// </summary>
        /// <value>The id.</value>
        public long Id 
        {
            get { return myId; }
        }

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public DateTime Timestamp 
        {
            get { return myTimestamp; }
        }

        /// <summary>
        /// Gets the transaction type.
        /// </summary>
        /// <value>The type.</value>
        public string Type 
        {
            get { return myType; }
        }

        /// <summary>
        /// Gets the status code.
        /// </summary>
        /// <value>The status.</value>
        public Status.Code Status 
        {
            get { return myStatus; }
        }

        /// <summary>
        /// Gets the status, if any, returned by the provider.
        /// </summary>
        /// <value>The provider status.</value>
        public MMI.Payment.ProviderStatus ProviderStatus 
        {
            get { return myProviderStatus; }
        }

        /// <summary>
        /// Gets or sets the input parameters.
        /// </summary>
        /// <value>The input parameters.</value>
        public ReadOnlyStringMap InputParameters 
        {
            get { return myInputParameters; }
            set 
            {
                if (myFrozen)
                    throw new PaymentException(
                                  "Can't modify parameter collection",
                                  MMI.Payment.Status.Code.INTERNAL_ERROR
                              );
                myInputParameters = value;
            }
        }

        /// <summary>
        /// Gets or sets the output parameters.
        /// </summary>
        /// <value>The output parameters.</value>
        public ReadOnlyStringMap OutputParameters 
        {
            get { return myOutputParameters; }
            set 
            {
                if (myFrozen)
                    throw new PaymentException(
                                  "Can't modify parameter collection",
                                  MMI.Payment.Status.Code.INTERNAL_ERROR
                              );
                myOutputParameters = value;
            }
        }

        /// <summary>
        /// Determines whether this instance is complete.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance is complete; otherwise, 
        /// 	<c>false</c>.
        /// </returns>
        public bool IsComplete()
        {
            return Status != MMI.Payment.Status.Code.PENDING;
        }

        /// <summary>
        /// Assigns this transaction the specified status code
        /// and records it in the database.
        /// </summary>
        /// <param name="status">The status.</param>
        public void Complete(Status.Code status)
        {
            if (status == MMI.Payment.Status.Code.PENDING)
                throw new PaymentException(
                              String.Format(
                                  "'{0}' is not a valid status for a " +
                                      "complete transaction",
                                  StringFormatter.Capitalize(
                                      MMI.Payment.Status.Message(status)
                                  )
                              ),
                              MMI.Payment.Status.Code.INTERNAL_ERROR
                          );
            if (status == MMI.Payment.Status.Code.SUCCESS)
                throw new PaymentException(
                              "Missing provider status",
                              MMI.Payment.Status.Code.INTERNAL_ERROR
                          );
            Complete(status, null, new StringMap());
        }

        /// <summary>
        /// Assigns this transaction the specified status code
        /// and provider status and records them in the database.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="providerStatus">The provider status.</param>
        public void Complete(Status.Code status, ProviderStatus providerStatus)
        {
            Complete(status, providerStatus, new StringMap());
        }

        /// <summary>
        /// Assigns this transaction the specified status code,
        /// provider status, and output parameters, and records them 
        /// in the database.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="providerStatus">The provider status.</param>
        /// <param name="outputParameters">The output parameters.</param>
        public void Complete(Status.Code status, ProviderStatus providerStatus, 
            StringMap outputParameters)
        {
            if (IsComplete())
                throw new PaymentException(
                              "Transaction is already complete",
                              MMI.Payment.Status.Code.INTERNAL_ERROR
                          );

            // Update database
            //SqlCommand command = null;
            //SqlTransaction trans = null;
            //try {
            //    trans = myConnection.SqlConnection.BeginTransaction();
            //    command =
            //        new SqlCommand(
            //                "CompleteProviderTransaction", 
            //                myConnection.SqlConnection
            //            );
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.Transaction = trans;
            //    command.Parameters.Add(
            //        new SqlParameter("@status", (int) status)
            //    );
            //    AddParameter(
            //        command, 
            //        "@provider_status", 
            //        providerStatus != null ?
            //            providerStatus.Status :
            //            null,
            //        SqlDbType.VarChar
            //    );
            //    AddParameter(
            //        command, 
            //        "@error_message", 
            //        providerStatus != null ?
            //            providerStatus.ErrorMessage :
            //            null,
            //        SqlDbType.VarChar
            //    );
            //    AddParameter(
            //        command, 
            //        "@reference_code", 
            //        providerStatus != null ?
            //            providerStatus.ReferenceCode :
            //            null,
            //        SqlDbType.VarChar
            //    );
            //    command.Parameters.Add(new SqlParameter("@id", Id));
            //    command.ExecuteNonQuery();
            //    if (outputParameters != null)
            //        StoreOutputParams(
            //            new ReadOnlyStringMap(outputParameters), trans
            //        );
            //    trans.Commit();
            //} catch (PaymentException e) {
            //    if (trans != null)
            //        trans.Rollback();
            //    throw e;
            //} catch (Exception e) {
            //    if (trans != null)
            //        trans.Rollback();
            //    throw new PaymentException(
            //                  "Failed updating provider transaction: " +
            //                      e.Message,
            //                  Mmi.Payment.Status.Code.DATABASE_ERROR,
            //                  e
            //              );
            //} finally {
            //    if (trans != null)
            //        trans.Dispose();
            //    if (command != null)
            //        command.Dispose();
            //}

            // Update members
            myStatus = status;
            myProviderStatus = providerStatus;
            if (outputParameters != null)
                myOutputParameters = new ReadOnlyStringMap(outputParameters);
        }

        /// <summary>
        /// Marks this transaction as frozen, so its parameters cannot
        /// be changed; used for instances representing transactions that
        /// are already complete.
        /// </summary>
        public void Freeze() { myFrozen = true; }

        public static bool operator==(ProviderTransaction lhs,
            ProviderTransaction rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(ProviderTransaction lhs,
            ProviderTransaction rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {        
            if (other == null)
                return false;
            try {
                ProviderTransaction trans = (ProviderTransaction) other;
                return Id == trans.Id &&
                       Timestamp == trans.Timestamp &&
                       Type == trans.Type &&
                       Status == trans.Status &&
                       ProviderStatus == trans.ProviderStatus &&
                       DictionaryComparer.AreEqual(InputParameters, 
                           trans.InputParameters) &&
                       DictionaryComparer.AreEqual(OutputParameters, 
                           trans.OutputParameters);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return 0; // No need to store GatewayTransactions in hash tables
        }

        /// <summary>
        /// Stores the this transaction's output parameters in the database.
        /// </summary>
        /// <param name="outputParameters">The output parameters.</param>
        /// <param name="trans">The trans.</param>
        //private void StoreOutputParams(ReadOnlyStringMap outputParameters,
        //    SqlTransaction trans)
        //{
        //    if (outputParameters.Count == 0)
        //        return;

        //    SqlCommand command = null;
        //    try {
        //        command =
        //            new SqlCommand(
        //                    "StoreProviderTransactionOutputParam", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Transaction = trans;
        //        SqlParameter date = 
        //            new SqlParameter("@date", SqlDbType.BigInt);
        //        SqlParameter transactionid = 
        //            new SqlParameter("@transactionid", SqlDbType.BigInt);
        //        SqlParameter name = 
        //            new SqlParameter("@name", SqlDbType.VarChar, 250);
        //        SqlParameter value = 
        //            new SqlParameter("@value", SqlDbType.VarChar, 250);
        //        command.Parameters.Add(date);
        //        command.Parameters.Add(transactionid);
        //        command.Parameters.Add(name);
        //        command.Parameters.Add(value);

        //        // Execute the command once for each parameter
        //        foreach (var param in outputParameters) {
        //            date.Value = DateTime.Now.ToFileTimeUtc();
        //            transactionid.Value = Id;
        //            name.Value = param.Key;
        //            value.Value = param.Value;
        //            command.Prepare();
        //            command.ExecuteNonQuery();
        //        }
        //    } finally {
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        private Connection          myConnection;
        private long                myId;
        private DateTime            myTimestamp;
        private string              myType;
        private Status.Code         myStatus;
        private ProviderStatus      myProviderStatus;
        private ReadOnlyStringMap   myInputParameters;
        private ReadOnlyStringMap   myOutputParameters;
        private bool                myFrozen;
    }
}
