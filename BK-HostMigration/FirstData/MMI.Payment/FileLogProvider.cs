﻿/*******************************************************************************
 *
 * Defines the class FileLogProvider.
 *
 * File:        PaymentGateway/Library/FileLogProvider.cs
 * Date:        Sat Nov  1 14:38:43 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace MMI.Payment
{
      /// <summary>
      /// Records log entries in log files.
      /// </summary>
    public class FileLogProvider : ILogProvider
    {
        public static readonly int MaxAttempts = 10;
        public static readonly int InitialSleep = 100;
        public static readonly string DefaultPathFormat = "yyyy-MM-dd/htt";

        /// <summary>
        /// Constructs a <see cref="FileLogProvider"/> from a directory
        /// path.
        /// </summary>
        /// <param name="rootDirectory">The directory where log files are
        /// created.</param>
        public FileLogProvider(string rootDirectory)
            : this(rootDirectory, DefaultPathFormat)
            { }

        /// <summary>
        /// Constructs a <see cref="FileLogProvider"/> from a directory
        /// path and a directory format, using the .NET custom date
        /// and time format.
        /// </summary>
        /// <param name="rootDirectory">The root directory.</param>
        /// <param name="pathFormat">The path format.</param>
        public FileLogProvider(string rootDirectory, string pathFormat)
        {
            myPath = rootDirectory + "\\" + DateTime.Now.ToString(pathFormat) +
                ".log";
            myMatchNewline = new Regex(@"\r\n|\n|\r");
        }

        /// <summary>
        /// Processes a log entry.
        /// </summary>
        /// <param name="entry">The log entry.</param>
        public void NewEntry(LogEntry entry)
        {
            FileStream stream = OpenFile();
            try {
                stream.Seek(0, SeekOrigin.End);
                string line = 
                    String.Format(
                        "{0} - {1} - {2} (Session {3})\n",
                        entry.TimestampString, 
                        Log.PrintLevel(entry.Level),
                        myMatchNewline.Replace(entry.Message, " "),
                        entry.SessionId
                    );
                byte[] info = new UTF8Encoding(true).GetBytes(line);
                stream.Write(info, 0, info.Length);
                stream.Close();
            } catch (Exception e) {
                throw new PaymentException(
                              "File log listener error: " + e.Message,
                              Status.Code.FILESYSTEM_ERROR,
                              e
                          );
            }
        }

        /// <summary>
        /// No-op.
        /// </summary>
        public void Dispose() { }

        /// <summary>
        /// Opens an existing file or creates a new one.
        /// </summary>
        /// <returns>A file stream.</returns>
        private FileStream OpenFile()
        {
            // Lazy initialization
            if (myRepeater == null)
                myRepeater = new Repeater(null);

            // Create parent directory
            if (!File.Exists(myPath)) {
                char[] separators = { '/', '\\' };
                int pos = myPath.LastIndexOfAny(separators);
                if (pos != -1) {
                    string directory = myPath.Substring(0, pos);
                    if (!Directory.Exists(directory)) {
                        try {
                            Directory.CreateDirectory(directory);
                        } catch (Exception e) { 
                            throw new PaymentException(
                                      String.Format(
                                          "Failed creating directory '{0}': " +
                                              "{1}",
                                          directory,
                                          e.Message
                                      ),
                                      Status.Code.FILESYSTEM_ERROR
                                  );
                        }
                        if (!Directory.Exists(directory))
                            throw new PaymentException(
                                      String.Format(
                                          "Failed creating directory '{0}'",
                                          directory
                                      ),
                                      Status.Code.FILESYSTEM_ERROR
                                  );
                    }
                }
            }

            // Open file
            Repeater.Operation openFile = 
                () => File.Open( myPath, FileMode.OpenOrCreate, 
                                 FileAccess.ReadWrite, FileShare.None );
            try {
                return (FileStream) myRepeater.Repeat(openFile);
            } catch (Exception e) {
                throw new PaymentException(
                              String.Format(
                                  "Failed opening file {0}: {1}",
                                  myPath, e.Message
                              ),
                              Status.Code.FILESYSTEM_ERROR
                          );
            }
        }

        private Regex     myMatchNewline;
        private string    myPath;
        private Repeater  myRepeater;
    }
}
