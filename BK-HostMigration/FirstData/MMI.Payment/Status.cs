﻿/*******************************************************************************
 *
 * Defines the class Status.
 *
 * File:        PaymentGateway/Library/Status.cs
 * Date:        Fri Oct 31 15:28:27 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
      /// <summary>
      /// Collects types and methods for manipulating status codes.
      /// </summary>
    public class Status
    {
        /// <summary>
        /// Represents the status of an operation. Intended for display
        /// to end users, not to expose implementation details.
        /// </summary>
        public enum Code
        {
            /// <summary>
            /// Represents a successful operation.
            /// </summary>
            SUCCESS = 0,
            /// <summary>
            /// Represents a pending operation.
            /// </summary>
            PENDING = 1,
            /// <summary>
            /// Indicates that a parameter is invalid.
            /// </summary>
            INVALID_PARAMETER = 2,
            /// <summary>
            /// Indicates that a parameter is missing.
            /// </summary>
            MISSING_PARAMETER = 3,
            /// <summary>
            /// Indicates that several parameter values are incompatible.
            /// </summary>
            INCONSISTENT_PARAMETERS = 4,
            /// <summary>
            /// Indicates that no provider with a specified name exists.
            /// </summary>
            INVALID_PROVIDER = 5,
            /// <summary>
            /// Indicates that no provider account with a specified name exists.
            /// </summary>
            INVALID_PROVIDER_ACCOUNT = 6,
            /// <summary>
            /// Indicates that no gateway transaction corresponds to a putative
            /// transaction ID.
            /// </summary>
            INVALID_TRANSACTION_REFERENCE = 7,
            /// <summary>
            /// Indicates that a transaction type is not supported by a
            /// particular provider.
            /// </summary>
            INVALID_TRANSACTION_TYPE = 8,
            /// <summary>
            /// Indicates a payment gateway configuration error.
            /// </summary>
            CONFIGURATION_ERROR = 9,
            /// <summary>
            /// Indicates that an error occurred while processing a log entry.
            /// </summary>
            LOG_ERROR = 10,
            /// <summary>
            /// Indicates that a filesystem error occurred.
            /// </summary>
            FILESYSTEM_ERROR = 11,
            /// <summary>
            /// Indicates that a database error occurred.
            /// </summary>
            DATABASE_ERROR = 12,
            /// <summary>
            /// Indicates that a network error occurred.
            /// </summary>
            NETWORK_ERROR = 13,
            /// <summary>
            /// Indicates that a HTTP error occurred.
            /// </summary>
            HTTP_ERROR = 14,
            /// <summary>
            /// Indicates that a response from a provider violated a
            /// web service protocol.
            /// </summary>
            WEBSERVICE_PROTOCOL_ERROR = 15,
            /// <summary>
            /// Indicates that a provider is offline.
            /// </summary>
            PROVIDER_UNAVAILABLE = 16,
            /// <summary>
            /// Indicates that a response from a provider violated the
            /// provider's specification.
            /// </summary>
            INVALID_PROVIDER_RESPONSE = 17,
            /// <summary>
            /// Indicates that a provider denied authentication.
            /// </summary>
            PROVIDER_AUTHENTICATION_FAILED = 18,
            /// <summary>
            /// Indicates that a customer's payment information has been
            /// rejected as invalid.
            /// </summary>
            INVALID_CUSTOMER_ACCOUNT = 19,
            /// <summary>
            /// Indicates that a debit card has expired.
            /// </summary>
            CARD_EXPIRED = 20,
            /// <summary>
            /// Indicates that a payment has been declined.
            /// </summary>
            PAYMENT_DECLINED = 21,
            /// <summary>
            /// Indicates that a provider has returned an error status that
            /// does not fall into one of the above categories.
            /// </summary>
            PROVIDER_ERROR = 22,
            /// <summary>
            /// Indicates that a payment method is not supported by a
            /// particular provider.
            /// </summary>
            INVALID_PAYMENT_METHOD = 23,
            /// <summary>
            /// Indicates that the payment gateway has behaved unexpectedly.
            /// </summary>
            INTERNAL_ERROR = -1,
        }

        /// <summary>
        /// Returns a human-readable message.
        /// </summary>
        /// <param name="code">A status code.</param>
        /// <returns>The message.</returns>
        public static string Message(Status.Code code)
        {
            switch (code)
            {
                case Code.SUCCESS:
                    return "success";
                case Code.PENDING:
                    return "pending";
                case Code.INVALID_PARAMETER:
                    return "invalid parameter";
                case Code.MISSING_PARAMETER:
                    return "missing parameter";
                case Code.INCONSISTENT_PARAMETERS:
                    return "inconsistent parameters";
                case Code.INVALID_PROVIDER:
                    return "invalid provider";
                case Code.INVALID_PROVIDER_ACCOUNT:
                    return "invalid provider account";
                case Code.INVALID_TRANSACTION_REFERENCE:
                    return "invalid transaction reference";
                case Code.INVALID_TRANSACTION_TYPE:
                    return "invalid transaction type";
                case Code.CONFIGURATION_ERROR:
                    return "configuration error";
                case Code.LOG_ERROR:
                    return "log error";
                case Code.FILESYSTEM_ERROR:
                    return "filesystem error";
                case Code.DATABASE_ERROR:
                    return "database error";
                case Code.NETWORK_ERROR:
                    return "network error";
                case Code.HTTP_ERROR:
                    return "HTTP error";
                case Code.WEBSERVICE_PROTOCOL_ERROR:
                    return "webservice protocol error";
                case Code.PROVIDER_UNAVAILABLE:
                    return "provider unavailable";
                case Code.INVALID_PROVIDER_RESPONSE:
                    return "invalid provider response";
                case Code.PROVIDER_AUTHENTICATION_FAILED:
                    return "provider authentication failed";
                case Code.INVALID_CUSTOMER_ACCOUNT:
                    return "invalid customer account";
                case Code.CARD_EXPIRED:
                    return "card expired";
                case Code.PAYMENT_DECLINED:
                    return "payment declined";
                case Code.PROVIDER_ERROR:
                    return "provider error";
                case Code.INVALID_PAYMENT_METHOD:
                    return "invalid payment method";
                case Code.INTERNAL_ERROR:
                    return "internal error";
                default:
                    return "<unknown>";
            }
        }
    }
}
