﻿/*******************************************************************************
 *
 * Defines the class Data.
 *
 * File:        PaymentGateway/Library/Data.cs
 * Date:        Fri Nov 14 11:05:27 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
  namespace Test 
  {

      /// <summary>
      /// Data used by the unit tests.
      /// </summary>
    public class Data
    {
        // CardNumber1 and RoutingNumber1 are provided by Vanco for
        // use with their test server
        public static readonly string FirstName1 = "John";
        public static readonly string FirstName2 = "Bob";
        public static readonly string LastName1 = "Barrymore";
        public static readonly string LastName2 = "Howard";
        public static readonly string Id1 = "1739931-AX";
        public static readonly string Id2 = "88918324-FP";
        public static readonly string Street1A = "443 E. Second St.";
        public static readonly string Street1B = "123 Example St.";
        public static readonly string Street2A = "Suite 4000";
        public static readonly string Street2B = null;
        public static readonly string City1 = "The Dalles";
        public static readonly string City2 = "Seattle";
        public static readonly string State1 = "OR";
        public static readonly string State2= "CA";
        public static readonly string Zip1 = "97058";
        public static readonly string Zip2 = "90210";
        public static readonly string Phone1 = "8005551212";
        public static readonly string Phone2 = "(800) 123-4567";
        public static readonly string AccountNumber1 = "9012859011";
        public static readonly string AccountNumber2 = "44739349";
        public static readonly string CardHolder1 = "John Barrymore";
        public static readonly string CardHolder2 = "Bob Howard";
        public static readonly string RoutingNumber1 = "123456780";
        public static readonly string RoutingNumber2 = "007210198";
        public static readonly string CardNumber1 = "4111111111111111";
        public static readonly string CardNumber2 = "340000000000009";
        public static readonly string ExpirationMonth1 = "05";
        public static readonly string ExpirationMonth2 = "11";
        public static readonly string ExpirationYear1 = "2010";
        public static readonly string ExpirationYear2 = "2012";
        public static readonly string VerificationCode1 = "338";
        public static readonly string VerificationCode2 = "1116";
        public static readonly string ReferenceCode1 = "12345678901234";
        public static readonly string ReferenceCode2 = "09876543210987";
    }

  }
}
