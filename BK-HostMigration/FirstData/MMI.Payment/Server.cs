﻿/*******************************************************************************
 *
 * Defines the class Server.
 *
 * File:        PaymentGateway/Library/Server.cs
 * Date:        Sat Nov  1 11:32:35 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Services.Description;
using System.Xml.Serialization;
using System.Reflection;

namespace MMI.Payment
{

      /// <summary>
      /// A thin ASMX wrapper around an instance of <see cref="Gateway"/>.
      /// </summary>
    [SoapRpcService]
    [XmlInclude(typeof(CreditCard))]
    [XmlInclude(typeof(DebitCard))]
    [XmlInclude(typeof(BankAccount))]
    [XmlInclude(typeof(PaymentToken))]
    [XmlInclude(typeof(CustomPaymentMethod))]
    [XmlInclude(typeof(Schedule))]
    public class Server : System.Web.Services.WebService
    {
        /// <summary>
        /// Constructs a <see cref="Server"/>.
        /// </summary>
        /// <param name="configUrl">Url of configuration file.</param>
        public Server(string configUrl)
        {
            Configuration config =
                WebConfigurationManager.OpenWebConfiguration(configUrl);
            AppSettingsConfiguration appConfig = 
                new AppSettingsConfiguration(config);
            myGateway = new Gateway(appConfig);
        }

        /// <summary>
        /// Processes a Vanco debit card transaction.
        /// </summary>
        /// <param name="accountName">Name of the Vanco account.</param>
        /// <param name="customer">The customer.</param>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="card">The debit card.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus VancoProcessDebit(string accountName, 
            Customer customer, string amount, DebitCard card, 
            string memo)
        {
            PaymentInfo info = new PaymentInfo(customer, amount, card);
            return myGateway.ProccessTransaction("vanco", accountName,
                TransactionType.SALE, info, null, null, memo);
        }

        /// <summary>
        /// Processes a transaction of type SALE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessSale(string provider, string accountName, 
            PaymentInfo info, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.SALE,
                       info, null, null, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type AUTHORIZATION.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessAuthorization(string provider, 
            string accountName, PaymentInfo info, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.AUTHORIZATION,
                       info, null, null, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type CREDIT.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCredit(string provider, string accountName, 
            PaymentInfo info, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.CREDIT,
                       info, null, null, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type CAPTURE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction of type CAPTURE to which the current transaction 
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCapture(string provider, string accountName, 
            string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.CAPTURE,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type RETURN.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">An instance of <see cref="PaymentInfo"/> 
        /// indicating the amount to be returned.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessReturn(string provider, string accountName, 
            PaymentInfo info, string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.RETURN,
                       info, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type VOID.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessVoid(string provider, string accountName, 
            string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.VOID,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type QUERY.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction whose status is to be queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQuery(string provider, string accountName, 
            string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.QUERY,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type CREATE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information to be registered.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCreateInfo(string provider, 
            string accountName, PaymentInfo info, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.CREATE_INFO,
                       info, null, null, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type QUERY_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQueryInfo(string provider, 
            string accountName, string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.QUERY_INFO,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The new payment information.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// updated.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessUpdateInfo(string provider, 
            string accountName, PaymentInfo info, string referenceCode, 
            string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.UPDATE_INFO,
                       info, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type DELETE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// deleted.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessDeleteInfo(string provider, 
            string accountName, string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.DELETE_INFO,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type CREATE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The payment schedule.</param>
        /// <param name="memo">The descriptive message to be recorded with
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCreateSchedule(string provider, 
            string accountName, PaymentInfo info, Schedule schedule,
            string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.CREATE_SCHEDULE,
                       info, schedule, null, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type QUERY_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction that established the payment schedule to be 
        /// queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQuerySchedule(string provider, 
            string accountName, string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.QUERY_SCHEDULE,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The new payment information.</param>
        /// <param name="schedule">The new payment schedule.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// updated.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessUpdateSchedule(string provider, 
            string accountName, PaymentInfo info, Schedule schedule, 
            string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.UPDATE_SCHEDULE,
                       info, schedule, referenceCode, memo
                   );
        }

        /// <summary>
        /// Processes a transaction of type DELETE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction that established the payment schedule to be 
        /// deleted.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessDeleteSchedule(string provider, 
            string accountName, string referenceCode, string memo)
        {
            return myGateway.ProccessTransaction(
                       provider, accountName, TransactionType.DELETE_SCHEDULE,
                       null, null, referenceCode, memo
                   );
        }

        /// <summary>
        /// Writes the specified message to the payment gateway log and returns
        /// it; can be used for testing a production server.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The log level.</param>
        /// <returns>The message.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public string Echo(string message, LogLevel level)
        {
            try {
                Log.Stream stream;
                if (stream = myGateway.Connection.Log.GetStream(level))
                    stream.Write(MethodBase.GetCurrentMethod(), message);
                return message;
            } catch (Exception e) {
                LogException(e);
                throw e;
            }
        }

        /// <summary>
        /// Returns the given payment information. Tests XML serialization.
        /// </summary>
        /// <param name="info">The payment information.</param>
        /// <returns>The payment information.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public PaymentInfo EchoPaymentInfo(PaymentInfo info)
        {
            try {
                Log.Stream stream =
                    myGateway.Connection.Log.GetStream(LogLevel.INFO);
                if (stream != null)
                    stream.Write(MethodBase.GetCurrentMethod(), "EchoPaymentInfo: " + info.ToString());
                return info;
            } catch (Exception e) {
                LogException(e);
                throw e;
            }
        }

        /// <summary>
        /// Returns the given payment schedule.Tests XML serialization.
        /// </summary>
        /// <param name="schedule">The payment schedule.</param>
        /// <returns>The payment schedule.</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        public Schedule EchoSchedule(Schedule schedule)
        {
            try {
                Log.Stream stream =
                    myGateway.Connection.Log.GetStream(LogLevel.INFO);
                if (stream != null)
                    stream.Write(MethodBase.GetCurrentMethod(), "EchoSchedule: " + schedule.ToString());
                return schedule;
            } catch (Exception e) {
                LogException(e);
                throw e;
            }
        }

        /// <summary>
        /// Tests XML serialization; can be used for testing a production 
        /// server.
        /// </summary>
        /// <param name="customer">A customer.</param>
        /// <param name="amount">A transaction amount.</param>
        /// <param name="method">A payment method.</param>
        /// <returns>A <see cref="PaymentInfo"/> combining the three 
        /// arguments</returns>
        [WebMethod]
        [SoapRpcMethod(Use=SoapBindingUse.Literal)]
        [Obsolete]
        public PaymentInfo TestEncoding(Customer customer, string amount,
            PaymentMethod method)
        {
            try {
                return new PaymentInfo(customer, amount, method);
            } catch (Exception e) {
                LogException(e);
                throw e;
            }
        }

        /// <summary>
        /// Logs the given exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        private void LogException(Exception e)
        {
            Log.Stream stream = 
                myGateway.Connection.Log.GetStream(LogLevel.FATAL_ERROR);
            if ( stream &&
                 ( !(e is PaymentException) ||
                   !(e as PaymentException).IsLogged() ) )
            {
                stream.Write(e.TargetSite, e.Message, e);
            }
            throw e;
        }

        private Gateway myGateway;
    }

}
