﻿/*******************************************************************************
 *
 * Defines the class Provider.
 *
 * File:        PaymentGateway/Library/Template.cs
 * Date:        Fri Oct 31 18:18:29 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Configuration;

namespace MMI.Payment
{

      /// <summary>
      /// Represents a payment services provider.
      /// </summary>
    public abstract class Provider
    {
        /// <summary>
        /// Constructs a <see cref="Provider"/>.
        /// </summary>
        /// <param name="name">The provider name, as referenced in the
        /// gateway configuration.</param>
        /// <param name="log">A log.</param>
        protected Provider(string name, Log log)
        {
            myName = name;
            myLog = log;
        }

        /// <summary>
        /// Processes a transaction of type SALE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public virtual void ProcessSale(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type SALE",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type AUTHORIZATION.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public virtual void ProcessAuthorization(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type AUTHORIZATION",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type CREDIT.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public virtual void ProcessCredit(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type CREDIT",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type CAPTURE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior transaction of type CAPTURE 
        /// to which the current transaction relates.</param>
        public virtual void ProcessCapture(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type CAPTURE",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type RETURN.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction 
        /// relates.</param>
        public virtual void ProcessReturn(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction, 
            string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type RETURN",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type VOID.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction 
        /// relates.</param>
        public virtual void ProcessVoid(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type QUERY.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction whose status is to be queried.</param>
        public virtual void ProcessQuery(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type CREATE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public virtual void ProcessCreateInfo(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type QUERY_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// queried.</param>
        public virtual void ProcessQueryInfo(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the 
        /// prior transaction that registered the payment information to be 
        /// updated.</param>
        public virtual void ProcessUpdateInfo(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction, 
            string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type DELETE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// deleted.</param>
        public virtual void ProcessDeleteInfo(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type CREATE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The payment schedule.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public virtual void ProcessCreateSchedule(ProviderAccount account, 
            PaymentInfo info, Schedule schedule, GatewayTransaction transaction)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type QUERY_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// queried.</param>
        public virtual void ProcessQuerySchedule(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The updated payment schedule.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// updated.</param>
        public virtual void ProcessUpdateSchedule(ProviderAccount account, 
            PaymentInfo info, Schedule schedule, GatewayTransaction transaction,
            string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// Processes a transaction of type DELETE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// deleted.</param>
        public virtual void ProcessDeleteSchedule(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            throw new PaymentException(
                          String.Format(
                              "Provider '{0}' does not support the " +
                                  "transaction type VOID",
                              Name
                          ),
                          Status.Code.INVALID_TRANSACTION_TYPE
                      );
        }

        /// <summary>
        /// The provider name, e.g., "vanco"
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return myName; }
        }

        /// <summary>
        /// Provides access to the payment gateway log.
        /// </summary>
        /// <value>The log.</value>
        protected MMI.Payment.Log Log
        {
            get { return myLog; }
        }
        private string   myName;
        private Log      myLog;
    }
}
