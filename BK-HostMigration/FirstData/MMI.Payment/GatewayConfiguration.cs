﻿/*******************************************************************************
 *
 * Defines the interface GatewayConfiguration.
 *
 * File:        PaymentGateway/Library/GatewayConfiguration.cs
 * Date:        Fri Nov  7 14:24:48 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;

namespace MMI.Payment
{
    /// <summary>
    /// Encapsulates a payment gateway configuration. 
    /// </summary>
    public interface IGatewayConfiguration : IDictionary<string, string> { }
}
