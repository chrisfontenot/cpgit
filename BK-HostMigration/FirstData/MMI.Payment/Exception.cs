﻿/*******************************************************************************
 *
 * Defines the class PaymentException.
 *
 * File:        PaymentGateway/Library/Exception.cs
 * Date:        Tue Oct 28 16:50:08 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Runtime.Serialization;

namespace MMI.Payment
{

      /// <summary>
      /// Thrown to components in the namespace MMI.Payment to
      /// report errors
      /// </summary>
    [Serializable]
    public class PaymentException : ApplicationException
    {
        /// <summary>
        /// Constructs a <see cref="PaymentException"/> from an error message;
        /// the error code is set to Status.Code.INTERNAL_ERROR.
        /// </summary>
        /// <param name="message">The message.</param>
        public PaymentException(string message)
            : this(message, Status.Code.INTERNAL_ERROR)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentException"/> from an error message
        /// and an error code.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The error code.</param>
        public PaymentException(string message, Status.Code code)
            : base(message)
        {
            myCode = code;
            myLogged = false;
        }

        /// <summary>
        /// Constructs a <see cref="PaymentException"/> from an error message,
        /// an error code, an an exception representing the underlying cause
        /// of the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The error code.</param>
        /// <param name="inner">An exception.</param>
        public PaymentException(string message, Status.Code code, Exception inner)
            : base(message, inner)
        {
            myCode = code;
            myLogged = false;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>The code.</value>
        public Status.Code Code
        {
            get { return myCode; }
        }

        /// <summary>
        /// Determines whether this instance has been logged.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance has been logged; otherwise, <c>false</c>.
        /// </returns>
        public bool IsLogged()
        {
            return myLogged;
        }

        /// <summary>
        /// Marks this instance as logged.
        /// </summary>
        public void SetLogged()
        {
            myLogged = true;
        }

        /// <summary>
        /// Constructor used for deserialization.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="ctx">The CTX.</param>
        protected PaymentException(SerializationInfo info, StreamingContext ctx)
            : base(info, ctx)
        {
            int code = info.GetInt32("Code");
            myCode = (Status.Code) Enum.ToObject(typeof(Status.Code), code);
            myLogged = info.GetBoolean("Logged");
        }

        /// <summary>
        /// Implements serialization.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="ctx">The CTX.</param>
        public override void GetObjectData(SerializationInfo info, 
            StreamingContext ctx)
        {
            base.GetObjectData(info, ctx);
            info.AddValue("Code", (int) myCode);
            info.AddValue("Logged", myLogged);
        }

        private Status.Code  myCode;
        private bool         myLogged;
    }
}
