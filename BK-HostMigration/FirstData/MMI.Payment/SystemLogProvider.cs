﻿/*******************************************************************************
 *
 * Defines the class SystemLogProvider.
 *
 * File:        PaymentGateway/Library/SystemLogProvider.cs
 * Date:        Sat Nov  1 14:38:43 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Diagnostics;

namespace MMI.Payment
{
      /// <summary>
      /// Records log entries in the Windows event log.
      /// </summary>
    public class SystemLogProvider : ILogProvider
    {
        public static readonly string DefaultEventSource = "MMI Payment Gateway";
        public static readonly string EventLog = "Application";

        /// <summary>
        /// Constructs a <see cref="SystemLogProvider"/>.
        /// </summary>
        public SystemLogProvider() : this(DefaultEventSource) { }

        /// <summary>
        /// Constructs a <see cref="SystemLogProvider"/> with a custom
        /// source name.
        /// </summary>
        /// <param name="source">The source.</param>
        public SystemLogProvider(string source)
        {
            mySource = source;
        }

        /// <summary>
        /// Processes a log entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void NewEntry(LogEntry entry)
        {
            // Check whether event source exists
            if (!System.Diagnostics.EventLog.SourceExists(mySource)) {

                // Attempt to create event source
                try {
                    System.Diagnostics.EventLog.CreateEventSource(mySource,
                        SystemLogProvider.EventLog);
                } catch (Exception) { }

                // Check again
                if (!System.Diagnostics.EventLog.SourceExists(mySource))
                    throw new PaymentException(
                                  "No such event source: " + mySource,
                                  Status.Code.LOG_ERROR
                              );
            }
            
            EventLog log = null;
            try {
                log = new EventLog();
                log.Source = mySource;
                log.WriteEntry(entry.Format(), GetType(entry));
            } catch (Exception e) {
                throw new PaymentException(
                              "System log listener error: " + e.Message,
                              Status.Code.LOG_ERROR,
                              e
                          );
            } finally {
                if (log != null)
                    log.Dispose();
            }
        }

        /// <summary>
        /// No-op.
        /// </summary>
        public void Dispose() { }

        /// <summary>
        /// Translates a log level to a Windows event log entry type.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <returns>The Windows log entry type.</returns>
        private EventLogEntryType GetType(LogEntry entry)
        {
            switch (entry.Level) 
            {
                case LogLevel.FATAL_ERROR:
                case LogLevel.ERROR:
                    return EventLogEntryType.Error;
                case LogLevel.WARNING:
                    return EventLogEntryType.Warning;
                default:
                    return EventLogEntryType.Information;
            }
        }

        private string mySource;
    }
}
