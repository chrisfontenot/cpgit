﻿/*******************************************************************************
 *
 * Defines the class Session.
 *
 * File:        PaymentGateway/Library/Session.cs
 * Date:        Fri Oct 31 17:02:08 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
      /// <summary>
      /// Used to associate log messages with the gateway transactions.
      /// Instead of tagging gateway transactions and log 
      /// messages with session IDs, it would be more elegant to tag log 
      /// messages with the database record ID of the corresponding
      /// gateway transaction. Unfortunately, log messages are sometimes
      /// generated before a transaction is stored in the database (e.g., if 
      /// the database connection fails), so that approach does not work.
      /// </summary>
    public class Session 
    {
        /// <summary>
        /// A GUID uniquely identifying a gateway transaction.
        /// </summary>
        public static readonly Guid Id = Guid.NewGuid();
    }
}
