﻿/*******************************************************************************
 *
 * Defines the class Customer.
 *
 * File:        PaymentGateway/Library/Customer.cs
 * Date:        Sat Nov  1 11:32:35 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
      /// <summary>
      /// Represents a customer
      /// </summary>
    [Serializable]
    public class Customer : Address {

        /// <summary>
        /// Default constructor, needed for XML serialization.
        /// </summary>
        public Customer() { }

        /// <summary>
        /// Constructs a <see cref="Customer"/>.
        /// </summary>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="id">The customer ID.</param>
        /// <param name="street1">The street address.</param>
        /// <param name="street2">The second line, if any, of the 
        /// street address.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The ZIP code.</param>
        /// <param name="phone">The phone number, if any.</param>
        public Customer( string firstName, string lastName, string id, 
                         string street1, string street2, string city, 
                         string state, string zip, string phone )
            : base(street1, street2, city, state, zip)
        {
            myFirstName = firstName;
            myLastName = lastName;
            myId = id;
            myPhone = phone;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">The customer to be copied.</param>
        public Customer(Customer other)
            : this( other.FirstName, other.LastName, other.Id, other.Street1,
                    other.Street2, other.City, other.State, other.Zip,
                    other.Phone )
            { }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName 
        { 
            get { return myFirstName; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "first name", 1, 50);
                }
                myFirstName = value; 
            }
        }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName 
        { 
            get { return myLastName; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "last name", 1, 50);
                }
                myLastName = value; 
            }
        }

        /// <summary>
        /// Gets or sets the customer ID.
        /// </summary>
        /// <value>The id.</value>
        public string Id 
        { 
            get { return myId; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "customer ID", 1, 15);
                }
                myId = value; 
            }
        }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone.</value>
        public string Phone 
        { 
            get { return myPhone; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "10 digit US phone number", 10, 20);
                }
                myPhone = value; 
            }
        }

        public static bool operator==(Customer lhs, Customer rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(Customer lhs, Customer rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            Customer customer = (Customer) other;
                return FirstName == customer.FirstName &&
                       LastName == customer.LastName &&
                       Id == customer.Id &&
                       Phone == customer.Phone &&
                       base.Equals(other);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (FirstName + LastName + Id + Phone).GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[FirstName: {0}; LastName: {1}; Id: {2}; Phone: {3}; " +
                           "Address: {4}]",
                       FirstName, LastName, Id, Phone, base.ToString()
                   );
        }

        private string myFirstName;
        private string myLastName;
        private string myId;
        private string myPhone;
    }
}
