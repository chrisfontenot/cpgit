﻿/*******************************************************************************
 *
 * Defines the class FirstDataProvider.
 *
 * File:        PaymentGateway/Library/Template.cs
 * Date:        Fri Oct 31 18:18:29 MDT 2008
 *
 * Copyright:   2009 Money Management International
 * Author:      CodeRage
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using MMI.Payment.FirstData;

namespace MMI.Payment
{
    using Facet = Schedule.Facet;
    using Pattern = Schedule.Pattern;
    using Rule = Schedule.Rule;

      /// <summary>
      /// Represents a payment services provider.
      /// </summary>
    public class FirstDataProvider : Provider
    {
        /// <summary>
        /// Name of the FirstData provider, as passed to
        /// <see cref="M:Mmi.Payment.Gateway.ProcessTransaction"/>.
        /// </summary>
        public static readonly string ProviderName = "first_data";

        /// <summary>
        /// URL of the FirstData PayPoint production server
        /// </summary>
        public static readonly string ProductionUri =
            "https://www.govone.com/epayadmin/epaywebservice.asmx?WSDL";

        /// <summary>
        /// URL of the FirstData PayPoint test server
        /// </summary>
        public static readonly string DevelopmentUri =
            "https://uat.fdgs.com/epay/epaywebservice.asmx?WSDL";

        public delegate Result EPayFunction<Request, Result>(Request req);

        /// <summary>
        /// Constructs a <see cref="FirstDataProvider"/> from a gateway
        /// configuration and a log.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        /// <param name="log">The log.</param>
        public FirstDataProvider(IGatewayConfiguration config, Log log)
            : base(ProviderName, log)
        {
            // Determine request URI
            string key = "provider.first_data.request_uri";
            if (config.ContainsKey(key)) {
                myClient = new EPayWebService();
                myClient.Url = config[key];
            } else {
                throw new PaymentException(
                              String.Format(
                                  "Missing value for configuration property " +
                                      "'{0}",
                                  key
                              ),
                              Status.Code.CONFIGURATION_ERROR
                          );
            }

            // Determine whether we are in test mode
            key = "provider.first_data.test_mode";
            myTestMode = config.ContainsKey(key) && config[key] == "true";
        }

        /// <summary>
        /// Processes a transaction of type SALE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public override void ProcessSale(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            // Debit cards require special validation
            ValidateDebitCard(account, transaction, info);

            // Construct request
            EPayMakePaymentRequest request = new EPayMakePaymentRequest();
            request.RegistrationID = CreateEPayRegisterID(info);
            request.PaymentAmount = info.Amount;
            request.PaymentInfo = CreateEPayPaymentInfo(info);
            request.Reference = info.Customer.Id; // Set Reference field to Account Number

            // Process request
            ProcessEPayRequest<EPayMakePaymentRequest, EPayMakePaymentResult>
                (account, transaction, request, myClient.MakePayment);
        }

        /// <summary>
        /// Processes a transaction of type RETURN.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction 
        /// relates.</param>
        public override void ProcessReturn(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction, 
            string referenceCode)
        {
            // Construct request
            EPayCancelPaymentRequest request = new EPayCancelPaymentRequest();
            request.ConfirmationNumber = referenceCode;
            request.RefundAmount = info.Amount;

            // Process request
            ProcessEPayRequest<EPayCancelPaymentRequest, EPayCancelPaymentResult>
                (account, transaction, request, myClient.CancelPayment);
        }

        /// <summary>
        /// Processes a transaction of type QUERY.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction whose status is to be queried.</param>
        public override void ProcessQuery(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            // Construct request
            EPayPaymentStatusRequest request = new EPayPaymentStatusRequest();
            request.ConfirmationNumber = referenceCode;

            // Process request
            ProcessEPayRequest<EPayPaymentStatusRequest, EPayPaymentStatusResult>
                (account, transaction, request, myClient.PaymentStatus);
        }

        /// <summary>
        /// Processes a transaction of type CREATE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public override void ProcessCreateInfo(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction)
        {
            // Debit cards require special validation
            ValidateDebitCard(account, transaction, info);

            // Construct request
            EPayRegistrationCRDRequest request = new EPayRegistrationCRDRequest();
            request.Action = EPayCRDAction.Create;
            request.AgreedToTerms = true;
            request.PaymentInfo = CreateEPayPaymentInfo(info);

            // Clear card status flag and CVV2
            if (request.PaymentInfo.PaymentInfoCC != null) {
                request.PaymentInfo.PaymentInfoCC.CardStatusFlag = 
                    EPayCreditCardStatusFlag.Unknown;
                request.PaymentInfo.PaymentInfoCC.CVV2 = null;
            }

            // Process request
            ProcessEPayRequest<EPayRegistrationCRDRequest, EPayRegistrationCRDResult>
                (account, transaction, request, myClient.RegistrationCRD);
        }

        /// <summary>
        /// Processes a transaction of type QUERY_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// queried.</param>
        public override void ProcessQueryInfo(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            // Construct request
            EPayRegistrationInquiryRequest request = new EPayRegistrationInquiryRequest();
            request.RegistrationID = referenceCode;

            // Process request
            EPayRegistrationInquiryResult result =
                ProcessEPayRequest<EPayRegistrationInquiryRequest, EPayRegistrationInquiryResult>
                    (account, transaction, request, myClient.RegistrationInquiry);

            // Set OutputPaymentInfo property of gateway transaction
            transaction.OutputPaymentInfo = CreatePaymentInfo(result.PaymentInfo);
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be updated.</param>
        public override void ProcessUpdateInfo(ProviderAccount account, 
            PaymentInfo info, GatewayTransaction transaction, 
            string referenceCode)
        {
            // Debit cards require special validation
            ValidateDebitCard(account, transaction, info);

            // Construct request
            EPayRegistrationCRDRequest request = new EPayRegistrationCRDRequest();
            request.Action = EPayCRDAction.Update;
            request.AgreedToTerms = true;
            request.RegistrationID = referenceCode;
            request.PaymentInfo = CreateEPayPaymentInfo(info);

            // Clear card status flag and CVV2
            if (request.PaymentInfo.PaymentInfoCC != null) {
                request.PaymentInfo.PaymentInfoCC.CardStatusFlag = 
                    EPayCreditCardStatusFlag.Unknown;
                request.PaymentInfo.PaymentInfoCC.CVV2 = null;
            }

            // Process request
            ProcessEPayRequest<EPayRegistrationCRDRequest, EPayRegistrationCRDResult>
                (account, transaction, request, myClient.RegistrationCRD);
        }

        /// <summary>
        /// Processes a transaction of type DELETE_INFO.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// deleted.</param>
        public override void ProcessDeleteInfo(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            // Construct request
            EPayRegistrationCRDRequest request = new EPayRegistrationCRDRequest();
            request.Action = EPayCRDAction.Delete;
            request.AgreedToTerms = true;
            request.RegistrationID = referenceCode;

            // Process request
            ProcessEPayRequest<EPayRegistrationCRDRequest, EPayRegistrationCRDResult>
                (account, transaction, request, myClient.RegistrationCRD);
        }

        /// <summary>
        /// Processes a transaction of type CREATE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The payment schedule.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        public override void ProcessCreateSchedule(ProviderAccount account, 
            PaymentInfo info, Schedule schedule, GatewayTransaction transaction)
        {
            // Construct request
            EPayRecurringPaymentCRDRequest request = new EPayRecurringPaymentCRDRequest();
            request.Action = RecurringPaymentAction.Create;
            request.RecurringPaymentInfo = 
                CreateEPayRecurringPaymentInfo(info, schedule);

            // Process request
            ProcessEPayRequest<EPayRecurringPaymentCRDRequest, EPayRecurringPaymentCRDResult>
                (account, transaction, request, myClient.RecurringPaymentCRD);
        }

        /// <summary>
        /// Processes a transaction of type QUERY_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// queried.</param>
        public override void ProcessQuerySchedule(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            // Construct request
            EPayRecurringPaymentInquiryRequest request = 
                new EPayRecurringPaymentInquiryRequest();
            request.RecurringID = referenceCode;

            // Process request
            EPayRecurringPaymentInquiryResult result =
                ProcessEPayRequest<EPayRecurringPaymentInquiryRequest, EPayRecurringPaymentInquiryResult>
                    (account, transaction, request, myClient.RecurringPaymentInquiry);

            // Set OutputPaymentInfo and OutputSchedule properties 
            // of gateway transaction
            transaction.OutputPaymentInfo = 
                CreatePaymentInfo(result.RecurringPaymentInfo);
            transaction.OutputSchedule = 
                CreateSchedule(result.RecurringPaymentInfo);
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The updated payment schedule.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// updated.</param>
        public override void ProcessUpdateSchedule(ProviderAccount account, 
            PaymentInfo info, Schedule schedule, GatewayTransaction transaction,
            string referenceCode)
        {
            // Construct request
            EPayRecurringPaymentCRDRequest request = new EPayRecurringPaymentCRDRequest();
            request.Action =             // Note: enumerator RecurringPaymentAction.Update 
                (RecurringPaymentAction) // is missing
                    Enum.ToObject(typeof(RecurringPaymentAction), 2);
            request.RecurringPaymentInfo = 
                CreateEPayRecurringPaymentInfo(info, schedule);
            request.RecurringID = referenceCode;

            // Process request
            ProcessEPayRequest<EPayRecurringPaymentCRDRequest, EPayRecurringPaymentCRDResult>
                (account, transaction, request, myClient.RecurringPaymentCRD);
        }

        /// <summary>
        /// Processes a transaction of type DELETE_SCHEDULE.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// deleted.</param>
        public override void ProcessDeleteSchedule(ProviderAccount account, 
            GatewayTransaction transaction, string referenceCode)
        {
            // Construct request
            EPayRecurringPaymentCRDRequest request = new EPayRecurringPaymentCRDRequest();
            request.Action = RecurringPaymentAction.Delete;
            request.RecurringID = referenceCode;

            // Process request
            ProcessEPayRequest<EPayRecurringPaymentCRDRequest, EPayRecurringPaymentCRDResult>
                (account, transaction, request, myClient.RecurringPaymentCRD);
        }

        /// <summary>
        /// Throws an exception if a debit card that is not signature eligible
        /// has been specified as a payment method.
        /// </summary>
        /// <param name="account">A provider account.</param>
        /// <param name="transaction">The current gateway transaction.</param>
        /// <param name="info">The payment information.</param>
        private void ValidateDebitCard(ProviderAccount account,
            GatewayTransaction transaction, PaymentInfo info)
        {
            if (info.PaymentMethod.Type != AccountType.DEBIT || myTestMode)
                return;

            // Construct request
            EPayPINlessDebitCheckRequest request = new EPayPINlessDebitCheckRequest();
            request.CardNumber = (info.PaymentMethod as DebitCard).CardNumber;

            // Process request
            EPayPINlessDebitCheckResult result = 
                ProcessEPayRequest<EPayPINlessDebitCheckRequest, EPayPINlessDebitCheckResult>
                    (account, transaction, request, myClient.PINlessDebitCheck);

            if (!result.SignatureEligible)
                throw new 
                    PaymentException(
                        "Card may not be used in a PINless debit transaction",
                        Status.Code.INVALID_PAYMENT_METHOD
                    );
        }

        /// <summary>
        /// Processes a FirstData PayPoint transaction.
        /// </summary>
        /// <typeparam name="Request">The request type.</typeparam>
        /// <typeparam name="Result">The result type.</typeparam>
        /// <param name="account">The provider account.</param>
        /// <param name="transaction">The gateway transaction.</param>
        /// <param name="request">The request instance.</param>
        /// <param name="function">The payment information.</param>
        /// <returns>The result.</returns>
        private Result ProcessEPayRequest<Request, Result>(
                ProviderAccount account, 
                GatewayTransaction transaction,
                Request request, 
                EPayFunction<Request, Result> function)
            where Request : EPayRequest
            where Result : EPayResult
        {
            // Construct provider transation
            string name = typeof(Request).Name;
            string type = name.Substring(0, name.Length - 7); // Strip "Request"
            ProviderTransaction providerTrans = 
                transaction.NewProviderTransaction(type);

            // Add request header
            request.Header = CreateEPayHeader(account);

            // Process request
            Result result = null;
            try {
                result = function(request);
            } catch (Exception e) {
                FailTransaction(providerTrans, e);
            }

            // Complete provider transaction and set gateway transaction reference code
            Status.Code code = CreateStatusCode(result.ReturnCode);
            ProviderStatus status = CreateProviderStatus(result);
            providerTrans.Complete(code, status);

            // Set transaction reference code
            transaction.ReferenceCode = status.ReferenceCode;
            transaction.ProviderTransactionCode = status.TransactionCode;

            if (code != Status.Code.SUCCESS)
                throw new PaymentException(status.ErrorMessage, code);

            return result;
        }

        /// <summary>
        /// Takes an <see cref="ProviderAccount"/> instance and returns a
        /// corresponding <see cref="FirstData.EPayHeader"/> instance.
        /// </summary>
        /// <param name="account">The provider account.</param>
        public EPayHeader CreateEPayHeader(ProviderAccount account)
        {
            EPayHeader result = new EPayHeader();
            result.ApplicationID = account.Username;
            result.SecurityKey = account.Password;
            result.PaymentChannel = EPayPaymentChannel.Web;
            return result;
        }

        /// <summary>
        /// Takes an <see cref="PaymentInfo"/> instance and returns a
        /// registered account identifier, if the underlying payment
        /// method has type <see cref="AccountType.TOKEN"/>.
        /// </summary>
        /// <param name="info">The payment information.</param>
        private string CreateEPayRegisterID(PaymentInfo info)
        {
            return info.PaymentMethod.Type == AccountType.TOKEN ?
                ((PaymentToken) info.PaymentMethod).Id :
                null;
        }

        /// <summary>
        /// Takes an <see cref="PaymentInfo"/> instance and returns a
        /// corresponding <see cref="FirstData.EPayPaymentInfo"/> instance.
        /// </summary>
        /// <param name="info">The payment information.</param>
        private EPayPaymentInfo CreateEPayPaymentInfo(PaymentInfo info)
        {
            EPayPaymentInfoCC cc = null;
            EPayPaymentInfoEFT eft = null;
            EPayPaymentMedium medium;
            switch (info.PaymentMethod.Type) {
            case AccountType.CUSTOM:
            case AccountType.TOKEN:
                return null;
            case AccountType.CREDIT:
            case AccountType.DEBIT:
                Card card = (Card) info.PaymentMethod;
                medium = EPayPaymentMedium.CreditCard;
                cc = new EPayPaymentInfoCC();
                cc.BillingAddress = 
                    CreateEPayAddress(card.CardHolder, card.BillingAddress);
                cc.ShippingAddress = CreateEPayAddress(info.Customer);
                cc.CardNumber = card.CardNumber;
                cc.CardStatusFlag = EPayCreditCardStatusFlag.Not_Present;
                cc.CardType = EPayCardType.Unknown;
                cc.CVV2 = card.VerificationCode;
                cc.ExpirationMonth = card.ExpirationMonth;
                cc.ExpirationYear = card.ExpirationYear.Substring(2);
                if (card.GetParameter("BillingFirstName") != null)
                    cc.BillingAddress.NameFirst = 
                        card.GetParameter("BillingFirstName");
                if (card.GetParameter("BillingLastName") != null)
                    cc.BillingAddress.NameLast = 
                        card.GetParameter("BillingLastName");
                break;
            case AccountType.CHECKING:
            case AccountType.SAVINGS:
                BankAccount account = (BankAccount) info.PaymentMethod;
                medium = EPayPaymentMedium.eCheck;
                eft = new EPayPaymentInfoEFT();
                eft.AccountType = info.PaymentMethod.Type == AccountType.CHECKING ?
                    EPayEFTAccountType.Checking :
                    EPayEFTAccountType.Savings;
                eft.AddressBilling = 
                    CreateEPayAddress(account.AccountHolder, account.BillingAddress);
                eft.AddressShipping = CreateEPayAddress(info.Customer);
                eft.BankAccountNumber = account.AccountNumber;
                eft.BankName = account.GetParameter("BankName");
                eft.BankRoutingNumber = account.RoutingNumber;
                eft.BankState = account.GetParameter("BankState");
                eft.DriversLicenseNumber = account.GetParameter("DriversLicenseNumber");
                eft.DriversLicenseState = account.GetParameter("DriversLicenseState");
                eft.SSN = account.GetParameter("SSN");
                eft.BusinessName = account.GetParameter("BusinessName");
                eft.FederalTaxID = account.GetParameter("FederalTaxID");
                if (account.GetParameter("BillingFirstName") != null)
                    eft.AddressBilling.NameFirst = 
                        account.GetParameter("BillingFirstName");
                if (account.GetParameter("BillingLastName") != null)
                    eft.AddressBilling.NameLast = 
                        account.GetParameter("BillingLastName");
                break;
            default:
                throw new
                    PaymentException(
                        String.Format(
                            "Invalid AccountType: {0}",
                            info.PaymentMethod.Type
                        )
                    );
            }
            EPayPaymentInfo result = new EPayPaymentInfo();
            result.PaymentInfoCC = cc;
            result.PaymentInfoEFT = eft;
            result.PaymentMedium = medium;
            return result;
        }

        /// <summary>
        /// Takes a <see cref="FirstData.EPayPaymentInfo"/> instance and returns a 
        /// corresponding <see cref="PaymentInfo"/> instance.
        /// </summary>
        /// <param name="info">The payment information.</param>
        private PaymentInfo CreatePaymentInfo(EPayPaymentInfo info)
        {
            return CreatePaymentInfo(info, "0.00");
        }

        /// <summary>
        /// Takes a <see cref="FirstData.EPayPaymentInfo"/> instance and 
        /// a payment amount and returns a corresponding 
        /// <see cref="PaymentInfo"/> instance.
        /// </summary>
        /// <param name="info">The payment information.</param>
        /// <param name="amount">The payment amount.</param>
        private PaymentInfo CreatePaymentInfo(EPayPaymentInfo info, 
            string amount)
        {
            bool isCard =
                info.PaymentMedium == EPayPaymentMedium.CheckCard ||
                info.PaymentMedium == EPayPaymentMedium.CommercialCreditCard ||
                info.PaymentMedium == EPayPaymentMedium.CreditCard ||
                info.PaymentMedium == EPayPaymentMedium.PINDebit ||
                info.PaymentMedium == EPayPaymentMedium.PINlessDebit;
            EPayAddress shipping = isCard ?
                info.PaymentInfoCC.ShippingAddress :
                info.PaymentInfoEFT.AddressShipping;
            EPayAddress billing = isCard ?
                info.PaymentInfoCC.BillingAddress :
                info.PaymentInfoEFT.AddressBilling;
            Pair<Customer, Address> ca = 
                CreateCustomerAndAddress(shipping, billing);
            PaymentInfo result = new PaymentInfo(ca.First, amount, null);
            if (isCard) {
                EPayPaymentInfoCC cc = info.PaymentInfoCC;
                Card card = info.PaymentMedium == EPayPaymentMedium.CreditCard ?
                    (Card) new CreditCard() :
                    (Card) new DebitCard();
                result.PaymentMethod = card;
                card.CardHolder = billing.NameFull != "" ?
                    billing.NameFull :
                    billing.NameFirst + " " + billing.NameLast;
                card.CardNumber = cc.CardNumber;
                card.ExpirationMonth = cc.ExpirationDate != "" ?
                    cc.ExpirationDate.Substring(0, 2) :
                    cc.ExpirationMonth;
                card.ExpirationYear = cc.ExpirationDate != "" ?
                    "20" + cc.ExpirationDate.Substring(2) :
                    "20" + cc.ExpirationYear;
                card.VerificationCode = cc.CVV2 != null && cc.CVV2 != "" ?
                    cc.CVV2 :
                    null;
                card.BillingAddress = ca.Second;
                if (cc.CardType != EPayCardType.Unknown) 
                    card.AddParameter("CardType", cc.CardType.ToString());
            } else {
                EPayPaymentInfoEFT eft = info.PaymentInfoEFT;
                BankAccount account = new BankAccount();
                result.PaymentMethod = account;
                account.Type = eft.AccountType == EPayEFTAccountType.Checking ?
                    AccountType.CHECKING :
                    AccountType.SAVINGS;
                account.AccountHolder = billing.NameFull != null ?
                    billing.NameFull :
                    billing.NameFirst + " " + billing.NameLast;
                account.AccountNumber = eft.BankAccountNumber;
                account.RoutingNumber = eft.BankRoutingNumber;
                account.BillingAddress = ca.Second;
                if (eft.BankName != "")
                    account.AddParameter("BankName", eft.BankName);
                if (eft.BankState != "")
                    account.AddParameter("BankState", eft.BankState);
                if (eft.DriversLicenseNumber != "")
                    account.AddParameter(
                        "DriversLicenseNumber", 
                        eft.DriversLicenseNumber
                    );
                if (eft.DriversLicenseState != "")
                    account.AddParameter(
                        "DriversLicenseState", 
                        eft.DriversLicenseState
                    );
                if (eft.SSN != "")
                    account.AddParameter("SSN", eft.SSN);
                if (eft.BusinessName != "")
                    account.AddParameter("BusinessName", eft.BusinessName);
                if (eft.FederalTaxID != "")
                    account.AddParameter("FederalTaxID", eft.FederalTaxID);
            }
            if (shipping.Phone2 != "")
                result.PaymentMethod.AddParameter("Phone2", shipping.Phone2);
            if (billing.Phone1 != "")
                result.PaymentMethod.AddParameter("BillingPhone", billing.Phone1);
            if (billing.Phone2 != "")
                result.PaymentMethod.AddParameter("BillingPhone2", billing.Phone2);
            if (shipping.Email != "")
                result.PaymentMethod.AddParameter("Email", shipping.Email);
            if (billing.Email != "")
                result.PaymentMethod.AddParameter("BillingEmail", billing.Email);
            return result;
        }

        /// <summary>
        /// Takes a <see cref="FirstData.EPayRecurringPaymentInfo"/> instance
        /// and returns a corresponding <see cref="PaymentInfo"/> instance.
        /// </summary>
        /// <param name="info">The payment information.</param>
        private PaymentInfo CreatePaymentInfo(EPayRecurringPaymentInfo info)
        {
            return 
                new PaymentInfo(
                        info.PaymentAmount,
                        new PaymentToken(info.RegistrationID)
                    );
        }

        /// <summary>
        /// Takes a <see cref="PaymentInfo"/> instance and a 
        /// <see cref="PaymentInfo"/> instance and returns a corresponding 
        /// <see cref="FirstData.EPayRecurringPaymentInfo"/> instance.
        /// This method is public only to facilitate testing.
        /// </summary>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The schedule.</param>
        /// <param name="memo">The descriptive message.</param>
        public static EPayRecurringPaymentInfo CreateEPayRecurringPaymentInfo(
            PaymentInfo info, Schedule schedule)
        {
            if (info.PaymentMethod.Type != AccountType.TOKEN)
                throw new 
                    PaymentException(
                        String.Format(
                            "Invalid account type: expected TOKEN; found {0}",
                            info.PaymentMethod.Type.ToString()
                        )
                    );
            PaymentToken token = (PaymentToken) info.PaymentMethod;
            EPayRecurringPaymentInfo result = new EPayRecurringPaymentInfo();
            result.PaymentAmount = info.Amount;
            result.RegistrationID = token.Id;
            result.BeginDate = schedule.Begin.ToString("MM/dd/yyyy");
            result.EndDate = schedule.End.ToString("MM/dd/yyyy");
            result.IntervalParam1 = "";
            result.IntervalParam2 = "";
            result.IntervalParam3 = "";
            result.IntervalParam4 = "";
            Schedule.Facet mask = schedule.Facets();
            if ((mask & Facet.DAY) != 0) {
                result.RecurringIntervalType = EPayRecurringIntervalType.Daily;
                Schedule.Pattern pattern = schedule.GetPattern(Facet.DAY);
                if (pattern.HasPeriod()) {
                    result.IntervalParam1 = "Interval";
                    result.IntervalParam2 = pattern.Period.ToString();
                }
            } else if ((mask & Facet.WEEKOFMONTH) != 0) {
                result.RecurringIntervalType = EPayRecurringIntervalType.Monthly;
                Schedule.Pattern pattern = schedule.GetPattern(Facet.WEEKOFMONTH);
                result.IntervalParam1 = "DaysByWeek";
                result.IntervalParam2 = pattern.Values[0].ToString();
                result.IntervalParam3 = 
                    (schedule.GetPattern(Facet.DAYOFWEEK).Values[0] + 1)
                        .ToString();
                if ((mask & Facet.MONTH) != 0) {
                    pattern = schedule.GetPattern(Facet.MONTH);
                    result.IntervalParam4 = pattern.Period.ToString();
                }
            } else if ((mask & Facet.DAYOFWEEK) != 0) {
                result.RecurringIntervalType = EPayRecurringIntervalType.Daily;
                Schedule.Pattern pattern = schedule.GetPattern(Facet.DAYOFWEEK);
                result.IntervalParam1 = "Days";
                for (int z = 0; z < 7; ++z)
                    result.IntervalParam2 += pattern.Values.Contains(z) ?
                        "1" :
                        "0";
                if ((mask & Facet.WEEK) != 0) {
                    result.IntervalParam3 = 
                        schedule.GetPattern(Facet.WEEK).Period.ToString();
                }
            } else if ((mask & Facet.DAYOFMONTH) != 0) {
                result.RecurringIntervalType = EPayRecurringIntervalType.Monthly;
                Schedule.Pattern pattern = schedule.GetPattern(Facet.DAYOFMONTH);
                result.IntervalParam1 = "Days";
                result.IntervalParam2 = 
                    String.Join(
                        ", ", 
                        pattern.Values.ConvertAll<string>(
                            (int i) => i.ToString()).ToArray()
                    );
                if ((mask & Facet.DAYSBEFORE) != 0) {
                    pattern = schedule.GetPattern(Facet.DAYSBEFORE);
                    result.IntervalParam3 = pattern.Values[0].ToString();
                }
                if ((mask & Facet.MONTH) != 0) {
                    pattern = schedule.GetPattern(Facet.MONTH);
                    result.IntervalParam4 = pattern.Period.ToString();
                }
            } else if ((mask & Facet.DAYOFYEAR) != 0) {
                result.RecurringIntervalType = EPayRecurringIntervalType.Yearly;
                Schedule.Pattern pattern = schedule.GetPattern(Facet.DAYOFYEAR);
                result.IntervalParam1 = "Days";
                result.IntervalParam2 = 
                    String.Join(
                        ", ", 
                        pattern.Dates.ConvertAll<string>(
                            (Schedule.Date d) => d.ToString()).ToArray()
                    );
            }

            return result;
        }

        /// <summary>
        /// Takes an <see cref="FirstData.EPayRecurringPaymentInfo"/> instance
        /// and returns a corresponding <see cref="Schedule"/> instance.
        /// This method is public only to facilitate testing.
        /// </summary>
        /// <param name="info">The recurring payment information.</param>
        public static Schedule CreateSchedule(EPayRecurringPaymentInfo info)
        {
            Schedule result = new Schedule();
            result.Begin = DateTime.Parse(info.BeginDate);
            result.End = DateTime.Parse(info.EndDate);
            EPayRecurringIntervalType type = info.RecurringIntervalType;
            if (type == EPayRecurringIntervalType.Daily) {
                if (info.IntervalParam1 == "") {
                    result.AddRule(Facet.DAY, "*");
                } else if (info.IntervalParam1 == "Interval") {
                    result.AddRule(
                        Facet.DAY, 
                        "*/" + info.IntervalParam2.ToString()
                    );
                } else if (info.IntervalParam1 == "Days") {
                    string days = info.IntervalParam2;
                    if (days.Length != 7)
                        throw new
                            PaymentException(
                                String.Format(
                                    "Invalid days of week specifier: {0}",
                                    days
                                ),
                                Status.Code.PROVIDER_ERROR
                            );
                    List<string> values = new List<string>();
                    for (int z = 0; z < 7; ++z)
                        if (days[z] == '1')
                            values.Add(z.ToString());
                    result.AddRule(
                        Facet.DAYOFWEEK,
                        String.Join(", ", values.ToArray())
                    );
                    if ( info.IntervalParam3 != null && 
                         info.IntervalParam3 != "" )
                    {
                        result.AddRule(Facet.WEEK, "*/" + info.IntervalParam3);
                    }
                }
            } else if (type == EPayRecurringIntervalType.Monthly) {
                if (info.IntervalParam1 == "Days") {
                    result.AddRule(Facet.DAYOFMONTH, info.IntervalParam2);
                    if ( info.IntervalParam3 != null && 
                         info.IntervalParam3 != "" )
                    {
                        result.AddRule(Facet.DAYSBEFORE, info.IntervalParam3);
                    }
                }
                else if (info.IntervalParam1 == "DaysByWeek")
                {
                    result.AddRule(
                        Facet.DAYOFWEEK,
                        (Int32.Parse(info.IntervalParam3) - 1).ToString()
                    );
                    result.AddRule(Facet.WEEKOFMONTH, info.IntervalParam2);
                }
                if ( info.IntervalParam4 != null && 
                     info.IntervalParam4 != "" )
                {
                    result.AddRule(Facet.MONTH, "*/" + info.IntervalParam4);
                }
            } else if (type == EPayRecurringIntervalType.Yearly) {
                result.AddRule(Facet.DAYOFYEAR, info.IntervalParam2);
            } else {
                throw new
                    PaymentException(
                        String.Format(
                            "Invalid recurring interval type: {0}",
                            type
                        ),
                        Status.Code.PROVIDER_ERROR
                    );
            }
            return result;
        }

        /// <summary>
        /// Takes a <see cref="Customer"/> instance and returns a 
        /// <see cref="FirstData.EPayAddress"/> instance
        /// representing a shipping address.
        /// </summary>
        /// <param name="customer">The customer.</param>
        private EPayAddress CreateEPayAddress(Customer customer)
        {
            EPayAddress result = new EPayAddress();
            result.NameFirst = customer.FirstName;
            result.NameLast = customer.LastName;
            result.Street1 = customer.Street1;
            result.Street2 = customer.Street2;
            result.City = customer.City;
            result.State = customer.State;
            result.Zip = customer.Zip;
            result.Phone1 = customer.Phone;
            return result;
        }

        /// <summary>
        /// Takes a full name and an <see cref="Address"/> 
        /// instance and returns a  <see cref="FirstData.EPayAddress"/> instance
        /// representing a billing address.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        /// <param name="address">The billing address.</param>
        private EPayAddress CreateEPayAddress(String fullName, Address address)
        {
            EPayAddress result = new EPayAddress();

            if (fullName != null && fullName != "" && fullName.Length > 0)
            {
                NameParser.Name name = NameParser.Parse(fullName);
                result.NameFull = fullName;
                result.NameFirst = name.First;
                result.NameLast = name.Last;
            }

            if (address != null) {
                result.Street1 = address.Street1;
                result.Street2 = address.Street2;
                result.City = address.City;
                result.State = address.State;
                result.Zip = address.Zip;
            }
            return result;
        }

        /// <summary>
        /// Takes two <see cref="FirstData.EPayAddress"/> instances, representing 
        /// a shipping address and a billing address, and returns a
        /// pair consisting of a <see cref="Customer"/> instance and a 
        /// <see cref="Address"/> instance.
        /// </summary>
        /// <param name="shipping">The shipping address.</param>
        /// <param name="billing">The billing address.</param>
        private Pair<Customer, Address> CreateCustomerAndAddress(
            EPayAddress shipping, EPayAddress billing)
        {
            EPayAddress address = 
                shipping != null && 
                ( shipping.Street1 != "" || shipping.Street2 != "" ||
                  shipping.City != "" || shipping.State != "" ||
                  shipping.Zip != "" || shipping.Phone1 != "" ) ?
                    shipping :
                    billing;
            String firstName = null;
            String lastName = null;
            if (address.NameFull != null && address.NameFull != "" && address.NameFull.Length > 0) {
                NameParser.Name name = NameParser.Parse(address.NameFull);
                firstName = name.First;
                lastName = name.Last;
            } else {
                firstName = address.NameFirst;
                lastName = address.NameLast;
            }
            Customer c = 
                new Customer(
                        firstName, lastName, null,
                        shipping.Street1 != "" ? 
                            shipping.Street1 : 
                            null,
                        shipping.Street2 != "" ? 
                            shipping.Street2 : 
                            null,
                        shipping.City != "" ? 
                            shipping.City : 
                            null,
                        shipping.State != "" ? 
                            shipping.State : 
                            null,
                        shipping.Zip != "" ? 
                            shipping.Zip : 
                            null,
                        shipping.Phone1 != "" ? 
                            shipping.Phone1 : 
                            null
                    );
            Address a =
                new Address(
                        billing.Street1 != "" ?
                            billing.Street1 :
                            null,
                        billing.Street2 != "" ?
                            billing.Street2 :
                            null,
                        billing.City != "" ?
                            billing.City :
                            null,
                        billing.State != "" ?
                            billing.State :
                            null,
                        billing.Zip != "" ?
                            billing.Zip :
                            null
                    );
            return new Pair<Customer, Address>(c, a);
        }

        /// <summary>
        /// Takes an <see cref="FirstData.EPayResult"/> instance
        /// and returns a corresponding <see cref="ProviderStatus"/> 
        /// instance.
        /// </summary>
        /// <param name="result">The result</param>
        private ProviderStatus CreateProviderStatus(EPayResult result)
        {
            String message = result.ResultMessage;
            String status = ((int) result.ReturnCode).ToString();
            String reference = null;
            String transactionCode = null;

            if (result is EPayCancelPaymentResult) {
                reference = (result as EPayCancelPaymentResult).ConfirmationNumber;
            } else if (result is EPayMakePaymentResult) {
                reference = (result as EPayMakePaymentResult).AuthorizationCode;
                transactionCode = (result as EPayMakePaymentResult).ConfirmationNumber;
            } else if (result is EPayPaymentStatusResult) {
                reference = (result as EPayPaymentStatusResult).PaymentStatus.ToString();
            } else if (result is EPayRecurringPaymentCRDResult) {
                reference = (result as EPayRecurringPaymentCRDResult).RecurringID;
            } else if (result is EPayRecurringPaymentCRDResult) {
                reference = (result as EPayRecurringPaymentCRDResult).RecurringID;
            } else if (result is EPayRegistrationCRDResult) {
                reference = (result as EPayRegistrationCRDResult).RegistrationID;
            }

            return new ProviderStatus(status, message, reference, transactionCode);
        }

        /// <summary>
        /// Takes an <see cref="FirstData.EPayResultCode"/> and returns
        /// and returns a corresponding <see cref="Status.Code"/>.
        /// </summary>
        /// <param name="code">The result code.</param>
        private Status.Code CreateStatusCode(EPayResultCode code)
        {
            switch (code) {
            case EPayResultCode.Cancel_Success:
            case EPayResultCode.Payment_Pending:
            case EPayResultCode.Payment_Success:
            case EPayResultCode.Success:
            case EPayResultCode.Waiting_On_PreNote:
            case EPayResultCode.Eligible:
            case EPayResultCode.Not_Eligible:
                return Status.Code.SUCCESS;
            case EPayResultCode.Communication_Error:
            case EPayResultCode.Network_Error:
                return Status.Code.NETWORK_ERROR;
            case EPayResultCode.CreditCards_Disabled:
            case EPayResultCode.PINDebit_Disabled:
            case EPayResultCode.Unaccepted_Card_Type:
            case EPayResultCode.eChecks_Disabled:
                return Status.Code.INVALID_PAYMENT_METHOD;
            case EPayResultCode.Declined:
            case EPayResultCode.Verification_Failed:
                return Status.Code.PAYMENT_DECLINED;
            case EPayResultCode.Missing_Identification:
            case EPayResultCode.Undefined_Item:
                return Status.Code.INVALID_PARAMETER;
            case EPayResultCode.Post_Date_Too_Large:
                return Status.Code.INVALID_PARAMETER;
            default:
                return Status.Code.PROVIDER_ERROR;
            }
        }

        /// <summary>
        /// Takes an integer representing a <see cref="FirstData.EPayResultCode"/> 
        /// instance and returns a corresponding <see cref="ProviderStatus"/> 
        /// instance.
        /// </summary>
        /// <param name="result">The result</param>
        private Status.Code CreateStatusCode(int result)
        {
            return CreateStatusCode((EPayResultCode) result);
        }

        /// <summary>
        /// Completes the given <see cref="ProviderTransaction"/> instance with
        /// a status of <see cref="Statuc.Code.WEBSERVICE_PROTOCOL_ERROR"/> and
        /// throws a <see cref="PaymentException"/>.
        /// </summary>
        /// <param name="trans">The provider transaction</param>
        /// <param name="e">The exception thrown during transaction 
        /// processing</param>
        private void FailTransaction(ProviderTransaction trans, Exception e)
        {
            Status.Code code = Status.Code.WEBSERVICE_PROTOCOL_ERROR;
            trans.Complete(code);
            throw new 
                PaymentException(
                    String.Format(
                        "Call to '{0}' failed: {1}", 
                        trans.Type, 
                        e.Message
                    ),
                    code,
                    e
                );
        }

        /// <summary>
        /// Validates the given schedule, enforcing FirstData-specific 
        /// constaints.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        private void ValidateSchedule(Schedule schedule)
        {
            Facet mask = schedule.Facets();
            if ( (mask & Facet.DAYSBEFORE) != 0 &&
                     schedule.GetPattern(Facet.DAYSBEFORE).Values[0] > 28 )
            {
                Pattern pattern = schedule.GetPattern(Facet.DAYSBEFORE);
                throw new
                    PaymentException(
                        String.Format(
                            "Unsupported pattern {0} for facet {1}: " +
                                "expected a value from 1 to 28",
                            pattern, Facet.DAYSBEFORE
                        ),
                        Status.Code.INVALID_PARAMETER
                    );
            }
            if ((mask & Facet.WEEKOFMONTH) != 0) {
                Pattern pattern = schedule.GetPattern(Facet.WEEKOFMONTH);
                if (pattern.HasPeriod())
                    throw new
                        PaymentException(
                            String.Format(
                                "Unsupported pattern {0} for facet {1}: " +
                                    "expected a value from 1 to 4",
                                pattern, Facet.WEEKOFMONTH
                            ),
                            Status.Code.INVALID_PARAMETER
                        );
                List<int> values =pattern.Values;
                if (values.Count > 1 || values[0] == 5) {
                    throw new
                        PaymentException(
                            String.Format(
                                "Unsupported pattern {0} for facet {1}: " +
                                    "expected a value from 1 to 4",
                                pattern, Facet.WEEKOFMONTH
                            ),
                            Status.Code.INVALID_PARAMETER
                        );
                }
                if ((mask & Facet.DAYOFWEEK) != 0) {
                    values = schedule.GetPattern(Facet.DAYOFWEEK).Values;
                    if (values.Count > 1) {
                        throw new
                            PaymentException(
                                String.Format(
                                    "Unsupported pattern {0} for facet {1} " +
                                        "when combined with facet {2}: " +
                                        "expected a value from 0 to 6",
                                    schedule.GetPattern(Facet.DAYOFWEEK), 
                                    Facet.DAYOFWEEK, Facet.WEEKOFMONTH
                                ),
                                Status.Code.INCONSISTENT_PARAMETERS
                            );
                    }
                }
            }
            if ( mask == Facet.DAY ||
                 mask == Facet.DAYOFWEEK ||
                 mask == (Facet.DAYOFWEEK | Facet.WEEK) ||
                 (mask & Facet.DAYOFMONTH) != 0 &&
                     (mask & ~(Facet.DAYSBEFORE | Facet.MONTH)) == 0 ||
                 (mask & (Facet.WEEKOFMONTH | Facet.DAYOFWEEK)) != 0 &&
                     (mask & ~(Facet.WEEKOFMONTH | Facet.DAYOFWEEK | Facet.MONTH)) == 0 ||
                 mask == Facet.DAYOFYEAR )
            {
                return;
            }
            string[] facets =
                schedule
                    .Rules
                    .ToList()
                    .ConvertAll((Rule r) => r.Facet.ToString())
                    .ToArray();
            throw new
                PaymentException(
                    String.Format(
                        "Invalid combination of facets: {0}",
                        String.Join(",", facets)
                    ),
                    Status.Code.INCONSISTENT_PARAMETERS
                );
        }

        private EPayWebService  myClient;
        private bool            myTestMode;
    }
}
