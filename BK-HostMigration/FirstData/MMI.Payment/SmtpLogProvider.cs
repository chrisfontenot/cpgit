﻿/*******************************************************************************
 *
 * Defines the class SmtpLogProvider.
 *
 * File:        PaymentGateway/Library/SmtpLogProvider.cs
 * Date:        Sat Nov  1 14:38:43 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Net;
using System.Net.Mail;

namespace MMI.Payment
{
      /// <summary>
      /// Delivers log messages via SMTP.
      /// </summary>
    public class SmtpLogProvider : ILogProvider
    {
        /// <summary>
        /// Constructs a <see cref="SmtpLogProvider"/>.
        /// </summary>
        /// <param name="from">Value of the From header.</param>
        /// <param name="to">Email address of the recipient.</param>
        SmtpLogProvider(string from, string to) 
            : this(from, to, null, null) 
            { }

        /// <summary>
        /// Constructs a <see cref="SmtpLogProvider"/>.
        /// </summary>
        /// <param name="from">Value of the From header.</param>
        /// <param name="to">Email address of the recipient.</param>
        /// <param name="username">The SMTP username.</param>
        /// <param name="password">The SMTP password.</param>
        public SmtpLogProvider( string from, string to, string username, 
                                string password )
            : this(from, to, username, password, null)
            { }

        /// <summary>
        /// Constructs a <see cref="SmtpLogProvider"/>.
        /// </summary>
        /// <param name="from">Value of the From header.</param>
        /// <param name="to">Email address of the recipient.</param>
        /// <param name="username">The SMTP username.</param>
        /// <param name="password">The SMTP password.</param>
        /// <param name="host">The host.</param>
        public SmtpLogProvider( string from, string to, string username, 
                                string password, string host )
            : this(from, to, username, password, host, -1 )
            { }

        /// <summary>
        /// Constructs a <see cref="SmtpLogProvider"/>.
        /// </summary>
        /// <param name="from">Value of the From header.</param>
        /// <param name="to">Email address of the recipient.</param>
        /// <param name="username">The SMTP username.</param>
        /// <param name="password">The SMTP password.</param>
        /// <param name="host">The SMTP host.</param>
        /// <param name="port">The SMTP port.</param>
        public SmtpLogProvider( string from, string to, string username, 
                                string password, string host, int port )
        {
            if (username != null && password == null)
                throw new PaymentException(
                              "Missing SMTP password",
                              Status.Code.CONFIGURATION_ERROR
                          );
            myFrom = from;
            myTo = to;
            myCredentials = username != null ?
                new Credentials(username, password) :
                null;
            myHost = host != null ? host : "localhost";
            myPort = port != -1 ? port : 25;
        }

        /// <summary>
        /// Processes a log entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void NewEntry(LogEntry entry)
        {
            try {
                SmtpClient client = new SmtpClient(myHost, myPort);
                if (myCredentials != null)
                    client.Credentials = myCredentials;
                client.Send(
                    myFrom, 
                    myTo, 
                    "Payment Gateway Error",
                    entry.Format()
                );
            } catch (Exception e) {
                throw new PaymentException(
                              "SMTP log listener error: " + e.Message,
                              Status.Code.LOG_ERROR,
                              e
                          );
            }
        }

        /// <summary>
        /// No-op.
        /// </summary>
        public void Dispose() { }

        /// <summary>
        /// Used to handle SMTP authentication.
        /// </summary>
        private class Credentials : ICredentialsByHost {
            public Credentials(string username, string password)
            {
                myCredentials = new NetworkCredential(username, password);
            }

            public NetworkCredential GetCredential(string host, int port, 
                string authType)
            {
                return myCredentials;
            }

            private NetworkCredential myCredentials;
        }

        private string       myFrom;
        private string       myTo;
        private Credentials  myCredentials;
        private string       myHost;
        private int          myPort;
    }
}
