﻿/*******************************************************************************
 *
 * Defines the class MMILogProvider.
 *
 * File:        PaymentGateway/Library/MMILogProvider.cs
 * Date:        Tue Jan 19 14:00:00 CST 2010
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Data;
using MMI.Components.Instrumentation;
using MMI.Components.ExceptionManagement;
using System.Reflection;
using System.Configuration;

namespace MMI.Payment
{
      /// <summary>
      /// Log listener that records log entries in the MMI system logger.
      /// </summary>
    public class MMILogProvider : ILogProvider
    {
        /// <summary>
        /// Constructs a <see cref="DatabaseLogProvider"/>.
        /// </summary>
        public MMILogProvider(IGatewayConfiguration config)
        {
        }

        /// <summary>
        /// Processes a log entry
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void NewEntry(LogEntry entry)
        {
            switch (entry.Level)
            {
                case LogLevel.FATAL_ERROR:
                case LogLevel.ERROR:
                    if (entry.Exception != null)
                    {
                        ExceptionHandler.LogException(entry.Exception);
                    }
                    else
                    {
                        Trace.WriteWarning(entry.Method, entry.Message);
                    }
                    break;
                case LogLevel.WARNING:
                    Trace.WriteWarning(entry.Method, entry.Message);
                    break;
                case LogLevel.DEBUG:
                    Trace.WriteDebug(entry.Method, entry.Message);
                    break;
                case LogLevel.INFO:
                case LogLevel.NONE:
                case LogLevel.VERBOSE:
                default:
                    Trace.WriteInformation(entry.Method, entry.Message);
                    break;
            }

        }

        /// <summary>
        /// Helper method to fetch a setting from a gateway configuration.
        /// </summary>
        /// <param name="config">The config.</param>
        /// <param name="key">The setting name.</param>
        /// <param name="throwOnError"><c>true</c> if an exception should
        /// be thrown when a setting is not found.</param>
        /// <returns>The value of the setting.</returns>
        private static string GetSetting(IGatewayConfiguration config,
            string key)
        {
            if (config.ContainsKey(key))
            {
                return config[key];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Closes the database connection.
        /// </summary>
        public void Dispose()
        {
        }

    }
}
