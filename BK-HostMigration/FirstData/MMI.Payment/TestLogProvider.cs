﻿/*******************************************************************************
 *
 * Defines the class Mmi.Payment.Test.LogProvider.
 *
 * File:        PaymentGateway/Library/TestLogProviders.cs
 * Date:        Sat Nov 15 11:48:35 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;

namespace MMI.Payment
{
  namespace Test {

      /// <summary>
      /// Used by the unit tests.
      /// </summary>
    public class LogProvider : ILogProvider
    {
        public LogProvider() : this("default") { }
        public LogProvider(string name)
        {
            if (myEntries.ContainsKey(name))
                throw new InvalidOperationException(
                              String.Format(
                                  "A CustomLogProvider with name '{0}' " +
                                      "already exists",
                                  name
                              )
                          );
            myName = name;
            myEntries[name] = new List<LogEntry>();
        }

        public static List<LogEntry> GetEntries(string name)
        {
            return myEntries[name];
        }

        public void NewEntry(LogEntry entry)
        {
            myEntries[myName].Add(entry);
        }

        public void Dispose() 
        {
            myEntries.Remove(myName);
        }

        private static Dictionary<string, List<LogEntry>> myEntries = 
            new Dictionary<string, List<LogEntry>>();
        private string  myName;
    }

    /// <summary>
    /// Used by the unit tests.
    /// </summary>
    public class BrokenLogProvider : ILogProvider
    {
        public void NewEntry(LogEntry entry)
        {
            throw new InvalidOperationException("Log provider is broken");
        }

        public void Dispose() { }
    }

  }
}
