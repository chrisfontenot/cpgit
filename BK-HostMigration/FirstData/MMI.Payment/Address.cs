﻿/*******************************************************************************
 *
 * Defines the class Address.
 *
 * File:        PaymentGateway/Library/Address.cs
 * Date:        Sat Nov  1 11:32:35 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
namespace MMI.Payment
{
      /// <summary>
      /// Represents an Address
      /// </summary>
      [Serializable]
      public class Address {
          /// <summary>
          /// Default constructor, needed for XML serialization.
          /// </summary>
        public Address() { }

        /// <summary>
        /// Constructs an <see cref="Address"/>.
        /// </summary>
        /// <param name="street1">The street address.</param>
        /// <param name="street2">The second line, if any, of the
        /// street address.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip.</param>
        public Address( string street1, string street2, string city, 
                        string state, string zip )
        {
            this.Street1 = street1;
            this.Street2 = street2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">The address to be copied.</param>
        public Address(Address other)
            : this(other.Street1, other.Street2, other.City, other.State,
                  other.Zip)
            { }

        /// <summary>
        /// Gets or sets the street address.
        /// </summary>
        /// <value>The street1.</value>
        public string Street1 
        { 
            get { return myStreet1; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "street address 1", 1, 50);
                }
                myStreet1 = value; 
            }
        }

        /// <summary>
        /// Gets or sets the second line, if any, of the
        /// street address.
        /// </summary>
        /// <value>The street2.</value>
        public string Street2 
        { 
            get { return myStreet2; }
            set 
            { 
                if (value != null) {
                    StringValidator.Validate(
                        value, "street address 2", 0, 50);
                }
                myStreet2 = value; 
            }
        }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City 
        { 
            get { return myCity; }
            set
            { 
                if (value != null)
                    StringValidator.Validate(value, "city", 1, 50);
                myCity = value; 
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State 
        { 
            get { return myState; }

            set
            {
                switch (value)
                {
                    case "AL": case "AK": case "AS": case "AZ": case "AR": 
                    case "CA": case "CO": case "CT": case "DE": case "DC": 
                    case "FM": case "FL": case "GA": case "GU": case "HI": 
                    case "ID": case "IL": case "IN": case "IA": case "KS": 
                    case "KY": case "LA": case "ME": case "MH": case "MD": 
                    case "MA": case "MI": case "MN": case "MS": case "MO": 
                    case "MT": case "NE": case "NV": case "NH": case "NJ": 
                    case "NM": case "NY": case "NC": case "ND": case "MP": 
                    case "OH": case "OK": case "OR": case "PW": case "PA": 
                    case "PR": case "RI": case "SC": case "SD": case "TN": 
                    case "TX": case "UT": case "VT": case "VI": case "VA": 
                    case "WA": case "WV": case "WI": case "WY": case "AA": 
                    case "AE": case "AP": 
                        break;
                    case null:
                        break;
                    default:
                        throw new PaymentException(
                                      "Invalid US State abbreviation: " + value,
                                      Status.Code.INVALID_PARAMETER
                                  );
                }
                myState = value;
            }
        }

        /// <summary>
        /// Gets or sets the ZIP code.
        /// </summary>
        /// <value>The zip.</value>
        public string Zip 
        { 
            get { return myZip; } 
            set 
            {
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "5-digit US ZIP code", 5, 5);
                }
                myZip = value;
            }
        }

        public static bool operator==(Address lhs, Address rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(Address lhs, Address rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            Address address = (Address) other;
                return Street1 == address.Street1 &&
                       Street2 == address.Street2 &&
                       City == address.City &&
                       State == address.State &&
                       Zip == address.Zip;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (Street1 + Street2 + City + State + Zip).GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Street1: {0}; Street2: {1}; City: {2}; State: {3}; " +
                           "Zip: {4}]",
                       Street1, Street2, City, State, Zip
                   );
        }

        private string myStreet1;
        private string myStreet2;
        private string myCity;
        private string myState;
        private string myZip;
    }
}
