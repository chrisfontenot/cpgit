﻿/*******************************************************************************
 *
 * Defines the class Template.
 *
 * File:        PaymentGateway/Library/Template.cs
 * Date:        TIMESTAMP
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MMI.Payment.Test
{
    [TestClass]
    public class Template
    {
        public Template()
        {
            System.Console.WriteLine("Template()");
        }

        public TestContext TestContext
        {
            get { return myContext; }
            set { myContext = value; }
        }

        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext) { }

        [ClassCleanup()]
        public static void ClassCleanup() { }

        [TestInitialize()]
        public void TestInitialize() { }

        [TestCleanup()]
        public void TestCleanup() { }

        [TestMethod]
        public void Execute()
        {

        }

        private TestContext myContext;
    }
}
