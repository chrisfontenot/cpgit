﻿/*******************************************************************************
 *
 * Defines the class Gateway.
 *
 * File:        PaymentGateway/Library/Gateway.cs
 * Date:        Fri Oct 31 19:32:17 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************
 * Amendment History
 *
 * Who           Date               Changes
 *-----------------------------------------------------------------------------
 * MDH         10/04/2009           StorePaymentInfoImpl Amended
 *                                  Account_number and expiration_date stored as
 *                                  null for complicance
 ******************************************************************************/
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;

      /// <summary>
      /// Encapsulates the payment gateway; this is the central component
      /// of the framework.
      /// </summary>
    public class Gateway : DataClient, IDisposable
    {
        /// <summary>
        /// Constructs a <see cref="Gateway"/> from a gateway configuration.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        public Gateway(IGatewayConfiguration config)
        {
            myConfiguration = config;
            myConnection = new Connection(config);
        }

        /// <summary>
        /// Returns the underlying connection to the database and log.
        /// </summary>
        /// <value>The connection.</value>
        public Connection Connection
        {
            get { return myConnection; }
        }

        /// <summary>
        /// Proccesses a transaction.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">Name of the provider account.</param>
        /// <param name="type">The transaction type.</param>
        /// <param name="info">An instance of <see cref="PaymentInfo"/>,
        /// representing the customer, the transaction amount, and the
        /// payment method.</param>
        /// <param name="schedule">An instance of <see cref="Schedule"/>,
        /// representing the payment schedule, if any.</param>
        /// <param name="referenceCode">The provider reference code of the 
        /// prior transaction to which the current transaction relates, if 
        /// any.</param>
        /// <param name="memo">A string that will be recorded with the
        /// current transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        public GatewayStatus ProccessTransaction(string providerName,
            string accountName, TransactionType type, PaymentInfo info,
            Schedule schedule, string referenceCode, string memo)
        {
            GatewayTransaction trans = null;
            GatewayStatus result;
            try {
                Provider provider = NewProvider(providerName);
                ProviderAccount account = accountName != null ?
                    LoadAccount(provider.Name, accountName) :
                    LoadAccount(provider.Name, "default");
                //if (referenceCode != null) {
                //    try {
                //        transactionRef = 
                //            LoadGatewayTransaction(providerName, referenceCode);
                //    } catch (PaymentException e) {
                //        Log.Stream warning = 
                //            myConnection.Log.GetStream(LogLevel.WARNING);
                //        if (warning)
                //            warning.Write(e.Message);
                //    }
                //}
                trans =
                    NewGatewayTransaction(
                        provider.Name, account, type, info, schedule, 
                        null, memo
                    );
                switch (type)
                {
                    case TransactionType.SALE:
                        provider.ProcessSale(account, info, trans);
                        break;
                    case TransactionType.AUTHORIZATION:
                        provider.ProcessAuthorization(account, info, trans);
                        break;
                    case TransactionType.CREDIT:
                        provider.ProcessCredit(account, info, trans);
                        break;
                    case TransactionType.CAPTURE:
                        provider.ProcessCapture(account, trans, referenceCode);
                        break;
                    case TransactionType.RETURN:
                        provider.ProcessReturn(account, info, trans,
                            referenceCode);
                        break;
                    case TransactionType.VOID:
                        provider.ProcessVoid(account, trans, referenceCode);
                        break;
                    case TransactionType.QUERY:
                        provider.ProcessQuery(account, trans, referenceCode);
                        break;
                    case TransactionType.CREATE_INFO:
                        provider.ProcessCreateInfo(account, info, trans);
                        break;
                    case TransactionType.QUERY_INFO:
                        provider.ProcessQueryInfo(account, trans, 
                            referenceCode);
                        break;
                    case TransactionType.UPDATE_INFO:
                        provider.ProcessUpdateInfo(account, info, trans,
                            referenceCode);
                        break;
                    case TransactionType.DELETE_INFO:
                        provider.ProcessDeleteInfo(account, trans,
                            referenceCode);
                        break;
                    case TransactionType.CREATE_SCHEDULE:
                        provider.ProcessCreateSchedule(account, info, schedule, 
                            trans);
                        break;
                    case TransactionType.QUERY_SCHEDULE:
                        provider.ProcessQuerySchedule(account, trans,
                            referenceCode);
                        break;
                    case TransactionType.UPDATE_SCHEDULE:
                        provider.ProcessUpdateSchedule(account, info, schedule,
                            trans, referenceCode);
                        break;
                    case TransactionType.DELETE_SCHEDULE:
                        provider.ProcessDeleteSchedule(account, trans,
                            referenceCode);
                        break;
                    default:
                        throw new PaymentException(
                                      "Invalid transaction type: " + type,
                                      Status.Code.INVALID_TRANSACTION_TYPE
                                  );
                }
                result = 
                    new GatewayStatus(
                            Status.Code.SUCCESS,
                            trans.ProviderTransactionCode,
                            trans.ReferenceCode,
                            trans.OutputPaymentInfo,
                            trans.OutputSchedule
                        );
                trans.Complete(result);
            } catch (PaymentException e) {
                LogException(e);
                result =
                    new GatewayStatus(
                            e.Code,
                            trans != null ? trans.Id.ToString() : "-1",
                            e.Message
                        );
                try {
                    trans.Complete(result);
                } catch (Exception f) {
                    LogException(f);
                }
            } catch (Exception e) {
                LogException(e);
                result =
                    new GatewayStatus(
                            Status.Code.INTERNAL_ERROR,
                            trans != null ? trans.Id.ToString() : "-1",
                            e.Message
                        );
                try {
                    trans.Complete(result);
                } catch (Exception f) {
                    LogException(f);
                }
            }

            return result;
        }

        /// <summary>
        /// Frees resources associated with this gateway.
        /// </summary>
        public void Dispose()
        {
            try {
                myConnection.Dispose();
            } catch (Exception) { }
        }

        /// <summary>
        /// Stores a provider account in the database.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">Name of the account.</param>
        //public void StoreAccount(ProviderAccount account,
        //    string providerName, string accountName)
        //{
        //    Repeater repeater = new Repeater(myConnection.Log);
        //    repeater.Repeat(
        //        () =>
        //            { StoreAccountImpl(account, providerName, accountName);
        //              return null; },
        //        String.Format("Storing account '{0}'", accountName),
        //        Status.Code.DATABASE_ERROR
        //    );
        //}

        /// <summary>
        /// Implements StoreAccount.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">Name of the account.</param>
        //private void StoreAccountImpl(ProviderAccount account,
        //    string providerName, string accountName)
        //{
        //    if (account.Id != 0)
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Account '{0}' already has a database " +
        //                              "record id",
        //                          account.Username
        //                      ),
        //                      Status.Code.INTERNAL_ERROR
        //                  );
        //    SqlCommand command = null;
        //    try {
        //        command = 
        //            new SqlCommand(
        //                    "CreateProviderAccount", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(
        //            new SqlParameter("@date", DateTime.Now.ToFileTimeUtc())
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@provider", providerName)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@account_name", accountName)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@username", account.Username)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@password", account.Password)
        //        );
        //        AddParameter(command, "@accountid1", account.AccountId1,
        //            SqlDbType.VarChar);
        //        AddParameter(command, "@accountid2", account.AccountId2,
        //            SqlDbType.VarChar);
        //        account.Id = Decimal.ToInt64((decimal) command.ExecuteScalar());
        //    } catch (Exception e) {
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Failed storing account '{0}' for " +
        //                              "provider '{1}': {2}",
        //                          accountName, providerName, e.Message
        //                      ),
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    } finally {
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        /// <summary>
        /// Sets the default account for a provider.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">Name of the account.</param>
        //public void SetDefaultAccount(string providerName, string accountName)
        //{
        //    SqlCommand command = null;
        //    try {
        //        command =
        //            new SqlCommand(
        //                    "CountProviderAccounts", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(
        //            new SqlParameter("@provider", providerName)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@account_name", accountName)
        //        );
        //        if (((int) command.ExecuteScalar()) == 0) {
        //            Mmi.Payment.Status.Code code =
        //                Mmi.Payment.Status.Code.INVALID_PROVIDER_ACCOUNT;
        //            throw new PaymentException(
        //                          String.Format(
        //                              "Invalid account '{0}' for provider '{1}'",
        //                              accountName, providerName
        //                          ),
        //                          code
        //                      );
        //        }
        //        Repeater repeat = new Repeater(myConnection.Log);
        //        repeat.Repeat(
        //            () => SetDefaultAccountImpl(providerName, accountName),
        //            String.Format(
        //                "Setting '{0}' as default account for provider '{1}'",
        //                 accountName, providerName
        //            ),
        //            Status.Code.DATABASE_ERROR
        //        );
        //    } catch (PaymentException e) {
        //        throw e;
        //    } catch (Exception e) {
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Failed setting '{0}' as default account " +
        //                              "for provider '{1}': {2}",
        //                          accountName, providerName, e.Message
        //                      ),
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    } finally {
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        //private object SetDefaultAccountImpl(string providerName,
        //    string accountName)
        //{
        //    SqlTransaction trans = null;
        //    SqlCommand command = null;
        //    try {
        //        trans = myConnection.SqlConnection.BeginTransaction();
        //        command =
        //            new SqlCommand(
        //                    "SetDefaultAccount", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Transaction = trans;
        //        command.Parameters.Add(
        //            new SqlParameter(
        //                    "@date", DateTime.Now.ToFileTimeUtc()
        //                )
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@provider", providerName)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@account_name", accountName)
        //        );
        //        command.ExecuteNonQuery();
        //        trans.Commit();
        //        return null; // return value requited by Repeater.Repeat().
        //    } catch (Exception e) {
        //        if (trans != null)
        //            trans.Rollback();
        //        throw e;
        //    } finally {
        //        if (trans != null)
        //            trans.Dispose();
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        /// <summary>
        /// Loads the default account for the given provider from
        /// the database. If no such account exists, throws an
        /// exception.</summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The account.</returns>
        //public ProviderAccount LoadAccount(string providerName)
        //{
        //    Repeater repeater = new Repeater(myConnection.Log);
        //    return (ProviderAccount)
        //        repeater.Repeat(
        //            () => LoadAccountImpl(providerName),
        //            String.Format(
        //                "Loading default account for provider '{0}'",
        //                providerName
        //            ),
        //            Status.Code.INVALID_PROVIDER_ACCOUNT
        //        );
        //}

        /// <summary>
        /// Implements LoadAccount
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The account.</returns>
        //private ProviderAccount LoadAccountImpl(string providerName)
        //{
        //    SqlCommand command = null;
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        command =
        //            new SqlCommand(
        //                    "LoadDefaultAccount",
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(
        //            new SqlParameter("@provider", providerName)
        //        );
        //        reader = command.ExecuteReader();
        //        if (!reader.Read())
        //            throw new PaymentException(
        //                          String.Format(
        //                              "No default account available for " +
        //                                  "provider '{0}'",
        //                              providerName
        //                          ),
        //                          Status.Code.INVALID_PROVIDER_ACCOUNT
        //                      );
        //        ProviderAccount account =
        //            new ProviderAccount(
        //                   ReadVarChar(reader, 1),
        //                   ReadVarChar(reader, 2),
        //                   ReadVarChar(reader, 3),
        //                   ReadVarChar(reader, 4)
        //                );
        //        account.Id = ReadBigInt(reader, 0);
        //        return account;
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Dispose();
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        /// <summary>
        /// Loads a provider account from the database.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">The account name.</param>
        /// <returns>The account.</returns>
        public ProviderAccount LoadAccount(string providerName,
            string accountName)
        {
            //Repeater repeater = new Repeater(myConnection.Log);
            //return (ProviderAccount)
            //    repeater.Repeat(
            //        () => LoadAccountImpl(providerName, accountName),
            //        String.Format(
            //            "Loading account '{0}' for provider '{1}'",
            //            accountName, providerName
            //        ),
            //        Status.Code.INVALID_PROVIDER_ACCOUNT
            //    );

            string username = "";
            string password = "";
            string accountid1 = "";
            string accountid2 = "";
            string key = "";
            string keyField = "";

            key = string.Concat("provider.", providerName, ".", accountName);

            keyField = string.Concat(key, ".username");
            if (myConfiguration.ContainsKey(keyField))
                username = myConfiguration[keyField];

            keyField = string.Concat(key, ".password");
            if (myConfiguration.ContainsKey(keyField))
                password = myConfiguration[keyField];

            keyField = string.Concat(key, ".accountid1");
            if (myConfiguration.ContainsKey(keyField))
                accountid1 = myConfiguration[keyField];

            keyField = string.Concat(key, ".accountid2");
            if (myConfiguration.ContainsKey(keyField))
                accountid2 = myConfiguration[keyField];

            ProviderAccount account =
                new ProviderAccount(
                       username,
                       password,
                       accountid1,
                       accountid2
                    );
            return account;
        }

        /// <summary>
        /// Implements LoadAccount.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <param name="accountName">The account name.</param>
        /// <returns>The account.</returns>
        //private ProviderAccount LoadAccountImpl(string providerName,
        //    string accountName)
        //{
        //    SqlCommand command = null;
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        command =
        //            new SqlCommand(
        //                    "LoadProvidertAccount",
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(
        //            new SqlParameter("@provider", providerName)
        //        );
        //        command.Parameters.Add(
        //            new SqlParameter("@account_name", accountName)
        //        );
        //        reader = command.ExecuteReader();
        //        if (!reader.Read())
        //            throw new PaymentException(
        //                          String.Format(
        //                              "Invalid account '{0}' for provider " +
        //                                  "'{1}'",
        //                              accountName, providerName
        //                          ),
        //                          Status.Code.INVALID_PROVIDER_ACCOUNT
        //                      );
        //        ProviderAccount account =
        //            new ProviderAccount(
        //                   ReadVarChar(reader, 1),
        //                   ReadVarChar(reader, 2),
        //                   ReadVarChar(reader, 3),
        //                   ReadVarChar(reader, 4)
        //                );
        //        account.Id = ReadBigInt(reader, 0);
        //        return account;
        //    }
        //    catch (PaymentException e)
        //    {
        //        throw e;
        //    }
        //    catch (Exception e)
        //    {
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Failed loading account '{0}' for provider " +
        //                              "'{1}': {2}",
        //                          accountName, providerName, e.Message
        //                      ),
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Dispose();
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        /// <summary>
        /// Deletes a provider account from the database.
        /// </summary>
        /// <param name="account">The account.</param>
        //public void DeleteAccount(ProviderAccount account)
        //{
        //    if (account.Id == 0)
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Account '{0}' has no database " +
        //                              "record id",
        //                          account.Username
        //                      ),
        //                      Status.Code.INTERNAL_ERROR
        //                  );
        //    SqlCommand command = null;
        //    try {
        //        command =
        //            new SqlCommand(
        //                    "DeleteProvidertAccount", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(
        //            new SqlParameter("@id", account.Id)
        //        );
        //        command.ExecuteNonQuery();
        //        account.Id = 0;
        //    } catch (Exception e) {
        //        throw new PaymentException(
        //                      String.Format(
        //                          "Failed deleting account '{0}': {1}",
        //                          account.Username, e.Message
        //                      ),
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    } finally {
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        /// <summary>
        /// Constructs a <see cref="Provider"/>, via reflection.
        /// </summary>
        /// <param name="providerName">Name of the provider.</param>
        /// <returns>The newly constructed provider.</returns>
        public Provider NewProvider(string providerName)
        {
            try {

                // Look for configuration property named
                // "provider.<provider-name>"
                string key = "provider." + providerName + ".class";
                if (!myConfiguration.ContainsKey(key))
                    throw new PaymentException(
                                  "Invalid provider: " + providerName,
                                  Status.Code.INVALID_PROVIDER
                              );
                string className = myConfiguration[key];

                // Create an instance of className
                Assembly asm = Assembly.Load("MMI.Payment");
                Type classType = null;
                try {
                    classType = asm.GetType(className, true);
                } catch (Exception e) {
                    throw new PaymentException(
                                  String.Format(
                                      "Invalid class name '{0}' implementing " +
                                          "provider '{1}': {2}",
                                      className, providerName, e.Message
                                  ),
                                  Status.Code.INVALID_PROVIDER
                              );
                }
                if (!classType.IsSubclassOf(typeof(Provider)))
                    throw new PaymentException(
                                  String.Format(
                                      "Class '{0}' implementing provider " +
                                          "'{1}' does not derived from {2}",
                                      className,
                                      providerName,
                                      typeof(Provider).ToString()
                                  ),
                                  Status.Code.INVALID_PROVIDER
                              );
                Type[] argList =
                    new Type[2] { typeof(IGatewayConfiguration), typeof(Log) };
                ConstructorInfo ctor = classType.GetConstructor(argList);
                if (ctor == null)
                    throw new PaymentException(
                                  String.Format(
                                      "No suitable constructor for class " +
                                          "'{0}' implementing provider '{1}'",
                                      className, providerName
                                  ),
                                  Status.Code.INVALID_PROVIDER
                              );
                IGatewayConfiguration config = myConfiguration;
                Log log = myConnection.Log;
                return (Provider) ctor.Invoke(new object[] { config, log });

            } catch (PaymentException e) {
                throw e;
            } catch (Exception e) {
                throw new PaymentException(
                              String.Format(
                                  "Failed loading provider {0}: {1}",
                                  providerName,
                                  e.Message
                              ),
                              Status.Code.CONFIGURATION_ERROR
                          );
            }
        }

        /// <summary>
        /// Constructs a <see cref="GatewayTransaction"/> and stores it in the
        /// database. Its initial status is PENDING.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="account">A provider account.</param>
        /// <param name="type">The transaction type.</param>
        /// <param name="paymentInfo">The payment information.</param>
        /// <param name="schedule">The schedule.</param>
        /// <param name="transactionRef">The referenced transaction.</param>
        /// <param name="memo">The memo.</param>
        /// <returns></returns>
        GatewayTransaction NewGatewayTransaction( 
            string provider, ProviderAccount account, TransactionType type, 
            PaymentInfo paymentInfo, Schedule schedule, 
            GatewayTransaction transactionRef, string memo)
        {
            //SqlCommand command = null;
            //SqlTransaction trans = null;

            try {
                DateTime timestamp = DateTime.Now;
                Status.Code status = Status.Code.PENDING;

                //// Store payment info
                //trans = myConnection.SqlConnection.BeginTransaction();

                //// Build command to insert transaction record
                //command =
                //    new SqlCommand(
                //            "CreateGatewayTransaction", 
                //            myConnection.SqlConnection
                //        );
                //command.CommandType = CommandType.StoredProcedure;
                //command.Transaction = trans;
                //command.Parameters.Add(
                //    new SqlParameter("@date", timestamp.ToFileTimeUtc())
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@session", Session.Id)
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@provider", provider)
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@account", account.Id)
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@type", type)
                //);
                //AddParameter(
                //    command, 
                //    "@ref",
                //    transactionRef != null ? 
                //        transactionRef.Id as long? :
                //        null,
                //    SqlDbType.BigInt
                //);
                //AddParameter(command, "@memo", memo, SqlDbType.VarChar);
                //command.Parameters.Add(
                //    new SqlParameter("@status", status)
                //);

                //// Execute command
                //long id = Decimal.ToInt64((decimal) command.ExecuteScalar());
                //trans.Commit();

                return new 
                    PrivateGatewayTransaction(
                        this, myConnection, 0, timestamp, provider, type,
                        paymentInfo, null, schedule, null, transactionRef, 
                        memo, status, null
                    );

            } catch (PaymentException e) {
                //if (trans != null)
                //    trans.Rollback();
                throw e;
            } catch (Exception e) {
                //if (trans != null)
                //    trans.Rollback();
                throw new PaymentException(
                              "Failed creating GatewayTransaction record: " +
                                  e.Message,
                              MMI.Payment.Status.Code.DATABASE_ERROR,
                              e
                          );
            } finally {
                //if (trans != null)
                //    trans.Dispose();
                //if (command != null)
                //    command.Dispose();
            }
        }

        /// <summary>
        /// Returns a <see cref="GatewayTransaction"/> loaded from the database.
        /// </summary>
        /// <param name="id">The record ID.</param>
        //public GatewayTransaction LoadGatewayTransaction(long id)
        //{
        //    SqlCommand command = null;
        //    SqlDataReader reader = null;
        //    try {

        //        // Fetch GatewayTransaction record
        //        command =
        //            new SqlCommand(
        //                    "LoadGatewayTransaction", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@id", id));
        //        reader = command.ExecuteReader();
        //        if (!reader.Read()) {
        //            Mmi.Payment.Status.Code code =
        //                Mmi.Payment.Status.Code.INVALID_TRANSACTION_REFERENCE;
        //            throw new PaymentException(
        //                          "Missing GatewayTransaction record: " + id,
        //                          code
        //                      );
        //        }
        //        return LoadGatewayTransaction(reader);

        //    } catch (Exception e) {
        //        if (reader != null)
        //            reader.Dispose();
        //        if (command != null)
        //            command.Dispose();
        //        if (e is PaymentException)
        //            throw e;
        //        throw new PaymentException(
        //                      "Failed loading gateway transaction: " + 
        //                          e.Message,
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    }
        //}

        /// <summary>
        /// Returns a <see cref="GatewayTransaction"/> loaded from the database.
        /// </summary>
        /// <param name="provider">The provider name.</param>
        /// <param name="referenceCode">The provider reference code.</param>
        //public GatewayTransaction LoadGatewayTransaction(
        //    string provider, string referenceCode)
        //{
        //    if (provider == null || referenceCode == null)
        //        throw new
        //            PaymentException(
        //                "Failed loading GatewayTransaction: missing " +
        //                    "provider or reference code",
        //                Mmi.Payment.Status.Code.MISSING_PARAMETER
        //            );

        //    SqlCommand command = null;
        //    SqlDataReader reader = null;
        //    try {

        //        // Fetch GatewayTransaction record
        //        command =
        //            new SqlCommand(
        //                    "LoadGatewayTransactionFromRef", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@provider", provider));
        //        command.Parameters.Add(new SqlParameter("@ref", referenceCode));
        //        reader = command.ExecuteReader();
        //        if (!reader.Read()) {
        //            throw new PaymentException(
        //                          String.Format(
        //                              "Missing GatewayTransaction for " +
        //                                  "provider {0}' and reference code " +
        //                                  "'{1}'",
        //                                provider, referenceCode
        //                           ),
        //                           Status.Code.INVALID_TRANSACTION_REFERENCE
        //                      );
        //        }
        //        return LoadGatewayTransaction(reader);

        //    } catch (Exception e) {
        //        if (reader != null)
        //            reader.Dispose();
        //        if (command != null)
        //            command.Dispose();
        //        if (e is PaymentException)
        //            throw e;
        //        throw new PaymentException(
        //                      "Failed loading gateway transaction: " + 
        //                          e.Message,
        //                      Mmi.Payment.Status.Code.DATABASE_ERROR,
        //                      e
        //                  );
        //    }
        //}

        /// <summary>
        /// Returns a <see cref="GatewayTransaction"/> loaded from the database.
        /// </summary>
        /// <param name="reader">A reader containing a GatewayTransaction 
        /// record.</param>
        //private GatewayTransaction LoadGatewayTransaction(SqlDataReader reader)
        //{
        //    int column = 0;
        //    long id = ReadBigInt(reader, column++);
        //    DateTime timestamp = 
        //        DateTime.FromFileTimeUtc(ReadBigInt(reader, column++));
        //    string provider = ReadVarChar(reader, column++);
        //    TransactionType type = (TransactionType) 
        //        Enum.ToObject(typeof(TransactionType), reader[column++]);
        //    long refId = ReadBigInt(reader, column++);
        //    long infoId = ReadBigInt(reader, column++);
        //    string memo = ReadVarChar(reader, column++);
        //    Status.Code status = (Status.Code) 
        //        Enum.ToObject(typeof(Status.Code), reader[column++]);
        //    string referenceCode = ReadVarChar(reader, column++);
        //    reader.Close();

        //    // Process foreign keys
        //    GatewayTransaction transRef = refId != 0 ?
        //        LoadGatewayTransaction(refId) :
        //        null;

        //    return
        //        new PrivateGatewayTransaction(
        //                this, myConnection, id, timestamp, provider, type, null,
        //                null, null, null, transRef, memo, status, referenceCode
        //            );
        //}

        /// <summary>
        /// Logs the given exception.
        /// </summary>
        /// <param name="e">The exception.</param>
        private void LogException(Exception e)
        {
            Log.Stream stream = 
                myConnection.Log.GetStream(LogLevel.FATAL_ERROR);
            if ( stream != null &&
                 ( !(e is PaymentException) ||
                   !(e as PaymentException).IsLogged() ) )
            {
                stream.Write(e.TargetSite, e.Message, e);
            }
        }

        private class PrivateGatewayTransaction : GatewayTransaction
        {
            public PrivateGatewayTransaction(
                  Gateway gateway, Connection conn, long id, DateTime timestamp,
                  string provider, TransactionType type, 
                  PaymentInfo paymentInfo, PaymentInfo outputPaymentInfo, 
                  Schedule schedule, Schedule outputSchedule, 
                  GatewayTransaction transactionRef, string memo, 
                  Status.Code status, string referenceCode )
                : base( gateway, conn, id, timestamp, provider, type, 
                        paymentInfo, outputPaymentInfo, schedule, 
                        outputSchedule, transactionRef, memo, status, 
                        referenceCode )
                { }
        }

        private IGatewayConfiguration  myConfiguration;
        private Connection             myConnection;
    }
}
