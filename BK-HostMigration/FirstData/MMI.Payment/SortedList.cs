﻿/*******************************************************************************
 *
 * Defines the class SortedList.
 *
 * File:        PaymentGateway/Library/SortedList.cs
 * Date:        Tue Nov 11 14:25:45 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MMI.Payment
{
      /// <summary>
      /// Represents a sorted list of elements. This is essentially
      /// a serializable version of System.Collections.Generic.SortedList.
      /// </summary>
      /// <typeparam name="T"></typeparam>
    public class SortedList<T> 
        : IEnumerable, ICollection, ICollection<T>, 
          IList<T> where T : IComparable
    {
        public SortedList() 
        {
            myList = new List<T>();
        }

        public SortedList(IEnumerable<T> list) 
            : this()
        {
            foreach (T t in list)
                Add(t);
        }

        public SortedList(int capacity)
        {
            myList = new List<T>(capacity);
        }

        public void Add(T t)
        {
            int index = 0;
            int cmp = -1;
            while ( index < Count && 
                    (cmp = t.CompareTo(myList[index])) > 0 )
            {
                ++index;
            }
            if (index < Count && cmp == 0)
                throw new ArgumentException(
                              "Attemp to insert a duplicate item " +
                                  "into a SortedList: " + t.ToString()
                          );
            myList.Insert(index, t);
        }

        public void Clear() { myList.Clear(); }

        public bool Contains(T t) { return myList.Contains(t); }

        public void CopyTo(T[] array, int index)
        {
            myList.CopyTo(array, index);
        }

        public void CopyTo(Array array, int index)
        {
            ((ICollection) myList).CopyTo(array, index);
        }

        public IEnumerator<T> GetEnumerator() 
        { 
            return myList.GetEnumerator(); 
        }

        IEnumerator IEnumerable.GetEnumerator() 
        { 
            return myList.GetEnumerator(); 
        }

        public int IndexOf(T t) { return myList.IndexOf(t); }

        public void Insert(int index, T t)
        {
            throw new NotSupportedException();
        }

        public bool Remove(T t) { return myList.Remove(t); }

        public void RemoveAt(int index) { myList.RemoveAt(index); }

        public int Count
        {
            get { return myList.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public T this[int index]
        {
            get { return myList[index]; }
            set { Insert(index, value); }
        }

        public bool IsSynchronized
        {
            get { return ((ICollection) myList).IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return ((ICollection) myList).SyncRoot; }
        }

        public override int GetHashCode()
        {
            return myList.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append('[');
            for (int z = 0, n = Count; z < n; ++z) {
                sb.Append(myList[z].ToString());
                if (z < n - 1)
                    sb.Append(';');
            }
            sb.Append(']');
            return sb.ToString();
        }

        private List<T> myList;
    }
}
