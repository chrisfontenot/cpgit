﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MMI;
using MMI.Payment;
using MMI.Payment.FirstData;
using MMI.Payment.Properties;
using MMI.Payment.Test;

namespace TestingConsoleApp
{
    class Program
    {
        private class cardInfo
        {
            public string AccountNumber { get; private set; }
            public Int32 ExpirationMonth { get { return 10; } }
            public Int32 ExpirationYear { get { return 2017; } }
            public string VerificationCode
            {
                get
                {
                    if (AccountNumber.StartsWith("3")) // AMEX cards start with "3" in our test.
                    {
                        return "1055";
                    }
                    return "055";
                }
            }

            public cardInfo(string accountNumber)
            {
                this.AccountNumber = accountNumber;
            }
        }

        static void Main(string[] args)
        {
            var cardList = new System.Collections.Generic.List<cardInfo>();
            cardList.AddRange(new cardInfo[] {
                new cardInfo("341111597241002"),
                new cardInfo("371449635392376"),
                new cardInfo("4005551155111114"),
                new cardInfo("4012000033330026"),
                new cardInfo("5111370000000002"),
                new cardInfo("5191919191919199"),
                new cardInfo("372842590511015"),
                new cardInfo("342020202020207"),
                new cardInfo("372073916281006"),
                new cardInfo("4212121212121214"),
                new cardInfo("5405558888888887"),
                new cardInfo("5419841234567890"),
                new cardInfo("5439750001500321"),
                new cardInfo("5499740000000008"),
                new cardInfo("6011181818181879")
            });

            foreach (var card in cardList)
            {
                var DebitCardPaymentInfo = new DebitCardPayment();
                DebitCardPaymentInfo.AccountNumber = card.AccountNumber;
                DebitCardPaymentInfo.ExpirationMonth = card.ExpirationMonth;
                DebitCardPaymentInfo.ExpirationYear = card.ExpirationYear;
                DebitCardPaymentInfo.VerificationCode = card.VerificationCode;

                DebitCardPaymentInfo.NameFirst = "Sam";
                DebitCardPaymentInfo.NameLast = "Jones";
                DebitCardPaymentInfo.StreetNumber = "100 Home Street";
                DebitCardPaymentInfo.ZipCode = "01103";

                var Charge = new ChargeInfo();
                Charge.TotalAmount = 1;

                // Do the operation for $1.00
                var response = Process("first_data", "debit", DebitCardPaymentInfo, Charge);

                // Log the status.
                Console.WriteLine();
                Console.Write("Card Number = ");
                Console.WriteLine(card.AccountNumber);

                Console.Write("Response status = ");
                Console.WriteLine(response.StatusCode.ToString());
                if (response.StatusCode == Status.Code.SUCCESS)
                {
                    Console.Write("Reference Code = ");
                    Console.WriteLine(((Success)response).ReferenceCode);
                    Console.Write("Transaction ID = ");
                    Console.WriteLine(((Success)response).TransactionID);
                }
                else
                {
                    Console.WriteLine(((Fail)response).Message);
                }
            }

            Console.WriteLine("Press return to exit");
            Console.ReadLine();
        }

        partial class DebitCardPayment
        {
            public string AccountNumber { get; set; }
            public Int32 ExpirationMonth { get; set; }
            public Int32 ExpirationYear { get; set; }
            public string VerificationCode { get; set; }
        }

        partial class DebitCardPayment
        {
            public string NameFirst { get; set; }
            public string NameLast { get; set; }
            public string StreetNumber { get; set; }
            public string ZipCode { get; set; }
        }

        class ChargeInfo
        {
            public class AccountInfo
            {
                public string Number { get; set; }
            }
            public AccountInfo Account = new AccountInfo();
            public decimal TotalAmount { get; set; }
        }

        public interface IResponse
        {
            MMI.Payment.Status.Code StatusCode { get; }
        }

        public class Fail : IResponse
        {
            public MMI.Payment.Status.Code StatusCode
            {
                get
                {
                    return MMI.Payment.Status.Code.PROVIDER_ERROR;
                }
            }

            public string Message { get; set; }
            public Fail() { }
            public Fail(string message)
                : this()
            {
                Message = message;
            }
        }

        public class Success : IResponse
        {
            public MMI.Payment.Status.Code StatusCode
            {
                get
                {
                    return MMI.Payment.Status.Code.SUCCESS;
                }
            }

            public string ReferenceCode { get; set; }
            public string TransactionID { get; set; }
            public Success() { }
            public Success(string referenceCode, string transactionId)
                : this()
            {
                ReferenceCode = referenceCode;
                TransactionID = transactionId;
            }
        }

        static IResponse Process(string firstDataProviderName, string firstDataAccountDebit, DebitCardPayment DebitCardPaymentInfo, ChargeInfo Charge)
        {
            var configurationValues = System.Configuration.ConfigurationManager.AppSettings.AllKeys.SelectMany(System.Configuration.ConfigurationManager.AppSettings.GetValues, (k, v) => new {Key = k, Value = v});

            Dictionary<string, string> values = new Dictionary<string, string>();
            foreach (var configurationValue in configurationValues)
            {
                values.Add(configurationValue.Key, configurationValue.Value);
            }

            IGatewayConfiguration config = new AppSettingsConfiguration(values);

            Customer c  = new Customer();
            c.FirstName = DebitCardPaymentInfo.NameFirst;
            c.LastName  = DebitCardPaymentInfo.NameLast;
            c.Street1   = DebitCardPaymentInfo.StreetNumber;
            c.Zip       = DebitCardPaymentInfo.ZipCode;
            c.Id        = Charge.Account.Number;

            var debitCard              = new CreditCard();
            debitCard.CardNumber       = DebitCardPaymentInfo.AccountNumber;
            debitCard.ExpirationMonth  = DebitCardPaymentInfo.ExpirationMonth.ToString("D2");
            debitCard.ExpirationYear   = DebitCardPaymentInfo.ExpirationYear.ToString();
            debitCard.VerificationCode = DebitCardPaymentInfo.VerificationCode;
            debitCard.Type             = AccountType.CREDIT;

            debitCard.CardHolder = string.Concat(c.FirstName, " ", c.LastName);

            PaymentInfo info   = new PaymentInfo();
            info.Customer      = c;
            info.Amount        = Charge.TotalAmount;
            info.PaymentMethod = debitCard;

            Gateway gateway = new Gateway(config);
            GatewayStatus status = gateway.ProccessTransaction(firstDataProviderName, firstDataAccountDebit, TransactionType.SALE, info, null, "BKAttorney", "WEB");
            if (status.StatusCode == MMI.Payment.Status.Code.SUCCESS)
            {
                return new Success(status.ReferenceCode, status.TransactionId);
            }

            return new Fail(status.ErrorMessage);
        }
    }
}
