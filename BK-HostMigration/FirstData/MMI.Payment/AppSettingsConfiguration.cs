﻿/*******************************************************************************
 *
 * Defines the class AppSettingsConfiguration.
 *
 * File:        PaymentGateway/Library/AppSettingsConfiguration.cs
 * Date:        Fri Nov  7 15:21:17 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;
    using ReadOnlyStringMap = ReadOnlyDictionary<string, string>;

      /// <summary>
      /// Implementation of <see cref="IGatewayConfiguration"/> initialized
      /// from and instance of System.Configuration.
      /// </summary>
    public class AppSettingsConfiguration 
        : ReadOnlyStringMap, IGatewayConfiguration
    {
        /// <summary>
        /// Constructs a <see cref="AppSettingsConfiguration"/>.
        /// </summary>
        /// <param name="config">The System.Configuration.</param>
        public AppSettingsConfiguration(Configuration config)
            : base(CollectSettings(config))
            { }

        /// <summary>
        /// Constructs a <see cref="AppSettingsConfiguration"/>.
        /// </summary>
        /// <param name="config">The System.Configuration.</param>
        public AppSettingsConfiguration(IDictionary<string, string> config)
            : base(config)
        { }

        private static StringMap CollectSettings(Configuration config)
        {
            AppSettingsSection section = 
                (AppSettingsSection) config.GetSection("appSettings");
            ConfigurationElementCollection settings = section.Settings;
            StringMap values = new StringMap();
            foreach (KeyValueConfigurationElement elt in settings)
                values[elt.Key] = elt.Value;
            return values;
        }
    }
}
