﻿/*******************************************************************************
 *
 * Defines the class ReadOnlyDictionary. 
 * 
 * Adapted from public domain code published here:
 * http://www.simple-talk.com/community/forums/thread/2263.aspx.
 *
 * File:        PaymentGateway/Library/Template.cs
 * Date:        Mon Nov  3 22:48:35 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MMI.Payment
{

      /// <summary>
      /// An associative counterpart to 
      /// System.Collections.Generic.ObjectModel.ReadOnlyCollection.
      /// </summary>
      /// <typeparam name="TKey">The type of the key.</typeparam>
      /// <typeparam name="TValue">The type of the value.</typeparam>
    [Serializable]
    public class ReadOnlyDictionary<TKey, TValue> 
        : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, 
          IEnumerable<KeyValuePair<TKey, TValue>>, 
          IDictionary, ICollection, IEnumerable
    {
        public ReadOnlyDictionary(IDictionary<TKey, TValue> backing)
        {
            myDict = backing;
        }

        public void Add(TKey key, TValue value)
        {
            UnsupportedOperation();
        }

        public bool ContainsKey(TKey key)
        {
            return myDict.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return myDict.Keys; }
        }

        public bool Remove(TKey key)
        {
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return myDict.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return myDict.Values; }
        }

        public TValue this[TKey key]
        {
            get { return myDict[key]; }
            set { UnsupportedOperation(); }
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            UnsupportedOperation();
        }

        public void Clear()
        {
            UnsupportedOperation();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return myDict.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            myDict.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return myDict.Count; }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return false;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return myDict.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary) myDict).GetEnumerator();
        }

        public void Add(object key, object value)
        {
            UnsupportedOperation();
        }

        public bool Contains(object key)
        {
            return ((IDictionary) myDict).Contains(key);
        }

        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            return ((IDictionary) myDict).GetEnumerator();
        }

        public bool IsFixedSize
        {
            get { return ((IDictionary) myDict).IsFixedSize; }
        }

        ICollection IDictionary.Keys
        {
            get { return ((IDictionary) myDict).Keys; }
        }

        public void Remove(object key)
        {
            UnsupportedOperation();
        }

        ICollection IDictionary.Values
        {
            get { return ((IDictionary) myDict).Values; }
        }

        public object this[object key]
        {
            get { return ((IDictionary) myDict)[key]; }
            set { UnsupportedOperation(); }
        }

        public void CopyTo(Array array, int index)
        {
            ((IDictionary) myDict).CopyTo(array, index);
        }

        public bool IsSynchronized
        {
            get { return ((IDictionary) myDict).IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return ((IDictionary) myDict).SyncRoot; }
        }

        private void UnsupportedOperation()
        {
            throw new NotSupportedException(
                          "Attempt to modify a read-only collection"
                      );
        }

        private IDictionary<TKey, TValue> myDict;
    }
}
