﻿/*******************************************************************************
 *
 * Defines the class Schedule.
 *
 * File:        PaymentGateway/Library/Schedule.cs
 * Date:        Mon May  4 13:02:24 MDT 2009
 *
 * Copyright:   2009 Money Management International
 * Author:      CodeRage
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace MMI.Payment
{

    using RuleList = SortedList<Schedule.Rule>;
    
    /// <summary>
    /// A payment schedule, represented as a collection of rules.
    /// </summary>
    [Serializable]
    public class Schedule 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public Schedule() 
            : this(new DateTime(0), new DateTime(0))
            { }

        /// <summary>
        /// Constructs a <see cref="Schedule"/> with an empty list of rules.
        /// </summary>
        public Schedule(DateTime begin, DateTime end)
        {
            myBegin = begin;
            myEnd = end;
            myRules = new RuleList();
        }

        /// <summary>
        /// Constructs a <see cref="Schedule"/> with an empty list of rules.
        /// </summary>
        public Schedule(DateTime begin, DateTime end, IEnumerable<Rule> rules)
        {
            myBegin = begin;
            myEnd = end;
            myRules = new RuleList(rules);
        }

        /// <summary>
        /// Gets or sets the begin date.
        /// </summary>
        /// <value>The begin date.</value>
        [XmlElement(DataType = "date")]
        public DateTime Begin
        {
            get { return myBegin; }
            set { myBegin = value; }
        }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        [XmlElement(DataType = "date")]
        public DateTime End
        {
            get { return myEnd; }
            set { myEnd = value; }
        }

        /// <summary>
        /// Gets or sets the list of rules.
        /// </summary>
        /// <value>The list of rules.</value>
        [XmlElement(ElementName="Rule")]
        public RuleList Rules
        {
            get { return myRules; }
            set 
            { 
                Validate(value);
                myRules = value; 
            }
        }

        /// <summary>
        /// Adds a rule.
        /// </summary>
        /// <param name="facet">The facet.</param>
        /// <param name="pattern">The pattern definition.</param>
        public void AddRule(Facet facet, string pattern)
        {
            RuleList copy = new RuleList(myRules);
            copy.Add(new Rule(facet, new Pattern(pattern)));
            Validate(copy);
            myRules = copy;
        }

        /// <summary>
        /// Adds a rule.
        /// </summary>
        /// <param name="facet">The facet.</param>
        /// <param name="pattern">The pattern.</param>
        public void AddRule(Facet facet, Pattern pattern)
        {
            RuleList copy = new RuleList(myRules);
            copy.Add(new Rule(facet, pattern));
            Validate(copy);
            myRules = copy;
        }
        
        /// <summary>
        /// Returns the bitwise OR of the facets of the rules in this
        /// Schedule. Useful for checking for combinations of facets
        /// not supported by a provider.
        /// </summary>
        /// <returns>A bitwise OR of Facet enumerators.</returns>
        public Facet Facets()
        {
            Facet mask = Facet.NULL;
            foreach (Rule rule in myRules)
                mask |= rule.Facet;
            return mask;
        }

        /// <summary>
        /// Returns the pattern, if any, associated with the given facet.
        /// </summary>
        /// <param name="facet">The facet.</param>
        /// <returns>The pattern.</returns>
        public Pattern GetPattern(Facet facet)
        {
            foreach (Rule r in myRules)
                if (r.Facet == facet)
                    return r.Pattern;
            return null;
        }

        public static bool operator==(Schedule lhs, Schedule rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(Schedule lhs, Schedule rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            Schedule schedule = (Schedule) other;
                return Begin == schedule.Begin &&
                       End == schedule.End &&
                       Rules.SequenceEqual(schedule.Rules);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Begin: {0}; End: {1}; Rules: {2}]", 
                       Begin, End, Rules
                   );
        }

        /// <summary>
        /// Represents a way of measuring time, for use as the 
        /// Facet property of a Schedule.Rule
        /// </summary>
        public enum Facet
        {
            /// <summary>
            /// An invalid facet. Used during XML serialization.
            /// </summary>
            [XmlIgnore]
            NULL = 0,

            /// <summary>
            /// Measures time in days, starting with day 1 and 
            /// continuing indefinitely into the future.
            /// </summary>
            DAY = 1,

            /// <summary>
            /// Measures time in days, starting with day 0 (Sunday),
            /// continuing through day 6 (Saturday), and then 
            /// starting again at day 0.
            /// </summary>
            DAYOFWEEK = 2,

            /// <summary>
            /// Measures time in days, starting with day 1, continuing
            /// through day 28, 29, 30, or 31 - depending on the month
            /// in question - and then starting again at day 1.
            /// </summary>
            DAYOFMONTH = 4,

            /// <summary>
            /// Measures time in days, starting with day 01/01, 
            /// continuing through 12/31, and then starting again at day
            /// 01/01.
            /// </summary>
            DAYOFYEAR = 8,

            /// <summary>
            /// Measures time in days as a negative offset from a given day,
            /// e.g., "5 days before ...."
            /// </summary>
            DAYSBEFORE = 16,

            /// <summary>
            /// Measures time in weeks, starting with week 1 and 
            /// continuing indefinitely into the future.
            /// </summary>
            WEEK = 32,

            /// <summary>
            /// Measures time in weeks, starting with week 1, continuing through
            /// week 4 or 5, and then starting again at week 1. 
            /// </summary>
            WEEKOFMONTH = 64,

            /// <summary>
            /// Measures time in months, starting with month 1 and 
            /// continuing indefinitely into the future.
            /// </summary>
            MONTH = 128,

            /// <summary>
            /// Measures time in months, starting with month 1 (January), 
            /// continuing through month 12 (December), and then starting
            /// again at month 1.
            /// </summary>
            MONTHOFYEAR = 256,
        }
        
        /// <summary>
        /// Represents a day of the year
        /// </summary>
        [Serializable]
        public class Date : IComparable
        {
            /// <summary>
            /// Default constructor, needed for XML serialization.
            /// </summary>
            public Date() : this(0, 0) { }

            /// <summary>
            /// Constructs a <see cref="Date"/>.
            /// </summary>
            /// <param name="month">The month.</param>
            /// <param name="day">The day.</param>
            public Date(int month, int day)
            {
                Validate(month, day);
                myMonth = month;
                myDay = day;
            }

            /// <summary>
            /// Copy constructor.
            /// </summary>
            /// <param name="other">The Date to be copied.</param>
            public Date(Date other) 
                : this(other.Month, other.Day)
                { }

            /// <summary>
            /// Returns the month of the year, as an integer from 1 to 12.
            /// </summary>
            /// <value>The month.</value>
            public int Month 
            { 
                get { return myMonth; } 
                set {
                    if (value != 0 && myDay != 0)
                        Validate(value, myDay);
                    myMonth = value;
                }
            }

            /// <summary>
            /// Returns the day of the month, as an integer from 1 to 31.
            /// </summary>
            /// <value>The day.</value>
            public int Day 
            { 
                get { return myDay; } 
                set {
                    if (value != 0 && myMonth != 0)
                        Validate(myMonth, value);
                    myDay = value;
                }
            }

            public static bool operator==(Date lhs, Date rhs)
            {
                return !Object.ReferenceEquals(lhs, null) ? 
                    lhs.Equals(rhs) : 
                    Object.ReferenceEquals(rhs, null);
            }

            public static bool operator!=(Date lhs, Date rhs)
            {
                return !(lhs == rhs);
            }

            public override bool Equals(object other)
            {
                if (other == null)
                    return false;
                try {
 	                Date date = (Date) other;
                    return Month == date.Month &&
                           Day == date.Day;
                } catch (InvalidCastException) {
                    return false;
                }
            }

            public override int GetHashCode()
            {
                return ToString().GetHashCode();
            }

            public override string ToString()
            {
                return myMonth.ToString("D2") + "/" + myDay.ToString("D2");
            }

            public int CompareTo(object other)
            {
                Date date = (Date) other;
                return Month != date.Month ?
                    Month - date.Month :
                    Day - date.Day;
            }

            /// <summary>
            /// Returns a Date constructed by parsing the given string.
            /// </summary>
            /// <param name="value">A string of the form mm/dd</param>
            /// <returns>The Date</returns>
            public static Date Parse(string value)
            {
                Match m = myMatchDate.Match(value);
                if (m.Success) {
                     int month = Int32.Parse(m.Groups[1].Captures[0].Value);
                     int day = Int32.Parse(m.Groups[2].Captures[0].Value);
                     return new Date(month, day);
                }

                // If we get here, string was not of the form xx/yy, one
                // of xx or yy is non-numeric, or xx/yy is not a valid
                // month/day combination.
                throw new
                    PaymentException(
                        "Invalid date: " + value,
                        Status.Code.INVALID_PARAMETER
                    );
            }

            private static void Validate(int month, int day)
            {
                try {
                    // 2000 is a leap year, and so provides a lenient test
                    new DateTime(2000, month, day);
                } catch (Exception) {
                    throw new 
                        PaymentException(
                            String.Format("Invalid date: {0}/{1}", month, day),
                            Status.Code.INVALID_PARAMETER
                        );
                }
            }

            private static Regex  myMatchDate = 
                new Regex(@"(^\d{1,2})/(\d{1,2})$");
            private int           myMonth;
            private int           myDay;
        }
      
        /// <summary>
        /// Represents a test that a duration of time must match in order 
        /// to be included in a schedule.
        /// </summary>
        [Serializable]
        public class Pattern 
        {

            /// <summary>
            /// Default constructor, needed for XML serialization.
            /// </summary>
            public Pattern() { }

            /// <summary>
            /// Constructs a <see cref="Pattern"/>.
            /// </summary>
            /// <param name="definition">The pattern definition.</param>
            public Pattern(string definition)
            {
                Definition = definition;
            }

            /// <summary>
            /// Determines whether this instance is periodic, i.e., whether
            /// it is of the form "*/x". E.g., the pattern "*/2" can be combined
            /// with the facet DAY to yield the rule "every other day."
            /// </summary>
            /// <returns>
            /// 	<c>true</c> if this instance is periodic; otherwise, 
            /// 	<c>false</c>.
            /// </returns>
            public bool HasPeriod() { return myPeriod.HasValue; }

            /// <summary>
            /// Returns this instance's period. E.g., the period of "*/2"
            /// is two.
            /// </summary>
            /// <value>The period</value>
            [XmlIgnore]
            public int Period 
            { 
                get { return myPeriod.Value; }
            }

            /// <summary>
            /// Determines whether this instance consists of a
            /// sequence of integers, e.g., "1,3,5".
            /// </summary>
            /// <returns>
            /// 	<c>true</c> if this instance consists of a sequence of
            /// 	integers; otherwise, <c>false</c>.
            /// </returns>
            public bool HasValues() { return myValues != null; }

            /// <summary>
            /// Returns this instance's sequence of integers, if any. E.g.,
            /// a pattern with definition "1,3,5" would return the list:
            /// 1, 3, 5.
            /// </summary>
            /// <value>A sequence of integers, in ascending order</value>
            [XmlIgnore]
            public List<int> Values 
            { 
                get { return myValues; }
            }

            /// <summary>
            /// Determines whether this instance consists of a
            /// sequence of days of the year. E.g., "01/01,03/15,04/15" 
            /// represents the sequence: January 1, March 15, April 15.
            /// </summary>
            /// <returns>
            /// 	<c>true</c> if this instance consists of a sequence of
            /// 	days of the year; otherwise, <c>false</c>.
            /// </returns>
            public bool HasDates() { return myDates != null; }

            /// <summary>
            /// Returns this instance's sequence of days of the year, if any.
            /// E.g., a pattern with definition "01/01,03/15,04/15" 
            /// would return a list of Date objects representing January 1, 
            /// March 15, and April 15.
            /// </summary>
            /// <value>A sequence of instances of Schedule.Date, in ascending 
            /// order</value>
            [XmlIgnore]
            public List<Date> Dates
            { 
                get { return myDates; }
            }
            
            /// <summary>
            /// Gets or sets the definition.
            /// </summary>
            /// <value>The definition.</value>
            public string Definition
            {
                get { return myDefinition; }
                set
                {
                    if (value == null)
                        throw new
                            PaymentException(
                                "Pattern definition must be non-null",
                                Status.Code.INVALID_PARAMETER
                            );

                    int?        period = null;
                    List<int>   values = null;
                    List<Date>  dates = null;
                    string      definition = value.Trim();
                    if (definition == "*") {
                        period = 1;
                    } else if (String.Compare(definition, 0, "*/", 0, 2) == 0) {
                        try {
                            period = Int32.Parse(definition.Substring(2));
                        } catch (FormatException) { 
                            throw new 
                                PaymentException(
                                    String.Format(
                                        "Invalid period: {0}",
                                        definition.Substring(2)
                                    ),
                                    Status.Code.INVALID_PARAMETER
                                );
                        }
                    } else if (myMatchDates.IsMatch(definition)) {

                        // Create list of dates and normalize definition
                        dates = 
                            myMatchComma
                                .Split(definition)
                                .ToList()
                                .ConvertAll((string d) => Date.Parse(d));
                        dates.Sort();
                        if (ContainsDuplicates(dates))
                            throw new
                                PaymentException(
                                    "Duplicate items in schedule pattern: " +
                                        definition,
                                    Status.Code.INVALID_PARAMETER
                                );
                        definition = 
                            String.Join(
                                ",", 
                                dates
                                    .ConvertAll((Date d) => d.ToString())
                                    .ToArray()
                            );
                    } else if (definition.Length > 0) {

                        // Create list of integers and normalize definition
                        try {
                            values = 
                                myMatchComma
                                    .Split(definition)
                                    .ToList()
                                    .ConvertAll((string i) => this.ParseInt(i));
                        } catch (Exception) {
                            throw new
                                PaymentException(
                                    String.Format(
                                        "Invalid pattern definition: {0}",
                                        definition
                                    ),
                                    Status.Code.INVALID_PARAMETER
                                );
                        }
                        values.Sort();
                        if (ContainsDuplicates(values))
                            throw new
                                PaymentException(
                                    "Duplicate items in schedule pattern: " +
                                        definition,
                                    Status.Code.INVALID_PARAMETER
                                );
                        definition =
                            String.Join(
                                ",", 
                                values
                                    .ConvertAll((int i) => i.ToString())
                                    .ToArray()
                            );
                    } else {
                        throw new
                            PaymentException(
                                String.Format(
                                    "Invalid pattern definition: {0}",
                                    definition
                                ),
                                Status.Code.INVALID_PARAMETER
                            );
                    }
                    myPeriod = period;
                    myValues = values;
                    myDates = dates;
                    myDefinition = definition;
                }
            }

            public static bool operator==(Pattern lhs, Pattern rhs)
            {
                return !Object.ReferenceEquals(lhs, null) ? 
                    lhs.Equals(rhs) : 
                    Object.ReferenceEquals(rhs, null);
            }

            public static bool operator!=(Pattern lhs, Pattern rhs)
            {
                return !(lhs == rhs);
            }

            public override bool Equals(object other)
            {
                if (other == null)
                    return false;
                try {
 	                Pattern pattern = (Pattern) other;
                    return Definition == pattern.Definition;
                } catch (InvalidCastException) {
                    return false;
                }
            }

            public override int GetHashCode()
            {
                return Definition.GetHashCode();
            }

            public override string ToString()
            {
                return Definition;
            }

            /// <summary>
            /// Determines whether the specified list contains 
            /// duplicates. Requires that the list be sorted.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="list">The list.</param>
            /// <returns>
            /// 	<c>true</c> if the specified list contains 
            /// 	duplicates; otherwise, <c>false</c>.
            /// </returns>
            private bool ContainsDuplicates<T> (List<T> list)
            {   
                if (list.Count < 2)
                    return false;
                T prev = list[0];
                for (int z = 1, n = list.Count; z < n; ++z) {
                    T cur = list[z];
                    if (cur.Equals(prev))
                        return true;
                    prev = cur;
                }
                return false;
            }
            
            /// <summary>
            /// Parses an element of a list of integers or symbolic
            /// constants.
            /// </summary>
            /// <param name="value">The unparsed list item.</param>
            /// <returns></returns>
            private int ParseInt(string value)
            {
                switch (value.ToLowerInvariant()) {
                case "sun":
                    return 0;
                case "mon":
                case "jan":
                    return 1;
                case "tue":
                case "feb":
                    return 2;
                case "wed":
                case "mar":
                    return 3;
                case "thu":
                case "apr":
                    return 4;
                case "fri":
                case "may":
                    return 5;
                case "sat":
                case "jun":
                    return 6;
                case "jul":
                    return 7;
                case "aug":
                    return 8;
                case "sep":
                    return 9;
                case "oct":
                    return 10;
                case "nov":
                    return 11;
                case "dec":
                    return 12;
                default:
                    return Int32.Parse(value);
                }
            }

            private static Regex  myMatchDates = 
                new Regex(@"^\d+/\d+(\s*,\s*\d+/\d+)*$");
            private static Regex  myMatchComma = new Regex(@"\s*,\s*");
            private int?          myPeriod;
            private List<int>     myValues;
            private List<Date>    myDates;
            private string        myDefinition;
        }

        /// <summary>
        /// Represents the combination of a schedule facet and a Pattern
        /// </summary>
        [Serializable]
        public class Rule : IComparable
        {

            /// <summary>
            /// Default constructor, needed for XML serialization.
            /// </summary>
            public Rule() : this(Facet.NULL, null) { }
            
            /// <summary>
            /// Constructs a <see cref="Schedule.Rule"/>.
            /// </summary>
            /// <param name="facet">The facet.</param>
            /// <param name="pattern">The pattern.</param>
            public Rule(Facet facet, Pattern pattern)
            {
                Facet = facet;
                Pattern = pattern;
            }
            
            /// <summary>
            /// Gets or sets the facet.
            /// </summary>
            /// <value>The facet.</value>
            public Facet Facet
            {
                get { return myFacet; }
                set
                {
                    if (value != Facet.NULL && myPattern != null)
                        Validate(value, myPattern);
                    myFacet = value;
                }
            }
                
            /// <summary>
            /// Gets or sets the Pattern.
            /// </summary>
            /// <value>The Pattern.</value>
            [XmlIgnore]
            public Pattern Pattern
            {
                get { return myPattern; }
                set
                {
                    if (value != null && myFacet != Facet.NULL)
                        Validate(myFacet, value);
                    myPattern = value;
                }
            }

            /// <summary>
            /// Gets or sets the pattern definition; used for XML
            /// serialization.
            /// </summary>
            /// <value>The Pattern.</value>
            [XmlElement(ElementName="Pattern")]
            public string PatternDefinition
            {
                get { return Pattern.Definition; }
                set { Pattern = new Pattern(value); }
            }

            public static bool operator==(Rule lhs, Rule rhs)
            {
                return !Object.ReferenceEquals(lhs, null) ? 
                    lhs.Equals(rhs) : 
                    Object.ReferenceEquals(rhs, null);
            }

            public static bool operator!=(Rule lhs, Rule rhs)
            {
                return !(lhs == rhs);
            }

            public override bool Equals(object other)
            {
                if (other == null)
                    return false;
                try {
 	                Rule Rule = (Rule) other;
                    return Facet == Rule.Facet &&
                           Pattern == Rule.Pattern;
                } catch (InvalidCastException) {
                    return false;
                }
            }

            public override int GetHashCode()
            {
                return ToString().GetHashCode();
            }

            public override string ToString()
            {
                return String.Format(
                           "[Facet: {0}; Pattern: {1}]",
                           Facet, Pattern
                       );
            }

            public int CompareTo(object other)
            {
                return Facet.CompareTo(((Rule) other).Facet);
            }

            private static void Validate(Facet facet, Pattern pattern)
            {
                List<int> values;
                switch (facet) {
                case Facet.DAY:
                case Facet.WEEK:
                case Facet.MONTH:
                    if (!pattern.HasPeriod())
                        RejectRule(facet, pattern);
                    break;
                case Facet.DAYOFWEEK:
                    if (pattern.HasPeriod() && pattern.Period < 7)
                        return;
                    ValidateList(facet, pattern, 0, 6, "weekdays");
                    break;
                case Facet.DAYOFMONTH:
                    if (pattern.HasPeriod() && pattern.Period < 31)
                        return;
                    ValidateList(facet, pattern, 1, 31, "days of the month");
                    break;
                case Facet.DAYOFYEAR:
                    if (!pattern.HasDates())
                        RejectRule(facet, pattern);
                    break;
                case Facet.DAYSBEFORE:
                    if (!pattern.HasValues())
                        RejectRule(facet, pattern);
                    values = pattern.Values;
                    if (values.Count > 1)
                        throw new 
                            PaymentException(
                                String.Format(
                                    "Invalid pattern for facet {0}: " +
                                        "expected a single integer; found " +
                                        "'{1}'",
                                    facet, 
                                    pattern
                                ),
                                Status.Code.INVALID_PARAMETER
                            );
                    if (values[0] <= 0)
                        throw new
                            PaymentException(
                                String.Format(
                                    "Invalid value for facet {0}: {1}",
                                    facet, 
                                    values[0]
                                ),
                                Status.Code.INVALID_PARAMETER
                            );
                    break;
                case Facet.WEEKOFMONTH:
                    if (pattern.HasPeriod() && pattern.Period < 5)
                        return;
                    ValidateList(facet, pattern, 1, 5, "weeks of the month");
                    break;
                case Facet.MONTHOFYEAR:
                    if (pattern.HasPeriod() && pattern.Period < 12)
                        return;
                    ValidateList(facet, pattern, 1, 12, "months of the year");
                    break;
                default:
                    throw new PaymentException(
                                  "Invalid facet: " + facet,
                                  Status.Code.INVALID_PARAMETER
                              );
                }
            }

            private static void ValidateList(
                Facet facet, Pattern pattern, int min, int max, string label)
            {
                if (!pattern.HasValues())
                    RejectRule(facet, pattern);
                List<int> values = pattern.Values;
                if (values[0] < min || values.Last() > max)
                    throw new 
                        PaymentException(
                            String.Format(
                                "Invalid list of {0}: {1}",
                                label, pattern
                            ),
                            Status.Code.INVALID_PARAMETER
                        );
            }

            private static void RejectRule(Facet facet, Pattern pattern)
            {
                throw new
                    PaymentException(
                        String.Format(
                            "The facet {0} cannot be used with a {1} " +
                                "(found '{2}')",
                            facet,
                            pattern.HasPeriod() ?
                                "periodic pattern" :
                                pattern.HasValues() ?
                                    "list of integers" :
                                    "list of dates",
                            pattern
                        ),
                        Status.Code.INCONSISTENT_PARAMETERS
                    );
            }

            [OnDeserializedAttribute()]
            private void OnDeserialized(StreamingContext ctx)
            {
                if (myFacet == Facet.NULL)
                    throw new 
                        PaymentException(
                            String.Format(
                                "Invalid schedule rule: invalid facet {0} " +
                                    "for pattern {1}",
                                myFacet,
                                myPattern
                            ),
                            Status.Code.MISSING_PARAMETER
                        );
                if (myPattern == null)
                    throw new 
                        PaymentException(
                            String.Format(
                                "Invalid schedule rule: missing pattern for " +
                                    "facet {1}",
                                myFacet
                            ),
                            Status.Code.MISSING_PARAMETER
                        );
            }

            private Facet    myFacet;
            private Pattern  myPattern;
        }

        private static void Validate(RuleList rules)
        {
            Facet mask = Facet.NULL;
            foreach (Rule rule in rules)
                mask |= rule.Facet;
            Facet days = 
                Facet.DAYOFWEEK | Facet.DAYOFMONTH | Facet.DAYOFYEAR;
            Facet weeks = Facet.WEEK | Facet.WEEKOFMONTH;
            Facet months = Facet.MONTH | Facet.MONTHOFYEAR;
            Facet weekOrMonths = Facet.WEEK | months;
            if ( (mask & days) != 0 && !IsPowerOfTwo(mask & days) ||
                 (mask & weeks) != 0 && !IsPowerOfTwo(mask & weeks) ||
                 (mask & months) != 0 && !IsPowerOfTwo(mask & months) ||
                 (mask & weekOrMonths) != 0 && 
                     !IsPowerOfTwo(mask & weekOrMonths) ||
                 (mask & weeks) != 0 && (mask & Facet.DAYOFWEEK) == 0 ||
                 (mask & months) != 0 && 
                     (mask & (Facet.DAYOFMONTH | Facet.DAYOFWEEK)) == 0 ||
                 (mask & Facet.DAY) != 0 && mask != Facet.DAY || 
                 (mask & Facet.DAYOFYEAR) != 0 &&
                     (mask & ~(Facet.DAYOFYEAR | Facet.DAYSBEFORE)) != 0 ||
                 (mask & Facet.DAYSBEFORE) != 0 &&
                     (mask & (Facet.DAYOFWEEK | Facet.WEEK)) != 0 ||
                 mask == Facet.DAYSBEFORE )
            {
                string[] facets =
                    rules
                        .ToList()
                        .ConvertAll((Rule r) => r.Facet.ToString())
                        .ToArray();
                throw new
                    PaymentException(
                        String.Format(
                            "Invalid combination of facets: {0}",
                            String.Join(",", facets)
                        ),
                        Status.Code.INCONSISTENT_PARAMETERS
                    );
            }
        }

        private static bool IsPowerOfTwo(Facet facet)
        {
            int value = (int) facet;
            return Math.Pow(2, Math.Floor(Math.Log(value, 2))) == value;
        }
        
        private DateTime myBegin;
        private DateTime myEnd;
        private RuleList myRules;
    }
}
