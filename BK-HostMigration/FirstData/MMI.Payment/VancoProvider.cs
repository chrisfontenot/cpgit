﻿/*******************************************************************************
 *
 * Defines the class VancoProvider.
 *
 * File:        PaymentGateway/Library/VancoProvider.cs
 * Date:        Mon Nov  3 17:33:32 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 *******************************************************************************
 * Amendment History
 *
 * Who           Date               Changes
 *-----------------------------------------------------------------------------
 * MDH         12/01/2008           VancoResponse Amended
 *                                  XML added to error message for Failed
 *                                  parsing Vanco response
 * MDH          12/22/2008          Send
 *                                  Code re-written to work with vanco live
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Web;
using System.Xml;
using System.Reflection;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;
    using ReadOnlyStringMap = ReadOnlyDictionary<string, string>;

      /// <summary>
      /// Encapsulates a Vanco Web Services 2.0 request.
      /// </summary>
    internal class VancoRequest {

        /// <summary>
        /// Length of the RequestID variable in a Vanco request.
        /// </summary>
        public static readonly int IdLength = 32;

        /// <summary>
        /// Format of the RequestTime variable in a Vanco request.
        /// </summary>
        public static readonly string DateFormat = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Constructs a <see cref="VancoRequest"/>.
        /// </summary>
        /// <param name="type">The service name, e.g., Login.</param>
        /// <param name="values">An associative array of input
        /// parameters to the service.</param>
        public VancoRequest(string type, StringMap values)
            : this(type, null, values)
            { }

        /// <summary>
        /// Constructs a <see cref="VancoRequest"/>.
        /// </summary>
        /// <param name="type">The service name, e.g., Login.</param>
        /// <param name="sessionId">A session ID returned by a prior
        /// call to the Login service.</param>
        /// <param name="values">An associative array of input
        /// parameters to the service.</param>
        public VancoRequest(string type, string sessionId, StringMap values)
        {
            myType = type;
            myId = GenerateId();
            myTime = DateTime.Now.ToString(DateFormat);
            mySessionId = sessionId;
            myValues = values;
        }

        /// <summary>
        /// Serializes this request as XML.
        /// </summary>
        /// <returns>The XML.</returns>
        public string ToXml()
        {
           // if (myType != "Login") return "";

            StringBuilder sb = new StringBuilder();

            sb.Append("<VancoWS><Auth><RequestType>");
            sb.Append(SecurityElement.Escape(myType));
            sb.Append("</RequestType><RequestID>");
            sb.Append(SecurityElement.Escape(myId));
            sb.Append("</RequestID><RequestTime>");
            sb.Append(SecurityElement.Escape(myTime));
            sb.Append("</RequestTime>");
            if (mySessionId != null) {
                sb.Append("<SessionID>");
                sb.Append(SecurityElement.Escape(mySessionId));
                sb.Append("</SessionID>");
            }
            sb.Append("<Version>2</Version></Auth><Request><RequestVars>");
            foreach (var param in myValues) {
                sb.Append("<");
                sb.Append(param.Key);
                sb.Append(">");
                sb.Append(SecurityElement.Escape(param.Value));
                sb.Append("</");
                sb.Append(param.Key);
                sb.Append(">");
            }
            sb.Append("</RequestVars></Request></VancoWS>");

            return sb.ToString();
        }

        /// <summary>
        /// Generates a random request ID.
        /// </summary>
        /// <returns>The ID.</returns>
        private static string GenerateId()
        {
            StringBuilder random = new StringBuilder();
            for (int z = 0; z < IdLength; ++z)
                random.Append(Convert.ToChar(96 + myRandom.Next(1, 26)));
            return random.ToString();
        }

        private static Random  myRandom = RandomFactory.NewRandom();
        private string         myType;
        private string         myId;
        private string         myTime;
        private string         mySessionId;
        private StringMap      myValues;
    }

    /// <summary>
    /// Represents an error contained in a <see cref="VancoResponse"/>.
    /// A single response may contain multiple errors.
    /// </summary>
    internal class VancoError {

        /// <summary>
        /// Constructs a <see cref="VancoError"/>.
        /// </summary>
        /// <param name="code">The error code.</param>
        /// <param name="message">The error message.</param>
        public VancoError(int code, string message)
        {
            myErrorCode = code;
            myErrorMessage = message;
        }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        /// <value>The status.</value>
        public int ErrorCode { get { return myErrorCode; } }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get { return myErrorMessage; } }

        public override string ToString()
        {
            return String.Format(
                       "[ErrorCode: {0}; ErrorMessage: {1}]",
                       ErrorCode.ToString(), ErrorMessage
                   );
        }

        /// <summary>
        /// The error code.
        /// </summary>
        private int     myErrorCode;
        /// <summary>
        /// The error message.
        /// </summary>
        private string  myErrorMessage;
    }

    /// <summary>
    /// Encapsulates a response to a Vanco Web Services 2.0 request.
    /// </summary>
    internal class VancoResponse {
        /// <summary>
        /// Constructs a <see cref="VancoResponse"/>.
        /// </summary>
        /// <param name="xml">The XML returned by Vanco.</param>
        public VancoResponse(string xml)
        {
            // MDHINDS 12/01/2008 XML added to error message for Failed parsing Vanco response

            XmlDocument doc = new XmlDocument();
            try {
                doc.LoadXml(xml);
            } catch (Exception e) {
                throw new PaymentException(
                              String.Format(
                                  "Failed parsing Vanco response: {0} XML: {1}",
                                  e.Message,
                                  xml
                              ),
                              Status.Code.INVALID_PROVIDER_RESPONSE,
                              e
                          );
            }

            // Check document element
            if (doc.DocumentElement.LocalName != "VancoWS")
                throw new PaymentException(
                              String.Format(
                                  "Failed parsing Vanco response: " +
                                      "unexpected document element '{0}'",
                                  doc.DocumentElement.LocalName
                              ),
                              Status.Code.INVALID_PROVIDER_RESPONSE
                          );

            // Fetch list of child elements
            List<XmlElement> kids = ChildElements(doc.DocumentElement);
            if (kids.Count != 2)
                throw new PaymentException(
                              "Failed parsing Vanco response: malformed " +
                                  "'VancoWS' element",
                              Status.Code.INVALID_PROVIDER_RESPONSE
                          );

            // Check second child of document element
            if (kids[1].LocalName != "Response")
                throw new PaymentException(
                              String.Format(
                                  "Failed parsing Vanco response: " +
                                      "unexpected element '{0}'",
                                  kids[1].LocalName
                              ),
                              Status.Code.INVALID_PROVIDER_RESPONSE
                          );

            // Fetch list of child elements
            kids = ChildElements(kids[1]);

            // Branch according to whether this is an error response
            StringMap values = new StringMap();
            List<VancoError> errors = new List<VancoError>();
            if (kids.Count > 0 && kids[0].LocalName == "Errors") {
                foreach (var e in ChildElements(kids[0]))
                    errors.Add(ParseError(e));
            } else {
                foreach (var k in kids)
                    values[k.LocalName] = TextContent(k);
            }
            myValues = new ReadOnlyStringMap(values);
            myErrors = new ReadOnlyCollection<VancoError>(errors);
        }

        /// <summary>
        /// Gets the associative array of output parameters.
        /// </summary>
        /// <value>The values.</value>
        public ReadOnlyStringMap Values
        {
            get { return myValues; }
        }

        /// <summary>
        /// Gets the list of errors, if any.
        /// </summary>
        /// <value>The errors.</value>
        public ReadOnlyCollection<VancoError> Errors
        {
            get { return myErrors; }
        }

        /// <summary>
        /// Parses an Error element.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <returns>The Vanco error</returns>
        private VancoError ParseError(XmlElement error)
        {
            List<XmlElement> kids = ChildElements(error);
            if ( kids.Count != 2 ||
                 error.LocalName != "Error" ||
                 kids[0].LocalName != "ErrorCode" ||
                 kids[1].LocalName != "ErrorDescription" )
            {
                throw new PaymentException(
                              String.Format(
                                  "Malformed 'Error' element: {0}",
                                  error.ToString()
                              ),
                              Status.Code.INVALID_PROVIDER_RESPONSE
                          );
            }
            int errorCode = 0;
            try {
                errorCode = Int32.Parse(TextContent(kids[0]));
            } catch (Exception e) {
                throw new PaymentException(
                              String.Format("Invalid error code: {0}", kids[0]),
                              Status.Code.INVALID_PROVIDER_RESPONSE,
                              e
                          );
            }
            return new VancoError(errorCode, TextContent(kids[1]));
        }

        /// <summary>
        /// Returns the list of child elements of an XML element.
        /// </summary>
        /// <param name="elt">The parent element.</param>
        /// <returns>The list of children.</returns>
        private List<XmlElement> ChildElements(XmlElement elt)
        {
            List<XmlElement> kids = new List<XmlElement>();
            foreach (XmlNode k in elt.ChildNodes)
                if (k.NodeType == XmlNodeType.Element)
                    kids.Add((XmlElement) k);
            return kids;
        }

        /// <summary>
        /// Returns the text content of an XML element.
        /// </summary>
        /// <param name="elt">The element.</param>
        /// <returns>The contents as a string.</returns>
        private string TextContent(XmlElement elt)
        {
            StringBuilder sb = new StringBuilder();
            foreach (XmlNode k in elt.ChildNodes)
                if (k.NodeType == XmlNodeType.Text)
                    sb.Append(k.Value);
            return sb.ToString();
        }

        private ReadOnlyStringMap               myValues;
        private ReadOnlyCollection<VancoError>  myErrors;
    }

    /// <summary>
      /// Encapsulates access to Vanco Web Services 2.0.
    /// </summary>
    public class VancoProvider : Provider
    {
        /// <summary>
        /// Name of the vanco provider, as passed to
        /// <see cref="M:Mmi.Payment.Gateway.ProcessTransaction"/>.
        /// </summary>
        public static readonly string ProviderName = "vanco";

        /// <summary>
        /// URL of the Vanco Web Services 2.0 production server
        /// </summary>
        public static readonly string ProductionUri =
            "https://www.vancoservices.com/cgi-bin/ws2.vps";

        /// <summary>
        /// URL of the Vanco Web Services 2.0 test server
        /// </summary>
        public static readonly string DevelopmentUri =
            "https://www.vancodev.com/cgi-bin/wstest2.vps";

        /// <summary>
        /// Constructs a <see cref="VancoProvider"/> from a gateway
        /// configuration and a log.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        /// <param name="log">The log.</param>
        public VancoProvider(IGatewayConfiguration config, Log log)
            : base(ProviderName, log)
        {
            string key = "provider.vanco.request_uri";
            if (config.ContainsKey(key)) {
                myRequestUri = config[key];
            } else {
                throw new PaymentException(
                              String.Format(
                                  "Missing value for configuration property " +
                                      "'{0}",
                                  key
                              ),
                              Status.Code.CONFIGURATION_ERROR
                          );
            }
        }

        /// <summary>
        /// Processes debit card transaction.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="info">The info.</param>
        /// <param name="transaction">The transaction.</param>
        public override void ProcessSale(ProviderAccount account,
            PaymentInfo info, GatewayTransaction transaction)
        {
            ValidatePaymentInfo(info);
            string sessionId = Login(account, info, transaction);
            EFTAddCompleteTransaction(account, info, transaction, sessionId);
        }

        /// <summary>
        /// Sends a Login request to Vanco.
        /// </summary>
        /// <param name="account">The Vanco account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The associated gateway
        /// transaction.</param>
        /// <returns>The session ID.</returns>
        private string Login(ProviderAccount account, PaymentInfo info,
            GatewayTransaction transaction)
        {
            // Send Login request
            ProviderTransaction providerTrans =
                transaction.NewProviderTransaction("Login");
            StringMap values = new StringMap();
            values["UserID"] = account.Username;
            values["Password"] = account.Password;
            VancoRequest request = new VancoRequest("Login", values);
            VancoResponse response = Send(request);
            if (response.Errors.Count > 0) {

                // Log errors
                LogErrors(response);

                // Complete provider transaction
                VancoError error = response.Errors[0];
                ProviderStatus status =
                    new ProviderStatus(
                            error.ErrorCode.ToString(),
                            error.ErrorMessage,
                            null
                        );
                Status.Code code = GetStatusCode(error.ErrorCode);
                providerTrans.Complete(code, status);

                // Bail
                throw new PaymentException(
                              error.ErrorMessage,
                              Status.Code.PROVIDER_AUTHENTICATION_FAILED
                          );
            } else if (!response.Values.ContainsKey("SessionID")) {

                // Log error
                Log.Stream stream;
                if (stream = Log.GetStream(LogLevel.ERROR))
                    stream.Write(MethodBase.GetCurrentMethod(), 
                        "Missing SessionID in response to Vanco Login Request"
                    );

                // Complete provider transaction
                Status.Code code = Status.Code.INVALID_PROVIDER_RESPONSE;
                providerTrans.Complete(code);

                // Bail
                throw new PaymentException(Status.Message(code), code);

            } else {
                string sessionId = response.Values["SessionID"];

                // Complete provider transaction
                ProviderStatus status =
                    new ProviderStatus("0", "", sessionId);
                providerTrans.Complete(Status.Code.SUCCESS, status);
                return sessionId;
            }
        }

        /// <summary>
        /// Sends an EFTAddCompleteTransaction request followed by a
        /// Logout request to Vanco.
        /// </summary>
        /// <param name="account">The provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="transaction">The gateway transaction.</param>
        /// <param name="sessionId">The session ID.</param>
        private void EFTAddCompleteTransaction(ProviderAccount account,
            PaymentInfo info, GatewayTransaction transaction, string sessionId)
        {
            ProviderTransaction providerTrans =
                transaction.NewProviderTransaction(
                    "EFTAddCompleteTransaction"
                );
            StringMap values = EncodePaymentInfo(info);
            values["ClientID"] = account.AccountId1;
            VancoRequest request =
                new VancoRequest(
                        "EFTAddCompleteTransaction",
                        sessionId,
                        values
                    );
            VancoResponse response = Send(request);
            if (response.Errors.Count > 0) {

                // Log errors
                LogErrors(response);

                // Complete provider transaction
                VancoError error = response.Errors[0];
                ProviderStatus status =
                    new ProviderStatus(
                            error.ErrorCode.ToString(),
                            error.ErrorMessage,
                            null
                        );
                Status.Code code = GetStatusCode(error.ErrorCode);
                providerTrans.Complete(code, status);

                // Logout
                Logout(transaction, sessionId);

                // Bail
                throw new PaymentException(error.ErrorMessage, code);
            } else  {

                // Process CustomerRef
                StringMap outputParameters = new StringMap();
                Log.Stream stream;
                if (response.Values.ContainsKey("CustomerRef")) {
                    outputParameters["CustomerRef"] =
                        response.Values["CustomerRef"];
                } else {
                    if (stream = Log.GetStream(LogLevel.WARNING))
                        stream.Write(MethodBase.GetCurrentMethod(), "Missing 'CustomerRef' in Vanco response");
                }

                // Process PaymentMethodRef
                if (response.Values.ContainsKey("PaymentMethodRef")) {
                    outputParameters["PaymentMethodRef"] =
                        response.Values["PaymentMethodRef"];
                } else {
                    if (stream = Log.GetStream(LogLevel.WARNING))
                        stream.Write(MethodBase.GetCurrentMethod(), 
                            "Missing 'PaymentMethodRef' in Vanco response"
                        );
                }

                // Process TransactionRef
                if (response.Values.ContainsKey("TransactionRef")) {

                    // Complete provider transaction
                    string transactionRef = response.Values["TransactionRef"];
                    ProviderStatus status =
                        new ProviderStatus("0", "", transactionRef);
                    providerTrans.Complete(Status.Code.SUCCESS, status);

                    // Logout
                    Logout(transaction, sessionId);

                } else {

                    // Log error
                    if (stream = Log.GetStream(LogLevel.ERROR))
                        stream.Write(MethodBase.GetCurrentMethod(), 
                            "Missing 'TransactionRef' in Vanco response"
                        );

                    // Complete provider transaction
                    Status.Code code = Status.Code.INVALID_PROVIDER_RESPONSE;
                    providerTrans.Complete(code);

                    // Logout
                    Logout(transaction, sessionId);

                    // Complete transaction
                    throw new PaymentException(Status.Message(code), code);
                }
            }
        }

        /// <summary>
        /// Sends a Logout request to Vanco.
        /// </summary>
        /// <param name="transaction">The gateway transaction.</param>
        /// <param name="sessionId">The session ID.</param>
        private void Logout(GatewayTransaction transaction, string sessionId)
        {
            ProviderTransaction providerTrans =
                transaction.NewProviderTransaction("Logout");
            StringMap values = new StringMap();
            values["Logout"] = "";
            VancoRequest request =
                new VancoRequest("Logout", sessionId, values);
            VancoResponse response = Send(request);
            if (response.Errors.Count > 0) {
                LogErrors(response);
                VancoError error = response.Errors[0];
                ProviderStatus status =
                    new ProviderStatus(
                            error.ErrorCode.ToString(),
                            error.ErrorMessage,
                            null
                        );
                Status.Code code = GetStatusCode(error.ErrorCode);
                providerTrans.Complete(code, status);
            } else {
                ProviderStatus status = new ProviderStatus("0", "", "");
                providerTrans.Complete(Status.Code.SUCCESS, status);
            }
        }

        /// <summary>
        /// Sends the specified request to Vanco
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The Vacno response.</returns>
        private VancoResponse Send(VancoRequest request)
        {
            // MDHINDS 12/22/2008 Code re-written to work with vanco live

            string requestXml = request.ToXml();
            Log.Stream logStream;
            if (logStream = Log.GetStream(LogLevel.DEBUG))
                logStream.Write(MethodBase.GetCurrentMethod(), 
                    String.Format("Vanco request: {0}", requestXml)
                    );

            // MDH 23/12/2008 Begins

                // Validate XML

                try
                {
                    if (requestXml.ToString().Length == 0)
                    {
                        throw new PaymentException(String.Format("No XML Generated"),Status.Code.INTERNAL_ERROR);
                    }

                    // THIS IS DONE TO ENSURE CORRECT XML HEADER INFORMATION ON SEND TO PRODUCTION SERVER

                    XmlDocument _doc = new XmlDocument();

                    _doc.LoadXml(requestXml.ToString());

                    requestXml = _doc.OuterXml.ToString();
                }
                catch (Exception e)
                {
                    throw new PaymentException(
                                  String.Format(
                                      "XML will not load into DOM: '{0}' {1} ",
                                      requestXml,
                                      e.Message
                                  ),
                                  Status.Code.INTERNAL_ERROR, e
                              );
                }

                // Prepare HTTP request DATA

                System.Net.WebClient objRequest = null;
                
                byte[] responseBuffer = null;
                System.Byte[] buffer = null;
                
                string sFullResponse = "";
                string sBaseAddress = string.Format("{0}?xml=",
                                                    myRequestUri);

                // Create request client

                try
                {
                    objRequest = new System.Net.WebClient();

                    objRequest.Encoding = System.Text.Encoding.ASCII;
                    buffer = System.Text.Encoding.ASCII.GetBytes(requestXml);
                }
                catch (Exception e)
                {
                    throw new PaymentException(
                                  String.Format(
                                      "Can not create Web Client {0}",
                                      e.Message
                                  ),
                                  Status.Code.INTERNAL_ERROR, e
                              );
                }

                // Send data to Vanco

                try
                {
                   responseBuffer = objRequest.UploadData(sBaseAddress, 
                                                          "POST", 
                                                          buffer);
                }
                catch (Exception e)
                {
                    throw new PaymentException(
                                  "Failed posting data to Vanco: " + e.Message,
                                  Status.Code.NETWORK_ERROR,
                                  e
                              );
                }

                // Obtain the response

                try
                {
                    sFullResponse = System.Text.Encoding.ASCII.GetString(responseBuffer);
                    if (logStream = Log.GetStream(LogLevel.DEBUG))
                        logStream.Write(MethodBase.GetCurrentMethod(), 
                            String.Format("Vanco Response: {0}", sFullResponse)
                        );
                }
                catch (Exception e)
                {
                    throw new PaymentException(
                                  "Failed receiving response from Vanco: " + e.Message,
                                  Status.Code.NETWORK_ERROR, e
                              );
                }

                // Process the response

                try
                {
                    return new VancoResponse(sFullResponse);
                }
                catch (PaymentException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw new PaymentException(
                                  "Failed receiving response from Vanco: " + e.Message,
                                  Status.Code.NETWORK_ERROR, e
                              );
                }
                finally
                {
                    if (objRequest != null)
                        (objRequest as IDisposable).Dispose();
                }
            
            // MDH 23/12/2008 Ends
        }

        /// <summary>
        /// Logs the errors contained in the given Vanco response.
        /// </summary>
        /// <param name="response">The response.</param>
        private void LogErrors(VancoResponse response)
        {
            Log.Stream stream;
            foreach (var e in response.Errors) {
                LogLevel level = GetLogLevel(e.ErrorCode);
                if (stream = Log.GetStream(level))
                    stream.Write(MethodBase.GetCurrentMethod(), e.ErrorMessage);
            }
        }

        /// <summary>
        /// Validates the given payment information
        /// </summary>
        /// <param name="info">The info.</param>
        private void ValidatePaymentInfo(PaymentInfo info)
        {
            // Validate customer
            Customer c = info.Customer;
            if (c == null)
                throw new PaymentException(
                              "Customer information is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.FirstName == null)
                throw new PaymentException(
                              "First name is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.LastName == null)
                throw new PaymentException(
                              "Last name is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.Id == null)
                throw new PaymentException(
                              "Customer ID is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.Street1 == null)
                throw new PaymentException(
                              "Street address is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.City == null)
                throw new PaymentException(
                              "City is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (c.State == null)
                throw new PaymentException(
                              "State is required",
                              Status.Code.MISSING_PARAMETER
                          );;
            if (c.Zip == null)
                throw new PaymentException(
                              "ZIP code is required",
                              Status.Code.MISSING_PARAMETER
                          );

            // Validate amount
            if (info.Amount == 0)
                throw new PaymentException(
                              "Transaction amount is required",
                              Status.Code.MISSING_PARAMETER
                          );

            // Validate payment method
            PaymentMethod method = info.PaymentMethod;
            if (method == null)
                throw new PaymentException(
                              "Payment method is required",
                              Status.Code.MISSING_PARAMETER
                          );
            if (method is BankAccount) {
                BankAccount account = (BankAccount) method;
                if (account.AccountNumber == null)
                    throw new PaymentException(
                                  "Account number is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                if (account.RoutingNumber == null)
                    throw new PaymentException(
                                  "Routing number is required",
                                  Status.Code.MISSING_PARAMETER
                              );
            } else if (method is DebitCard) {
                DebitCard card = (DebitCard) method;
                if (card.CardHolder == null)
                    throw new PaymentException(
                                  "Cardholder name is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                if (card.CardNumber == null)
                    throw new PaymentException(
                                  "Card number is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                if (card.ExpirationMonth == null)
                    throw new PaymentException(
                                  "Expiration month is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                if (card.ExpirationYear == null)
                    throw new PaymentException(
                                  "Expiration year is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                if (card.VerificationCode == null)
                    throw new PaymentException(
                                  "Card verification code is required",
                                  Status.Code.MISSING_PARAMETER
                              );
                Address a = card.BillingAddress;
                if ( a.Street1 != null || a.Street2 != null ||
                     a.City != null || a.State != null || a.Zip != null )
                {
                    if (a.Street1 == null)
                        throw new PaymentException(
                                      "Billing street address is required",
                                      Status.Code.MISSING_PARAMETER
                                  );
                    if (a.City == null)
                        throw new PaymentException(
                                      "Billing city is required",
                                      Status.Code.MISSING_PARAMETER
                                  );
                    if (a.State == null)
                        throw new PaymentException(
                                      "Billing state is required",
                                      Status.Code.MISSING_PARAMETER
                                  );;
                    if (a.Zip == null)
                        throw new PaymentException(
                                      "Billing ZIP code is required",
                                      Status.Code.MISSING_PARAMETER
                                  );
                }
            } else {
                throw new PaymentException(
                              String.Format(
                                  "Invalid payment method: {0}",
                                  method.GetType().ToString()
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
            }
        }

        /// <summary>
        /// Encodes the given payment information as an associative
        /// array of values to be passed to Vanco in a
        /// EFTAddCompleteTransaction request.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <returns>The collection of values.</returns>
        private StringMap EncodePaymentInfo(PaymentInfo info)
        {
            StringMap values = new StringMap();

            // Encode customer info
            values["CustomerID"] = info.Customer.Id;
            values["CustomerName"] = info.Customer.LastName + "," +
                info.Customer.FirstName;
            values["CustomerAddress1"] = info.Customer.Street1;
            values["CustomerAddress2"] = info.Customer.Street2 != null ?
                info.Customer.Street2 :
                "";
            values["CustomerCity"] = info.Customer.City;
            values["CustomerState"] = info.Customer.State;
            values["CustomerZip"] = info.Customer.Zip;
            if (info.Customer.Phone != null)
                values["CustomerPhone"] = info.Customer.Phone;

            // Encode payment method
            StringMap properties = info.PaymentMethod.Properties;
            string type = properties["account_type"];
            values["AccountType"] = 
                type == AccountType.DEBIT.ToString() ?
                    "CC" :
                    type == AccountType.CHECKING.ToString() ?
                        "C" :
                        "S";
            values["AccountNumber"] = properties["account_number"];
            if (properties["routing_number"] != null)
                values["RoutingNumber"] = properties["routing_number"];
            if (type == AccountType.DEBIT.ToString()) {
                values["CardBillingName"] = properties["billing_name"];
                values["CardExpMonth"] =
                    properties["expiration_date"].Substring(0, 2);
                values["CardExpYear"] =
                    properties["expiration_date"].Substring(2);
                values["CardCVV2"] = properties["verification_code"];
                bool sameAddress = properties["billing_street1"] == null;
                values["SameCCBillingAddrAsCust"] = sameAddress ? "Yes" : "No";
                if (!sameAddress) {
                    values["CardBillingAddr1"] = properties["billing_street1"];
                    values["CardBillingAddr2"] =
                        properties["billing_street2"] != null ?
                            properties["billing_street2"] :
                            "";
                    values["CardBillingCity"] = properties["billing_city"];
                    values["CardBillingState"] = properties["billing_state"];
                    values["CardBillingZip"] = properties["billing_zip"];
                }
            }
            values["Amount"] = info.Amount.ToString("0.00");
            values["StartDate"] = "0000-00-00";
            values["FrequencyCode"] = "O";
            return values;
        }

        /// <summary>
        /// Translates the given Vanco error into a Status.Code.
        /// </summary>
        /// <param name="vancoError">The Vanco error.</param>
        /// <returns>The status code.</returns>
        private Status.Code GetStatusCode(int vancoError)
        {
            switch (vancoError)
            {
                case 158:
                    // Specified Client is Invalid
                    return Status.Code.PROVIDER_ERROR;
                case 159:
                    // Customer ID Required
                    return Status.Code.PROVIDER_ERROR;
                case 160:
                    // Customer ID is Already in Use
                    return Status.Code.PROVIDER_ERROR;
                case 161:
                    // Customer Name Required
                    return Status.Code.MISSING_PARAMETER;
                case 162:
                    // Invalid Date Format (Start Date)
                    return Status.Code.INVALID_PARAMETER;
                case 163:
                    // Transaction Type is required
                    return Status.Code.MISSING_PARAMETER;
                case 164:
                    // Transaction Type is invalid
                    return Status.Code.PROVIDER_ERROR;
                case 165:
                    // Fund required
                    return Status.Code.MISSING_PARAMETER;
                case 166:
                    // Customer Required
                    return Status.Code.MISSING_PARAMETER;
                case 167:
                    // Payment Method Not Found
                    return Status.Code.MISSING_PARAMETER;
                case 168:
                    // Amount Required
                    return Status.Code.MISSING_PARAMETER;
                case 170:
                    // Start Date Required
                    return Status.Code.MISSING_PARAMETER;
                case 171:
                    // Invalid Start Date
                    return Status.Code.INVALID_PARAMETER;
                case 172:
                    // End Date earlier than Start Date
                    return Status.Code.INCONSISTENT_PARAMETERS;
                case 173:
                    // Cannot Prenote a Credit Card
                    return Status.Code.INCONSISTENT_PARAMETERS;
                case 174:
                    // Cannot Prenote processed account
                    return Status.Code.PROVIDER_ERROR;
                case 175:
                    // Transaction pending for Prenote account
                    return Status.Code.PROVIDER_ERROR;
                case 176:
                    // Invalid Account Type
                    return Status.Code.INVALID_PARAMETER;
                case 177:
                    // Account Number Required
                    return Status.Code.MISSING_PARAMETER;
                case 178:
                    // Invalid Routing Number
                    return Status.Code.INVALID_CUSTOMER_ACCOUNT;
                case 179:
                    // Client doesn't accept Credit Card Transactions
                    return Status.Code.PROVIDER_ERROR;
                case 180:
                    // Client is in test mode for Credit Cards
                    return Status.Code.PROVIDER_ERROR;
                case 181:
                    // Client is cancelled for Credit Cards
                    return Status.Code.PROVIDER_ERROR;
                case 182:
                    // Name on Credit Card is Required
                    return Status.Code.MISSING_PARAMETER;
                case 183:
                    // Invalid Expiration Date
                    return Status.Code.INVALID_PARAMETER;
                case 184:
                    // Complete Billing Address is Required
                    return Status.Code.MISSING_PARAMETER;
                case 196:
                    // Recurring Telephone Entry Transaction NOT Allowed
                    return Status.Code.INCONSISTENT_PARAMETERS;
                case 198:
                    // Invalid State
                    return Status.Code.INVALID_PARAMETER;
                case 201:
                    // Frequency Required
                    return Status.Code.MISSING_PARAMETER;
                case 202:
                    // Account Cannot Be Deleted, Active Transaction Exists
                    return Status.Code.PROVIDER_ERROR;
                case 203:
                    // Client Does Not Accept ACH Transactions
                    return Status.Code.PROVIDER_ERROR;
                case 204:
                    // Duplicate Transaction
                    return Status.Code.PROVIDER_ERROR;
                case 210:
                    // Recurring Credits NOT Allowed
                    return Status.Code.PROVIDER_ERROR;
                case 224:
                    // Credit Card Credits NOT Allowed - Must Be Refunded
                    return Status.Code.PROVIDER_ERROR;
                case 231:
                    // Customer Not Found For Client
                    return Status.Code.PROVIDER_ERROR;
                case 232:
                    // Invalid Account Number
                    return Status.Code.INVALID_CUSTOMER_ACCOUNT;
                case 233:
                    // Invalid Country Code
                    return Status.Code.INVALID_PARAMETER;
                case 234:
                    // Transactions Are Not Allow From <COUNTRYCODE>
                    return Status.Code.PROVIDER_ERROR;
                case 286:
                    // Client not set up for International Credit Card
                    // Processing
                    return Status.Code.PROVIDER_ERROR;
                case 344:
                    // Phone Number Must be 10 Digits Long
                    return Status.Code.INVALID_PARAMETER;
                case 378:
                    // Invalid Loginkey
                    return Status.Code.PROVIDER_ERROR;
                case 379:
                    // Invalid RequestType
                    return Status.Code.PROVIDER_ERROR;
                case 380:
                    // Invalid SessionID
                    return Status.Code.PROVIDER_ERROR;
                case 381:
                    // Invalid ClientID for Session
                    return Status.Code.PROVIDER_ERROR;
                case 383:
                    // Internal Handler Error, Contact Vanco
                    return Status.Code.PROVIDER_ERROR;
                case 393:
                    // IP Address Blocked
                    return Status.Code.PROVIDER_ERROR;
                case 395:
                    // Transactions cannot be processed on Weekends
                    return Status.Code.PROVIDER_ERROR;
                case 410:
                    // Credits Cannot Be WEB or TEL
                    return Status.Code.PROVIDER_ERROR;
                case 415:
                    // End date falls on a weekend and will not process. To
                    // process this transaction, change the end date to the
                    // next business day.
                    return Status.Code.PROVIDER_ERROR;
                case 416:
                    // Customer Cannot Be Edited By This User
                    return Status.Code.PROVIDER_ERROR;
                case 429:
                    // Transaction Cannot Be Edited By This User
                    return Status.Code.PROVIDER_ERROR;
                case 434:
                    // Credit Card Processor Error. An error was returned by
                    // the Credit Card Company.   Description will include
                    // message returned from Credit Card Company.
                    return Status.Code.PAYMENT_DECLINED;
                case 489:
                    // Customer ID Exceeds 15 Characters
                    return Status.Code.INVALID_PARAMETER;
                case 495:
                    // (field name) Field Contains Invalid Characters. Note:
                    // This error description will change depending on what
                    // field the invalid character(s) was/were in.
                    return Status.Code.INVALID_PARAMETER;
                case 496:
                    // (field name) Field Contains Too Many Characters. Note:
                    // This error description will change depending on what
                    // field's content length was too long.
                    return Status.Code.INVALID_PARAMETER;
                case 500:
                    // Too Many Results, Please Narrow Search. Note: This error
                    // code will only be sent back for services that use search
                    // parameters
                    return Status.Code.PROVIDER_ERROR;
                case 501:
                    // Report Not Found
                    return Status.Code.PROVIDER_ERROR;
                default:
                    return Status.Code.PROVIDER_ERROR;
            }
        }

        /// <summary>
        /// Translates the given Vanco error into a LogLevel.
        /// </summary>
        /// <param name="vancoError">The vanco error.</param>
        /// <returns>The log level.</returns>
        private LogLevel GetLogLevel(int vancoError)
        {
            switch (vancoError)
            {
                case 158:
                    // Specified Client is Invalid
                    return LogLevel.FATAL_ERROR;
                case 159:
                    // Customer ID Required
                    return LogLevel.FATAL_ERROR;
                case 160:
                    // Customer ID is Already in Use
                    return LogLevel.FATAL_ERROR;
                case 161:
                    // Customer Name Required
                    return LogLevel.FATAL_ERROR;
                case 162:
                    // Invalid Date Format (Start Date)
                    return LogLevel.FATAL_ERROR;
                case 163:
                    // Transaction Type is required
                    return LogLevel.FATAL_ERROR;
                case 164:
                    // Transaction Type is invalid
                    return LogLevel.FATAL_ERROR;
                case 165:
                    // Fund required
                    return LogLevel.FATAL_ERROR;
                case 166:
                    // Customer Required
                    return LogLevel.FATAL_ERROR;
                case 167:
                    // Payment Method Not Found
                    return LogLevel.FATAL_ERROR;
                case 168:
                    // Amount Required
                    return LogLevel.FATAL_ERROR;
                case 170:
                    // Start Date Required
                    return LogLevel.FATAL_ERROR;
                case 171:
                    // Invalid Start Date
                    return LogLevel.FATAL_ERROR;
                case 172:
                    // End Date earlier than Start Date
                    return LogLevel.FATAL_ERROR;
                case 173:
                    // Cannot Prenote a Credit Card
                    return LogLevel.FATAL_ERROR;
                case 174:
                    // Cannot Prenote processed account
                    return LogLevel.FATAL_ERROR;
                case 175:
                    // Transaction pending for Prenote account
                    return LogLevel.FATAL_ERROR;
                case 176:
                    // Invalid Account Type
                    return LogLevel.FATAL_ERROR;
                case 177:
                    // Account Number Required
                    return LogLevel.FATAL_ERROR;
                case 178:
                    // Invalid Routing Number
                    return LogLevel.INFO;
                case 179:
                    // Client doesn't accept Credit Card Transactions
                    return LogLevel.INFO;
                case 180:
                    // Client is in test mode for Credit Cards
                    return LogLevel.FATAL_ERROR;
                case 181:
                    // Client is cancelled for Credit Cards
                    return LogLevel.FATAL_ERROR;
                case 182:
                    // Name on Credit Card is Required
                    return LogLevel.FATAL_ERROR;
                case 183:
                    // Invalid Expiration Date
                    return LogLevel.FATAL_ERROR;
                case 184:
                    // Complete Billing Address is Required
                    return LogLevel.FATAL_ERROR;
                case 196:
                    // Recurring Telephone Entry Transaction NOT Allowed
                    return LogLevel.FATAL_ERROR;
                case 198:
                    // Invalid State
                    return LogLevel.FATAL_ERROR;
                case 201:
                    // Frequency Required
                    return LogLevel.FATAL_ERROR;
                case 202:
                    // Account Cannot Be Deleted, Active Transaction Exists
                    return LogLevel.FATAL_ERROR;
                case 203:
                    // Client Does Not Accept ACH Transactions
                    return LogLevel.FATAL_ERROR;
                case 204:
                    // Duplicate Transaction
                    return LogLevel.WARNING;
                case 210:
                    // Recurring Credits NOT Allowed
                    return LogLevel.FATAL_ERROR;
                case 224:
                    // Credit Card Credits NOT Allowed - Must Be Refunded
                    return LogLevel.INFO;
                case 231:
                    // Customer Not Found For Client
                    return LogLevel.FATAL_ERROR;
                case 232:
                    // Invalid Account Number
                    return LogLevel.INFO;
                case 233:
                    // Invalid Country Code
                    return LogLevel.FATAL_ERROR;
                case 234:
                    // Transactions Are Not Allow From <COUNTRYCODE>
                    return LogLevel.INFO;
                case 286:
                    // Client not set up for International Credit Card
                    // Processing
                    return LogLevel.INFO;
                case 344:
                    // Phone Number Must be 10 Digits Long
                    return LogLevel.FATAL_ERROR;
                case 378:
                    // Invalid Loginkey
                    return LogLevel.FATAL_ERROR;
                case 379:
                    // Invalid RequestType
                    return LogLevel.FATAL_ERROR;
                case 380:
                    // Invalid SessionID
                    return LogLevel.FATAL_ERROR;
                case 381:
                    // Invalid ClientID for Session
                    return LogLevel.FATAL_ERROR;
                case 383:
                    // Internal Handler FATAL_ERROR, Contact Vanco
                    return LogLevel.FATAL_ERROR;
                case 393:
                    // IP Address Blocked
                    return LogLevel.FATAL_ERROR;
                case 395:
                    // Transactions cannot be processed on Weekends
                    return LogLevel.INFO;
                case 410:
                    // Credits Cannot Be WEB or TEL
                    return LogLevel.FATAL_ERROR;
                case 415:
                    // End date falls on a weekend and will not process. To
                    // process this transaction, change the end date to the
                    // next business day.
                    return LogLevel.INFO;
                case 416:
                    // Customer Cannot Be Edited By This User
                    return LogLevel.FATAL_ERROR;
                case 429:
                    // Transaction Cannot Be Edited By This User
                    return LogLevel.FATAL_ERROR;
                case 434:
                    // Credit Card Processor Error. An error was returned by
                    // the Credit Card Company. Description will include
                    // message returned from Credit Card Company.
                    return LogLevel.INFO;
                case 489:
                    // Customer ID Exceeds 15 Characters
                    return LogLevel.FATAL_ERROR;
                case 495:
                    // (field name) Field Contains Invalid Characters. Note:
                    // This error description will change depending on what
                    // field the invalid character(s) was/were in.
                    return LogLevel.INFO;
                case 496:
                    // (field name) Field Contains Too Many Characters. Note:
                    // This error description will change depending on what
                    // field's content length was too long.
                    return LogLevel.WARNING;
                case 500:
                    // Too Many Results, Please Narrow Search. Note: This error
                    // code will only be sent back for services that use search
                    // parameters
                    return LogLevel.FATAL_ERROR;
                case 501:
                    // Report Not Found
                    return LogLevel.FATAL_ERROR;
                default:
                    return LogLevel.FATAL_ERROR;
            }
        }

        private string myRequestUri;
    }
}
