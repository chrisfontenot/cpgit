﻿/*******************************************************************************
 *
 * Defines the class DatabaseLogProvider.
 *
 * File:        PaymentGateway/Library/DatabaseLogProvider.cs
 * Date:        Sat Nov  1 14:38:43 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;

namespace MMI.Payment
{
      /// <summary>
      /// Log listener that records log entries in the database.
      /// </summary>
    public class DatabaseLogProvider : DataClient, ILogProvider
    {
        /// <summary>
        /// Constructs a <see cref="DatabaseLogProvider"/> from a
        /// database connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public DatabaseLogProvider(string connectionString)
        {
            myConnection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Processes a log entry
        /// </summary>
        /// <param name="entry">The entry.</param>
        public void NewEntry(LogEntry entry)
        {
            if (myConnection.State != ConnectionState.Open) {
                try {
                    myConnection.Open();
                } catch (Exception e) {
                    throw new PaymentException(
                                  String.Format(
                                      "Failed connecting to data source " +
                                          "'{0}': {1}",
                                       SanitizeConnectionString(
                                           myConnection.ConnectionString
                                       ),
                                       e.Message
                                  ),
                                  Status.Code.DATABASE_ERROR,
                                  e
                              );
                }
            }

            try {

                // Create command
                SqlCommand command =
                    new SqlCommand("CreateLogMessage", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Create and attach parameters
                command.Parameters.Add(
                    new SqlParameter("@date", DateTime.Now.ToFileTimeUtc())
                );
                command.Parameters.Add(
                    new SqlParameter("@time", entry.TimestampString)
                );
                command.Parameters.Add(
                    new SqlParameter("@session", entry.SessionId)
                );
                command.Parameters.Add(
                    new SqlParameter("@level", entry.Level)
                );
                command.Parameters.Add(
                    new SqlParameter("@message", entry.Message)
                );

                // Execute command
                command.ExecuteNonQuery();

            } catch (Exception e) {
                throw new PaymentException(
                              e.Message, Status.Code.DATABASE_ERROR, e
                          );
            }
        }

        /// <summary>
        /// Closes the database connection.
        /// </summary>
        public void Dispose() { myConnection.Dispose(); }

        private SqlConnection  myConnection;
    }
}
