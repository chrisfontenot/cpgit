﻿/*******************************************************************************
 *
 * Defines the class Connection.
 *
 * File:        PaymentGateway/Library/Connection.cs
 * Date:        Mon Nov  3 22:48:35 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 *******************************************************************************
 * Amendment History
 *  
 * Who           Date               Changes
 *-----------------------------------------------------------------------------
 * MDH         11/26/2008           InitializeConnection Amended
 *                                  SqlConnection.ClearPool Call added
 *                                  getting error "transport-level error has occurred
 *                                  when sending" etc
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace MMI.Payment
{
      /// <summary>
      /// Encapsulated access to the payment gateway database and log.
      /// </summary>
    public class Connection : DataClient, IDisposable
    {
        /// <summary>
        /// Constructs a <see cref="Connection"/> from an instance of
        /// <see cref="IGatewayConfiguration"/>.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        public Connection(IGatewayConfiguration config) 
        {
            InitializeLog(config);
            //InitializeConnection(config);
        }

        /// <summary>
        /// Returns the log.
        /// </summary>
        /// <value>The log.</value>
        public MMI.Payment.Log Log 
        { 
            get { return myLog; }
        }

        /// <summary>
        /// Returns the SQL connection.
        /// </summary>
        /// <value>The SQL connection.</value>
        public System.Data.SqlClient.SqlConnection SqlConnection 
        { 
            get { return mySqlConnection; }
        }

        /// <summary>
        /// Frees resources used by the database connection and log.
        /// </summary>
        public void Dispose()
        {
            if (mySqlConnection != null)
                try { mySqlConnection.Dispose(); } catch (Exception) { }

            try { myLog.Dispose(); } catch (Exception) { }
        }

        /// <summary>
        /// Utility method for creating a log from a gateway configuration; 
        /// used by the unit tests.</summary>
        /// <param name="config">The gateway configuration.</param>
        /// <returns>A log</returns>
        public static Log CreateLog(IGatewayConfiguration config)
        {
            Log log = new Log();

            // Handle system event log provider
            string key = "log.event_log.level";
            if (config.ContainsKey(key)) {
                LogLevel level = ParseLogLevel(config[key]);
                if (level != LogLevel.NONE) {
                    key = "log.event_log.source";
                    log.RegisterProvider(
                        config.ContainsKey(key) ?
                            new SystemLogProvider(config[key]) :
                            new SystemLogProvider(),
                        level
                    );
                }
            }

            // Handle file log provider
            key = "log.file.level";
            if (config.ContainsKey(key)) {
                LogLevel level = ParseLogLevel(config[key]);
                if (level != LogLevel.NONE) {
                    string root = GetSetting(config, "log.file.root_directory");
                    key = "log.file.path_format";
                    log.RegisterProvider(
                        config.ContainsKey(key) ?
                            new FileLogProvider(root, config[key]) :
                            new FileLogProvider(root),
                        level
                    );
                }
            }

            // Handle database log provider
            key = "log.database.level";
            if (config.ContainsKey(key)) {
                LogLevel level = ParseLogLevel(config[key]);
                if (level != LogLevel.NONE) {
                    string connectionString = 
                        GetSetting(config, "database.connection_string");
                    log.RegisterProvider(
                        new DatabaseLogProvider(connectionString),
                        level
                    );
                }
            }

            // Handle SMTP log provider
            key = "log.smtp.level";
            if (config.ContainsKey(key)) {
                LogLevel level = ParseLogLevel(config[key]);
                if (level != LogLevel.NONE) {
                    string to = GetSetting(config, "log.smtp.to");
                    string from = GetSetting(config, "log.smtp.from");
                    string username = 
                        GetSetting(config, "log.smtp.username", false);
                    string password = 
                        GetSetting(config, "log.smtp.password", false);
                    string host = GetSetting(config, "log.smtp.host", false);
                    string port = GetSetting(config, "log.smtp.port", false);
                    int portNum = -1;
                    if (port != null) {
                        try {
                            portNum = Int32.Parse(port);
                        } catch (Exception e) {
                            throw new PaymentException(
                                          "Invalid port: " + port,
                                           Status.Code.CONFIGURATION_ERROR,
                                           e
                                      );
                        }
                    }
                    log.RegisterProvider(
                        new SmtpLogProvider(
                                from, to, username, password, host, portNum
                            ),
                        level
                    );
                }
            }

            // Handle custom providers
            foreach (KeyValuePair<string, string> param in config) {
                key = param.Key;
                if ( key.Substring(0, 4) == "log." && 
                     key.Substring(key.Length - 6) == ".class" )
                {
                    string name = key.Substring(4, key.Length - 10);
                    var pair = NewCustomProvider(config, name);
                    if (pair.Second != LogLevel.NONE)
                        log.RegisterProvider(pair.First, pair.Second);
                }
            }

            return log;
        }

        /// <summary>
        /// Initializes the log.
        /// </summary>
        /// <param name="config">The config.</param>
        private void InitializeLog(IGatewayConfiguration config)
        {
            myLog = CreateLog(config);
        }

        /// <summary>
        /// Creates a custom log provider, via reflection, based on the 
        /// given gateway configuration.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        /// <param name="name">The provider name.</param>
        /// <returns>A pair consiting of a log provider and
        /// the corresponding log level.</returns>
        private static Pair<ILogProvider, LogLevel> NewCustomProvider(
            IGatewayConfiguration config, string name)
        {
            StringValidator.Validate(name, "custom log provider name", 1, 
                Int32.MaxValue, Status.Code.CONFIGURATION_ERROR);
            string className = GetSetting(config, "log." + name + ".class");
            LogLevel level = 
                ParseLogLevel(GetSetting(config, "log." + name + ".level"));
            if (level == LogLevel.NONE)
                return new Pair<ILogProvider, LogLevel>(null, LogLevel.NONE);
            string providerParams = 
                GetSetting(config, "log." + name + ".params", false);
            try {

                // Create an instance of className
                Assembly asm = Assembly.Load("MMI.Payment");
                Type classType = null;
                try {
                    classType = asm.GetType(className, true);
                } catch (Exception e) {
                    throw new PaymentException(
                                  String.Format(
                                      "Invalid class name '{0}' implementing " +
                                          "log provider '{1}': {2}",
                                      className, name, e.Message
                                  ),
                                  Status.Code.CONFIGURATION_ERROR
                              );
                }
                Type iface = classType.GetInterface("MMI.Payment.ILogProvider");
                if (iface == null || !iface.Equals(typeof(ILogProvider)))
                    throw new PaymentException(
                                  String.Format(
                                      "Class '{0}' implementing log provider " +
                                          "'{1}' does not derived from {2}",
                                      className, 
                                      name,
                                      typeof(ILogProvider).ToString()
                                  ),
                                  Status.Code.CONFIGURATION_ERROR
                              );
                Type[] argList;
                
                if (name == "MMI")
                {
                    argList = new Type[1] { typeof(IGatewayConfiguration) };
                }
                else
                {
                argList = providerParams != null ?
                    new Type[1] { typeof(string) } :
                    new Type[0] { };
                }

                ConstructorInfo ctor = null;
                try {
                    ctor = classType.GetConstructor(argList);
                } catch (Exception e) {
                    throw new PaymentException(
                                  String.Format(
                                      "No suitable constructor for class " +
                                          "'{0}' implementing log provider " + 
                                          "'{1}': {2}",
                                      className, name, e.Message
                                  ),
                                  Status.Code.CONFIGURATION_ERROR
                              );
                }

                object[] args;

                if (name == "MMI")
                {
                    args = new object[1] {config};
                }
                else
                {
                    args = providerParams != null ?
                        new object[1] { providerParams } :
                        new object[0] { };
                }
                return new Pair<ILogProvider, LogLevel>(
                               (ILogProvider) ctor.Invoke(args),
                               level
                           );
            } catch (PaymentException e) {
                throw e;
            } catch (Exception e) {
                throw new PaymentException(
                              String.Format(
                                  "Failed registering custom provider '{0}': " +
                                      "{1}",
                                  name,
                                  e.Message
                              ),
                              Status.Code.CONFIGURATION_ERROR
                          );
            }
        }

        /// <summary>
        /// Initializes the database connection.
        /// </summary>
        /// <param name="config">The config.</param>
        private void InitializeConnection(IGatewayConfiguration config)
        {
            string key = "database.connection_string";
            if (!config.ContainsKey(key))
                throw NewMissingKeyException(key);
            Repeater repeater = new Repeater(myLog);

            mySqlConnection = new SqlConnection(config[key]);

            // getting error "transport-level error has occurred when sending" etc
            // call to ClearPool added MDHinds  11/26/2008

            SqlConnection.ClearPool(mySqlConnection);

            repeater.Repeat(
                () => { mySqlConnection.Open(); return null; },
                String.Format(
                    "Failed connecting to data source '{0}'",
                    SanitizeConnectionString(config[key])
                ),
                Status.Code.DATABASE_ERROR
            );
        }

        /// <summary>
        /// Parses a string representation of a log level.
        /// </summary>
        /// <param name="level">The string.</param>
        /// <returns>A log level.</returns>
        private static LogLevel ParseLogLevel(string level)
        {
            return (LogLevel) Enum.Parse(typeof(LogLevel), level, true);
        }

        /// <summary>
        /// Helper method to fetch a setting from a gateway configuration.
        /// </summary>
        /// <param name="config">The gateway configuration.</param>
        /// <param name="key">The setting name.</param>
        /// <returns>The value of the setting.</returns>
        private static string GetSetting(IGatewayConfiguration config, 
            string key)
        {
            return GetSetting(config, key, true);
        }

        /// <summary>
        /// Helper method to fetch a setting from a gateway configuration.
        /// </summary>
        /// <param name="config">The config.</param>
        /// <param name="key">The setting name.</param>
        /// <param name="throwOnError"><c>true</c> if an exception should
        /// be thrown when a setting is not found.</param>
        /// <returns>The value of the setting.</returns>
        private static string GetSetting(IGatewayConfiguration config, 
            string key, bool throwOnError)
        {
            if (config.ContainsKey(key)) {
                return config[key];
            } else if (throwOnError) {
                throw NewMissingKeyException(key);
            } else {
                return null;
            }
        }

        /// <summary>
        /// Helper method to throw an exception.
        /// </summary>
        /// <param name="key">The missing setting.</param>
        /// <returns>The exception.</returns>
        private static PaymentException NewMissingKeyException(string key)
        {
            return new PaymentException(
                           String.Format(
                               "Missing value for configuration property '{0}'",
                               key
                           ),
                           Status.Code.CONFIGURATION_ERROR
                       );
        }

        /// <summary>
        /// Logs a fatal error.
        /// </summary>
        /// <param name="message">The message.</param>
        private void LogFatalError(string message)
        {
            Log.Stream stream;
            if ( myLog != null && 
                 (stream = myLog.GetStream(LogLevel.FATAL_ERROR)) )
            {
                stream.Write(MethodBase.GetCurrentMethod(), message);
            }
        }

        private SqlConnection  mySqlConnection;
        private Log            myLog;
    }
}
