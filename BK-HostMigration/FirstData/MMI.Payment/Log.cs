﻿/*******************************************************************************
 *
 * Defines classes and interfaces for logging.
 *
 * File:        PaymentGateway/Library/Log.cs
 * Date:        Tue Oct 28 16:50:08 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Reflection;

namespace MMI.Payment
{
      /// <summary>
      /// Represents one of the six log level.
      /// </summary>
    [Serializable]
    public enum LogLevel 
    {
        /// <summary>
        /// Used to indicate that no logging should occur
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Indicates that processing has failed and cannot continue.
        /// </summary>
        FATAL_ERROR = 1,
        /// <summary>
        /// Indicates that processing has failed, but may continue.
        /// </summary>
        ERROR       = 2,
        /// <summary>
        /// Indicates that an unusual condition has occurred.
        /// </summary>
        WARNING     = 3,
        /// <summary>
        /// Used for informational messages.
        /// </summary>
        INFO        = 4,
        /// <summary>
        /// Used for detailed informational messaged.
        /// </summary>
        VERBOSE     = 5,
        /// <summary>
        /// Used for debugging.
        /// </summary>
        DEBUG       = 6
    }

    /// <summary>
    /// Represents a component that processes log entries and optionally
    /// records or transmits them
    /// </summary>
    public interface ILogProvider : IDisposable
    {
        /// <summary>
        /// Processes a log entry.
        /// </summary>
        /// <param name="entry">The log entry.</param>
        void NewEntry(LogEntry entry);
    }

    /// <summary>
    /// Encapsulates a single log message
    /// </summary>
    public class LogEntry
    {
        public static readonly string TimestampFormat = 
            "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Constructs a <see cref="LogEntry"/>.
        /// </summary>
        /// <param name="level">The log level.</param>
        /// <param name="message">The message.</param>
        public LogEntry(MethodBase method, LogLevel level, string message, Exception e)
        {
            myTimestamp = DateTime.Now;
            myLevel = level;
            myMessage = message;
            myMethod = method;
            myException = e;
        }

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public DateTime Timestamp
        {
            get { return myTimestamp; }
        }

        /// <summary>
        /// Gets the timestamp as a string.
        /// </summary>
        /// <value>The timestamp string.</value>
        public string TimestampString
        {
            get { return myTimestamp.ToString(TimestampFormat); }
        }

        /// <summary>
        /// Gets the session ID.
        /// </summary>
        /// <value>The session id.</value>
        public Guid SessionId
        {
            get { return Session.Id; }
        }

        /// <summary>
        /// Gets the log level.
        /// </summary>
        /// <value>The level.</value>
        public LogLevel Level
        {
            get { return myLevel; }
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message
        {
            get { return myMessage; }
        }

        /// <summary>
        /// Gets the method.
        /// </summary>
        /// <value>The method.</value>
        public MethodBase Method
        {
            get { return myMethod; }
        }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception
        {
            get { return myException; }
        }

        public override string ToString()
        {
            return String.Format(
                       "[Timestamp: {0}; Level: {1}; Session ID: {2};" + 
                           "Message: {3}]",
                       TimestampString, 
                       Log.PrintLevel(Level),
                       SessionId,
                       Message
                   );
        }

        public string Format()
        {
            return String.Format(
                       "Time: {0}\nLevel: {1}\nSession ID: {2}\n\n{3}",
                       TimestampString,
                       Log.PrintLevel(Level),
                       SessionId,
                       Message
                   );
        }

        private DateTime  myTimestamp;
        private LogLevel  myLevel;
        private string    myMessage;
        private MethodBase myMethod;
        private Exception myException;
    }

    /// <summary>
    /// Represents a component which accepts messages and dispatches them
    /// to providers for additional processing.
    /// </summary>
    public class Log : IDisposable
    {
        /// <summary>
        /// Construct a <see cref="Log"/>.
        /// </summary>
        public Log()
        {
            myMaxLevel = 0;
            myStreams = new Dictionary<LogLevel, Stream>();
            myProviders = new List<Provider>();
        }

        /// <summary>
        /// Represents a component for writing messages to the log.
        /// This abstraction helps prevent the costly construction of
        /// log messages which will never be sent to any provider, 
        /// and prevents the programmer from overriding the log
        /// levels specified in the gateway configuration.
        /// </summary>
        public class Stream 
        {
            /// <summary>
            /// Constructs a <see cref="Stream"/>.
            /// </summary>
            /// <param name="log">The log.</param>
            /// <param name="level">The log level.</param>
            public Stream(Log log, LogLevel level)
            {
                myLog = log;
                myLevel = level;
            }

            /// <summary>
            /// Writes the specified message to the log.
            /// </summary>
            /// <param name="message">The message.</param>
            /// <returns>A timestamp</returns>
            public DateTime Write(MethodBase method, string message)
            {
                LogEntry entry = 
                    new LogEntry(method, myLevel, message, null);
                myLog.NewEntry(entry);
                return entry.Timestamp;
            }

            /// <summary>
            /// Writes the specified message to the log.
            /// </summary>
            /// <param name="message">The message.</param>
            /// <returns>A timestamp</returns>
            public DateTime Write(MethodBase method, string message, Exception e)
            {
                LogEntry entry =
                    new LogEntry(method, myLevel, message, e);
                myLog.NewEntry(entry);
                return entry.Timestamp;
            }

            public static implicit operator bool(Stream stream)
            {
                return stream != null;
            }
            private Log       myLog;
            private LogLevel  myLevel;
        }

        /// <summary>
        /// Gets the stream, if any, associated with the given log level.
        /// </summary>
        /// <param name="level">The log level.</param>
        /// <returns>The stream.</returns>
        public Stream GetStream(LogLevel level)
        {
            return myStreams.ContainsKey(level) ?
                myStreams[level] :
                null;
        }

        /// <summary>
        /// Registers the given provider.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="level">The log level associated with the
        /// provider.</param>
        public void RegisterProvider(ILogProvider provider, LogLevel level)
        {
            switch (level)
            {
                case LogLevel.FATAL_ERROR:
                case LogLevel.ERROR:
                case LogLevel.WARNING:
                case LogLevel.INFO:
                case LogLevel.VERBOSE:
                case LogLevel.DEBUG:
                    break;
                case LogLevel.NONE:
                default:
                    throw new PaymentException(
                                  String.Format(
                                      "Invalid log level: {0}",
                                      level.ToString()
                                  ),
                                  Status.Code.LOG_ERROR
                              );
            }
            myProviders.Add(new Provider(provider, level));
            for (LogLevel n = level; n > myMaxLevel; --n)
                myStreams[n] = new Stream(this, n);
            myMaxLevel = myMaxLevel < level ? level : myMaxLevel;
        }

        /// <summary>
        /// Frees the resources used by all registered providers.
        /// </summary>
        public void Dispose()
        {
            foreach (Provider provider in myProviders) {
                try {
                    provider.myProvider.Dispose();
                } catch (Exception) { }
            }
            myMaxLevel = 0;
            myProviders.Clear();
            myStreams.Clear();
        }

        /// <summary>
        /// Returns a log level as a human-readable string.
        /// </summary>
        /// <param name="level">The log level.</param>
        /// <returns>The human-readable string.</returns>
        public static string PrintLevel(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.FATAL_ERROR:
                    return "Fatal Error";
                case LogLevel.ERROR:
                    return "Error";
                case LogLevel.WARNING:
                    return "Warning";
                case LogLevel.INFO:
                    return "Info";
                case LogLevel.VERBOSE:
                    return "Verbose";
                case LogLevel.DEBUG:
                    return "Debug";
                default:
                    return "Unknown";
            }
        }

        /// <summary>
        /// Processes a log entry.
        /// </summary>
        /// <param name="entry">The entry.</param>
        private void NewEntry(LogEntry entry)
        {
            foreach (Provider provider in myProviders) {
                if (provider.myBroken)
                    continue;
                if (entry.Level <= provider.myLevel) {
                    try {
                        provider.myProvider.NewEntry(entry);
                    } catch (Exception e) {

                        // Mark provider as permanently broken, to
                        // avoid an infinite loop when a log-related
                        // error is logged
                        provider.myBroken = true;
                        Stream stream;
                        if (stream = GetStream(LogLevel.ERROR)) {
                            string msg = 
                                String.Format(
                                    "Log provider '{0}' failed: {1}",
                                    provider.myProvider.GetType(),
                                    e.Message
                                );
                            stream.Write(MethodBase.GetCurrentMethod(), msg, e);
                        }
                    }
                }
            }
        }

        private class Provider 
        {
            public Provider(ILogProvider provider, LogLevel level)
            {
                myProvider = provider;
                myLevel = level;
                myBroken = false;
            }

            public ILogProvider myProvider;
            public LogLevel     myLevel;
            public bool         myBroken;
        }

        private LogLevel                      myMaxLevel;
        private Dictionary<LogLevel, Stream>  myStreams;
        private List<Provider>                myProviders;
    }
}
