<?php 

/**
 * Defines the class CSharpEncoder.
 *
 * File:        PaymentGateway/Library/CSharpEncoder.php
 * Date:        Tue May 12 22:55:22 MDT 2009
 * Notice:      This document contains confidential information
 *              and trade secrets
 * 
 * @copyright   2009 Money Management International
 * @author      CodeRage
 * @license     All rights reserved
 * @package     Mmi.Payment
 */

/**
 * Encodes PHP objects as C# expressions. It is assumed that all expressions
 * will be evaluated in the scope of using declarations for the namespaces
 * System and Mmi.Payment.
 * 
 * Used for testing, but included in the Library directory so that relative 
 * paths reference will resolve similarly in when the source files are in their
 * original locations and when they are copied to the deployment directory.
 * 
 * @package Mmi.Payment
 */
class CSharpEncoder {
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.GatewayStatus equivalent to the given GatewayStatus.
     *
     * @param GatewayStatus $status
     * @return string
     */
    static function EncodeGatewayStatus(GatewayStatus $status)
    {
        $statusCode = 
            "(Status.Code)Enum.ToObject(typeof(Status.Code)," .
            "$status->StatusCode)";
        list($errorMessage, $transactionId, $referenceCode) =
            self::EncodeScalar(
                $status->ErrorMessage,
                $status->TransactionId,
                $status->ReferenceCode
            );
        $info = self::EncodePaymentInfo($status->PaymentInfo);
        $schedule = self::EncodeSchedule($status->Schedule);
        return $status->ErrorMessage ?
            "new GatewayStatus($statusCode,$transactionId,$errorMessage)" :
            "new GatewayStatus($statusCode,$transactionId,$referenceCode," .
                "$info,$schedule)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.PaymentMethod equivalent to the given PaymentMethod.
     *
     * @param PaymentMethod $method
     * @return string
     */
    static function EncodePaymentInfo($info)
    {
        if ($info === null)
            return 'null';
        $customer = self::EncodeCustomer($info->Customer);
        $amount = $info->Amount !== null ?
            self::EncodeScalar($info->Amount) :
            '-1M';
        $method = self::EncodePaymentMethod($info->PaymentMethod);
        return "new PaymentInfo($customer,$amount,$method)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.Address equivalent to the given Customer.
     *
     * @param Address $address
     * @return string
     */
    static function EncodeAddress($address)
    {
        if ($address === null)
            return 'null';
        list($street1, $street2, $city, $state, $zip) =
            self::EncodeScalar(
                $address->Street1, $address->Street2,
                $address->City, $address->State, $address->Zip
            );
        return "new Address($street1,$street2,$city,$state,$zip)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.Customer equivalent to the given Customer.
     *
     * @param Customer $customer
     * @return string
     */
    static function EncodeCustomer($customer)
    {
        if ($customer === null)
            return 'null';
        list( $first, $last, $id, $street1, $street2, 
              $city, $state, $zip, $phone ) =
            self::EncodeScalar(
                $customer->FirstName, $customer->LastName, $customer->Id, 
                $customer->Street1, $customer->Street2, $customer->City, 
                $customer->State, $customer->Zip, $customer->Phone
            );
        return "new Customer($first,$last,$id,$street1,$street2,$city,$state," .
               "$zip,$phone)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.PaymentMethod equivalent to the given PaymentMethod.
     *
     * @param PaymentMethod $method
     * @return string
     */
    static function EncodePaymentMethod($method)
    {
        if ($method === null)
            return 'null';
        switch ($method->Type) {
        case 'CHECKING':
        case 'SAVINGS':
            return self::EncodeBankAccount($method);
        case 'DEBIT':
        case 'CREDIT':
            return self::EncodeCard($method);
        case 'TOKEN':
            return self::EncodePaymentToken($method);
        case 'CUSTOM':
            return self::EncodeCustomerPaymentMethod($method);
        default:
            throw new Exception("Invalid account type: $method->Type");
        }
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.BankAccount equivalent to the given BankAccount.
     *
     * @param BankAccount $account
     * @return string
     */
    static function EncodeBankAccount(BankAccount $account)
    {
        list($accountNumber, $routingNumber, $accountHolder) =
            self::EncodeScalar(
                $account->AccountNumber,
                $account->RoutingNumber,
                $account->AccountHolder
            );
        $billing = self::EncodeAddress($account->BillingAddress);
        $params = self::EncodeParameterList($account);
        return "new BankAccount(AccountType.$account->Type,$accountNumber," .
               "$routingNumber,$accountHolder,$billing,$params)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.Card equivalent to the given Card.
     *
     * @param Card $card
     * @return string
     */
    static function EncodeCard(Card $card)
    {
        list($cardHolder, $cardNumber, $expMonth, $expYear, $verificationCode) =
            self::EncodeScalar(
                $card->CardHolder,
                $card->CardNumber,
                $card->ExpirationMonth,
                $card->ExpirationYear,
                $card->VerificationCode
            );
        $billing = self::EncodeAddress($card->BillingAddress);
        $params = self::EncodeParameterList($card);
        $class = $card->Type == 'DEBIT' ? 'DebitCard' : 'CreditCard';
        return "new $class($cardHolder,$cardNumber,$expMonth,$expYear," .
               "$verificationCode,$billing,$params)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.PaymentToken equivalent to the given PaymentToken.
     *
     * @param PaymentToken $token
     * @return string
     */
    static function EncodePaymentToken(PaymentToken $token)
    {
        $id = self::EncodeScalar($token->Id);
        $params = self::EncodeParameterList($token);
        return "new PaymentToken($id,$params)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.CustomPaymentMethod equivalent to the given 
     * CustomPaymentMethod.
     *
     * @param CustomPaymentMethod $method
     * @return string
     */
    static function EncodeCustomPaymentMethod(CustomPaymentMethod $method)
    {
        $name = self::EncodeScalar($method->Name);
        $params = self::EncodeParameterList($method);
        return "new CustomPaymentMethod($name,$params)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.SortedList<PaymentMethod.Param> equivalent to the list
     * of parameters of the given PaymentMethod.
     *
     * @param PaymentMethod $method
     * @return string
     */
    static function EncodeParameterList(PaymentMethod $method)
    {
        $callback = array('CSharpEncoder', 'EncodeParameter');
        return 'new SortedList<PaymentMethod.Param>(' .
               'new PaymentMethod.Param[] {' .
               join(',', array_map($callback, $method->Parameters)) .
               '})';
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.SortedList<PaymentMethod.Param> equivalent to the list
     * of parameters of the given PaymentMethod.
     *
     * @param PaymentMethod $method
     * @return string
     */
    static function EncodeParameter(PaymentMethodParam $param)
    {
        list($name, $value) = self::EncodeScalar($param->Name, $param->Value);
        return "new PaymentMethod.Param($name,$value)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.Schedule equivalent to the given Schedule.
     *
     * @param Schedule $schedule
     * @return string
     */
    static function EncodeSchedule($schedule)
    {
        if ($schedule === null)
            return 'null';
        $begin = self::EncodeDate($schedule->Begin);
        $end = self::EncodeDate($schedule->End);
		$rules = $schedule->Rules !== null ? 
            $schedule->Rules :
            array();
        $callback = array('CSharpEncoder', 'EncodeScheduleRule');
        $rules = 
            'new Schedule.Rule[] { ' .
            join(',', array_map($callback, $rules)) . '}';
        return "new Schedule($begin,$end,$rules)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * System.DateTime equivalent to the given DateTime object representing
     * a date.
     *
     * @param DateTime $date
     * @return string
     */
    static function EncodeDate(DateTime $date)
    {
        list($year, $month, $day) = split('-', $date->format('Y-m-d'));
        return "new DateTime($year,$month,$day)";
    }
    
    /**
     * Returns a C# expression evaluating to an instance of 
     * Mmi.Payment.Schedule.Rule equivalent to the given ScheduleRules.
     *
     * @param Schedule $schedule
     * @return string
     */
    static function EncodeScheduleRule(ScheduleRule $rule)
    {
        $pattern = self::EncodeScalar($rule->Pattern);
        return "new Schedule.Rule(Schedule.Facet.$rule->Facet," .
               "new Schedule.Pattern($pattern))";
    }
    
    /**
     * Returns a C# expression evaluating to a string, int, or boolean
     * equivalent to the given scalar value.
     * 
     * Several arguments may also be passed, in which case an array of 
     * expressions
     *
     * @return string
     */
    static function EncodeScalar()
    {
        $args = func_get_args();
        if (sizeof($args) > 1) {
            $callback = array('CSharpEncoder', 'EncodeScalar');
            return array_map($callback, $args);
        } else {
            $value = $args[0];
            return is_null($value) ?
                'null' :
                ( is_bool($value) ?
                      ($value ? 'true' : 'false') :
                      ( is_string($value) ? 
                            '"' . addcslashes($value, "\"\\\n") . '"' :
                            (string) $value ) );
        }
    }
}

?>
