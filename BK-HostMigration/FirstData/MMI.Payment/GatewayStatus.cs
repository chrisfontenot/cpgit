﻿/*******************************************************************************
 *
 * Defines the class GatewayStatus.cs.
 *
 * File:        PaymentGateway/Library/GatewayStatus.cs
 * Date:        Mon Nov  3 22:48:35 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;
    using ReadOnlyStringMap = Dictionary<string, string>;

      /// <summary>
      /// Represents the return value of a gateway transaction. 
      /// </summary>
    [Serializable]
    public class GatewayStatus
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public GatewayStatus()
            : this(0, "-1", null, null, null, "")
            { }

        /// <summary>
        /// Constructs a <see cref="GatewayStatus"/> from a status code,
        /// a gateway transaction ID, payment information, and a 
        /// payment schedule.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="referenceCode">The provider reference code, if
        /// any.</param>
        /// <param name="info">The payment info, if any.</param>
        /// <param name="schedule">The payment schedule, if any.</param>
        public GatewayStatus(
                   Status.Code statusCode, string transactionId,
                   string referenceCode, PaymentInfo info, Schedule schedule)
            : this(statusCode, transactionId, referenceCode, info, schedule, "")
            { }

        /// <summary>
        /// Constructs a <see cref="GatewayStatus"/> from a status code,
        /// a gateway transaction ID, and an error message.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="errorMessage">The error message.</param>
        public GatewayStatus(
                   Status.Code statusCode, string transactionId,
                   string errorMessage)
            : this(statusCode, transactionId, null, null, null, errorMessage)
            { }

        /// <summary>
        /// Constructs a <see cref="GatewayStatus"/> from a status code,
        /// a gateway transaction ID, payment information, a payment
        /// schedule, and an error message.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="referenceCode">The provider reference code, if
        /// any.</param>
        /// <param name="info">The payment info, if any.</param>
        /// <param name="schedule">The payment schedule, if any.</param>
        /// <param name="errorMessage">The error message.</param>
        private GatewayStatus(Status.Code statusCode, string transactionId,
            string referenceCode, PaymentInfo info, Schedule schedule, 
            string errorMessage)
        {
            StatusCode = statusCode;
            TransactionId = transactionId;
            ReferenceCode = referenceCode;
            PaymentInfo = info;
            Schedule = schedule;
            ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code</value>
        public Status.Code StatusCode
        {
            get { return myStatusCode; }
            set { myStatusCode = value; }
        }

        /// <summary>
        /// Gets or sets the transaction ID.
        /// </summary>
        /// <value>The transaction id</value>
        public string TransactionId
        {
            get { return myTransactionId; }
            set { myTransactionId = value; }
        }

        /// <summary>
        /// Gets or sets the provider reference code, if any.
        /// </summary>
        /// <value>The reference code</value>
        public string ReferenceCode
        {
            get { return myReferenceCode; }
            set { myReferenceCode = value; }
        }

        /// <summary>
        /// Gets or sets the payment info.
        /// </summary>
        /// <value>The payment info</value>
        public PaymentInfo PaymentInfo
        {
            get { return myPaymentInfo; }
            set { myPaymentInfo = value; }
        }

        /// <summary>
        /// Gets or sets the payment schedule.
        /// </summary>
        /// <value>The schedule</value>
        public Schedule Schedule
        {
            get { return mySchedule; }
            set { mySchedule = value; }
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message</value>
        public string ErrorMessage
        {
            get { return myErrorMessage; }
            set { myErrorMessage = value; }
        }

        public static bool operator==(GatewayStatus lhs, GatewayStatus rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(GatewayStatus lhs, GatewayStatus rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            GatewayStatus status = (GatewayStatus) other;
                return StatusCode == status.StatusCode &&
                       TransactionId == status.TransactionId &&
                       ReferenceCode == status.ReferenceCode &&
                       PaymentInfo == status.PaymentInfo &&
                       Schedule == status.Schedule &&
                       ErrorMessage == status.ErrorMessage;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[StatusCode: {0}; TransactionId: {1}; " +
                           "ReferenceCode: {2}; PaymentInfo: {3}; " +
                           "Schedule: {4}; ErrorMessage: {5}; ",
                       Status.Message(StatusCode),
                       TransactionId, ReferenceCode, PaymentInfo,
                       Schedule, ErrorMessage
                   );
        }

        private Status.Code  myStatusCode;
        private string       myTransactionId;
        private string       myReferenceCode;
        private PaymentInfo  myPaymentInfo;
        private Schedule     mySchedule;
        private string       myErrorMessage;
    }
}
