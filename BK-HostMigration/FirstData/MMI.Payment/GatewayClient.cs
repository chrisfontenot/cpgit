﻿/*******************************************************************************
 *
 * Defines the class Client.
 *
 * File:        PaymentGateway/Library/Client.cs
 * Date:        Sat Nov  1 13:33:01 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace MMI.Payment
{

      /// <summary>
      /// Client component used to connect to the payment gateway.
      /// </summary>
    [WebServiceBindingAttribute(
        Name="ServerSoap", Namespace="http://moneymanagement.org/payment-gateway")]
    [SoapRpcService]
    [XmlInclude(typeof(CreditCard))]
    [XmlInclude(typeof(DebitCard))]
    [XmlInclude(typeof(BankAccount))]
    [XmlInclude(typeof(PaymentToken))]
    [XmlInclude(typeof(CustomPaymentMethod))]
    public class GatewayClient : System.Web.Services.Protocols.SoapHttpClientProtocol 
    {
        /// <summary>
        /// Constructs a <see cref="GatewayClient"/>.
        /// </summary>
        /// <param name="url">The URL.</param>
        public GatewayClient(string url) { Url = url; }

        /// <summary>
        /// Processes a Vanco debit card transaction.
        /// </summary>
        /// <param name="accountName">Name of the Vanco account.</param>
        /// <param name="customer">The customer.</param>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="card">The debit card.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The status of the transaction.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/VancoProcessDebit", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus VancoProcessDebit(string accountName, 
            Customer customer, string amount, DebitCard card, 
            string memo) 
        {
            object[] args = 
                new object[] { accountName, customer, amount, card, memo };
            object[] results = Invoke("VancoProcessDebit", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type SALE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessSale", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessSale(string provider, string accountName, 
            PaymentInfo info, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, info, memo };
            object[] results = Invoke("ProcessSale", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type AUTHORIZATION.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessAuthorization", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessAuthorization(string provider, 
            string accountName, PaymentInfo info, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, info, memo };
            object[] results = Invoke("ProcessAuthorization", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type CREDIT.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessCredit", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCredit(string provider, string accountName, 
            PaymentInfo info, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, info, memo };
            object[] results = Invoke("ProcessCredit", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type CAPTURE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction of type CAPTURE to which the current transaction 
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessCapture", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCapture(string provider, string accountName, 
            string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessCapture", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type RETURN.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">An instance of <see cref="PaymentInfo"/> 
        /// indicating the amount to be returned.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessReturn", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessReturn(string provider, string accountName, 
            PaymentInfo info, string referenceCode, string memo)
        {
            object[] args = 
                new object[] { 
                        provider, accountName, info, referenceCode, memo 
                    };
            object[] results = Invoke("ProcessReturn", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type VOID.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction of type SALE or CAPTURE to which the current transaction
        /// relates.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessVoid", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessVoid(string provider, string accountName, 
            string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessVoid", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type QUERY.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction whose status is to be queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessQuery", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQuery(string provider, string accountName, 
            string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessQuery", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type CREATE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information to be registered.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessCreateInfo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCreateInfo(string provider, 
            string accountName, PaymentInfo info, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, info, memo };
            object[] results = Invoke("ProcessCreateInfo", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type QUERY_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessQueryInfo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQueryInfo(string provider, 
            string accountName, string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessQueryInfo", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The new payment information.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// updated.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessUpdateInfo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessUpdateInfo(string provider, 
            string accountName, PaymentInfo info, string referenceCode, 
            string memo)
        {
            object[] args = 
                new object[] { 
                        provider, accountName, info, referenceCode, memo
                    };
            object[] results = Invoke("ProcessUpdateInfo", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type DELETE_INFO.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that registered the payment information to be 
        /// deleted.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessDeleteInfo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessDeleteInfo(string provider, 
            string accountName, string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessDeleteInfo", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type CREATE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The payment information.</param>
        /// <param name="schedule">The payment schedule.</param>
        /// <param name="memo">The descriptive message to be recorded with
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessCreateSchedule", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessCreateSchedule(string provider, 
            string accountName, PaymentInfo info, Schedule schedule,
            string memo)
        {
            object[] args = 
                new object[] { provider, accountName, info, schedule, memo };
            object[] results = Invoke("ProcessCreateSchedule", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type QUERY_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction that established the payment schedule to be 
        /// queried.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessQuerySchedule", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessQuerySchedule(string provider, 
            string accountName, string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessQuerySchedule", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type UPDATE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="info">The new payment information.</param>
        /// <param name="schedule">The new payment schedule.</param>
        /// <param name="referenceCode">The provider reference code of the prior 
        /// transaction that established the payment schedule to be 
        /// updated.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessUpdateSchedule", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessUpdateSchedule(string provider, 
            string accountName, PaymentInfo info, Schedule schedule, 
            string referenceCode, string memo)
        {
            object[] args = 
                new object[] {
                        provider, accountName, info, schedule, 
                        referenceCode, memo 
                    };
            object[] results = Invoke("ProcessUpdateSchedule", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Processes a transaction of type DELETE_SCHEDULE.
        /// </summary>
        /// <param name="provider">A provider name.</param>
        /// <param name="accountName">The name of the provider account.</param>
        /// <param name="referenceCode">The provider reference code of the prior
        /// transaction that established the payment schedule to be 
        /// deleted.</param>
        /// <param name="memo">The descriptive message to be recorded with 
        /// the transaction in the database.</param>
        /// <returns>The transaction status.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/ProcessDeleteSchedule", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public GatewayStatus ProcessDeleteSchedule(string provider, 
            string accountName, string referenceCode, string memo)
        {
            object[] args = 
                new object[] { provider, accountName, referenceCode, memo };
            object[] results = Invoke("ProcessDeleteSchedule", args);
            return (GatewayStatus) results[0];
        }

        /// <summary>
        /// Echoes the specified message, and writes it to the payment gateway log; 
        /// can be used for testing a production server.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The log level.</param>
        /// <returns>The message.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/Echo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public string Echo(string message, LogLevel level) 
        {
            object[] results = Invoke("Echo", new object[] {message, level});
            return (string) results[0];
        }

        /// <summary>
        /// Returns the given payment information. Tests XML serialization.
        /// </summary>
        /// <param name="info">The payment information.</param>
        /// <returns>The payment information.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/EchoPaymentInfo", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public PaymentInfo EchoPaymentInfo(PaymentInfo info)
        {
            object[] results = Invoke("EchoPaymentInfo", new object[] {info});
            return (PaymentInfo) results[0];
        }

        /// <summary>
        /// Returns the given payment schedule. Tests XML serialization.
        /// </summary>
        /// <param name="schedule">The payment schedule.</param>
        /// <returns>The payment schedule.</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/EchoSchedule", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        public Schedule EchoSchedule(Schedule schedule)
        {
            object[] results = Invoke("EchoSchedule", new object[] {schedule});
            return (Schedule) results[0];
        }

        /// <summary>
        /// Tests XML serialization; can be used for testing a production server.
        /// </summary>
        /// <param name="customer">A customer.</param>
        /// <param name="amount">A transaction amount.</param>
        /// <param name="method">A payment method.</param>
        /// <returns>A <see cref="PaymentInfo"/> combining the three 
        /// arguments</returns>
        [SoapRpcMethodAttribute(
            "http://moneymanagement.org/payment-gateway/TestEncoding", 
            RequestNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            ResponseNamespace="http://moneymanagement.org/payment-gateway/literalTypes", 
            Use=SoapBindingUse.Literal)]
        [Obsolete]
        public PaymentInfo TestEncoding(Customer customer, string amount,
            PaymentMethod method)
        {
            object[] results = 
                Invoke("TestEncoding", new object[] {customer, amount, method});
            return (PaymentInfo) results[0];
        }
    }

}
