﻿/*******************************************************************************
 *
 * Defines the class ProviderStatus.
 *
 * File:        PaymentGateway/Library/ProviderStatus.cs
 * Date:        Fri Oct 31 18:50:35 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
      /// <summary>
      /// 
      /// </summary>
    public class ProviderStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderStatus"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="referenceCode">The reference code.</param>
        public ProviderStatus( string status, string errorMessage, 
                               string referenceCode )
        {
            myStatus = status;
            myErrorMessage = errorMessage;
            myReferenceCode = referenceCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderStatus"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="referenceCode">The reference code.</param>
        /// <param name="transactionCode">The transaction code.</param>
        public ProviderStatus(string status, string errorMessage,
                               string referenceCode, string transactionCode)
        {
            myStatus = status;
            myErrorMessage = errorMessage;
            myReferenceCode = referenceCode;
            myTransactionCode = transactionCode;
        }

        /// <summary>
        /// Gets the status code.
        /// </summary>
        /// <value>The status.</value>
        public string Status { get { return myStatus; } }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get { return myErrorMessage; } }

        /// <summary>
        /// Gets the reference code.
        /// </summary>
        /// <value>The reference code.</value>
        public string ReferenceCode { get { return myReferenceCode; } }

        /// <summary>
        /// Gets the transaction code.
        /// </summary>
        /// <value>The transaction code.</value>
        public string TransactionCode { get { return myTransactionCode; } }

        public static bool operator==(ProviderStatus lhs, ProviderStatus rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(ProviderStatus lhs, ProviderStatus rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            ProviderStatus status = (ProviderStatus) other;
                return Status == status.Status &&
                       ErrorMessage == status.ErrorMessage &&
                       ReferenceCode == status.ReferenceCode;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return (Status + ErrorMessage + ReferenceCode).GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Status: {0}; ErrorMessage: {1}; ReferenceCode: {2}]",
                       Status, ErrorMessage, ReferenceCode
                   );
        }

        /// <summary>
        /// 
        /// </summary>
        private string myStatus;
        /// <summary>
        /// 
        /// </summary>
        private string myErrorMessage;
        /// <summary>
        /// 
        /// </summary>
        private string myReferenceCode;
        /// <summary>
        /// 
        /// </summary>
        private string myTransactionCode;

    }
}
