﻿/*******************************************************************************
 *
 * Defines the class ProviderAccount.
 *
 * File:        PaymentGateway/Library/ProviderAccount.cs
 * Date:        Sat Nov  1 13:33:01 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;

namespace MMI.Payment
{
      /// <summary>
      /// Represents an account with a payment services provider.
      /// </summary>
    public class ProviderAccount
    {
        /// <summary>
        /// Constructs a <see cref="ProviderAccount"/>.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public ProviderAccount(string username, string password)
            : this(username, password, null, null)
            { }

        /// <summary>
        /// Constructs a <see cref="ProviderAccount"/>.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="accountId1">Additional information associated
        /// with the account.</param>
        public ProviderAccount( string username, string password, 
                                string accountId1 )
            : this(username, password, accountId1, null)
            { }

        /// <summary>
        /// Constructs a <see cref="ProviderAccount"/>.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="accountId1">Additional information associated
        /// with the account.</param>
        /// <param name="accountId2">Additional information associated
        /// with the account.</param>
        public ProviderAccount(string username, string password, 
            string accountId1, string accountId2)
        {
            myId = 0;
            myUsername = username;
            myPassword = password;
            myAccountId1 = accountId1;
            myAccountId2 = accountId2;
        }

        /// <summary>
        /// Gets or sets the database record ID.
        /// </summary>
        /// <value>The id.</value>
        public long Id 
        {
            get { return myId; }
            set { myId = value; }
        }

        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <value>The username.</value>
        public string Username 
        {
            get { return myUsername; }
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password
        {
            get { return myPassword; }
        }

        /// <summary>
        /// Gets additional information associated
        /// with the account
        /// </summary>
        /// <value>A string.</value>
        public string AccountId1 
        {
            get { return myAccountId1; }
        }

        /// <summary>
        /// Gets additional information associated
        /// with the account
        /// </summary>
        /// <value>A string.</value>
        public string AccountId2 
        {
            get { return myAccountId2; }
        }

        public static bool operator==(ProviderAccount lhs, ProviderAccount rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(ProviderAccount lhs, ProviderAccount rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
                ProviderAccount account = (ProviderAccount) other;
                return Username == account.Username &&
                       Password == account.Password &&
                       AccountId1 == account.AccountId1 &&
                       AccountId2 == account.AccountId2;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Username: {0}; Password: {1}; AccountId1: {2}; " +
                           "AccountId2: {3}; ]",
                       Username, Password, AccountId1, AccountId2
                   );
        }
        
        private long  myId;
        private string   myUsername;
        private string   myPassword;
        private string   myAccountId1;
        private string   myAccountId2;
    }
}
