﻿/*******************************************************************************
 *
 * Defines the class PaymentInfo.
 *
 * File:        PaymentGateway/Library/PaymentInfo.cs
 * Date:        Sat Nov  1 13:58:35 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace MMI.Payment
{
      /// <summary>
      /// Represents a customer, a transaction amount, and a payment method.
      /// </summary>
    [Serializable]
    public class PaymentInfo
    {
        private static Regex MatchAmount = new Regex("\\d{0,8}\\.\\d{2}");

        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public PaymentInfo() : this(null, 0, null) { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>. For use with the
        /// transaction type RETURN, in which only a dollor amount
        /// needs to be specified.
        /// </summary>
        /// <param name="amount">The transaction amount.</param>
        public PaymentInfo(decimal amount)
            : this(null, amount, null)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>. For use with the
        /// transaction type RETURN, in which only a dollor amount
        /// needs to be specified.
        /// </summary>
        /// <param name="amount">The transaction amount.</param>
        public PaymentInfo(string amount)
            : this(null, amount, null)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>. For use with
        /// payment methods such as PaymentCookie that provide access 
        /// to customer data, making a Customer object unnecessary.
        /// </summary>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="paymentMethod">The payment method.</param>
        public PaymentInfo(decimal amount, PaymentMethod paymentMethod)
            : this(null, amount, paymentMethod)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>. For use with
        /// payment methods such as PaymentCookie that provide access 
        /// to customer data, making a Customer object unnecessary.
        /// </summary>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="paymentMethod">The payment method.</param>
        public PaymentInfo(string amount, PaymentMethod paymentMethod)
            : this(null, amount, paymentMethod)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>. For use when 
        /// pre-registering payment info with a provider.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="paymentMethod">The payment method.</param>
        public PaymentInfo(Customer customer, PaymentMethod paymentMethod)
            : this(customer, -1, paymentMethod)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="amount">The transaction amount.</param>
        /// <param name="paymentMethod">The payment method.</param>
        public PaymentInfo(Customer customer, decimal amount, 
            PaymentMethod paymentMethod)
        {
            myCustomer = customer;
            myAmount = amount;
            myPaymentMethod = paymentMethod;
        }

        /// <summary>
        /// Constructs a <see cref="PaymentInfo"/>.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="paymentMethod">The payment method.</param>
        public PaymentInfo(Customer customer, string amount, 
            PaymentMethod paymentMethod)
        {
            if (!MatchAmount.IsMatch(amount))
                throw new PaymentException(
                              String.Format("Invalid amount: {0}", amount),
                              Status.Code.INVALID_PARAMETER
                          );
            myCustomer = customer;
            myAmount = Decimal.Parse(amount);
            myPaymentMethod = paymentMethod;
        }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        /// <value>The customer.</value>
        public Customer Customer 
        {
            get { return myCustomer; }
            set { myCustomer = value; }
        }

        /// <summary>
        /// Gets or sets the transaction amount.
        /// </summary>
        /// <value>The amount.</value>
        [DefaultValue(typeof(decimal), "-1")]
        public decimal Amount 
        {
            get { return myAmount; }
            set { myAmount = value; }
        }

        /// <summary>
        /// Gets or sets the payment method.
        /// </summary>
        /// <value>The payment method.</value>
        public PaymentMethod PaymentMethod 
        {
            get { return myPaymentMethod; }
            set { myPaymentMethod = value; }
        }

        public static bool operator==(PaymentInfo lhs, PaymentInfo rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(PaymentInfo lhs, PaymentInfo rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            PaymentInfo info = (PaymentInfo) other;
                return Customer == info.Customer &&
                       Amount == info.Amount &&
                       PaymentMethod == info.PaymentMethod;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            int first = Customer != null ?
                Customer.GetHashCode() :
                0;
            int second = Amount.GetHashCode();
            int third = PaymentMethod != null ?
                PaymentMethod.GetHashCode() :
                0;
            return first ^ (second << 8 | second >> 24) ^
                   (third << 16 | third >> 16);
        }

        public override string ToString()
        {
            return String.Format(
                       "[Customer: {0}; Amount: {1}; PaymentMethod: {2}]",
                       Customer, 
                       Amount != -1 ? 
                            '$' + Amount.ToString("0.00") : 
                            "", 
                       PaymentMethod
                   );
        }

        private Customer       myCustomer;
        private decimal        myAmount;
        private PaymentMethod  myPaymentMethod;
    }
}
