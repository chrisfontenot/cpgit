﻿/*******************************************************************************
 *
 * Defines the class DataClient.
 *
 * File:        PaymentGateway/Library/DataClient.cs
 * Date:        Mon Nov 10 11:20:10 MST 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace MMI.Payment
{
      /// <summary>
      /// Helper base class for components that access a database.
      /// </summary>
    public class DataClient
    {
        /// <summary>
        /// Adds a parameter to an instance of SqlCommand.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="name">The parameter name.</param>
        /// <param name="value">The parameter value.</param>
        /// <param name="type">The data type.</param>
        protected static void AddParameter(SqlCommand command, string name, 
            object value, SqlDbType type)
        {
            SqlParameter p = new SqlParameter(name, type);
            p.Value = value != null ? value : DBNull.Value;
            command.Parameters.Add(p);
        }

        /// <summary>
        /// Adds a parameter to an instance of SqlCommand.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="name">The parameter name.</param>
        /// <param name="value">The parameter value, if any.</param>
        /// <param name="type">The data type.</param>
        protected static void AddParameter(SqlCommand command, string name, 
            long? value, SqlDbType type)
        {
            SqlParameter p = new SqlParameter(name, type);
            if (value.HasValue) {
                p.Value = value.Value;
            } else {
                p.Value = DBNull.Value;
            }
            command.Parameters.Add(p);
        }

        /// <summary>
        /// Reads a VARCHAR value from a SqlDataReader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="column">The column index.</param>
        /// <returns>The string value</returns>
        protected static string ReadVarChar(SqlDataReader reader, int column)
        {
            object value = reader[column];
            return !value.Equals(DBNull.Value) ? (string) value : null;
        }

        /// <summary>
        /// Reads a BIGINT value from a SqlDataReader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="column">The column index.</param>
        /// <returns>The integer value.</returns>
        protected static long ReadBigInt(SqlDataReader reader, int column)
        {
            object value = reader[column];
            return !value.Equals(DBNull.Value) ? (long) value : 0;
        }

        /// <summary>
        /// Sanitizes the connection string for use in an error message.
        /// </summary>
        /// <param name="conn">The conn.</param>
        /// <returns>The connection string with password 
        /// obfuscated.</returns>
        protected static string SanitizeConnectionString(string conn)
        {
            Regex pattern = 
                new Regex(
                        "\\bPassword[[:space:]]*=[^;]+",  
                        RegexOptions.IgnoreCase
                    );
            return pattern.Replace(conn, "Password=XXXXXXXX");
        }
    }
}
