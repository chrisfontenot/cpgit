﻿/*******************************************************************************
 *
 * Defines the class GatewayTransaction.
 *
 * File:        PaymentGateway/Library/GatewayTransaction.cs
 * Date:        GatewayTransaction
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace MMI.Payment
{
    using ProviderTransactionList = 
        ReadOnlyCollection<ProviderTransaction>;
    using StringMap = Dictionary<string, string>;
    using ReadOnlyStringMap = ReadOnlyDictionary<string, string>;

      /// <summary>
      /// Represents one of the six basic types of gateway transactions.
      /// </summary>
    public enum TransactionType 
    {
        /// <summary>
        /// A transaction in which a single payment is made
        /// </summary>
        SALE,
        /// <summary>
        /// A transaction in which a payment is authorized for later caputre
        /// </summary>
        AUTHORIZATION,
        /// <summary>
        /// A transaction in which payment is credited to a customer account
        /// </summary>
        CREDIT,
        /// <summary>
        /// A transaction in which a previously authorized payment is finalized
        /// </summary>
        CAPTURE,
        /// <summary>
        /// A transaction in which a payment is partially or wholly refunded
        /// to the customer.
        /// </summary>
        RETURN,
        /// <summary>
        /// A transaction in which a payment is canceled.
        /// </summary>
        VOID,
        /// <summary>
        /// A transaction in which the status of a previous transaction is 
        /// queried.
        /// </summary>
        QUERY,
        /// <summary>
        /// A transaction in which payment information is registered for use in
        /// a subsequent transaction
        /// </summary>
        CREATE_INFO,
        /// <summary>
        /// A transaction in which payment information previous registered is
        /// retrieved
        /// </summary>
        QUERY_INFO,
        /// <summary>
        /// A transaction in which payment information previous registered is
        /// updated
        /// </summary>
        UPDATE_INFO,
        /// <summary>
        /// A transaction in which payment information previous registered is
        /// deleted
        /// </summary>
        DELETE_INFO,
        /// <summary>
        /// A transaction in which a payment schedule is established
        /// </summary>
        CREATE_SCHEDULE,
        /// <summary>
        /// A transaction in which a payment schedule is retrieved
        /// </summary>
        QUERY_SCHEDULE,
        /// <summary>
        /// A transaction in which a payment schedule is updated
        /// </summary>
        UPDATE_SCHEDULE,
        /// <summary>
        /// A transaction in which a payment schedule is deleted
        /// </summary>
        DELETE_SCHEDULE
    }

    /// <summary>
    /// Represents a single payment gateway transaction.
    /// </summary>
    public class GatewayTransaction : DataClient
    {
        /// <summary>
        /// Constructs a <see cref="GatewayTransaction"/> and stores it in the
        /// database. Its initial status is PENDING.
        /// </summary>
        /// <param name="gateway">The payment gateway.</param>
        /// <param name="conn">An instance of <see cref="Connection"/>.</param>
        /// <param name="id">The id.</param>
        /// <param name="timestamp">The timestamp.</param>
        /// <param name="provider">The provider.</param>
        /// <param name="type">The transaction type.</param>
        /// <param name="paymentInfo">The payment information.</param>
        /// <param name="outputPaymentInfo">The output payment 
        /// information.</param>
        /// <param name="schedule">The schedule.</param>
        /// <param name="outputSchedule">The output schedule.</param>
        /// <param name="transactionRef">The transaction, if any,
        /// referenced by the new transaction.</param>
        /// <param name="memo">The memo.</param>
        /// <param name="status">The status.</param>
        /// <param name="referenceCode">The reference code.</param>
        protected GatewayTransaction(Gateway gateway, Connection conn, 
            long id, DateTime timestamp, string provider, TransactionType type, 
            PaymentInfo paymentInfo, PaymentInfo outputPaymentInfo, 
            Schedule schedule, Schedule outputSchedule,
            GatewayTransaction transactionRef, string memo, Status.Code status,
            string referenceCode )
        {
            myGateway = gateway;
            myConnection = conn;
            myId = id;
            myTimestamp = timestamp;
            myProvider = provider;
            myType = type;
            myPaymentInfo = paymentInfo;
            myOutputPaymentInfo = outputPaymentInfo;
            mySchedule = schedule;
            myOutputSchedule = outputSchedule;
            myTransactionRef = transactionRef;
            myMemo = memo;
            myStatus = status;
            myReferenceCode = referenceCode;
            myProviderTransactions = new List<ProviderTransaction>();
            //if (id > 0)
            //    LoadProviderTransactions();
        }

        /// <summary>
        /// Gets the transaction ID.
        /// </summary>
        /// <value>The id.</value>
        public long Id 
        {
            get { return myId; }
        }

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <value>The timestamp.</value>
        public DateTime Timestamp 
        {
            get { return myTimestamp; }
        }

        /// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        public string Provider 
        {
            get { return myProvider; }
        }

        /// <summary>
        /// Gets the transaction type.
        /// </summary>
        /// <value>The type.</value>
        public TransactionType Type 
        {
            get { return myType; }
        }

        /// <summary>
        /// Gets the input payment information.
        /// </summary>
        /// <value>The payment info.</value>
        public MMI.Payment.PaymentInfo PaymentInfo 
        {
            get { return myPaymentInfo; }
        }

        /// <summary>
        /// Gets or sets the output payment information.
        /// </summary>
        /// <value>The payment info.</value>
        public MMI.Payment.PaymentInfo OutputPaymentInfo 
        {
            get { return myOutputPaymentInfo; }
            set { myOutputPaymentInfo = value; }
        }

        /// <summary>
        /// Gets the input payment schedule.
        /// </summary>
        /// <value>The payment info.</value>
        public MMI.Payment.Schedule Schedule 
        {
            get { return mySchedule; }
        }

        /// <summary>
        /// Gets or sets the output payment schedule.
        /// </summary>
        /// <value>The payment info.</value>
        public MMI.Payment.Schedule OutputSchedule
        {
            get { return myOutputSchedule; }
            set { myOutputSchedule = value; }
        }

        /// <summary>
        /// Gets or sets the referenced transaction.
        /// </summary>
        /// <value>The referenced transaction.</value>
        public GatewayTransaction TransactionRef 
        {
            get { return myTransactionRef; }
        }

        /// <summary>
        /// Gets the memo.
        /// </summary>
        /// <value>The memo.</value>
        public string Memo 
        {
            get { return myMemo; }
        }

        /// <summary>
        /// Gets the status code.
        /// </summary>
        /// <value>The status.</value>
        public Status.Code Status 
        {
            get { return myStatus; }
        }

        /// <summary>
        /// Gets or sets the provider reference code.
        /// </summary>
        /// <value>The reference code.</value>
        public string ReferenceCode 
        {
            get { return myReferenceCode; }
            set { myReferenceCode = value; }
        }

        /// <summary>
        /// Gets or sets the provider transaction code.
        /// </summary>
        /// <value>The provider transaction code.</value>
        public string ProviderTransactionCode
        {
            get { return myProviderTransactionCode; }
            set { myProviderTransactionCode = value; }
        }

        /// <summary>
        /// Gets the list of provider transactions associated with this
        /// gateway transaction.
        /// </summary>
        /// <value>The provider transactions.</value>
        public ProviderTransactionList ProviderTransactions 
        {
            get
            {
                return new ProviderTransactionList(myProviderTransactions);
            }
        }

        /// <summary>
        /// Creates a new provider transaction associated with this
        /// gateway transaction, and stores it in the database. Its
        /// initial status is PENDING.
        /// </summary>
        /// <param name="type">The transaction type.</param>
        /// <returns>The new provider transaction.</returns>
        public ProviderTransaction NewProviderTransaction(string type)
        {
            return NewProviderTransaction(type, new StringMap());
        }

        /// <summary>
        /// Creates a new provider transaction associated with this
        /// gateway transaction, and stores it in the database. Its
        /// initial status is PENDING.
        /// </summary>
        /// <param name="type">The transaction type.</param>
        /// <param name="inputParameters">The input parameters.</param>
        /// <returns>The new provider transaction.</returns>
        public ProviderTransaction NewProviderTransaction(string type, 
            StringMap inputParameters)
        {
            //SqlCommand command = null;
            //SqlTransaction trans = null;
            try {
                //trans = myConnection.SqlConnection.BeginTransaction();

                // Create ProviderTransaction record
                //command =
                //    new SqlCommand(
                //            "CreateProviderTransaction", 
                //            myConnection.SqlConnection
                //        );
                //command.CommandType = CommandType.StoredProcedure;
                //command.Transaction = trans;
                DateTime timestamp = DateTime.Now;
                //command.Parameters.Add(
                //    new SqlParameter("@date", timestamp.ToFileTimeUtc())
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@gateway_transaction", myId)
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@provider", myProvider)
                //);
                //command.Parameters.Add(
                //    new SqlParameter("@type", type)
                //);
                MMI.Payment.Status.Code status =
                    MMI.Payment.Status.Code.PENDING;
                //command.Parameters.Add(
                //    new SqlParameter("@status", status)
                //);
                //long id = Decimal.ToInt64((decimal) command.ExecuteScalar());
                long id = 0;

                // Create provider transaction object
                PrivateProviderTransaction providerTrans = 
                    new PrivateProviderTransaction(
                               myConnection, id, timestamp, type, status, null, 
                               new ReadOnlyStringMap(inputParameters), null
                           );
                providerTrans.Freeze();

                // Create Provider Transaction records
                //StoreProviderTransactionParams(providerTrans, trans);
                //trans.Commit();
                
                // Add to list of provider transations
                myProviderTransactions.Add(providerTrans);

                return providerTrans;

            } catch (PaymentException e) {
                //if (trans != null)
                //    trans.Rollback();
                throw e;
            } catch (Exception e) {
                //if (trans != null)
                //    trans.Rollback();
                throw new PaymentException(
                              "Failed storing provider transaction: " + 
                                   e.Message,
                              MMI.Payment.Status.Code.DATABASE_ERROR,
                              e
                          );
            } finally {
                //if (trans != null)
                //    trans.Dispose();
                //if (command != null)
                //    command.Dispose();
            }
        }

        /// <summary>
        /// Determines whether this instance is complete.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if this instance is complete; otherwise, <c>false</c>.
        /// </returns>
        public bool IsComplete()
        {
            return myStatus != MMI.Payment.Status.Code.PENDING;
        }

        public void Complete(GatewayStatus status)
        {
            Repeater repeater = new Repeater(null);
            repeater.Repeat(
                () => { CompleteImpl(status); return null; }
            );
        }

        private void CompleteImpl(GatewayStatus status)
        {
            if (IsComplete())
                throw new PaymentException(
                              "Transaction is already complete",
                              MMI.Payment.Status.Code.INTERNAL_ERROR
                          );
            Status.Code code = status.StatusCode;
            if (code == MMI.Payment.Status.Code.PENDING)
                throw new PaymentException(
                              String.Format(
                                  "'{0}' is not a valid status for a " +
                                      "complete transaction",
                                  StringFormatter.Capitalize(
                                      MMI.Payment.Status.Message(code)
                                  )
                              ),
                              MMI.Payment.Status.Code.INTERNAL_ERROR
                          );

            // Complete pending provider transaction, if any.
            // Only executed if an error has already occurred.
            try {
                if ( ProviderTransactions.Count > 0 &&
                     !ProviderTransactions.Last().IsComplete() )
                {
                    ProviderTransactions.Last().Complete(code);
                }
            } catch (Exception) { }

            // Update database
            //SqlCommand command = null;
            //try {
            //    command =
            //        new SqlCommand(
            //                "CompleteGatewayTransaction", 
            //                myConnection.SqlConnection
            //            );
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.Parameters.Add(
            //        new SqlParameter("@status", code)
            //    );
            //    AddParameter(
            //        command, "@ref", status.ReferenceCode, SqlDbType.VarChar
            //    );
            //    command.Parameters.Add(
            //        new SqlParameter("@id", Id)
            //    );
            //    command.ExecuteNonQuery();
            //} catch (Exception e) {
            //    throw new PaymentException(
            //                  "Failed updating gateway transaction status: " +
            //                       e.Message,
            //                  Mmi.Payment.Status.Code.DATABASE_ERROR
            //              );
            //} finally {
            //    if (command != null)
            //        command.Dispose();
            //}

            // Update status property
            SetStatus(code);
        }

        public static bool operator==(GatewayTransaction lhs, 
            GatewayTransaction rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(GatewayTransaction lhs,
            GatewayTransaction rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {        
            if (other == null)
                return false;
            try {
                GatewayTransaction trans = (GatewayTransaction) other;
                return Id == trans.Id &&
                       Timestamp == trans.Timestamp &&
                       Provider == trans.Provider &&
                       Type == trans.Type &&
                       Memo == trans.Memo &&
                       Status == trans.Status &&
                       PaymentInfo == trans.PaymentInfo &&
                       OutputPaymentInfo == trans.OutputPaymentInfo &&
                       Schedule == trans.Schedule &&
                       OutputSchedule == trans.OutputSchedule &&
                       TransactionRef == trans.TransactionRef &&
                       ProviderTransactions.SequenceEqual(
                           trans.ProviderTransactions);

            } catch (InvalidCastException) {
                return false;
            }

        }

        public override int GetHashCode()
        {
            return 0; // No need to store GatewayTransactions in hash tables
        }

        protected void SetStatus(Status.Code status)
        {
            myStatus = status;
        }

        protected Connection Connection
        {
            get { return myConnection; }
        }
        
        /// <summary>
        /// Private wrapper for <see cref="ProviderTransaction"/>, used to
        /// limit access to the database by implementors of custom
        /// providers.
        /// </summary>
        private class PrivateProviderTransaction : ProviderTransaction 
        {
            public PrivateProviderTransaction( 
                        Connection conn, long id, 
                        DateTime timestamp, string type,
                        Status.Code status, 
                        ProviderStatus providerStatus, 
                        ReadOnlyStringMap inputParameters,
                        ReadOnlyStringMap outputParameters )
                : base( conn, id, timestamp, type, status, 
                        providerStatus, inputParameters, outputParameters )
                { }
        }

        /// <summary>
        /// Loads the list of provider transactions associated with this 
        /// gateway transaction.
        /// </summary>
        //private void LoadProviderTransactions()
        //{
        //    SqlCommand command = myConnection.SqlConnection.CreateCommand();
        //    SqlDataReader reader = null;

        //    try {
        //        command =
        //            new SqlCommand(
        //                    "LoadProviderTransactions", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@id", myId));
        //        reader = command.ExecuteReader();

        //        // Construct provider transactions
        //        while (reader.Read()) {
        //            int column = 0;
        //            long id = ReadBigInt(reader, column++);
        //            DateTime timestamp = 
        //                DateTime.FromFileTimeUtc(ReadBigInt(reader, column++));
        //            string type = ReadVarChar(reader, column++);
        //            Status.Code status =
        //                (Status.Code) Enum.ToObject(
        //                    typeof(Status.Code), reader[column++]);
        //            string statusString = ReadVarChar(reader, column++);
        //            string errorMessage = ReadVarChar(reader, column++);
        //            string referenceCode =  ReadVarChar(reader, column++);
        //            ProviderStatus providerStatus =  
        //                statusString != null ?
        //                    new ProviderStatus(
        //                            statusString,
        //                            errorMessage,
        //                            referenceCode
        //                        ) :
        //                    null;
        //            PrivateProviderTransaction providerTrans = 
        //                new PrivateProviderTransaction(
        //                        myConnection, id, timestamp, type, status,
        //                        providerStatus, null, null
        //                    );
        //            this.myProviderTransactions.Add(providerTrans);
        //        }
        //        reader.Close();

        //        // Load provider transaction parameters
        //        foreach (var trans in myProviderTransactions)
        //            LoadProviderTransactionParams(
        //                (PrivateProviderTransaction) trans
        //            );

        //    } finally {
        //        if (reader != null)
        //            reader.Dispose();
        //        command.Dispose();
        //    }
        //}

        /// <summary>
        /// Loads the input and output parameters associated with the
        /// given provider transaction.
        /// </summary>
        /// <param name="trans">A provider transaction.</param>
        //private void LoadProviderTransactionParams(PrivateProviderTransaction trans)
        //{
        //    SqlCommand command = myConnection.SqlConnection.CreateCommand();
        //    SqlDataReader reader = null;

        //    try {
        //        command =
        //            new SqlCommand(
        //                    "LoadProviderTransactionParams", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(new SqlParameter("@id", trans.Id));
                
        //        // Fetch parameter records
        //        StringMap input = new StringMap();
        //        StringMap output = new StringMap();
        //        reader = command.ExecuteReader();
        //        while (reader.Read()) {
        //            if ((bool) reader[0]) {
        //                input[(string) reader[1]] = (string) reader[2];
        //            } else {
        //                output[(string) reader[1]] = (string) reader[2];
        //            }
        //        }
        //        trans.InputParameters =
        //            new ReadOnlyDictionary<string,string>(input);
        //        trans.OutputParameters =
        //            new ReadOnlyDictionary<string,string>(output);
        //        trans.Freeze();

        //    } finally {
        //        if (reader != null)
        //            reader.Dispose();
        //        command.Dispose();
        //    }
        //}

        /// <summary>
        /// Stores the input and output parameters associated with the
        /// given provider transaction.
        /// </summary>
        /// <param name="providerTrans">The provider transaction.</param>
        /// <param name="sqlTrans">The current SQL transaction.</param>
        //private void StoreProviderTransactionParams(
        //    PrivateProviderTransaction providerTrans, SqlTransaction sqlTrans)
        //{
        //    SqlCommand command = null;
        //    try {
        //        command =
        //            new SqlCommand(
        //                    "StoreProviderTransactionParam", 
        //                    myConnection.SqlConnection
        //                );
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Transaction = sqlTrans;
        //        SqlParameter transactionid = 
        //            new SqlParameter("@transactionid", SqlDbType.BigInt);
        //        SqlParameter date = 
        //            new SqlParameter("@date", SqlDbType.BigInt);
        //        SqlParameter name = 
        //            new SqlParameter("@name", SqlDbType.VarChar, 250);
        //        SqlParameter value = 
        //            new SqlParameter("@value", SqlDbType.VarChar, 250);
        //        command.Parameters.Add(date);
        //        command.Parameters.Add(transactionid);
        //        command.Parameters.Add(name);
        //        command.Parameters.Add(value);

        //        // Execute the command once for each parameter
        //        foreach (var param in providerTrans.InputParameters) {
        //            date.Value = DateTime.Now.ToFileTimeUtc();
        //            transactionid.Value = providerTrans.Id;
        //            name.Value = param.Key;
        //            value.Value = param.Value;
        //            command.Prepare();
        //            command.ExecuteNonQuery();
        //        }
        //    } finally {
        //        if (command != null)
        //            command.Dispose();
        //    }
        //}

        private Gateway                    myGateway;
        private Connection                 myConnection;
        private long                       myId;
        private DateTime                   myTimestamp;
        private string                     myProvider;
        private TransactionType            myType;
        private PaymentInfo                myPaymentInfo;
        private PaymentInfo                myOutputPaymentInfo;
        private Schedule                   mySchedule;
        private Schedule                   myOutputSchedule;
        private GatewayTransaction         myTransactionRef;
        private string                     myMemo;
        private Status.Code                myStatus;
        private string                     myReferenceCode;
        private List<ProviderTransaction>  myProviderTransactions;
        private string                     myProviderTransactionCode;
    }
}
