﻿/*******************************************************************************
 *
 * Defines the class CustomerInfo.
 *
 * File:        PaymentGateway/Library/CustomerInfo.cs
 * Date:        Tue Oct 28 16:50:08 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;

namespace MMI.Payment
{
      /// <summary>
      /// Represents a tuple of length two.
      /// </summary>
      /// <typeparam name="FirstType">The type of the first element.</typeparam>
      /// <typeparam name="SecondType">The type of the second 
      /// element.</typeparam>
    public class Pair<FirstType, SecondType>
    {
        /// <summary>
        /// Constructs a <see cref="Pair&lt;FirstType, SecondType&gt;"/>.
        /// </summary>
        /// <param name="first">The first element.</param>
        /// <param name="second">The second element.</param>
        public Pair(FirstType first, SecondType second)
        {
            First = first;
            Second = second;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">The other.</param>
        public Pair(Pair<FirstType, SecondType> other)
            : this(other.First, other.Second)
            { }

        public static bool operator==(Pair<FirstType, SecondType> lhs,
            Pair<FirstType, SecondType> rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(Pair<FirstType, SecondType> lhs,
            Pair<FirstType, SecondType> rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            Pair<FirstType, SecondType> pair = 
                    (Pair<FirstType, SecondType>) other;
                return First.Equals(pair.First) && Second.Equals(pair.Second);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override string ToString()
        {
            return String.Format("[First: {0}; Second: {1}]", First, Second);
        }

        public override int GetHashCode()
        {
            return First.GetHashCode() ^ Second.GetHashCode();
        }

        /// <summary>
        /// The first element.
        /// </summary>
        public FirstType   First;
        /// <summary>
        /// The second element.
        /// </summary>
        public SecondType  Second;
    }

    /// <summary>
    /// Provides static methods to capitalize and transform to title
    /// case.
    /// </summary>
    public class StringFormatter {
        private StringFormatter() { }

        /// <summary>
        /// Capitalizes the specified string.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The modified string.</returns>
        public static string Capitalize(string value)
        {
            for (int z = 0, n = value.Length; z < n; ++z)
                if (Char.IsWhiteSpace(value[z]))
                    return ToTitleCase(value.Substring(0, z)) +
                        value.Substring(z);
            return ToTitleCase(value);
        }
        /// <summary>
        /// Transforms the specified string to title case.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The modified string.</returns>
        public static string ToTitleCase(string value)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            return textInfo.ToTitleCase(value);
        }

    }

    /// <summary>
    /// Provides static methods to validate strings based on length
    /// and whether they consist entirely of digits.
    /// </summary>
    public class StringValidator {
        private StringValidator() { }

        /// <summary>
        /// Validates the specified string.
        /// </summary>
        /// <param name="value">The string.</param>
        /// <param name="label">A descriptive label for use in 
        /// error messages.</param>
        /// <param name="minLength">The minimum length.</param>
        /// <param name="maxLength">The maximum length.</param>
        public static void Validate(string value, string label, int minLength,
            int maxLength)
        {
            Validate(value, label, minLength, maxLength, 
                Status.Code.INVALID_PARAMETER);
        }
        /// <summary>
        /// Validates the specified value.
        /// </summary>
        /// <param name="value">The string.</param>
        /// <param name="label">A descriptive label for use in 
        /// error messages.</param>
        /// <param name="minLength">The minimum length.</param>
        /// <param name="maxLength">The maximum length.</param>
        /// <param name="code">The error code to use if validation 
        /// fails.</param>
        public static void Validate(string value, string label, int minLength,
            int maxLength, Status.Code code)
        {
            if (minLength > 0 && value.Length == 0)
                throw new PaymentException(
                              String.Format("{0} cannot be an empty string",
                                  StringFormatter.Capitalize(label)),
                              code
                          );
            else if (minLength == maxLength && value.Length != minLength)
                throw new PaymentException(
                              String.Format(
                                  "{0} must consist of {1} {2}; found '{3}'",
                                  StringFormatter.Capitalize(label),
                                  minLength.ToString(),
                                  minLength == 1 ?
                                     "character" :
                                     "characters",
                                  value
                              ),
                              code
                          );
            else if (value.Length < minLength || value.Length > maxLength)
                throw new PaymentException(
                              String.Format(
                                  "{0} must be between {1} and {2} " +
                                      "characters; found '{3}'",
                                  StringFormatter.Capitalize(label), 
                                  minLength.ToString(), 
                                  maxLength.ToString(), value
                              ),
                              code
                          );
        }
        /// <summary>
        /// Verifies that the given string consists entirely of
        /// digits.
        /// </summary>
        /// <param name="num">The numeric string.</param>
        /// <param name="label">A descriptive label for use in 
        /// error messages.</param>
        /// <param name="minLength">The minimum length.</param>
        /// <param name="maxLength">The maximum length.</param>
        public static void ValidateDigits(string num, string label,
            int minLength, int maxLength)
        {
            ValidateDigits(num, label, minLength, maxLength, 
                Status.Code.INVALID_PARAMETER);
        }
        /// <summary>
        /// Verifies that the given string consists entirely of
        /// digits.
        /// </summary>
        /// <param name="num">The num.</param>
        /// <param name="label">A descriptive label for use in 
        /// error messages.</param>
        /// <param name="minLength">The minimum length.</param>
        /// <param name="maxLength">The maximum length.</param>
        /// <param name="code">The error code to use if validation 
        /// fails.</param>
        public static void ValidateDigits(string num, string label, 
            int minLength, int maxLength, Status.Code code)
        {
            Validate(num, label, minLength, maxLength, code);
            foreach (char d in num)
                if (!Char.IsDigit(d))
                    throw new PaymentException(
                                  String.Format(
                                      "{0} must consist entirely of digits; " + 
                                          "found '{1}'", 
                                      StringFormatter.Capitalize(label), 
                                      num
                                  ),
                                  code
                              );
        }
    }

    /// <summary>
    /// Used to compare associative collections for value equality.
    /// </summary>
    public class DictionaryComparer {
        private DictionaryComparer() { }

        /// <summary>
        /// Returns <c>true</c> if the given collections have the same set of
        /// keys and associate the same value with each key.
        /// </summary>
        /// <typeparam name="TKey">The type of the keys.</typeparam>
        /// <typeparam name="TVal">The type of the values.</typeparam>
        /// <param name="lhs">The first collection.</param>
        /// <param name="rhs">The second collection.</param>
        /// <returns><c>true</c> if the given collections are equal.</returns>
        public static bool AreEqual<TKey, TVal>(
            IDictionary<TKey, TVal> lhs, IDictionary<TKey, TVal> rhs)
                where TVal : IEquatable<TVal>
        {
            if (lhs == null || rhs == null)
                return lhs == rhs;
            if (lhs.Count != rhs.Count)
                return false;
            foreach (var pair in lhs)
                if ( !rhs.ContainsKey(pair.Key) ||
                     !rhs[pair.Key].Equals(pair.Value) )
                {
                    return false;
                }
            return true;
        }
    }

    /// <summary>
    /// Used to generate a pre-seeded instance of System.Random.
    /// </summary>
    public class RandomFactory 
    {
        private RandomFactory() { }

        /// <summary>
        /// Returns a pre-seeded instance of System.Random.
        /// </summary>
        /// <returns>The instance of <see cref="System.Random"/></returns>
        public static Random NewRandom()
        {
            byte[] bytes = Guid.NewGuid().ToByteArray();
            int seed = 1;
            for (int z = 0; z < 16; ++z)
                seed = (seed << 7) + bytes[z];
            return new Random(seed);
        }
    }

    /// <summary>
    /// Parses personal names.
    /// </summary>
    public class NameParser {
        public struct Name {
            public String First, Middle, Last;
        }
        public static Name Parse(String name)
        {
            if (compound == null) {
                compound = 
                    new Regex(
                            @"(?:([-.'\w]+)\s+((?:[-.'\w]+\s)*))((([A|E]l\s+)|" + 
                            @"(Ap\s+)|(Ben\s+)|(Dell([a|e])?\s+)|(Dalle\s+)|" +
                            @"(D[a|e]ll')|(Dela\s+)|(Del\s+)|" +
                            @"(De\s+(La\s+|Los\s+)?)|(D[a|i|u]\s+)|" +
                            @"(L[a|e|o]\s+)|([D|L|O]')|(St\.?\s+)|(San\s+)|" +
                            @"(Den\s+)|(Von\s+(Der\s+)?)|" +
                            @"(Van\s+(De(n|r)?\s+)?))([-.'\w]+))",
                            RegexOptions.IgnoreCase
                        );
                noncompound =
                    new Regex(
                            @"(?:([-.'\w]+)\s+((?:[-.'\w]+\s)*))?([-.'\w]+)"
                        );
            }
            Name result = new Name();
            Match match = compound.Match(name);
            if (match.Success) {
                result.First = match.Groups[1].Captures[0].ToString();
                result.Middle = match.Groups[2].Length > 0 ?
                    match.Groups[2].Captures[0].ToString().Trim() :
                    null;
                result.Last = match.Groups[3].Captures[0].ToString();
            } else {
                match = noncompound.Match(name);
                if (match.Success) {
                    result.First = match.Groups[1].Captures[0].ToString();
                    result.Middle = match.Groups[2].Length > 0 ?
                        match.Groups[2].Captures[0].ToString().Trim() :
                        null;
                    result.Last = match.Groups[3].Captures[0].ToString();
                } else {
                    result.First = "";
                    result.Last = name;
                }
            }
            return result;
        }

        private static Regex compound, noncompound;
    }

}