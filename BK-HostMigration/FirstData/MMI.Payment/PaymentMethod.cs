﻿/*******************************************************************************
 *
 * Defines the class PaymentMethod and its two subclasses, BankAccount and 
 * DebitCard.
 *
 * File:        PaymentGateway/Library/CustomerInfo.cs
 * Date:        Tue Oct 28 16:50:08 MDT 2008
 *
 * Copyright:   2008-2009 Money Management International
 * Author:      CounselNow
 * License:     All rights reserved
 *
 ******************************************************************************/

using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MMI.Payment
{
    using StringMap = Dictionary<string, string>;
    using ReadOnlystringMap = ReadOnlyDictionary<string, string>;
    using ParamList = SortedList<PaymentMethod.Param>;

    /// <summary>
    /// Use to categorize payment methods to determine which class should
    /// represent them and to further subdivide instances of a single class
    /// (e.g., checking and savings accounts, both represented by the class
    /// BankAccount).
    /// </summary>
    public enum AccountType {
        /// <summary>
        /// Invalid account type; used for XML serialization.
        /// </summary>
        [XmlIgnore]
        NONE,
        /// <summary>
        /// Represents a checking account used for ACH transaction.
        /// </summary>
        CHECKING,
        /// <summary>
        /// Represents a savings account used for ACH transaction.
        /// </summary>
        SAVINGS,
        /// <summary>
        /// Represents a debit card.
        /// </summary>
        DEBIT,
        /// <summary>
        /// Represents a credit card; for providers that do not distinguish
        /// between debit cards and credit cards, this account type should
        /// be selected,
        /// </summary>
        CREDIT,
        /// <summary>
        /// Represents payment information that has been pre-registered with a
        /// provider.
        /// </summary>
        TOKEN,
        /// <summary>
        /// Represents a custom payment method.
        /// </summary>
        CUSTOM
    }

      /// <summary>
      /// Represents a payment method, e.g., automated clearinghouse or
      /// debit card. The implementation of this class and its subclasses
      /// is complicated by the default XML serialization rules, which 
      /// would dictate that the payment method parameters appear at the
      /// beginning, rather than the end, of the XML element representing
      /// a payment method if a simpler implementation technique had been
      /// used.
      /// </summary>
    public abstract class PaymentMethod {

        /// <summary>
        /// Constructs a <see cref="PaymentMethod"/> .
        /// </summary>
        protected PaymentMethod()
            : this(AccountType.NONE, new ParamList())
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentMethod"/>.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="parameters">The parameter list.</param>
        protected PaymentMethod(AccountType type, StringMap parameters)
            : this(type, new ParamList())
        {
            foreach (var p in parameters)
                myParameters.Add(new Param(p.Key, p.Value));
        }

        /// <summary>
        /// Constructs a <see cref="PaymentMethod"/>.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="parameters">The parameter list.</param>
        protected PaymentMethod(AccountType type, ParamList parameters)
        {
            myType = type;
            myParameters = parameters;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A payment method.</param>
        protected PaymentMethod(PaymentMethod other)
            : this(other.myType, new ParamList(other.myParameters))
            { }

        /// <summary>
        /// Names of columns in the table PaymentInfo containing
        /// values related to the payment method, together with the names
        /// of several properties that are not allowed to be stored
        /// in the database
        /// </summary>
        public static readonly ReadOnlyCollection<string> PropertyNames = 
            new ReadOnlyCollection<string>(
                    new string[] 
                    {
                        "account_type",
                        "account_number",
                        "routing_number",
                        "verification_code",
                        "expiration_date",
                        "account_username",
                        "account_password",
                        "billing_name",
                        "billing_street1",
                        "billing_street2",
                        "billing_city",
                        "billing_state",
                        "billing_zip"
                    }
                );

        /// <summary>
        /// Subsequence of <see cref="PropertyNames"/> representing 
        /// properties that are not allowed to be stored in the database
        /// </summary>
        public static readonly ReadOnlyCollection<string> NonStorable = 
            new ReadOnlyCollection<string>(
                    new string[] 
                    {
                        "account_number",
                        "expiration_date",
                        "verification_code",
                        "account_password"
                    }
                );

        /// <summary>
        /// Represents a provider-specific payment method property. Allows
        /// additional providers to be supported without updating the
        /// database schema.
        /// </summary>
        [Serializable]
        public class Param : IComparable
        {
            /// <summary>
            /// Default constructor, required for XML serialization.
            /// </summary>
            public Param() { }

            /// <summary>
            /// Constructs a <see cref="Param"/>.
            /// </summary>
            /// <param name="name">The parameter name.</param>
            /// <param name="value">The parameter value.</param>
            public Param(string name, string value)
            {
                myName = name;
                myValue = value;
            }

            /// <summary>
            /// Gets the name.
            /// </summary>
            /// <value>The name.</value>
            public string Name 
            { 
                get { return myName; } 
                set { myName = value; } 
            }

            /// <summary>
            /// Gets the value.
            /// </summary>
            /// <value>The value.</value>
            public string Value 
            { 
                get { return myValue; } 
                set { myValue = value; } 
            }

            public override bool Equals(object other)
            {
                Param p = null;
                try {
                    p = (Param) other;
                } catch (InvalidCastException) {
                    return false;
                }
                return Name == p.Name && Value == p.Value;
            }

            public override int GetHashCode()
            {
                return (Name + Value).GetHashCode();
            }

            public override string ToString()
            {
                return String.Format("[Name: {0}; Value: {1}]", Name, Value);
            }

            public int CompareTo(object other)
            {
                return Name.CompareTo(((Param) other).Name);
            }

            [OnDeserializedAttribute()]
            private void OnDeserialized(StreamingContext ctx)
            {
                if (myName == null)
                    throw new
                        PaymentException(
                            "Missing parameter name",
                            Status.Code.MISSING_PARAMETER
                        );
                if (myValue == null)
                    throw new
                        PaymentException(
                            "Missing parameter value",
                            Status.Code.MISSING_PARAMETER
                        );
            }

            /// <summary>
            /// The parameter name.
            /// </summary>
            private string myName;
            /// <summary>
            /// The parameter value.
            /// </summary>
            private string myValue;
        }

        /// <summary>
        /// Gets or sets the account type.
        /// </summary>
        /// <value>The account type.</value>
        public AccountType Type
        {
            get { return myType; }
            set 
            { 
                ValidateType(value);
                myType = value;
            }
        }

        /// <summary>
        /// Returns the collection of this payment method's
        /// standard (i.e., not provider-specific) properties
        /// as an associative collection.
        /// </summary>
        /// <value>The collection of properties.</value>
        [XmlIgnore]
        public abstract StringMap Properties
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets list of provider-specific parameters.
        /// </summary>
        /// <value>The parameters.</value>
        [XmlIgnore]
        public ParamList Parameters
        {
            get { return myParameters; }
            set { myParameters = value; }
        }

        /// <summary>
        /// Adds a provider-specific parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void AddParameter(string name, string value)
        {
            myParameters.Add(new Param(name, value));
        }

        /// <summary>
        /// Returns the value, if any, associated with the named
        /// parameter.
        /// </summary>
        /// <param name="name">The parameter name.</param>
        /// <returns>The value.</returns>
        public string GetParameter(string name)
        {
            foreach (Param p in myParameters)
                if (p.Name == name)
                    return p.Value;
            return null;
        }

        /// <summary>
        /// Creates a new instance of PaymentMethod from the given collection
        /// of properties.
        /// </summary>
        public static PaymentMethod Create(StringMap properties)
        {
            // Check that properties has the correct collection of keys
            bool error = false;
            if (properties.Count != PropertyNames.Count) {
                error = true;
            } else {
                foreach (string key in PropertyNames) 
                    if (!properties.ContainsKey(key)) {
                        error = true;
                        break;
                    }
            }
            if (error) {
                string[] expected = PropertyNames.ToArray();
                string[] found = properties.Keys.ToArray();
                Array.Sort(expected);
                Array.Sort(found);
                throw new PaymentException(
                              String.Format(
                                  "Invalid payment method properties: " +
                                  "expected keys '{0}'; found '{1}'",
                                  String.Join("', '", expected),
                                  String.Join("', '", found)
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
            }

            // Branch according to account type
            PaymentMethod method;
            string accountType = properties["account_type"];
            if ( accountType == AccountType.CHECKING.ToString() ||
                 accountType == AccountType.SAVINGS.ToString() )
            {
                method = new BankAccount();
            } else if (accountType == AccountType.DEBIT.ToString()) {
                method = new DebitCard();
            } else if (accountType == AccountType.CREDIT.ToString()) {
                method = new CreditCard();
            } else if (accountType == AccountType.TOKEN.ToString()) {
                method = new PaymentToken();
            } else {
                method = new CustomPaymentMethod();
            }
            method.Properties = properties;

            return method;
        }

        public static bool operator==(PaymentMethod lhs, PaymentMethod rhs)
        {
            return !Object.ReferenceEquals(lhs, null) ? 
                lhs.Equals(rhs) : 
                Object.ReferenceEquals(rhs, null);
        }

        public static bool operator!=(PaymentMethod lhs, PaymentMethod rhs)
        {
            return !(lhs == rhs);
        }

        public abstract override bool Equals(object o);

        public abstract override int GetHashCode();

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected abstract void ValidateType(AccountType type);

        private AccountType  myType;
        private ParamList    myParameters;
    }

    /// <summary>
    /// Represents payment by transfer of funds from a bank account.
    /// </summary>
    [Serializable]
    public class BankAccount : PaymentMethod 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public BankAccount() : this(AccountType.NONE, null, null) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BankAccount"/> class.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="routingNumber">The routing number.</param>
        public BankAccount( AccountType type, 
                            string accountNumber,
                            string routingNumber )
            : this(type, accountNumber, routingNumber, new ParamList())
            { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BankAccount"/> class.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="routingNumber">The routing number.</param>
        /// <param name="parameters">The parameter list.</param>
        public BankAccount( AccountType type, 
                            string accountNumber,
                            string routingNumber,
                            ParamList parameters )
            : this(type, accountNumber, routingNumber, null, null, parameters)
            { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BankAccount"/> class.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="routingNumber">The routing number.</param>
        /// <param name="accountHolder">The account holder name.</param>
        /// <param name="billingAddress">The billing address.</param>
        /// <param name="parameters">The parameter list.</param>
        public BankAccount( AccountType type, string accountNumber,
                            string routingNumber, string accountHolder,
                            Address billingAddress )
            : base(type, new ParamList())
        {
            AccountNumber = accountNumber;
            RoutingNumber = routingNumber;
            AccountHolder = accountHolder;
            BillingAddress = billingAddress;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BankAccount"/> class.
        /// </summary>
        /// <param name="type">The account type.</param>
        /// <param name="accountNumber">The account number.</param>
        /// <param name="routingNumber">The routing number.</param>
        /// <param name="accountHolder">The account holder name.</param>
        /// <param name="billingAddress">The billing address.</param>
        /// <param name="parameters">The parameter list.</param>
        public BankAccount( AccountType type, string accountNumber,
                            string routingNumber, string accountHolder,
                            Address billingAddress, ParamList parameters )
            : base(type, parameters)
        {
            AccountNumber = accountNumber;
            RoutingNumber = routingNumber;
            AccountHolder = accountHolder;
            BillingAddress = billingAddress;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A BankAccount.</param>
        public BankAccount(BankAccount other)
            : this( other.Type, other.AccountNumber, other.RoutingNumber,
                    new ParamList(other.Parameters) )
            { }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        /// <value>The account number.</value>
        public string AccountNumber
        {
            get { return myAccountNumber; }
            set
            { 
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "account number", 1, Int32.MaxValue );
                }
                myAccountNumber = value;
                myProperties = null; 
            }
        }

        /// <summary>
        /// Gets or sets the routing number.
        /// </summary>
        /// <value>The routing number.</value>
        public string RoutingNumber
        {
            get { return myRoutingNumber; }
            set 
            { 
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "routing number", 9, 9);
                }
                myRoutingNumber = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the account holder name.
        /// </summary>
        /// <value>The card holder.</value>
        public string AccountHolder
        {
            get { return myAccountHolder; }
            set 
            { 
                if (value != null)
                    StringValidator.Validate(
                        value, "account holder name", 3, 50);
                myAccountHolder = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the billing address.
        /// </summary>
        /// <value>The billing address.</value>
        public Address BillingAddress
        {
            get { return myBillingAddress; }
            set 
            { 
                myBillingAddress = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Returns the collection of this payment method's
        /// standard (i.e., not provider-specific) properties
        /// as an associative collection.
        /// </summary>
        /// <value>The collection of properties.</value>
        [XmlIgnore]
        public override StringMap Properties
        {
            get 
            {
                if (myProperties == null) {
                    myProperties = new StringMap();
                    myProperties["account_type"] = this.Type.ToString();
                    myProperties["account_number"] = AccountNumber;
                    myProperties["routing_number"] = RoutingNumber;
                    myProperties["verification_code"] = null;
                    myProperties["expiration_date"] = null;
                    myProperties["account_username"] = null;
                    myProperties["account_password"] = null;
                    myProperties["billing_name"] = AccountHolder;
                    myProperties["billing_street1"] = BillingAddress != null ?
                        BillingAddress.Street1 :
                        null;
                    myProperties["billing_street2"] = BillingAddress != null ?
                        BillingAddress.Street2 :
                        null;
                    myProperties["billing_city"] = BillingAddress != null ?
                        BillingAddress.City :
                        null;
                    myProperties["billing_state"] = BillingAddress != null ?
                        BillingAddress.State :
                        null;
                    myProperties["billing_zip"] = BillingAddress != null ?
                        BillingAddress.Zip :
                        null;
                }
                return myProperties;
            }

            set 
            {
                myProperties = null;
                this.Type = 
                    value["account_type"] == AccountType.CHECKING.ToString() ?
                        AccountType.CHECKING :
                        AccountType.SAVINGS;
                AccountNumber = value["account_number"];
                RoutingNumber = value["routing_number"];
                AccountHolder = value["billing_name"];
                if (value["billing_street1"] != null) {
                    BillingAddress =
                        new Address(
                                value["billing_street1"],
                                value["billing_street2"],
                                value["billing_city"],
                                value["billing_state"],
                                value["billing_zip"]
                            );
                } else {
                    BillingAddress = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets list of provider-specific parameters. This
        /// property implements XML serialization of the parameter
        /// list at the end of the element representing the payment
        /// method.
        /// </summary>
        /// <value>The parameters.</value>
        [XmlElement(ElementName="Param")]
        public ParamList XmlParameterList
        {
            get { return Parameters; }
            set { Parameters = value; }
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            BankAccount account = (BankAccount) other;
                return this.Type == account.Type &&
                       AccountNumber == account.AccountNumber &&
                       RoutingNumber == account.RoutingNumber &&
                       Parameters.SequenceEqual(account.Parameters);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.Type.ToString());
            sb.Append(AccountNumber);
            sb.Append(RoutingNumber);
            sb.Append(AccountHolder);
            sb.Append(BillingAddress);
            foreach (var p in XmlParameterList) {
                sb.Append(p.Name);
                sb.Append(p.Value);
            }
            return sb.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Type: {0}; AccountNumber: {1}; RoutingNumber: {2}; " +
                           "AccountHolder: {3}; BillingAddress: {4}; " + 
                           "Parameters: {5}]",
                       this.Type.ToString(),
                       AccountNumber,
                       RoutingNumber,
                       AccountHolder,
                       BillingAddress,
                       Parameters
                   );
        }

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected override void ValidateType(AccountType type)
        {
            if ( type != AccountType.CHECKING &&
                 type != AccountType.SAVINGS )
            {
                throw new PaymentException(
                              String.Format(
                                  "Invalid account type: {0}", 
                                  type
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
            }
        }

        private string       myAccountNumber;
        private string       myRoutingNumber;
        private string       myAccountHolder;
        private Address      myBillingAddress;
        private StringMap    myProperties;
    }

    /// <summary>
    /// Represents payment by debit or credit card.
    /// </summary>
    public abstract class Card : PaymentMethod 
    {
        /// <summary>
        /// Constructs a <see cref="Card"/>.
        /// </summary>
        /// <param name="type">One of AccountType.CREDIT or
        /// AccountType.DEBIT.</param>
        public Card(AccountType type)
            : this(type, null, null, null, null)
            { }

        /// <summary>
        /// Constructs a <see cref="Card"/>.
        /// </summary>
        /// <param name="type">One of AccountType.CREDIT or
        /// AccountType.DEBIT.</param>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        public Card( AccountType type, string cardHolder, string cardNumber, 
                     string expirationMonth, string expirationYear )
            : this( type, cardHolder, cardNumber, expirationMonth, 
                    expirationYear, null, null )
            { }

        /// <summary>
        /// Constructs a <see cref="Card"/>.
        /// </summary>
        /// <param name="type">One of AccountType.CREDIT or
        /// AccountType.DEBIT.</param>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        public Card( AccountType type, string cardHolder, string cardNumber, 
                     string expirationMonth, string expirationYear, 
                     string verificationCode )
            : this( type, cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, null )
            { }

        /// <summary>
        /// Constructs a <see cref="Card"/>.
        /// </summary>
        /// <param name="type">One of AccountType.CREDIT or
        /// AccountType.DEBIT.</param>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        public Card( AccountType type, string cardHolder, string cardNumber,
                     string expirationMonth, string expirationYear, 
                     string verificationCode, Address billingAddress )
            : this( type, cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, billingAddress, 
                    new ParamList() )
            { }

        /// <summary>
        /// Constructs a <see cref="Card"/>.
        /// </summary>
        /// <param name="type">One of AccountType.CREDIT or
        /// AccountType.DEBIT.</param>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        /// <param name="parameters">The parameter list.</param>
        public Card( AccountType type, string cardHolder, string cardNumber,
                     string expirationMonth, string expirationYear, 
                     string verificationCode, Address billingAddress,
                     ParamList parameters )
            : base(type, parameters)
        {
            CardHolder = cardHolder;
            CardNumber = cardNumber;
            ExpirationMonth = expirationMonth;
            ExpirationYear = expirationYear;
            VerificationCode = verificationCode;
            BillingAddress = billingAddress;
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A Card.</param>
        public Card(Card other)
            : this( other.Type, other.CardHolder, other.CardNumber, 
                    other.ExpirationMonth, other.ExpirationYear, 
                    other.VerificationCode, new Address(other.BillingAddress),
                    new ParamList(other.Parameters) )
            { }

        /// <summary>
        /// Gets or sets the card holder name.
        /// </summary>
        /// <value>The card holder.</value>
        public string CardHolder
        {
            get { return myCardHolder; }
            set 
            { 
                if (value != null)
                    StringValidator.Validate(
                        value, "card holder name", 3, 50);
                myCardHolder = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the card number.
        /// </summary>
        /// <value>The card number.</value>
        public string CardNumber
        {
            get { return myCardNumber; }
            set 
            { 
                if (value != null) {
                    if (value.Length == 4)
                        value = "xxxxxxxxxxxx" + value;
                    StringValidator.Validate(
                        value, "card number", 15, 16
                    );
                    StringValidator.ValidateDigits(
                        value.Substring(value.Length - 4),
                        "last four digits of card number", 4, 4
                    );
                }
                myCardNumber = value;
                myProperties = null; 
            }
        }

        /// <summary>
        /// Gets or sets the two-digit expiration month.
        /// </summary>
        /// <value>The expiration month.</value>
        public string ExpirationMonth
        {
            get { return myExpirationMonth; }
            set 
            { 
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "expiration month", 2, 2);
                    int num = Int32.Parse(value);
                    if (num > 12)
                        throw new PaymentException(
                                      "Invalid expiration month: " + value,
                                      Status.Code.INVALID_PARAMETER
                                  );
                }
                myExpirationMonth = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the four-digit expiration year.
        /// </summary>
        /// <value>The expiration year.</value>
        public string ExpirationYear
        {
            get { return myExpirationYear; }
            set 
            { 
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "expiration year", 4, 4);
                    int num = Int32.Parse(value);
                    if (num < 2000 || num > 2100)
                        throw new PaymentException(
                                      "Invalid expiration year: " + value,
                                      Status.Code.INVALID_PARAMETER
                                  );
                }
                myExpirationYear = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the verification code.
        /// </summary>
        /// <value>The verification code.</value>
        public string VerificationCode
        {
            get { return myVerificationCode; }
            set 
            { 
                if (value != null) {
                    StringValidator.ValidateDigits(
                        value, "verification code", 3, 4);
                }
                myVerificationCode = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Gets or sets the billing address.
        /// </summary>
        /// <value>The billing address.</value>
        public Address BillingAddress
        {
            get { return myBillingAddress; }
            set 
            { 
                myBillingAddress = value;
                myProperties = null;
            }
        }

        /// <summary>
        /// Returns the collection of this payment method's
        /// standard (i.e., not provider-specific) properties
        /// as an associative collection.
        /// </summary>
        /// <value>The collection of properties.</value>
        [XmlIgnore]
        public override StringMap Properties
        {
            get {
                if (myProperties == null) {
                    myProperties = new StringMap();
                    myProperties["account_type"] = this.Type.ToString();
                    myProperties["account_number"] = CardNumber;
                    myProperties["routing_number"] = null;
                    myProperties["verification_code"] = VerificationCode;
                    myProperties["expiration_date"] = 
                        ExpirationMonth != null && 
                        ExpirationYear != null ?
                            ExpirationMonth + ExpirationYear.Substring(2) :
                            null;
                    myProperties["account_username"] = null;
                    myProperties["account_password"] = null;
                    myProperties["billing_name"] = CardHolder;
                    myProperties["billing_street1"] = BillingAddress != null ?
                        BillingAddress.Street1 :
                        null;
                    myProperties["billing_street2"] = BillingAddress != null ?
                        BillingAddress.Street2 :
                        null;
                    myProperties["billing_city"] = BillingAddress != null ?
                        BillingAddress.City :
                        null;
                    myProperties["billing_state"] = BillingAddress != null ?
                        BillingAddress.State :
                        null;
                    myProperties["billing_zip"] = BillingAddress != null ?
                        BillingAddress.Zip :
                        null;
                }
                return myProperties;
            }

            set {
                myProperties = null;
                CardHolder = value["billing_name"];
                CardNumber = value["account_number"];
                if (value["expiration_date"] != null) {
                    ExpirationMonth = value["expiration_date"].Substring(0, 2);
                    ExpirationYear = value["expiration_date"].Substring(2, 2);
                } else {
                    ExpirationMonth = myExpirationYear = null;
                }
                VerificationCode = value["verification_code"];
                if (value["billing_street1"] != null) {
                    BillingAddress =
                        new Address(
                                value["billing_street1"],
                                value["billing_street2"],
                                value["billing_city"],
                                value["billing_state"],
                                value["billing_zip"]
                            );
                } else {
                    BillingAddress = null;
                }
            }
        }

        /// <summary>
        /// Sets the billing address to that of the specified customer.
        /// </summary>
        /// <param name="customer">A customer.</param>
        public void SetBillingAddress(Customer customer)
        {
            myBillingAddress = new Address(customer);
            myProperties = null;
        }

        /// <summary>
        /// Gets or sets list of provider-specific parameters. This
        /// property implements XML serialization of the parameter
        /// list at the end of the element representing the payment
        /// method.
        /// </summary>
        /// <value>The XML parameter list.</value>
        [XmlElement(ElementName="Param")]
        public ParamList XmlParameterList
        {
            get { return Parameters; }
            set { Parameters = value; }
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            Card card = (Card) other;
                return this.Type == card.Type &&
                       CardHolder == card.CardHolder &&
                       CardNumber == card.CardNumber && 
                       ExpirationMonth == card.ExpirationMonth &&
                       ExpirationYear == card.ExpirationYear &&
                       VerificationCode == card.VerificationCode &&
                       Parameters.SequenceEqual(card.Parameters) &&
                       BillingAddress == card.BillingAddress;
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.Type.ToString());
            sb.Append(CardHolder);
            sb.Append(CardNumber);
            sb.Append(ExpirationMonth);
            sb.Append(ExpirationYear);
            sb.Append(VerificationCode);
            foreach (var p in XmlParameterList) {
                sb.Append(p.Name);
                sb.Append(p.Value);
            }
            int code = sb.ToString().GetHashCode();
            if (BillingAddress != null)
                code ^= BillingAddress.GetHashCode();
            return code;
        }

        public override string ToString()
        {
            return String.Format(
                       "[Type: {0}, CardHolder: {1}; CardNumber: {2}; " +
                           "ExpirationMonth {3}; ExpirationYear: {4}; " +
                           "VerificationCode: {5}; BillingAddress: {6}; " +
                           "Parameters: {7}]",
                       this.Type, CardHolder, CardNumber, ExpirationMonth,
                       ExpirationYear, VerificationCode, BillingAddress,
                       Parameters
                   );
        }

        private string       myCardHolder;
        private string       myCardNumber;
        private string       myExpirationMonth;
        private string       myExpirationYear;
        private string       myVerificationCode;
        private Address      myBillingAddress;
        private StringMap    myProperties;
    }

    /// <summary>
    /// Represents payment by debit card.
    /// </summary>
    public class DebitCard : Card 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public DebitCard()
            : this(null, null, null, null)
            { }

        /// <summary>
        /// Constructs a <see cref="DebitCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        public DebitCard( string cardHolder, string cardNumber, 
                          string expirationMonth, string expirationYear )
            : this( cardHolder, cardNumber, expirationMonth, expirationYear, 
                    null, null )
            { }

        /// <summary>
        /// Constructs a <see cref="DebitCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        public DebitCard( string cardHolder, string cardNumber, 
                          string expirationMonth, string expirationYear, 
                          string verificationCode )
            : this( cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, null )
            { }

        /// <summary>
        /// Constructs a <see cref="DebitCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        public DebitCard( string cardHolder, string cardNumber,
                          string expirationMonth, string expirationYear, 
                          string verificationCode, Address billingAddress )
            : this( cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, billingAddress, 
                    new ParamList() )
            { }

        /// <summary>
        /// Constructs a <see cref="DebitCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        /// <param name="parameters">The parameter list.</param>
        public DebitCard( string cardHolder, string cardNumber,
                          string expirationMonth, string expirationYear, 
                          string verificationCode, Address billingAddress,
                          ParamList parameters )
            : base( AccountType.DEBIT, cardHolder, cardNumber, expirationMonth,
                    expirationYear, verificationCode, billingAddress,
                    parameters )
            { }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A DebitCard.</param>
        public DebitCard(DebitCard other)
            : this( other.CardHolder, other.CardNumber, other.ExpirationMonth,
                    other.ExpirationYear, other.VerificationCode, 
                    new Address(other.BillingAddress), 
                    new ParamList(other.Parameters) )
            { }

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected override void ValidateType(AccountType type)
        {
            if (type != AccountType.DEBIT)
                throw new PaymentException(
                              String.Format(
                                  "Invalid account type: {0}", 
                                  type
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
        }
    }

    /// <summary>
    /// Represents payment by credit card.
    /// </summary>
    public class CreditCard : Card 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public CreditCard()
            : this(null, null, null, null)
            { }

        /// <summary>
        /// Constructs a <see cref="CreditCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        public CreditCard( string cardHolder, string cardNumber, 
                           string expirationMonth, string expirationYear )
            : this( cardHolder, cardNumber, expirationMonth, expirationYear,
                    null, null )
            { }

        /// <summary>
        /// Constructs a <see cref="CreditCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        public CreditCard( string cardHolder, string cardNumber, 
                           string expirationMonth, string expirationYear, 
                           string verificationCode )
            : this( cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, null )
            { }

        /// <summary>
        /// Constructs a <see cref="CreditCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        public CreditCard( string cardHolder, string cardNumber,
                           string expirationMonth, string expirationYear, 
                           string verificationCode, Address billingAddress )
            : this( cardHolder, cardNumber, expirationMonth, 
                    expirationYear, verificationCode, billingAddress, 
                    new ParamList() )
            { }

        /// <summary>
        /// Constructs a <see cref="CreditCard"/>.
        /// </summary>
        /// <param name="cardHolder">The card holder name.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <param name="expirationMonth">The two-digit expiration 
        /// month.</param>
        /// <param name="expirationYear">The four-digit expiration 
        /// year.</param>
        /// <param name="verificationCode">The verification code.</param>
        /// <param name="billingAddress">The billing address.</param>
        /// <param name="parameters">The parameter list.</param>
        public CreditCard( string cardHolder, string cardNumber,
                           string expirationMonth, string expirationYear, 
                           string verificationCode, Address billingAddress,
                           ParamList parameters )
            : base( AccountType.CREDIT, cardHolder, cardNumber, expirationMonth,
                    expirationYear, verificationCode, billingAddress,
                    parameters )
            { }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A CreditCard.</param>
        public CreditCard(CreditCard other)
            : this( other.CardHolder, other.CardNumber, other.ExpirationMonth,
                    other.ExpirationYear, other.VerificationCode, 
                    new Address(other.BillingAddress), 
                    new ParamList(other.Parameters) )
            { }

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected override void ValidateType(AccountType type)
        {
            if (type != AccountType.CREDIT)
                throw new PaymentException(
                              String.Format(
                                  "Invalid account type: {0}", 
                                  type
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
        }
    }

    /// <summary>
    /// Represents payment using an opaque identifier used to reference 
    /// pre-registered payment information.
    /// </summary>
    [Serializable]
    public class PaymentToken : PaymentMethod 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public PaymentToken() 
            : this((string) null)
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentToken"/>.
        /// </summary>
        /// <param name="id">The token identifier.</param>
        public PaymentToken(string id) 
            : this(id, new ParamList())
            { }

        /// <summary>
        /// Constructs a <see cref="PaymentToken"/>.
        /// </summary>
        /// <param name="id">The token identifier.</param>
        /// <param name="parameters">The parameter list.</param>
        public PaymentToken(string id, ParamList parameters)
            : base(AccountType.TOKEN, parameters)
        {
            myId = id;
        }

        /// <summary>
        /// Constructs a <see cref="PaymentToken"/>.
        /// </summary>
        /// <param name="id">The account number.</param>
        /// <param name="parameters">The parameter list.</param>
        public PaymentToken(string id, StringMap parameters)
            : this(id)
        {
            foreach (var p in parameters)
                Parameters.Add(new Param(p.Key, p.Value));
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A PaymentToken.</param>
        public PaymentToken(PaymentToken other)
            : this(other.Id, new ParamList(other.Parameters))
            { }

        /// <summary>
        /// Gets or sets the token identifier.
        /// </summary>
        /// <value>The name.</value>
        public string Id
        {
            get { return myId; }
            set { 
                if (value != null)
                    StringValidator.Validate(
                        value, "token ID", 1, 250);
                myId = value; 
            }
        }

        /// <summary>
        /// Returns the collection of this payment method's
        /// standard (i.e., not provider-specific) properties
        /// as an associative collection.
        /// </summary>
        /// <value>The collection of properties.</value>
        [XmlIgnore]
        public override StringMap Properties
        {
            get
            {
                if (myProperties == null) {
                    myProperties = new StringMap();
                    myProperties["account_type"] = AccountType.TOKEN.ToString();
                    myProperties["account_number"] = myId;
                    myProperties["routing_number"] = null;
                    myProperties["routing_number"] = null;
                    myProperties["verification_code"] = null;
                    myProperties["expiration_date"] = null;
                    myProperties["account_username"] = null;
                    myProperties["account_password"] = null;
                    myProperties["billing_name"] = null;
                    myProperties["billing_street1"] = null;
                    myProperties["billing_street2"] = null;
                    myProperties["billing_city"] = null;
                    myProperties["billing_state"] = null;
                    myProperties["billing_zip"] = null;
                }
                return myProperties;
            }

            set 
            {
                myProperties = null;
                foreach (string p in PaymentMethod.PropertyNames)
                    if ( p != "account_type" && 
                         p != "account_number" && 
                         value[p] != null )
                    {
                        throw new 
                            PaymentException(
                                "Invalid property: " + p,
                                Status.Code.INTERNAL_ERROR
                            );
                    }
                Id = value["account_number"];
            }
        }

        /// <summary>
        /// Gets or sets list of provider-specific parameters. This
        /// property implements XML serialization of the parameter
        /// list at the end of the element representing the payment
        /// method.
        /// </summary>
        /// <value>The XML parameter list.</value>
        [XmlElement(ElementName="Param")]
        public ParamList XmlParameterList
        {
            get { return Parameters; }
            set { Parameters = value; }
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            PaymentToken token = (PaymentToken) other;
                return Id == token.Id && 
                       Parameters.SequenceEqual(token.Parameters);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Id);
            foreach (var p in XmlParameterList) {
                sb.Append(p.Name);
                sb.Append(p.Value);
            }
            return sb.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Id: {0}; Parameters: {1}]",
                       Id, Parameters
                   );
        }

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected override void ValidateType(AccountType type)
        {
            if (type != AccountType.TOKEN)
                throw new PaymentException(
                              String.Format(
                                  "Invalid account type: {0}", 
                                  type
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
        }

        [OnDeserializedAttribute()]
        private void OnDeserialized(StreamingContext ctx)
        {
            if (myId == null)
                throw new
                    PaymentException(
                        "Missing payment token ID",
                        Status.Code.MISSING_PARAMETER
                    );
        }
        
        private string     myId;
        private StringMap  myProperties;
    }

    /// <summary>
    /// Represents a custom payment method.
    /// </summary>
    [Serializable]
    public class CustomPaymentMethod : PaymentMethod 
    {
        /// <summary>
        /// Default constructor, required for XML serialization.
        /// </summary>
        public CustomPaymentMethod() 
            : this((string) null)
            { }

        /// <summary>
        /// Constructs a <see cref="CustomPaymentMethod"/>.
        /// </summary>
        /// <param name="name">The name of the payment method.</param>
        public CustomPaymentMethod(string name) 
            : this(name, new ParamList())
            { }

        /// <summary>
        /// Constructs a <see cref="CustomPaymentMethod"/>.
        /// </summary>
        /// <param name="name">The name of the payment method.</param>
        /// <param name="parameters">The parameter list.</param>
        public CustomPaymentMethod(string name, ParamList parameters)
            : base(AccountType.CUSTOM, parameters)
        {
            myName = name;
        }

        /// <summary>
        /// Constructs a <see cref="CustomPaymentMethod"/>.
        /// </summary>
        /// <param name="name">The name of the payment method.</param>
        /// <param name="parameters">The parameter list.</param>
        public CustomPaymentMethod(string name, StringMap parameters)
            : this(name)
        {
            foreach (var p in parameters)
                Parameters.Add(new Param(p.Key, p.Value));
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="other">A CustomPaymentMethod.</param>
        public CustomPaymentMethod(CustomPaymentMethod other)
            : this(other.Name, new ParamList(other.Parameters))
            { }

        /// <summary>
        /// Gets or sets the name of the payment method.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return myName; }
            set { 
                if (value != null)
                    StringValidator.Validate(
                        value, "payment method", 1, 20);
                myName = value; 
            }
        }

        /// <summary>
        /// Returns the collection of this payment method's
        /// standard (i.e., not provider-specific) properties
        /// as an associative collection.
        /// </summary>
        /// <value>The collection of properties.</value>
        [XmlIgnore]
        public override StringMap Properties
        {
            get
            {
                if (myProperties == null) {
                    myProperties = new StringMap();
                    myProperties["account_type"] = Name;
                    myProperties["account_number"] = null;
                    myProperties["routing_number"] = null;
                    myProperties["routing_number"] = null;
                    myProperties["verification_code"] = null;
                    myProperties["expiration_date"] = null;
                    myProperties["account_username"] = null;
                    myProperties["account_password"] = null;
                    myProperties["billing_name"] = null;
                    myProperties["billing_street1"] = null;
                    myProperties["billing_street2"] = null;
                    myProperties["billing_city"] = null;
                    myProperties["billing_state"] = null;
                    myProperties["billing_zip"] = null;
                }
                return myProperties;
            }

            set 
            {
                myProperties = null;
                foreach (string p in PaymentMethod.PropertyNames)
                    if (p != "account_type" && value[p] != null)
                        throw new 
                            PaymentException(
                                "Invalid property: " + p,
                                Status.Code.INTERNAL_ERROR
                            );
                Name = value["account_type"];
            }
        }

        /// <summary>
        /// Gets or sets list of provider-specific parameters. This
        /// property implements XML serialization of the parameter
        /// list at the end of the element representing the payment
        /// method.
        /// </summary>
        /// <value>The XML parameter list.</value>
        [XmlElement(ElementName="Param")]
        public ParamList XmlParameterList
        {
            get { return Parameters; }
            set { Parameters = value; }
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            try {
 	            CustomPaymentMethod method = (CustomPaymentMethod) other;
                return Name == method.Name && 
                       Parameters.SequenceEqual(method.Parameters);
            } catch (InvalidCastException) {
                return false;
            }
        }

        public override int GetHashCode()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Name);
            foreach (var p in XmlParameterList) {
                sb.Append(p.Name);
                sb.Append(p.Value);
            }
            return sb.ToString().GetHashCode();
        }

        public override string ToString()
        {
            return String.Format(
                       "[Name: {0}; Parameters: {1}]",
                       Name, Parameters
                   );
        }

        /// <summary>
        /// Validates the given account type.
        /// </summary>
        /// <param name="type">The account type.</param>
        protected override void ValidateType(AccountType type)
        {
            if (type != AccountType.CUSTOM)
                throw new PaymentException(
                              String.Format(
                                  "Invalid account type: {0}", 
                                  type
                              ),
                              Status.Code.INVALID_PARAMETER
                          );
        }

        [OnDeserializedAttribute()]
        private void OnDeserialized(StreamingContext ctx)
        {
            if (myName == null)
                throw new
                    PaymentException(
                        "Missing custom payment method name",
                        Status.Code.MISSING_PARAMETER
                    );
        }

        private string     myName;
        private StringMap  myProperties;
    }
}
