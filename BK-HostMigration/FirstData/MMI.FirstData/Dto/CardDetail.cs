﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMI.FirstData.Dto
{
    public class CardDetail
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public string AccountNumber { get; set; }
        public Int32 ExpirationMonth { get; set; }
        public Int32 ExpirationYear { get; set; }
        public string VerificationCode { get; set; }
    }
}
