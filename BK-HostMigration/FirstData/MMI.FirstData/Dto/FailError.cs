﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMI.FirstData
{
	public class FailError : IResponse
	{
        public MMI.Payment.Status.Code StatusCode
        {
            get
            {
                return MMI.Payment.Status.Code.PROVIDER_ERROR;
            }
        }

        public string Message { get; set; }
        public FailError() { }
        public FailError(string message) : this()
        {
            Message = message;
        }
    }
}
