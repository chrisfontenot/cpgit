﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMI.FirstData
{
	public class SuccessResult : IResponse
	{
        public MMI.Payment.Status.Code StatusCode
        {
            get
            {
                return MMI.Payment.Status.Code.SUCCESS;
            }
        }

        public string Message { get; set; }
        public string ReferenceCode { get; set; }
        public string TransactionID { get; set; }
        public SuccessResult() { }
        public SuccessResult(string referenceCode, string transactionId) : this()
        {
            ReferenceCode = referenceCode;
            TransactionID = transactionId;
        }
    }
}
