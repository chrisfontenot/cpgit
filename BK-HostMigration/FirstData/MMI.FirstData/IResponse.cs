﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMI.FirstData
{
    public interface IResponse
    {
        string Message { get; set; }
        MMI.Payment.Status.Code StatusCode { get; }
    }
}
