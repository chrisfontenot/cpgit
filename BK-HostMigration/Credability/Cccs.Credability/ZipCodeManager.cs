﻿using System.Linq;
using Cccs.Credability.Dal;

namespace Cccs.Credability
{
	public class ZipCodeManager
	{
		public static bool ZipCodeAvailable(string zipCode)
		{
			using(var context = new CredabilityDataContext())
			{
				var zipCodes = from item in context.zipcodes
							   where item.id == zipCode
							   select item;

				return zipCodes.Count() > 0;
			}
		}
	}
}