﻿using System;
using System.Data;
using Cccs.Vanco;
using Cccs.Credability.Dal;
//using Cccs.Host.Dto;
using Cccs.Debtplus.Data;

namespace Cccs.Credability
{
	public interface ICredability
	{
		RegulatedState RegulatedStateGet(string StateCode);
		void SetCounselorId(int ClientNumber);
		void SetCounselorId(int ClientNumber, int counselorID);
		int UserClientNumberGetTEST(string TempID, UserLoginTest userLogin);
		AFS AfsGet(long userId, int clientNumber);
		AFSResult AfsAddUpdate(AFS Afs);
		#region ContactDetails Interface Declaration Begin
		// Put all ContactDetails related interfasace declaration here
		#region ContactDetails Interface Declaration Begin
		// Put all ContactDetails related interfasace declaration here
		#region  Get User Information
		UserContactDetails UserInfoGet(int ClientNumber);
		#endregion

		#region
		UserContactDetailsResult UserInformationUpdate(UserContactDetails userContactDetails);
		UserContactDetailsResult UserInformationUpdateForPrePurchase(UserContactDetails userContactDetails);
		#endregion

		#region Add or Update User Situation Description
		UserSituationDescriptionResult UserSituationDescriptionUpdate(UserSituationDescription userSituationDescription);
		#endregion
		#region Get User Situation Description Info
		UserSituationDescription UserSituationDescriptionGet(int ClientNumber);
		#endregion

		#region
		UserDecsribeYourSituationHousingOnly UserDecsribeYourSituationHousingOnlyGet(int ClientNumber);
		UserDecsribeYourSituationHousingOnlyResult UserDecsribeYourSituationHousingOnlyUpdate(UserDecsribeYourSituationHousingOnly userDecsribeYourSituationHousingOnly);
		#endregion

		#region User Income Documetation
		UserMonthlyIncome UserMonthlyIncomeGet(int ClientNumber);
		UserMonthlyIncomeResult UserMonthlyIncomeUpadte(UserMonthlyIncome userDocumentationIncome);
		UserAssetsLiabilities UserAssetLiabilitiesGet(int ClientNumber);
		UserAssetsLiabilitiesResult UserAssetsLiabilitiesUpdate(UserAssetsLiabilities UserAssetNetWorths);
		#endregion


		#region Add User Monthly Expences details
		// string userMonthlyexpensesAdd(UserMonthlyexpenses userMonthlyexpenses);
		UserMonthlyExpensesResult UserMonthlyExpensesUpdate(UserMonthlyExpenses UsersMonthlyexpenses);
		UserMonthlyExpenses UserMonthlyExpensesGet(int ClientNumber);
		UserCreditorTotals CreditorTotalsGet(int ClientNumber);

		#endregion
		#endregion ContactDetails Interface Declaration End

		#region
        FinanicalSituationDetails UserFinalFinanicalSituationGet(int ClientNumber);
		//UserAnalyzingYourFinanicalSituationResult UserFinalFinanicalSituationGet(int ClientNumber);

		UserReasonForHelpResult UserReasonForHelpUpdate(UserReasonForHelp UserReasonsForHelp);
		FormSaveInforResult SaveFormInfo(FormSaveInfor FormSaveInfos);
		FormSaveInfor SavedFormInfoGet(int ClientNumber);
		#endregion

		#region UserProfile BCH
		UserProfileExtraQuest UserProfileExtraQuestGet(int ClientNumber);
		UserProfileExtraQuestResult UserProfileExtraQuestAddUpdate(UserProfileExtraQuest userProfileExtraQuest);
		#endregion UserProfile BCH End
		#endregion ContactDetails Interface Declaration End
		UserDebtListings[] UserDebtListingsGet(int ClientNumber);
		UserDebtListingsResult UserDebtListingsAddupdate(UserDebtListings UsersDebtListings);
		UserDebtListingsResult UserDebtListingsDelete(int CreditorId);

		#region Servicer
		int OtherServicerId { get; }
		bool IsOtherServicer(int servId);
		CompanyInfo[] CompanyListGet();
		string GetServicerName(int servId);
		#endregion

		#region User Debt Listings Housing Only
		UserDebtListingsHousingOnly[] UserDebtListingsHousingOnlyGet(int ClientNumber);
		UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyAddupdate(UserDebtListingsHousingOnly userDebtListingsHousingOnly);
		UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyDelete(int CreditorId);


		#endregion

		#region  ContactDetails Housing Only
		UserContactDetailsHousingOnly UserContactDetailsHousingOnlyGet(int ClientNumber);
		UserContactDetailsHousingOnlyResult UserContactDetailsHousingOnlyUpdate(UserContactDetailsHousingOnly userContactDetailsHousingOnly);
		#endregion

		#region User Situation Description Extra Quest
		UserSituationDescriptionExtraQuest UserSituationDescriptionExtraQuestGet(int ClientNumber);
		UserSituationDescriptionExtraQuestResult UserSituationDescriptionExtraQuestAddUpdate(UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest);
		#endregion

		#region ExtraHudAbt
		UserSituationDescriptionExtraHudAbt UserSituationDescriptionExtraHudAbtGet(int ClientNumber);
		UserSituationDescriptionExtraHudAbtResult UserSituationDescriptionExtraHudAbtAddUpdate(UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt);
		#endregion


		#region DecsribeYourSituationBCH
		UserDecsribeYourSituationBCH UserDecsribeYourSituationBCHGet(int ClientNumber);
		UserDecsribeYourSituationBCHResult UserDecsribeYourSituationBCHAddUpdate(UserDecsribeYourSituationBCH userDecsribeYourSituationBCH);
		#endregion

		#region User Reference Details

		UserReferenceDetails UserReferenceDetailsGet(int ClientNumber);
		UserReferenceDetailsResults UserPreHudAddUpdate(UserReferenceDetails userPrehud);
		UserReferenceDetailsResults DescribeYourSelfPreHudAddUpdate(UserReferenceDetails userPrehud);
		UserMonthlyExpenses NewMonthlyExpensesPreHudGet(int ClientNumber);
		UserReferenceDetailsResults NewMonthlyExpensesPreHudAddUpdate(UserMonthlyExpenses NewMonthlyExpenses);

		#endregion

		UserDebtCommentUpdateResult UserCommentsUpdate(UserDebtCommentUpdate DebtComment);
		UserDebtManagementComment UserCommentGet(int ClientNumber);

		#region Update AFS details for DmpOnly
		AfsDmpOnlyResult AfsDmpOnlyUpdate(AfsDmpOnly AfsDmpOnly);
		AfsDmpOnly AfsDmpOnlyGet(int ClientNumber);
		#endregion

		#region Add or Update Describing your situation details for DMP Only
		DescribingYourSituationResult DescribingYourSituationUpdate(DescribingYourSituation DescribingYourSituation);
		DescribingYourSituation DescribingYourSituationGet(int ClientNumber);
		#endregion

		#region Update CommonOption details for DmpOnly
		CommonOptionDealingDmpOnlyResult CommonOptionDealingUpdate(CommonOptionDealingDmpOnly CommonOptionDealingDmpOnly);
		CommonOptionDealingDmpOnly CommonOptionDealingDmpOnlyGet(int ClientNumber);
		#endregion

		CreditorComment CreditorCommentGet(int ClientNumber);
		CreditorCommentResult CreaditorCommentsUpdate(CreditorComment creditorComment);

		#region Fee




		WaverAppBKCounseling WaverAppBKCounselingGet(int ClientNumber);
		WaverAppBKCounselingResult WaverAppBKCounselingUpdate(WaverAppBKCounseling waverAppBKCounseling);
		Result PaymentTypeUpdate(Int32 ClientNumber);
		#endregion

		#region QuicCalc BK Counseling

		QuicalcBKCounseling QuicalcBKCounselingGet(int ClientNumber);

		QuicalcBKCounselingResult QuicalcBKCounselingUpdate(QuicalcBKCounseling quicalcBKCounseling);

		#endregion

		#region ProvideYourInfo BK Counseling

		UserContactDetailsBKCResults UserInformationUpdateForBKC(UserContactDetailsBKC userContactDetailsBKC);

		UserContactDetailsBKC BKCUserInfoGet(int ClientNumber);

		#endregion

		ZipCodes[] ZipCodeGet(string ZipNumber);

		#region User Describe Your Situation BKC

		UserDecsribeYourSituationBKCResult UserDescribeYourSituationForBKCAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC);

		UserDecsribeYourSituationBKCResult UserDescribeYourSituation2ForBKCAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC);

		UserDescribeYourSituationBKC BKCUserDescribeYourSituationGet(int ClientNumber);

		UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCAddUpdateSecEvent(UserDescribeYourSituationBKC userDescribeYourSituationBKC);

		UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCExtraQuestAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC);

		UserDescribeYourSituationBKC BKCUserDescribeYourSituationExtraQuestGet(int ClientNumber);

		UserDescribeYourSituationBKC UserDescribeYourSituationBKCExtraHudAbtGet(int ClientNumber);

		UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCExtraHudAbtAddUpdate(UserDescribeYourSituationBKC UserDescribeYourSituationBKC);

		#endregion User Describe Your Situation BKC



		#region DisclosureContactDetails BKC

		DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounselingGet(int ClientNumber);
		DisclosureContactDetailsBKCounselingResult DisclosureContactDetailsBKCounselingAddUpdate(DisclosureContactDetailsBKCounseling disclosureContactDetailsBKCounseling);
		abadetailBKCounseling AbaDetailsGet(String ABANumber);

		#endregion DisclosureContactDetails BKC

		#region DisclosureVancoRef BKC

		DisclosureVancoRefBKCounseling DisclosureVancoRefBKCounselingGet(int ClientNumber);
		IVancoRef DisclosureVancoRefAddUpdate(DisclosureVancoRefBKCounseling disclosureVancoRefBKCounseling);
		IVancoRef DisclosureVancoMimRefAddUpdate(DisclosureVancoRefBKCounseling disclosureVancoRefBKCounseling);

		#endregion DisclosureVancoRef BKC



		#region AttnyRegistration

		RegistrationBKCounseling RegistrationBKCounselingGet(int ClientNumber);
		RegistrationBKCounselingResult RegistrationBKCounselingAddUpdate(RegistrationBKCounseling registrationBKCounseling);

		#endregion

		#region CommonOption BKCounseling
		CommonOptionBKCounseling CommonOptionBKCounselingExtraHudGet(int ClientNumber);
		CommonOptionBKCounseling CommonOptionBKCounselingContactDetailsGet(int ClientNumber);
		#endregion CommonOption BKCounseling

		#region BKC Final
		FinalContactdetailBKCounseling FinalContactdetailBKCounselingGet(int ClientNumber);
		FinalContactdetailBKCounselingResult FinalContactdetailBKCounselingUpadte(FinalContactdetailBKCounseling finalContactdetailBKCounseling);
		#endregion

		#region  CommonOptionforDealingBCH
		CommonOptionforDealingBCH CommonOptionforDealingBCHGet(int ClientNumber);
		CommonOptionforDealingBCHResult CommonOptionforDealingBCHAddUpadte(CommonOptionforDealingBCH commonOptionforDealingBCH);

		#endregion  CommonOptionforDealingBCH

		ClientTypeTimeStampResult ClienttypeTimeStampAddUpdate(ClientTimeTracking ClienttypeTimeStamp);
		Result CompleteClient(int ClientNumber);
		PayementType PayementTypeGet(int ClientNumber);
		bool GetCreateNewUser(String Query);
		MySqlUsersInfoUpdateResult UserInfoMySqlUpdate(MySqlUsersInfoUpdate UserInfo);
		DataSet UserinfoMySqlGet(String RegNum);
		bool CreateNewRegNumInMySql(UserNamePasswordRegnum UserNamePassword);
		DataSet RegNumGet(String username);
		void MySqlRevCountUpdate(string username);
		DataSet MySQLUserRecordGet(String UserName);
		DataSet MySQLUserRecordGet(long RegNo);
		bool UserScoreUpdate(String UserName, String TestDTS, String PostTest);
		bool LastPagePhpPageInfoUpdate(string userName, int lastPage, int preTest);
		void FixMySqlName(long RegNo, string UserName);

		#region Vanco

		bool IsVancoActive { get; }

		VancoRef VancoRefGet(int reg_id, bool is_bkc);

		IVanco GetVancoManager(bool isBkc, bool isEnglish);
		VancoDraftSaveResult VancoDraftSave(IVanco vanco, VancoDraft vancoDraft, IVancoRef vancoRef);
		void DoHostTransactionUpdate(VancoDraft vanco_draft, IVancoRef vanco_ref);
		Result<int> VancoRefSave(IVancoRef vanco_ref);
		IVancoRef CurVancoMimRefGet(long regNum);
		Result<int> VancoMimRefSave(IVancoRef vanco_ref);

		int? CurVancoRefGet(int client_number);

		Result CurVancoRefSave(int client_number, int cur_vanco_ref);

		#endregion

		DataSet GetSQLDataset(String Query);

		CcrcHpfData CcrcHpfDataGet(int clientNumber);
		Result HpfIdSet(int ClientNumber, int HpfID);

		DebtplusPushData GetContactDetailForUv(int InNo);


        #region SysControl
        bool isChatCodeActive(string websiteCode,string Language);
      
        #endregion

    }
}