﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class RegistrationBKCounseling
    {
        public Int32 ClientNumber { get; set; }
        public string FIRMid { get; set; }
        public string ATTYSit { get; set; }
        public string ATTYname { get; set; }
        public string ATTYemail { get; set; }
        public string FIRMname { get; set; }
        public string ZIPCode { get; set; }
        public string FIRMcode { get; set; }
    }
}
