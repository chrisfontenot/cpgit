﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class DisclosureVancoRefBKCounseling
    {
        public Int32 ReqID { get; set; }
        public Int32 ClientNumber { get; set; }
        public long REGInNum { get; set; }
        public string CARDFirstNameEnc { get; set; }
        public string CARDMidNameEnc { get; set; }
        public string CARDLastNameEnc { get; set; }
        public string CARDAddrEnc { get; set; }
        public string CARDAddr2Enc { get; set; }
        public string CARDCityEnc { get; set; }
        public string CARDSTEnc { get; set; }
        public string CARDZipEnc { get; set; }
        public string CARDNumberEnc { get; set; }
        public string CARDExpMonEnc { get; set; }
        public string CARDExpYearEnc { get; set; }
        public DateTime CREATEDTS { get; set; }
        public string Status { get; set; }
        public string VanCustRef { get; set; }
        public Boolean SincWithVan { get; set; }

        
    }
}
