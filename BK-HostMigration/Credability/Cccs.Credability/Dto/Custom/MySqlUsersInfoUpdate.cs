﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
   public class MySqlUsersInfoUpdate
    {
        public Int32 RegNum { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string educationlvl { get; set; }
        public string gender { get; set; }
        public string mstatus { get; set; }
        public string age { get; set; }
        public string income { get; set; }
        public string numhousehold { get; set; }
       
    }
}
