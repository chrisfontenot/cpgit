﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class CommonOptionforDealingBCH
    {
        public Int32 ClientNumber { get; set; }
        public string Dmp_fco_pcp_flag { get; set; }
        public string Dmp_fco_pcp_date { get; set; }
        public string ContactMe { get; set; }
        public string ContactDay { get; set; }
        public string Contacttelephone { get; set; } 
        public string Contactemail { get; set; }
        public string ContactTime { get; set; }
        public string ClientType { get; set; }
        public int? CounselorId { get; set; }
        public string SpanFlag { get; set; }
    }
}
