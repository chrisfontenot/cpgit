﻿using System;
namespace Cccs.Credability
{
	public class FinanicalSituationDetails
	{
		public int ClientNumber { get; set; }
		public UserMonthlyExpenses MonthlyExpenses { get; set; }
		public UserMonthlyIncome Income { get; set; }
		public UserAssetsLiabilities AssetsLiabilities { get; set; }

		public string ReasonForhelp { get; set; }

		public double DisposableIncome
		{
			get 
			{ 
				return
					Income.NetIncomeTotalBoth
					- (double) MonthlyExpenses.ExpensesTotal
					- AssetsLiabilities.CreditorTotals.Payments; 
			}
		}
	}
}
