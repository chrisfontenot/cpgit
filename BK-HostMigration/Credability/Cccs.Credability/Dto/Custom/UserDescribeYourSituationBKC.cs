﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserDescribeYourSituationBKC
    {
        /* For Describe Your Situation */
        public Int32 ClientNumber { get; set; }
        public Int32? BkForPrv { get; set; }
        public String Bankrupt  { get; set; }
        public String ContactReason { get; set; }
        public String ContactComments { get; set; }
        public Int32? MortCurrent { get; set; }
        public decimal MosDelinq { get; set; }
        public char HousingType { get; set; }
        public Int32 SizeofHouseHold { get; set; }
        public String SecEvent { get; set; }
        /* For Describe Your Situation */

        /* For Describe Your Situation2 */
        public String MortHolder { get; set; }
	    public String LoanNumber { get; set; }
	    public String  MortType { get; set; }
	    public decimal MortYears { get; set; }
	    public String MortDate { get; set; }
        public String MortRate { get; set; }
	    public String RateType { get; set; }
	    public decimal OrigBal { get; set; }
	    public decimal OweHome { get; set; }
	    public decimal ValHome { get; set; }
	    public decimal MoPmt { get; set; }
	    public decimal AmtAvail { get; set; }
	    public String LastContactDate { get; set; }
	    public String LastContactDesc { get; set; }
	    public String RepayPlan { get; set; }
	    public String SecondaryHolder { get; set; }
	    public decimal SecondaryAmt { get; set; }
	    public String SecondaryStatus { get; set; }
	    public String Prop4Sale { get; set; }
	    public String Note4Close { get; set; }
	    public String WhoInHouse { get; set; }
	    public String LoanNumber2 { get; set; }
	    public Int32 PriServID { get; set; }
	    public Int32 SecServID { get; set; }
	    public String MakeHomeAff { get; set; }
	    public Int32 ModForm1  { get; set; }

        public String MedExp  { get; set; }
		public String LostSO  { get; set; }
		public String HealthWage  { get; set; }
		public String Unemployed  { get; set; }
		public String TaxLien  { get; set; }
        public decimal MortRate2 { get; set; }
		
        public String GovGrant  { get; set; }
		public String IncludeIns  { get; set; }
		public String IncludeTax  { get; set; }
		public decimal StartIntRate  { get; set; }
		public decimal TopIntRate  { get; set; }
		public decimal AnPropTax  { get; set; }
        public decimal AnPropIns { get; set; }

        public Int32 ServID { get; set; }
        public String ServName { get; set; }

        /* For Describe Your Situation2 */

    }
}
