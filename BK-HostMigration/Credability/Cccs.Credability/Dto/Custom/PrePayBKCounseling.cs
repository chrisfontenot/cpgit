﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class PrePayBKCounseling
    {
        public Int32 ClientNumber { get; set; }
        public string FirmId { get; set; }
        public string ATTySit { get; set; }
        public string FirmName { get; set; }
        public string AttyName { get; set; }
        public string AttyEmail { get; set; }
        
    }
}
