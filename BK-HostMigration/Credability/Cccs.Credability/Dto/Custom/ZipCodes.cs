﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace Cccs.Credability
{
    public class ZipCodes
    {
        public string ZipCodeNumber { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        
    }
}
