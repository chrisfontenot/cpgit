﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;



namespace Cccs.Credability
{

    public class UserProfileExtraQuest
    {
        public Int32 ClientNumber { get; set; }
        public string ContactEmployed { get; set; }
        public string CosignEmployed { get; set; }
        public string CoCounsPres { get; set; }
        public string PriorCouns { get; set; }
        public string PriorDMP { get; set; }
        public string RecentDMP { get; set; }
        public string ContactEducation { get; set; }
        public string MilitaryStatus { get; set; }
        public string Grade { get; set; }
        public string BranchOfService { get; set; }
        public string Rank { get; set; }  
    }
}
