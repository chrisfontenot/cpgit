﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserAssetsLiabilities
    {
        public Int32 ClientNumber { get; set; }
        public int ModForm5 { get; set; }
		public UserCreditorTotals CreditorTotals { get; set; }

		public float ValHome { get; set; }
        public float ValCar { get; set; }
        public float ValOther { get; set; }
        public float ValSav { get; set; }
        public float ValProp { get; set; }
        public float ValRet { get; set; }
        public float OweHome { get; set; }
        public float OweCar { get; set; }
        public float OweOther { get; set; }
        public float SecondaryAmt { get; set; }
        public float MortRate { get; set; }
        public string HousingType { get; set; }

		public double Liabilities
		{
			get
			{
				return
					OweCar
					+ OweHome
					+ OweOther
					+ SecondaryAmt;
			}
		}

		public double Assets
		{
			get
			{
				return
					ValHome
					+ ValCar
					+ ValOther
					+ ValSav
					+ ValProp
					+ ValRet;
			}
		}

		public double TotalDebt
		{
			get
			{
				return
					CreditorTotals.Balance
					+ Liabilities;
			}
		}

		public double NetWorth
		{
			get
			{
				return
					Assets
					- TotalDebt;
			}
		}
	}
}
