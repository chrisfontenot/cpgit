﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class WaiverWaiver
    {
        public Int32 FwID { get; set; }
        public Int32 ClientNumber { get; set; }
        public int  SIgnUp { get; set; }
        public string Pri4SsnEnc { get; set; }
        public string Sec4SsnEnc { get; set; }
        public string PriZipEnc { get; set; }
        public string SecZipEnc { get; set; }
        public int JointFile { get; set; }
        public DateTime LastModDTS { get; set; }
        public DateTime SignUpDTS { get; set; }
        

    }
}
