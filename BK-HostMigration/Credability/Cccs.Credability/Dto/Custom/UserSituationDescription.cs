﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
    [DataContract]
    public class UserSituationDescription
    {
        [DataMember]
        public Int32 ClientNumber { get; set; }
        [DataMember]
        public string ContactReason { get; set; }
        [DataMember]
        public string ContactComments { get; set; }
        [DataMember]
        public int MortCurrent { get; set; }
        [DataMember]
        public string HousingType { get; set; }
        [DataMember]
        public int MosDelinq { get; set; }
        [DataMember]
        public int SizeOfHouseHold { get; set; }

        
        
    }
}
