﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class FeeWaiverAFS
    {
        public string ClientType { get; set; }
        public Int32 ClientNumber { get; set; }
        public Int32 HostId { get; set; }
        public string Language { get; set; }
        
    }
}
