﻿using System;

namespace Cccs.Credability
{
	public class UserMonthlyIncome
	{
		public Int32 ClientNumber { get; set; }

		public float GrossIncome { get; set; }
		public float GrossIncome2 { get; set; }
		public float NetIncome { get; set; }
		public float NetIncome2 { get; set; }
		public float PartTimeGrossIncome { get; set; }
		public float PartTimeGrossIncome2 { get; set; }
		public float PartTimeNetIncome { get; set; }
		public float PartTimeNetIncome2 { get; set; }
		public float AlimonyIncome { get; set; }
		public float AlimonyIncome2 { get; set; }
		public float ChildSupportIncome { get; set; }
		public float ChildSupportIncome2 { get; set; }
		public float GovAssistIncome { get; set; }
		public float GovAssistIncome2 { get; set; }
		public float OtherIncome { get; set; }
		public float OtherIncome2 { get; set; }

		public double NetIncomeTotalBoth
		{
			get
			{
				return
					NetIncome
					+ NetIncome2
					+ PartTimeNetIncome
					+ PartTimeNetIncome2
					+ AlimonyIncome
					+ AlimonyIncome2
					+ ChildSupportIncome
					+ ChildSupportIncome2
					+ GovAssistIncome
					+ GovAssistIncome2
					+ OtherIncome
					+ OtherIncome2;
			}
		}

		public double NetIncomeTotal
		{
			get
			{
				return
					 NetIncome
					 + PartTimeNetIncome
					 + AlimonyIncome
					 + ChildSupportIncome
					 + GovAssistIncome
					 + OtherIncome;
			}
		}

		public double NetIncomeTotal2
		{
			get
			{
				return
					 NetIncome2
					 + PartTimeNetIncome2
					 + AlimonyIncome2
					 + ChildSupportIncome2
					 + GovAssistIncome2
					 + OtherIncome2;
			}
		}

		public double GrossIncomeTotalBoth
		{
			get
			{
				return
					GrossIncome
					+ GrossIncome2
					+ PartTimeGrossIncome
					+ PartTimeGrossIncome2
					+ AlimonyIncome
					+ AlimonyIncome2
					+ ChildSupportIncome
					+ ChildSupportIncome2
					+ GovAssistIncome
					+ GovAssistIncome2
					+ OtherIncome
					+ OtherIncome2;
			}
		}

		public double GrossIncomeTotal
		{
			get
			{
				return
					GrossIncome
					+ PartTimeGrossIncome
					+ AlimonyIncome
					+ ChildSupportIncome
					+ GovAssistIncome
					+ OtherIncome;
			}
		}

		public double GrossIncomeTotal2
		{
			get
			{
				return
					GrossIncome2
					+ PartTimeGrossIncome2
					+ AlimonyIncome2
					+ ChildSupportIncome2
					+ GovAssistIncome2
					+ OtherIncome2;
			}
		}
	}
}
