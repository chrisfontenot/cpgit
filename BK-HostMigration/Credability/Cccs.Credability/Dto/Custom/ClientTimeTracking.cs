﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
	public class ClientTimeTracking
	{
		public int ClientNumber { get; set; }
		public string ConfirmIpAddress { get; set; }
		public int TotalMinuteOnPage { get; set; }
	}
}
