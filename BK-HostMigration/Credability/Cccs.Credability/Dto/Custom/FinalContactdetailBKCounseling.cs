﻿using System;

namespace Cccs.Credability
{
	public class FinalContactdetailBKCounseling
	{
		// ContactDetails
		public Int32 ClientNumber { get; set; }
		public string Contact_firstname { get; set; }
		public string Contact_initial { get; set; }
		public string Contact_lastname { get; set; }
		public string Cosign_firstname { get; set; }
		public string Cosign_initial { get; set; }
		public string Cosign_lastname { get; set; }
		public string Confirm_ipaddress { get; set; }
		public string ClientType { get; set; }
		public string Span_flag { get; set; }
		public DateTime Confirm_datetime { get; set; }
	}
}
