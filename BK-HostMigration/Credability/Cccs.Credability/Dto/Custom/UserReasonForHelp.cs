﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserReasonForHelp
    {
       public int ClientNumber { get; set; }
       public string ReasonForHelp { get; set; }
       public decimal MonthlyDisposableIncome { get; set; }
       public float MonthlyExpenses { get; set; }
       
           
    }
}
