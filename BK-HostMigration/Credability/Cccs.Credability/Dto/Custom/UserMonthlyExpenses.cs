﻿using System;

namespace Cccs.Credability
{
	public class UserMonthlyExpenses
	{
		public int ClientNumber { get; set; }
		//Monthly Food & Shelter
		public decimal RentMort { get; set; }

		public string IncludeFha { get; set; }
		public decimal Mort2nd { get; set; }
		public float MoEquity { get; set; }
		public float Mo2ndMort { get; set; }

		public float AnPropIns { get; set; }
		public float MoPropIns { get { return AnPropIns / 12; } set { AnPropIns = (value * 12); } }
		public float AnPropTax { get; set; }
		public float MoPropTax { get { return AnPropTax / 12; } set { AnPropTax = (value * 12); } }
		public float MoFee { get; set; }
		public decimal HomeMaintenance { get; set; }

		public float UtlElectric { get; set; }
		public float UtlWater { get; set; }
		public float UtlGas { get; set; }
		public float UtlTv { get; set; }
		public float UtlTrash { get; set; }
		public decimal Telephone { get; set; }
		public decimal FoodAway { get; set; }
		public decimal Groceries { get; set; }

		//Monthly Transportation
		public int CarCurrent { get; set; }
		public decimal CarPayments { get; set; }
		public decimal CarInsurance { get; set; }
		public decimal CarMaintenance { get; set; }
		public decimal PublicTransportation { get; set; }

		//Monthly Insurance, Medical & Childcare
		public decimal Insurance { get; set; }
		public decimal MedicalPrescription { get; set; }
		public decimal ChildSupportAlimony { get; set; }
		public decimal ChildElderCare { get; set; }
		public decimal Education { get; set; }

		//Monthly Personal & Miscellaneous Expenses
		public int OtherLoans { get; set; }
		public decimal Contributions { get; set; }
		public decimal Clothing { get; set; }
		public decimal Laundry { get; set; }
		public decimal PersonalExpenses { get; set; }
		public decimal BeautyBarber { get; set; }
		public decimal Recreation { get; set; }
		public decimal ClubDues { get; set; }
		public decimal Gifts { get; set; }
		public decimal Miscellaneous { get; set; }

		public decimal ExpensesTotal
		{
			get
			{
				return
					RentMort
					+ (decimal) MoEquity
					+ (decimal) Mo2ndMort
					+ (decimal) MoPropIns
					+ (decimal) MoPropTax
					+ (decimal) MoFee
					+ HomeMaintenance
					+ Utilities
					+ Telephone
					+ FoodAway
					+ Groceries
					+ CarPayments
					+ CarInsurance
					+ CarMaintenance
					+ PublicTransportation
					+ Insurance
					+ MedicalPrescription
					+ ChildSupportAlimony
					+ ChildElderCare
					+ Education
					+ Contributions
					+ PersonalExpenses
					+ Clothing
					+ Laundry
					+ BeautyBarber
					+ Recreation
					+ ClubDues
					+ Gifts
					+ Miscellaneous
					+ OtherLoans;
			}
		}

		public decimal Utilities
		{
			get
			{
				return
					(decimal) UtlElectric
					+ (decimal) UtlWater
					+ (decimal) UtlGas
					+ (decimal) UtlTv
					+ (decimal) UtlTrash;
			}
		}
	}
}