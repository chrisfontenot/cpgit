﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class CreditorComment
    {
        public int ClientNumber { get; set; }
        public string CreditorsComment { get; set; }
        public int SizeOfHouseHold { get; set; }
    }
}
