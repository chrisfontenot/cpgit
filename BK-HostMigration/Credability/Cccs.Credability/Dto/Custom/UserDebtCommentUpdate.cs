﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
   public  class UserDebtCommentUpdate
    {
       public int ClientNumber { get; set; }
       public string UserComment { get; set; }
       public int ModForm { get; set; }
    }
}
