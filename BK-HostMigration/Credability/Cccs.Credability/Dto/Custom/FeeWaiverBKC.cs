﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class FeeWaiverBKC
    {
        public Int32 FwID { get; set; }
        public Int32 SignUp { get; set; }
        public Int32 Approved { get; set; }
        public string WAiverType { get; set; }
        public Int32 LastModCouns { get; set; }
    }

}
