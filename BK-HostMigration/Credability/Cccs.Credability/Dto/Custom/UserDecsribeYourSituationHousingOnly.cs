﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserDecsribeYourSituationHousingOnly
    {
        public Int32 ClientNumber { get; set; }
        public string ContactReason { get; set; }
        public string ContactComments { get; set; }
        public int MortCurrent { get; set; }
        public string HousingType { get; set; }
        public float MosDelinq { get; set; }
        public int SizeOfHouseHold { get; set; }
        public string MortHolder { get; set; }
        public string LoanNumber { get; set; }
        public string MortType { get; set; }
        public float MortYears { get; set; }
        public string MortDate { get; set; }
        public float MortRate { get; set; }
        public string RateType { get; set; }
        public string LastContactDesc { get; set; }
        public float OrigBal { get; set; }
        public float Owehome { get; set; }
        public float ValHome { get; set; }
        public float MoPmt { get; set; }
        public float AmtAvail { get; set; }
        public string RepayPlan { get; set; }
        public int PRIServID { get; set; }
        public int SECServID { get; set; }
        public float SecondaryAmt { get; set; }
        public string LastContactDate { get; set; }
        public string ContactState { get; set; }
        public string PROP4Sale { get; set; }
        public string NOTE4Close { get; set; }
        public string WHOInHouse { get; set; }
        public string LoanNumber2 { get; set; }
        public string MAKEHomeAff { get; set; }
        public string ClientType { get; set; }
        public string SecondaryHolder { get; set; }
        public string SecondaryStatus { get; set; }
    }
}
