﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class AfsDmpOnly
    {
        public int ClientNumber { get; set; }
        public string PrimaryPerson { get; set; }
        public string SecondaryPerson { get; set; }
        public string TermsText { get; set; }
    }
}
