﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class DescribingYourSituation
    {
        public string FinancesOfftrack { get; set; }
        public string DescribeComment { get; set; }
        public string RentingorBuyingHouse { get; set; }
        public string LastContactDesc { get; set; }
        public string ContactReason { get;set;}
        
        public int  ClientNumber { get; set; }
        public string CompanyName { get; set; }
        public string LoanNumber { get; set; }
        public string MortgageType { get; set; }
        public string MortgageTerm { get; set; }
        public string DateofMortgage { get; set; }
        public string InterestRate { get; set; }
        public string InterestType { get; set; }
        public string OriLoanBalance { get; set; }
        public string CurrBalance { get; set; }
        public string EstimateValue { get; set; }
        public string MonthlyPayment { get; set; }
        public string InsurancePayment { get; set; }
        public string PeopleLivingIn { get; set; }


        //Current Loan Status
        public string AreyoucurrentonyourPayments { get; set; }
        public string IfNohowmanymonths { get; set; }
        public string CurrloanMortCompany { get; set; }
        public string replacementPlan { get; set; }
        public string Discussions { get; set; }
        public string LastContactDate { get; set; }
        

        //Second / Home Equity Loan
        public string Secondhomeequityloan { get; set; }
        public string SecondCompanyName { get; set; }
        public string SecondLoanBalance { get; set; }
        public string SecondLoanStatus { get; set; }    
 
    }
}
