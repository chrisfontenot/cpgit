﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class FeeWaiverForAprWait
    {
        public Int32 FwID { get; set; }
        public string CounsNotes { get; set; }
        public Int32 Aproved { get; set; }
        public Int32 SignUp { get; set; }
        public Int32 FeePolicy { get; set; }
    }
}
