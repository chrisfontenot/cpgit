﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class DisclosureContactDetailsBKCounseling
    {
        public Int32 ClientNumber { get; set; }
        public string ADSbankphone { get; set; }
        public string ADSbankname { get; set; }
        public string adsNAMEONCHECK { get; set; }
        public string paymentTYPE { get; set; }
        public string abaNUMBER { get; set; }
        public string acctNUMBER { get; set; }
        public string dcFNAMEONCARD { get; set; }
        public string dcMNAMEONCARD { get; set; }
        public string dcLNAMEONCARD { get; set; }
        public string dcBILLADDR { get; set; }
        public string dcBILLADDR2 { get; set; }
        public string dcBILLCITY { get; set; }
        public string dcBILLSTATE { get; set; }
        public string dcBILLZIP { get; set; }
        public string dcACCTNUM { get; set; }
        public string dcEXPDATE { get; set; }
        public string mtcn { get; set; }
        public Int32 CurVancoRef { get; set; }
        public Int32 Completed { get; set; }
        public Int32 Processed { get; set; }


    }
}
