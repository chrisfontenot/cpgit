﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
	public class UserReferenceDetails
	{
		public Int32 ClientNumber { get; set; }
		public string RefInstitutionName { get; set; }
		public string RefContactName { get; set; }
		public string RefContactEmail { get; set; }

		public bool BegPro { get; set; }
		public bool AstPay { get; set; }
		public bool BenAprov { get; set; }
		public bool HaveCloDate { get; set; }
		public string TheCloDate { get; set; }
		public string HadHouse { get; set; }
		public string FndHouse { get; set; }
		public string HouseAdr { get; set; }
		public string HouseCity { get; set; }
		public string HouseSt { get; set; }
		public string HouseZip { get; set; }
		public decimal HousePrice { get; set; }
	}
}