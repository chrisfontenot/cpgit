﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
	public class CcrcHpfData
	{
		public int? HpfID { get; set; }
		public DateTime CreateDTS { get; set; }
		public string CcrcAprov { get; set; }
	}
}
