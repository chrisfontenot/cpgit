﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserDebtManagementComment
    {
        public int ClientNumber { get; set; }
        //public int ModForm { get; set; }
        public string UserComments { get; set; }
    }
}
