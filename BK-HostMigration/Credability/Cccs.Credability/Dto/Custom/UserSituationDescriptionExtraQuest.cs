﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace Cccs.Credability
{
    public class UserSituationDescriptionExtraQuest
    {
        public Int32 ClientNumber { get; set; }
        public string SECEvent { get; set; }
        public string MEDExp { get; set; }
        public string LOSTSO { get; set; }
        public string HEALTHWage { get; set; }
        public string UNEmployed { get; set; }
        public string TAXLien { get; set; }
        public string MORTRate { get; set; }
        public string LOANModify {get;set;}
        public string ASSISTModify { get; set; }
        // Hamp Questions
        public string MortgageModifiedHamp { get; set; }
        public string MortgageInvestor { get; set; }
        public string IsBusinessOrPersonalMortgage { get; set; }
        public string HousingType { get; set; }
        public string AgreeEscrowAccount { get; set; }

        // Hamp 2 tier Questions
        public string ConvictedFelony { get; set; }
        public string IsLoanFirstLienFMorFMae { get; set; }

        // Harp Questions
        public string LoanSoldFMorFmae { get; set; }
        public string LoanRefinanceProgram { get; set; }

        // Short Sale/Deed in Lieu

      
        public string AnyLiens { get; set; }
        
        // Fp - Housing only questions
        public string ClientGoals { get; set; }
        public string TimeLineEvents { get; set; }

       


    }
}
