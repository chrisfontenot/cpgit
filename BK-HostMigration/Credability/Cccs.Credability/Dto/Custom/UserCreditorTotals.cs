﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
	public class UserCreditorTotals
	{
		public double Payments { get; set; }
		public double Balance { get; set; }
		public int Count { get; set; }
	}
}
