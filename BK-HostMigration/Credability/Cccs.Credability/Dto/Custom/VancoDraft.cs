﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
	public class VancoDraft
	{
		public int ClientNumber { get; set; }

		public string DcFNameOnCard { get; set; } 
		public string DcMNameOnCard { get; set; }  
		public string DcLNameOnCard { get; set; }  
		public string DcBillAddr1 { get; set; }  
		public string DcBillAddr2 { get; set; }  
		public string DcBillCity { get; set; }  
		public string DcBillState { get; set; }  
		public string DcBillZip { get; set; }
		public string DcAcctNum { get; set; }
		public int CardExpMon { get; set; }  
		public int CardExpYear { get; set; }
        public double CharAmount { get; set; }
	}
}
