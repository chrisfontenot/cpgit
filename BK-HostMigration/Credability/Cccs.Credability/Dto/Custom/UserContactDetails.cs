﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
	public class UserContactDetails
	{
		public int ClientNumber { get; set; }
        public int? CounselorID { get; set; }
        public string CosignBirthOfCity { get; set; }
		public string BirthOfCity { get; set; }
		public string ContactTelephone { get; set; }
		public string CcrcAprov { get; set; }
		public int NumberOfPeople { get; set; }
		public string RefCode { get; set; }
        public string ClientType { get; set; }

        public string ZipCode { get; set; }
        public string FirmID { get; set; }

		#region The following fields should come from IDM Only
		//public string FirstName { get; set; }
		//public string LastName { get; set; }
		//public string Initial { get; set; }
		//public string Address { get; set; }
		//public string Address2 { get; set; }
		//public string City { get; set; }
		//public string State { get; set; }
		//public string Zip { get; set; }
		//public string Ssn { get; set; }
		//public string Dob { get; set; }
		//public string Marital { get; set; }
		//public string Sex { get; set; }
		//public string Race { get; set; }
		//public string Hispanic { get; set; }
		//public string Telephone { get; set; }
		//public string SecTelephone { get; set; }
		//public string Email { get; set; }
		//public string CosignFirstName { get; set; }
		//public string CosignInitial { get; set; }
		//public string CosignLastname { get; set; }
		//public string CosignSsn { get; set; }
		//public string CosignDob { get; set; }
		//public string CosignMarital { get; set; }
		//public string CosignSex { get; set; }
		//public string CosignRace { get; set; }
		//public string CosignHispanic { get; set; }
		//public string OtherTelephone { get; set; }
		//public string EmailAddress { get; set; }
		//public byte[] ContactAddressEnc { get; set; }
		//public byte[] ContactCityEnc { get; set; }
		//public byte[] ContactFirstNameEnc { get; set; }
		//public byte[] ContactInitialEnc { get; set; }
		//public byte[] ContactLastNameEnc { get; set; }
		//public byte[] ContactZipEnc { get; set; }
		//public byte[] ContactStateEnc { get; set; }
		//public byte[] ContactTelephoneEnc { get; set; }
		//public byte[] ContactSsnEnc { get; set; }
		//public byte[] ContactOtherTelephoneEnc { get; set; }
		//public byte[] CosignFirstNameEnc { get; set; }
		//public byte[] CosignInitialEnc { get; set; }
		//public byte[] CosignLastNameEnc { get; set; }
		//public byte[] CosignSsnEnc { get; set; }
		//public byte[] ContactAddress2Enc { get; set; }
		//public byte[] RealEmailEnc { get; set; }	}
		#endregion
	}
}
