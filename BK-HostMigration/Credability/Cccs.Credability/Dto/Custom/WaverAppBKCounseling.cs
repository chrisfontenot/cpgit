﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class WaverAppBKCounseling
    {
        public Int32 ClientNumber { get; set; }
        public string ContactFirstname { get; set; }
        public string ContactLastname { get; set; }
        public string ContactAddress { get; set; }
        public string ContactAddress2 { get; set; }
        public string ContactCity { get; set; }
        public string ContactState { get; set; }
        public string ContactZip { get; set; }
        public string ContactTelephone { get; set; }
        public string RealEmail { get; set; }
        public string CosignFirstname { get; set; }
        public string CosignLastname { get; set; }
        

    }
}
