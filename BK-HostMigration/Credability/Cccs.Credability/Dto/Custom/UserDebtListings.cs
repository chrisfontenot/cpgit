﻿using System;

namespace Cccs.Credability
{
	public class UserDebtListings
	{
		public int CreditorId { get; set; }
		public int ClientNumber { get; set; }
		public string CreditorName { get; set; }
		public float CreditorBal { get; set; }
		public float CreditorPayments { get; set; }
		public string creditorPastDue { get; set; }
		public byte[] PreAcntNoEnc { get; set; }
		public byte[] CrAcntNoEnc { get; set; }
		public string PreAcntNoEncs { get; set; }
		public string CrAcntNoEncs { get; set; }
		public string CreditorJointAcct { get; set; }
		public DateTime LastModDTS { get; set; }
		public float CreditorIntrate { get; set; }
		public string PriAcctHolder { get; set; }
	}
}