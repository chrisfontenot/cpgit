﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class CommonOptionBKCounseling
    {
        //contactdetails
        public Int32 ClientNumber { get; set; }
        public char HousingType { get; set; }
        public String MortHolder { get; set; }
        public String LoanNumber { get; set; }
        public String MortType { get; set; }
        public decimal MortYears { get; set; }
        public String MortDate { get; set; }
        public decimal MortRate { get; set; }
        public String RateType { get; set; }
        public String ContactReason { get; set; }
        public String ContactComments { get; set; }
        public String LastContactDesc { get; set; }
        public decimal OrigBal { get; set; }
        public decimal OweHome { get; set; }
        public decimal ValHome { get; set; }
        public decimal MoPmt { get; set; }
        public String Includeti { get; set; }
        public decimal MosDelinq { get; set; }
        public decimal AmtAvail { get; set; }
        public String RepayPlan { get; set; }
        public String SecondaryHolder { get; set; }
        public decimal SecondaryAmt { get; set; }
        public String SecondaryStatus { get; set; }
        public Int32 SizeofHouseHold { get; set; }
        public Int32 MortCurrent { get; set; }
        public String LastContactDate { get; set; }
        public String CcrcAprov { get; set; }
        public String Prop4Sale { get; set; }
        public String Note4Close { get; set; }
        public String WhoInHouse { get; set; }
        public String LoanNumber2 { get; set; }
        public String Contactstate { get; set; }
        public String Contactzip { get; set; }

        //ExtraHudAbt
        public String GovGrant { get; set; }
        public String IncludeIns { get; set; }
        public String IncludeTax { get; set; }
        public decimal StartIntRate { get; set; }
        public decimal TopIntRate { get; set; }
        public decimal AnPropTax { get; set; }
        public decimal AnPropIns { get; set; }

        public Int32 SecServID { get; set; }
        

    }
}
