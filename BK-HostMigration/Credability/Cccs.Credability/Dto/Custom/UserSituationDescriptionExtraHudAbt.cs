﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
    public class UserSituationDescriptionExtraHudAbt
    {
        public Int32 ClientNumber { get; set; }
        public string GOVGrant { get; set; }
        public float STARTIntRate { get; set; }
        public float TOPIntRate { get; set; }
        public float ANPropTax { get; set; }
        public float ANPropIns { get; set; }
        public string INCludeIns { get; set; }
        public string INCludeTax { get; set; }
        



    }
}
