﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserDebtListingsHousingOnlyResult:Result
    {
        public int CreditorId { get; set; }
        public int ClientNumber { get; set; }

    }
}
