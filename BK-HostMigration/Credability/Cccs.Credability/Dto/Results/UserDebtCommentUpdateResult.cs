﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class UserDebtCommentUpdateResult:Result
    {
        public int ClientNumber { get; set; }
    }
}
