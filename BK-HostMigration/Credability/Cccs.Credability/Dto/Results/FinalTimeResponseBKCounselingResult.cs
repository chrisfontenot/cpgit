﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability
{
    public class FinalTimeResponseBKCounselingResult:Result
    {
        public Int32 ClientNumber { get; set; }
        public Int32 Timeid { get; set; }

    }
}
