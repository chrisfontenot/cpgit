﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
    public class UserSituationDescriptionExtraQuestResult : Result
    {
        public Int32 ClientNumber { get; set; }
    }
}
