﻿using System;

namespace Cccs.Credability
{
	public class UserAssetsLiabilitiesResult : Result
	{
		public Int32 ClientNumber { get; set; }
	}
}