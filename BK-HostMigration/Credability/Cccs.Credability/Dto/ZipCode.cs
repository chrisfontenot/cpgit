﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
	[DataContract]
	public class ZipCode
	{
		[DataMember]
		public string Zip { get; set; }

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public string State { get; set; }
	}
}
