﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Credability
{
	[DataContract]
	public class VancoRef
	{
		#region Constants

		public const string STATUS_PAYED     = "Payed";
		public const string STATUS_OLD       = "Old";
		public const string STATUS_NOT_PAYED = "NotPayed";

		public const string ERROR_ON_CUST = "Cust";
		public const string ERROR_ON_PAYMET = "PayMet";
		public const string ERROR_ON_TRANS = "Trans";

		#endregion

		[DataMember]
		public int ReqID { get; set; }

		[DataMember]
		public long? RegInNum { get; set; }

		[DataMember]
		public string VanCustRef { get; set; }

		[DataMember]
		public string VanPayMethRef { get; set; }

		[DataMember]
		public string VanTransRef { get; set; }

		[DataMember]
		public DateTime? CreateDTS { get; set; }

		[DataMember]
		public DateTime? TransDTS { get; set; }

		[DataMember]
		public string CardFirstName { get; set; }

		[DataMember]
		public string CardMidName { get; set; }

		[DataMember]
		public string CardLastName { get; set; }

		[DataMember]
		public string CardAddr { get; set; }

		[DataMember]
		public string CardAddr2 { get; set; }

		[DataMember]
		public string CardCity { get; set; }

		[DataMember]
		public string CardST { get; set; }

		[DataMember]
		public string CardZip { get; set; }

		[DataMember]
		public string CardPhone { get; set; }

		[DataMember]
		public string CardNumber { get; set; }

		[DataMember]
		public string CardExpMon { get; set; }

		[DataMember]
		public string CardExpYear { get; set; }

		[DataMember]
		public string Status { get; set; }

		[DataMember]
		public bool? SincWithVan { get; set; }

		[DataMember]
		public int? LastErrCode { get; set; }

		[DataMember]
		public string ErrorOn { get; set; }

        [DataMember]
        public bool IsBkc { get; set; }
	}
}
