﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using System.Data.Linq;

namespace Cccs.Credability
{
    public class FeeWaiverManager
    {
        public static Binary EncryptText(string clearText)
        {
            using (var context = new CredabilityDataContext())
            {
                return context.EncryptText(clearText);
            }
        }

        public FeeWaiver AddFeeWaiver(FeeWaiver feeWaiver)
        {
            using (var context = new CredabilityDataContext())
            {
                context.FeeWaivers.InsertOnSubmit(feeWaiver);
                context.SubmitChanges();
            }

            return feeWaiver;
        }

        public FeeWaiver UpdateFeeWaiver(FeeWaiver feeWaiver)
        {
            using (var context = new CredabilityDataContext())
            {
                var updateFeeWaiver = (from item in context.FeeWaivers
                                       where item.FwID == feeWaiver.FwID
                                       select item).First();
                UpdateValues(updateFeeWaiver, feeWaiver);
                context.SubmitChanges();

                return updateFeeWaiver;
            }
        }

        private static void UpdateValues(FeeWaiver newFeeWaiver, FeeWaiver oldFeeWaiver)
        {
            newFeeWaiver.Aproved = oldFeeWaiver.Aproved;
            newFeeWaiver.BaseSt = oldFeeWaiver.BaseSt;
            newFeeWaiver.client_number = oldFeeWaiver.client_number;
            newFeeWaiver.ClientType = oldFeeWaiver.ClientType;
            newFeeWaiver.CounsNotes = oldFeeWaiver.CounsNotes;
            newFeeWaiver.DecisionDTS = oldFeeWaiver.DecisionDTS;
            newFeeWaiver.FeePolicy = oldFeeWaiver.FeePolicy;
            newFeeWaiver.FeePolicyDTS = oldFeeWaiver.FeePolicyDTS;
            newFeeWaiver.GotPri1040Doc = oldFeeWaiver.GotPri1040Doc;
            newFeeWaiver.GotPri1040DTS = oldFeeWaiver.GotPri1040DTS;
            newFeeWaiver.GotPriPayStubDoc = oldFeeWaiver.GotPriPayStubDoc;
            newFeeWaiver.GotPriPayStubDTS = oldFeeWaiver.GotPriPayStubDTS;
            newFeeWaiver.GotProBonoDoc = oldFeeWaiver.GotProBonoDoc;
            newFeeWaiver.GotProBonoDTS = oldFeeWaiver.GotProBonoDTS;
            newFeeWaiver.GotScheduleIDoc = oldFeeWaiver.GotScheduleIDoc;
            newFeeWaiver.GotScheduleIDTS = oldFeeWaiver.GotScheduleIDTS;
            newFeeWaiver.GotSec1040Doc = oldFeeWaiver.GotSec1040Doc;
            newFeeWaiver.GotSec1040DTS = oldFeeWaiver.GotSec1040DTS;
            newFeeWaiver.GotSecPayStubDoc = oldFeeWaiver.GotSecPayStubDoc;
            newFeeWaiver.GotSecPayStubDTS = oldFeeWaiver.GotSecPayStubDTS;
            newFeeWaiver.GotSsdiDoc = oldFeeWaiver.GotSsdiDoc;
            newFeeWaiver.GotSsdiDTS = oldFeeWaiver.GotSsdiDTS;
            newFeeWaiver.GrossIncome = oldFeeWaiver.GrossIncome;
            newFeeWaiver.JointFile = oldFeeWaiver.JointFile;
            newFeeWaiver.JointID = oldFeeWaiver.JointID;
            newFeeWaiver.Language = oldFeeWaiver.Language;
            newFeeWaiver.LastModCouns = oldFeeWaiver.LastModCouns;
            newFeeWaiver.LastModDTS = oldFeeWaiver.LastModDTS;
            newFeeWaiver.NumDep = oldFeeWaiver.NumDep;
            newFeeWaiver.Pri4SsnEnc = oldFeeWaiver.Pri4SsnEnc;
            newFeeWaiver.PriFName = oldFeeWaiver.PriFName;
            newFeeWaiver.PriLName = oldFeeWaiver.PriLName;
            newFeeWaiver.PriZipEnc = oldFeeWaiver.PriZipEnc;
            newFeeWaiver.ResnForDny = oldFeeWaiver.ResnForDny;
            newFeeWaiver.Sec4SsnEnc = oldFeeWaiver.Sec4SsnEnc;
            newFeeWaiver.SecFName = oldFeeWaiver.SecFName;
            newFeeWaiver.SecLName = oldFeeWaiver.SecLName;
            newFeeWaiver.SecZipEnc = oldFeeWaiver.SecZipEnc;
            newFeeWaiver.SignUp = oldFeeWaiver.SignUp;
            newFeeWaiver.SignUpDTS = oldFeeWaiver.SignUpDTS;
            newFeeWaiver.WaitPolicy = oldFeeWaiver.WaitPolicy;
            newFeeWaiver.WaiverType = oldFeeWaiver.WaiverType;
        }

        public FeeWaiver GetByFeeWaiverId(int feeWaiverId)
        {
            using (var context = new CredabilityDataContext())
            {
                var feeWaiver = (from item in context.FeeWaivers
                                 where item.FwID == feeWaiverId
                                 select item).First();

                return feeWaiver;
            }
        }

        public FeeWaiver GetByClientNumber(int clientNumber)
        {
            using (var context = new CredabilityDataContext())
            {
                var feeWaiver = (from item in context.FeeWaivers
                                 where item.client_number == clientNumber 
                                 select item).FirstOrDefault();

                return feeWaiver;
            }
        }

        public FeeWaiver GetByEducationClient(int hostId)
        {
            using (var context = new CredabilityDataContext())
            {
                var feeWaiver = (from item in context.FeeWaivers
                                 where item.client_number == hostId && item.ClientType == "Edu"
                                 select item).FirstOrDefault();

                return feeWaiver;
            }
        }
    }
}
