﻿using System;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Dal;
using Cccs.Identity.Dal;
using System.Collections.Generic;

namespace Cccs.Credability.Certificates.AutoMapperProfiles
{
    public class PreFilingSnapshotProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "PreFilingSnapshotProfile";
            }
        }

        protected override void Configure()
        {
            CreateMap<Account, PreFilingAccountDto>();

            RepositoryMaps();
            ServiceMaps();
        }

        private void RepositoryMaps()
        {
            CreateMap<UserDetail, IUserInfo>()
                .ForMember(d => d.Ssn, o => o.Ignore())
                .ForMember(d => d.LanguageCode, o => o.Ignore());
            CreateMap<Address, IAddressInfo>()
                .ForMember(d => d.PostalCode, o => o.MapFrom(s => s.Zip));
            CreateMap<Account, IAccountInfo>()
                .ForMember(d => d.ClientNumber, o => o.MapFrom(s => s.InternetId))
                .ForMember(d => d.CounselorName, o => o.MapFrom(s => s.CounselorName));

            CreateMap<contactdetail, IContactDetailInfo>()
                .ForMember(d => d.ClientNumber, o => o.MapFrom(s => s.client_number))
                .ForMember(d => d.Reason, o => o.MapFrom(s => s.contact_reason))
                .ForMember(d => d.CourseCompleted, o => o.MapFrom(s => s.confirm_datetime.Value))
                .ForMember(d => d.IncomeAfterTaxes, o => o.ResolveUsing<ContactDetailIncomeAfterTaxesResolver>())
                .ForMember(d => d.LivingExpenses, o => o.ResolveUsing<ContactDetailLivingExpensesResolver>())
                .ForMember(d => d.TotalAssets, o => o.ResolveUsing<ContactDetailTotalAssetsResolver>())
                .ForMember(d => d.TotalLiabilities, o => o.ResolveUsing<ContactDetailTotalLiabilitiesResolver>());

            CreateMap<creditordetail, PreFilingContactDetailCreditorDto>()
                .ForMember(d => d.CreditorId, o => o.MapFrom(s => s.id))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.creditor_name))
                .ForMember(d => d.Balance, o => o.ResolveUsing<CreditorBalanceResolver>())
                .ForMember(d => d.Payment, o => o.ResolveUsing<CreditorPaymentResolver>())
                .ForMember(d => d.InterestRate, o => o.ResolveUsing<CreditorInterestRateResolver>());

            CreateMap<PreFilingSnapshotDto, PreFilingSnapshot>()
                .ForMember(d => d.PreFilingSnapshotId, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshotCreditors, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshotExpenseCategories, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshotExpenseCodes, o => o.Ignore())
                .ForMember(d => d.UserDetail, o => o.Ignore())
                .ForMember(d => d.PreFilingActionPlans, o => o.Ignore())
                .ForMember(d => d.PreFilingActionPlans1, o => o.Ignore());
            CreateMap<PreFilingSnapshotCreditorDto, PreFilingSnapshotCreditor>()
                .ForMember(d => d.PreFilingSnapshotId, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshot, o => o.Ignore());
            CreateMap<PreFilingSnapshotExpenseCategoryDto, PreFilingSnapshotExpenseCategory>()
                .ForMember(d => d.PreFilingSnapshotId, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshot, o => o.Ignore())
                .ForMember(d => d.RecommendedAmount, o => o.MapFrom(s => s.RecommendedTotal));
            CreateMap<PreFilingSnapshotExpenseCodeDto, PreFilingSnapshotExpenseCode>()
                .ForMember(d => d.PreFilingSnapshotId, o => o.Ignore())
                .ForMember(d => d.PreFilingSnapshot, o => o.Ignore());

            CreateMap<ExpenseCategory, ExpenseCategoryDto>();
            CreateMap<ExpenseCode, ExpenseCodeDto>();
            CreateMap<ExpenseCodeMap, PreFilingContactDetailBudgetDto>();

            CreateMap<PreFilingSnapshot, CertificateDto>();
            CreateMap<PreFilingActionPlan, ActionPlanDto>()
                .ForMember(d => d.Certificates, o => o.MapFrom(s =>
                {
                    var snapshotList = new List<PreFilingSnapshot>();
                    snapshotList.Add(s.PreFilingSnapshot);

                    if (s.PreFilingSnapshot1 != null)
                    {
                        snapshotList.Add(s.PreFilingSnapshot1);
                    }
                    return snapshotList;
                }));

            CreateMap<PreFilingActionPlan, PreFilingActionPlanDto>()
                .ForMember(d => d.Snapshots, o => o.MapFrom(s =>
                    {
                        var snapshotList = new List<PreFilingSnapshot>();
                        snapshotList.Add(s.PreFilingSnapshot);
                        if (s.PreFilingSnapshot1 != null)
                        {
                            snapshotList.Add(s.PreFilingSnapshot1);
                        }
                        return snapshotList;
                    }));

            CreateMap<UserDetail, Cccs.Identity.Dto.PreFiling.ReIssueSearchResult>()
                .ForMember(d => d.Snapshots, o => o.MapFrom(s => s.PreFilingSnapshots));
            CreateMap<PreFilingSnapshot, Cccs.Identity.Dto.PreFiling.ReIssueSearchResultSnapshot>();
        }

        private void ServiceMaps()
        {
            CreateMap<PreFilingUserDetailDto, ISnapshotUserInfo>()
                .ForMember(d => d.SsnLast4, o => o.ResolveUsing<SsnLast4Resolver>())
                .ForMember(d => d.CertificateGenerated, o => o.ResolveUsing<CurrentDateTimeResolver>())
                .ForMember(d => d.CertificateVersionType, o => o.Ignore())
                .ForMember(d => d.ArchiveStatus, o => o.UseValue(0))
                .ForMember(d => d.CounselorName, o => o.ResolveUsing<SnapshotCounselorNameResolver>())
                .ForMember(d => d.CounselorTitle, o => o.ResolveUsing<SnapshotCounselorTitleResolver>());

            CreateMap<PreFilingContactDetailDto, ISnapshotContactDetailInfo>()
                .ForMember(d => d.TotalLiabilities, o => o.ResolveUsing<SnapshotTotalLiabilitiesResolver>())
                .ForMember(d => d.DebtPayment, o => o.ResolveUsing<SnapshotDebtPaymentResolver>())
                .ForMember(d => d.CashAtMonthEnd, o => o.ResolveUsing<SnapshotCashAtMonthEndResolver>())
                .ForMember(d => d.NetWorth, o => o.ResolveUsing<SnapshotNetWorthResolver>())
                .ForMember(d => d.JudicialDistrict, o => o.Ignore())
                .ForMember(d => d.CreditScore, o => o.Ignore())
                .ForMember(d => d.DmpInterestRate, o => o.Ignore())
                .ForMember(d => d.DmpBalance, o => o.Ignore())
                .ForMember(d => d.DmpMonthlyPayment, o => o.Ignore())
                .ForMember(d => d.DmpMonthsToRepay, o => o.Ignore());
            CreateMap<PreFilingSnapshot, PreFilingSnapshotDto>()
                .ForMember(d => d.PreFilingActionPlanId, o => o.ResolveUsing<SnapshotActionPlanResolver>())
                .ForMember(d => d.Creditors, o => o.MapFrom(s => s.PreFilingSnapshotCreditors))
                .ForMember(d => d.ExpenseCategories, o => o.MapFrom(s => s.PreFilingSnapshotExpenseCategories))
                .ForMember(d => d.ExpenseCodes, o => o.MapFrom(s => s.PreFilingSnapshotExpenseCodes));
            CreateMap<PreFilingSnapshotCreditor, PreFilingSnapshotCreditorDto>();
            CreateMap<PreFilingSnapshotExpenseCategory, PreFilingSnapshotExpenseCategoryDto>()
                .ForMember(d => d.RecommendedTotal, o => o.MapFrom(s => s.RecommendedAmount));
            CreateMap<PreFilingSnapshotExpenseCode, PreFilingSnapshotExpenseCodeDto>();
            CreateMap<PreFilingContactDetailCreditorDto, PreFilingSnapshotCreditorDto>()
                .ForMember(d => d.InterestRate, o => o.MapFrom(s => s.InterestRate / 100))
                .ForMember(d => d.DisplayOrder, o => o.Ignore())
                .ForMember(d => d.MonthlyPayment, o => o.MapFrom(s => s.Payment))
                .ForMember(d => d.MonthsToRepay, o => o.ResolveUsing<SnapshotMonthsToRepayResolver>());
        }

        private class SsnLast4Resolver : ValueResolver<PreFilingUserDetailDto, string>
        {
            protected override string ResolveCore(PreFilingUserDetailDto source)
            {
                if (source.Ssn.IsNullOrWhiteSpace()) return String.Empty;
                if (source.Ssn.Length < 4) return String.Empty;

                return source.Ssn.Substring(source.Ssn.Length - 4, 4);
            }
        }

        private class CurrentDateTimeResolver : ValueResolver<PreFilingUserDetailDto, DateTime>
        {
            protected override DateTime ResolveCore(PreFilingUserDetailDto source)
            {
                return DateTime.Now;
            }
        }

        private class SnapshotCounselorNameResolver : ValueResolver<PreFilingUserDetailDto, string>
        {
            protected override string ResolveCore(PreFilingUserDetailDto source)
            {
                return source.CounselorName.IsNullOrWhiteSpace() ? "Brian Young" : source.CounselorName.ToLower().ToTitleCase();
            }
        }

        private class SnapshotCounselorTitleResolver : ValueResolver<PreFilingUserDetailDto, string>
        {
            protected override string ResolveCore(PreFilingUserDetailDto source)
            {
                return source.CounselorName.IsNullOrWhiteSpace() ? "Vice President of Counseling" : "Counselor";
            }
        }

        private class ContactDetailIncomeAfterTaxesResolver : ValueResolver<contactdetail, decimal>
        {
            protected override decimal ResolveCore(contactdetail source)
            {
                var incomeAfterTaxes = source.monthly_net_income.HasValue ? source.monthly_net_income.Value : 0f;
                incomeAfterTaxes += source.monthly_net_income2.HasValue ? source.monthly_net_income2.Value : 0f;
                incomeAfterTaxes += source.MonthlyPartNet.HasValue ? source.MonthlyPartNet.Value : 0f;
                incomeAfterTaxes += source.MonthlyPartNet2.HasValue ? source.MonthlyPartNet2.Value : 0f;
                incomeAfterTaxes += source.monthly_alimony_income.HasValue ? source.monthly_alimony_income.Value : 0f;
                incomeAfterTaxes += source.monthly_alimony_income2.HasValue ? source.monthly_alimony_income2.Value : 0f;
                incomeAfterTaxes += source.monthly_childsupport_income.HasValue ? source.monthly_childsupport_income.Value : 0f;
                incomeAfterTaxes += source.monthly_childsupport_income2.HasValue ? source.monthly_childsupport_income2.Value : 0f;
                incomeAfterTaxes += source.monthly_govtassist_income.HasValue ? source.monthly_govtassist_income.Value : 0f;
                incomeAfterTaxes += source.monthly_govtassist_income2.HasValue ? source.monthly_govtassist_income2.Value : 0f;
                incomeAfterTaxes += source.monthly_other_income.HasValue ? source.monthly_other_income.Value : 0f;
                incomeAfterTaxes += source.monthly_other_income2.HasValue ? source.monthly_other_income2.Value : 0f;

                return Convert.ToDecimal(incomeAfterTaxes);
            }
        }

        private class ContactDetailLivingExpensesResolver : ValueResolver<contactdetail, decimal>
        {
            protected override decimal ResolveCore(contactdetail source)
            {
                var livingExpenses = source.rent_mort.HasValue ? source.rent_mort.Value : 0m;
                livingExpenses += source.equity_loan_tax_ins.HasValue ? source.equity_loan_tax_ins.Value : 0m;
                livingExpenses += source.home_maintenance.HasValue ? source.home_maintenance.Value : 0m;
                livingExpenses += source.utilities.HasValue ? source.utilities.Value : 0m;
                livingExpenses += source.telephone.HasValue ? source.telephone.Value : 0m;
                livingExpenses += source.food_away.HasValue ? source.food_away.Value : 0m;
                livingExpenses += source.groceries.HasValue ? source.groceries.Value : 0m;
                livingExpenses += source.vehicle_payments.HasValue ? source.vehicle_payments.Value : 0m;
                livingExpenses += source.car_insurance.HasValue ? source.car_insurance.Value : 0m;
                livingExpenses += source.car_maintenance.HasValue ? source.car_maintenance.Value : 0m;
                livingExpenses += source.public_transportation.HasValue ? source.public_transportation.Value : 0m;
                livingExpenses += source.insurance.HasValue ? source.insurance.Value : 0m;
                livingExpenses += source.medical_prescription.HasValue ? source.medical_prescription.Value : 0m;
                livingExpenses += source.child_support.HasValue ? source.child_support.Value : 0m;
                livingExpenses += source.child_elder_care.HasValue ? source.child_elder_care.Value : 0m;
                livingExpenses += source.education.HasValue ? source.education.Value : 0m;
                livingExpenses += source.contributions.HasValue ? source.contributions.Value : 0m;
                livingExpenses += source.clothing.HasValue ? source.clothing.Value : 0m;
                livingExpenses += source.laundry.HasValue ? source.laundry.Value : 0m;
                livingExpenses += source.personal_exp.HasValue ? source.personal_exp.Value : 0m;
                livingExpenses += source.beauty_barber.HasValue ? source.beauty_barber.Value : 0m;
                livingExpenses += source.recreation.HasValue ? source.recreation.Value : 0m;
                livingExpenses += source.club_dues.HasValue ? source.club_dues.Value : 0m;
                livingExpenses += source.gifts.HasValue ? source.gifts.Value : 0m;
                livingExpenses += source.misc_exp.HasValue ? source.misc_exp.Value : 0m;
                livingExpenses += source.other.HasValue ? source.other.Value : 0m;

                return livingExpenses;
            }
        }

        private class ContactDetailTotalAssetsResolver : ValueResolver<contactdetail, decimal>
        {
            protected override decimal ResolveCore(contactdetail source)
            {
                var totalAssets = source.val_home.HasValue ? source.val_home.Value : 0f;
                totalAssets += source.val_car.HasValue ? source.val_car.Value : 0f;
                totalAssets += source.val_other.HasValue ? source.val_other.Value : 0f;
                totalAssets += source.val_sav.HasValue ? source.val_sav.Value : 0f;
                totalAssets += source.val_prop.HasValue ? source.val_prop.Value : 0f;
                totalAssets += source.val_ret.HasValue ? source.val_ret.Value : 0f;

                return Convert.ToDecimal(totalAssets);
            }
        }

        private class ContactDetailTotalLiabilitiesResolver : ValueResolver<contactdetail, decimal>
        {
            protected override decimal ResolveCore(contactdetail source)
            {
                var totalLiabilities = source.owe_car.HasValue ? source.owe_car.Value : 0f;
                totalLiabilities += source.owe_home.HasValue ? source.owe_home.Value : 0f;
                totalLiabilities += source.owe_other.HasValue ? source.owe_other.Value : 0f;
                totalLiabilities += source.secondary_amt.HasValue ? source.secondary_amt.Value : 0f;

                return Convert.ToDecimal(totalLiabilities);
            }
        }

        private class CreditorBalanceResolver : ValueResolver<creditordetail, decimal>
        {
            protected override decimal ResolveCore(creditordetail source)
            {
                return Convert.ToDecimal(source.creditor_bal.HasValue ? source.creditor_bal.Value : 0f);
            }
        }

        private class CreditorPaymentResolver : ValueResolver<creditordetail, decimal>
        {
            protected override decimal ResolveCore(creditordetail source)
            {
                return Convert.ToDecimal(source.creditor_payments.HasValue ? source.creditor_payments.Value : 0f);
            }
        }

        private class CreditorInterestRateResolver : ValueResolver<creditordetail, decimal>
        {
            protected override decimal ResolveCore(creditordetail source)
            {
                return Convert.ToDecimal(source.creditor_intrate.HasValue ? source.creditor_intrate.Value : 0f);
            }
        }

        private class SnapshotTotalLiabilitiesResolver : ValueResolver<PreFilingContactDetailDto, decimal>
        {
            protected override decimal ResolveCore(PreFilingContactDetailDto source)
            {
                var totalLiabilities = source.TotalLiabilities;
                totalLiabilities += source.Creditors.Sum(s => s.Balance);

                return totalLiabilities;
            }
        }

        private class SnapshotDebtPaymentResolver : ValueResolver<PreFilingContactDetailDto, decimal>
        {
            protected override decimal ResolveCore(PreFilingContactDetailDto source)
            {
                var payments = source.Creditors.Sum(s => s.Payment);

                return payments;
            }
        }

        private class SnapshotCashAtMonthEndResolver : ValueResolver<PreFilingContactDetailDto, decimal>
        {
            protected override decimal ResolveCore(PreFilingContactDetailDto source)
            {
                var payments = source.Creditors.Sum(s => s.Payment);
                var cashAtMonthEnd = source.IncomeAfterTaxes - source.LivingExpenses - payments;

                return cashAtMonthEnd;
            }
        }

        private class SnapshotNetWorthResolver : ValueResolver<PreFilingContactDetailDto, decimal>
        {
            protected override decimal ResolveCore(PreFilingContactDetailDto source)
            {
                var totalLiabilities = source.TotalLiabilities;
                totalLiabilities += source.Creditors.Sum(s => s.Balance);

                var netWorth = source.TotalAssets - totalLiabilities;

                return netWorth;
            }
        }

        private class SnapshotMonthsToRepayResolver : ValueResolver<PreFilingContactDetailCreditorDto, short>
        {
            protected override short ResolveCore(PreFilingContactDetailCreditorDto source)
            {
                var payment = source.Payment;
                var interestRate = source.InterestRate;
                if (interestRate <= 0)
                    return 0;

                if (interestRate >= 1)
                    interestRate = interestRate / 100;

                var monthlyInterestRate = interestRate / 12;
                var principal = source.Balance;
                short monthsToRepay = 0;
                while (principal > 0)
                {
                    var monthlyInterest = Math.Ceiling(principal * monthlyInterestRate * 100) / 100;
                    if (principal > payment)
                    {
                        principal = principal - payment + monthlyInterest;
                    }
                    else
                    {
                        principal = 0;
                    }

                    monthsToRepay++;
                }

                return monthsToRepay;
            }
        }

        private class SnapshotActionPlanResolver : ValueResolver<PreFilingSnapshot, long>
        {
            protected override long ResolveCore(PreFilingSnapshot source)
            {
                var actionPlanId = 0L;

                var compareActionPlanId = GetLatestActionPlanId(source.PreFilingActionPlans);
                actionPlanId = compareActionPlanId > actionPlanId ? compareActionPlanId : actionPlanId;

                compareActionPlanId = GetLatestActionPlanId(source.PreFilingActionPlans1);
                actionPlanId = compareActionPlanId > actionPlanId ? compareActionPlanId : actionPlanId;

                return actionPlanId;
            }

            private static long GetLatestActionPlanId(IEnumerable<PreFilingActionPlan> actionPlans)
            {
                var actionPlanId = 0L;

                if (actionPlans == null)
                    return actionPlanId;

                actionPlans.ForEach(a =>
                    {
                        actionPlanId = a.PreFilingActionPlanId > actionPlanId ? a.PreFilingActionPlanId : actionPlanId;
                    });

                return actionPlanId;
            }
        }
    }
}
