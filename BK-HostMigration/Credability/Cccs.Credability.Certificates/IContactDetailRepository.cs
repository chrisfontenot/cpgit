﻿using Cccs.Credability.Certificates.DataTransferObjects;
using System.Collections.Generic;

namespace Cccs.Credability.Certificates
{
    public interface IContactDetailRepository
    {
        PreFilingContactDetailDto Get(int clientNumber);
    }
}
