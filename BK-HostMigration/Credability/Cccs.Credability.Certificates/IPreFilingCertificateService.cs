﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface IPreFilingCertificateService
    {
        /// <summary>
        /// Creates a snapshot of the user's current pre-filing information.
        /// </summary>
        /// <param name="userDetailId">The specific user to snapshot.</param>
        void SnapshotData(long userDetailId, string creditScore, string certificateVersionType);

        /// <summary>
        /// Creates a new snapshot from the provided snapshot and leaves the certificate number blank so that
        /// a new number can be issued.
        /// </summary>
        /// <param name="snapshot">The snapshot to duplicate.</param>
        PreFilingSnapshotDto ReIssueSnapshotData(PreFilingSnapshotDto snapshot);

        /// <summary>
        /// Creates an action plan for the specified snapshots.
        /// </summary>
        /// <param name="primarySnapshot">The primary user's snapshot.</param>
        /// <param name="secondarySnapshot">The secondary user's snapshot.</param>
        /// <returns>The new action plan item.</returns>
        PreFilingActionPlanDto CreateActionPlan(PreFilingSnapshotDto primarySnapshot, PreFilingSnapshotDto secondarySnapshot);

        /// <summary>
        /// Retrieves a specific action plan and its associated snapshots.
        /// </summary>
        /// <param name="actionPlanId">The action plan to search.</param>
        /// <returns>The specified action plan if found, null otherwise.</returns>
        PreFilingActionPlanDto GetActionPlan(long actionPlanId);

        /// <summary>
        /// Retrieves the specified snapshot.
        /// </summary>
        /// <param name="preFilingSnapshotId">The Id of the snapshot to retrieve.</param>
        /// <returns>The snapshot if found, null otherwise.</returns>
        PreFilingSnapshotDto GetSnapshot(long preFilingSnapshotId);

        /// <summary>
        /// Retrieves the snapshot associated with the certificate number.
        /// </summary>
        /// <param name="certificateNumber">The certificate number to search.</param>
        /// <returns>The snapshot for the certificate number, null otherwise.</returns>
        PreFilingSnapshotDto GetSnapshotByCertificateNumber(string certificateNumber);

        /// <summary>
        /// Retrieves the the most recent snapshot belonging to this user's supplied certificate number that is not also
        /// the supplied certificate number.
        /// </summary>
        /// <param name="certificateNumber">The certificate number to search.</param>
        /// <returns>The most recently associated snapshot to the supplied certificate number.</returns>
        PreFilingSnapshotDto GetAssociatedSnapshotByCertificateNumber(string certificateNumber);

        /// <summary>
        /// Retrieves the most recent snapshot for the specified user detail.
        /// </summary>
        /// <param name="userDetailId">The user detail to search.</param>
        /// <returns>The most recent snapshot for the user detail, null otherwise.</returns>
        PreFilingSnapshotDto GetCurrentSnapshotForUserDetail(long userDetailId);

        /// <summary>
        /// Retrieves a complete list of snapshots for the specified user.
        /// </summary>
        /// <param name="userId">The user to search.</param>
        /// <returns>A list of snapshots. An empty list if no snapshots are found.</returns>
        IList<CertificateDto> GetCertificateListForUser(long userId);

        /// <summary>
        /// Retrieves a complete list of action plans for the specified user.
        /// </summary>
        /// <param name="userId">The user to search.</param>
        /// <returns>A list of action plans. An empty list if no snapshots are found.</returns>
        IList<ActionPlanDto> GetActionPlanList(long userId);

        /// <summary>
        /// Assigns a certificate number to the specified snapshot.
        /// </summary>
        /// <param name="preFilingSnapshotId">The snapshot to update.</param>
        void AssignCertificateToSnapshot(long preFilingSnapshotId);

        /// <summary>
        /// Determines if the specified user has a snapshot within the allowed time frame.
        /// </summary>
        /// <param name="userDetailId">The user detail to search.</param>
        /// <returns>True if a snapshot is found, false otherwise.</returns>
        bool HasCurrentSnapshot(long userDetailId);

        /// <summary>
        /// Determines if the specified user has a certificate number downloaded from EOUST for the most recent snapshot.
        /// </summary>
        /// <param name="userDetailId">The user detail to search.</param>
        /// <returns>True if a certificate number is assigned to the most recent snapshot, false otherwise.</returns>
        bool HasDownloadedCertificate(long userDetailId);

        /// <summary>
        /// Retrieves certificates not marked as archived and stores them in the archive SAN location.
        /// </summary>
        void ArchiveCertificates();

        /// <summary>
        /// Retrieves snapshots that do not have a certificate number and attempts to download one from EOUST.
        /// </summary>
        void FillCertificates();
    }
}
