﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Net.Mail;
using System.Threading;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Credability.Certificates.PreFiling.Queues;
using Cccs.Email;

namespace Cccs.Credability.Certificates.Email
{
    public class PreFilingPoll : IDisposable
    {
        private ILoggingService logger;
        private IPreFilingCertificateService service;
        private IUserRepository userRepository;
        private ClientActionPlanQueue clientQueue;
        private AttorneyCertificateQueue attorneyQueue;

        public PreFilingPoll(ILoggingService loggingService, IPreFilingCertificateService service, IUserRepository userRepository)
        {
            this.logger = loggingService;
            this.service = service;
            this.userRepository = userRepository;

            var configuration = EmailQueueConfiguration.GetConfiguration();

            ValidateAndCreateQueues();
        }

        private void ValidateAndCreateQueues()
        {
            logger.Info(() => "Validating queues.");

            clientQueue = new ClientActionPlanQueue(logger);
            attorneyQueue = new AttorneyCertificateQueue(logger);
        }

        public void ProcessEmails()
        {
            try
            {
                CheckClientQueue();
                CheckAttorneyQueue();
            }
            catch (Exception ex)
            {
                logger.Error("Error processing PreFiling queues.", ex);
            }
        }

        private void CheckClientQueue()
        {
            logger.Info(() => "Processing Client Action Plan Queue.");

            var message = clientQueue.GetNextActionPlan();
            while (message != null)
            {
                SendClientActionPlanEmail(message);
                message = clientQueue.GetNextActionPlan();
            }
        }

        private void SendClientActionPlanEmail(ClientActionPlanQueueMessage message)
        {
            var actionPlan = service.GetActionPlan(message.PreFilingActionPlanId);
            var primarySnapshot = null as PreFilingSnapshotDto;
            var secondarySnapshot = null as PreFilingSnapshotDto;
            SortSnapshots(actionPlan, out primarySnapshot, out secondarySnapshot);

            var culture = new CultureInfo(primarySnapshot.LanguageCode);
            Resources.EmailText.Culture = culture;

            using (var mailMessage = new MailMessage())
            {
                mailMessage.To.Add(new MailAddress(message.EmailAddress));

                var clientName = FormatClientName(primarySnapshot, secondarySnapshot);
                mailMessage.Subject = Resources.EmailText.BKCertificateClientSubject;
                mailMessage.Body = Resources.EmailText.BKCertificateClientBody.FormatWith(clientName);

                var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
                var generatorType = assembly.GetType(primarySnapshot.CertificateVersionType);

                var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
                var stream = generator.CreateActionPlan(primarySnapshot, secondarySnapshot);
                stream.Position = 0;

                mailMessage.Attachments.Add(new Attachment(stream, "ActionPlan.pdf"));

                var client = new SmtpClient();
                client.Send(mailMessage);
            }
        }

        private void CheckAttorneyQueue()
        {
            logger.Info(() => "Processing Attorney Certificate Queue.");

            var message = attorneyQueue.GetNextCertificate();
            while (message != null)
            {
                SendAttorneyEmail(message);
                message = attorneyQueue.GetNextCertificate();
            }
        }

        private void SendAttorneyEmail(AttorneyCertificateQueueMessage message)
        {
            var actionPlan = service.GetActionPlan(message.PreFilingActionPlanId);
            var primarySnapshot = null as PreFilingSnapshotDto;
            var secondarySnapshot = null as PreFilingSnapshotDto;
            SortSnapshots(actionPlan, out primarySnapshot, out secondarySnapshot);

            var culture = new CultureInfo("EN");
            Resources.EmailText.Culture = culture;

            using (var mailMessage = new MailMessage())
            {
                var emailList = message.EmailAddress.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in emailList)
                {
                    mailMessage.To.Add(new MailAddress(email));
                }

                var clientName = FormatClientName(primarySnapshot, secondarySnapshot);
                mailMessage.Subject = Resources.EmailText.BKCertificateAttorneySubject.FormatWith(clientName);
                mailMessage.Body = Resources.EmailText.BKCertificateAttorneyBody;

                var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
                var generatorType = assembly.GetType(primarySnapshot.CertificateVersionType);
                var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
                var primaryStream = generator.CreateCertificate(primarySnapshot);
                primaryStream.Position = 0;
                mailMessage.Attachments.Add(new Attachment(primaryStream, String.Format("{0}.pdf", primarySnapshot.CertificateNumber)));

                if (secondarySnapshot != null)
                {
                    var secondaryStream = generator.CreateCertificate(secondarySnapshot);
                    secondaryStream.Position = 0;
                    mailMessage.Attachments.Add(new Attachment(secondaryStream, String.Format("{0}.pdf", secondarySnapshot.CertificateNumber)));

                    var combinedStream = generator.CreateCombinedCertificate(primarySnapshot, secondarySnapshot);
                    combinedStream.Position = 0;
                    mailMessage.Attachments.Add(new Attachment(combinedStream, String.Format("{0}_{1}.pdf", primarySnapshot.CertificateNumber, secondarySnapshot.CertificateNumber)));
                }

            }
        }

        private void SortSnapshots(PreFilingActionPlanDto actionPlan, out PreFilingSnapshotDto primarySnapshot, out PreFilingSnapshotDto secondarySnapshot)
        {
            primarySnapshot = null;
            secondarySnapshot = null;

            foreach (var snapshot in actionPlan.Snapshots)
            {
                var userDetail = userRepository.GetDetail(snapshot.UserDetailId);
                primarySnapshot = userDetail.IsPrimary ? snapshot : primarySnapshot;
                secondarySnapshot = !userDetail.IsPrimary ? snapshot : secondarySnapshot;
            }
        }

        private string FormatClientName(PreFilingSnapshotDto primarySnapshot, PreFilingSnapshotDto secondarySnapshot)
        {
            var primaryName = FormatName(primarySnapshot);

            if (secondarySnapshot != null)
            {
                var secondaryName = FormatName(secondarySnapshot);
                return Resources.EmailText.JointClientName.FormatWith(primaryName, secondaryName);
            }

            return primaryName;
        }

        private string FormatName(PreFilingSnapshotDto dto)
        {
            return String.Format("{0}{1} {2}",
                dto.FirstName,
                String.IsNullOrEmpty(dto.MiddleName) ? String.Empty : String.Format(" {0}", dto.MiddleName),
                dto.LastName);
        }

        #region IDisposable
        ~PreFilingPoll()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (clientQueue != null)
                {
                    clientQueue.Dispose();
                    clientQueue = null;
                }
                if (attorneyQueue != null)
                {
                    attorneyQueue.Dispose();
                    attorneyQueue = null;
                }
            }
        }
        #endregion
    }
}
