﻿using AutoMapper;
using Cccs.Credability.Certificates.PreFiling;
//using Cccs.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CuttingEdge.Conditions;
using Cccs.Response;
using Cccs.Credability.Certificates.DataTransferObjects;
//using Cccs.Host.Dto;
using Cccs.Identity;
using Cccs.Identity.Dal;
using Cccs.Credability.Certificates;
using Cccs.Debtplus;

namespace Cccs.Credability.Certificates.Education
{
    public class EducationCertificateService : IEducationCertificateService
    {
        private const int CertificateValidMonths = 6;

        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        private readonly IUserRepository userRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IContactDetailRepository contactDetailRepository;
        private readonly ISnapshotRepository snapshotRepository;
        private readonly IExpenseRepository expenseRepository;
        //private readonly IHost hostService;
        private readonly IDebtplus debtplusService;
        private readonly ICounselingCertificateDownload certificateDownload;
        private readonly IPredischargeCertificateRepository certificateRepository;

        public EducationCertificateService(ILoggingService logger, IMappingEngine mapper,
            IUserRepository userRepository, IAccountRepository accountRepository,
            IContactDetailRepository contactDetailRepository, ISnapshotRepository snapshotRepository,
            IExpenseRepository expenseRepository, ICounselingCertificateDownload certificateDownload,
            /*IHost hostService, */IDebtplus debtplusService, IPredischargeCertificateRepository certificateRepository)
        {
            #region Parameter Validation
            Condition.Requires(logger, "logger").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();
            Condition.Requires(userRepository, "userRepository").IsNotNull();
            Condition.Requires(accountRepository, "accountRepository").IsNotNull();
            Condition.Requires(contactDetailRepository, "contactDetailRepository").IsNotNull();
            Condition.Requires(snapshotRepository, "snapshotRepository").IsNotNull();
            Condition.Requires(expenseRepository, "expenseRepository").IsNotNull();
            Condition.Requires(certificateDownload, "certificateDownload").IsNotNull();
            //Condition.Requires(hostService, "hostService").IsNotNull();
            Condition.Requires(debtplusService, "debtplusService").IsNotNull();
            Condition.Requires(certificateRepository, "certificateRepository").IsNotNull();
            #endregion

            this.logger = logger;
            this.userRepository = userRepository;
            this.accountRepository = accountRepository;
            this.contactDetailRepository = contactDetailRepository;
            this.snapshotRepository = snapshotRepository;
            this.expenseRepository = expenseRepository;
            this.mapper = mapper;
            this.certificateDownload = certificateDownload;
            //this.hostService = hostService;
            this.debtplusService = debtplusService;
            this.certificateRepository = certificateRepository;
        }

        public Cccs.Identity.Dal.EducationCertificate GetUserDetail(Identity.UserDetail userDetail, long accountID, string bankruptcyCaseNumber)
        {
            // Bankruptcy Conversion
            //var judicialDistrict = hostService.GetJudicialDistrict(userDetail.Zipcode);
            var judicialDistrict = debtplusService.GetJudicialDistrict(userDetail.Zipcode);

            var certificateData = certificateRepository.GetPredischargeCertificate(accountID);

            var data = new EducationCertificate()
            {
                Address1 = userDetail.Address1,
                Address2 = userDetail.Address2,
                BankruptcyNumber = bankruptcyCaseNumber,
                CertificateGenerated = (certificateData != null) ? certificateData.CompletionDate : DateTime.MinValue,
                CertificateNumber = (certificateData != null) ? certificateData.CertificateNumber : "",
                CertificateVersionType = "2",
                City = userDetail.City,
                CourseCompleted = DateTime.Now,
                //EducationCertificateId = 1001,
                FirstName = userDetail.FirstName,
                JudicialDistrict = judicialDistrict.DistrictName,
                LastName = userDetail.LastName,
                PostalCode = userDetail.Zipcode,
                State = userDetail.State,
                //UserDetailId = userDetail.UserDetailId,
            };

            return data;
        }

        public bool HasCurrentSnapshot(long userDetailId)
        {
            var currentSnapshot = snapshotRepository.GetMostRecent(userDetailId);
            var snapshotIsCurrent = (currentSnapshot != null && currentSnapshot.CourseCompleted > DateTime.Now.AddMonths(-CertificateValidMonths));

            return snapshotIsCurrent;
        }

        public bool HasDownloadedCertificate(long accountID)
        {
            var cert = certificateRepository.GetPredischargeCertificate(accountID);

            if (cert == null)
            {
                return false;
            }

            return !String.IsNullOrEmpty(cert.CertificateNumber);
        }

        public void AssignCertificate(Identity.UserDetail userDetail, long accountID, DateTime completionDate)
        {
            //var snapshot = snapshotRepository.GetSnapshot(preFilingSnapshotId);
            AssignNewCertificate(userDetail, accountID, completionDate);
            //snapshot.ArchiveStatus = 0;
            //snapshotRepository.Update(snapshot);
        }

        private void AssignNewCertificate(Identity.UserDetail userDetail, long accountID, DateTime completionDate)
        {
            //if (!String.IsNullOrEmpty(snapshot.CertificateNumber))
            //    throw new InvalidOperationException("A certificate number is already assigned to this record.");

            // Bankruptcy Conversion
            // var judicialDistrict = hostService.GetJudicialDistrict(userDetail.Zipcode);
            var judicialDistrict = debtplusService.GetJudicialDistrict(userDetail.Zipcode);
            var districtName = judicialDistrict.DistrictName;

            // Bankruptcy Conversion
            // var districtCode = hostService.GetEoustJudicialDistrictByName(districtName);
            var districtCode = debtplusService.GetEoustJudicialDistrictByName(districtName);
            if (!districtCode.IsNullOrWhiteSpace())
            {
                DataResponse<string> dr = certificateDownload.GetEducationCertificateNumber(userDetail.FirstName, userDetail.LastName, completionDate, districtCode);
                if (dr.IsSuccessful)
                {
                    var certificateNumber = dr.Payload;

                    var certificateData = new PredischargeCertificateDto() {
                        AccountId = accountID,
                        CertificateNumber = certificateNumber,
                        CompletionDate = completionDate
                    };

                    certificateRepository.AddPredischangeCertificate(certificateData);
                }
                else
                {
                    return;
                }
            }
            else
            {
                //var userDetail = userRepository.GetDetail(snapshot.UserDetailId);
                //snapshot.CertificateNumber = String.Format("IN{0}-{1}", snapshot.ClientNumber, userDetail.IsPrimary ? "1" : "2");
            }
            //AssignCertificateToHost(snapshot);
        }

        private bool AssignCertificateToHost(PreFilingSnapshotDto snapshot)
        {
            // Bankruptcy Conversion
            //CertReIssue CertData = new CertReIssue();
            Cccs.Debtplus.Data.CertReIssue CertData = new Cccs.Debtplus.Data.CertReIssue();
            var CurDetail = userRepository.GetDetail(snapshot.UserDetailId);
            if (CurDetail != null)
            {
                CertData.IsPrimary = CurDetail.IsPrimary;
                var CurAccount = accountRepository.GetPreFilingAccountByUserDetail(snapshot.UserDetailId);
                if (CurAccount != null && !CurAccount.HostId.IsNullOrWhiteSpace() && CurAccount.InternetId == snapshot.ClientNumber)
                {
                    CertData.CertIssue = snapshot.CertificateGenerated;
                    CertData.CertNo = snapshot.CertificateNumber;
                    CertData.ClNo = CurAccount.HostId;

                    // Bankruptcy Conversion
                    //return hostService.SetCertNo(CertData);
                    return debtplusService.SetCertNo(CertData);
                }
            }
            return false;
        }
    }
}
