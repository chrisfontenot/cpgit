﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cccs.Credability.Certificates.Education
{
    public interface ICertificateGenerator
    {
        MemoryStream CreateCoverLetter(string languageCode, Identity.Dal.EducationCertificate data);
        MemoryStream CreateCertificate(Identity.Dal.EducationCertificate data);
    }
}
