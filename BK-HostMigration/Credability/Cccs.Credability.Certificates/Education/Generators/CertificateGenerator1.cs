﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using Doc = Aspose.Words;
using Barcode = Aspose.BarCode;

namespace Cccs.Credability.Certificates.Education.Generators
{
    public class CertificateGenerator1 : ICertificateGenerator
    {
        private const string EnglishCoverLetterDocumentName = "Cccs.Credability.Certificates.Education.Documents.CoverLetter1_en.docx";
        private const string SpanishCoverLetterDocumentName = "Cccs.Credability.Certificates.Education.Documents.CoverLetter1_es.docx";
        private const string CertificateDocumentName = "Cccs.Credability.Certificates.Education.Documents.EducationCertificate1.docx";
        private const string BKEducationCertificateDocument = "Cccs.Credability.Certificates.Education.Documents.BKEducationCertificate.docx";

        /// <summary>
        /// Creates a Cover Letter PDF document with the education certificate included.
        /// </summary>
        /// <param name="languageCode">The language of the action plan to produce.</param>
        /// <param name="data">The client's data.</param>
        /// <returns>A MemoryStream containing the PDF document created.</returns>
        public MemoryStream CreateCoverLetter(string languageCode, Identity.Dal.EducationCertificate data)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var coverLetterDocumentName = languageCode.ToLower() == "es" ? SpanishCoverLetterDocumentName : EnglishCoverLetterDocumentName;
            using (var coverLetterStream = assembly.GetManifestResourceStream(coverLetterDocumentName))
            using (var certificateStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var coverLetterDataSource = new CoverLetterMailMergeDataSource(languageCode, data);
            var coverLetterDocument = new Doc.Document(coverLetterStream);
            coverLetterDocument.MailMerge.Execute(coverLetterDataSource);

            var certificateDocument = GenerateCertificateDocument(data, certificateStream);
            coverLetterDocument.AppendDocument(certificateDocument, Doc.ImportFormatMode.KeepSourceFormatting);

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
                    coverLetterDocument.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        public MemoryStream CreateCertificate(Identity.Dal.EducationCertificate data)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var documentStream = assembly.GetManifestResourceStream(BKEducationCertificateDocument))
            {
            var document = GenerateCertificateDocument(data, documentStream);

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();

                try
                {
                    document.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        private Doc.Document GenerateCertificateDocument(Identity.Dal.EducationCertificate data, Stream documentStream)
        {
            documentStream.Position = 0;
            var document = new Doc.Document(documentStream);

            var dataSource = new CertificateMailMergeDataSource(data);
            document.MailMerge.Execute(dataSource);
            using (var barcode = new Barcode.BarCodeBuilder(data.CertificateNumber, Barcode.Symbology.Code128))
            {
            InsertBarcodeImage(document, barcode);
            }

            return document;
        }

        private void InsertBarcodeImage(Doc.Document document, Barcode.BarCodeBuilder barcode)
        {
            var builder = new Doc.DocumentBuilder(document);
            builder.MoveToBookmark("BarcodeImage");

            barcode.AutoSize = true;
            barcode.GraphicsUnit = System.Drawing.GraphicsUnit.Pixel;
            barcode.BarHeight = 35;
            builder.InsertImage(barcode.BarCodeImage);
        }
    }
}
