﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;

namespace Cccs.Credability.Certificates.Education
{
    public class CoverLetterMailMergeDataSource : IMailMergeDataSource
    {
        private readonly Identity.Dal.EducationCertificate Data;
        private readonly string LanguageCode;

        private int RecordIndex;

        public CoverLetterMailMergeDataSource(string languageCode, Identity.Dal.EducationCertificate data)
        {
            Data = data;
            LanguageCode = languageCode;
            RecordIndex = -1;
        }

        public string TableName
        {
            get { return "Certificate"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "CertificateNumber":
                    fieldValue = Data.CertificateNumber;
                    return true;

                case "CertDate":
                    fieldValue = Data.CourseCompleted.ToString("MM/dd/yyyy");
                    return true;

                case "CertTime":
                    fieldValue = Data.CourseCompleted.ToString("hh:mm");
                    return true;

                case "CertZone":
                    fieldValue = String.Format("{0:tt} EST", Data.CourseCompleted);
                    return true;

                case "Name":
                    fieldValue = FormatName(Data);
                    return true;

                case "AddressBlock":
                    fieldValue = String.Format("{0}\n{1}{2}, {3} {4}",
                        Data.Address1.Trim(),
                        String.IsNullOrEmpty(Data.Address2) ? String.Empty : String.Format("{0}\n", Data.Address2.Trim()),
                        Data.City.Trim(),
                        Data.State,
                        Data.PostalCode);
                    return true;

                case "JudicialDistrict":
                    fieldValue = Data.JudicialDistrict;
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        private string FormatName(Identity.Dal.EducationCertificate data)
        {
            return String.Format("{0}{1}{2}",
                data.FirstName,
                String.IsNullOrEmpty(data.MiddleInitial) ? String.Empty : String.Format(" {0}", data.MiddleInitial),
                String.Format(" {0}", data.LastName));
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == 0); }
        }
    }
}
