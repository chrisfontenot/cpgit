﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
    [Serializable]
    public class UserDetailNotFoundException : Exception
    {
        public UserDetailNotFoundException() : base()
        {
        }

        public UserDetailNotFoundException(string message)
            : base(message)
        {
        }

        public UserDetailNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected UserDetailNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
