﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.PreFiling.Reasons;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class ActionPlanMailMergeDataSource : IMailMergeDataSource
    {
        private readonly Identity.Dal.CounselingCertificate PrimaryData;
        private readonly Identity.Dal.CounselingCertificate SecondaryData;
        private readonly string LanguageCode;

        private int RecordIndex;

        public ActionPlanMailMergeDataSource(string languageCode, Identity.Dal.CounselingCertificate primaryData, Identity.Dal.CounselingCertificate secondaryData)
        {
            PrimaryData = primaryData;
            SecondaryData = secondaryData;
            LanguageCode = languageCode;
            RecordIndex = -1;

            if (LanguageCode.ToLower() == "es")
            {
                Reason1.Culture = new System.Globalization.CultureInfo(LanguageCode);
            }
            else
            {
                Reason1.Culture = System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        public string TableName
        {
            get { return "Certificate"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "CertificateNumber":
                    fieldValue = String.Format("{0}{1}",
                        PrimaryData.CertificateNumber,
                        SecondaryData != null ? "\n" + SecondaryData.CertificateNumber : String.Empty);
                    return true;

                case "ClientNumber":
                    fieldValue = PrimaryData.ClientNumber;
                    return true;

                case "CertDate":
                    fieldValue = PrimaryData.CourseCompleted.ToString("MM/dd/yyyy");
                    return true;

                case "CertTime":
                    fieldValue = PrimaryData.CourseCompleted.ToString("hh:mm");
                    return true;

                case "CertZone":
                    fieldValue = String.Format("{0:tt} EST", PrimaryData.CourseCompleted);
                    return true;

                case "Name":
                    fieldValue = String.Format("{0}{1}",
                        FormatName(PrimaryData), 
                        SecondaryData != null ? " and " + FormatName(SecondaryData) : String.Empty);
                    return true;

                case "AddressBlock":
                    fieldValue = String.Format("{0}\n{1}{2}, {3} {4}",
                        PrimaryData.Address1.Trim(),
                        String.IsNullOrEmpty(PrimaryData.Address2) ? String.Empty : String.Format("{0}\n", PrimaryData.Address2.Trim()),
                        PrimaryData.City.Trim(),
                        PrimaryData.State,
                        PrimaryData.PostalCode);
                    return true;

                case "SsnMasked":
                    fieldValue = String.Format("{0}{1}", 
                        String.Format("###-##-{0}", PrimaryData.SsnLast4),
                        SecondaryData != null ? String.Format("\n###-##-{0}", SecondaryData.SsnLast4) : String.Empty);
                    return true;

                case "NetIncome":
                    fieldValue = PrimaryData.IncomeAfterTaxes.HasValue ? PrimaryData.IncomeAfterTaxes.Value.ToString("C") : String.Empty;
                    return true;

                case "LivingExpenses":
                    fieldValue = PrimaryData.LivingExpenses.HasValue ? PrimaryData.LivingExpenses.Value.ToString("C") : String.Empty;
                    return true;

                case "DebtPayment":
                    fieldValue = PrimaryData.DebtPayment.HasValue ? PrimaryData.DebtPayment.Value.ToString("C") : String.Empty;
                    return true;

                case "CashAtMonthEnd":
                    fieldValue = PrimaryData.CashAtMonthEnd.HasValue ? PrimaryData.CashAtMonthEnd.Value.ToString("C") : String.Empty;
                    return true;

                case "TotalAssets":
                    fieldValue = PrimaryData.TotalAssets.HasValue ? PrimaryData.TotalAssets.Value.ToString("C") : String.Empty;
                    return true;

                case "TotalLiabilities":
                    fieldValue = PrimaryData.TotalLiabilities.HasValue ? PrimaryData.TotalLiabilities.Value.ToString("C") : String.Empty;
                    return true;

                case "NetWorth":
                    fieldValue = PrimaryData.NetWorth.HasValue ? PrimaryData.NetWorth.Value.ToString("C") : String.Empty;
                    return true;

                case "Reason":
                    fieldValue = DetermineReason(PrimaryData.Reason);
                    return true;

                case "JudicialDistrict":
                    fieldValue = PrimaryData.JudicialDistrict;
                    return true;

                case "CounselorNameSignature":
                    fieldValue = "S/Brian Young";
                    return true;

                case "CounselorName":
                    fieldValue = "Brian Young";
                    return true;

                case "Title":
                    fieldValue = "Vice President of Counseling";
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        private string FormatName(Identity.Dal.CounselingCertificate data)
        {
            return String.Format("{0}{1}{2}",
                data.FirstName,
                String.IsNullOrEmpty(data.MiddleInitial) ? String.Empty : String.Format(" {0}", data.MiddleInitial),
                String.Format(" {0}", data.LastName));
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == 0); }
        }

        private string DetermineReason(string reasonCode)
        {
            switch (reasonCode)
            {
                case "01":
                    return Reason1.OverObligation;

                case "02":
                    return Reason1.ReducedIncome;

                case "03":
                    return Reason1.MedicalAccident;

                case "04":
                    return Reason1.DeathOfAFamilyMember;

                case "05":
                    return Reason1.DivorceSeparation;

                case "06":
                    return Reason1.Unemployment;

                case "08":
                    return Reason1.Disability;

                case "09":
                    return Reason1.Gambling;

                case "12":
                    return Reason1.ChildSupportAlimony;

                case "14":
                    return Reason1.StudentLoan;

                default:
                    return Reason1.Other;
            }
        }
    }
}
