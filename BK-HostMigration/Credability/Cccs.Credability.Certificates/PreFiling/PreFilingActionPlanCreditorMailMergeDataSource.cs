﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling.Reasons;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingActionPlanCreditorMailMergeDataSource : IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto DisplayData;
        private IList<CreditorItem> CreditorList;
        private int RecordIndex;
        public PreFilingActionPlanCreditorMailMergeDataSource(PreFilingSnapshotDto displayData)
        {
            DisplayData = displayData;
            RecordIndex = -1;
            CreditorList = new List<CreditorItem>();

            DisplayData.Creditors.ForEach(c =>
            {
                var item = new CreditorItem
                {
                    Name = c.Name,
                    Interest = c.InterestRate.ToString("0%"),
                    Balance = c.Balance.ToString("C"),
                    MonthlyPayment = c.MonthlyPayment.ToString("C"),
                    MonthsToRepay = c.MonthsToRepay.ToString(),
                };
                CreditorList.Add(item);
            });

            if (CreditorList.Count == 0)
            {
                var blankItem = new CreditorItem
                {
                    Name = String.Empty,
                    Interest = String.Empty,
                    Balance = String.Empty,
                    MonthlyPayment = String.Empty,
                    MonthsToRepay = String.Empty,
                };
                CreditorList.Add(blankItem);
        }
        }

        public string TableName
        {
            get { return "Creditor"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "CreditorName":
                    fieldValue = CreditorList[RecordIndex].Name;
                    return true;

                case "CreditorInterest":
                    fieldValue = CreditorList[RecordIndex].Interest;
                    return true;

                case "CreditorBalance":
                    fieldValue = CreditorList[RecordIndex].Balance;
                    return true;

                case "CreditorPayment":
                    fieldValue = CreditorList[RecordIndex].MonthlyPayment;
                    return true;

                case "CreditorMonthsToRepay":
                    fieldValue = CreditorList[RecordIndex].MonthsToRepay;
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == CreditorList.Count - 1); }
        }

        private class CreditorItem
        {
            public string Name { get; set; }
            public string Interest { get; set; }
            public string Balance { get; set; }
            public string MonthlyPayment { get; set; }
            public string MonthsToRepay { get; set; }
        }
    }
}
