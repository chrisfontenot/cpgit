﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.PreFiling
{
    [Serializable]
    public class CertificateDownloadException : Exception
    {
        public CertificateDownloadException()
        {
        }

        public CertificateDownloadException(string message)
            : base(message)
        {
        }
    }
}
