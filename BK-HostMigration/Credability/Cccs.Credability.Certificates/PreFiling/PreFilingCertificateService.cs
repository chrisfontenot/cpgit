﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Identity;
using System.Configuration;
using Aspose.Network.Ftp;
//using Cccs.Host;
using System.Globalization;
using CuttingEdge.Conditions;
//using Cccs.Host.Dto;
using Cccs.Response;
using StructureMap;
using Cccs.Debtplus;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingCertificateService : IPreFilingCertificateService
    {
        private const int CertificateValidMonths = 6;

        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        private readonly IUserRepository userRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IContactDetailRepository contactDetailRepository;
        private readonly ISnapshotRepository snapshotRepository;
        private readonly IExpenseRepository expenseRepository;
        //private readonly IHost hostService;
        private readonly IDebtplus debtplusService;
        private readonly ICounselingCertificateDownload certificateDownload;
        public PreFilingCertificateService(ILoggingService logger, IMappingEngine mapper,
            IUserRepository userRepository, IAccountRepository accountRepository,
            IContactDetailRepository contactDetailRepository, ISnapshotRepository snapshotRepository,
            IExpenseRepository expenseRepository, ICounselingCertificateDownload certificateDownload,
            IDebtplus debtplusService)
        {
            #region Parameter Validation
            Condition.Requires(logger, "logger").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();
            Condition.Requires(userRepository, "userRepository").IsNotNull();
            Condition.Requires(accountRepository, "accountRepository").IsNotNull();
            Condition.Requires(contactDetailRepository, "contactDetailRepository").IsNotNull();
            Condition.Requires(snapshotRepository, "snapshotRepository").IsNotNull();
            Condition.Requires(expenseRepository, "expenseRepository").IsNotNull();
            Condition.Requires(certificateDownload, "certificateDownload").IsNotNull();
            //Condition.Requires(hostService, "hostService").IsNotNull();
            Condition.Requires(debtplusService, "debtplusService").IsNotNull();
            #endregion

            this.logger = logger;
            this.userRepository = userRepository;
            this.accountRepository = accountRepository;
            this.contactDetailRepository = contactDetailRepository;
            this.snapshotRepository = snapshotRepository;
            this.expenseRepository = expenseRepository;
            this.mapper = mapper;
            this.certificateDownload = certificateDownload;
            //this.hostService = hostService;
            this.debtplusService = debtplusService;
        }

        public void ArchiveCertificates()
        {
            long userId;
            while (snapshotRepository.TryGetUnarchivedCertificateUser(out userId))
            {
                var primaryDetail = userRepository.GetDetail(userId, true);
                var secondaryDetail = userRepository.GetDetail(userId, false);
                var primaryData = snapshotRepository.GetMostRecent(primaryDetail.UserDetailId);
                var secondaryData = secondaryDetail == null ? null : snapshotRepository.GetMostRecent(secondaryDetail.UserDetailId);

                var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
                var generatorType = assembly.GetType(primaryData.CertificateVersionType);
                var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
                var stream = generator.CreateActionPlan(primaryData, secondaryData);
                stream.Position = 0;

                var host = ConfigurationManager.AppSettings["CertificateArchiveFtpHost"];
                var folders = new string[]
                {
                    primaryData.CourseCompleted.ToString("yyyy"),   // Year Folder
                    primaryData.CourseCompleted.ToString("MM"),     // Month Folder
                    ArchiveSundayDate(primaryData.CourseCompleted)  // Date Folder
                };

                using (var ftp = new FtpClient(host))
                {
                    ftp.Connect(true);

                    foreach (var folder in folders)
                    {
                        if (!ftp.Exists(folder))
                        {
                            ftp.MakeDirectory(folder);
                        }
                        ftp.ChangeDirectory(folder);
                    }

                    ftp.TransferType = TransferType.Binary;

                    var filename = String.Format("IN{0}.pdf", primaryData.ClientNumber);
                    var fileCounter = 0;
                    while (ftp.Exists(filename))
                    {
                        filename = String.Format("IN{0}-{1}.pdf", primaryData.ClientNumber, ++fileCounter);
                    }
                    ftp.Upload(stream, filename);
                    ftp.Disconnect();
                }

                primaryData.ArchiveStatus = 1;
                snapshotRepository.Update(primaryData);
                if (secondaryData != null)
                {
                    secondaryData.ArchiveStatus = 1;
                    snapshotRepository.Update(secondaryData);
                }
            }
        }

        private static string ArchiveSundayDate(DateTime courseCompleted)
        {
            // If it's the first of the month, then that's the day we use.
            if (courseCompleted.Day == 1)
            {
                return "01";
            }

            // Back up until we get to a Sunday, or the first of the month.
            var returnDate = courseCompleted;
            while (returnDate.Day > 1 && returnDate.DayOfWeek != DayOfWeek.Sunday)
            {
                returnDate = returnDate.AddDays(-1);
            }

            return returnDate.ToString("dd");
        }

        public void FillCertificates()
        {
            long preFilingSnapshotId;
            while (snapshotRepository.TryGetSnapshotWithMissingCertificate(out preFilingSnapshotId))
            {
                var snapshot = snapshotRepository.GetSnapshot(preFilingSnapshotId);
                try
                {
                    AssignCertificateToSnapshot(snapshot);
                }
                finally
                {
                    snapshot.ArchiveStatus = 0;
                    snapshotRepository.Update(snapshot);
                }
            }
        }

        public IList<ActionPlanDto> GetActionPlanList(long userId)
        {
            var list = snapshotRepository.GetActionPlanListByUser(userId);
            return list;
        }

        public PreFilingSnapshotDto GetCurrentSnapshotForUserDetail(long userDetailId)
        {
            var currentSnapshot = snapshotRepository.GetMostRecent(userDetailId);
            return currentSnapshot;
        }

        public PreFilingActionPlanDto GetActionPlan(long actionPlanId)
        {
            var actionPlan = snapshotRepository.GetActionPlan(actionPlanId);
            return actionPlan;
        }

        public PreFilingSnapshotDto GetSnapshot(long preFilingSnapshotId)
        {
            var snapshot = snapshotRepository.GetSnapshot(preFilingSnapshotId);
            return snapshot;
        }

        public PreFilingSnapshotDto GetSnapshotByCertificateNumber(string certificateNumber)
        {
            var snapshot = snapshotRepository.GetByCertificateNumber(certificateNumber);
            return snapshot;
        }

        public PreFilingSnapshotDto GetAssociatedSnapshotByCertificateNumber(string certificateNumber)
        {
            var snapshot = snapshotRepository.GetAssociatedByCertificateNumber(certificateNumber);
            return snapshot;
        }

        public IList<CertificateDto> GetCertificateListForUser(long userId)
        {
            var list = snapshotRepository.GetCertificateListByUser(userId);
            return list;
        }

        public bool HasCurrentSnapshot(long userDetailId)
        {
            var currentSnapshot = snapshotRepository.GetMostRecent(userDetailId);
            var snapshotIsCurrent = (currentSnapshot != null && currentSnapshot.CourseCompleted > DateTime.Now.AddMonths(-CertificateValidMonths));

            return snapshotIsCurrent;
        }

        public bool HasDownloadedCertificate(long userDetailId)
        {
            var currentSnapshot = snapshotRepository.GetMostRecent(userDetailId);
            if (currentSnapshot == null) return false;

            return !String.IsNullOrEmpty(currentSnapshot.CertificateNumber);
        }

        public void AssignCertificateToSnapshot(long preFilingSnapshotId)
        {
            var snapshot = snapshotRepository.GetSnapshot(preFilingSnapshotId);
            // NOTE: Remove Certificate Generation Temporarily
            //AssignCertificateToSnapshot(snapshot);
            snapshot.ArchiveStatus = 0;
            snapshotRepository.Update(snapshot);
        }

        private void AssignCertificateToSnapshot(PreFilingSnapshotDto snapshot)
        {
            if (!String.IsNullOrEmpty(snapshot.CertificateNumber))
                throw new InvalidOperationException("A certificate number is already assigned to this record.");

            // Bankruptcy Conversion
            //var districtCode = hostService.GetEoustJudicialDistrictByName(snapshot.JudicialDistrict);
            var districtCode = debtplusService.GetEoustJudicialDistrictByName(snapshot.JudicialDistrict);
            if (!districtCode.IsNullOrWhiteSpace())
            {
                DataResponse<string> dr = certificateDownload.GetCertificateNumber(snapshot.FirstName, snapshot.LastName, snapshot.CourseCompleted, districtCode);
                if (dr.IsSuccessful)
                {
                    snapshot.CertificateNumber = dr.Payload;
                }
                else
                {
                    return;
                }
            }
            else
            {
                var userDetail = userRepository.GetDetail(snapshot.UserDetailId);
                snapshot.CertificateNumber = String.Format("IN{0}-{1}", snapshot.ClientNumber, userDetail.IsPrimary ? "1" : "2");
            }
            AssignCertificateToHost(snapshot);
        }

        public PreFilingActionPlanDto CreateActionPlan(PreFilingSnapshotDto primarySnapshot, PreFilingSnapshotDto secondarySnapshot)
        {
            var actionPlan = snapshotRepository.CreateActionPlan(primarySnapshot, secondarySnapshot);

            return actionPlan;
        }

        public PreFilingSnapshotDto ReIssueSnapshotData(PreFilingSnapshotDto snapshot)
        {
            snapshot.CertificateNumber = String.Empty;
            var preFilingSnapshotId = snapshotRepository.Insert(snapshot);

            var dto = snapshotRepository.GetSnapshot(preFilingSnapshotId);
            return dto;
        }

        public virtual void SnapshotData(long userDetailId, string creditScore, string certificateVersionType)
        {
            var userDetail = userRepository.GetDetail(userDetailId);
            if (userDetail == null)
                new UserDetailNotFoundException(Resources.ErrorMessages.UserDetailNotFound.FormatWith(userDetailId)).LogAndThrow(logger);

            // We can only add one snapshot per 6 months.
            var snapshotDate = snapshotRepository.GetMostRecentDate(userDetailId);
            if (snapshotDate > DateTime.Now.AddMonths(-CertificateValidMonths))
                new PreFilingSnapshotException(Resources.ErrorMessages.SnapshotWithinSixMonths.FormatWith(userDetailId, snapshotDate)).LogAndThrow(logger);

            var account = accountRepository.GetPreFilingAccountByUserDetail(userDetailId);
            if (account == null)
                new PreFilingAccountNotFoundException(Resources.ErrorMessages.PreFilingAccountNotFoundForUserDetail.FormatWith(userDetailId)).LogAndThrow(logger);

            var contactDetail = contactDetailRepository.Get(account.InternetId);
            if (contactDetail == null)
                new ContactDetailNotFoundException(account.InternetId, Resources.ErrorMessages.ContactDetailNotFound.FormatWith(account.InternetId)).LogAndThrow(logger);

            // Bankruptcy Conversion
            //var judicialDistrict = hostService.GetJudicialDistrict(userDetail.PostalCode);
            var judicialDistrict = debtplusService.GetJudicialDistrict(userDetail.PostalCode);
            var snapshot = new PreFilingSnapshotDto();
            snapshot.JudicialDistrict = judicialDistrict.DistrictName;
            snapshot.CertificateVersionType = certificateVersionType;

            short creditScoreValue;
            if (Int16.TryParse(creditScore, out creditScoreValue))
            {
                snapshot.CreditScore = creditScoreValue;
            }

            mapper.Map<PreFilingUserDetailDto, ISnapshotUserInfo>(userDetail, snapshot);
            mapper.Map<PreFilingContactDetailDto, ISnapshotContactDetailInfo>(contactDetail, snapshot);

            byte displayOrder = 1;
            mapper.Map<IList<PreFilingContactDetailCreditorDto>, IList<PreFilingSnapshotCreditorDto>>(contactDetail.Creditors, snapshot.Creditors);
            snapshot.Creditors.ForEach(c => c.DisplayOrder = displayOrder++);

            var culture = new CultureInfo(userDetail.LanguageCode);
            var expenseCategories = ExpenseCategories(snapshot.IncomeAfterTaxes, contactDetail.Budget, culture);
            expenseCategories.ForEach(c => snapshot.ExpenseCategories.Add(c));

            var expenseCodes = ExpenseCodes(contactDetail.Budget, expenseCategories, culture);
            expenseCodes.ForEach(c => snapshot.ExpenseCodes.Add(c));

            SetDmpValues(snapshot);
            snapshotRepository.Insert(snapshot);
        }

        private IList<PreFilingSnapshotExpenseCategoryDto> ExpenseCategories(decimal netIncome, IEnumerable<PreFilingContactDetailBudgetDto> budgetList, CultureInfo culture)
        {
            var expenseCategories = expenseRepository.GetExpenseCategories();

            var returnList = new List<PreFilingSnapshotExpenseCategoryDto>();
            foreach (var category in expenseCategories)
            {
                var categorySum = (from c in category.ExpenseCodes
                                   join b in budgetList on c.LookupCode equals b.LookupCode
                                   select b.Amount).Sum();

                var item = new PreFilingSnapshotExpenseCategoryDto
                {
                    DisplayOrder = category.DisplayOrder,
                    DisplayText = Resources.ExpenseCodes.ResourceManager.GetString(category.LookupCode, culture),
                    ExpenseCategory = category.Name,
                    ExpenseCategoryId = category.ExpenseCategoryId,
                    RecommendedPercentage = category.RecommendedPercentage,
                    RecommendedTotal = netIncome * category.RecommendedPercentage,
                    SessionAmount = categorySum,
                };

                returnList.Add(item);
            }

            var sessionTotal = (from c in returnList
                                select c.SessionAmount).Sum();
			if(sessionTotal > (decimal) 0.0)
			{
				foreach(var item in returnList)
				{
					item.PercentOfTotal = item.SessionAmount / sessionTotal;
				}
			}
            return returnList;
        }

        private IList<PreFilingSnapshotExpenseCodeDto> ExpenseCodes(IEnumerable<PreFilingContactDetailBudgetDto> budgetList, IList<PreFilingSnapshotExpenseCategoryDto> categoryList, CultureInfo culture)
        {
            var expenseCategories = expenseRepository.GetExpenseCategories();
            var returnList = new List<PreFilingSnapshotExpenseCodeDto>();
            foreach (var category in expenseCategories)
            {
                var codes = (from c in category.ExpenseCodes
                             join b in budgetList on c.LookupCode equals b.LookupCode
                             select new PreFilingSnapshotExpenseCodeDto
                             {
                                 DisplayOrder = c.DisplayOrder,
                                 DisplayText = Resources.ExpenseCodes.ResourceManager.GetString(c.LookupCode, culture),
                                 ExpenseCategoryId = category.ExpenseCategoryId,
                                 ExpenseCode = c.Name,
                                 ExpenseCodeId = c.ExpenseCodeId,
                                 SessionAmount = b.Amount,
                             }).ToList();

                returnList.AddRange(codes);
            }

            return returnList;
        }

        public void SetDmpValues(PreFilingSnapshotDto snapshot)
        {
            var defaultDmpPrincipalPayment = .0225m;
            var defaultDmpInterestRate = .09m;
            var defaultDmpMinimumPayment = 10m;


            // Calculate each creditor's minimum payment.
            var dmpList = new List<CreditorDmpPayment>();
            var dmpProposedList = new List<CreditorDmpPayment>();
            foreach (var item in snapshot.Creditors)
            {
                // var interestRate = item.InterestRate < defaultDmpInterestRate ? item.InterestRate : defaultDmpInterestRate;
                // var interestRate =;


                //   var firstMonthInterest = (item.Balance * monthlyInterestRate).CeilingByDecimals(2);
                //   var firstMonthPrincipalPayment = (item.Balance * defaultDmpPrincipalPayment).CeilingByDecimals(2);
                //  var firstMonthPrincipalPayment = (item.Balance * defaultDmpPrincipalPayment).CeilingByDecimals(2) + firstMonthInterest;

                //   if (firstMonthPrincipalPayment < defaultDmpMinimumPayment)
                //     firstMonthPrincipalPayment = defaultDmpMinimumPayment;

                var monthlyInterestRate = item.InterestRate / 12m;

                var firstMonthPrincipalPayment = item.MonthlyPayment < defaultDmpMinimumPayment ? defaultDmpMinimumPayment : item.MonthlyPayment;
                var payment = new CreditorDmpPayment()
                 {
                     //MonthlyPayment = firstMonthPrincipalPayment,
                     // if the default monthly payment is less than 10 set it to 10.
                     MonthlyPayment = firstMonthPrincipalPayment,
                     Balance = item.Balance,
                     MonthlyInterestRate = monthlyInterestRate,
                     MonthsToRepay = 0

                 };
                dmpList.Add(payment);
            }

            // calculate DMP Payment
            snapshot.DmpBalance = (from c in snapshot.Creditors select c.Balance).Sum();
            snapshot.DmpInterestRate = defaultDmpInterestRate;
            //  snapshot.DmpMonthlyPayment = (from d in dmpList select d.MonthlyPayment).Sum();
            snapshot.DmpMonthlyPayment = (snapshot.DmpBalance * defaultDmpPrincipalPayment).CeilingByDecimals(2);

            var DmpProposed = new CreditorDmpPayment
            {
                MonthlyPayment = snapshot.DmpMonthlyPayment,
                Balance = snapshot.DmpBalance,
                MonthlyInterestRate = snapshot.DmpInterestRate / 12m,
                MonthsToRepay = 0

            };
            dmpProposedList.Add(DmpProposed);

            IDmpCalculationService service = ObjectFactory.GetInstance<IDmpCalculationService>();
            service.CalculateMonthsToRepay(dmpList);
            service.CalculateMonthsToRepay(dmpProposedList);

            snapshot.DmpMonthsToRepay = dmpProposedList[0].MonthsToRepay;
            for (int i = 0; i < snapshot.Creditors.Count; i++)
            {
                snapshot.Creditors[i].MonthsToRepay = dmpList[i].MonthsToRepay;
            }
        }

        private bool AssignCertificateToHost(PreFilingSnapshotDto snapshot)
        {
            // Bankruptcy Conversion
            //CertReIssue CertData = new CertReIssue();
            Cccs.Debtplus.Data.CertReIssue CertData = new Cccs.Debtplus.Data.CertReIssue();
            var CurDetail = userRepository.GetDetail(snapshot.UserDetailId);
            if (CurDetail != null)
            {
                CertData.IsPrimary = CurDetail.IsPrimary;
                var CurAccount = accountRepository.GetPreFilingAccountByUserDetail(snapshot.UserDetailId);
                if (CurAccount != null && !CurAccount.HostId.IsNullOrWhiteSpace() && CurAccount.InternetId == snapshot.ClientNumber)
                {
                    CertData.CertIssue = snapshot.CertificateGenerated;
                    CertData.CertNo = snapshot.CertificateNumber;
                    CertData.ClNo = CurAccount.HostId;

                    // Bankruptcy Conversion
                    //return hostService.SetCertNo(CertData);
                    return debtplusService.SetCertNo(CertData);
                }
            }
            return false;
        }


    }
}
