﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling.Reasons;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingActionPlanMailMergeDataSource : IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto PrimaryData;
        private readonly PreFilingSnapshotDto SecondaryData;
        private readonly PreFilingSnapshotDto DisplayData;
        private int RecordIndex;

        public PreFilingActionPlanMailMergeDataSource(CultureInfo culture, PreFilingSnapshotDto displayData, PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData)
        {
            DisplayData = displayData;
            PrimaryData = primaryData;
            SecondaryData = secondaryData;
            RecordIndex = -1;
            Reason1.Culture = culture;
        }

        public string TableName
        {
            get { return "Certificate"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "CertificateNumber":
                    fieldValue = String.Format("{0}{1}",
                        PrimaryData.CertificateNumber,
                        SecondaryData != null ? "\n" + SecondaryData.CertificateNumber : String.Empty);
                    return true;

                case "ClientNumber":
                    fieldValue = DisplayData.ClientNumber;
                    return true;

                case "CertDate":
                    fieldValue = DisplayData.CourseCompleted.ToString("MM/dd/yyyy");
                    return true;

                case "CertTime":
                    fieldValue = DisplayData.CourseCompleted.ToString("hh:mm");
                    return true;

                case "CertZone":
                    fieldValue = String.Format("{0:tt} EST", DisplayData.CourseCompleted);
                    return true;

                case "Name":
                    fieldValue = String.Format("{0}{1}",
                        FormatName(PrimaryData),
                        SecondaryData != null ? " and " + FormatName(SecondaryData) : String.Empty);
                    return true;

                case "AddressBlock":
                    fieldValue = String.Format("{0}\n{1}{2}, {3} {4}",
                        DisplayData.StreetLine1.Trim(),
                        String.IsNullOrEmpty(DisplayData.StreetLine2) ? String.Empty : String.Format("{0}\n", DisplayData.StreetLine2.Trim()),
                        DisplayData.City.Trim(),
                        DisplayData.State,
                        DisplayData.PostalCode);
                    return true;

                case "SsnMasked":
                    fieldValue = String.Format("{0}{1}",
                        String.Format("###-##-{0}", PrimaryData.SsnLast4),
                        SecondaryData != null ? String.Format("\n###-##-{0}", SecondaryData.SsnLast4) : String.Empty);
                    return true;

                case "NetIncome":
                    fieldValue = DisplayData.IncomeAfterTaxes.ToString("C");
                    return true;

                case "LivingExpenses":
                    fieldValue = DisplayData.LivingExpenses.ToString("C");
                    return true;

                case "DebtPayment":
                    fieldValue = DisplayData.DebtPayment.ToString("C");
                    return true;

                case "CashAtMonthEnd":
                    fieldValue = DisplayData.CashAtMonthEnd.ToString("C");
                    return true;

                case "TotalAssets":
                    fieldValue = DisplayData.TotalAssets.ToString("C");
                    return true;

                case "TotalLiabilities":
                    fieldValue = DisplayData.TotalLiabilities.ToString("C");
                    return true;

                case "NetWorth":
                    fieldValue = DisplayData.NetWorth.ToString("C");
                    return true;

                case "Reason":
                    fieldValue = DetermineReason(DisplayData.Reason);
                    return true;

                case "JudicialDistrict":
                    fieldValue = DisplayData.JudicialDistrict;
                    return true;

                case "CounselorNameSignature":
                    fieldValue = String.Format("S/{0}", DisplayData.CounselorName);
                    return true;

                case "CounselorName":
                    fieldValue = DisplayData.CounselorName;
                    return true;

                case "Title":
                    fieldValue = "Counselor";
                    return true;

                case "PrimaryCreditScore":
                    fieldValue = PrimaryData.CreditScore == 0 ? "N/A" : PrimaryData.CreditScore.ToString();
                    return true;

                case "SecondaryCreditScore":
                    fieldValue = SecondaryData == null || SecondaryData.CreditScore == 0 ? "N/A" : SecondaryData.CreditScore.ToString();
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        private string FormatName(PreFilingSnapshotDto data)
        {
            return String.Format("{0}{1}{2}",
                data.FirstName,
                String.IsNullOrEmpty(data.MiddleName) ? String.Empty : String.Format(" {0}", data.MiddleName),
                String.Format(" {0}", data.LastName));
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == 0); }
        }

        private string DetermineReason(string reasonCode)
        {
            switch (reasonCode)
            {
                case "01":
                    return Reason1.OverObligation;

                case "02":
                    return Reason1.ReducedIncome;

                case "03":
                    return Reason1.MedicalAccident;

                case "04":
                    return Reason1.DeathOfAFamilyMember;

                case "05":
                    return Reason1.DivorceSeparation;

                case "06":
                    return Reason1.Unemployment;

                case "08":
                    return Reason1.Disability;

                case "09":
                    return Reason1.Gambling;

                case "12":
                    return Reason1.ChildSupportAlimony;

                case "14":
                    return Reason1.StudentLoan;

                default:
                    return Reason1.Other;
            }
        }
    }
}
