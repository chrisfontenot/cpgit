﻿using System.IO;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates.PreFiling
{
    public interface IDebtAnalysisGenerator
    {
        MemoryStream CreateDebtAnalysis(PreFilingSnapshotDto snapshotData);
    }
}
