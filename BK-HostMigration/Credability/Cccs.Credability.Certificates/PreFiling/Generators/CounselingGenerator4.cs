﻿using System.IO;
using System.Reflection;
using System.Linq;
using Barcode = Aspose.BarCode;
using Doc = Aspose.Words;
using Cccs.Credability.Certificates.DataTransferObjects;
using Aspose.Words.Reporting;
using Aspose.Words;
using System;
using Aspose.Words.Tables;
using Aspose.Chart;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling.Generators
{
    public class CounselingGenerator4 : IPreFilingCertificateGenerator, IDebtAnalysisGenerator
    {
        private const string EnglishActionPlanDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingActionPlan3_en.docx";
        private const string SpanishActionPlanDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingActionPlan3_es.docx";
        private const string EnglishDebtAnalysisDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.DebtAnalysis1_en.docx";
        private const string SpanishDebtAnalysisDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.DebtAnalysis1_es.docx";
        private const string CertificateDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingCertificate1.docx";

        /// <summary>
        /// Creates an Action Plan PDF document with both primary and secondary certificates included.
        /// </summary>
        /// <param name="primaryData">The primary client's data.</param>
        /// <param name="secondaryData">The secondary client's data. Null if no secondary client.</param>
        /// <returns>A MemoryStream containing the PDF document created.</returns>
        public MemoryStream CreateActionPlan(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData)
        {
            var culture = new CultureInfo(primaryData.LanguageCode);
            Resources.ExpenseCodes.Culture = culture;

            var actionPlanDocumentName = primaryData.LanguageCode.ToLower() == "es" ? SpanishActionPlanDocumentName : EnglishActionPlanDocumentName;
            var assembly = Assembly.GetExecutingAssembly();
            using (var actionStream = assembly.GetManifestResourceStream(actionPlanDocumentName))
            using (var certificateStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var displayData = secondaryData != null && secondaryData.CourseCompleted > primaryData.CourseCompleted ? secondaryData : primaryData;

            var actionPlanDataSource = new PreFilingActionPlanMailMergeDataSource(culture, displayData, primaryData, secondaryData);
            var budgetDataSource = new PreFilingActionPlanBudgetPlanMailMergeDataSource(culture, displayData);
            var creditorDataSource = new PreFilingActionPlanCreditorMailMergeDataSource(displayData);
            var dmpDataSource = new PreFilingActionPlanDmpMailMergeDataSource(culture, displayData);

            var actionDocument = new Doc.Document(actionStream);
            actionDocument.MailMerge.Execute(actionPlanDataSource);
            actionDocument.MailMerge.FieldMergingCallback = new HandleBudgetPlanMergeField(actionDocument);
            actionDocument.MailMerge.ExecuteWithRegions(budgetDataSource);
            actionDocument.MailMerge.ExecuteWithRegions(creditorDataSource);
            actionDocument.MailMerge.ExecuteWithRegions(dmpDataSource);

                using (var budgetPlanActualChartStream = GetActualBudgetChartStream(primaryData, culture))
                using (var budgetPlanRecommendedChartStream = GetRecommendedBudgetChartStream(primaryData, culture))
                {
            var builder = new DocumentBuilder(actionDocument);
            builder.MoveToBookmark("BudgetPlanActualChart");
            budgetPlanActualChartStream.Position = 0;
            builder.InsertImage(budgetPlanActualChartStream);

            builder.MoveToBookmark("BudgetPlanRecommendedChart");
            budgetPlanRecommendedChartStream.Position = 0;
            builder.InsertImage(budgetPlanRecommendedChartStream);

                    // NOTE: Remove Certificate Generation Temporarily
                    //var primaryDocument = GenerateCertificateDocument(primaryData, certificateStream);
                    //actionDocument.AppendDocument(primaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);

                    //if (secondaryData != null)
                    //{
                    //    var secondaryDocument = GenerateCertificateDocument(secondaryData, certificateStream);
                    //    actionDocument.AppendDocument(secondaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);
                    //}

                    var returnStream = null as MemoryStream;
                    var tempStream = new MemoryStream();
                    try
                    {
                        actionDocument.Save(tempStream, Doc.SaveFormat.Pdf);

                        returnStream = tempStream;
                        tempStream = null;
                    }
                    finally
                    {
                        if (tempStream != null)
                        {
                            tempStream.Close();
                        }
                    }

            return returnStream;
        }
            }
        }

        private static MemoryStream GetActualBudgetChartStream(PreFilingSnapshotDto primaryData, CultureInfo culture)
        {
            var actual = new Chart();

            var glossEffect = new EffectGloss();
            glossEffect.BorderWidth = 4;

            var series = new Series();
            series.ChartType = ChartType.Pie;
            series.Effect = glossEffect;
            series.CustomAttributes.IsAllExploded = true;
            series.CustomAttributes.ExplodedWidthRate = 0.1f;

            for (int i = 0; i < primaryData.ExpenseCategories.Count; i++)
            {
                var pointName = String.Format("{0} {1:0%}", primaryData.ExpenseCategories[i].DisplayText, primaryData.ExpenseCategories[i].PercentOfTotal);
                series.DataPoints.Add(new DataPoint(pointName, i, primaryData.ExpenseCategories[i].PercentOfTotal));
            }
            ApplyBudgetChartProperties(actual, Resources.ExpenseCodes.ResourceManager.GetString("ActualBudget", culture), series);

            var returnStream = null as MemoryStream;
            var tempStream = new MemoryStream();
            try
            {
                actual.Save(tempStream, ImageFormat.Bmp);

                returnStream = tempStream;
                tempStream = null;
        }
            finally
            {
                if (tempStream != null)
                    tempStream.Close();
            }

            return returnStream;
        }

        private static MemoryStream GetRecommendedBudgetChartStream(PreFilingSnapshotDto primaryData, CultureInfo culture)
        {
            var chart = new Chart();

            var glossEffect = new EffectGloss();
            var series = new Series();
            series.ChartType = ChartType.Pie;
            series.Effect = glossEffect;
            series.CustomAttributes.IsAllExploded = true;
            series.CustomAttributes.ExplodedWidthRate = 0.1f;

            for (int i = 0; i < primaryData.ExpenseCategories.Count; i++)
            {
                var pointName = String.Format("{0} {1:0%}", primaryData.ExpenseCategories[i].DisplayText, primaryData.ExpenseCategories[i].RecommendedPercentage);
                series.DataPoints.Add(new DataPoint(pointName, i, primaryData.ExpenseCategories[i].RecommendedPercentage));
            }
            ApplyBudgetChartProperties(chart, Resources.ExpenseCodes.ResourceManager.GetString("RecommendedBudget", culture), series);

            var returnStream = null as MemoryStream;
            var tempStream = new MemoryStream();
            try
            {
                chart.Save(tempStream, ImageFormat.Bmp);

                returnStream = tempStream;
                tempStream = null;
        }
            finally
            {
                if (tempStream != null)
                    tempStream.Close();
            }

            return returnStream;
        }

        private static void ApplyBudgetChartProperties(Chart chart, string title, Series series)
        {
            var chartTitle = new Title(title);
            chartTitle.Font = new System.Drawing.Font("Calibri", 14f);
            chart.Titles.Add(chartTitle);

            chart.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            chart.BackColor = Color.White;

            chart.ChartArea.BackColor = Color.White;
            chart.ChartArea.Margin = 0;
            chart.ChartArea.Transparence = 255;

            chart.ChartArea.LegendBox.BackColor = Color.White;
            chart.ChartArea.LegendBox.LegendPositionType = LegendPositionType.Bottom;
            chart.ChartArea.LegendBox.LayoutType = LayoutType.Row;
            chart.ChartArea.LegendBox.LegendBoxType = LegendBoxType.OutsideChartArea;
            chart.ChartArea.LegendBox.SpacingType = SpacingType.Margin;
            chart.ChartArea.LegendBox.Font = new System.Drawing.Font("Calibri", 12f);

            chart.Height = 250f;
            chart.Width = 250f;

            chart.SeriesCollection.Add(series);
        }

        public MemoryStream CreateDebtAnalysis(PreFilingSnapshotDto snapshotData)
        {
            var documentName = snapshotData.LanguageCode.ToLower() == "es" ? SpanishDebtAnalysisDocumentName : EnglishDebtAnalysisDocumentName;
            var assembly = Assembly.GetExecutingAssembly();
            using (var documentStream = assembly.GetManifestResourceStream(documentName))
            {
                var culture = new CultureInfo(snapshotData.LanguageCode);
                Resources.ExpenseCodes.Culture = culture;

                var analysisDataSource = new PreFilingDebtAnalysisMailMergeDataSource(snapshotData);
                var creditorDataSource = new PreFilingActionPlanCreditorMailMergeDataSource(snapshotData);
                var dmpDataSource = new PreFilingActionPlanDmpMailMergeDataSource(culture, snapshotData);

                var document = new Doc.Document(documentStream);
                document.MailMerge.Execute(analysisDataSource);
                document.MailMerge.ExecuteWithRegions(creditorDataSource);
                document.MailMerge.ExecuteWithRegions(dmpDataSource);

                using (var chartStream = GetDebtAnalysisChartStream(snapshotData))
                {
                    var builder = new DocumentBuilder(document);
                    builder.MoveToBookmark("DebtAnalysisChart");
                    chartStream.Position = 0;
                    builder.InsertImage(chartStream);
                }

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
                    document.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

                return returnStream;
            }
        }

        private static MemoryStream GetDebtAnalysisChartStream(PreFilingSnapshotDto snapshotData)
        {
            var actualTotal = snapshotData.Creditors.Sum(c => c.Balance);
            var actualLength = snapshotData.Creditors.Max(c => c.MonthsToRepay);

            var credabilityTotal = snapshotData.DmpBalance;
            var credabilityLength = snapshotData.DmpMonthsToRepay;

            var maxLength = actualLength > credabilityLength ? actualLength : credabilityLength;

            var actualSeries = new Series();
            actualSeries.Name = "Self";
            actualSeries.ChartType = ChartType.Line;
            actualSeries.DataPoints.Add(new DataPoint(0, actualTotal));
            actualSeries.DataPoints.Add(new DataPoint(actualLength, 0));

            var credabilitySeries = new Series();
            credabilitySeries.Name = "ClearPoint Credit Counseling Solutions";
            credabilitySeries.ChartType = ChartType.Line;
            credabilitySeries.DataPoints.Add(new DataPoint(0, credabilityTotal));
            credabilitySeries.DataPoints.Add(new DataPoint(credabilityLength, 0));

            var chart = new Chart();
            ApplyDebtAnalysisChartProperties(chart, Resources.ExpenseCodes.ResourceManager.GetString("YearsToRepay"));
            chart.SeriesCollection.Add(actualSeries);
            chart.SeriesCollection.Add(credabilitySeries);

            chart.ChartArea.AxisX.Title.Text = "Years to Repay";
            chart.ChartArea.AxisX.IsAutoCalc = false;
            chart.ChartArea.AxisX.Interval = 12;
            chart.ChartArea.AxisX.Minimum = 0;
            chart.ChartArea.AxisX.Maximum = maxLength % 12 > 0 ? maxLength + 12 - (maxLength % 12) : maxLength;

            chart.ChartArea.AxisY.Title.Text = "Total Debt";
            chart.ChartArea.AxisY.IsAutoCalc = false;
            chart.ChartArea.AxisY.Minimum = 0;

            var year = 0;
            foreach (AxisLabel label in chart.ChartArea.AxisX.AxisLabels)
            {
                label.Text = String.Format("Year {0}", year++);
            }
            foreach (AxisLabel label in chart.ChartArea.AxisY.AxisLabels)
            {
                label.Text = String.Format("{0:C}", Double.Parse(label.Text));
            }

            var returnStream = null as MemoryStream;
            var tempStream = new MemoryStream();
            try
            {
                chart.Save(tempStream, ImageFormat.Bmp);

                returnStream = tempStream;
                tempStream = null;
            }
            finally
            {
                if (tempStream != null)
                    tempStream.Close();
            }

            return returnStream;
        }

        private static void ApplyDebtAnalysisChartProperties(Chart chart, string title)
        {
            var chartTitle = new Title(title);
            chartTitle.Font = new System.Drawing.Font("Calibri", 14f);
            chart.Titles.Add(chartTitle);

            chart.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            chart.BackColor = Color.White;

            chart.ChartArea.BackColor = Color.White;
            chart.ChartArea.Margin = 10;
            chart.ChartArea.Transparence = 255;

            chart.ChartArea.LegendBox.BackColor = Color.White;
            chart.ChartArea.LegendBox.LegendPositionType = LegendPositionType.RightMiddle;
            chart.ChartArea.LegendBox.LayoutType = LayoutType.Column;
            chart.ChartArea.LegendBox.LegendBoxType = LegendBoxType.OutsideChartArea;
            chart.ChartArea.LegendBox.SpacingType = SpacingType.Margin;
            chart.ChartArea.LegendBox.Font = new System.Drawing.Font("Calibri", 12f);

            chart.Height = 250f;
            chart.Width = 600f;
        }

        public MemoryStream CreateCertificate(PreFilingSnapshotDto data)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var documentStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var document = GenerateCertificateDocument(data, documentStream);

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
                    document.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        public MemoryStream CreateCombinedCertificate(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (var documentStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var primaryDocument = GenerateCertificateDocument(primaryData, documentStream);
            if (secondaryData != null)
            {
                var secondaryDocument = GenerateCertificateDocument(secondaryData, documentStream);
                primaryDocument.AppendDocument(secondaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);
            }

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
                    primaryDocument.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        private Doc.Document GenerateCertificateDocument(PreFilingSnapshotDto data, Stream documentStream)
        {
            documentStream.Position = 0;
            var document = new Doc.Document(documentStream);

            var dataSource = new CertificateMailMergeDataSource(data);
            document.MailMerge.Execute(dataSource);
            using (var barcode = new Barcode.BarCodeBuilder(data.CertificateNumber, Barcode.Symbology.Code128))
            {
            InsertBarcodeImage(document, barcode);
            }

            return document;
        }

        private void InsertBarcodeImage(Doc.Document document, Barcode.BarCodeBuilder barcode)
        {
            var builder = new Doc.DocumentBuilder(document);
            builder.MoveToBookmark("BarcodeImage");

            barcode.AutoSize = true;
            barcode.GraphicsUnit = System.Drawing.GraphicsUnit.Pixel;
            barcode.BarHeight = 35;
            builder.InsertImage(barcode.BarCodeImage);
        }

        private class HandleBudgetPlanMergeField : IFieldMergingCallback
        {
            private readonly DocumentBuilder builder;
            private int rowIndex;
            internal HandleBudgetPlanMergeField(Document document)
            {
                builder = new DocumentBuilder(document);
                rowIndex = 0;   // There is one header row. Skip it.
            }

            public void FieldMerging(FieldMergingArgs args)
            {
                if (args.FieldName.Equals("BudgetPlanRecommendedTotal"))
                {
                    var hasValue = !String.IsNullOrEmpty(Convert.ToString(args.FieldValue));
                    rowIndex++;

                    if (!hasValue) return;

                    builder.MoveToSection(1);
                    for (int columnIndex = 0; columnIndex < 5; columnIndex++)
                    {
                        builder.MoveToCell(2, rowIndex, columnIndex, 0);
                        builder.CurrentParagraph.ParagraphFormat.StyleName = "BudgetPlanBold";
                    }
                }
            }

            public void ImageFieldMerging(ImageFieldMergingArgs args)
            {
                // Do Nothing
            }
        }
    }
}
