﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using Doc = Aspose.Words;
using Barcode = Aspose.BarCode;
using System.Drawing.Imaging;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.DataTransferObjects;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling.Generators
{
    public class CounselingGenerator1 : IPreFilingCertificateGenerator
    {
        private const string EnglishActionPlanDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingActionPlan1_en.docx";
        private const string SpanishActionPlanDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingActionPlan1_es.docx";
        private const string CertificateDocumentName = "Cccs.Credability.Certificates.PreFiling.Documents.CounselingCertificate1.docx";

        /// <summary>
        /// Creates an Action Plan PDF document with both primary and secondary certificates included.
        /// </summary>
        /// <param name="primaryData">The primary client's data.</param>
        /// <param name="secondaryData">The secondary client's data. Null if no secondary client.</param>
        /// <returns>A MemoryStream containing the PDF document created.</returns>
        public MemoryStream CreateActionPlan(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var actionPlanDocumentName = primaryData.LanguageCode.ToLower() == "es" ? SpanishActionPlanDocumentName : EnglishActionPlanDocumentName;

            using (var actionStream = assembly.GetManifestResourceStream(actionPlanDocumentName))
            using (var certificateStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var culture = new CultureInfo(primaryData.LanguageCode);
            var actionPlanDataSource = new PreFilingActionPlanMailMergeDataSource(culture, primaryData, primaryData, secondaryData);
            var actionDocument = new Doc.Document(actionStream);
            actionDocument.MailMerge.Execute(actionPlanDataSource);

            var primaryDocument = GenerateCertificateDocument(primaryData, certificateStream);
            actionDocument.AppendDocument(primaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);

            if (secondaryData != null)
            {
                var secondaryDocument = GenerateCertificateDocument(secondaryData, certificateStream);
                actionDocument.AppendDocument(secondaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);
            }

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
                    actionDocument.Save(tempStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        public MemoryStream CreateCertificate(PreFilingSnapshotDto data)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var documentStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var document = GenerateCertificateDocument(data, documentStream);

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
            document.Save(returnStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        public MemoryStream CreateCombinedCertificate(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData)
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var documentStream = assembly.GetManifestResourceStream(CertificateDocumentName))
            {
            var primaryDocument = GenerateCertificateDocument(primaryData, documentStream);
            if (secondaryData != null)
            {
                var secondaryDocument = GenerateCertificateDocument(secondaryData, documentStream);
                primaryDocument.AppendDocument(secondaryDocument, Doc.ImportFormatMode.KeepSourceFormatting);
            }

                var returnStream = null as MemoryStream;
                var tempStream = new MemoryStream();
                try
                {
            primaryDocument.Save(returnStream, Doc.SaveFormat.Pdf);

                    returnStream = tempStream;
                    tempStream = null;
                }
                finally
                {
                    if (tempStream != null)
                        tempStream.Close();
                }

            return returnStream;
        }
        }

        private Doc.Document GenerateCertificateDocument(PreFilingSnapshotDto data, Stream documentStream)
        {
            documentStream.Position = 0;
            var document = new Doc.Document(documentStream);

            var dataSource = new CertificateMailMergeDataSource(data);
            document.MailMerge.Execute(dataSource);
            using (var barcode = new Barcode.BarCodeBuilder(data.CertificateNumber, Barcode.Symbology.Code128))
            {
            InsertBarcodeImage(document, barcode);
            }

            return document;
        }

        private void InsertBarcodeImage(Doc.Document document, Barcode.BarCodeBuilder barcode)
        {
            var builder = new Doc.DocumentBuilder(document);
            builder.MoveToBookmark("BarcodeImage");

            barcode.AutoSize = true;
            barcode.GraphicsUnit = System.Drawing.GraphicsUnit.Pixel;
            barcode.BarHeight = 35;
            builder.InsertImage(barcode.BarCodeImage);
        }
    }
}
