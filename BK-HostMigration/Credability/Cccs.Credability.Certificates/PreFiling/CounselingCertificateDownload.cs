﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Configuration;
using CuttingEdge.Conditions;
using HtmlAgilityPack;
//using Cccs.Host;
using Cccs.Response;
using Cccs.Debtplus;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class CounselingCertificateDownload : ICounselingCertificateDownload
    {
        private const string JSessionId = "JSESSIONID";
        //private const string LoginUrl = "https://ccdecert.ustp.usdoj.gov/ccdecert/loginConfirm.do";
        //private const string CreateCertificateUrl = "https://ccdecert.ustp.usdoj.gov/ccdecert/createCCCertificate.do";
        private string LoginUrl; // = "https://ccdecert.ustp.usdoj.gov/ccdecert/loginConfirm.do";
        private string CreateCertificateUrl; // = "https://ccdecert.ustp.usdoj.gov/ccdecert/createCCCertificate.do";
        private string AgentID = "";
        private string UserName = "";
        private string Password = "";

        // Bankruptcy Conversion
        //private readonly IHost hostService;
        //public CounselingCertificateDownload(IHost hostService)
        private readonly IDebtplus debtplusService;
        public CounselingCertificateDownload(IDebtplus debtplusService)
        {
            //this.hostService = hostService;
            this.debtplusService = debtplusService;
            this.LoginUrl = ConfigurationManager.AppSettings["CGSLoginUrl"].ToString();
            this.CreateCertificateUrl = ConfigurationManager.AppSettings["CGSCreateCertificateUrl"].ToString();
            this.AgentID  = ConfigurationManager.AppSettings["CGSAgentID"].ToString();
            this.UserName = ConfigurationManager.AppSettings["CGSUserName"].ToString();
            this.Password = ConfigurationManager.AppSettings["CGSPassword"].ToString();
        }

        /// <summary>
        /// Performs a login to the EOUST website.
        /// </summary>
        /// <returns>A cookie containing the JSessionId for use in tying request to the user's authenticated session.</returns>
        private Cookie LoginToWebsite()
        {
            // Bankruptcy Conversion
            //var login = hostService.GetEoustLogin();

            var login = debtplusService.GetEoustLogin();

            var postData = new Dictionary<string, string>();
            //postData.Add("agencyId", "ga02114");
            //postData.Add("agencyUserId", login.Username);
            //postData.Add("password", login.Password);
            postData.Add("agencyId", AgentID);
            postData.Add("agencyUserId", login.Username);
            postData.Add("password", login.Password);

            CookieCollection cookieCollection;
            var cookies = new CookieContainer();
            var response = SendHttpRequest(postData, LoginUrl, cookies, out cookieCollection);

            Condition.Ensures(cookieCollection)
                .IsNotNull()
                .IsNotEmpty()
                .Evaluate(x => x[JSessionId] != null);

            var jSessionId = cookieCollection[JSessionId];
            return jSessionId;
        }

        /// <summary>
        /// Creates and returns a certificate number for the indicated client.
        /// </summary>
        /// <returns>The certificate number generated.</returns>
        public DataResponse<string> GetCertificateNumber(string firstName, string lastName, DateTime certificateDate, string judicialDistrict)
        {
            DataResponse<string> dr = new DataResponse<string>();

            var IssuerIdentity = ConfigurationManager.AppSettings["IssuerIdentity"];

            if (string.IsNullOrEmpty(IssuerIdentity))
            {
                dr.AddErrorMessage("Issuer Identity is not configured.");
            }

            var CertificatePrefix = ConfigurationManager.AppSettings["CertificatePrefix"];

            if (string.IsNullOrEmpty(CertificatePrefix))
            {
                dr.AddErrorMessage("Certificate Prefix is not configured.");
            }

            try
            {
                var jSessionId = LoginToWebsite();
                var postData = new Dictionary<string, string>()
                    {
                        { "debtorFirstName", firstName },
                        { "debtorLastName", lastName },
                        { "dateCourseCompleted", certificateDate.ToString("MM/dd/yyyy") },
                        { "timeCourseCompleted", certificateDate.ToString("hh:mm") },
                        { "ampmCourseCompleted", certificateDate.ToString("tt") },
                        { "timeZone", "US/Eastern" },
                        { "judicialDistrictIdentity", judicialDistrict },
                        { "serviceMethodIdentity", "2" },
                        { "debtManagementPlanPrepared", "N" },
                        { "issuerIdentity", IssuerIdentity },
                        { "electronicallySigned", "Y" },
                        { "certificateTypeId", "1" }
                    };
                var cookies = new CookieContainer();
                cookies.Add(jSessionId);

                CookieCollection cookieCollection;
                var response = SendHttpRequest(postData, CreateCertificateUrl, cookies, out cookieCollection);

                int start = response.IndexOf(CertificatePrefix);
                int i = start;
                int end;
                while (response[i] != '<')
                {
                    i++;
                }
                end = i;
                string certificateNumber = response.Substring(start, end - start);

                if (string.IsNullOrEmpty(certificateNumber))
                {
                    dr.AddErrorMessage("Certificate Number is empty.");
                }
                else
                {
                    dr.Payload = certificateNumber;
                    dr.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                dr.AddException(ex);
            }

            return dr;
        }

        /// <summary>
        /// Sends an HttpRequest and returns the response string.
        /// </summary>
        /// <param name="data">Key-value pairs of data needed for a form post.</param>
        /// <param name="url">The url to which to post the form data.</param>
        /// <param name="cookies">The cookies to send along with the request.</param>
        /// <param name="cookieCollection">The cookies returned by the response.</param>
        /// <returns>The string of Html returned in the response.</returns>
        private static string SendHttpRequest(Dictionary<string, string> data, string url, CookieContainer cookies, out CookieCollection cookieCollection)
        {
            var postData = new StringBuilder();
            foreach (var key in data.Keys)
            {
                postData.AppendFormat("{0}={1}&", key, data[key]);
            }

            var webRequest = WebRequest.Create(url) as HttpWebRequest;
            webRequest.KeepAlive = false;
            webRequest.Method = "POST";
            webRequest.Accept = "*/*";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.CookieContainer = cookies;

            using (var requestStream = webRequest.GetRequestStream())
            {
                var memoryStream = new MemoryStream();
                try
                {
                    using (var writer = new BinaryWriter(memoryStream))
                    {
                        memoryStream = null;

                        writer.Write(Encoding.GetEncoding(1252).GetBytes(postData.ToString()));
                        ((MemoryStream)writer.BaseStream).WriteTo(requestStream);
                    }
                }
                finally
                {
                    if (memoryStream != null)
                        memoryStream.Dispose();
                }
            }

            return GetWebResponse(webRequest, out cookieCollection);
        }

        /// <summary>
        /// Retrieves the response and converts it to a string.
        /// </summary>
        /// <param name="webRequest">The request sent.</param>
        /// <param name="cookies">The cookies returned by the response.</param>
        /// <returns>The Html source of the response.</returns>
        private static string GetWebResponse(HttpWebRequest webRequest, out CookieCollection cookies)
        {
            const int ReadSize = 256;

            using (var webResponse = webRequest.GetResponse() as HttpWebResponse)
            {
                if (webResponse.StatusCode != HttpStatusCode.OK) throw new InvalidOperationException("Unable to retrieve web response successfully.");

                cookies = webResponse.Cookies;
                var responseStream = webResponse.GetResponseStream();
                try
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseStream = null;

                        var read = new Char[ReadSize];
                        int count = reader.Read(read, 0, ReadSize);

                        var builder = new StringBuilder();
                        while (count > 0)
                        {
                            builder.Append(read, 0, count);
                            count = reader.Read(read, 0, ReadSize);
                        }

                        return builder.ToString();
                    }
                }
                finally
                {
                    if (responseStream != null)
                        responseStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Creates and returns a certificate number for the indicated client.
        /// </summary>
        /// <returns>The certificate number generated.</returns>
        public DataResponse<string> GetEducationCertificateNumber(string firstName, string lastName, DateTime certificateDate, string judicialDistrict)
        {
            DataResponse<string> dr = new DataResponse<string>();

            var IssuerIdentity = ConfigurationManager.AppSettings["IssuerIdentity"];

            if (string.IsNullOrEmpty(IssuerIdentity))
            {
                dr.AddErrorMessage("Issuer Identity is not configured.");
            }

            var CertificatePrefix = ConfigurationManager.AppSettings["CertificatePrefix"];

            if (string.IsNullOrEmpty(CertificatePrefix))
            {
                dr.AddErrorMessage("Certificate Prefix is not configured.");
            }

            try
            {
                var jSessionId = LoginToWebsite();
                var postData = new Dictionary<string, string>()
                    {
                        { "debtorFirstName", firstName },
                        { "debtorLastName", lastName },
                        { "dateCourseCompleted", certificateDate.ToString("MM/dd/yyyy") },
                        { "timeCourseCompleted", certificateDate.ToString("hh:mm") },
                        { "ampmCourseCompleted", certificateDate.ToString("tt") },
                        { "timeZone", "US/Eastern" },
                        { "judicialDistrictIdentity", judicialDistrict },
                        { "serviceMethodIdentity", "2" },
                        { "debtManagementPlanPrepared", "N" },
                        { "issuerIdentity", IssuerIdentity },
                        { "electronicallySigned", "Y" },
                        { "certificateTypeId", "2" }
                    };
                var cookies = new CookieContainer();
                cookies.Add(jSessionId);

                CookieCollection cookieCollection;
                var response = SendHttpRequest(postData, CreateCertificateUrl, cookies, out cookieCollection);

                int start = response.IndexOf(CertificatePrefix);
                int i = start;
                int end;
                while (response[i] != '<')
                {
                    i++;
                }
                end = i;
                string certificateNumber = response.Substring(start, end - start);

                if (string.IsNullOrEmpty(certificateNumber))
                {
                    dr.AddErrorMessage("Certificate Number is empty.");
                }
                else
                {
                    dr.Payload = certificateNumber;
                    dr.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                dr.AddException(ex);
            }

            return dr;
        }
    }
}
