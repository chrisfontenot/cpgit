﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Response;

namespace Cccs.Credability.Certificates.PreFiling
{
    public interface ICounselingCertificateDownload
    {
        DataResponse<string> GetCertificateNumber(string firstName, string lastName, DateTime certificateDate, string judicialDistrict);

        DataResponse<string> GetEducationCertificateNumber(string firstName, string lastName, DateTime certificateDate, string judicialDistrict);
    }
}
