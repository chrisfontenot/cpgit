﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cccs.Credability.Certificates.PreFiling.Queues
{
    [DataContract]
    public class CounselorActionPlanQueueMessage
    {
        [DataMember]
        public long PreFilingActionPlanId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string JudicialDistrict { get; set; }

        [DataMember]
        public string ChatCounselorName { get; set; }

        [DataMember]
        public string ConfirmDateTime { get; set; }
    }
}
