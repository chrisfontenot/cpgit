﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Email;
using System.Messaging;

namespace Cccs.Credability.Certificates.PreFiling.Queues
{
    public class AttorneyCertificateQueue : IDisposable
    {
        private ILoggingService logger;
        private QueueSettings settings;
        private TimeSpan queueTimeout;
        private MessageQueue queue;
        
        public bool IsQueueValid { get; private set; }

        public AttorneyCertificateQueue(ILoggingService logger)
        {
            var configuration = EmailQueueConfiguration.GetConfiguration();
            this.settings = configuration.PreFiling.AttorneyCertificateSettings;
            this.queueTimeout = TimeSpan.FromSeconds(settings.TimeoutSeconds);
            this.logger = logger;

            queue = ValidateQueue();
        }

        private MessageQueue ValidateQueue()
        {
            IsQueueValid = true;

            if (!MessageQueue.Exists(settings.QueuePath))
            {
                logger.Info(() => "Creating Attorney Certificate Queue.");

                try
                {
                    var newQueue = MessageQueue.Create(settings.QueuePath, true);
                    newQueue.Dispose();
                }
                catch (MessageQueueException ex)
                {
                    logger.Error(() => String.Format("Unable to create message queue '{0}'", settings.QueuePath), ex);
                    IsQueueValid = false;
                }
            }

            var queue = null as MessageQueue;
            var tempQueue = new MessageQueue(settings.QueuePath);
            try
            {
                if (!tempQueue.CanRead)
                {
                    logger.Warn(() => String.Format("Unable to read message queue '{0}'", settings.QueuePath));
                    IsQueueValid = false;
                }

                if (!tempQueue.CanWrite)
                {
                    logger.Warn(() => String.Format("Unable to write to message queue '{0}'", settings.QueuePath));
                    IsQueueValid = false;
                }

                queue = tempQueue;
                tempQueue = null;
            }
            finally
            {
                if (tempQueue != null)
                    tempQueue.Close();
            }

            return queue;
        }

        public AttorneyCertificateQueueMessage GetNextCertificate()
        {
            using (var transaction = new MessageQueueTransaction())
            {
                try
                {
                    transaction.Begin();

                    var message = queue.Receive(queueTimeout, transaction);
                    var attorneyMessage = ParseMessage(message);

                    transaction.Commit();

                    return attorneyMessage;
                }
                catch (MessageQueueException ex)
                {
                    transaction.Abort();

                    if (ex.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout && ex.MessageQueueErrorCode != MessageQueueErrorCode.MessageNotFound)
                    {
                        logger.Error("Unable to process Attorney Certificate message.", ex);
                    }
                }
            }

            return null;
        }

        private AttorneyCertificateQueueMessage ParseMessage(Message message)
        {
            message.Formatter = new XmlMessageFormatter(new Type[] { typeof(AttorneyCertificateQueueMessage) });
            var clientMessage = message.Body as AttorneyCertificateQueueMessage;
            if (clientMessage == null)
            {
                var errorMessage = String.Format("Invalid message in queue.\nLabel: {0}\nBody: {1}", message.Label, message.Body);
                new InvalidOperationException(errorMessage).LogAndThrow(logger);
            }

            return clientMessage;
        }

        public void SendAttorneyCertificate(AttorneyCertificateQueueMessage clientMessage)
        {
            using (var transaction = new MessageQueueTransaction())
            {
                transaction.Begin();

                try
                {
                    using (var message = new Message(clientMessage))
                    {
                        message.Label = "Attorney Certificate";

                        queue.Send(message, transaction);

                        transaction.Commit();
                    }
                }
                catch(Exception ex)
                {
                    transaction.Abort();
                    logger.Error("Unable to send Attorney Certificate message.", ex);
                }
            }
        }

        #region IDisposable
        ~AttorneyCertificateQueue()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (queue != null)
                {
                    queue.Dispose();
                    queue = null;
                }
            }
        }
        #endregion
    }
}
