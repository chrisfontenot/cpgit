﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Credability.Certificates.PreFiling.Queues
{
    [DataContract]
    public class ClientActionPlanQueueMessage
    {
        [DataMember]
        public long PreFilingActionPlanId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string JudicialDistrict { get; set; }

        [DataMember]
        public string ChatCounselorName { get; set; }

        [DataMember]
        public string ConfirmDateTime { get; set; }
    }
}
