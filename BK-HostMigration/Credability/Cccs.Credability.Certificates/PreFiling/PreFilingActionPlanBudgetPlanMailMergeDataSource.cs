﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling.Reasons;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingActionPlanBudgetPlanMailMergeDataSource : IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto DisplayData;
        private IList<BudgetPlanItem> BudgetPlanList;
        private readonly CultureInfo Culture;
        private int RecordIndex;

        public PreFilingActionPlanBudgetPlanMailMergeDataSource(CultureInfo culture, PreFilingSnapshotDto displayData)
        {
            DisplayData = displayData;
            Culture = culture;
            RecordIndex = -1;
            BudgetPlanList = new List<BudgetPlanItem>();

            // Add Net Income Line.
            var incomeItem = new BudgetPlanItem
            {
                Name = Resources.ExpenseCodes.ResourceManager.GetString("NetIncome"),
                Amount = DisplayData.IncomeAfterTaxes.ToString("C"),
                Percent = String.Empty,
                RecommendedAmount = DisplayData.IncomeAfterTaxes.ToString("C"),
                RecommendedPercent = String.Empty,
                IsCategory = true,
            };
            BudgetPlanList.Add(incomeItem);
            BudgetPlanList.Add(CreateBlankLineItem());

            foreach (var category in DisplayData.ExpenseCategories)
            {
                var categoryItem = new BudgetPlanItem
                {
                    Name = category.DisplayText,
                    Amount = category.SessionAmount.ToString("C"),
                    Percent = category.PercentOfTotal.ToString("0%"),
                    RecommendedAmount = category.RecommendedTotal.ToString("C"),
                    RecommendedPercent = category.RecommendedPercentage.ToString("0%"),
                    IsCategory = true,
                };
                BudgetPlanList.Add(categoryItem);

                var codeList = (from c in DisplayData.ExpenseCodes
                                where c.ExpenseCategoryId == category.ExpenseCategoryId
                                orderby c.DisplayOrder ascending
                                select c).ToList();
                foreach (var code in codeList)
                {
                    var codeItem = new BudgetPlanItem
                    {
                        Name = code.DisplayText,
                        Amount = code.SessionAmount.ToString("C"),
                        Percent = String.Empty,
                        RecommendedAmount = String.Empty,
                        RecommendedPercent = String.Empty,
                        IsCategory = false,
                    };
                    BudgetPlanList.Add(codeItem);
                }
                BudgetPlanList.Add(CreateBlankLineItem());
            }

            // Totaling Line
            var expensesTotal = DisplayData.ExpenseCategories.Sum(c => c.SessionAmount);
            var surplusDeficit = DisplayData.IncomeAfterTaxes - expensesTotal;
            var totalItem = new BudgetPlanItem
            {
                Name = Resources.ExpenseCodes.ResourceManager.GetString("BudgetSurplusDeficit"),
                Amount = surplusDeficit.ToString("C"),
                Percent = String.Empty,
                RecommendedAmount = Decimal.Zero.ToString("C"),
                RecommendedPercent = String.Empty,
                IsCategory = true,
            };
            BudgetPlanList.Add(totalItem);

            Reason1.Culture = Culture;
        }

        private static BudgetPlanItem CreateBlankLineItem()
        {
            var blankItem = new BudgetPlanItem
            {
                Name = String.Empty,
                Amount = String.Empty,
                Percent = String.Empty,
                RecommendedAmount = String.Empty,
                RecommendedPercent = String.Empty,
            };

            return blankItem;
        }

        public string TableName
        {
            get { return "BudgetPlan"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "BudgetPlanName":
                    fieldValue = BudgetPlanList[RecordIndex].Name;
                    return true;

                case "BudgetPlanTotal":
                    fieldValue = BudgetPlanList[RecordIndex].Amount;
                    return true;

                case "BudgetPlanPercent":
                    fieldValue = BudgetPlanList[RecordIndex].Percent;
                    return true;

                case "BudgetPlanRecommendedTotal":
                    fieldValue = BudgetPlanList[RecordIndex].RecommendedAmount;
                    return true;

                case "BudgetPlanRecommendedPercent":
                    fieldValue = BudgetPlanList[RecordIndex].RecommendedPercent;
                    return true;

                case "BudgetPlanIsCategory":
                    fieldValue = BudgetPlanList[RecordIndex].IsCategory;
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == BudgetPlanList.Count - 1); }
        }

        private class BudgetPlanItem
        {
            public string Name { get; set; }
            public string Amount { get; set; }
            public string Percent { get; set; }
            public string RecommendedAmount { get; set; }
            public string RecommendedPercent { get; set; }
            public bool IsCategory { get; set; }
        }
    }
}
