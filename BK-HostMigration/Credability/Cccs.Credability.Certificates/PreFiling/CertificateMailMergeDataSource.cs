﻿using System;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.PreFiling.Reasons;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class CertificateMailMergeDataSource : IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto Data;
        private int RecordIndex;

        public CertificateMailMergeDataSource(PreFilingSnapshotDto data)
        {
            Data = data;
            RecordIndex = -1;
        }

        public string TableName
        {
            get { return "Certificate"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "CertificateNumber":
                    fieldValue = Data.CertificateNumber;
                    return true;

                case "CertDate"://June 21, 2013
                    fieldValue = Data.CourseCompleted.ToString("MMMM dd, yyyy");
                    return true;

                case "CertTime":
                    fieldValue = Data.CourseCompleted.ToString("hh:mm");
                    return true;

                case "CertZone":
                    fieldValue = String.Format("{0:tt} EST", Data.CourseCompleted);
                    return true;

                case "Name":
                    fieldValue = FormatName(Data);
                    return true;

                case "AddressBlock":
                    fieldValue = String.Format("{0}\n{1}{2}, {3} {4}",
                        Data.StreetLine1.Trim(),
                        String.IsNullOrEmpty(Data.StreetLine1) ? String.Empty : String.Format("{0}\n", Data.StreetLine2.Trim()),
                        Data.City.Trim(),
                        Data.State,
                        Data.PostalCode);
                    return true;

                case "JudicialDistrict":
                    fieldValue = Data.JudicialDistrict;
                    return true;

                case "CounselorNameSignature":
                    fieldValue = "/s/Eric Dina";
                    return true;

                case "CounselorName":
                    fieldValue = "Eric Dina";
                    return true;

                case "Title":
                    fieldValue = "Customer Service";
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        private string FormatName(PreFilingSnapshotDto data)
        {
            return String.Format("{0}{1}{2}",
                data.FirstName,
                String.IsNullOrEmpty(data.MiddleName) ? String.Empty : String.Format(" {0}", data.MiddleName),
                String.Format(" {0}", data.LastName));
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == 0); }
        }

        private string DetermineReason(string reasonCode)
        {
            switch (reasonCode)
            {
                case "01":
                    return Reason1.OverObligation;

                case "02":
                    return Reason1.ReducedIncome;

                case "03":
                    return Reason1.MedicalAccident;

                case "04":
                    return Reason1.DeathOfAFamilyMember;

                case "05":
                    return Reason1.DivorceSeparation;

                case "06":
                    return Reason1.Unemployment;

                case "08":
                    return Reason1.Disability;

                case "09":
                    return Reason1.Gambling;

                case "12":
                    return Reason1.ChildSupportAlimony;

                case "14":
                    return Reason1.StudentLoan;

                default:
                    return Reason1.Other;
            }
        }
    }
}
