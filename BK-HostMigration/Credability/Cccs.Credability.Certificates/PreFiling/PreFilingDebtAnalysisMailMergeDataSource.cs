﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using System.Globalization;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingDebtAnalysisMailMergeDataSource: IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto DisplayData;
        private int RecordIndex;
        public PreFilingDebtAnalysisMailMergeDataSource(PreFilingSnapshotDto displayData)
        {
            DisplayData = displayData;
            RecordIndex = -1;
        }

        public string TableName
        {
            get { return "DebtAnalysis"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "DmpAmountSavings":
                    fieldValue = ComputeDmpAmountSavings();
                    return true;

                case "DmpTimeSavings":
                    fieldValue = ComputeDmpTimeSavings();
                    return true;

                case "DmpTotalPayments":
                    fieldValue = ComputeDmpTotalPayments();
                    return true;

                case "DmpCompleteDate":
                    fieldValue = ComputeDmpCompletionDate();
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        private string ComputeDmpAmountSavings()
        {
            var totalCreditorCost = DisplayData.Creditors.Sum(c => c.MonthlyPayment * c.MonthsToRepay);
            var dmpTotalCost = DisplayData.DmpMonthlyPayment * DisplayData.DmpMonthsToRepay;

            return (totalCreditorCost - dmpTotalCost).ToString("C");
        }

        private string ComputeDmpTimeSavings()
        {
            var maximumCreditorTime = DisplayData.Creditors.Max(c => c.MonthsToRepay);

            return (maximumCreditorTime - DisplayData.DmpMonthsToRepay).ToString();
        }

        private string ComputeDmpTotalPayments()
        {
            return (DisplayData.DmpMonthlyPayment * DisplayData.DmpMonthsToRepay).ToString("C");
        }

        private string ComputeDmpCompletionDate()
        {
            return DisplayData.CourseCompleted.AddMonths(DisplayData.DmpMonthsToRepay).ToShortDateString();
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return (RecordIndex == 0); }
        }
    }
}
