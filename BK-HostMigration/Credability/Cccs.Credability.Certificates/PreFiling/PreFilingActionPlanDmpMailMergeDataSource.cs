﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Words.Reporting;
using Cccs.Credability.Certificates.DataTransferObjects;
using System.Globalization;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class PreFilingActionPlanDmpMailMergeDataSource : IMailMergeDataSource
    {
        private readonly PreFilingSnapshotDto DisplayData;
        private readonly CultureInfo Culture;
        private int RecordIndex;
        public PreFilingActionPlanDmpMailMergeDataSource(CultureInfo culture, PreFilingSnapshotDto displayData)
        {
            DisplayData = displayData;
            Culture = culture;
            RecordIndex = -1;
        }

        public string TableName
        {
            get { return "Dmp"; }
        }

        public bool GetValue(string fieldName, out object fieldValue)
        {
            switch (fieldName)
            {
                case "DmpName":
                    fieldValue = Resources.ExpenseCodes.ResourceManager.GetString("DebtManagementPlan", Culture);
                    return true;

                case "DmpInterest":
                    fieldValue = DisplayData.DmpInterestRate.ToString("0%");
                    return true;

                case "DmpBalance":
                    fieldValue = DisplayData.DmpBalance.ToString("C");
                    return true;

                case "DmpPayment":
                    fieldValue = DisplayData.DmpMonthlyPayment.ToString("C");
                    return true;

                case "DmpMonthsToRepay":
                    fieldValue = DisplayData.DmpMonthsToRepay.ToString();
                    return true;

                default:
                    fieldValue = null;
                    return false;
            }
        }

        public bool MoveNext()
        {
            var more = !IsEof;
            if (more)
                RecordIndex++;

            return more;
        }

        public IMailMergeDataSource GetChildDataSource(string tableName)
        {
            return null;
        }

        private bool IsEof
        {
            get { return RecordIndex == 0; }
        }

        private class CreditorItem
        {
            public string Name { get; set; }
            public string Interest { get; set; }
            public string Balance { get; set; }
            public string MonthlyPayment { get; set; }
            public string MonthsToRepay { get; set; }
        }
    }
}
