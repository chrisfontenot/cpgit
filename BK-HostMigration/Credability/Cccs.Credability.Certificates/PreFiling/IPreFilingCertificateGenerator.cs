﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates.PreFiling
{
    public interface IPreFilingCertificateGenerator
    {
        MemoryStream CreateActionPlan(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData);
        MemoryStream CreateCertificate(PreFilingSnapshotDto data);
        MemoryStream CreateCombinedCertificate(PreFilingSnapshotDto primaryData, PreFilingSnapshotDto secondaryData);
    }
}
