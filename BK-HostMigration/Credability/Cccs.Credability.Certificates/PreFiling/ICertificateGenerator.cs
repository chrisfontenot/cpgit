﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cccs.Credability.Certificates.PreFiling
{
    public interface ICertificateGenerator
    {
        MemoryStream CreateActionPlan(string languageCode, Identity.Dal.CounselingCertificate primaryData, Identity.Dal.CounselingCertificate secondaryData);
        MemoryStream CreateCertificate(Identity.Dal.CounselingCertificate data);
        MemoryStream CreateCombinedCertificate(Identity.Dal.CounselingCertificate primaryData, Identity.Dal.CounselingCertificate secondaryData);
    }
}
