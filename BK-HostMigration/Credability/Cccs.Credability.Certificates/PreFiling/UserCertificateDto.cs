﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.PreFiling
{
    public class UserCertificateDto
    {
        public int UserId { get; set; }
        public int ClientNumber { get; set; }
        public long PrimaryUserDetailId { get; set; }
        public long SecondaryUserDetailId { get; set; }
        public string PrimaryCertificateNumber { get; set; }
        public string SecondaryCertificateNumber { get; set; }
    }
}
