﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface IDmpRepository
    {
        DmpConfigurationDto GetConfiguration();
    }
}
