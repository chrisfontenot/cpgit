﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Cccs.Credability.Certificates
{
    [Serializable]
    public class ContactDetailNotFoundException : Exception
    {
        public long ClientNumber { get; set; }

        public ContactDetailNotFoundException(long clientNumber) : base()
        {
            ClientNumber = clientNumber;
        }

        public ContactDetailNotFoundException(long clientNumber, string message)
            : base(message)
        {
            ClientNumber = clientNumber;
        }

        public ContactDetailNotFoundException(long clientNumber, string message, Exception innerException)
            : base(message, innerException)
        {
            ClientNumber = clientNumber;
        }

        protected ContactDetailNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            ClientNumber = info.GetInt64("ClientNumber");
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("ClientNumber", ClientNumber);
        }
    }
}
