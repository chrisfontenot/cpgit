﻿using Cccs.Credability.Certificates.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates
{
    public interface IPredischargeCertificateRepository
    {
        bool AddPredischangeCertificate(PredischargeCertificateDto certificateData);
        bool UpdatePredischangeCertificate(PredischargeCertificateDto certificateData);
        PredischargeCertificateDto GetPredischargeCertificate(long accountID);
    }
}
