﻿using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface IUserRepository
    {
        PreFilingUserDetailDto GetDetail(long userDetailId);
        PreFilingUserDetailDto GetDetail(long userId, bool isPrimary);
    }
}
