﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
    [Serializable]
    public class PreFilingSnapshotException : Exception
    {
        public PreFilingSnapshotException() : base()
        {
        }

        public PreFilingSnapshotException(string message)
            : base(message)
        {
        }

        public PreFilingSnapshotException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected PreFilingSnapshotException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
