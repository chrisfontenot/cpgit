﻿using Cccs.Identity;
using Cccs.Identity.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates
{
    public interface IEducationCertificateService
    {
        /// <summary>
        /// Assigns a certificate number to the specified snapshot.
        /// </summary>
        /// <param name="preFilingSnapshotId">The snapshot to update.</param>
        void AssignCertificate(Identity.UserDetail userDetail, long accountID, DateTime completionDate);

        /// <summary>
        /// Determines if the specified user has a snapshot within the allowed time frame.
        /// </summary>
        /// <param name="userDetailId">The user detail to search.</param>
        /// <returns>True if a snapshot is found, false otherwise.</returns>
        bool HasCurrentSnapshot(long userDetailId);

        /// <summary>
        /// Determines if the specified user has a certificate number downloaded from EOUST for the most recent snapshot.
        /// </summary>
        /// <param name="userDetailId">The user detail to search.</param>
        /// <returns>True if a certificate number is assigned to the most recent snapshot, false otherwise.</returns>
        bool HasDownloadedCertificate(long userDetailId);

        EducationCertificate GetUserDetail(Identity.UserDetail userDetail, long accountID, string bankruptcyCaseNumber);
    }
}
