﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public class DmpCalculatorService : IDmpCalculatorService
    {
        private readonly IDmpRepository dmpRepository;
        public DmpCalculatorService(IDmpRepository dmpRepository)
        {
            this.dmpRepository = dmpRepository;
        }

        /// <summary>
        /// Calculates the average payment and months to repay for a list of creditors.
        /// </summary>
        /// <param name="creditors">The list of creditors to repay.</param>
        /// <returns></returns>
        public DmpAverageDto Average(IEnumerable<DmpCreditorDto> creditors)
        {
            var configuration = dmpRepository.GetConfiguration();
            var totalPrincipal = creditors.Sum(c => c.Balance);
            
            var monthlyInterestRate = configuration.AverageInterestRate / 12m;
            var principalPayoff = Math.Round(totalPrincipal * configuration.AveragePaymentPercent, 2, MidpointRounding.AwayFromZero);
            var firstMonthInterest = Math.Round(totalPrincipal * monthlyInterestRate, 2, MidpointRounding.AwayFromZero);

            var monthlyPayment = principalPayoff + firstMonthInterest;
            if (monthlyPayment < configuration.AverageMinimumPayment)
                monthlyPayment = configuration.AverageMinimumPayment;

            var dto = new DmpAverageDto
            {
                DmpBalance = totalPrincipal,
                DmpInterestRate = configuration.AverageInterestRate,
                DmpMonthlyPayment = monthlyPayment,
                DmpMonthsToRepay = 0,
            };

            var currentPrincipal = totalPrincipal;
            while (currentPrincipal > 0)
            {
                var currentInterest = Math.Round(currentPrincipal * monthlyInterestRate, 2, MidpointRounding.AwayFromZero);
                currentPrincipal = currentPrincipal - monthlyPayment + currentInterest;
                dto.DmpMonthsToRepay++;
            }

            return dto;
        }
    }
}
