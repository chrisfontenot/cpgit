﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
    [Serializable]
    public class PreFilingAccountNotFoundException : Exception
    {
        public PreFilingAccountNotFoundException() : base()
        {
        }

        public PreFilingAccountNotFoundException(string message)
            : base(message)
        {
        }

        public PreFilingAccountNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected PreFilingAccountNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
