﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class ExpenseCodeDto
    {
        public int ExpenseCodeId { get; set; }
        public int ExpenseCategoryId { get; set; }
        public string Name { get; set; }
        public string LookupCode { get; set; }
        public byte DisplayOrder { get; set; }
    }
}
