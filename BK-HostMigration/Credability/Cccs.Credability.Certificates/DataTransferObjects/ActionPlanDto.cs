﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class ActionPlanDto
    {
        public long PreFilingActionPlanId { get; set; }
        public List<CertificateDto> Certificates { get; set; }
    }
}
