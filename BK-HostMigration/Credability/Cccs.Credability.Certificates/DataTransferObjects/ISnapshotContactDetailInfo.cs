﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface ISnapshotContactDetailInfo
    {
        string Reason { get; set; }
        string JudicialDistrict { get; set; }
        DateTime CourseCompleted { get; set; }
        short CreditScore { get; set; }
        decimal IncomeAfterTaxes { get; set; }
        decimal LivingExpenses { get; set; }
        decimal TotalAssets { get; set; }
        decimal TotalLiabilities { get; set; }
        decimal DebtPayment { get; set; }
        decimal CashAtMonthEnd { get; set; }
        decimal NetWorth { get; set; }
        decimal DmpInterestRate { get; set; }
        decimal DmpBalance { get; set; }
        decimal DmpMonthlyPayment { get; set; }
        short DmpMonthsToRepay { get; set; }
    }
}
