﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PredischargeCertificateDto
    {
        public long AccountId { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime CompletionDate { get; set; }
    }
}
