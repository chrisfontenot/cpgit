﻿using System;
using System.Collections.Generic;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingSnapshotDto : ISnapshotUserInfo, ISnapshotContactDetailInfo
    {
        public long PreFilingSnapshotId { get; set; }
        public long PreFilingActionPlanId { get; set; }
        public long UserDetailId { get; set; }
        public int ClientNumber { get; set; }
        public string CertificateNumber { get; set; } 
        public string LanguageCode { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string JudicialDistrict { get; set; }
        public DateTime CourseCompleted { get; set; }
        public DateTime CertificateGenerated { get; set; }
        public string CertificateVersionType { get; set; }
        public string SsnLast4 { get; set; }
        public short CreditScore { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public decimal IncomeAfterTaxes { get; set; }
        public decimal LivingExpenses { get; set; }
        public decimal DebtPayment { get; set; }
        public decimal CashAtMonthEnd { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal TotalLiabilities { get; set; }
        public decimal NetWorth { get; set; }
        public string Reason { get; set; }
        public string CounselorName { get; set; }
        public string CounselorTitle { get; set; }
        public byte ArchiveStatus { get; set; }
        public decimal DmpInterestRate { get; set; }
        public decimal DmpBalance { get; set; }
        public decimal DmpMonthlyPayment { get; set; }
        public short DmpMonthsToRepay { get; set; }

        public IList<PreFilingSnapshotCreditorDto> Creditors { get; set; }
        public IList<PreFilingSnapshotExpenseCategoryDto> ExpenseCategories { get; set; }
        public IList<PreFilingSnapshotExpenseCodeDto> ExpenseCodes { get; set; }

        public PreFilingSnapshotDto()
        {
            Creditors = new List<PreFilingSnapshotCreditorDto>();
            ExpenseCategories = new List<PreFilingSnapshotExpenseCategoryDto>();
            ExpenseCodes = new List<PreFilingSnapshotExpenseCodeDto>();
        }
    }
}
