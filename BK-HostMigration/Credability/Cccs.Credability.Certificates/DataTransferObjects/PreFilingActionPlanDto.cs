﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingActionPlanDto
    {
        public long PreFilingActionPlanId { get; set; }
        public IList<PreFilingSnapshotDto> Snapshots { get; set; }
    }
}
