﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface IContactDetailInfo
    {
        int ClientNumber { get; set; }
        string Reason { get; set; }
        DateTime CourseCompleted { get; set; }
        decimal IncomeAfterTaxes { get; set; }
        decimal LivingExpenses { get; set; }
        decimal TotalAssets { get; set; }
        decimal TotalLiabilities { get; set; }
    }
}
