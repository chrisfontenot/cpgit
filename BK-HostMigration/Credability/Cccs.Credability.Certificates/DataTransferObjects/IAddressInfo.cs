﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface IAddressInfo
    {
        long UserId { get; set; }
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
    }
}
