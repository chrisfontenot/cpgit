﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class CertificateDto
    {
        public long PreFilingSnapshotId { get; set; }
        public string CertificateNumber { get; set; }
		public string CourseCompleted { get; set; }
		public string CertificateGenerated { get; set; }
		public string CertificateVersionType { get; set; }
    }
}
