﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface IAccountInfo
    {
        int ClientNumber { get; set; }
        string CounselorName { get; set; }
    }
}
