﻿using System;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingAccountDto
    {
        public long AccountId { get; set; }
        public long UserId { get; set; }
		public string AccountTypeCode { get; set; }
		public string HostId { get; set; }
		public int InternetId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public bool IsActive { get; set; }
        public string CounselorName { get; set; }
    }
}
