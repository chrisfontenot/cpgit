﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface IUserInfo
    {
        long UserDetailId { get; set; }
        long UserId { get; set; }
        bool IsPrimary { get; set; }
        string LanguageCode { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string Ssn { get; set; }
        string Email { get; set; }
    }
}
