﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class DmpAverageDto
    {
        public decimal DmpInterestRate { get; set; }
        public decimal DmpBalance { get; set; }
        public decimal DmpMonthlyPayment { get; set; }
        public short DmpMonthsToRepay { get; set; }
    }
}
