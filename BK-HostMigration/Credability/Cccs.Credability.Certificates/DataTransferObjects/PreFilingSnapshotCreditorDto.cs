﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingSnapshotCreditorDto
    {
        public string Name { get; set; }
        public byte DisplayOrder { get; set; }
        public decimal InterestRate { get; set; }
        public decimal Balance { get; set; }
        public decimal MonthlyPayment { get; set; }
        public short MonthsToRepay { get; set; }
    }
}
