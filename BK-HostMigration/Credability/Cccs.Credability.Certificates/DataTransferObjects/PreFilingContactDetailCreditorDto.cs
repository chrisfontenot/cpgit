﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingContactDetailCreditorDto
    {
        public int CreditorId { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public decimal Payment { get; set; }
        public decimal InterestRate { get; set; }
    }
}
