﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class DmpCreditorDto
    {
        public decimal Balance { get; set; }
        public decimal InterestRate { get; set; }
    }
}
