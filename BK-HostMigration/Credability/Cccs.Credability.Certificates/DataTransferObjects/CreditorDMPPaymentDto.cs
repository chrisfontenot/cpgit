﻿// -----------------------------------------------------------------------
// <copyright file="CreditorDMPPayment.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CreditorDmpPayment
    {
        public decimal MonthlyPayment { get; set; }
        public decimal Balance { get; set; }
        public decimal MonthlyInterestRate { get; set; }

        public short MonthsToRepay { get; set; }
        public decimal additionalMonthlyPayment { get; set; }

    }

}
