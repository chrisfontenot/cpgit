﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class DmpConfigurationDto
    {
        public decimal AverageInterestRate { get; set; }
        public decimal AveragePaymentPercent { get; set; }
        public decimal AverageMinimumPayment { get; set; }
        public decimal AverageDmpFeePercent { get; set; }
    }
}
