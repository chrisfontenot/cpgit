﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingSnapshotExpenseCodeDto
    {
        public int ExpenseCategoryId { get; set; }
        public int ExpenseCodeId { get; set; }
        public string ExpenseCode { get; set; }
        public string DisplayText { get; set; }
        public byte DisplayOrder { get; set; }
        public decimal SessionAmount { get; set; }
    }
}
