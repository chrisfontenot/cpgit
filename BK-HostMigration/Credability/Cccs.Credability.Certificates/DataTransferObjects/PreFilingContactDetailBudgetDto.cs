﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingContactDetailBudgetDto
    {
        public string LookupCode { get; set; }
        public decimal Amount { get; set; }
    }
}
