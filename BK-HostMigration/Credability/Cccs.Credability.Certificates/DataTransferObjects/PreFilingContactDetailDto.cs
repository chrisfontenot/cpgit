﻿using System;
using System.Collections.Generic;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingContactDetailDto : IContactDetailInfo
    {
        public int ClientNumber { get; set; }
        public string Reason { get; set; }
        public DateTime CourseCompleted { get; set; }
        public decimal IncomeAfterTaxes { get; set; }
        public decimal LivingExpenses { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal TotalLiabilities { get; set; }

        public IList<PreFilingContactDetailCreditorDto> Creditors { get; set; }
        public IList<PreFilingContactDetailBudgetDto> Budget { get; set; }

        public PreFilingContactDetailDto()
        {
            Creditors = new List<PreFilingContactDetailCreditorDto>();
            Budget = new List<PreFilingContactDetailBudgetDto>();
        }

    }
}
