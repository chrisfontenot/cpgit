﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class ExpenseCategoryDto
    {
        public int ExpenseCategoryId { get; set; }
        public string Name { get; set; }
        public string LookupCode { get; set; }
        public decimal RecommendedPercentage { get; set; }
        public byte DisplayOrder { get; set; }
        public IList<ExpenseCodeDto> ExpenseCodes { get; set; }

        public ExpenseCategoryDto()
        {
            this.ExpenseCodes = new List<ExpenseCodeDto>();
        }
    }
}
