﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingSnapshotExpenseCategoryDto
    {
        public int ExpenseCategoryId { get; set; }
        public string ExpenseCategory { get; set; }
        public string DisplayText { get; set; }
        public byte DisplayOrder { get; set; }
        public decimal SessionAmount { get; set; }
        public decimal PercentOfTotal { get; set; }
        public decimal RecommendedPercentage { get; set; }
        public decimal RecommendedTotal { get; set; }
    }
}
