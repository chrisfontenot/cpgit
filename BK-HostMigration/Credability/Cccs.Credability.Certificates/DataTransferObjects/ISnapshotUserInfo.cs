﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public interface ISnapshotUserInfo
    {
        long UserDetailId { get; set; }
        string LanguageCode { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        string SsnLast4 { get; set; }
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PostalCode { get; set; }
        int ClientNumber { get; set; }
        string CounselorName { get; set; }
        string CounselorTitle { get; set; }
        DateTime CertificateGenerated { get; set; }
        string CertificateVersionType { get; set; }
        byte ArchiveStatus { get; set; }
    }
}
