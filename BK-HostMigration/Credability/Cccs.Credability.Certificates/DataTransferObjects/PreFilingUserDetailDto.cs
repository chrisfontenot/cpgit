﻿
namespace Cccs.Credability.Certificates.DataTransferObjects
{
    public class PreFilingUserDetailDto : IUserInfo, IAddressInfo, IAccountInfo
    {
        public long UserDetailId { get; set; }
        public long UserId { get; set; }
        public string LanguageCode { get; set; }
        public int ClientNumber { get; set; }
        public bool IsPrimary { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Ssn { get; set; }
        public string CounselorName { get; set; }
        public string Email { get; set; }
    }
}
