﻿// -----------------------------------------------------------------------
// <copyright file="DMPCalculatorService.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Cccs.Credability.Certificates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Cccs.Credability.Certificates.DataTransferObjects;



    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DMPCalculationService : IDmpCalculationService
    {
        public DMPCalculationService()
        {

        }
        public void CalculateMonthsToRepay(List<CreditorDmpPayment> dmpList)
        {


            var morePayments = (from d in dmpList where d.Balance > 0 select d).Any();
            while (morePayments)
            {
                // Pay off any creditors that have a balance less than the payment amount and store remaining payment amount
                // for redistribution amongst remaining creditors.
                var remainingPayments = 0m;


                foreach (var item in dmpList)
                    item.additionalMonthlyPayment = 0;

                var payOffCreditors = (from d in dmpList where (d.Balance < (d.MonthlyPayment)) select d).ToList();

                foreach (var item in payOffCreditors)
                {
                    if (item.Balance > 0) item.MonthsToRepay++;
                    remainingPayments += (item.MonthlyPayment + item.additionalMonthlyPayment) - item.Balance;
                    item.Balance -= item.Balance;
                }

                // Divide remaining payments across remaining creditors. Give the smallest balance any remainder.
                var paymentDistributors = (from d in dmpList where (d.Balance > 0) orderby d.Balance select d).ToList();
                if (remainingPayments > 0 && paymentDistributors.Count > 0)
                {
                    var additionalPaymentAmount = 0m;
                    var previousAdditionalPaymentAmount = -1m;

                    while (additionalPaymentAmount != previousAdditionalPaymentAmount && paymentDistributors.Count > 0)
                    {
                        previousAdditionalPaymentAmount = additionalPaymentAmount;
                        additionalPaymentAmount = (remainingPayments / paymentDistributors.Count).CeilingByDecimals(2);
                        paymentDistributors = (from d in dmpList
                                               where (additionalPaymentAmount + d.MonthlyPayment) <= d.Balance
                                               select d).ToList();
                    }

                    foreach (var item in paymentDistributors)
                    {
                        if (additionalPaymentAmount >= remainingPayments)
                        {

                            item.additionalMonthlyPayment += remainingPayments;
                            remainingPayments -= remainingPayments;
                        }
                        else
                        {

                            item.additionalMonthlyPayment += additionalPaymentAmount;
                            remainingPayments -= additionalPaymentAmount;
                        }
                    }
                }

                // Make this month's payments.
                var creditorPayments = (from d in dmpList where d.Balance > 0 select d).ToList();
                foreach (var item in creditorPayments)
                {
                    var interestAmount = (item.Balance * item.MonthlyInterestRate).CeilingByDecimals(2);
                    item.Balance += interestAmount;
                    item.Balance -= item.MonthlyPayment + item.additionalMonthlyPayment;
                    item.MonthsToRepay++;


                }

                morePayments = (from d in dmpList where d.Balance > 0 select d).Any();
            }
        }
    }
}
