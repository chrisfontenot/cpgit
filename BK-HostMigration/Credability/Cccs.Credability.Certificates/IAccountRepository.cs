﻿using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface IAccountRepository
    {
        /// <summary>
        /// Retrieves the PreFiling account information for a user.
        /// </summary>
        /// <param name="userDetailId">The user to retrieve.</param>
        /// <returns>A data transfer object containing the user's account information.</returns>
        PreFilingAccountDto GetPreFilingAccountByUserDetail(long userDetailId);
    }
}
