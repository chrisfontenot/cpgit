﻿using System;
using System.Collections.Generic;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface ISnapshotRepository
    {
        /// <summary>
        /// Creates a new Action Plan in the data store.
        /// </summary>
        /// <param name="primarySnapshot">Information about the primary snapshot.</param>
        /// <param name="secondarySnapshot">Information about the secondary snapshot if it exists.</param>
        /// <returns>The information about the new action plan.</returns>
        PreFilingActionPlanDto CreateActionPlan(PreFilingSnapshotDto primarySnapshot, PreFilingSnapshotDto secondarySnapshot);

        /// <summary>
        /// Retrieves the snapshot associated with the supplied certificate number.
        /// </summary>
        /// <param name="certificateNumber">The certificate number to search.</param>
        /// <returns>The snapshot for the indicated certificate number if found, null otherwise.</returns>
        PreFilingSnapshotDto GetByCertificateNumber(string certificateNumber);

        /// <summary>
        /// Retrieves the joint snapshot associated with the supplied certificate number.
        /// </summary>
        /// <param name="certificateNumber">The certificate number to search.</param>
        /// <returns>The snapshot for the joint certificate number if found, null otherwise.</returns>
        PreFilingSnapshotDto GetAssociatedByCertificateNumber(string certificateNumber);

        /// <summary>
        /// Retrieves the most recent snapshot date created for the specified user.
        /// </summary>
        /// <param name="userDetailId">The Id of the user to search.</param>
        /// <returns>The date if found, DateTime.MinValue otherwise.</returns>
        DateTime GetMostRecentDate(long userDetailId);

        /// <summary>
        /// Retrieves the most recent snapshot for the specified user.
        /// </summary>
        /// <param name="userDetailId">The Id of the user to search.</param>
        /// <returns>The snapshot found, null otherwise.</returns>
        PreFilingSnapshotDto GetMostRecent(long userDetailId);

        /// <summary>
        /// Retrieves the specific action plan.
        /// </summary>
        /// <param name="actionPlanId">The Id of the action plan to search.</param>
        /// <returns>The action plan found, null otherwise.</returns>
        PreFilingActionPlanDto GetActionPlan(long actionPlanId);

        /// <summary>
        /// Retrieves the specific snapshot.
        /// </summary>
        /// <param name="preFilingSnapshotId">The Id of the snapshot to search.</param>
        /// <returns>The snapshot found, null otherwise.</returns>
        PreFilingSnapshotDto GetSnapshot(long preFilingSnapshotId);

        /// <summary>
        /// Retrieve the most recent snapshot for the indicated action plan.
        /// </summary>
        /// <param name="actionPlanId">The Id of the action plan to search.</param>
        /// <returns>The most recent snapshot found, null otherwise.</returns>
        PreFilingSnapshotDto GetSnapshotByActionPlan(long actionPlanId);
        
        /// <summary>
        /// Attempts to retrieve a single user that has an unarchived certificate.
        /// </summary>
        /// <param name="userId">The Id of the user found.</param>
        /// <returns>True if a user is found, false otherwise.</returns>
        bool TryGetUnarchivedCertificateUser(out long userId);

        /// <summary>
        /// Attempts to retrieve a snapshot with a missing certificate number.
        /// </summary>
        /// <param name="preFilingSnapshotId">The Id of the snapshot found.</param>
        /// <returns>True if a snapshot is found, false otherwise.</returns>
        bool TryGetSnapshotWithMissingCertificate(out long preFilingSnapshotId);

        /// <summary>
        /// Retrieves the list of certificates associated with the specified user.
        /// </summary>
        /// <param name="userId">The Id of the user to search.</param>
        /// <returns>The list of certificates found. An empty list of no certificates are found.</returns>
        IList<CertificateDto> GetCertificateListByUser(long userId);

        /// <summary>
        /// Retrieves the list of action plans associated with the specified user.
        /// </summary>
        /// <param name="userId">The Id of the user to search.</param>
        /// <returns>The list of action plans found. An empty list of no action plans are found.</returns>
        IList<ActionPlanDto> GetActionPlanListByUser(long userId);

        /// <summary>
        /// Inserts a new snapshot into the data store.
        /// </summary>
        /// <param name="snapshot">The data of the snapshot to store.</param>
        /// <returns>The Id of the snapshot record created.</returns>
        long Insert(PreFilingSnapshotDto snapshot);

        /// <summary>
        /// Updates the certificate number and archive status of the specified snapshot.
        /// </summary>
        /// <param name="dto">The record to update containing the updated values.</param>
        void Update(PreFilingSnapshotDto dto);
    }
}
