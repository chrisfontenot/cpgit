﻿using System;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Identity.Dal;
using System.Collections.Generic;
using System.Transactions;
using CuttingEdge.Conditions;
using System.Diagnostics.CodeAnalysis;

namespace Cccs.Credability.Certificates.Repositories
{
    public class SnapshotRepository : LinqToSqlRepositoryBase, ISnapshotRepository
    {
        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        public SnapshotRepository(ILoggingService logger, IMappingEngine mapper)
            : base(logger)
        {
            Condition.Requires(logger, "logger").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();

            this.logger = logger;
            this.mapper = mapper;
        }

        public PreFilingActionPlanDto CreateActionPlan(PreFilingSnapshotDto primarySnapshot, PreFilingSnapshotDto secondarySnapshot)
        {
            Condition.Requires(primarySnapshot, "primarySnapshot").IsNotNull();

            using (var context = NewIdentityDataContext())
            {
                var actionPlan = new PreFilingActionPlan()
                {
                    PrimarySnapshotId = primarySnapshot.PreFilingSnapshotId,
                    SecondarySnapshotId = secondarySnapshot.ReturnIfNotNull(null, s => new Int64?(s.PreFilingSnapshotId)),
                };
                context.PreFilingActionPlans.InsertOnSubmit(actionPlan);
                context.SubmitChanges();

                var dto = mapper.Map<PreFilingActionPlan, PreFilingActionPlanDto>(actionPlan);
                return dto;
            }
        }

        public DateTime GetMostRecentDate(long userDetailId)
        {
            using (var context = NewIdentityDataContext())
            {
                var snapshotDate = (from s in context.PreFilingSnapshots
                                    where s.UserDetailId == userDetailId
                                    orderby s.PreFilingSnapshotId descending
                                    select s.CourseCompleted).FirstOrDefault();

                return snapshotDate;
            }
        }

        public PreFilingActionPlanDto GetActionPlan(long actionPlanId)
        {
            using (var context = NewIdentityDataContext())
            {
                var actionPlan = (from a in context.PreFilingActionPlans
                                  where a.PreFilingActionPlanId == actionPlanId
                                  select a).FirstOrDefault();
                if (actionPlan == null)
                    return null;

                var dto = mapper.Map<PreFilingActionPlan, PreFilingActionPlanDto>(actionPlan);
                return dto;
            }
        }

        public PreFilingSnapshotDto GetSnapshot(long preFilingSnapshotId)
        {
            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from s in context.PreFilingSnapshots
                                where s.PreFilingSnapshotId == preFilingSnapshotId
                                select s).FirstOrDefault();
                if (snapshot == null)
                    return null;

                var dto = mapper.Map<PreFilingSnapshot, PreFilingSnapshotDto>(snapshot);
                return dto;
            }
        }

        public PreFilingSnapshotDto GetSnapshotByActionPlan(long actionPlanId)
        {
            using (var context = NewIdentityDataContext())
            {
                var actionPlan = (from a in context.PreFilingActionPlans
                                  where a.PreFilingActionPlanId == actionPlanId
                                  select a).FirstOrDefault();
                if (actionPlan == null)
                    return null;

                var snapshot = actionPlan.PreFilingSnapshot;
                if (actionPlan.PreFilingSnapshot1.ConditionIfNotNull(s => s.CertificateGenerated > snapshot.CertificateGenerated))
                    snapshot = actionPlan.PreFilingSnapshot1;

                var dto = mapper.Map<PreFilingSnapshot, PreFilingSnapshotDto>(snapshot);
                return dto;
            }
        }

        public PreFilingSnapshotDto GetByCertificateNumber(string certificateNumber)
        {
            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from s in context.PreFilingSnapshots
                                where s.CertificateNumber == certificateNumber
                                orderby s.CourseCompleted descending
                                select s).FirstOrDefault();
                if (snapshot == null)
                    return null;

                var dto = mapper.Map<PreFilingSnapshot, PreFilingSnapshotDto>(snapshot);
                return dto;
            }
        }

        public PreFilingSnapshotDto GetAssociatedByCertificateNumber(string certificateNumber)
        {
            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from s in context.PreFilingSnapshots
                                join u in context.UserDetails on s.UserDetailId equals u.UserDetailId
                                join u2 in context.UserDetails on u.UserId equals u2.UserId
                                join s2 in context.PreFilingSnapshots on u2.UserDetailId equals s2.UserDetailId
                                where s.CertificateNumber == certificateNumber && s2.UserDetailId != s.UserDetailId
                                orderby s2.PreFilingSnapshotId descending
                                select s2).FirstOrDefault();
                if (snapshot == null)
                    return null;

                var dto = mapper.Map<PreFilingSnapshot, PreFilingSnapshotDto>(snapshot);
                return dto;
            }
        }

        public PreFilingSnapshotDto GetMostRecent(long userDetailId)
        {
            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from s in context.PreFilingSnapshots
                                where s.UserDetailId == userDetailId
                                orderby s.PreFilingSnapshotId descending
                                select s).FirstOrDefault();
                if (snapshot == null)
                    return null;

                var dto = mapper.Map<PreFilingSnapshot, PreFilingSnapshotDto>(snapshot);
                return dto;
            }
        }

        public IList<CertificateDto> GetCertificateListByUser(long userId)
        {
            using (var context = NewIdentityDataContext())
            {
                var list = (from s in context.PreFilingSnapshots
                            join d in context.UserDetails on s.UserDetailId equals d.UserDetailId
                            where d.UserId == userId
                            select s).ToList();

                var dtoList = Mapper.Map<IList<PreFilingSnapshot>, IList<CertificateDto>>(list);
                return dtoList;
            }
        }

        public IList<ActionPlanDto> GetActionPlanListByUser(long userId)
        {
            using (var context = NewIdentityDataContext())
            {
                var list = (from s in context.PreFilingSnapshots
                            join d in context.UserDetails on s.UserDetailId equals d.UserDetailId
                            where d.UserId == userId && d.IsPrimary
                            select s).ToList();

                var actionPlanList = new List<PreFilingActionPlan>();
                foreach (var item in list)
                {
                    actionPlanList.AddRange(item.PreFilingActionPlans.ToList());
                    actionPlanList.AddRange(item.PreFilingActionPlans1.ToList());
                }

                var dtoList = Mapper.Map<IList<PreFilingActionPlan>, IList<ActionPlanDto>>(actionPlanList.Distinct().ToList());
                return dtoList;
            }
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The scope variable is disposed in all code paths.")]
        public bool TryGetUnarchivedCertificateUser(out long userId)
        {
            using (var scope = new TransactionScope())
            using (var context = NewIdentityDataContext())
            {
                var user = (from s in context.PreFilingSnapshots
                              join d in context.UserDetails on s.UserDetailId equals d.UserDetailId
                              where (s.CertificateNumber != null && s.CertificateNumber != String.Empty) && (s.ArchiveStatus == null || s.ArchiveStatus == 0)
                              select d.User).FirstOrDefault();

                if (user != null)
                {
                    foreach (var detail in user.UserDetails)
                    {
                        foreach (var snapshot in detail.PreFilingSnapshots.Where(s => !String.IsNullOrEmpty(s.CertificateNumber) && (s.ArchiveStatus == null || s.ArchiveStatus == 0)))
                        {
                            snapshot.ArchiveStatus = 2;
                        }
                    }
                    context.SubmitChanges();
                    userId = user.UserId;

                    scope.Complete();
                    return true;
                }

            }

            userId = 0;
            return false;
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The scope variable is disposed in all code paths.")]
        public bool TryGetSnapshotWithMissingCertificate(out long preFilingSnapshotId)
        {
            using (var scope = new TransactionScope())
            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from d in context.UserDetails
                                join s in context.PreFilingSnapshots on d.UserDetailId equals s.UserDetailId
                                where (s.CertificateNumber == null || s.CertificateNumber == String.Empty) && (s.ArchiveStatus == null || s.ArchiveStatus == 0)
                                select s).FirstOrDefault();

                if (snapshot != null)
                {
                    snapshot.ArchiveStatus = 2;
                    context.SubmitChanges();
                    preFilingSnapshotId = snapshot.PreFilingSnapshotId;

                    scope.Complete();
                    return true;
        }
            }

            preFilingSnapshotId = 0;
            return false;
        }

        public long Insert(PreFilingSnapshotDto dto)
        {
            Condition.Requires(dto, "dto").IsNotNull();

            using (var context = NewIdentityDataContext())
            {
                var snapshot = mapper.Map<PreFilingSnapshotDto, PreFilingSnapshot>(dto);
                var creditors = mapper.Map<IList<PreFilingSnapshotCreditorDto>, IList<PreFilingSnapshotCreditor>>(dto.Creditors);
                var categories = mapper.Map<IList<PreFilingSnapshotExpenseCategoryDto>, IList<PreFilingSnapshotExpenseCategory>>(dto.ExpenseCategories);
                var codes = mapper.Map<IList<PreFilingSnapshotExpenseCodeDto>, IList<PreFilingSnapshotExpenseCode>>(dto.ExpenseCodes);

                snapshot.PreFilingSnapshotCreditors.AddRange(creditors);
                snapshot.PreFilingSnapshotExpenseCategories.AddRange(categories);
                snapshot.PreFilingSnapshotExpenseCodes.AddRange(codes);

                context.PreFilingSnapshots.InsertOnSubmit(snapshot);

                using (var scope = new TransactionScope())
                {
                    context.SubmitChanges();

                    scope.Complete();
                }

                return snapshot.PreFilingSnapshotId;
            }
        }

        public void Update(PreFilingSnapshotDto dto)
        {
            Condition.Requires(dto, "dto").IsNotNull();

            using (var context = NewIdentityDataContext())
            {
                var snapshot = (from s in context.PreFilingSnapshots
                                where s.PreFilingSnapshotId == dto.PreFilingSnapshotId
                                select s).First();

                // The only thing we should be doing is assigning a certificate number in the event EOUST was down
                // or updating the archive status.
                snapshot.CertificateNumber = dto.CertificateNumber;
                snapshot.ArchiveStatus = dto.ArchiveStatus;

                using (var scope = new TransactionScope())
                {
                    context.SubmitChanges();

                    scope.Complete();
                }
            }
        }
    }
}
