﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Certificates.DataTransferObjects;
using AutoMapper;
using Cccs.Identity.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public class PredischargeCertificateRepository : LinqToSqlRepositoryBase, IPredischargeCertificateRepository
    {
        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        public PredischargeCertificateRepository(ILoggingService logger, IMappingEngine mapper)
            : base(logger)
        {
            this.logger = logger;
            this.mapper = mapper;
        }

        public bool AddPredischangeCertificate(PredischargeCertificateDto certificateData)
        {
            bool result = false;

            //var certificate = mapper.Map<PredischangeCertificateDto, PreDischangeCertificate>(certificateData);
            if (certificateData != null)
            {
                var certificate = new PreDischargeCertificate()
                {
                    AccountId            = certificateData.AccountId,
                    CompletionDate       = certificateData.CompletionDate,
                    CertificateNumber   = certificateData.CertificateNumber
                };

                using (var context = NewIdentityDataContext())
                {
                    context.PreDischargeCertificates.InsertOnSubmit(certificate);
                    context.SubmitChanges();

                    result = true;
                    //var account = (from d in context.UserDetails
                    //               join a in context.Accounts on d.UserId equals a.UserId
                    //               where d.UserDetailId == userDetailId && a.AccountTypeCode == "BR.CAM"
                    //               select a).FirstOrDefault();
                    //if (account == null)
                    //{
                    //    logger.Warn(() => String.Format("Unable to find account for user detail record '{0}'", userDetailId));
                    //    return null;
                    //}
                }
            }

            return result;
        }

        public PredischargeCertificateDto GetPredischargeCertificate(long accountID)
        {
            PredischargeCertificateDto certificateDataDto = null;

            using (var context = NewIdentityDataContext())
            {
                var certificateData = context.PreDischargeCertificates.Where(c => c.AccountId == accountID).FirstOrDefault();

                if(certificateData != null)
                {
                    certificateDataDto = new PredischargeCertificateDto()
                    {
                        AccountId = certificateData.AccountId,
                        CompletionDate = certificateData.CompletionDate,
                        CertificateNumber = certificateData.CertificateNumber,
                    };
                }
            }

            return certificateDataDto;
        }

        public bool UpdatePredischangeCertificate(PredischargeCertificateDto certificateData)
        {
            return false;
        }
    }
}
