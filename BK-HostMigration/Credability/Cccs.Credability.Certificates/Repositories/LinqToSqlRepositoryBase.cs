﻿using System;
using System.Configuration;
using Cccs.Credability.Dal;
using Cccs.Identity.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public abstract class LinqToSqlRepositoryBase
    {
        private const string IdentityConnectionStringName = "Cccs.Identity.Dal.Properties.Settings.Cccs_IdentityConnectionString";
        private const string CccsConnectionStringName = "Cccs.Credability.Dal.Properties.Settings.cccsDevConnectionString";

        private readonly ILoggingService logger;
        private readonly ConnectionStringSettings identityConnectionString;
        private readonly ConnectionStringSettings cccsConnectionString;
        public LinqToSqlRepositoryBase(ILoggingService logger)
        {
            this.logger = logger;

            this.identityConnectionString = ConfigurationManager.ConnectionStrings[IdentityConnectionStringName];
            if (this.identityConnectionString == null)
                new ConfigurationErrorsException(String.Format("Unable to retrieve connection string setting with name '{0}' from configuration.", IdentityConnectionStringName)).LogAndThrow(logger);

            this.cccsConnectionString = ConfigurationManager.ConnectionStrings[CccsConnectionStringName];
            if (this.cccsConnectionString == null)
                new ConfigurationErrorsException(String.Format("Unable to retrieve connection string setting with name '{0}' from configuration.", CccsConnectionStringName)).LogAndThrow(logger);
        }

        protected IdentityDataContext NewIdentityDataContext()
        {
            return new IdentityDataContext(identityConnectionString.ConnectionString);
        }

        protected CredabilityDataContext NewCccsDataContext()
        {
            return new CredabilityDataContext(cccsConnectionString.ConnectionString);
        }
    }
}
