﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public class ContactDetailRepository : LinqToSqlRepositoryBase, IContactDetailRepository
    {
        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        public ContactDetailRepository(ILoggingService logger, IMappingEngine mapper)
            : base(logger)
        {
            this.logger = logger;
            this.mapper = mapper;
        }

        public PreFilingContactDetailDto Get(int clientNumber)
        {
            using (var context = NewCccsDataContext())
            {
                var detail = (from d in context.contactdetails
                              where d.client_number == clientNumber && d.completed == 1 && d.confirm_datetime != null
                              select d).FirstOrDefault();
                if (detail == null)
                {
                    logger.Warn(() => String.Format("Unable to retrieve contact detail record '{0}'", clientNumber));
                    return null;
                }

                var creditors = (from d in context.creditordetails
                                 where d.client_number == clientNumber
                                 select d).ToList();
                if (creditors.Count == 0)
                {
                    logger.Warn(() => String.Format("No creditors retrieved for contact detail record '{0}'", clientNumber));
                }

                var expenses = DetailExpenses(detail);

                var dto = new PreFilingContactDetailDto();
                mapper.Map<contactdetail, IContactDetailInfo>(detail, dto);
                mapper.Map<IList<creditordetail>, IList<PreFilingContactDetailCreditorDto>>(creditors, dto.Creditors);
                mapper.Map<IEnumerable<ExpenseCodeMap>, IEnumerable<PreFilingContactDetailBudgetDto>>(expenses, dto.Budget);

                return dto;
            }
        }

        private IEnumerable<ExpenseCodeMap> DetailExpenses(contactdetail detail)
        {
            if (detail.rent_mort.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "RentOrMortgage", Amount = detail.rent_mort.Value };

            if (detail.equity_loan_tax_ins.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "SecondMortgage", Amount = detail.equity_loan_tax_ins.Value };

            if (detail.home_maintenance.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "HomeMaintenance", Amount = detail.home_maintenance.Value };

            if (detail.utilities.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Utilities", Amount = detail.utilities.Value };

            if (detail.vehicle_payments.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "AutoPayments", Amount = detail.vehicle_payments.Value };

            if (detail.car_insurance.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "AutoInsurance", Amount = detail.car_insurance.Value };

            if (detail.car_maintenance.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "AutoMaintenance", Amount = detail.car_maintenance.Value };

            if (detail.public_transportation.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "PublicTransportation", Amount = detail.public_transportation.Value };

            if (detail.food_away.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "FoodOut", Amount = detail.food_away.Value };

            if (detail.groceries.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "FoodIn", Amount = detail.groceries.Value };

            if (detail.telephone.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Telephone", Amount = detail.telephone.Value };

            if (detail.insurance.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Insurance", Amount = detail.insurance.Value };

            if (detail.medical_prescription.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Medical", Amount = detail.medical_prescription.Value };

            if (detail.child_support.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "ChildSupportAlimony", Amount = detail.child_support.Value };

            if (detail.child_elder_care.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "ChildElderCare", Amount = detail.child_elder_care.Value };

            if (detail.education.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Education", Amount = detail.education.Value };

            if (detail.CardPayments.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "CreditCard", Amount = Convert.ToDecimal(detail.CardPayments.Value) };

            if (detail.OtherLoans.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "OtherInstallmentLoans", Amount = Convert.ToDecimal(detail.OtherLoans.Value) };

            if (detail.contributions.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Charity", Amount = detail.contributions.Value };

            if (detail.clothing.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Clothing", Amount = detail.clothing.Value };

            if (detail.laundry.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Laundry", Amount = detail.laundry.Value };

            if (detail.personal_exp.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Personal", Amount = detail.personal_exp.Value };

            if (detail.beauty_barber.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Beauty", Amount = detail.beauty_barber.Value };

            if (detail.recreation.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Recreation", Amount = detail.recreation.Value };

            if (detail.club_dues.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "ClubUnionDues", Amount = detail.club_dues.Value };

            if (detail.gifts.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Gifts", Amount = detail.gifts.Value };

            if (detail.misc_exp.HasValue)
                yield return new ExpenseCodeMap { LookupCode = "Miscellaneous", Amount = detail.misc_exp.Value };
        }
    }
}
