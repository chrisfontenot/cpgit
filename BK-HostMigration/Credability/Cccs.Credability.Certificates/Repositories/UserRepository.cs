﻿using System;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Identity.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public class UserRepository : LinqToSqlRepositoryBase, IUserRepository
    {
        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        public UserRepository(ILoggingService logger, IMappingEngine mapper) : base(logger)
        {
            this.logger = logger;
            this.mapper = mapper;
        }

        public PreFilingUserDetailDto GetDetail(long userId, bool isPrimary)
        {
            using (var context = NewIdentityDataContext())
            {
                var detail = (from d in context.UserDetails
                              where d.UserId == userId && d.IsPrimary == isPrimary
                              select d).FirstOrDefault();

                if (detail == null)
                    return null;

                return MapDetailToDto(detail.UserDetailId, context, detail);
            }
        }

        public PreFilingUserDetailDto GetDetail(long userDetailId)
        {
            //logger.Debug(() => "GetDetail");
            //logger.DebugParameter("userDetailId", userDetailId);

            using (var context = NewIdentityDataContext())
            {
                var detail = (from d in context.UserDetails
                              where d.UserDetailId == userDetailId
                              select d).FirstOrDefault();
                if (detail == null)
                {
                    logger.Warn(() => String.Format("Unable to retrieve user detail record: {0}", userDetailId));
                    return null;
                }

                return MapDetailToDto(userDetailId, context, detail);
            }
        }

        private PreFilingUserDetailDto MapDetailToDto(long userDetailId, IdentityDataContext context, UserDetail detail)
        {
            var languageCode = detail.User.LanguageCode;

            var address = detail.User.Addresses.FirstOrDefault();
            if (address == null)
            {
                logger.Warn(() => String.Format("Unable to retrieve address information for user detail record: {0}", userDetailId));
                return null;
            }

            var account = detail.User.Accounts.Where(a => a.AccountTypeCode == "BR.CAM").FirstOrDefault();
            if (account == null)
            {
                logger.Warn(() => String.Format("Unable to retrieve account information for user detail record: {0}", userDetailId));
                return null;
            }

            var dto = new PreFilingUserDetailDto();
            mapper.Map<UserDetail, IUserInfo>(detail, dto);
            mapper.Map<Address, IAddressInfo>(address, dto);
            mapper.Map<Account, IAccountInfo>(account, dto);

            context.OpenEncryptionKey();
            dto.Ssn = context.DecryptText(detail.Ssn);
            dto.LanguageCode = languageCode;

            return dto;
        }
    }
}
