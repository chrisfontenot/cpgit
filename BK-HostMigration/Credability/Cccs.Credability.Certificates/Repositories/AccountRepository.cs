﻿using System;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Identity.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public class AccountRepository : LinqToSqlRepositoryBase, IAccountRepository
    {
        private readonly ILoggingService logger;
        private readonly IMappingEngine mapper;
        public AccountRepository(ILoggingService logger, IMappingEngine mapper)
            : base(logger)
        {
            this.logger = logger;
            this.mapper = mapper;
        }

        public PreFilingAccountDto GetPreFilingAccountByUserDetail(long userDetailId)
        {
            logger.Debug(() => "GetPreFilingAccountByUserDetail");
            logger.DebugParameter("userDetailId", userDetailId);

            using (var context = NewIdentityDataContext())
            {
                var account = (from d in context.UserDetails
                               join a in context.Accounts on d.UserId equals a.UserId
                               where d.UserDetailId == userDetailId && a.AccountTypeCode == "BR.CAM"
                               select a).FirstOrDefault();
                if (account == null)
                {
                    logger.Warn(() => String.Format("Unable to find account for user detail record '{0}'", userDetailId));
                    return null;
                }

                var dto = mapper.Map<Account, PreFilingAccountDto>(account);
                return dto;
            }
        }
    }
}
