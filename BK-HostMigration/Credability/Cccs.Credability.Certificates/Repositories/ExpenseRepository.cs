﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Identity.Dal;

namespace Cccs.Credability.Certificates.Repositories
{
    public class ExpenseRepository : LinqToSqlRepositoryBase, IExpenseRepository
    {
        public ExpenseRepository(ILoggingService logger)
            : base(logger)
        {
        }

        public IList<ExpenseCategoryDto> GetExpenseCategories()
        {
            using (var context = NewIdentityDataContext())
            {
                var categoryList = (from c in context.ExpenseCategories
                                    select c).ToList();

                var dto = Mapper.Map<IList<ExpenseCategory>, IList<ExpenseCategoryDto>>(categoryList);
                return dto;
            }
        }
    }
}
