﻿using System.Collections.Generic;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates
{
    public interface IExpenseRepository
    {
        IList<ExpenseCategoryDto> GetExpenseCategories();
    }
}
