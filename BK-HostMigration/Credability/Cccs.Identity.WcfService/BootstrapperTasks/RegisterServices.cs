﻿using AutoMapper;
using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.Email;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Debtplus;
using Cccs.Host;
using StructureMap.Configuration.DSL;

namespace Cccs.Identity.WcfService.BootstrapperTasks
{
    public class RegisterServices : Registry
    {
        public RegisterServices()
        {
            For<ILoggingService>().Use<Log4NetLoggingService>();
            For<IMappingEngine>().Use(Mapper.Engine);
            For<IPreFilingCertificateService>().Use<PreFilingCertificateService>();
            For<ICounselingCertificateDownload>().Use<CounselingCertificateDownload>();
            For<IDebtplus>().Use(Global.Debtplus);
            //For<IHost>().Use(Global.Host);
            For<PreFilingPoll>().Use<PreFilingPoll>();
        }
   }
}