﻿using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.Repositories;
using StructureMap.Configuration.DSL;

namespace Cccs.Identity.WcfService.BootstrapperTasks
{
    public class RegisterCertificateRepositories : Registry
    {
        public RegisterCertificateRepositories()
        {
            For<IAccountRepository>().Use<AccountRepository>();
            For<IContactDetailRepository>().Use<ContactDetailRepository>();
            For<ISnapshotRepository>().Use<SnapshotRepository>();
            For<IUserRepository>().Use<UserRepository>();
            For<IExpenseRepository>().Use<ExpenseRepository>();
        }
    }
}