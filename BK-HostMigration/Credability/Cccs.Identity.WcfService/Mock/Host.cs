﻿using System;
using Cccs.Host;
using Cccs.Host.Dto;

namespace Cccs.Identity.WcfService.Mock
{
	public class Host : IHost
	{
		public string GetCounselorName(string counselorId)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> GetEscrow(int firm_id)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> ReqCredRpt(long user_id, int client_number)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> RequestCreditReport(int client_number, string client_type, string ref_code, string primary_birth_city, string secondary_birth_city, string primary_first_name, string primary_last_name, string primary_ssn, DateTime? primary_birth_date, string street_line1, string city, string state, string zip, string secondary_first_name, string secondary_last_name, string secondary_ssn, DateTime? secondary_birth_date)
		{
			throw new NotImplementedException();
		}

		public Result GetCredRpt(int client_number)
		{
			throw new NotImplementedException();
		}

		public Result<CreditScoreDto> GetCredScore(int client_number)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> SaveTrans(int client_number, int van_tran_ref, string invoice_no, float char_amt)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> GetNextSeqNo(int client_number)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> HaveCredRpt(int client_number)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> UpdateOCS(int client_number, int stat)
		{
			throw new NotImplementedException();
		}

		public Result<System.Xml.XmlDocument> LoadDmp(int client_number)
		{
			throw new NotImplementedException();
		}
        
		public Result<string> GetUvDts()
		{
			throw new NotImplementedException();
		}

		public Result Bch_HomeSaver_SaveData(string need_help, string help_type, string contact_time, string loan_number, string first_name, string last_name, string address_line1, string address_line2, string city, string state, string zip, string phone, string email)
		{
			throw new NotImplementedException();
		}

		public JudicialDistrict GetJudicialDistrict(string zipCode)
		{
			throw new NotImplementedException();
		}

		public string GetEoustJudicialDistrictByName(string districtName)
		{
			throw new NotImplementedException();
		}

		public EoustLogin GetEoustLogin()
		{
			throw new NotImplementedException();
		}

		public string GetFirmEmail(string FirmID)
		{
			throw new NotImplementedException();
		}

		public MimLoginClientResult Mim_LoginClient(string reg_num)
		{
			throw new NotImplementedException();
		}

		public MimCreateLoginValidateDataResult Mim_CreateLogin_ValidateData(string ssn)
		{
			throw new NotImplementedException();
		}

		public MimDisclosureLoadDataResult Mim_Disclosure_LoadData(string ClNo)
		{
			throw new NotImplementedException();
		}

		public Result Mim_Disclosure_SaveTransaction(string ClNo, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state, string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no, string van_trans_ref, string char_amt)
		{
			throw new NotImplementedException();
		}

		public MimForm7LoadDataResult Mim_Form7_LoadData(string ClNo, string regnum)
		{
			throw new NotImplementedException();
		}

		public Result Mim_Form7_SaveData(string ClNo, string contact_lastname, string contact_firstname, string contact_initial, string contact_ssn, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string contact_marital, string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity, string cemail, string dayphone, string nitephone, string attyname, string attyemail, string firm_id, string escrow, string secondary_flag, string char_type, string casenumber, string bankruptcy_type, string income_range, string number_in_house, string languageCode)
		{
			throw new NotImplementedException();
		}

		public Result Mim_Form7_SaveWriteOff(string ClNo, string regnum, string char_amt)
		{
			throw new NotImplementedException();
		}

		public MimForm7MainLineResult Mim_Form7_MainLine(string contact_state)
		{
			throw new NotImplementedException();
		}

		public MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum)
		{
			throw new NotImplementedException();
		}

		public Result MimCompleteCourse(string ClNo, string prescore, string postscore, string TestDTS)
		{
			throw new NotImplementedException();
		}

		public MimQuicCalcLoadDataResult Mim_QuicCalc_LoadData(string ClNo, string regnum)
		{
			throw new NotImplementedException();
		}

		public Result Mim_QuicCalc_SaveData(string ClNo, string contact_state, string monthly_gross_income, string size_of_household)
		{
			throw new NotImplementedException();
		}

		public Result Mim_WaiverWaiver_SaveUv(string ClNo)
		{
			throw new NotImplementedException();
		}

		public MimWaverAppLoadDataResult Mim_WaverApp_LoadData(string ClNo, string regnum)
		{
			throw new NotImplementedException();
		}

		public Result Mim_WaverApp_SaveData(string clid, string contact_firstname, string contact_lastname, string contact_initial, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string real_email, string contact_telephone)
		{
			throw new NotImplementedException();
		}

		public string Mim_NewClsID_Get(string reg_num)
		{
			throw new NotImplementedException();
		}

		public string Mim_HostID_ReturningUser_Get(string reg_num)
		{
			throw new NotImplementedException();
		}

		public Result MimSaveRegisterAttorney(string ClNo, string CaseNo, string Ssn, string FirmId, string AttnyName, string AttnyEmail, string UserName)
		{
			throw new NotImplementedException();
		}

		public Result MimSavePreTest(string ClNo, int PreTest)
		{
			throw new NotImplementedException();
		}

		public Result MimDisclosureSaveMtcn(string ClNo, string mtcn)
		{
			throw new NotImplementedException();
		}

		public Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number)
		{
			throw new NotImplementedException();
		}

		public int GetMimSequenceNumber(string ClNo)
		{
			throw new NotImplementedException();
		}

		public double MimDisclosureGetChargeAmount(string ClNo)
		{
			throw new NotImplementedException();
		}

		public Result MimSaveXRef(string PriSsn, string SecSsn)
		{
			throw new NotImplementedException();
		}

		public CaAccountTransactionsResult CaTrans(string ClientNo, string TransOpt)
		{
			throw new NotImplementedException();
		}

		public CaCredResult CaCreds(string ClientNo)
		{
			throw new NotImplementedException();
		}

		public CaMailPreferenceResult CaPerfGet(string ClientNo)
		{
			throw new NotImplementedException();
		}

		public Result CaPerfSet(string ClientNo, string LetterPref, string StatmentPref)
		{
			throw new NotImplementedException();
		}

		public Result CaInfoSet(string ClientNo, string HomePhone, string WorkPhone, string Addr1, string Addr2, string City, string State, string Zip, string FirstName, string LastName, string Email, string EmailW)
		{
			throw new NotImplementedException();
		}

		public Result CaCredBalSet(string AccountID, float NewBal)
		{
			throw new NotImplementedException();
		}

		public bool SetCertNo(CertReIssue ReIssueData)
		{
			throw new NotImplementedException();
		}

		public Result<string> StageUvData(UvPushData UvData)
		{
			throw new NotImplementedException();
		}

		public void SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip)
		{
			throw new NotImplementedException();
		}


        public Result<double> GetCharAmt(string AttorneyCode, string ZipCode, string LineOfService)
        {
            throw new NotImplementedException();
        }

        string IHost.GetCounselorName(string counselorId)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.GetEscrow(int firm_id)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.ReqCredRpt(long user_id, int client_number)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.RequestCreditReport(int client_number, string client_type, string ref_code, string primary_birth_city, string secondary_birth_city, string primary_first_name, string primary_last_name, string primary_ssn, DateTime? primary_birth_date, string street_line1, string city, string state, string zip, string secondary_first_name, string secondary_last_name, string secondary_ssn, DateTime? secondary_birth_date)
        {
            throw new NotImplementedException();
        }

        Result IHost.GetCredRpt(int client_number)
        {
            throw new NotImplementedException();
        }

        Result<CreditScoreDto> IHost.GetCredScore(int client_number)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.SaveTrans(int client_number, int van_tran_ref, string invoice_no, float char_amt)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.GetNextSeqNo(int client_number)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.HaveCredRpt(int client_number)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.UpdateOCS(int client_number, int stat)
        {
            throw new NotImplementedException();
        }

        Result<System.Xml.XmlDocument> IHost.LoadDmp(int client_number)
        {
            throw new NotImplementedException();
        }

        Result<double> IHost.GetCharAmt(string AttorneyCode, string ZipCode, string LineOfService)
        {
            throw new NotImplementedException();
        }

        Result<string> IHost.GetUvDts()
        {
            throw new NotImplementedException();
        }

        Result<string> IHost.StageUvData(UvPushData UvData)
        {
            throw new NotImplementedException();
        }

        Result IHost.Bch_HomeSaver_SaveData(string need_help, string help_type, string contact_time, string loan_number, string first_name, string last_name, string address_line1, string address_line2, string city, string state, string zip, string phone, string email)
        {
            throw new NotImplementedException();
        }

        void IHost.SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip)
        {
            throw new NotImplementedException();
        }

        JudicialDistrict IHost.GetJudicialDistrict(string zipCode)
        {
            throw new NotImplementedException();
        }

        string IHost.GetEoustJudicialDistrictByName(string districtName)
        {
            throw new NotImplementedException();
        }

        EoustLogin IHost.GetEoustLogin()
        {
            throw new NotImplementedException();
        }

        string IHost.GetFirmEmail(string FirmID)
        {
            throw new NotImplementedException();
        }

        bool IHost.SetCertNo(CertReIssue ReIssueData)
        {
            throw new NotImplementedException();
        }

        MimLoginClientResult IHost.Mim_LoginClient(string reg_num)
        {
            throw new NotImplementedException();
        }

        MimCreateLoginValidateDataResult IHost.Mim_CreateLogin_ValidateData(string ssn)
        {
            throw new NotImplementedException();
        }

        MimDisclosureLoadDataResult IHost.Mim_Disclosure_LoadData(string ClNo)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_Disclosure_SaveTransaction(string ClNo, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state, string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no, string van_trans_ref, string char_amt)
        {
            throw new NotImplementedException();
        }

        MimForm7LoadDataResult IHost.Mim_Form7_LoadData(string ClNo, string regnum)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_Form7_SaveData(string ClNo, string contact_lastname, string contact_firstname, string contact_initial, string contact_ssn, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string contact_marital, string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity, string cemail, string dayphone, string nitephone, string attyname, string attyemail, string firm_id, string escrow, string secondary_flag, string char_type, string casenumber, string bankruptcy_type, string income_range, string number_in_house, string languageCode, string char_amt)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_Form7_SaveWriteOff(string ClNo, string regnum, string char_amt)
        {
            throw new NotImplementedException();
        }

        MimForm7MainLineResult IHost.Mim_Form7_MainLine(string contact_state)
        {
            throw new NotImplementedException();
        }


        Result IHost.MimCompleteCourse(string ClNo, string prescore, string postscore, string TestDTS)
        {
            throw new NotImplementedException();
        }

        MimQuicCalcLoadDataResult IHost.Mim_QuicCalc_LoadData(string ClNo, string regnum)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_QuicCalc_SaveData(string ClNo, string contact_state, string monthly_gross_income, string size_of_household)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_WaiverWaiver_SaveUv(string ClNo)
        {
            throw new NotImplementedException();
        }

        MimWaverAppLoadDataResult IHost.Mim_WaverApp_LoadData(string ClNo, string regnum)
        {
            throw new NotImplementedException();
        }

        Result IHost.Mim_WaverApp_SaveData(string clid, string contact_firstname, string contact_lastname, string contact_initial, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string real_email, string contact_telephone)
        {
            throw new NotImplementedException();
        }

        string IHost.Mim_NewClsID_Get(string reg_num)
        {
            throw new NotImplementedException();
        }

        string IHost.Mim_HostID_ReturningUser_Get(string reg_num)
        {
            throw new NotImplementedException();
        }

        Result IHost.MimSaveRegisterAttorney(string ClNo, string CaseNo, string Ssn, string FirmId, string AttnyName, string AttnyEmail, string UserName)
        {
            throw new NotImplementedException();
        }

        Result IHost.MimSavePreTest(string ClNo, int PreTest)
        {
            throw new NotImplementedException();
        }
        
        int IHost.GetMimSequenceNumber(string ClNo)
        {
            throw new NotImplementedException();
        }
        
        Result IHost.MimSaveXRef(string PriSsn, string SecSsn)
        {
            throw new NotImplementedException();
        }

        CaAccountTransactionsResult IHost.CaTrans(string ClientNo, string TransOpt)
        {
            throw new NotImplementedException();
        }

        CaCredResult IHost.CaCreds(string ClientNo)
        {
            throw new NotImplementedException();
        }

        CaMailPreferenceResult IHost.CaPerfGet(string ClientNo)
        {
            throw new NotImplementedException();
        }

        Result IHost.CaPerfSet(string ClientNo, string LetterPref, string StatmentPref)
        {
            throw new NotImplementedException();
        }

        Result IHost.CaInfoSet(string ClientNo, string HomePhone, string WorkPhone, string Addr1, string Addr2, string City, string State, string Zip, string FirstName, string LastName, string Email, string EmailW)
        {
            throw new NotImplementedException();
        }

        Result IHost.CaCredBalSet(string AccountID, float NewBal)
        {
            throw new NotImplementedException();
        }


        public MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum, string char_amt)
        {
            throw new NotImplementedException();
        }

        public Result MimDisclosureSaveMtcn(string ClNo, string mtcn, string char_amt)
        {
            throw new NotImplementedException();
        }

        public Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number, string char_amt)
        {
            throw new NotImplementedException();
        }


        public double MimDisclosureGetChargeAmount(string ZipCode, string FirmCode)
        {
            throw new NotImplementedException();
        }


        public Result<double> GetCharAmt(string AttorneyCode, string ZipCode)
        {
            throw new NotImplementedException();
        }


        public Result<string> SavePayrollDeductions(int InternetNumber, System.Collections.Generic.List<PayrollDeduction> Deductions)
        {
            throw new NotImplementedException();
        }


        public Result<System.Collections.Generic.List<PayrollDeduction>> ReadPayrollDeductions(int InternetNumber)
        {
            throw new NotImplementedException();
        }
    }
}