﻿using System;
using System.Reflection;
using AutoMapper;
using Cccs.Credability.Certificates.AutoMapperProfiles;
using Cccs.Identity.Impl.AutoMapperProfile;
using Cccs.Identity.WcfService.BootstrapperTasks;
using StructureMap;
using Cccs.Host;
using Cccs.Translation;
using Cccs.Geographics;
using Cccs.Debtplus;

namespace Cccs.Identity.WcfService
{
    public class Global : System.Web.HttpApplication
    {
        private const string AsposeLicense = "Cccs.Identity.WcfService.Aspose.Total.lic";

        protected void Application_Start(object sender, EventArgs e)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var licenseStream = assembly.GetManifestResourceStream(AsposeLicense);

            var wordLicense = new Aspose.Words.License();
            licenseStream.Position = 0;
            wordLicense.SetLicense(licenseStream);

            var barcodeLicense = new Aspose.BarCode.License();
            licenseStream.Position = 0;
            barcodeLicense.SetLicense(licenseStream);

            var reportLicense = new Aspose.Report.License();
            licenseStream.Position = 0;
            reportLicense.SetLicense(licenseStream);

            ConfigureDebtplus();

            ConfigureDependencyInjection();
            ConfigureAutoMapper();
        }

        private static Cccs.Configuration.IConfiguration m_configuration;
        private static ITranslation m_translation;
        private static IGeographics m_Geographics;
        private static IIdentity identityService;
        //private static IHost hostService;
        private static IDebtplus debtplusService;
        private void ConfigureDebtplus()
        {
            m_configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());
            m_translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());
            m_Geographics = new Cccs.Geographics.Impl.Geographics();
            identityService = new Cccs.Identity.Impl.Identity(m_configuration, m_translation, m_Geographics);
            //hostService = new Cccs.Host.Impl.Host(identityService);
            debtplusService = new Cccs.Debtplus.Impl.Debtplus(identityService);
        }

        //public static IHost Host
        //{
        //    get { return hostService; }
        //}

        public static IDebtplus Debtplus
        {
            get
            { return debtplusService; }
        }

        private void ConfigureDependencyInjection()
        {
            ObjectFactory.Initialize(r =>
            {
                r.AddRegistry<RegisterServices>();
                r.AddRegistry<RegisterCertificateRepositories>();
            });
        }

        private void ConfigureAutoMapper()
        {
            // Add any profile classes in the current domain.
            var configuration = Mapper.Configuration;
            configuration.AddProfile<PreFilingSnapshotProfile>();
            configuration.AddProfile<SnapshotProfile>();

            Mapper.AssertConfigurationIsValid();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}