﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Credability.Dal
{
    public interface IVancoRef
    {
        int ReqID { get; set; }
        System.Nullable<long> RegInNum { get; set; }
        string VanCustRef { get; set; }
        string VanPayMethRef { get; set; }
        string VanTransRef { get; set; }
        System.Nullable<System.DateTime> CreateDTS { get; set; }
        System.Nullable<System.DateTime> TransDTS { get; set; }
        string CardFirstName { get; set; }
        System.Data.Linq.Binary CardFirstNameEnc { get; set; }
        string CardMidName { get; set; }
        System.Data.Linq.Binary CardMidNameEnc { get; set; }
        string CardLastName { get; set; }
        System.Data.Linq.Binary CardLastNameEnc { get; set; }
        string CardAddr { get; set; }
        System.Data.Linq.Binary CardAddrEnc { get; set; }
        string CardAddr2 { get; set; }
        System.Data.Linq.Binary CardAddr2Enc { get; set; }
        string CardCity { get; set; }
        System.Data.Linq.Binary CardCityEnc { get; set; }
        string CardST { get; set; }
        System.Data.Linq.Binary CardSTEnc { get; set; }
        string CardZip { get; set; }
        System.Data.Linq.Binary CardZipEnc { get; set; }
        string CardPhone { get; set; }
        string CardNumber { get; set; }
        System.Data.Linq.Binary CardNumberEnc { get; set; }
        string CardExpMon { get; set; }
        System.Data.Linq.Binary CardExpMonEnc { get; set; }
        string CardExpYear { get; set; }
        System.Data.Linq.Binary CardExpYearEnc { get; set; }
        string Status { get; set; }
        System.Nullable<bool> SincWithVan { get; set; }
        System.Nullable<int> LastErrCode { get; set; }
        string ErrorOn { get; set; }
    }
}
