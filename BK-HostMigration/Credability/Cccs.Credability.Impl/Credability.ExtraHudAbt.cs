﻿using System;
using System.Linq;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{
		public UserSituationDescriptionExtraHudAbt UserSituationDescriptionExtraHudAbtGet(int ClientNumber)
		{
			UserSituationDescriptionExtraHudAbt UserSituationDescriptionExtraHudAbt = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserSituationDescriptionExtraHudAbt =
				 (
					 from entity in Entities.ExtraHudAbts
					 where entity.client_number == ClientNumber
					 select new UserSituationDescriptionExtraHudAbt
					 {
						 // ClientNumber = ClientNumber,
						 GOVGrant = entity.GovGrant,
						 STARTIntRate = entity.StartIntRate != null ? (float) entity.StartIntRate : 0,
						 TOPIntRate = entity.TopIntRate != null ? (float) entity.TopIntRate : 0,
						 ANPropTax = entity.AnPropTax != null ? (float) entity.AnPropTax : 0,
						 ANPropIns = entity.AnPropIns != null ? (float) entity.AnPropIns : 0,
						 INCludeIns = entity.IncludeIns,
						 INCludeTax = entity.IncludeTax,
					 }
					).FirstOrDefault<UserSituationDescriptionExtraHudAbt>();
			}
			return UserSituationDescriptionExtraHudAbt;
		}

		public UserSituationDescriptionExtraHudAbtResult UserSituationDescriptionExtraHudAbtAddUpdate(UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt)
		{
			ExtraHudAbt extraHudAbt = null;
			UserSituationDescriptionExtraHudAbtResult result = new UserSituationDescriptionExtraHudAbtResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{

					extraHudAbt =
					 (
						 from entity in Entities.ExtraHudAbts
						 where entity.client_number == userSituationDescriptionExtraHudAbt.ClientNumber
						 select entity
					 ).FirstOrDefault<ExtraHudAbt>();
					if(extraHudAbt != null) //Update
					{
						// extraQuests.client_number = userProfileExtraQuest.ClientNumber;
						extraHudAbt.GovGrant = userSituationDescriptionExtraHudAbt.GOVGrant;
						extraHudAbt.StartIntRate = userSituationDescriptionExtraHudAbt.STARTIntRate;
						extraHudAbt.TopIntRate = userSituationDescriptionExtraHudAbt.TOPIntRate;
						extraHudAbt.AnPropTax = userSituationDescriptionExtraHudAbt.ANPropTax;
						extraHudAbt.AnPropIns = userSituationDescriptionExtraHudAbt.ANPropIns;
						extraHudAbt.IncludeIns = userSituationDescriptionExtraHudAbt.INCludeIns;
						extraHudAbt.IncludeTax = userSituationDescriptionExtraHudAbt.INCludeTax;

						Entities.SubmitChanges();
						result.IsSuccessful = true;

					}
					else // Add New User 
					{
						extraHudAbt = new ExtraHudAbt();
						//ExtraQuests
						extraHudAbt.client_number = userSituationDescriptionExtraHudAbt.ClientNumber;
						extraHudAbt.GovGrant = userSituationDescriptionExtraHudAbt.GOVGrant;
						extraHudAbt.StartIntRate = userSituationDescriptionExtraHudAbt.STARTIntRate;
						extraHudAbt.TopIntRate = userSituationDescriptionExtraHudAbt.TOPIntRate;
						extraHudAbt.AnPropTax = userSituationDescriptionExtraHudAbt.ANPropTax;
						extraHudAbt.AnPropIns = userSituationDescriptionExtraHudAbt.ANPropIns;
						extraHudAbt.IncludeIns = userSituationDescriptionExtraHudAbt.INCludeIns;
						extraHudAbt.IncludeTax = userSituationDescriptionExtraHudAbt.INCludeTax;
						Entities.ExtraHudAbts.InsertOnSubmit(extraHudAbt);
						Entities.SubmitChanges();
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}


		public UserDescribeYourSituationBKC UserDescribeYourSituationBKCExtraHudAbtGet(int ClientNumber)
		{
			UserDescribeYourSituationBKC UserDescribeYourSituationBKC = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserDescribeYourSituationBKC =
				 (
					 from entity in Entities.ExtraHudAbts
					 where entity.client_number == ClientNumber
					 select new UserDescribeYourSituationBKC
					 {
						 ClientNumber = ClientNumber,
						 GovGrant = entity.GovGrant,
						 StartIntRate = entity.StartIntRate != null ? (decimal) entity.StartIntRate : 0,
						 TopIntRate = entity.TopIntRate != null ? (decimal) entity.TopIntRate : 0,
						 AnPropTax = entity.AnPropTax != null ? (decimal) entity.AnPropTax : 0,
						 AnPropIns = entity.AnPropIns != null ? (decimal) entity.AnPropIns : 0,
						 IncludeIns = entity.IncludeIns,
						 IncludeTax = entity.IncludeTax,
					 }
				 ).FirstOrDefault<UserDescribeYourSituationBKC>();
			}
			return UserDescribeYourSituationBKC;
		}

		public UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCExtraHudAbtAddUpdate(UserDescribeYourSituationBKC UserDescribeYourSituationBKC)
		{

			ExtraHudAbt extraHudAbt = null;

			UserDecsribeYourSituationBKCResult result = new UserDecsribeYourSituationBKCResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{

					extraHudAbt =
					 (
						 from entity in Entities.ExtraHudAbts
						 where entity.client_number == UserDescribeYourSituationBKC.ClientNumber
						 select entity
					 ).FirstOrDefault<ExtraHudAbt>();
					if(extraHudAbt != null) //Update
					{
						// extraQuests.client_number = userProfileExtraQuest.ClientNumber;
						extraHudAbt.GovGrant = UserDescribeYourSituationBKC.GovGrant;
						extraHudAbt.StartIntRate = (float) UserDescribeYourSituationBKC.StartIntRate;
						extraHudAbt.TopIntRate = (float) UserDescribeYourSituationBKC.TopIntRate;
						extraHudAbt.AnPropTax = (float) UserDescribeYourSituationBKC.AnPropTax;
						extraHudAbt.AnPropIns = (float) UserDescribeYourSituationBKC.AnPropIns;
						extraHudAbt.IncludeIns = UserDescribeYourSituationBKC.IncludeIns;
						extraHudAbt.IncludeTax = UserDescribeYourSituationBKC.IncludeTax;

						Entities.SubmitChanges();
						result.IsSuccessful = true;

					}
					else // Add New User 
					{
						extraHudAbt = new ExtraHudAbt();
						//ExtraQuests
						extraHudAbt.client_number = UserDescribeYourSituationBKC.ClientNumber;
						extraHudAbt.GovGrant = UserDescribeYourSituationBKC.GovGrant;
						extraHudAbt.StartIntRate = (float) UserDescribeYourSituationBKC.StartIntRate;
						extraHudAbt.TopIntRate = (float) UserDescribeYourSituationBKC.TopIntRate;
						extraHudAbt.AnPropTax = (float) UserDescribeYourSituationBKC.AnPropTax;
						extraHudAbt.AnPropIns = (float) UserDescribeYourSituationBKC.AnPropIns;
						extraHudAbt.IncludeIns = UserDescribeYourSituationBKC.IncludeIns;
						extraHudAbt.IncludeTax = UserDescribeYourSituationBKC.IncludeTax;
						Entities.ExtraHudAbts.InsertOnSubmit(extraHudAbt);
						Entities.SubmitChanges();
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}

		public CommonOptionBKCounseling CommonOptionBKCounselingExtraHudGet(int ClientNumber)
		{
			CommonOptionBKCounseling CommonOptionBKCounselings = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				CommonOptionBKCounselings =
				 (
					 from entity in Entities.ExtraHudAbts
					 where entity.client_number == ClientNumber
					 select new CommonOptionBKCounseling
					 {
						 ClientNumber = ClientNumber,
						 GovGrant = entity.GovGrant,
						 StartIntRate = entity.StartIntRate != null ? (decimal) entity.StartIntRate : 0,
						 TopIntRate = entity.TopIntRate != null ? (decimal) entity.TopIntRate : 0,
						 AnPropTax = entity.TopIntRate != null ? (decimal) entity.AnPropTax : 0,
						 AnPropIns = entity.AnPropIns != null ? (decimal) entity.AnPropIns : 0,
						 IncludeIns = entity.IncludeIns,
						 IncludeTax = entity.IncludeTax,
					 }
					).FirstOrDefault<CommonOptionBKCounseling>();
			}
			return CommonOptionBKCounselings;
		}
	}
}