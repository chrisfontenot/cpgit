﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
    public partial class Credability:ICredability
    {
        //Add Update Creditors Commnet
        public CreditorComment CreditorCommentGet(int ClientNumber)
        {
            CreditorComment creditorComment = null;

            using (CredabilityDataContext entities = new CredabilityDataContext())
            {
                creditorComment =
                (
                    from entity in entities.contactdetails
                    where (entity.client_number == ClientNumber)
                    select new CreditorComment
                    {
                        ClientNumber =( entity.client_number!=null)?entity.client_number:0,
                        CreditorsComment = (entity.creditor_comments != null) ? entity.creditor_comments : string.Empty,
                        SizeOfHouseHold = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
                        //ModForm = (entity.ModDebtMnt!=null)? Convert.ToInt32(entity.ModDebtMnt):0,


                    }
                ).FirstOrDefault<CreditorComment>();
            }

            return creditorComment;
        }
        public CreditorCommentResult CreaditorCommentsUpdate(CreditorComment creditorComment)
        {
            CreditorCommentResult Result = new CreditorCommentResult();
            contactdetail Contactdetail = null;
            using (CredabilityDataContext transactionEntities = new CredabilityDataContext())
            {
                try
                {
                    Contactdetail =
                     (
                       from contat in transactionEntities.contactdetails
                       where contat.client_number == creditorComment.ClientNumber
                       select contat
                      ).FirstOrDefault<contactdetail>();

                    if (Contactdetail != null)
                    {
                        Contactdetail.creditor_comments = creditorComment.CreditorsComment;
                        Contactdetail.ModDebtMnt = 1;
                        transactionEntities.SubmitChanges();
                        Result.IsSuccessful = true;
                        Result.ClientNumber = creditorComment.ClientNumber;
                    }
                    else
                    {
                        Result.ClientNumber = 0;
                        Result.IsSuccessful = false;
                    }

                }
                catch (Exception exception)
                {
                    Result.IsSuccessful = false;
                    Result.Exception = exception;
                    Result.Messages = new string[] { "An error occured attempting to save." };
                }
            }

            return Result;


        }
    }
}
