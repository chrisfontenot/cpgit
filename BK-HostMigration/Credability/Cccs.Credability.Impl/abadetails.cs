﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Credability.Impl;
namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public abadetailBKCounseling AbaDetailsGet(String ABANumber)
        {
            abadetailBKCounseling abadetail = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                abadetail =
                 (
                     from entity in Entities.aba_details
                     where entity.aba_number == ABANumber 
                     select new abadetailBKCounseling
                     {
                         ABAnumber = entity.aba_number,
                         ABAname = entity.aba_name,
                         ABAphone = entity.aba_phone,
                     }
                    ).FirstOrDefault<abadetailBKCounseling>();
            }
            return abadetail;
        }
    }
}
