﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Credability.Impl;

namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public DisclosureVancoRefBKCounseling DisclosureVancoRefBKCounselingGet(int ClientNumber)
        {
            DisclosureVancoRefBKCounseling DisclosureVancoRefBKCounselings = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                Entities.OpenEncryptionKey();
                DisclosureVancoRefBKCounselings =
                 (
                         from entity in Entities.VancoRefs
                         where entity.RegInNum == ClientNumber
                         select new DisclosureVancoRefBKCounseling
                         {
                             ClientNumber = ClientNumber,
                             REGInNum = (long)entity.RegInNum,
                             ReqID = entity.ReqID,
                             CARDFirstNameEnc = (entity.CardFirstNameEnc != null) ? Entities.DecryptText(entity.CardFirstNameEnc) : string.Empty,
                             CARDMidNameEnc = (entity.CardMidNameEnc != null) ? Entities.DecryptText(entity.CardMidNameEnc) : string.Empty,
                             CARDLastNameEnc = (entity.CardLastNameEnc != null) ? Entities.DecryptText(entity.CardLastNameEnc) : string.Empty,
                             CARDAddrEnc = (entity.CardAddrEnc != null) ? Entities.DecryptText(entity.CardAddrEnc) : string.Empty,
                             CARDAddr2Enc = (entity.CardAddr2Enc != null) ? Entities.DecryptText(entity.CardAddr2Enc) : string.Empty,
                             CARDCityEnc = (entity.CardCityEnc != null) ? Entities.DecryptText(entity.CardCityEnc) : string.Empty,
                             CARDSTEnc = (entity.CardSTEnc != null) ? Entities.DecryptText(entity.CardSTEnc) : string.Empty,
                             CARDZipEnc = (entity.CardZipEnc != null) ? Entities.DecryptText(entity.CardZipEnc) : string.Empty,
                             CARDNumberEnc = (entity.CardNumberEnc != null) ? Entities.DecryptText(entity.CardNumberEnc) : string.Empty,
                             CARDExpMonEnc = (entity.CardExpMonEnc != null) ? Entities.DecryptText(entity.CardExpMonEnc) : string.Empty,
                             CARDExpYearEnc = (entity.CardExpYearEnc != null) ? Entities.DecryptText(entity.CardExpYearEnc) : string.Empty,
                             Status = entity.Status,
                             SincWithVan = (entity.SincWithVan != null) ? Convert.ToBoolean(entity.SincWithVan) : false,
                             VanCustRef = entity.VanCustRef,
                             CREATEDTS = (DateTime)entity.CreateDTS,

                         }
                        ).FirstOrDefault<DisclosureVancoRefBKCounseling>();
            }
            return DisclosureVancoRefBKCounselings;
        }

        public IVancoRef DisclosureVancoRefAddUpdate(DisclosureVancoRefBKCounseling disclosureVancoRefBKCounseling)
        {
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                var vancoRef =
                 (
                         from entity in Entities.VancoRefs
                         where entity.RegInNum == disclosureVancoRefBKCounseling.REGInNum
                         select entity
                 ).FirstOrDefault();

                if (vancoRef == null)
                {
                    vancoRef = new Cccs.Credability.Dal.VancoRef();
                    vancoRef.CreateDTS = disclosureVancoRefBKCounseling.CREATEDTS;
                    Entities.VancoRefs.InsertOnSubmit(vancoRef);
                }

                vancoRef.RegInNum = disclosureVancoRefBKCounseling.REGInNum;
                vancoRef.CardFirstName = disclosureVancoRefBKCounseling.CARDFirstNameEnc;
                vancoRef.CardFirstNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDFirstNameEnc);
                vancoRef.CardMidName = disclosureVancoRefBKCounseling.CARDMidNameEnc;
                vancoRef.CardMidNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDMidNameEnc);
                vancoRef.CardLastName = disclosureVancoRefBKCounseling.CARDLastNameEnc;
                vancoRef.CardLastNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDLastNameEnc);
                vancoRef.CardAddr = disclosureVancoRefBKCounseling.CARDAddrEnc;
                vancoRef.CardAddrEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDAddrEnc);
                vancoRef.CardAddr2 = disclosureVancoRefBKCounseling.CARDAddr2Enc;
                vancoRef.CardAddr2Enc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDAddr2Enc);
                vancoRef.CardCity = disclosureVancoRefBKCounseling.CARDCityEnc;
                vancoRef.CardCityEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDCityEnc);
                vancoRef.CardExpMon = disclosureVancoRefBKCounseling.CARDExpMonEnc;
                vancoRef.CardExpMonEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDExpMonEnc);
                vancoRef.CardExpYear = disclosureVancoRefBKCounseling.CARDExpYearEnc;
                vancoRef.CardExpYearEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDExpYearEnc);
                vancoRef.CardNumber = disclosureVancoRefBKCounseling.CARDNumberEnc;
                vancoRef.CardNumberEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDNumberEnc);
                vancoRef.CardZip = disclosureVancoRefBKCounseling.CARDZipEnc;
                vancoRef.CardZipEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDZipEnc);
                vancoRef.CardST = disclosureVancoRefBKCounseling.CARDSTEnc;
                vancoRef.CardSTEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDSTEnc);
                vancoRef.CardCity = disclosureVancoRefBKCounseling.CARDCityEnc;
                vancoRef.CardCityEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDCityEnc);

                Entities.SubmitChanges();

                return vancoRef;
            }
        }

        public IVancoRef DisclosureVancoMimRefAddUpdate(DisclosureVancoRefBKCounseling disclosureVancoRefBKCounseling)
        {
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                var vancoRef =
                 (
                         from entity in Entities.VancoMimRefs
                         where entity.RegInNum == disclosureVancoRefBKCounseling.REGInNum
                         select entity
                 ).FirstOrDefault();

                if (vancoRef == null)
                {
                    vancoRef = new VancoMimRef();
                    vancoRef.CreateDTS = disclosureVancoRefBKCounseling.CREATEDTS;
                    Entities.VancoMimRefs.InsertOnSubmit(vancoRef);
                }

                vancoRef.RegInNum = disclosureVancoRefBKCounseling.REGInNum;
                vancoRef.CardFirstName = disclosureVancoRefBKCounseling.CARDFirstNameEnc;
                vancoRef.CardFirstNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDFirstNameEnc);
                vancoRef.CardMidName = disclosureVancoRefBKCounseling.CARDMidNameEnc;
                vancoRef.CardMidNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDMidNameEnc);
                vancoRef.CardLastName = disclosureVancoRefBKCounseling.CARDLastNameEnc;
                vancoRef.CardLastNameEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDLastNameEnc);
                vancoRef.CardAddr = disclosureVancoRefBKCounseling.CARDAddrEnc;
                vancoRef.CardAddrEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDAddrEnc);
                vancoRef.CardAddr2 = disclosureVancoRefBKCounseling.CARDAddr2Enc;
                vancoRef.CardAddr2Enc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDAddr2Enc);
                vancoRef.CardCity = disclosureVancoRefBKCounseling.CARDCityEnc;
                vancoRef.CardCityEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDCityEnc);
                vancoRef.CardExpMon = disclosureVancoRefBKCounseling.CARDExpMonEnc;
                vancoRef.CardExpMonEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDExpMonEnc);
                vancoRef.CardExpYear = disclosureVancoRefBKCounseling.CARDExpYearEnc;
                vancoRef.CardExpYearEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDExpYearEnc);
                vancoRef.CardNumber = disclosureVancoRefBKCounseling.CARDNumberEnc;
                vancoRef.CardNumberEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDNumberEnc);
                vancoRef.CardZip = disclosureVancoRefBKCounseling.CARDZipEnc;
                vancoRef.CardZipEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDZipEnc);
                vancoRef.CardST = disclosureVancoRefBKCounseling.CARDSTEnc;
                vancoRef.CardSTEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDSTEnc);
                vancoRef.CardCity = disclosureVancoRefBKCounseling.CARDCityEnc;
                vancoRef.CardCityEnc = Entities.EncryptText(disclosureVancoRefBKCounseling.CARDCityEnc);

                Entities.SubmitChanges();

                return vancoRef;
            }
        }
    }


}

