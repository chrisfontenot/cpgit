﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        private const int otherServicerId = 12982;

        public int OtherServicerId { get { return otherServicerId; } }

        public CompanyInfo[] CompanyListGet()
        {
            CompanyInfo[] companyInfo = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                companyInfo =
                 (
                     from entity in Entities.ServMorts orderby
                        entity.ServName
                     
                     select new CompanyInfo
                     {
                         ServID = entity.ServID.GetValueOrDefault(0),
                         CompanyName =entity.ServName,
                     }
                    ).ToArray();
            }
            return companyInfo;
        }

        public bool IsOtherServicer(int servId)
        {
            return servId == OtherServicerId;
        }

        public string GetServicerName(int servId)
        {
            string servName = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                servName =
                (
                    from entity in Entities.ServMorts
                     where entity.ServID == servId
                     select entity.ServName
                ).FirstOrDefault();
            }
            return servName;
        }
    }
}
