﻿// -----------------------------------------------------------------------
// <copyright file="Credability.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Linq;
using Cccs.Credability.Dal;
namespace Cccs.Credability.Impl
{

    /// <summary>
    /// Get configurations from the syscontrol table.
    /// </summary>
    partial class Credability : ICredability
    {

        public bool isChatCodeActive(string websiteCode,string language)
        {
            string configKey = string.Format("{0}.{1}.Code", websiteCode, language);
            using (CredabilityDataContext entities = new CredabilityDataContext())
            {
                var x = from c in entities.SysControls
                        where c.WhatSys == configKey || c.WhatSys == "ChatCode"
                        select new { isActive = c.IsActive };

                if (x.Any(y => !(y.isActive ?? true)))
                    return false;
                else
                    return true;

            }
        }
    }
}
