﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Identity;
namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{

        //public AFS AFSGet(int ClientNumber)
        //{
        //    AFS Afs = null;
        //    //contactdetail Contactdetail = null;
        //    using (CredabilityDataContext transactionEntities = new CredabilityDataContext())
        //    {
        //        transactionEntities.OpenEncryptionKey();

        //        Afs =
        //        (
        //                from contat in transactionEntities.contactdetails
        //                where contat.client_number == ClientNumber
        //                select new AFS
        //                {
        //                    ClientNumber = Convert.ToInt32(ClientNumber),
        //                    IsSecondaryPersonNeeded = contat.CosignLastNameEnc != null && transactionEntities.DecryptText(contat.CosignLastNameEnc) != string.Empty,
        //                    PrimaryPerson = (contat.ConfirmMaidenNameEnc != null) ? transactionEntities.DecryptText(contat.ConfirmMaidenNameEnc) : string.Empty,
        //                    SecondaryPerson = (contat.CosignMaidenNameEnc != null) ? transactionEntities.DecryptText(contat.CosignMaidenNameEnc) : string.Empty,
        //                }
        //        ).FirstOrDefault<AFS>();
        //    }
        //    return Afs;
        //}

        public AFS AfsGet(long userId, int clientNumber)
        {
            AFS afs = null;
            using (CredabilityDataContext transactionEntities = new CredabilityDataContext())
            {
                transactionEntities.OpenEncryptionKey();

                afs =
                (
                    from contat in transactionEntities.contactdetails
                    where contat.client_number == clientNumber
                    select new AFS
                    {
                        ClientNumber = clientNumber,
                        PrimaryPerson = (contat.ConfirmMaidenNameEnc != null) ? transactionEntities.DecryptText(contat.ConfirmMaidenNameEnc) : string.Empty,
                        SecondaryPerson = (contat.CosignMaidenNameEnc != null) ? transactionEntities.DecryptText(contat.CosignMaidenNameEnc) : string.Empty,
                        SecondaryPersonFirstName = (contat.cosign_firstname != null) ? contat.cosign_firstname : string.Empty,
                    }
                ).FirstOrDefault<AFS>();
            }
            UserDetail secondary = identityService.UserDetailGet(userId, false);
			//afs.IsSecondaryPersonNeeded = secondary != null && secondary.LastName != null && secondary.LastName != string.Empty;
			afs.IsSecondaryPersonNeeded = secondary != null && !String.IsNullOrEmpty(secondary.LastName);
			return afs;
        }

		public AFSResult AfsAddUpdate(AFS Afs)
		{
			contactdetail Contactdetail = null;

			AFSResult Result = new AFSResult();
			using (CredabilityDataContext Entities = new CredabilityDataContext())
			{

				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == Afs.ClientNumber
						 select contat
					).FirstOrDefault<contactdetail>();
				if (Contactdetail != null)
				{
					try
					{
                        Contactdetail.client_number = Afs.ClientNumber;
                        Contactdetail.ConfirmMaidenNameEnc = Entities.EncryptText(Afs.PrimaryPerson);
                        Contactdetail.CosignMaidenNameEnc = Entities.EncryptText(Afs.SecondaryPerson);
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
					catch (Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
				else //Add New User 
				{
					try
					{
						Contactdetail = new contactdetail();

						Contactdetail.client_number = Afs.ClientNumber;
                        Contactdetail.temp_id = DateTime.Now.ToString("F");
						Contactdetail.ConfirmMaidenNameEnc = Entities.EncryptText(Afs.PrimaryPerson);
						Contactdetail.CosignMaidenNameEnc = Entities.EncryptText(Afs.SecondaryPerson);
                        Contactdetail.completed = 0;
                        Contactdetail.processed = 0;
						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
					catch (Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}




			}
			return Result;
		}
	}
}
