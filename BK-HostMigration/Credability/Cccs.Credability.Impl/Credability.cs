﻿using System;
using Cccs.Credability.Dal;
using System.Configuration;
using Cccs.Identity;
//using Cccs.Host;
using Cccs.Vanco;
using CommonResources = Cccs.Resources;
using Cccs.Debtplus;

namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{
        
        private readonly NLog.Logger logger;
		private readonly IIdentity identityService;
		//private readonly IHost hostService;
        private readonly IDebtplus debtplusService;
        private readonly IVanco vancoBkcEnglish;
		private readonly IVanco vancoBkcSpanish;
		private readonly IVanco vancoBkeEnglish;
		private readonly IVanco vancoBkeSpanish;
		private readonly string bkeMySqlConnection;

		private const string BkeMySqlConnectionSetting = "BkeMySqlConn";
		private const string EncryptionKeySetting = "EnKey";
		private const string EncryptionPasswordSetting = "EnPassword";

		public Credability()
		{
			SetEncryption();
		}

		//public Credability(IIdentity identity, IHost host, IVanco bkc_vanco_en, IVanco bkc_vanco_es, IVanco bke_vanco_en, IVanco bke_vanco_es)
       public Credability(IIdentity identity, IDebtplus debtplus, IVanco bkc_vanco_en, IVanco bkc_vanco_es, IVanco bke_vanco_en, IVanco bke_vanco_es)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();

			identityService = identity;
            //hostService = host;
            debtplusService = debtplus;

			this.vancoBkcEnglish = bkc_vanco_en;
			this.vancoBkcSpanish = bkc_vanco_es;
			this.vancoBkeEnglish = bke_vanco_en;
			this.vancoBkeSpanish = bke_vanco_es;

			this.bkeMySqlConnection = SetBkeMySqlConnectionString();
			SetEncryption();
		}

		private string SetBkeMySqlConnectionString()
		{
			var bkeConnection = ConfigurationManager.ConnectionStrings[BkeMySqlConnectionSetting];
			if(bkeConnection == null || String.IsNullOrEmpty(bkeConnection.ConnectionString))
			{
				var verify = CommonResources.LogMessages.VerifyConnectionString.FormatWith(BkeMySqlConnectionSetting);
				logger.Fatal(Resources.LogMessages.MissingMySqlConnectionSetting.FormatWith(verify));

				throw new ConfigurationErrorsException(CommonResources.ErrorMessages.UnableToConfigureDatabase);
			}

			return bkeConnection.ConnectionString;
		}

		private void SetEncryption()
		{
			const string dataContext = "CredabilityDataContext";
			var configurationIsValid = true;

			var encryptionKey = ConfigurationManager.AppSettings[EncryptionKeySetting];
			if(encryptionKey == null)
			{
				var verify = CommonResources.LogMessages.VerifyAppSettings.FormatWith(EncryptionKeySetting);
				logger.Fatal(CommonResources.LogMessages.UnableToRetrieveDataContextEncryptionKey.FormatWith(dataContext, verify));

				configurationIsValid = false;
			}

			var encryptionPassword = ConfigurationManager.AppSettings[EncryptionPasswordSetting];
			if(encryptionPassword == null)
			{
				var verify = CommonResources.LogMessages.VerifyAppSettings.FormatWith(EncryptionPasswordSetting);
				logger.Fatal(CommonResources.LogMessages.UnableToRetrieveDataContextEncryptionKey.FormatWith(dataContext, verify));

				configurationIsValid = false;
			}

			if(!configurationIsValid)
				throw new ConfigurationErrorsException(CommonResources.ErrorMessages.UnableToConfigureEncryption);

			CredabilityDataContext.SetEncryption(encryptionKey, encryptionPassword);
		}
	}
}