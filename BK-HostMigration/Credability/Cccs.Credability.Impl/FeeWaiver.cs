﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Cccs.Credability.Dal;
//using Cccs.Credability.Impl;

//namespace Cccs.Credability.Impl
//{
//    public partial class Credability : ICredability
//    {

//        public FeeWaiverAFSResult FeeWaiverAdd(FeeWaiverAFS feeWaiverAFS)
//        {
//            FeeWaiverAFSResult FeeWaiverAFSResults =new FeeWaiverAFSResult();
//            FeeWaiver feeWaiver = null;
            
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                feeWaiver =
//                 (
//                     from feewaiver in Entities.FeeWaivers
//                     where feewaiver.client_number == feeWaiverAFS.ClientNumber
//                     select feewaiver
//                  ).FirstOrDefault<FeeWaiver>();
//                if (feeWaiver == null)
//                {
//                    feeWaiver = new FeeWaiver();
//                    try
//                    {
//                        feeWaiver.client_number = feeWaiverAFS.ClientNumber;
//                        feeWaiver.ClientType = feeWaiverAFS.ClientType;
//                        feeWaiver.Language = feeWaiverAFS.Language;
//                        feeWaiver.FeePolicyDTS = System.DateTime.Now;
//                        Entities.FeeWaivers.InsertOnSubmit(feeWaiver);
//                        Entities.SubmitChanges();
//                        FeeWaiverAFSResults.FwID = feeWaiver.FwID;
//                        FeeWaiverAFSResults.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        FeeWaiverAFSResults.IsSuccessful = false;
//                        FeeWaiverAFSResults.Exception = exception;
//                        FeeWaiverAFSResults.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }
           
//            }
//            return FeeWaiverAFSResults;
//        }

//        public FeeWaiverAFSResult FeeWaiverUpdateHostId(FeeWaiverAFS feeWaiverAFS)
//        {
//            FeeWaiverAFSResult FeeWaiverAFSResults = new FeeWaiverAFSResult();
//            FeeWaiver feeWaiver = null;

//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                feeWaiver =
//                 (
//                     from feewaiver in Entities.FeeWaivers
//                     where feewaiver.client_number == feeWaiverAFS.ClientNumber
//                     select feewaiver
//                  ).FirstOrDefault<FeeWaiver>();
//                if (feeWaiver == null)
//                {
//                    feeWaiver
//                    =
//                     (
//                         from feewaiver in Entities.FeeWaivers
//                         where feewaiver.client_number == feeWaiverAFS.HostId
//                         select feewaiver
//                      ).FirstOrDefault<FeeWaiver>();
//                    if (feeWaiver != null)
//                    {
//                        try
//                        {
//                            feeWaiver.client_number = feeWaiverAFS.HostId;
//                            Entities.SubmitChanges();
//                            //FeeWaiverAFSResults.FwID = 001;
//                            FeeWaiverAFSResults.IsSuccessful = true;

//                        }
//                        catch (Exception exception)
//                        {
//                            FeeWaiverAFSResults.IsSuccessful = false;
//                            FeeWaiverAFSResults.Exception = exception;
//                            FeeWaiverAFSResults.Messages = new string[] { "An error occured attempting to save." };
//                        }
//                    }
//                }
//                else
//                {
//                    try
//                    {
//                        feeWaiver.client_number = feeWaiverAFS.HostId;
//                        Entities.SubmitChanges();
//                        //FeeWaiverAFSResults.FwID = 001;
//                        FeeWaiverAFSResults.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        FeeWaiverAFSResults.IsSuccessful = false;
//                        FeeWaiverAFSResults.Exception = exception;
//                        FeeWaiverAFSResults.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }


//            }
//            return FeeWaiverAFSResults;
//        }

//        public FeeWaiverAFSResult FWIDGet(int clientNumber)
//        {
//            FeeWaiverAFSResult FeeWaiverAFSResults = null;
//            using (CredabilityDataContext transactionEntities = new CredabilityDataContext())
//            {


//                FeeWaiverAFSResults =
//                (
//                    from contat in transactionEntities.FeeWaivers
//                    where contat.client_number == clientNumber && contat.ClientType == "Coun"
//                    select new FeeWaiverAFSResult
//                    {
//                        FwID = contat.FwID,
                        
//                    }
//                ).FirstOrDefault<FeeWaiverAFSResult>();
//            }

//            return FeeWaiverAFSResults;
//        }

//        public FeeWaiverAFSResult FWIDEduClientGet(int clientNumber)
//        {
//            using (CredabilityDataContext transactionEntities = new CredabilityDataContext())
//            {
//                var FeeWaiverAFSResults =
//                (
//                    from contat in transactionEntities.FeeWaivers
//                    where contat.client_number == clientNumber && contat.ClientType == "Edu"
//                    select new FeeWaiverAFSResult
//                    {
//                        FwID = contat.FwID,

//                    }
//                ).FirstOrDefault<FeeWaiverAFSResult>();

//                return FeeWaiverAFSResults;
//            }
//        }


//        public WaiverWaiverResult WaiverWaiverBKCounselingUpdate(WaiverWaiver waiverWaiver)
//        {
//            WaiverWaiverResult Result = new WaiverWaiverResult();
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                var feeWaiver =
//                 (
//                     from feewaiver in Entities.FeeWaivers
//                     where feewaiver.FwID == waiverWaiver.FwID
//                     select feewaiver
//                  ).FirstOrDefault<FeeWaiver>();
//                if (feeWaiver != null)
//                {
//                    try
//                    {
//                        feeWaiver.SignUp = waiverWaiver.SIgnUp;
//                        feeWaiver.Pri4SsnEnc = Entities.EncryptText(waiverWaiver.Pri4SsnEnc);
//                        feeWaiver.PriZipEnc = Entities.EncryptText(waiverWaiver.PriZipEnc);
//                        feeWaiver.Sec4SsnEnc = Entities.EncryptText(waiverWaiver.Sec4SsnEnc);
//                        feeWaiver.SecZipEnc = Entities.EncryptText(waiverWaiver.SecZipEnc);
//                        feeWaiver.JointFile = waiverWaiver.JointFile;
//                        feeWaiver.LastModDTS = DateTime.Now;
//                        feeWaiver.SignUpDTS = DateTime.Now;

//                        Entities.SubmitChanges();
//                        Result.FwID = feeWaiver.FwID;
//                        Result.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        Result.IsSuccessful = false;
//                        Result.Exception = exception;
//                        Result.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }
//                else
//                {
//                    try
//                    {
//                        feeWaiver = new FeeWaiver(); 
                        
//                        feeWaiver.SignUp = waiverWaiver.SIgnUp;
//                        feeWaiver.Pri4SsnEnc = Entities.EncryptText(waiverWaiver.Pri4SsnEnc);
//                        feeWaiver.PriZipEnc = Entities.EncryptText(waiverWaiver.PriZipEnc);
//                        feeWaiver.Sec4SsnEnc = Entities.EncryptText(waiverWaiver.Sec4SsnEnc);
//                        feeWaiver.SecZipEnc = Entities.EncryptText(waiverWaiver.SecZipEnc);
//                        feeWaiver.JointFile = waiverWaiver.JointFile;
//                        feeWaiver.client_number = waiverWaiver.ClientNumber;
//                        feeWaiver.LastModDTS = DateTime.Now;
//                        feeWaiver.SignUpDTS = DateTime.Now;
//                        Entities.FeeWaivers.InsertOnSubmit(feeWaiver);
//                        Entities.SubmitChanges();
//                        Result.FwID = feeWaiver.FwID;
//                        Result.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        Result.IsSuccessful = false;
//                        Result.Exception = exception;
//                        Result.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }

//            }
//            return Result;
//        }

//        public Boolean IsZipCodeAvailable(string code)
//        {
//            Boolean flag = false;
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                var zipCode = from zip in Entities.zipcodes
//                              where zip.id == code
//                              select zip;
//                if (zipCode.Count() > 0) flag = true; 
//            }
//            return flag;
//        }

//        public WaverAppFeeWaiverBKCounseling WaverAppFeeWaiverBKCounselingGet(int FwID)
//        {
//            WaverAppFeeWaiverBKCounseling waverAppFeeWaiverBKCounselings = null;
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                waverAppFeeWaiverBKCounselings =
//                     (
//                         from entity in Entities.FeeWaivers
//                         where entity.FwID == FwID
//                         select new WaverAppFeeWaiverBKCounseling
//                         {
//                             FwID = (entity.FwID  != null) ? entity.FwID: 0,
//                             SIgnUp = (entity.SignUp!=null)? Convert.ToInt32(entity.SignUp):0,
//                             WAiverType = (entity.WaiverType != null) ? (entity.WaiverType) : String.Empty,
//                             JOintFile = (entity.JointFile!=null)? Convert.ToInt32(entity.JointFile):0,
//                             WAitPolicy = (entity.WaitPolicy!=null)?(Convert.ToBoolean(entity.WaitPolicy)):false,
//                             LAstModDTS = (entity.LastModDTS!=null)? Convert.ToDateTime(entity.LastModDTS):DateTime.Now,
//                             PRiFName = (entity.PriFName!=null)?(entity.PriFName):String.Empty,
//                             PRiLName = (entity.PriLName!=null)?(entity.PriLName):String.Empty,
//                             SEcFName = (entity.SecFName!=null)?(entity.SecFName ):String.Empty,
//                             SEcLName=(entity.SecLName!=null)?(entity.SecLName):String.Empty,
//                             SIgnUpDTS=(entity.SignUpDTS!=null)?Convert.ToDateTime(entity.SignUpDTS):DateTime.Now,

//                         }
//                     ).FirstOrDefault<WaverAppFeeWaiverBKCounseling>();
//            }
//            return waverAppFeeWaiverBKCounselings;
//        }

//        public WaverAppFeeWaiverBKCounseling FeeWaiverBKCounselingGetByClientNumber(int ClientNumber)
//        {
//            WaverAppFeeWaiverBKCounseling waverAppFeeWaiverBKCounselings = null;
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                waverAppFeeWaiverBKCounselings =
//                     (
//                         from entity in Entities.FeeWaivers
//                         where entity.client_number == ClientNumber
//                         select new WaverAppFeeWaiverBKCounseling
//                         {
//                             FwID = (entity.FwID != null) ? entity.FwID : 0,
//                             SIgnUp = (entity.SignUp != null) ? Convert.ToInt32(entity.SignUp) : 0,
//                             WAiverType = (entity.WaiverType != null) ? (entity.WaiverType) : String.Empty,
//                             JOintFile = (entity.JointFile != null) ? Convert.ToInt32(entity.JointFile) : 0,
//                             WAitPolicy = (entity.WaitPolicy != null) ? (Convert.ToBoolean(entity.WaitPolicy)) : false,
//                             LAstModDTS = (entity.LastModDTS != null) ? Convert.ToDateTime(entity.LastModDTS) : DateTime.Now,
//                             PRiFName = (entity.PriFName != null) ? (entity.PriFName) : String.Empty,
//                             PRiLName = (entity.PriLName != null) ? (entity.PriLName) : String.Empty,
//                             SEcFName = (entity.SecFName != null) ? (entity.SecFName) : String.Empty,
//                             SEcLName = (entity.SecLName != null) ? (entity.SecLName) : String.Empty,
//                             SIgnUpDTS = (entity.SignUpDTS != null) ? Convert.ToDateTime(entity.SignUpDTS) : DateTime.Now,

//                         }
//                     ).FirstOrDefault<WaverAppFeeWaiverBKCounseling>();
//            }
//            return waverAppFeeWaiverBKCounselings;
//        }

//        public WaverAppFeeWaiverBKCounselingResult WaverAppFeeWaiverBKCounselingUpdate(WaverAppFeeWaiverBKCounseling waverAppFeeWaiverBKCounseling)
//        {
//            FeeWaiver feeWaiver = null;
//            WaverAppFeeWaiverBKCounselingResult Result = new WaverAppFeeWaiverBKCounselingResult();
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                feeWaiver =
//                 (
//                     from feewaiver in Entities.FeeWaivers
//                     where feewaiver.FwID == waverAppFeeWaiverBKCounseling.FwID
//                     select feewaiver
//                  ).FirstOrDefault<FeeWaiver>();
//                if (feeWaiver != null)
//                {
//                    try
//                    {
//                        feeWaiver.SignUp = waverAppFeeWaiverBKCounseling.SIgnUp;
//                        feeWaiver.WaiverType = waverAppFeeWaiverBKCounseling.WAiverType;
//                        feeWaiver.WaitPolicy = waverAppFeeWaiverBKCounseling.WAitPolicy;
//                        feeWaiver.PriFName = waverAppFeeWaiverBKCounseling.PRiFName;
//                        feeWaiver.PriLName = waverAppFeeWaiverBKCounseling.PRiLName;
//                        feeWaiver.SecFName = waverAppFeeWaiverBKCounseling.SEcFName;
//                        feeWaiver.SecLName = waverAppFeeWaiverBKCounseling.SEcLName;
//                        feeWaiver.JointFile = waverAppFeeWaiverBKCounseling.JOintFile;
//                        feeWaiver.LastModDTS = waverAppFeeWaiverBKCounseling.LAstModDTS;
//                        feeWaiver.SignUpDTS = waverAppFeeWaiverBKCounseling.SIgnUpDTS;
//                        //feeWaiver.Aproved = waverAppFeeWaiverBKCounseling.Approved; 
//                        Entities.SubmitChanges();
//                        Result.FwID = feeWaiver.FwID;
//                        Result.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        Result.IsSuccessful = false;
//                        Result.Exception = exception;
//                        Result.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }

//            }
//            return Result;
//        }

//        public WaverAppFeeWaiverBKCounselingResult FeeWaiverBKCounselingUpdate(FeeWaiverBKC FeeWaiverBKC)
//        {
//            FeeWaiver feeWaiver = null;
//            WaverAppFeeWaiverBKCounselingResult Result = new WaverAppFeeWaiverBKCounselingResult();
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                feeWaiver =
//                 (
//                     from feewaiver in Entities.FeeWaivers
//                     where feewaiver.FwID == FeeWaiverBKC.FwID
//                     select feewaiver
//                  ).FirstOrDefault<FeeWaiver>();
//                if (feeWaiver != null)
//                {
//                    try
//                    {
//                        feeWaiver.SignUp = FeeWaiverBKC.SignUp;
//                        feeWaiver.Aproved = FeeWaiverBKC.Approved;
//                        feeWaiver.WaiverType = FeeWaiverBKC.WAiverType;
//                        feeWaiver.LastModCouns = FeeWaiverBKC.LastModCouns;
//                        feeWaiver.DecisionDTS = DateTime.Now;
//                        feeWaiver.LastModDTS = DateTime.Now; ;
//                        feeWaiver.SignUpDTS = DateTime.Now; 

//                        Entities.SubmitChanges();
//                        Result.FwID = feeWaiver.FwID;
//                        Result.IsSuccessful = true;

//                    }
//                    catch (Exception exception)
//                    {
//                        Result.IsSuccessful = false;
//                        Result.Exception = exception;
//                        Result.Messages = new string[] { "An error occured attempting to save." };
//                    }
//                }

//            }
//            return Result;
//        }

//     public FeeWaiverForAprWait FeeWaiverForAprWaitGet(int FWID)
//        {
//            FeeWaiverForAprWait FeeWaiverForAprWaits = null;
//            using (CredabilityDataContext Entities = new CredabilityDataContext())
//            {
//                FeeWaiverForAprWaits =
//                 (
//                         from entity in Entities.FeeWaivers
//                         where entity.FwID == FWID
//                         select new FeeWaiverForAprWait
//                         {
//                             FwID = FWID,
//                             FeePolicy = (entity.FeePolicy!=null)?(int)entity.FeePolicy:2,
//                             CounsNotes=entity.CounsNotes,
//                             Aproved=(entity.Aproved!=null)?(int)entity.Aproved:2,
//                             SignUp = (entity.SignUp!=null)?(int)entity.SignUp:2,
//                         }
//                 ).FirstOrDefault();
//            }
//            return FeeWaiverForAprWaits;
//        }

     


//    }

    
    

//}
