﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        #region
        public UserProfileExtraQuest UserProfileExtraQuestGet(int ClientNumber)
        {
            UserProfileExtraQuest UserProfileExtraQuest = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                UserProfileExtraQuest =
                 (
                     from entity in Entities.ExtraQuests
                     where entity.client_number == ClientNumber
                     select new UserProfileExtraQuest
                     {
                         ClientNumber = ClientNumber,
                         ContactEmployed = entity.contact_Employed,
                         CosignEmployed = entity.cosign_Employed,
                         CoCounsPres = entity.CoCounsPres,
                         PriorCouns = entity.PriorCouns,
                         PriorDMP = entity.PriorDMP,
                         RecentDMP = entity.RecentDMP,
                         ContactEducation = entity.contact_Education,
                         MilitaryStatus = entity.military_Status,
                         Grade = entity.Grade,
                         BranchOfService = entity.branchOfService,
                         Rank = entity.Rank,
                     }
                    ).FirstOrDefault();
            }
            return UserProfileExtraQuest;
        }

        public UserProfileExtraQuestResult UserProfileExtraQuestAddUpdate(UserProfileExtraQuest userProfileExtraQuest)
        {

            ExtraQuest extraQuests = null;

            UserProfileExtraQuestResult result = new UserProfileExtraQuestResult();
            string Message = string.Empty;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                try
                {

                    extraQuests =
                     (
                         from entity in Entities.ExtraQuests
                         where entity.client_number == userProfileExtraQuest.ClientNumber
                         select entity
                     ).FirstOrDefault<ExtraQuest>();
                    if (extraQuests != null) //Update
                    {
                        // extraQuests.client_number = userProfileExtraQuest.ClientNumber;
                        extraQuests.contact_Employed = userProfileExtraQuest.ContactEmployed;
                        extraQuests.cosign_Employed = userProfileExtraQuest.CosignEmployed;
                        extraQuests.PriorCouns = userProfileExtraQuest.PriorCouns;
                        extraQuests.PriorDMP = userProfileExtraQuest.PriorDMP;
                        extraQuests.RecentDMP = userProfileExtraQuest.RecentDMP;
                        extraQuests.contact_Education = userProfileExtraQuest.ContactEducation;
                        extraQuests.military_Status = userProfileExtraQuest.MilitaryStatus;
                        extraQuests.Grade = userProfileExtraQuest.Grade;
                        extraQuests.branchOfService = userProfileExtraQuest.BranchOfService;
                        extraQuests.Rank = userProfileExtraQuest.Rank;

                        extraQuests.CoCounsPres = userProfileExtraQuest.CoCounsPres;

                        //;
                        Entities.SubmitChanges();

                        result.ClientNumber = Convert.ToInt32(extraQuests.client_number);
                        result.IsSuccessful = true;

                    }
                    else // Add New User 
                    {

                        //ExtraQuests
                        extraQuests = new ExtraQuest();
                        extraQuests.client_number = Convert.ToInt32(userProfileExtraQuest.ClientNumber);

                        extraQuests.contact_Employed = userProfileExtraQuest.ContactEmployed;
                        extraQuests.cosign_Employed = userProfileExtraQuest.CosignEmployed;
                        extraQuests.PriorCouns = userProfileExtraQuest.PriorCouns;
                        extraQuests.CoCounsPres = userProfileExtraQuest.CoCounsPres;
                        extraQuests.PriorDMP = userProfileExtraQuest.PriorDMP;
                        extraQuests.RecentDMP = userProfileExtraQuest.RecentDMP;
                        extraQuests.contact_Education = userProfileExtraQuest.ContactEducation;
                        extraQuests.military_Status = userProfileExtraQuest.MilitaryStatus;
                        extraQuests.Grade = userProfileExtraQuest.Grade;
                        extraQuests.branchOfService = userProfileExtraQuest.BranchOfService;
                        extraQuests.Rank = userProfileExtraQuest.Rank;

                        Entities.ExtraQuests.InsertOnSubmit(extraQuests);
                        // Entities.ExtraQuests.InsertOnSubmit(ExtraQuest);
                        Entities.SubmitChanges();
                        // UpdateResult.ClientNumber = ExtraQuest;
                        result.IsSuccessful = true;
                    }
                }
                catch (Exception exception)
                {
                    result.IsSuccessful = false;
                    result.Exception = exception;
                    result.Messages = new string[] { "An error occured attempting to save." };
                }
                return result;
            }


        }

        public UserSituationDescriptionExtraQuest UserSituationDescriptionExtraQuestGet(int ClientNumber)
        {
            UserSituationDescriptionExtraQuest UserSituationDescriptionExtraQuest = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                UserSituationDescriptionExtraQuest =
                 (
                     from entity in Entities.ExtraQuests
                     where entity.client_number == ClientNumber
                     select new UserSituationDescriptionExtraQuest
                     {
                         ClientNumber = ClientNumber,
                         SECEvent = entity.SecEvent,
                         MEDExp = entity.MedExp,
                         LOSTSO = entity.LostSO,
                         HEALTHWage = entity.HealthWage,
                         UNEmployed = entity.Unemployed,
                         TAXLien = entity.TaxLien,
                         MORTRate = entity.MortRate,
                         ASSISTModify=entity.AssistModify,
                         LOANModify=entity.LoanModify,
                         //Hamp
                         MortgageModifiedHamp=entity.MortgageModifiedHamp,
                         MortgageInvestor=entity.MortgageInvestor,
                         IsBusinessOrPersonalMortgage=entity.IsBusinessMortgage,
                         HousingType=entity.HousingType,
                         AgreeEscrowAccount=entity.EscrowAccount,
                         AnyLiens=entity.AnyLiens,
                         ConvictedFelony=entity.ConvictedFelony,
                         IsLoanFirstLienFMorFMae=entity.IsLoanFirstLien,
                         LoanSoldFMorFmae=entity.LoanSoldFMorFMae,
                         LoanRefinanceProgram=entity.LoanRefinance,
                         TimeLineEvents=entity.TimeLineEvents,
                         ClientGoals=entity.ClientGoals

                     }
                    ).FirstOrDefault();
            }
            return UserSituationDescriptionExtraQuest;
        }

        public UserSituationDescriptionExtraQuestResult UserSituationDescriptionExtraQuestAddUpdate(UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest)
        {

            ExtraQuest extraQuests = null;

            UserSituationDescriptionExtraQuestResult result = new UserSituationDescriptionExtraQuestResult();
            string Message = string.Empty;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                try
                {

                    extraQuests =
                     (
                         from entity in Entities.ExtraQuests
                         where entity.client_number == userSituationDescriptionExtraQuest.ClientNumber
                         select entity
                     ).FirstOrDefault<ExtraQuest>();
                    if (extraQuests != null) //Update
                    {
                        // extraQuests.client_number = userProfileExtraQuest.ClientNumber;
                        extraQuests.SecEvent = userSituationDescriptionExtraQuest.SECEvent;
                        extraQuests.MedExp = userSituationDescriptionExtraQuest.MEDExp;
                        extraQuests.LostSO = userSituationDescriptionExtraQuest.LOSTSO;
                        extraQuests.HealthWage = userSituationDescriptionExtraQuest.HEALTHWage;
                        extraQuests.Unemployed = userSituationDescriptionExtraQuest.UNEmployed;
                        extraQuests.TaxLien = userSituationDescriptionExtraQuest.TAXLien;
                        extraQuests.MortRate = userSituationDescriptionExtraQuest.MORTRate;
                        extraQuests.AssistModify = userSituationDescriptionExtraQuest.ASSISTModify;
                        extraQuests.LoanModify = userSituationDescriptionExtraQuest.LOANModify;
                        // Hamp Questions
                        extraQuests.MortgageModifiedHamp = userSituationDescriptionExtraQuest.MortgageModifiedHamp;
                        extraQuests.MortgageInvestor = userSituationDescriptionExtraQuest.MortgageInvestor;
                        extraQuests.IsBusinessMortgage = userSituationDescriptionExtraQuest.IsBusinessOrPersonalMortgage;
                        extraQuests.HousingType = userSituationDescriptionExtraQuest.HousingType;
                        extraQuests.EscrowAccount = userSituationDescriptionExtraQuest.AgreeEscrowAccount;

                         // Hamp 2 tier Questions
                        extraQuests.ConvictedFelony = userSituationDescriptionExtraQuest.ConvictedFelony;
                        extraQuests.IsLoanFirstLien = userSituationDescriptionExtraQuest.IsLoanFirstLienFMorFMae;

                         // Harp Questions
                        extraQuests.LoanSoldFMorFMae = userSituationDescriptionExtraQuest.LoanSoldFMorFmae;
                        extraQuests.LoanRefinance = userSituationDescriptionExtraQuest.LoanRefinanceProgram;

                         // Short Sale/Deed in Lieu

                        extraQuests.AnyLiens = userSituationDescriptionExtraQuest.AnyLiens;

                         // Fp - Housing only questions
                        extraQuests.ClientGoals = userSituationDescriptionExtraQuest.ClientGoals;
                        extraQuests.TimeLineEvents = userSituationDescriptionExtraQuest.TimeLineEvents;
                        
                        Entities.SubmitChanges();

                        //UpdateResult.ClientNumber = Convert.ToInt32(ExtraQuest.client_number);
                        result.IsSuccessful = true;

                    }
                    else // Add New User 
                    {

                        //ExtraQuests
                        extraQuests = new ExtraQuest();
                        extraQuests.client_number = Convert.ToInt32(userSituationDescriptionExtraQuest.ClientNumber);
                        extraQuests.SecEvent = userSituationDescriptionExtraQuest.SECEvent;
                        extraQuests.MedExp = userSituationDescriptionExtraQuest.MEDExp;
                        extraQuests.LostSO = userSituationDescriptionExtraQuest.LOSTSO;
                        extraQuests.HealthWage = userSituationDescriptionExtraQuest.HEALTHWage;
                        extraQuests.Unemployed = userSituationDescriptionExtraQuest.UNEmployed;
                        extraQuests.TaxLien = userSituationDescriptionExtraQuest.TAXLien;
                        extraQuests.MortRate = userSituationDescriptionExtraQuest.MORTRate;

                        extraQuests.AssistModify = userSituationDescriptionExtraQuest.ASSISTModify;
                        extraQuests.LoanModify = userSituationDescriptionExtraQuest.LOANModify;
                        // Hamp Questions
                        extraQuests.MortgageModifiedHamp = userSituationDescriptionExtraQuest.MortgageModifiedHamp;
                        extraQuests.MortgageInvestor = userSituationDescriptionExtraQuest.MortgageInvestor;
                        extraQuests.IsBusinessMortgage = userSituationDescriptionExtraQuest.IsBusinessOrPersonalMortgage;
                        extraQuests.HousingType = userSituationDescriptionExtraQuest.HousingType;
                        extraQuests.EscrowAccount = userSituationDescriptionExtraQuest.AgreeEscrowAccount;

                        // Hamp 2 tier Questions
                        extraQuests.ConvictedFelony = userSituationDescriptionExtraQuest.ConvictedFelony;
                        extraQuests.IsLoanFirstLien = userSituationDescriptionExtraQuest.IsLoanFirstLienFMorFMae;

                        // Harp Questions
                        extraQuests.LoanSoldFMorFMae = userSituationDescriptionExtraQuest.LoanSoldFMorFmae;
                        extraQuests.LoanRefinance = userSituationDescriptionExtraQuest.LoanRefinanceProgram;

                        // Short Sale/Deed in Lieu

                        extraQuests.AnyLiens = userSituationDescriptionExtraQuest.AnyLiens;

                        // Fp - Housing only questions
                        extraQuests.ClientGoals = userSituationDescriptionExtraQuest.ClientGoals;
                        extraQuests.TimeLineEvents = userSituationDescriptionExtraQuest.TimeLineEvents;
                        Entities.ExtraQuests.InsertOnSubmit(extraQuests);
                        Entities.SubmitChanges();
                        // UpdateResult.ClientNumber = ExtraQuest;
                        result.IsSuccessful = true;
                    }
                }
                catch (Exception exception)
                {
                    result.IsSuccessful = false;
                    result.Exception = exception;
                    result.Messages = new string[] { "An error occured attempting to save." };
                }
                return result;
            }


        }

        #region User Describe Your Situation BKC

        public UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCAddUpdateSecEvent(UserDescribeYourSituationBKC userDescribeYourSituationBKC)
        {
            var result = new UserDecsribeYourSituationBKCResult();
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                try
                {
                    var extraQuests =
                     (
                         from entity in Entities.ExtraQuests
                         where entity.client_number == userDescribeYourSituationBKC.ClientNumber
                         select entity
                     ).FirstOrDefault();

                    if (extraQuests != null) //Update
                    {
                        extraQuests.SecEvent = userDescribeYourSituationBKC.SecEvent;
                        Entities.SubmitChanges();

                        result.IsSuccessful = true;

                    }
                    else // Add New User 
                    {
                        //ExtraQuests
                        extraQuests = new ExtraQuest();
                        extraQuests.client_number = Convert.ToInt32(userDescribeYourSituationBKC.ClientNumber);
                        if (userDescribeYourSituationBKC.SecEvent != "")
                            extraQuests.SecEvent = userDescribeYourSituationBKC.SecEvent;
                        extraQuests.MedExp = userDescribeYourSituationBKC.MedExp;
                        extraQuests.LostSO = userDescribeYourSituationBKC.LostSO;
                        extraQuests.HealthWage = userDescribeYourSituationBKC.HealthWage;
                        extraQuests.Unemployed = userDescribeYourSituationBKC.Unemployed;
                        extraQuests.TaxLien = userDescribeYourSituationBKC.TaxLien;
                        extraQuests.MortRate = userDescribeYourSituationBKC.MortRate;

                        Entities.ExtraQuests.InsertOnSubmit(extraQuests);
                        Entities.SubmitChanges();

                        result.IsSuccessful = true;
                    }
                }
                catch (Exception exception)
                {
                    result.IsSuccessful = false;
                    result.Exception = exception;
                    result.Messages = new string[] { "An error occured attempting to save." };
                }
            }

            return result;
        }

        public UserDecsribeYourSituationBKCResult UserDescribeYourSituationBKCExtraQuestAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC)
        {
            ExtraQuest extraQuests = null;

            UserDecsribeYourSituationBKCResult result = new UserDecsribeYourSituationBKCResult();
            string Message = string.Empty;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                try
                {

                    extraQuests =
                     (
                         from entity in Entities.ExtraQuests
                         where entity.client_number == userDescribeYourSituationBKC.ClientNumber
                         select entity
                     ).FirstOrDefault<ExtraQuest>();
                    if (extraQuests != null) //Update
                    {
                        extraQuests.SecEvent = userDescribeYourSituationBKC.SecEvent;
                        if (userDescribeYourSituationBKC.SecEvent != "") extraQuests.SecEvent = userDescribeYourSituationBKC.SecEvent;
                        if (userDescribeYourSituationBKC.MedExp != null) extraQuests.MedExp = userDescribeYourSituationBKC.MedExp;
                        if (userDescribeYourSituationBKC.LostSO != null) extraQuests.LostSO = userDescribeYourSituationBKC.LostSO;
                        if (userDescribeYourSituationBKC.HealthWage != null) extraQuests.HealthWage = userDescribeYourSituationBKC.HealthWage;
                        if (userDescribeYourSituationBKC.Unemployed != null) extraQuests.Unemployed = userDescribeYourSituationBKC.Unemployed;
                        if (userDescribeYourSituationBKC.TaxLien != null) extraQuests.TaxLien = userDescribeYourSituationBKC.TaxLien;
                        if (userDescribeYourSituationBKC.MortRate != null) extraQuests.MortRate = userDescribeYourSituationBKC.MortRate.ToString();
                        Entities.SubmitChanges();

                        result.IsSuccessful = true;

                    }
                    else // Add New User 
                    {

                        //ExtraQuests
                        extraQuests = new ExtraQuest();
                        extraQuests.client_number = Convert.ToInt32(userDescribeYourSituationBKC.ClientNumber);
                        if (userDescribeYourSituationBKC.SecEvent != "") extraQuests.SecEvent = userDescribeYourSituationBKC.SecEvent;
                        extraQuests.MedExp = userDescribeYourSituationBKC.MedExp;
                        extraQuests.LostSO = userDescribeYourSituationBKC.LostSO;
                        extraQuests.HealthWage = userDescribeYourSituationBKC.HealthWage;
                        extraQuests.Unemployed = userDescribeYourSituationBKC.Unemployed;
                        extraQuests.TaxLien = userDescribeYourSituationBKC.TaxLien;
                        extraQuests.MortRate = userDescribeYourSituationBKC.MortRate.ToString();
                        Entities.ExtraQuests.InsertOnSubmit(extraQuests);
                        Entities.SubmitChanges();
                        // UpdateResult.ClientNumber = ExtraQuest;
                        result.IsSuccessful = true;
                    }
                }
                catch (Exception exception)
                {
                    result.IsSuccessful = false;
                    result.Exception = exception;
                    result.Messages = new string[] { "An error occured attempting to save." };
                }
                return result;
            }


        }

        public UserDescribeYourSituationBKC BKCUserDescribeYourSituationExtraQuestGet(int ClientNumber)
        {
            UserDescribeYourSituationBKC UserContactDetail = null;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {
                UserContactDetail =
                 (
                     from entity in Entities.ExtraQuests
                     where entity.client_number == ClientNumber
                     select new UserDescribeYourSituationBKC
                     {
                         ClientNumber = Convert.ToInt32(entity.client_number),
                         SecEvent = (entity.SecEvent != null) ? entity.SecEvent : string.Empty,
                         MedExp = entity.MedExp,
                         LostSO = entity.LostSO,
                         HealthWage = entity.HealthWage,
                         Unemployed = entity.Unemployed,
                         TaxLien = entity.TaxLien,
                         MortRate = entity.MortRate,
                     }
                     ).FirstOrDefault();
            }
            return UserContactDetail;
        }

        #endregion User Describe Your Situation BKC

    }

        #endregion

}

