﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Credability.Impl;

namespace Cccs.Credability.Impl
{
	public partial class Credability
	{
		public ClientTypeTimeStampResult ClienttypeTimeStampAddUpdate(ClientTimeTracking ClienttypeTimeStamp)
		{
			contactdetail Contactdetail = null;

			ClientTypeTimeStampResult Result = new ClientTypeTimeStampResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{

				Contactdetail =
				(
					from contat in Entities.contactdetails
					where contat.client_number == ClienttypeTimeStamp.ClientNumber
					select contat
				).FirstOrDefault<contactdetail>();
				if(Contactdetail != null) //Update
				{
					try
					{
						Contactdetail.CurDTS = DateTime.Now;
						Contactdetail.TotMin = (Contactdetail.TotMin.HasValue ? Contactdetail.TotMin.Value: 0) + ClienttypeTimeStamp.TotalMinuteOnPage;
						if(!ClienttypeTimeStamp.ConfirmIpAddress.IsNullOrWhiteSpace())
						{
							Contactdetail.confirm_ipaddress = ClienttypeTimeStamp.ConfirmIpAddress;
						}

						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
				else
				{
					Result.IsSuccessful = false;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
			}
			return Result;
		}
	}
}