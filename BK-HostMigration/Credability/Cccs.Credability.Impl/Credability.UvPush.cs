﻿using System;
using System.Collections.Generic;
using Cccs.Credability.Dal;
//using Cccs.Host.Dto;
using Cccs.Debtplus.Data;

namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public DebtplusPushData GetContactDetailForUv(int InNo)
        {
            DebtplusPushData UvData = new DebtplusPushData();
            UvData.PushClient = false;
            using (CredabilityDataContext CredAbility = new CredabilityDataContext())
            {
                var HoldUvData = CredAbility.ColectUvData(InNo);
                foreach (ColectUvDataResult CurUvData in HoldUvData)
                {
                    if (CurUvData.client_number == InNo)
                    {
                        UvData.InNo = CurUvData.client_number;
                        UvData.ConfirmDateTime = (CurUvData.confirm_datetime != null) ? (DateTime)CurUvData.confirm_datetime : DateTime.Now;
                        UvData.Processed = (CurUvData.processed != null) ? (int)CurUvData.processed : 0;
                        UvData.Addr1 = (CurUvData.contact_address != null) ? CurUvData.contact_address.Trim() : String.Empty;
                        UvData.Addr2 = (CurUvData.contact_address2 != null) ? CurUvData.contact_address2.Trim() : String.Empty;
                        UvData.City = (CurUvData.contact_city != null) ? CurUvData.contact_city.Trim() : String.Empty;
                        UvData.State = (CurUvData.contact_state != null) ? CurUvData.contact_state.Trim() : String.Empty;
                        UvData.Zip = (CurUvData.contact_zip != null) ? CurUvData.contact_zip.Trim() : String.Empty;
                        UvData.Phone1 = (CurUvData.contact_telephone != null) ? CurUvData.contact_telephone.Trim() : String.Empty;
                        UvData.Phone2 = (CurUvData.contact_other_telephone != null) ? CurUvData.contact_other_telephone.Trim() : String.Empty;
                        UvData.eMail = (CurUvData.contact_email != null) ? CurUvData.contact_email.Trim() : String.Empty;

                        UvData.PriFirstName = (CurUvData.contact_firstname != null) ? CurUvData.contact_firstname.Trim() : String.Empty;
                        UvData.PriMidName = (CurUvData.contact_initial != null) ? CurUvData.contact_initial.Trim() : String.Empty;
                        UvData.PriLastName = (CurUvData.contact_lastname != null) ? CurUvData.contact_lastname.Trim() : String.Empty;
                        UvData.PriSsn = (CurUvData.contact_ssn != null) ? CurUvData.contact_ssn.Trim() : String.Empty;
                        UvData.PriConfirmMaiden = (CurUvData.confirm_maidenname != null) ? CurUvData.confirm_maidenname.Trim() : String.Empty;
                        UvData.PriMarital = (CurUvData.contact_marital != null) ? CurUvData.contact_marital.Trim() : String.Empty;
                        UvData.PriDob = (CurUvData.contact_dob != null) ? CurUvData.contact_dob.Trim() : String.Empty;
                        UvData.PriSex = (CurUvData.contact_sex != null) ? CurUvData.contact_sex.Trim() : String.Empty;
                        UvData.PriRace = (CurUvData.contact_race != null) ? CurUvData.contact_race.Trim() : String.Empty;
                        UvData.PriHispanic = (CurUvData.contact_hispanic != null) ? CurUvData.contact_hispanic.Trim() : String.Empty;
                        UvData.PriBirthCity = (CurUvData.cred_rpt_bcity != null) ? CurUvData.cred_rpt_bcity.Trim() : String.Empty;
                        UvData.PriEmployed = (CurUvData.contact_Employed != null) ? CurUvData.contact_Employed.Trim() : String.Empty;
                        UvData.PriEducation = (CurUvData.contact_Education != null) ? CurUvData.contact_Education.Trim() : String.Empty;

                        UvData.SecFirstName = (CurUvData.cosign_firstname != null) ? CurUvData.cosign_firstname.Trim() : String.Empty;
                        UvData.SecMidName = (CurUvData.cosign_initial != null) ? CurUvData.cosign_initial.Trim() : String.Empty;
                        UvData.SecLastName = (CurUvData.cosign_lastname != null) ? CurUvData.cosign_lastname.Trim() : String.Empty;
                        UvData.SecSsn = (CurUvData.cosign_ssn != null) ? CurUvData.cosign_ssn.Trim() : String.Empty;
                        UvData.SecConfirmMaiden = (CurUvData.confirm_maidenname2 != null) ? CurUvData.confirm_maidenname2.Trim() : String.Empty;
                        UvData.SecMarital = (CurUvData.cosign_marital != null) ? CurUvData.cosign_marital.Trim() : String.Empty;
                        UvData.SecDob = (CurUvData.cosign_dob != null) ? CurUvData.cosign_dob.Trim() : String.Empty;
                        UvData.SecSex = (CurUvData.cosign_sex != null) ? CurUvData.cosign_sex.Trim() : String.Empty;
                        UvData.SecRace = (CurUvData.cosign_race != null) ? CurUvData.cosign_race.Trim() : String.Empty;
                        UvData.SecHispanic = (CurUvData.cosign_hispanic != null) ? CurUvData.cosign_hispanic.Trim() : String.Empty;
                        UvData.SecBirthCity = (CurUvData.cred_rpt_bcity2 != null) ? CurUvData.cred_rpt_bcity2.Trim() : String.Empty;
                        UvData.SecEmployed = (CurUvData.cosign_Employed != null) ? CurUvData.cosign_Employed.Trim() : String.Empty;

                        //Payment
                        UvData.PaymentType = (CurUvData.payment_type != null) ? CurUvData.payment_type.Trim() : String.Empty;
                        UvData.DebitCardFirstName = (CurUvData.dc_fnameoncard != null) ? CurUvData.dc_fnameoncard.Trim() : String.Empty;
                        UvData.DebitCardMidName = (CurUvData.dc_mnameoncard != null) ? CurUvData.dc_mnameoncard.Trim() : String.Empty;
                        UvData.DebitCardLastName = (CurUvData.dc_lnameoncard != null) ? CurUvData.dc_lnameoncard.Trim() : String.Empty;
                        UvData.DebitCardBillAddr1 = (CurUvData.dc_bill_addr != null) ? CurUvData.dc_bill_addr.Trim() : String.Empty;
                        UvData.DebitCardBillAddr2 = (CurUvData.dc_bill_addr2 != null) ? CurUvData.dc_bill_addr2.Trim() : String.Empty;
                        UvData.DebitCardBillCity = (CurUvData.dc_bill_city != null) ? CurUvData.dc_bill_city.Trim() : String.Empty;
                        UvData.DebitCardBillState = (CurUvData.dc_bill_state != null) ? CurUvData.dc_bill_state.Trim() : String.Empty;
                        UvData.DebitCardBillZip = (CurUvData.dc_bill_zip != null) ? CurUvData.dc_bill_zip.Trim() : String.Empty;
                        UvData.DebitCardAcctNo = (CurUvData.dc_acctnum != null) ? CurUvData.dc_acctnum.Trim() : String.Empty;
                        UvData.DebitCardExpDate = (CurUvData.dc_exp_date != null) ? CurUvData.dc_exp_date.Trim() : String.Empty;
                        UvData.DebitCardTransDTS = CurUvData.TransDTS;
                        UvData.DebitCardVanTransRef = (CurUvData.VanTransRef != null) ? CurUvData.VanTransRef.Trim() : String.Empty;
                        UvData.BankAbaNo = (CurUvData.abanumber != null) ? CurUvData.abanumber.Trim() : String.Empty;
                        UvData.BankAcctNo = (CurUvData.acctnumber != null) ? CurUvData.acctnumber.Trim() : String.Empty;
                        UvData.MTCN = (CurUvData.mtcn != null) ? CurUvData.mtcn.Trim() : String.Empty;

                        //Net Worth
                        UvData.ValHome = (CurUvData.val_home != null) ? (float)CurUvData.val_home : 0;
                        UvData.ValCar = (CurUvData.val_car != null) ? (float)CurUvData.val_car : 0;
                        UvData.ValProperty = (CurUvData.val_prop != null) ? (float)CurUvData.val_prop : 0;
                        UvData.ValRetirement = (CurUvData.val_ret != null) ? (float)CurUvData.val_ret : 0;
                        UvData.ValSavings = (CurUvData.val_sav != null) ? (float)CurUvData.val_sav : 0;
                        UvData.ValOther = (CurUvData.val_other != null) ? (float)CurUvData.val_other : 0;
                        UvData.OweHome = (CurUvData.owe_home != null) ? (float)CurUvData.owe_home : 0;
                        UvData.OweCar = (CurUvData.owe_car != null) ? (float)CurUvData.owe_car : 0;
                        UvData.OweOther = (CurUvData.owe_other != null) ? (float)CurUvData.owe_other : 0;

                        //Monthly Food & Shelter
                        UvData.RentMort = (CurUvData.rent_mort != null) ? (decimal)CurUvData.rent_mort : 0;
                        UvData.EquityLoanTaxIns = (CurUvData.equity_loan_tax_ins != null) ? (decimal)CurUvData.equity_loan_tax_ins : 0;
                        UvData.MoEquity = (CurUvData.MoEquity != null) ? (float)CurUvData.MoEquity : 0;
                        UvData.Mo2ndMort = (CurUvData.Mo2ndMort != null) ? (float)CurUvData.Mo2ndMort : 0;
                        UvData.AnPropIns = (CurUvData.AnPropIns != null) ? (float)CurUvData.AnPropIns : 0;
                        UvData.AnPropTax = (CurUvData.AnPropTax != null) ? (float)CurUvData.AnPropTax : 0;
                        UvData.MoFee = (CurUvData.MoFee != null) ? (float)CurUvData.MoFee : 0;
                        UvData.HomeMaintenance = (CurUvData.home_maintenance != null) ? (decimal)CurUvData.home_maintenance : 0;

                        UvData.Utilities = (CurUvData.utilities != null) ? (decimal)CurUvData.utilities : 0;

                        UvData.UtlElectric = (CurUvData.UtlElectric != null) ? (float)CurUvData.UtlElectric : 0;
                        UvData.UtlWater = (CurUvData.UtlWater != null) ? (float)CurUvData.UtlWater : 0;
                        UvData.UtlGas = (CurUvData.UtlGas != null) ? (float)CurUvData.UtlGas : 0;
                        UvData.UtlTv = (CurUvData.UtlTv != null) ? (float)CurUvData.UtlTv : 0;
                        UvData.UtlTrash = (CurUvData.UtlTrash != null) ? (float)CurUvData.UtlTrash : 0;
                        UvData.Telephone = (CurUvData.telephone != null) ? (decimal)CurUvData.telephone : 0;
                        UvData.FoodAway = (CurUvData.food_away != null) ? (decimal)CurUvData.food_away : 0;
                        UvData.Groceries = (CurUvData.groceries != null) ? (decimal)CurUvData.groceries : 0;
                        //Monthly Transportation
                        UvData.CarPayments = (CurUvData.vehicle_payments != null) ? (decimal)CurUvData.vehicle_payments : 0;
                        UvData.CarInsurance = (CurUvData.car_insurance != null) ? (decimal)CurUvData.car_insurance : 0;
                        UvData.CarMaintenance = (CurUvData.car_maintenance != null) ? (decimal)CurUvData.car_maintenance : 0;
                        UvData.PublicTransportation = (CurUvData.public_transportation != null) ? (decimal)CurUvData.public_transportation : 0;
                        //Monthly Insurance, Medical & Childcare
                        UvData.MedicalInsurance = (CurUvData.insurance != null) ? (decimal)CurUvData.insurance : 0;
                        UvData.MedicalPrescription = (CurUvData.medical_prescription != null) ? (decimal)CurUvData.medical_prescription : 0;
                        UvData.ChildSupportAlimony = (CurUvData.child_support != null) ? (decimal)CurUvData.child_support : 0;
                        UvData.ChildElderCare = (CurUvData.child_elder_care != null) ? (decimal)CurUvData.child_elder_care : 0;
                        UvData.Education = (CurUvData.education != null) ? (decimal)CurUvData.education : 0;
                        //Monthly Personal & Miscellaneous Expenses
                        UvData.CardPayments = (CurUvData.CardPayments != null) ? (int)CurUvData.CardPayments : 0;
                        UvData.OtherLoans = (CurUvData.OtherLoans != null) ? (int)CurUvData.OtherLoans : 0;
                        UvData.Contributions = (CurUvData.contributions != null) ? (decimal)CurUvData.contributions : 0;
                        UvData.Clothing = (CurUvData.clothing != null) ? (decimal)CurUvData.clothing : 0;
                        UvData.Laundry = (CurUvData.laundry != null) ? (decimal)CurUvData.laundry : 0;
                        UvData.PersonalExpenses = (CurUvData.personal_exp != null) ? (decimal)CurUvData.personal_exp : 0;
                        UvData.BeautyBarber = (CurUvData.beauty_barber != null) ? (decimal)CurUvData.beauty_barber : 0;
                        UvData.Recreation = (CurUvData.recreation != null) ? (decimal)CurUvData.recreation : 0;
                        UvData.ClubDues = (CurUvData.club_dues != null) ? (decimal)CurUvData.club_dues : 0;
                        UvData.Gifts = (CurUvData.gifts != null) ? (decimal)CurUvData.gifts : 0;
                        UvData.Miscellaneous = (CurUvData.misc_exp != null) ? (decimal)CurUvData.misc_exp : 0;
                        //Income
                        UvData.GrossIncome = (CurUvData.monthly_gross_income != null) ? (float)CurUvData.monthly_gross_income : 0;
                        UvData.GrossIncome2 = (CurUvData.monthly_gross_income2 != null) ? (float)CurUvData.monthly_gross_income2 : 0;
                        UvData.NetIncome = (CurUvData.monthly_net_income != null) ? (float)CurUvData.monthly_net_income : 0;
                        UvData.NetIncome2 = (CurUvData.monthly_net_income2 != null) ? (float)CurUvData.monthly_net_income2 : 0;
                        UvData.PartTimeGrossIncome = (CurUvData.monthly_parttime_income != null) ? (float)CurUvData.monthly_parttime_income : 0;
                        UvData.PartTimeGrossIncome2 = (CurUvData.monthly_parttime_income2 != null) ? (float)CurUvData.monthly_parttime_income2 : 0;
                        UvData.PartTimeNetIncome = (CurUvData.MonthlyPartNet != null) ? (float)CurUvData.MonthlyPartNet : 0;
                        UvData.PartTimeNetIncome2 = (CurUvData.MonthlyPartNet2 != null) ? (float)CurUvData.MonthlyPartNet2 : 0;
                        UvData.AlimonyIncome = (CurUvData.monthly_alimony_income != null) ? (float)CurUvData.monthly_alimony_income : 0;
                        UvData.AlimonyIncome2 = (CurUvData.monthly_alimony_income2 != null) ? (float)CurUvData.monthly_alimony_income2 : 0;
                        UvData.ChildSupportIncome = (CurUvData.monthly_childsupport_income != null) ? (float)CurUvData.monthly_childsupport_income : 0;
                        UvData.ChildSupportIncome2 = (CurUvData.monthly_childsupport_income2 != null) ? (float)CurUvData.monthly_childsupport_income2 : 0;
                        UvData.GovtAssistIncome = (CurUvData.monthly_govtassist_income != null) ? (float)CurUvData.monthly_govtassist_income : 0;
                        UvData.GovtAssistIncome2 = (CurUvData.monthly_govtassist_income2 != null) ? (float)CurUvData.monthly_govtassist_income2 : 0;
                        UvData.OtherIncome = (CurUvData.monthly_other_income != null) ? (float)CurUvData.monthly_other_income : 0;
                        UvData.OtherIncome2 = (CurUvData.monthly_other_income2 != null) ? (float)CurUvData.monthly_other_income2 : 0;
                        //Housing
                        UvData.HpfID = CurUvData.HpfID;
                        UvData.HousingType = (CurUvData.housing_type != null) ? CurUvData.housing_type.ToString() : String.Empty;
                        UvData.MosDelinq = (CurUvData.mos_delinq != null) ? (float)CurUvData.mos_delinq : 0;
                        UvData.MortCurrent = CurUvData.mort_current;
                        UvData.MortHolder = (CurUvData.mort_holder != null) ? CurUvData.mort_holder.Trim() : String.Empty;
                        UvData.MortLoanNo = (CurUvData.loan_number != null) ? CurUvData.loan_number.Trim() : String.Empty;
                        UvData.MortIntrestRate = (CurUvData.mort_rate != null) ? (float)CurUvData.mort_rate : 0;
                        UvData.MortType = (CurUvData.mort_type != null) ? CurUvData.mort_type.Trim() : String.Empty;
                        UvData.MortOrignalBal = (CurUvData.orig_bal != null) ? (float)CurUvData.orig_bal : 0;
                        UvData.MortRateType = (CurUvData.rate_type != null) ? CurUvData.rate_type.Trim() : String.Empty;
                        UvData.MortDate = (CurUvData.mort_date != null) ? CurUvData.mort_date.Trim() : String.Empty;
                        UvData.MortYears = (CurUvData.mort_years != null) ? (float)CurUvData.mort_years : 0;
                        UvData.MortAmtAvail = (CurUvData.amt_avail != null) ? (float)CurUvData.amt_avail : 0;
                        UvData.MortLastContactDate = (CurUvData.last_contact_date != null) ? CurUvData.last_contact_date.Trim() : String.Empty;
                        UvData.MortLastContactDesc = (CurUvData.last_contact_desc != null) ? CurUvData.last_contact_desc.Trim() : String.Empty;
                        UvData.MakeHomeAff = (CurUvData.MakeHomeAff != null) ? CurUvData.MakeHomeAff.Trim() : String.Empty;
                        UvData.WhoInHouse = (CurUvData.WhoInHouse != null) ? CurUvData.WhoInHouse.Trim() : String.Empty;
                        UvData.Note4Close = (CurUvData.Note4Close != null) ? CurUvData.Note4Close.Trim() : String.Empty;
                        UvData.Prop4Sale = (CurUvData.Prop4Sale != null) ? CurUvData.Prop4Sale.Trim() : String.Empty;
                        UvData.RepayPlan = (CurUvData.repay_plan != null) ? CurUvData.repay_plan.Trim() : String.Empty;
                        UvData.IncludeFha = (CurUvData.IncludeFha != null) ? CurUvData.IncludeFha.Trim() : String.Empty;
                        UvData.IncludeIns = (CurUvData.IncludeIns != null) ? CurUvData.IncludeIns.Trim() : String.Empty;
                        UvData.IncludeTax = (CurUvData.IncludeTax != null) ? CurUvData.IncludeTax.Trim() : String.Empty;
                        UvData.StartIntRate = (CurUvData.StartIntRate != null) ? (float)CurUvData.StartIntRate : 0;
                        UvData.TopIntRate = (CurUvData.TopIntRate != null) ? (float)CurUvData.TopIntRate : 0;
                        UvData.MortSecHolder = (CurUvData.secondary_holder != null) ? CurUvData.secondary_holder.Trim() : String.Empty;
                        UvData.MortSecLoanNo = (CurUvData.loan_number2 != null) ? CurUvData.loan_number2.Trim() : String.Empty;
                        UvData.MortSecAmt = (CurUvData.secondary_amt != null) ? (float)CurUvData.secondary_amt : 0;
                        UvData.MortSecStatus = (CurUvData.secondary_status != null) ? CurUvData.secondary_status.Trim() : String.Empty;
                        UvData.ForeclosureDate = (CurUvData.foreclosure_date != null) ? CurUvData.foreclosure_date.Trim() : String.Empty;

                        UvData.RefCode = (CurUvData.refcode != null) ? CurUvData.refcode.Trim() : String.Empty;
                        UvData.BkForPrv = (CurUvData.BkForPrv != null) ? (int)CurUvData.BkForPrv : 0;
                        UvData.SpanishFlag = (CurUvData.span_flag != null) ? CurUvData.span_flag.Trim() : String.Empty;
                        UvData.ClientType = (CurUvData.ClientType != null) ? CurUvData.ClientType.Trim() : String.Empty;
                        UvData.CcrcAprov = (CurUvData.CcrcAprov != null) ? CurUvData.CcrcAprov.Trim() : String.Empty;
                        UvData.SizeOfHousehold = (CurUvData.size_of_household != null) ? (int)CurUvData.size_of_household : 0;
                        UvData.DmpFcoPcpFlag = (CurUvData.dmp_fco_pcp_flag != null) ? CurUvData.dmp_fco_pcp_flag.Trim() : String.Empty;
                        UvData.DmpFcoPcpDate = (CurUvData.dmp_fco_pcp_date != null) ? CurUvData.dmp_fco_pcp_date.Trim() : String.Empty;
                        UvData.CredRptPull = (CurUvData.cred_rpt_pull != null) ? CurUvData.cred_rpt_pull.ToString() : String.Empty;
                        UvData.ContactReason = (CurUvData.contact_reason != null) ? CurUvData.contact_reason.Trim() : String.Empty;
                        UvData.ContactReason2 = (CurUvData.SecEvent != null) ? CurUvData.SecEvent.Trim() : String.Empty;
                        UvData.Bankrupt = (CurUvData.bankrupt != null) ? CurUvData.bankrupt.Trim() : String.Empty;
                        UvData.CoCounsPres = (CurUvData.CoCounsPres != null) ? CurUvData.CoCounsPres.Trim() : String.Empty;
                        UvData.PriorCouns = (CurUvData.PriorCouns != null) ? CurUvData.PriorCouns.Trim() : String.Empty;
                        UvData.PriorDMP = (CurUvData.PriorDMP != null) ? CurUvData.PriorDMP.Trim() : String.Empty;
                        UvData.RecentDMP = (CurUvData.RecentDMP != null) ? CurUvData.RecentDMP.Trim() : String.Empty;
                        UvData.MedExp = (CurUvData.MedExp != null) ? CurUvData.MedExp.Trim() : String.Empty;
                        UvData.LostSO = (CurUvData.LostSO != null) ? CurUvData.LostSO.Trim() : String.Empty;
                        UvData.HealthWage = (CurUvData.HealthWage != null) ? CurUvData.HealthWage.Trim() : String.Empty;
                        UvData.Unemployed = (CurUvData.Unemployed != null) ? CurUvData.Unemployed.Trim() : String.Empty;
                        UvData.TaxLien = (CurUvData.TaxLien != null) ? CurUvData.TaxLien.Trim() : String.Empty;
                        UvData.MortRate = (CurUvData.MortRate != null) ? CurUvData.MortRate.Trim() : String.Empty;
                        UvData.GovGrant = (CurUvData.GovGrant != null) ? CurUvData.GovGrant.Trim() : String.Empty;
                        UvData.ContactComments = (CurUvData.contact_comments != null) ? CurUvData.contact_comments.Trim() : String.Empty;
                        UvData.CreditorComments = (CurUvData.creditor_comments != null) ? CurUvData.creditor_comments.Trim() : String.Empty;
                        UvData.BsrpStAlowed = (CurUvData.Alowed != null) ? (bool)CurUvData.Alowed : false;
                        UvData.LoanModify = (CurUvData.LoanModify != null) ? CurUvData.LoanModify.Trim() : String.Empty;
                        UvData.AssistModify = (CurUvData.AssistModify != null) ? CurUvData.AssistModify.Trim() : String.Empty;
                        /* Harp Hamp questions */


                        UvData.MortgageModifiedHamp = (CurUvData.MortgageModifiedHamp != null) ? CurUvData.MortgageModifiedHamp.Trim() : String.Empty;
                        UvData.MortgageInvestor = (CurUvData.MortgageInvestor != null) ? CurUvData.MortgageInvestor.Trim() : String.Empty;
                        UvData.HouseType = (CurUvData.HousingType != null) ? CurUvData.HousingType.Trim() : String.Empty;
                        UvData.AgreeEscrowAccount = (CurUvData.EscrowAccount != null) ? CurUvData.EscrowAccount.Trim() : String.Empty;
                      
                        UvData.ConvictedFelony = (CurUvData.ConvictedFelony != null) ? CurUvData.ConvictedFelony.Trim() : String.Empty;
                        UvData.IsLoanFirstLienFMorFMae = (CurUvData.IsLoanFirstLien != null) ? CurUvData.IsLoanFirstLien.Trim() : String.Empty;
                        UvData.IsBusinessOrPersonalMortgage = (CurUvData.IsBusinessMortgage != null) ? CurUvData.IsBusinessMortgage.Trim() : string.Empty;

                        UvData.LoanSoldFMorFmae = (CurUvData.LoanSoldFMorFMae != null) ? CurUvData.LoanSoldFMorFMae.Trim() : String.Empty;
                        UvData.LoanRefinanceProgram = (CurUvData.LoanRefinance != null) ? CurUvData.LoanRefinance.Trim() : String.Empty;

                        UvData.AnyLiens = (CurUvData.AnyLiens != null) ? CurUvData.AnyLiens.Trim() : String.Empty;
                        UvData.ActiveBankruptcy = (CurUvData.ActiveBankruptcy != null) ? CurUvData.ActiveBankruptcy.Trim() : String.Empty;


                        /*end harp Hamp Questions */

                        UvData.ConThem = (CurUvData.ConThem != null) ? CurUvData.ConThem.Trim() : String.Empty;
                        UvData.ConMeth = (CurUvData.ConMeth != null) ? CurUvData.ConMeth.Trim() : String.Empty;
                        UvData.ConInfo = (CurUvData.ConInfo != null) ? CurUvData.ConInfo.Trim() : String.Empty;
                        UvData.ConTime = (CurUvData.ConTime != null) ? CurUvData.ConTime.Trim() : String.Empty;
                        UvData.ConDay = (CurUvData.ConDay != null) ? CurUvData.ConDay.Trim() : String.Empty;

                        UvData.AttySit = (CurUvData.AttySit != null) ? CurUvData.AttySit.Trim() : String.Empty;
                        UvData.FirmCode = (CurUvData.firm_id != null) ? CurUvData.firm_id.Trim() : String.Empty;
                        UvData.FirmName = (CurUvData.firm_name != null) ? CurUvData.firm_name.Trim() : String.Empty;
                        UvData.AttyName = (CurUvData.atty_name != null) ? CurUvData.atty_name.Trim() : String.Empty;
                        UvData.FirmEmail = (CurUvData.atty_email != null) ? CurUvData.atty_email.Trim() : String.Empty;

                        UvData.NewUtlTv = (CurUvData.NewUtlTv != null) ? (float)CurUvData.NewUtlTv : 0;
                        UvData.NewUtlGas = (CurUvData.NewUtlGas != null) ? (float)CurUvData.NewUtlGas : 0;
                        UvData.NewUtlWater = (CurUvData.NewUtlWater != null) ? (float)CurUvData.NewUtlWater : 0;
                        UvData.NewUtlElectric = (CurUvData.NewUtlElectric != null) ? (float)CurUvData.NewUtlElectric : 0;
                        UvData.NewUtlTrash = (CurUvData.NewUtlTrash != null) ? (float)CurUvData.NewUtlTrash : 0;
                        UvData.NewMoFee = (CurUvData.NewMoFee != null) ? (float)CurUvData.NewMoFee : 0;
                        UvData.NewMoPropIns = (CurUvData.NewMoPropIns != null) ? (float)CurUvData.NewMoPropIns : 0;
                        UvData.NewMoPropTax = (CurUvData.NewMoPropTax != null) ? (float)CurUvData.NewMoPropTax : 0;
                        UvData.NewMo2ndMort = (CurUvData.NewMo2ndMort != null) ? (float)CurUvData.NewMo2ndMort : 0;
                        UvData.NewMoEquity = (CurUvData.NewMoEquity != null) ? (float)CurUvData.NewMoEquity : 0;
                        UvData.NewCardPayments = (CurUvData.NewCardPayments != null) ? (float)CurUvData.NewCardPayments : 0;
                        UvData.NewOtherLoans = (CurUvData.NewOtherLoans != null) ? (float)CurUvData.NewOtherLoans : 0;
                        UvData.NewMiscExp = (CurUvData.NewMiscExp != null) ? (float)CurUvData.NewMiscExp : 0;
                        UvData.NewGifts = (CurUvData.NewGifts != null) ? (float)CurUvData.NewGifts : 0;
                        UvData.NewContributions = (CurUvData.NewContributions != null) ? (float)CurUvData.NewContributions : 0;
                        UvData.NewPublicTrans = (CurUvData.NewPublicTrans != null) ? (float)CurUvData.NewPublicTrans : 0;
                        UvData.NewClubDues = (CurUvData.NewClubDues != null) ? (float)CurUvData.NewClubDues : 0;
                        UvData.NewCarMaint = (CurUvData.NewCarMaint != null) ? (float)CurUvData.NewCarMaint : 0;
                        UvData.NewCarIns = (CurUvData.NewCarIns != null) ? (float)CurUvData.NewCarIns : 0;
                        UvData.NewVehiclePay = (CurUvData.NewVehiclePay != null) ? (float)CurUvData.NewVehiclePay : 0;
                        UvData.NewBeauty = (CurUvData.NewBeauty != null) ? (float)CurUvData.NewBeauty : 0;
                        UvData.NewPerdonalExp = (CurUvData.NewPerdonalExp != null) ? (float)CurUvData.NewPerdonalExp : 0;
                        UvData.NewElderCare = (CurUvData.NewElderCare != null) ? (float)CurUvData.NewElderCare : 0;
                        UvData.NewClothing = (CurUvData.NewClothing != null) ? (float)CurUvData.NewClothing : 0;
                        UvData.NewLaundry = (CurUvData.NewLaundry != null) ? (float)CurUvData.NewLaundry : 0;
                        UvData.NewChildSupport = (CurUvData.NewChildSupport != null) ? (float)CurUvData.NewChildSupport : 0;
                        UvData.NewEdu = (CurUvData.NewEdu != null) ? (float)CurUvData.NewEdu : 0;
                        UvData.NewMed = (CurUvData.NewMed != null) ? (float)CurUvData.NewMed : 0;
                        UvData.NewRecreation = (CurUvData.NewRecreation != null) ? (float)CurUvData.NewRecreation : 0;
                        UvData.NewInsurance = (CurUvData.NewInsurance != null) ? (float)CurUvData.NewInsurance : 0;
                        UvData.NewFoodAway = (CurUvData.NewFoodAway != null) ? (float)CurUvData.NewFoodAway : 0;
                        UvData.NewGroceries = (CurUvData.NewGroceries != null) ? (float)CurUvData.NewGroceries : 0;
                        UvData.NewPhone = (CurUvData.NewPhone != null) ? (float)CurUvData.NewPhone : 0;
                        UvData.NewMaint = (CurUvData.NewMaint != null) ? (float)CurUvData.NewMaint : 0;
                        UvData.NewMort = (CurUvData.NewMort != null) ? (float)CurUvData.NewMort : 0;
                        UvData.BegPro = (CurUvData.BegPro != null) ? (bool)CurUvData.BegPro : false;
                        UvData.AstPay = (CurUvData.AstPay != null) ? (bool)CurUvData.AstPay : false;
                        UvData.BenAprov = (CurUvData.BenAprov != null) ? (bool)CurUvData.BenAprov : false;
                        UvData.HaveCloDate = (CurUvData.HaveCloDate != null) ? (bool)CurUvData.HaveCloDate : false;
                        UvData.TheCloDate = CurUvData.TheCloDate;
                        UvData.HousePrice = (CurUvData.HousePrice != null) ? (float)CurUvData.HousePrice : 0;
                        UvData.HouseAdr = (CurUvData.HouseAdr != null) ? CurUvData.HouseAdr.Trim() : String.Empty;
                        UvData.HouseCity = (CurUvData.HouseCity != null) ? CurUvData.HouseCity.Trim() : String.Empty;
                        UvData.HouseSt = (CurUvData.HouseSt != null) ? CurUvData.HouseSt.Trim() : String.Empty;
                        UvData.HouseZip = (CurUvData.HouseZip != null) ? CurUvData.HouseZip.Trim() : String.Empty;
                        UvData.FndHouse = (CurUvData.FndHouse != null) ? CurUvData.FndHouse.Trim() : String.Empty;
                        UvData.HadHouse = (CurUvData.HadHouse != null) ? CurUvData.HadHouse.Trim() : String.Empty;
                        UvData.RefEmail = (CurUvData.RefEmail != null) ? CurUvData.RefEmail.Trim() : String.Empty;
                        UvData.RefName = (CurUvData.RefName != null) ? CurUvData.RefName.Trim() : String.Empty;
                        UvData.RefInst = (CurUvData.RefInst != null) ? CurUvData.RefInst.Trim() : String.Empty;


                        UvData.FeePolicyDTS = CurUvData.FeePolicyDTS;
                        UvData.DecisionDTS = CurUvData.DecisionDTS;
                        UvData.SignUpDTS = CurUvData.SignUpDTS;
                        UvData.GotSsdiDTS = CurUvData.GotSsdiDTS;
                        UvData.GotProBonoDTS = CurUvData.GotProBonoDTS;
                        UvData.GotPri1040DTS = CurUvData.GotPri1040DTS;
                        UvData.GotPriPayStubDTS = CurUvData.GotPriPayStubDTS;
                        UvData.GotSec1040DTS = CurUvData.GotSec1040DTS;
                        UvData.GotSecPayStubDTS = CurUvData.GotSecPayStubDTS;
                        UvData.GotScheduleIDTS = CurUvData.GotScheduleIDTS;
                        UvData.SignUp = (CurUvData.SignUp != null) ? (int)CurUvData.SignUp : 0;
                        UvData.WaiverType = (CurUvData.WaiverType != null) ? CurUvData.WaiverType.Trim() : String.Empty;
                        UvData.Aproved = (CurUvData.Aproved != null) ? (int)CurUvData.Aproved : 0;
                        UvData.JointFile = (CurUvData.JointFile != null) ? (int)CurUvData.JointFile : 0;

                        UvData.PushClient = true;
                    }
                }
                
                UvData.Creditors = new List<DebtplusPushCreditor>();
                var CredDtl = UserDebtListingsGet(InNo);
                DebtplusPushCreditor UvCredDtl;

                foreach (var CurCredDtl in CredDtl)
                {
                    UvCredDtl = new DebtplusPushCreditor();
                    UvCredDtl.Balance = CurCredDtl.CreditorBal;
                    UvCredDtl.CrAcctNo = (CurCredDtl.CrAcntNoEncs != null) ? CurCredDtl.CrAcntNoEncs.Trim() : String.Empty;
                    UvCredDtl.IntRate = CurCredDtl.CreditorIntrate;
                    UvCredDtl.Name = (CurCredDtl.CreditorName != null) ? CurCredDtl.CreditorName.Trim() : String.Empty;
                    UvCredDtl.PastDue = (CurCredDtl.creditorPastDue != null) ? CurCredDtl.creditorPastDue.Trim() : String.Empty;
                    UvCredDtl.Payment = CurCredDtl.CreditorPayments;
                    UvCredDtl.PreAcctNo = (CurCredDtl.PreAcntNoEncs != null) ? CurCredDtl.PreAcntNoEncs : String.Empty;
                    UvCredDtl.PriAcctHolder = (CurCredDtl.PriAcctHolder != null) ? CurCredDtl.PriAcctHolder : String.Empty;
                    UvData.Creditors.Add(UvCredDtl);
                }
            }
            return UvData;
        }
    }
}