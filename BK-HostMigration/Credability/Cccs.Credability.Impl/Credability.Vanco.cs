﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Vanco;
using System.Xml;
using System.Xml.Linq;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public bool IsVancoActive
        {
            get
            {
                bool is_active = true;

                // TODO:  
                //Set xRec = SqlSerConn.Execute("select IsActive from SysControl where WhatSys = 'VancoPay'")
                //If Not xRec.Eof Then
                //  VancoActive = xRec("IsActive")
                //End If

                return is_active;
            }
        }

        public VancoRef VancoRefGet(int reg_id, bool is_bkc)
        {
            VancoRef vanco_ref = null;

            using (CredabilityDataContext entities = new CredabilityDataContext())
            {
                entities.OpenEncryptionKey();

                vanco_ref = is_bkc ? GetBkcVancoRef(reg_id, entities) : GetNonBkcVancoRef(reg_id, entities);
            }


            return vanco_ref;
        }

        private static VancoRef GetBkcVancoRef(int reg_id, CredabilityDataContext entities)
        {
            var vanco_ref =
                        (
                            from entity in entities.VancoRefs
                            where (entity.ReqID == reg_id)
                            select new VancoRef
                            {
                                ReqID = entity.ReqID,
                                RegInNum = entity.RegInNum,
                                VanCustRef = entity.VanCustRef,
                                VanPayMethRef = entity.VanPayMethRef,
                                VanTransRef = entity.VanTransRef,
                                CreateDTS = entity.CreateDTS,
                                TransDTS = entity.TransDTS,
                                CardFirstName = entities.DecryptText(entity.CardFirstNameEnc),
                                CardMidName = entities.DecryptText(entity.CardMidNameEnc),
                                CardLastName = entities.DecryptText(entity.CardLastNameEnc),
                                CardAddr = entities.DecryptText(entity.CardAddrEnc),
                                CardAddr2 = entities.DecryptText(entity.CardAddr2Enc),
                                CardCity = entities.DecryptText(entity.CardCityEnc),
                                CardST = entities.DecryptText(entity.CardSTEnc),
                                CardZip = entities.DecryptText(entity.CardZipEnc),
                                CardPhone = entity.CardPhone,
                                CardNumber = entities.DecryptText(entity.CardNumberEnc),
                                CardExpMon = entities.DecryptText(entity.CardExpMonEnc),
                                CardExpYear = entities.DecryptText(entity.CardExpYearEnc),
                                Status = entity.Status,
                                SincWithVan = entity.SincWithVan,
                                LastErrCode = entity.LastErrCode,
                                ErrorOn = entity.ErrorOn,
                                IsBkc = true
                            }
                        ).FirstOrDefault();
            return vanco_ref;
        }

        private static VancoRef GetNonBkcVancoRef(int reg_id, CredabilityDataContext entities)
        {
            var vanco_ref =
                        (
                            from entity in entities.VancoMimRefs
                            where (entity.ReqID == reg_id)
                            select new VancoRef
                            {
                                ReqID = entity.ReqID,
                                RegInNum = entity.RegInNum,
                                VanCustRef = entity.VanCustRef,
                                VanPayMethRef = entity.VanPayMethRef,
                                VanTransRef = entity.VanTransRef,
                                CreateDTS = entity.CreateDTS,
                                TransDTS = entity.TransDTS,
                                CardFirstName = entities.DecryptText(entity.CardFirstNameEnc),
                                CardMidName = entities.DecryptText(entity.CardMidNameEnc),
                                CardLastName = entities.DecryptText(entity.CardLastNameEnc),
                                CardAddr = entities.DecryptText(entity.CardAddrEnc),
                                CardAddr2 = entities.DecryptText(entity.CardAddr2Enc),
                                CardCity = entities.DecryptText(entity.CardCityEnc),
                                CardST = entities.DecryptText(entity.CardSTEnc),
                                CardZip = entities.DecryptText(entity.CardZipEnc),
                                CardPhone = entity.CardPhone,
                                CardNumber = entities.DecryptText(entity.CardNumberEnc),
                                CardExpMon = entities.DecryptText(entity.CardExpMonEnc),
                                CardExpYear = entities.DecryptText(entity.CardExpYearEnc),
                                Status = entity.Status,
                                SincWithVan = entity.SincWithVan,
                                LastErrCode = entity.LastErrCode,
                                ErrorOn = entity.ErrorOn,
                                IsBkc = false
                            }
                        ).FirstOrDefault();
            return vanco_ref;
        }

        public Result<int> VancoRefSave(IVancoRef vanco_ref)
        {
            Result<int> result = new Result<int>();

            try
            {
                using (CredabilityDataContext entities = new CredabilityDataContext())
                {
                    if (vanco_ref.ReqID == 0) // Insert
                    {
                        Cccs.Credability.Dal.VancoRef new_vanco_ref = new Cccs.Credability.Dal.VancoRef
                        {

                            ReqID = vanco_ref.ReqID,
                            RegInNum = vanco_ref.RegInNum,
                            VanCustRef = vanco_ref.VanCustRef,
                            VanPayMethRef = vanco_ref.VanPayMethRef,
                            VanTransRef = vanco_ref.VanTransRef,
                            CreateDTS = vanco_ref.CreateDTS,
                            TransDTS = vanco_ref.TransDTS,
                            CardFirstName = vanco_ref.CardFirstName, // TODO: Don't save un-encrypted values?
                            CardFirstNameEnc = entities.EncryptText(vanco_ref.CardFirstName),
                            CardMidName = vanco_ref.CardMidName,		 // TODO: Don't save un-encrypted values?
                            CardMidNameEnc = entities.EncryptText(vanco_ref.CardMidName),
                            CardLastName = vanco_ref.CardLastName,	 // TODO: Don't save un-encrypted values?
                            CardLastNameEnc = entities.EncryptText(vanco_ref.CardLastName),
                            CardAddr = vanco_ref.CardAddr,					 // TODO: Don't save un-encrypted values?
                            CardAddrEnc = entities.EncryptText(vanco_ref.CardAddr),
                            CardAddr2 = vanco_ref.CardAddr2,				 // TODO: Don't save un-encrypted values?
                            CardAddr2Enc = entities.EncryptText(vanco_ref.CardAddr2),
                            CardCity = vanco_ref.CardCity,					 // TODO: Don't save un-encrypted values?
                            CardCityEnc = entities.EncryptText(vanco_ref.CardCity),
                            CardST = vanco_ref.CardST,							 // TODO: Don't save un-encrypted values?
                            CardSTEnc = entities.EncryptText(vanco_ref.CardST),
                            CardZip = vanco_ref.CardZip,						 // TODO: Don't save un-encrypted values?
                            CardZipEnc = entities.EncryptText(vanco_ref.CardZip),
                            CardPhone = vanco_ref.CardPhone,
                            CardNumber = vanco_ref.CardNumber,			 // TODO: Don't save un-encrypted values?
                            CardNumberEnc = entities.EncryptText(vanco_ref.CardNumber),
                            CardExpMon = vanco_ref.CardExpMon,			 // TODO: Don't save un-encrypted values?
                            CardExpMonEnc = entities.EncryptText(vanco_ref.CardExpMon),
                            CardExpYear = vanco_ref.CardExpYear,		 // TODO: Don't save un-encrypted values?
                            CardExpYearEnc = entities.EncryptText(vanco_ref.CardExpYear),
                            Status = vanco_ref.Status,
                            SincWithVan = vanco_ref.SincWithVan,
                            LastErrCode = vanco_ref.LastErrCode,
                            ErrorOn = vanco_ref.ErrorOn,
                        };

                        entities.VancoRefs.InsertOnSubmit(new_vanco_ref);

                        entities.SubmitChanges();

                        vanco_ref.ReqID = new_vanco_ref.ReqID;
                        result.Value = new_vanco_ref.ReqID;

                        result.IsSuccessful = true;
                    }
                    else // Update
                    {
                        int req_id = vanco_ref.ReqID;

                        Cccs.Credability.Dal.VancoRef dal_vanco_ref =
                        (
                            from entity in entities.VancoRefs
                            where (entity.ReqID == req_id)
                            select entity
                        ).FirstOrDefault();

                        if (dal_vanco_ref != null)
                        {
                            dal_vanco_ref.RegInNum = vanco_ref.RegInNum;
                            dal_vanco_ref.VanCustRef = vanco_ref.VanCustRef;
                            dal_vanco_ref.VanPayMethRef = vanco_ref.VanPayMethRef;
                            dal_vanco_ref.VanTransRef = vanco_ref.VanTransRef;
                            dal_vanco_ref.CreateDTS = vanco_ref.CreateDTS;
                            dal_vanco_ref.TransDTS = vanco_ref.TransDTS;
                            dal_vanco_ref.CardFirstName = vanco_ref.CardFirstName; // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardFirstNameEnc = entities.EncryptText(vanco_ref.CardFirstName);
                            dal_vanco_ref.CardMidName = vanco_ref.CardMidName;		 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardMidNameEnc = entities.EncryptText(vanco_ref.CardMidName);
                            dal_vanco_ref.CardLastName = vanco_ref.CardLastName;	 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardLastNameEnc = entities.EncryptText(vanco_ref.CardLastName);
                            dal_vanco_ref.CardAddr = vanco_ref.CardAddr;					 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardAddrEnc = entities.EncryptText(vanco_ref.CardAddr);
                            dal_vanco_ref.CardAddr2 = vanco_ref.CardAddr2;				 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardAddr2Enc = entities.EncryptText(vanco_ref.CardAddr2);
                            dal_vanco_ref.CardCity = vanco_ref.CardCity;					 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardCityEnc = entities.EncryptText(vanco_ref.CardCity);
                            dal_vanco_ref.CardST = vanco_ref.CardST;							 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardSTEnc = entities.EncryptText(vanco_ref.CardST);
                            dal_vanco_ref.CardZip = vanco_ref.CardZip;						 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardZipEnc = entities.EncryptText(vanco_ref.CardZip);
                            dal_vanco_ref.CardPhone = vanco_ref.CardPhone;
                            dal_vanco_ref.CardNumber = vanco_ref.CardNumber;			 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardNumberEnc = entities.EncryptText(vanco_ref.CardNumber);
                            dal_vanco_ref.CardExpMon = vanco_ref.CardExpMon;				// TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardExpMonEnc = entities.EncryptText(vanco_ref.CardExpMon);
                            dal_vanco_ref.CardExpYear = vanco_ref.CardExpYear;			// TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardExpYearEnc = entities.EncryptText(vanco_ref.CardExpYear);
                            dal_vanco_ref.Status = vanco_ref.Status;
                            dal_vanco_ref.SincWithVan = vanco_ref.SincWithVan;
                            dal_vanco_ref.LastErrCode = vanco_ref.LastErrCode;
                            dal_vanco_ref.ErrorOn = vanco_ref.ErrorOn;

                            entities.SubmitChanges();

                            result.Value = req_id;
                            result.IsSuccessful = true;
                        }
                        else
                        {
                            result.Messages = new string[] { "Record not found." };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
            }

            return result;
        }

        public Result<int> VancoMimRefSave(IVancoRef vanco_ref)
        {
            Result<int> result = new Result<int>();

            try
            {
                using (CredabilityDataContext entities = new CredabilityDataContext())
                {
                    if (vanco_ref.ReqID == 0) // Insert
                    {
                        Cccs.Credability.Dal.VancoMimRef new_vanco_ref = new Cccs.Credability.Dal.VancoMimRef
                        {

                            ReqID = vanco_ref.ReqID,
                            RegInNum = vanco_ref.RegInNum,
                            VanCustRef = vanco_ref.VanCustRef,
                            VanPayMethRef = vanco_ref.VanPayMethRef,
                            VanTransRef = vanco_ref.VanTransRef,
                            CreateDTS = vanco_ref.CreateDTS,
                            TransDTS = vanco_ref.TransDTS,
                            CardFirstName = vanco_ref.CardFirstName, // TODO: Don't save un-encrypted values?
                            CardFirstNameEnc = entities.EncryptText(vanco_ref.CardFirstName),
                            CardMidName = vanco_ref.CardMidName,		 // TODO: Don't save un-encrypted values?
                            CardMidNameEnc = entities.EncryptText(vanco_ref.CardMidName),
                            CardLastName = vanco_ref.CardLastName,	 // TODO: Don't save un-encrypted values?
                            CardLastNameEnc = entities.EncryptText(vanco_ref.CardLastName),
                            CardAddr = vanco_ref.CardAddr,					 // TODO: Don't save un-encrypted values?
                            CardAddrEnc = entities.EncryptText(vanco_ref.CardAddr),
                            CardAddr2 = vanco_ref.CardAddr2,				 // TODO: Don't save un-encrypted values?
                            CardAddr2Enc = entities.EncryptText(vanco_ref.CardAddr2),
                            CardCity = vanco_ref.CardCity,					 // TODO: Don't save un-encrypted values?
                            CardCityEnc = entities.EncryptText(vanco_ref.CardCity),
                            CardST = vanco_ref.CardST,							 // TODO: Don't save un-encrypted values?
                            CardSTEnc = entities.EncryptText(vanco_ref.CardST),
                            CardZip = vanco_ref.CardZip,						 // TODO: Don't save un-encrypted values?
                            CardZipEnc = entities.EncryptText(vanco_ref.CardZip),
                            CardPhone = vanco_ref.CardPhone,
                            CardNumber = vanco_ref.CardNumber,			 // TODO: Don't save un-encrypted values?
                            CardNumberEnc = entities.EncryptText(vanco_ref.CardNumber),
                            CardExpMon = vanco_ref.CardExpMon,			 // TODO: Don't save un-encrypted values?
                            CardExpMonEnc = entities.EncryptText(vanco_ref.CardExpMon),
                            CardExpYear = vanco_ref.CardExpYear,		 // TODO: Don't save un-encrypted values?
                            CardExpYearEnc = entities.EncryptText(vanco_ref.CardExpYear),
                            Status = vanco_ref.Status,
                            SincWithVan = vanco_ref.SincWithVan,
                            LastErrCode = vanco_ref.LastErrCode,
                            ErrorOn = vanco_ref.ErrorOn,
                        };

                        entities.VancoMimRefs.InsertOnSubmit(new_vanco_ref);

                        entities.SubmitChanges();

                        vanco_ref.ReqID = new_vanco_ref.ReqID;
                        result.Value = new_vanco_ref.ReqID;

                        result.IsSuccessful = true;
                    }
                    else // Update
                    {
                        int req_id = vanco_ref.ReqID;

                        Cccs.Credability.Dal.VancoMimRef dal_vanco_ref =
                        (
                            from entity in entities.VancoMimRefs
                            where (entity.ReqID == req_id)
                            select entity
                        ).FirstOrDefault();

                        if (dal_vanco_ref != null)
                        {
                            dal_vanco_ref.RegInNum = vanco_ref.RegInNum;
                            dal_vanco_ref.VanCustRef = vanco_ref.VanCustRef;
                            dal_vanco_ref.VanPayMethRef = vanco_ref.VanPayMethRef;
                            dal_vanco_ref.VanTransRef = vanco_ref.VanTransRef;
                            dal_vanco_ref.CreateDTS = vanco_ref.CreateDTS;
                            dal_vanco_ref.TransDTS = vanco_ref.TransDTS;
                            dal_vanco_ref.CardFirstName = vanco_ref.CardFirstName; // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardFirstNameEnc = entities.EncryptText(vanco_ref.CardFirstName);
                            dal_vanco_ref.CardMidName = vanco_ref.CardMidName;		 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardMidNameEnc = entities.EncryptText(vanco_ref.CardMidName);
                            dal_vanco_ref.CardLastName = vanco_ref.CardLastName;	 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardLastNameEnc = entities.EncryptText(vanco_ref.CardLastName);
                            dal_vanco_ref.CardAddr = vanco_ref.CardAddr;					 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardAddrEnc = entities.EncryptText(vanco_ref.CardAddr);
                            dal_vanco_ref.CardAddr2 = vanco_ref.CardAddr2;				 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardAddr2Enc = entities.EncryptText(vanco_ref.CardAddr2);
                            dal_vanco_ref.CardCity = vanco_ref.CardCity;					 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardCityEnc = entities.EncryptText(vanco_ref.CardCity);
                            dal_vanco_ref.CardST = vanco_ref.CardST;							 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardSTEnc = entities.EncryptText(vanco_ref.CardST);
                            dal_vanco_ref.CardZip = vanco_ref.CardZip;						 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardZipEnc = entities.EncryptText(vanco_ref.CardZip);
                            dal_vanco_ref.CardPhone = vanco_ref.CardPhone;
                            dal_vanco_ref.CardNumber = vanco_ref.CardNumber;			 // TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardNumberEnc = entities.EncryptText(vanco_ref.CardNumber);
                            dal_vanco_ref.CardExpMon = vanco_ref.CardExpMon;				// TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardExpMonEnc = entities.EncryptText(vanco_ref.CardExpMon);
                            dal_vanco_ref.CardExpYear = vanco_ref.CardExpYear;			// TODO: Don't save un-encrypted values?
                            dal_vanco_ref.CardExpYearEnc = entities.EncryptText(vanco_ref.CardExpYear);
                            dal_vanco_ref.Status = vanco_ref.Status;
                            dal_vanco_ref.SincWithVan = vanco_ref.SincWithVan;
                            dal_vanco_ref.LastErrCode = vanco_ref.LastErrCode;
                            dal_vanco_ref.ErrorOn = vanco_ref.ErrorOn;

                            entities.SubmitChanges();

                            result.Value = req_id;
                            result.IsSuccessful = true;
                        }
                        else
                        {
                            result.Messages = new string[] { "Record not found." };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
            }

            return result;
        }

        public int? CurVancoRefGet(int client_number)
        {
            int? cur_vanco_ref = null;

            using (CredabilityDataContext entities = new CredabilityDataContext())
            {
                cur_vanco_ref =
                (
                    from entity in entities.contactdetails
                    where (entity.client_number == client_number)
                    select entity.CurVancoRef
                ).FirstOrDefault();
            }

            return cur_vanco_ref;
        }

        public IVancoRef CurVancoMimRefGet(long regNum)
        {
            using (var context = new CredabilityDataContext())
            {
                var item = (from mimRef in context.VancoMimRefs
                            where mimRef.RegInNum == regNum
                            select mimRef).FirstOrDefault();

                return item;
            }
        }

        public Result CurVancoRefSave(int client_number, int cur_vanco_ref)
        {
            Result result = new Result();

            try
            {
                using (CredabilityDataContext entities = new CredabilityDataContext())
                {
                    Cccs.Credability.Dal.contactdetail contact_detail =
                    (
                        from entity in entities.contactdetails
                        where (entity.client_number == client_number)
                        select entity
                    ).FirstOrDefault();

                    if (contact_detail != null)
                    {
                        contact_detail.CurVancoRef = cur_vanco_ref;

                        entities.SubmitChanges();

                        result.IsSuccessful = true;
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
            }

            return result;
        }

        public IVanco GetVancoManager(bool isBkc, bool isEnglish)
        {
            if (isBkc)
            {
                return isEnglish ? vancoBkcEnglish : vancoBkcSpanish;
            }
            else
            {
                return isEnglish ? vancoBkeEnglish : vancoBkeSpanish;
            }
        }

        public VancoDraftSaveResult VancoDraftSave(IVanco vanco, VancoDraft vancoDraft, IVancoRef vancoRef)
        {
            VancoDraftSaveResult result = new VancoDraftSaveResult();
            try
            {
                logger.Debug("calling DoLogin...");
                VancoResult login_result = DoLogin(vanco, vancoRef.ReqID);

                if(login_result != null)
                {
                    logger.DebugFormat("Vanco Result {0}", login_result.IsSuccessful);
                }
                else
                {
                    logger.Debug("Vanco login result is null");
                }

                logger.Debug("check if vanco session id is invalid");
                if (IsInvalidVancoSessionId(login_result))
                {
                    // We could have hit the once a day race condition when the Vanco SessionId
                    // expires and we have to get the new SessionId.
                    logger.Debug("Vanco session id is invalid, getting a new session id");
                    login_result = DoLogin(vanco, vancoRef.ReqID);

                    if (login_result != null)
                    {
                        logger.DebugFormat("Vanco Result {0}", login_result.IsSuccessful);
                    }
                    else
                    {
                        logger.Debug("Vanco login result is null");
                    }
                }
                else
                {
                    logger.Debug("Vanco Session ID is invalid.");
                }

                logger.Debug("calling DoCustomerSave...");
                VancoResult customer_result = DoCustomerSave(vanco, vancoDraft, vancoRef.ReqID, vancoRef.VanCustRef);

                if (customer_result != null)
                {
                    logger.DebugFormat("Customer Result Successful: {0}", customer_result.IsSuccessful);

                    if(customer_result.Errors != null)
                    {
                        foreach (var error in customer_result.Errors)
                        {
                            logger.DebugFormat("Error : {0}, {1}", error.ErrorCode, error.ErrorDescription);
                        }
                    }
                }
                else
                {
                    logger.Debug("customer_result is null");
                }

                if (customer_result.IsSuccessful || IsErrorCode(VancoError.CUSTOMER_ALREADY_EXISTS, customer_result.Errors))
                {
                    logger.DebugFormat("customer_result.IsSuccessful : {0}", customer_result.IsSuccessful);
                    logger.DebugFormat("customer_result.Value : {0}", customer_result.Value);
                    logger.DebugFormat("vancoRef.VanCustRef : {0}", vancoRef.VanCustRef);

                    vancoRef.VanCustRef = customer_result.IsSuccessful ? customer_result.Value : vancoRef.VanCustRef;

                    logger.Debug("calling DoPaymentMethodSave...");
                    VancoResult payment_method_result = DoPaymentMethodSave(vanco, vancoDraft, vancoRef.ReqID, vancoRef.VanCustRef, vancoRef.VanPayMethRef);

                    vancoRef.SincWithVan = payment_method_result.IsSuccessful; // TODO: Verify SincWithVan Logic

                    if (payment_method_result.IsSuccessful)
                    {
                        logger.DebugFormat("Payment successful. result value {0}", payment_method_result.Value);
                        vancoRef.VanPayMethRef = payment_method_result.Value;

                        logger.Debug("calling DoTransactionSave...");
                        VancoResult transaction_result = DoTransactionSave(vanco, vancoDraft, vancoRef.ReqID, vancoRef.VanCustRef, vancoRef.VanPayMethRef);

                        if (transaction_result.IsSuccessful)
                        {
                            logger.Debug("transaction_result successful");
                            logger.DebugFormat("transaction_result value: {0}", transaction_result.Value);

                            vancoRef.VanTransRef = transaction_result.Value;
                            vancoRef.Status = VancoRef.STATUS_PAYED;
                            vancoRef.TransDTS = DateTime.Now;

                            result.IsSuccessful = true;
                        }
                        else // TODO: Transaction Error Handling
                        {
                            logger.Debug("transaction_result not successful");
                            if (transaction_result.Messages != null)
                            {
                                logger.DebugFormat("Messages Count: {0}", transaction_result.Messages.Length);
                            }
                            else
                            {
                                logger.Debug("transaction_result Messages null");
                            }
                            result.Messages = transaction_result.Messages;

                            vancoRef.ErrorOn = VancoRef.ERROR_ON_TRANS;

                            if ((transaction_result.Errors != null) && (transaction_result.Errors.Length > 0))
                            {
                                vancoRef.LastErrCode = transaction_result.Errors[0].ErrorCode;

                                logger.DebugFormat("transaction_result details, Code: {0}, Description: {1}", transaction_result.Errors[0].ErrorCode, transaction_result.Errors[0].ErrorDescription);
                            }
                        }
                    }
                    else // TODO: PaymentMethod Error Handling
                    {
                        logger.Debug("Payment failed.");

                        if(payment_method_result.Messages != null)
                        {
                            logger.DebugFormat("Messages Count: {0}", payment_method_result.Messages.Length);
                        }
                        else
                        {
                            logger.Debug("payment_method_result Messages null");
                        }

                        result.Messages = payment_method_result.Messages;

                        vancoRef.ErrorOn = VancoRef.ERROR_ON_PAYMET;

                        if ((payment_method_result.Errors != null) && (payment_method_result.Errors.Length > 0))
                        {
                            vancoRef.LastErrCode = payment_method_result.Errors[0].ErrorCode;

                            logger.DebugFormat("Payment Error details, Code: {0}, Description: {1}", payment_method_result.Errors[0].ErrorCode, payment_method_result.Errors[0].ErrorDescription);
                        }
                    }
                }
                else // TODO: Customer Error Handling
                {
                    if (customer_result.Messages != null)
                    {
                        logger.DebugFormat("customer_result Messages Count: {0}", customer_result.Messages.Length);
                    }
                    else
                    {
                        logger.Debug("customer_result Messages null");
                    }

                    result.Messages = customer_result.Messages;

                    vancoRef.ErrorOn = VancoRef.ERROR_ON_CUST;
                    if ((customer_result.Errors != null) && (customer_result.Errors.Length > 0))
                    {
                        vancoRef.LastErrCode = customer_result.Errors[0].ErrorCode;

                        logger.DebugFormat("customer_result Error details, Code: {0}, Description: {1}", customer_result.Errors[0].ErrorCode, customer_result.Errors[0].ErrorDescription);
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;

                logger.DebugFormat("Error Occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
            }

            return result;
        }

        private static bool IsInvalidVancoSessionId(VancoResult login_result)
        {
            var isValidSessionId = !login_result.IsSuccessful && (login_result.Messages != null && login_result.Messages.Count() > 0 && login_result.Messages[0].Contains("380|"));
            return isValidSessionId;
        }

        #region VancoDraftSave Helper Methods

        private static object m_vancoLockObject = new object();
        private const int MaximumVancoSessionHours = 23;

        private VancoResult DoLogin(IVanco vanco, int cur_vanco_ref)
        {
            VancoResult result = new VancoResult();
            result.IsSuccessful = true;

            try
            {
                logger.Debug("check Vanco Total Hours");

                if (vanco.LoginDts != null && vanco.LoginDts.HasValue)
                {
                    logger.DebugFormat("Login Dts: {0}", vanco.LoginDts);
                    logger.DebugFormat("Totl Hours: {0}, MaximumVancoSessionHours: {1}", DateTime.Now.Subtract(vanco.LoginDts.Value).TotalHours, MaximumVancoSessionHours);
                }
                else
                {
                    logger.Debug("LoginDts is null.");
                }

                if (vanco.LoginDts == null || DateTime.Now.Subtract(vanco.LoginDts.Value).TotalHours > MaximumVancoSessionHours)
                {
                    lock (m_vancoLockObject)
                    {
                        if (vanco.LoginDts == null || DateTime.Now.Subtract(vanco.LoginDts.Value).TotalHours > MaximumVancoSessionHours)
                        {
                            logger.Debug("login to vanco...");
                            result = vanco.Login(cur_vanco_ref);

                            if(result != null)
                            {
                                logger.DebugFormat("Vanco login result {0}", result.IsSuccessful);
                            }
                            else
                            {
                                logger.Debug("Vanco login result null");
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;

                logger.DebugFormat("Exception occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
            }

            return result;
        }

        private VancoResult DoCustomerSave(IVanco vanco, VancoDraft vanco_draft, int cur_vanco_ref, string van_cust_ref)
        {
            VancoResult result = new VancoResult();

            try
            {
                logger.Debug("calling vanco AddEditCustomer...");
                logger.DebugFormat("Parameter cur_vanco_ref: {0}", cur_vanco_ref);
                logger.DebugFormat("Parameter van_cust_ref: {0}", van_cust_ref);
                logger.DebugFormat("Parameter vanco_draft.ClientNumber: {0}", vanco_draft.ClientNumber);
                logger.DebugFormat("Parameter vanco_draft.DcFNameOnCard: {0}", vanco_draft.DcFNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.DcMNameOnCard: {0}", vanco_draft.DcMNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.DcLNameOnCard: {0}", vanco_draft.DcLNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.DcBillAddr1: {0}", vanco_draft.DcBillAddr1);
                logger.DebugFormat("Parameter vanco_draft.DcBillAddr2: {0}", vanco_draft.DcBillAddr2);
                logger.DebugFormat("Parameter vanco_draft.DcBillCity: {0}", vanco_draft.DcBillCity);
                logger.DebugFormat("Parameter vanco_draft.DcBillState: {0}", vanco_draft.DcBillState);
                logger.DebugFormat("Parameter vanco_draft.DcBillZip: {0}", vanco_draft.DcBillZip);
                logger.DebugFormat("Parameter cur_vanco_ref: {0}", cur_vanco_ref);
                result = vanco.AddEditCustomer(cur_vanco_ref, van_cust_ref, vanco_draft.ClientNumber, vanco_draft.DcFNameOnCard, vanco_draft.DcMNameOnCard, vanco_draft.DcLNameOnCard, vanco_draft.DcBillAddr1, vanco_draft.DcBillAddr2, vanco_draft.DcBillCity, vanco_draft.DcBillState, vanco_draft.DcBillZip);

                if (result != null)
                {
                    logger.DebugFormat("Result of call AddEditCustomer: {0}", result.IsSuccessful);
                }
                else
                {
                    logger.Debug("result of call AddEditCustomer is null");
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;

                logger.DebugFormat("Error occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
            }

            return result;
        }

        private VancoResult DoPaymentMethodSave(IVanco vanco, VancoDraft vanco_draft, int cur_vanco_ref, string van_cust_ref, string van_pay_meth_ref)
        {
            VancoResult result = new VancoResult();

            try
            {
                logger.Debug("calling AddEditPaymentMethod...");
                logger.DebugFormat("Parameter cur_vanco_ref : {0}", cur_vanco_ref);
                logger.DebugFormat("Parameter van_cust_ref : {0}", van_cust_ref);
                logger.DebugFormat("Parameter vanco_draft.ClientNumber : {0}", vanco_draft.ClientNumber);
                logger.DebugFormat("Parameter van_pay_meth_ref : {0}", van_pay_meth_ref);
                logger.DebugFormat("Parameter vanco_draft.DcAcctNum : {0}", vanco_draft.DcAcctNum);
                logger.DebugFormat("Parameter vanco_draft.DcFNameOnCard : {0}", vanco_draft.DcFNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.DcMNameOnCard : {0}", vanco_draft.DcMNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.DcLNameOnCard : {0}", vanco_draft.DcLNameOnCard);
                logger.DebugFormat("Parameter vanco_draft.CardExpMon : {0}", vanco_draft.CardExpMon);
                logger.DebugFormat("Parameter vanco_draft.CardExpYear : {0}", vanco_draft.CardExpYear);
                result = vanco.AddEditPaymentMethod(cur_vanco_ref, van_cust_ref, vanco_draft.ClientNumber, van_pay_meth_ref, vanco_draft.DcAcctNum, vanco_draft.DcFNameOnCard, vanco_draft.DcMNameOnCard, vanco_draft.DcLNameOnCard, vanco_draft.CardExpMon, vanco_draft.CardExpYear);

                if(result != null)
                {
                    logger.DebugFormat("result successful: {0}", result.IsSuccessful);
                }
                else
                {
                    logger.Debug("result is null");
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;

                logger.DebugFormat("Error occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
            }

            return result;
        }

        private VancoResult DoTransactionSave(IVanco vanco, VancoDraft vanco_draft, int cur_vanco_ref, string van_cust_ref, string van_pay_meth_ref)
        {
            VancoResult result = new VancoResult();

            try
            {
                logger.Debug("calling SeqNo");
                int seq_no = SeqNo(vanco_draft.ClientNumber);
                logger.DebugFormat("seq_no: {0}", seq_no);

                logger.Debug("calling AddTransaction...");
                logger.DebugFormat("Parameter cur_vanco_ref: {0}", cur_vanco_ref);
                logger.DebugFormat("Parameter CurInvNo(vanco_draft.ClientNumber, seq_no): {0}", CurInvNo(vanco_draft.ClientNumber, seq_no));
                logger.DebugFormat("Parameter van_cust_ref: {0}", van_cust_ref);
                logger.DebugFormat("Parameter van_pay_meth_ref: {0}", van_pay_meth_ref);
                logger.DebugFormat("Parameter vanco_draft.CharAmount: {0}", vanco_draft.CharAmount);
                result = vanco.AddTransaction(cur_vanco_ref, CurInvNo(vanco_draft.ClientNumber, seq_no), van_cust_ref, van_pay_meth_ref, vanco_draft.CharAmount);

                if(result != null)
                {
                    logger.Debug("result successful");
                }
                else
                {
                    logger.Debug("result null");
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;

                logger.DebugFormat("Error Occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
            }

            return result;
        }

        private string CurInvNo(int client_number, int seq_no)
        {
            return string.Format("IN{0}-{1}", client_number, seq_no);
        }

        private int SeqNo(int client_number)
        {
            int seq_no = 1;

            logger.DebugFormat("calling hostService.GetNextSeqNo for client {0}", client_number);

            // Bankruptcy Conversion
            //Result<XmlDocument> result = hostService.GetNextSeqNo(client_number);

            //if (result.IsSuccessful)
            //{
            //    logger.DebugFormat("result successful, xml result: {0}", result.Value.Value);
            //    XDocument xdoc = new XDocument(result.Value.Value);

            //    var seq_no_node = from s in xdoc.Descendants("") select s;

            //    if (seq_no_node.Count() > 0)
            //    {
            //        int.TryParse(seq_no_node.FirstOrDefault().Value, out seq_no);
            //    }

            //    logger.DebugFormat("seq_no value  {0}", seq_no);
            //}

            int result = debtplusService.GetNextSeqNo(client_number);

            seq_no = result;

            return seq_no;
        }

        public void DoHostTransactionUpdate(VancoDraft vanco_draft, IVancoRef vanco_ref)
        {
            int van_trans_ref = 0;

            if (int.TryParse(vanco_ref.VanTransRef, out van_trans_ref))
            {
                int seq_no = SeqNo(vanco_draft.ClientNumber);


                // Bankruptcy Conversion
                // hostService.SaveTrans(vanco_draft.ClientNumber, van_trans_ref, CurInvNo(vanco_draft.ClientNumber, seq_no), (float)vanco_draft.CharAmount);
                debtplusService.SaveTrans(vanco_draft.ClientNumber, van_trans_ref, CurInvNo(vanco_draft.ClientNumber, seq_no), (float)vanco_draft.CharAmount);
            }
        }

        private bool IsErrorCode(int error_code, VancoError[] error_codes)
        {
            bool is_error_code = false;

            if ((error_codes != null) && (error_codes.Length == 1))
            {
                is_error_code = (error_codes[0].ErrorCode == error_code);
            }

            return is_error_code;
        }

        #endregion
    }
}
