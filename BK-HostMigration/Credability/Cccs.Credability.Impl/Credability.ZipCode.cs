﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
namespace Cccs.Credability.Impl
{
    public partial class Credability:ICredability
    {
        public ZipCodes[] ZipCodeGet(string ZipNumber)
        {
            ZipCodes[] zipCodes = null;

            using (CredabilityDataContext entities = new CredabilityDataContext())
            {
                zipCodes =
                (
                    from entity in entities.zipcodes
                    where (entity.id == ZipNumber)
                    select new ZipCodes
                    {
                        ZipCodeNumber = (entity.id!=null)?entity.id:string.Empty,
                        State = (entity.state != null) ? entity.state : string.Empty,
                        City = (entity.city != null) ? entity.city : string.Empty,

                    }
                ).ToArray<ZipCodes>();
            }

            return zipCodes;
        }
    }
}
