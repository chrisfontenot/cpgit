﻿using System;
using System.Linq;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{
		#region User Operational Information Like Add or Update profile info ---For "Reverse Mortgage Calculator Website "

		public UserContactDetails UserInfoGet(int ClientNumber)
		{
			UserContactDetails UserContactDetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Entities.OpenEncryptionKey();
				UserContactDetail =
			 (
				 from entity in Entities.contactdetails
				 where entity.client_number == ClientNumber
				 select new UserContactDetails
				 {
                     ZipCode = entity.contact_zip != null ? entity.contact_zip : String.Empty,
                     FirmID = entity.firm_id != null ? entity.firm_id : String.Empty,
					 ClientNumber = entity.client_number,
					 //FirstName = (entity.ContactFirstNameEnc != null) ? Entities.DecryptText(entity.ContactFirstNameEnc) : string.Empty,
					 //LastName = (entity.ContactLastNameEnc != null) ? Entities.DecryptText(entity.ContactLastNameEnc) : string.Empty,
					 //Initial = (entity.ContactInitialEnc != null) ? Entities.DecryptText(entity.ContactInitialEnc) : string.Empty,
					 //Dob = (entity.contact_dob != null) ? entity.contact_dob : string.Empty,
					 //Sex = (entity.contact_sex != null) ? entity.contact_sex : string.Empty,
					 //Ssn = (entity.ContactSsnEnc != null) ? Entities.DecryptText(entity.ContactSsnEnc) : string.Empty,
					 //Race = (entity.contact_race != null) ? entity.contact_race : string.Empty,
					 //Marital = (entity.contact_marital != null) ? entity.contact_marital : string.Empty,
					 //City = (entity.contact_city != null) ? entity.contact_city : string.Empty,
					 //State = (entity.contact_state != null) ? entity.contact_state : string.Empty,
					 //Zip = (entity.contact_zip != null) ? entity.contact_zip : string.Empty,
					 //Address = (entity.ContactAddressEnc != null) ? Entities.DecryptText(entity.ContactAddressEnc) : string.Empty,
					 //Address2 = (entity.ContactAddress2Enc != null) ? Entities.DecryptText(entity.ContactAddress2Enc) : string.Empty,
					 //Telephone = (entity.ContactTelephoneEnc != null) ? Entities.DecryptText(entity.ContactTelephoneEnc) : string.Empty,
					 //OtherTelephone = (entity.ContactOtherTelephoneEnc != null) ? Entities.DecryptText(entity.ContactOtherTelephoneEnc) : string.Empty,
					 //Email = (entity.RealEmailEnc != null) ? Entities.DecryptText(entity.RealEmailEnc) : string.Empty,
					 //Hispanic = (entity.contact_hispanic != null) ? entity.contact_hispanic : string.Empty,
					 //CosignFirstName = (entity.CosignFirstNameEnc != null) ? Entities.DecryptText(entity.CosignFirstNameEnc) : string.Empty,
					 //CosignInitial = (entity.CosignInitialEnc != null) ? Entities.DecryptText(entity.CosignInitialEnc) : string.Empty,
					 //CosignLastname = (entity.CosignLastNameEnc != null) ? Entities.DecryptText(entity.CosignLastNameEnc) : string.Empty,
					 //CosignMarital = (entity.cosign_marital != null) ? entity.cosign_marital : string.Empty,
					 //CosignRace = (entity.cosign_race != null) ? entity.cosign_race : string.Empty,
					 //CosignDob = (entity.cosign_dob != null) ? entity.cosign_dob : string.Empty,
					 //CosignSex = (entity.cosign_sex != null) ? entity.cosign_sex : string.Empty,
					 //CosignSsn = (entity.CosignSsnEnc != null) ? Entities.DecryptText(entity.CosignSsnEnc) : string.Empty,

					 BirthOfCity = (entity.cred_rpt_bcity != null) ? entity.cred_rpt_bcity : string.Empty,
					 CosignBirthOfCity = (entity.cred_rpt_bcity2 != null) ? entity.cred_rpt_bcity2 : string.Empty,
					 //CosignHispanic = (entity.cosign_hispanic != null) ? entity.cosign_hispanic : string.Empty,
					 //ContactTelephone = (entity.contact_telephone != null) ? entity.contact_telephone : string.Empty,
					 CcrcAprov = (entity.CcrcAprov != null) ? entity.CcrcAprov : string.Empty,
					 RefCode = (entity.refcode != null) ? entity.refcode : string.Empty,
					 NumberOfPeople = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
					 //EmailAddress = (entity.contact_email != null) ? entity.contact_email : string.Empty,
					 ClientType = entity.ClientType,
				 }
				).FirstOrDefault();
			}
			return UserContactDetail;
		}

		public WaverAppBKCounseling WaverAppBKCounselingGet(int ClientNumber)
		{
			WaverAppBKCounseling WaverAppBKCounselings = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				WaverAppBKCounselings =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new WaverAppBKCounseling
						 {
							 ClientNumber = entity.client_number,
							 ContactFirstname = entity.contact_firstname,
							 ContactLastname = entity.contact_lastname,
							 ContactAddress = entity.contact_address,
							 ContactAddress2 = entity.contact_address2,
							 ContactCity = entity.contact_city,
							 ContactState = entity.contact_state,
							 ContactZip = entity.contact_zip,
							 ContactTelephone = entity.contact_telephone,
							 RealEmail = entity.real_email,
							 CosignFirstname = entity.cosign_firstname,
							 CosignLastname = entity.cosign_lastname,

						 }
						).FirstOrDefault();
			}
			return WaverAppBKCounselings;
		}

		public UserContactDetailsHousingOnly UserContactDetailsHousingOnlyGet(int ClientNumber)
		{
			UserContactDetailsHousingOnly UserContactDetailsHousingOnlys = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserContactDetailsHousingOnlys =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new UserContactDetailsHousingOnly
						 {
							 ClientNumber = entity.client_number,
							 //FirstName = entity.contact_firstname,
							 //LastName = entity.contact_lastname,
							 //Initial = entity.contact_initial,
							 //Dob = entity.contact_dob,
							 //Sex = entity.contact_sex,
							 //Ssn = entity.contact_ssn,
							 //Race = entity.contact_race,
							 //Marital = entity.contact_marital,
							 //City = entity.contact_city,
							 //State = entity.contact_state,
							 //Zip = entity.contact_zip,
							 //Address = entity.contact_address,
							 //Address2 = entity.contact_address2,
							 //Hispanic = entity.contact_hispanic,
							 //CosignFirstName = entity.cosign_firstname,
							 //CosignInitial = entity.cosign_initial,
							 //CosignLastname = entity.cosign_lastname,
							 //CosignMarital = entity.cosign_marital,
							 //CosignRace = entity.cosign_race,
							 //CosignDob = entity.cosign_dob,
							 //CosignSex = entity.cosign_sex,
							 //CosignSsn = entity.cosign_ssn,
							 BirthOfCity = entity.cred_rpt_bcity,
							 CosignBirthOfCity = entity.cred_rpt_bcity2,
							 //CosignHispanic = entity.cosign_hispanic,
							 //ContactTelephone = entity.contact_telephone,
							 //OtherTelephone = entity.contact_other_telephone,
							 CcrcAprov = entity.CcrcAprov,
						 }
						).FirstOrDefault();
			}
			return UserContactDetailsHousingOnlys;
		}

		public UserContactDetailsResult UserInformationUpdate(UserContactDetails user_contact_details)
		{
			UserContactDetailsResult result = new UserContactDetailsResult();

			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Entities.OpenEncryptionKey();
				try
				{
					contactdetail contact_detail = (user_contact_details.ClientNumber == 0) ? null :
					(
						 from entity in Entities.contactdetails
						 where (entity.client_number == user_contact_details.ClientNumber)
						 select entity
					 ).FirstOrDefault<contactdetail>();

					if(contact_detail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetails.FirstName);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetails.LastName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetails.Initial);
						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetails.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetails.Address2);
						//Contactdetail.contact_city = userContactDetails.City;
						//Contactdetail.contact_state = userContactDetails.State;
						//Contactdetail.contact_zip = userContactDetails.Zip;
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetails.Ssn);
						//Contactdetail.contact_dob = userContactDetails.Dob;
						//Contactdetail.contact_marital = userContactDetails.Marital;
						//Contactdetail.contact_sex = userContactDetails.Sex;
						//Contactdetail.contact_race = userContactDetails.Race;
						//Contactdetail.contact_hispanic = userContactDetails.Hispanic;
						//Contactdetail.RealEmailEnc = Entities.EncryptText(userContactDetails.Email);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetails.Telephone);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetails.SecTelephone);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetails.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetails.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetails.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetails.CosignSsn);
						//Contactdetail.cosign_dob = userContactDetails.CosignDob;
						//Contactdetail.cosign_marital = userContactDetails.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetails.CosignSex;
						//Contactdetail.cosign_race = userContactDetails.Race;
                        contact_detail.contact_zip = user_contact_details.ZipCode;
						contact_detail.cred_rpt_bcity = user_contact_details.BirthOfCity;
						contact_detail.cred_rpt_bcity2 = user_contact_details.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetails.CosignHispanic;
						//contact_detail.contact_telephone = user_contact_details.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetails.OtherTelephone;
						if(!string.IsNullOrEmpty(user_contact_details.CcrcAprov))
							contact_detail.CcrcAprov = user_contact_details.CcrcAprov;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;
						contact_detail.refcode = user_contact_details.RefCode;
						contact_detail.size_of_household = user_contact_details.NumberOfPeople;
						contact_detail.ClientType = user_contact_details.ClientType;

						Entities.SubmitChanges();

						result.ClientNumber = contact_detail.client_number;
						result.IsSuccessful = true;
					}
					else // Add New User 
					{
						contact_detail = new contactdetail();
						//contact_detail.client_number = user_contact_details.ClientNumber;
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetails.FirstName);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetails.LastName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetails.Initial);
						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetails.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetails.Address2);
						//Contactdetail.contact_city = userContactDetails.City;
						//Contactdetail.contact_state = userContactDetails.State;
						//Contactdetail.contact_zip = userContactDetails.Zip;
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetails.Ssn);
						//Contactdetail.contact_dob = userContactDetails.Dob;
						//Contactdetail.contact_marital = userContactDetails.Marital;
						//Contactdetail.contact_sex = userContactDetails.Sex;
						//Contactdetail.contact_race = userContactDetails.Race;
						//Contactdetail.contact_hispanic = userContactDetails.Hispanic;
						//Contactdetail.RealEmailEnc = Entities.EncryptText(userContactDetails.Email);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetails.Telephone);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetails.SecTelephone);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetails.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetails.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetails.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetails.CosignSsn);
						//Contactdetail.cosign_dob = userContactDetails.CosignDob;
						//Contactdetail.cosign_marital = userContactDetails.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetails.CosignSex;
						//Contactdetail.cosign_race = userContactDetails.Race;
						contact_detail.cred_rpt_bcity = user_contact_details.BirthOfCity;
						contact_detail.cred_rpt_bcity2 = user_contact_details.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetails.CosignHispanic;
						contact_detail.contact_telephone = user_contact_details.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetails.OtherTelephone;
						contact_detail.CcrcAprov = user_contact_details.CcrcAprov;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;
						contact_detail.refcode = user_contact_details.RefCode;
						contact_detail.size_of_household = user_contact_details.NumberOfPeople;
						contact_detail.ClientType = user_contact_details.ClientType;
						contact_detail.temp_id = DateTime.Now.ToString();
						contact_detail.CreateDTS = DateTime.Now;

						Entities.contactdetails.InsertOnSubmit(contact_detail);
						Entities.SubmitChanges();

						result.ClientNumber = contact_detail.client_number;
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}

		public WaverAppBKCounselingResult WaverAppBKCounselingUpdate(WaverAppBKCounseling waverAppBKCounseling)
		{
			contactdetail Contactdetail = null;

			WaverAppBKCounselingResult UpdateResult = new WaverAppBKCounselingResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == waverAppBKCounseling.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						Contactdetail.contact_firstname = waverAppBKCounseling.ContactFirstname;
						Contactdetail.contact_lastname = waverAppBKCounseling.ContactLastname;
						Contactdetail.contact_address = waverAppBKCounseling.ContactAddress;
						Contactdetail.contact_address2 = waverAppBKCounseling.ContactAddress2;
						Contactdetail.contact_city = waverAppBKCounseling.ContactCity;
						Contactdetail.contact_state = waverAppBKCounseling.ContactState;
						Contactdetail.contact_zip = waverAppBKCounseling.ContactZip;
						Contactdetail.contact_telephone = waverAppBKCounseling.ContactTelephone;
						Contactdetail.real_email = waverAppBKCounseling.RealEmail;
						Contactdetail.cosign_firstname = waverAppBKCounseling.CosignFirstname;
						Contactdetail.cosign_lastname = waverAppBKCounseling.CosignLastname;

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
					else // Add New User 
					{
						Contactdetail = new contactdetail();
						Contactdetail.contact_firstname = waverAppBKCounseling.ContactFirstname;
						Contactdetail.contact_lastname = waverAppBKCounseling.ContactLastname;
						Contactdetail.contact_address = waverAppBKCounseling.ContactAddress;
						Contactdetail.contact_address2 = waverAppBKCounseling.ContactAddress2;
						Contactdetail.contact_city = waverAppBKCounseling.ContactCity;
						Contactdetail.contact_state = waverAppBKCounseling.ContactState;
						Contactdetail.contact_zip = waverAppBKCounseling.ContactZip;
						Contactdetail.contact_telephone = waverAppBKCounseling.ContactTelephone;
						Contactdetail.real_email = waverAppBKCounseling.RealEmail;
						Contactdetail.cosign_firstname = waverAppBKCounseling.CosignFirstname;
						Contactdetail.cosign_lastname = waverAppBKCounseling.CosignLastname;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}
		}

		public UserContactDetailsResult UserInformationUpdateForPrePurchase(UserContactDetails userContactDetails)
		{
			contactdetail Contactdetail = null;

			UserContactDetailsResult UpdateResult = new UserContactDetailsResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == userContactDetails.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						//Contactdetail.contact_firstname = userContactDetails.FirstName;
						//Contactdetail.contact_lastname = userContactDetails.LastName;
						//Contactdetail.contact_initial = userContactDetails.Initial;
						//Contactdetail.contact_address = userContactDetails.Address;
						//Contactdetail.contact_address2 = userContactDetails.Address2;
						//Contactdetail.contact_city = userContactDetails.City;
						//Contactdetail.contact_state = userContactDetails.State;
						//Contactdetail.contact_zip = userContactDetails.Zip;
						//Contactdetail.contact_ssn = userContactDetails.Ssn;
						//Contactdetail.contact_dob = userContactDetails.Dob;
						//Contactdetail.contact_marital = userContactDetails.Marital;
						//Contactdetail.contact_sex = userContactDetails.Sex;
						//Contactdetail.contact_race = userContactDetails.Race;
						//Contactdetail.contact_hispanic = userContactDetails.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetails.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetails.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetails.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetails.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetails.CosignDob;
						//Contactdetail.cosign_marital = userContactDetails.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetails.CosignSex;
						//Contactdetail.cosign_race = userContactDetails.Race;
						Contactdetail.cred_rpt_bcity = userContactDetails.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetails.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetails.CosignHispanic;
						//Contactdetail.contact_telephone = userContactDetails.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetails.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetails.CcrcAprov;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;
						Contactdetail.refcode = userContactDetails.RefCode;
						Contactdetail.size_of_household = userContactDetails.NumberOfPeople;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;

						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetails.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetails.Address2);
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetails.FirstName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetails.Initial);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetails.LastName);
						//Contactdetail.ContactStateEnc = Entities.EncryptText(userContactDetails.State);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetails.Telephone);
						//Contactdetail.ContactCityEnc = Entities.EncryptText(userContactDetails.City);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetails.OtherTelephone);
						//Contactdetail.ContactZipEnc = Entities.EncryptText(userContactDetails.Zip);
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetails.Ssn);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetails.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetails.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetails.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetails.CosignSsn);
						Contactdetail.cred_rpt_pull = 'y';
						Contactdetail.ModForm7 = 1;

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
					else // Add New User 
					{
						Contactdetail = new contactdetail();
						//Contactdetail.contact_firstname = userContactDetails.FirstName;
						//Contactdetail.contact_lastname = userContactDetails.LastName;
						//Contactdetail.contact_initial = userContactDetails.Initial;
						//Contactdetail.contact_address = userContactDetails.Address;
						//Contactdetail.contact_address2 = userContactDetails.Address2;
						//Contactdetail.contact_city = userContactDetails.City;
						//Contactdetail.contact_state = userContactDetails.State;
						//Contactdetail.contact_zip = userContactDetails.Zip;
						//Contactdetail.contact_ssn = userContactDetails.Ssn;
						//Contactdetail.contact_dob = userContactDetails.Dob;
						//Contactdetail.contact_marital = userContactDetails.Marital;
						//Contactdetail.contact_sex = userContactDetails.Sex;
						//Contactdetail.contact_race = userContactDetails.Race;
						//Contactdetail.contact_hispanic = userContactDetails.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetails.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetails.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetails.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetails.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetails.CosignDob;
						//Contactdetail.cosign_marital = userContactDetails.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetails.CosignSex;
						//Contactdetail.cosign_race = userContactDetails.Race;
						Contactdetail.cred_rpt_bcity = userContactDetails.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetails.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetails.CosignHispanic;
						Contactdetail.contact_telephone = userContactDetails.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetails.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetails.CcrcAprov;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;
						Contactdetail.refcode = userContactDetails.RefCode;
						Contactdetail.size_of_household = userContactDetails.NumberOfPeople;
						//Contactdetail.contact_email = userContactDetails.EmailAddress;

						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetails.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetails.Address2);
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetails.FirstName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetails.Initial);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetails.LastName);
						//Contactdetail.ContactStateEnc = Entities.EncryptText(userContactDetails.State);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetails.Telephone);
						//Contactdetail.ContactCityEnc = Entities.EncryptText(userContactDetails.City);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetails.OtherTelephone);
						//Contactdetail.ContactZipEnc = Entities.EncryptText(userContactDetails.Zip);
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetails.Ssn);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetails.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetails.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetails.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetails.CosignSsn);
						Contactdetail.cred_rpt_pull = 'y';
						Contactdetail.ModForm7 = 1;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}
		}

		public UserContactDetailsHousingOnlyResult UserContactDetailsHousingOnlyUpdate(UserContactDetailsHousingOnly userContactDetailsHousingOnly)
		{
			contactdetail Contactdetail = null;

			UserContactDetailsHousingOnlyResult UserContactDetailsHousingOnlyResults = new UserContactDetailsHousingOnlyResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == userContactDetailsHousingOnly.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						//Contactdetail.contact_firstname = userContactDetailsHousingOnly.FirstName;
						//Contactdetail.contact_lastname = userContactDetailsHousingOnly.LastName;
						//Contactdetail.contact_initial = userContactDetailsHousingOnly.Initial;
						//Contactdetail.contact_address = userContactDetailsHousingOnly.Address;
						//Contactdetail.contact_address2 = userContactDetailsHousingOnly.Address2;
						//Contactdetail.contact_city = userContactDetailsHousingOnly.City;
						//Contactdetail.contact_state = userContactDetailsHousingOnly.State;
						//Contactdetail.contact_zip = userContactDetailsHousingOnly.Zip;
						//Contactdetail.contact_ssn = userContactDetailsHousingOnly.Ssn;
						//Contactdetail.contact_dob = userContactDetailsHousingOnly.Dob;
						//Contactdetail.contact_marital = userContactDetailsHousingOnly.Marital;
						//Contactdetail.contact_sex = userContactDetailsHousingOnly.Sex;
						//Contactdetail.contact_race = userContactDetailsHousingOnly.Race;
						//Contactdetail.contact_hispanic = userContactDetailsHousingOnly.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetailsHousingOnly.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetailsHousingOnly.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetailsHousingOnly.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetailsHousingOnly.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetailsHousingOnly.CosignDob;
						//Contactdetail.cosign_marital = userContactDetailsHousingOnly.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetailsHousingOnly.CosignSex;
						//Contactdetail.cosign_race = userContactDetailsHousingOnly.Race;
						Contactdetail.cred_rpt_bcity = userContactDetailsHousingOnly.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetailsHousingOnly.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetailsHousingOnly.CosignHispanic;
						//Contactdetail.contact_telephone = userContactDetailsHousingOnly.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetailsHousingOnly.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetailsHousingOnly.CcrcAprov;

						Entities.SubmitChanges();

						UserContactDetailsHousingOnlyResults.ClientNumber = Contactdetail.client_number;
						UserContactDetailsHousingOnlyResults.IsSuccessful = true;

					}
					else // Add New User 
					{
						//Contactdetail.contact_firstname = userContactDetailsHousingOnly.FirstName;
						//Contactdetail.contact_lastname = userContactDetailsHousingOnly.LastName;
						//Contactdetail.contact_initial = userContactDetailsHousingOnly.Initial;
						//Contactdetail.contact_address = userContactDetailsHousingOnly.Address;
						//Contactdetail.contact_address2 = userContactDetailsHousingOnly.Address2;
						//Contactdetail.contact_city = userContactDetailsHousingOnly.City;
						//Contactdetail.contact_state = userContactDetailsHousingOnly.State;
						//Contactdetail.contact_zip = userContactDetailsHousingOnly.Zip;
						//Contactdetail.contact_ssn = userContactDetailsHousingOnly.Ssn;
						//Contactdetail.contact_dob = userContactDetailsHousingOnly.Dob;
						//Contactdetail.contact_marital = userContactDetailsHousingOnly.Marital;
						//Contactdetail.contact_sex = userContactDetailsHousingOnly.Sex;
						//Contactdetail.contact_race = userContactDetailsHousingOnly.Race;
						//Contactdetail.contact_hispanic = userContactDetailsHousingOnly.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetailsHousingOnly.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetailsHousingOnly.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetailsHousingOnly.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetailsHousingOnly.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetailsHousingOnly.CosignDob;
						//Contactdetail.cosign_marital = userContactDetailsHousingOnly.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetailsHousingOnly.CosignSex;
						//Contactdetail.cosign_race = userContactDetailsHousingOnly.Race;
						Contactdetail.cred_rpt_bcity = userContactDetailsHousingOnly.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetailsHousingOnly.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetailsHousingOnly.CosignHispanic;
						//Contactdetail.contact_telephone = userContactDetailsHousingOnly.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetailsHousingOnly.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetailsHousingOnly.CcrcAprov;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UserContactDetailsHousingOnlyResults.ClientNumber = Contactdetail.client_number;
						UserContactDetailsHousingOnlyResults.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UserContactDetailsHousingOnlyResults.IsSuccessful = false;
					UserContactDetailsHousingOnlyResults.Exception = exception;
					UserContactDetailsHousingOnlyResults.Messages = new string[] { "An error occured attempting to save." };
				}
				return UserContactDetailsHousingOnlyResults;
			}
		}

		public UserSituationDescriptionResult UserSituationDescriptionUpdate(UserSituationDescription userSituationDescription)
		{
			string Message = string.Empty;
			UserSituationDescriptionResult Result = new UserSituationDescriptionResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == userSituationDescription.ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						Contactdetail.contact_reason = userSituationDescription.ContactReason;
						Contactdetail.contact_comments = userSituationDescription.ContactComments;
						Contactdetail.housing_type = !string.IsNullOrEmpty(userSituationDescription.HousingType) ? (char?) userSituationDescription.HousingType[0] : null;
						Contactdetail.size_of_household = userSituationDescription.SizeOfHouseHold;
						Contactdetail.mort_current = userSituationDescription.MortCurrent;
						Contactdetail.mos_delinq = userSituationDescription.MosDelinq;

						transactionEntities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = userSituationDescription.ClientNumber;
					}
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
				return Result;
			}
		}

		public DisclosureContactDetailsBKCounselingResult DisclosureContactDetailsBKCounselingAddUpdate(DisclosureContactDetailsBKCounseling disclosureContactDetailsBKCounseling)
		{
			string Message = string.Empty;
			DisclosureContactDetailsBKCounselingResult Result = new DisclosureContactDetailsBKCounselingResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							 from contat in transactionEntities.contactdetails
							 where contat.client_number == disclosureContactDetailsBKCounseling.ClientNumber
							 select contat
							).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						Contactdetail.ads_bankname = disclosureContactDetailsBKCounseling.ADSbankname;
						Contactdetail.ads_bankphone = disclosureContactDetailsBKCounseling.ADSbankphone;
						Contactdetail.ads_NameonCheck = disclosureContactDetailsBKCounseling.adsNAMEONCHECK;
						Contactdetail.payment_type = disclosureContactDetailsBKCounseling.paymentTYPE;
						Contactdetail.abanumber = disclosureContactDetailsBKCounseling.abaNUMBER;
						Contactdetail.acctnumber = disclosureContactDetailsBKCounseling.acctNUMBER;
						Contactdetail.AcctNumberEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.acctNUMBER);
						Contactdetail.CardFirstNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcFNAMEONCARD);
						Contactdetail.CardLastNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcLNAMEONCARD);
						Contactdetail.CardMidNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcMNAMEONCARD);
						Contactdetail.CardBillAddrEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLADDR);
						Contactdetail.CardBillAddr2Enc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLADDR2);
						Contactdetail.CardBillCityEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLCITY);
						Contactdetail.CardBillStEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLSTATE);
						Contactdetail.CardBillZipEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLZIP);
						Contactdetail.CardExpDateEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcEXPDATE);
						Contactdetail.CardAcctNumEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcACCTNUM);
						Contactdetail.mtcn = disclosureContactDetailsBKCounseling.mtcn;

						transactionEntities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = disclosureContactDetailsBKCounseling.ClientNumber;
					}
					else
					{
						Contactdetail = new contactdetail();
						Contactdetail.ads_bankname = disclosureContactDetailsBKCounseling.ADSbankname;
						Contactdetail.ads_bankphone = disclosureContactDetailsBKCounseling.ADSbankphone;
						Contactdetail.ads_NameonCheck = disclosureContactDetailsBKCounseling.adsNAMEONCHECK;
						Contactdetail.payment_type = disclosureContactDetailsBKCounseling.paymentTYPE;
						Contactdetail.abanumber = disclosureContactDetailsBKCounseling.abaNUMBER;
						Contactdetail.acctnumber = disclosureContactDetailsBKCounseling.acctNUMBER;
						Contactdetail.AcctNumberEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.acctNUMBER);
						Contactdetail.CardFirstNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcFNAMEONCARD);
						Contactdetail.CardLastNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcLNAMEONCARD);
						Contactdetail.CardMidNameEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcMNAMEONCARD);
						Contactdetail.CardBillAddrEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLADDR);
						Contactdetail.CardBillAddr2Enc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLADDR2);
						Contactdetail.CardBillCityEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLCITY);
						Contactdetail.CardBillStEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLSTATE);
						Contactdetail.CardBillZipEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcBILLZIP);
						Contactdetail.CardExpDateEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcEXPDATE);
						Contactdetail.CardAcctNumEnc = transactionEntities.EncryptText(disclosureContactDetailsBKCounseling.dcACCTNUM);
						Contactdetail.client_number = Contactdetail.client_number;
						Contactdetail.mtcn = disclosureContactDetailsBKCounseling.mtcn;
						transactionEntities.contactdetails.InsertOnSubmit(Contactdetail);
						transactionEntities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = disclosureContactDetailsBKCounseling.ClientNumber;
					}
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
				return Result;
			}
		}

		public UserDecsribeYourSituationHousingOnlyResult UserDecsribeYourSituationHousingOnlyUpdate(UserDecsribeYourSituationHousingOnly userDecsribeYourSituationHousingOnly)
		{
			string Message = string.Empty;
			UserDecsribeYourSituationHousingOnlyResult Result = new UserDecsribeYourSituationHousingOnlyResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							from contat in transactionEntities.contactdetails
							where contat.client_number == userDecsribeYourSituationHousingOnly.ClientNumber
							select contat
					 ).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						Contactdetail.contact_reason = userDecsribeYourSituationHousingOnly.ContactReason;
						Contactdetail.contact_comments = userDecsribeYourSituationHousingOnly.ContactComments;
						Contactdetail.housing_type = !string.IsNullOrEmpty(userDecsribeYourSituationHousingOnly.HousingType) ? (char?) userDecsribeYourSituationHousingOnly.HousingType[0] : null;
						Contactdetail.size_of_household = userDecsribeYourSituationHousingOnly.SizeOfHouseHold;
						Contactdetail.mort_current = userDecsribeYourSituationHousingOnly.MortCurrent;
						Contactdetail.mos_delinq = userDecsribeYourSituationHousingOnly.MosDelinq;

						Contactdetail.mort_holder = userDecsribeYourSituationHousingOnly.MortHolder;
						Contactdetail.loan_number = userDecsribeYourSituationHousingOnly.LoanNumber;
						Contactdetail.mort_type = userDecsribeYourSituationHousingOnly.MortType;
						Contactdetail.mort_years = userDecsribeYourSituationHousingOnly.MortYears;
						Contactdetail.mort_date = userDecsribeYourSituationHousingOnly.MortDate;
						Contactdetail.mort_rate = userDecsribeYourSituationHousingOnly.MortRate;
						Contactdetail.rate_type = userDecsribeYourSituationHousingOnly.RateType;
						Contactdetail.last_contact_desc = userDecsribeYourSituationHousingOnly.LastContactDesc;
						Contactdetail.orig_bal = userDecsribeYourSituationHousingOnly.OrigBal;
						Contactdetail.owe_home = userDecsribeYourSituationHousingOnly.Owehome;
						Contactdetail.val_home = userDecsribeYourSituationHousingOnly.ValHome;
						Contactdetail.mo_pmt = userDecsribeYourSituationHousingOnly.MoPmt;
						Contactdetail.amt_avail = userDecsribeYourSituationHousingOnly.AmtAvail;
						Contactdetail.repay_plan = userDecsribeYourSituationHousingOnly.RepayPlan;
						Contactdetail.PriServID = userDecsribeYourSituationHousingOnly.PRIServID;
						Contactdetail.SecServID = userDecsribeYourSituationHousingOnly.SECServID;
						Contactdetail.secondary_amt = userDecsribeYourSituationHousingOnly.SecondaryAmt;
						Contactdetail.secondary_status = userDecsribeYourSituationHousingOnly.SecondaryStatus;
						Contactdetail.last_contact_date = userDecsribeYourSituationHousingOnly.LastContactDate;
						Contactdetail.contact_state = userDecsribeYourSituationHousingOnly.ContactState;
						Contactdetail.Prop4Sale = userDecsribeYourSituationHousingOnly.PROP4Sale;
						Contactdetail.Note4Close = userDecsribeYourSituationHousingOnly.NOTE4Close;
						Contactdetail.WhoInHouse = userDecsribeYourSituationHousingOnly.WHOInHouse;
						Contactdetail.loan_number2 = userDecsribeYourSituationHousingOnly.LoanNumber2;
						Contactdetail.MakeHomeAff = userDecsribeYourSituationHousingOnly.MAKEHomeAff;
						Contactdetail.ClientType = userDecsribeYourSituationHousingOnly.ClientType;

						transactionEntities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = userDecsribeYourSituationHousingOnly.ClientNumber;
					}
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
				return Result;
			}
		}

		public UserSituationDescription UserSituationDescriptionGet(int ClientNumber)
		{
			UserSituationDescription UserSituationDescriptionResults = new UserSituationDescription();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
					 from contat in transactionEntities.contactdetails
					 where contat.client_number == ClientNumber
					 select contat
					).FirstOrDefault<contactdetail>();
				if(Contactdetail != null)
				{//rash
					UserSituationDescriptionResults.ClientNumber = ClientNumber;

					UserSituationDescriptionResults.ContactComments = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					UserSituationDescriptionResults.ContactReason = (Contactdetail.contact_reason != null) ? Contactdetail.contact_reason : string.Empty;
					UserSituationDescriptionResults.SizeOfHouseHold = Convert.ToInt32(Contactdetail.size_of_household);
					UserSituationDescriptionResults.MosDelinq = Convert.ToInt32(Contactdetail.mos_delinq);
					UserSituationDescriptionResults.MortCurrent = Convert.ToInt32(Contactdetail.mort_current);

				}
				else
				{
					UserSituationDescriptionResults.ClientNumber = 0;
				}
				return UserSituationDescriptionResults;
			}
		}

		public DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounselingGet(int ClientNumber)
		{
			DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounselingResults = new DisclosureContactDetailsBKCounseling();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					transactionEntities.OpenEncryptionKey();
					DisclosureContactDetailsBKCounselingResults.ClientNumber = ClientNumber;
					DisclosureContactDetailsBKCounselingResults.abaNUMBER = (Contactdetail.abanumber != null) ? Contactdetail.abanumber : string.Empty;

					DisclosureContactDetailsBKCounselingResults.acctNUMBER = (Contactdetail.AcctNumberEnc != null) ? transactionEntities.DecryptText(Contactdetail.AcctNumberEnc) : string.Empty;

					DisclosureContactDetailsBKCounselingResults.ADSbankname = (Contactdetail.ads_bankname != null) ? Contactdetail.ads_bankname : string.Empty;
					DisclosureContactDetailsBKCounselingResults.ADSbankphone = (Contactdetail.ads_bankphone != null) ? Contactdetail.ads_bankphone : string.Empty;
					DisclosureContactDetailsBKCounselingResults.adsNAMEONCHECK = (Contactdetail.ads_NameonCheck != null) ? Contactdetail.ads_NameonCheck : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcACCTNUM = (Contactdetail.AcctNumberEnc != null) ? transactionEntities.DecryptText(Contactdetail.AcctNumberEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcBILLADDR = (Contactdetail.CardBillAddrEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardBillAddrEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcBILLADDR2 = (Contactdetail.CardBillAddr2Enc != null) ? transactionEntities.DecryptText(Contactdetail.CardBillAddr2Enc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcBILLCITY = (Contactdetail.CardBillCityEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardBillCityEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcBILLSTATE = (Contactdetail.CardBillStEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardBillStEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcBILLZIP = (Contactdetail.CardBillZipEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardBillZipEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcEXPDATE = (Contactdetail.CardExpDateEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardExpDateEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcFNAMEONCARD = (Contactdetail.CardFirstNameEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardFirstNameEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcLNAMEONCARD = (Contactdetail.CardLastNameEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardLastNameEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.dcMNAMEONCARD = (Contactdetail.CardMidNameEnc != null) ? transactionEntities.DecryptText(Contactdetail.CardMidNameEnc) : string.Empty;
					DisclosureContactDetailsBKCounselingResults.mtcn = (Contactdetail.mtcn != null) ? Contactdetail.mtcn : string.Empty;
					DisclosureContactDetailsBKCounselingResults.paymentTYPE = (Contactdetail.payment_type != null) ? Contactdetail.payment_type : string.Empty;
					DisclosureContactDetailsBKCounselingResults.CurVancoRef = (Contactdetail.CurVancoRef != null) ? Convert.ToInt32(Contactdetail.CurVancoRef) : 0;
				}
				else
				{
					DisclosureContactDetailsBKCounselingResults.ClientNumber = 0;
				}
				return DisclosureContactDetailsBKCounselingResults;
			}
		}

		public UserDecsribeYourSituationHousingOnly UserDecsribeYourSituationHousingOnlyGet(int ClientNumber)
		{
			UserDecsribeYourSituationHousingOnly UserDecsribeYourSituationHousingOnlys = new UserDecsribeYourSituationHousingOnly();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					UserDecsribeYourSituationHousingOnlys.ClientNumber = ClientNumber;

					UserDecsribeYourSituationHousingOnlys.ContactReason = (!string.IsNullOrEmpty(Contactdetail.contact_reason)) ? Contactdetail.contact_reason.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.ContactComments = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MortCurrent = (Contactdetail.mort_current != null) ? Convert.ToInt32(Contactdetail.mort_current) : 0;
					UserDecsribeYourSituationHousingOnlys.HousingType = (Contactdetail.housing_type != null) ? Contactdetail.housing_type.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MosDelinq = (Contactdetail.mos_delinq != null) ? (float) Contactdetail.mos_delinq : 0;
					UserDecsribeYourSituationHousingOnlys.SizeOfHouseHold = (Contactdetail.size_of_household != null) ? Convert.ToInt32(Contactdetail.size_of_household) : 0;
					UserDecsribeYourSituationHousingOnlys.MortHolder = (Contactdetail.mort_holder != null) ? Contactdetail.mort_holder.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.LoanNumber = (Contactdetail.loan_number != null) ? Contactdetail.loan_number.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MortType = (Contactdetail.mort_type != null) ? Contactdetail.mort_type.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MortYears = (Contactdetail.mort_years != null) ? (float) Contactdetail.mort_years : 0;
					UserDecsribeYourSituationHousingOnlys.MortDate = (Contactdetail.mort_date != null) ? Contactdetail.mort_date.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MortRate = (Contactdetail.mort_rate != null) ? (float) Contactdetail.mort_rate : 0;
					UserDecsribeYourSituationHousingOnlys.RateType = (Contactdetail.rate_type != null) ? Contactdetail.rate_type.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.LastContactDesc = (Contactdetail.last_contact_desc != null) ? Contactdetail.last_contact_desc.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.OrigBal = (Contactdetail.orig_bal != null) ? (float) Contactdetail.orig_bal : 0;
					UserDecsribeYourSituationHousingOnlys.Owehome = (Contactdetail.owe_home != null) ? (float) Contactdetail.owe_home : 0;
					UserDecsribeYourSituationHousingOnlys.ValHome = (Contactdetail.val_home != null) ? (float) Contactdetail.val_home : 0;
					UserDecsribeYourSituationHousingOnlys.MoPmt = (Contactdetail.mo_pmt != null) ? (float) Contactdetail.mo_pmt : 0;
					UserDecsribeYourSituationHousingOnlys.AmtAvail = (Contactdetail.amt_avail != null) ? (float) Contactdetail.amt_avail : 0;
					UserDecsribeYourSituationHousingOnlys.RepayPlan = (Contactdetail.repay_plan != null) ? Contactdetail.repay_plan.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.PRIServID = (Contactdetail.PriServID != null) ? Convert.ToInt32(Contactdetail.PriServID) : 0;
					UserDecsribeYourSituationHousingOnlys.SECServID = (Contactdetail.SecServID != null) ? Convert.ToInt32(Contactdetail.SecServID) : 0;
					UserDecsribeYourSituationHousingOnlys.SecondaryAmt = (Contactdetail.secondary_amt != null) ? (float) Contactdetail.secondary_amt : 0;
					UserDecsribeYourSituationHousingOnlys.SecondaryStatus = (Contactdetail.secondary_status != null) ? Contactdetail.secondary_status.ToString().Trim() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.LastContactDate = (Contactdetail.last_contact_date != null) ? Contactdetail.last_contact_date.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.ContactState = (Contactdetail.contact_state != null) ? Contactdetail.contact_state.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.PROP4Sale = (Contactdetail.Prop4Sale != null) ? Contactdetail.Prop4Sale.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.NOTE4Close = (Contactdetail.Note4Close != null) ? Contactdetail.Note4Close.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.WHOInHouse = (Contactdetail.WhoInHouse != null) ? Contactdetail.WhoInHouse.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.LoanNumber2 = (Contactdetail.loan_number2 != null) ? Contactdetail.loan_number2.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.MAKEHomeAff = (Contactdetail.MakeHomeAff != null) ? Contactdetail.MakeHomeAff.ToString() : string.Empty;
					UserDecsribeYourSituationHousingOnlys.ClientType = (Contactdetail.ClientType != null) ? Contactdetail.ClientType.ToString() : string.Empty;

					//end
					//UserDecsribeYourSituationHousingOnlys.ClientNumber = ClientNumber;

					//UserDecsribeYourSituationHousingOnlys.ContactComments = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					//UserDecsribeYourSituationHousingOnlys.ContactReason = (Contactdetail.contact_reason != null) ? Contactdetail.contact_reason : string.Empty;
					//UserDecsribeYourSituationHousingOnlys.SizeOfHouseHold = Convert.ToInt32(Contactdetail.size_of_household);
					//UserDecsribeYourSituationHousingOnlys.MosDelinq = Convert.ToInt32(Contactdetail.mos_delinq);
					//UserDecsribeYourSituationHousingOnlys.MortCurrent = Convert.ToInt32(Contactdetail.mort_current);
					//UserDecsribeYourSituationHousingOnlys.HousingType = (!string.IsNullOrEmpty(Contactdetail.housing_type.ToString())) ? Contactdetail.housing_type.ToString() : string.Empty;
				}
				else
				{
					UserDecsribeYourSituationHousingOnlys.ClientNumber = 0;
				}
				return UserDecsribeYourSituationHousingOnlys;
			}
		}

		public UserMonthlyIncome UserMonthlyIncomeGet(int ClientNumber)
		{
			UserMonthlyIncome Income = new UserMonthlyIncome();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();
				if(Contactdetail != null)
				{
					if(Contactdetail.monthly_gross_income == null)
						Income.GrossIncome = 0;
					else
						Income.GrossIncome = (float) Contactdetail.monthly_gross_income;

					if(Contactdetail.monthly_net_income == null)
						Income.NetIncome = 0;
					else
						Income.NetIncome = (float) Contactdetail.monthly_net_income;

					if(Contactdetail.monthly_other_income == null)
						Income.OtherIncome = 0;
					else
						Income.OtherIncome = (float) Contactdetail.monthly_other_income;

					if(Contactdetail.monthly_parttime_income == null)
						Income.PartTimeGrossIncome = 0;
					else
						Income.PartTimeGrossIncome = (float) Contactdetail.monthly_parttime_income;
					if(Contactdetail.monthly_alimony_income == null)
						Income.AlimonyIncome = 0;
					else
						Income.AlimonyIncome = (float) Contactdetail.monthly_alimony_income;

					if(Contactdetail.monthly_childsupport_income == null)
						Income.ChildSupportIncome = 0;
					else
						Income.ChildSupportIncome = (float) Contactdetail.monthly_childsupport_income;
					if(Contactdetail.monthly_govtassist_income == null)
						Income.GovAssistIncome = 0;
					else
						Income.GovAssistIncome = (float) Contactdetail.monthly_govtassist_income;

					if(Contactdetail.monthly_gross_income2 == null)
						Income.GrossIncome2 = 0;
					else
						Income.GrossIncome2 = (float) Contactdetail.monthly_gross_income2;

					if(Contactdetail.monthly_net_income2 == null)
						Income.NetIncome2 = 0;
					else
						Income.NetIncome2 = (float) Contactdetail.monthly_net_income2;

					if(Contactdetail.monthly_other_income2 == null)
						Income.OtherIncome2 = 0;
					else
						Income.OtherIncome2 = (float) Contactdetail.monthly_other_income2;

					if(Contactdetail.monthly_parttime_income2 == null)
						Income.PartTimeGrossIncome2 = 0;
					else
						Income.PartTimeGrossIncome2 = (float) Contactdetail.monthly_parttime_income2;

					if(Contactdetail.monthly_alimony_income2 == null)
						Income.AlimonyIncome2 = 0;
					else
						Income.AlimonyIncome2 = (float) Contactdetail.monthly_alimony_income2;

					if(Contactdetail.monthly_childsupport_income2 == null)
						Income.ChildSupportIncome2 = 0;
					else
						Income.ChildSupportIncome2 = (float) Contactdetail.monthly_childsupport_income2;

					if(Contactdetail.monthly_govtassist_income2 == null)
						Income.GovAssistIncome2 = 0;
					else
						Income.GovAssistIncome2 = (float) Contactdetail.monthly_govtassist_income2;

					if(Contactdetail.MonthlyPartNet == null)
						Income.PartTimeNetIncome = 0;
					else
						Income.PartTimeNetIncome = (float) Contactdetail.MonthlyPartNet;

					if(Contactdetail.MonthlyPartNet2 == null)
						Income.PartTimeNetIncome2 = 0;
					else
						Income.PartTimeNetIncome2 = (float) Contactdetail.MonthlyPartNet2;
				}
				return Income;
			}
		}

		public UserMonthlyIncomeResult UserMonthlyIncomeUpadte(UserMonthlyIncome userDocumentationIncome)
		{
			contactdetail Contactdetail = null;
			UserMonthlyIncomeResult Result = new UserMonthlyIncomeResult();

			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == userDocumentationIncome.ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.monthly_gross_income = userDocumentationIncome.GrossIncome;
						Contactdetail.monthly_net_income = userDocumentationIncome.NetIncome;
						Contactdetail.monthly_other_income = userDocumentationIncome.OtherIncome;
						Contactdetail.monthly_parttime_income = userDocumentationIncome.PartTimeGrossIncome;
						Contactdetail.monthly_alimony_income = userDocumentationIncome.AlimonyIncome;
						Contactdetail.monthly_childsupport_income = userDocumentationIncome.ChildSupportIncome;
						Contactdetail.monthly_govtassist_income = userDocumentationIncome.GovAssistIncome;
						Contactdetail.monthly_gross_income2 = userDocumentationIncome.GrossIncome2;
						Contactdetail.monthly_net_income2 = userDocumentationIncome.NetIncome2;
						Contactdetail.monthly_other_income2 = userDocumentationIncome.OtherIncome2;
						Contactdetail.monthly_parttime_income2 = userDocumentationIncome.PartTimeGrossIncome2;
						Contactdetail.monthly_alimony_income2 = userDocumentationIncome.AlimonyIncome2;
						Contactdetail.monthly_childsupport_income2 = userDocumentationIncome.ChildSupportIncome2;
						Contactdetail.monthly_govtassist_income2 = userDocumentationIncome.GovAssistIncome2;
						Contactdetail.MonthlyPartNet = (int) userDocumentationIncome.PartTimeNetIncome;
						Contactdetail.MonthlyPartNet2 = (int) userDocumentationIncome.PartTimeNetIncome2;

						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}

		public UserAssetsLiabilities UserAssetLiabilitiesGet(int ClientNumber)
		{
			UserAssetsLiabilities AssetsLiabilities = new UserAssetsLiabilities();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();
				if(Contactdetail != null)
				{
					if(Contactdetail.val_home == null)
						AssetsLiabilities.ValHome = 0;
					else
						AssetsLiabilities.ValHome = (float) Contactdetail.val_home;

					if(Contactdetail.val_car == null)
						AssetsLiabilities.ValCar = 0;
					else
						AssetsLiabilities.ValCar = (float) Contactdetail.val_car;

					if(Contactdetail.val_other == null)
						AssetsLiabilities.ValOther = 0;
					else
						AssetsLiabilities.ValOther = (float) Contactdetail.val_other;

					if(Contactdetail.owe_home == null)
						AssetsLiabilities.OweHome = 0;
					else
						AssetsLiabilities.OweHome = (float) Contactdetail.owe_home;

					if(Contactdetail.owe_car == null)
						AssetsLiabilities.OweCar = 0;
					else
						AssetsLiabilities.OweCar = (float) Contactdetail.owe_car;

					if(Contactdetail.secondary_amt == null)
						AssetsLiabilities.SecondaryAmt = 0;
					else
						AssetsLiabilities.SecondaryAmt = (float) Contactdetail.secondary_amt;

					if(Contactdetail.owe_other == null)
						AssetsLiabilities.OweOther = 0;
					else
						AssetsLiabilities.OweOther = (float) Contactdetail.owe_other;

					if(Contactdetail.mort_rate == null)
						AssetsLiabilities.MortRate = 0;
					else
						AssetsLiabilities.MortRate = (float) Contactdetail.mort_rate;

					if(Contactdetail.val_prop == null)
						AssetsLiabilities.ValProp = 0;
					else
						AssetsLiabilities.ValProp = (float) Contactdetail.val_prop;

					if(Contactdetail.val_ret == null)
						AssetsLiabilities.ValRet = 0;
					else
						AssetsLiabilities.ValRet = (float) Contactdetail.val_ret;

					if(Contactdetail.val_sav == null)
						AssetsLiabilities.ValSav = 0;
					else
						AssetsLiabilities.ValSav = (float) Contactdetail.val_sav;

					if(Contactdetail.val_sav == null)
						AssetsLiabilities.ModForm5 = 0;
					else
						AssetsLiabilities.ModForm5 = (int) Contactdetail.ModForm5.GetValueOrDefault(0);

					if(Contactdetail.housing_type == null)
						AssetsLiabilities.HousingType = "";
					else
						AssetsLiabilities.HousingType = Convert.ToString(Contactdetail.housing_type);

				}
				else
				{
					AssetsLiabilities.ClientNumber = 0;
				}
				AssetsLiabilities.CreditorTotals = CreditorTotalsGet(ClientNumber);
				return AssetsLiabilities;
			}
		}

		public UserAssetsLiabilitiesResult UserAssetsLiabilitiesUpdate(UserAssetsLiabilities AssetLiabilities)
		{
			contactdetail Contactdetail = null;
			UserAssetsLiabilitiesResult Result = new UserAssetsLiabilitiesResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == AssetLiabilities.ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.val_home = AssetLiabilities.ValHome;
						Contactdetail.val_car = AssetLiabilities.ValCar;
						Contactdetail.val_other = AssetLiabilities.ValOther;
						Contactdetail.owe_home = AssetLiabilities.OweHome;
						Contactdetail.owe_car = AssetLiabilities.OweCar;
						Contactdetail.secondary_amt = AssetLiabilities.SecondaryAmt;
						Contactdetail.owe_other = AssetLiabilities.OweOther;
						Contactdetail.mort_rate = AssetLiabilities.MortRate;
						Contactdetail.val_prop = AssetLiabilities.ValProp;
						Contactdetail.val_ret = AssetLiabilities.ValRet;
						Contactdetail.val_sav = AssetLiabilities.ValSav;
						Contactdetail.ModForm5 = AssetLiabilities.ModForm5;
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = AssetLiabilities.ClientNumber;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
				return Result;
			}
		}

		public UserReasonForHelpResult UserReasonForHelpUpdate(UserReasonForHelp UsersReasonForHelp)
		{
			UserReasonForHelpResult Result = new UserReasonForHelpResult();
			contactdetail Contactdetail = null;

			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserReasonForHelp obj = new UserReasonForHelp();
				Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == UsersReasonForHelp.ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					Contactdetail.reason_for_help = UsersReasonForHelp.ReasonForHelp;
					Contactdetail.monthly_disposable_income = UsersReasonForHelp.MonthlyDisposableIncome;
					Contactdetail.monthly_expenses = UsersReasonForHelp.MonthlyExpenses;
					try
					{
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = UsersReasonForHelp.ClientNumber;

					}
					catch(Exception)
					{
						Result.IsSuccessful = false;
						Result.ClientNumber = 0;
						// Result.Messages="error occured while".to;
					}
				}
			}
			return Result;
		}

		public FormSaveInforResult SaveFormInfo(FormSaveInfor FormSaveInfos)
		{
			FormSaveInforResult Result = new FormSaveInforResult();
			contactdetail Contactdetail = null;

			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
					 (
							from Entity in Entities.contactdetails
							where Entity.client_number == FormSaveInfos.ClientNumber
							select Entity
						 ).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					Contactdetail.ModForm1 = FormSaveInfos.Form1;
					Contactdetail.ModForm3 = FormSaveInfos.Form3;
					Contactdetail.ModForm4 = FormSaveInfos.Form4;
					Contactdetail.ModDebtMnt = FormSaveInfos.Form5;
					Contactdetail.ModForm7 = FormSaveInfos.Form7;
					Entities.SubmitChanges();
					Result.IsSuccessful = true;
					Result.ClientNumber = FormSaveInfos.ClientNumber;
				}
				else
				{
					Result.IsSuccessful = false;
					Result.ClientNumber = FormSaveInfos.ClientNumber;
				}
			}
			return Result;
		}

		public FormSaveInfor SavedFormInfoGet(int ClientNumber)
		{
			FormSaveInfor Result = new FormSaveInfor();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
					 (
							from Entity in Entities.contactdetails
							where Entity.client_number == ClientNumber
							select Entity
						 ).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					Result.Form1 = (Contactdetail.ModForm1 != null) ? Convert.ToInt32(Contactdetail.ModForm1) : 0;
					Result.Form3 = (Contactdetail.ModForm3 != null) ? Convert.ToInt32(Contactdetail.ModForm3) : 0;
					Result.Form4 = (Contactdetail.ModForm4 != null) ? Convert.ToInt32(Contactdetail.ModForm4) : 0;
					Result.Form5 = (Contactdetail.ModDebtMnt != null) ? Convert.ToInt32(Contactdetail.ModDebtMnt) : 0;
					Result.Form7 = (Contactdetail.ModForm7 != null) ? Convert.ToInt32(Contactdetail.ModForm7) : 0;
					Result.ClientNumber = ClientNumber;
				}
				else
				{
					Result.ClientNumber = 0;
				}
			}
			return Result;
		}

		public UserDebtListings[] UserDebtListingsGet(int ClientNumber)
		{
			UserDebtListings[] UsersDebtListings = null;

			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				entities.OpenEncryptionKey();
				UsersDebtListings =
				(
								from entity in entities.creditordetails
								where (entity.client_number == ClientNumber)
								select new UserDebtListings
								{
									CreditorId = entity.id,
									CreditorName = entity.creditor_name,
									CreditorPayments = (float) entity.creditor_payments,
									CreditorBal = (float) entity.creditor_bal,
									CreditorIntrate = (float) entity.creditor_intrate,
									//ClientNumber = Convert.ToInt16(entity.client_number),
									PreAcntNoEncs = entities.DecryptText(entity.PreAcntNoEnc),
									CreditorJointAcct = entity.creditor_jointacct,
									creditorPastDue = entity.creditor_past_due,

								}
				).ToArray();
			}
			return UsersDebtListings;
		}

		public UserDebtListingsHousingOnly[] UserDebtListingsHousingOnlyGet(int ClientNumber)
		{
			UserDebtListingsHousingOnly[] UserDebtListingsHousingOnly = null;

			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				UserDebtListingsHousingOnly =
				(
								from entity in entities.creditordetails
								where (entity.client_number == ClientNumber)
								select new UserDebtListingsHousingOnly
								{
									CreditorId = entity.id,
									CreditorName = entity.creditor_name,
									CreditorPayments = (float) entity.creditor_payments,
									CreditorBal = (float) entity.creditor_bal,
									CreditorIntrate = (float) entity.creditor_intrate,
									//ClientNumber = Convert.ToInt16(entity.client_number),
									PreAcntNoEncs = entities.DecryptText(entity.PreAcntNoEnc),

								}
				).ToArray();
			}
			return UserDebtListingsHousingOnly;
		}

		public UserDebtListingsResult UserDebtListingsDelete(int CreditorId)
		{
			UserDebtListingsResult UserDebtListingsResults = new UserDebtListingsResult();
			creditordetail Creditordetail = null;
			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				Creditordetail =
				(
						from entity in entities.creditordetails
						where (entity.id == CreditorId)
						select entity

				).FirstOrDefault<creditordetail>();
				if(Creditordetail != null)
				{
					entities.creditordetails.DeleteOnSubmit(Creditordetail);

					entities.SubmitChanges();
					UserDebtListingsResults.IsSuccessful = true;
					UserDebtListingsResults.ClientNumber = 0;
				}
				else
				{
					UserDebtListingsResults.IsSuccessful = false;
					UserDebtListingsResults.ClientNumber = 0;
				}
			}
			return UserDebtListingsResults;
		}

		public UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyDelete(int CreditorId)
		{
			UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyResults = new UserDebtListingsHousingOnlyResult();
			creditordetail Creditordetail = null;
			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				Creditordetail =
				(
								from entity in entities.creditordetails
								where (entity.id == CreditorId)
								select entity

				).FirstOrDefault<creditordetail>();
				if(Creditordetail != null)
				{
					entities.creditordetails.DeleteOnSubmit(Creditordetail);

					entities.SubmitChanges();
					UserDebtListingsHousingOnlyResults.IsSuccessful = true;
					UserDebtListingsHousingOnlyResults.ClientNumber = 0;
				}
				else
				{
					UserDebtListingsHousingOnlyResults.IsSuccessful = false;
					UserDebtListingsHousingOnlyResults.ClientNumber = 0;
				}
			}
			return UserDebtListingsHousingOnlyResults;
		}

		public UserDebtListingsResult UserDebtListingsAddupdate(UserDebtListings UsersDebtListings)
		{
			creditordetail Creditordetails = null;
			UserDebtListingsResult UsersDebtListingsResult = new UserDebtListingsResult();
			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				Creditordetails =
				(
						from entity in entities.creditordetails
						where entity.id == UsersDebtListings.CreditorId
						select entity

				).FirstOrDefault<creditordetail>();

				if(Creditordetails == null) // New record
				{
					Creditordetails = new creditordetail();
					Creditordetails.client_number = UsersDebtListings.ClientNumber;
					Creditordetails.creditor_name = UsersDebtListings.CreditorName;
					Creditordetails.creditor_past_due = UsersDebtListings.creditorPastDue;
					Creditordetails.creditor_bal = UsersDebtListings.CreditorBal;
					Creditordetails.creditor_payments = UsersDebtListings.CreditorPayments;
					Creditordetails.creditor_intrate = UsersDebtListings.CreditorIntrate;
					Creditordetails.creditor_jointacct = UsersDebtListings.CreditorJointAcct;
					Creditordetails.PreAcntNoEnc = entities.EncryptText(UsersDebtListings.PreAcntNoEncs);
					Creditordetails.LastModDTS = UsersDebtListings.LastModDTS;
					entities.creditordetails.InsertOnSubmit(Creditordetails);
					entities.SubmitChanges();
					UsersDebtListingsResult.IsSuccessful = true;
					UsersDebtListingsResult.ClientNumber = UsersDebtListings.ClientNumber;
					UsersDebtListingsResult.CreditorId = Creditordetails.id;
				}
				else // update
				{
					Creditordetails.client_number = UsersDebtListings.ClientNumber;
					Creditordetails.creditor_name = UsersDebtListings.CreditorName;
					Creditordetails.creditor_past_due = UsersDebtListings.creditorPastDue;
					Creditordetails.creditor_bal = UsersDebtListings.CreditorBal;
					Creditordetails.creditor_payments = UsersDebtListings.CreditorPayments;
					Creditordetails.creditor_intrate = UsersDebtListings.CreditorIntrate;
					Creditordetails.creditor_jointacct = UsersDebtListings.CreditorJointAcct;
					Creditordetails.PreAcntNoEnc = entities.EncryptText(UsersDebtListings.PreAcntNoEncs);
					Creditordetails.LastModDTS = UsersDebtListings.LastModDTS;
					entities.SubmitChanges();
					UsersDebtListingsResult.IsSuccessful = true;
					UsersDebtListingsResult.ClientNumber = UsersDebtListings.ClientNumber;
					UsersDebtListingsResult.CreditorId = Creditordetails.id;
				}
			}
			return UsersDebtListingsResult;
		}

		public UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyAddupdate(UserDebtListingsHousingOnly UserDebtListingsHousingOnly)
		{
			creditordetail Creditordetails = null;
			UserDebtListingsHousingOnlyResult UserDebtListingsHousingOnlyResults = new UserDebtListingsHousingOnlyResult();
			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				Creditordetails =
				(
								from entity in entities.creditordetails
								where entity.id == UserDebtListingsHousingOnly.CreditorId
								select entity

				).FirstOrDefault<creditordetail>();

				if(Creditordetails == null) // New record
				{
					Creditordetails = new creditordetail();
					Creditordetails.client_number = UserDebtListingsHousingOnly.ClientNumber;
					Creditordetails.creditor_name = UserDebtListingsHousingOnly.CreditorName;
					Creditordetails.creditor_past_due = UserDebtListingsHousingOnly.creditorPastDue;
					Creditordetails.creditor_bal = UserDebtListingsHousingOnly.CreditorBal;
					Creditordetails.creditor_payments = UserDebtListingsHousingOnly.CreditorPayments;
					Creditordetails.creditor_intrate = UserDebtListingsHousingOnly.CreditorIntrate;
					//Creditordetails.CrAcntNoEnc = UserDebtListingsHousingOnly.CrAcntNoEnc;
					Creditordetails.creditor_jointacct = UserDebtListingsHousingOnly.CreditorJointAcct;
					Creditordetails.PreAcntNoEnc = UserDebtListingsHousingOnly.PreAcntNoEnc;
					Creditordetails.LastModDTS = UserDebtListingsHousingOnly.LastModDTS;
					entities.creditordetails.InsertOnSubmit(Creditordetails);
					entities.SubmitChanges();
					UserDebtListingsHousingOnlyResults.IsSuccessful = true;
					UserDebtListingsHousingOnlyResults.ClientNumber = UserDebtListingsHousingOnly.ClientNumber;
					//UserDebtListingsHousingOnlyResult.IsSuccessful = false;
					//UserDebtListingsHousingOnlyResult.ClientNumber = 0;
				}
				else // update
				{
					Creditordetails.client_number = UserDebtListingsHousingOnly.ClientNumber;
					Creditordetails.creditor_name = UserDebtListingsHousingOnly.CreditorName;
					Creditordetails.creditor_past_due = UserDebtListingsHousingOnly.creditorPastDue;
					Creditordetails.creditor_bal = UserDebtListingsHousingOnly.CreditorBal;
					Creditordetails.creditor_payments = UserDebtListingsHousingOnly.CreditorPayments;
					Creditordetails.creditor_intrate = UserDebtListingsHousingOnly.CreditorIntrate;
					//Creditordetails.CrAcntNoEnc = UsersDebtListings.CrAcntNoEnc;
					Creditordetails.creditor_jointacct = UserDebtListingsHousingOnly.CreditorJointAcct;
					Creditordetails.PreAcntNoEnc = UserDebtListingsHousingOnly.PreAcntNoEnc;
					Creditordetails.LastModDTS = UserDebtListingsHousingOnly.LastModDTS;
					entities.SubmitChanges();
					UserDebtListingsHousingOnlyResults.IsSuccessful = true;
					UserDebtListingsHousingOnlyResults.ClientNumber = UserDebtListingsHousingOnly.ClientNumber;
					//UsersDebtListingsResult.IsSuccessful = false;
					//UsersDebtListingsResult.ClientNumber = 0;
				}
			}
			return UserDebtListingsHousingOnlyResults;
		}

		public UserCreditorTotals CreditorTotalsGet(int ClientNumber)
		{
			UserCreditorTotals CreditorTotals = new UserCreditorTotals();
			UserDebtListings[] DebtList = UserDebtListingsGet(ClientNumber);
			CreditorTotals.Balance = 0;
			CreditorTotals.Payments = 0;
			CreditorTotals.Count = 0;
			foreach(var DebtItem in DebtList)
			{
				CreditorTotals.Balance += DebtItem.CreditorBal;
				CreditorTotals.Payments += DebtItem.CreditorPayments;
				CreditorTotals.Count++;
			}

			return CreditorTotals;
		}

		#endregion

		#region BCH site implementation
		public UserDecsribeYourSituationBCH UserDecsribeYourSituationBCHGet(int ClientNumber)
		{
			UserDecsribeYourSituationBCH UserDecsribeYourSituationBCHResults = new UserDecsribeYourSituationBCH();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					UserDecsribeYourSituationBCHResults.ClientNumber = ClientNumber;

					UserDecsribeYourSituationBCHResults.ContactReason = (!string.IsNullOrEmpty(Contactdetail.contact_reason)) ? Contactdetail.contact_reason.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.ContactComments = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					UserDecsribeYourSituationBCHResults.MortCurrent = Contactdetail.mort_current;
					UserDecsribeYourSituationBCHResults.HousingType = (Contactdetail.housing_type != null) ? Contactdetail.housing_type.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.MosDelinq = (Contactdetail.mos_delinq != null) ? (float) Contactdetail.mos_delinq : 0;
					UserDecsribeYourSituationBCHResults.SizeOfHouseHold = (Contactdetail.size_of_household != null) ? Convert.ToInt32(Contactdetail.size_of_household) : 0;
					UserDecsribeYourSituationBCHResults.MortCurrent = Contactdetail.mort_current;
					UserDecsribeYourSituationBCHResults.MortHolder = (Contactdetail.mort_holder != null) ? Contactdetail.mort_holder.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.LoanNumber = (Contactdetail.loan_number != null) ? Contactdetail.loan_number.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.MortType = (Contactdetail.mort_type != null) ? Contactdetail.mort_type.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.MortYears = (Contactdetail.mort_years != null) ? (float) Contactdetail.mort_years : 0;
					UserDecsribeYourSituationBCHResults.MortDate = (Contactdetail.mort_date != null) ? Contactdetail.mort_date.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.MortRate = (Contactdetail.mort_rate != null) ? (float) Contactdetail.mort_rate : 0;
					UserDecsribeYourSituationBCHResults.RateType = (Contactdetail.rate_type != null) ? Contactdetail.rate_type.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.LastContactDesc = (Contactdetail.last_contact_desc != null) ? Contactdetail.last_contact_desc.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.OrigBal = (Contactdetail.orig_bal != null) ? (float) Contactdetail.orig_bal : 0;
					UserDecsribeYourSituationBCHResults.Owehome = (Contactdetail.owe_home != null) ? (float) Contactdetail.owe_home : 0;
					UserDecsribeYourSituationBCHResults.ValHome = (Contactdetail.val_home != null) ? (float) Contactdetail.val_home : 0;
					UserDecsribeYourSituationBCHResults.MoPmt = (Contactdetail.mo_pmt != null) ? (float) Contactdetail.mo_pmt : 0;
					UserDecsribeYourSituationBCHResults.AmtAvail = (Contactdetail.amt_avail != null) ? (float) Contactdetail.amt_avail : 0;
					UserDecsribeYourSituationBCHResults.RepayPlan = (Contactdetail.repay_plan != null) ? Contactdetail.repay_plan.ToString().Trim() : string.Empty;
					UserDecsribeYourSituationBCHResults.PRIServID = (Contactdetail.PriServID != null) ? Convert.ToInt32(Contactdetail.PriServID) : 0;
					UserDecsribeYourSituationBCHResults.SecondaryHolder = (Contactdetail.secondary_holder != null) ? Contactdetail.secondary_holder.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.SECServID = (Contactdetail.SecServID != null) ? Convert.ToInt32(Contactdetail.SecServID) : 0;
					UserDecsribeYourSituationBCHResults.SecondaryAmt = (Contactdetail.secondary_amt != null) ? (float) Contactdetail.secondary_amt : 0;
					UserDecsribeYourSituationBCHResults.SecondaryStatus = (Contactdetail.secondary_status != null) ? Contactdetail.secondary_status.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.LastContactDate = (Contactdetail.last_contact_date != null) ? Contactdetail.last_contact_date.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.ContactState = (Contactdetail.contact_state != null) ? Contactdetail.contact_state.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.PROP4Sale = (Contactdetail.Prop4Sale != null) ? Contactdetail.Prop4Sale.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.NOTE4Close = (Contactdetail.Note4Close != null) ? Contactdetail.Note4Close.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.WHOInHouse = (Contactdetail.WhoInHouse != null) ? Contactdetail.WhoInHouse.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.LoanNumber2 = (Contactdetail.loan_number2 != null) ? Contactdetail.loan_number2.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.MAKEHomeAff = (Contactdetail.MakeHomeAff != null) ? Contactdetail.MakeHomeAff.ToString() : string.Empty;
					UserDecsribeYourSituationBCHResults.ClientType = (Contactdetail.ClientType != null) ? Contactdetail.ClientType.ToString() : string.Empty;
                    UserDecsribeYourSituationBCHResults.ActiveBankruptcy = (Contactdetail.ActiveBankruptcy != null) ? Contactdetail.ActiveBankruptcy.ToString() : string.Empty;
				}
				else
				{
					UserDecsribeYourSituationBCHResults.ClientNumber = 0;
				}
				return UserDecsribeYourSituationBCHResults;
			}
		}

		public UserDecsribeYourSituationBCHResult UserDecsribeYourSituationBCHAddUpdate(UserDecsribeYourSituationBCH userDecsribeYourSituationBCH)
		{
			string Message = string.Empty;
			UserDecsribeYourSituationBCHResult Result = new UserDecsribeYourSituationBCHResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
							 from contat in transactionEntities.contactdetails
							 where contat.client_number == userDecsribeYourSituationBCH.ClientNumber
							 select contat
							).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						Contactdetail.contact_reason = userDecsribeYourSituationBCH.ContactReason;
						Contactdetail.contact_comments = userDecsribeYourSituationBCH.ContactComments;
						Contactdetail.housing_type = !string.IsNullOrEmpty(userDecsribeYourSituationBCH.HousingType) ? (char?) userDecsribeYourSituationBCH.HousingType[0] : null;
						Contactdetail.size_of_household = userDecsribeYourSituationBCH.SizeOfHouseHold;
						Contactdetail.mort_current = userDecsribeYourSituationBCH.MortCurrent;
						Contactdetail.mos_delinq = userDecsribeYourSituationBCH.MosDelinq;

						Contactdetail.PriServID = userDecsribeYourSituationBCH.PRIServID;
						// Check if the primary mortgage holder is an other (unknown) servicer
						if(IsOtherServicer(userDecsribeYourSituationBCH.PRIServID))
						{
							Contactdetail.mort_holder = userDecsribeYourSituationBCH.MortHolder;
						}
						else
						{
							// The mort_holder must be set to the known servicer name when it's selected
							Contactdetail.mort_holder = GetServicerName(userDecsribeYourSituationBCH.PRIServID);
						}
						Contactdetail.loan_number = userDecsribeYourSituationBCH.LoanNumber;
						Contactdetail.mort_type = userDecsribeYourSituationBCH.MortType;
						Contactdetail.mort_years = userDecsribeYourSituationBCH.MortYears;
						Contactdetail.mort_date = userDecsribeYourSituationBCH.MortDate;
						Contactdetail.mort_rate = userDecsribeYourSituationBCH.MortRate;
						Contactdetail.rate_type = userDecsribeYourSituationBCH.RateType;
						Contactdetail.mort_current = userDecsribeYourSituationBCH.MortCurrent;
						Contactdetail.last_contact_desc = userDecsribeYourSituationBCH.LastContactDesc;
						Contactdetail.orig_bal = userDecsribeYourSituationBCH.OrigBal;
						Contactdetail.owe_home = userDecsribeYourSituationBCH.Owehome;
						Contactdetail.val_home = userDecsribeYourSituationBCH.ValHome;
						Contactdetail.mo_pmt = userDecsribeYourSituationBCH.MoPmt;
						Contactdetail.amt_avail = userDecsribeYourSituationBCH.AmtAvail;
						Contactdetail.repay_plan = userDecsribeYourSituationBCH.RepayPlan;
						Contactdetail.SecServID = userDecsribeYourSituationBCH.SECServID;
						// Check if the 2nd mortgage holder is an other (unknown) servicer
						if(IsOtherServicer(userDecsribeYourSituationBCH.SECServID))
						{
							Contactdetail.secondary_holder = userDecsribeYourSituationBCH.SecondaryHolder;
						}
						else
						{
							// The secondary_holder must be set to the known servicer name when it's selected
							Contactdetail.secondary_holder = GetServicerName(userDecsribeYourSituationBCH.SECServID);
						}
						Contactdetail.secondary_amt = userDecsribeYourSituationBCH.SecondaryAmt;
						Contactdetail.secondary_status = userDecsribeYourSituationBCH.SecondaryStatus;
						Contactdetail.last_contact_date = userDecsribeYourSituationBCH.LastContactDate;
						Contactdetail.contact_state = userDecsribeYourSituationBCH.ContactState;
						Contactdetail.Prop4Sale = userDecsribeYourSituationBCH.PROP4Sale;
						Contactdetail.Note4Close = userDecsribeYourSituationBCH.NOTE4Close;
                 
						Contactdetail.WhoInHouse = userDecsribeYourSituationBCH.WHOInHouse;
						Contactdetail.loan_number2 = userDecsribeYourSituationBCH.LoanNumber2;
						Contactdetail.MakeHomeAff = userDecsribeYourSituationBCH.MAKEHomeAff;
						Contactdetail.ClientType = userDecsribeYourSituationBCH.ClientType;
						Contactdetail.HpfID = userDecsribeYourSituationBCH.HpfID;
                        Contactdetail.ActiveBankruptcy = userDecsribeYourSituationBCH.ActiveBankruptcy;

						transactionEntities.SubmitChanges();
						Result.IsSuccessful = true;
						Result.ClientNumber = userDecsribeYourSituationBCH.ClientNumber;
					}
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
				return Result;
			}
		}

		public RegistrationBKCounseling RegistrationBKCounselingGet(int ClientNumber)
		{
			contactdetail Contactdetail = null;
			RegistrationBKCounseling RegistrationBKCounselings = new RegistrationBKCounseling();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from Entity in Entities.contactdetails
						 where Entity.client_number == ClientNumber
						 select Entity
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						RegistrationBKCounselings.ClientNumber = Contactdetail.client_number;
						RegistrationBKCounselings.FIRMid = (Contactdetail.firm_id != null) ? Contactdetail.firm_id.ToString() : string.Empty;
						RegistrationBKCounselings.ATTYSit = (Contactdetail.AttySit != null) ? Contactdetail.AttySit.ToString() : string.Empty;
						RegistrationBKCounselings.ATTYname = (Contactdetail.atty_name != null) ? Contactdetail.atty_name.ToString() : string.Empty;
						RegistrationBKCounselings.ATTYemail = (Contactdetail.atty_email != null) ? Contactdetail.atty_email.ToString() : string.Empty;
						RegistrationBKCounselings.FIRMname = (Contactdetail.firm_name != null) ? Contactdetail.firm_name : string.Empty;
					}
					catch(Exception)
					{
					}
				}
			}
			return RegistrationBKCounselings;
		}

		public UserDebtManagementComment UserCommentGet(int ClientNumber)
		{
			UserDebtManagementComment UserDebtComment = null;
			using(CredabilityDataContext entities = new CredabilityDataContext())
			{
				UserDebtComment =
				(
						from entity in entities.contactdetails
						where (entity.client_number == ClientNumber)
						select new UserDebtManagementComment
						{
							ClientNumber = entity.client_number,
							UserComments = entity.contact_comments,
						}
				).FirstOrDefault<UserDebtManagementComment>();
			}
			return UserDebtComment;
		}

		public UserDebtCommentUpdateResult UserCommentsUpdate(UserDebtCommentUpdate DebtComment)
		{
			UserDebtCommentUpdateResult Result = new UserDebtCommentUpdateResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == DebtComment.ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

					Contactdetail.contact_comments = DebtComment.UserComment;
					Contactdetail.ModDebtMnt = 1;
					transactionEntities.SubmitChanges();
					Result.IsSuccessful = true;
					Result.ClientNumber = DebtComment.ClientNumber;
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
			}
			return Result;
		}

		public RegistrationBKCounselingResult RegistrationBKCounselingAddUpdate(RegistrationBKCounseling registrationBKCounseling)
		{
			RegistrationBKCounselingResult Result = new RegistrationBKCounselingResult();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				try
				{
					Contactdetail =
					 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == registrationBKCounseling.ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();
					if(Contactdetail != null)
					{
						Contactdetail.firm_id = registrationBKCounseling.FIRMid;
						Contactdetail.AttySit = registrationBKCounseling.ATTYSit;
						Contactdetail.atty_name = registrationBKCounseling.ATTYname;
						Contactdetail.atty_email = registrationBKCounseling.ATTYemail;
						Contactdetail.firm_name = registrationBKCounseling.FIRMname;
						transactionEntities.SubmitChanges();
					}
					Result.IsSuccessful = true;
					Result.ClientNumber = registrationBKCounseling.ClientNumber;

				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
			}
			return Result;
		}

		public AfsDmpOnly AfsDmpOnlyGet(int ClientNumber)
		{
			AfsDmpOnly AfsDmpOnlyResult = new AfsDmpOnly();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();
				if(Contactdetail != null)
				{
					AfsDmpOnlyResult.ClientNumber = ClientNumber;

					AfsDmpOnlyResult.PrimaryPerson = (Contactdetail.confirm_maidenname != null) ? Contactdetail.confirm_maidenname.ToString() : string.Empty;
					AfsDmpOnlyResult.SecondaryPerson = (Contactdetail.confirm_maidenname2 != null) ? Contactdetail.confirm_maidenname2.ToString() : string.Empty;
				}
				else
				{
					AfsDmpOnlyResult.ClientNumber = 0;
				}
				return AfsDmpOnlyResult;
			}
		}

		public AfsDmpOnlyResult AfsDmpOnlyUpdate(AfsDmpOnly AfsDmpOnly)
		{
			contactdetail Contactdetail = null;
			AfsDmpOnlyResult Result = new AfsDmpOnlyResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == AfsDmpOnly.ClientNumber
						 select contat
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.confirm_maidenname = AfsDmpOnly.PrimaryPerson;
						Contactdetail.confirm_maidenname2 = AfsDmpOnly.SecondaryPerson;
						//Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}

		public DescribingYourSituationResult DescribingYourSituationUpdate(DescribingYourSituation describingYourSituation)
		{
			contactdetail Contactdetail = null;
			DescribingYourSituationResult Result = new DescribingYourSituationResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == describingYourSituation.ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						// Contactdetail.loan_number = DescribingYourSituation.CompanyName;
						Contactdetail.contact_comments = describingYourSituation.DescribeComment;
						Contactdetail.loan_number = describingYourSituation.LoanNumber;
						Contactdetail.last_contact_desc = describingYourSituation.LastContactDesc;
						Contactdetail.mort_type = describingYourSituation.MortgageType;
						Contactdetail.mort_years = (!string.IsNullOrEmpty(describingYourSituation.MortgageTerm)) ? Convert.ToInt32(describingYourSituation.MortgageTerm) : 0;
						Contactdetail.rate_type = describingYourSituation.InterestType;
						Contactdetail.mort_date = describingYourSituation.DateofMortgage;
						Contactdetail.orig_bal = (!string.IsNullOrEmpty(describingYourSituation.OriLoanBalance)) ? float.Parse(describingYourSituation.OriLoanBalance) : 0;
						Contactdetail.mort_rate = (!string.IsNullOrEmpty(describingYourSituation.InterestRate)) ? float.Parse(describingYourSituation.InterestRate) : 0;
						Contactdetail.owe_home = (!string.IsNullOrEmpty(describingYourSituation.CurrBalance)) ? float.Parse(describingYourSituation.CurrBalance) : 0;
						Contactdetail.val_home = (!string.IsNullOrEmpty(describingYourSituation.EstimateValue)) ? float.Parse(describingYourSituation.EstimateValue) : 0;
						Contactdetail.mo_pmt = (!string.IsNullOrEmpty(describingYourSituation.MonthlyPayment)) ? float.Parse(describingYourSituation.MonthlyPayment) : 0;
						Contactdetail.size_of_household = (!string.IsNullOrEmpty(describingYourSituation.PeopleLivingIn)) ? Convert.ToInt32(describingYourSituation.PeopleLivingIn) : 0;
						Contactdetail.mos_delinq = (!string.IsNullOrEmpty(describingYourSituation.IfNohowmanymonths)) ? float.Parse(describingYourSituation.IfNohowmanymonths) : 0;
						// Contactdetail.repay_plan = (!string.IsNullOrEmpty(describingYourSituation.replacementPlan) ))? Convert.ToInt32(describingYourSituation.replacementPlan) : 0 ;
						Contactdetail.contact_reason = describingYourSituation.ContactReason;
						Contactdetail.housing_type = !string.IsNullOrEmpty(describingYourSituation.RentingorBuyingHouse) ? (char?) describingYourSituation.RentingorBuyingHouse[0] : null;
						Contactdetail.mort_current = (!string.IsNullOrEmpty(describingYourSituation.AreyoucurrentonyourPayments)) ? Convert.ToInt32(describingYourSituation.AreyoucurrentonyourPayments) : 0;
						Contactdetail.amt_avail = (!string.IsNullOrEmpty(describingYourSituation.CurrloanMortCompany)) ? Convert.ToInt32(describingYourSituation.CurrloanMortCompany) : 0;
						Contactdetail.last_contact_date = describingYourSituation.LastContactDate;
						Contactdetail.repay_plan = describingYourSituation.replacementPlan;
						//Second / 
						Contactdetail.secondary_holder = describingYourSituation.SecondCompanyName;
						Contactdetail.secondary_amt = (!string.IsNullOrEmpty(describingYourSituation.Secondhomeequityloan)) ? Convert.ToInt32(describingYourSituation.Secondhomeequityloan) : 0;
						Contactdetail.secondary_status = describingYourSituation.SecondLoanStatus;

						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}

		public DescribingYourSituation DescribingYourSituationGet(int ClientNumber)
		{
			DescribingYourSituation DescribingYourSituationResult = new DescribingYourSituation();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					DescribingYourSituationResult.ClientNumber = ClientNumber;
					DescribingYourSituationResult.CompanyName = (Contactdetail.PriServID != null) ? Contactdetail.PriServID.ToString() : string.Empty;
					DescribingYourSituationResult.LoanNumber = (Contactdetail.loan_number != null) ? Contactdetail.loan_number : string.Empty;
					DescribingYourSituationResult.MortgageType = (Contactdetail.mort_type != null) ? Contactdetail.mort_type : string.Empty;
					DescribingYourSituationResult.MortgageTerm = (Contactdetail.mort_years != null) ? Contactdetail.mort_years.ToString() : string.Empty;
					DescribingYourSituationResult.InterestType = (Contactdetail.rate_type != null) ? Contactdetail.rate_type : string.Empty;
					DescribingYourSituationResult.DateofMortgage = (Contactdetail.mort_date != null) ? Contactdetail.mort_date : string.Empty;
					DescribingYourSituationResult.CurrBalance = (Contactdetail.owe_home != null) ? Contactdetail.owe_home.ToString() : string.Empty;
					DescribingYourSituationResult.EstimateValue = (Contactdetail.val_home != null) ? Contactdetail.val_home.ToString() : string.Empty;
					DescribingYourSituationResult.DescribeComment = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					DescribingYourSituationResult.OriLoanBalance = (Contactdetail.orig_bal != null) ? Contactdetail.orig_bal.ToString() : string.Empty;
					DescribingYourSituationResult.InterestRate = (Contactdetail.mort_rate != null) ? Contactdetail.mort_rate.ToString() : string.Empty;
					DescribingYourSituationResult.MonthlyPayment = (Contactdetail.mo_pmt != null) ? Contactdetail.mo_pmt.ToString() : string.Empty;
					DescribingYourSituationResult.ContactReason = (Contactdetail.contact_reason != null) ? Contactdetail.contact_reason.ToString() : string.Empty;
					DescribingYourSituationResult.Discussions = (Contactdetail.contact_comments != null) ? Contactdetail.contact_comments : string.Empty;
					DescribingYourSituationResult.PeopleLivingIn = (Contactdetail.size_of_household != null) ? Contactdetail.size_of_household.ToString() : string.Empty;
					DescribingYourSituationResult.RentingorBuyingHouse = (Contactdetail.housing_type != null) ? Contactdetail.housing_type.ToString() : string.Empty;
					DescribingYourSituationResult.IfNohowmanymonths = (Contactdetail.mos_delinq != null) ? Contactdetail.mos_delinq.ToString() : string.Empty;
					DescribingYourSituationResult.CurrloanMortCompany = (Contactdetail.amt_avail != null) ? Contactdetail.amt_avail.ToString() : string.Empty;
					DescribingYourSituationResult.SecondCompanyName = (Contactdetail.secondary_holder != null) ? Contactdetail.secondary_holder.ToString() : string.Empty;
					DescribingYourSituationResult.Secondhomeequityloan = (Contactdetail.secondary_amt != null) ? Contactdetail.secondary_amt.ToString() : string.Empty;
					DescribingYourSituationResult.SecondLoanStatus = (Contactdetail.secondary_status != null) ? Contactdetail.secondary_status.ToString() : string.Empty;
					// DescribingYourSituationResult.SecondLoanBalance = (Contactdetail.secondary_amt != null) ? Contactdetail.secondary_amt.ToString() : string.Empty;
					DescribingYourSituationResult.RentingorBuyingHouse = (Contactdetail.housing_type != null) ? Contactdetail.housing_type.ToString() : string.Empty;
					DescribingYourSituationResult.AreyoucurrentonyourPayments = (Contactdetail.mort_current != null) ? Contactdetail.mort_current.ToString() : string.Empty;
					DescribingYourSituationResult.LastContactDate = (Contactdetail.last_contact_date != null) ? Contactdetail.last_contact_date.ToString() : string.Empty;
					DescribingYourSituationResult.replacementPlan = (Contactdetail.repay_plan != null) ? Contactdetail.repay_plan.ToString() : string.Empty;
					DescribingYourSituationResult.LastContactDesc = (Contactdetail.last_contact_desc != null) ? Contactdetail.last_contact_desc.ToString() : string.Empty;
					DescribingYourSituationResult.PeopleLivingIn = (Contactdetail.size_of_household != null) ? Contactdetail.size_of_household.ToString() : string.Empty;
				}
				else
				{
					DescribingYourSituationResult.ClientNumber = 0;
				}
				return DescribingYourSituationResult;
			}
		}

		public CommonOptionDealingDmpOnlyResult CommonOptionDealingUpdate(CommonOptionDealingDmpOnly CommonOptionDealingDmpOnly)
		{
			contactdetail Contactdetail = null;
			CommonOptionDealingDmpOnlyResult Result = new CommonOptionDealingDmpOnlyResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == CommonOptionDealingDmpOnly.ClientNumber
						 select contat
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.dmp_fco_pcp_flag = CommonOptionDealingDmpOnly.YesorNo;
						//Contactdetail.dmp_fco_pcp_flag = CommonOptionDealingDmpOnly.No;
						Contactdetail.dmp_fco_pcp_date = CommonOptionDealingDmpOnly.YesDate;
						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}

		public CommonOptionDealingDmpOnly CommonOptionDealingDmpOnlyGet(int ClientNumber)
		{
			CommonOptionDealingDmpOnly CommonOptionDealingDmpOnlyResult = new CommonOptionDealingDmpOnly();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					CommonOptionDealingDmpOnlyResult.ClientNumber = ClientNumber;

					CommonOptionDealingDmpOnlyResult.YesorNo = (Contactdetail.dmp_fco_pcp_flag != null) ? Contactdetail.dmp_fco_pcp_flag.ToString() : string.Empty;
					//CommonOptionDealingDmpOnlyResult.No = (Contactdetail.dmp_fco_pcp_flag != null) ? Contactdetail.dmp_fco_pcp_flag.ToString() : string.Empty;
					CommonOptionDealingDmpOnlyResult.YesDate = (Contactdetail.dmp_fco_pcp_date != null) ? Contactdetail.dmp_fco_pcp_date.ToString() : string.Empty;
				}
				else
				{
					CommonOptionDealingDmpOnlyResult.ClientNumber = 0;
				}
				return CommonOptionDealingDmpOnlyResult;
			}
		}
		#endregion

		#region QuicCalc BK Counseling
		public QuicalcBKCounseling QuicalcBKCounselingGet(int ClientNumber)
		{
			QuicalcBKCounseling UserContactDetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserContactDetail =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new QuicalcBKCounseling
						 {
							 ClientNumber = entity.client_number,
							 ContactState = (entity.ContactStateEnc != null) ? Entities.DecryptText(entity.ContactStateEnc) : string.Empty,
							 SizeofHousehold = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
							 MonthlyGrossIncome = (entity.monthly_gross_income != null) ? (float) (entity.monthly_gross_income) : 0,
						 }
						).FirstOrDefault();
			}
			return UserContactDetail;
		}

		public QuicalcBKCounselingResult QuicalcBKCounselingUpdate(QuicalcBKCounseling quicalcBKCounseling)
		{
			contactdetail Contactdetail = null;

			QuicalcBKCounselingResult Result = new QuicalcBKCounselingResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == quicalcBKCounseling.ClientNumber
						 select contat
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.ContactStateEnc = Entities.EncryptText(quicalcBKCounseling.ContactState);
						Contactdetail.size_of_household = quicalcBKCounseling.SizeofHousehold;
						Contactdetail.monthly_gross_income = quicalcBKCounseling.MonthlyGrossIncome;

						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}

		public FinalContactdetailBKCounseling FinalContactdetailBKCounselingGet(int ClientNumber)
		{
			FinalContactdetailBKCounseling FinalContactdetailBKCounselingResults = new FinalContactdetailBKCounseling();
			contactdetail Contactdetail = null;
			using(CredabilityDataContext transactionEntities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in transactionEntities.contactdetails
						 where contat.client_number == ClientNumber
						 select contat
						).FirstOrDefault<contactdetail>();
				if(Contactdetail != null)
				{
					FinalContactdetailBKCounselingResults.ClientNumber = ClientNumber;
					FinalContactdetailBKCounselingResults.Contact_firstname = (Contactdetail.contact_firstname != null) ? Contactdetail.contact_firstname : string.Empty;
					FinalContactdetailBKCounselingResults.Contact_initial = (Contactdetail.contact_initial != null) ? Contactdetail.contact_initial : string.Empty;
					FinalContactdetailBKCounselingResults.Contact_lastname = (Contactdetail.contact_lastname != null) ? Contactdetail.contact_lastname : string.Empty;
					FinalContactdetailBKCounselingResults.Cosign_firstname = (Contactdetail.cosign_firstname != null) ? Contactdetail.cosign_firstname : string.Empty;
					FinalContactdetailBKCounselingResults.Cosign_initial = (Contactdetail.cosign_initial != null) ? Contactdetail.cosign_initial : string.Empty;
					FinalContactdetailBKCounselingResults.Cosign_lastname = (Contactdetail.cosign_lastname != null) ? Contactdetail.cosign_lastname : string.Empty;
					FinalContactdetailBKCounselingResults.Confirm_ipaddress = !String.IsNullOrEmpty(Contactdetail.confirm_ipaddress) ? Contactdetail.confirm_ipaddress : String.Empty;
					FinalContactdetailBKCounselingResults.ClientType = (Contactdetail.ClientType != null) ? Contactdetail.ClientType : string.Empty;
					FinalContactdetailBKCounselingResults.Span_flag = (Contactdetail.span_flag != null) ? Contactdetail.span_flag : string.Empty;
					FinalContactdetailBKCounselingResults.Confirm_datetime = Contactdetail.confirm_datetime.HasValue ? Contactdetail.confirm_datetime.Value : DateTime.Now;
				}
				else
				{
					FinalContactdetailBKCounselingResults.ClientNumber = 0;
				}
				return FinalContactdetailBKCounselingResults;
			}
		}

		public FinalContactdetailBKCounselingResult FinalContactdetailBKCounselingUpadte(FinalContactdetailBKCounseling finalContactdetailBKCounseling)
		{
			contactdetail Contactdetail = null;

			FinalContactdetailBKCounselingResult Result = new FinalContactdetailBKCounselingResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				 (
						 from contat in Entities.contactdetails
						 where contat.client_number == finalContactdetailBKCounseling.ClientNumber
						 select contat
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					try
					{
						Contactdetail.confirm_ipaddress = finalContactdetailBKCounseling.Confirm_ipaddress;
						Contactdetail.ClientType = finalContactdetailBKCounseling.ClientType;
						Contactdetail.span_flag = finalContactdetailBKCounseling.Span_flag == null ? String.Empty : finalContactdetailBKCounseling.Span_flag;
						if(Contactdetail.confirm_datetime == null || Contactdetail.confirm_datetime.Value.AddMonths(6) < DateTime.Now)
						{
							Contactdetail.confirm_datetime = finalContactdetailBKCounseling.Confirm_datetime;
						}
						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
			}
			return Result;
		}


		#endregion

		#region CommonOptionforDealingBCHAddUpadte
		public CommonOptionforDealingBCHResult CommonOptionforDealingBCHAddUpadte(CommonOptionforDealingBCH commonOptionforDealingBCH)
		{
			contactdetail Contactdetail = null;

			CommonOptionforDealingBCHResult UpdateResult = new CommonOptionforDealingBCHResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Entities.OpenEncryptionKey();
				try
				{
					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == commonOptionforDealingBCH.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();

					if(Contactdetail != null)
					{
						Contactdetail.dmp_fco_pcp_date = (commonOptionforDealingBCH.Dmp_fco_pcp_date != null) ? commonOptionforDealingBCH.Dmp_fco_pcp_date : string.Empty;
						Contactdetail.dmp_fco_pcp_flag = (commonOptionforDealingBCH.Dmp_fco_pcp_flag != null) ? commonOptionforDealingBCH.Dmp_fco_pcp_flag : string.Empty;
						Contactdetail.ConThem = (commonOptionforDealingBCH.ContactMe != null) ? commonOptionforDealingBCH.ContactMe : string.Empty;
						Contactdetail.ConDay = (commonOptionforDealingBCH.ContactDay != null) ? commonOptionforDealingBCH.ContactDay : string.Empty;
						Contactdetail.contact_email = (commonOptionforDealingBCH.Contactemail != null) ? commonOptionforDealingBCH.Contactemail : string.Empty;
						Contactdetail.contact_telephone = (commonOptionforDealingBCH.Contacttelephone != null) ? commonOptionforDealingBCH.Contacttelephone : string.Empty;
						Contactdetail.ConTime = (commonOptionforDealingBCH.ContactTime != null) ? commonOptionforDealingBCH.ContactTime : string.Empty;
						Contactdetail.counselor_id = commonOptionforDealingBCH.CounselorId;
						Contactdetail.span_flag = commonOptionforDealingBCH.SpanFlag;

						if(!String.IsNullOrEmpty(commonOptionforDealingBCH.Contactemail))
						{
							Contactdetail.ConMeth = "email";
							Contactdetail.ConInfo = commonOptionforDealingBCH.Contactemail;
						}

						if(!String.IsNullOrEmpty(commonOptionforDealingBCH.Contacttelephone))
						{
							Contactdetail.ConMeth = "phone";
							Contactdetail.ConInfo = commonOptionforDealingBCH.Contacttelephone;
						}

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}
		}
		#endregion CommonOptionforDealingBCHAddUpadte

		#region CommonOptionforDealingBCHGet
		public CommonOptionforDealingBCH CommonOptionforDealingBCHGet(int ClientNumber)
		{
			CommonOptionforDealingBCH CommonOptionforDealingBCHs = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				CommonOptionforDealingBCHs =
					(
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new CommonOptionforDealingBCH
						 {
							 ClientNumber = entity.client_number,
							 Dmp_fco_pcp_date = entity.dmp_fco_pcp_date,
							 Dmp_fco_pcp_flag = entity.dmp_fco_pcp_flag,
							 Contactemail = entity.contact_email,
							 Contacttelephone = entity.contact_telephone,
							 ContactTime = entity.ConTime,
							 ContactDay = entity.ConDay,
							 ContactMe = entity.ConThem,
							 ClientType = entity.ClientType,
						 }
					).FirstOrDefault();
			}
			return CommonOptionforDealingBCHs;
		}
		#endregion CommonOptionforDealingBCHGet

		#region Fee




		public Result PaymentTypeUpdate(Int32 ClientNumber)
		{
			Result result = new Result();
			contactdetail Contact = new contactdetail();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					Contact =
					 (
							 from entity in Entities.contactdetails
							 where entity.client_number == ClientNumber
							 select entity
					 ).FirstOrDefault<contactdetail>();
					if(Contact != null) //Update
					{
						Contact.payment_type = "N1";
						Entities.SubmitChanges();

						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}
		#endregion

		public PayementType PayementTypeGet(int ClientNumber)
		{
			PayementType PayementTypes = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				PayementTypes =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new PayementType
						 {
							 PaymentType = entity.payment_type,
						 }
				 ).FirstOrDefault();
			}
			return PayementTypes;
		}

		public CcrcHpfData CcrcHpfDataGet(int clientNumber)
		{
			CcrcHpfData Data = new CcrcHpfData();
			using(var context = new CredabilityDataContext())
			{
				var detail = (from details in context.contactdetails
							  where details.client_number == clientNumber
							  select details).FirstOrDefault();
				if(detail != null)
				{
					Data.CcrcAprov = detail.CcrcAprov;
					Data.HpfID = detail.HpfID;
					if(detail.CreateDTS == null)
						Data.CreateDTS = DateTime.Now;
					else
						Data.CreateDTS = (DateTime) detail.CreateDTS;
				}
			}
			return Data;
		}

		public Result HpfIdSet(int ClientNumber, int HpfID)
		{
			Result result = new Result();
			contactdetail Contact = new contactdetail();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					Contact = (
						from entity in Entities.contactdetails
						where entity.client_number == ClientNumber
						select entity
						).FirstOrDefault<contactdetail>();
					if(Contact != null) //Update
					{
						Contact.HpfID = HpfID;
						Entities.SubmitChanges();
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}

		public void SetCounselorId(int ClientNumber)
		{
			// Internet Counselor : 634
            SetCounselorId(ClientNumber, 634);
		}

        public void SetCounselorId(int clientNumber, int counselorID)
        {
            using (var context = new CredabilityDataContext())
            {
                var detail = (from details in context.contactdetails
                              where details.client_number == clientNumber
                              select details).FirstOrDefault();
                if (detail == null)
                    return;

                detail.counselor_id = counselorID;
                detail.processed = 1;

                context.SubmitChanges();
            }
        }

        public Result CompleteClient(int ClientNumber)
		{
			contactdetail Contactdetail = null;
			Result Result = new Result();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
				(
					from contat in Entities.contactdetails
					where contat.client_number == ClientNumber
					select contat
				).FirstOrDefault<contactdetail>();
				if(Contactdetail != null) //Update
				{
					try
					{
						Contactdetail.counselor_id = null;
						Contactdetail.completed = 1;
						if(Contactdetail.ClientType.Trim() == "BK")
						{
							if(Contactdetail.confirm_datetime == null || Contactdetail.confirm_datetime.Value.AddMonths(6) < DateTime.Now)
							{
								Contactdetail.confirm_datetime = DateTime.Now;
							}
						}
						else
						{
							Contactdetail.confirm_datetime = DateTime.Now;
						}
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}
				else
				{
					Result.IsSuccessful = false;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
			}
			return Result;
		}
	}
}