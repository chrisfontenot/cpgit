﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Credability.Impl;

namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{

		public UserReferenceDetails UserReferenceDetailsGet(int ClientNumber)
		{
			UserReferenceDetails userReferenceDetails = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				userReferenceDetails =
						 (
								 from entity in Entities.PreHuds
								 where entity.client_number == ClientNumber
								 select new UserReferenceDetails
								 {
									 ClientNumber = (entity.client_number != null) ? entity.client_number : 0,
									 RefInstitutionName = (entity.RefInst != null) ? entity.RefInst : String.Empty,
									 RefContactName = (entity.RefName != null) ? entity.RefName : String.Empty,
									 RefContactEmail = (entity.RefEmail != null) ? entity.RefEmail : String.Empty,
									 BegPro = (entity.BegPro != null) ? Convert.ToBoolean(entity.BegPro) : false,
									 AstPay = (entity.AstPay != null) ? Convert.ToBoolean(entity.AstPay) : false,
									 BenAprov = (entity.BenAprov != null) ? Convert.ToBoolean(entity.BenAprov) : false,
									 HaveCloDate = (entity.HaveCloDate != null) ? Convert.ToBoolean(entity.HaveCloDate) : false,
									 HadHouse = (entity.HadHouse != null) ? entity.HadHouse : String.Empty,
									 FndHouse = (entity.FndHouse != null) ? entity.FndHouse : String.Empty,
									 HouseAdr = (entity.HouseAdr != null) ? entity.HouseAdr : String.Empty,
									 HouseCity = (entity.HouseCity != null) ? entity.HouseCity : String.Empty,
									 HouseSt = (entity.HouseSt != null) ? entity.HouseSt : String.Empty,
									 HouseZip = (entity.HouseZip != null) ? entity.HouseZip : String.Empty,
									 HousePrice = (entity.HousePrice != null) ? Convert.ToDecimal(entity.HousePrice) : 0,
									 TheCloDate = (entity.TheCloDate != null) ? Convert.ToString(Convert.ToDateTime(entity.TheCloDate)) : String.Empty,
								 }
						 ).FirstOrDefault<UserReferenceDetails>();
			}
			return userReferenceDetails;
		}

		public UserMonthlyExpenses NewMonthlyExpensesPreHudGet(int ClientNumber)
		{
			UserMonthlyExpenses MonthlyExpenses = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				MonthlyExpenses =
						 (
								 from entity in Entities.PreHuds
								 where entity.client_number == ClientNumber
								 select new UserMonthlyExpenses
								 {
									 ClientNumber = (entity.client_number != null) ? entity.client_number : 0,

									 RentMort = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewMort) : 0,
									 HomeMaintenance = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewMaint) : 0,
									 Telephone = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewPhone) : 0,
									 FoodAway = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewFoodAway) : 0,
									 Groceries = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewGroceries) : 0,
									 MoEquity = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewMoEquity) : 0,
									 Mo2ndMort = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewMo2ndMort) : 0,
									 MoFee = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewMoFee) : 0,
									 Insurance = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewInsurance) : 0,
									 Recreation = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewRecreation) : 0,
									 MedicalPrescription = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewMed) : 0,
									 Education = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewEdu) : 0,
									 ChildSupportAlimony = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewChildSupport) : 0,
									 Laundry = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewLaundry) : 0,
									 Clothing = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewClothing) : 0,
									 ChildElderCare = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewElderCare) : 0,
									 PersonalExpenses = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewPerdonalExp) : 0,
									 BeautyBarber = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewBeauty) : 0,
									 CarPayments = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewVehiclePay) : 0,
									 CarInsurance = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewCarIns) : 0,
									 CarMaintenance = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewCarMaint) : 0,
									 ClubDues = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewClubDues) : 0,
									 PublicTransportation = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewPublicTrans) : 0,
									 Contributions = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewContributions) : 0,
									 Gifts = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewGifts) : 0,
									 Miscellaneous = (entity.NewMort != null) ? Convert.ToDecimal(entity.NewMiscExp) : 0,
									 MoPropTax = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewMoPropTax) : 0,
									 MoPropIns = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewMoPropIns) : 0,
									 OtherLoans = (entity.NewOtherLoans != null) ? Convert.ToInt32(entity.NewOtherLoans) : 0,
									 UtlElectric = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewUtlElectric) : 0,
									 UtlWater = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewUtlWater) : 0,
									 UtlGas = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewUtlGas) : 0,
									 UtlTv = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewUtlTv) : 0,
									 UtlTrash = (entity.NewMort != null) ? (float) Convert.ToDecimal(entity.NewUtlTrash) : 0,
								 }
						 ).FirstOrDefault<UserMonthlyExpenses>();
			}
			return MonthlyExpenses;
		}

		public UserReferenceDetailsResults UserPreHudAddUpdate(UserReferenceDetails userPrehud)
		{

			UserReferenceDetailsResults result = new UserReferenceDetailsResults();
			PreHud prehud = null;
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					prehud =
					 (
							 from entity in Entities.PreHuds
							 where entity.client_number == userPrehud.ClientNumber
							 select entity
					 ).FirstOrDefault<PreHud>();
					if(prehud != null) //Update
					{
						prehud.RefInst = userPrehud.RefInstitutionName;
						prehud.RefName = userPrehud.RefContactName;
						prehud.RefEmail = userPrehud.RefContactEmail;
						Entities.SubmitChanges();

						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;

					}
					else // Add New 
					{
						prehud = new PreHud();
						prehud.client_number = userPrehud.ClientNumber;
						prehud.RefInst = userPrehud.RefInstitutionName;
						prehud.RefName = userPrehud.RefContactName;
						prehud.RefEmail = userPrehud.RefContactEmail;
						Entities.PreHuds.InsertOnSubmit(prehud);
						Entities.SubmitChanges();
						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}
		}

		public UserReferenceDetailsResults NewMonthlyExpensesPreHudAddUpdate(UserMonthlyExpenses NewMonthlyExpenses)
		{
			UserReferenceDetailsResults result = new UserReferenceDetailsResults();
			PreHud prehud = null;
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					prehud =
					 (
							 from entity in Entities.PreHuds
							 where entity.client_number == NewMonthlyExpenses.ClientNumber
							 select entity
					 ).FirstOrDefault<PreHud>();
					if(prehud != null) //Update
					{
						prehud.NewMort = (float) NewMonthlyExpenses.RentMort;
						prehud.NewMoEquity = NewMonthlyExpenses.MoEquity;
						prehud.NewMo2ndMort = NewMonthlyExpenses.Mo2ndMort;
						prehud.NewMoPropTax = NewMonthlyExpenses.MoPropTax;
						prehud.NewMoPropIns = NewMonthlyExpenses.MoPropIns;
						prehud.NewMoFee = NewMonthlyExpenses.MoFee;
						prehud.NewMaint = (float) NewMonthlyExpenses.HomeMaintenance;
						prehud.NewUtlElectric = NewMonthlyExpenses.UtlElectric;
						prehud.NewUtlWater = NewMonthlyExpenses.UtlWater;
						prehud.NewUtlGas = NewMonthlyExpenses.UtlGas;
						prehud.NewUtlTv = NewMonthlyExpenses.UtlTv;
						prehud.NewUtlTrash = NewMonthlyExpenses.UtlTrash;
						prehud.NewPhone = (float) NewMonthlyExpenses.Telephone;
						prehud.NewFoodAway = (float) NewMonthlyExpenses.FoodAway;
						prehud.NewGroceries = (float) NewMonthlyExpenses.Groceries;

						prehud.NewVehiclePay = (float) NewMonthlyExpenses.CarPayments;
						prehud.NewCarIns = (float) NewMonthlyExpenses.CarInsurance;
						prehud.NewCarMaint = (float) NewMonthlyExpenses.CarMaintenance;
						prehud.NewPublicTrans = (float) NewMonthlyExpenses.PublicTransportation;

						prehud.NewInsurance = (float) NewMonthlyExpenses.Insurance;
						prehud.NewMed = (float) NewMonthlyExpenses.MedicalPrescription;
						prehud.NewChildSupport = (float) NewMonthlyExpenses.ChildSupportAlimony;
						prehud.NewElderCare = (float) NewMonthlyExpenses.ChildElderCare;
						prehud.NewEdu = (float) NewMonthlyExpenses.Education;

						prehud.NewOtherLoans = (float) NewMonthlyExpenses.OtherLoans;
						prehud.NewContributions = (float) NewMonthlyExpenses.Contributions;
						prehud.NewClothing = (float) NewMonthlyExpenses.Clothing;
						prehud.NewLaundry = (float) NewMonthlyExpenses.Laundry;
						prehud.NewPerdonalExp = (float) NewMonthlyExpenses.PersonalExpenses;
						prehud.NewBeauty = (float) NewMonthlyExpenses.BeautyBarber;
						prehud.NewRecreation = (float) NewMonthlyExpenses.Recreation;
						prehud.NewClubDues = (float) NewMonthlyExpenses.ClubDues;
						prehud.NewGifts = (float) NewMonthlyExpenses.Gifts;
						prehud.NewMiscExp = (float) NewMonthlyExpenses.Miscellaneous;

						Entities.SubmitChanges();

						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;
					}
					else
					{
						prehud = new PreHud();
						prehud.client_number = Convert.ToInt32(prehud.client_number);

						prehud.NewMort = (float) NewMonthlyExpenses.RentMort;
						prehud.NewMoEquity = NewMonthlyExpenses.MoEquity;
						prehud.NewMo2ndMort = NewMonthlyExpenses.Mo2ndMort;
						prehud.NewMoPropTax = NewMonthlyExpenses.MoPropTax;
						prehud.NewMoPropIns = NewMonthlyExpenses.MoPropIns;
						prehud.NewMoFee = NewMonthlyExpenses.MoFee;
						prehud.NewMaint = (float) NewMonthlyExpenses.HomeMaintenance;
						prehud.NewUtlElectric = NewMonthlyExpenses.UtlElectric;
						prehud.NewUtlWater = NewMonthlyExpenses.UtlWater;
						prehud.NewUtlGas = NewMonthlyExpenses.UtlGas;
						prehud.NewUtlTv = NewMonthlyExpenses.UtlTv;
						prehud.NewUtlTrash = NewMonthlyExpenses.UtlTrash;
						prehud.NewPhone = (float) NewMonthlyExpenses.Telephone;
						prehud.NewFoodAway = (float) NewMonthlyExpenses.FoodAway;
						prehud.NewGroceries = (float) NewMonthlyExpenses.Groceries;

						prehud.NewVehiclePay = (float) NewMonthlyExpenses.CarPayments;
						prehud.NewCarIns = (float) NewMonthlyExpenses.CarInsurance;
						prehud.NewCarMaint = (float) NewMonthlyExpenses.CarMaintenance;
						prehud.NewPublicTrans = (float) NewMonthlyExpenses.PublicTransportation;

						prehud.NewInsurance = (float) NewMonthlyExpenses.Insurance;
						prehud.NewMed = (float) NewMonthlyExpenses.MedicalPrescription;
						prehud.NewChildSupport = (float) NewMonthlyExpenses.ChildSupportAlimony;
						prehud.NewElderCare = (float) NewMonthlyExpenses.ChildElderCare;
						prehud.NewEdu = (float) NewMonthlyExpenses.Education;

						prehud.NewOtherLoans = (float) NewMonthlyExpenses.OtherLoans;
						prehud.NewContributions = (float) NewMonthlyExpenses.Contributions;
						prehud.NewClothing = (float) NewMonthlyExpenses.Clothing;
						prehud.NewLaundry = (float) NewMonthlyExpenses.Laundry;
						prehud.NewPerdonalExp = (float) NewMonthlyExpenses.PersonalExpenses;
						prehud.NewBeauty = (float) NewMonthlyExpenses.BeautyBarber;
						prehud.NewRecreation = (float) NewMonthlyExpenses.Recreation;
						prehud.NewClubDues = (float) NewMonthlyExpenses.ClubDues;
						prehud.NewGifts = (float) NewMonthlyExpenses.Gifts;
						prehud.NewMiscExp = (float) NewMonthlyExpenses.Miscellaneous;

						Entities.SubmitChanges();

						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}


		}

		public UserReferenceDetailsResults DescribeYourSelfPreHudAddUpdate(UserReferenceDetails userPrehud)
		{

			//PreHud prehud = null;

			UserReferenceDetailsResults result = new UserReferenceDetailsResults();
			PreHud prehud = null;
			string Message = string.Empty;
			DateTime? TheCloseDate = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{
					prehud =
					 (
							 from entity in Entities.PreHuds
							 where entity.client_number == userPrehud.ClientNumber
							 select entity
					 ).FirstOrDefault<PreHud>();
					if(prehud != null) //Update
					{

						prehud.BegPro = userPrehud.BegPro;
						prehud.AstPay = userPrehud.AstPay;
						prehud.BenAprov = userPrehud.BenAprov;
						prehud.HadHouse = userPrehud.HadHouse;
						prehud.FndHouse = userPrehud.FndHouse;
						prehud.HaveCloDate = userPrehud.HaveCloDate;
						if(userPrehud.HaveCloDate == true && userPrehud.TheCloDate != string.Empty)
						{
							TheCloseDate = Convert.ToDateTime(userPrehud.TheCloDate);
							prehud.TheCloDate = TheCloseDate;

						}
						//prehud.TheCloDate = TheCloseDate;
						prehud.HouseAdr = userPrehud.HouseAdr;
						prehud.HouseCity = userPrehud.HouseCity;
						prehud.HouseSt = userPrehud.HouseSt;
						prehud.HouseZip = userPrehud.HouseZip;
						prehud.HousePrice = (float) userPrehud.HousePrice;

						Entities.SubmitChanges();

						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;

					}
					else // Add New 
					{
						prehud = new PreHud();
						prehud.client_number = userPrehud.ClientNumber;
						prehud.BegPro = userPrehud.BegPro;
						prehud.AstPay = userPrehud.AstPay;
						prehud.BenAprov = userPrehud.BenAprov;
						prehud.HadHouse = userPrehud.HadHouse;
						prehud.FndHouse = userPrehud.FndHouse;
						prehud.HaveCloDate = userPrehud.HaveCloDate;
						if(userPrehud.HaveCloDate == true)
						{
							TheCloseDate = Convert.ToDateTime(userPrehud.TheCloDate);
							prehud.TheCloDate = TheCloseDate;
						}
						prehud.TheCloDate = TheCloseDate;
						prehud.HouseAdr = userPrehud.HouseAdr;
						prehud.HouseCity = userPrehud.HouseCity;
						prehud.HouseSt = userPrehud.HouseSt;
						prehud.HouseZip = userPrehud.HouseZip;
						prehud.HousePrice = (float) userPrehud.HousePrice;
						Entities.PreHuds.InsertOnSubmit(prehud);
						Entities.SubmitChanges();
						result.ClientNumber = Convert.ToInt32(prehud.client_number);
						result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					result.IsSuccessful = false;
					result.Exception = exception;
					result.Messages = new string[] { "An error occured attempting to save." };
				}
				return result;
			}


		}

		#region Provide Your Information BKC

		public UserContactDetailsBKC BKCUserInfoGet(int ClientNumber)
		{
			UserContactDetailsBKC UserContactDetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserContactDetail =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new UserContactDetailsBKC
						 {
							 ClientNumber = entity.client_number,
							 //FirstName = (entity.ContactFirstNameEnc != null) ? Entities.DecryptText(entity.ContactFirstNameEnc) : string.Empty,
							 //LastName = (entity.ContactLastNameEnc != null) ? Entities.DecryptText(entity.ContactLastNameEnc) : string.Empty,
							 //Initial = (entity.ContactInitialEnc != null) ? Entities.DecryptText(entity.ContactInitialEnc) : string.Empty,
							 //Dob = (entity.contact_dob != null) ? entity.contact_dob : string.Empty,
							 //Sex = (entity.contact_sex != null) ? entity.contact_sex : string.Empty,
							 //Ssn = (entity.ContactSsnEnc != null) ? Entities.DecryptText(entity.ContactSsnEnc) : string.Empty,
							 //Race = (entity.contact_race != null) ? entity.contact_race : string.Empty,
							 //Marital = (entity.contact_marital != null) ? entity.contact_marital : string.Empty,
							 //City = (entity.contact_city != null) ? entity.contact_city : string.Empty,
							 //State = (entity.contact_state != null) ? entity.contact_state : string.Empty,
							 //Zip = (entity.contact_zip != null) ? entity.contact_zip : string.Empty,
							 //Address = (entity.ContactAddressEnc != null) ? Entities.DecryptText(entity.ContactAddressEnc) : string.Empty,

							 //Address2 = (entity.ContactAddress2Enc != null) ? Entities.DecryptText(entity.ContactAddress2Enc) : string.Empty,

							 //Telephone = (entity.ContactTelephoneEnc != null) ? Entities.DecryptText(entity.ContactTelephoneEnc) : string.Empty,

							 //OtherTelephone = (entity.ContactOtherTelephoneEnc != null) ? Entities.DecryptText(entity.ContactOtherTelephoneEnc) : string.Empty,

							 //Email = (entity.RealEmailEnc != null) ? Entities.DecryptText(entity.RealEmailEnc) : string.Empty,

							 //Hispanic = (entity.contact_hispanic != null) ? entity.contact_hispanic : string.Empty,
							 //CosignFirstName = (entity.CosignFirstNameEnc != null) ? Entities.DecryptText(entity.CosignFirstNameEnc) : string.Empty,

							 //CosignInitial = (entity.CosignInitialEnc != null) ? Entities.DecryptText(entity.CosignInitialEnc) : string.Empty,

							 //CosignLastname = (entity.CosignLastNameEnc != null) ? Entities.DecryptText(entity.CosignLastNameEnc) : string.Empty,

							 //CosignMarital = (entity.cosign_marital != null) ? entity.cosign_marital : string.Empty,
							 //CosignRace = (entity.cosign_race != null) ? entity.cosign_race : string.Empty,
							 //CosignDob = (entity.cosign_dob != null) ? entity.cosign_dob : string.Empty,
							 //CosignSex = (entity.cosign_sex != null) ? entity.cosign_sex : string.Empty,
							 //CosignSsn = (entity.CosignSsnEnc != null) ? Entities.DecryptText(entity.CosignSsnEnc) : string.Empty,

							 BirthOfCity = (entity.cred_rpt_bcity != null) ? entity.cred_rpt_bcity : string.Empty,
							 CosignBirthOfCity = (entity.cred_rpt_bcity2 != null) ? entity.cred_rpt_bcity2 : string.Empty,
							 //CosignHispanic = (entity.cosign_hispanic != null) ? entity.cosign_hispanic : string.Empty,
							 //ContactTelephone = (entity.contact_telephone != null) ? entity.contact_telephone : string.Empty,
							 CcrcAprov = (entity.CcrcAprov != null) ? entity.CcrcAprov : string.Empty,
							 RefCode = (entity.refcode != null) ? entity.refcode : string.Empty,
							 NumberOfPeople = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
							 //EmailAddress = (entity.contact_email != null) ? entity.contact_email : string.Empty,
							 AttyHelp = (entity.atty_help != null) ? entity.atty_help : string.Empty,

							 ClientType = (entity.ClientType != null) ? entity.ClientType : string.Empty,
							 HousingType = entity.housing_type != null ? Convert.ToChar(entity.housing_type) : ' ',
							 MortCurrent = entity.mort_current != null ? Convert.ToInt32(entity.mort_current) : 0,
							 Completed = entity.completed != null ? Convert.ToInt32(entity.completed) : 0,

						 }
						).FirstOrDefault();
			}
			return UserContactDetail;
		}

		public UserContactDetailsBKCResults UserInformationUpdateForBKC(UserContactDetailsBKC userContactDetailsBKC)
		{

			contactdetail Contactdetail = null;

			UserContactDetailsBKCResults UpdateResult = new UserContactDetailsBKCResults();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{

					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == userContactDetailsBKC.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();
					if(Contactdetail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						//Contactdetail.contact_firstname = userContactDetailsBKC.FirstName;
						//Contactdetail.contact_lastname = userContactDetailsBKC.LastName;
						//Contactdetail.contact_initial = userContactDetailsBKC.Initial;
						//Contactdetail.contact_address = userContactDetailsBKC.Address;
						//Contactdetail.contact_address2 = userContactDetailsBKC.Address2;
						//Contactdetail.contact_city = userContactDetailsBKC.City;
						//Contactdetail.contact_state = userContactDetailsBKC.State;
						//Contactdetail.contact_zip = userContactDetailsBKC.Zip;
						//Contactdetail.contact_ssn = userContactDetailsBKC.Ssn;
						//Contactdetail.contact_dob = userContactDetailsBKC.Dob;
						//Contactdetail.contact_marital = userContactDetailsBKC.Marital;
						//Contactdetail.contact_sex = userContactDetailsBKC.Sex;
						//Contactdetail.contact_race = userContactDetailsBKC.Race;
						//Contactdetail.contact_hispanic = userContactDetailsBKC.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetailsBKC.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetailsBKC.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetailsBKC.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetailsBKC.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetailsBKC.CosignDob;
						//Contactdetail.cosign_marital = userContactDetailsBKC.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetailsBKC.CosignSex;
						//Contactdetail.cosign_race = userContactDetailsBKC.Race;
						Contactdetail.cred_rpt_bcity = userContactDetailsBKC.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetailsBKC.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetailsBKC.CosignHispanic;
						//Contactdetail.contact_telephone = userContactDetailsBKC.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetailsBKC.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetailsBKC.CcrcAprov;
						//Contactdetail.contact_email = userContactDetailsBKC.EmailAddress;
						Contactdetail.atty_help = userContactDetailsBKC.AttyHelp;
						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetailsBKC.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetailsBKC.Address2);
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetailsBKC.FirstName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetailsBKC.Initial);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetailsBKC.LastName);
						//Contactdetail.ContactStateEnc = Entities.EncryptText(userContactDetailsBKC.State);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetailsBKC.Telephone);
						//Contactdetail.ContactCityEnc = Entities.EncryptText(userContactDetailsBKC.City);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetailsBKC.OtherTelephone);
						//Contactdetail.ContactZipEnc = Entities.EncryptText(userContactDetailsBKC.Zip);
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetailsBKC.Ssn);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetailsBKC.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetailsBKC.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetailsBKC.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetailsBKC.CosignSsn);
						Contactdetail.cred_rpt_pull = 'y';
						Contactdetail.ModForm7 = 1;

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;

					}
					else // Add New User 
					{
						Contactdetail = new contactdetail();
						//Contactdetail.contact_firstname = userContactDetailsBKC.FirstName;
						//Contactdetail.contact_lastname = userContactDetailsBKC.LastName;
						//Contactdetail.contact_initial = userContactDetailsBKC.Initial;
						//Contactdetail.contact_address = userContactDetailsBKC.Address;
						//Contactdetail.contact_address2 = userContactDetailsBKC.Address2;
						//Contactdetail.contact_city = userContactDetailsBKC.City;
						//Contactdetail.contact_state = userContactDetailsBKC.State;
						//Contactdetail.contact_zip = userContactDetailsBKC.Zip;
						//Contactdetail.contact_ssn = userContactDetailsBKC.Ssn;
						//Contactdetail.contact_dob = userContactDetailsBKC.Dob;
						//Contactdetail.contact_marital = userContactDetailsBKC.Marital;
						//Contactdetail.contact_sex = userContactDetailsBKC.Sex;
						//Contactdetail.contact_race = userContactDetailsBKC.Race;
						//Contactdetail.contact_hispanic = userContactDetailsBKC.Hispanic;
						//Contactdetail.cosign_firstname = userContactDetailsBKC.CosignFirstName;
						//Contactdetail.cosign_initial = userContactDetailsBKC.CosignInitial;
						//Contactdetail.cosign_lastname = userContactDetailsBKC.CosignLastname;
						//Contactdetail.cosign_ssn = userContactDetailsBKC.CosignSsn;
						//Contactdetail.cosign_dob = userContactDetailsBKC.CosignDob;
						//Contactdetail.cosign_marital = userContactDetailsBKC.CosignMarital;
						//Contactdetail.cosign_sex = userContactDetailsBKC.CosignSex;
						//Contactdetail.cosign_race = userContactDetailsBKC.Race;
						Contactdetail.cred_rpt_bcity = userContactDetailsBKC.BirthOfCity;
						Contactdetail.cred_rpt_bcity2 = userContactDetailsBKC.CosignBirthOfCity;
						//Contactdetail.cosign_hispanic = userContactDetailsBKC.CosignHispanic;
						//Contactdetail.contact_telephone = userContactDetailsBKC.ContactTelephone;
						//Contactdetail.contact_other_telephone = userContactDetailsBKC.OtherTelephone;
						Contactdetail.CcrcAprov = userContactDetailsBKC.CcrcAprov;
						//Contactdetail.contact_email = userContactDetailsBKC.EmailAddress;
						Contactdetail.atty_help = userContactDetailsBKC.AttyHelp;
						//Contactdetail.ContactAddressEnc = Entities.EncryptText(userContactDetailsBKC.Address);
						//Contactdetail.ContactAddress2Enc = Entities.EncryptText(userContactDetailsBKC.Address2);
						//Contactdetail.ContactFirstNameEnc = Entities.EncryptText(userContactDetailsBKC.FirstName);
						//Contactdetail.ContactInitialEnc = Entities.EncryptText(userContactDetailsBKC.Initial);
						//Contactdetail.ContactLastNameEnc = Entities.EncryptText(userContactDetailsBKC.LastName);
						//Contactdetail.ContactStateEnc = Entities.EncryptText(userContactDetailsBKC.State);
						//Contactdetail.ContactTelephoneEnc = Entities.EncryptText(userContactDetailsBKC.Telephone);
						//Contactdetail.ContactCityEnc = Entities.EncryptText(userContactDetailsBKC.City);
						//Contactdetail.ContactOtherTelephoneEnc = Entities.EncryptText(userContactDetailsBKC.OtherTelephone);
						//Contactdetail.ContactZipEnc = Entities.EncryptText(userContactDetailsBKC.Zip);
						//Contactdetail.ContactSsnEnc = Entities.EncryptText(userContactDetailsBKC.Ssn);
						//Contactdetail.CosignFirstNameEnc = Entities.EncryptText(userContactDetailsBKC.CosignFirstName);
						//Contactdetail.CosignInitialEnc = Entities.EncryptText(userContactDetailsBKC.CosignInitial);
						//Contactdetail.CosignLastNameEnc = Entities.EncryptText(userContactDetailsBKC.CosignLastname);
						//Contactdetail.CosignSsnEnc = Entities.EncryptText(userContactDetailsBKC.CosignSsn);
						Contactdetail.cred_rpt_pull = 'y';
						Contactdetail.ModForm7 = 1;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}


		}

		#endregion Provide Your Information BKC

		#region User Describe Your Situation BKC

		public UserDecsribeYourSituationBKCResult UserDescribeYourSituationForBKCAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC)
		{

			contactdetail Contactdetail = null;

			UserDecsribeYourSituationBKCResult UpdateResult = new UserDecsribeYourSituationBKCResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{

					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == userDescribeYourSituationBKC.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();
					if(Contactdetail != null)
					{
						//Contactdetail.client_number = userContactDetails.ClientNumber;
						Contactdetail.BkForPrv = userDescribeYourSituationBKC.BkForPrv;
						Contactdetail.bankrupt = userDescribeYourSituationBKC.Bankrupt;
						Contactdetail.contact_reason = userDescribeYourSituationBKC.ContactReason;
						Contactdetail.contact_comments = userDescribeYourSituationBKC.ContactComments;
						Contactdetail.mort_current = userDescribeYourSituationBKC.MortCurrent;
						Contactdetail.mos_delinq = (float) userDescribeYourSituationBKC.MosDelinq;
						Contactdetail.housing_type = userDescribeYourSituationBKC.HousingType;
						Contactdetail.size_of_household = userDescribeYourSituationBKC.SizeofHouseHold;

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;

					}
					else // Add New User 
					{
						Contactdetail = new contactdetail();
						Contactdetail.BkForPrv = userDescribeYourSituationBKC.BkForPrv;
						Contactdetail.bankrupt = userDescribeYourSituationBKC.Bankrupt;
						Contactdetail.contact_reason = userDescribeYourSituationBKC.ContactReason;
						Contactdetail.contact_comments = userDescribeYourSituationBKC.ContactComments;
						Contactdetail.mort_current = userDescribeYourSituationBKC.MortCurrent;
						Contactdetail.mos_delinq = (float) userDescribeYourSituationBKC.MosDelinq;
						Contactdetail.housing_type = userDescribeYourSituationBKC.HousingType;
						Contactdetail.size_of_household = userDescribeYourSituationBKC.SizeofHouseHold;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}


		}

		public UserDecsribeYourSituationBKCResult UserDescribeYourSituation2ForBKCAddUpdate(UserDescribeYourSituationBKC userDescribeYourSituationBKC)
		{

			contactdetail Contactdetail = null;

			UserDecsribeYourSituationBKCResult UpdateResult = new UserDecsribeYourSituationBKCResult();
			string Message = string.Empty;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				try
				{

					Contactdetail =
					 (
							 from contat in Entities.contactdetails
							 where contat.client_number == userDescribeYourSituationBKC.ClientNumber
							 select contat
					 ).FirstOrDefault<contactdetail>();
					if(Contactdetail != null)
					{
						Contactdetail.mort_holder = userDescribeYourSituationBKC.MortHolder;
						Contactdetail.loan_number = userDescribeYourSituationBKC.LoanNumber;
						Contactdetail.mort_type = userDescribeYourSituationBKC.MortType;
						Contactdetail.mort_years = (float) userDescribeYourSituationBKC.MortYears;
						Contactdetail.mort_date = userDescribeYourSituationBKC.MortDate;
						Contactdetail.mort_rate = (float) userDescribeYourSituationBKC.MortRate2;
						Contactdetail.rate_type = userDescribeYourSituationBKC.RateType;
						Contactdetail.orig_bal = (float) userDescribeYourSituationBKC.OrigBal;
						Contactdetail.owe_home = (float) userDescribeYourSituationBKC.OweHome;
						Contactdetail.val_home = (float) userDescribeYourSituationBKC.ValHome;
						Contactdetail.mo_pmt = (float) userDescribeYourSituationBKC.MoPmt;
						Contactdetail.amt_avail = (float) userDescribeYourSituationBKC.AmtAvail;
						Contactdetail.last_contact_date = userDescribeYourSituationBKC.LastContactDate;
						Contactdetail.last_contact_desc = userDescribeYourSituationBKC.LastContactDesc;
						Contactdetail.repay_plan = userDescribeYourSituationBKC.RepayPlan;
						Contactdetail.secondary_holder = userDescribeYourSituationBKC.SecondaryHolder;
						Contactdetail.secondary_amt = (float) userDescribeYourSituationBKC.SecondaryAmt;
						Contactdetail.secondary_status = userDescribeYourSituationBKC.SecondaryStatus;
						Contactdetail.Prop4Sale = userDescribeYourSituationBKC.Prop4Sale;
						Contactdetail.Note4Close = userDescribeYourSituationBKC.Note4Close;
						Contactdetail.WhoInHouse = userDescribeYourSituationBKC.WhoInHouse;
						Contactdetail.loan_number2 = userDescribeYourSituationBKC.LoanNumber2;
						Contactdetail.PriServID = userDescribeYourSituationBKC.PriServID;
						Contactdetail.SecServID = userDescribeYourSituationBKC.SecServID;
						Contactdetail.MakeHomeAff = userDescribeYourSituationBKC.MakeHomeAff;
						Contactdetail.ModForm1 = userDescribeYourSituationBKC.ModForm1;

						Entities.SubmitChanges();

						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;

					}
					else // Add New User 
					{
						Contactdetail = new contactdetail();
						Contactdetail.mort_holder = userDescribeYourSituationBKC.MortHolder;
						Contactdetail.loan_number = userDescribeYourSituationBKC.LoanNumber;
						Contactdetail.mort_type = userDescribeYourSituationBKC.MortType;
						Contactdetail.mort_years = (float) userDescribeYourSituationBKC.MortYears;
						Contactdetail.mort_date = userDescribeYourSituationBKC.MortDate;
						Contactdetail.mort_rate = (float) userDescribeYourSituationBKC.MortRate2;
						Contactdetail.rate_type = userDescribeYourSituationBKC.RateType;
						Contactdetail.orig_bal = (float) userDescribeYourSituationBKC.OrigBal;
						Contactdetail.owe_home = (float) userDescribeYourSituationBKC.OweHome;
						Contactdetail.val_home = (float) userDescribeYourSituationBKC.ValHome;
						Contactdetail.mo_pmt = (float) userDescribeYourSituationBKC.MoPmt;
						Contactdetail.amt_avail = (float) userDescribeYourSituationBKC.AmtAvail;
						Contactdetail.last_contact_date = userDescribeYourSituationBKC.LastContactDate;
						Contactdetail.last_contact_desc = userDescribeYourSituationBKC.LastContactDesc;
						Contactdetail.repay_plan = userDescribeYourSituationBKC.RepayPlan;
						Contactdetail.secondary_holder = userDescribeYourSituationBKC.SecondaryHolder;
						Contactdetail.secondary_amt = (float) userDescribeYourSituationBKC.SecondaryAmt;
						Contactdetail.secondary_status = userDescribeYourSituationBKC.SecondaryStatus;
						Contactdetail.Prop4Sale = userDescribeYourSituationBKC.Prop4Sale;
						Contactdetail.Note4Close = userDescribeYourSituationBKC.Note4Close;
						Contactdetail.WhoInHouse = userDescribeYourSituationBKC.WhoInHouse;
						Contactdetail.loan_number2 = userDescribeYourSituationBKC.LoanNumber2;
						Contactdetail.PriServID = userDescribeYourSituationBKC.PriServID;
						Contactdetail.SecServID = userDescribeYourSituationBKC.SecServID;
						Contactdetail.MakeHomeAff = userDescribeYourSituationBKC.MakeHomeAff;
						Contactdetail.ModForm1 = userDescribeYourSituationBKC.ModForm1;

						Entities.contactdetails.InsertOnSubmit(Contactdetail);
						Entities.SubmitChanges();
						UpdateResult.ClientNumber = Contactdetail.client_number;
						UpdateResult.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					UpdateResult.IsSuccessful = false;
					UpdateResult.Exception = exception;
					UpdateResult.Messages = new string[] { "An error occured attempting to save." };
				}
				return UpdateResult;
			}


		}

		public UserDescribeYourSituationBKC BKCUserDescribeYourSituationGet(int ClientNumber)
		{
			UserDescribeYourSituationBKC UserContactDetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserContactDetail =
				 (from entity in Entities.contactdetails
				  where entity.client_number == ClientNumber
				  select new UserDescribeYourSituationBKC
				  {
					  ClientNumber = entity.client_number,
					  BkForPrv = entity.BkForPrv,
					  Bankrupt = (entity.bankrupt != null) ? entity.bankrupt : string.Empty,
					  ContactReason = (entity.contact_reason != null) ? entity.contact_reason : string.Empty,
					  ContactComments = (entity.contact_comments != null) ? entity.contact_comments : string.Empty,
					  MortCurrent = entity.mort_current,
					  MosDelinq = (entity.mos_delinq != null) ? Convert.ToDecimal(entity.mos_delinq) : 0,
					  HousingType = (entity.housing_type != null) ? Convert.ToChar(entity.housing_type) : '0',
					  MortHolder = (entity.mort_holder != null) ? entity.mort_holder : String.Empty,
					  LoanNumber = (entity.loan_number != null) ? entity.loan_number : String.Empty,
					  MortType = (entity.mort_type != null) ? entity.mort_type : String.Empty,
					  MortYears = (entity.mort_years != null) ? Convert.ToDecimal(entity.mort_years) : 0,
					  MortDate = (entity.mort_date != null) ? entity.mort_date : String.Empty,
					  MortRate2 = (entity.mort_rate != null) ? Convert.ToDecimal(entity.mort_rate) : 0,
					  RateType = (entity.rate_type != null) ? entity.rate_type : String.Empty,
					  OrigBal = (entity.orig_bal != null) ? Convert.ToDecimal(entity.orig_bal) : 0,
					  OweHome = (entity.owe_home != null) ? Convert.ToDecimal(entity.owe_home) : 0,
					  ValHome = (entity.val_home != null) ? Convert.ToDecimal(entity.val_home) : 0,
					  MoPmt = (entity.mo_pmt != null) ? Convert.ToDecimal(entity.mo_pmt) : 0,
					  AmtAvail = (entity.amt_avail != null) ? Convert.ToDecimal(entity.amt_avail) : 0,
					  LastContactDate = (entity.last_contact_date != null) ? entity.last_contact_date : String.Empty,
					  LastContactDesc = (entity.last_contact_desc != null) ? entity.last_contact_desc : String.Empty,
					  RepayPlan = (entity.repay_plan != null) ? entity.repay_plan : String.Empty,
					  SecondaryHolder = (entity.secondary_holder != null) ? entity.secondary_holder : String.Empty,
					  SecondaryAmt = (entity.secondary_amt != null) ? Convert.ToDecimal(entity.secondary_amt) : 0,
					  SecondaryStatus = (entity.secondary_status != null) ? entity.secondary_status : String.Empty,
					  Prop4Sale = (entity.Prop4Sale != null) ? entity.Prop4Sale : String.Empty,
					  Note4Close = (entity.Note4Close != null) ? entity.Note4Close : String.Empty,
					  WhoInHouse = (entity.WhoInHouse != null) ? entity.WhoInHouse : String.Empty,
					  LoanNumber2 = (entity.loan_number2 != null) ? entity.loan_number2 : String.Empty,
					  PriServID = (entity.PriServID != null) ? Convert.ToInt32(entity.PriServID) : 0,
					  SecServID = (entity.SecServID != null) ? Convert.ToInt32(entity.SecServID) : 0,
					  MakeHomeAff = (entity.MakeHomeAff != null) ? entity.MakeHomeAff : String.Empty,
					  ModForm1 = (entity.ModForm1 != null) ? Convert.ToInt32(entity.ModForm1) : 0,
					  SizeofHouseHold = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
				  }
				).FirstOrDefault();
			}

			return UserContactDetail;
		}

		#endregion User Describe Your Situation BKC

		#region BKC Comman Options

		public CommonOptionBKCounseling CommonOptionBKCounselingContactDetailsGet(int ClientNumber)
		{
			CommonOptionBKCounseling UserContactDetail = null;
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				UserContactDetail =
				 (
						 from entity in Entities.contactdetails
						 where entity.client_number == ClientNumber
						 select new CommonOptionBKCounseling
						 {
							 ClientNumber = entity.client_number,
							 ContactReason = (entity.contact_reason != null) ? entity.contact_reason : string.Empty,
							 ContactComments = (entity.contact_comments != null) ? entity.contact_comments : string.Empty,
							 MortCurrent = (entity.mort_current != null) ? Convert.ToInt32(entity.mort_current) : 2,
							 MosDelinq = (entity.mos_delinq != null) ? Convert.ToDecimal(entity.mos_delinq) : 0,
							 HousingType = entity.housing_type != null ? Convert.ToChar(entity.housing_type) : '\0',
							 MortHolder = (entity.mort_holder != null) ? entity.mort_holder : String.Empty,
							 LoanNumber = (entity.loan_number != null) ? entity.loan_number : String.Empty,
							 MortType = (entity.mort_type != null) ? entity.mort_type : String.Empty,
							 MortYears = (entity.mort_years != null) ? Convert.ToDecimal(entity.mort_years) : 0,
							 MortDate = (entity.mort_date != null) ? entity.mort_date : String.Empty,
							 MortRate = (entity.mort_rate != null) ? Convert.ToDecimal(entity.mort_rate) : 0,
							 RateType = (entity.rate_type != null) ? entity.rate_type : String.Empty,
							 OrigBal = (entity.orig_bal != null) ? Convert.ToDecimal(entity.orig_bal) : 0,
							 OweHome = (entity.owe_home != null) ? Convert.ToDecimal(entity.owe_home) : 0,
							 ValHome = (entity.val_home != null) ? Convert.ToDecimal(entity.val_home) : 0,
							 MoPmt = (entity.mo_pmt != null) ? Convert.ToDecimal(entity.mo_pmt) : 0,
							 AmtAvail = (entity.amt_avail != null) ? Convert.ToDecimal(entity.amt_avail) : 0,
							 LastContactDate = (entity.last_contact_date != null) ? entity.last_contact_date : String.Empty,
							 Includeti = (entity.include_ti != null) ? entity.include_ti : String.Empty,
							 CcrcAprov = (entity.CcrcAprov != null) ? entity.CcrcAprov : String.Empty,
							 LastContactDesc = (entity.last_contact_desc != null) ? entity.last_contact_desc : String.Empty,
							 RepayPlan = (entity.repay_plan != null) ? entity.repay_plan : String.Empty,
							 SecondaryHolder = (entity.secondary_holder != null) ? entity.secondary_holder : String.Empty,
							 SecondaryAmt = (entity.secondary_amt != null) ? Convert.ToDecimal(entity.secondary_amt) : 0,
							 SecondaryStatus = (entity.secondary_status != null) ? entity.secondary_status : String.Empty,
							 Prop4Sale = (entity.Prop4Sale != null) ? entity.Prop4Sale : String.Empty,
							 Note4Close = (entity.Note4Close != null) ? entity.Note4Close : String.Empty,
							 WhoInHouse = (entity.WhoInHouse != null) ? entity.WhoInHouse : String.Empty,
							 LoanNumber2 = (entity.loan_number2 != null) ? entity.loan_number2 : String.Empty,
							 Contactstate = (entity.contact_state != null) ? entity.contact_state : String.Empty,
							 Contactzip = (entity.contact_zip != null) ? entity.contact_zip : String.Empty,
							 SecServID = (entity.SecServID != null) ? Convert.ToInt32(entity.SecServID) : 0,
							 SizeofHouseHold = (entity.size_of_household != null) ? Convert.ToInt32(entity.size_of_household) : 0,
						 }
						).FirstOrDefault();
			}
			return UserContactDetail;
		}

		#endregion

	}
}