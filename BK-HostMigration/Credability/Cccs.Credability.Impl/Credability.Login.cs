﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public int UserClientNumberGetTEST(string TempID,UserLoginTest userLogin)
        {
            contactdetail Contactdetail = null;
            int ClientNumber=0;
            using (CredabilityDataContext Entities = new CredabilityDataContext())
            {

                Contactdetail = new contactdetail();
                Contactdetail.contact_email = userLogin.Email;
                Entities.contactdetails.InsertOnSubmit(Contactdetail);
                Entities.SubmitChanges();

                Contactdetail =
                     (
                         from contat in Entities.contactdetails
                         where contat.temp_id == TempID
                         select contat
                     ).FirstOrDefault<contactdetail>();

                ClientNumber = Contactdetail.client_number;
            }
            return ClientNumber;

        }
    }
}
