﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Credability.Dal;
using Cccs.Identity;
using System.Data.Odbc;
using System.Data;
using System.Web;
using System.Configuration;
using Cccs.Configuration;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace Cccs.Credability.Impl
{
    public partial class Credability : ICredability
    {
        public DataSet GetSQLDataset(String Query)
        {
            DataSet dataset = new DataSet();
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlDataAdapter adapter = new MySqlDataAdapter(Query, conn))
                {
                    adapter.Fill(dataset);
                }
                conn.Close();
            }
            return dataset;
        }

        public DataSet UserinfoMySqlGet(String RegNum)
        {
            DataSet dataset = new DataSet();
            string Query = String.Format("select educationlvl,age,income,numhousehold from users where regnum={0} ", RegNum);
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlDataAdapter adapter = new MySqlDataAdapter(Query, conn))
                {
                    adapter.Fill(dataset);
                }
                conn.Close();
            }
            return dataset;
        }

        public MySqlUsersInfoUpdateResult UserInfoMySqlUpdate(MySqlUsersInfoUpdate UserInfo)
        {
            MySqlUsersInfoUpdateResult Result = new MySqlUsersInfoUpdateResult();
            var lastName = Regex.Replace(UserInfo.lname.Trim(), @"[\r\n\x00\x1a\\'""]", @"\$0");
            string Query = "update users set fname='" + UserInfo.fname.Trim() + "',lname='" + lastName + "',phone='" + UserInfo.phone.Trim() + "',email='" + UserInfo.email.Trim() + "',educationlvl='" + UserInfo.educationlvl.Trim() + "',gender='" + UserInfo.gender.Trim() + "',mstatus='" + UserInfo.mstatus.Trim() + "',numhousehold='" + UserInfo.numhousehold.Trim() + "',age='" + UserInfo.age.Trim() + "',income='" + UserInfo.income.Trim() + "'  where regnum=" + UserInfo.RegNum.ToString();
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlCommand MySqlCommands = new MySqlCommand(Query, conn))
                {
                    int RecordCount = MySqlCommands.ExecuteNonQuery();
                    Result.IsSuccessful = (RecordCount != 0);
                }
                conn.Close();
            }
            return Result;
        }

        public bool GetCreateNewUser(String Query)
        {
            bool is_successful = false;
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlCommand MySqlCommands = new MySqlCommand(Query, conn))
                {
                    int RecordCount = MySqlCommands.ExecuteNonQuery();
                    is_successful = (RecordCount != 0);
                }
                conn.Close();
            }
            return is_successful;
        }

        public bool CreateNewRegNumInMySql(UserNamePasswordRegnum UserNamePassword)
        {
            string Query = String.Format("insert into users(uid,password,RegDateStamp) values('{0}', '{1}', '{2}')", UserNamePassword.username, UserNamePassword.username, DateTime.Now.ToString("yyyy-MM-dd"));
            bool is_successful = false;
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlCommand MySqlCommands = new MySqlCommand(Query, conn))
                {
                    int RecordCount = MySqlCommands.ExecuteNonQuery();
                    is_successful = (RecordCount != 0);
                }
                conn.Close();
            }
            return is_successful;
        }

        public DataSet RegNumGet(String username)
        {
            DataSet dataset = new DataSet();
            string Query = "select regnum from users where uid='" + username.Trim().ToString() + "'";
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlDataAdapter adapter = new MySqlDataAdapter(Query, conn))
                {
                    adapter.Fill(dataset);
                }
                conn.Close();
            }
            return dataset;
        }

        public void MySqlRevCountUpdate(string username)
        {
            var query = String.Format("update users set RevCnt = RevCnt + 1 where uid = '{0}'", username);
            using(var conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(var command = new MySqlCommand(query, conn))
                {
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public DataSet MySQLUserRecordGet(String UserName)
        {
            DataSet dataset = new DataSet();
            string Query = "Select * from users where uid='" + UserName.ToString() + "'";
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlDataAdapter adapter = new MySqlDataAdapter(Query, conn))
                {
                    adapter.Fill(dataset);
                }
                conn.Close();
            }
            return dataset;
        }

        public DataSet MySQLUserRecordGet(long RegNo)
        {
            DataSet dataset = new DataSet();
            string Query = "Select * from users where regnum='" + RegNo.ToString() + "'";
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlDataAdapter adapter = new MySqlDataAdapter(Query, conn))
                {
                    adapter.Fill(dataset);
                }
                conn.Close();
            }
            return dataset;
        }

        public bool LastPagePhpPageInfoUpdate(string userName, int lastPage, int preTest)
        {
            bool is_successful = false;
            string Query = String.Format("update users set lastpage='{0}', pretest='{1}' where uid='{2}'", lastPage.ToString(), preTest.ToString(), userName);
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlCommand MySqlCommands = new MySqlCommand(Query, conn))
                {
                    int RecordCount = MySqlCommands.ExecuteNonQuery();
                    is_successful = (RecordCount != 0);
                }
                conn.Close();
            }
            return is_successful;
        }

        public bool UserScoreUpdate(String UserName, String TestDTS, String PostTestScore)
        {
            bool is_successful = false;
            string Query = "update users set posttest='" + PostTestScore + "', TestDTS='" + TestDTS + "' where uid='" + UserName + "'";
            using(MySqlConnection conn = new MySqlConnection(bkeMySqlConnection))
            {
                conn.Open();
                using(MySqlCommand MySqlCommands = new MySqlCommand(Query, conn))
                {
                    int RecordCount = MySqlCommands.ExecuteNonQuery();
                    is_successful = (RecordCount != 0);
                }
                conn.Close();
            }
            return is_successful;
        }

        public void FixMySqlName(long RegNo, string UserName)
        {
            bool DoneOuter = false, DoneInner;
            int i = 1;
            string xSel, CurRegNo;
            MySqlDataReader xRec;
            using(MySqlConnection MyConn = new MySqlConnection(bkeMySqlConnection))
            {
                MyConn.Open();
                using(MySqlCommand MyCom = new MySqlCommand())
                {
                    MyCom.Connection = MyConn;
                    while(!DoneOuter)
                    {
                        xSel = String.Format("select regnum from users where uid = '{0}'", UserName);
                        MyCom.CommandText = xSel;
                        xRec = MyCom.ExecuteReader();
                        if(xRec.HasRows)
                        {
                            xRec.Read();
                            CurRegNo = xRec.GetValue(xRec.GetOrdinal("regnum")).ToString();
                            xRec.Close();
                            DoneInner = false;
                            while(!DoneInner)
                            {
                                xSel = String.Format("select regnum from users where uid = '{0}_{1}'", UserName, i.ToString());
                                MyCom.CommandText = xSel;
                                xRec = MyCom.ExecuteReader();
                                if(xRec.HasRows)
                                {
                                    i++;
                                    xRec.Close();
                                }
                                else
                                {
                                    xRec.Close();
                                    xSel = String.Format("update users set uid = '{0}_{1}' where regnum = '{2}'", UserName, i.ToString(), CurRegNo);
                                    MyCom.CommandText = xSel;
                                    MyCom.ExecuteNonQuery();
                                    DoneInner = true;
                                }
                            }
                        }
                        else
                        {
                            xRec.Close();
                            xSel = String.Format("update users set uid = '{0}' where regnum = '{1}'", UserName, RegNo.ToString());
                            MyCom.CommandText = xSel;
                            MyCom.ExecuteNonQuery();
                            DoneOuter = true;
                        }
                    }
                }
                MyConn.Close();
            }
        }
    }
}
