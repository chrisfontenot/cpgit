﻿using System.Linq;
using Cccs.Credability.Dal;
namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{
		public RegulatedState RegulatedStateGet(string StateCode)
		{
			using(CredabilityDataContext DataCon = new CredabilityDataContext())
			{
				return (
				from d in DataCon.RegulatedStates
				where (d.ST == StateCode)
				select new RegulatedState
				{
					ST = d.ST,
					State = d.State,
					DmpReg = d.DmpReg,
					RvmBan = d.RvmBan
				}
				).FirstOrDefault();
			}
		}
	}
}