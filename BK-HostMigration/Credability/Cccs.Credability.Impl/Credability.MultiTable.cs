﻿using System;
using System.Linq;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Impl
{
	public partial class Credability : ICredability
	{
		public FinanicalSituationDetails UserFinalFinanicalSituationGet(int ClientNumber)
		{
			FinanicalSituationDetails Result = new FinanicalSituationDetails();
			contactdetail Contactdetail = null;

			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				Contactdetail =
					(
					from Entity in Entities.contactdetails
					where Entity.client_number == ClientNumber
					select Entity
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					Result.ReasonForhelp = (Contactdetail.reason_for_help != null) ? (Contactdetail.reason_for_help) : string.Empty;
					Result.ClientNumber = Contactdetail.client_number;
				}
			}

			Result.MonthlyExpenses = UserMonthlyExpensesGet(ClientNumber);
			//Result.CreditorTotals = //CreditorPaymentsGet(ClientNumber);
			Result.Income = UserMonthlyIncomeGet(ClientNumber);
			Result.AssetsLiabilities = UserAssetLiabilitiesGet(ClientNumber);
			return Result;
		}


		public UserMonthlyExpenses UserMonthlyExpensesGet(int ClientNumber)
		{
			UserMonthlyExpenses MonthlyExpenses = new UserMonthlyExpenses();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				var Contactdetail =
				 (
					 from Entity in Entities.contactdetails
					 where Entity.client_number == ClientNumber
					 select Entity
					).FirstOrDefault<contactdetail>();

				if(Contactdetail != null)
				{
					MonthlyExpenses.ClientNumber = Contactdetail.client_number;
					//Monthly Food & Shelter
					MonthlyExpenses.RentMort = Contactdetail.rent_mort.GetValueOrDefault(0);
					if(MonthlyExpenses.RentMort == 0)
					{
						MonthlyExpenses.RentMort = (decimal) Contactdetail.mo_pmt.GetValueOrDefault(0);
					}
					MonthlyExpenses.Mort2nd = (Contactdetail.equity_loan_tax_ins != null) ? Convert.ToDecimal(Contactdetail.equity_loan_tax_ins) : 0;
					MonthlyExpenses.HomeMaintenance = (Contactdetail.home_maintenance != null) ? Convert.ToDecimal(Contactdetail.home_maintenance) : 0;
					//Kill Meeee!!!!!
					//MonthlyExpenses.Utilities = (Contactdetail.utilities != null) ? Convert.ToDecimal(Contactdetail.utilities) : 0;
					MonthlyExpenses.Telephone = (Contactdetail.telephone != null) ? Convert.ToDecimal(Contactdetail.telephone) : 0;
					MonthlyExpenses.FoodAway = (Contactdetail.food_away != null) ? Convert.ToDecimal(Contactdetail.food_away) : 0;
					MonthlyExpenses.Groceries = (Contactdetail.groceries != null) ? Convert.ToDecimal(Contactdetail.groceries) : 0;

					// Monthly Transportation
					MonthlyExpenses.CarCurrent = (Contactdetail.car_current != null) ? Convert.ToInt32(Contactdetail.car_current) : -1;
					MonthlyExpenses.CarPayments = (Contactdetail.vehicle_payments != null) ? Convert.ToDecimal(Contactdetail.vehicle_payments) : 0;
					MonthlyExpenses.CarInsurance = (Contactdetail.car_insurance != null) ? Convert.ToDecimal(Contactdetail.car_insurance) : 0;
					MonthlyExpenses.CarMaintenance = (Contactdetail.car_maintenance != null) ? Convert.ToDecimal(Contactdetail.car_maintenance) : 0;
					MonthlyExpenses.PublicTransportation = (Contactdetail.public_transportation != null) ? Convert.ToDecimal(Contactdetail.public_transportation) : 0;

					//Monthly Insurance, Medical & Childcare
					MonthlyExpenses.Insurance = (Contactdetail.insurance != null) ? Convert.ToDecimal(Contactdetail.insurance) : 0;
					MonthlyExpenses.MedicalPrescription = (Contactdetail.medical_prescription != null) ? Convert.ToDecimal(Contactdetail.medical_prescription) : 0;
					MonthlyExpenses.ChildSupportAlimony = (Contactdetail.child_support != null) ? Convert.ToDecimal(Contactdetail.child_support) : 0;
					MonthlyExpenses.ChildElderCare = (Contactdetail.child_elder_care != null) ? Convert.ToDecimal(Contactdetail.child_elder_care) : 0;
					MonthlyExpenses.Education = (Contactdetail.education != null) ? Convert.ToDecimal(Contactdetail.education) : 0;

					//Personal & Miscellaneous Expenses
					MonthlyExpenses.OtherLoans = (Contactdetail.OtherLoans != null) ? Convert.ToInt32(Contactdetail.OtherLoans) : 0;
					MonthlyExpenses.Contributions = (Contactdetail.contributions != null) ? Convert.ToInt32(Contactdetail.contributions) : 0;
					MonthlyExpenses.Clothing = (Contactdetail.clothing != null) ? Convert.ToDecimal(Contactdetail.clothing) : 0;
					MonthlyExpenses.Laundry = (Contactdetail.laundry != null) ? Convert.ToDecimal(Contactdetail.laundry) : 0;
					MonthlyExpenses.PersonalExpenses = (Contactdetail.personal_exp != null) ? Convert.ToDecimal(Contactdetail.personal_exp) : 0;
					MonthlyExpenses.BeautyBarber = (Contactdetail.beauty_barber != null) ? Convert.ToDecimal(Contactdetail.beauty_barber) : 0;
					MonthlyExpenses.Recreation = (Contactdetail.recreation != null) ? Convert.ToDecimal(Contactdetail.recreation) : 0;
					MonthlyExpenses.ClubDues = (Contactdetail.club_dues != null) ? Convert.ToDecimal(Contactdetail.club_dues) : 0;
					MonthlyExpenses.Gifts = (Contactdetail.gifts != null) ? Convert.ToDecimal(Contactdetail.gifts) : 0;
					MonthlyExpenses.Miscellaneous = (Contactdetail.misc_exp != null) ? Convert.ToDecimal(Contactdetail.misc_exp) : 0;
				}
				var HudAbt =
				 (
					 from Entity in Entities.ExtraHudAbts
					 where Entity.client_number == ClientNumber
					 select Entity
					).FirstOrDefault<ExtraHudAbt>();

				if(HudAbt != null)
				{
					MonthlyExpenses.IncludeFha = (HudAbt.IncludeFha != null) ? HudAbt.IncludeFha : String.Empty;
					MonthlyExpenses.MoEquity = (HudAbt.MoEquity != null) ? (float) HudAbt.MoEquity : 0;
					MonthlyExpenses.Mo2ndMort = (HudAbt.Mo2ndMort != null) ? (float) HudAbt.Mo2ndMort : 0;
					MonthlyExpenses.AnPropIns = (HudAbt.AnPropIns != null) ? (float) HudAbt.AnPropIns : 0;
					MonthlyExpenses.AnPropTax = (HudAbt.AnPropTax != null) ? (float) HudAbt.AnPropTax : 0;
					MonthlyExpenses.MoFee = (HudAbt.MoFee != null) ? (float) HudAbt.MoFee : 0;
					MonthlyExpenses.UtlElectric = (HudAbt.UtlElectric != null) ? (float) HudAbt.UtlElectric : 0;
					MonthlyExpenses.UtlWater = (HudAbt.UtlWater != null) ? (float) HudAbt.UtlWater : 0;
					MonthlyExpenses.UtlGas = (HudAbt.UtlGas != null) ? (float) HudAbt.UtlGas : 0;
					MonthlyExpenses.UtlTv = (HudAbt.UtlTv != null) ? (float) HudAbt.UtlTv : 0;
					MonthlyExpenses.UtlTrash = (HudAbt.UtlTrash != null) ? (float) HudAbt.UtlTrash : 0;
				}
			}
			return MonthlyExpenses;
		}


		public UserMonthlyExpensesResult UserMonthlyExpensesUpdate(UserMonthlyExpenses MonthlyExpenses)
		{
			UserMonthlyExpensesResult Result = new UserMonthlyExpensesResult();
			using(CredabilityDataContext Entities = new CredabilityDataContext())
			{
				var Contactdetail =
				 (
					 from contat in Entities.contactdetails
					 where contat.client_number == MonthlyExpenses.ClientNumber
					 select contat
					).FirstOrDefault<contactdetail>();


				if(Contactdetail != null)
				{
					try
					{
						//Contactdetail.client_number = UsersMonthlyexpenses.ClientNumber;
						Contactdetail.rent_mort = MonthlyExpenses.RentMort;
						Contactdetail.equity_loan_tax_ins = MonthlyExpenses.Mort2nd;
						Contactdetail.home_maintenance = MonthlyExpenses.HomeMaintenance;
						//Killl Mee!!!!!!
						//Contactdetail.utilities = MonthlyExpenses.Utilities;
						Contactdetail.telephone = MonthlyExpenses.Telephone;
						Contactdetail.food_away = MonthlyExpenses.FoodAway;
						Contactdetail.groceries = MonthlyExpenses.Groceries;

						// Monthly Transportation
						Contactdetail.car_current = MonthlyExpenses.CarCurrent;
						Contactdetail.vehicle_payments = MonthlyExpenses.CarPayments;
						Contactdetail.car_insurance = MonthlyExpenses.CarInsurance;
						Contactdetail.car_maintenance = MonthlyExpenses.CarMaintenance;
						Contactdetail.public_transportation = MonthlyExpenses.PublicTransportation;

						//Monthly Insurance, Medical & Childcare
						Contactdetail.insurance = MonthlyExpenses.Insurance;
						Contactdetail.medical_prescription = MonthlyExpenses.MedicalPrescription;
						Contactdetail.child_support = MonthlyExpenses.ChildSupportAlimony;
						Contactdetail.child_elder_care = MonthlyExpenses.ChildElderCare;
						Contactdetail.education = MonthlyExpenses.Education;
						//Personal & Miscellaneous Expenses
						Contactdetail.OtherLoans = MonthlyExpenses.OtherLoans;
						Contactdetail.contributions = MonthlyExpenses.Contributions;
						Contactdetail.clothing = MonthlyExpenses.Clothing;
						Contactdetail.laundry = MonthlyExpenses.Laundry;
						Contactdetail.personal_exp = MonthlyExpenses.PersonalExpenses;
						Contactdetail.beauty_barber = MonthlyExpenses.BeautyBarber;
						Contactdetail.recreation = MonthlyExpenses.Recreation;
						Contactdetail.club_dues = MonthlyExpenses.ClubDues;
						Contactdetail.gifts = MonthlyExpenses.Gifts;
						Contactdetail.misc_exp = MonthlyExpenses.Miscellaneous;
						Entities.SubmitChanges();
						Result.ClientNumber = Contactdetail.client_number;
						Result.IsSuccessful = true;
					}
					catch(Exception exception)
					{
						Result.IsSuccessful = false;
						Result.Exception = exception;
						Result.Messages = new string[] { "An error occured attempting to save." };
					}
				}

				try
				{

					var HudAbt =
					 (
						 from entity in Entities.ExtraHudAbts
						 where entity.client_number == MonthlyExpenses.ClientNumber
						 select entity
					 ).FirstOrDefault<ExtraHudAbt>();
					if(HudAbt != null) //Update
					{
						HudAbt.IncludeFha = MonthlyExpenses.IncludeFha;
						HudAbt.MoEquity = MonthlyExpenses.MoEquity;
						HudAbt.Mo2ndMort = MonthlyExpenses.Mo2ndMort;
						HudAbt.MoFee = MonthlyExpenses.MoFee;
						HudAbt.UtlElectric = MonthlyExpenses.UtlElectric;
						HudAbt.UtlGas = MonthlyExpenses.UtlGas;
						HudAbt.UtlTv = MonthlyExpenses.UtlTv;
						HudAbt.UtlTrash = MonthlyExpenses.UtlTrash;
						if(MonthlyExpenses.AnPropIns != 0.0)
							HudAbt.AnPropIns = MonthlyExpenses.AnPropIns;
						if(MonthlyExpenses.AnPropTax != 0.0)
							HudAbt.AnPropTax = MonthlyExpenses.AnPropTax;
						HudAbt.UtlWater = MonthlyExpenses.UtlWater;

						Entities.SubmitChanges();
						Result.IsSuccessful = true;

					}
					else // Add New User 
					{
						HudAbt = new ExtraHudAbt();
						HudAbt.client_number = MonthlyExpenses.ClientNumber;
						HudAbt.IncludeFha = MonthlyExpenses.IncludeFha;
						HudAbt.MoEquity = MonthlyExpenses.MoEquity;
						HudAbt.Mo2ndMort = MonthlyExpenses.Mo2ndMort;
						HudAbt.MoFee = MonthlyExpenses.MoFee;
						HudAbt.UtlElectric = MonthlyExpenses.UtlElectric;
						HudAbt.UtlGas = MonthlyExpenses.UtlGas;
						HudAbt.UtlTv = MonthlyExpenses.UtlTv;
						HudAbt.UtlTrash = MonthlyExpenses.UtlTrash;
						if(MonthlyExpenses.AnPropIns != 0.0)
							HudAbt.AnPropIns = MonthlyExpenses.AnPropIns;
						if(MonthlyExpenses.AnPropTax != 0.0)
							HudAbt.AnPropTax = MonthlyExpenses.AnPropTax;
						HudAbt.UtlWater = MonthlyExpenses.UtlWater;

						Entities.ExtraHudAbts.InsertOnSubmit(HudAbt);
						Entities.SubmitChanges();
						Result.IsSuccessful = true;
					}
				}
				catch(Exception exception)
				{
					Result.IsSuccessful = false;
					Result.Exception = exception;
					Result.Messages = new string[] { "An error occured attempting to save." };
				}
			}
			return Result;
		}
	}
}