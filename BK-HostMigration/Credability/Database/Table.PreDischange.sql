USE [cccsIdentity]
GO

/****** Object:  Table [dbo].[PreDischangeCertificate]    Script Date: 11/10/2016 6:11:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreDischangeCertificate](
	[AccountId] [bigint] NOT NULL,
	[PrimaryCertificate] [varchar](50) NOT NULL,
	[SecondaryCertificate] [varchar](50) NULL,
	[CompletionDate] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

