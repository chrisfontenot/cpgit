﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cccs.Tests;
using Cccs.Credability.Certificates.PreFiling.Generators;
using Cccs.Credability.Certificates.DataTransferObjects;
using System.IO;
using Aspose.Pdf.Kit;
using System.Reflection;

namespace Cccs.Credability.Certificates.Tests.PreFiling.Generators
{
    [TestClass]
    public class CounselingGenerator4Tests
    {
        private const string AsposeLicense = "Cccs.Credability.Certificates.Tests.Aspose.Total.lic";
        public CounselingGenerator4Tests()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var licenseStream = assembly.GetManifestResourceStream(AsposeLicense);

            var wordLicense = new Aspose.Words.License();
            licenseStream.Position = 0;
            wordLicense.SetLicense(licenseStream);

            var barcodeLicense = new Aspose.BarCode.License();
            licenseStream.Position = 0;
            barcodeLicense.SetLicense(licenseStream);

            var pdfLicense = new Aspose.Pdf.Kit.License();
            licenseStream.Position = 0;
            pdfLicense.SetLicense(licenseStream);

            var chartLicense = new Aspose.Report.License();
            licenseStream.Position = 0;
            chartLicense.SetLicense(licenseStream);

        }

        [TestMethod]
        public void CreateActionPlan_Method_Should_Generate_Document()
        {
            var primarySnapshotDto = new PreFilingSnapshotDto
            {
                ClientNumber = 1477870,
                CertificateNumber = "02114-GAN-CC-013265134",
                FirstName = "Test",
                LastName = "Client",
                LanguageCode = "en",
                JudicialDistrict = "Northern District of Georgia",
                CourseCompleted = new DateTime(2010, 12, 10, 13, 41, 00),
                CertificateGenerated = new DateTime(2010, 12, 10, 13, 41, 01),
                CertificateVersionType = "Cccs.Credability.Certificates.PreFiling.Generators.CounselingGenerator3",
                SsnLast4 = "3333",
                CreditScore = 500,
                StreetLine1 = "100 Test St.",
                City = "Testville",
                State = "GA",
                PostalCode = "30306",
                IncomeAfterTaxes = 1700m,
                LivingExpenses = 3002m,
                DebtPayment = 554m,
                CashAtMonthEnd = -1856m,
                TotalAssets = 212000m,
                TotalLiabilities = 211000m,
                NetWorth = 1000m,
                Reason = "01",
                CounselorName = "Brian Young",
                ArchiveStatus = 0,
                DmpInterestRate = .09m,
                DmpBalance = 12000m,
                DmpMonthlyPayment = 300m,
                DmpMonthsToRepay = 48,

                Creditors = new List<PreFilingSnapshotCreditorDto>
                {
                    new PreFilingSnapshotCreditorDto { Name = "TEST", DisplayOrder = 1, InterestRate = .24m, Balance = 5000m, MonthlyPayment = 254m, MonthsToRepay = 26 },
                    new PreFilingSnapshotCreditorDto { Name = "TEST 2", DisplayOrder = 2, InterestRate = .29m, Balance = 7000m, MonthlyPayment = 300m, MonthsToRepay = 31 },
                },

                ExpenseCategories = new List<PreFilingSnapshotExpenseCategoryDto>
                {
                    new PreFilingSnapshotExpenseCategoryDto { ExpenseCategoryId = 1, ExpenseCategory = "Housing", DisplayText = "Housing", DisplayOrder = 1, SessionAmount = 2327m, PercentOfTotal = .664m, RecommendedPercentage = .38m, RecommendedTotal = 646m },
                    new PreFilingSnapshotExpenseCategoryDto { ExpenseCategoryId = 2, ExpenseCategory = "Transportation", DisplayText = "Transportation", DisplayOrder = 2, SessionAmount = 295m, PercentOfTotal = .084m, RecommendedPercentage = .15m, RecommendedTotal = 255m },
                    new PreFilingSnapshotExpenseCategoryDto { ExpenseCategoryId = 3, ExpenseCategory = "Food", DisplayText = "Food", DisplayOrder = 3, SessionAmount = 186m, PercentOfTotal = .053m, RecommendedPercentage = .12m, RecommendedTotal = 204m },
                    new PreFilingSnapshotExpenseCategoryDto { ExpenseCategoryId = 4, ExpenseCategory = "Other Expenses", DisplayText = "Other Expenses", DisplayOrder = 4, SessionAmount = 694m, PercentOfTotal = .198m, RecommendedPercentage = .35m, RecommendedTotal = 595m },
                },

                ExpenseCodes = new List<PreFilingSnapshotExpenseCodeDto>
                {
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 1, ExpenseCodeId = 1, ExpenseCode = "Rent or Mortgage Payment", DisplayText = "Rent or Mortgage Payment", DisplayOrder = 1, SessionAmount = 1750m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 1, ExpenseCodeId = 4, ExpenseCode = "Utilities", DisplayText = "Utilities", DisplayOrder = 4, SessionAmount = 527m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 1, ExpenseCodeId = 3, ExpenseCode = "Home Maintenance", DisplayText = "Home Maintenance", DisplayOrder = 4, SessionAmount = 50m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 2, ExpenseCodeId = 5, ExpenseCode = "Auto Payments", DisplayText = "Auto Payments", DisplayOrder = 1, SessionAmount = 155m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 2, ExpenseCodeId = 6, ExpenseCode = "Auto Insurance", DisplayText = "Auto Insurance", DisplayOrder = 2, SessionAmount = 55m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 2, ExpenseCodeId = 7, ExpenseCode = "Auto Maintenance", DisplayText = "Auto Maintenance", DisplayOrder = 3, SessionAmount = 85m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 3, ExpenseCodeId = 10, ExpenseCode = "Food In", DisplayText = "Food In", DisplayOrder = 2, SessionAmount = 100m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 3, ExpenseCodeId = 9, ExpenseCode = "Food Out", DisplayText = "Food Out", DisplayOrder = 1, SessionAmount = 86m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 19, ExpenseCode = "Charity", DisplayText = "Charity", DisplayOrder = 9, SessionAmount = 44m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 21, ExpenseCode = "Laundry", DisplayText = "Laundry", DisplayOrder = 11, SessionAmount = 25m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 20, ExpenseCode = "Clothing", DisplayText = "Clothing", DisplayOrder = 10, SessionAmount = 25m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 23, ExpenseCode = "Beauty", DisplayText = "Beauty", DisplayOrder = 13, SessionAmount = 25m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 22, ExpenseCode = "Personal Expenses", DisplayText = "Personal Expenses", DisplayOrder = 12, SessionAmount = 100m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 24, ExpenseCode = "Recreation", DisplayText = "Recreation", DisplayOrder = 14, SessionAmount = 50m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 26, ExpenseCode = "Gifts", DisplayText = "Gifts", DisplayOrder = 16, SessionAmount = 25m },
                    new PreFilingSnapshotExpenseCodeDto { ExpenseCategoryId = 4, ExpenseCodeId = 18, ExpenseCode = "Other Installment Loans", DisplayText = "Other Installment Loans", DisplayOrder = 8, SessionAmount = 400m },
                }
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<CounselingGenerator4>();
            var actionPlan = service.CreateActionPlan(primarySnapshotDto, null);
            File.WriteAllBytes(String.Format("{0}.pdf", Path.Combine("C:\\Temp", Guid.NewGuid().ToString())), actionPlan.ToArray());

            //var secureActionPlan = new MemoryStream();
            //var fileSecurity = new PdfFileSecurity(actionPlan, secureActionPlan);
            //var password = String.Format("{0}{1}", primarySnapshotDto.SsnLast4, primarySnapshotDto.PostalCode);
            //fileSecurity.EncryptFile(password, password, DocumentPrivilege.AllowAll, KeySize.x256, Algorithm.AES);
            //File.WriteAllBytes(String.Format("{0}.pdf", Path.Combine("C:\\Temp", Guid.NewGuid().ToString())), secureActionPlan.ToArray());
        }
    }
}
