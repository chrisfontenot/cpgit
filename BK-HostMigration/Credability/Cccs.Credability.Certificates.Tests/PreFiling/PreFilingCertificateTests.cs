﻿using System;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Identity;
using Cccs.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Castle.DynamicProxy;
//using Cccs.Host;
using System.Collections.Generic;
using Cccs.Credability.Certificates.Repositories;

namespace Cccs.Credability.Certificates.Tests.PreFiling
{
    [TestClass]
    public class PreFilingCertificateTests
    {
        #region HasCurrentSnapshot Tests

        [TestMethod]
        public void HasCurrentSnapshot_Should_Return_False_If_Record_Not_Found()
        {
            long expectedUserDetailId = 1;

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var repository = container.GetMock<ISnapshotRepository>();
            repository.Setup(r => r.GetMostRecent(expectedUserDetailId)).Returns(null as PreFilingSnapshotDto);

            var result = service.HasCurrentSnapshot(expectedUserDetailId);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasCurrentSnapshot_Should_Return_False_If_Record_Not_Within_Six_Months()
        {
            long expectedUserDetailId = 1;
            var dto = new PreFilingSnapshotDto
            {
                CourseCompleted = DateTime.Now.AddMonths(-7),
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var repository = container.GetMock<ISnapshotRepository>();
            repository.Setup(r => r.GetMostRecent(expectedUserDetailId)).Returns(dto);

            var result = service.HasCurrentSnapshot(expectedUserDetailId);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void HasCurrentSnapshot_Should_Return_True_If_Record_Is_Within_Six_Months()
        {
            long expectedUserDetailId = 1;
            var dto = new PreFilingSnapshotDto
            {
                CourseCompleted = DateTime.Now.AddMonths(-5),
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var repository = container.GetMock<ISnapshotRepository>();
            repository.Setup(r => r.GetMostRecent(expectedUserDetailId)).Returns(dto);

            var result = service.HasCurrentSnapshot(expectedUserDetailId);

            Assert.IsTrue(result);
        }

        #endregion

        #region SnapshotData Tests

        [TestMethod]
        [ExpectedException(typeof(UserDetailNotFoundException))]
        public void SnapshotData_Should_Error_If_UserDetail_Not_Found()
        {
            long expectedUserDetailId = 1;

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var repository = container.GetMock<IUserRepository>();
            repository.Setup(r => r.GetDetail(expectedUserDetailId)).Returns(null as PreFilingUserDetailDto);

            service.SnapshotData(expectedUserDetailId, String.Empty, String.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(PreFilingAccountNotFoundException))]
        public void SnapshotData_Should_Error_If_Account_Not_Found()
        {
            long expectedUserDetailId = 1;
            var expectedUserDetailDto = new PreFilingUserDetailDto
            {
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var userRepository = container.GetMock<IUserRepository>();
            userRepository.Setup(r => r.GetDetail(expectedUserDetailId)).Returns(expectedUserDetailDto);
            var snapshotRepository = container.GetMock<ISnapshotRepository>();
            snapshotRepository.Setup(r => r.GetMostRecentDate(expectedUserDetailId)).Returns(DateTime.Now.AddMonths(-7));
            var accountRepository = container.GetMock<IAccountRepository>();
            accountRepository.Setup(r => r.GetPreFilingAccountByUserDetail(expectedUserDetailId)).Returns(null as PreFilingAccountDto);

            service.SnapshotData(expectedUserDetailId, String.Empty, String.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ContactDetailNotFoundException))]
        public void SnapshotData_Should_Error_If_ContactDetail_Not_Found()
        {
            long expectedUserDetailId = 1;
            var expectedAccountDto = new PreFilingAccountDto
            {
                AccountId = 1,
                UserId = 1,
                AccountTypeCode = "BR.CAM",
                InternetId = 123456789,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedBy = "user1",
                IsActive = true,
            };
            var expectedUserDetailDto = new PreFilingUserDetailDto
            {
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var userRepository = container.GetMock<IUserRepository>();
            userRepository.Setup(r => r.GetDetail(expectedUserDetailId)).Returns(expectedUserDetailDto);
            var snapshotRepository = container.GetMock<ISnapshotRepository>();
            snapshotRepository.Setup(r => r.GetMostRecentDate(expectedUserDetailId)).Returns(DateTime.Now.AddMonths(-7));
            var accountRepository = container.GetMock<IAccountRepository>();
            accountRepository.Setup(r => r.GetPreFilingAccountByUserDetail(expectedUserDetailId)).Returns(expectedAccountDto);
            var contactDetailRepository = container.GetMock<IContactDetailRepository>();
            contactDetailRepository.Setup(r => r.Get(expectedAccountDto.InternetId)).Returns(null as PreFilingContactDetailDto);

            service.SnapshotData(expectedUserDetailId, String.Empty, String.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(PreFilingSnapshotException))]
        public void SnapshotData_Should_Error_If_Last_Snapshot_Was_Within_Six_Months()
        {
            long expectedUserDetailId = 1;
            var expectedUserDetailDto = new PreFilingUserDetailDto
            {
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var userRepository = container.GetMock<IUserRepository>();
            userRepository.Setup(r => r.GetDetail(expectedUserDetailId)).Returns(expectedUserDetailDto);
            var snapshotRepository = container.GetMock<ISnapshotRepository>();
            snapshotRepository.Setup(r => r.GetMostRecentDate(expectedUserDetailId)).Returns(DateTime.Now.AddMonths(-5));

            service.SnapshotData(expectedUserDetailId, String.Empty, String.Empty);
        }

        [TestMethod]
        public void SnapshotData_Should_Create_New_PreFilingSnapshot_Record()
        {
            long expectedUserDetailId = 47;

            var expectedUserDetailDto = new PreFilingUserDetailDto
            {
                PostalCode = "30303",
                LanguageCode = "EN",
            };
            var expectedAccountDto = new PreFilingAccountDto
            {
                AccountId = 1,
                UserId = 1,
                AccountTypeCode = "BR.CAM",
                InternetId = 123456789,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedBy = "user1",
                IsActive = true,
            };
            var expectedContactDetailDto = new PreFilingContactDetailDto
            {
                Budget = new List<PreFilingContactDetailBudgetDto>()
                {
                    new PreFilingContactDetailBudgetDto() { Amount = 12, LookupCode = "Charity" },
                },

                Creditors = new List<PreFilingContactDetailCreditorDto>()
              {
                new PreFilingContactDetailCreditorDto() 
                { Balance=100,
                    InterestRate=0.25m,                  
                    Name="XXX"                   
                    
                },
                   new PreFilingContactDetailCreditorDto() 
                { Balance=10000,
                    InterestRate=0.17m,
                 
                    Name="YYY"                  
                    
               }
              }
            };
            //var judicialDistrict = new JudicialDistrict()
            //{
            //    DistrictCode = "6",
            //    DistrictName = "Northern District of Georgia",
            //};
            var expenseCategories = new List<ExpenseCategoryDto>()
            {
                new ExpenseCategoryDto() 
                { 
                    ExpenseCategoryId = 1, 
                    LookupCode = "Charity", 
                    DisplayOrder = 1, 
                    ExpenseCodes = new List<ExpenseCodeDto>() 
                    { 
                        new ExpenseCodeDto { DisplayOrder = 1, ExpenseCategoryId = 1, ExpenseCodeId = 1, LookupCode = "Charity", Name = "TestCode" },
                    },
                },
            };



            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();

            var userRepository = container.GetMock<IUserRepository>();
            userRepository.Setup(r => r.GetDetail(expectedUserDetailId)).Returns(expectedUserDetailDto);
            var snapshotRepository = container.GetMock<ISnapshotRepository>();
            snapshotRepository.Setup(r => r.GetMostRecentDate(expectedUserDetailId)).Returns(DateTime.Now.AddMonths(-7));
            var accountRepository = container.GetMock<IAccountRepository>();
            accountRepository.Setup(r => r.GetPreFilingAccountByUserDetail(expectedUserDetailId)).Returns(expectedAccountDto);
            var contactDetailRepository = container.GetMock<IContactDetailRepository>();
            contactDetailRepository.Setup(r => r.Get(expectedAccountDto.InternetId)).Returns(expectedContactDetailDto);
            var expenseRepository = container.GetMock<IExpenseRepository>();
            expenseRepository.Setup(r => r.GetExpenseCategories()).Returns(expenseCategories);
            //var host = container.GetMock<IHost>();
            //host.Setup(r => r.GetJudicialDistrict(expectedUserDetailDto.PostalCode)).Returns(judicialDistrict);
            //var debtplus = container.GetMock<IDebtplus>();
            //debtplus.Setup(r => r.GetJudicialDistrict(expectedUserDetailDto.PostalCode)).Returns(judicialDistrict);


            service.SnapshotData(expectedUserDetailId, String.Empty, String.Empty);
        }


      
        #endregion
        [TestMethod]
        public void CheckDMP()
        {
            //long expectedUserDetailId = 47;

            //var expectedUserDetailDto = new PreFilingUserDetailDto
            //{
            //    PostalCode = "30303",
            //    LanguageCode = "EN",
            //};
            //var expectedAccountDto = new PreFilingAccountDto
            //{
            //    AccountId = 1,
            //    UserId = 1,
            //    AccountTypeCode = "BR.CAM",
            //    InternetId = 123456789,
            //    CreatedDate = DateTime.Now,
            //    ModifiedDate = DateTime.Now,
            //    ModifiedBy = "user1",
            //    IsActive = true,
            //};
            //var expectedContactDetailDto = new PreFilingContactDetailDto
            //{
            //    Budget = new List<PreFilingContactDetailBudgetDto>()
            //    {
            //        new PreFilingContactDetailBudgetDto() { Amount = 12, LookupCode = "Charity" },
            //    },

            //    Creditors = new List<PreFilingContactDetailCreditorDto>()
            //  {
            //    new PreFilingContactDetailCreditorDto() 
            //    { Balance=100,
            //        InterestRate=0.25m,                  
            //        Name="XXX"                   
                    
            //    },
            //       new PreFilingContactDetailCreditorDto() 
            //    { Balance=10000,
            //        InterestRate=0.17m,
                 
            //        Name="YYY"                  
                    
            //   }
            //  }
            //};
            //var judicialDistrict = new JudicialDistrict()
            //{
            //    DistrictCode = "6",
            //    DistrictName = "Northern District of Georgia",
            //};
            //var expenseCategories = new List<ExpenseCategoryDto>()
            //{
            //    new ExpenseCategoryDto() 
            //    { 
            //        ExpenseCategoryId = 1, 
            //        LookupCode = "Charity", 
            //        DisplayOrder = 1, 
            //        ExpenseCodes = new List<ExpenseCodeDto>() 
            //        { 
            //            new ExpenseCodeDto { DisplayOrder = 1, ExpenseCategoryId = 1, ExpenseCodeId = 1, LookupCode = "Charity", Name = "TestCode" },
            //        },
            //    },
            //};
        
            PreFilingSnapshotDto snapshot = new PreFilingSnapshotDto();
            snapshot.Creditors = new List<PreFilingSnapshotCreditorDto>()
            {
                // new PreFilingSnapshotCreditorDto()
                //{
                //    Balance=500,
                //    DisplayOrder=1,
                //    InterestRate=0,
                //    MonthlyPayment=200,
                //    MonthsToRepay=0, //2
                //    Name="AAA"                   
                   
                //},
                // new PreFilingSnapshotCreditorDto()
                //{
                //    Balance=100,
                //    DisplayOrder=1,
                //    InterestRate=0,
                //    MonthlyPayment=50, //2
                //    MonthsToRepay=0,
                //    Name="YYY"                   
                   
                //},
                //   new PreFilingSnapshotCreditorDto()
                //{
                //    Balance=200,
                //    DisplayOrder=1,
                //    InterestRate=0,
                //    MonthlyPayment=150, //2
                //    MonthsToRepay=0,
                //    Name="YYY"                   
                   
                //}
                new PreFilingSnapshotCreditorDto()
                {
                    Balance=1304,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=77,
                    MonthsToRepay=0,
                    Name="AAA"                   
                   
                },
                 new PreFilingSnapshotCreditorDto()
                {
                    Balance=587,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=29,
                    MonthsToRepay=0,
                    Name="YYY"                   
                   
                },
                 new PreFilingSnapshotCreditorDto()
                {
                    Balance=2796,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=139,
                    MonthsToRepay=0,
                    Name="YYY"                   
                   
                },
                 new PreFilingSnapshotCreditorDto()
                {
                    Balance=756,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=108,
                    MonthsToRepay=0,
                    Name="YYY"                   
                   
                },
                 new PreFilingSnapshotCreditorDto()
                {
                    Balance=255,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=0,
                    MonthsToRepay=0,
                    Name="YYY"                   
                   
                },
                  new PreFilingSnapshotCreditorDto()
                {
                    Balance=823,
                    DisplayOrder=1,
                    InterestRate=0,
                    MonthlyPayment=33,
                    MonthsToRepay=0,
                    Name="YYY"                   
                   
                }
                
                

            };
            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<PreFilingCertificateService>();
            service.SetDmpValues(snapshot);
        }


        [TestMethod]
        public void ArchiveCertificates()
        {
            var container = MockHelper.CreateAutoMockContainer();
            var snapshotRepository = container.Create<SnapshotRepository>();
            container.Register<ISnapshotRepository>(snapshotRepository);

            var service = container.Create<PreFilingCertificateService>();

            service.ArchiveCertificates();
        }

        [TestMethod]
        public void FillCertificates()
        {
            var container = MockHelper.CreateAutoMockContainer();
            var snapshotRepository = container.Create<SnapshotRepository>();
            container.Register<ISnapshotRepository>(snapshotRepository);

            var service = container.Create<PreFilingCertificateService>();

            service.FillCertificates();
        }
    }
}
