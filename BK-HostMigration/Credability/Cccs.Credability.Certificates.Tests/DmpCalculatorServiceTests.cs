﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cccs.Tests;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Certificates.Tests
{
    [TestClass]
    public class DmpCalculatorServiceTests
    {
        [TestMethod]
        public void Average_Method_Calculators_Correct_Average()
        {
            var configurationDto = new DmpConfigurationDto
            {
                AverageDmpFeePercent = 0.075m,
                AverageInterestRate = 0.09m,
                AverageMinimumPayment = 10m,
                AveragePaymentPercent = 0.02m,
            };
            var creditors = new List<DmpCreditorDto>
            {
                new DmpCreditorDto { Balance = 1000m, InterestRate = 0.21m, },
            };

            var container = MockHelper.CreateAutoMockContainer();
            var service = container.Create<DmpCalculatorService>();
            var dmpRepository = container.GetMock<IDmpRepository>();

            dmpRepository.Setup(r => r.GetConfiguration()).Returns(configurationDto);

            var averageDto = service.Average(creditors);

            Assert.AreEqual(27.5m, averageDto.DmpMonthlyPayment, "Monthly Payment is incorrect.");
            Assert.AreEqual(43m, averageDto.DmpMonthsToRepay, "Months to repay is incorrect.");
        }
    }
}
