﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Error
{
    public partial class CustomError : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblHeading.Text = Cccs.Credability.Website.App.Translate("CUSTOM ERROR PAGE");//"CUSTOM ERROR PAGE";
            Page.Title = Cccs.Credability.Website.App.Translate("CUSTOM ERROR PAGE"); //"CUSTOM ERROR PAGE ";
        }
    }
}
