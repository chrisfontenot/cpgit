﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Error
{
    public partial class PageNotFound : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblHeading.Text = Cccs.Credability.Website.App.Translate("404_Page_Not_Found.");
            Page.Title = Cccs.Credability.Website.App.Translate("404_Page_Not_Found.");
        }
    }
}
