﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomError.aspx.cs" Inherits="Cccs.Credability.Website.Error.CustomError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link type="text/css" rel="Stylesheet" href="../Content/error.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="wrapper">
        <div class="header">
            <div class="logo">
                <a id="HrefLogo" runat="server"></a>
            </div>
            <div class="DvLanguage">
                <%--  <uc1:languageselect id="LanguageSelect1" runat="server" />--%>
            </div>
        </div>
        <div class="DvtopNavControl">
            <%--<uc:fontcontrol runat="server" id="UcFontControl" />
            <uc:breadcrum id="UcBreadcrum" runat="server" visible="false"></uc:breadcrum>
            <uc:breadcrumbch id="UcBreadcrumBch" runat="server" visible="false"></uc:breadcrumbch>
            <uc:breadcrumbkc id="UcBreadcrumBKC" runat="server" visible="false" />--%>
        </div>
        <div class="headline">
            <asp:Label ID="lblHeading" runat="server" class="lblpagetitle"></asp:Label>
        </div>
        <div class="body">
            <div class="content-full">
                <div class="c-body c-large">
                    <div class="errortxt">
                        </div>
                    <div class="customerrorcont">
                        <div class="msg">
                            <%= Cccs.Credability.Website.App.Translate("CUSTOM_MESSAGE") %></div>
                    </div>
                </div>
            </div>
            <div class="clearboth">
            </div>
        </div>
        <div class="footer">
            <div class="footer-links">
            </div>
            <div class="copyright">
                <p>
                    &#169;Copyright 2010
                </p>
            </div>
            <div class="clearboth">
            </div>
            <div class="partners-logos">
                <ul>
                    <li>
                        <img src="/images/partners-logos.gif"></li>
                </ul>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
