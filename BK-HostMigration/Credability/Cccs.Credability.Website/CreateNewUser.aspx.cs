﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;

namespace Cccs.Credability.Website
{
	public partial class CreateNewUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Cccs.Credability.Dal.Properties.Settings.cccsDevConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("pAccountAddressFromWeb_s", conn);
            cmd.Parameters.AddWithValue("biUserId", 8890);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 120;

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                parameters = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
            }
        }

    }
}
