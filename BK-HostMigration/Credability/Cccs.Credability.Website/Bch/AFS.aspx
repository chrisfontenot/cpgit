﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AFS.aspx.cs" Inherits="Cccs.Credability.Website.Bch.AFS" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility AFS BCH"%>
<%@ Register Src="~/Controls/Shared/Pages/UcAfs.ascx" TagPrefix="Uc" TagName="UCAFS" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<%-- %><Uc:UCAFS ID="UcUCAFS" runat="server" RedirectOnContinue="UserProfile.aspx" RedirectOnNeedAuthorization="DescribeYourSituation.aspx"></Uc:UCAFS>--%>
   	<Uc:UCAFS ID="UCAFS1" runat="server" RedirectOnContinue="WhatYouNeed_BCH.aspx" RedirectOnNeedAuthorization="DescribeYourSituation.aspx"></Uc:UCAFS>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>