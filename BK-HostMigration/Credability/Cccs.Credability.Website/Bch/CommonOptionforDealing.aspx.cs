﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Bch
{
	public partial class CommonOptionforDealing : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				CommonFunction.UserProgressSave(PercentComplete.COMMON_OPTIONS_FOR_DEALING, SessionState.Username);
			}
		}
	}
}
