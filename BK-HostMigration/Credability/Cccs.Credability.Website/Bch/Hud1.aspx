﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Hud1.aspx.cs" Inherits="Cccs.Credability.Website.Bch.Hud1" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility HUD1 BCH" %>
<%@ Register Src="~/Controls/BchControls/UCHud1.ascx" TagPrefix="Uc" TagName="UCHud1" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCHud1 ID="UcUCAlwaysAvailableToHelp" runat="server"></Uc:UCHud1>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder"
	runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>