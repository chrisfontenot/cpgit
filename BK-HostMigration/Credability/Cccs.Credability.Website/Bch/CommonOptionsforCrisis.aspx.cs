﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.Bch
{
	public partial class CommonOptionsforCrisis : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUCommonOption.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
		    AuthCode.Visible = true;
            if (AuthCode != null)
            {
                AuthCode.ChatCode = ChatCodeOption.UnnumberedChat;
                AuthCode.CodeLabel = "Credability|CommonOptionsForCrisisBCH|ChatLabel";
            }

			if (!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Summary);

                CommonFunction.UserProgressSave(PercentComplete.COMMON_OPTIONS_FOR_CRISIS, SessionState.Username);
			}
		}
	}
}
