﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionforDealing.aspx.cs" Inherits="Cccs.Credability.Website.Bch.CommonOptionforDealing"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Common Option for Dealing BCH"%>
<%@ Register Src="~/Controls/BchControls/UCCommonOptionforDealing.ascx" TagPrefix="Uc"  TagName="UCCommonOptionforDealing"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCCommonOptionforDealing id="UcUCCommonOptionforDealing" runat="server"></Uc:UCCommonOptionforDealing>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>