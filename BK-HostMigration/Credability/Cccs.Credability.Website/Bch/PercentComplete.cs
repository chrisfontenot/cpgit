﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website.Bch
{
	public static class PercentComplete
	{
		public const byte AFS = 5;
		public const byte USER_PROFILE = 10;
		public const byte DESCRIBE_YOUR_SITUATION = 30;
		public const byte POST_PURCHASE = 35;
		public const byte HUD1 = 40;
		public const byte USER_DEBT_LISTING = 50;
		public const byte USER_INCOME_DOCUMENTATION = 65;
		public const byte MONTHLY_EXPENSES = 70;
        public const byte UNDERSTANDING_YOUR_NET_WORTH = 75;
		public const byte USER_FINANCIAL_SITUATION_RECAP = 80;
		public const byte COMMON_OPTIONS_FOR_CRISIS = 85;
		public const byte HUD2 = 90;
		public const byte COMMON_OPTIONS_FOR_DEALING_WITH_A_FINANCAL_CRISIS = 95;
		public const byte COMMON_OPTIONS_FOR_DEALING = 95;
		public const byte COMPLETE = 100;
	}
}
