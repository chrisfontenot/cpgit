﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.Bch
{
	public partial class Hud1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUCAlwaysAvailableToHelp.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
            if (AuthCode != null)
            {
                AuthCode.ChatCode=ChatCodeOption.Session1;
            }

			if (!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.TellUs);

                CommonFunction.UserProgressSave(PercentComplete.HUD1, SessionState.Username);
			}
		}
	}
}
