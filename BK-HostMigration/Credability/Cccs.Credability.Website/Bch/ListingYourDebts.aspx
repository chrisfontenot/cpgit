﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListingYourDebts.aspx.cs" Inherits="Cccs.Credability.Website.Bch.UserDebtListing" MasterPageFile="~/MasterPages/Master.Master"  Title="CredAbility Listing Your Debts BCH"%>
<%@ Register Src="~/Controls/Shared/Pages/UcDebtListing.ascx" TagPrefix="Uc" TagName="DebtList" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:DebtList ID="UcDebtList" runat="server" RedirectOnContinue="UserIncomeDocumentation.aspx" RedirectOnPrevious="DescribeYourSituation.aspx"></Uc:DebtList>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>