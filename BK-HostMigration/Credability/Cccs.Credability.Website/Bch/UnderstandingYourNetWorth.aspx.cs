﻿using System;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.Bch
{
	public partial class UnderstandingYourNetWorth : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (SessionState.ClientType == "BC" || SessionState.ClientType == "REC")
				UcUnderstandingYourNetWorth.PageContinue = "CommonOptionsforCrisis.aspx";

			if(!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Summary);
				CommonFunction.UserProgressSave(PercentComplete.UNDERSTANDING_YOUR_NET_WORTH, SessionState.Username);
			}
		}
	}
}