﻿using System;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.Bch
{
	public partial class UserFinancialSituationRecap : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcFinancialSituationRecap.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode = ChatCodeOption.NoChat;
			}
			if(!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Summary);
				CommonFunction.UserProgressSave(PercentComplete.USER_FINANCIAL_SITUATION_RECAP, SessionState.Username);
			}
		}
	}
}