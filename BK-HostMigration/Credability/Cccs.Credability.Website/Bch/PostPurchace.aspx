﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostPurchace.aspx.cs" Inherits="Cccs.Credability.Website.Bch.PostPurchace" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility PostPurchace BCH" %>
<%@ Register Src="~/Controls/Shared/Pages/UcPostPurchace.ascx" TagPrefix="Uc" TagName="UCPostPurchace" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCPostPurchace ID="UcUCPostPurchace" runat="server" RedirectOnContinue="ListingYourDebts.aspx" RedirectOnPrevious="DescribeYourSituation.aspx"></Uc:UCPostPurchace>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder"
	runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>