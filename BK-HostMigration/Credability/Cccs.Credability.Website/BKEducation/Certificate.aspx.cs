﻿using System;
using Cccs.Credability.Certificates.PreFiling;
using StructureMap;
using Cccs.Identity.Dal;
using Cccs.Credability.Certificates.Education;
using Cccs.Credability.Certificates;
using Cccs.Debtplus.Data;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class Certificate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var isPrimary = Convert.ToBoolean(Request.QueryString["IsPrimary"]);
            var isActionPlan = Convert.ToBoolean(Request.QueryString["IsActionPlan"]);

            if (isActionPlan)
            {
                var primaryDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, true);
                var secondaryDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, false);
                GenerateActionPlanPdf(primaryDetail, secondaryDetail);
            }
            else
            {
                var userDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, isPrimary);
                GenerateCertificatePdf(userDetail);
            }
        }
        
        private void GenerateActionPlanPdf(Identity.UserDetail primaryUserDetail, Identity.UserDetail secondaryUserDetail)
        {
            //var counselingCertificate = ObjectFactory.GetInstance<IPreFilingCertificateService>();
            //var primaryData = counselingCertificate.GetCurrentSnapshotForUserDetail(primaryUserDetail.UserDetailId);
            //var secondaryData = secondaryUserDetail != null ? counselingCertificate.GetCurrentSnapshotForUserDetail(secondaryUserDetail.UserDetailId) : null;

            //var assembly = typeof(IPreFilingCertificateService).Assembly;
            //var generatorType = assembly.GetType(primaryData.CertificateVersionType);

            //var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
            //var stream = generator.CreateActionPlan(primaryData, secondaryData);

            //Response.Expires = 0;
            //Response.Buffer = true;
            //Response.ClearHeaders();
            //Response.Clear();
            //Response.AddHeader("content-disposition", String.Format("inline; filename={0}-ActionPlan.pdf", primaryData.CertificateNumber));
            //Response.ContentType = "application/pdf";

            //var buffer = stream.GetBuffer();
            //Response.BinaryWrite(buffer);
            //stream.Close();
            //Response.Flush();
            //Response.End();
        }

        private void GenerateCertificatePdf(Identity.UserDetail userDetail)
        {
            //var data = educationCertificate.GetCurrentSnapshotForUserDetail(userDetail.UserDetailId);

            if (SessionState.AccountId.HasValue)
            {
                // Bankruptcy Conversion
                //Cccs.Host.MimForm7LoadDataResult resultLoadData = App.Host.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
                MimForm7LoadData resultLoadData = App.Debtplus.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);

                if (resultLoadData != null)
                {
                    var bankruptcyNumber = resultLoadData.casenumber;// SessionState.CaseNumber;

                    var educationCertificate = ObjectFactory.GetInstance<IEducationCertificateService>();
                    var data = educationCertificate.GetUserDetail(userDetail, (long)SessionState.AccountId, bankruptcyNumber);

                    var assembly = typeof(Certificates.Education.ICertificateGenerator).Assembly;
                    var generatorType = assembly.GetType("Cccs.Credability.Certificates.Education.Generators.CertificateGenerator1");// data.CertificateVersionType);

                    var generator = Activator.CreateInstance(generatorType) as Certificates.Education.ICertificateGenerator;

                    var stream = generator.CreateCertificate(data);

                    Response.Expires = 0;
                    Response.Buffer = true;
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.AddHeader("content-disposition", String.Format("inline; filename={0}.pdf", data.CertificateNumber));
                    Response.ContentType = "application/pdf";

                    var buffer = stream.GetBuffer();
                    Response.BinaryWrite(buffer);
                    stream.Close();
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }
}