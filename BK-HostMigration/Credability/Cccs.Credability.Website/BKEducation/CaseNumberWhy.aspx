﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseNumberWhy.aspx.cs" Inherits="Cccs.Credability.Website.BKEducation.CaseNumberWhy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WH")%></title>
</head>
<body>
<table cellpadding="0" cellspacing="0" width="420" align="center">
	<tr style="background-color:Blue;">
		<td align="center" style="color:#ffffff;font-weight:bold;">
			<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WDW")%>
		</td>
	</tr>
	<tr>
		<td height="15"></td>
	</tr>
	<tr>
		<td class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TUS")%>
		</td>
	</tr>
</table>
</body>
</html>

