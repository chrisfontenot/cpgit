﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvideYourPersonalInformation.aspx.cs" Inherits="Cccs.Credability.Website.BKEducation.ProvideYourPersonalInformation" MasterPageFile="~/MasterPages/Master.Master" Title="CCCS Provide Your Personal Information" %>

<%@ Register src="../Controls/BKEducationControls/UCProvideYourPersonalInfo.ascx" tagname="UCProvideYourPersonalInfo" tagprefix="uc1" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <script language="javascript" src="../Content/FormChek.js"></script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <uc1:UCProvideYourPersonalInfo ID="UCProvideYourPersonalInfo1" runat="server" />
</asp:Content>
