﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Translation;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class MimRevCnt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionState.Username == null)
            {
                Response.Redirect(CommonFunction.GetHomePageUrl());
            }

            App.Credability.MySqlRevCountUpdate(SessionState.Username);

            var redirectUrl = String.Format("{0}/MimReview/chapters/chapter1.php?uid={1}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), SessionState.Username);
            if (SessionState.LanguageCode == Language.ES)
            {
                redirectUrl = String.Format("{0}/MimReview/spanish/chapters/chapter1.php?uid={1}", CommonFunction.PHPMIMSiteUrl().Replace("spanish", String.Empty).TrimEnd('/'), SessionState.Username);
            }

            Response.Redirect(redirectUrl);
        }
    }
}
