﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class Disclosure : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UcUCDisclosure.RedirectOnContinue = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;

            System.Web.UI.HtmlControls.HtmlGenericControl spnBKCTitle = UcUCDisclosure.FindControl("spnBKCTitle") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnBKCTitle != null)
            {
                spnBKCTitle.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnBKETitle = UcUCDisclosure.FindControl("spnBKETitle") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnBKETitle != null)
            {
                spnBKETitle.Visible = true;
            }

        }
    }
}
