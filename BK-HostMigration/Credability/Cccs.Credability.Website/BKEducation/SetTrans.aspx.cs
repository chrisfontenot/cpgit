﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Cccs.Host;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class SetTrans : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(SessionState.Clid) && !string.IsNullOrEmpty(SessionState.RegNum))
            {
                UpdatePayementInfoForescrowAttorney();
            }
        }
        private void UpdatePayementInfoForescrowAttorney()
        {
            string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
            if(SessionState.Escrow6 == "Y")
            {
                if(SessionState.SecondaryFlag != "S")
                {
                    // Bankruptcy COnversion
                    //MimSetTransSaveWriteResult Result = null;
                    //Result = App.Host.Mim_SetTrans_SaveWriteOff(SessionState.Clid, SessionState.RegNum, SessionState.ChargeAmount);
                    var Result = App.Debtplus.Mim_SetTrans_SaveWriteOff(SessionState.Clid, SessionState.RegNum, SessionState.ChargeAmount);
                }
                Response.Redirect(Url);
            }
            else if(SessionState.Escrow == "Y")
            {
                Response.Redirect(Url);
            }
            else
            {
                Response.Redirect("AprWait.aspx");
            }
        }
    }
}
