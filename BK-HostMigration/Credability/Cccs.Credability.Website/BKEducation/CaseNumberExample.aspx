﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseNumberExample.aspx.cs" Inherits="Cccs.Credability.Website.BKEducation.CaseNumberExample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|E")%></title>
</head>
<body>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0">
	<tr style="background-color:Blue;">
		<td width="150" align="center" style="color:#ffffff;font-weight:bold;">
			<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WA")%>
		</td>
		<td width="80" align="center"  style="color:#ffffff;font-weight:bold; vertical-align:top;">
			<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|EN")%>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="MedTxt">
			<strong><span class="MedHead"><%=DateTime.Now.Year.ToString().Substring(2, 2)%></span>-REL-<span class="MedHead">98765</span></strong>
		</td>
		<td align="center" class="MedTxt">
			<strong><%=DateTime.Now.Year.ToString().Substring(2, 2)%>-98765</strong>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<tr>
		<td align="center" class="MedTxt">
			<strong><span class="MedHead"><%=DateTime.Now.Year.ToString().Substring(2, 2)%></span>-<span class="MedHead">13536</span>-BFL</strong>
		</td>
		<td align="center" class="MedTxt">
			<strong><%=DateTime.Now.Year.ToString().Substring(2, 2)%>-13536</strong>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<hr>
		</td>
	</tr>
	<tr>
		<td align="center" class="MedTxt">
			<strong>3:<span class="MedHead"><%=DateTime.Now.Year.ToString().Substring(2, 2)%></span>-6K-<span class="MedHead">31909</span></strong>
		</td>
		<td align="center" class="MedTxt">
			<strong><%=DateTime.Now.Year.ToString().Substring(2, 2)%>-31909</strong>
		</td>
	</tr>
</table>
    </form>
</body>
</html>
