﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class introNewPage : DefaultPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowLanguageCodes();
        }
    }
}
