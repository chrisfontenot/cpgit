﻿using System;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			SessionState.Username = null;
			SessionState.Clid = null;
			SessionState.JointSsn = null;
			SessionState.RegNum = null;
			SessionState.ContactSsn = null;
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
        }
    }
}
