﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaverApp.aspx.cs" Inherits="Cccs.Credability.Website.BKEducation.WaverApp" MasterPageFile="~/MasterPages/Master.Master" Title="CCCS WaverApp BKEducation"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script type="text/javascript">

    function ReSetPhone(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatUSPhone(Val);
        }
    }
    function ReSetSSN(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatSSN(Val);
        }
    }

    var SendSubmit = true;

    function ShowMenu() {
        var menu, PayOpt, i, j, Opt = "";
        PayOpt = document.getElementsByName("payment_type");
        for (i = 0; i < PayOpt.length; i++)//>
        {
            if (PayOpt[i].checked) {
                Opt = PayOpt[i].value;
                i = PayOpt.length;
            }
        }
        for (j = 0; j <= 3; j++)//>
        {
            menu = "pay";
            menu = menu + j;
            document.getElementById(menu).style.display = 'none';
        }
        document.getElementById("Check").style.fontWeight = 'normal';
        document.getElementById("Debt").style.fontWeight = 'normal';
        document.getElementById("West").style.fontWeight = 'normal';
        switch (Opt) {
            case "C":
                document.getElementById('pay1').style.display = 'block';
                document.getElementById("Check").style.fontWeight = 'bold';
                break;
            case "W":
                document.getElementById('pay2').style.display = 'block';
                document.getElementById("West").style.fontWeight = 'bold';
                break;
            case "D":
                document.getElementById('pay3').style.display = 'block';
                document.getElementById("Debt").style.fontWeight = 'bold';
                break;
            default:
                document.getElementById('pay0').style.display = 'block';
        }
    }

    function ShowMenu() {
        var menu, PayOpt, i, j, Opt = "";
        PayOpt = document.getElementsByName("payment_type");
        for (i = 0; i < PayOpt.length; i++)//>
        {
            if (PayOpt[i].checked) {
                Opt = PayOpt[i].value;
                i = PayOpt.length;
            }
        }
        for (j = 0; j <= 2; j++)//>
        {
            menu = "pay";
            menu = menu + j;
            document.getElementById(menu).style.display = 'none';
        }
        document.getElementById("Check").style.fontWeight = 'normal';
        document.getElementById("West").style.fontWeight = 'normal';
        switch (Opt) {
            case "C":
                document.getElementById('pay1').style.display = 'block';
                document.getElementById("Check").style.fontWeight = 'bold';
                break;
            case "W":
                document.getElementById('pay2').style.display = 'block';
                document.getElementById("West").style.fontWeight = 'bold';
                break;
            default:
                document.getElementById('pay0').style.display = 'block';
        }
    }

    function validateForm(form) {
        Send = SendSubmit;
        SendSubmit = false;
        setTimeout("SendSubmit = true", 5000);
        return Send;
    }
</script>

<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
</div>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YET")%>
</p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="user" />
<div class="dvform2col">
    <div class="colformlft">
        <div class="dvform">
            <div class="dvrow">
                <p class="col_title">
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PrimaryP")%>
                </p>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FirstN")%></label>
                <asp:TextBox ID="txtcontactfirstname" runat="server" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcontactfirstname"
                    Display="Dynamic" Text="!" ErrorMessage=" First Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|LastN")%></label>
                <asp:TextBox ID="txtcontactlastname" runat="server" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtcontactlastname"
                    Display="Dynamic" Text="!" ErrorMessage=" Last Name Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SADD")%></label>
                <asp:TextBox ID="txtcontactaddress" runat="server" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtcontactaddress"
                    Display="Dynamic" Text="!" ErrorMessage=" Address Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AD2")%></label>
                <asp:TextBox ID="txtcontactaddress2" runat="server" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ST")%></label>
                <asp:DropDownList ID="ddlState" runat="server">
                    <asp:ListItem Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="AA">AA</asp:ListItem>
                    <asp:ListItem Value="AE">AE</asp:ListItem>
                    <asp:ListItem Value="AK">AK</asp:ListItem>
                    <asp:ListItem Value="AL">AL</asp:ListItem>
                    <asp:ListItem Value="AP">AP</asp:ListItem>
                    <asp:ListItem Value="AR">AR</asp:ListItem>
                    <asp:ListItem Value="AS">AS</asp:ListItem>
                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
                    <asp:ListItem Value="CA">CA</asp:ListItem>
                    <asp:ListItem Value="CO">CO</asp:ListItem>
                    <asp:ListItem Value="CT">CT</asp:ListItem>
                    <asp:ListItem Value="DC">DC</asp:ListItem>
                    <asp:ListItem Value="DE">DE</asp:ListItem>
                    <asp:ListItem Value="FL">FL</asp:ListItem>
                    <asp:ListItem Value="FM">FM</asp:ListItem>
                    <asp:ListItem Value="GA">GA</asp:ListItem>
                    <asp:ListItem Value="GU">GU</asp:ListItem>
                    <asp:ListItem Value="HI">HI</asp:ListItem>
                    <asp:ListItem Value="IA">IA</asp:ListItem>
                    <asp:ListItem Value="ID">ID</asp:ListItem>
                    <asp:ListItem Value="IL">IL</asp:ListItem>
                    <asp:ListItem Value="IN">IN</asp:ListItem>
                    <asp:ListItem Value="KS">KS</asp:ListItem>
                    <asp:ListItem Value="KY">KY</asp:ListItem>
                    <asp:ListItem Value="LA">LA</asp:ListItem>
                    <asp:ListItem Value="MA">MA</asp:ListItem>
                    <asp:ListItem Value="MD">MD</asp:ListItem>
                    <asp:ListItem Value="ME">ME</asp:ListItem>
                    <asp:ListItem Value="MH">MH</asp:ListItem>
                    <asp:ListItem Value="MI">MI</asp:ListItem>
                    <asp:ListItem Value="MN">MN</asp:ListItem>
                    <asp:ListItem Value="MO">MO</asp:ListItem>
                    <asp:ListItem Value="MP">MP</asp:ListItem>
                    <asp:ListItem Value="MS">MS</asp:ListItem>
                    <asp:ListItem Value="MT">MT</asp:ListItem>
                    <asp:ListItem Value="NC">NC</asp:ListItem>
                    <asp:ListItem Value="ND">ND</asp:ListItem>
                    <asp:ListItem Value="NE">NE</asp:ListItem>
                    <asp:ListItem Value="NH">NH</asp:ListItem>
                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
                    <asp:ListItem Value="NM">NM</asp:ListItem>
                    <asp:ListItem Value="NV">NV</asp:ListItem>
                    <asp:ListItem Value="NY">NY</asp:ListItem>
                    <asp:ListItem Value="OH">OH</asp:ListItem>
                    <asp:ListItem Value="OK">OK</asp:ListItem>
                    <asp:ListItem Value="OR">OR</asp:ListItem>
                    <asp:ListItem Value="PA">PA</asp:ListItem>
                    <asp:ListItem Value="PR">PR</asp:ListItem>
                    <asp:ListItem Value="PW">PW</asp:ListItem>
                    <asp:ListItem Value="RI">RI</asp:ListItem>
                    <asp:ListItem Value="SC">SC</asp:ListItem>
                    <asp:ListItem Value="SD">SD</asp:ListItem>
                    <asp:ListItem Value="TN">TN</asp:ListItem>
                    <asp:ListItem Value="TX">TX</asp:ListItem>
                    <asp:ListItem Value="UT">UT</asp:ListItem>
                    <asp:ListItem Value="VA">VA</asp:ListItem>
                    <asp:ListItem Value="VI">VI</asp:ListItem>
                    <asp:ListItem Value="VT">VT</asp:ListItem>
                    <asp:ListItem Value="WA">WA</asp:ListItem>
                    <asp:ListItem Value="WI">WI</asp:ListItem>
                    <asp:ListItem Value="WV">WV</asp:ListItem>
                    <asp:ListItem Value="WY">WY</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="!" ErrorMessage=" Select State" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|C")%></label>
                <asp:TextBox ID="txtcontactcity" runat="server" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontactcity"
                    Display="Dynamic" Text="!" ErrorMessage=" City Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ZipC")%></label>
                <asp:TextBox ID="txtcontactzip" runat="server" MaxLength="5" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtcontactzip"
                    Display="Dynamic" Text="!" ErrorMessage=" Zip Code Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="rfvtxtZip" SetFocusOnError="true"
                    ControlToValidate="txtcontactzip" ValidationExpression="^[0-9]{5}$" Text="!" ErrorMessage="Enter  valid Zip Code must be 5 numeric digits"
                    Display="Dynamic" ValidationGroup="user" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PP#")%>
                </label>
                <asp:TextBox ID="txtcontacttelephone" runat="server"  CssClass="txtBox"></asp:TextBox>
               
               
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|EADD")%></label>
                <asp:TextBox ID="txtrealemail" runat="server" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtrealemail"
                    Display="Dynamic" Text="!" ErrorMessage=" Email Address Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtrealemail"
                    Display="Dynamic" Text="!" ErrorMessage=" Invalid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ValidationGroup="user"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <div class="colformrht">
        <div class="dvform">
            <div class="dvrow">
                <p class="col_title">
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SPerson")%>
                </p>
            </div>
            <div class="dvrow">
                <label>
                </label>
                <asp:TextBox ID="txtcosignfirstname" runat="server" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                </label>
                <asp:TextBox ID="txtcosignlastname" runat="server" CssClass="txtBox"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="clearboth">
    </div>
</div>

<table class="rdolstsnew">
  <tr>
    <td>
      <asp:radiobutton id="rboApplyYes" runat="server" groupname="Waver" />
      <label for="ApplyYes" class="padT3">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IIn")%>
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <table class="mgrL20" width="100%">
        <tr>
          <td>
            <asp:checkbox id="chkUnderstand" runat="server" />
            <label for="Understand" class="padT3">
              <%= Cccs.Credability.Website.App.Translate("Credability|BKE|IUThat")%>
            </label>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <asp:radiobutton id="rboApplyNo" runat="server" groupname="Waver" />
      <label for="ApplyNo" class="padT3">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IWish")%>
      </label>
    </td>
  </tr>
</table>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturnToPrevious" runat="server" CssClass="previous" OnClick="btnReturnToPrevious_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="Continue_Click" ValidationGroup="user" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
</asp:Content>


