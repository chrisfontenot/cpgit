﻿using System;
using System.Web;
using System.Text;
using Cccs.Credability.Certificates.Education.Generators;
using Cccs.Credability.Certificates;
using StructureMap;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class loginVal : System.Web.UI.Page
    {
        private readonly IEducationCertificateService educationCertificateService;
        private int postTestScore = 0;
        private int percentageComplete = 0;

        public loginVal()
        {
            educationCertificateService = ObjectFactory.GetInstance<IEducationCertificateService>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (SessionState.Username != null)
                {
                    int LastPage = Dologin();
                    string RedirectUrl;
                    if (Request.QueryString["redirectURL"].IsNullOrWhiteSpace())
                    {
                        RedirectUrl = String.Format("{0}/students/chapters/chapter{1}.php?uid={2}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), LastPage.ToString(), SessionState.Username.ToString());
                    }
                    else
                    {
                        string HoldUrl = Request.QueryString["redirectURL"].TrimStart('/');

                        if (HoldUrl.Contains("evaluation.php") && percentageComplete == 100 && postTestScore > 0)
                        {
                            GenerateClientCertificateNumber();
                            InsertDebtplusClient();
                        }
                        if (!HoldUrl.Contains("congrats.php") || HoldUrl.Contains("evaluation.php"))
                        {
                            RedirectUrl = String.Format("{0}/{1}?uid={2}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), HoldUrl, SessionState.Username.ToString());
                        }
                        else
                        {
                            if (LastPage < 9)
                                RedirectUrl = String.Format("{0}/students/chapters/chapter{1}.php?uid={2}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), LastPage.ToString(), SessionState.Username.ToString());
                            else
                                RedirectUrl = String.Format("{0}/{1}?uid={2}&unum={3}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), HoldUrl, SessionState.Username.ToString(), SessionState.Clid.ToString());
                        }
                    }
                    Response.Redirect(RedirectUrl);
                }
            }
            catch
            {
            }
            Response.Redirect("LogOut.aspx");
        }

        private void InsertDebtplusClient()
        {
            string waiverType = string.Empty;
            var fm = new FeeWaiverManager();

            var feeWaiver = fm.GetByClientNumber((int)SessionState.ClientNumber);

            if (feeWaiver != null)
            {
                waiverType = feeWaiver.WaiverType;
            }

            var certificateNumber = App.Identity.GetEducationCertificateNumber((long)SessionState.AccountId);
            
            App.Debtplus.SendDataToDebtplus((int)SessionState.ClientNumber, (long)SessionState.AccountId, SessionState.FirmID, SessionState.PrimaryZipCode, certificateNumber, waiverType, SessionState.Escrow, App.WebsiteCode, SessionState.EscrowProbonoValue, SessionState.SecondaryFlag);
        }

        private int Dologin()
        {
            // Find the user.
            var dsMySqlUser = App.Credability.MySQLUserRecordGet(SessionState.Username);
            if (dsMySqlUser.Tables.Count == 0 || dsMySqlUser.Tables[0].Rows.Count == 0)
            {
                throw new ApplicationException(String.Format("User '{0}' not found in MySql database.", SessionState.Username));
            }
            var drUser = dsMySqlUser.Tables[0].Rows[0];

            // Determine the last page based
            var queryStringLastPage = 0;
            Int32.TryParse(Request.QueryString["savesession"], out queryStringLastPage);
            var databaseLastPage = 0;
            Int32.TryParse(drUser["lastpage"].ToString(), out databaseLastPage);
            string TestDTS = drUser["TestDTS"].ToString();

            var newLastPage = databaseLastPage < queryStringLastPage ? queryStringLastPage : databaseLastPage;
            if (databaseLastPage == 0)
            {
                newLastPage = 1;
            }

            // Set the preTest value.
            var PreTest = 99;
            Int32.TryParse(drUser["pretest"].ToString(), out PreTest);
            if (PreTest < 0)
            {
                PreTest = 99;
            }

            // Calculate percent complete.
            var totalMinutes = Convert.ToInt32(drUser["totalhrs"].ToString());
            var percentAverageComplete = CalculatePercentComplete(newLastPage, totalMinutes);

            percentageComplete = percentAverageComplete;
            // if compleate update stuff
            if (percentAverageComplete >= 100)
            {
                string PostTest = drUser["posttest"].ToString();

                int.TryParse(PostTest, out postTestScore);

                if (TestDTS.IsNullOrWhiteSpace())
                {
                    TestDTS = DateTime.Now.ToString();
                    PostTest = "99";
                    App.Credability.UserScoreUpdate(SessionState.Username.ToString(), TestDTS, PostTest);
                }

                // Bankruptcy Conversion
                //App.Host.MimCompleteCourse(SessionState.Clid, PreTest.ToString(), PostTest, TestDTS);
                App.Debtplus.MimCompleteCourse(SessionState.Clid, PreTest.ToString(), PostTest, TestDTS);
            }
            // Update the database.
            var result = App.Credability.LastPagePhpPageInfoUpdate(SessionState.Username, newLastPage, PreTest);

            //Bankruptcy conversion-Seethal
            //var UvResult = App.Host.MimSavePreTest(SessionState.Clid, PreTest);
            var UvResult = App.Debtplus.MimSavePreTest(SessionState.Clid, PreTest);

            var defaultUrl = GetDefaultUrl(HttpContext.Current.Request.Url);
            CommonFunction.UserProgressSave((byte)percentAverageComplete, SessionState.Username, defaultUrl);
            if (newLastPage > 10)
            {
                if (percentAverageComplete >= 100)
                    return 9;//finished
                else
                    return newLastPage - 10;//last chapter
            }
            else
                return 1;//starting
        }

        private string GetDefaultUrl(Uri uri)
        {
            // Remove the query information.
            var defaultUrl = uri.OriginalString.Replace(uri.Query, String.Empty);

            // Substitute the page for default.aspx.
            var lastIndex = defaultUrl.LastIndexOf("/");
            var currentPage = defaultUrl.Substring(lastIndex, defaultUrl.Length - lastIndex);

            defaultUrl = defaultUrl.Replace(currentPage, "/default.aspx");

            return defaultUrl;
        }

        private int CalculatePercentComplete(int lastPage, int totalMinutes)
        {
            var percentLastPage = (double)lastPage;
            var percentTotalMinutes = (double)totalMinutes;

            var percentPageComplete = percentLastPage > 10d ? ((percentLastPage - 10d) / 8d) * 100d : 0d;
            var percentTimeComplete = percentTotalMinutes >= 115d ? 100d : percentTotalMinutes / 115d * 100d;
            var percentAverageComplete = (percentPageComplete + percentTimeComplete) / 2d;

            return (int)percentAverageComplete;
        }

        private void GenerateClientCertificateNumber()
        {
            if (SessionState.UserId.HasValue)
            {
                long userId = (long)SessionState.UserId;

                var generator = new CertificateGenerator1();

                var primaryUser = App.Identity.UserDetailGet(userId, true);

                var certificateVersionType = generator.GetType().ToString();

                // Bankruptcy Conversion
                //var creditScore = App.Host.GetCredScore(SessionState.ClientNumber.Value);
                var creditScore = App.Debtplus.GetCredScore(SessionState.ClientNumber.Value);

                var primaryCertificateGenerated = false;
                //if (!educationCertificateService.HasCurrentSnapshot(primaryUser.UserDetailId))
                //{
                //    //educationCertificateService.SnapshotData(primaryUser.UserDetailId, creditScore.Value.PrimaryScore, certificateVersionType);
                //}
                if (!educationCertificateService.HasDownloadedCertificate((long)SessionState.AccountId))
                {
                    if (SessionState.AccountId.HasValue)
                    {
                        var userData = App.Identity.UserWebsiteGet(userId, "BKDE");

                        if (userData != null && userData.CompletedDate.HasValue)
                        {
                            //var snapshot = educationCertificateService.GetCurrentSnapshotForUserDetail(primaryUser.UserDetailId);
                            educationCertificateService.AssignCertificate(primaryUser, (long)SessionState.AccountId, (DateTime)userData.CompletedDate);
                        }
                    }
                }

                primaryCertificateGenerated = true;
            }

            //var secondaryCertificateGenerated = false;
            //var secondaryUser = App.Identity.UserDetailGet(userId, false);
            //if (secondaryUser != null && !preFilingCertificateService.HasCurrentSnapshot(secondaryUser.UserDetailId))
            //{
            //    preFilingCertificateService.SnapshotData(secondaryUser.UserDetailId, creditScore.Value.SecondaryScore, certificateVersionType);
            //}
            //if (secondaryUser != null && !preFilingCertificateService.HasDownloadedCertificate(secondaryUser.UserDetailId))
            //{
            //    var snapshot = preFilingCertificateService.GetCurrentSnapshotForUserDetail(secondaryUser.UserDetailId);
            //    preFilingCertificateService.AssignCertificateToSnapshot(snapshot.PreFilingSnapshotId);
            //    secondaryCertificateGenerated = true;
            //}

            //if (CertificatesGenerated(primaryCertificateGenerated, secondaryCertificateGenerated))
            //{
            //    long preFilingActionPlanId = GenerateActionPlan(primaryUser, secondaryUser);
            //    //EmailCertificates(preFilingActionPlanId, primaryUser, judicialDistrict, chatCounselorName, confirmDateTime);
            //}
        }

        private bool CertificatesGenerated(bool primaryCertificateGenerated, bool secondaryCertificateGenerated)
        {
            return primaryCertificateGenerated || secondaryCertificateGenerated;
        }
    }
}