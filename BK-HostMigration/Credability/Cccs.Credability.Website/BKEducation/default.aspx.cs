﻿using System;
using Cccs.Identity;
using System.Data;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Account WsAcct = App.Identity.AccountGet(SessionState.UserId.Value, App.AccountTypeCode);
            if(String.IsNullOrEmpty(WsAcct.HostId))
            {
                Response.Redirect("introNewPage.aspx");
            }

            DataSet dsMySqlUser = new DataSet();
            var dsUser = App.Credability.RegNumGet(SessionState.Username.ToString());

            if (dsUser != null && dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
            {
                var regNum = dsUser.Tables[0].Rows[0]["regnum"].ToString();
                SessionState.RegNum = regNum;
                
                dsMySqlUser = App.Credability.MySQLUserRecordGet(Convert.ToInt64(regNum));
                if (dsMySqlUser.Tables.Count != 0 && dsMySqlUser.Tables[0].Rows.Count == 0)
                {
                    Response.Redirect("introNewPage.aspx");
                }
            }
            else
            {
                Response.Redirect("introNewPage.aspx");
            }

            //SessionState.RegNum = WsAcct.InternetId.ToString();
            //DataSet dsMySqlUser = new DataSet();
            //dsMySqlUser = App.Credability.MySQLUserRecordGet((long)WsAcct.InternetId);
            //// does this need done now????
            //if(dsMySqlUser.Tables.Count != 0 && dsMySqlUser.Tables[0].Rows.Count == 0)
            //{
            //    Response.Redirect("introNewPage.aspx");
            //}

            //SessionState.RegNum = dsMySqlUser.Tables[0].Rows[0]["regnum"].ToString();
            //var ClientResult = App.Host.Mim_LoginClient(SessionState.RegNum);
            // Bankruptcy Conversion
            var ClientResult = App.Debtplus.Mim_LoginClient(SessionState.RegNum, App.WebsiteCode);

            if (ClientResult.LoginMimOk)
            {
                if(SessionState.Username != dsMySqlUser.Tables[0].Rows[0]["uid"].ToString())
                {
                    App.Credability.FixMySqlName((long)WsAcct.InternetId, SessionState.Username);
                }

                SessionState.Clid = ClientResult.ClId;
                SessionState.ContactSsn = ClientResult.ContactSsn;
                SessionState.JointSsn = ClientResult.JointSsn;
                SessionState.SecondaryFlag = ClientResult.SecondaryFlag;
                SessionState.Escrow = ClientResult.Escrow;
                SessionState.Escrow6 = ClientResult.Escrow6;
				SessionState.FirmID = ClientResult.FirmId;
                SessionState.PrimaryZipCode = ClientResult.ZipCode;

                string UvStatus = ClientResult.UvStat;
                int LastPage = Convert.ToInt32(dsMySqlUser.Tables[0].Rows[0]["lastpage"].ToString());
                int PostTest = Convert.ToInt32(dsMySqlUser.Tables[0].Rows[0]["posttest"].ToString());
                int totalhrs = Convert.ToInt32(dsMySqlUser.Tables[0].Rows[0]["totalhrs"].ToString());
                int Q8 = Convert.ToInt32(dsMySqlUser.Tables[0].Rows[0]["Q8"].ToString());

                int hostId;
                if(SessionState.SecondaryFlag == "S")
                    hostId = Convert.ToInt32(ClientResult.SpUvId.Replace("W", String.Empty));
                else
                    hostId = Convert.ToInt32(SessionState.Clid.Replace("W", String.Empty));
                var feeWaiver = App.FeeWaiver.GetByEducationClient(hostId);
                SessionState.FwID = feeWaiver.FwID;

                if(LastPage >= 1)
                {
                    if(PostTest > -1)
                    {
						Response.Redirect("LoginVal.aspx?redirectURL=students/tests/congrats.php");
                    }
                    else if(LastPage == 18 && Q8 > -1 && totalhrs >= 115)
                    {
						Response.Redirect("LoginVal.aspx?redirectURL=students/tests/congrats.php");
					}
                    Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/login.php?uid=" + SessionState.Username);
                }
                Response.Redirect(ClientResult.ReDirTarget);
            }
            else
            {
                Response.Redirect("introNewPage.aspx");
            }
        }
    }
}