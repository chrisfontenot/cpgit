﻿using Cccs.Debtplus.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKEducation
{
    public partial class WaverApp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SessionState.UserPageArrivalTime = DateTime.Now;

            /*
            if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                pnlSSD.Visible = true;
            if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                pnlIW.Visible = true;
            if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                pnlPB.Visible = true;
            */

            if (!IsPostBack)
            {
                ValidationControlTranslation();
//                lblClientID.Text = String.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|ForJSE"), SessionState.Clid);
                btnReturnToPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                SessionState.UserPageArrivalTime = DateTime.Now;
        }
            Label lblHeading = new Label();
            lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
            {
                if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                    lblHeading.Text = App.Translate("WaiverApp|Heading");
                else if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                    lblHeading.Text = App.Translate("WaiverApp|Heading1");
                else if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                    lblHeading.Text = App.Translate("WaiverApp|Heading2");
                else
                    lblHeading.Text = App.Translate("WaiverApp|Heading");
    }

            if (!IsPostBack)
            {
                if (SessionState.RegNum != null)
                {
                    LoadFormData();
                }
                else
                    Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
            }
        }

        private void ValidationControlTranslation()
        {
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");

            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|FNR");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|UserProfileBCH|LNR");
            RequiredFieldValidator3.ErrorMessage = App.Translate("Credability|UserProfileBCH|UAR");
            RequiredFieldValidator5.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
            RequiredFieldValidator4.ErrorMessage = App.Translate("Credability|UserProfileBCH|UCR");
            RequiredFieldValidator6.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
            rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
            RegularExpressionValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|Credability|UserProfileBCH|EAWRR");
            RequiredFieldValidator8.ErrorMessage = App.Translate("Credability|UserProfileBCH|EDR");
        }

        public void LoadFormData()
        {
            // Bankruptcy Conversion
            //Cccs.Host.MimForm7LoadDataResult resultLoadData = App.Host.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
            MimForm7LoadData resultLoadData = App.Debtplus.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
            if (resultLoadData.IsSuccessful)
            {
                txtcontactfirstname.Text = resultLoadData.contact_firstname;
                txtcontactlastname.Text = resultLoadData.contact_lastname;
                txtcontactaddress.Text = resultLoadData.contact_address;
                txtcontactaddress2.Text = resultLoadData.contact_address2;
                txtcontactcity.Text = resultLoadData.contact_city;
                ddlState.SelectedValue = resultLoadData.contact_state;
                txtcontactzip.Text = resultLoadData.contact_zip;
                txtrealemail.Text = resultLoadData.contact_email;
                txtcontacttelephone.Text = resultLoadData.dayphone;
            }
        }

        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            SaveWaiverHostInfo();
            //Bankruptcy conversion-Seethal
            //App.Host.Mim_WaiverWaiver_SaveUv(SessionState.Clid);
            App.Debtplus.Mim_WaiverWaiver_SaveUv(SessionState.Clid);
            Response.Redirect("WaiverPath.aspx");
        }

        protected void Continue_Click(object sender, EventArgs e)
        {
            if ((rboApplyYes.Checked && chkUnderstand.Checked) || (rboApplyNo.Checked))
            {
                SaveWaiverHostInfo();
                //Bankruptcy conversion-Seethal
                //Result Results = App.Host.Mim_WaiverWaiver_SaveUv(SessionState.Clid);
                DebtplusResult Results = App.Debtplus.Mim_WaiverWaiver_SaveUv(SessionState.Clid);
                if (Results.IsSuccessful)
                {
                    if (rboApplyYes.Checked)
                        Response.Redirect("AppSucess.aspx");
                    if (rboApplyNo.Checked)
                        Response.Redirect("WaiverWaiver.aspx");
                }
            }
            else if (rboApplyYes.Checked && !chkUnderstand.Checked)
            {
                //Please Check the Understand Check box.
                dvErrorMessage.InnerHtml = "You must acknowledge that you understand that you will not be able to continue with your counseling until a decision has been made about your fee waiver application.";
            }
        }

        private void SaveWaiverHostInfo()
        {
            var hostId = Convert.ToInt32(SessionState.Clid.Replace("W", String.Empty));

            var feeWaiver = App.FeeWaiver.GetByEducationClient(hostId);
            feeWaiver.JointFile = String.IsNullOrEmpty(txtcosignfirstname.Text) ? 0 : 1;
            feeWaiver.SignUp = 1;
            feeWaiver.WaiverType = SessionState.WaiverType;
            feeWaiver.WaitPolicy = false;
            feeWaiver.LastModDTS = DateTime.Now;
            feeWaiver.PriFName = txtcontactfirstname.Text.Trim();
            feeWaiver.PriLName = txtcontactlastname.Text.Trim();
            feeWaiver.SecFName = txtcosignfirstname.Text.Trim();
            feeWaiver.SecLName = txtcosignlastname.Text.Trim();
            feeWaiver.SignUpDTS = DateTime.Now;

            App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
        }
    }
}