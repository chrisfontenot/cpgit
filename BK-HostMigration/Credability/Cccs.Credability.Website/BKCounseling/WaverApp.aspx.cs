﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Web;
using Cccs.Identity;
using System.Text;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class WaverApp : ContactDetailPage
    {
        #region ViewState

        private long? UserAddressId
        {
            get { return ViewState.ValueGet<long?>("UserAddressID"); }
            set { ViewState.ValueSet("UserAddressID", value); }
        }

        private long? PrimaryUserDetailId
        {
            get { return ViewState.ValueGet<long?>("PrimaryUserDetailId"); }
            set { ViewState.ValueSet("PrimaryUserDetailId", value); }
        }

        private long? SecondaryUserDetailId
        {
            get { return ViewState.ValueGet<long?>("SecondaryUserDetailId"); }
            set { ViewState.ValueSet("SecondaryUserDetailId", value); }
        }

        #endregion

        #region Translations

        private string EmailAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateEmail.");						 // TODO: DONE
        private string SSNAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN.");								 // TODO: DONE
        private string SecondaryEmailAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN."); // TODO: DONE
        private string SecondarySSNAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN.");			 // TODO: DONE

        #endregion

        #region Control Parameters

        public string RedirectOnContinue
        {
            get
            {
                string url = ViewState.ValueGet<string>("RedirectOnContinue");

                return !string.IsNullOrEmpty(url) ? url : "AppSucess.aspx";
            }

            set
            {
                ViewState.ValueSet("RedirectOnContinue", value);
            }
        }

        public string RedirectOnNeedAuthorization
        {
            get
            {
                string url = ViewState.ValueGet<string>("RedirectOnNeedAuthorization");

                return !string.IsNullOrEmpty(url) ? url : "WhatYouNeed.aspx?from=WA";
            }

            set
            {
                ViewState.ValueSet("RedirectOnNeedAuthorization", value);
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
//                lblClientID.Text = String.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|ForJSE"), String.Format("IN{0}", SessionState.ClientNumber));
                btnReturnToPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }

            /*
            if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                pnlSSD.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                pnlIW.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                pnlPB.Visible = true;
            */

//            lblClientID.Text = SessionState.ClientNumber.ToString();

            if (!IsPostBack)
            {
                Initialize();
            }

            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
            {
                if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                    lblHeading.Text = App.Translate("WaiverApp|Heading");
                else if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                    lblHeading.Text = App.Translate("WaiverApp|Heading1");
                else if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                    lblHeading.Text = App.Translate("WaiverApp|Heading2");
                else
                    lblHeading.Text = App.Translate("WaiverApp|Heading");

            }
            ValidationControlTranslation();


        }

        private void ValidationControlTranslation()
        {
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
            rfvFname.ErrorMessage = App.Translate("Credability|UserProfileBCH|FNR");
            rfvLname.ErrorMessage = App.Translate("Credability|UserProfileBCH|LNR");
            rfvaddress.ErrorMessage = App.Translate("Credability|UserProfileBCH|ADD");
            rfvcity.ErrorMessage = App.Translate("Credability|UserProfileBCH|UCR");
            rfvstate.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
            rfvZipCode.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
            rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
            rfvcontactphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNR");
            cfvPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNWFR");
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|EDR");
            RegularExpressionValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|EAWRR");
        }

        private void Initialize()
        {
            if (SessionState.UserId.HasValue)
            {
                GetUserDetailsFromIDM(SessionState.UserId.Value);
            }
        }

        public void GetUserDetailsFromIDM(long user_id)
        {
            // Address
            //
            GetUserAddressFromIDM(user_id);

            // Primary UserDetail
            //
            UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);
  
            if (user_detail_primary != null)
            {
                PrimaryUserDetailId = user_detail_primary.UserDetailId;

                txtcontactfirstname.Text = user_detail_primary.FirstName.Clean();
                txtcontactlastname.Text = user_detail_primary.LastName.Clean();
                txtrealemail.Text = user_detail_primary.Email.Clean();
                txtcontacttelephone.Text = user_detail_primary.PhoneHome.Clean();
                hdnSSN.Value = user_detail_primary.Ssn.Clean();
            }

            // Secondary UserDetail
            //
            UserDetail user_detail_secondary = App.Identity.UserDetailGet(user_id, false);

            if (user_detail_secondary != null)
            {
                SecondaryUserDetailId = user_detail_secondary.UserDetailId;

                txtcosignfirstname.Text = user_detail_secondary.FirstName.Clean();
                txtcosignlastname.Text = user_detail_secondary.LastName.Clean();
            }
        }

        private void GetUserAddressFromIDM(long user_id)
        {
            Address[] address = App.Identity.AddressesGet(user_id);

            if ((address != null) && (address.Length != 0))
            {
                hdnISNewUser.Value = "N";
                UserAddressId = address[0].AddressId;
                txtcontactaddress.Text = address[0].StreetLine1.Clean();
                txtcontactaddress2.Text = address[0].StreetLine2.Clean();
                txtcontactcity.Text = address[0].City.Clean();
                txtcontactzip.Text = address[0].Zip.Clean();
                ddlState.SelectedValue = address[0].State.Clean();
            }
            else
            {
                hdnISNewUser.Value = "Y";
            }
        }

        public bool SaveUserDetailPrimary(long user_id)
        {
            bool is_successful = false;

            UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);

            if (user_detail_primary != null)
            {
                user_detail_primary.FirstName = txtcontactfirstname.Text.Clean();
                user_detail_primary.LastName = txtcontactlastname.Text.Clean();
                user_detail_primary.Email = txtrealemail.Text.Clean();
                user_detail_primary.PhoneHome = txtcontacttelephone.Text.Clean();
                user_detail_primary.Ssn = hdnSSN.Value.ToString();

                string AuditorName = txtcontactfirstname.Text.Clean() + " " + txtcontactlastname.Text.Clean(); // TODO: SessionState.Username

                UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_primary, AuditorName);

                is_successful = user_detail_save_result.IsSuccessful;

                if (!is_successful)
                {
                    if (user_detail_save_result.IsDuplicateEmail == true)
                    {
                        CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, EmailAlreadyExistMessage, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, EmailAlreadyExistMessage, true);
                    }

                    if (user_detail_save_result.IsDuplicateSsn == true)
                    {
                        CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, SSNAlreadyExistMessage, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
                    }

                    if (!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
                    {
#if DEBUG
                        CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, user_detail_save_result.ExceptionStr, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
#endif
                    }
                }
            }

            return is_successful;
        }

        private void MakeErrorDivVisible(bool visible)
        {
            dvServerSideValidation.Visible = visible;
        }

        public bool SaveUserDetailSecondary(long UserId)
        {
            bool is_successful = false;

            UserDetail user_detail_secondary = App.Identity.UserDetailGet(UserId, false);

            if (user_detail_secondary == null) // For new Secondry User
            {
                user_detail_secondary = new UserDetail { UserId = UserId, IsPrimary = false, };
            }

            user_detail_secondary.FirstName = txtcosignfirstname.Text.Clean();
            user_detail_secondary.LastName = txtcosignlastname.Text.Clean();

            string AuditorName = txtcosignfirstname.Text.Trim() + " " + txtcosignlastname.Text.Trim(); // TODO: SessionState.Username

            UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_secondary, AuditorName);

            is_successful = user_detail_save_result.IsSuccessful;

            if (!is_successful)
            {
                if (user_detail_save_result.IsDuplicateEmail == true)
                {
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, EmailAlreadyExistMessage, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, EmailAlreadyExistMessage, true);
                }

                if (user_detail_save_result.IsDuplicateSsn == true)
                {
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, SSNAlreadyExistMessage, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
                }

                if (!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
                {
#if DEBUG
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, user_detail_save_result.Exception.ToString(), true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
#endif

                }
            }

            return is_successful;
        }

        private bool UserAddressUpdateIntoIDM(long user_id)
        {
            bool is_successful = false;

            Address address = null;

            if (UserAddressId != null)
            {
                Address[] addresses = App.Identity.AddressesGet(user_id);

                if ((addresses != null) && (addresses.Length > 0))
                {
                    address = addresses[0];
                }
            }

            if (address == null)
            {
                address = new Address { UserId = user_id, };
            }

            address.AddressType = "HOME";
            address.StreetLine1 = txtcontactaddress.Text.Clean();
            address.StreetLine2 = txtcontactaddress2.Text.Clean();
            address.City = txtcontactcity.Text.Clean();
            address.Zip = txtcontactzip.Text.Clean();
            address.State = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;

            string AuditorName = txtcontactfirstname.Text.Trim() + " " + txtcontactlastname.Text.Trim();

            AddressSaveResult result = App.Identity.AddressSave(address, AuditorName);

            is_successful = result.IsSuccessful;

            if (is_successful)
            {
                UserAddressId = result.AddressId;
            }
            else
            {
                if (result.IsInvalidZipCode == true)
                {
                    string ErrorMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|InValidState") + " " + result.StateExpectedForZip;      // TODO: Translate "Invalid ZipCode."; //
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, ErrorMessage, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, ErrorMessage, true);
                }

                if (!string.IsNullOrEmpty(result.ExceptionStr))
                {
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, result.ExceptionStr, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, result.ExceptionStr, true);
                }
            }

            return is_successful;
        }

        private void SaveWaiverInfo(Int32 FeeWaiverID)
        {
            var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(FeeWaiverID);

            feeWaiver.JointFile = String.IsNullOrEmpty(txtcosignfirstname.Text) ? 0 : 1;
            feeWaiver.SignUp = 1;
            feeWaiver.WaiverType = SessionState.WaiverType;
            feeWaiver.WaitPolicy = false;
            feeWaiver.LastModDTS = DateTime.Now;
            feeWaiver.PriFName = txtcontactfirstname.Text.Trim();
            feeWaiver.PriLName = txtcontactlastname.Text.Trim();
            feeWaiver.SecFName = txtcosignfirstname.Text.Trim();
            feeWaiver.SecLName = txtcosignlastname.Text.Trim();
            feeWaiver.SignUpDTS = DateTime.Now;

            feeWaiver = App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
        }

        private void UserInfoAddUpdate(Boolean is_secondary_valid)
        {
            if (RedirectOnContinue == "AppSucess.aspx")
            {
                if (rboApplyYes.Checked == true) RedirectOnContinue = "AppSucess.aspx";
                else if (rboApplyNo.Checked == true) RedirectOnContinue = "WaiverWaiver.aspx";
            }

            // Update UserContactDetails
            bool success = true;
            UserContactDetails user_contact_details = App.Credability.UserInfoGet(SessionState.ClientNumber.Value) ?? new UserContactDetails();

            user_contact_details.ClientNumber = SessionState.ClientNumber.Value;

            user_contact_details.ZipCode = txtcontactzip.Text.Clean();

            if (success)
            {
                UserContactDetailsResult result = App.Credability.UserInformationUpdate(user_contact_details);
                if (result.IsSuccessful)
                {
                    // Bankruptcy Conversion
                    //App.Host.ReqCredRpt(SessionState.UserId.Value, SessionState.ClientNumber.Value);
                        App.Debtplus.ReqCredRpt(SessionState.UserId.Value, SessionState.ClientNumber.Value);

                    if (is_secondary_valid)
                    {
                        var afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);

                        if ((afs != null) && string.IsNullOrEmpty(afs.SecondaryPerson))
                        {
                            Response.Redirect(RedirectOnNeedAuthorization);
                        }
                        else
                        {
                            Response.Redirect(RedirectOnContinue);
                        }
                    }
                    else
                    {
                        Response.Redirect(RedirectOnContinue);
                    }
                }
                else
                {
                    dvTopErrorMessage.InnerHtml = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"); // TODO: Translate
                    DesValidationSummary.Visible = false;
                }
            }
            else
            {
                dvTopErrorMessage.InnerHtml = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"); // TODO: Translate
                DesValidationSummary.Visible = false;
            }

        }


        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");
        }

        protected void Continue_Click(object sender, EventArgs e)
        {
            Boolean ErrorFlag = true;
            if (rboApplyYes.Checked == false && rboApplyNo.Checked == false)
            {
                dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|PSW") + "</li>";
                dvServerSideValidation.Visible = true;
                ErrorFlag = false;
            }
            else
            {
                ErrorFlag = true;
                if (rboApplyYes.Checked == true && chkUnderstand.Checked == false)
                {
                    dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                    dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|PSW") + "</li>";
                    dvServerSideValidation.Visible = true;
                    ErrorFlag = false;
                }
                else
                    ErrorFlag = true;
            }

            if (txtcosignfirstname.Text.Trim() != string.Empty)
            {

                if (txtcosignlastname.Text.Trim() == string.Empty)
                {
                    ErrorFlag = false;
                    dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>"; //"You must enter a value in the following fields:<ul>";
                    dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COLNR") + "</li>";
                    dvServerSideValidation.Visible = true;
                }

            }

            if (ErrorFlag == true)
            {
                if ((rboApplyYes.Checked == true && chkUnderstand.Checked == true) || (rboApplyNo.Checked == true))
                {
                    StringBuilder sb = new StringBuilder(); ;
                    if (Page.IsValid)
                    {
                        dvServerSideValidation.Visible = false;
                        dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";	// TODO: Translate

                        bool is_primary_valid = true;

                        // Owner validation
                        //
                        if (string.IsNullOrEmpty(txtcontactcity.Text.Clean()))
                        {
                            dvServerSideValidation.InnerHtml += "<li>City is required.</li>"; // TODO: Translate
                            is_primary_valid = false;
                        }

                        string state_code = App.Geographics.StateCodeGetByZip(txtcontactzip.Text.Clean());

                        if (string.IsNullOrEmpty(state_code))
                        {
                            dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|IZC") + "</li>"; // TODO: Translate
                            is_primary_valid = false;
                        }
                        else if (string.Compare(state_code, ddlState.SelectedValue, true) != 0)
                        {
                            dvServerSideValidation.InnerHtml += string.Format("<li>" + App.Translate("Credability|UserProfileBCH|SEFZI") + "</li>", txtcontactzip.Text, state_code); //string.Format("<li>State expected for Zip {0} is {1}.</li>", txtcontactzip.Text, state_code); // TODO: Translate
                            is_primary_valid = false;
                        }


                        // Co-owner Server side validation
                        //
                        bool? is_secondary_valid = null;

                        if (txtcosignfirstname.Text != string.Empty)
                        {
                            is_secondary_valid = true;

                            if (txtcosignlastname.Text.Trim() == string.Empty)
                            {
                                dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COLNR") + "</li>"; //"<li>Co-Owner  last Name is Required.</li>"; // TODO: Translate
                                is_secondary_valid = false;
                            }
                        }

                        if (!is_primary_valid || (is_secondary_valid == false))
                        {
                            dvServerSideValidation.Visible = true;
                            dvServerSideValidation.Focus();
                        }

                        dvServerSideValidation.InnerHtml += "</ul>";

                        // Code Added on 06/10/2010 - for Save the Fee Waiver Information based on the FeeWaiverID
                        if (SessionState.FwID != null)
                        {
                            SaveWaiverInfo(SessionState.FwID.Value);
                        }

                        if (hdnISNewUser.Value == "N")
                        {
                            if (SaveUserDetailPrimary(SessionState.UserId.Value) && UserAddressUpdateIntoIDM(SessionState.UserId.Value))
                            {
                                if (is_secondary_valid == true)
                                {
                                    if (SaveUserDetailSecondary(SessionState.UserId.Value))
                                    {
                                        UserInfoAddUpdate(true); // Update the information in contactdetails.
                                    }
                                }
                                else
                                {
                                    App.Identity.UserDetailDeleteSecondary(SessionState.UserId.Value);
                                    UserInfoAddUpdate(false); // Update the information in contactdetails.
                                }
                            }
                        }
                        else if (hdnISNewUser.Value == "Y")
                        {
                            WaverAppBKCounseling waverAppBKCounseling = new WaverAppBKCounseling();
                            WaverAppBKCounselingResult waverAppBKCounselingResult = null;

                            waverAppBKCounselingResult = App.Credability.WaverAppBKCounselingUpdate(waverAppBKCounseling);

                            if (is_secondary_valid == true)
                            {
                                UserInfoAddUpdate(true); // Update the information in contactdetails.
                            }
                            else
                            {
                                App.Identity.UserDetailDeleteSecondary(SessionState.UserId.Value);
                                UserInfoAddUpdate(false); // Update the information in contactdetails.
                            }
                        }

                        if (rboApplyYes.Checked == true) Response.Redirect("AppSucess.aspx");
                        if (rboApplyNo.Checked == true) Response.Redirect("WaiverWaiver.aspx");
                    }
                }
            }
        }
    }
}
