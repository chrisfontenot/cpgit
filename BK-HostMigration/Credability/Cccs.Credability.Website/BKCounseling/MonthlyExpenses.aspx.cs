﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class MonthlyExpenses : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Budget);

				CommonFunction.UserProgressSave(PercentComplete.MONTHLY_EXPENSES, SessionState.Username);
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcMonthlyExpenses.FindControl("TopTextGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl FhaQuestion = UcMonthlyExpenses.FindControl("FhaQuestion") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(FhaQuestion != null)
			{
				FhaQuestion.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl HideForBK = UcMonthlyExpenses.FindControl("HideForBK") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(HideForBK != null)
			{
				HideForBK.Visible = false;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Equity = UcMonthlyExpenses.FindControl("EquityGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Equity != null)
			{
				Equity.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Mort2nd = UcMonthlyExpenses.FindControl("Mort2ndGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Mort2nd != null)
			{
				Mort2nd.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl PayTax = UcMonthlyExpenses.FindControl("PayTaxGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PayTax != null)
			{
				PayTax.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl PayIns = UcMonthlyExpenses.FindControl("PayInsGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PayIns != null)
			{
				PayIns.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl AssocFee = UcMonthlyExpenses.FindControl("AssocFeeGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(AssocFee != null)
			{
				AssocFee.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Gas = UcMonthlyExpenses.FindControl("GasGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Gas != null)
			{
				Gas.Visible = true;
			}
		}
	}
}