﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BKCounselingControls;
using Cccs.Web;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class DescribingYourSituation : ContactDetailPage
    {
        #region ViewState

        private string LanguageCode
        {
            get { return ViewState.ValueGet<string>("LanguageCode"); }
            set { ViewState.ValueSet("LanguageCode", value); }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.TellUs);
                CommonFunction.UserProgressSave(PercentComplete.DESCRIBE_YOUR_SITUATION, SessionState.Username);

                btnMonthlyPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnMonthlyExpensesCon.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                btnMonthlySaveExitPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
                CommonFunction.AddItems(ddlHousingSituation.Items, "HousingOptionsDropDown", SessionState.LanguageCode); //HousingOptionsDropDown

                InitializeTranslation();
                RadioButtonTransaltion();
                ValidationControlTranslation();

                if (SessionState.ClientNumber.HasValue)
                {
                    UserDescribeYourSituationBKCGet(SessionState.ClientNumber.Value);
                }

                ShowHideHousingControls();
            }
        }

        private void ValidationControlTranslation()
        {
            rfddlPriEvent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSTE");
            rfvddlSecEvent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SEIR");
            rfvtxtComments.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CIR");
            rfvddlHousingSituation.ErrorMessage = App.Translate("Credability|DescYourSituRVM|HUO");
            RequiredFieldValidator3.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SYP");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SYP");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
            rfvtxtHouseholdSize.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPI");
            RequiredFieldValidator4.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPI");
            CompareValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IHS");
            //RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YNTE");
        }

        private void RadioButtonTransaltion()
        {
            CommonFunction.AddItems(rbtFilledBankruptcy.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
            CommonFunction.AddItems(rbtForclPrev.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
            CommonFunction.AddItems(rbtMortCurrent.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
        }

        private void InitializeTranslation()
        {
            if (LanguageCode != SessionState.LanguageCode) // First page load, or language has changed 
            {
                rfddlPriEvent.InitialValue = App.Translate("Credability|PickList|Select");
                ddlPriEvent.Items.Clear();
                ddlPriEvent.Items.Add(new ListItem(App.Translate("Credability|PickList|Select")));
                CommonFunction.AddItems(ddlPriEvent.Items, "TheEventBch", SessionState.LanguageCode);

                //Secondary Event Pick List
                ddlSecEvent.Items.Clear();
                ddlSecEvent.Items.Add(new ListItem(App.Translate("Credability|PickList|Select")));
                rfvddlSecEvent.InitialValue = App.Translate("Credability|PickList|Select");
                CommonFunction.AddItems(ddlSecEvent.Items, "TheEventBch", SessionState.LanguageCode);
            }
        }

        public void UserDescribeYourSituationBKCGet(Int32 ClientNumber)
        {
            UserDescribeYourSituationBKC UserDescribeYourSituationBKC = null;
            UserDescribeYourSituationBKC = App.Credability.BKCUserDescribeYourSituationGet(ClientNumber);

            if (UserDescribeYourSituationBKC != null)
            {
                txtComments.Text = (UserDescribeYourSituationBKC.ContactComments != null) ? UserDescribeYourSituationBKC.ContactComments.Trim() : String.Empty;
                ddlPriEvent.SelectedValue = (UserDescribeYourSituationBKC.ContactReason != null) ? UserDescribeYourSituationBKC.ContactReason.Trim() : String.Empty;
                rbtFilledBankruptcy.SelectedValue = (UserDescribeYourSituationBKC.Bankrupt != null) ? UserDescribeYourSituationBKC.Bankrupt.Trim() : String.Empty;
                ddlHousingSituation.SelectedValue = UserDescribeYourSituationBKC.HousingType.ToString();
                rbtForclPrev.SelectedValue = UserDescribeYourSituationBKC.BkForPrv.HasValue ? UserDescribeYourSituationBKC.BkForPrv.ToString() : String.Empty;
                txtHouseholdSize.Text = UserDescribeYourSituationBKC.SizeofHouseHold == 0 ? String.Empty : UserDescribeYourSituationBKC.SizeofHouseHold.ToString();
                txtMortLate.Text = Convert.ToInt32(UserDescribeYourSituationBKC.MosDelinq).ToString();

                rbtMortCurrent.SelectedValue = UserDescribeYourSituationBKC.MortCurrent.HasValue ? UserDescribeYourSituationBKC.MortCurrent.ToString() : String.Empty;
                rbtMortCurrent.SelectedValue = rbtMortCurrent.SelectedValue == "0" ? "1" : "0"; // Question is swapped from legacy so reverse answer from database.

                pnlMort.Visible = true;
                if (ddlHousingSituation.SelectedValue == "B")
                {
                    SessionState.DelqType = "Mort";
                    MortM.Visible = true;
                    MortR.Visible = false;
                    //UcAuthCode.Visible = rbtMortCurrent.SelectedValue == "0";
                }
                else if (ddlHousingSituation.SelectedValue == "R")
                {
                    SessionState.DelqType = "Rent";
                    MortR.Visible = true;
                    MortM.Visible = false;
                    //UcAuthCode.Visible = rbtMortCurrent.SelectedValue == "0";
                }
                else if (ddlHousingSituation.SelectedValue == "X" || ddlHousingSituation.SelectedValue == "O" || ddlHousingSituation.SelectedValue == "0")
                {
                    //UcAuthCode.Visible = true;
                    MortM.Visible = false;
                    MortR.Visible = false;
                    Mort0.Visible = false;
                }
            }

            UserDescribeYourSituationBKC UserDescribeYourSituationBKC2 = null;
            UserDescribeYourSituationBKC2 = App.Credability.BKCUserDescribeYourSituationExtraQuestGet(ClientNumber);
            if (UserDescribeYourSituationBKC2 != null)
                ddlSecEvent.SelectedValue = (UserDescribeYourSituationBKC2.SecEvent != null) ? UserDescribeYourSituationBKC2.SecEvent.Trim() : String.Empty;

        }

        private UserDecsribeYourSituationBKCResult SaveDescribeYourSelfInfo(Int32 ClientNumber)
        {
            UserDescribeYourSituationBKC UserDescribeYourSituationBKC = new UserDescribeYourSituationBKC();
            UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult = null;

            UserDescribeYourSituationBKC.ClientNumber = ClientNumber;
            UserDescribeYourSituationBKC.Bankrupt = rbtFilledBankruptcy.SelectedValue.Trim();
            UserDescribeYourSituationBKC.BkForPrv = Convert.ToInt32(rbtForclPrev.SelectedValue);
            UserDescribeYourSituationBKC.ContactReason = ddlPriEvent.SelectedValue.Trim();
            UserDescribeYourSituationBKC.ContactComments = txtComments.Text.Trim();
            UserDescribeYourSituationBKC.HousingType = Convert.ToChar(ddlHousingSituation.SelectedValue.Trim());
            if (ddlHousingSituation.SelectedValue == "B" || ddlHousingSituation.SelectedValue == "R")
            {
                UserDescribeYourSituationBKC.MortCurrent = (rbtMortCurrent.SelectedValue != null) ? Convert.ToInt32(rbtMortCurrent.SelectedValue) : 0;
                UserDescribeYourSituationBKC.MortCurrent = UserDescribeYourSituationBKC.MortCurrent == 0 ? 1 : 0; // Question is reversed from legacy so swap answer from database.
            }
            UserDescribeYourSituationBKC.SizeofHouseHold = Convert.ToInt32(txtHouseholdSize.Text);
            UserDescribeYourSituationBKC.MosDelinq = txtMortLate.Text.ToDecimal(0);

            UserDecsribeYourSituationBKCResult = App.Credability.UserDescribeYourSituationForBKCAddUpdate(UserDescribeYourSituationBKC);
            return UserDecsribeYourSituationBKCResult;
        }

        private UserDecsribeYourSituationBKCResult SaveDescribeYourSelfExtraInfo(Int32 ClientNumber)
        {
            var UserDescribeYourSituationBKC = new UserDescribeYourSituationBKC();
            UserDescribeYourSituationBKC.ClientNumber = ClientNumber;
            UserDescribeYourSituationBKC.SecEvent = ddlSecEvent.SelectedValue.Trim();

            var UserDecsribeYourSituationBKCResult = App.Credability.UserDescribeYourSituationBKCAddUpdateSecEvent(UserDescribeYourSituationBKC);

            return UserDecsribeYourSituationBKCResult;
        }

        protected void btnMonthlyPreviousPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProvideYourPersonalInformation.aspx");
        }

        protected void btnMonthlyExpensesCon_Click(object sender, EventArgs e)
        {
			if(ddlHousingSituation.SelectedValue != "B")
            {
                //if (txtAuthCode.Text != "")
                //{
                    //if (CommonFunction.IsValidChatCode(ChatCodeOption.Session1, txtAuthCode.Text))
                    //{
                        UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult = SaveDescribeYourSelfInfo(Convert.ToInt32(SessionState.ClientNumber));
                        UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult2 = SaveDescribeYourSelfExtraInfo(Convert.ToInt32(SessionState.ClientNumber));
                        if (UserDecsribeYourSituationBKCResult.IsSuccessful && UserDecsribeYourSituationBKCResult2.IsSuccessful)
                        {
                            if (rbtMortCurrent != null &&
                                                    rbtMortCurrent.SelectedItem != null &&
                                                    rbtMortCurrent.SelectedValue == "0" && (ddlHousingSituation.SelectedValue == "B" || ddlHousingSituation.SelectedValue == "R") &&
                                                    txtMortLate.Text.ToInt(0) > 0)
                            {
                                CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "You have indicated that you are not behind on your mortgage but your number of missed payments is greater than 0.Please correct in order to proceed.", true);
                            }
                            else
                            {
								if(ddlHousingSituation.SelectedValue == "B")
                                {
                                    Response.Redirect("DescribingYourSituation2.aspx");
                                }
                                else if (ddlHousingSituation.SelectedValue == "R" && rbtMortCurrent.SelectedValue == "0")
                                {
                                    Response.Redirect("ListingYourDebts.aspx");
                                }
                                else if (ddlHousingSituation.SelectedValue == "R" && rbtMortCurrent.SelectedValue == "1")
                                {
                                    Response.Redirect("DescribeYourSituationRentalInfo.aspx");
                                }
                                else if ((ddlHousingSituation.SelectedValue == "X" || ddlHousingSituation.SelectedValue == "O") &&
                                        (rbtMortCurrent.SelectedValue == "1") || (rbtMortCurrent.SelectedValue == "0"))
                                {
                                    Response.Redirect("ListingYourDebts.aspx");
                                }
                                else
                                {
                                    Response.Redirect("ListingYourDebts.aspx");
                                }
                            }

                        }
                        else
                        {
                            dvErrorMessage.InnerHtml = UserDecsribeYourSituationBKCResult.Exception.ToString() + "<br /><br />" + UserDecsribeYourSituationBKCResult.Exception.ToString();  //"Error in Updating Information.";
                        }
                    //}
                    //else
                    //{
                    //    dvErrorMessage.InnerHtml = "Invalid Authentication Code Entered.";
                    //}
                //}
                //else
                //{
                //    dvErrorMessage.InnerHtml = "Authentication Code is Required.";
                //}

            }
            else
            {
                if (rbtMortCurrent != null &&
                                                    rbtMortCurrent.SelectedItem != null &&
                                                    rbtMortCurrent.SelectedValue == "1" && (ddlHousingSituation.SelectedValue == "B" || ddlHousingSituation.SelectedValue == "R") &&
                                                    txtMortLate.Text.ToInt(0) == 0)
                {
                    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "You have indicated that you are not current on your payments but your number of missed payments is 0.Please correct in order to proceed.", true);
                }
                else
                {

                    UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult = SaveDescribeYourSelfInfo(Convert.ToInt32(SessionState.ClientNumber));
                    UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult2 = SaveDescribeYourSelfExtraInfo(Convert.ToInt32(SessionState.ClientNumber));
                    if (UserDecsribeYourSituationBKCResult.IsSuccessful && UserDecsribeYourSituationBKCResult2.IsSuccessful)
                    {
                        if (ddlHousingSituation.SelectedValue == "R" && rbtMortCurrent.SelectedValue == "1")
                            Response.Redirect("DescribeYourSituationRentalInfo.aspx");
                        else if (ddlHousingSituation.SelectedValue == "B")
                            Response.Redirect("DescribingYourSituation2.aspx");
                        else
                            Response.Redirect("ListingYourDebts.aspx");
                    }
                    else
                    {
                        if (!UserDecsribeYourSituationBKCResult.IsSuccessful && UserDecsribeYourSituationBKCResult.Exception != null)
                        {
                            dvErrorMessage.InnerHtml += UserDecsribeYourSituationBKCResult.Exception.ToString() + "<br /><br />";
                        }
                        if (!UserDecsribeYourSituationBKCResult2.IsSuccessful && UserDecsribeYourSituationBKCResult2.Exception != null)
                        {
                            dvErrorMessage.InnerHtml += UserDecsribeYourSituationBKCResult2.Exception.ToString() + "<br /><br />";
                        }
                    }
                }
            }
        }

        protected void btnMonthlySaveExitPage_Click(object sender, EventArgs e)
        {
            UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult = SaveDescribeYourSelfInfo(Convert.ToInt32(SessionState.ClientNumber));
            UserDecsribeYourSituationBKCResult UserDecsribeYourSituationBKCResult2 = SaveDescribeYourSelfExtraInfo(Convert.ToInt32(SessionState.ClientNumber));
            if (UserDecsribeYourSituationBKCResult.IsSuccessful && UserDecsribeYourSituationBKCResult2.IsSuccessful)
                Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
            else
            {
                dvErrorMessage.InnerHtml = UserDecsribeYourSituationBKCResult.Exception.ToString() + "<br /><br />" + UserDecsribeYourSituationBKCResult.Exception.ToString();  //"Error in Updating Information.";
            }
        }

        protected void ddlHousingSituation_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowHideHousingControls();
            ddlHousingSituation.Focus();
        }


        protected void ShowHideHousingControls()
        {
            if (ddlHousingSituation.SelectedValue == "B" || ddlHousingSituation.SelectedValue == "R")
            {
                pnlMort.Visible = true;
                //UcAuthCode.Visible = false;
                if (ddlHousingSituation.SelectedValue == "B")
                {
                    SessionState.DelqType = "Mort";
                    Mort0.Visible = true;
                    MortM.Visible = true;
                    MortR.Visible = false;
					//UcAuthCode.Visible = false;
                }
                else if (ddlHousingSituation.SelectedValue == "R")
                {
                    SessionState.DelqType = "Rent";
                    Mort0.Visible = true;
                    MortR.Visible = true;
                    MortM.Visible = false;
					//UcAuthCode.Visible = true;
                }
            }
            else if (ddlHousingSituation.SelectedValue == "X" || ddlHousingSituation.SelectedValue == "O" || ddlHousingSituation.SelectedValue == "0")
            {
                pnlMort.Visible = false;
                //UcAuthCode.Visible = true;
            }
            else
            {
                //UcAuthCode.Visible = false;
                pnlMort.Visible = false;
            }
        }
    }
}