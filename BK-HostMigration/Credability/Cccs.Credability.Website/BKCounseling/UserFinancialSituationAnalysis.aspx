﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserFinancialSituationAnalysis.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.UserFinancialSituationAnalysis" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Financial Situation Analysis BKCounseling"%>
<%@ Register Src="~/Controls/Shared/Pages/UcFinancialAnalysis.ascx" TagPrefix="Uc" TagName="UcFinalFinancialSituationAnalysis" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UcFinalFinancialSituationAnalysis id="UcFinalFinancialSituationAnalysis" 
        runat="server" RedirectOnContinue="CommonOptionsFinancialCrisis.aspx" RedirectOnPrevious="UnderstandingYourNetWorth.aspx" ReturnTo="UserFinancialSituationAnalysis" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>