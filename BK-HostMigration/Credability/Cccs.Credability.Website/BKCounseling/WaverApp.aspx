﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaverApp.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.WaverApp"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility WaverApp BKCounseling"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script type="text/javascript">
    var SendSubmit = true;

    function ShowMenu() {
        var menu, PayOpt, i, j, Opt = "";
        PayOpt = document.getElementsByName("payment_type");
        for (i = 0; i < PayOpt.length; i++)//>
        {
            if (PayOpt[i].checked) {
                Opt = PayOpt[i].value;
                i = PayOpt.length;
            }
        }
        for (j = 0; j <= 3; j++)//>
        {
            menu = "pay";
            menu = menu + j;
            document.getElementById(menu).style.display = 'none';
        }
        document.getElementById("Check").style.fontWeight = 'normal';
        document.getElementById("Debt").style.fontWeight = 'normal';
        document.getElementById("West").style.fontWeight = 'normal';
        switch (Opt) {
            case "C":
                document.getElementById('pay1').style.display = 'block';
                document.getElementById("Check").style.fontWeight = 'bold';
                break;
            case "W":
                document.getElementById('pay2').style.display = 'block';
                document.getElementById("West").style.fontWeight = 'bold';
                break;
            case "D":
                document.getElementById('pay3').style.display = 'block';
                document.getElementById("Debt").style.fontWeight = 'bold';
                break;
            default:
                document.getElementById('pay0').style.display = 'block';
        }
    }
    function ReSetPhone(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatUSPhone(Val);
        }
    }

    function ShowMenu() {
        var menu, PayOpt, i, j, Opt = "";
        PayOpt = document.getElementsByName("payment_type");
        for (i = 0; i < PayOpt.length; i++)//>
        {
            if (PayOpt[i].checked) {
                Opt = PayOpt[i].value;
                i = PayOpt.length;
            }
        }
        for (j = 0; j <= 2; j++)//>
        {
            menu = "pay";
            menu = menu + j;
            document.getElementById(menu).style.display = 'none';
        }
        document.getElementById("Check").style.fontWeight = 'normal';
        document.getElementById("West").style.fontWeight = 'normal';
        switch (Opt) {
            case "C":
                document.getElementById('pay1').style.display = 'block';
                document.getElementById("Check").style.fontWeight = 'bold';
                break;
            case "W":
                document.getElementById('pay2').style.display = 'block';
                document.getElementById("West").style.fontWeight = 'bold';
                break;
            default:
                document.getElementById('pay0').style.display = 'block';
        }
    }

    function validateForm(form) {

        Send = SendSubmit;
        SendSubmit = false;
        setTimeout("SendSubmit = true", 5000);
        return Send;
    }
    function CheckprimaryPhone(sender, args) {

        if (args.Value.length > 14 || args.Value.length < 14 || args.Value.length == 10) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }

        //args.Value;

    }
</script>

<p class="col_title">
  <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YET")%></p>
<asp:validationsummary id="DesValidationSummary" validationgroup="user" cssclass="DvErrorSummary" displaymode="BulletList"
  enableclientscript="true" runat="server" forecolor="#A50000" />
<div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false">
</div>
<div class="error" id="dvTopErrorMessage" runat="server">
</div>
<div class="dvform2col">
  <div class="colformlft">
    <div class="dvform">
      <div class="dvrow">
        <p class="col_title">
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PrimaryP")%></p>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FirstN")%></label><asp:textbox id="txtcontactfirstname"
            runat="server" cssclass="txtBox"></asp:textbox>
        <span style="color: Red; font-size: large"> *</span>
        <%--<font color="red"> * </font><br />--%>
        <asp:requiredfieldvalidator id="rfvFname" runat="server" controltovalidate="txtcontactfirstname" enableclientscript="true"
          text="!" errormessage="First name Required." validationgroup="user">
        </asp:requiredfieldvalidator>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|LastN")%></label><asp:textbox id="txtcontactlastname"
            runat="server" cssclass="txtBox"></asp:textbox>
        <span style="color: Red; font-size: large"> *</span>
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="txtcontactlastname" 
                            ErrorMessage=" Last Name Required" SetFocusOnError="True" 
                            ValidationGroup="user" CssClass="error">*</asp:RequiredFieldValidator>--%>
        <asp:requiredfieldvalidator id="rfvLname" runat="server" controltovalidate="txtcontactlastname" enableclientscript="true"
          text="!" errormessage="Last Name Required." validationgroup="user">
        </asp:requiredfieldvalidator>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SADD")%></label><asp:textbox id="txtcontactaddress"
            runat="server" cssclass="txtBox"></asp:textbox>
        <span style="color: Red; font-size: large"> *</span>
        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                            ControlToValidate="txtcontactaddress" Display="Static" 
                            ErrorMessage="Address Required" SetFocusOnError="True" 
                            ValidationGroup="user" CssClass="error"></asp:RequiredFieldValidator>--%>
        <asp:requiredfieldvalidator id="rfvaddress" runat="server" controltovalidate="txtcontactaddress" enableclientscript="true"
          text="!" errormessage="Address Required." validationgroup="user">
        </asp:requiredfieldvalidator>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AD2")%></label><asp:textbox id="txtcontactaddress2"
            runat="server" cssclass="txtBox"></asp:textbox>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|C")%></label><asp:textbox id="txtcontactcity"
            runat="server" cssclass="txtBox">
          </asp:textbox>
        <span style="color: Red; font-size: large"> *</span>
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                            ControlToValidate="txtcontactcity" Display="Static" 
                            ErrorMessage=" City Required" SetFocusOnError="true" 
                            ValidationGroup="user" CssClass="error"></asp:RequiredFieldValidator>--%>
        <asp:requiredfieldvalidator id="rfvcity" runat="server" controltovalidate="txtcontactcity" enableclientscript="true"
          text="!" errormessage="City Required." validationgroup="user">
        </asp:requiredfieldvalidator>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ST")%></label>
        <asp:dropdownlist id="ddlState" runat="server">
          <asp:listitem value="">--select--</asp:listitem>
          <asp:listitem value="AA">AA</asp:listitem>
          <asp:listitem value="AE">AE</asp:listitem>
          <asp:listitem value="AK">AK</asp:listitem>
          <asp:listitem value="AL">AL</asp:listitem>
          <asp:listitem value="AP">AP</asp:listitem>
          <asp:listitem value="AR">AR</asp:listitem>
          <asp:listitem value="AS">AS</asp:listitem>
          <asp:listitem value="AZ">AZ</asp:listitem>
          <asp:listitem value="CA">CA</asp:listitem>
          <asp:listitem value="CO">CO</asp:listitem>
          <asp:listitem value="CT">CT</asp:listitem>
          <asp:listitem value="DC">DC</asp:listitem>
          <asp:listitem value="DE">DE</asp:listitem>
          <asp:listitem value="FL">FL</asp:listitem>
          <asp:listitem value="FM">FM</asp:listitem>
          <asp:listitem value="GA">GA</asp:listitem>
          <asp:listitem value="GU">GU</asp:listitem>
          <asp:listitem value="HI">HI</asp:listitem>
          <asp:listitem value="IA">IA</asp:listitem>
          <asp:listitem value="ID">ID</asp:listitem>
          <asp:listitem value="IL">IL</asp:listitem>
          <asp:listitem value="IN">IN</asp:listitem>
          <asp:listitem value="KS">KS</asp:listitem>
          <asp:listitem value="KY">KY</asp:listitem>
          <asp:listitem value="LA">LA</asp:listitem>
          <asp:listitem value="MA">MA</asp:listitem>
          <asp:listitem value="MD">MD</asp:listitem>
          <asp:listitem value="ME">ME</asp:listitem>
          <asp:listitem value="MH">MH</asp:listitem>
          <asp:listitem value="MI">MI</asp:listitem>
          <asp:listitem value="MN">MN</asp:listitem>
          <asp:listitem value="MO">MO</asp:listitem>
          <asp:listitem value="MP">MP</asp:listitem>
          <asp:listitem value="MS">MS</asp:listitem>
          <asp:listitem value="MT">MT</asp:listitem>
          <asp:listitem value="NC">NC</asp:listitem>
          <asp:listitem value="ND">ND</asp:listitem>
          <asp:listitem value="NE">NE</asp:listitem>
          <asp:listitem value="NH">NH</asp:listitem>
          <asp:listitem value="NJ">NJ</asp:listitem>
          <asp:listitem value="NM">NM</asp:listitem>
          <asp:listitem value="NV">NV</asp:listitem>
          <asp:listitem value="NY">NY</asp:listitem>
          <asp:listitem value="OH">OH</asp:listitem>
          <asp:listitem value="OK">OK</asp:listitem>
          <asp:listitem value="OR">OR</asp:listitem>
          <asp:listitem value="PA">PA</asp:listitem>
          <asp:listitem value="PR">PR</asp:listitem>
          <asp:listitem value="PW">PW</asp:listitem>
          <asp:listitem value="RI">RI</asp:listitem>
          <asp:listitem value="SC">SC</asp:listitem>
          <asp:listitem value="SD">SD</asp:listitem>
          <asp:listitem value="TN">TN</asp:listitem>
          <asp:listitem value="TX">TX</asp:listitem>
          <asp:listitem value="UT">UT</asp:listitem>
          <asp:listitem value="VA">VA</asp:listitem>
          <asp:listitem value="VI">VI</asp:listitem>
          <asp:listitem value="VT">VT</asp:listitem>
          <asp:listitem value="WA">WA</asp:listitem>
          <asp:listitem value="WI">WI</asp:listitem>
          <asp:listitem value="WV">WV</asp:listitem>
          <asp:listitem value="WY">WY</asp:listitem>
        </asp:dropdownlist>
        <span style="color: Red; font-size: large"> *</span>
        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                            ControlToValidate="ddlState" Display="Static" 
                            ErrorMessage=" Select State" SetFocusOnError="True" 
                            ValidationGroup="user" CssClass="error"></asp:RequiredFieldValidator>--%>
        <asp:requiredfieldvalidator id="rfvstate" runat="server" controltovalidate="ddlState" enableclientscript="true"
          text="!" errormessage="Select State." validationgroup="user">
        </asp:requiredfieldvalidator>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ZipC")%></label>
        <asp:textbox id="txtcontactzip" runat="server" cssclass="txtBox" MaxLength="5">
        </asp:textbox><span style="color: Red; font-size: large"> *</span>
        <asp:requiredfieldvalidator id="rfvZipCode" runat="server" controltovalidate="txtcontactzip" enableclientscript="true"
          text="!" errormessage="Zip Code is required." display="Dynamic" validationgroup="user">
        </asp:requiredfieldvalidator>
        <asp:regularexpressionvalidator runat="server" id="rfvtxtZip" controltovalidate="txtcontactzip" validationexpression="^[0-9]{5}$"
          errormessage="Enter  valid Zip Code must be 5 numeric digits" display="Dynamic" text="!" validationgroup="user" />
        <span id="divZipError" runat="server" class="error"></span>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PP#")%></label>
        <asp:textbox id="txtcontacttelephone" runat="server" onblur="ReSetPhone(this)" cssclass="txtBox">
        </asp:textbox>
        <span style="color: Red; font-size: large"> *</span>
        <%--<asp:RequiredFieldValidator ID="rfvPphone" runat="server" ControlToValidate="txtcontacttelephone"
                    EnableClientScript="true" SetFocusOnError="true" ErrorMessage="Your Primary Phone Number Is Required."
                     Display="Static" ValidationGroup="user" CssClass="error"></asp:RequiredFieldValidator>
                     --%>
        <asp:requiredfieldvalidator id="rfvcontactphone" runat="server" controltovalidate="txtcontacttelephone"
          enableclientscript="true" setfocusonerror="true" text="!" errormessage="Your Primary Phone Number Is Required."
          display="Dynamic" validationgroup="user">
        </asp:requiredfieldvalidator>
        <asp:customvalidator id="cfvPphone" runat="server" errormessage="Invalid Phone Number Number." clientvalidationfunction="CheckprimaryPhone"
          validationgroup="user" controltovalidate="txtcontacttelephone" display="Dynamic" text="!">
        </asp:customvalidator>
        <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Invalid Phone Number Number."
                    ClientValidationFunction="CheckprimaryPhone" ControlToValidate="txtcontacttelephone" 
                    Display="Static" > 
                </asp:CustomValidator>--%>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|EADD")%></label>
        <asp:textbox id="txtrealemail" runat="server" cssclass="txtBox">
        </asp:textbox><span style="color: Red; font-size: large"> *</span>
        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" text="!" errormessage="Email Address Required"
          controltovalidate="txtrealemail" display="Dynamic" setfocusonerror="True" validationgroup="user">
        </asp:requiredfieldvalidator>
        <asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" errormessage="Invalid Email Address"
          controltovalidate="txtrealemail" display="Dynamic" setfocusonerror="True" validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
          validationgroup="user">*</asp:regularexpressionvalidator>
      </div>
    </div>
  </div>
  <div class="colformrht">
    <div class="dvform">
      <div class="dvrow">
        <p class="col_title">
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SPerson")%></p>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SPersonF")%></label>
        <asp:textbox id="txtcosignfirstname" runat="server" cssclass="txtBox">
        </asp:textbox>
      </div>
      <div class="dvrow">
        <label>
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SPersonL")%></label>
        <asp:textbox id="txtcosignlastname" runat="server" cssclass="txtBox">
        </asp:textbox>
      </div>
    </div>
  </div>
  <div class="clearboth">
  </div>
</div>
<table class="rdolstsnew">
  <tr>
    <td>
      <asp:radiobutton id="rboApplyYes" runat="server" groupname="Waver" />
      <label for="ApplyYes" class="padT3">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IIn")%>
      </label>
    </td>
  </tr>
  <tr>
    <td>
      <table class="mgrL20" width="100%">
        <tr>
          <td>
            <asp:checkbox id="chkUnderstand" runat="server" />
            <label for="Understand" class="padT3">
              <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IUThat")%>
            </label>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <asp:radiobutton id="rboApplyNo" runat="server" groupname="Waver" />
      <label for="ApplyNo" class="padT3">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IWish")%>
      </label>
    </td>
  </tr>
</table>
<div class="DvErrorSummary" id="dvBottomServerSideErrorMessage" runat="server" visible="false">
</div>
<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
</div>
<div class="dvbtncontainer">
  <div class="lnkbutton">
    <asp:linkbutton id="btnReturnToPrevious" cssclass="previous" runat="server" onclick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:linkbutton>
  </div>
  <div class="lnkbutton">
    <asp:linkbutton id="btnContinue" validationgroup="user" runat="server" onclick="Continue_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:linkbutton>
  </div>
  <asp:HiddenField ID="hdnSSN" runat="server" />
  <asp:HiddenField ID="hdnISNewUser" runat="server" value="N" />
</div>
</asp:Content>