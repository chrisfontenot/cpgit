﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class CommonOptionsFinancialCrisis : ContactDetailPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            UcAuthCode.ChatCode=ChatCodeOption.Session3;
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Summary);
				CommonFunction.UserProgressSave(PercentComplete.COMMAN_OPTIONS_CRISIS, SessionState.Username);

				btnReturntoPreviousPage.ToolTip = App.Translate("Credability|RVMButtons|Previous");
				btnContinue.ToolTip = App.Translate("Credability|RVMButtons|Continue");
				btnReturntoPrevious.ToolTip = App.Translate("Credability|RVMButtons|SaveContinueLater");
				DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
			}
		}

		protected void btnReturntoPreviousPage_Click(object sender, EventArgs e)
		{
			Response.Redirect("UserFinancialSituationAnalysis.aspx");
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				Response.Redirect("Final.aspx");
			}
		}

		protected void btnReturntoPrevious_Click(object sender, EventArgs e)
		{
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
	}
}