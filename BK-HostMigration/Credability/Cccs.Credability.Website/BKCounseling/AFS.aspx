﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AFS.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.AFS" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility AFS BKCounseling" ValidateRequest="false" %>

<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
	</div>
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|AFS")%></h1>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|TYFC")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|BWCC")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|FAPV")%>
		<a href="../BKCounseling/Term.aspx" class="NormLink">
			<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|CLH")%></a></p>
	<textarea readonly class="TxtBox" cols="150" rows="15">
<%=Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|S1964")%>
</textarea>
	<div class="dvform2col">
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|BEYM")%>
		</p>
		<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="BchAfs" />
		<asp:Label ID="lblSecMsg" runat="server" Text="With a second person, their mother's maiden name is also required." Visible="False" Font-Bold="True" ForeColor="#CC3300"></asp:Label>
		<div class="colformlft">
			<div class="dvform">
				<div class="dvrow" style="padding: 0 0 0 150px">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|PP")%></p>
				</div>
				<div class="dvrow">
					<label>
						&nbsp;</label>
					<asp:TextBox ID="txtPrimaryPerson" runat="server" MaxLength="50" OnTextChanged="txtPrimaryPerson_TextChanged"></asp:TextBox>
					<asp:RequiredFieldValidator ID="rfvPrimaryPerson" runat="server" EnableClientScript="true" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPrimaryPerson" Text="!" ValidationGroup="BchAfs"></asp:RequiredFieldValidator>
				</div>
			</div>
		</div>
		<div class="colformrht">
			<div class="dvform">
				<div class="dvrow" style="padding: 0 0 0 150px">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|SP")%>
					</p>
				</div>
				<div class="dvrow">
					<label>
						&nbsp;</label>
					<asp:TextBox ID="txtSecondPerson" runat="server" MaxLength="50"></asp:TextBox>
				</div>
			</div>
		</div>
		<div class="clearboth">
		</div>
	</div>
	<div class="DvErrorSummary" id="dvErrorSumary" runat="server" visible="false">
	</div>
	<div class="dvbtncontainer">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnAfsContinue" runat="server" OnClick="btnAfsContinue_Click" ToolTip="Continue" ValidationGroup="BchAfs"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
		</div>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
   <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>
