﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
   public partial class PrePay : ContactDetailPage
   {
      private void ShowPageTitle()
      {
         var lblHeading = Page.Master.FindControl("lblHeading") as Label;
         if (lblHeading != null)
            lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|PrePay|BWGS");
      }

      protected void Page_Load(object sender, EventArgs e)
      {
         ShowPageTitle();

         if (!IsPostBack)
         {
            SessionState.FwID = FeeWaiverBKCounselingGetByClientNumber(SessionState.ClientNumber.Value);
            SessionState.DelqType = ContactDetailsGetByClientNumber(SessionState.ClientNumber.Value);
            if (Request["url"] != null)
            {
               if (Request["url"] == "escrow")
               {
                  dvEscrow1.Visible = true;
                  dvEscrow2.Visible = true;
               }
            }
         }

      }

      public int FeeWaiverBKCounselingGetByClientNumber(Int32 ClientNumber)
      {
         var feeWaiver = App.FeeWaiver.GetByClientNumber(ClientNumber);
         return feeWaiver.FwID;
      }

      public String ContactDetailsGetByClientNumber(Int32 ClientNumber)
      {
         String DelqType = "";

         UserContactDetailsBKC UserContactDetailBKC = null;
         UserContactDetailBKC = App.Credability.BKCUserInfoGet(ClientNumber);

         if (UserContactDetailBKC != null)
         {
            if (UserContactDetailBKC.ClientType == "BK" && UserContactDetailBKC.MortCurrent == 0)
            {
               if (UserContactDetailBKC.HousingType == 'B') DelqType = "Mort";
               else if (UserContactDetailBKC.HousingType == 'R') DelqType = "Rent";
               else DelqType = "";
            }
         }
         return DelqType;

      }

      public RegistrationBKCounselingResult SaveRegistrationforNoOption()
      {
         RegistrationBKCounseling registrationBKCounseling = new RegistrationBKCounseling();
         RegistrationBKCounselingResult registrationBKCounselingResult = null;

         registrationBKCounseling.ClientNumber = Convert.ToInt32(SessionState.ClientNumber);
         registrationBKCounseling.ATTYname = "";
         registrationBKCounseling.ATTYemail = "";
         registrationBKCounseling.FIRMid = "";
         registrationBKCounseling.FIRMname = "";
         registrationBKCounseling.ATTYSit = "NoAt";


         registrationBKCounselingResult = App.Credability.RegistrationBKCounselingAddUpdate(registrationBKCounseling);
         return registrationBKCounselingResult;
      }

      protected void btnYes_Click(object sender, EventArgs e)
      {
         //SessionState.PrimaryZipCode = zipCode.Text;
         //SessionState.FirmID = firmCode.Text;
         SessionState.isRegistration = true;

         Response.Redirect("Registration.aspx");
      }


      //down vote



      //Try like below...It will work...

      //HTML
      //<div id="divID" runat="server"></div>
      //<asp:Button ID="btnClick" runat="server" OnClientClick="sam();" Text="Get Div Value" />
      //C#
      //public String Test = "Hello World !";
      //In Page Load SET this Value to DIV
      //divID.InnerHtml = Test; 
      protected void btnNo_Click(object sender, EventArgs e)
      {
         SessionState.isRegistration = false;
         SessionState.Escrow = "N"; // This Session is used to Validate the Attorney.
         RegistrationBKCounselingResult RegistrationBKCounselingResult = SaveRegistrationforNoOption();
         if (RegistrationBKCounselingResult.IsSuccessful == true)
         {
            Response.Redirect("Registration.aspx");
            //Response.Redirect("WaiverPath.aspx");
         }
         else
         {
            dvServerSideValidation.InnerHtml = RegistrationBKCounselingResult.Exception.ToString();
         }
      }

      protected void firmCode_TextChanged(object sender, EventArgs e)
      {

      }

      protected void zipCode_TextChanged(object sender, EventArgs e)
      {

      }
   }
}