﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class ListingYourDebts : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Debt);
				CommonFunction.UserProgressSave(PercentComplete.USER_DEBT_LISTING, SessionState.Username);
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl PageHead = UcDebtList.FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PageHead != null)
			{
				PageHead.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcDebtList.FindControl("TopTextGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl BottomText = UcDebtList.FindControl("BottomTextGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BottomText != null)
			{
				BottomText.Visible = true;
			}
		}
	}
}
