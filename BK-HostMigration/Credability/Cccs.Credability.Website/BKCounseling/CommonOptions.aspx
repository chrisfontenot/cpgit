﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptions.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.CommonOptions"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility CommonOptions BKCounseling"%>
<%@ Register Src="~/Controls/Shared/Pages/UcPostPurchace.ascx" TagPrefix="Uc" TagName="UCCommonOptions" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCCommonOptions id="UcUCCommonOptions" runat="server" RedirectOnContinue="ListingYourDebts.aspx?from=CO" RedirectOnPrevious="DescribingYourSituation.aspx"></Uc:UCCommonOptions>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>