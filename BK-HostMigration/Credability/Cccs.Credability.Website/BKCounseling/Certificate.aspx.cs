﻿using System;
using Cccs.Credability.Certificates.PreFiling;
using StructureMap;
using Cccs.Credability.Certificates;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class Certificate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var isPrimary = Convert.ToBoolean(Request.QueryString["IsPrimary"]);
            var isActionPlan = Convert.ToBoolean(Request.QueryString["IsActionPlan"]);

            if (isActionPlan)
            {
                var primaryDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, true);
                var secondaryDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, false);
                GenerateActionPlanPdf(primaryDetail, secondaryDetail);
            }
            // NOTE: Remove Certificate Generation Temporarily
            //else
            //{
            //    var userDetail = App.Identity.UserDetailGet(SessionState.UserId.Value, isPrimary);
            //    GenerateCertificatePdf(userDetail);
            //}
        }
        
        private void GenerateActionPlanPdf(Identity.UserDetail primaryUserDetail, Identity.UserDetail secondaryUserDetail)
        {
            var counselingCertificate = ObjectFactory.GetInstance<IPreFilingCertificateService>();
            var primaryData = counselingCertificate.GetCurrentSnapshotForUserDetail(primaryUserDetail.UserDetailId);
            var secondaryData = secondaryUserDetail != null ? counselingCertificate.GetCurrentSnapshotForUserDetail(secondaryUserDetail.UserDetailId) : null;

            var assembly = typeof(IPreFilingCertificateService).Assembly;
            var generatorType = assembly.GetType(primaryData.CertificateVersionType);

            var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
            var stream = generator.CreateActionPlan(primaryData, secondaryData);

            Response.Expires = 0;
            Response.Buffer = true;
            Response.ClearHeaders();
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("inline; filename={0}-ActionPlan.pdf", primaryData.CertificateNumber));
            Response.ContentType = "application/pdf";

            var buffer = stream.GetBuffer();
            Response.BinaryWrite(buffer);
            stream.Close();
            Response.Flush();
            Response.End();
        }

        private void GenerateCertificatePdf(Identity.UserDetail userDetail)
        {
            var counselingCertificate = ObjectFactory.GetInstance<IPreFilingCertificateService>();
            var data = counselingCertificate.GetCurrentSnapshotForUserDetail(userDetail.UserDetailId);

            var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
            var generatorType = assembly.GetType(data.CertificateVersionType);

            var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
            var stream = generator.CreateCertificate(data);

            Response.Expires = 0;
            Response.Buffer = true;
            Response.ClearHeaders();
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("inline; filename={0}.pdf", data.CertificateNumber));
            Response.ContentType = "application/pdf";

            var buffer = stream.GetBuffer();
            Response.BinaryWrite(buffer);
            stream.Close();
            Response.Flush();
            Response.End();
        }
    }
}