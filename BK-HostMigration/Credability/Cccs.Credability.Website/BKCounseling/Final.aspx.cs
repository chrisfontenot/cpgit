﻿using System;
using System.Web;
using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Credability.Certificates.PreFiling.Generators;
using Cccs.Credability.Website.Controls.BKCounselingControls;
using StructureMap;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling.Queues;
using Cccs.Identity;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class Final : ContactDetailPage
	{
		private readonly ILoggingService logger;
		private readonly IPreFilingCertificateService preFilingCertificateService;
		public Final()
		{
			logger = ObjectFactory.GetInstance<ILoggingService>();
			preFilingCertificateService = ObjectFactory.GetInstance<IPreFilingCertificateService>();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
            if (String.IsNullOrEmpty(SessionState.FirmID)
                || String.IsNullOrEmpty(SessionState.PrimaryZipCode)) 
            {
                var myUrl = HttpContext.Current.Request.Url.AbsolutePath;
                Response.Redirect(ResolveUrl("~/GetRequiredFeeInformation.aspx?redirect=" + myUrl));
            }

			btnReturntoPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Summary);
				CommonFunction.UserProgressSave(PercentComplete.COMPLETE, SessionState.Username);
			}
		}

		protected void btnReturntoPreviousPage_Click(object sender, EventArgs e)
		{
			Response.Redirect("CommonOptionsFinancialCrisis.aspx");
		}

		protected void btnSendToCounselor_Click(object sender, EventArgs e)
		{
			var contactDetail = App.Credability.FinalContactdetailBKCounselingGet(SessionState.ClientNumber.Value);
			if(contactDetail != null)
			{
				var finalContactDetail = new FinalContactdetailBKCounseling();
				finalContactDetail.ClientNumber = SessionState.ClientNumber.Value;
				finalContactDetail.Confirm_ipaddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
				finalContactDetail.ClientType = "BK";
				finalContactDetail.Span_flag = SessionState.LanguageCode == Cccs.Translation.Language.ES ? "y" : String.Empty;
				finalContactDetail.Confirm_datetime = DateTime.Now;

				var finalContactDetailResult = App.Credability.FinalContactdetailBKCounselingUpadte(finalContactDetail);
				if(finalContactDetailResult.IsSuccessful == true)
				{
					HttpRequest currentRequest = HttpContext.Current.Request;
					string ipAddress = currentRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];
					if(ipAddress == null || ipAddress.ToLower() == "unknown")
						ipAddress = currentRequest.ServerVariables["REMOTE_ADDR"];
					UpdateTimeSpent(SessionState.ClientNumber.Value, ipAddress, UserPageArrival.Value);
					CommonFunction.SendToCounselor(true);

					try
					{
						var generator = new CounselingGenerator4();
						GenerateCertificates(SessionState.UserId.Value, SessionState.LanguageCode, generator);
					}
					catch(Exception ex)
					{
						logger.Error(String.Format("Error generating certificate for UserId {0}", SessionState.UserId.Value), ex);

						// Allow the user to continue. Error displayed on next page.
					}

                    var snapshot = App.Identity.GetCurrentSnapshot(SessionState.UserId.Value);
                    var certificateNumber = string.Empty;

                    if (snapshot != null)
                    {
                        certificateNumber = snapshot.CertificateNumber;
                    }

                    string waiverType = string.Empty;
                    var fm = new FeeWaiverManager();

                    var feeWaiver = fm.GetByClientNumber((int)SessionState.ClientNumber);

                    if (feeWaiver != null)
                    {
                        waiverType = feeWaiver.WaiverType;

                        logger.Debug(() => "Fee Waiver Type" + waiverType);
                    }
                    else
                    {
                        logger.Debug(() => "No Fee Waiver found");
                    }

                    //if (!string.IsNullOrEmpty(SessionState.FirmID))
                    //{
                    //    var result = App.Debtplus.GetEscrow(Convert.ToInt32(SessionState.FirmID), App.WebsiteCode);

                    //    if(result != null)
                    //    {
                    //        SessionState.EscrowProbonoValue = result.EscrowProBono;
                    //        SessionState.Escrow = result.Escrow;

                    //        if (result.IsLegalAid)
                    //        {
                    //            SessionState.Escrow6 = "Y";
                    //        }
                    //    }
                    //}

                    try
                    {
                        string counselorEmail = string.Empty;
                        CounselorResult counselor = App.Identity.GetCounselorByID(SessionState.CounselorID.Value);

                        if (counselor != null)
                        {
                            counselorEmail = counselor.CounselorEmail;
                        }

                        CommonFunction.PushClient(certificateNumber, waiverType, SessionState.Escrow, SessionState.EscrowProbonoValue, counselorEmail);
                    }
                    catch (Exception ex)
                    {
                        logger.Debug(() => "Error Occured in SendToCounselor. Message: " + ex.Message);
                        logger.Debug(() => "Stack Trace: " + ex.StackTrace.ToString());

                        if (ex.InnerException != null)
                        {
                            var iex = ex.InnerException;
                            logger.Debug(() => "Error Occured in SendToCounselor. Inner Exception Message: " + iex.Message);
                            logger.Debug(() => "Stack Trace: " + iex.StackTrace.ToString());
                        }
                    }

					Response.Redirect("Done.aspx");
				}
				else
				{
					dvErrorMessage.InnerHtml = finalContactDetailResult.Exception.ToString();  //"Some error occured during the operation please try later.";
				}
			}
		}

		private void PerformMaintenance(object stateInfo)
		{
			try
			{
				preFilingCertificateService.ArchiveCertificates();
				preFilingCertificateService.FillCertificates();
			}
			catch(Exception ex)
			{
				logger.Fatal("Error archiving certificates.", ex);
			}
		}

		private void GenerateCertificates(long userId, string languageCode, IPreFilingCertificateGenerator generator)
		{
			logger.Info(() => String.Format("Generating Certificate(s) for UserId: '{0}'", userId));

			var addressList = App.Identity.AddressesGet(userId);
			var address = addressList[0];
			string judicialDistrict = "";
			string confirmDateTime = "";
			string chatCounselorName = "";

			var primaryUser = App.Identity.UserDetailGet(userId, true);
			var contactDetail = App.Credability.FinalContactdetailBKCounselingGet(SessionState.ClientNumber.Value);
			var certificateVersionType = generator.GetType().ToString();
            // Bankruptcy Conversion
            //var creditScore = App.Host.GetCredScore(SessionState.ClientNumber.Value);
            var creditScore = App.Debtplus.GetCredScore(SessionState.ClientNumber.Value);

			CounselorResult counselor = App.Identity.GetCounselorByID(SessionState.CounselorID.Value);
            confirmDateTime = contactDetail.Confirm_datetime.ToString();

			var primaryCertificateGenerated = false;
			if(!preFilingCertificateService.HasCurrentSnapshot(primaryUser.UserDetailId))
			{
				logger.Info(() => String.Format("Generating Primary Snapshot for UserDetailId: '{0}'", primaryUser.UserDetailId));
                // Bankruptcy Conversion
                //preFilingCertificateService.SnapshotData(primaryUser.UserDetailId, creditScore.Value.PrimaryScore, certificateVersionType);
                preFilingCertificateService.SnapshotData(primaryUser.UserDetailId, creditScore.PrimaryScore, certificateVersionType);
            }
			if(!preFilingCertificateService.HasDownloadedCertificate(primaryUser.UserDetailId))
			{
				logger.Info(() => String.Format("Generating Primary Certificate for UserDetailId: '{0}'", primaryUser.UserDetailId));
				var snapshot = preFilingCertificateService.GetCurrentSnapshotForUserDetail(primaryUser.UserDetailId);
				preFilingCertificateService.AssignCertificateToSnapshot(snapshot.PreFilingSnapshotId);
				primaryCertificateGenerated = true;
				chatCounselorName = (counselor != null) ? counselor.CounselorName : "";
				judicialDistrict = snapshot.JudicialDistrict;
			}

			var secondaryCertificateGenerated = false;
			var secondaryUser = App.Identity.UserDetailGet(userId, false);
			if(secondaryUser != null && !preFilingCertificateService.HasCurrentSnapshot(secondaryUser.UserDetailId))
			{
				logger.Info(() => String.Format("Generating Secondary Snapshot for UserDetailId: '{0}'", secondaryUser.UserDetailId));
                // Bankruptcy Conversion
                //preFilingCertificateService.SnapshotData(secondaryUser.UserDetailId, creditScore.Value.SecondaryScore, certificateVersionType);
                preFilingCertificateService.SnapshotData(secondaryUser.UserDetailId, creditScore.SecondaryScore, certificateVersionType);
            }
			if(secondaryUser != null && !preFilingCertificateService.HasDownloadedCertificate(secondaryUser.UserDetailId))
			{
				logger.Info(() => String.Format("Generating Secondary Certificate for UserDetailId: '{0}'", primaryUser.UserDetailId));
				var snapshot = preFilingCertificateService.GetCurrentSnapshotForUserDetail(secondaryUser.UserDetailId);
				preFilingCertificateService.AssignCertificateToSnapshot(snapshot.PreFilingSnapshotId);
				secondaryCertificateGenerated = true;
			}
            if (CertificatesGenerated(primaryCertificateGenerated, secondaryCertificateGenerated))
            {
                long preFilingActionPlanId = GenerateActionPlan(primaryUser, secondaryUser);
                EmailCertificates(preFilingActionPlanId, primaryUser, judicialDistrict, counselor, confirmDateTime);
            }
        }

		private bool CertificatesGenerated(bool primaryCertificateGenerated, bool secondaryCertificateGenerated)
		{
			return primaryCertificateGenerated || secondaryCertificateGenerated;
		}

		private long GenerateActionPlan(Identity.UserDetail primaryUser, Identity.UserDetail secondaryUser)
		{
			var primarySnapshot = preFilingCertificateService.GetCurrentSnapshotForUserDetail(primaryUser.UserDetailId);

			var secondarySnapshot = null as PreFilingSnapshotDto;
			if(secondaryUser != null)
			{
				secondarySnapshot = preFilingCertificateService.GetCurrentSnapshotForUserDetail(secondaryUser.UserDetailId);
			}

			var dto = preFilingCertificateService.CreateActionPlan(primarySnapshot, secondarySnapshot);
			return dto.PreFilingActionPlanId;
		}

		private void EmailCertificates(long preFilingActionPlanId, Identity.UserDetail primaryUser, string judicialDistrict, CounselorResult counselor, string confirmDateTime) //string languageCode, Identity.UserDetail primaryUser, Identity.UserDetail secondaryUser)
		{
			var registrationInfo = App.Credability.RegistrationBKCounselingGet(SessionState.ClientNumber.Value);
			/*var attorneyEmail = registrationInfo.ATTYemail.TrimOrNull();
			if(attorneyEmail.IsNullOrWhiteSpace())
			{
				var firmCode = registrationInfo.FIRMid.TrimOrNull();
				if(!firmCode.IsNullOrWhiteSpace())
				{
                    // Bankruptcy Conversion
                    //attorneyEmail = App.Host.GetFirmEmail(firmCode).TrimOrNull();
                    attorneyEmail = App.Debtplus.GetFirmEmail(firmCode).TrimOrNull();
                }
			}*/

            string counselorName = (counselor != null) ? counselor.CounselorName : "";

			SendClientActionPlan(preFilingActionPlanId, primaryUser.Email, judicialDistrict, counselorName, confirmDateTime);
            SendCounselorActionPlan(preFilingActionPlanId, counselor, judicialDistrict, confirmDateTime);
            //SendAttorneyCertificate(preFilingActionPlanId, attorneyEmail, judicialDistrict, counselorName, confirmDateTime);
        }

        private void SendCounselorActionPlan(long preFilingActionPlanId, CounselorResult counselor, string judicialDistrict, string confirmDateTime)
		{
            if (counselor == null || string.IsNullOrEmpty(counselor.CounselorEmail))
            {
                return;
            }

			try
			{
				using(CredTalkWebServices.CredTalkWebServicesClient webClient = new CredTalkWebServices.CredTalkWebServicesClient())
				{
					CounselorActionPlanQueueMessage message = new CounselorActionPlanQueueMessage
					{
						PreFilingActionPlanId = preFilingActionPlanId,
						EmailAddress = counselor.CounselorEmail,
						JudicialDistrict = judicialDistrict,
						ChatCounselorName = counselor.CounselorName,
						ConfirmDateTime = confirmDateTime
					};

					CredTalkWebServices.StatusResponse statusResponse = webClient.SubmitCounselorActionPlanQueueMessage(message);
					if(!statusResponse.IsSuccessful)
					{
						logger.Info(() => statusResponse.ErrorMessages[0]);
					}
				}
			}
			catch(Exception ex)
			{
				logger.Fatal("Error sending Counselor action plan", ex);
                logger.Debug(() => "Message: " + ex.Message + ", Stack Trace: " + ex.StackTrace.ToString());
                if (ex.InnerException != null)
                {
                    logger.Debug(() => "Inner Exception Message: " + ex.InnerException.Message + ", Stack Trace: " + ex.InnerException.StackTrace.ToString());
                }
            }
		}

        private void SendClientActionPlan(long preFilingActionPlanId, string clientEmail, string judicialDistrict, string chatCounselorName, string confirmDateTime)
        {
            if (clientEmail.IsNullOrWhiteSpace())
                return;

            try
            {
                using (CredTalkWebServices.CredTalkWebServicesClient webClient = new CredTalkWebServices.CredTalkWebServicesClient())
                {
                    ClientActionPlanQueueMessage message = new ClientActionPlanQueueMessage
                    {
                        PreFilingActionPlanId = preFilingActionPlanId,
                        EmailAddress = clientEmail,
                        JudicialDistrict = judicialDistrict,
                        ChatCounselorName = chatCounselorName,
                        ConfirmDateTime = confirmDateTime
                    };

                    CredTalkWebServices.StatusResponse statusResponse = webClient.SubmitClientActionPlanQueueMessage(message);
                    if (!statusResponse.IsSuccessful)
                    {
                        logger.Info(() => statusResponse.ErrorMessages[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Fatal("Error sending client action plan", ex);
                logger.Debug(() => "Message: " + ex.Message + ", Stack Trace: " + ex.StackTrace.ToString());
                if (ex.InnerException != null)
                {
                    logger.Debug(() => "Inner Exception Message: " + ex.InnerException.Message + ", Stack Trace: " + ex.InnerException.StackTrace.ToString());
                }
            }
        }

        private void SendAttorneyCertificate(long preFilingActionPlanId, string attorneyEmail, string judicialDistrict, string chatCounselorName, string confirmDateTime)
		{
			if(attorneyEmail.IsNullOrWhiteSpace())
				return;

			try
			{
				using(CredTalkWebServices.CredTalkWebServicesClient webClient = new CredTalkWebServices.CredTalkWebServicesClient())
				{
					AttorneyCertificateQueueMessage attorneyMessage = new AttorneyCertificateQueueMessage
					{
						PreFilingActionPlanId = preFilingActionPlanId,
						EmailAddress = attorneyEmail,
						JudicialDistrict = judicialDistrict,
						ChatCounselorName = chatCounselorName,
						ConfirmDateTime = confirmDateTime
					};

					CredTalkWebServices.StatusResponse statusResponse = webClient.SubmitAttorneyCertificateQueueMessage(attorneyMessage);

					if(!statusResponse.IsSuccessful)
					{
						logger.Info(() => statusResponse.ErrorMessages[0]);
					}
				}
			}
			catch(Exception ex)
			{
				logger.Fatal("Error sending attorney certificate", ex);
			}
		}
	}
}