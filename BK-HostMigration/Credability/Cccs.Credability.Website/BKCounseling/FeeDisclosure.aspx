﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeDisclosure.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.FeeDisclosure" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility FeeDisclosure BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TYFC")%></p>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BWC")%></p>
<%if (!IsBKED)
   
     { %> <p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|ARC")%></p><%} %>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FDS")%></p>
<div>
    <ul>
        <li>
            <%= ((SessionState.BKFee.CompareTo(50.00) == 0)? Cccs.Credability.Website.App.Translate("Credability|BKC|OFI5"):
                  string.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|OFI4"), SessionState.BKFee,
                                50.00 - SessionState.BKFee))%></li>
        <li>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SFCCCS")%></li>
        <li>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|OCA")%>
        </li>
    </ul>
</div>
<p>
    &nbsp;</p>
<div id="dvErrorCont" runat="server" style="display: none" class="ErrorCont">
<div id="dvError" runat="server">
</div>
</div>
<div class="dvform2col">
    <div class="dvform">
    <div id="dvErrorMessage" runat="server" style="color:Red;font-weight:bold;text-align:center;"></div>
        <table class="DvradiolistAuto">
            <tr>
                <td style="width:600px">
                    <asp:CheckBox ID="chkUnderstand" runat="server" />
                    <label for="<%=chkUnderstand.ClientID %>" style="width:600px">
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHR")%>
                       <em style="color:#FF0000;font-size:large; font-style:normal;">*</em>
                    </label> 
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div class="clearboth">
    </div>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnbkFeeDisclosureContinue" runat="server" OnClick="click_btnbkFeeDisclosureContinue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton></div>
</div>
</asp:Content>

