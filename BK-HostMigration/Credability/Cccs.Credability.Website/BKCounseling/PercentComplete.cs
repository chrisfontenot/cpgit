﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website.BKCounseling
{
    public class PercentComplete
    {
        public const byte AFS = 5;
        public const byte USER_PROFILE = 10;
        public const byte DESCRIBE_YOUR_SITUATION = 30;
        public const byte COMMAN_OPTIONS = 40;
        public const byte USER_DEBT_LISTING = 55;
        public const byte USER_INCOME_DOCUMENTATION = 60;
        public const byte MONTHLY_EXPENSES = 65;
        public const byte UNDERSTANDING_YOUR_NET_WORTH = 70;
        public const byte ANALYZING_YOUR_FINANCIAL_SITUATION = 85;
        public const byte COMMAN_OPTIONS_CRISIS = 90;
        public const byte DONE = 95;
        public const byte COMPLETE = 100;
    }
}
