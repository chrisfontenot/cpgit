﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribeYourSituationRentalInfo.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.DescribeYourSituationRentalInfo" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Describe Your Situation RentalInfo BKCounseling" %>

<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|RF")%></h1>
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" runat="server" ValidationGroup="userprofile" />
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|RDH")%>
	</p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYF")%>
	</p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HLD")%>
	</p>
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HFCCCS")%>
	</p>
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|COIYAR")%>
	</p>
	<ul>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TTYL")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TLH")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|OTR")%>
			<a href="http://www.dca.state.ga.us/" target="_blank" class="NormLink">http://www.dca.state.ga.us/</a><br />
			<a href="http://www.indolink.com/Law/rental.html" target="_blank" class="NormLink">http://www.indolink.com/Law/rental.html</a>
		</li>
	</ul>
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYA")%>
	</p>
	<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
	<div class="dvbtncontainer" style="margin: 1px">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnReturntoPreviousPage" runat="server" CssClass="previous" OnClick="btnReturntoPreviousPage_Click" CausesValidation="false"><span>Return To Previous Page</span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnSaveandExit" runat="server" OnClick="btnSaveandExit_Click"><span>Save & Exit</span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ValidationGroup="userprofile"><span>Continue</span></asp:LinkButton>
		</div>
	</div>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>
