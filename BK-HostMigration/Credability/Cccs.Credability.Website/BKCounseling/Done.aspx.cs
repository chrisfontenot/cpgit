﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Credability.Website.Resources;
using StructureMap;
using Cccs.Credability.Certificates;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class Done : System.Web.UI.Page
    {
        private readonly IPreFilingCertificateService preFilingCertificateService;
        public Done()
        {
            preFilingCertificateService = ObjectFactory.GetInstance<IPreFilingCertificateService>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Summary);
                CommonFunction.UserProgressSave(PercentComplete.DONE, SessionState.Username);

                FinalContactDetailsGet(SessionState.ClientNumber.Value);
            }
        }

        private void FinalContactDetailsGet(Int32 ClientNumber)
        {
            var FinalContactdetailBKCounseling = App.Credability.FinalContactdetailBKCounselingGet(ClientNumber);
            if (FinalContactdetailBKCounseling == null)
                return;

            var primaryUser = App.Identity.UserDetailGet(SessionState.UserId.Value, true);
            // NOTE: Remove Certificate Generation TemporarilyGeneration
            //if (!preFilingCertificateService.HasDownloadedCertificate(primaryUser.UserDetailId))
            //{
            //    PrimaryCertificateAnchor.Visible = false;
            //    PrimaryActionPlanAnchor.Visible = false;

            //    dvErrorMessage.InnerHtml = "<br />" + ExceptionMessages.CertificateDownloadException.FormatWith(CommonFunction.CreateChatLink(LinkText.Here));
            //}
            //else
            //{
            //    var primaryName = String.Format("{0}{1}{2}",
            //        FinalContactdetailBKCounseling.Contact_firstname,
            //        " " + FinalContactdetailBKCounseling.Contact_initial ?? String.Empty,
            //        FinalContactdetailBKCounseling.Contact_lastname);

            //    PrimaryCertificateAnchor.InnerText = Resources.BKCounseling.DownloadCertificateFor.FormatWith(primaryName);
            //    PrimaryActionPlanAnchor.InnerText = Resources.BKCounseling.DownloadActionPlanFor;

            //    if (String.IsNullOrEmpty(FinalContactdetailBKCounseling.Cosign_firstname))
            //        return;

            //    var secondaryName = String.Format("{0}{1}{2}",
            //            FinalContactdetailBKCounseling.Cosign_firstname,
            //            " " + FinalContactdetailBKCounseling.Cosign_initial ?? String.Empty,
            //            FinalContactdetailBKCounseling.Cosign_lastname);

            //    SecondaryCertificateAnchor.Visible = true;
            //    SecondaryCertificateAnchor.InnerText = Resources.BKCounseling.DownloadCertificateFor.FormatWith(secondaryName);

            PrimaryCertificateAnchor.Visible = false;
            PrimaryActionPlanAnchor.Visible = false;
        }

        protected void RTCCCS_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
        }
    }
}