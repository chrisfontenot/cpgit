﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvideYourPersonalInformation.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.UserProfile"
  Title="CredAbility BKC Provide Your Personal Information" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UserProfile.ascx" TagName="UCProvideYourPersonalInformation" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <uc1:UCProvideYourPersonalInformation ID="UCProvideYourPersonalInformation1" runat="server" RedirectOnContinue="DescribingYourSituation.aspx" RedirectOnNeedAuthorization="WhatYouNeed.aspx?from=UP" IsEmailVisible="true"/>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>