﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayWarning.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.PayWarning" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility PayWarning BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<div class="paywarning" style="font-size:15px !important">
    <asp:Label ID="lblWarningMsg" runat="server" Text=""></asp:Label>
    <%--<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TADD")%>--%>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnRegistration" runat="server" OnClick="btnRegistration_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
</asp:Content>

