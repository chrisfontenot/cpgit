﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Final.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.Final" MasterPageFile="~/MasterPages/Master.Master" Title ="CredAbility Final BKC" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<div id="dvErrorMessage" runat="server" class="error"></div>
<p class="col_title"><%= Cccs.Credability.Website.Resources.BKCounseling.YouAreAlmostFinished %></p>

<p><%= Cccs.Credability.Website.Resources.BKCounseling.ByClickingTheSendToCounselorButton %></p>
<p><%= Cccs.Credability.Website.Resources.BKCounseling.TheActionPlanWillContain %></p>

 <div class="ButtonParaGraph">
 
    
</div>
<div class="dvbtncontainer">
 <div class="lnkbutton">
   <asp:LinkButton ID="btnReturntoPreviousPage" runat="server" CssClass="previous" onclick="btnReturntoPreviousPage_Click" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
 </div>
  <div class="lnkbutton">
  <asp:LinkButton ID="btnSendToCounselor" runat="server" onclick="btnSendToCounselor_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SC")%></span></asp:LinkButton>
    <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
        <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
            <ProgressTemplate>
                <div style="position: relative; top: 30%; text-align: center;">
                    <img src="../images/loading.gif" style="vertical-align: middle" alt="Processing" />
                    <%= Cccs.Credability.Website.App.Translate("Processing")%> ...
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
        BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
 </div>

 </div>
 <script type="text/javascript" language="javascript">
     var ModalProgress = '<%= ModalProgress.ClientID %>';
     Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
     Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

     function beginReq(sender, args) {
         // shows the Popup     
         $find(ModalProgress).show();
     }

     function endReq(sender, args) {
         //  shows the Popup
         $find(ModalProgress).hide();
     }
</script>
</asp:Content>
