﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribingYourSituation.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.DescribingYourSituation" EnableEventValidation="true" Title="CredAbility BKC Describing Your Situation" MasterPageFile="~/MasterPages/Master.Master" %>

<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
	<script language="javascript" src="../Content/FormChek.js"></script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<script>
		var HoldDelq = "", HoldMonts = "";
		function getEvent(form)
		{
			form.elements["MainEvent"].value = form.elements["TheEvent"].options[form.elements["TheEvent"].selectedIndex].text;
		}



		function ShowMort()
		{
			var HudSit = document.getElementById('<%=ddlHousingSituation.ClientID%>'), ShowWhat;
			switch (HudSit.options[HudSit.selectedIndex].value)
			{
				case "B":
					ShowWhat = 'block';
					document.getElementById('MortM').style.display = 'block';
					document.getElementById('MortR').style.display = 'none';
					break;
				case "R":
					ShowWhat = 'block';
					document.getElementById('MortM').style.display = 'none';
					document.getElementById('MortR').style.display = 'block';
					break;
				default:
					ShowWhat = 'none';
					document.getElementById('MortM').style.display = 'none';
					document.getElementById('MortR').style.display = 'none';
					break;
			}
			for (i = 0; i < 3; i++)//>
			{
				document.getElementById('Mort' + i).style.display = ShowWhat;
			}

		}
	</script>
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DUS")%></h1>
	<div id="dvErrorMessage" runat="server" class="error">
	</div>
	<div id="dvTopErrorMessage" cssclass="DvErrorSummary" runat="server" visible="false" style="color: Red;">
	</div>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|EPSI")%></p>
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
	<div class="dvform2col dvformlblbig">
		<div class="dvform">
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|PTCO")%></label>

                <table>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlPriEvent" runat="server">
            				</asp:DropDownList>
                        </td>
                        <td>
                            <span class="requiredField">*</span>
                        </td>
                    </tr>
                </table>  
                				
				<asp:RequiredFieldValidator class="validationMessage2" ID="rfddlPriEvent" SetFocusOnError="true" InitialValue="No" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="ddlPriEvent" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|TSE")%></label>
                <table>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlSecEvent" runat="server">
				            </asp:DropDownList>
                        </td>
                        <td>
                            <span class="requiredField">*</span>
                        </td>
                    </tr>
                </table>  
                <!-- CssClass="error padL12"-->
				<asp:RequiredFieldValidator style="margin-left:261px;" class="validationMessage" ID="rfvddlSecEvent" InitialValue="No" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="ddlSecEvent" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

			</div>
			<div class="dvrow">
				<p>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|DTCT")%></p>
				<span class="requiredField">*</span>

				<asp:TextBox ID="txtComments" runat="server" Width="98%" Rows="5" TextMode="MultiLine"></asp:TextBox><br />
				<asp:RequiredFieldValidator style="margin-left:260px;" class="validationMessage" ID="rfvtxtComments" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="txtComments" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|HYEF")%></label>
				<asp:RadioButtonList ID="rbtFilledBankruptcy" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CssClass="rdotbllsts">
					
				</asp:RadioButtonList>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|Housing")%></label>
				<asp:DropDownList ID="ddlHousingSituation" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlHousingSituation_SelectedIndexChanged">
				</asp:DropDownList>
				<span class="requiredField">*</span>
				<br />
				<asp:RequiredFieldValidator class="validationMessage2" ID="rfvddlHousingSituation" InitialValue="0" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="ddlHousingSituation" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">



				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|AYSB")%></label>
                <table>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="rbtForclPrev" runat="server" RepeatLayout="Table" RepeatColumns="2" CssClass="rdotbllsts" RepeatDirection="Horizontal" AutoPostBack="True">					
            				</asp:RadioButtonList>
                        </td>
                        <td>
                            <span class="requiredField">*</span>
                        </td>
                    </tr>
                </table>  
                				
				<asp:RequiredFieldValidator class="validationMessage2" ID="RequiredFieldValidator3" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="rbtForclPrev" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

			</div>
			<asp:Panel ID="pnlMort" runat="server" Visible="true">
				<div class="dvrow">
				</div>
				<div class="dvrow">
					<label id="MortR" runat="server" visible="false">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|AYDOR")%>
					</label>
					<label id="MortM" runat="server" visible="false" class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|AYDOM")%>
					</label>
					<div id="Mort0" runat="server">
						<asp:RadioButtonList ID="rbtMortCurrent" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CssClass="rdotbllsts" AutoPostBack="True">
						</asp:RadioButtonList>
						<br />
						<span class="requiredField">*</span>
						<asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" SetFocusOnError="true" CssClass="error padL12" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="rbtMortCurrent" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div class="dvrow">
					<label id="Mort1" runat="server">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|IYHM")%>
					</label>
					<div id="Mort2" runat="server">
						<asp:TextBox ID="txtMortLate" runat="server" MaxLength="3" Width="39px"></asp:TextBox>
						<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|NOM")%>
					</div>
				</div>
			</asp:Panel>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|NOPL")%></label>
				<asp:TextBox ID="txtHouseholdSize" runat="server" MaxLength="3" Width="39px"></asp:TextBox>
                <span class="requiredField">*</span><br />
				<asp:RequiredFieldValidator class="validationMessage2" ID="RequiredFieldValidator4" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="txtHouseholdSize" Text="" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
				<asp:RequiredFieldValidator class="validationMessage2" ID="rfvtxtHouseholdSize" SetFocusOnError="true" Display="Dynamic" runat="server" EnableClientScript="true" ControlToValidate="txtHouseholdSize" Text="" InitialValue="0" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
				<asp:CompareValidator clas="validationMessage2" ID="CompareValidator1" runat="server" ControlToValidate="txtHouseholdSize" ErrorMessage="Invalid Household Size must be digits!" Operator="DataTypeCheck" SetFocusOnError="True" Type="Integer" ValidationGroup="userprofile"></asp:CompareValidator>
			</div>                    

		</div>
		<div class="clearboth">
		</div>
	</div>
	<div class="dvbtncontainer">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnMonthlyPreviousPage" CssClass="previous" runat="server" OnClick="btnMonthlyPreviousPage_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnMonthlySaveExitPage" runat="server" Text="" OnClick="btnMonthlySaveExitPage_Click" CssClass="Button" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnMonthlyExpensesCon" runat="server" OnClick="btnMonthlyExpensesCon_Click" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
		</div>
	</div>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>