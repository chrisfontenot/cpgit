﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class AppSucess : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                pnlSSD.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                pnlIW.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                pnlPB.Visible = true;

            lblInNumber.Text = String.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|FJSE"), String.Format("IN{0}", SessionState.ClientNumber.Value));
            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = App.Translate("AppSuccess|Heading");
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
        }

        protected void btnIwouldlike_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverWaiver.aspx");
        }
    }
}