﻿using System;
using System.Linq;
using Cccs.Identity.Dal;
using System.Data.Linq;
using System.Reflection;

namespace Cccs.Credability.Website.BKCounseling
{
	/// <summary>
	/// Author: Ric Williams
	/// Date: 12/10/2010
	/// Purpose: To recieve and save values from a Live Person Chat so the 
	/// certificate can have the last counselors name and not the VP's name
	/// 
	/// the page receives values from a LivePerson third party notification call. In that 
	/// url call the three parameters below are passed. Validating the token is the security 
	/// process and the client ID is used for that. 
	/// </summary> 
	public partial class LivepersonCounselor : System.Web.UI.Page
    {
        private NLog.Logger logger;
		protected void Page_Load(object sender, EventArgs e)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();

			int clientNumber = Convert.ToInt32(Request.QueryString["ClientNumber"]);
			var token = Request.QueryString["Token"];
			var livePersonId = Request.QueryString["LivePersonId"];

			logger.DebugFormat("ClientNumber: {0}", clientNumber);
			logger.DebugFormat("Token: {0}", token);
			logger.DebugFormat("LivePersonId: { 0}", livePersonId);

			// Validate the live person record.
			if(String.IsNullOrEmpty(livePersonId))
				livePersonId = "IA";

			livePersonId = livePersonId.ToUpper(); // Host has everything in uppercase.

            // Bankruptch Conversion
            //var counselorName = App.Host.GetCounselorName(livePersonId);
            var counselorName = App.Debtplus.GetCounselorName(livePersonId);

            if (String.IsNullOrEmpty(counselorName))
			{
				logger.WarnFormat("CounselorName not found for LivePersonId: {0}", livePersonId);
				return;
			}

			// Validate the token.
			var comparisonToken = GenerateComparisonToken(clientNumber);
			if(token != comparisonToken)
			{
				logger.WarnFormat("Token comparison failed for ClientNumber: {0}", clientNumber);
				return;
			}

			// If token and counselor are valid then save.
			SaveCounselorInfo(clientNumber, counselorName, livePersonId);
		}

		private string GenerateComparisonToken(int clientNumber)
		{
			using(var entities = new Cccs.Identity.Dal.IdentityDataContext())
			{
				var user =
				(
					from u in entities.Users
					join a in entities.Accounts
					on u.UserId equals a.UserId
					where a.AccountTypeCode == Cccs.Identity.Account.BKC && a.InternetId == clientNumber
					select u
				).FirstOrDefault();

				if(user == null)
				{
					logger.WarnFormat("User not found for ClientNumber: {0}", clientNumber);
					return String.Empty;
				}

				return App.Identity.CreateSsoToken(user.UserId, user.SessionGuid.Value, user.LastLoginDate.Value);
			}
		}

		private void SaveCounselorInfo(int clientNumber, string counselorName, string CounselorId)
		{
			// Get the appropriate record to modify and store in an account object.
			using(var entities = new Cccs.Identity.Dal.IdentityDataContext())
			{
				Account account =
				(
					from a in entities.Accounts
					where a.AccountTypeCode == Cccs.Identity.Account.BKC && a.InternetId == clientNumber
					select a
				).FirstOrDefault();

				// Use the record retrieved and update the values.
				if(account == null)
				{
					logger.WarnFormat("Account not found for ClientNumber: {0}", clientNumber);
					return;
				}

				account.ModifiedDate = DateTime.Now;
				account.ModifiedBy = "LivePerson";
				account.CounselorName = counselorName;
				account.CounselorId = CounselorId;
				entities.SubmitChanges();
			}
		}
	}
}