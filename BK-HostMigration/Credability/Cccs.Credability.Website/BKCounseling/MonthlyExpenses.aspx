﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpenses.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.MonthlyExpenses" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs MonthlyExpenses" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyExpenses.ascx" TagPrefix="Uc" TagName="MonthlyExpenses" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:MonthlyExpenses ID="UcMonthlyExpenses" runat="server" RedirectOnPrevious="UserIncomeDocumentation.aspx" RedirectOnContinue="UnderstandingYourNetWorth.aspx"></Uc:MonthlyExpenses>
</asp:Content>
<asp:Content ID="UcLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>