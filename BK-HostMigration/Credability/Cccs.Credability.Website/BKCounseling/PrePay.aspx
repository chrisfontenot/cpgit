﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrePay.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.PrePay" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility PrePay BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|HYMW")%></h1>
<div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false">
</div>
<div class="" id="dvEscrow1" runat="server" visible="false">
    <p>
        You have already pre-paid for your counseling session with your attorney. Are you
        sure you’d like to continue to fee waiver information? (Disclaimer regarding Hold
        and Refund information)
    </p>
</div>
 <%--  <div>
      Please enter your zip code:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:TextBox ID="zipCode" runat="server" OnTextChanged="zipCode_TextChanged"></asp:TextBox>
      <br />
      Please enter the attorney&#39;s 4 digit firm code:&nbsp;&nbsp;&nbsp;
      <asp:TextBox ID="firmCode" runat="server" OnTextChanged="firmCode_TextChanged"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
   </div>--%>
<div class="" id="dvEscrow2" runat="server" visible="false">
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnYes" runat="server" OnClick="btnYes_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YI")%></span></asp:LinkButton></div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnNo" runat="server" OnClick="btnNo_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|NI")%></span></asp:LinkButton></div>
</div>
<br /><br /><br />
   <div>
      <br />
      <br />
   </div>
</asp:Content>


