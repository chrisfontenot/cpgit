﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Done.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.Done"  MasterPageFile="~/MasterPages/SynchronousMaster.Master" Title="CredAbility Done BKCounseling"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.SynchronousMaster" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<p class="col_title"><%= Cccs.Credability.Website.Resources.BKCounseling.ThankYouForUsingCredAbilitys %></p>

<div id="dvErrorMessage" runat="server" style="color:red;font-weight:bold;"></div>
<a class="external" id="PrimaryActionPlanAnchor" runat="server" href="../BKCounseling/Certificate.aspx?IsPrimary=true&IsActionPlan=true" onclick="javascript:pageTracker._trackPageview('/bkcounseling/certificate/actionPlan');" target="_blank" /><br />
<a class="external" id="PrimaryCertificateAnchor" runat="server" href="../BKCounseling/Certificate.aspx?IsPrimary=true&IsActionPlan=false" onclick="javascript:pageTracker._trackPageview('/bkcounseling/certificate/primaryCertificate');" target="_blank" /><br />
<a class="external" id="SecondaryCertificateAnchor" runat="server" href="../BKCounseling/Certificate.aspx?IsPrimary=false&IsActionPlan=false" onclick="javascript:pageTracker._trackPageview('/bkcounseling/certificate/secondaryCertificate');" target="_blank" visible="false" />

<br />
<p><%= Cccs.Credability.Website.Resources.BKCounseling.IfYouDecideToFileForBankruptcyProtection %></p>
<p><%= Cccs.Credability.Website.Resources.BKCounseling.RegardlessOfWhichOptionYouChoose %></p>
<ul>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.TwentyFourSevenContactCenter %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeAreAvailableByPhoneOrEmail %><br /><br /></li>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.BudgetAndCreditCounseling %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeOfferFreeBudgetAndCreditCounseling %><br /><br /></li>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.ComprehensiveHousingCounseling %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeOfferAFullArrayOfHousingCounseling %><br /><br /></li>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.DebtManagementPlans %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeOfferStructuredRepaymentPlans %><br /><br /></li>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.EmailNewsletters %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeProvideInformation %><br /><br /></li>
    <li><u><%= Cccs.Credability.Website.Resources.BKCounseling.OnlineEducationResources %></u> <%= Cccs.Credability.Website.Resources.BKCounseling.WeOfferAVarietyOf.FormatWith(Cccs.Credability.Website.Resources.BKCounseling.CredAbilityWebsiteAddress) %></li>
</ul>
<br /><br />
<h3 class="thanks"><%= Cccs.Credability.Website.Resources.BKCounseling.ThanksForTheOpportunity %></h3>

<p align="center">
    <asp:LinkButton ID="RTCCCS" runat="server" CssClass="external" onclick="RTCCCS_Click"><%= Cccs.Credability.Website.Resources.ButtonText.ReturnToCredAbilityHomepage %></asp:LinkButton>
</p>
</asp:Content>


