﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class UserProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender , EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Profile);

                CommonFunction.UserProgressSave(PercentComplete.USER_PROFILE, SessionState.Username);
            }

            System.Web.UI.HtmlControls.HtmlGenericControl Rvmspan = UCProvideYourPersonalInformation1.FindControl("spnBKCRow") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (Rvmspan != null)
            {
                Rvmspan.Visible = true;
            }
        }
    }
}
