﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Dal;
using Cccs.Web;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class FeeDisclosure : ContactDetailPage
    {
        public bool IsBKED = false;
        public string RedirectOnContinue
        {
            get
            {
                string url = ViewState.ValueGet<string>("RedirectOnContinue");

                return !string.IsNullOrEmpty(url) ? url : "PrePay.aspx";
            }

            set
            {
                ViewState.ValueSet("RedirectOnContinue", value);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnbkFeeDisclosureContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                UpdateFeeWaiverSessionID(SessionState.ClientNumber.Value);
            }

            Label lblHeading = new Label();
            lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = App.Translate("FreeDisclosure|Heading");
        }

        /// <summary>
        /// This Function used to insert/update the FeewaiverID into the SessionState for tempararly without having the login screen.
        /// </summary>
        /// <param name="ClientNumber"></param>
        public void UpdateFeeWaiverSessionID(Int32 ClientNumber)
        {
            var feeWaiver = App.FeeWaiver.GetByClientNumber(ClientNumber);

            if (feeWaiver != null)
            {
                SessionState.FwID = feeWaiver.FwID;
            }
            else
            {
                feeWaiver = new FeeWaiver()
                {
                    client_number = ClientNumber,
                    Language = SessionState.LanguageCode == Cccs.Translation.Language.EN ? "Eng" : "Esp",
                    ClientType = "Coun",
                };
                feeWaiver = App.FeeWaiver.AddFeeWaiver(feeWaiver);
                SessionState.FwID = feeWaiver.FwID;
            }
        }

        protected void click_btnbkFeeDisclosureContinue(object sender, EventArgs e)
        {
            
            if (chkUnderstand.Checked)
            {
                UpdateFeePolicy();
                Response.Redirect("WaiverPath.aspx");
            }
            else
            {
                dvErrorMessage.InnerHtml = App.Translate("ValidationSummary|YouMustAgree"); //"You must agree to the CredAbility fee policy before continuing.  Please read the policy and check the box indicating you have read and understand the fee policy.";
            }
        }

        public void UpdateFeePolicy()
        {
            var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(SessionState.FwID.Value);
            if (feeWaiver.FeePolicyDTS == null)
            {
                feeWaiver.FeePolicyDTS = DateTime.Now;
                feeWaiver.FeePolicy = 1;
                feeWaiver = App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
            }
        }
    }
}