﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuicCalc.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.QuicCalc" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility QuicCalc BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="JavaScript1.2">
    function Qualify()
    {
	    var St, Dep, GrossInc, PovLev, i;
	    LiveIn = document.getElementById('<%=ddlLiveIn.ClientID%>')
	    St = LiveIn.options[LiveIn.selectedIndex].text;
	    Dep = document.getElementById('<%=ddlTotDep.ClientID%>').selectedIndex;
	    GrossInc = stripCharsNotInBag(document.getElementById('<%=txtGrossInc.ClientID%>').value, "0123456789.");
	    document.getElementById('<%=txtGrossInc.ClientID%>').value = GrossInc
	    if(isNaN(GrossInc) || GrossInc == "")
	    {
		    alert('<%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCVIA")%>');
		    return false;
	    }
	    if(St.length != 2)
	    {
	        alert('<%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCSR")%>');   
		    return false;
	    }
        PovLev = St == "AK" ? 14350 + (Dep * 5030) :    //Alaska
                 St == "HI" ? 13230 + (Dep * 4620) :    //Hawaii
                 11490 + (Dep * 4020);                  //All other states

	    if(GrossInc <= PovLev * 1.5)
	    {
		    document.getElementById("Continue").style.display = 'block';
		    document.getElementById("Deny").style.display = 'none';
		    document.getElementById("Approve").style.display = 'block';
	    }
	    else
	    {
		    document.getElementById("Continue").style.display = 'block';
		    document.getElementById("Deny").style.display = 'block';
		    document.getElementById("Approve").style.display = 'none';
	    }
    }


</script>

<h1><%= Cccs.Credability.Website.App.Translate("Credability|BKC|AEFW")%></h1>
<div class="error" id="DvError" runat="server">
</div>
<p>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTQ")%></p>
<p>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTE")%></p>
 <asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile"/>
<div class="dvform2col dvformlblbig">
 <div class="dvform">
  <div class="dvrow">
   <label>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WIY")%></label>
   <asp:DropDownList ID="ddlLiveIn" runat="server">
    </asp:DropDownList>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLiveIn"
    Display="Dynamic" ValidationGroup="userprofile" Text="!" ErrorMessage="State is Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
  </div>
  <div class="dvrow">
   <label>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WIYG")%></label>
   <asp:TextBox ID="txtGrossInc" runat="server"></asp:TextBox>
   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGrossInc"
    Display="Dynamic" ValidationGroup="userprofile" Text="!" ErrorMessage="Gross Income is Required"
    SetFocusOnError="True"></asp:RequiredFieldValidator>
  </div>
  <div class="dvrow">
   <p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|GII")%></p>
  </div>
  <div class="dvrow">
   <label>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TDLI")%></label>
   <asp:DropDownList ID="ddlTotDep" runat="server">
    <asp:ListItem Value="1">1</asp:ListItem>
    <asp:ListItem Value="2">2</asp:ListItem>
    <asp:ListItem Value="3">3</asp:ListItem>
    <asp:ListItem Value="4">4</asp:ListItem>
    <asp:ListItem Value="5">5</asp:ListItem>
    <asp:ListItem Value="6">6</asp:ListItem>
    <asp:ListItem Value="7">7</asp:ListItem>
    <asp:ListItem Value="8">8</asp:ListItem>
    <asp:ListItem Value="9">9</asp:ListItem>
    <asp:ListItem Value="10">10</asp:ListItem>
    <asp:ListItem Value="11">11</asp:ListItem>
    <asp:ListItem Value="12">12</asp:ListItem>
   </asp:DropDownList>
  </div>
 </div>
 <div class="clearboth">
 </div>
</div>
<div class="dvbtncontainer">
 <div class="lnkbutton">
 <asp:LinkButton ID="btnReturnToPrevious" CssClass="previous" runat="server" OnClick="btnReturnToPrevious_Click" CausesValidation="False"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
  </div>
   <div class="lnkbutton">
 <asp:LinkButton ID="btnCalculate" runat="server" OnClientClick="Qualify();return false;" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Calculate")%></span></asp:LinkButton>
  </div>
</div>
<div id="Approve" style="display: none;">
    
	<table align="center" width="85%" cellpadding="0" cellspacing="0" border="0" class="NormTxt">
		<tr>
			<td>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCAFICF")%>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
	</table>
</div>
<div id="Deny" style="display: none;">
    <br /><br />
	<table align="center" width="85%" cellpadding="0" cellspacing="0" border="0" class="NormTxt">
		<tr>
			<td>
				<p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|PIY")%></p>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
	</table>
</div>
<div id="Continue" style="display: none;">
<div class="dvbtncontainer">
 <div class="lnkbutton">
    <asp:LinkButton ID="btnIwouldlike" runat="server" OnClick="btnIwouldlike_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IWTA")%></span></asp:LinkButton>
 </div>
 <div class="lnkbutton" style="margin-top:10px;">
    <asp:LinkButton ID="btnIdonotwish" runat="server" OnClick="btnIdonotwish_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IDN")%></span></asp:LinkButton></div>
 </div>
</div>
<div id="dvContinue" runat="server" visible="false"></div>
</asp:Content>


