﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class AFS : DefaultPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Cccs.Configuration.Dal.Properties.Settings.Cccs_IdentityConnectionString"].ConnectionString);
            conn.Open();

            SqlCommand cmd = new SqlCommand("pAccountAddressFromWeb_s", conn);
            cmd.Parameters.AddWithValue("iUserID", 8890);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 120;

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    for (int lp = 0; lp < reader.FieldCount; lp++)
                    {
                        parameters.Add(reader.GetName(lp), reader.GetValue(lp).ToString());
                    }
                }
                //parameters = Enumerable.Range(0, reader.FieldCount).ToDictionary(i => reader.GetName(i), i => reader.GetValue(i).ToString());
               // parameters = Enumerable.Range(0, reader.FieldCount)               .ToDictionary(reader.GetName, reader.GetValue);
            }
            if (!IsPostBack)
            {
                this.ShowLanguageCodes();

                InitializeTranslation();

                var afsResult = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
                if (afsResult != null)
                {
                    lblSecMsg.Visible = afsResult.IsSecondaryPersonNeeded;
                    
                    txtPrimaryPerson.Text = (afsResult.PrimaryPerson != null) ? afsResult.PrimaryPerson.Trim() : String.Empty;
                    txtSecondPerson.Text = (afsResult.SecondaryPerson != null) ? afsResult.SecondaryPerson.Trim() : String.Empty;

                    SessionState.IsSecPersonMaidenNameRequired = afsResult.IsSecondaryPersonNeeded;
                }
            }

        }

        private void CheckFeeWaiverCompletion(Int32 ClientNumber)
        {
            UserContactDetailsBKC UserContactDetailBKC = App.Credability.BKCUserInfoGet(ClientNumber);

            if (UserContactDetailBKC != null)
            {
                SessionState.SessionCompleted = UserContactDetailBKC.Completed;
            }

            PayementType paymentType = App.Credability.PayementTypeGet(ClientNumber);
            var feeWaiver = App.FeeWaiver.GetByClientNumber(ClientNumber);

            if (SessionState.SessionCompleted == 0 && paymentType != null)
            {
                int SignUp = feeWaiver.SignUp.HasValue ? feeWaiver.SignUp.Value : 0;
                if (SignUp == 1)
                {
                    Response.Redirect("ProvideYourPersonalInformation.aspx");
                    //Response.Redirect("AprWait.aspx");
                }

                if (!String.IsNullOrEmpty(paymentType.PaymentType))
                {
                    if (paymentType.PaymentType == "C" || paymentType.PaymentType == "W")
                        Response.Redirect("Disclosure.aspx");
                }
            }
        }

        private void InitializeTranslation()
        {
            rfvPrimaryPerson.ErrorMessage = App.Translate("Credability|Afs|EPP");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
        }

        protected void btnAfsContinue_Click(object sender, EventArgs e)
        {
            string fromurl = "";
            if (Request["from"] != null)
                fromurl = Request["from"].ToString();

            #region Describing Your Situation For Update DmpOnly

            var afsResult = new Credability.AFS();
            afsResult.ClientNumber = SessionState.ClientNumber.Value;
            afsResult.PrimaryPerson = txtPrimaryPerson.Text;
            afsResult.SecondaryPerson = txtSecondPerson.Text;
            if (SessionState.IsSecPersonMaidenNameRequired && string.IsNullOrEmpty(txtSecondPerson.Text))
            {
                //Error Message Mother Maiden Name is Required
                CommonFunction.ShowErrorMessageAtPagebottom(dvErrorSumary, App.Translate("Credability|AFS|SPMNR"), true);
            }
            else
            {
                AFSResult AFSResults = App.Credability.AfsAddUpdate(afsResult);



                if (AFSResults.IsSuccessful)
                {
                    CheckFeeWaiverCompletion(SessionState.ClientNumber.Value);

                    if (SessionState.Escrow == "Y")
                    {
                        Response.Redirect("ProvideYourPersonalInformation.aspx");
                    }
                    else {
                        Response.Redirect("WhatYouNeed_BKC.aspx?from=" + fromurl);
                    }
                }
                else
                {
                    CommonFunction.ShowErrorMessageAtPagebottom(dvErrorSumary, "Some error occured during the operation please try later.", true);
                }
            }
            #endregion
        }

        protected void txtPrimaryPerson_TextChanged(object sender, EventArgs e)
        {

        }
    }
}