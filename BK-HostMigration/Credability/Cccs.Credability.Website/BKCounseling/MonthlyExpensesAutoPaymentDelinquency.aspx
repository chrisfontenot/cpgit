﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpensesAutoPaymentDelinquency.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.MonthlyExpensesAutoPaymentDelinquency" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility MonthlyExpenses AutoPayment Delinquency BKCounseling" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<asp:Image ID="Image1" ImageUrl="~/Images/congrats.gif" runat="server" />
	<p>&nbsp;</p>
	<h1><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|AU")%></h1>
	<p>
							<strong><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|HAS")%></strong>
						</p>
						<p>
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YM")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Forbearance")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TIP")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Refinance")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TPI")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Sale")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TIT")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Deferment")%></b>&nbsp; <b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|or")%></b><b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Extension")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TI")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Repossession")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TITP")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|DB")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|AR")%>
						</p>
						<p>
							<b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|UD")%></b> -
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TIAS")%>
						</p>
<div class="dvbtncontainer">
                <div class="lnkbutton">
              <asp:LinkButton ID="btnReturntoPreviousPage" runat="server"  CssClass="previous"
                                    onclick="btnReturntoPreviousPage_Click" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
								 
                </div>
                <div class="lnkbutton">
                
                <asp:LinkButton ID="btnContinue" runat="server" onclick="btnContinue_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
                </div>
                </div>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>