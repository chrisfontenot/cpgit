﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class CommonOptions : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUCCommonOptions.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode = ChatCodeOption.NoChat;
				//AuthCode.SetChat(ChatCodeOption.Session1);
			}
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.TellUs);
				CommonFunction.UserProgressSave(PercentComplete.COMMAN_OPTIONS, SessionState.Username);
				System.Web.UI.HtmlControls.HtmlGenericControl BKCspan = UcUCCommonOptions.FindControl("SpanBKC") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(BKCspan != null)
				{
					BKCspan.Visible = true;
				}
				System.Web.UI.HtmlControls.HtmlGenericControl BKCspanbottom = UcUCCommonOptions.FindControl("SpanBKCBottom") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(BKCspanbottom != null)
				{
					BKCspanbottom.Visible = true;
				}
			}
		}
	}
}
