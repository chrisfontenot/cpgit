﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class QuicCalc : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetStatePickList();
                ValidationControlTranslation();
            }
        }

        private void GetStatePickList()
        {
            ddlLiveIn.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            string[] statesCodes = App.Geographics.StateCodesGet();

            if (statesCodes != null)
            {
                foreach (string s in statesCodes)
                {
                    ddlLiveIn.Items.Add(s);
                }
            }

        }
        private void ValidationControlTranslation()
        {
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|GIR");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
        }

        private QuicalcBKCounselingResult SaveQuiCalcInfo(Int32 ClientNumber)
        {
            QuicalcBKCounselingResult Result = null;

            QuicalcBKCounseling quicalcBKCounseling = new QuicalcBKCounseling();

            quicalcBKCounseling.ClientNumber = ClientNumber; // Convert.ToInt32(SessionState.ClientNumber);
            quicalcBKCounseling.ContactState = ddlLiveIn.SelectedValue;
            if (ddlTotDep.SelectedValue != "") quicalcBKCounseling.SizeofHousehold = Convert.ToInt32(ddlTotDep.SelectedValue);
            quicalcBKCounseling.MonthlyGrossIncome = (txtGrossInc.Text != "") ? (float)Convert.ToDecimal(txtGrossInc.Text) : 0;

            Result = App.Credability.QuicalcBKCounselingUpdate(quicalcBKCounseling);

            return Result;
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            //dvContinue.Visible = true;
        }

        protected void btnIwouldlike_Click(object sender, EventArgs e)
        {
            Session["WaiverType"] = "IW";

            QuicalcBKCounselingResult Result = new QuicalcBKCounselingResult();

            Result = SaveQuiCalcInfo(SessionState.ClientNumber.Value);

            if (Result.IsSuccessful == true)
            {
                Response.Redirect("WaverApp.aspx");
            }
            else
                DvError.InnerHtml = "Exception Occured. Please Try Later." + Result.Exception.ToString();

        }

        protected void btnIdonotwish_Click(object sender, EventArgs e)
        {
            QuicalcBKCounselingResult Result = new QuicalcBKCounselingResult();

            Result = SaveQuiCalcInfo(SessionState.ClientNumber.Value);

            if (Result.IsSuccessful == true)
            {
                Response.Redirect("WaiverWaiver.aspx");
            }
            else
                DvError.InnerHtml = "Exception Occured. Please Try Later.<BR /><BR />" + Result.Exception.ToString();
        }

        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");
        }
    }
}
