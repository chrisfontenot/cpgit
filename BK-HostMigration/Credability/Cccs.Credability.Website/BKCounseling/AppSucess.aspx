﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AppSucess.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.AppSucess" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility AppSucess BKCounseling"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<h1>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YHSS")%></h1>

      <p class="col_title">&nbsp;</p>
      <h1> <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PSITF")%></h1>
     <asp:Panel id="pnlIW" runat="server" visible="false">
      <p class="col_title"><%--<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYAA")%>--%></p>
      <ul>
       <li>
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ACOY1")%>
       </li>
       <li>
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YSIP")%>
       </li>
      </ul>
      </asp:Panel>
      <asp:Panel id="pnlSSD" runat="server" visible="false">
       <p class="col_title"><%--<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYAAF")%>--%></p>
      <ul>
       <li>
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ACOYM")%>
       </li>
      </ul>
       </asp:Panel>
      <asp:Panel id="pnlPB" runat="server" visible="false">
      <p class="col_title"><%--<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYRFL")%>--%></p>
      <ul>
       <li>
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ACOAL")%>
       </li>
      </ul>
      </asp:Panel>
     
      <p class="col_title"><asp:Label id="lblInNumber" runat="server"></asp:Label></p>
      <p class="col_title"> <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TSIY")%></p>
      
   <div class="dvform2col">
 <div class="colformlft">
  <div class="dvform">
     <div class="dvrow">
    <label><b><%= Cccs.Credability.Website.App.Translate("Credability|BKC|MA")%></b></label>
    <div class="dvvdata mL165"><%= Cccs.Credability.Website.App.Translate("Credability|BKC|CCC")%><br />
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AF")%><br />
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|EA")%>&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|BKC|S")%><br />
<%= Cccs.Credability.Website.App.Translate("Credability|BKC|GA")%></div>
   </div>
  </div>
  </div>
  <div class="colformrht">
  <div class="dvform">
     <div class="dvrow">
    <label><b><%= Cccs.Credability.Website.App.Translate("Credability|BKC|Email")%></b></label>
    <div class="dvvdata padT4"> <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FEE")%></div>
   </div>

   <div class="dvrow">
    <label><b> <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Fax")%></b></label>
    <div class="dvvdata padT4"><%= Cccs.Credability.Website.App.Translate("Credability|BKC|877")%></div>
   </div>
  </div>
  </div>

   <div class="clearboth">
 </div>
</div>      

<div class="dvbtncontainer">
 <div class="lnkbutton">
  <asp:LinkButton ID="btnLogOut"  CssClass="previous" runat="server" OnClick="btnLogOut_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|LO")%></span></asp:LinkButton>
 </div>
  <div class="lnkbutton">
  <asp:LinkButton ID="btnIwouldlike" runat="server"
      OnClick="btnIwouldlike_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IWL")%></span></asp:LinkButton>
 </div>
 
 </div>
 </asp:Content>


