﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class RegistrationVerify : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnRegistrationVerify.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                if (SessionState.FirmID != null)
                    lblFirmID.Text = SessionState.FirmID.ToString();

                if (SessionState.FirmName != null)
                    lblFirmName.Text = SessionState.FirmName.ToString();
            }

        }

        protected void btnRegistrationVerify_Click(object sender, EventArgs e)
        {
            if (SessionState.Escrow == "Y")
            {
                if (SessionState.Escrow6 == "Y")
                {
                    Result Result = App.Credability.PaymentTypeUpdate(SessionState.ClientNumber.Value);

                    var feeWaiver = App.FeeWaiver.GetByClientNumber(SessionState.ClientNumber.Value);
                    bool isInsert = false;
                    if (feeWaiver == null)
                    {
                        feeWaiver = new Cccs.Credability.Dal.FeeWaiver();
                        isInsert = true;
                    }

                    feeWaiver.SignUp = 1;
                    feeWaiver.Aproved = 1;
                    feeWaiver.WaiverType = "PB";
                    feeWaiver.Language = SessionState.LanguageCode == Cccs.Translation.Language.EN ? "Eng" : "Esp";
                    feeWaiver.LastModCouns = 1;

                    if (isInsert)
                    {
                        App.FeeWaiver.AddFeeWaiver(feeWaiver);
                    }
                    else
                    {
                        App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
                    }
                }
                Response.Redirect("WhatYouNeed_BKC.aspx");
            }
            else
            {
                Response.Redirect("PayWarning.aspx");
            }
        }
    }
}