﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.Shared.Pages
{
    public partial class WhatYouNeed_BKC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            string fromurl = "";
            if (Request["from"] != null)
                fromurl = Request["from"].ToString();

            if (CheckFeeWaiverCompletion(SessionState.ClientNumber.Value))
                Response.Redirect("ProvideYourPersonalInformation.aspx");
            else if (SessionState.IsSecPersonMaidenNameRequired && fromurl == "WA")
                Response.Redirect("AppSucess.aspx");
            else if (SessionState.IsSecPersonMaidenNameRequired && fromurl == "UP")
                Response.Redirect("DescribingYourSituation.aspx");
            else
                Response.Redirect("WaiverPath.aspx");
        }

        private bool CheckFeeWaiverCompletion(Int32 ClientNumber)
        {
            UserContactDetailsBKC UserContactDetailBKC = App.Credability.BKCUserInfoGet(ClientNumber);
            int completed = UserContactDetailBKC == null ? 0 : UserContactDetailBKC.Completed;

            PayementType paymentType = App.Credability.PayementTypeGet(ClientNumber);
            var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(SessionState.FwID.Value);

            if (completed == 0 && paymentType != null)
            {
                if (feeWaiver != null)
                {
                    int SignUp = feeWaiver.SignUp.HasValue ? feeWaiver.SignUp.Value : 0;
                    if (SignUp == 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}