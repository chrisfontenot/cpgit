﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.Registration"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Registration BKCounseling"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<asp:validationsummary id="DesValidationSummary" validationgroup="user" cssclass="DvErrorSummary" displaymode="BulletList"
  enableclientscript="true" runat="server" forecolor="#A50000" />
<div id="dvErrorMessage" class="DvErrorSummary" runat="server" visible="false"></div>
   <asp:Label ID="idIsRegistration" runat="server" Visible="true">
      <p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|MCS")%></p><br/>
      <div>1.</div> 
      <p>
         <asp:RadioButton ID="rdoCode" runat="server" GroupName="AttySit" AutoPostBack="True" OnCheckedChanged="rdoCode_CheckedChanged"
            CssClass="fL" />
         <label for="<%=rdoCode.ClientID %>" class="fL clrnone mgrTP34">
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHMW")%>
         </label>
      </p>

      <div class="clrbth">
      </div>
      <asp:Panel ID="pnlCode" runat="server" Enabled="false">
         <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PET")%>
         <asp:TextBox ID="txtfirmid" runat="server" MaxLength="4" CssClass="mgrT5">
         </asp:TextBox>
         <%= Cccs.Credability.Website.App.Translate("Credability|BKC|4digits")%>
         <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" SetFocusOnError="true"
            ControlToValidate="txtfirmid" ValidationExpression="^[0-9]{4}$" Text="!" ErrorMessage="Invalid Attorney Firm Code accepts digits only and must be 4 digits"
            Display="Dynamic" ValidationGroup="user" />
         <p class="col_title">
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|digitsBY")%>
         </p>
      </asp:Panel>
      <h2>Or</h2><br/>
      <asp:RadioButton ID="rboName" runat="server" GroupName="AttySit" AutoPostBack="True" OnCheckedChanged="rboName_CheckedChanged"
         CssClass="fL" />
      <label for="<%=rboName.ClientID %>" class="fL clrnone mgrTP34">
         <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHM")%></label>
      <div class="clrbth">
      </div>
      <asp:Panel ID="pnlName" runat="server" Enabled="false">
         <p class="col_title">
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PETN")%>
         </p>
         <div class="dvform2col dvformlblbig">
            <div class="dvform">
               <div class="dvrow">
                  <label>
                     <%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOLF")%></label>
                  <asp:TextBox ID="txtfirmname" runat="server" MaxLength="50">
                  </asp:TextBox>
               </div>
               <div class="dvrow">
                  <label>
                     <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AName")%></label>
                  <asp:TextBox ID="txtattyname" runat="server" MaxLength="50">
                  </asp:TextBox>
               </div>
               <div class="dvrow">
                  <label>
                     <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AEA")%></label>
                  <asp:TextBox ID="txtattyemail" runat="server" MaxLength="100">
                  </asp:TextBox><br />
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email Address"
                     Text="!" ControlToValidate="txtattyemail" Display="Dynamic" SetFocusOnError="True" CssClass="error"
                     ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="user">
                  </asp:RegularExpressionValidator>
               </div>
            </div>
            <div class="clearboth">
            </div>
         </div>
      </asp:Panel>
      <asp:RadioButton ID="rboNoAtty" runat="server" GroupName="AttySit" AutoPostBack="True" OnCheckedChanged="rboNoAtty_CheckedChanged" CssClass="fL" Visible="false" Text="I have not met with an attorney yet." />
   </asp:Label>
   <div>
     <%= (idIsRegistration.Visible? "2. ":"") + Cccs.Credability.Website.App.Translate("Credability|BKC|HOMEZIPCODE").Trim()%>
   </div>
   <asp:TextBox id="txtZipCode" runat="server" MaxLength="5"></asp:TextBox>

   <div class="dvbtncontainer">
      <div class="lnkbutton">
         <%--<asp:LinkButton ID="btnRegistration" runat="server" OnClick="btnRegistration_Click"  ValidationGroup="user"><span>Continue</span></asp:LinkButton></div>--%>
         <br /><br />
         <asp:linkbutton id="btnRegistration" runat="server" onclick="btnRegistration_Click" validationgroup="user">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
         </asp:linkbutton>
      </div>
   </div>
</asp:Content>



