﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaiverWaiver.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.WaiverWaiver" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility WaiverWaiver BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="javascript" type="text/javascript">
    function ReSetSSN(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatSSN(Val);
        }
    }
    function CheckSsn(sender, args) {

        if (args.Value.length < 11 || args.Value.length == 0) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
        //args.Value;

    }

</script>

<div style="width: 100%; color: Red; font-weight: bold" id="DvError" runat="server">
</div>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|BKC|WOA")%></h1>

<asp:Panel ID="pnlEscrow" runat="server" Visible="true">
<!--    
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BYH")%><br />
     <div class="lnkbutton">
        <asp:LinkButton ID="lnkFeeWaiver" runat="server" OnClick="lnkFeeWaiver_Click"
            CausesValidation="False"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IUT")%></span>
        </asp:LinkButton>
    </div>
</p>
-->    
</asp:Panel>    
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YHCTY")%></p>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTC")%></p>
<asp:ValidationSummary ID="DesValidationSummary" ValidationGroup="user" CssClass="DvErrorSummary"
    HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList"
    EnableClientScript="true" runat="server" ForeColor="#A50000" />
<div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false">
</div>
<div class="error" id="dvTopErrorMessage" runat="server">
</div>
<div class="dvform2col dvformlblbig">
    <div class="dvform">
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AYF")%></label>
            <asp:RadioButtonList ID="rblJointFile" runat="server" CssClass="rdotbllsts mgrT7 fL"
                RepeatDirection="Horizontal" RepeatLayout="Table" AutoPostBack="True" OnSelectedIndexChanged="rblJointFile_SelectedIndexChanged">
                
                <%--<asp:ListItem Value="1">Yes</asp:ListItem>
                <asp:ListItem Value="0">No</asp:ListItem>--%>
            </asp:RadioButtonList>
            <span style="color: Red; font-size:large;vertical-align:bottom;" CssClass="fL padT5">*</span>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblJointFile"
                Display="Dynamic" Text="!" ErrorMessage="" SetFocusOnError="True"
                ValidationGroup="user" CssClass="fL padT5"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YZC")%></label>
            <asp:TextBox ID="txtPriZipEnc" runat="server" MaxLength="5" CssClass="txtBox"></asp:TextBox>
              <span style="color: Red; font-size: large">*</span>
            <asp:RequiredFieldValidator ID="rfvZipCode" runat="server" ControlToValidate="txtPriZipEnc"
                EnableClientScript="true" Text="!" ErrorMessage="Zip Code is required." Display="Dynamic"
                ValidationGroup="user"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="rfvtxtZip" ControlToValidate="txtPriZipEnc"
                ValidationExpression="^[0-9]{5}$" ErrorMessage="Enter  valid Zip Code must be 5 numeric digits"
                Display="Dynamic" Text="*" ValidationGroup="user" />
            <span id="divZipError" runat="server" class="error" style="display: none"></span>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|L4")%></label>
            <asp:TextBox ID="txtPri4SsnEnc" runat="server" MaxLength="4" CssClass="txtBox"></asp:TextBox>
              <span style="color: Red; font-size: large">*</span>
           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPri4SsnEnc"
                Display="Dynamic" ErrorMessage="SSN is Required" SetFocusOnError="True" ValidationGroup="continue"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="rfvSSN" runat="server" ControlToValidate="txtPri4SsnEnc"
                ErrorMessage="Your SSN is Required." Text="!" Display="Dynamic" ValidationGroup="user"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" ControlToValidate="txtPri4SsnEnc"
                    ValidationExpression="^[0-9]{4}$" ErrorMessage="Invalid SSN Number must be accepts 4 numeric digits"
                    Display="Dynamic" Text="*" ValidationGroup="user" />     
            <%--<asp:CustomValidator ID="cfvSSN" runat="server" EnableClientScript="true" ErrorMessage="Invalid SSN Number.Valid Format xxx-xx-xxxx"
                ClientValidationFunction="CheckSsn" ControlToValidate="txtPri4SsnEnc" Text="*"
                Display="Dynamic" ValidationGroup="user"> 
            </asp:CustomValidator>--%>
        </div>
        <asp:Panel ID="pnlSpouse" runat="server" Visible="false">
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SZ")%></label>
                <asp:TextBox ID="txtSecZipEnc" runat="server" MaxLength="5" CssClass="txtBox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvSzip" runat="server" ControlToValidate="txtSecZipEnc"
                    EnableClientScript="true" Text="!" ErrorMessage="Zip Code is required." Display="Dynamic"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="txtSecZipEnc"
                    ValidationExpression="^[0-9]{5}$" ErrorMessage="Enter  valid Zip Code must be 5 numeric digits"
                    Display="Dynamic" Text="*" ValidationGroup="user" />
                <span id="Span1" runat="server" class="error" style="display: none"></span>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|L4D")%></label>
                <asp:TextBox ID="txtSec4SsnEnc" runat="server" CssClass="txtBox" MaxLength="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSec4SsnEnc"
                    ErrorMessage="Your SSN is Required." Text="!" Display="Dynamic" ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ControlToValidate="txtSec4SsnEnc"
                    ValidationExpression="^[0-9]{4}$" ErrorMessage="Invalid SSN Number must be accepts 4 numeric digits"
                    Display="Dynamic" Text="*" ValidationGroup="user" />     
                <%--<asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="true"
                    ErrorMessage="Invalid SSN Number.Valid Format xxx-xx-xxxx" ClientValidationFunction="CheckSsn"
                    ControlToValidate="txtSec4SsnEnc" Text="*" Display="Dynamic" ValidationGroup="user"> --%>
                </asp:CustomValidator>
            </div>
        </asp:Panel>
    </div>
    <div class="clearboth">
    </div>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturn" CssClass="previous" runat="server" OnClick="btnReturn_Click"
            CausesValidation="False"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ValidationGroup="user"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
</asp:Content>


