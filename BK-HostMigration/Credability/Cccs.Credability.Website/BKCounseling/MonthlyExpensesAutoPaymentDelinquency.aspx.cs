﻿using System;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class MonthlyExpensesAutoPaymentDelinquency : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnReturntoPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }
        }

        protected void btnReturntoPreviousPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("MonthlyExpenses.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("UnderstandingYourNetWorth.aspx");
        }
    }
}