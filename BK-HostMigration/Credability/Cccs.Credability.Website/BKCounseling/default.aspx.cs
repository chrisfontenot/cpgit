﻿using System;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Website.BKCounseling
{
   public partial class _default : System.Web.UI.Page
   {
      public Int32 SignUp;
      public Int32 Aproved;
      protected void Page_Load(object sender, EventArgs e)
      {
         SetFWIDSession();
         Credability.AFS Afs = null;
         Afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
         if (Afs != null)
         {
            CheckFeeWaiverCompletion(SessionState.ClientNumber.Value);
            if (String.IsNullOrEmpty(Afs.PrimaryPerson))
            {
               Response.Redirect("PrePay.aspx");
            }
            else if (Afs.IsSecondaryPersonNeeded && String.IsNullOrEmpty(Afs.SecondaryPerson))
            {
               Response.Redirect("PrePay.aspx");
            }
         }
         else
         {
            CheckFeeWaiverCompletion(SessionState.ClientNumber.Value);
         }
      }

      private int AddFeeWaiverRecord()
      {
         var feeWaiver = new FeeWaiver()
         {
            Language = SessionState.LanguageCode == Cccs.Translation.Language.EN ? "Eng" : "Esp",
            client_number = SessionState.ClientNumber.Value,
            ClientType = "Coun",
         };
         feeWaiver = App.FeeWaiver.AddFeeWaiver(feeWaiver);
         return feeWaiver.FwID;
      }

      private void SetFWIDSession()
      {
         var feeWaiver = App.FeeWaiver.GetByClientNumber(SessionState.ClientNumber.Value);
         if (feeWaiver != null)
         {
            SessionState.FwID = feeWaiver.FwID;
         }
         else
         {
            SessionState.FwID = AddFeeWaiverRecord();
         }
      }

      private void CheckFeeWaiverCompletion(Int32 ClientNumber)
      {
         UserContactDetailsBKC UserContactDetailBKC = App.Credability.BKCUserInfoGet(ClientNumber);
         int completed = UserContactDetailBKC == null ? 0 : UserContactDetailBKC.Completed;

         PayementType paymentType = App.Credability.PayementTypeGet(ClientNumber);
         var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(SessionState.FwID.Value);

         if (completed == 0 && paymentType != null)
         {
            if (feeWaiver != null)
            {
               int SignUp = feeWaiver.SignUp.HasValue ? feeWaiver.SignUp.Value : 0;
               if (SignUp == 1)
               {
                  Response.Redirect("AprWait.aspx");
               }
            }

            if (!String.IsNullOrEmpty(paymentType.PaymentType))
            {
               if (paymentType.PaymentType.Trim() == "C" || paymentType.PaymentType.Trim() == "W")
                  Response.Redirect("ProvideYourPersonalInformation.aspx");
            }
         }
         if (completed == 1)
         {
            Response.Redirect("ProvideYourPersonalInformation.aspx");
         }
         Response.Redirect("PrePay.aspx");
      }
   }
}
