﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrationVerify.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.RegistrationVerify" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility RegistrationVerify BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<p>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AIV")%></p>
<p>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YAF")%>
 <asp:Label ID="lblFirmID" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
 <asp:Label ID="lblFirmName" runat="server" Text=""></asp:Label>
 
 </p>
<p>
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WWS")%></p>
<p>
 <a href="Registration.aspx">
  <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ITIN")%></a></p>
<div class="dvbtncontainer">
 <div class="lnkbutton">
  <asp:LinkButton ID="btnRegistrationVerify" runat="server" OnClick="btnRegistrationVerify_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
 </div>
</div>
</asp:Content>


