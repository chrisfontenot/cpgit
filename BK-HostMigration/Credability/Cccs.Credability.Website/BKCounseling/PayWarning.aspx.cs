﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class PayWarning : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnRegistration.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }

            if (SessionState.EditAttorney == "Y")
                lblWarningMsg.Text = App.Translate("Credability|BKC|WAPA");
            else
                lblWarningMsg.Text = App.Translate("Credability|BKC|TADD");
        }

        protected void btnRegistration_Click(object sender, EventArgs e)
        {
            if (SessionState.EditAttorney == "Y")
                Response.Redirect("Disclosure.aspx");
            else
                Response.Redirect("WhatYouNeed_BKC.aspx");
        }
    }
}