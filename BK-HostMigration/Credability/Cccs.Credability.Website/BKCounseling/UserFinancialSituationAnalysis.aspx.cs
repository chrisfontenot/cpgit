﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class UserFinancialSituationAnalysis : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Summary);

				CommonFunction.UserProgressSave(PercentComplete.ANALYZING_YOUR_FINANCIAL_SITUATION, SessionState.Username);
			}
		}
	}
}