﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class UnderstandingYourNetWorth : ContactDetailPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Budget);
				CommonFunction.UserProgressSave(PercentComplete.UNDERSTANDING_YOUR_NET_WORTH, SessionState.Username);
			}
		}
	}
}