﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class DescribingYourSituation2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.TellUs);

                CommonFunction.UserProgressSave(PercentComplete.DESCRIBE_YOUR_SITUATION, SessionState.Username);

                System.Web.UI.HtmlControls.HtmlGenericControl BKCspan = UcUCDescribeYourSituation.FindControl("areYouCurOnPaySpan") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (BKCspan != null)
                {
                    BKCspan.Visible = false;
                }

            }
        }
    }
}
