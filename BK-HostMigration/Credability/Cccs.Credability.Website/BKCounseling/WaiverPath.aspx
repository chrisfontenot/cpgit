﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaiverPath.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.WaiverPath" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility WaiverPath BKCounseling" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

<h1><%= Cccs.Credability.Website.App.Translate("Credability|BKC|PTS")%></h1>
<p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|TYFCS")%></p>

<div class="dvbtncontainer">
    <div class="lnkbutton fNone">
        <asp:LinkButton ID="btnIwouldliketoapply" runat="server" OnClick="btnIwouldliketoapply_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IWLT")%></span></asp:LinkButton>
    </div>
    <p>&nbsp;</p>
</div>

<div style="margin-top:100px;">
    <p><%= Cccs.Credability.Website.App.Translate("LegalAidWaiverExplain")%></p>
</div>

<div style="margin-top:-30px;">
    <p>&nbsp;</p>
    <div class="lnkbutton">
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnIqualifyforafee_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IQFAF")%></span></asp:LinkButton>
    </div>
    <p>&nbsp;</p>
</div>

</asp:Content>