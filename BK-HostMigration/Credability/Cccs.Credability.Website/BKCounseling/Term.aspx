﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Term.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.Term" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Term BKCounseling" %>
<%@ Register Src="~/Controls/Shared/Pages/UcTerm.ascx" TagPrefix="Uc" TagName="UCTerm" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCTerm ID="UcUCTerm" runat="server" RedirectOnPrevious="WhatYouNeed.aspx"></Uc:UCTerm>
</asp:Content>