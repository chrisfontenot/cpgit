﻿using System;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class UserIncomeDocumentation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUserIncome.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode=ChatCodeOption.NoChat;
				//AuthCode.SetChat(ChatCodeOption.Session2);
			}
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.Budget);
				CommonFunction.UserProgressSave(PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
				System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = UcUserIncome.FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(HeadSpan != null)
				{
					HeadSpan.Visible = true;
				}
				System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(introText != null)
				{
					introText.Visible = true;
				}
			}
		}
	}
}