﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribingYourSituation2.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.DescribingYourSituation2" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility DescribingYourSituation2 BKC" %>
<%@ Register Src="~/Controls/BchControls/UCDescribeYourSituation.ascx" TagPrefix="Uc"  TagName="UCDescribeYourSituation"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCDescribeYourSituation id="UcUCDescribeYourSituation" runat="server"></Uc:UCDescribeYourSituation>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>
