﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class WaiverPath : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnIqualifyforafee_Click(object sender, EventArgs e)
        {
            SessionState.WaiverType = "PB";
            Response.Redirect("WaverApp.aspx");
        }

        protected void btnSSDI_Click(object sender, EventArgs e)
        {
            SessionState.WaiverType = "SSD";
            Response.Redirect("WaverApp.aspx");
        }

        protected void btnIwouldliketoapply_Click(object sender, EventArgs e)
        {
            SessionState.WaiverType = "IW";
            Response.Redirect("QuicCalc.aspx");
        }

        protected void btnNoIdonot_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverWaiver.aspx");
        }
    }
}