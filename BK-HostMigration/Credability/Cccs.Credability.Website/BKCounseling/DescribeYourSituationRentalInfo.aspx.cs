﻿using System;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BKCounselingControls;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class DescribeYourSituationRentalInfo : ContactDetailPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            UcAuthCode.ChatCode=ChatCodeOption.Session1;
			if(!IsPostBack)
			{
				Master.BreadCrumbBKC.ActiveTabSet(BreadCrumControl.Tab.TellUs);
				CommonFunction.UserProgressSave(PercentComplete.COMMAN_OPTIONS, SessionState.Username);
				btnReturntoPreviousPage.ToolTip = App.Translate("Credability|RVMButtons|Previous");
				btnContinue.ToolTip = App.Translate("Credability|RVMButtons|Continue");
				btnSaveandExit.ToolTip = App.Translate("Credability|RVMButtons|SaveContinueLater");
				DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				Response.Redirect("ListingYourDebts.aspx?from=Rent");
			}
		}

		protected void btnReturntoPreviousPage_Click(object sender, EventArgs e)
		{
			Response.Redirect("DescribingYourSituation.aspx");
		}

		protected void btnSaveandExit_Click(object sender, EventArgs e)
		{
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
	}
}