﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class WaiverWaiver : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnReturn.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                if (SessionState.Escrow == "Y")
                    pnlEscrow.Visible = true;
                else
                    pnlEscrow.Visible = false;

                RadioButtonTransaltion();
                ValidationControlTranslation();
            }
        }

        private void ValidationControlTranslation()
        {
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OFT");
            rfvZipCode.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
            rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
            rfvSSN.ErrorMessage = App.Translate("Credability|UserProfileBCH|SSNR");
            RegularExpressionValidator3.ErrorMessage = App.Translate("Credability|UserProfileBCH|ISSN");

            rfvSzip.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ZR");
            RegularExpressionValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|EV");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YSSNR");
            RegularExpressionValidator2.ErrorMessage = App.Translate("Credability|UserProfileBCH|ISSN");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
        }

        private void RadioButtonTransaltion()
        {
            CommonFunction.AddItems(rblJointFile.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
        }

        private void SaveWaiverInfo(Int32 FeeWaiverID)
        {
            var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(FeeWaiverID);
            feeWaiver.SignUp = 0;
            if (!String.IsNullOrEmpty(rblJointFile.SelectedValue))
            {
                feeWaiver.JointFile = Convert.ToInt32(rblJointFile.SelectedValue);
            }
            feeWaiver.Pri4SsnEnc = FeeWaiverManager.EncryptText(txtPri4SsnEnc.Text.Trim());
            feeWaiver.PriZipEnc = FeeWaiverManager.EncryptText(txtPriZipEnc.Text.Trim());
            feeWaiver.Sec4SsnEnc = FeeWaiverManager.EncryptText(txtSec4SsnEnc.Text.Trim());
            feeWaiver.SecZipEnc = FeeWaiverManager.EncryptText(txtSecZipEnc.Text.Trim());

            App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
        }

        protected void lnkFeeWaiver_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            SessionState.PrimaryZipCode = txtPriZipEnc.Text.Trim();
            var feeWaiver = App.FeeWaiver.GetByClientNumber(SessionState.ClientNumber.Value); //216; 
            var feeWaiverId = feeWaiver.FwID;
            Boolean PrimaryZipcodeFlag = true, SecondaryZipcodeFlag = true;
            if (rblJointFile.SelectedValue == "1")
            {
                if (PrimaryZipcodeFlag && SecondaryZipcodeFlag)
                {
                    DvError.InnerHtml = "";
                    SaveWaiverInfo(feeWaiverId);
//                    if (SessionState.Escrow == "Y")
//                        Response.Redirect("ProvideYourPersonalInformation.aspx");
//                    else
                        Response.Redirect("Disclosure.aspx");
                }
                else
                {
                    DvError.InnerHtml = "The zip code is not a valid zip code please correct.";
                }
            }
            else if (rblJointFile.SelectedValue == "0")
            {

                var isZipCodeAvailable = ZipCodeManager.ZipCodeAvailable(txtPriZipEnc.Text.Trim());
                if (isZipCodeAvailable)
                {
                    DvError.InnerHtml = "";
                    SaveWaiverInfo(feeWaiverId);
//                    if (SessionState.Escrow == "Y")
//                        Response.Redirect("ProvideYourPersonalInformation.aspx");
//                    else
                        Response.Redirect("Disclosure.aspx");
                }
                else
                {
                    DvError.InnerHtml = "The zip code is not a valid zip code please correct.";
                }
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("PrePay.aspx");
        }

        protected void rblJointFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblJointFile.SelectedValue == "1")
                pnlSpouse.Visible = true;
            else
                pnlSpouse.Visible = false;
        }
    }
}
