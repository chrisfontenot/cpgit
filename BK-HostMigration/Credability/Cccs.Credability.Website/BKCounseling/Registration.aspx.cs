﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Cccs.Credability.Website.BKCounseling
{
    public partial class Registration : ContactDetailPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            idIsRegistration.Visible = SessionState.isRegistration;

            if (!IsPostBack)
            {
                btnRegistration.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }

            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
            {
                if (SessionState.isRegistration)
                {
                    lblHeading.Text = App.Translate("Registeration|Heading");
                }
                else
                {
                    lblHeading.Text = App.Translate("Registeration|PEYZC");
                }
            }

            rboNoAtty.Text = App.Translate("Credability|BKC|INMA");
            if (SessionState.EditAttorney == "Y")
                rboNoAtty.Visible = true;

            if (!IsPostBack)
            {
                ValidationControlTranslation();
                var RegistrationBKCounselings = App.Credability.RegistrationBKCounselingGet(Convert.ToInt32(SessionState.ClientNumber));

                if (RegistrationBKCounselings != null)
                {
                    if (RegistrationBKCounselings.ATTYSit.Trim() == "Code")
                    {
                        rdoCode.Checked = true;
                        pnlCode.Enabled = true;
                        pnlName.Enabled = false;
                    }
                    else if (RegistrationBKCounselings.ATTYSit.Trim() == "Name")
                    {
                        rboName.Checked = true;
                        pnlName.Enabled = true;
                        pnlCode.Enabled = false;
                    }

                    txtfirmid.Text = RegistrationBKCounselings.FIRMid.Clean();
                    txtfirmname.Text = RegistrationBKCounselings.FIRMname.Clean();
                    txtattyname.Text = RegistrationBKCounselings.ATTYname.Clean();
                    txtattyemail.Text = RegistrationBKCounselings.ATTYemail.Clean();
                }

            }
        }

        private void ValidationControlTranslation()
        {
            RegularExpressionValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IA");
            RegularExpressionValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IE");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");

        }

        public RegistrationBKCounselingResult SaveRegistrationDetails()
        {
            String AttySit = "";
            if (rdoCode.Checked) AttySit = "Code";
            if (rboName.Checked) AttySit = "Name";

            RegistrationBKCounseling registrationBKCounseling = new RegistrationBKCounseling();
            registrationBKCounseling.ClientNumber = SessionState.ClientNumber.Value;
            registrationBKCounseling.ATTYname = txtattyname.Text;
            registrationBKCounseling.ATTYemail = txtattyemail.Text;
            registrationBKCounseling.FIRMid = txtfirmid.Text;
            registrationBKCounseling.FIRMname = txtfirmname.Text;
            registrationBKCounseling.ATTYSit = AttySit;

            var registrationBKCounselingResult = App.Credability.RegistrationBKCounselingAddUpdate(registrationBKCounseling);
            return registrationBKCounselingResult;
        }

        protected void btnRegistration_Click(object sender, EventArgs e)
        {
            if (!ZipCodeManager.ZipCodeAvailable(txtZipCode.Text.Trim()))
            {
                dvErrorMessage.Visible = true;
                dvErrorMessage.InnerHtml = "The zip code is invalid. Please correct.";
                return;
            }
            SessionState.PrimaryZipCode = txtZipCode.Text.Trim();

            if (txtfirmid.Text.Trim().IsNullOrEmpty())
            {
                SessionState.FirmID = "9999";
            }
            else
            {
                SessionState.FirmID = txtfirmid.Text.Trim();
            }

            //Bankruptcy conversion-Seethal
            //SessionState.BKFee =  App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B").Value;;
            SessionState.BKFee = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");

            if (!idIsRegistration.Visible)
            {
                Response.Redirect("WhatYouNeed_BKC.aspx");
                return;
            }

            if (rboName.Checked == false && rdoCode.Checked == false && rboNoAtty.Checked == false)
            {
                dvErrorMessage.Visible = true;
                dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|PMA") + "</li></ul>";
            }
            else
            {
                dvErrorMessage.InnerHtml = "";

                String AttySit = "";
                if (rdoCode.Checked) AttySit = "Code";
                if (rboName.Checked) AttySit = "Name";
                if (rboNoAtty.Checked) AttySit = "NoAtty";

                if (AttySit == "Code")
                {
                    SessionState.Escrow = "N"; // This Session is used to Validate the Attorney.

                    if (txtfirmid.Text == "9999")
                    {
                        dvErrorMessage.Visible = true;
                        dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                        dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|PT") + "</li></ul>";
                    }
                    else if (txtfirmid.Text.Length != 4)
                    {
                        dvErrorMessage.Visible = true;
                        dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                        dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|PTRY") + txtfirmid.Text + App.Translate("Credability|UserProfileBCH|WV") + "</li></ul>";
                    }
                    else
                    {
                        int firmId;
                        bool flag = false;

                        if (Int32.TryParse(SessionState.FirmID, out firmId))
                            flag = true;

                        if (flag)
                        {
                            CheckFirmID(firmId);
                            RegistrationBKCounselingResult registrationBKCounselingResult = SaveRegistrationDetails();

                            if (SessionState.BKFee.ToString().IsNullOrEmpty())
                            {
                                //Bankruptcy conversion-Seethal
                                //double char_amt = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B").Value;
                                double char_amt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");
                                SessionState.BKFee = char_amt;
                                SessionState.ChargeAmount = char_amt.ToString();
                            }

                            if (registrationBKCounselingResult.IsSuccessful)
                            {
                                Response.Redirect("RegistrationVerify.aspx");
                            }
                        }
                        else
                        {
                            dvErrorMessage.Visible = true;
                            dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                            dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|TRW") + txtfirmid.Text + App.Translate("Credability|UserProfileBCH|WV") + "</li></ul>";
                        }

                    }

                }
                else if (AttySit == "Name")
                {
                    SessionState.Escrow = "N"; // This Session is used to Validate the Attorney.
                    Boolean ErrorFlag = false;
                    String ErrorMsg = "";
                    if (txtfirmname.Text == String.Empty)
                    {
                        ErrorMsg += "<li>" + App.Translate("Credability|UserProfileBCH|PSN") + "</li>";
                        ErrorFlag = true;
                    }

                    if (ErrorFlag == true)
                    {
                        dvErrorMessage.Visible = true;
                        dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>" + ErrorMsg + "</ul>";
                    }
                    else
                    {
                        if (txtfirmid.Text != String.Empty)
                            SessionState.FirmName = txtfirmname.Text.Clean();
                        else
                            SessionState.FirmName = "-";

                        if (txtfirmid.Text != String.Empty)
                            SessionState.FirmID = txtfirmid.Text.Clean();
                        else
                            SessionState.FirmID = "-";

                        RegistrationBKCounselingResult registrationBKCounselingResult = SaveRegistrationDetails();

                        //Bankruptcy conversion-Seethal
                        //double char_amt = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B").Value;
                        double char_amt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");
                        SessionState.BKFee = char_amt;
                        SessionState.ChargeAmount = char_amt.ToString();

                        if (registrationBKCounselingResult.IsSuccessful)
                        {
                            Response.Redirect("PayWarning.aspx");
                        }
                        else
                            dvErrorMessage.InnerHtml = registrationBKCounselingResult.Exception.ToString();  //"Some error occured during the operation please try later.";
                    }
                }
                else
                {
                    if (SessionState.BKFee.ToString().IsNullOrEmpty())
                    {
                        //Bankruptcy conversion-Seethal
                        //double char_amt = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B").Value;
                        double char_amt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");
                        SessionState.BKFee = char_amt;
                        SessionState.ChargeAmount = char_amt.ToString();
                    }

                    if (SessionState.Escrow == "Y")
                        Response.Redirect("WaverWaiver.apsx");
                    else
                        Response.Redirect("PayWarning.aspx");
                }
            }
        }


        private bool CheckFirmID(int FirmCode)
        {
            return App.SetEscrowInfo();
        }

        protected void rdoCode_CheckedChanged(object sender, EventArgs e)
        {
            pnlCode.Enabled = true;
            pnlName.Enabled = false;
            txtfirmid.Focus();
        }

        protected void rboName_CheckedChanged(object sender, EventArgs e)
        {
            pnlName.Enabled = true;
            pnlCode.Enabled = false;
            txtfirmname.Focus();
        }

        protected void rboNoAtty_CheckedChanged(object sender, EventArgs e)
        {
            pnlName.Enabled = false;
            pnlCode.Enabled = false;
        }
    }
}