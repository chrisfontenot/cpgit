﻿using System;
using System.Web;

namespace Cccs.Credability.Website.BKCounseling
{
	public partial class AprWait : ContactDetailPage
	{
        protected string CounsNotes = String.Empty;
        private NLog.Logger logger;

		protected void Page_Load(object sender, EventArgs e)
        {
            if (
                String.IsNullOrEmpty(SessionState.FirmID)
                || String.IsNullOrEmpty(SessionState.PrimaryZipCode)
            )
            {
                var myUrl = HttpContext.Current.Request.Url.AbsolutePath;
                Response.Redirect(ResolveUrl("~/GetRequiredFeeInformation.aspx?redirect=" + myUrl));
            }

            this.logger = NLog.LogManager.GetCurrentClassLogger();
			if(!IsPostBack)
			{
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnContinueDisApproved.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");

				ApprovalState();
			}

            if (SessionState.ClientNumber.HasValue) {
                var PreviouslySavedUserInfo = App.Credability.UserInfoGet(SessionState.ClientNumber.Value);
                
                SessionState.FirmID = !String.IsNullOrEmpty(PreviouslySavedUserInfo.FirmID) ? PreviouslySavedUserInfo.FirmID : SessionState.FirmID;
                SessionState.PrimaryZipCode = !String.IsNullOrEmpty(PreviouslySavedUserInfo.ZipCode) ? PreviouslySavedUserInfo.ZipCode : SessionState.PrimaryZipCode;

                try{
                    if(SessionState.PrimaryZipCode == String.Empty){
                        var addresses = App.Identity.AddressesGet(SessionState.UserId.Value);

                        foreach (var address in addresses) {
                            if (address.Zip == String.Empty) {
                                SessionState.PrimaryZipCode = address.Zip;
                                break;
                            }
                        }                                               
                    }
                }catch(Exception ex){
                    logger.LogException(NLog.LogLevel.Fatal, "Something went wrong trying to retrieve a client address.", ex);
                }

            }

            //Bankruptcy conversion-Seethal
            //double char_amt = App.Host.GetCharAmt(SessionState.FirmID,SessionState.PrimaryZipCode, "B").Value;

            double char_amt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");

            lblCharAmt1.Text = char_amt.ToString();
			GetCharAmt2.Text = char_amt.ToString();
		}

		private void SetFWIDSession()
		{
			var feeWaiver = App.FeeWaiver.GetByClientNumber(SessionState.ClientNumber.Value);
			if(feeWaiver != null)
			{
				SessionState.FwID = feeWaiver.FwID;
			}
		}

		private void ApprovalState()
		{
			if(SessionState.FwID == null)
			{
				SetFWIDSession();
			}

			hdnValues.Value = "FeeWaiverID: " + SessionState.FwID.ToString() + "ClientNumber: " + SessionState.ClientNumber.ToString();

			if(SessionState.FwID != null)
			{
				PayementType PayementTypes = null;

				if(SessionState.ClientNumber != null)
					PayementTypes = App.Credability.PayementTypeGet(SessionState.ClientNumber.Value);

				if(PayementTypes != null)
				{
					if(String.IsNullOrEmpty(PayementTypes.PaymentType))
					{
						Payed = false;
                        PageRedirectUrl = "WaiverPath.aspx";
					}
					else
					{
						Payed = true;
                        PageRedirectUrl = "WaiverPath.aspx";
					}
				}

				var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(SessionState.FwID.Value);
				this.CounsNotes = feeWaiver.CounsNotes;
				int SignUp = feeWaiver.SignUp.Value;
				int Aproved = feeWaiver.Aproved.HasValue ? feeWaiver.Aproved.Value : 2;
				int FeePolicy = feeWaiver.FeePolicy.HasValue ? feeWaiver.FeePolicy.Value : 0;
				if(SignUp == 1)
				{
					if(Aproved == 2)
						pnlWait.Visible = true;
					else
					{
						if(Aproved == 1)
							pnlApproved.Visible = true;
						else
							pnlDisApproved.Visible = true;
					}
				}
				else
				{
					if(FeePolicy == 1 && !Payed)
					{
						PageRedirectUrl = "PrePay.aspx";
					}
					Response.Redirect(PageRedirectUrl);
				}
			}
		}
		
        protected void btnAbandonSession_Click(object sender, EventArgs e)
		{
			Response.Redirect("WaiverWaiver.aspx");
		}
		protected void btnLogOut_Click(object sender, EventArgs e)
		{
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
		protected void btnContinueDisApproved_Click(object sender, EventArgs e)
		{
			Response.Redirect("Disclosure.aspx");
		}
		protected void btnContinue_Click(object sender, EventArgs e)
		{
			Response.Redirect("ProvideYourPersonalInformation.aspx");
		}
		private string Page_Redirect_Url = string.Empty;
		private bool Payed_Value;
		public string PageRedirectUrl
		{
			get
			{
				return Page_Redirect_Url;
			}
			set
			{
				Page_Redirect_Url = value;
			}
		}
		public bool Payed
		{
			get
			{
				return Payed_Value;
			}
			set
			{
				Payed_Value = value;
			}
		}
	}
}