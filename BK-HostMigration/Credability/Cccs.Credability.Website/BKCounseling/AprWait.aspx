﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AprWait.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.AprWait" MasterPageFile="~/MasterPages/Master.Master" Title="BKC" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script type="text/javascript">
<!--
    function mailto() {
        var username = "feewaiver";
        var hostname = "credability.org";
        var linktext = '<%= Cccs.Credability.Website.App.Translate("Credability|BKC|FEEW")%>';  //username + "@" + hostname;
        document.write("<a href=" + "mail" + "to:" + username +
            "@" + hostname + " class='NormLink'>" + linktext + "</a>")
    }
//-->
</script>
<asp:Panel ID="pnlWait" runat="server" Visible="false">
    <p class="col_title">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ORI")%><script type="text/javascript">                                                                               mailto();</script>
        <!--a href="mailto:feewaiver@credability.org" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|BKC|FEEW")%></a-->
    </p>
    <p class="col_title">
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYS")%></p>
    <div class="dvform2col">
        <div class="colformlft">
            <div class="dvform">
                <div class="dvrow">
                    <label>
                        <b>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|MADD")%></p __designer:mapid="7b"></b></label><div class="dvvdata mL165">
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CCC")%><br />
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ATF")%><br />
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|100")%><br />
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|1800")%>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|303")%></div>
                </div>
            </div>
        </div>
        <div class="colformrht">
            <div class="dvform">
                <div class="dvrow">
                    <label>
                        <b>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Email")%></b></label>
                    <div class="dvvdata padT4">
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|org")%></div>
                </div>
                <div class="dvrow">
                    <label>
                        <b>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|F")%></b></label>
                    <div class="dvvdata padT4">
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|8772")%></div>
                </div>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </div>
    <p>
        <%=CounsNotes%></p>
    <%--<td id="FeeW0" onclick="GoToPage('FeeW0','Out','')" onmouseover="HiLight('FeeW0','In')" onmouseout="HiLight('FeeW0','Out')">--%>
    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="btnLogOut" runat="server" OnClick="btnLogOut_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|LGO")%></span></asp:LinkButton>
        </div>
    </div>
    <%--<td id="FeeW1" onclick="GoToPage('FeeW1','Pay','')" onmouseover="HiLight('FeeW1','In')" onmouseout="HiLight('FeeW1','Out')">--%>
    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="btnAbandonSession" runat="server" OnClick="btnAbandonSession_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IWL")%></span></asp:LinkButton>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlApproved" runat="server" Visible="false">
    <table width="75%" border="0" align="center" cellpadding="6" cellspacing="0">
        <tr>
            <td align="left">
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CCCSW")%> $<asp:Label ID="lblCharAmt1" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="10%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto">
        <tr>
            <td align="left">
                <div class="dvbtncontainer">
                    <div class="lnkbutton">
                        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlDisApproved" runat="server" Visible="false">
    <table width="75%" border="0" align="center" cellpadding="6" cellspacing="0">
        <tr>
            <td align="left">
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CCCSWL")%> $<asp:Label ID="GetCharAmt2" runat="server"></asp:Label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|DOL")%>
            </td>
        </tr>
    </table>
    <table width="10%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0 auto">
        <tr>
            <td align="left">
                <div class="dvbtncontainer">
                    <div class="lnkbutton">
                        <asp:LinkButton ID="btnContinueDisApproved" runat="server" OnClick="btnContinueDisApproved_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hdnValues" runat="server" />
</asp:Content>
