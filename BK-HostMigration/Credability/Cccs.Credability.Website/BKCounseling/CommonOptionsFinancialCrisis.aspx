﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionsFinancialCrisis.aspx.cs" Inherits="Cccs.Credability.Website.BKCounseling.CommonOptionsFinancialCrisis" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Common Options Financial Crisis BKCounseling" %>

<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DYC")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HTY")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|UYA")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|EIAD")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|NDS")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|CYM")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|FFB")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WOT")%></p>
	<p>
		<ul>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|PLA")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|PCO")%>
			</li>
		</ul>
	</p>
	<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
	<div class="dvbtncontainer">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnReturntoPreviousPage" runat="server" CssClass="previous" OnClick="btnReturntoPreviousPage_Click" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnReturntoPrevious" runat="server" OnClick="btnReturntoPrevious_Click" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
		</div>
		<div class="lnkbutton">
			<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
		</div>
	</div>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>