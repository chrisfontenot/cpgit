﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.Shared.Pages
{
    public partial class GetRequiredFeeInformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RedirectLocation.Value = HttpContext.Current.Request["redirect"];
                this.txtAttorneyCode.Enabled = true;
                this.txtZipCode.Enabled = true;
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {

            //if we do not have an attorney code, then use the default
            if (this.NoAttorneyCode.Checked)
            {
                SessionState.FirmID = "9999";
            }            
            //if we have an attorney code, then one must be supplied!
            else {
                if (!String.IsNullOrEmpty(this.txtAttorneyCode.Text))
                {
                    SessionState.FirmID = this.txtAttorneyCode.Text;

                    int firmId = 0;
                    if(int.TryParse(this.txtAttorneyCode.Text, out firmId))
                    {
                        var result = App.Debtplus.GetEscrow(firmId, App.WebsiteCode);

                        if (result != null)
                        {
                            SessionState.EscrowProbonoValue = result.EscrowProBono;
                            SessionState.Escrow = result.Escrow;

                            if (result.IsLegalAid)
                            {
                                SessionState.Escrow6 = "Y";
                            }
                        }
                    }
                }
            }

            //we must have a non-empty zipcode
            if (!String.IsNullOrEmpty(this.txtZipCode.Text))
            {                
                SessionState.PrimaryZipCode = this.txtZipCode.Text;
            }

            //if zipcode && (attorney code set || user doesn't have a code)
            //then the original page should continue
            //otherwise it will re-direct back to this page
            Response.Redirect(RedirectLocation.Value);
        }

    }
}