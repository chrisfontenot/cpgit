﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountSummary.ascx.cs" Inherits="Cccs.Credability.Website.Controls.ClientAccessControls.AccountSummary" %>
<%@ Register Src="~/Controls/ClientAccessControls/TopNavigationTabCA.ascx" TagName="TopNav" TagPrefix="UC" %>
<div class="innerPad18">
<UC:TopNav id="ucTopNav" runat="server"></UC:TopNav>
<div class="clear"> </div>
<div>
  <div class="iconLink">
    <ul>
      <li><input name="" type="button" value="View My Statements" class="btnorange" /></li>      
      <li><input name="" type="button" value="Make a Deposit Now" class="btnorange" /></li>
    </ul>
  </div>
  <h1 class="txtListing">Account Summary</h1>
</div>
<div class="clear"> </div>	
<div class="verSpaceFx"> </div>
<div class="dvDataContainer">
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Next Due Date:</b></div>
      <div class="dvRowRht"> <span id="spnNextDueDate" runat="server">05/15/10</span> </div>
    </div>
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Auto-Draft Deposit Active?</b></div>
      <div class="dvRowRht"> <span id="spnAutoDraftDepositActive" runat="server">Y</span> </div>
    </div>
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Amount Due:</b></div>
      <div class="dvRowRht"> <span id="spnAmountDue" runat="server">$ 625.00</span> </div>
    </div>
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Original Debt Total:</b></div>
      <div class="dvRowRht"> <span id="spnOriginalDebtTotal" runat="server">$ 600.00</span> </div>
    </div>
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Plan Start Date:</b></div>
      <div class="dvRowRht"> <span id="spnPlanStartDate" runat="server">04/15/08</span> </div>
    </div>
<div class="dvRow">
      <div class="dvRowLft"> <span class="req">&nbsp;</span><b>Total Pd to Creditors this mo:</b></div>
      <div class="dvRowRht"> <span id="spnTotalPd" runat="server">Y</span> </div>
    </div>
</div>
	<div class="tblProject">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th>Verify Balance</th>
    <th>Creditor Name</th>
    <th>Account Number</th>
    <th>Balance Verified*</th>
    <th>Date Verified*</th>
    <th>Accepted Proposal on File**</th>
  </tr>
  <tr>
    <td><input name="" type="button" value="Verify" class="btnorange" /></td>
    <td>Bank of America</td>
    <td>**********4444</td>
    <td>Yes</td>
    <td>05/21/10</td>
    <td>No</td>
  </tr>
  <tr>
    <td><input name="" type="button" value="Verify" class="btnorange" /></td>
    <td>Search Financial</td>
    <td>***4567</td>
    <td>Yes</td>
    <td>04/29/10</td>
    <td>Yes</td>
  </tr>
</table>
	</div>
	
	<div class="clear"> </div>	
<div class="verSpaceFx"> </div>

<div class="note">
	<span>*</span><div>If your balance is not verified or the date verified is more than 6 months ago, please obtain the balance from your creditor's statement</div>
	<span>**</span><div>If your balance is not verified or the date verified is more than 6 months ago, please obtain the balance from your creditor's statement</div>
</div>




</div>