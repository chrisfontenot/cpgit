﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNavigationTabCA.ascx.cs" Inherits="Cccs.Credability.Website.Controls.ClientAccessControls.TopNavigationTabCA" %>
<div class="RoundTab">
  <ul style="width:95%;">
    <li class="active"><a href="#"><span>Debt</span></a></li>
    <li><a href="#"><span>Personal Information</span></a></li>
    <li><a href="#"><span>Account Transactions</span></a></li>
    <li><a href="#"><span>Account Summary</span></a></li>
  </ul>
</div>