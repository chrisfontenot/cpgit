﻿using System;
using System.Web.UI;
using Cccs.Credability.Website.Code;
using System.ComponentModel;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcNavigationControl : CounselingUcBase
	{
		public delegate void delegateOnContinue();
		public event delegateOnContinue OnContinue;
		public event delegateOnContinue OnSaveAndExit;
		public event delegateOnContinue OnSaveAndSubmitToQueue;

		protected new void Page_Load(object sender, EventArgs e)
		{
			btnContinue.Visible = NextButtonIsVisible;
			btnReturnToPrevious.Visible = PreviousButtonIsVisible;
			btnSaveAndExit.Visible = SaveButtonIsVisible;
			btnSubmit.Visible = SaveAndSubmitToQueueButtonIsVisible;
			base.Page_Load(sender, e);
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				if(OnContinue != null)
				{
					OnContinue();
				}
				NavigateContinue();
			}
		}

		protected void btnSaveAndExit_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				if(OnSaveAndExit != null)
				{
					OnSaveAndExit();
				}
				NavigateExit();
			}
		}

		protected void btnSaveAndExitAndSubmitToQueue_Click(object sender, EventArgs e)
		{
			if(OnSaveAndSubmitToQueue != null)
			{
				OnSaveAndSubmitToQueue();
			}
			SubmitToQueue();
		}

		protected void btnReturnToPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		public void ValidationGroupSet(string ValidationGroup)
		{
			btnContinue.ValidationGroup = ValidationGroup;
			btnSaveAndExit.ValidationGroup = ValidationGroup;
		}

		private bool _previousIsVisible = true;
		[DefaultValue(true)]
		public bool PreviousButtonIsVisible
		{
			get { return _previousIsVisible; }
			set { _previousIsVisible = value; }
		}

		private bool _nextIsVisible = true;
		[DefaultValue(true)]
		public bool NextButtonIsVisible
		{
			get { return _nextIsVisible; }
			set { _nextIsVisible = value; }
		}

		private bool _saveIsVisible = true;
		[DefaultValue(true)]
		public bool SaveButtonIsVisible
		{
			get { return _saveIsVisible; }
			set { _saveIsVisible = value; }
		}

		private bool _saveAndSubmitToQueueVisible = false;
		[DefaultValue(false)]
		public bool SaveAndSubmitToQueueButtonIsVisible
		{
			get { return _saveAndSubmitToQueueVisible; }
			set { _saveAndSubmitToQueueVisible = value; }
		}
	}
}