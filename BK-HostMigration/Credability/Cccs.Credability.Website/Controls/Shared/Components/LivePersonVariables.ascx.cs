﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls
{
    public partial class LivePersonBCVariables : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (App.WebsiteCode != Cccs.Identity.Website.BKDE)
            {
                JavaScriptBlockLiteral.Text = App.LivePersonVariableScriptBlock(SessionState.UserId.Value, SessionState.ClientNumber.Value);
            }
        }
    }
}