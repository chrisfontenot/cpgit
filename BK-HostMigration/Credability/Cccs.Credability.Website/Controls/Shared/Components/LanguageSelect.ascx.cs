﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Translation;
namespace Cccs.Credability.Website.Controls
{
	public partial class LanguageSelect : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				if (App.IsSpanishSupported)
				{
					Initialize();
				}
				else
				{
					Visible = false;
				}
			}
		}

		private void Initialize()
		{
			Language[] languages = App.Translation.LanguagesGet();

			if (languages != null)
			{
				Language language =
				(
						from lang in languages
						where (string.Compare(SessionState.LanguageCode, lang.LanguageCode) != 0)
						select lang
				).FirstOrDefault();

				if (language != null)
				{
					LinkButtonLanguageSelect.Text = App.Translate("Sandbox|LanguageSelect");
					LinkButtonLanguageSelect.Attributes["language_code"] = language.LanguageCode;
				}
			}
		}

		protected void LinkButtonLanguageSelect_Click(object sender, EventArgs e)
		{
			string language_code = ((WebControl)sender).Attributes["language_code"];

			if (!string.IsNullOrEmpty(language_code))
			{
				SessionState.LanguageCode = language_code;
				Response.Redirect(Request.RawUrl);
			}
		}
	}
}