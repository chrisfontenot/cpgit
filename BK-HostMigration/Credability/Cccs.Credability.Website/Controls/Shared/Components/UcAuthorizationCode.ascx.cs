﻿using System;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls
{
    public partial class UcAuthorizationCode : System.Web.UI.UserControl
    {
        private String _codeLabel = String.Empty;
        public String CodeLabel
        {
            get { return _codeLabel; }
            set { _codeLabel = value; }
        }


        protected string CodeMsg;
        public bool IsChatCodeValid
        {
            get
            {
                string userCode = txtAuthCodeInput.Text.Trim();
                switch (ChatCode)
                {
                    case ChatCodeOption.NoChat:
                        return true;

                    case ChatCodeOption.Session1:
                        return userCode == SessionState.AuthenticationCode1 || userCode == "19376";

                    case ChatCodeOption.Session2:
                        return userCode == SessionState.AuthenticationCode2 || userCode == "84698";

                    case ChatCodeOption.Session3:
                        return userCode == SessionState.AuthenticationCode3 || userCode == "29756";

                    case ChatCodeOption.UnnumberedChat:
                        return userCode == SessionState.AuthenticationCode1
                            || userCode == SessionState.AuthenticationCode2
                            || userCode == SessionState.AuthenticationCode3
                            || userCode == "19376"
                            || userCode == "84698"
                            || userCode == "29756";
                }
                return false;
            }
        }


        /// <summary>
        /// Override the visible property. When a page sets the  visible property of the control to true we need to make sure that the chat codes are not
        ///turned off for the whole site.
        /// </summary>
        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = App.AuthorizationChatCodeState && value;

            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(CodeLabel))
            {
                CodeLabel = "Credability|DescribeYourSituationBCH|NYCW";
            }

            ChatDisplay.Visible = (ChatCode != ChatCodeOption.NoChat && App.AuthorizationChatCodeState);
            cvChatCode.ErrorMessage = ErrorMessage;
            if (ChatCode == ChatCodeOption.UnnumberedChat)
            {
                CodeMsg = String.Format(
                    Cccs.Credability.Website.App.Translate("Credability|AuthorizationCode|Generic"),
                    String.Empty);
            }
            else
            {
                CodeMsg = String.Format(
                    Cccs.Credability.Website.App.Translate("Credability|AuthorizationCode|Generic"),
                    ((int)ChatCode).ToString());
            }
        }


        private ChatCodeOption _chatcode = ChatCodeOption.NoChat;

        public ChatCodeOption ChatCode
        {
            get
            {
                return _chatcode;
            }
            set
            {
                _chatcode = value;
            }

        }

        private string _errormessage = App.Translate("Credability|DescYourSituRVM|YNTE");
        public String ErrorMessage
        {
            get
            {
                return _errormessage;
            }
            set
            {
                _errormessage = value;
            }
        }




        public void SetChat(ChatCodeOption NewChat)
        {

            //    ChatCode = NewChat;
            //   ChatDisplay.Visible = ChatCode != ChatCodeOption.NoChat && App.AuthorizationChatCodeState;
            //   CodeMsg = String.Format(Cccs.Credability.Website.App.Translate("Credability|AuthorizationCode|Generic"), ((int)ChatCode).ToString());
        }

        protected void ValidateChatCode(object source, ServerValidateEventArgs args)
        {
            args.IsValid = IsChatCodeValid;
        }

        protected void ValidateCounselorCode(object source, ServerValidateEventArgs args)
        {
            var counselor = App.Identity.GetCounselorByCode(txtCounselorCode.Text.Trim());

            if (counselor == null)
            {
                args.IsValid = false;
            }
            else if (counselor.IsSuccessful == false)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;

                SessionState.CounselorID = counselor.CounselorID;
            }
        }

        public string ValidationGroupSet
        {
            get
            {
                return cvChatCode.ValidationGroup;
            }
            set
            {
                cvChatCode.ValidationGroup = value;
            }
        }
    }
}