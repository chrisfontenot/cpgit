﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcFontControler.ascx.cs" Inherits="Cccs.Credability.Website.Controls.RvmControls.UcFontControler" %>
<div class="dvFontControl">
	<span class="dvFontControlLabel">&nbsp;|&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|Masterpage|TS")%>:</span>
	<span id="FontUp"><a href="#" onclick="javascript: FontChange('+'); return false;"><img src="/images/FontUp.gif" border="0"></a></span>
	<span id="FontDown"><a href="#" onclick="javascript: FontChange('-'); return false;"><img src="/images/FontDn.gif" border="0"></a></span>
</div>