﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcNavigationControl.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcNavigationControl" %>
<div class="lnkbutton">
	<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click" CausesValidation="false">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
</div>
<div class="lnkbutton">
	<asp:LinkButton ID="btnSubmit" class="save" Visible="false" runat="server" ToolTip="Submit" CausesValidation="false" OnClick="btnSaveAndExitAndSubmitToQueue_Click">      
        <span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SendToCounselor")%></span></asp:LinkButton>
</div>
<div class="lnkbutton">
	<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click" CausesValidation="true" ValidationGroup="userprofile">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
</div>
<div class="lnkbutton">
	<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true" ValidationGroup="userprofile">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
</div>
