﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAuthorizationCode.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcAuthorizationCode" %>
<div id="ChatDisplay" runat="server" visible="false" style="display:block;">
	<%= Cccs.Credability.Website.App.Translate(CodeLabel)%>
	<%= Cccs.Credability.Website.CommonFunction.CreateChatLink(Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CH"))%>
	<br />
	<%=CodeMsg%>
	<asp:TextBox ID="txtAuthCodeInput" runat="server" CssClass="mgrT5"></asp:TextBox>
	<asp:CustomValidator ID="cvChatCode"  runat="server" OnServerValidate="ValidateChatCode" Text="!" ValidateEmptyText="true" ValidationGroup="userprofile"></asp:CustomValidator>
	<%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|CVOT")%>
    <div style="display:<%=App.WebsiteCode == Cccs.Identity.Website.BKC ? "block" : "none" %>">
    <span>Counselor Code: </span><asp:TextBox ID="txtCounselorCode" runat="server" CssClass="mgrT5"></asp:TextBox>
	<asp:CustomValidator ID="cvCounselorCode"  runat="server" OnServerValidate="ValidateCounselorCode" Text="!" ValidateEmptyText="true" ValidationGroup="userprofile"></asp:CustomValidator>
    </div>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKDesYourSit|CVOT2")%>
</div>