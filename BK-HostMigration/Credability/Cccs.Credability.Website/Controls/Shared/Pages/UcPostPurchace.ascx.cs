﻿using System;
using System.Web.UI;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.BchControls
{
	public partial class UcPostPurchace : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			btnReturnToPreviousPage.ToolTip = App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = App.Translate("Credability|RVMButtons|Continue");
			btnSaveExit.ToolTip = App.Translate("Credability|RVMButtons|SaveContinueLater");

			if(!IsPostBack)
			{
				GetCommanOptions();
			}
			base.Page_Load(sender, e);
		}

		public void GetCommanOptions()
		{
			CommonOptionBKCounseling CommonOptionBKCounselings = App.Credability.CommonOptionBKCounselingContactDetailsGet(SessionState.ClientNumber.Value);

			if(CommonOptionBKCounselings != null)
			{
				txtMortHolder.Text = (SessionState.CompanyName != null) ? SessionState.CompanyName : string.Empty;
				txtLoanNumber.Text = CommonOptionBKCounselings.LoanNumber.Clean();
				ShowMortType(CommonOptionBKCounselings.MortType);

				txtMortYears.Text = CommonOptionBKCounselings.MortYears.ToString("F0") + " years";
				txtMortDate.Text = CommonOptionBKCounselings.MortDate.Clean();
				txtMortRate.Text = (CommonOptionBKCounselings.MortRate / 100).ToString("P");
				ShowInterestType(CommonOptionBKCounselings.RateType);

				SetInterestRateText();

				txtOrigBal.Text = CommonOptionBKCounselings.OrigBal.ToString(App.CurrencyFormat);
				txtOweHome.Text = CommonOptionBKCounselings.OweHome.ToString(App.CurrencyFormat);
				txtValHome.Text = CommonOptionBKCounselings.ValHome.ToString(App.CurrencyFormat);
				txtMopmt.Text = CommonOptionBKCounselings.MoPmt.ToString(App.CurrencyFormat);
				txtAmtavail.Text = CommonOptionBKCounselings.AmtAvail.ToString(App.CurrencyFormat);
				txtLastcontactdate.Text = CommonOptionBKCounselings.LastContactDate.Clean();
				txtLastcontactdesc.Text = CommonOptionBKCounselings.LastContactDesc.Clean();
				txtRepayplan.Text = CommonOptionBKCounselings.RepayPlan.Clean();

				txtSecMort.Text = GetHasSecondaryServicerText(CommonOptionBKCounselings.SecondaryHolder);
				if(!string.IsNullOrEmpty(CommonOptionBKCounselings.SecondaryHolder.Clean()))
				{
					pnlScondaryMort.Visible = true;
					txtSecondaryAm.Text = CommonOptionBKCounselings.SecondaryAmt.ToString(App.CurrencyFormat);
					txtSecondarystatus.Text = CommonOptionBKCounselings.SecondaryStatus.Clean();
					txtSecondaryholder.Text = CommonOptionBKCounselings.SecondaryHolder.Clean();
				}
				else
				{
					pnlScondaryMort.Visible = false;
					txtSecondaryAm.Text = String.Empty;
					txtSecondarystatus.Text = String.Empty;
					txtSecondaryholder.Text = String.Empty;
				}
				txtProp4Sale.Text = CommonOptionBKCounselings.Prop4Sale.Clean();
				txtNote4Close.Text = CommonOptionBKCounselings.Note4Close.Clean();
				txtSizeofhousehold.Text = CommonOptionBKCounselings.SizeofHouseHold.ToString();
				ShowMortCurrent(CommonOptionBKCounselings.MortCurrent);
				txtMosdelinq.Text = CommonOptionBKCounselings.MosDelinq.ToString("F0");
				ShowWhoInHouse(CommonOptionBKCounselings.WhoInHouse);
			}

			CommonOptionBKCounseling CommonOptionExtraHudBKCounselings = null;
			CommonOptionExtraHudBKCounselings = App.Credability.CommonOptionBKCounselingExtraHudGet(SessionState.ClientNumber.Value);

			if(CommonOptionExtraHudBKCounselings != null)
			{
				txtGovGrant.Text = CommonOptionExtraHudBKCounselings.GovGrant.Clean();
				txtIncludeIns.Text = CommonOptionExtraHudBKCounselings.IncludeIns.Clean();
				txtIncludeTax.Text = CommonOptionExtraHudBKCounselings.IncludeTax.Clean();
				txtStartIntRate.Text = (CommonOptionExtraHudBKCounselings.StartIntRate / 100).ToString("P");
				txtTopIntRate.Text = (CommonOptionExtraHudBKCounselings.TopIntRate / 100).ToString("P");
				txtMortRate1.Text = (CommonOptionBKCounselings.MortRate / 100).ToString("P");
				txtAnPropTax.Text = CommonOptionExtraHudBKCounselings.AnPropTax.ToString(App.CurrencyFormat);
				txtAnPropIns.Text = CommonOptionExtraHudBKCounselings.AnPropIns.ToString(App.CurrencyFormat);
			}
		}

		private string GetHasSecondaryServicerText(string value)
		{
			return string.IsNullOrEmpty(value.Clean()) ? App.Translate("No") : App.Translate("Yes");
		}

		private void ShowIncludeTax(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtIncludeTax.Text = App.Translate("Yes");
					break;
				case "N":
					txtIncludeTax.Text = App.Translate("No");
					break;

				default:
					txtIncludeTax.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowGovGrant(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtGovGrant.Text = App.Translate("Yes");
					break;
				case "N":
					txtGovGrant.Text = App.Translate("No");
					break;
				default:
					txtGovGrant.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowincludeInc(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtIncludeIns.Text = App.Translate("Yes");
					break;
				case "N":
					txtIncludeIns.Text = App.Translate("No");
					break;
				default:
					txtIncludeIns.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowNotet4Close(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtNote4Close.Text = App.Translate("Yes");
					break;
				case "N":
					txtNote4Close.Text = App.Translate("No");
					break;
				default:
					txtNote4Close.Text = App.Translate("None Selected");
					break;
			}
		}

		private void Showpropert4Sale(string value)
		{
			switch(value.Trim())
			{
				case "Y":
					txtProp4Sale.Text = App.Translate("Yes");
					break;
				case "N":
					txtProp4Sale.Text = App.Translate("No");
					break;
				default:
					txtProp4Sale.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowWhoInHouse(string value)
		{
			switch(value.Clean())
			{
				case "01":
					txtWhoInHouse.Text = App.Translate("Owner Property Occupied");
					break;
				case "02":
					txtWhoInHouse.Text = App.Translate("Owner Property Vacant");
					break;
				case "03":
					txtWhoInHouse.Text = App.Translate("Investor Property Occupied");
					break;
				case "04":
					txtWhoInHouse.Text = App.Translate("Investor Property Vacant");
					break;
				default:
					txtWhoInHouse.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowMortCurrent(int value)
		{
			switch(value)
			{
				case 1:
					txtMortcurrent.Text = App.Translate("Yes");
					break;
				case 0:
					txtMortcurrent.Text = App.Translate("No");
					break;
				default:
					txtMortType.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowMortType(string value)
		{
			switch(value.Trim())
			{
				case "CONV":
					txtMortType.Text = App.Translate("Conventional");
					break;
				case "FHA":
					txtMortType.Text = App.Translate("FHA");
					break;
				case "VA":
					txtMortType.Text = App.Translate("VA");
					break;
				default:
					txtMortType.Text = App.Translate("None Selected");
					break;
			}
		}

		private void ShowInterestType(string value)
		{
			switch(value.Trim())
			{
				case "F":
					txtRateType.Text = App.Translate("Fixed");
					break;
				case "A":
					txtRateType.Text = App.Translate("Standard Arm");
					break;
				case "I":
					txtRateType.Text = App.Translate("Interest Only");
					break;
				case "O":
					txtRateType.Text = App.Translate("Option Arm");
					break;
				default:
					txtRateType.Text = App.Translate("None Selected");
					break;
			}
		}

		protected void click_btnReturnToPreviousPage(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void click_btnContinue(object sender, EventArgs e)
		{
			//if(CommonFunction.IsValidChatCode(ChatCodeOption.Session1, txtAuthCodeInput.Text))
			if(Page.IsValid)
			{
				NavigateContinue();
			}
		}

		protected void click_btnSaveExit(object sender, EventArgs e)
		{
			NavigateExit();
		}

		private void SetInterestRateText()
		{
			if(txtRateType.Text == "Fixed" || txtRateType.Text == "Interest Only")
			{
				pnlArmInrest.Visible = false;
				pnlFixed.Visible = true;
			}
			else
			{
				pnlArmInrest.Visible = true;
				pnlFixed.Visible = false;
			}
		}
	}
}