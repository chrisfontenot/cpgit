﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcMonthlyIncome : CounselingUcBase
	{
		public bool IsSubmitButtonVisible
		{
			get
			{
				return btnSubmit.Visible;
			}
			set
			{
				btnSubmit.Visible = value;
			}
		}

		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
                MonthlyChecked.Checked = true;
                YearlyChecked.Checked = false;

				SetTranslations();
				UserMonthlyIncome MonthlyIncome = App.Credability.UserMonthlyIncomeGet(SessionState.ClientNumber.Value);
				if(MonthlyIncome != null)
				{
					txtPriIncGross.Text = MonthlyIncome.GrossIncome.ToString(App.CurrencyFormat);
					txtSecIncGross.Text = MonthlyIncome.GrossIncome2.ToString(App.CurrencyFormat);

					txtPriIncPtGross.Text = MonthlyIncome.PartTimeGrossIncome.ToString(App.CurrencyFormat);
					txtSecIncPtGross.Text = MonthlyIncome.PartTimeGrossIncome2.ToString(App.CurrencyFormat);

					txtPriIncAlimonyGross.Text = MonthlyIncome.AlimonyIncome.ToString(App.CurrencyFormat);
					txtSecIncAlimonyGross.Text = MonthlyIncome.AlimonyIncome2.ToString(App.CurrencyFormat);

					txtPriIncChildSupportGross.Text = MonthlyIncome.ChildSupportIncome.ToString(App.CurrencyFormat);
					txtSecIncChildSupportGross.Text = MonthlyIncome.ChildSupportIncome2.ToString(App.CurrencyFormat);

					txtPriIncGovAstGross.Text = MonthlyIncome.GovAssistIncome.ToString(App.CurrencyFormat);
					txtSecIncGovAstGross.Text = MonthlyIncome.GovAssistIncome2.ToString(App.CurrencyFormat);

					txtPriIncOtherGross.Text = MonthlyIncome.OtherIncome.ToString(App.CurrencyFormat);
					txtSecIncOtherGross.Text = MonthlyIncome.OtherIncome2.ToString(App.CurrencyFormat);

					dvTotalMonthlyGrossIncomePrimary.InnerHtml = MonthlyIncome.GrossIncomeTotal.ToString(App.CurrencyFormat);
					PriIncTotalGross.Value = MonthlyIncome.GrossIncomeTotal.ToString(App.CurrencyFormat);

					dvTotalMonthlyGrossIncomeSec.InnerHtml = MonthlyIncome.GrossIncomeTotal2.ToString(App.CurrencyFormat);
					SecIncTotalGross.Value = MonthlyIncome.GrossIncomeTotal2.ToString(App.CurrencyFormat);

					txtPriIncNet.Text = MonthlyIncome.NetIncome.ToString(App.CurrencyFormat);
					txtSecIncNet.Text = MonthlyIncome.NetIncome2.ToString(App.CurrencyFormat);

					txtPriIncPtNet.Text = MonthlyIncome.PartTimeNetIncome.ToString(App.CurrencyFormat);
					txtSecIncPtNet.Text = MonthlyIncome.PartTimeNetIncome2.ToString(App.CurrencyFormat);

					txtPriIncAlimonyNet.Text = MonthlyIncome.AlimonyIncome.ToString(App.CurrencyFormat);
					txtSecIncAlimonyNet.Text = MonthlyIncome.AlimonyIncome2.ToString(App.CurrencyFormat);

					txtPriIncChildSupportNet.Text = MonthlyIncome.ChildSupportIncome.ToString(App.CurrencyFormat);
					txtSecIncChildSupportNet.Text = MonthlyIncome.ChildSupportIncome2.ToString(App.CurrencyFormat);

					txtPriIncGovAstNet.Text = MonthlyIncome.GovAssistIncome.ToString(App.CurrencyFormat);
					txtSecIncGovAstNet.Text = MonthlyIncome.GovAssistIncome2.ToString(App.CurrencyFormat);

					txtPriIncOtherNet.Text = MonthlyIncome.OtherIncome.ToString(App.CurrencyFormat);
					txtSecIncOtherNet.Text = MonthlyIncome.OtherIncome2.ToString(App.CurrencyFormat);

					dvNetHouseholdIncomePrimary.InnerHtml = MonthlyIncome.NetIncomeTotal.ToString(App.CurrencyFormat);
					PriIncTotalNet.Value = MonthlyIncome.NetIncomeTotal.ToString(App.CurrencyFormat);

					dvNetHouseholdIncomeSec.InnerHtml = MonthlyIncome.NetIncomeTotal2.ToString(App.CurrencyFormat);
					SecIncTotalNet.Value = MonthlyIncome.NetIncomeTotal2.ToString(App.CurrencyFormat);
				}
			}
			base.Page_Load(sender, e);
		}

		private bool UpdateUserIncome()
		{
            var convertToMonthly = YearlyChecked.Checked;

			UserMonthlyIncome Income = new UserMonthlyIncome();
			Income.ClientNumber = SessionState.ClientNumber.Value;
			Income.GrossIncome = txtPriIncGross.Text.ToFloat(0);
            Income.GrossIncome = convertToMonthly ? Income.GrossIncome / 12 : Income.GrossIncome;
			
            Income.GrossIncome2 = txtSecIncGross.Text.ToFloat(0);
            Income.GrossIncome2 = convertToMonthly ? Income.GrossIncome2 / 12 : Income.GrossIncome2;
			
            Income.NetIncome = txtPriIncNet.Text.ToFloat(0);
            Income.NetIncome = convertToMonthly ? Income.NetIncome / 12 : Income.NetIncome;
			
            Income.NetIncome2 = txtSecIncNet.Text.ToFloat(0);
            Income.NetIncome2 = convertToMonthly ? Income.NetIncome2 / 12 : Income.NetIncome2;
			
            Income.PartTimeGrossIncome = txtPriIncPtGross.Text.ToFloat(0);
            Income.PartTimeGrossIncome = convertToMonthly ? Income.PartTimeGrossIncome / 12 : Income.PartTimeGrossIncome;
			
            Income.PartTimeGrossIncome2 = txtSecIncPtGross.Text.ToFloat(0);
            Income.PartTimeGrossIncome2 = convertToMonthly ? Income.PartTimeGrossIncome2 / 12 : Income.PartTimeGrossIncome2;
			
            Income.PartTimeNetIncome = txtPriIncPtNet.Text.ToFloat(0);
            Income.PartTimeNetIncome = convertToMonthly ? Income.PartTimeNetIncome / 12 : Income.PartTimeNetIncome;

			Income.PartTimeNetIncome2 = txtSecIncPtNet.Text.ToFloat(0);
            Income.PartTimeNetIncome2 = convertToMonthly ? Income.PartTimeNetIncome2 / 12 : Income.PartTimeNetIncome2;

			Income.AlimonyIncome = txtPriIncAlimonyGross.Text.ToFloat(0);
            Income.AlimonyIncome = convertToMonthly ? Income.AlimonyIncome / 12 : Income.AlimonyIncome;

			Income.AlimonyIncome2 = txtSecIncAlimonyGross.Text.ToFloat(0);
            Income.AlimonyIncome2 = convertToMonthly ? Income.AlimonyIncome2 / 12 : Income.AlimonyIncome2;

			Income.ChildSupportIncome = txtPriIncChildSupportGross.Text.ToFloat(0);
            Income.ChildSupportIncome = convertToMonthly ? Income.ChildSupportIncome / 12 : Income.ChildSupportIncome;

			Income.ChildSupportIncome2 = txtSecIncChildSupportGross.Text.ToFloat(0);
            Income.ChildSupportIncome2 = convertToMonthly ? Income.ChildSupportIncome2 / 12 : Income.ChildSupportIncome2;

			Income.GovAssistIncome = txtPriIncGovAstGross.Text.ToFloat(0);
            Income.GovAssistIncome = convertToMonthly ? Income.GovAssistIncome / 12 : Income.GovAssistIncome;

			Income.GovAssistIncome2 = txtSecIncGovAstGross.Text.ToFloat(0);
            Income.GovAssistIncome2 = convertToMonthly ? Income.GovAssistIncome2 / 12 : Income.GovAssistIncome2;

			Income.OtherIncome = txtPriIncOtherGross.Text.ToFloat(0);
            Income.OtherIncome = convertToMonthly ? Income.OtherIncome / 12 : Income.OtherIncome;

			Income.OtherIncome2 = txtSecIncOtherGross.Text.ToFloat(0);
            Income.OtherIncome2 = convertToMonthly ? Income.OtherIncome2 / 12 : Income.OtherIncome2;

			UserMonthlyIncomeResult IncomeResult = App.Credability.UserMonthlyIncomeUpadte(Income);
			return IncomeResult.IsSuccessful;
		}

		protected void imgBtnDocumentingContinue_Click(object sender, EventArgs e)
		{
			if(UpdateUserIncome() && Page.IsValid)
			{
				NavigateContinue();
			}
		}

		protected void imgBtnDocumentingPrevious_Click(object sender, EventArgs e)
		{
			if(UpdateUserIncome())
			{
				NavigatePrevious();
			}
		}

		protected void imgBtnDocumentingSaveExit_Click(object sender, EventArgs e)
		{
			if(UpdateUserIncome())
			{
				NavigateExit();
			}
		}

		public FormSaveInforResult SaveFormValue()
		{
			FormSaveInfor FormMod = App.Credability.SavedFormInfoGet(SessionState.ClientNumber.Value);
			if(FormMod != null)
			{
				FormMod.Form3 = 1;
				return App.Credability.SaveFormInfo(FormMod);
			}
			return null;
		}

		protected void SetTranslations()
		{
			imgBtnDocumentingPrevious.ToolTip = App.Translate("Credability|RVMButtons|Previous");
			imgBtnDocumentingContinue.ToolTip = App.Translate("Credability|RVMButtons|Continue");
			imgBtnDocumentingSaveExit.ToolTip = App.Translate("Credability|RVMButtons|SaveContinueLater");
			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
			imgCal1.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal2.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal3.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal4.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal5.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal6.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal7.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal8.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal9.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal10.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal11.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
			imgCal12.ToolTip = App.Translate("Credability|IncomeDocumentationInfoRVM|CHT");
		}

		protected void btnSaveAndExitAndSubmitToQueue_Click(object sender, EventArgs e)
		{
			SubmitToQueue();
		}
	}
}