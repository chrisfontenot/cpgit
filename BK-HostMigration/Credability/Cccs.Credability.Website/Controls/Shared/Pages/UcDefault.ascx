﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDefault.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcDefault" %>
<p>
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|RVMDefault|DefaultTtile")%></h1>
</p>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|RVMDefault|DefaultFirstParagraph")%>
	<br />
</p>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|RVMDefault|DefaultSecondParagraph")%>
	<br />
</p>
<p>
	<br />
	<b>
		<%= Cccs.Credability.Website.App.Translate("Credability|RVMDefault|DefaultThirdParagraph")%></b>
</p>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
