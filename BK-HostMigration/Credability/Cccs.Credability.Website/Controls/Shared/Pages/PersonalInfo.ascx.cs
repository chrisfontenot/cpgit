﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;
using Cccs.Web;
using System.Text;

namespace Cccs.Credability.Website.Controls
{
	public partial class PersonalInfo : System.Web.UI.UserControl
	{
		private string priTitleTransKey = "Credability|UserProfile|Owner";
		private string secTitleTransKey = "Credability|UserProfile|Co-OwnerRequired";

		public string PrimaryTitleTranslationKey
		{
			get { return priTitleTransKey; }
			set { /*priTitleTransKey = value;*/ }
		}

		public string SecondaryTitleTranslationKey
		{
			get { return secTitleTransKey; }
			set { /*secTitleTransKey = value;*/ }
		}

		public bool IsPrimaryEmploymentVisible
		{
			get { return pnlPriEmpEdu.Visible; }
			set { pnlPriEmpEdu.Visible = value; }
		}

		public bool IsSecondaryExtraVisible
		{
			get { return pnlCoOwnerExtra.Visible; }
			set { pnlCoOwnerExtra.Visible = value; }
		}

		public bool ValidateAges
		{
			get { return CheckAges.GetValueOrDefault(false); }
			set { CheckAges = value; }
		}

		public bool IsSecondaryEmploymentVisible
		{
			get { return pnlCoEmp.Visible; }
			set { pnlCoEmp.Visible = value; }
		}

		public bool IsPrimaryEmailVisible
		{
			get { return pnlEmail.Visible; }
			set { pnlEmail.Visible = value; }
		}

		public PersonalInfoData Data
		{
			get { return RetrieveData(); }
			set { BindingData(value); }
		}

		public PersonalInfoData Validate()
		{
			PersonalInfoData data = Data;
			if (data != null)
			{
				IEnumerable<string> errs = data.ValidateData();
				if (errs != null && errs.Count<string>() > 0)
				{
					DisplayError(errs);
					data = null;
				}
			}
			return data;
		}

		public PersonalInfoData ProcessData()
		{
			PersonalInfoData data = Validate();
			if (data != null)
			{
				IEnumerable<string> errs = data.SaveData(
						SessionState.UserId.GetValueOrDefault(0), SessionState.Username);
				// It's important to keep those view states in case of partial save.
				UserAddressId = data.PrimaryUserAddress.AddressId;
				PrimaryUserDetailId = data.PrimaryUserDetail.UserDetailId;
				SecondaryUserDetailId = data.SecondUserDetail.UserDetailId;
				if (errs != null && errs.Count<string>() > 0)
				{
					DisplayError(errs);
					data = null;
				}
				else
				{
					errs = data.RequestCreditReport(SessionState.ClientNumber.Value,	App.ClientType,	SessionState.RefCode);
					if (errs != null && errs.Count<string>() > 0)
					{
						DisplayError(errs);
						data = null;
					}
				}
			}
			return data;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				//lblPriTitle.Text = App.Translate(PrimaryTitleTranslationKey);
				//lblCoTitle.Text = App.Translate(SecondaryTitleTranslationKey);
				InitializeTranslations();
				PersonalInfoData data = PersonalInfoData.GetData(SessionState.UserId.Value);
				BindingData(data);
			}
			ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSecondRequired", "FlipFlopBox();", true);
		}

		private bool? CheckAges
		{
			get { return ViewState.ValueGet<bool?>("CheckAges"); }
			set { ViewState.ValueSet("CheckAges", value); }
		}

		private long? UserAddressId
		{
			get { return ViewState.ValueGet<long?>("UserAddressID"); }
			set { ViewState.ValueSet("UserAddressID", value); }
		}

		private long? PrimaryUserDetailId
		{
			get { return ViewState.ValueGet<long?>("PrimaryUserDetailId"); }
			set { ViewState.ValueSet("PrimaryUserDetailId", value); }
		}

		private long? SecondaryUserDetailId
		{
			get { return ViewState.ValueGet<long?>("SecondaryUserDetailId"); }
			set { ViewState.ValueSet("SecondaryUserDetailId", value); }
		}

		private void DisplayError(IEnumerable<string> errs)
		{
			CommonFunction.ShowMultipleErrorMessage(dvTopErrorMessage, errs.ToArray<string>());
		}

		private void BindingData(PersonalInfoData data)
		{
			if (data != null)
			{
				BindingPrimaryUserDetail(data.PrimaryUserDetail);
				BindingSecondUserDetail(data.SecondUserDetail);
				BindingPrimaryUserAddress(data.PrimaryUserAddress);
			}
		}

		private PersonalInfoData RetrieveData()
		{
			PersonalInfoData data = new PersonalInfoData();
			data.PrimaryUserDetail = RetrievePrimaryUserDetail();
			data.SecondUserDetail = RetrieveSecondUserDetail();
			data.PrimaryUserAddress = RetrievePrimaryUserAddress();
			data.DiscardData = chkDiscardData.Checked;
			data.ValidateAges = ValidateAges;
			return data;
		}

		private Address RetrievePrimaryUserAddress()
		{
			Address address = new Address
			{
				UserId = SessionState.UserId.GetValueOrDefault(0),
				AddressId = UserAddressId.GetValueOrDefault(0),
				AddressType = "HOME",
				StreetLine1 = txtAddress.Text.Clean(),
				StreetLine2 = txtAddress2.Text.Clean(),
				City = txtCity.Text.Clean(),
				Zip = txtZipCode.Text.Clean(),
				State = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null,
			};
			return address;
		}

		private UserDetail RetrieveSecondUserDetail()
		{
			UserDetail user_detail_secondary = new UserDetail
			{
				UserId = SessionState.UserId.GetValueOrDefault(0),
				UserDetailId = SecondaryUserDetailId.GetValueOrDefault(0),
				IsPrimary = false,
				FirstName = txtCoOwnerFirstName.Text.Clean(),
				MiddleName = txtCoOwnerInitial.Text.Clean(),
				LastName = txtCoOwnerLastName.Text.Clean(),
				Ssn = txtCoOwnerSSN.Text.Clean(),
				BirthDate = txtCoOwnerDOB.Text.Clean().ToDateTime(null),
			};
			return user_detail_secondary;
		}

		private UserDetail RetrievePrimaryUserDetail()
		{
			UserDetail userDetail = new UserDetail
			{
				UserId = SessionState.UserId.GetValueOrDefault(0),
				UserDetailId = PrimaryUserDetailId.GetValueOrDefault(0),
				IsPrimary = true,
				FirstName = txtFirstName.Text.Clean(),
				LastName = txtLastName.Text.Clean(),
				MiddleName = txtInitial.Text.Clean(),
				Ssn = txtSSN.Text.Clean(),
				BirthDate = txtDOB.Text.Clean().ToDateTime(null),
				PhoneHome = txtPphone.Text.Clean(),
				PhoneWork = txtSphone.Text.Clean(),
			};
			switch (ddlHispanic.SelectedIndex)
			{
				case 1:
					userDetail.IsHispanic = true;
					break;
				case 2:
					userDetail.IsHispanic = false;
					break;
				default:
					userDetail.IsHispanic = null;
					break;
			}
			switch (ddlSex.SelectedIndex)
			{
				case 1:
					userDetail.IsMale = true;
					break;
				case 2:
					userDetail.IsMale = false;
					break;
				default:
					userDetail.IsMale = null;
					break;
			}
			if (!string.IsNullOrEmpty(ddlMaritalStatus.SelectedValue))
			{
				userDetail.MaritalStatus = new MaritalStatus { MaritalStatusCode = ddlMaritalStatus.SelectedValue[0] };
			}
			if (!string.IsNullOrEmpty(ddlRace.SelectedValue))
			{
				userDetail.Race = new Race { RaceCode = ddlRace.SelectedValue[0] };
			}
			return userDetail;
		}

		private void BindingPrimaryUserAddress(Address address)
		{
			if (address != null)
			{
				UserAddressId = address.AddressId;
				txtAddress.Text = address.StreetLine1.Clean();
				txtAddress2.Text = address.StreetLine2.Clean();
				txtCity.Text = address.City.Clean();
				txtZipCode.Text = address.Zip.Clean();
				ddlState.SelectedValue = address.State.Clean();
			}
		}

		private void BindingSecondUserDetail(UserDetail userDetail)
		{
			if (userDetail != null)
			{
				SecondaryUserDetailId = userDetail.UserDetailId;
				txtCoOwnerFirstName.Text = userDetail.FirstName.Clean();
				txtCoOwnerInitial.Text = userDetail.MiddleName.Clean();
				txtCoOwnerLastName.Text = userDetail.LastName.Clean();
				txtCoOwnerSSN.Text = userDetail.Ssn.Clean();
				txtCoOwnerDOB.Text = App.DateString(userDetail.BirthDate);
			}
		}

		void BindingPrimaryUserDetail(UserDetail userDetail)
		{
			if (userDetail != null)
			{
				PrimaryUserDetailId = userDetail.UserDetailId;
				txtFirstName.Text = userDetail.FirstName.Clean();
				txtInitial.Text = userDetail.MiddleName.Clean();
				txtLastName.Text = userDetail.LastName.Clean();
				txtSSN.Text = userDetail.Ssn.Clean();
				txtDOB.Text = App.DateString(userDetail.BirthDate);
				txtPphone.Text = userDetail.PhoneHome.Clean();
				txtSphone.Text = userDetail.PhoneWork.Clean();
				switch (userDetail.IsHispanic)
				{
					case true:
						ddlHispanic.SelectedIndex = 1;
						break;
					case false:
						ddlHispanic.SelectedIndex = 2;
						break;
					default: //null
						ddlHispanic.SelectedIndex = 0;
						break;
				}
				switch (userDetail.IsMale)
				{
					case true:
						ddlSex.SelectedIndex = 1;
						break;
					case false:
						ddlSex.SelectedIndex = 2;
						break;
					default: //null
						ddlSex.SelectedIndex = 0;
						break;
				}
				ddlMaritalStatus.SelectedValue = (userDetail.MaritalStatus != null) ?
						userDetail.MaritalStatus.MaritalStatusCode.ToString() : string.Empty;
				ddlRace.SelectedValue = (userDetail.Race != null) ?
						userDetail.Race.RaceCode.ToString() : string.Empty;
			}
		}

		private void InitializeTranslations()
		{
			GetSexPickList();
			GetStatePickList();
			GetRacePickList();
			GetHispanicPickList();

			HispanicPickListCoOwner();

			GetSexPickListCoOwner();
			GetMaritalSataus();

			EmploymentInfoPickList();
			PrimaryEducationPickList();
			ValidationControlTranslation();
		}

		private void PrimaryEducationPickList()
		{
			ddlPriEmp.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlPriEmp.Items, "PrimaryEducationDetails", SessionState.LanguageCode);
		}

		private void EmploymentInfoPickList()
		{
			ddlPriEdu.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlPriEdu.Items, "EmploymentInfo", SessionState.LanguageCode);
		}
		private void HispanicPickListCoOwner()
		{
			ddlCoHispanic.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlCoHispanic.Items, "Hispanic", SessionState.LanguageCode);
		}
		private void GetSexPickListCoOwner()
		{
			ddlCoOwnerSex.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlCoOwnerSex.Items, "Sex", SessionState.LanguageCode);
		}

		private void GetMaritalSataus()
		{
			MaritalStatus[] maritalStatus = App.Identity.MaritalStatusesGet(SessionState.LanguageCode);

			ddlCoOwnerMaritalStatus.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			ddlMaritalStatus.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			if ((maritalStatus != null) && (maritalStatus.Length > 0))
			{
				for (int i = 0; i < (maritalStatus.Length - 3); i++)
				{
					ddlCoOwnerMaritalStatus.Items.Add(new ListItem(maritalStatus[i].Text, maritalStatus[i].MaritalStatusCode.ToString() ?? maritalStatus[i].MaritalStatusCode.ToString())); // Uses Text if Value is null
					ddlMaritalStatus.Items.Add(new ListItem(maritalStatus[i].Text, maritalStatus[i].MaritalStatusCode.ToString() ?? maritalStatus[i].MaritalStatusCode.ToString())); // Uses Text if Value is null
				}
			}
		}

		private void GetHispanicPickList()
		{
			ddlHispanic.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlHispanic.Items, "Hispanic", SessionState.LanguageCode);
		}

		private void GetRacePickList()
		{
			Race[] race = App.Identity.RacesGet(SessionState.LanguageCode);
			ddlRace.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			ddlCoOwnerRace.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			if ((race != null) && (race.Length > 0))
			{
				for (int i = 0; i < race.Length; i++)
				{
					ddlRace.Items.Add(new ListItem(race[i].Text, race[i].RaceCode.ToString() ?? race[i].RaceCode.ToString())); // Uses Text if Value is null
					ddlCoOwnerRace.Items.Add(new ListItem(race[i].Text, race[i].RaceCode.ToString() ?? race[i].RaceCode.ToString())); // Uses Text if Value is null
				}
			}
		}

		private void GetSexPickList()
		{
			ddlSex.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlSex.Items, "Sex", SessionState.LanguageCode);
		}

		private void GetStatePickList()
		{
			ddlState.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			string[] statesCodes = App.Geographics.StateCodesGet();

			if (statesCodes != null)
			{
				foreach (string s in statesCodes)
				{
					ddlState.Items.Add(s);
				}
			}
		}

		private void ValidationControlTranslation()
		{
			rfvFname.ErrorMessage = App.Translate("Credability|UserProfileBCH|FNR");
			rfvLastName.ErrorMessage = App.Translate("Credability|UserProfileBCH|LNR");
			rfvSSN.ErrorMessage = App.Translate("Credability|UserProfileBCH|SSNR");
			rfvDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|DOBR");
			revtxtDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|DOBNPFR");
			rfvMaritalStatus.ErrorMessage = App.Translate("Credability|UserProfileBCH|MSR");
			rfvAddress.ErrorMessage = App.Translate("Credability|UserProfileBCH|UAR");
			rfvcity.ErrorMessage = App.Translate("Credability|UserProfileBCH|UCR");
			rfvState.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
			rfvZipCode.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
			rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
			rfvPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNR");
			RegularExpressionValidator_txtPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNWFR");
			RegularExpressionValidator_txtSphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|SPNWFR");
			cfvEmailAddress.ErrorMessage = App.Translate("Credability|UserProfileBCH|EAWRR");
			revtxtCoOwnerDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|CODOBWFR");
			RegularExpressionValidator_txtCoOwnerSSN.ErrorMessage = App.Translate("Credability|UserProfileBCH|COSSNWFR");
		}
	}
}