﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcFinancialRecap.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcFinancialRecap" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|UFS")%></h1>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
<p class="col_title">
	<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|IOFYT")%></p>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|LBS")%></p>
<div class="dvfinance">
	<div class="dvcollft">
		<h1>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Income")%></h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TMI")%><br />
					<a href="UserIncomeDocumentation.aspx?back=<%=ReturnTo%><%--UFSR--%>" class="Link">
						<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTM")%></a>
				</td>
				<td class="col2">
					<asp:Label ID="lblTotalMonthlyIncome" runat="server" Font-Bold="true"></asp:Label>
				</td>
			</tr>
		</table>
		<p>
			&nbsp;</p>
		<h1>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Debt")%></h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TUD")%>
				</td>
				<td class="col2">
					<asp:Label ID="TotalUnsecuredDebt" runat="server" Font-Bold="true"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TP")%><br />
					<a href="ListingYourDebts.aspx?back=<%=ReturnTo%>" class="Link">
						<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTM")%></a>
				</td>
				<td class="col2">
					<asp:Label ID="lblTotalPayments" runat="server" Font-Bold="true"></asp:Label>
				</td>
			</tr>
		</table>
	</div>
	<div class="dvcolrht">
		<h1>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Expenses")%></h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TME")%><br />
					<a href="MonthlyExpenses.aspx?back=<%=ReturnTo%><%--UFSR--%>" class="Link">
						<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTMA")%></a>
				</td>
				<td class="col2">
					<asp:Label ID="lblTotalMonthlyExp" runat="server" Font-Bold="true"></asp:Label>
				</td>
			</tr>
		</table>
		<p>
			&nbsp;</p>
		<h1>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|BS")%></h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TDI")%>
				</td>
				<td class="col2">
					<asp:Label ID="lblDiscretionaryIncome" runat="server" Font-Bold="true"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					&nbsp;
				</td>
				<td class="col2">
					&nbsp;<br />
					&nbsp;
				</td>
			</tr>
		</table>
	</div>
</div>
<p>
	&nbsp;
</p>
<div id="PrpText" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysisRecap|Para3")%>
	</p>
</div>
<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous" OnClick="btnPrevious_Click" ToolTip="Return To Previous Page" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>