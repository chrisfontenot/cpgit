﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcDebtListing : CounselingUcBase
	{
		public bool CheckWebSite = true;
		string ErrorMessageDelSucc = Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|RDS");
		string ErrorMessageNoRec = Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NRD");
		string ErrorMessageNoRecAvail = Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NRA");
		string ErrorMessageMissingRequiredData = Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NRF");
		UserDebtListingsResult UserDebtListingsResults = null;
		string ErrorConfirmDelete = Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|ConfirmDelete");

		protected new void Page_Load(object sender, EventArgs e)
		{
			//Get the user comment
			btnADDMoreCred.ToolTip = Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|AddMoreCreditors");
			btnDelete.ToolTip = Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|DeleteSelected");
			btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			LinkButton1.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
			cmpTxtDate.ErrorMessage = "Please correct the date";
			btnDelete.Attributes.Add("onclick", "return confirm('" + Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|ConfirmDelete") + "');");
			if(!Page.IsPostBack)
			{
				ShowUserDebtListingRecord();
				ShowDMPDate();
				GetUserComment();
				hdnSiteCode.Value = App.WebsiteCode;
			}
			string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
			if(url.ToLower().Contains("BKCounseling".ToLower()))
				CheckWebSite = false;
			else if(url.ToLower().Contains("PrePurchase".ToLower()))
				CheckWebSite = false;
			base.Page_Load(sender, e);
		}

		public void PickListtransaltion(DropDownList ddlPastDues)
		{
			if(ddlPastDues != null)
				CommonFunction.AddItems(ddlPastDues.Items, "UserDebtListingPastDuePickList", SessionState.LanguageCode);
		}

		public void ddljointAccountNumberPickListtransaltion(DropDownList ddljointAccountNumber)
		{
			if(ddljointAccountNumber != null)
			{
				CommonFunction.AddItems(ddljointAccountNumber.Items, "YesNo", SessionState.LanguageCode);
				ddljointAccountNumber.SelectedValue = "N";
			}
		}

		private void GetUserComment()
		{
			CreditorComment creditorComment = App.Credability.CreditorCommentGet(SessionState.ClientNumber.Value);
			if(creditorComment != null)
			{
				txtComment.Text = creditorComment.CreditorsComment;
			}
			else
			{
				txtComment.Text = string.Empty;
			}
		}

		protected void AddMoreRow_Click(object sender, EventArgs e)
		{
			AddNewRowToGrid();
		}

		protected void DeleteRow_Click(object sender, EventArgs e)
		{
			UserDebtListingsResult UserDebtListingsResults = null;
			bool Result = false;
			Int32 RowCount = 0;
			//New Code To Preserve the newly added Values Into Viewstate which is not there in database Start 
			DataTable NewRowAddedDebtListingTable = new DataTable();
			NewRowAddedDebtListingTable = ViewState["CurrentTable"] as DataTable;
			int GrindRowCount = UserDebtListingGrid.Rows.Count;
			foreach(GridViewRow GrdUserDebtListing in UserDebtListingGrid.Rows)
			{
				//Get the valuse of grid controls
				Label lblCreditorId = GrdUserDebtListing.FindControl("lblid") as Label;
				TextBox txtCreditorNameGrd = GrdUserDebtListing.FindControl("txtCreditorName") as TextBox;
				TextBox txtBalanceGrd = GrdUserDebtListing.FindControl("txtBalance") as TextBox;
				TextBox txtInterestGrd = GrdUserDebtListing.FindControl("txtInterest") as TextBox;
				TextBox txtmPaymentGrd = GrdUserDebtListing.FindControl("txtmPayment") as TextBox;
				TextBox txtAccountNumberGrd = GrdUserDebtListing.FindControl("txtAccountNumber") as TextBox;
				DropDownList ddlJiontAccountNumberGrd = GrdUserDebtListing.FindControl("ddlJiontAccountNumber") as DropDownList;
				DropDownList ddlDebtPastDueGrd = GrdUserDebtListing.FindControl("ddlDebtPastDue") as DropDownList;
				//Add Row data into table
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorId"] = lblCreditorId.Text;
				NewRowAddedDebtListingTable.Rows[RowCount]["PreAcntNoEncs"] = txtAccountNumberGrd.Text;
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorBal"] = txtBalanceGrd.Text.Replace("$", "");
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorIntrate"] = txtInterestGrd.Text.Replace("%", "");
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorName"] = txtCreditorNameGrd.Text;
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorPayments"] = txtmPaymentGrd.Text.Replace("$", "");
				NewRowAddedDebtListingTable.Rows[RowCount]["CreditorJointAcct"] = ddlJiontAccountNumberGrd.SelectedValue.Trim();
				NewRowAddedDebtListingTable.Rows[RowCount]["creditorPastDue"] = ddlDebtPastDueGrd.SelectedValue.Trim();

				RowCount = RowCount + 1;
			}
			ViewState["CurrentTable"] = NewRowAddedDebtListingTable;

			//New Code End 
			RowCount = 0;
			foreach(GridViewRow gvr in UserDebtListingGrid.Rows)
			{
				//Programmatically access the CheckBox from the TemplateField
				CheckBox cbkBoxGrid = gvr.FindControl("chkId") as CheckBox;
				Label lblCreditorId = gvr.FindControl("lblid") as Label;
				if(cbkBoxGrid.Checked == true && lblCreditorId.Text != string.Empty)
				{
					//Delete row from datatable
					UserDebtListingsResults = App.Credability.UserDebtListingsDelete(Convert.ToInt32(lblCreditorId.Text));
					Result = UserDebtListingsResults.IsSuccessful;

					//Delete the record from the Table in view state also 
					NewRowAddedDebtListingTable = ViewState["CurrentTable"] as DataTable;
					NewRowAddedDebtListingTable.Rows[RowCount].Delete();
					ViewState["CurrentTable"] = null;
					ViewState["CurrentTable"] = NewRowAddedDebtListingTable;

					// End Code
					RowCount -= 1;
				}
				if(cbkBoxGrid.Checked == true && lblCreditorId.Text == string.Empty)
				{
					//Delete row from datatable
					NewRowAddedDebtListingTable = ViewState["CurrentTable"] as DataTable;
					NewRowAddedDebtListingTable.Rows[RowCount].Delete();
					ViewState["CurrentTable"] = null;
					ViewState["CurrentTable"] = NewRowAddedDebtListingTable;
					Result = true;
					RowCount -= 1;
				}
				RowCount += 1;
			}
			if(Result == true)
			{

				NewRowAddedDebtListingTable = ViewState["CurrentTable"] as DataTable;
				UserDebtListingGrid.DataSource = NewRowAddedDebtListingTable;
				UserDebtListingGrid.DataBind();
				lblError.Text = ErrorMessageDelSucc;
			}
			else
			{
				lblError.Text = ErrorMessageNoRec;
			}
			ShowUserDebtListingRecord();
			GetUserComment();
		}

		private void AddNewRowToGrid()
		{
			// int rowIndex;
			if(ViewState["CurrentTable"] != null)
			{
				DataTable NewRowAddedDebtListingTable = new DataTable();
				NewRowAddedDebtListingTable = ViewState["CurrentTable"] as DataTable;

				int RowCount = 0;
				int GrindRowCount = UserDebtListingGrid.Rows.Count;
				foreach(GridViewRow GrdUserDebtListing in UserDebtListingGrid.Rows)
				{
					//Get the valuse of grid controls
					Label lblCreditorId = GrdUserDebtListing.FindControl("lblid") as Label;
					TextBox txtCreditorNameGrd = GrdUserDebtListing.FindControl("txtCreditorName") as TextBox;
					TextBox txtBalanceGrd = GrdUserDebtListing.FindControl("txtBalance") as TextBox;
					TextBox txtInterestGrd = GrdUserDebtListing.FindControl("txtInterest") as TextBox;
					TextBox txtmPaymentGrd = GrdUserDebtListing.FindControl("txtmPayment") as TextBox;
					TextBox txtAccountNumberGrd = GrdUserDebtListing.FindControl("txtAccountNumber") as TextBox;
					DropDownList ddlJiontAccountNumberGrd = GrdUserDebtListing.FindControl("ddlJiontAccountNumber") as DropDownList;
					DropDownList ddlDebtPastDueGrd = GrdUserDebtListing.FindControl("ddlDebtPastDue") as DropDownList;
					//Add Row data into table
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorId"] = lblCreditorId.Text;
					NewRowAddedDebtListingTable.Rows[RowCount]["PreAcntNoEncs"] = txtAccountNumberGrd.Text;
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorBal"] = txtBalanceGrd.Text.Replace("$", "");
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorIntrate"] = txtInterestGrd.Text.Replace("%", "");
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorName"] = txtCreditorNameGrd.Text;
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorPayments"] = txtmPaymentGrd.Text.Replace("$", "");
					NewRowAddedDebtListingTable.Rows[RowCount]["CreditorJointAcct"] = ddlJiontAccountNumberGrd.SelectedValue.Trim();
					NewRowAddedDebtListingTable.Rows[RowCount]["creditorPastDue"] = ddlDebtPastDueGrd.SelectedValue.Trim();
					RowCount = RowCount + 1;
				}
				DataRow NewRow;
				NewRow = NewRowAddedDebtListingTable.NewRow();
				NewRowAddedDebtListingTable.Rows.Add(NewRow);

				DataRow NewRow1;
				NewRow1 = NewRowAddedDebtListingTable.NewRow();
				NewRowAddedDebtListingTable.Rows.Add(NewRow1);

				DataRow NewRow2;
				NewRow2 = NewRowAddedDebtListingTable.NewRow();
				NewRowAddedDebtListingTable.Rows.Add(NewRow2);

				DataRow NewRow3;
				NewRow3 = NewRowAddedDebtListingTable.NewRow();
				NewRowAddedDebtListingTable.Rows.Add(NewRow3);

				DataRow NewRow4;
				NewRow4 = NewRowAddedDebtListingTable.NewRow();
				NewRowAddedDebtListingTable.Rows.Add(NewRow4);

				ViewState["CurrentTable"] = NewRowAddedDebtListingTable;
				UserDebtListingGrid.DataSource = NewRowAddedDebtListingTable;
				UserDebtListingGrid.DataBind();
				lblError.Text = string.Empty;
			}
			else
			{
				Response.Write("ViewState is null");
			}
		}

		public string CalculateDMPDate
		{
			get
			{
				DateTime dt = DateTime.Now.AddDays(14);
				if(dt.DayOfWeek == DayOfWeek.Saturday)
					return dt.AddDays(-1).ToShortDateString();
				else if(dt.DayOfWeek == DayOfWeek.Sunday)
					return dt.AddDays(1).ToShortDateString();
				else
					return dt.ToShortDateString();
			}
		}

		private void ShowDMPDate()
		{
			if(IsDMPDateVisible)
			{
				CommonOptionDealingDmpOnly option = App.Credability.CommonOptionDealingDmpOnlyGet(SessionState.ClientNumber.Value);
				if(!string.IsNullOrEmpty(option.YesDate))
					txtDMPDate.Text = option.YesDate;
				else
					txtDMPDate.Text = CalculateDMPDate;
			}
		}

		private void ShowUserDebtListingRecord()
		{
            // Bankruptcy Conversion
            //App.Host.GetCredRpt(SessionState.ClientNumber.Value);
            App.Debtplus.GetCredRpt(SessionState.ClientNumber.Value);

            UserDebtListings[] userDebtListing = App.Credability.UserDebtListingsGet(SessionState.ClientNumber.Value);
			if(userDebtListing != null)
			{
				DataTable DebtListingTable = new DataTable();
				DebtListingTable.Columns.Add("CreditorId");
				DebtListingTable.Columns.Add("PreAcntNoEncs");
				DebtListingTable.Columns.Add("CreditorBal");
				DebtListingTable.Columns.Add("CreditorIntrate");
				DebtListingTable.Columns.Add("CreditorPayments");
				DebtListingTable.Columns.Add("CreditorName");
				DebtListingTable.Columns.Add("CreditorJointAcct");
				DebtListingTable.Columns.Add("creditorPastDue");
				DataRow dr;
				for(int index = 0; index < userDebtListing.Length; index++)
				{
					dr = DebtListingTable.NewRow();
					dr["CreditorId"] = userDebtListing[index].CreditorId;
					dr["CreditorName"] = userDebtListing[index].CreditorName;
					dr["CreditorBal"] = userDebtListing[index].CreditorBal;
					dr["CreditorIntrate"] = userDebtListing[index].CreditorIntrate;
					dr["CreditorPayments"] = userDebtListing[index].CreditorPayments;
					dr["CreditorJointAcct"] = userDebtListing[index].CreditorJointAcct;
					dr["creditorPastDue"] = userDebtListing[index].creditorPastDue;
					if(userDebtListing[index].PreAcntNoEncs != null)
					{
						dr["PreAcntNoEncs"] = userDebtListing[index].PreAcntNoEncs.ToString();
					}
					else
					{
						userDebtListing[index].PreAcntNoEncs = "0000000";
					}
					DebtListingTable.Rows.Add(dr);
				}
				if(ViewState["CurrentTable"] != null)
				{
					DebtListingTable = ViewState["CurrentTable"] as DataTable;
				}
				int AddRows = 5 - DebtListingTable.Rows.Count;
				if(AddRows <= 0)
					AddRows = 2;
				for(int index = 0; index < AddRows; index++)
				{
					dr = DebtListingTable.NewRow();
					dr["CreditorId"] = "";
					dr["CreditorName"] = "";
					dr["CreditorBal"] = "";
					dr["CreditorIntrate"] = "";
					dr["CreditorPayments"] = "";
					dr["CreditorJointAcct"] = "";
					dr["creditorPastDue"] = "";
					dr["PreAcntNoEncs"] = "";
					DebtListingTable.Rows.Add(dr);
				}
				ViewState["CurrentTable"] = null;
				ViewState["CurrentTable"] = DebtListingTable;
				UserDebtListingGrid.DataSource = DebtListingTable;
				UserDebtListingGrid.DataBind();
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				if(AddUpdateDebtListingData())
				{
					AddComment();
					if(IsDMPDateVisible)
					{
						CalculateDMP calc = new CalculateDMP(SessionState.ClientNumber.Value);
						if(calc.isDMP)
							PageContinue = "CreditorScreenwb.aspx";
						else
							PageContinue = "CreditorScreenwob.aspx";

					}
					NavigateContinue();
				}
			}
		}


		public bool IsDMPDateVisible
		{
			get
			{
				return pnlDMP.Visible;
			}
			set
			{
				pnlDMP.Visible = value;
			}
		}

		protected bool BlankRow(string creditorName, string creditorId)
		{
			return creditorName.IsNullOrWhiteSpace() && creditorId.IsNullOrWhiteSpace();
		}

		// 05/04/2010: CREDA-391 - Able to save "Listing Your Debt" page without completing required fields
		// The logic of the method has been updated.  If any required fields are missing, it displays error message
		// and return false.  If the grid contains 0 row, it returns true.
		private bool AddUpdateDebtListingData()
		{
			var validResult = true;
			foreach(GridViewRow row in UserDebtListingGrid.Rows)
			{
				var creditorIdLabel = row.FindControl("lblid") as Label;
				var creditorNameTextBox = row.FindControl("txtCreditorName") as TextBox;
				if(BlankRow(creditorNameTextBox.Text, creditorIdLabel.Text))
					continue;

				var balanceTextBox = row.FindControl("txtBalance") as TextBox;
				var interestTextBox = row.FindControl("txtInterest") as TextBox;
				var paymentTextBox = row.FindControl("txtmPayment") as TextBox;
				var accountNumberTextBox = row.FindControl("txtAccountNumber") as TextBox;

				bool creditorNameHasData = !creditorNameTextBox.Text.IsNullOrWhiteSpace();
				bool balanceHasData = !balanceTextBox.Text.IsNullOrWhiteSpace();
				bool interestHasData = !interestTextBox.Text.IsNullOrWhiteSpace();
				bool paymentHasData = !paymentTextBox.Text.IsNullOrWhiteSpace();
				bool accountNumberHasData = !accountNumberTextBox.Text.IsNullOrWhiteSpace();

				//  Do all required fields have data?
				if(creditorNameHasData && balanceHasData && interestHasData && paymentHasData)
				{
					var jointAccountDropDownList = row.FindControl("ddlJiontAccountNumber") as DropDownList;
					var pastDueDropDownList = row.FindControl("ddlDebtPastDue") as DropDownList;

					var balance = balanceTextBox.Text.ToDecimal(0);
					var interest = interestTextBox.Text.ToDecimal(0);
					var payment = paymentTextBox.Text.ToDecimal(0);

					var minimumPayment = balance.MinimumMonthlyPayment(interest);
					if(payment <= minimumPayment)
					{
						// Tell the user the payment is not high enough to pay off the debt.
						lblError.Text = Resources.ExceptionMessages.ResourceManager.GetString("LowPaymentForInterestAndBalance", new System.Globalization.CultureInfo(SessionState.LanguageCode.ToLower())).FormatWith(balanceTextBox.Text, interestTextBox.Text, paymentTextBox.Text, creditorNameTextBox.Text);
						validResult = false;
						break;
					}

					var userDebtListing = new UserDebtListings();
					userDebtListing.CreditorId = creditorIdLabel.Text.ToInt(0);
					userDebtListing.CreditorName = creditorNameTextBox.Text;
					userDebtListing.ClientNumber = SessionState.ClientNumber.Value;
					userDebtListing.CreditorBal = balanceTextBox.Text.ToFloat(0);
					userDebtListing.CreditorPayments = paymentTextBox.Text.ToFloat(0);
					userDebtListing.CreditorIntrate = interestTextBox.Text.ToFloat(0);
					userDebtListing.CreditorJointAcct = jointAccountDropDownList.SelectedValue.Trim();
					userDebtListing.creditorPastDue = pastDueDropDownList.SelectedValue.Trim();
					userDebtListing.PreAcntNoEncs = accountNumberTextBox.Text;
					userDebtListing.LastModDTS = DateTime.Now;


					UserDebtListingsResults = App.Credability.UserDebtListingsAddupdate(userDebtListing);


					creditorIdLabel.Text = UserDebtListingsResults.CreditorId.ToString();
					validResult = UserDebtListingsResults.IsSuccessful;
				}
				// Else if there is a non empty row
				else if(creditorNameHasData || balanceHasData || interestHasData || paymentHasData || accountNumberHasData)
				{
					lblError.Text = ErrorMessageMissingRequiredData;
					validResult = false;
					break;
				}
			}
			if(IsDMPDateVisible && validResult)
			{
				CommonOptionDealingDmpOnly option = new CommonOptionDealingDmpOnly();
				option.ClientNumber = SessionState.ClientNumber.Value;
				option.YesDate = string.IsNullOrEmpty(txtDMPDate.Text) ? CalculateDMPDate : txtDMPDate.Text;
				option.YesorNo = "Yes";
				CommonOptionDealingDmpOnlyResult result = App.Credability.CommonOptionDealingUpdate(option);
				validResult = result.IsSuccessful;
			}
			return validResult;
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				if(AddUpdateDebtListingData())
				{
					AddComment();
					NavigateExit();
				}
			}
		}

		private void AddComment()
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			CreditorComment creditorComment = new CreditorComment();
			CreditorCommentResult Result = null;
			creditorComment.ClientNumber = SessionState.ClientNumber.Value;
			creditorComment.CreditorsComment = txtComment.Text.ToString().Trim();
			Result = App.Credability.CreaditorCommentsUpdate(creditorComment);
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			if(AddUpdateDebtListingData())
			{
				if(App.WebsiteCode == Cccs.Identity.Website.BKC && Request["from"] != null)
				{
					if(Request["from"] == "CO")
					{
						PagePrevious = "CommonOptions.aspx";
					}
					else if(Request["from"] == "Rent")
					{
						PagePrevious = "DescribeYourSituationRentalInfo.aspx";
					}
				}
				NavigatePrevious();
			}
		}

		protected void UserDebtListingGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			DataRowView dr = e.Row.DataItem as DataRowView;
			if(e.Row.RowType == DataControlRowType.DataRow && dr != null)
			{
				if(dr["CreditorBal"] != null)
				{
					TextBox txtCreditorbalance = e.Row.Cells[2].FindControl("txtBalance") as TextBox;
					if(!string.IsNullOrEmpty(txtCreditorbalance.Text))
					{
						txtCreditorbalance.Text = string.Empty;
						txtCreditorbalance.Text = "$" + dr["CreditorBal"].ToString();
					}
				}
				if(dr["CreditorIntrate"] != null)
				{
					TextBox txtInterest = e.Row.Cells[3].FindControl("txtInterest") as TextBox;
					if(!string.IsNullOrEmpty(txtInterest.Text))
					{
						txtInterest.Text = string.Empty;
						txtInterest.Text = dr["CreditorIntrate"].ToString() + "%";
					}
				}
				if(dr["CreditorPayments"] != null)
				{
					TextBox txtPayment = e.Row.Cells[4].FindControl("txtmPayment") as TextBox;
					if(!string.IsNullOrEmpty(txtPayment.Text))
					{
						txtPayment.Text = string.Empty;
						txtPayment.Text = "$" + dr["CreditorPayments"].ToString();
					}
				}
				if(dr["CreditorJointAcct"] != null)
				{
					DropDownList ddljointAccountNumber = e.Row.Cells[6].FindControl("ddlJiontAccountNumber") as DropDownList;
					ddljointAccountNumberPickListtransaltion(ddljointAccountNumber);
					ddljointAccountNumber.SelectedValue = dr["CreditorJointAcct"].ToString().Trim();
				}

				if(dr["creditorPastDue"] != null)
				{
					DropDownList ddlcreditorPastDue = e.Row.Cells[7].FindControl("ddlDebtPastDue") as DropDownList;
					PickListtransaltion(ddlcreditorPastDue);
					ddlcreditorPastDue.SelectedValue = dr["creditorPastDue"].ToString().Trim();
				}
			}
			else if(e.Row.RowType == DataControlRowType.Header)
			{
				Label lblWhy = e.Row.Cells[5].FindControl("lblAccNumWhy") as Label;
				Label lblWhy2 = e.Row.Cells[5].FindControl("lblJointAccWhy") as Label;
				if(App.WebsiteCode == Cccs.Identity.Website.PRP)
				{
					lblWhy.Text = "<a class='Link' href='#' onClick='javascript: return false;' title='" + App.Translate("Credability|PRPDEBTLISTING|WHYTP") + "'>" + App.Translate("Credability|UserDebtListing|Why") + "?</a>";
					lblWhy2.Text = "<a class='Link' href='#' onClick='javascript: return false;' title='" + App.Translate("Credability|PRPDEBTLISTING|WHYTPJAC") + "'>" + App.Translate("Credability|UserDebtListing|Why") + "?</a>";
				}
				else
				{
					lblWhy.Text = "<a class='Link' href='#' onClick='javascript: return false;' title='" + App.Translate("Credability|UserDebtListing|WNEY") + "'>" + App.Translate("Credability|UserDebtListing|Why") + "?</a>";
					lblWhy2.Text = "<a class='Link' href='#' onClick='javascript: return false;' title='" + App.Translate("Credability|UserDebtListing|WNEEY") + "'>" + App.Translate("Credability|UserDebtListing|Why") + "?</a>";
				}
			}
		}
	}
}