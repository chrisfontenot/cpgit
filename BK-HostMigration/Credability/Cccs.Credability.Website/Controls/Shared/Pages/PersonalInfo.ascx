﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonalInfo.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.PersonalInfo" %>

    <script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script language="javascript" type="text/javascript">
    function ReSetPhone(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatUSPhone(Val);
        }
    }
    function FormatAsText(theField) {
        var Val = stripCharsNotInBag(theField.value, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-, /'");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = Val;
        }
    }
    function ReSetSSN(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatSSN(Val);
        }
    }
    function ReSetLastName(theField) {
        var LastName = theField.value;
        LastName = LastName.replace("-", " ");
        theField.value = LastName;
        CleanField(theField);
    }
    function ReSetLastName(theField) {
        var LastName = theField.value;
        LastName = LastName.replace("-", " ");
        theField.value = LastName;
        CleanField(theField);
    }
    function CleanField(theField) {
        var theValue = theField.value;
        theValue = stripCharsNotInBag(theValue, digits + lowercaseLetters + uppercaseLetters + whitespace);
        theField.value = theValue;
    }
    var CoOwnerLastNamePre;
    function FlipFlopBox() {
        try {

            var CoSignFName = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerFirstName.ClientID %>').value);
            var CoSignLName = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerLastName.ClientID %>').value);
            if (CoSignFName.length > 0 || CoSignLName.length > 0) {
                document.getElementById('spnCoOwnerDobReq').style.display = '';
                document.getElementById('spnCoOwnerLNameReq').style.display = '';
                document.getElementById('spnCoOwnerSsnReq').style.display = '';
                document.getElementById('spnCoOwnerMStatusReq').style.display = '';
                document.getElementById('spnCoOwnerEmpReq').style.display = '';
            }
            else {
                document.getElementById('spnCoOwnerDobReq').style.display = 'none';
                document.getElementById('spnCoOwnerLNameReq').style.display = 'none';
                document.getElementById('spnCoOwnerSsnReq').style.display = 'none';
                document.getElementById('spnCoOwnerMStatusReq').style.display = 'none';
                document.getElementById('spnCoOwnerEmpReq').style.display = 'none';
            }
        }
        catch (e) {
        }
    }

    function CheckSsn(sender, args) {

        if (args.Value.length < 11 || args.Value.length == 0) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }

    function CheckprimaryPhone(sender, args) {

        if (args.Value.length > 14 || args.Value.length < 14 || args.Value.length == 10) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>

<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|PYoPI")%>&nbsp;&nbsp;</h1>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:"
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" />
<div class="DvErrorSummary" id="dvTopErrorMessage" runat="server" visible="false">
</div>
<div class="dvrow">
    <table>
    <tr valign="top" >
    <td><asp:CheckBox ID="chkDiscardData" runat="server" TextAlign="Right" Checked="false" /> </td>
    <td >&nbsp;&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DiscardData")%></td>
    </tr>
    </table>
</div>
<div class="dvform2col">
    <div class="colformlft">
        <div class="dvform">
            <div class="dvrow">
                <p class="col_title">
                    <label><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Owner")%></label>
                    <%--<asp:Label ID="lblPriTitle" runat="server" />--%></p>
            </div>
            <div class="dvrow">
                <label for="<%=txtFirstName.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|FirstName")%>:</label>
                <asp:TextBox ID="txtFirstName" CssClass="txtBox requiredField" runat="server"></asp:TextBox><span
                    >&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvFname" runat="server" ControlToValidate="txtFirstName"
                    EnableClientScript="true" Text="" ErrorMessage="First name Required."></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtInitial.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Initial")%>:</label>
                <asp:TextBox ID="txtInitial" runat="server" CssClass="txtBox" MaxLength="1"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label for="<%=txtLastName.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|LastName")%>:</label>
                <asp:TextBox ID="txtLastName" CssClass="txtBox requiredField" runat="server"></asp:TextBox><span
                    >&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                    ErrorMessage="Last name required." Text=""></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtSSN.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%>:</label>
                <asp:TextBox ID="txtSSN" CssClass="txtBox requiredField" runat="server" onBlur="ReSetSSN(this);"></asp:TextBox><span
                    >&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvSSN" runat="server" ControlToValidate="txtSSN"
                    ErrorMessage="Your SSN is Required." Text="" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtSSN" runat="server"
                    ControlToValidate="txtSSN" Display="Dynamic" ValidationExpression="(\d{3}-\d{2}-\d{4})|(\d{3}\d{2}\d{4})"
                    ErrorMessage="Please enter a valid SSN." Text=""></asp:RegularExpressionValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtDOB.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Dob")%>:</label>
                <asp:TextBox ID="txtDOB" runat="server" MaxLength="20" CssClass="txtBox requiredField"></asp:TextBox><span
                    >&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvDOB" runat="server" ControlToValidate="txtDOB"
                    Display="Dynamic" EnableClientScript="true" ErrorMessage="DOB is Required." Text=""></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="revtxtDOB" ControlToValidate="txtDOB"
                    ValidationExpression="^(([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$)|(([1-9]|0[1-9]|1[012])[-]([1-9]|0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$)"
                    ErrorMessage="Date is not Acceptable" Text="" Display="Dynamic" />
                <br />
                <label>
                    &nbsp;</label>
                <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%>
            </div>
            <div class="dvrow">
                <label for="<%=ddlMaritalStatus.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|MaritalStatus")%>:</label>
                <asp:DropDownList ID="ddlMaritalStatus" runat="server">
                </asp:DropDownList>
                <span >&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus"
                    EnableClientScript="true" SetFocusOnError="true" ErrorMessage="Your Marital Status Required."
                    Text="" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label for="<%=ddlSex.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Sex")%>:</label>
                <asp:DropDownList ID="ddlSex" runat="server">
                </asp:DropDownList>
            </div>
            <div class="dvrow">
                <label for="<%=ddlRace.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Race")%>:</label>
                <asp:DropDownList ID="ddlRace" runat="server">
                </asp:DropDownList>
            </div>
            <div class="dvrow">
                <label for="<%=ddlHispanic.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Hispanic")%>?:</label>
                <asp:DropDownList ID="ddlHispanic" runat="server">
                </asp:DropDownList>
                &nbsp;<a class="Link" href="#" onclick="javascript: return false;" title='<%= Cccs.Credability.Website.App.Translate("WhyLinkTitle")%>'><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|W")%></a>
            </div>
            <asp:Panel ID="pnlPriEmpEdu" runat="server" Visible="true">
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Employment")%></label>
                    <asp:DropDownList ID="ddlPriEmp" runat="server">
                    </asp:DropDownList>
                    <span class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvEmployment" runat="server" Display="Dynamic" EnableClientScript="true"
                         ControlToValidate="ddlPriEmp" Text="" ErrorMessage="Employment Is equired."></asp:RequiredFieldValidator>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Education")%></label>
                    <asp:DropDownList ID="ddlPriEdu" runat="server">
                    </asp:DropDownList>
                    <span class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvEducation" runat="server" EnableClientScript="true"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="ddlPriEdu" Text="!"
                        ErrorMessage="Education Is Required."></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>
            <div class="dvrow">
                <label for="<%=txtAddress.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress")%>:</label>
                <asp:TextBox ID="txtAddress" runat="server" CssClass="txtBox"></asp:TextBox><span
                    class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                    Display="Dynamic" EnableClientScript="true" Text="" ErrorMessage="Street Address Is Required."></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtAddress2.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress1")%>:</label>
                <asp:TextBox ID="txtAddress2" runat="server" CssClass="txtBox"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label for="<%=ddlSex.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|City")%>:</label>
                <asp:TextBox ID="txtCity" runat="server" CssClass="txtBox" OnBlur="FormatAsText(this)"></asp:TextBox><span
                    class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvcity" runat="server" ControlToValidate="txtCity"
                    Display="Dynamic" Text="" OnBlur="FormatAsText(this)" ErrorMessage="City required."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCity"
                    ErrorMessage="City must be in text format." ValidationExpression="[^1234567890]+">!</asp:RegularExpressionValidator>
            </div>
            <div class="dvrow">
                <label for="<%=ddlState.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|State")%>:</label>
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvState" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" EnableClientScript="true" Text="" ErrorMessage="Please Select State."></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtZipCode.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|ZipCode")%>:</label>
                <asp:TextBox ID="txtZipCode" runat="server" CssClass="txtBox"></asp:TextBox><span
                    class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvZipCode" runat="server" ControlToValidate="txtZipCode"
                    EnableClientScript="true" Text="" ErrorMessage="Zip Code is required." Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" ControlToValidate="txtZipCode"
                    ValidationExpression="^[0-9]{5}$" ErrorMessage="Enter valid Zip Code." Display="Dynamic"
                    Text="" />
                <span id="divZipError" runat="server" class="error"></span>
            </div>
            <div class="dvrow">
                <label for="<%=txtPphone.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|PrimaryPhone")%>:</label>
                <asp:TextBox ID="txtPphone" runat="server" onBlur="ReSetPhone(this)" CssClass="txtBox"></asp:TextBox><span
                    class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="rfvPphone" runat="server" ControlToValidate="txtPphone"
                    EnableClientScript="true" SetFocusOnError="true" Text="" ErrorMessage="Your Primary Phone Number Is Required."
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtPphone" runat="server"
                    ControlToValidate="txtPphone" Display="Dynamic" ErrorMessage="Please enter a valid Phone #."
                    ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Text=""></asp:RegularExpressionValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtSphone.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|SecondaryPhone")%>:</label>
                <asp:TextBox ID="txtSphone" CssClass="txtBox" runat="server" onBlur="ReSetPhone(this)"></asp:TextBox>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtSphone" runat="server"
                    ControlToValidate="txtSphone" ErrorMessage="Please enter a valid Phone #." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                    Display="Dynamic" Text=""></asp:RegularExpressionValidator>
            </div>
            <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                <div class="dvrow">
                    <label for="<%=txtemailaddress.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|EmailAddress")%>:</label>
                    <asp:TextBox ID="txtemailaddress" CssClass="txtBox" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator class="validationMessage" ID="cfvEmailAddress" runat="server" ControlToValidate="txtemailaddress"
                        Display="Dynamic" ErrorMessage="Enter valid Email Address." Text="" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="colformrht">
        <div class="dvform">
            <p class="col_title">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Co-OwnerRequired")%></label>
                <%--<asp:Label ID="lblCoTitle" runat="server" />--%></p>
            <div class="dvrow">
                <label for="<%=txtCoOwnerFirstName.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|FirstName")%>:</label>
                <asp:TextBox CssClass="txtBox" ID="txtCoOwnerFirstName" runat="server" onBlur="FlipFlopBox();"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label for="<%=txtCoOwnerInitial.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Initial")%>:</label>
                <asp:TextBox CssClass="txtBox" ID="txtCoOwnerInitial" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label for="<%=txtCoOwnerLastName.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|LastName")%>:</label>
                <asp:TextBox CssClass="txtBox" ID="txtCoOwnerLastName" runat="server" onBlur="FlipFlopBox();"></asp:TextBox>
                <span id="spnCoOwnerLNameReq" visible="false" style="color: Red; font-size: large">&nbsp;*</span>
            </div>
            <div class="dvrow">
                <label for="<%=txtCoOwnerSSN.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%>:</label>
                <asp:TextBox CssClass="txtBox" ID="txtCoOwnerSSN" runat="server" onBlur="ReSetSSN(this);"></asp:TextBox>
                <span id="spnCoOwnerSsnReq" visible="false" class="requiredField">&nbsp;*</span>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtCoOwnerSSN" runat="server"
                    ControlToValidate="txtCoOwnerSSN" Display="Dynamic" ErrorMessage="Please enter a valid SSN."
                    ValidationExpression="(\d{3}-\d{2}-\d{4})|(\d{3}\d{2}\d{4})">!</asp:RegularExpressionValidator>
            </div>
            <div class="dvrow">
                <label for="<%=txtCoOwnerDOB.ClientID %>">
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Dob")%>:</label>
                <asp:TextBox CssClass="txtBox" ID="txtCoOwnerDOB" runat="server"></asp:TextBox>
                <span id="spnCoOwnerDobReq" visible="false" class="requiredField">&nbsp;*</span>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="revtxtCoOwnerDOB" ControlToValidate="txtCoOwnerDOB"
                    ValidationExpression="^(([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$)|(([1-9]|0[1-9]|1[012])[-]([1-9]|0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$)"
                    ErrorMessage="Co-Owner's DOB must be in the format: MM/DD/YYYY." Display="Dynamic"
                    Text="" />
                <br />
                <label>
                    &nbsp;</label>
                <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%>
            </div>
            <asp:Panel ID="pnlCoOwnerExtra" runat="server">
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerMaritalStatus.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|MaritalStatus")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerMaritalStatus" runat="server">
                    </asp:DropDownList>
                    <span id="spnCoOwnerMStatusReq" visible="false" class="requiredField">
                        &nbsp;*</span>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerSex.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Sex")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerSex" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerRace.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Race")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerRace" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoHispanic.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Hispanic")%>?:</label>
                    <asp:DropDownList ID="ddlCoHispanic" runat="server">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCoEmp" runat="server" Visible="false">
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Employment")%></label>
                    <asp:DropDownList ID="ddlCoEmp" runat="server">
                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                        <asp:ListItem Value="Full Time">Full Time</asp:ListItem>
                        <asp:ListItem Value="Part Time">Part Time</asp:ListItem>
                        <asp:ListItem Value="Retired">Retired</asp:ListItem>
                        <asp:ListItem Value="Unemployed">Unemployed</asp:ListItem>
                    </asp:DropDownList>
                    <span id="spnCoOwnerEmpReq" visible="false" style="color: Red; font-size: large">&nbsp;*</span>
                </div>
            </asp:Panel>
        </div>
    </div>
</div>
