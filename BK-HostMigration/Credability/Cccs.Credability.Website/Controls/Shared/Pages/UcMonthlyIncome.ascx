﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcMonthlyIncome.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcMonthlyIncome" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<p>
	<h1>
		<span id="PageHeadGenral" runat="server" visible="false">
			<%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|DYN")%>
		</span>
		<span id="PageHeadDmpOnly" runat="server" visible="false">
			<%=App.Translate("Credability|UserIncomeDocumentationInfo|StepTitle")%>
		</span>
	</h1>
</p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
<span id="introText" runat="server" visible="false">
	<%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|PCon")%>
	<%=CommonFunction.CreateChatLink(App.Translate("Credability|DescribeYourSituationBCH|CH")) %>
	<%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|PCon2")%>
	<p />
	<div id="basic-modal-content">
		<%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|PCon3")%>
	</div>
	<a href='javascript:void' onclick="javascript:$('#basic-modal-content').modal();return false;"><%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|PConQA")%></a>
</span>
<span id="introTextPrp" runat="server" visible="false">
	<%=App.Translate("Credability|UserIncomeDocumentationInfoBCH|APO")%>
</span>
<script type="text/javascript">
function CalcMonthlyTotals()
{
	//<!--Primary Person-->
	document.getElementById('<%=dvTotalMonthlyGrossIncomePrimary.ClientID %>').style.display='none';
	document.getElementById('<%=PriIncTotalGross.ClientID %>').value = Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGross.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncPtGross.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncAlimonyGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncChildSupportGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGovAstGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncOtherGross.ClientID %>').value, "0123456789."))
	document.getElementById('<%=PriIncTotalGross.ClientID %>').style.display='';
	ReFormatCurrency(document.getElementById('<%=PriIncTotalGross.ClientID %>'));
	
	//<!--Second Person-->
	document.getElementById('<%=dvTotalMonthlyGrossIncomeSec.ClientID %>').style.display='none';
	document.getElementById('<%=SecIncTotalGross.ClientID %>').value = Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGross.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncPtGross.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncAlimonyGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncChildSupportGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGovAstGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncOtherGross.ClientID %>').value, "0123456789."))
	document.getElementById('<%=SecIncTotalGross.ClientID %>').style.display='';
	ReFormatCurrency(document.getElementById('<%=SecIncTotalGross.ClientID %>'));
	
	//<!--Net Household Income -->
	document.getElementById('<%=dvNetHouseholdIncomePrimary.ClientID %>').style.display='none';
	document.getElementById('<%=PriIncTotalNet.ClientID %>').value = Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncPtNet.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncAlimonyNet.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncChildSupportNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGovAstNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncOtherNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncNet.ClientID %>').value, "0123456789."))
	document.getElementById('<%=PriIncTotalNet.ClientID %>').style.display='';
	ReFormatCurrency(document.getElementById('<%=PriIncTotalNet.ClientID %>'));
	
	//<!--Secondry Net Household Income -->
	document.getElementById('<%=dvNetHouseholdIncomeSec.ClientID %>').style.display='none';
	document.getElementById('<%=SecIncTotalNet.ClientID %>').value = Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncNet.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncPtNet.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncAlimonyNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncChildSupportNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGovAstNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncOtherNet.ClientID %>').value, "0123456789."))
	document.getElementById('<%=SecIncTotalNet.ClientID %>').style.display='';
	ReFormatCurrency(document.getElementById('<%=SecIncTotalNet.ClientID %>'));
}

function validateForm()
{
    var bReturn = true;
    var monthlyIsChecked = document.getElementById('<%=MonthlyChecked.ClientID %>').checked;
	var total_Gross = Number(stripCharsNotInBag (document.getElementById('<%=PriIncTotalGross.ClientID %>').value, "0123456789."));
	var total_Gross_Sec= Number(stripCharsNotInBag (document.getElementById('<%=SecIncTotalGross.ClientID %>').value, "0123456789."));
	var total_Net = Number(stripCharsNotInBag (document.getElementById('<%=PriIncTotalNet.ClientID %>').value, "0123456789."));
	var total_Net_Sec = Number(stripCharsNotInBag (document.getElementById('<%=SecIncTotalNet.ClientID %>').value, "0123456789."));
	if (total_Gross != 0)
	{
		if (total_Gross < 1.15 * total_Net) 
		{
			bReturn = confirm("<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|YGI")%>");
		}
	}
    //this one
	if (monthlyIsChecked && ((total_Gross >= 10000) || (total_Net >= 8000) || (total_Gross_Sec >= 10000) || (total_Net_Sec >= 8000)))
	{
		bReturn = confirm("<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TIY")%>");
	}
	if ((total_Gross == 0) && (total_Net == 0) && (total_Gross_Sec == 0) && (total_Net_Sec == 0))
	{
		bReturn = confirm("<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|BG")%>");
	}
	else
	{
		if (total_Gross == 0)
		{
			bReturn = confirm("<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TGIY")%>");
		}
		if (total_Net == 0)
		{
			bReturn = confirm("<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TNIY")%>");
		}
	}
	return bReturn;
}

function syncFields(Field)
{
	var sValue=Field.value;
	var sName = Field.name;
	var myRegExp = /Gross/;
	var matchPos1 = sName.search(myRegExp);
//check whether it's a 'Gross' field or a "Net' field
	if(matchPos1 != -1)
	{
		var snewName=sName.replace("Gross", "Net");
	}
	else
	{
		var snewName=sName.replace("Net", "Gross");
	}
	
	document.getElementById(snewName.replaceAll("$", "_")).value=sValue;
	ReFormatCurrency(document.getElementById(snewName.replaceAll("$", "_")));
}

// Replaces all instances of the given substring.
String.prototype.replaceAll = function( 
	strTarget, // The substring you want to replace
	strSubString // The string you want to replace in.
	){
	var strText = this;
	var intIndexOfMatch = strText.indexOf( strTarget );
	 
	// Keep looping while an instance of the target string
	// still exists in the string.
	while (intIndexOfMatch != -1){
		// Relace out the current instance.
		strText = strText.replace( strTarget, strSubString )
		 
		// Get the index of any next matching substring.
		intIndexOfMatch = strText.indexOf( strTarget );
	}
	 
	// Return the updated string with ALL the target strings
	// replaced out with the new substring.
	return( strText );
}
</script>
<script src="../../Content/calculator.js" type="text/javascript"></script>
<p>
	&nbsp;</p>
<h1>Fields are in:</h1>
<div class="dvform2col dvformlblbig">
    <h2>
        <input id="MonthlyChecked" type="radio" name="incomeType" value="Monthly" runat="Server" />Monthly<br />
        <input id="YearlyChecked" type="radio" name="incomeType" value="Yearly" runat="Server" />Yearly
    </h2>
</div>

<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GHI")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|PP")%></p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGI")%></label>
				<asp:TextBox ID="txtPriIncGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGross.ClientID %>']);">
					<asp:Image ID="imgCal1" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGIF")%></label>
				<asp:TextBox ID="txtPriIncPtGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncPtGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncPtGross.ClientID %>'])">
					<%--<img width="15" height="13" border="0" alt="Click here to see the calculator" src="img/calc.gif">--%><asp:Image ID="imgCal2" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
				<asp:TextBox ID="txtPriIncAlimonyGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncAlimonyGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncAlimonyGross.ClientID %>'])">
					<asp:Image ID="imgCal3" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
				<asp:TextBox ID="txtPriIncChildSupportGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncChildSupportGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncChildSupportGross.ClientID %>'])">
					<asp:Image ID="imgCal4" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
				<asp:TextBox ID="txtPriIncGovAstGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGovAstGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGovAstGross.ClientID %>'])">
					<asp:Image ID="imgCal5" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
				<asp:TextBox ID="txtPriIncOtherGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncOtherGross.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncOtherGross.ClientID %>'])">
					<asp:Image ID="imgCal6" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMGI")%></label>
				<input name="PriIncTotalGross" id="PriIncTotalGross" runat="server" type="text" disabled="true" style="border: none; background-color: #e0e0e0; display: none;" size="14">
				<div id="dvTotalMonthlyGrossIncomePrimary" runat="server" style="font-weight: bold; line-height: 25px; font-size: 14px; font-weight: bold">
				</div>
				<span id="test">
				</span>
			</div>
		</div>
	</div>
	<div class="colformrht">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|SP")%></p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGI")%></label>
				<asp:TextBox ID="txtSecIncGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGIF")%></label>
				<asp:TextBox ID="txtSecIncPtGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
				<asp:TextBox ID="txtSecIncAlimonyGross" runat="server" MaxLength="7" onChange="syncFields(this)" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
				<asp:TextBox ID="txtSecIncChildSupportGross" runat="server" MaxLength="7" onChange="syncFields(this)" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
				<asp:TextBox ID="txtSecIncGovAstGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
				<asp:TextBox ID="txtSecIncOtherGross" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMGI")%></label>
				<input name="SecIncTotalGross" id="SecIncTotalGross" runat="server" type="text" disabled="true" style="border: none; background-color: #e0e0e0; display: none;" size="14" />
				<div id="dvTotalMonthlyGrossIncomeSec" runat="server" style="font-size: 14px; font-weight: bold; font-weight: bold; line-height: 25px">
				</div>
			</div>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|NHI")%></h1>
<!-- Begin net income add form -->
<div class="dvform2col dvformlblbig">
	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|PP")%></p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFP")%></label>
				<asp:TextBox ID="txtPriIncNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncNet.ClientID %>'])">
					<asp:Image ID="imgCal7" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFS")%></label>
				<asp:TextBox ID="txtPriIncPtNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncPtNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncPtNet.ClientID %>'])">
					<asp:Image ID="imgCal8" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
				<asp:TextBox ID="txtPriIncAlimonyNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncAlimonyNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncAlimonyNet.ClientID %>'])">
					<asp:Image ID="imgCal9" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
				<asp:TextBox ID="txtPriIncChildSupportNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncChildSupportNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncChildSupportNet.ClientID %>'])">
					<asp:Image ID="imgCal10" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
				<asp:TextBox ID="txtPriIncGovAstNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGovAstNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGovAstNet.ClientID %>'])">
					<asp:Image ID="imgCal11" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
				<asp:TextBox ID="txtPriIncOtherNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
				<a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncOtherNet.ClientID %>'])" onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncOtherNet.ClientID %>'])">
					<asp:Image ID="imgCal12" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMNI")%></label>
				<input name="PriIncTotalNet" id="PriIncTotalNet" runat="server" type="text" disabled="true" style="border: none; background-color: #e0e0e0; display: none;" size="14" />
				<div id="dvNetHouseholdIncomePrimary" runat="server" style="font-size: 14px; font-weight: bold; font-weight: bold; line-height: 25px">
				</div>
			</div>
		</div>
	</div>
	<div class="colformrht">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|SP")%></p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFP")%></label>
				<asp:TextBox ID="txtSecIncNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFS")%></label>
				<asp:TextBox ID="txtSecIncPtNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
				<asp:TextBox ID="txtSecIncAlimonyNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
				<asp:TextBox ID="txtSecIncChildSupportNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
				<asp:TextBox ID="txtSecIncGovAstNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
				<asp:TextBox ID="txtSecIncOtherNet" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);CalcMonthlyTotals();" onChange="syncFields(this)" onFocus="SetUpField(this);" CssClass="txtBox"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMNI")%></label>
				<input name="SecIncTotalNet" id="SecIncTotalNet" runat="server" type="text" disabled="true" style="border: none; background-color: #e0e0e0; display: none;" size="14" />
				<div id="dvNetHouseholdIncomeSec" runat="server" style="font-size: 14px; font-weight: bold; font-weight: bold; line-height: 25px">
				</div>
			</div>
		</div>
	</div>
	<div class="clearboth">
		&nbsp;
	</div>
	<Uc:AuthCode id="UcAuthCode" runat="server"></Uc:AuthCode>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="imgBtnDocumentingPrevious" runat="server" CssClass="previous" OnClick="imgBtnDocumentingPrevious_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSubmit" class="save" Visible="false" runat="server" ToolTip="Submit" CausesValidation="false" OnClick="btnSaveAndExitAndSubmitToQueue_Click">      
        <span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SendToCounselor")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="imgBtnDocumentingSaveExit" runat="server" OnClick="imgBtnDocumentingSaveExit_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="imgBtnDocumentingContinue" runat="server" OnClick="imgBtnDocumentingContinue_Click" OnClientClick="return validateForm();" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>