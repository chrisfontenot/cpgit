﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;
using Cccs.Web;
using System.Text;
//using Cccs.Identity.Dal;

namespace Cccs.Credability.Website.Controls
{
	public partial class UserProfile : System.Web.UI.UserControl
	{
		#region Control Parameters

		public bool IsEmailVisible
		{
			get { return pnlEmail.Visible; }
			set { pnlEmail.Visible = value; }
		}

		public bool IsCounselingVisible
		{
			get { return pnlCounseling.Visible; }
			set { pnlCounseling.Visible = value; }
		}

        public bool IsAuthorizeShareParticipateVisible
        {
            get { return pnlAuthorizeShareParticipate.Visible; }
            set { pnlAuthorizeShareParticipate.Visible = value; }
        }
		public string WebsiteCode
		{
			get { return ViewState.ValueGet<string>("WebsiteCode"); }

			set { ViewState.ValueSet("WebsiteCode", value); }
		}

		public string RedirectOnContinue
		{
			get
			{
				string url = ViewState.ValueGet<string>("RedirectOnContinue");

				return !string.IsNullOrEmpty(url) ? url : "Eligibility.aspx";
			}
			set
			{
				ViewState.ValueSet("RedirectOnContinue", value);
			}
		}

		public string RedirectOnNeedAuthorization
		{
			get
			{
				string url = ViewState.ValueGet<string>("RedirectOnNeedAuthorization");

				return !string.IsNullOrEmpty(url) ? url : "AFS.aspx";
			}
			set
			{
				ViewState.ValueSet("RedirectOnNeedAuthorization", value);
			}
		}

		public bool IsPriEmploymentEducationVisible
		{
			get { return pnlPriEmpEdu.Visible; }
			set { pnlPriEmpEdu.Visible = value; }
		}

		public bool IsCoEmploymentVisible
		{
			get { return pnlCoEmp.Visible; }
			set { pnlCoEmp.Visible = value; }
		}

        public bool IsMilitaryServiceVisible
        {
            get { return pnlMilService.Visible; }
            set { pnlMilService.Visible = value; }
        }

		#endregion

		#region ViewState

		private string LanguageCode
		{
			get { return ViewState.ValueGet<string>("LanguageCode"); }
			set { ViewState.ValueSet("LanguageCode", value); }
		}

		private long? UserAddressId
		{
			get { return ViewState.ValueGet<long?>("UserAddressID"); }
			set { ViewState.ValueSet("UserAddressID", value); }
		}

		private long? PrimaryUserDetailId
		{
			get { return ViewState.ValueGet<long?>("PrimaryUserDetailId"); }
			set { ViewState.ValueSet("PrimaryUserDetailId", value); }
		}

		private long? SecondaryUserDetailId
		{
			get { return ViewState.ValueGet<long?>("SecondaryUserDetailId"); }
			set { ViewState.ValueSet("SecondaryUserDetailId", value); }
		}

		#endregion

		#region Translations

		private string EmailAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateEmail.");						 // TODO: DONE
		private string SSNAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN.");								 // TODO: DONE
		private string SecondaryEmailAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN."); // TODO: DONE
		private string SecondarySSNAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN.");			 // TODO: DONE

		#endregion

        string _pageheading;
        public string PageHeading
        {
            get
            {
                if (string.IsNullOrEmpty(_pageheading))
                return 
                    Cccs.Credability.Website.App.Translate("Credability|UserProfile|PYoPI");
                else
                return 
                    _pageheading;
            }

            set
            {
                _pageheading = value;
            }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
            if (this.ddlState != null && this.ddlState.Text != null) {
                SessionState.State = this.ddlState.Text;
            }

			if(!IsPostBack)
			{
				Credability.AFS Afs = null;
				Afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
				if (Afs == null || String.IsNullOrEmpty(Afs.PrimaryPerson))
				{
					Response.Redirect("AFS.aspx"); //We have a bug here!! Rec is not getting afs primary person back after save!!
				}
				else if(Afs.IsSecondaryPersonNeeded && String.IsNullOrEmpty(Afs.SecondaryPerson))
				{
					Response.Redirect("AFS.aspx");
				}

				InitializeTranslations();
				CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
				Initialize();

                User user = App.Identity.UserGet((long)SessionState.UserId);

                SessionState.Username = user.Username;
                textSecurityQuestion.InnerText = user.SecurityQuestionAnswer.Text;

				if (App.WebsiteCode == Cccs.Identity.Website.BKC)
				{
					btnContinue.Attributes.Add("OnClick", "return VerifyClientName();");
				}
				else if(SessionState.RefCode == "ccrc/620" && App.WebsiteCode == Cccs.Identity.Website.HUD)
				{
					RedirectOnContinue = "ClientCounseled.aspx";
				}

				if(App.WebsiteCode == Cccs.Identity.Website.BKC)
				{
					btnContinue.Attributes.Add("OnClick", "return VerifyClientName();");
				}
			}
			ScriptManager.RegisterStartupScript(this, this.GetType(), "SetSecondRequired", "FlipFlopBox();", true);
			//New Code To Update
		}

		public void ShowHideControl()
		{
			dvBchHoShareInfo.Visible = true;
			spnBchRow.Visible = true;
			spnbchShareinfo.Visible = true;
		}

		public static void ShowHideRvmControl()
		{
		}

		private void ValidationControlTranslation()
		{
			rfvCreditCounseling.ErrorMessage = App.Translate("Credability|UserProfile|CCR");
			rfvEducation.ErrorMessage = App.Translate("Credability|UserProfile|EDU");
			rfvEmployment.ErrorMessage = App.Translate("Credability|UserProfile|EMP");
			rfvDMP.ErrorMessage = App.Translate("Credability|UserProfile|DR");
			rfvRecentlyDMP.ErrorMessage = App.Translate("Credability|UserProfile|RDR");
			rfvShare.ErrorMessage = App.Translate("Credability|UserProfile|SYI");
			rfvFname.ErrorMessage = App.Translate("Credability|UserProfileBCH|FNR");
			rfvLastName.ErrorMessage = App.Translate("Credability|UserProfileBCH|LNR");
			rfvSSN.ErrorMessage = App.Translate("Credability|UserProfileBCH|SSNR");
			rfvDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|DOBR");
			revtxtDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|DOBNPFR");
			rfvMaritalStatus.ErrorMessage = App.Translate("Credability|UserProfileBCH|MSR");
			rfvAddress.ErrorMessage = App.Translate("Credability|UserProfileBCH|UAR");
			rfvcity.ErrorMessage = App.Translate("Credability|UserProfileBCH|UCR");
			rfvState.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
			rfvZipCode.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
			rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
			rfvPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNR");
			RegularExpressionValidator_txtPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|PPNWFR");
			RegularExpressionValidator_txtSphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|SPNWFR");
			cfvEmailAddress.ErrorMessage = App.Translate("Credability|UserProfileBCH|EAWRR");
			rfvOwner.ErrorMessage = App.Translate("Credability|UserProfileBCH|OCOBIR");
			rftxtHouseholdSize.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NO");
			rvtxtHouseholdSize.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOP");
			//Co-Owner information 
			revtxtCoOwnerDOB.ErrorMessage = App.Translate("Credability|UserProfileBCH|CODOBWFR");
			RegularExpressionValidator_txtCoOwnerSSN.ErrorMessage = App.Translate("Credability|UserProfileBCH|COSSNWFR");

         RequiredFieldValidator_UserName.ErrorMessage = App.Translate("Credability|UserProfileBCH|UNR");
		}

		private void InitializeTranslations()
		{
			if(LanguageCode != SessionState.LanguageCode) // First page load, or language has changed 
			{
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("SaveExit");
				LanguageCode = SessionState.LanguageCode;
				GetSexPickList();
				GetStatePickList();
				GetRacePickList();
				GetHispanicPickList();
				GetRefSourcePickList();
				HispanicPickListCoOwner();
				GetSexPickListCoOwner();
				GetMaritalSataus();
				GetEmpickList();
				EmploymentInfoPickList();
                MilStatusPickList();
                BranchOfServicePickList();
				PrimaryEducationPickList();
				if(App.WebsiteCode == Cccs.Identity.Website.PRP)
				{
					chkCoOwnerConfirmation.Text = Cccs.Credability.Website.App.Translate("Credability|UserProfilePRP|IAffirmCoOwner");
					chkConfirmation.Text = Cccs.Credability.Website.App.Translate("Credability|UserProfilePRP|IAffirmOwner");
				}
				else
				{
					chkCoOwnerConfirmation.Text = Cccs.Credability.Website.App.Translate("Credability|UserProfile|IAffirmCoOwner");
					chkConfirmation.Text = Cccs.Credability.Website.App.Translate("Credability|UserProfile|IAffirmOwner");
				}
				if(App.WebsiteCode == Cccs.Identity.Website.BKC)
				{
					lblIntro.InnerHtml = App.Translate("Credability|UserProfile|BKCTopLine");
					EditAttorney.Visible = true;
				}
				else if(App.WebsiteCode == Cccs.Identity.Website.RVM)
				{
					lblIntro.InnerHtml = App.Translate("Credability|UserProfile|TopLine");
				}
				else if(App.WebsiteCode == Cccs.Identity.Website.PRP)
				{
					lblIntro.InnerHtml = App.Translate("Credability|UserProfile|PRPTopLine");
				}
				else
				{
					lblIntro.InnerHtml = App.Translate("Credability|UserProfileBCH|DYS");
				}
				ValidationControlTranslation();

				if(pnlCounseling.Visible == true)
					GetRadioButtonTranslation();

				AgreeDisAgreeRadioButton();
			}
		}

		private void AgreeDisAgreeRadioButton()
		{
			CommonFunction.AddItems(rblShareDataYesorNO.Items, "AgreeDisagree", SessionState.LanguageCode);
		}

		private void GetRadioButtonTranslation()
		{
			CommonFunction.AddItems(rblHadConsYesorNO.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblHist2DMPYesorNO.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblHistDMPYesorNO.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblIsPresent.Items, "YesNoRadioButtons", SessionState.LanguageCode);
		}

		//Co-owner pick list Translation
		#region Co-owner pickList Data

		private void PrimaryEducationPickList()
		{
			ddlPriEmp.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlPriEmp.Items, "PrimaryEducationDetails", SessionState.LanguageCode);
		}
		private void EmploymentInfoPickList()
		{
			ddlPriEdu.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlPriEdu.Items, "EmploymentInfo", SessionState.LanguageCode);
		}

        private void MilStatusPickList()
        {
            ddlMilStatus.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            CommonFunction.AddItems(ddlMilStatus.Items, "MilStatus", SessionState.LanguageCode);
        }
        private void BranchOfServicePickList()
        {
            ddlBranchOfService.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            CommonFunction.AddItems(ddlBranchOfService.Items, "BranchOfService", SessionState.LanguageCode);
        }

		private void HispanicPickListCoOwner()
		{
			ddlCoHispanic.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlCoHispanic.Items, "Hispanic", SessionState.LanguageCode);
		}
		private void GetSexPickListCoOwner()
		{
			ddlCoOwnerSex.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlCoOwnerSex.Items, "Sex", SessionState.LanguageCode);
		}

		private void GetRefSourcePickList()
		{
			ddlRefSorce.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlRefSorce.Items, "RefSorceOptions", SessionState.LanguageCode);
		}

		private void GetMaritalSataus()
		{
			MaritalStatus[] maritalStatus = App.Identity.MaritalStatusesGet(SessionState.LanguageCode);

			ddlCoOwnerMaritalStatus.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			ddlMaritalStatus.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			if((maritalStatus != null) && (maritalStatus.Length > 0))
			{
				for(int i = 0; i < (maritalStatus.Length - 3); i++)
				{
					ddlCoOwnerMaritalStatus.Items.Add(new ListItem(maritalStatus[i].Text, maritalStatus[i].MaritalStatusCode.ToString() ?? maritalStatus[i].MaritalStatusCode.ToString())); // Uses Text if Value is null
					ddlMaritalStatus.Items.Add(new ListItem(maritalStatus[i].Text, maritalStatus[i].MaritalStatusCode.ToString() ?? maritalStatus[i].MaritalStatusCode.ToString())); // Uses Text if Value is null
				}
			}
		}

		#endregion

		#region Owner pickList Data

		private void GetHispanicPickList()
		{
			ddlHispanic.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlHispanic.Items, "Hispanic", SessionState.LanguageCode);
		}

		private void GetRacePickList()
		{
			Race[] race = App.Identity.RacesGet(SessionState.LanguageCode);
			ddlRace.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			ddlCoOwnerRace.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			if((race != null) && (race.Length > 0))
			{
				for(int i = 0; i < race.Length; i++)
				{
					ddlRace.Items.Add(new ListItem(race[i].Text, race[i].RaceCode.ToString() ?? race[i].RaceCode.ToString())); // Uses Text if Value is null
					ddlCoOwnerRace.Items.Add(new ListItem(race[i].Text, race[i].RaceCode.ToString() ?? race[i].RaceCode.ToString())); // Uses Text if Value is null
				}
			}
		}

		private void GetEmpickList()
		{
			ddlCoEmp.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
			CommonFunction.AddItems(ddlCoEmp.Items, "EmploymentPickList", SessionState.LanguageCode);
		}

		private void GetSexPickList()
		{
			ddlSex.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlSex.Items, "Sex", SessionState.LanguageCode);
		}

		private void GetStatePickList()
		{
			ddlState.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			string[] statesCodes = App.Geographics.StateCodesGet();

			if(statesCodes != null)
			{
				foreach(string s in statesCodes)
				{
					ddlState.Items.Add(s);
				}
			}

		}
		#endregion

		private void Initialize()
		{
			if(SessionState.ClientNumber != null)
			{
				var contactDetail = UserInfoFromContactDetails(SessionState.ClientNumber.Value);
				GetUserDetailsFromIDM(SessionState.UserId.Value, contactDetail);
				GetUserProfileExtraQuests(SessionState.ClientNumber.Value);
			}
		}

		private void GetUserProfileExtraQuests(int clientId)
		{
			UserProfileExtraQuest UserProfileExtraQuests = null;
			UserProfileExtraQuests = App.Credability.UserProfileExtraQuestGet(clientId);
			if(UserProfileExtraQuests != null)
			{
				rblHadConsYesorNO.SelectedValue = UserProfileExtraQuests.PriorCouns;
				rblHist2DMPYesorNO.SelectedValue = UserProfileExtraQuests.PriorDMP;
				rblHistDMPYesorNO.SelectedValue = UserProfileExtraQuests.RecentDMP;
				rblIsPresent.SelectedValue = UserProfileExtraQuests.CoCounsPres;

				if(UserProfileExtraQuests.ContactEmployed != null)
				{
					ddlPriEmp.ClearSelection();
					ddlPriEmp.SelectedValue = UserProfileExtraQuests.ContactEmployed.Trim();
				}
				if(UserProfileExtraQuests.CosignEmployed != null)
				{
					ddlCoEmp.ClearSelection();
					ddlCoEmp.SelectedValue = UserProfileExtraQuests.CosignEmployed.Trim();

				}
				if(UserProfileExtraQuests.ContactEducation != null)
				{
					ddlPriEdu.ClearSelection();
					ddlPriEdu.SelectedValue = UserProfileExtraQuests.ContactEducation.Trim();
				}

                if (UserProfileExtraQuests.MilitaryStatus != null)
                {
                    ddlMilStatus.ClearSelection();
                    ddlMilStatus.SelectedValue = UserProfileExtraQuests.MilitaryStatus.Trim();
                }
                if (UserProfileExtraQuests.Grade != null)
                {
                    txtGrade.Text = UserProfileExtraQuests.Grade.Trim();
                }
                if (UserProfileExtraQuests.BranchOfService != null)
                {
                    ddlBranchOfService.ClearSelection();
                    ddlBranchOfService.SelectedValue = UserProfileExtraQuests.BranchOfService.Trim();
                }
                if (UserProfileExtraQuests.Rank != null)
                {
                    txtRank.Text = UserProfileExtraQuests.Rank.Trim();
                }
			}
		}

		private bool SaveUserProfileExtraQuests(int clientId)
		{
			bool bUpdate = false;
			bool success = true;
			UserProfileExtraQuest userProfileExtraQuest = new UserProfileExtraQuest();
			UserProfileExtraQuestResult userProfileExtraQuestResult = null;
			userProfileExtraQuest.ClientNumber = clientId;
			if(IsCounselingVisible)
			{
				userProfileExtraQuest.PriorCouns = rblHadConsYesorNO.SelectedValue.Trim();
				userProfileExtraQuest.PriorDMP = rblHist2DMPYesorNO.SelectedValue.Trim();
				userProfileExtraQuest.RecentDMP = rblHistDMPYesorNO.SelectedValue.Trim();
				if(rblIsPresent.SelectedItem != null)
					userProfileExtraQuest.CoCounsPres = rblIsPresent.SelectedValue.Trim();
				bUpdate = true;
			}
			if(IsPriEmploymentEducationVisible)
			{
				userProfileExtraQuest.ContactEducation = ddlPriEdu.SelectedValue.ToString();
				userProfileExtraQuest.ContactEmployed = ddlPriEmp.SelectedValue.ToString();
				bUpdate = true;
			}
            if (IsMilitaryServiceVisible)
            {
                userProfileExtraQuest.MilitaryStatus = ddlMilStatus.SelectedValue.ToString();
                userProfileExtraQuest.Grade = txtGrade.Text;
                userProfileExtraQuest.BranchOfService = ddlBranchOfService.SelectedValue.ToString();
                userProfileExtraQuest.Rank = txtRank.Text;
                bUpdate = true;
            }
			if(IsCoEmploymentVisible)
			{
				userProfileExtraQuest.CosignEmployed = ddlCoEmp.SelectedValue.ToString();
				bUpdate = true;
			}
			if(bUpdate)
			{
				userProfileExtraQuestResult = App.Credability.UserProfileExtraQuestAddUpdate(userProfileExtraQuest);
				if(!userProfileExtraQuestResult.IsSuccessful)
				{
					ShowError(Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
					success = false;
				}
			}
			return success;
		}

		private void ShowError(string error)
		{
			CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, error, true);
		}

		public void GetUserDetailsFromIDM(long user_id, UserContactDetailsBKC contactDetail)
		{
			// Address
			//
			GetUserAddressFromIDM(user_id);
			UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);
			if(user_detail_primary != null)
			{
				PrimaryUserDetailId = user_detail_primary.UserDetailId;

				txtFirstName.Text = user_detail_primary.FirstName.Clean();
				txtInitial.Text = user_detail_primary.MiddleName.Clean();
				txtLastName.Text = user_detail_primary.LastName.Clean();
				txtSSN.Text = user_detail_primary.Ssn.Clean();
				txtDOB.Text = App.DateString(user_detail_primary.BirthDate);
				txtPphone.Text = user_detail_primary.PhoneHome.Clean();
				txtSphone.Text = user_detail_primary.PhoneWork.Clean();
				txtemailaddress.Text = user_detail_primary.Email.Clean();
				hdnFirstName.Value = user_detail_primary.FirstName.Clean();
				hdnLastName.Value = user_detail_primary.LastName.Clean();
				switch(user_detail_primary.IsHispanic)
				{
					case true:
						ddlHispanic.SelectedIndex = 1;
						break;
					case false:
						ddlHispanic.SelectedIndex = 2;
						break;
					default: //null
						ddlHispanic.SelectedIndex = 0;
						break;
				}
				switch(user_detail_primary.IsMale)
				{
					case true:
						ddlSex.SelectedIndex = 1;
						break;
					case false:
						ddlSex.SelectedIndex = 2;
						break;
					default: //null
						ddlSex.SelectedIndex = 0;
						break;
				}
				ddlMaritalStatus.SelectedValue = (user_detail_primary.MaritalStatus != null) ? user_detail_primary.MaritalStatus.MaritalStatusCode.ToString() : string.Empty;
				ddlRace.SelectedValue = (user_detail_primary.Race != null) ? user_detail_primary.Race.RaceCode.ToString() : string.Empty;
			}

			UserDetail user_detail_secondary = App.Identity.UserDetailGet(user_id, false);
			if(user_detail_secondary != null)
			{
				SecondaryUserDetailId = user_detail_secondary.UserDetailId;
				txtCoOwnerFirstName.Text = user_detail_secondary.FirstName.Clean();
				txtCoOwnerInitial.Text = user_detail_secondary.MiddleName.Clean();
				txtCoOwnerLastName.Text = user_detail_secondary.LastName.Clean();
				txtCoOwnerSSN.Text = user_detail_secondary.Ssn.Clean();
				txtCoOwnerDOB.Text = App.DateString(user_detail_secondary.BirthDate);
				ddlCoHispanic.SelectedValue = (user_detail_secondary.IsHispanic == true) ? "1" : "0";
				if(user_detail_secondary.IsMale != null)
					ddlCoOwnerSex.SelectedValue = (user_detail_secondary.IsMale == true) ? "M" : "F";
				ddlCoOwnerMaritalStatus.SelectedValue = (user_detail_secondary.MaritalStatus != null) ? user_detail_secondary.MaritalStatus.MaritalStatusCode.ToString() : string.Empty;
				ddlCoOwnerRace.SelectedValue = (user_detail_secondary.Race != null) ? user_detail_secondary.Race.RaceCode.ToString() : string.Empty;
			}

			if(contactDetail != null && contactDetail.Completed == 1)
			{
				txtFirstName.Enabled = false;
				txtInitial.Enabled = false;
				txtLastName.Enabled = false;
				txtSSN.Enabled = false;
			}
		}

		public UserContactDetailsBKC UserInfoFromContactDetails(int ClientNumber)
		{
			var contactDetail = App.Credability.BKCUserInfoGet(ClientNumber);
			if(contactDetail != null)
			{
				txtOwnerbirthOfCity.Text = contactDetail.BirthOfCity.Clean();
				txtCoOwnersbirthOfCity.Text = contactDetail.CosignBirthOfCity.Clean();
				txtHouseholdSize.Text = contactDetail.NumberOfPeople.ToString().Clean();
				rblShareDataYesorNO.SelectedValue = contactDetail.CcrcAprov;

				// Code added for Pre Purchase Block on 06/24/2010
				if(contactDetail.RefCode != null)
				{
					string[] strRefCCCS = Convert.ToString(contactDetail.RefCode).Split('/');

					if(strRefCCCS.Length > 1)
					{
						ddlRefSorce.SelectedValue = strRefCCCS[1].ToString().Trim();
						UserReferenceDetails userReferenceDetails = App.Credability.UserReferenceDetailsGet(ClientNumber);
						if(userReferenceDetails != null && ddlRefSorce.SelectedValue == "124")
						{
							pnlLendingInstitution.Visible = true;
							txtInstCont.Text = (userReferenceDetails.RefInstitutionName != null) ? userReferenceDetails.RefInstitutionName.Trim() : string.Empty;
							txtInstName.Text = (userReferenceDetails.RefContactName != null) ? userReferenceDetails.RefContactName.Trim() : string.Empty;
							txtInstEmail.Text = (userReferenceDetails.RefContactEmail != null) ? userReferenceDetails.RefContactEmail.Trim() : string.Empty;
						}
						else
							pnlLendingInstitution.Visible = false;
					}
				}
			}
			return contactDetail;
		}

		private void GetUserAddressFromIDM(long user_id)
		{
			Address[] address = App.Identity.AddressesGet(user_id);
			if((address != null) && (address.Length != 0))
			{
				UserAddressId = address[0].AddressId;
				txtAddress.Text = address[0].StreetLine1.Clean();
				txtAddress2.Text = address[0].StreetLine2.Clean();
				txtCity.Text = address[0].City.Clean();
				txtZipCode.Text = address[0].Zip.Clean();
				ddlState.SelectedValue = address[0].State.Clean();
			}
		}

		public bool SaveUserDetailPrimary(long user_id)
		{
			bool is_successful = false;
			UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);
			if(user_detail_primary != null)
			{
				user_detail_primary.FirstName = txtFirstName.Text.Clean();
				user_detail_primary.LastName = txtLastName.Text.Clean();
				user_detail_primary.MiddleName = txtInitial.Text.Clean();
				user_detail_primary.Ssn = txtSSN.Text.Clean();
				user_detail_primary.BirthDate = Convert.ToDateTime(txtDOB.Text.Clean());
				if(IsEmailVisible)
				{
					user_detail_primary.Email = txtemailaddress.Text.Clean();
				}
				user_detail_primary.PhoneHome = txtPphone.Text.Clean();
				user_detail_primary.PhoneWork = txtSphone.Text.Clean();
				switch(ddlHispanic.SelectedIndex)
				{
					case 0:
						user_detail_primary.IsHispanic = null;
						break;
					case 1:
						user_detail_primary.IsHispanic = true;
						break;
					case 2:
						user_detail_primary.IsHispanic = false;
						break;
				}
				switch(ddlSex.SelectedIndex)
				{
					case 0:
						user_detail_primary.IsMale = null;
						break;
					case 1:
						user_detail_primary.IsMale = true;
						break;
					case 2:
						user_detail_primary.IsMale = false;
						break;
				}
				if(!string.IsNullOrEmpty(ddlMaritalStatus.SelectedValue))
				{
					user_detail_primary.MaritalStatus = new MaritalStatus { MaritalStatusCode = ddlMaritalStatus.SelectedValue[0] };
				}
				if(!string.IsNullOrEmpty(ddlRace.SelectedValue))
				{
					user_detail_primary.Race = new Race { RaceCode = ddlRace.SelectedValue[0] };
				}

				string AuditorName = txtFirstName.Text.Clean() + " " + txtLastName.Text.Clean(); // TODO: SessionState.Username
				UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_primary, AuditorName);
            UserAddressUpdateIntoIDM(user_detail_primary.UserId);

				is_successful = user_detail_save_result.IsSuccessful;
				if(!is_successful)
				{
					if((bool) user_detail_save_result.IsDuplicateEmail)
					{
						CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, EmailAlreadyExistMessage, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 719: " + EmailAlreadyExistMessage, true);
					}
					if((bool) user_detail_save_result.IsDuplicateSsn)
					{
						CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, SSNAlreadyExistMessage, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 724: " + SSNAlreadyExistMessage, true);
					}

					if(!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
					{                        
                        CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, user_detail_save_result.ExceptionStr, true);
                        CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 730: " + user_detail_save_result.Exception.ToString(), true);
					}
				}
			}
			return is_successful;
		}

		private void MakeErrorDivVisible(bool visible)
		{
			dvServerSideValidation.Visible = visible;
		}

		public bool SaveUserDetailSecondary(long UserId)
		{
			bool is_successful = false;
			UserDetail user_detail_secondary = App.Identity.UserDetailGet(UserId, false);
			if(user_detail_secondary == null) // For new Secondry User
			{
				user_detail_secondary = new UserDetail { UserId = UserId, IsPrimary = false, };
			}

			user_detail_secondary.FirstName = txtCoOwnerFirstName.Text.Clean();
			user_detail_secondary.MiddleName = txtCoOwnerInitial.Text.Clean();
			user_detail_secondary.LastName = txtCoOwnerLastName.Text.Clean();
			user_detail_secondary.Ssn = txtCoOwnerSSN.Text.Clean();
			user_detail_secondary.BirthDate = txtCoOwnerDOB.Text.Clean() != string.Empty ? Convert.ToDateTime(txtCoOwnerDOB.Text.Clean()) : (DateTime?) null;
			user_detail_secondary.IsHispanic = (ddlCoHispanic.SelectedValue == "1");
			user_detail_secondary.IsMale = (ddlCoOwnerSex.SelectedValue == "M");

			if(!string.IsNullOrEmpty(ddlCoOwnerMaritalStatus.SelectedValue))
			{
				user_detail_secondary.MaritalStatus = new MaritalStatus { MaritalStatusCode = Convert.ToChar(ddlCoOwnerMaritalStatus.SelectedValue.Trim()) };
				;
			}

			if(!string.IsNullOrEmpty(ddlCoOwnerRace.SelectedValue.Trim()))
			{
				user_detail_secondary.Race = new Race { RaceCode = Convert.ToChar(ddlCoOwnerRace.SelectedValue.Trim()) };
			}


			string AuditorName = txtCoOwnerFirstName.Text.Trim() + " " + txtCoOwnerLastName.Text.Trim(); // TODO: SessionState.Username

			UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_secondary, AuditorName);

			is_successful = user_detail_save_result.IsSuccessful;

			if(!is_successful)
			{
				if(user_detail_save_result.IsDuplicateEmail == true)
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, EmailAlreadyExistMessage, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 782: " + EmailAlreadyExistMessage, true);
				}

				if(user_detail_save_result.IsDuplicateSsn == true)
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, SSNAlreadyExistMessage, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 788: " + SSNAlreadyExistMessage, true);
				}

				if(!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
				{
                    CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, user_detail_save_result.Exception.ToString(), true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 749: " + user_detail_save_result.Exception.ToString(), true);

				}
			}

			return is_successful;
		}

		private bool UserAddressUpdateIntoIDM(long user_id)
		{
			bool is_successful = false;

			Address address = null;

			if(UserAddressId != null)
			{
				Address[] addresses = App.Identity.AddressesGet(user_id);

				if((addresses != null) && (addresses.Length > 0))
				{
					address = addresses[0];
				}
			}

			if(address == null)
			{
				address = new Address { UserId = user_id, };
			}

			address.AddressType = "HOME";
			address.StreetLine1 = txtAddress.Text.Clean();
			address.StreetLine2 = txtAddress2.Text.Clean();
			address.City = txtCity.Text.Clean();
			address.Zip = txtZipCode.Text.Clean();
			address.State = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;

			string AuditorName = txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim();

			AddressSaveResult result = App.Identity.AddressSave(address, AuditorName);

			is_successful = result.IsSuccessful;

			if(is_successful)
			{
				UserAddressId = result.AddressId;
			}
			else
			{
				if(result.IsInvalidZipCode == true)
				{
					string ErrorMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|InValidState") + " " + result.StateExpectedForZip;     // TODO: Translate

					CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, ErrorMessage, true);
					CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 847: " + ErrorMessage, true);
				}

				if(!string.IsNullOrEmpty(result.ExceptionStr))
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvServerSideValidation, result.ExceptionStr, true);
                    CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, "Line 853: " + result.ExceptionStr, true);
				}
			}

			return is_successful;
		}

		public FormSaveInforResult SaveFormValue()
		{
			FormSaveInfor FormSaveInfor = new FormSaveInfor();
			FormSaveInfor.ClientNumber = Convert.ToInt32(SessionState.ClientNumber);
			FormSaveInfor.Form1 = 0;
			FormSaveInfor.Form2 = 0;
			FormSaveInfor.Form3 = 0;
			FormSaveInfor.Form4 = 0;
			FormSaveInfor.Form5 = 0;
			FormSaveInfor.Form6 = 0;
			FormSaveInfor.Form7 = 1;

			FormSaveInforResult FormSaveInforResult = App.Credability.SaveFormInfo(FormSaveInfor);
			SessionState.ModForm = 2;
			return FormSaveInforResult;
		}

		private void UserInfoAddUpdate(Boolean is_secondary_valid)
		{
			if((chkConfirmation.Checked || (!pnlAuthorizeShareParticipate.Visible)) && !string.IsNullOrEmpty(txtOwnerbirthOfCity.Text.Clean()))
			{
				// Update UserContactDetails
				bool success = true;
				UserContactDetails user_contact_details = App.Credability.UserInfoGet(SessionState.ClientNumber.Value) ?? new UserContactDetails();

				user_contact_details.ClientNumber = SessionState.ClientNumber.Value;
				user_contact_details.BirthOfCity = txtOwnerbirthOfCity.Text.Clean();


				if(is_secondary_valid)
				{
					user_contact_details.CosignBirthOfCity = txtCoOwnersbirthOfCity.Text.Clean();

				}
				else
				{
					user_contact_details.CosignBirthOfCity = string.Empty;
					//AFS afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
					//if (afs != null)
					//{
					//    //afs.IsSecondaryPersonNeeded = false;
					//    afs.SecondaryPerson = string.Empty;
					//    AFSResult afsResult = App.Credability.AfsAddUpdate(afs);
					//    success = afsResult.IsSuccessful;
					//}
				}
              //  if (pnlAuthorizeShareParticipate.Visible == true)
                if(dvBchHoShareInfo.Visible == true)				
				{
					if(rblShareDataYesorNO.SelectedItem != null)
						user_contact_details.CcrcAprov = rblShareDataYesorNO.SelectedValue;
				}

				if(spnPrePurchase.Visible == true)
				{
					user_contact_details.NumberOfPeople = Convert.ToInt32(txtHouseholdSize.Text.Trim());
					user_contact_details.RefCode = "Prp/" + ddlRefSorce.SelectedValue;

					#region User Reference Details Add/Update

					UserReferenceDetails userReferenceDetails = new UserReferenceDetails();
					UserReferenceDetailsResults userReferenceDetailsResults = null;
					if(ddlRefSorce.SelectedValue == "124")
					{
						userReferenceDetails.ClientNumber = SessionState.ClientNumber.Value;
						userReferenceDetails.RefInstitutionName = txtInstName.Text.Trim();
						userReferenceDetails.RefContactEmail = txtInstEmail.Text.Trim();
						userReferenceDetails.RefContactName = txtInstCont.Text.Trim();
					}
					else
					{
						userReferenceDetails.ClientNumber = SessionState.ClientNumber.Value;
						userReferenceDetails.RefInstitutionName = String.Empty;
						userReferenceDetails.RefContactEmail = String.Empty;
						userReferenceDetails.RefContactName = String.Empty;
					}
					userReferenceDetailsResults = App.Credability.UserPreHudAddUpdate(userReferenceDetails);

					#endregion
				}

				if(success)
				{
					UserContactDetailsResult result = App.Credability.UserInformationUpdate(user_contact_details);
					if(result.IsSuccessful)
					{
                        // Bankruptcy Conversion
                        //App.Host.ReqCredRpt(SessionState.UserId.Value, SessionState.ClientNumber.Value);
                        App.Debtplus.ReqCredRpt(SessionState.UserId.Value, SessionState.ClientNumber.Value);

                        FormSaveInforResult FormSaveInforResult = SaveFormValue();

						if(FormSaveInforResult.IsSuccessful)
						{
							if(is_secondary_valid)
							{
								AFS afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);

								if(txtCoOwnerFirstName.Text != String.Empty && txtCoOwnerLastName.Text != String.Empty)
								{
									AFS AfsUpdate = new AFS();
									AFSResult AFSResults = null;
									AfsUpdate.ClientNumber = SessionState.ClientNumber.Value;
									AfsUpdate.PrimaryPerson = afs.PrimaryPerson;
									AfsUpdate.SecondaryPerson = String.Empty;
									AFSResults = App.Credability.AfsAddUpdate(AfsUpdate);
								}

								if((afs != null) && string.IsNullOrEmpty(afs.SecondaryPerson))
								{
									Response.Redirect(RedirectOnNeedAuthorization);
								}
								else
								{
									Response.Redirect(RedirectOnContinue);
								}
							}
							else
							{
								Response.Redirect(RedirectOnContinue);
							}
						}
						else
						{
							dvTopErrorMessage.InnerHtml = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"); // TODO: Translate
						}

					}
					else
					{
						dvTopErrorMessage.InnerHtml = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"); // TODO: Translate
					}
				}
				else
				{
					dvTopErrorMessage.InnerHtml = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"); // TODO: Translate
				}
			}
			else
			{
				dvTopErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI");		// TODO: Translate
			}
		}
      protected void click_btnSaveExit(object sender, EventArgs e)
      {
         CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
         Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
      }

      protected void btnSaveExit_Click(object sender, EventArgs e)
      {
         CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
         if (Page.IsValid)
         {
            SaveUserDetailPrimary(SessionState.UserId.Value);
            SaveUserDetailSecondary(SessionState.UserId.Value);
         }
         //SessionState.SessionCompleted = true;
         Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
         Response.Redirect("http://credability.org");
      }

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			StringBuilder sb = new StringBuilder();
			if(Page.IsValid)
			{
				dvServerSideValidation.Visible = false;
				dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";	// TODO: Translate

				bool is_primary_valid = true;
				bool? is_secondary_valid = null;

                var _user = App.Identity.UserGet((long)SessionState.UserId);

                SessionState.Username = _user.Username;
                textSecurityQuestion.InnerText = _user.SecurityQuestionAnswer.Text;
                var _SecurityAns = _user.SecurityQuestionAnswer.SecurityAnswer;

            AffirmError.Visible = false;
            if (textUsername.Text.IsNullOrWhiteSpace() || textUsername.Text.Trim().ToLower() != SessionState.Username.Trim().ToLower())
            {
               AffirmError.Visible = true;
               AffirmError.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|ISN") + "</li>";
               return;
            }
            if (textSecurityAns.Text.IsNullOrWhiteSpace() || _SecurityAns.Trim().ToLower() != textSecurityAns.Text.Trim().ToLower())
            {
               AffirmError.Visible = true;
               AffirmError.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|ISA") + "</li>";
               return;
            }
            if (_SecurityAns.Trim().IsNullOrWhiteSpace())
            {
                AffirmError.Visible = true;
                AffirmError.InnerHtml += "<li>Unable to determine your original security answer.  Please log back in and try again.</li>";
                return;
            }
            if (AffirmError.Visible) {
                return;
            }

            DateTime ownerDateOfBirth;
				int ownerYears;
				if(!DateTime.TryParse(txtDOB.Text, out ownerDateOfBirth) || !IsAgeInRange(ownerDateOfBirth, out ownerYears))
				{
					dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|YASP") + "</li>";
					is_primary_valid = false;
				}

				if(!String.IsNullOrEmpty(txtCoOwnerDOB.Text))
				{
					DateTime coOwnerDateOfBirth;
					int coOwnerYears;
					if(!DateTime.TryParse(txtCoOwnerDOB.Text, out coOwnerDateOfBirth) || !IsAgeInRange(coOwnerDateOfBirth, out coOwnerYears))
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|CASS") + "</li>";
						is_secondary_valid = false;
					}
				}

				// Owner validation
				//
				if(string.IsNullOrEmpty(txtOwnerbirthOfCity.Text.Clean()))
				{
					dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|OBOCIR") + "</li>"; // TODO: Translate
					is_primary_valid = false;
				}            

				if(!chkConfirmation.Checked && pnlAuthorizeShareParticipate.Visible)
				{
					dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|YMAITTC") + "</li>"; // TODO: Translate
					is_primary_valid = false;
				}

				string state_code = App.Geographics.StateCodeGetByZip(txtZipCode.Text.Clean());
				if(string.IsNullOrEmpty(state_code))
				{
					dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|IZC") + "</li>"; // TODO: Translate
					is_primary_valid = false;
				}
				else if(string.Compare(state_code, ddlState.SelectedValue, true) != 0)
				{
					dvServerSideValidation.InnerHtml += string.Format("<li>" + App.Translate("Credability|UserProfileBCH|SEFZI") + "</li>", txtZipCode.Text, state_code); // TODO: Translate
					is_primary_valid = false;
				}

				// Co-owner Server side validation
				//
				if (txtCoOwnerFirstName.Text.Clean() != string.Empty || txtCoOwnerLastName.Text.Clean() != string.Empty)
				{
					is_secondary_valid = true;
                    if (!chkCoOwnerConfirmation.Checked && pnlAuthorizeShareParticipate.Visible)
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COMATTATC") + "</li>"; // TODO: Translate
						is_secondary_valid = false;
					}

					if(txtCoOwnerLastName.Text.Clean() == string.Empty)
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COLNR") + "</li>"; // TODO: Translate
						is_secondary_valid = false;
					}

					if(txtCoOwnerSSN.Text.Clean() == string.Empty)
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COSSNR") + "</li>";	// TODO: Translate
						is_secondary_valid = false;
					}

					if(ddlCoOwnerMaritalStatus.SelectedValue == string.Empty)	 // TODO: Translate
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COMSIR") + "</li>";	// TODO: Translate
						is_secondary_valid = false;
					}

					if(IsCoEmploymentVisible)
					{
						if(ddlCoEmp.SelectedValue == "0")
						{
							// TODO: add translation data entry
							//dvServerSideValidation.InnerHtml += "<li>" + Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|CoOwnerEmpReq") + "</li>";	
							dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COE") + "</li>";
							is_secondary_valid = false;
						}
					}

					if(pnlCounseling.Visible == true && txtCoOwnerFirstName.Text != String.Empty && rblIsPresent.SelectedValue == "")
					{
						dvServerSideValidation.InnerHtml += string.Format("<li>" + App.Translate("Credability|BCH|ISPR") + "</li>", txtCoOwnerFirstName.Text); // TODO: Translate
						is_secondary_valid = false;
					}

					if(string.IsNullOrEmpty(txtCoOwnersbirthOfCity.Text.Clean()))
					{
						dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|COBOCR") + "</li>";	// TODO: Translate
						is_secondary_valid = false;
					}
				}
				dvServerSideValidation.InnerHtml += "</ul>";

				if((!chkConfirmation.Checked && pnlAuthorizeShareParticipate.Visible) || !is_primary_valid || is_secondary_valid == false)
				{
					dvServerSideValidation.Visible = true;
					dvServerSideValidation.Focus();
				}
				else if(SaveUserDetailPrimary(SessionState.UserId.Value)
					&& UserAddressUpdateIntoIDM(SessionState.UserId.Value)
					&& SaveUserProfileExtraQuests(SessionState.ClientNumber.Value))
				{
					if(txtCoOwnerFirstName.Text == String.Empty && txtCoOwnerLastName.Text == String.Empty)
					{
						AFS afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);

						AFS AfsUpdate = new AFS();
						AFSResult AFSResults = null;
						AfsUpdate.ClientNumber = SessionState.ClientNumber.Value;
						AfsUpdate.PrimaryPerson = afs.PrimaryPerson;
						AfsUpdate.SecondaryPerson = String.Empty;
						AFSResults = App.Credability.AfsAddUpdate(AfsUpdate);
					}

					if(is_secondary_valid == true)
					{
						if(SaveUserDetailSecondary(SessionState.UserId.Value))
						{
							UserInfoAddUpdate(true); // Update the information in contactdetails.
						}
					}
					else
					{
						App.Identity.UserDetailDeleteSecondary(SessionState.UserId.Value);
						UserInfoAddUpdate(false); // Update the information in contactdetails.
					}
				}
			}
			else
			{
				// TODO: Unknown Validation Error message
			}
		}

		public static int CalculateAge(DateTime birthdate)
		{
			DateTime now = DateTime.Today;
			int years = now.Year - birthdate.Year;

			// Subtract another year if we're before the birthday in the current year.
			if(now.Month < birthdate.Month || (now.Month == birthdate.Month && now.Day < birthdate.Day))
				years--;

			return years;
		}

		private static bool IsAgeInRange(DateTime dateOfBirth, out int ageInYears)
		{
			int years = CalculateAge(dateOfBirth);
			if(years < 13 || years >= 108)
			{
				ageInYears = 0;
				return false;
			}

			ageInYears = years;
			return true;
		}

		protected void btnEditAttorney_Click(object sender, EventArgs e)
		{
			SessionState.EditAttorney = "Y";
			Response.Redirect("Registration.aspx");
		}

		protected void ddlRefSorce_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(ddlRefSorce.SelectedValue == "124")
			{
				pnlLendingInstitution.Visible = true;
				txtInstName.Focus();
			}
			else
				pnlLendingInstitution.Visible = false;

		}
	}
}