﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcFinancialAnalysis : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
				if(SessionState.ClientNumber != null)
				{
					CalculateFinancialSituation();
				}
			}
			base.Page_Load(sender, e);
		}

		private void CalculateFinancialSituation()
		{
			FinanicalSituationDetails AnalyzedResult = App.Credability.UserFinalFinanicalSituationGet(SessionState.ClientNumber.Value);
			if(AnalyzedResult != null)
			{
				lblNetMonthlyIncome.Text = AnalyzedResult.Income.NetIncomeTotalBoth.ToString(App.CurrencyFormat);
				lblMonthlyLivingExpenses.Text = AnalyzedResult.MonthlyExpenses.ExpensesTotal.ToString(App.CurrencyFormat);
				MonthlyDebtPayments.Text = AnalyzedResult.AssetsLiabilities.CreditorTotals.Payments.ToString(App.CurrencyFormat);
				lblNetWorth.Text = AnalyzedResult.AssetsLiabilities.NetWorth.ToString(App.CurrencyFormat);
				if(AnalyzedResult.AssetsLiabilities.NetWorth >= 0)
				{
					lblNetWorth.Style["color"] = "#333";
				}
				else
				{
					lblNetWorth.Style["color"] = "Red";
				}
				if(Session["TotalExp"] == null)
					Session.Add("TotalExp", AnalyzedResult.MonthlyExpenses.ExpensesTotal);
				else
					Session["TotalExp"] = AnalyzedResult.MonthlyExpenses.ExpensesTotal;
				if(Session["DispIncome"] == null)
					Session.Add("DispIncome", AnalyzedResult.DisposableIncome);
				else
					Session["DispIncome"] = AnalyzedResult.DisposableIncome;
				lblDisposableIncome.Text = AnalyzedResult.DisposableIncome.ToString(App.CurrencyFormat);
				if(AnalyzedResult.DisposableIncome >= 0)
				{
					lblDisposableIncome.Style["color"] = "#333";
				}
				else
				{
					lblDisposableIncome.Style["color"] = "Red";
				}
				txtReasonHelp.Text = AnalyzedResult.ReasonForhelp;

                // Bankruptcy Conversion
				var creditScoreResult = App.Debtplus.GetCredScore(SessionState.ClientNumber.Value);
				if(creditScoreResult.IsSuccessful)
				{
                    //var creditScores = creditScoreResult.Value;
                    PrimaryCreditScoreLiteral.Text = creditScoreResult.PrimaryScore;// creditScores.PrimaryScore;
                    SecondaryCreditScoreLiteral.Text = String.IsNullOrEmpty(creditScoreResult.SecondaryScore) ? "N/A" : creditScoreResult.SecondaryScore;// String.IsNullOrEmpty(creditScores.SecondaryScore) ? "N/A" : creditScores.SecondaryScore;
				}
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			var SaveResult = SaveIncomeExpenses();
			if(SaveResult.IsSuccessful)
			{
				NavigateContinue();
			}
			else
			{
				DvError.InnerHtml = "Some Error Occured Please try later";
			}
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			var SaveResult = SaveIncomeExpenses();
			if(SaveResult.IsSuccessful)
			{
				NavigateExit();
			}
			else
			{
				DvError.InnerHtml = "Some Error Occured Please try later";
			}
		}

		protected UserReasonForHelpResult SaveIncomeExpenses()
		{
			UserReasonForHelp UserReasonsForHelp = new UserReasonForHelp();
			UserReasonsForHelp.ReasonForHelp = txtReasonHelp.Text;
			UserReasonsForHelp.ClientNumber = SessionState.ClientNumber.Value;
			if(Session["DispIncome"] != null)
			{
				UserReasonsForHelp.MonthlyDisposableIncome = Convert.ToDecimal(Session["DispIncome"]);
			}
			else
			{
				UserReasonsForHelp.MonthlyDisposableIncome = 0;
			}
			if(Session["TotalExp"] != null)
			{
				UserReasonsForHelp.MonthlyExpenses = float.Parse(Session["TotalExp"].ToString());
			}
			else
			{
				UserReasonsForHelp.MonthlyExpenses = 0;
			}
			return App.Credability.UserReasonForHelpUpdate(UserReasonsForHelp);
		}
	}
}