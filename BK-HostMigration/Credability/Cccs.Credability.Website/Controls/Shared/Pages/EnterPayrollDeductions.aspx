﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnterPayrollDeductions.aspx.cs" Inherits="Cccs.Credability.Website.Controls.Shared.Pages.EnterPayrollDeductions" MasterPageFile="~/MasterPages/Master.Master" Title="Enter Payroll Deductions" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    
    <div style="display:none;" runat="server" id="divAction"></div>
    <asp:HiddenField runat="server" id="frmAction" />
    <asp:HiddenField runat="server" id="deductionsList" />
    <div style="width:50%;margin-left:20%;">
    <h1>Payroll Deductions</h1>
    <p class="NormTxt">Please enter your payroll deductions here.</p>
    </div>

        <table id="Table1" border="0" class="TblUserdebtListing" style="width:50%;margin-left:20%;">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: Deductions">
                <tr data-bind="">
                    <td><input type="text" data-bind="value: Description" /></td>
                    <td><input type="text" data-bind="value: Amount" />
                    </td>
                    <td><button data-bind="click: $root.Remove">Remove</button>
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="width:50%;margin-left:20%;margin-right:30%;">
            <button data-bind="click: addDeduction">Add</button>
        </div>
    
    <div id="divNav" style="display:none;width:50%;margin-left:20%;margin-right:30%;margin-top:20px;">
        <div class="lnkbutton">
            <a id="btnPrevious" class="previous" title="Return To Previous Page" ><span>Return To Previous Page</span></a>
        </div>
        <div class="lnkbutton">
            <a id="btnExit" title="Save & Exit"><span>Save & Exit</span></a>
        </div>
        <div class="lnkbutton">
            <a id="btnContinue" title="Continue"><span>Continue</span></a>
        </div>
    </div>
    
    <script type="text/javascript" src="https://credability-cdn.azurewebsites.net/js/json2/json2-10.19.11.min.js"></script>
    <script type="text/javascript" src="https://credability-cdn.azurewebsites.net/js/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="https://credability-cdn.azurewebsites.net/js/knockout/knockout-2.2.0.js"></script>
    <script type="text/javascript" src="https://credability-cdn.azurewebsites.net/js/knockout.mapping/knockout.mapping-2.3.2.min.js"></script>
    <script type="text/javascript">
        var Deduction = function (description, amount) {
            var self = this;

            this.Description = ko.observable(description);

            this.Description.isValid = function () {
                return true;
            };

            this.Amount = ko.observable(amount);

            //can't get a good regex that works, so gonna try another way
            this.Amount.isValid = function () {
                var amt = self.Amount().toString();
                var split = amt.split('.');
                if (split.length > 2) {
                    return false;
                }
                var dollars = split[0].replace(/[^\d+]/g, '');
                var cents = split.length > 1 ? split[1].replace(/[^\d+]/g, '') : '0';

                dollars = parseInt(dollars);
                cents = parseInt(cents);

                if (isNaN(dollars) || isNaN(cents)) {
                    return false;
                }

                self.Amount(dollars + (cents / 100));

                return true;
            };
        }

        var viewModel = function () {
            var self = this;

            this.Deductions = ko.observableArray([]);

            var deductions = $('[name$=deductionsList]').val();
            if (deductions && deductions !== '') {
                deductions = JSON.parse(deductions);
            } else {
                deductions = [];
            }

            for (var i = 0, l = deductions.length; i < l; i++) {
                var curr = deductions[i];
                if (curr.Description && curr.Amount) {
                    this.Deductions.push(new Deduction(curr.Description, curr.Amount));
                }
            }

            if (deductions.length < 1) {
                this.Deductions.push(new Deduction('', ''));
            }

            this.Remove = function (deduction) {
                var msg = 'Are you sure you wish to remove the deduction ' + deduction.Description()
                    + ' for $' + deduction.Amount() + '?';

                if (confirm(msg)) {
                    self.Deductions.remove(deduction);
                }
            };
        };

        viewModel.prototype.addDeduction = function () {
            var d = new Deduction('', 0);
            this.Deductions.push(d);
            d.Amount(0);
        };

        viewModel.prototype.isValid = function () {
            var toReturn = true;
            var deductions = this.Deductions();

            for (var i = 0, l = deductions.length; i < l; i++) {
                //totally blank is acceptable, b/c we'll just strip it out before sending
                if (deductions[i].Amount() === '' && deductions[i].Description() === '') {
                    return true;
                }

                try {//validate one that has values
                    toReturn = toReturn && deductions[i].Amount.isValid();
                    toReturn = toReturn && deductions[i].Description.isValid();
                } catch (err) {
                    alert(err);
                }

                if (!toReturn) {
                    return false;
                }
            }

            return toReturn;
        };

        //removes 'emtpy' entries from an observable array
        var stripEmptyDeductions = function (Deductions) {
            var deductions = Deductions();

            for (var i = deductions.length - 1; i >= 0; i--) {
                var curr = deductions[i];
                if (curr.Description() === '' && curr.Amount() === '') {
                    Deductions.remove(curr);
                }
            }
        };

        $(function () {
            var myVm = new viewModel();

            ko.applyBindings(myVm);

            var _saveDeductions = function () {

                if (myVm.isValid()) {

                    stripEmptyDeductions(myVm.Deductions);

                    var d = ko.mapping.toJS(myVm.Deductions);

                    $('[name$=deductionsList]').val(JSON.stringify(d));

                } else {
                    console.log('invalid');
                }
            };

            $('#btnPrevious').click(function (e) {

                e.preventDefault();

                $('[name$=frmAction]').val('Previous');

                _saveDeductions();

                $('form').submit();
            });

            $('#btnExit').click(function (e) {
                e.preventDefault();

                $('[name$=frmAction]').val('Exit');

                _saveDeductions();

                $('form').submit();
            });

            $('#btnContinue').click(function (e) {
                e.preventDefault();

                $('[name$=frmAction]').val('Continue');

                _saveDeductions();

                $('form').submit();
            });

            $('#divNav').show();
        });
    </script>

</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>