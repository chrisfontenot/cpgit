﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcNetWorth : CounselingUcBase
	{
		public string HousingType = string.Empty;
		public string CredBal = "0";
		public bool IsSubmitButtonVisible
		{
			get
			{
				return btnSubmit.Visible;
			}
			set
			{
				btnSubmit.Visible = value;
			}
		}

		protected new void Page_Load(object sender, EventArgs e)
		{
			InitializeTranslation();

			if(!IsPostBack)
			{
				UserAssetsLiabilities AssetLiabilities = App.Credability.UserAssetLiabilitiesGet(SessionState.ClientNumber.Value);
				if(AssetLiabilities != null)
				{
					CredBal = AssetLiabilities.CreditorTotals.Balance.ToString();
					ViewState["CreaditBals"] = AssetLiabilities.CreditorTotals.Balance;
					lblUnsecuredDebt.Text = AssetLiabilities.CreditorTotals.Balance.ToString(App.CurrencyFormat);
					txtHomeVal.Text = AssetLiabilities.ValHome.ToString(App.CurrencyFormat);
					txtAutoVal.Text = AssetLiabilities.ValCar.ToString(App.CurrencyFormat);
					txtOthrVal.Text = AssetLiabilities.ValOther.ToString(App.CurrencyFormat);
					txtHomeOwe.Text = AssetLiabilities.OweHome.ToString(App.CurrencyFormat);
					txtAutoOwe.Text = AssetLiabilities.OweCar.ToString(App.CurrencyFormat);
					txtSecOwe.Text = AssetLiabilities.SecondaryAmt.ToString(App.CurrencyFormat);
					txtOthrOwe.Text = AssetLiabilities.OweOther.ToString(App.CurrencyFormat);
					txtInterRate.Text = (AssetLiabilities.MortRate / 100).ToString("P");
					txtPrptVal.Text = AssetLiabilities.ValProp.ToString(App.CurrencyFormat);
					txtRetrVal.Text = AssetLiabilities.ValRet.ToString(App.CurrencyFormat);
					txtBnkAccVal.Text = AssetLiabilities.ValSav.ToString(App.CurrencyFormat);
					HousingType = AssetLiabilities.HousingType;
				}
			}
			base.Page_Load(sender, e);
		}

		private void InitializeTranslation()
		{
			ValidationControlTranslation();
		}

		private void ValidationControlTranslation()
		{
			rfvHomeVal.ErrorMessage = App.Translate("Credability|UnderstandingYourNetWorths|EC");
			rfvPrptVal.ErrorMessage = App.Translate("Credability|UnderstandingYourNetWorths|RE");
			rfvAutoVal.ErrorMessage = App.Translate("Credability|UnderstandingYourNetWorths|AR");
			rftxtInterRate.ErrorMessage = App.Translate("Credability|UnderstandingYourNetWorths|MBR");
			rfvAutoOwe.ErrorMessage = App.Translate("Credability|UnderstandingYourNetWorths|ALR");
			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			UserAssetsLiabilitiesResult UserAssetNetWorthResults = App.Credability.UserAssetsLiabilitiesUpdate(GetFormData());
			if(UserAssetNetWorthResults.IsSuccessful && Page.IsValid)
			{
                if (App.WebsiteCode == Cccs.Identity.Website.REC || App.WebsiteCode == Cccs.Identity.Website.BCH || App.WebsiteCode == Cccs.Identity.Website.HUD)
					SubmitToQueue(true);
				NavigateContinue();
			}
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			UserAssetsLiabilitiesResult UserAssetNetWorthResults = App.Credability.UserAssetsLiabilitiesUpdate(GetFormData());
			if(UserAssetNetWorthResults.IsSuccessful)
			{
				NavigateExit();
			}
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			UserAssetsLiabilitiesResult UserAssetNetWorthResults = App.Credability.UserAssetsLiabilitiesUpdate(GetFormData());
			if(UserAssetNetWorthResults.IsSuccessful)
			{
				NavigatePrevious();
			}
		}

		protected void btnSaveAndExitAndSubmitToQueue_Click(object sender, EventArgs e)
		{
			App.Credability.UserAssetsLiabilitiesUpdate(GetFormData());
			SubmitToQueue();
		}

		private UserAssetsLiabilities GetFormData()
		{
			UserAssetsLiabilities AssetsLiabilities = new UserAssetsLiabilities();

			AssetsLiabilities.ClientNumber = SessionState.ClientNumber.Value;
			AssetsLiabilities.ValHome = txtHomeVal.Text.ToFloat(0);
			AssetsLiabilities.ValCar = txtAutoVal.Text.ToFloat(0);
			AssetsLiabilities.ValOther = txtOthrVal.Text.ToFloat(0);
			AssetsLiabilities.OweHome = txtHomeOwe.Text.ToFloat(0);
			AssetsLiabilities.OweCar = txtAutoOwe.Text.ToFloat(0);
			AssetsLiabilities.SecondaryAmt = txtSecOwe.Text.ToFloat(0);
			AssetsLiabilities.OweOther = txtOthrOwe.Text.ToFloat(0);
			AssetsLiabilities.MortRate = txtInterRate.Text.ToFloat(0);
			AssetsLiabilities.ValProp = txtPrptVal.Text.ToFloat(0);
			AssetsLiabilities.ValRet = txtRetrVal.Text.ToFloat(0);
			AssetsLiabilities.ValSav = txtBnkAccVal.Text.ToFloat(0);
			AssetsLiabilities.ModForm5 = 1;

			return AssetsLiabilities;
		}
	}
}