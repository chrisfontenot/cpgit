﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.UserProfile" %>
<style type="text/css">  
    .style1
    {
        width: 13%;
    }
    .style2
    {
        width: 87px;
    }
</style>
<asp:Panel ID="pnlUserProfile" runat="server" DefaultButton="btnContinue">
    <script type="text/javascript" src="/Content/ErrorHandling.js"></script>
    <script type="text/javascript">
        
        function ReSetPhone(theField) {
            var Val = stripCharsNotInBag(theField.value, "1234567890");
            if (Val == "") {
                theField.value = ""
            }
            else {
                theField.value = reformatUSPhone(Val);
            }
        }

        function FormatAsText(theField) {
            var Val = stripCharsNotInBag(theField.value, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-, /'");
            if (Val == "") {
                theField.value = ""
            }
            else {
                theField.value = Val;
            }
        }
        function ReSetSSN(theField) {
            var Val = stripCharsNotInBag(theField.value, "1234567890");
            if (Val == "") {
                theField.value = ""
            }
            else {
                theField.value = reformatSSN(Val);
            }
        }
        function ReSetLastName(theField) {
            var LastName = theField.value;
            LastName = LastName.replace("-", " ");
            theField.value = LastName;
            CleanField(theField);
        }
        function ReSetLastName(theField) {
            var LastName = theField.value;
            LastName = LastName.replace("-", " ");
            theField.value = LastName;
            CleanField(theField);
        }
        function CleanField(theField) {
            var theValue = theField.value;
            theValue = stripCharsNotInBag(theValue, digits + lowercaseLetters + uppercaseLetters + whitespace);
            theField.value = theValue;
        }
        var CoOwnerLastNamePre;
        function FlipFlopBox() {
            try {


                var CoSignFName = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerFirstName.ClientID %>').value);
                var CoSignLName = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerLastName.ClientID %>').value);

                if (CoSignFName.length > 0) {

                    if (document.getElementById('<%= pnlCounseling.ClientID%>') != null) {
                        if (document.getElementById('<%= pnlCounseling.ClientID%>').style.display == "") {

                            document.getElementById('IsPresentrow').style.display = 'block';
                            document.getElementById('ShowCoCouns').innerHTML = document.getElementById('ShowCoCouns').innerHTML.replace("#coowner", CoSignFName); //"<span class='NormTxt'>Is " + CoSign + " present now?:</span>"
                            document.getElementById('ShowCoCouns').innerHTML = document.getElementById('ShowCoCouns').innerHTML.replace(CoOwnerLastNamePre, CoSignFName); //"<span class='NormTxt'>Is " + CoSign + " present now?:</span>"
                            CoOwnerLastNamePre = CoSignFName;
                        }
                    }

                    document.getElementById('CoOwnerDob').style.display = '';
                    document.getElementById('OwnerLastName').style.display = '';
                    document.getElementById('CoOwnerMStatus').style.display = '';
                    document.getElementById('CoOwnerSSN').style.display = '';

                    document.getElementById('CoOwnerEmp').style.display = '';
                }
                else {
                    document.getElementById('IsPresentrow').style.display = 'none';
                    document.getElementById('OwnerLastName').style.display = 'none'; 
                    document.getElementById('CoOwnerDob').style.display = 'none';
                    document.getElementById('CoOwnerLName').style.display = 'none';
                    document.getElementById('CoOwnerMStatus').style.display = 'none';

                    document.getElementById('CoOwnerSSN').style.display = 'none';

                    document.getElementById('CoOwnerEmp').style.display = 'none';
                }
            }
            catch (e) {
            }
        }

        function VerifyClientName() {
            var hdnPrifirstname = stripInitialWhitespace(document.getElementById('<%=hdnFirstName.ClientID %>').value);
            var hdnPrilastname = stripInitialWhitespace(document.getElementById('<%=hdnLastName.ClientID %>').value);
            var hdnSecfirstname = stripInitialWhitespace(document.getElementById('<%=hdnCoOwnerFirstName.ClientID %>').value);
            var hdnSeclastname = stripInitialWhitespace(document.getElementById('<%=hdnCoOwnerLastName.ClientID %>').value);

            var Prifirstname = stripInitialWhitespace(document.getElementById('<%=txtFirstName.ClientID %>').value);
            var Priinitialname = stripInitialWhitespace(document.getElementById('<%=txtInitial.ClientID %>').value);
            var Prilastname = stripInitialWhitespace(document.getElementById('<%=txtLastName.ClientID %>').value);
            var Secfirstname = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerFirstName.ClientID %>').value);
            var Secinitialname = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerInitial.ClientID %>').value);
            var Seclastname = stripInitialWhitespace(document.getElementById('<%=txtCoOwnerLastName.ClientID %>').value);
           var hdPUserName = stripInitialWhitespace(document.getElementById('<%=hdTextusername.ClientID %>').value); 

            var strMsg = (Secfirstname == "" && Seclastname == "") ? "Your complete name as it will be listed on your certificate is " + Prifirstname + " " + Priinitialname + " " + Prilastname + ", is this correct?" : "Your complete name as it will be listed on your certificate is " + Prifirstname + " " + Priinitialname + " " + Prilastname + " and " + Secfirstname + " " + Secinitialname + " " + Seclastname + ", is this correct?";
            var confirmation = confirm(strMsg);

            if (confirmation) {
                return true;
            }
            else {
                return false;
            }
        }


        function CheckSsn(sender, args) {

            if (args.Value.length < 11 || args.Value.length == 0) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }

        function CheckprimaryPhone(sender, args) {

            if (args.Value.length > 14 || args.Value.length < 14 || args.Value.length == 10) {
                args.IsValid = false;
            }
            else {
                args.IsValid = true;
            }
        }
        function AskSave() {
            if (confirm("The changes you have made will be lost, do you want to continue?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <h1>
    <%=PageHeading %>
      </h1>
    <p>
        &nbsp;</p>
    <div class="dvbtnEditAttorneyContainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="EditAttorney" runat="server" Visible="false" OnClick="btnEditAttorney_Click"
                CausesValidation="false" OnClientClick="return AskSave();"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKCButtons|EditAttorney")%></span></asp:LinkButton>
        </div>
    </div>
    <label runat="server" id="lblIntro">
    </label>
    <div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false">
    </div>
    <div class="error" id="dvTopErrorMessage" runat="server">
    </div>
    <div class="vspace15">
        &nbsp;</div>
    <div class="dvform2col">
        <div class="dvhdcont">
            <div class="dvhdlft">
                <span id="spnRVMPri" runat="server" visible="true"><b>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Owner")%></b></span>
                <span id="spnPRPPri" runat="server" visible="false"><b>
                    <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|OW")%></b></span>
            </div>
            <div class="dvhdrht">
                <span id="spnRVMSec" runat="server" visible="true"><b>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Co-OwnerRequired")%></b></span>
                <span id="spnPRPSec" runat="server" visible="false"><b>
                    <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|COO")%></b></span>
                <br />
            </div>
            <div class="clearboth">
            </div>
        </div>
        <div class="clearboth">
        </div>
        <div class="colformlft">
            <div class="dvform">
                <div class="dvrow">
                    <label>
                        &nbsp;</label>
                </div>
                <div class="dvrow">
                    <label for="<%=txtFirstName.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|FirstName")%>:</label>
                    <asp:TextBox ID="txtFirstName" CssClass="txtBox" runat="server"></asp:TextBox><span
                        class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvFname" runat="server" ControlToValidate="txtFirstName"
                        EnableClientScript="true" Text="" ErrorMessage="First name Required."></asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hdnFirstName" runat="server" Value="" />
                </div>
                <div class="dvrow">
                    <label for="<%=txtInitial.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Initial")%>:</label>
                    <asp:TextBox ID="txtInitial" runat="server" CssClass="txtBox" MaxLength="4"></asp:TextBox>
                </div>
                <div class="dvrow">
                    <label for="<%=txtLastName.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|LastName")%>:</label>
                    <asp:TextBox ID="txtLastName" CssClass="txtBox" runat="server"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                        ErrorMessage="Last name required." Text=""></asp:RequiredFieldValidator>
                    <asp:HiddenField ID="hdnLastName" runat="server" Value="" />
                </div>
                <div class="dvrow">
                    <label for="<%=txtSSN.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%>:</label>
                    <asp:TextBox ID="txtSSN" CssClass="txtBox" runat="server" onBlur="ReSetSSN(this);"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvSSN" runat="server" ControlToValidate="txtSSN"
                        ErrorMessage="Your SSN is Required." Text="" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtSSN" runat="server"
                        ControlToValidate="txtSSN" Display="Dynamic" ValidationExpression="(\d{3}-\d{2}-\d{4})|(\d{3}\d{2}\d{4})"
                        ErrorMessage="Please enter a valid SSN." Text=""></asp:RegularExpressionValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=txtDOB.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Dob")%>:</label>
                    <asp:TextBox ID="txtDOB" runat="server" MaxLength="20" CssClass="txtBox"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    
                    <label>&nbsp;</label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%>

                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvDOB" runat="server" ControlToValidate="txtDOB"
                        Display="Dynamic" EnableClientScript="true" ErrorMessage="DOB is Required." Text=""></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="revtxtDOB" ControlToValidate="txtDOB"
                        ValidationExpression="^(([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$)|(([1-9]|0[1-9]|1[012])[-]([1-9]|0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$)"
                        ErrorMessage="Date is not Acceptable" Text="" Display="Dynamic" />
                   
                </div>
                <div class="dvrow">
                    <label for="<%=ddlMaritalStatus.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|MaritalStatus")%>:</label>
                    <asp:DropDownList ID="ddlMaritalStatus" runat="server">
                    </asp:DropDownList>
                    <span style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator style="margin-top:10px;" class="validationMessage" ID="rfvMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus"
                        EnableClientScript="true" SetFocusOnError="true" ErrorMessage="Your Marital Status Required."
                        Text="" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlSex.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Sex")%>:</label>
                    <asp:DropDownList ID="ddlSex" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlRace.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Race")%>:</label>
                    <asp:DropDownList ID="ddlRace" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlHispanic.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Hispanic")%>?:</label>
                    <asp:DropDownList ID="ddlHispanic" runat="server">
                    </asp:DropDownList>
                    &nbsp; <a class="Link" href="#" style="vertical-align: middle" onclick="javascript: return false;"
                        title='<%= Cccs.Credability.Website.App.Translate("WhyLinkTitle")%>'>
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|W")%></a>
                </div>
                <asp:Panel ID="pnlPriEmpEdu" runat="server" Visible="false">
                    <div class="dvrow">
                        <div>
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Employment")%></label>
                            <asp:DropDownList ID="ddlPriEmp" runat="server">
                            </asp:DropDownList>
                            <span style="color: Red;" class="requiredField">&nbsp;*</span>

                        </div><br />
                        <div>
                            <asp:RequiredFieldValidator class="validationMessage" ID="rfvEmployment" runat="server" Display="Dynamic" EnableClientScript="true"
                            ControlToValidate="ddlPriEmp" Text="" ErrorMessage="Employment Is required."></asp:RequiredFieldValidator>
                        </div>

                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Education")%></label>
                        <asp:DropDownList ID="ddlPriEdu" runat="server">
                        </asp:DropDownList>
                        <span style="color: Red;" class="requiredField">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvEducation" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="ddlPriEdu" Text=""
                            ErrorMessage="Education Is Required."></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlMilService" runat="server" Visible="false">
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileREC|MilStatus")%></label>
                        <asp:DropDownList ID="ddlMilStatus" runat="server">
                        </asp:DropDownList>
                        <span style="color: Red;" class="requiredField">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvMilStatus" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="ddlMilStatus" Text=""
                            ErrorMessage="Military Status Is Required."></asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileREC|Grade")%></label>
                        <asp:TextBox ID="txtGrade" runat="server" CssClass="txtBox" MaxLength="20"></asp:TextBox>
                        <span style="color: Red;" class="requiredField">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvGrade" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtGrade" Text=""
                            ErrorMessage="Grade Is Required."></asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileREC|BrnchSvc")%></label>
                        <asp:DropDownList ID="ddlBranchOfService" runat="server">
                        </asp:DropDownList>
                        <span style="color: Red;" class="requiredField">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvBranchOfService" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="ddlBranchOfService" Text=""
                            ErrorMessage="Branch Of Service Is Required."></asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileREC|Rank")%></label>
                        <asp:TextBox ID="txtRank" runat="server" CssClass="txtBox" MaxLength="20"></asp:TextBox>
                        <span style="color: Red;" class="requiredField">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvRank" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtRank" Text=""
                            ErrorMessage="Rank Is Required."></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div class="dvrow">
                    <label for="<%=txtAddress.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress")%>:</label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="txtBox"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvAddress" runat="server" ControlToValidate="txtAddress"
                        Display="Dynamic" EnableClientScript="true" Text="" ErrorMessage="Street Address Is Required."></asp:RequiredFieldValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=txtAddress2.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress1")%>:</label>
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="txtBox"></asp:TextBox>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlSex.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|City")%>:</label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="txtBox" OnBlur="FormatAsText(this)"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvcity" runat="server" ControlToValidate="txtCity"
                        Display="Dynamic" Text="" OnBlur="FormatAsText(this)" ErrorMessage="City required."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCity"
                        ErrorMessage="City must be in text format." ValidationExpression="[^1234567890]+">!</asp:RegularExpressionValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlState.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|State")%>:</label>
                    <asp:DropDownList ID="ddlState" runat="server">
                    </asp:DropDownList>
                    <span style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvState" runat="server" ControlToValidate="ddlState"
                        Display="Dynamic" EnableClientScript="true" Text="" ErrorMessage="Please Select State."></asp:RequiredFieldValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=txtZipCode.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|ZipCode")%>:</label>
                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="txtBox"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvZipCode" runat="server" ControlToValidate="txtZipCode"
                        EnableClientScript="true" Text="" ErrorMessage="Zip Code is required." Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" ControlToValidate="txtZipCode"
                        ValidationExpression="^[0-9]{5}$" ErrorMessage="Enter valid Zip Code." Display="Dynamic"
                        Text="" />
                    <span id="divZipError" runat="server" class="error"></span>
                </div>
                <div class="dvrow">
                    <label for="<%=txtPphone.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|PrimaryPhone")%>:</label>
                    <asp:TextBox ID="txtPphone" runat="server" onBlur="ReSetPhone(this)" CssClass="txtBox"></asp:TextBox><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvPphone" runat="server" ControlToValidate="txtPphone"
                        EnableClientScript="true" SetFocusOnError="true" Text="" ErrorMessage="Your Primary Phone Number Is Required."
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator_txtPphone" runat="server"
                        ControlToValidate="txtPphone" Display="Dynamic" ErrorMessage="Please enter a valid Phone #."
                        ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}" Text=""></asp:RegularExpressionValidator>
                </div>
                <div class="dvrow">
                    <label for="<%=txtSphone.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|SecondaryPhone")%>:</label>
                    <asp:TextBox ID="txtSphone" CssClass="txtBox" runat="server" onBlur="ReSetPhone(this)"></asp:TextBox>
                    <asp:RegularExpressionValidator class="validationMessage doNotHighlight" ID="RegularExpressionValidator_txtSphone" runat="server"
                        ControlToValidate="txtSphone" ErrorMessage="Please enter a valid Phone #." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"
                        Display="Dynamic" Text=""></asp:RegularExpressionValidator>
                </div>
                <asp:Panel ID="pnlEmail" runat="server" Visible="false">
                    <div class="dvrow">
                        <label for="<%=txtemailaddress.ClientID %>">
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|EmailAddress")%>:</label>
                        <asp:TextBox ID="txtemailaddress" CssClass="txtBox" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator class="validationMessage" ID="cfvEmailAddress" runat="server" ControlToValidate="txtemailaddress"
                            Display="Dynamic" ErrorMessage="Enter valid Email Address." Text="" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,3}|[0-9]{1,3})(\]?)$">
                        </asp:RegularExpressionValidator>
                    </div>
                </asp:Panel>
                <span id="spnPrePurchase" runat="server" visible="false">
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdNumThat")%></label>
                        <asp:TextBox ID="txtHouseholdSize" runat="server" MaxLength="2">
                        </asp:TextBox>
                        <span style="color: Red;" class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rftxtHouseholdSize" runat="server" ControlToValidate="txtHouseholdSize"
                            CssClass="error" Display="Dynamic" EnableClientScript="true" SetFocusOnError="true"
                            Text="">
                        </asp:RequiredFieldValidator>
                        <asp:RangeValidator class="validationMessage" ID="rvtxtHouseholdSize" runat="server" MinimumValue="1" Type="Integer"
                            MaximumValue="100" ControlToValidate="txtHouseholdSize" Text="" Display="Dynamic"
                            CssClass="error">
                        </asp:RangeValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|HWYRTC")%></label>
                        <asp:DropDownList ID="ddlRefSorce" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRefSorce_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="pnlLendingInstitution" runat="server" Visible="false" valign="top">
                        <div class="colformrht">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|PETNOTLI")%></label><br />
                        </div>
                        <div class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|NOLI")%></label>
                            <asp:TextBox ID="txtInstName" MaxLength="20" runat="server"></asp:TextBox>
                        </div>
                        <div class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|CN")%></label>
                            <asp:TextBox ID="txtInstCont" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
                        <div class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|CE")%></label>
                            <asp:TextBox ID="txtInstEmail" MaxLength="100" runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </span>
                <asp:Panel ID="pnlCounseling" runat="server" Visible="false">
                    <div class="dvrow" style="display: none;" id="IsPresentrow">
                        <label class="ispresent" id="ShowCoCouns">
                            <br />
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|IsPresent")%>
                        </label>
                        <div id="ShowCoCouns0">
                            <span style="color: Red; font-size: large">&nbsp;</span></div>
                        <table class="style1">
                            <tr>
                                <td class="style2" valign="top">
                                    <asp:RadioButtonList ID="rblIsPresent" runat="server" CssClass="Dvradiolist" RepeatDirection="Horizontal">
                                    </asp:RadioButtonList>
                                </td>
                                <td valign="top">
                                    <span class="requiredField">*</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|HYER")%></label>
                        
                        <div id="ShowCoCouns1">
                            <span style="color: Red; font-size: large">&nbsp;</span>
                        </div>
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblHadConsYesorNO" runat="server" CssClass="Dvradiolist"
                                        RepeatDirection="Horizontal">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red;" class="requiredField">*</span>
                                 </td>
                            </tr>
                        </table>

                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvCreditCounseling" runat="server" ControlToValidate="rblHadConsYesorNO"
                                        Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text=""></asp:RequiredFieldValidator>
                                
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|HYEBODmp")%>
                        </label>
                        
                        
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblHist2DMPYesorNO" runat="server" CssClass="Dvradiolist"
                                        RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                                <td>                                    
                                    <span style="color: Red;" class="requiredField">*</span>
                                </td>
                            </tr>
                        </table>
                        
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvDMP" runat="server" ControlToValidate="rblHist2DMPYesorNO" 
                                        Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text=""></asp:RequiredFieldValidator>
                        

                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|HYRBDemp")%>
                        </label>
                        <table>
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblHistDMPYesorNO" runat="server" CssClass="Dvradiolist"
                                        RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red;" class="requiredField">*</span>
                                </td>
                            </tr>
                        </table>
                        
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvRecentlyDMP" runat="server" ControlToValidate="rblHistDMPYesorNO" 
                            EnableClientScript="true" Display="Dynamic" SetFocusOnError="true" Text=""></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="colformrht">
            <div class="dvform">
                <label>
                    &nbsp;
                </label>
                <div class="dvrow">
                    <label for="<%=txtCoOwnerFirstName.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|FirstName")%>:</label>
                    <asp:TextBox CssClass="txtBox" ID="txtCoOwnerFirstName" runat="server" onblur="FlipFlopBox();"></asp:TextBox>
                    <asp:HiddenField ID="hdnCoOwnerFirstName" runat="server" Value="" />
                </div>
                <div class="dvrow">
                    <label for="<%=txtCoOwnerInitial.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Initial")%>:</label>
                    <asp:TextBox CssClass="txtBox" ID="txtCoOwnerInitial" runat="server" MaxLength="4"></asp:TextBox>
                </div>
                <div class="dvrow">
                    <label for="<%=txtCoOwnerLastName.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|LastName")%>:</label>
                    <asp:TextBox CssClass="txtBox" ID="txtCoOwnerLastName" runat="server" onblur="FlipFlopBox();"></asp:TextBox>
                   <asp:HiddenField ID="hdnCoOwnerLastName" runat="server" Value="" />
                </div>
                <div class="dvrow">
                    <label for="<%=txtCoOwnerSSN.ClientID %>" oninit="FlipFlopBox();">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%>:</label>
                    <asp:TextBox CssClass="txtBox" ID="txtCoOwnerSSN" runat="server" onBlur="ReSetSSN(this);"></asp:TextBox>
                    <asp:RegularExpressionValidator class="validationMessage doNotHighlight" ID="RegularExpressionValidator_txtCoOwnerSSN" runat="server"
                        ControlToValidate="txtCoOwnerSSN" Display="Dynamic" Text="" ErrorMessage="Please enter a valid SSN."
                        ValidationExpression="(\d{3}-\d{2}-\d{4})|(\d{3}\d{2}\d{4})">!</asp:RegularExpressionValidator>
                </div>
                <div class="dvrow">
                    
                    <label for="<%=txtCoOwnerDOB.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Dob")%>:</label>
                    <asp:TextBox CssClass="txtBox" ID="txtCoOwnerDOB" runat="server"></asp:TextBox>

                    <label>&nbsp;</label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%>

                    <asp:RegularExpressionValidator class="validationMessage doNotHighlight" runat="server" ID="revtxtCoOwnerDOB" ControlToValidate="txtCoOwnerDOB"
                        ValidationExpression="^(([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$)|(([1-9]|0[1-9]|1[012])[-]([1-9]|0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$)"
                        ErrorMessage="Co-Owner's DOB must be in the format: MM/DD/YYYY." Display="Dynamic"
                        Text="" />
                    
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerMaritalStatus.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|MaritalStatus")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerMaritalStatus" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerSex.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Sex")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerSex" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoOwnerRace.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Race")%>:</label>
                    <asp:DropDownList ID="ddlCoOwnerRace" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="dvrow">
                    <label for="<%=ddlCoHispanic.ClientID %>">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Hispanic")%>?:</label>
                    <asp:DropDownList ID="ddlCoHispanic" runat="server">
                    </asp:DropDownList>
                </div>
                <asp:Panel ID="pnlCoEmp" runat="server" Visible="false">
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|Employment")%></label>
                        <asp:DropDownList ID="ddlCoEmp" runat="server">
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </div>
    <span id="spnBottomAuthorizationRow" runat="server" visible="true">
        <p>
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|BottomAuthorizationRow")%></p>
    </span><span id="spnPRPBottomAuthorizationRow" runat="server" visible="false">
        <p>
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|PRPBottomAuthorizationRow")%></p>
    </span>
    <div class="dvform2col">
        <div class="colformlft">
            <div class="dvform">
                <div class="dvrow">
                    <label>&nbsp;</label>
                    <span id="spnRVMPriInfo" runat="server" visible="true">
                        <p class="col_title" style="white-space: nowrap">
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Owner")%></p>
                    </span><span id="spnPRPPriInfo" runat="server" visible="false">
                        <p class="col_title" style="white-space: nowrap">
                            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|OW")%></p>
                    </span>
                </div>
                <div class="dvrow">
                    <label>&nbsp;</label>
                    <asp:TextBox runat="server" CssClass="txtBox" MaxLength="50" class="TxtBox" ID="txtOwnerbirthOfCity" /><span
                        style="color: Red;" class="requiredField">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" SetFocusOnError="true" ID="rfvOwner" runat="server" Display="Dynamic"
                        ControlToValidate="txtOwnerbirthOfCity" Text="" ErrorMessage="Owner's city of birth is required."></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="colformrht">
            <div class="dvform">
                <div class="dvrow">
                    <label>&nbsp;</label>
                    <span id="spnRVMSecInfo" runat="server" visible="true">
                        <p class="col_title" style="white-space: nowrap">
                            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|CoOwner")%></p>
                    </span><span id="spnPRPSecInfo" runat="server" visible="false">
                        <p class="col_title" style="white-space: nowrap">
                            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|COOWN")%></p>
                    </span>
                </div>
                <div class="dvrow">
                    <label>&nbsp;</label>
                    <asp:TextBox CssClass="txtBox" runat="server" MaxLength="50" class="TxtBox" ID="txtCoOwnersbirthOfCity" />
                </div>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </div>
        <span id="spnbchShareinfo" runat="server" visible="false">
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|PContent")%></span><p />
         <div class="dvform2col" id="dvBchHoShareInfo" runat="server" visible="false">
            <div class="colformlft">
                <div class="dvform">
                    <div class="dvrow">
                        <asp:RadioButtonList ID="rblShareDataYesorNO" runat="server" RepeatDirection="Horizontal"
                            CssClass="dvradiolistAgree" RepeatLayout="Table" BorderWidth="0">
                        </asp:RadioButtonList>
                      <asp:RequiredFieldValidator class="validationMessage2" ID="rfvShare" runat="server" EnableClientScript="true"
                            Display="Dynamic" ControlToValidate="rblShareDataYesorNO" Text=""></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="clearboth">
            </div>
        </div>
        <div class="clearboth">
        </div>
        <span id="spnBchRow" runat="server" visible="false">
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|PContentUl")%></span>
        <span id="spnPRPRow" runat="server" visible="false">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchasePersonal|AYIACTP")%></span>
        <span id="spnRvmRow" runat="server" visible="false">
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|BottomAuthorizationRow1")%></span>&nbsp;&nbsp;<br />
        <span id="spnBKCRow" runat="server" visible="false">
            <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|BKCBottomAuthorizationRow1")%><br />
            <asp:CheckBox ID="chkCounselingComplete" runat="server" />&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|BCT")%><br /><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|BKCBottomAuthorizationRow2")%><br /></span>
        <asp:Panel runat="server" ID="pnlAuthorizeShareParticipate">
        <div class="dvform2col dvform2colchk">
            <div class="colformlft">
                <div class="dvform">
                    <div class="dvrow" style="padding: 0 0 0 150px">
                        <asp:CheckBox ID="chkConfirmation" runat="server" />
                    </div>
                </div>
            </div>
            <div class="colformrht">
                <div class="dvform">
                    <div class="dvrow" style="padding: 0 0 0 150px">
                        <asp:CheckBox ID="chkCoOwnerConfirmation" runat="server" />
                    </div>
                </div>
            </div>
            <div class="dvrow">
             </div>
           </div>
         </asp:Panel>

   <div id="AffirmID_Form" runat="server">
      <div class="DvErrorSummary" id="AffirmError" runat="server" visible="false">
      </div>
      <div>
         <b><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|AffrimID_PETFI")%></b><br/>
      </div>
      <%= Cccs.Credability.Website.App.Translate("Sandbox|Default|Username")%>
      <asp:TextBox CssClass="txtBox" ID="textUsername" runat="server"></asp:TextBox> <span
            style="color: Red;" class="requiredField">&nbsp;*</span>
         <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator_UserName" runat="server" ControlToValidate="textUsername"
            Display="Dynamic" EnableClientScript="true" Text="" ErrorMessage="User Name Is Required."></asp:RequiredFieldValidator>
         <asp:HiddenField ID="hdTextusername" runat="server" Value="" />

      <div style="white-space: nowrap">
         <br /><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|AffirmID_SQ")%>
         <span id="textSecurityQuestion" runat="server" > </span>
      </div>

      <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|AffirmID_SA")%>
      <asp:TextBox CssClass="txtBox" ID="textSecurityAns" runat="server" ></asp:TextBox>
      <span style="color: Red;" class="requiredField">&nbsp;*</span>
      <br/>
   </div>
    <div class="dvform2col"></div>
    <div class="DvErrorSummary" id="dvBottomServerSideErrorMessage" runat="server" visible="false">
    </div>
    <div class="dvbtncontainer">
      <div class="lnkbutton">
         <asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnSaveExit_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
      </div>
      <div class="lnkbutton">
           <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
      </div>
           <%--<asp:LinkButton CssClass="" ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click"><span><%= Cccs.Credability.Website.App.Translate("SaveExit")%></span></asp:LinkButton>--%>
    </div>
</asp:Panel>

<%--<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturnToPreviousPage" runat="server" CssClass="previous" OnClick="click_btnReturnToPreviousPage"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="click_btnSaveExit"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="click_btnContinue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>--%>
