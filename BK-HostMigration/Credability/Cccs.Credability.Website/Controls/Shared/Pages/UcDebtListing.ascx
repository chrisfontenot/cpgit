﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDebtListing.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcDebtListing" %>
<script type="text/javascript" src="../Content/FormChek.js"></script>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
	var theCreditorCount, CredRptCnt = 10;
	function clearDeleted(form)
	{
		var aCheckBoxes = document.getElementsByName("id");
		for (var x = 0; x < aCheckBoxes.length; x++) //>
		{
			if (aCheckBoxes[x].checked)
			{
				if (isEmpty(form.elements["creditor_" + x].value))
				{
					form.elements["name_" + x].value = "";
					form.elements["bal_" + x].value = "";
					form.elements["intrate_" + x].value = "";
					form.elements["payments_" + x].value = "";
					form.elements["pre_acctnbr_" + x].value = "";
					form.elements["jointacct_" + x].value = "";
					form.elements["pastdue_" + x].value = "";
					form.elements["acct_" + x].value = "";
					form.elements["cred_rpt_" + x].value = "";
				}
			}
		}
	}

	function IsGeaterThanZero(Value)
	{
		Value = stripCharsNotInBag(Value, "0123456789.");
		if (Value == "")
		{
			return false
		}
		if (Number(Value) > 0)
		{
			return true;
		}
		return false;
	}

	function validateForm(form)
	{
		var SndMsg = true, Back = true;
		if (CredRptCnt > 0)
		{
			SndMsg = false;
		}
		for (var x = 0; (x < theCreditorCount) && SndMsg; x++)
		{
			if (!isWhitespace(form.elements["name_" + x].value))
			{
				if (!isWhitespace(form.elements["bal_" + x].value))
				{
					SndMsg = false;
				}
			}
		}
		if (SndMsg)
			Back = confirm('<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|Title")%>');
		return Back;
	}

	function validateDebtListing()
	{
		var Cells = document.getElementById('<%=UserDebtListingGrid.ClientID%>').getElementsByTagName('tr');
		var Flag = true;

		for (var p = 1; p < Cells.length; p++)
		{
			var cell = Cells[p];
			var _elements = cell.getElementsByTagName('input');

			var chk = _elements[0];
			var txt = _elements[1];
            
			if (txt.value != '')
			{
				document.getElementById('<%=hdnItemsCount.ClientID%>').value = 'Y'
			}
		}
		if (document.getElementById('<%=hdnItemsCount.ClientID%>').value == 'N' && document.getElementById('<%=hdnSiteCode.ClientID%>').value == 'DMP')
		{
			alert('<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|PleaseEnter")%>');
			Flag = false;
		}
		else if (document.getElementById('<%=hdnItemsCount.ClientID%>').value == 'N')
		{
			Flag = confirm('<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|PleaseConfirm")%>');
		}
		return Flag;
	}
</script>
<h1>
	<span id="PageHeadGenral" runat="server" visible="false">
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|Title")%>
	</span>
	<span id="PageHeadDmp" runat="server" visible="false">
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|StepTitle")%>
	</span>
</h1>
<span id="TopTextGenral" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebtsRVM|GACA")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|EIYS")%></p>
</span>
<span id="TopTextPrp" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|PRPDEBTLISTING|GACA")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|PRPDEBTLISTING|EIYS")%></p>
</span>
<span id="TopTextDmp" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DMPListingYourDebts|GACA")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|EIYS")%></p>
</span>
<div id="TopContent">
	<p>
		<% if(CheckWebSite)
	 { %><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NrOrCl")%><%}
	 else
	 { %><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NrOrCl1")%><%} %><asp:Label></asp:Label>
	</p>
	<input type="hidden" id="txtOKNoVals" name="txtOKNoVals" value="N" />
	<input id="txtChecked" name="txtChecked" type="hidden" value="N" />
</div>
<div id="dvMessage" runat="server" style="text-align: center; margin-bottom: 10px;">
	<asp:Label ID="lblError" runat="server" Style="color: Red; font-weight: bold;"></asp:Label>
</div>
<div id="dvContent">
	<asp:HiddenField ID="hdnItemsCount" runat="server" Value="N" />
	<asp:HiddenField ID="hdnSiteCode" runat="server" Value="" />
	<asp:GridView ID="UserDebtListingGrid" runat="server" ShowFooter="false" AutoGenerateColumns="false" BorderWidth="1" AllowPaging="false" CssClass="TblUserdebtListing" GridLines="None" AlternatingRowStyle-CssClass="altrw" OnRowDataBound="UserDebtListingGrid_RowDataBound">
		<Columns>
			<asp:TemplateField ItemStyle-CssClass="col1">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|SelectforDelete")%>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						&nbsp;</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:Label ID="lblid" runat="server" Visible="false" Text='<%# Bind("CreditorId")%>'></asp:Label>
					<asp:CheckBox ID="chkId" runat="server" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col2">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|CreditorsName")%>
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|EXAMPLE")%></div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:TextBox ID="txtCreditorName" runat="server" Text='<%# Bind("CreditorName") %>' MaxLength="60" CssClass="txtbx"></asp:TextBox><br />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col3">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Balance")%><br />
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|UseCurrentBalance")%><br />
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						$4,356</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:TextBox ID="txtBalance" runat="server" Text='<%# Bind("CreditorBal") %>' MaxLength="20" onBlur="ReFormatCurrency(this)" onFocus="SetUpField(this);" CssClass="txtbx"></asp:TextBox><br />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col4">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|IntrestRate")%>
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						12.34%</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:TextBox ID="txtInterest" runat="server" Text='<%# Bind("CreditorIntrate") %>' MaxLength="20" onBlur="ReFormatPercent(this);" onFocus="SetUpField(this);" CssClass="txtbx"></asp:TextBox><br />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col5">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|MonthlyPayment")%>
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						$78</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:TextBox ID="txtmPayment" runat="server" Text='<%# Bind("CreditorPayments") %>' MaxLength="20" onBlur="ReFormatCurrency(this)" onFocus="SetUpField(this);" CssClass="txtbx"></asp:TextBox><br />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col6">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|AccountNumber")%>
					<div>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|FullAccountNumber")%>
						<asp:Label ID="lblAccNumWhy" runat="server" Text=""></asp:Label>
					</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:TextBox ID="txtAccountNumber" runat="server" Text='<%# Bind("PreAcntNoEncs") %>' MaxLength="20" CssClass="txtbx"></asp:TextBox><br />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col7">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|JointAct")%><br />
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|No")%>
						<br />
						<asp:Label ID="lblJointAccWhy" runat="server" Text=""></asp:Label>
					</div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:DropDownList ID="ddlJiontAccountNumber" runat="server">
					</asp:DropDownList>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-CssClass="col8">
				<HeaderTemplate>
					<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|DebtPastDue")%><br />
					<span>
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
					<div>
						&nbsp;<br />
						&nbsp;<br />
						<%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Current")%></div>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:DropDownList ID="ddlDebtPastDue" runat="server">
					</asp:DropDownList>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnADDMoreCred" runat="server" OnClick="AddMoreRow_Click" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|AddMoreCreditors")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnDelete" runat="server" OnClick="DeleteRow_Click" ToolTip="Delete Selected"><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|DeleteSelected")%></span></asp:LinkButton>
	</div>
</div>
<div class="clearboth">
</div>
<p>
	<asp:TemplateField HeaderText="<center>Creditor Name</center><center>(or collection agency)</center><center>Required</center><BR><BR>-------------------------------------<BR>EXAMPLE"></p>
<span id="BottomTextGenral" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NOTE")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|Comments")%></p>
</span>
<span id="BottomTextPrp" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebtsPRP|NOTE")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebtsPRP|Comments")%></p>
</span>
<div style="margin-bottom: 20px;">
	<asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="100%" Rows="5" MaxLength="200"></asp:TextBox>
</div>
<asp:Panel runat="server" Visible="false" ID="pnlDMP">
	<div>
		<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|DmpDisclaimer")%>
	</div>
	<div>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|DmpStart")%>
			<%= CalculateDMPDate %>.
			<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|DmpStart2")%>
	</div>
	<div>
		<label for="<%=txtDMPDate.ClientID %>">
			<%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|DmpDate")%>
		</label>
		<asp:TextBox runat="server" ID="txtDMPDate"></asp:TextBox>
		<asp:CompareValidator class="validationMessage" ID="cmpTxtDate" runat="server" Type="Date" Display="None" Operator="DataTypeCheck" ControlToValidate="txtDMPDate" />
	</div>
</asp:Panel>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous" OnClick="btnPrevious_Click" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="LinkButton1" runat="server" OnClick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClientClick="return validateDebtListing();" OnClick="btnContinue_Click" CausesValidation="true"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
	<asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
		<asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
			<ProgressTemplate>
				<div style="position: relative; top: 30%; text-align: center;">
					<img src="../images/loading.gif" style="vertical-align: middle" alt="Processing" />
					<%= Cccs.Credability.Website.App.Translate("Processing")%>
					...
				</div>
			</ProgressTemplate>
		</asp:UpdateProgress>
	</asp:Panel>
	<ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
</div>
<script type="text/javascript" language="javascript">
	var ModalProgress = '<%= ModalProgress.ClientID %>';
	Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

	function beginReq(sender, args)
	{
		// shows the Popup     
		$find(ModalProgress).show();
	}

	function endReq(sender, args)
	{
		//  shows the Popup
		$find(ModalProgress).hide();
	}
</script>