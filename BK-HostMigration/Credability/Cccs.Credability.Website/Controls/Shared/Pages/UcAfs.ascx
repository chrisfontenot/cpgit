﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAfs.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcAfs" %>


<script type="text/javascript" src="/Content/ErrorHandling.js"></script>

<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
</div>
<span id="BCHTopText" runat="server" visible="true">
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|AFS")%></h1>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|TYFC")%></p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|BWCC")%>
</span>
<span id="PRPTopText" runat="server" visible="false">
	<h1>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsPRP|AFS")%></h1>
	<p class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseAfs|TYFCC")%>
	</p>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseAfs|BWB")%>
	<p class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseAfs|TAFS")%>
	</p>
</span>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|FAPV")%>
		<a href="Term.aspx" class="NormLink">
			<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|CLH")%></a>
	</p>
<textarea readonly class="TxtBox" cols="150" rows="15">
<%=Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|S1964")%>
</textarea>
<div class="dvform2col">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|BEYM")%>
	</p>

    <div style="display:none;">
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="BchAfs" />
	<asp:Label ID="lblSecMsg" runat="server" Visible="False" Font-Bold="True" ForeColor="#CC3300"></asp:Label>
    </div>

	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow" style="padding: 0 0 0 150px">
				<span id="BCHPriPerson" runat="server" visible="true">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|PP")%></p>
				</span>
				<span id="PRPPriPerson" runat="server" visible="false">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseAfs|OOPCO")%></p>
				</span>
			</div>
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<asp:TextBox ID="txtPrimaryPerson" runat="server" MaxLength="50"></asp:TextBox>
				<asp:RequiredFieldValidator class="validationMessage" ID="rfvPrimaryPerson" runat="server" EnableClientScript="true" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtPrimaryPerson" Text="" ValidationGroup="BchAfs"></asp:RequiredFieldValidator>
			</div>
		</div>
	</div>
	<div class="colformrht">
		<div class="dvform">
			<div class="dvrow" style="padding: 0 0 0 150px">
				<span id="BCHSecPerson" runat="server" visible="true" class="col_title">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|AfsDmpOnlyDMP|SP")%></p>
				</span>
				<span id="PRPSecPerson" runat="server" visible="false" class="col_title">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseAfs|CO")%></p>
				</span>
			</div>
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<asp:TextBox ID="txtSecondPerson" runat="server" MaxLength="50"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<div class="DvErrorSummary" id="dvErrorSumary" runat="server" visible="false">
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnAfsContinue" runat="server" OnClick="btnAfsContinue_Click" ToolTip="Continue" ValidationGroup="BchAfs"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
