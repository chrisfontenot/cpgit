﻿using System;
using Cccs.Web;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcAfs : CounselingUcBase
	{
		//		return !string.IsNullOrEmpty(url) ? url : "Userprofile.aspx";
		public string RedirectOnNeedAuthorization
		{
			get
			{
				string url = ViewState.ValueGet<string>("RedirectOnNeedAuthorization");

				return !string.IsNullOrEmpty(url) ? url : "DescribeYourSituation.aspx";
			}

			set
			{
				ViewState.ValueSet("RedirectOnNeedAuthorization", value);
			}
		}

		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				InitializeTranslation();
				btnAfsContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				lblSecMsg.Text = Cccs.Credability.Website.App.Translate("Credability|AFS|WSMR");
				SessionState.UserPageArrivalTime = DateTime.Now;
				AFS Afs = null;
				Afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
				if(Afs != null)
				{
					if(Afs.IsSecondaryPersonNeeded && Afs.SecondaryPerson == String.Empty)
						lblSecMsg.Visible = true;
					else
						lblSecMsg.Visible = false;

					txtPrimaryPerson.Text = (Afs.PrimaryPerson != null) ? Afs.PrimaryPerson.Trim() : string.Empty;
					txtSecondPerson.Text = (Afs.SecondaryPerson != null) ? Afs.SecondaryPerson.Trim() : string.Empty;
					//if (!string.IsNullOrEmpty(Afs.PrimaryPerson) && string.IsNullOrEmpty(Afs.SecondaryPerson))
					//    SessionState.IsSecPersonMaidenNameRequired = true;
					SessionState.IsSecPersonMaidenNameRequired = Afs.IsSecondaryPersonNeeded;
				}
			}
			base.Page_Load(sender, e);
		}

		private void InitializeTranslation()
		{
			ValidationControlTranslation();
		}

		private void ValidationControlTranslation()
		{
			rfvPrimaryPerson.ErrorMessage = App.Translate("Credability|Afs|PP");
			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}
		protected void btnAfsContinue_Click(object sender, EventArgs e)
		{
			AFSResult AFSResults = null;
			AFS Afs = new AFS();

			Afs.ClientNumber = SessionState.ClientNumber.Value;
			Afs.PrimaryPerson = txtPrimaryPerson.Text;
			Afs.SecondaryPerson = txtSecondPerson.Text;
			if(SessionState.IsSecPersonMaidenNameRequired && string.IsNullOrEmpty(txtSecondPerson.Text))
			{
				//Error Message Mother Maiden Name is Required
				CommonFunction.ShowErrorMessageAtPagebottom(dvErrorSumary, Cccs.Credability.Website.App.Translate("Credability|AFS|WSMR"), true);//"Second Person Mother's Maiden Name Is Required."
			}
			else
			{
				AFSResults = App.Credability.AfsAddUpdate(Afs);
				if(AFSResults.IsSuccessful)
				{
					CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
					if(SessionState.IsSecPersonMaidenNameRequired)
					{
						NavigatePage(RedirectOnNeedAuthorization);
					}
					else
					{
						NavigateContinue();
					}
				}
				else
				{
					dvErrorMessage.InnerHtml = "Some error occured during the operation please try later.";
				}
			}
		}
	}
}