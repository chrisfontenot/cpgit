﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcMonthlyExpenses.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BchControls.UCMonthlyExpenses" %>

<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
	function CheckMaxNumberLarge(sender, args)
	{
		CheckMaxNumber(args, 9999);
	}

	function CheckMaxNumberSmall(sender, args)
	{
		CheckMaxNumber(args, 999);
	}

	function CheckMaxNumber(args, MaxValue)
	{
		var CurValue = String(args.Value)
		if (parseInt(stripCharsNotInBag(CurValue, NoChr)) > MaxValue)
		{
			args.IsValid = confirm('<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpenses|PleaseConfirm")%>' + CurValue + "?")
		}
	}
</script>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpenses|Title")%></h1>
<span id="TopTextGenral" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|IFWT")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PETA")%></p>
</span>
<span id="TopTextPrp" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesPRP|IFWT")%>
	</p>
</span>
<span id="TopTextDmp" runat="server" visible="false">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesDMP|IFWT")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PETA")%></p>
</span>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="UserExpenses" />
<!-- Begin the expenses add form -->
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MFS")%></h1>
<!-- Monthly Food & Shelter -->
<div class="DvErrorSummary" id="dvErrorMessage" runat="server" visible="false">
</div>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ROMP")%></label>
			<asp:TextBox ID="txtRentMort" runat="server" MaxLength="6" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" OnTextChanged="RentMort_TextChanged" TabIndex="1"></asp:TextBox>
			<span class="requiredField">
				*</span>
			<asp:RegularExpressionValidator class="validationMessage2" ID="RentMortRegEx" runat="server" ControlToValidate="txtRentMort" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
			<asp:CustomValidator class="validationMessage" ID="RentMortCustom" runat="server" ControlToValidate="txtRentMort" ClientValidationFunction="CheckMaxNumberLarge" Text="!"></asp:CustomValidator>
		</div>
		<div id="HideForBK" runat="server" visible="true">
			<div class="dvrow" id="FhaQuestion" runat="server" visible="true">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|DTMM")%></label>
				<asp:DropDownList style="margin-left:166px;" ID="ddlIsFhaInc" runat="server" TabIndex="2">
				</asp:DropDownList>
				<span class="requiredField">
					*</span>
				<asp:RequiredFieldValidator style="margin-left:426px;" class="validationMessage" ID="IsFhaIncRequired" runat="server" EnableClientScript="true" InitialValue="0" Display="Dynamic" SetFocusOnError="true" ControlToValidate="ddlIsFhaInc" Text="" ValidationGroup="UserExpenses"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">
				<span id="EquityGenral" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MELoan")%></label>
				</span>
				<span id="EquityPrp" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMontExp|EL")%></label>
				</span>
				<asp:TextBox ID="txtMoEquity" runat="server" MaxLength="6" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" CssClass="txtbx" TabIndex="3"></asp:TextBox>
				<span class="requiredField">*</span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="MoEquityRegEx" runat="server" ControlToValidate="txtMoEquity" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="MoEquityCustom" runat="server" ControlToValidate="txtMoEquity" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
			</div>
			<div class="dvrow">
				<span id="Mort2ndGenral" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|M2M")%></label>
				</span>
				<span id="Mort2ndPrp" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMontExp|2M")%></label>
				</span>
				<asp:TextBox ID="txtMo2ndMort" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" CssClass="txtbx" TabIndex="4" Height="21px"></asp:TextBox>
				<span class="requiredField">*</span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="Mo2ndMortRegEx" runat="server" ControlToValidate="txtMo2ndMort" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="Mo2ndMortCustom" runat="server" ControlToValidate="txtMo2ndMort" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
			</div>
			<div id="HideTaxIns" runat="server" visible="true">


				<div class="dvrow">
					<span id="PayTaxGenral" runat="server" visible="false">
						<label>
							<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MPT")%></label>
					</span>
					<span id="PayTaxPrp" runat="server" visible="false">
						<label>
							<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMontExp|PT")%></label>
					</span>
					<asp:TextBox style="margin-left:166px;" ID="txtMoPropTax" runat="server" onBlur="ReFormatCurrency (this);" onFocus="SetUpField(this);" CssClass="txtbx" TabIndex="5" Enabled="false"></asp:TextBox>
					&nbsp;
					<asp:RegularExpressionValidator class="" ID="MoPropTaxRegEx" runat="server" ControlToValidate="txtMoPropTax" Text="" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
					&nbsp;</div>


				<div class="dvrow">
					<span id="PayInsGenral" runat="server" visible="false">
						<label>
							<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MHOI")%></label>
					</span>
					<span id="PayInsPrp" runat="server" visible="false">
						<label>
							<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMontExp|HOI")%></label>
					</span>
					<asp:TextBox style="margin-left:166px;" ID="txtMoPropIns" runat="server" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" CssClass="txtbx" TabIndex="6" Enabled="false"></asp:TextBox>
					&nbsp;
					<asp:RegularExpressionValidator class="" ID="MoPropInsRegEx" runat="server" ControlToValidate="txtMoPropIns" Text="" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
					&nbsp;</div>

			</div>
			<div class="dvrow">
				<span id="AssocFeeGenral" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MAF")%></label>
				</span>
				<span id="AssocFeePrp" runat="server" visible="false">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMontExp|HAF")%></label>
				</span>
				<asp:TextBox style="margin-left:-17px;" ID="txtMoFee" runat="server" MaxLength="7" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" CssClass="txtbx" TabIndex="7"></asp:TextBox>
				<span class="requiredField">*</span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="MoFeeRegEx" runat="server" ControlToValidate="txtMoFee" Text="" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="MoFeeCustom" runat="server" ControlToValidate="txtMoFee" Text="" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
			</div>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|HM")%></label>
			<asp:TextBox style="margin-left:-10px;" ID="txtHomeMaintenance" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" CssClass="txtbx" TabIndex="8"></asp:TextBox>
			<span class="requiredField">*</span>
			<asp:RegularExpressionValidator class="validationMessage2" ID="HomeMaintenanceRegEx" runat="server" ControlToValidate="txtHomeMaintenance" Text="" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
			&nbsp;<asp:CustomValidator class="" ID="HomeMaintenanceCustom" runat="server" ControlToValidate="txtHomeMaintenance" Text="" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			<br />
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Ele")%></label>
			<asp:TextBox ID="txtUtlElectric" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" CssClass="txtbx" TabIndex="9"></asp:TextBox>
			<span class="requiredField">*</span>                
            <asp:RegularExpressionValidator class="validationMessage2" ID="UtlElectricRegEx" runat="server" ControlToValidate="txtUtlElectric" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="UtlElectricCustom" runat="server" ControlToValidate="txtUtlElectric" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			
			<br />
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|WS")%></label>
			<asp:TextBox ID="txtUtlWater" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" CssClass="txtbx" TabIndex="10"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="UtlWaterRegEx" runat="server" ControlToValidate="txtUtlWater" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="UtlWaterCustom" runat="server" ControlToValidate="txtUtlWater" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
			<br />
		</div>
		<div class="dvrow">
			<span id="GasGenral" runat="server" visible="true">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Gas")%></label>
			</span>
			<span id="GasPrp" runat="server" visible="false">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|NGOFO")%></label>
			</span>
			<asp:TextBox ID="txtUtlGas" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" CssClass="txtbx" TabIndex="11"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="UtlGasRegEx" runat="server" ControlToValidate="txtUtlGas" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="UtlGasCustom" runat="server" ControlToValidate="txtUtlGas" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
			<br />
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CS")%></label>
			<asp:TextBox style="margin-left:46px;" ID="txtUtlTv" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" CssClass="txtbx" TabIndex="12"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="UtlTvRegEx" runat="server" ControlToValidate="txtUtlTv" Text="" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="" ID="UtlTvCustom" runat="server" ControlToValidate="txtUtlTv" Text="" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TC")%></label>
			<asp:TextBox ID="txtUtlTrash" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="13"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="UtlTrashRegEx" runat="server" ControlToValidate="txtUtlTrash" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="UtlTrashCustom" runat="server" ControlToValidate="txtUtlTrash" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TPI")%></label>
			<asp:TextBox ID="txtTelephone" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="14"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="TelephoneRegEx" runat="server" ControlToValidate="txtTelephone" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="TelephoneCustom" runat="server" ControlToValidate="txtTelephone" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|FAFH")%></label>
			<asp:TextBox ID="txtFoodAway" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="15"></asp:TextBox>
			<span class="requiredField">*</span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="FoodAwayRegEx" runat="server" ControlToValidate="txtFoodAway" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="FoodAwayCustom" runat="server" ControlToValidate="txtFoodAway" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>			
			<br />
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|FAH")%></label>
			<asp:TextBox ID="txtGroceries" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" TabIndex="16"></asp:TextBox>
			<span class="requiredField">*</span>
				<asp:RegularExpressionValidator class="validationMessage2" ID="GroceriesRegEx" runat="server" ControlToValidate="txtGroceries" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$"></asp:RegularExpressionValidator>
				&nbsp;<asp:CustomValidator class="validationMessage" ID="GroceriesCustom" runat="server" ControlToValidate="txtGroceries" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			
			<br />
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MT")%></h1>
<!-- Monthly Transportation -->
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AYCO")%></label>
			<asp:DropDownList style="margin-left:162px;" ID="ddlPayAutoCur" runat="server" OnSelectedIndexChanged="ddlPayAutoCur_SelectedIndexChanged" AutoPostBack="true" TabIndex="17">
			</asp:DropDownList>
			<span class="requiredField">*</span>
			<asp:RequiredFieldValidator style="margin-left:424px;" class="validationMessage" ID="PayAutoCurRequired" runat="server" EnableClientScript="true" Display="Dynamic" SetFocusOnError="true" InitialValue="0" ControlToValidate="ddlPayAutoCur" Text="" ValidationGroup="UserExpenses"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AP")%></label>
			<asp:TextBox ID="txtCarPayments" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="18"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="CarPaymentsCustom" runat="server" ControlToValidate="txtCarPayments" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AI")%></label>
			<asp:TextBox ID="txtCarInsurance" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="19"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="CarInsuranceCustom" runat="server" ControlToValidate="txtCarInsurance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|GAM")%></label>
			<asp:TextBox ID="txtCarMaintenance" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="20"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="CarMaintenanceCustom" runat="server" ControlToValidate="txtCarMaintenance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PTranp")%></label>
			<asp:TextBox ID="txtPublicTransportation" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" TabIndex="21"></asp:TextBox>
			<span class="requiredField">*</span>
            <span>
                <asp:CustomValidator class="validationMessage" ID="PublicTransportationCustom" runat="server" ControlToValidate="txtPublicTransportation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<!-- Monthly Insurance, Medical & Childcare  -->
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MIMC")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ILMI")%></label>
			<asp:TextBox ID="txtInsurance" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="22"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="InsuranceCustom" runat="server" ControlToValidate="txtInsurance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MVAP")%></label>
			<asp:TextBox ID="txtMedicalPrescription" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="23"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="MedicalPrescriptionCustom" runat="server" ControlToValidate="txtMedicalPrescription" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CSA")%></label>
			<asp:TextBox ID="txtChildSupport" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="24"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="ChildSupportCustom" runat="server" ControlToValidate="txtChildSupport" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CCEC")%></label>
			<asp:TextBox ID="txtChildElderCare" onBlur="ReFormatCurrency (this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="25"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="ChildElderCareCustom" runat="server" ControlToValidate="txtChildElderCare" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ERE")%></label>
			<asp:TextBox ID="txtEducation" onBlur="ReFormatCurrency (this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="26"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="EducationCustom" runat="server" ControlToValidate="txtEducation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MPME")%></h1>
<!-- Monthly Personal & Miscellaneous Expenses -->
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TPOO")%></label>
			<asp:TextBox ID="txtOtherLoans" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="6" TabIndex="28"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="OtherLoansCustom" runat="server" ControlToValidate="txtOtherLoans" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CCRO")%></label>
			<asp:TextBox ID="txtContributions" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="29"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="ContributionsCustom" runat="server" ControlToValidate="txtContributions" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Clothing")%></label>
			<asp:TextBox ID="txtClothing" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="30"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="ClothingCustom" runat="server" ControlToValidate="txtClothing" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|LDC")%></label>
			<asp:TextBox ID="txtLaundry" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="31"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="LaundryCustom" runat="server" ControlToValidate="txtLaundry" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PE")%></label>
			<asp:TextBox ID="txtPersonalExpenses" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="32"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="PersonalExpensesCustom" runat="server" ControlToValidate="txtPersonalExpenses" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|BBS")%></label>
			<asp:TextBox ID="txtBeautyBarber" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="33"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="BeautyBarberCustom" runat="server" ControlToValidate="txtBeautyBarber" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|RMV")%></label>
			<asp:TextBox ID="txtRecreation" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="34"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="RecreationCustom" runat="server" ControlToValidate="txtRecreation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CUD")%></label>
			<asp:TextBox ID="txtClubDues" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="35"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="ClubDuesCustom" runat="server" ControlToValidate="txtClubDues" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Gifts")%></label>
			<asp:TextBox ID="txtGifts" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="36"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="GiftsCustom" runat="server" ControlToValidate="txtGifts" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MBCM")%></label>
			<asp:TextBox ID="txtMiscellaneous" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);" runat="server" MaxLength="7" TabIndex="37"></asp:TextBox>
			<span class="requiredField">*</span>
			<span>
				<asp:CustomValidator class="validationMessage" ID="MiscellaneousCustom" runat="server" ControlToValidate="txtMiscellaneous" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
			</span>
		</div>
		<div class="dvrow">
			<label>
			</label>
		</div>
		<div class="dvrow">
			<label>
			</label>
		</div>
		<div class="dvrow">
			<label>
			</label>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|WTPR")%></p>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous" OnClick="btnPrevious_Click" CausesValidation="false" TabIndex="38"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSubmit" class="save" Visible="false" runat="server" ToolTip="Submit" CausesValidation="false" OnClick="btnSaveAndExitAndSubmitToQueue_Click">      
        <span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SendToCounselor")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click" TabIndex="40" ValidationGroup="UserExpenses"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" TabIndex="39" ValidationGroup="UserExpenses"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
<input type="hidden" id="_CONFIRMED" name="_CONFIRMED" value="" />