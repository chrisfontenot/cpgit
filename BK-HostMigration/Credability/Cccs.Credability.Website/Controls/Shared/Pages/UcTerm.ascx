﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcTerm.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcTerm" %>
<script type="text/javascript">
<!--
    function mailto() {
        var username = "info";
        var hostname = "credability.org";
        var linktext = '<%= Cccs.Credability.Website.App.Translate("info@credability.org")%>';  //username + "@" + hostname;
        document.write("<a href=" + "mail" + "to:" + username +
            "@" + hostname + " class='NormLink'>" + linktext + "</a>")
    }
//-->
</script>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" CssClass="print" OnClientClick="window.self.print(); return false;"><span>Print</span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="previous" OnClick="click_btnReverseMortgagePrevious"
            ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
</div>
<div class="clearboth">
</div>
<%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|Pcont")%>
<%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|ROC")%>
<p class="SmallTxt">
    <%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|ROD")%>
    <a href="http://www.state.de.us/attgen/" class="SmallTxt">www.state.de.us/attgen/</a>.
    <br>
    <br>
    <%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|YMCT")%>
</p>
<%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|PCon")%>
<p class="SmallTxt">
    <%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|YMCTA")%>
    <script type="text/javascript">mailto();</script>
    <!--a href="mailto:info@credability.org" class="SmallLink">info@credability.org</a-->
    <%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|OrMail")%>
</p>
<%= Cccs.Credability.Website.App.Translate("Credability|AuthorizationforServiceDMP|TrContent")%>
