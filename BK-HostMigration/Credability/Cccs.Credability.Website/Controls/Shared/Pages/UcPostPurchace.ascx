﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcPostPurchace.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BchControls.UcPostPurchace" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<tr>
	<td class="NormTxt">
		<span id="SpanBKC" runat="server" visible="false">
			<h1>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|COFD")%></h1>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|BYWF")%></p>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MDH")%></p>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HASP")%></p>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYFB")%></p>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HLDI")%></p>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|FTVS")%></p>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HFC")%></p>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MPBT")%></p>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|O5O")%></p>
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MLH")%></p>
		</span>
		<span id="Spanbch" runat="server" visible="false">
			<p>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HAS")%>
			</p>
		</span>
	</td>
</tr>
<tr>
	<td height="10">
	</td>
</tr>
<tr>
	<td class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IFYFB")%>
	</td>
</tr>
<tr>
	<td height="10">
	</td>
</tr>
<tr>
	<td class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|ULC")%>
		<ul class="NormTxt">
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|LOE")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MI")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DOS")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DOA")%>
			</li>
		</ul>
	</td>
</tr>
<tr>
	<td class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WMI")%>
	</td>
</tr>
<tr>
	<td height="10">
	</td>
</tr>
<tr>
	<td class="NormTxt">
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|RTA")%>
		<ul class="NormTxt">
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MOC")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|UCT")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|BUT")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|POT")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|AFN")%>
			</li>
			<li>
				<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HTC")%>
			</li>
		</ul>
	</td>
</tr>
<tr>
	<td height="10">
	</td>
</tr>
<tr>
	<td class="NormTxt">
		<strong><u>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WHW")%></u></strong><br />
		<%= Cccs.Credability.Website.App.Translate("Credability|BKC|FMO")%>
	</td>
</tr>
<tr>
	<td height="10">
	</td>
</tr>
<br />
<strong><u>
	<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYM")%></u></strong><br />
<%= Cccs.Credability.Website.App.Translate("Credability|BKC|ACCCS")%>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MIS")%></h1>
<div class="dvform2col">
	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow">
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|AYML")%>
				</p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOC")%></label>
				<asp:TextBox ID="txtMortHolder" runat="server" Text="" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|LN")%></label>
				<asp:TextBox ID="txtLoanNumber" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label for="<%=txtMortType.ClientID%> %>">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MT")%></label>
				<asp:TextBox ID="txtMortType" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label for="<%=txtMortYears.ClientID%> %>">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MTerm")%></label>
				<asp:TextBox ID="txtMortYears" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label for="<%=txtMortDate.ClientID%> ">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DOM")%></label>
				<asp:TextBox ID="txtMortDate" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label for="<%=txtMortDate.ClientID%> ">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DYM")%></label>
				<asp:TextBox ID="txtGovGrant" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label for="<%=txtRateType.ClientID%> ">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IY")%></label>
				<asp:TextBox ID="txtRateType" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<asp:Panel ID="pnlArmInrest" runat="server">
				<div class="dvrow">
					<label for="<%=txtMortRate.ClientID%> ">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|CIR")%></label>
					<asp:TextBox ID="txtMortRate" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
				<div class="dvrow">
					<label for="<%=txtStartIntRate.ClientID %>">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|OIR")%></label>
					<asp:TextBox ID="txtStartIntRate" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
				<div class="dvrow">
					<label for="<%=txtTopIntRate.ClientID %>">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WITM")%></label>
					<asp:TextBox ID="txtTopIntRate" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
			</asp:Panel>
			<asp:Panel ID="pnlFixed" runat="server">
				<div class="dvrow">
					<label for="<%=txtMortRate1.ClientID %>">
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IR")%></label>
					<asp:TextBox ID="txtMortRate1" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
			</asp:Panel>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|OLB")%></label>
				<asp:TextBox ID="txtOrigBal" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|CB")%></label>
				<asp:TextBox ID="txtOweHome" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|EV")%></label>
				<asp:TextBox ID="txtValHome" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|SMP")%></label>
				<asp:TextBox ID="txtMopmt" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|ATI")%></label>
				<asp:TextBox ID="txtIncludeTax" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WITA")%></label>
				<asp:TextBox ID="txtAnPropTax" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|III")%></label>
				<asp:TextBox ID="txtIncludeIns" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WITAH")%></label>
				<asp:TextBox ID="txtAnPropIns" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOPL")%></label>
				<asp:TextBox ID="txtSizeofhousehold" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="colformrht">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|CLS")%>
				</p>
			</div>
			<div class="dvrow">
				<label for="lblProp4Sale">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYP")%></label>
				<asp:TextBox ID="txtProp4Sale" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HYR")%></label>
				<asp:TextBox ID="txtNote4Close" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HYE")%></label>
				<asp:TextBox ID="txtRepayplan" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|AYC")%></label>
				<asp:TextBox ID="txtMortcurrent" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|POP|IFN")%></label>
				<asp:TextBox ID="txtMosdelinq" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HMDY")%></label>
				<asp:TextBox ID="txtAmtavail" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WDYL")%></label>
				<asp:TextBox ID="txtLastcontactdate" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WWTO")%></label>
				<asp:TextBox ID="txtLastcontactdesc" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|PS")%></label>
				<asp:TextBox ID="txtWhoInHouse" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<p class="col_title">
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|SHE")%></p>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DYHA")%></label>
				<asp:TextBox ID="txtSecMort" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
			</div>
			<asp:Panel ID="pnlScondaryMort" runat="server" Visible="false">
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOC:")%></label>
					<asp:TextBox ID="txtSecondaryholder" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|LoanB:")%></label>
					<asp:TextBox ID="txtSecondaryAm" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|BKC|LS:")%></label>
					<asp:TextBox ID="txtSecondarystatus" runat="server" CssClass="txtBox" Enabled="false" Style="border: 0px;"></asp:TextBox>
				</div>
			</asp:Panel>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<div class="NormTxt" id="ShowTmp">
	<span id="SpanBKCBottom" runat="server" visible="false">
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|COIYA")%></p>
		<p>
			<ul>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|Reinstatement")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|GTBA")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|RP")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|MYR")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|Forbearance")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TLA")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|Modification")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TLATC")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|Refinance")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|TOAN")%>
				</li>
			</ul>
		</p>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|ITB")%></p>
		<p>
			<ul>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|SS")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|SYH")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|DIL")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|GTH")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|Bankruptcy")%>
					<%= Cccs.Credability.Website.App.Translate("Credability|BKC|ELO")%>
				</li>
			</ul>
		</p>
		<p class="col_title">
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|OR")%></p>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|BKC|HRS")%></p>
		<p>
			<ul>
				<li>
					<a href="http://www.hud.gov/foreclosure/index.cfm" target="_blank">http://www.hud.gov/foreclosure/index.cfm</a></li>
				<li>
					<a href="http://www.credability.org/get-started/foreclosure-prevention.aspx" target="_blank">http://www.credability.org/get-started/foreclosure-prevention.aspx</a></li>
			</ul>
			<br />
		</p>
	</span>
</div>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="userprofile" />
<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReturnToPreviousPage" runat="server" CssClass="previous" OnClick="click_btnReturnToPreviousPage"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="click_btnSaveExit" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="click_btnContinue" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
