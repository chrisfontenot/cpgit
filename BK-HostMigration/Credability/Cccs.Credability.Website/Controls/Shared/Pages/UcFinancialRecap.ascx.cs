﻿using System;
using Cccs.Credability.Website.Code;
using Cccs.Web;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcFinancialRecap : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
				DesValidationSummary.HeaderText = Cccs.Credability.Website.App.Translate("ValidationSummary|YouMustEnterAValue");
			}

			FinanicalSituationDetails AnalyzedResult = App.Credability.UserFinalFinanicalSituationGet(SessionState.ClientNumber.Value);
			if(AnalyzedResult != null)
			{
				TotalUnsecuredDebt.Text = AnalyzedResult.AssetsLiabilities.CreditorTotals.Balance.ToString(App.CurrencyFormat);
				lblTotalPayments.Text = AnalyzedResult.AssetsLiabilities.CreditorTotals.Payments.ToString(App.CurrencyFormat);
				lblTotalMonthlyIncome.Text = AnalyzedResult.Income.NetIncomeTotalBoth.ToString(App.CurrencyFormat);
				lblTotalMonthlyExp.Text = AnalyzedResult.MonthlyExpenses.ExpensesTotal.ToString(App.CurrencyFormat);
				lblDiscretionaryIncome.Text = AnalyzedResult.DisposableIncome.ToString(App.CurrencyFormat);
			}
			base.Page_Load(sender, e);
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(Page.IsValid)
			{
				NavigateContinue();
			}
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			NavigateExit();
		}
	}
}