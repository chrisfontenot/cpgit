﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.BchControls
{
	public partial class UCMonthlyExpenses : CounselingUcBase
	{
		private static readonly string _CONFIRMED = "_CONFIRMED";
		public bool IsSubmitButtonVisible
		{
			get
			{
				return btnSubmit.Visible;
			}
			set
			{
				btnSubmit.Visible = value;
			}

		}

		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				ddlIsFhaInc.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
				CommonFunction.AddItems(ddlIsFhaInc.Items, "YesNo", SessionState.LanguageCode);
				CommonFunction.AddItems(ddlPayAutoCur.Items, "CurrentOnAutoPayments", SessionState.LanguageCode);
				FormDataSet(App.Credability.UserMonthlyExpensesGet(SessionState.ClientNumber.Value));
				PageSetUp();
			}

			base.Page_Load(sender, e);
		}

		private void ValidationControlTranslation()
		{
			RentMortRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|ROMPR");
			RentMortCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|VMB");
			IsFhaIncRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IO");
			MoEquityRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MELR");
			MoEquityCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|VMBL");
			Mo2ndMortRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|M2MiR");
			Mo2ndMortCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|VM");
			MoPropTaxRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MPT");
			MoPropInsRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MHO");
			MoFeeRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MOVM");
			HomeMaintenanceRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|HMIR");
			HomeMaintenanceCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|HMVM");
			UtlElectricRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EE");
			UtlElectricCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|WVM");
			UtlWaterRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EWS");
			UtlWaterCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EWSV");
			UtlGasRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EG");
			UtlGasCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EGV");
			UtlTvRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|ECS");
			UtlTvCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CSV");
			UtlTrashRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|TC");
			UtlTrashCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|TCV");
			TelephoneRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|TE");
			TelephoneCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|TEV");
			FoodAwayRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|FAFHR");
			FoodAwayCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|FAVM");
			GroceriesRegEx.ErrorMessage = App.Translate("Credability|MonthlyExpenses|FAHR");
			GroceriesCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|FAHRV");

			PayAutoCurRequired.ErrorMessage = App.Translate("Credability|MonthlyExpenses|PSIY");
			CarPaymentsCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|PSIYV");
			CarInsuranceCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|AIV");
			CarMaintenanceCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|AMV");
			PublicTransportationCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|PTV");

			InsuranceCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|INV");
			MedicalPrescriptionCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MVV");
			ChildSupportCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CSVM");
			ChildElderCareCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CCVM");
			EducationCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|EREV");

			OtherLoansCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|TPV");
			ContributionsCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CV");
			ClothingCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CLV");
			LaundryCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|LAV");
			PersonalExpensesCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|PEV");
			BeautyBarberCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|BEV");
			RecreationCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|REV");
			ClubDuesCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|CUDV");
			GiftsCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|GIV");
			MiscellaneousCustom.ErrorMessage = App.Translate("Credability|MonthlyExpenses|MISV");

			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		private void InitializeTranslation()
		{
			ValidationControlTranslation();
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			SaveData();
			NavigatePrevious();
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			if(SaveData())
			{
				NavigateExit();
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(ValidateData())
			{
				if(SaveData())
				{
					NavigateContinue();
				}
			}
		}

		protected void btnSaveAndExitAndSubmitToQueue_Click(object sender, EventArgs e)
		{
			SaveData();
			SubmitToQueue();
		}

		public TextBox EnableDisableAutoPayments()
		{
			if(ddlPayAutoCur.SelectedValue == "3")
			{
				txtCarPayments.Enabled = false;
				txtCarPayments.Text = "$0";
				return txtCarInsurance;
			}
			else
			{
				txtCarPayments.Enabled = true;
				return txtCarPayments;
			}
		}

		private bool SaveData()
		{
			bool ret = true;
			UserMonthlyExpensesResult result = App.Credability.UserMonthlyExpensesUpdate(FormDataGet());
			if(!result.IsSuccessful)
			{
				ShowError(result);
				ret = false;
			}
			return ret;
		}

		private void ShowError(Result result)
		{
			if(result != null && !result.IsSuccessful)
			{
				string error = null;
#if DEBUG
				if(!string.IsNullOrEmpty(result.ExceptionStr))
				{
					error = result.ExceptionStr;
				}
#endif
				if(string.IsNullOrEmpty(error))
				{
					error = App.Translate("Credability|CredabilityWebSite|CommonErrorMessage");
				}
				CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, error, true);
			}
		}

		private bool ConfirmZerosForContinue()
		{
			return !RequireRegisterConfirmZerosScript(btnContinue.UniqueID);
		}

		private UserMonthlyExpenses FormDataGet()
		{
			UserMonthlyExpenses MonthlyExpenses = new UserMonthlyExpenses();
			MonthlyExpenses.ClientNumber = SessionState.ClientNumber.Value;

			MonthlyExpenses.RentMort = txtRentMort.Text.ToDecimal(0);
			MonthlyExpenses.IncludeFha = ddlIsFhaInc.SelectedItem.Value.Trim();
			MonthlyExpenses.MoEquity = txtMoEquity.Text.ToFloat(0);
			MonthlyExpenses.Mo2ndMort = txtMo2ndMort.Text.ToFloat(0);
			if(txtMoPropIns.Enabled)
				MonthlyExpenses.MoPropIns = txtMoPropIns.Text.ToFloat(0);
			if(txtMoPropTax.Enabled)
				MonthlyExpenses.MoPropTax = txtMoPropTax.Text.ToFloat(0);
			MonthlyExpenses.MoFee = txtMoFee.Text.ToFloat(0);
			MonthlyExpenses.HomeMaintenance = txtHomeMaintenance.Text.ToDecimal(0);
			MonthlyExpenses.UtlElectric = txtUtlElectric.Text.ToFloat(0);
			MonthlyExpenses.UtlWater = txtUtlWater.Text.ToFloat(0);
			MonthlyExpenses.UtlGas = txtUtlGas.Text.ToFloat(0);
			MonthlyExpenses.UtlTv = txtUtlTv.Text.ToFloat(0);
			MonthlyExpenses.UtlTrash = txtUtlTrash.Text.ToFloat(0);
			MonthlyExpenses.Telephone = txtTelephone.Text.ToDecimal(0);
			MonthlyExpenses.FoodAway = txtFoodAway.Text.ToDecimal(0);
			MonthlyExpenses.Groceries = txtGroceries.Text.ToDecimal(0);

			if(txtCarPayments.Enabled)
			{
				MonthlyExpenses.CarPayments = txtCarPayments.Text.ToDecimal(0);
			}
			MonthlyExpenses.CarCurrent = ddlPayAutoCur.SelectedValue.ToInt(0);
			MonthlyExpenses.CarInsurance = txtCarInsurance.Text.ToDecimal(0);
			MonthlyExpenses.CarMaintenance = txtCarMaintenance.Text.ToDecimal(0);
			MonthlyExpenses.PublicTransportation = txtPublicTransportation.Text.ToDecimal(0);

			MonthlyExpenses.Insurance = txtInsurance.Text.ToDecimal(0);
			MonthlyExpenses.MedicalPrescription = txtMedicalPrescription.Text.ToDecimal(0);
			MonthlyExpenses.ChildSupportAlimony = txtChildSupport.Text.ToDecimal(0);
			MonthlyExpenses.ChildElderCare = txtChildElderCare.Text.ToDecimal(0);
			MonthlyExpenses.Education = txtEducation.Text.ToDecimal(0);

			MonthlyExpenses.OtherLoans = txtOtherLoans.Text.ToInt(0);
			MonthlyExpenses.Contributions = txtContributions.Text.ToDecimal(0);
			MonthlyExpenses.Clothing = txtClothing.Text.ToDecimal(0);
			MonthlyExpenses.Laundry = txtLaundry.Text.ToDecimal(0);
			MonthlyExpenses.PersonalExpenses = txtPersonalExpenses.Text.ToDecimal(0);
			MonthlyExpenses.BeautyBarber = txtBeautyBarber.Text.ToDecimal(0);
			MonthlyExpenses.Recreation = txtRecreation.Text.ToDecimal(0);
			MonthlyExpenses.ClubDues = txtClubDues.Text.ToDecimal(0);
			MonthlyExpenses.Gifts = txtGifts.Text.ToDecimal(0);
			MonthlyExpenses.Miscellaneous = txtMiscellaneous.Text.ToDecimal(0);

			return MonthlyExpenses;
		}

		private void FormDataSet(UserMonthlyExpenses MonthlyExpenses)
		{
			if(MonthlyExpenses != null)
			{
				txtRentMort.Text = MonthlyExpenses.RentMort.ToString(App.CurrencyFormat);
				if(MonthlyExpenses.IncludeFha != null)
				{
					ddlIsFhaInc.ClearSelection();
					ddlIsFhaInc.SelectedValue = MonthlyExpenses.IncludeFha.Trim();
				}
				txtMoEquity.Text = MonthlyExpenses.MoEquity.ToString(App.CurrencyFormat);
				txtMo2ndMort.Text = MonthlyExpenses.Mo2ndMort.ToString(App.CurrencyFormat);
				txtMoPropTax.Text = MonthlyExpenses.MoPropTax.ToString(App.CurrencyFormat);
				txtMoPropIns.Text = MonthlyExpenses.MoPropIns.ToString(App.CurrencyFormat);
				txtMoFee.Text = MonthlyExpenses.MoFee.ToString(App.CurrencyFormat);
				txtHomeMaintenance.Text = MonthlyExpenses.HomeMaintenance.ToString(App.CurrencyFormat);
				txtUtlElectric.Text = MonthlyExpenses.UtlElectric.ToString(App.CurrencyFormat);
				txtUtlWater.Text = MonthlyExpenses.UtlWater.ToString(App.CurrencyFormat);
				txtUtlGas.Text = MonthlyExpenses.UtlGas.ToString(App.CurrencyFormat);
				txtUtlTv.Text = MonthlyExpenses.UtlTv.ToString(App.CurrencyFormat);
				txtUtlTrash.Text = MonthlyExpenses.UtlTrash.ToString(App.CurrencyFormat);
				txtTelephone.Text = MonthlyExpenses.Telephone.ToString(App.CurrencyFormat);
				txtFoodAway.Text = MonthlyExpenses.FoodAway.ToString(App.CurrencyFormat);
				txtGroceries.Text = MonthlyExpenses.Groceries.ToString(App.CurrencyFormat);

				ddlPayAutoCur.ClearSelection();
				ddlPayAutoCur.SelectedValue = MonthlyExpenses.CarCurrent.ToString();
				txtCarPayments.Text = MonthlyExpenses.CarPayments.ToString(App.CurrencyFormat);
				txtCarInsurance.Text = MonthlyExpenses.CarInsurance.ToString(App.CurrencyFormat);
				txtCarMaintenance.Text = MonthlyExpenses.CarMaintenance.ToString(App.CurrencyFormat);
				txtPublicTransportation.Text = MonthlyExpenses.PublicTransportation.ToString(App.CurrencyFormat);

				txtInsurance.Text = MonthlyExpenses.Insurance.ToString(App.CurrencyFormat);
				txtMedicalPrescription.Text = MonthlyExpenses.MedicalPrescription.ToString(App.CurrencyFormat);
				txtChildSupport.Text = MonthlyExpenses.ChildSupportAlimony.ToString(App.CurrencyFormat);
				txtChildElderCare.Text = MonthlyExpenses.ChildElderCare.ToString(App.CurrencyFormat);
				txtEducation.Text = MonthlyExpenses.Education.ToString(App.CurrencyFormat);

				txtOtherLoans.Text = MonthlyExpenses.OtherLoans.ToString(App.CurrencyFormat);
				txtContributions.Text = MonthlyExpenses.Contributions.ToString(App.CurrencyFormat);
				txtClothing.Text = MonthlyExpenses.Clothing.ToString(App.CurrencyFormat);
				txtLaundry.Text = MonthlyExpenses.Laundry.ToString(App.CurrencyFormat);
				txtPersonalExpenses.Text = MonthlyExpenses.PersonalExpenses.ToString(App.CurrencyFormat);
				txtBeautyBarber.Text = MonthlyExpenses.BeautyBarber.ToString(App.CurrencyFormat);
				txtRecreation.Text = MonthlyExpenses.Recreation.ToString(App.CurrencyFormat);
				txtClubDues.Text = MonthlyExpenses.ClubDues.ToString(App.CurrencyFormat);
				txtGifts.Text = MonthlyExpenses.Gifts.ToString(App.CurrencyFormat);
				txtMiscellaneous.Text = MonthlyExpenses.Miscellaneous.ToString(App.CurrencyFormat);
			}
			else
			{
				txtRentMort.Text = "$0";
				txtMoEquity.Text = "$0";
				txtMo2ndMort.Text = "$0";
				txtMoPropTax.Text = "$0";
				txtMoPropIns.Text = "$0";
				txtMoFee.Text = "$0";
				txtHomeMaintenance.Text = "$0";
				txtUtlElectric.Text = "$0";
				txtUtlWater.Text = "$0";
				txtUtlGas.Text = "$0";
				txtUtlTv.Text = "$0";
				txtUtlTrash.Text = "$0";
				txtTelephone.Text = "$0";
				txtFoodAway.Text = "$0";
				txtGroceries.Text = "$0";

				txtCarPayments.Text = "$0";
				txtCarInsurance.Text = "$0";
				txtCarMaintenance.Text = "$0";
				txtPublicTransportation.Text = "$0";

				txtInsurance.Text = "$0";
				txtMedicalPrescription.Text = "$0";
				txtChildSupport.Text = "$0";
				txtChildElderCare.Text = "$0";
				txtEducation.Text = "$0";

				txtOtherLoans.Text = "$0";
				txtContributions.Text = "$0";
				txtClothing.Text = "$0";
				txtLaundry.Text = "$0";
				txtPersonalExpenses.Text = "$0";
				txtBeautyBarber.Text = "$0";
				txtRecreation.Text = "$0";
				txtClubDues.Text = "$0";
				txtGifts.Text = "$0";
				txtMiscellaneous.Text = "$0";
			}
		}

		private bool RequireRegisterConfirmZerosScript(string ctrlName)
		{
			bool ret = false;
			string[] vals = Request.Params.GetValues(_CONFIRMED);
			// Check if the hidden value has been set
			if(vals == null || vals.Length == 0 || string.IsNullOrEmpty(vals[0]))
			{
				string expText = BuildConfirmZerosText(FormDataGet());

				if(!string.IsNullOrEmpty(expText))
				{
					//string script = string.Format(_ScriptTemplate, expText, ctrlName);
					string script = "if(confirm('" + Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SHE") + "\\r\\n" +
									expText +
									"\\r\\n\\r\\n" + Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IT") + "')){window.setTimeout(\"document.getElementById('" +
									_CONFIRMED +
									"').value = 'Y';__doPostBack('" +
									ctrlName +
									"','');\", 1)}";
					ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ConfirmZeroScript", script, true);
					ret = true;
				}
			}
			return ret;
		}

		private string BuildConfirmZerosText(UserMonthlyExpenses MonthlyExpenses)
		{
			StringBuilder builder = new StringBuilder();

			if(MonthlyExpenses.RentMort == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ROMP"));
			// non BK stuff
			if(App.WebsiteCode != Cccs.Identity.Website.BKC)
			{
				if(MonthlyExpenses.MoEquity == 0)
					AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MELoan"));
				if(MonthlyExpenses.Mo2ndMort == 0)
					AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|M2M"));
				// MoPropTax?
				// MoPropIns?
				if(MonthlyExpenses.MoFee == 0)
					AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MAF"));
			}
			if(MonthlyExpenses.HomeMaintenance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|HOM"));
			if(MonthlyExpenses.UtlElectric == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Ele"));
			if(MonthlyExpenses.UtlWater == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|WS"));
			if(MonthlyExpenses.UtlGas == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Gas"));
			if(MonthlyExpenses.UtlTv == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CS"));
			if(MonthlyExpenses.UtlTrash == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|TC"));
			if(MonthlyExpenses.Telephone == 0)
				AppendListItem(builder, "Telephone");
			if(MonthlyExpenses.FoodAway == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|FAFH"));
			if(MonthlyExpenses.Groceries == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|FAH"));

			if(txtCarPayments.Enabled)
			{
				if(MonthlyExpenses.CarPayments == 0)
					AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|AP"));
			}
			if(MonthlyExpenses.CarInsurance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|AI"));
			if(MonthlyExpenses.CarMaintenance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|GAM"));
			if(MonthlyExpenses.PublicTransportation == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|PTranp"));

			if(MonthlyExpenses.Insurance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ILMI"));
			if(MonthlyExpenses.MedicalPrescription == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MVAP"));
			if(MonthlyExpenses.ChildSupportAlimony == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CSA"));
			if(MonthlyExpenses.ChildElderCare == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CCEC"));
			if(MonthlyExpenses.Education == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ERE"));

			if(MonthlyExpenses.OtherLoans == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|TPOO"));
			if(MonthlyExpenses.Contributions == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CO"));
			if(MonthlyExpenses.Clothing == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Clothing"));
			if(MonthlyExpenses.Laundry == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|LDC"));
			if(MonthlyExpenses.PersonalExpenses == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|PEE"));
			if(MonthlyExpenses.BeautyBarber == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|BBS"));
			if(MonthlyExpenses.Recreation == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|RE"));
			if(MonthlyExpenses.ClubDues == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CUD"));
			if(MonthlyExpenses.Gifts == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Gifts"));
			if(MonthlyExpenses.Miscellaneous == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Miscellaneous"));
			return builder.ToString();
		}

		private void AppendListItem(StringBuilder builder, string item)
		{
			builder.Append("\\r\\n    ").Append(item);
		}

		protected void RentMort_TextChanged(object sender, EventArgs e)
		{
			Page.Validate();
		}

		protected void ddlPayAutoCur_SelectedIndexChanged(object sender, EventArgs e)
		{
			TextBox GetFocus = EnableDisableAutoPayments();
			if(GetFocus != null)
				GetFocus.Focus();
		}

		protected void PageSetUp()
		{
			InitializeTranslation();
			btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
			EnableDisableAutoPayments();
		}

		private bool ValidateData()
		{
			bool PageValid = true;
			//if(ddlIsFhaInc.SelectedValue == "0" && App.WebsiteCode != Cccs.Identity.Website.PRP)
			//{
			//    CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, "Please indicate if private or FHA insurance is included in mortgage payment.", true);
			//    PageValid = false;
			//}
			//if(ddlPayAutoCur.SelectedValue == "0")
			//{
			//    CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, Cccs.Credability.Website.App.Translate("Credability|MonthlyExpenses|PII"), true);
			//    PageValid = false;
			//}
			if(PageValid)
			{
				PageValid = ConfirmZerosForContinue();
				if(ddlPayAutoCur.SelectedValue == "2" && App.WebsiteCode == Cccs.Identity.Website.BKC)
				{
					PageContinue = "MonthlyExpensesAutoPaymentDelinquency.aspx";
				}
			}
			return PageValid;
		}
	}
}