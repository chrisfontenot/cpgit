﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcNetWorth.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcNetWorth" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>

<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
	function CalculateNetWorth(TxtBox)
	{
		var CredBal = <%=CredBal%>, Asset = 0, Liab = 0, OutTxt = "";
		var NumStr, Len, i, Point;
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtHomeVal.ClientID%>").value,"0123456789."));
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtPrptVal.ClientID%>").value,"0123456789."));
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtAutoVal.ClientID%>").value,"0123456789."));
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtRetrVal.ClientID%>").value,"0123456789."));
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtBnkAccVal.ClientID%>").value,"0123456789."));
		Asset += Number(stripCharsNotInBag(document.getElementById("<%=txtOthrVal.ClientID%>").value,"0123456789."));
		Liab += Number(stripCharsNotInBag(document.getElementById("<%=txtHomeOwe.ClientID%>").value,"0123456789."));
		Liab += Number(stripCharsNotInBag(document.getElementById("<%=txtAutoOwe.ClientID%>").value,"0123456789."));
		Liab += Number(stripCharsNotInBag(document.getElementById("<%=txtSecOwe.ClientID%>").value,"0123456789."));
		Liab += Number(stripCharsNotInBag(document.getElementById("<%=txtOthrOwe.ClientID%>").value,"0123456789."));
		NumStr = Math.abs(Asset - (Liab + CredBal));
		Des = NumStr % 1;
		NumStr -= Des;
		NumStr = String(NumStr);
		Len = NumStr.length - 1;
		for (i = 0; i <= Len; i++)
		{
			OutTxt += NumStr.charAt(i);
			if(i < Len && (Len - i) % 3 == 0)//>
			{
				OutTxt += ",";
			}
		}
		if(Asset - (Liab + CredBal) < 0) //>
		{
			OutTxt = "&nbsp;&nbsp;<strong><span style='color:Red;'>- ($" + OutTxt +")</span></strong>";
		}
		else
		{
			OutTxt = "&nbsp;&nbsp;<strong><span style='color:#333;'>+ $" + OutTxt +"</span></strong>";
		}
		document.getElementById("<%=lblNetWorth.ClientID%>").innerHTML = OutTxt;
		ReFormatCurrency(TxtBox);
	}

	function CheckBuyingOrRenting()
	{
		if ("<%=HousingType%>" == "R")
		{
			var HomeVal = document.getElementById('<%=txtHomeVal.ClientID %>');
			reformatCCCSCurrency(HomeVal);
			if (Number(stripCharsNotInBag(HomeVal.value, "0123456789")) > 0)
			{
				return confirm('<%= Cccs.Credability.Website.App.Translate("UnderstandingYourNetWorth|CheckBuyingOrRenting1")%>' + " " + HomeVal.value + " " + '<%=Cccs.Credability.Website.App.Translate("UnderstandingYourNetWorth|CheckBuyingOrRenting2") %>');
			}
		}
		return true;
	}
</script>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Title")%>&nbsp;&nbsp;
</h1>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|MPDNK")%>
</p>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|PLTEO")%>
</h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				&nbsp;</label>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Assets")%></p>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CVOH")%></label>
			<asp:TextBox ID="txtHomeVal" TabIndex="1" value="$0" runat="server" MaxLength="7" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);"></asp:TextBox>
            <span class="requiredField">*</span>
			<a class="NormLink" href="http://www.zillow.com/" target="_blank">
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|RYHEV")%></a>
			<asp:RequiredFieldValidator class="validationMessage" SetFocusOnError="true" ID="rfvHomeVal" runat="server" CssClass="error" EnableClientScript="true" ControlToValidate="txtHomeVal" Display="Dynamic" Text="!"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|REPP")%></label>
			<asp:TextBox ID="txtPrptVal" value="$0" runat="server" MaxLength="7" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="2"></asp:TextBox>
            <span class="requiredField">*</span>
			<asp:RequiredFieldValidator class="validationMessage" SetFocusOnError="true" ID="rfvPrptVal" runat="server" CssClass="error" Display="Dynamic" EnableClientScript="true" ControlToValidate="txtPrptVal" Text="!"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Automobiles")%>:
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|DNITV")%></label>
			<asp:TextBox ID="txtAutoVal" value="$0" runat="server" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="3"></asp:TextBox>
            <span class="requiredField">*</span>
			<a class="NormLink" href="http://www.edmunds.com/tmv/used" target="_blank">
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|RYAE")%>&nbsp;&nbsp;</a><br />
			<asp:RequiredFieldValidator class="validationMessage" SetFocusOnError="true" ID="rfvAutoVal" runat="server" CssClass="error" EnableClientScript="true" Display="Dynamic" ControlToValidate="txtAutoVal" Text="!"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CV")%></label>
			<asp:TextBox ID="txtRetrVal" value="$0" runat="server" MaxLength="7" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="4"></asp:TextBox>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CSA")%></label>
			<asp:TextBox ID="txtBnkAccVal" value="$0" runat="server" MaxLength="20" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="5"></asp:TextBox>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|other")%></label>
			<asp:TextBox ID="txtOthrVal" value="$0" runat="server" MaxLength="50" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="6"></asp:TextBox>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<!-- Liabilities -->
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|PLTES")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				&nbsp;</label>
			<p class="col_title">
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Liabilities")%></p>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|MB")%></label>
			<asp:TextBox ID="txtHomeOwe" value="$0" runat="server" MaxLength="50" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="7"></asp:TextBox>
            <span class="requiredField">*</span>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|IR")%></label>
			<asp:TextBox ID="txtInterRate" value="$0" runat="server" MaxLength="7" onBlur="ReFormatPercent(this);" onFocus="SetUpField(this);" TabIndex="8"></asp:TextBox>
			<span class="requiredField">*</span>
			<asp:RequiredFieldValidator class="error validationMessage" SetFocusOnError="true" ID="rftxtInterRate" runat="server" EnableClientScript="true" ControlToValidate="txtInterRate" Display="Dynamic" Text="!"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|AL")%></label>
			<asp:TextBox ID="txtAutoOwe" value="$0" runat="server" MaxLength="50" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="9"></asp:TextBox>
            <span class="requiredField">*</span>
			<asp:RequiredFieldValidator class="validationMessage" SetFocusOnError="true" ID="rfvAutoOwe" runat="server" CssClass="error" EnableClientScript="true" ControlToValidate="txtAutoOwe" Display="Dynamic" Text="!"></asp:RequiredFieldValidator>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|SD")%></label>
			<asp:TextBox ID="txtSecOwe" value="$0" runat="server" MaxLength="50" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="10"></asp:TextBox>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|OB")%></label>
			<asp:TextBox ID="txtOthrOwe" value="$0" runat="server" MaxLength="50" onBlur="CalculateNetWorth(this);" onFocus="SetUpField(this);" TabIndex="11"></asp:TextBox>
		</div>
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|UD")%></label>
			<asp:Label ID="lblUnsecuredDebt" runat="server" Width="150" Style="line-height: 25px; color: #333; font-weight: bold; font-size: 14px"></asp:Label>
			<a href="ListingYourDebts.aspx?Back=<%=ReturnTo%>">
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|INEED")%></a>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|NetWorth")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<div class="dvrow">
			<label>
				<%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|YNW")%></label>
			<asp:Label ID="lblNetWorth" runat="server" Style="line-height: 25px; color: #333; font-weight: bold; font-size: 14px"></asp:Label>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous" OnClick="btnPrevious_Click" TabIndex="12" ToolTip="Return To Previous Page" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton class="save" ID="btnSubmit" onfocus="SetFocus(this);" runat="server" OnClick="btnSaveAndExitAndSubmitToQueue_Click" CausesValidation="false" Visible="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SendToCounselor")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" TabIndex="14" runat="server" OnClick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" TabIndex="13" runat="server" OnClientClick="return CheckBuyingOrRenting();" OnClick="btnContinue_Click" ToolTip="Continue" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
<script type="text/javascript">
	CalculateNetWorth();
</script>
