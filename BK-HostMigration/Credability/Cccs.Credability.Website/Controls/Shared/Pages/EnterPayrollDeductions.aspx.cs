﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft;
using Newtonsoft.Json;
//using Cccs.Host.Dto;
using Cccs.Credability.Website.Controls.BchControls;
using Cccs.Credability.Website.Bch;
using Cccs.Debtplus.Data;

namespace Cccs.Credability.Website.Controls.Shared.Pages
{

    public partial class EnterPayrollDeductions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!PayrollDeductionRequired()) {
                goNext();
            }

            this.divAction.InnerText = SessionState.ClientNumber.Value.ToString() + " - " + SessionState.State;

            if (IsPostBack)
            {
                handlePostBackAction(this.frmAction.Value.ToLower());
            }
            else {
                SetHeaderByLos();

                //get anything we want to display on the page from database or session
                List<PayrollDeduction> previousDeductions = getPreviouslyEnteredDeductions();

                //put it on the page
                this.deductionsList.Value = JsonConvert.SerializeObject(previousDeductions);
            }
        }

        private bool PayrollDeductionRequired() {
            var isRightLOS = (App.WebsiteCode == Cccs.Identity.Website.BCH || App.WebsiteCode == Cccs.Identity.Website.REC
                || App.WebsiteCode == Cccs.Identity.Website.DMP);
                //what is "Housing only?" || App.WebsiteCode == Cccs.Identity.Website.;

            return
                isRightLOS &&
                (SessionState.State == "MI" || SessionState.State == "SC" || String.IsNullOrEmpty(SessionState.State));
                
        }

        private void SetHeaderByLos() {

            if (App.WebsiteCode == Cccs.Identity.Website.BCH)
            {
                Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Budget);
                CommonFunction.UserProgressSave(PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
                System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (HeadSpan != null)
                {
                    HeadSpan.Visible = true;
                }
                /*
                System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (introText != null)
                {
                    introText.Visible = true;
                }
                */
            }
            if (App.WebsiteCode == Cccs.Identity.Website.DMP) {
                CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
                System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = FindControl("PageHeadDmpOnly") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (HeadSpan != null)
                {
                    HeadSpan.Visible = true;
                }
                /*
                System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (introText != null)
                {
                    introText.Visible = true;
                }
                */
            }

            if (App.WebsiteCode == Cccs.Identity.Website.HUD)
            {
                Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Budget);
                CommonFunction.UserProgressSave(PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
                System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (HeadSpan != null)
                {
                    HeadSpan.Visible = true;
                }
                /*
                System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
                if (introText != null)
                {
                    introText.Visible = true;
                }
                */
            }

        }

        private void handlePostBackAction(string action)
        {                        
            try
            {
                //always save the data if we have it
                if (!String.IsNullOrEmpty(this.deductionsList.Value))
                {
                    List<Debtplus.Data.PayrollDeduction> deductionsToSave = new List<PayrollDeduction>();
                    deductionsToSave = JsonConvert.DeserializeObject<List<Debtplus.Data.PayrollDeduction>>(this.deductionsList.Value);
                    //only save if we have values
                    if (deductionsToSave.Count > 0){
                        //Bancruptcy conversion-Seethal
                        //App.Host.SavePayrollDeductions(SessionState.ClientNumber.Value, deductionsToSave);
                        App.Debtplus.SavePayrollDeductions(SessionState.ClientNumber.Value, deductionsToSave);
                    }
                }                        

                /*here is where "I" am at:
                 "~/Controls/Shared/Pages/EnterPayrollDeductions.aspx"
                 */
                //navigate to the next valid url
                switch (action) { 
                    case "exit":
                        goExit();
                        break;
                    case "previous":
                        goPrevious();
                        break;
                    case "continue":
                        goNext();
                        break;
                }
            }
            catch (Exception ex) { 
                //dunno what to do here yet...probably need to at least log something :)
            }
        }

        private void goNext() { 
            //bch: "MonthlyExpenses.aspx"
            if (App.WebsiteCode == Cccs.Identity.Website.REC)
            {
                Response.Redirect("/Rec/MonthlyExpenses.aspx", false);
            }
            if (App.WebsiteCode == Cccs.Identity.Website.BCH)
            {
                Response.Redirect("/Bch/MonthlyExpenses.aspx", false);
            }
            if (App.WebsiteCode == Cccs.Identity.Website.DMP) {
                Response.Redirect("/DMPOnly/MonthlyExpenses.aspx", false);
            }
            if (App.WebsiteCode == Cccs.Identity.Website.HUD)
            {
                Response.Redirect("/HousingOnly/MonthlyExpenses.aspx", false);
            }
        }
        private void goPrevious() {
            //bch: "~/Bch/UserIncomeDocumentation.aspx"
            if (App.WebsiteCode == Cccs.Identity.Website.BCH)
            {
                Response.Redirect("/Bch/UserIncomeDocumentation.aspx", false);
            }
            if (App.WebsiteCode == Cccs.Identity.Website.DMP)
            {
                Response.Redirect("/DMPOnly/UserIncomeDocumentation.aspx", false);
            }
            if (App.WebsiteCode == Cccs.Identity.Website.HUD)
            {
                Response.Redirect("/HousingOnly/UserIncomeDocumentation.aspx", false);
            }
        }
        private void goExit() {
            //dunno where this goes yet...but we'll try this
            Response.Redirect("/", false);
        }

        private List<PayrollDeduction> getPreviouslyEnteredDeductions() {
            //Bankruptcy conversion-Seethal
            //return App.Host.ReadPayrollDeductions(SessionState.ClientNumber.Value).Value;
            return App.Debtplus.ReadPayrollDeductions(SessionState.ClientNumber.Value);
        }
    }
}