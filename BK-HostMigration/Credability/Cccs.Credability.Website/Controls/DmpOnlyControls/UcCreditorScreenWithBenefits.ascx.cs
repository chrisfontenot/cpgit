﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Code;


namespace Cccs.Credability.Website.Controls.DmpOnlyControls
{
	public partial class UcCreditorScreenWithBenefits : CounselingUcBase
	{
		public string DmpMsg;
		protected new void Page_Load(object sender, EventArgs e)
		{
			CalculateDMP calc = new CalculateDMP(SessionState.ClientNumber.Value);
			DmpMsg = String.Format(App.Translate("Credability|CreditorScreen|DmpStats"), calc.MonthlyPayment, calc.TotalPayment, calc.TotalPeriod, calc.MonthlyDMPPayment, calc.TotalDMPPeriodDifference, calc.InterestDMPSaved);


			DesValidationSummary.HeaderText = Cccs.Credability.Website.App.Translate("ValidationSummary|YouMustEnterAValue");
			base.Page_Load(sender, e);
		}
		
		public void onContinue()
		{
		}
	}
}