﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCThanksUser.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.DmpOnlyControls.UCThanksUser" %>
<table width="90%" border="0" align="center" cellpadding="6" cellspacing="0">
    <tr>
        <td valign="top" width="6%">
            &nbsp;
        </td>
        <td valign="top" width="94%">
            <asp:Image ID="Image1" ImageUrl="../../images/congrats.gif" runat="server" />
        </td>
    </tr>
</table>
<hr width="95%" noshade />
<table width="90%" border="0" align="center" cellpadding="6" cellspacing="0">
    <tr>
        <td align="center" class="MedTxt" style="color: #003366; font-weight: bold;">
  
         <%= Cccs.Credability.Website.App.Translate("Credability|ThanksUser|Complete")%>
          
        </td>
    </tr>
</table>
