﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumControl.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.DmpOnlyControls.BreadCrumControl" %>
<p>
    &nbsp</p>
<div class="dvtab-ctrl">
    <ul>
    

              <li><a id="aSummary" runat="server" class="last"><span>
         <%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Budget")%></span></a></li>
 
               <li><a id="aBudget" runat="server"><span> <%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Income")%>
          </span></a></li>
   <%-- <li><a id="aTellUs" runat="server"><span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|TellUs")%></span></a></li>--%>
        <li><a id="aDebt" runat="server"><span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Debt")%></span></a></li>
        <li><a id="aProfile" runat="server" class="first"><span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|UserProfile")%></span></a></li>
    </ul>
    <div class="clear">
    </div>
    <%--<div class="dvstepTracker" style="width:414px">
		<asp:Label ID="lblDmpProgressTracker" runat="server" Width="100%">&nbsp;</asp:Label>
    <asp:Label ID="lblStep" runat="server" Width="100%"></asp:Label>
</div>--%>
</div>
<div class="clear">
</div>
