﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.DmpOnlyControls
{
	public partial class UcPointsToRemember : CounselingUcBase
    {
        protected new void Page_Load(object sender, EventArgs e)
        {
			base.Page_Load(sender, e);
        }

        public void OnContinue()
        {
			SubmitToQueue(true);
        }
    }
}