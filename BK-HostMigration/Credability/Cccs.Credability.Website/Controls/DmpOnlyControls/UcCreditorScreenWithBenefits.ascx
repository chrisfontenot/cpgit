﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcCreditorScreenWithBenefits.ascx.cs" Inherits="Cccs.Credability.Website.Controls.DmpOnlyControls.UcCreditorScreenWithBenefits" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<span>
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
</span>
<p>
	<b><%=DmpMsg%></b>
</p>
<table cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td style="text-align: justify; width: 50%">
			<%=App.Translate("Credability|CreditorScreen|Debt")%>
		</td>
		<td width="200px" valign="middle" align="left">
			<%=App.Translate("Credability|CreditorScreenWB|Graph")%>
		</td>
	</tr>
</table>
<p />
<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>
<p />
<p />