﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcCreditorScreenWithoutBenefits.ascx.cs" Inherits="Cccs.Credability.Website.Controls.DmpOnlyControls.UcCreditorScreenWithoutBenefits" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<p>
	<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="userprofile" />
</p>
<div style="text-align: justify; width: 95%">
	<%= Cccs.Credability.Website.App.Translate("Credability|CreditorScreen|Debt")%>
</div>
<br />
<Uc:AuthCode ID="UcAuthCode" runat="server"></Uc:AuthCode>