﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.DmpOnlyControls
{
	public partial class UcCreditorScreenWithoutBenefits : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			DesValidationSummary.HeaderText = Cccs.Credability.Website.App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		public void onContinue()
		{
		}
	}
}