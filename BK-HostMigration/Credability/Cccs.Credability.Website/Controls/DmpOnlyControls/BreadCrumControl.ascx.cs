﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Cccs.Credability.Website.Code;
using System.IO;
using Cccs.Credability.Website.Controls.Navigation;

namespace Cccs.Credability.Website.Controls.DmpOnlyControls
{

    public partial class BreadCrumControl : BreadCrumbTemplate
    {


  
        protected override void OnLoad(EventArgs e)
        {
            base.NavigationList = new List<NavigationEntity>()
        {     
        new  NavigationEntity{Order=0, URL="afs.aspx",PercentComplete=5,},  
        new  NavigationEntity{Order=1, URL="userprofile.aspx",tab=  Tab.Profile,PercentComplete=10,FirstPageInTab=true},     
        new  NavigationEntity{Order=2, URL="ListingYourDebts.aspx",tab=Tab.Debt,PercentComplete=30,FirstPageInTab=true},
        new  NavigationEntity{Order=3, URL="creditorscreenwob.aspx",tab=Tab.Debt,PercentComplete=50,FirstPageInTab=false},
        new  NavigationEntity{Order=3, URL="creditorscreenwb.aspx",tab=Tab.Debt,PercentComplete=50,FirstPageInTab=false},
        new  NavigationEntity{Order=4, URL="DescribingYourSituation.aspx",tab=Tab.Budget,PercentComplete=65,FirstPageInTab=true},        
        new  NavigationEntity{Order=5, URL="UserIncomeDocumentation.aspx",tab=Tab.Budget,PercentComplete=70,FirstPageInTab=false},
        new  NavigationEntity{Order=6, URL="monthlyexpenses.aspx",tab=Tab.Summary,PercentComplete=75,FirstPageInTab=true},
        new  NavigationEntity{Order=7, URL="understandingyournetworth.aspx",tab=Tab.Summary,PercentComplete=80,FirstPageInTab=false},
        new  NavigationEntity{Order=8, URL="PointsToRemember.aspx",tab=Tab.Summary,PercentComplete=90,FirstPageInTab=false},
        new  NavigationEntity{Order=9, URL="thankyou.aspx",PercentComplete=100,FirstPageInTab=false},
        };

            base.OnLoad(e);
        }
      
    }
}
//if (!IsPostBack)
//{
//    //       lblDmpProgressTracker.Width = System.Web.UI.WebControls.Unit.Percentage(App.PercentComplete * 0.97);

//    // Enable based on Percent complete
//    //
//    // int percent_complete = App.PercentComplete;
//    NavigationManager manager = new NavigationManager(NavigationList);
//    NavigationEntity CurrentPage = manager.GetCurrentNavigationEntity(Request.PhysicalPath);
//    if (CurrentPage != null)
//    {

//        lblStep.Text = string.Format("Step {0} of {1}",
//        manager.GetCurrentStep(Request.PhysicalPath).ToString(), manager.GetNumberOfSteps());

//        List<NavigationEntity> list = manager.GetListOfActiveTabs();

//        foreach (NavigationEntity entity in list)
//        {
//            HtmlAnchor href = FindControl(string.Format("a{0}", entity.tab)) as HtmlAnchor;
//            href.HRef = AppRelativePathReplace(entity.URL);

//        }
//    }



//if (App.WebsiteCode == Cccs.Identity.Website.DMP)
//{


//if (percent_complete >= Cccs.Credability.Website.DMPonly.PercentComplete.USER_PROFILE)
//{
//    aProfile.HRef = AppRelativePathReplace("UserProfile.aspx");
//}
//if (percent_complete >= Cccs.Credability.Website.DMPonly.PercentComplete.USER_DEBT_LISTING)
//{
//    aDebt.HRef = AppRelativePathReplace("ListingYourDebts.aspx");
//}

//if (percent_complete >= Cccs.Credability.Website.DMPonly.PercentComplete.DESCRIBE_YOUR_SITUATION)
//{
//    aTellUs.HRef = AppRelativePathReplace("DescribingYourSituation.aspx");
//}

//if (percent_complete >= Cccs.Credability.Website.DMPonly.PercentComplete.USER_INCOME_DOCUMENTATION)
//{
//    aBudget.HRef = AppRelativePathReplace("UserIncomeDocumentation.aspx");
//}

//if (percent_complete >= Cccs.Credability.Website.DMPonly.PercentComplete.POINTS_TO_REMEMBER)
//{
//    aSummary.HRef = AppRelativePathReplace("PointsToRemember.aspx");
//}
//   }
//        }
//     }


//   }
//}