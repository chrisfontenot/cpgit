﻿using System;
using Cccs.CcrcHpf;

namespace Cccs.Credability.Website.Controls.HousingOnly
{
	public partial class UcClientCounseled : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			string NextPage = "DescribeYourSituation.aspx";
			CounseldInfo.Visible = false;
			if(IsHpfClient())
			{
				Response.Redirect(NextPage);
			}
			CheckHpfData CheckAccnt = new CheckHpfData();
			PersonalInfoData ProfileData = PersonalInfoData.GetData(SessionState.UserId.Value);
			CheckAccnt.FName = ProfileData.PrimaryUserDetail.FirstName;
			CheckAccnt.LName = ProfileData.PrimaryUserDetail.LastName;
			CheckAccnt.Ssn = ProfileData.PrimaryUserDetail.Ssn;
			CheckAccnt.Addr1 = ProfileData.PrimaryUserAddress.StreetLine1;
			CheckAccnt.City = ProfileData.PrimaryUserAddress.City;
			CheckAccnt.ZipCode = ProfileData.PrimaryUserAddress.Zip;
			CcrcHpf.Impl.CcrcHpf CheckHpf = new CcrcHpf.Impl.CcrcHpf();
			CheckHpfResult HpfInfo = CheckHpf.CheckHpfCase(CheckAccnt);
			if(HpfInfo.CredAbilityClient)
			{
				App.Credability.HpfIdSet(SessionState.ClientNumber.Value, (int) HpfInfo.HpfId);
				Response.Redirect(NextPage);
			}
			if(!HpfInfo.AnotherAgencyClient)
			{
				Response.Redirect(NextPage);
			}
			CounseldInfo.Visible = true;
			LblCounsAgncy.Text = String.Format(App.Translate("Credability|ClientCounseled|CounsAgncy"), HpfInfo.AgencyName);
			LblCounsName.Text = String.Format(App.Translate("Credability|ClientCounseled|CounsName"), HpfInfo.CounselorFullName);
			LblContInfo.Text = App.Translate("Credability|ClientCounseled|ContInfo");
			if(String.IsNullOrEmpty(HpfInfo.CounselorEmail))
				LblContEmail.Visible = false;
			else
				LblContEmail.Text = String.Format(App.Translate("Credability|ClientCounseled|CounsEmail"), HpfInfo.CounselorEmail);
			if(String.IsNullOrEmpty(HpfInfo.CounselorPhone))
			{
				LblContPhone.Visible = false;
				LblContExt.Visible = false;
			}
			else
			{
				LblContPhone.Text = String.Format(App.Translate("Credability|ClientCounseled|CounsPhone"), HpfInfo.CounselorPhone);
				if(String.IsNullOrEmpty(HpfInfo.CounselorExt))
					LblContExt.Visible = false;
				else
					LblContExt.Text = String.Format(App.Translate("Credability|ClientCounseled|CounsExt"), HpfInfo.CounselorExt);
			}
		}

		private static bool IsHpfClient()
		{
			return App.Credability.CcrcHpfDataGet(SessionState.ClientNumber.Value).HpfID != null;
		}
	}
}