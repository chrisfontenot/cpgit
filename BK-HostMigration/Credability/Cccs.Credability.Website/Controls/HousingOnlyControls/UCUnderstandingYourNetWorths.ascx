﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCUnderstandingYourNetWorths.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCUnderstandingYourNetWorths" %>
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Title")%>&nbsp;&nbsp;</h1>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|MPDNK")%></p>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|PLTEO")%></h1>
<div class="dvform2col dvformlblbig">
   
        <div class="dvform">
            <div class="dvrow">
            <label>&nbsp;</label>
            <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Assets")%></p>            
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CVOH")%></label>
            <asp:TextBox ID="txtHomeVal" value="$0" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox><span> *</span> <a class="NormLink" href="http://www.zillow.com/" target="_blank"><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|RYHEV")%></a>
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvHomeVal" runat="server" CssClass="error"
                EnableClientScript="true" ControlToValidate="txtHomeVal" ErrorMessage="Current Value of Home required"
                Display="Dynamic" Text="Enter Current Value"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|REPP")%></label>
            <asp:TextBox ID="txtPrptVal" value="$0" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox><span> *</span>
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvPrptVal" runat="server" CssClass="error"
                Display="Dynamic" EnableClientScript="true" ControlToValidate="txtPrptVal" ErrorMessage="Real Estate/Personal Property"
                Text="Enter Real Estate/Personal"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Automobiles")%> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|DNITV")%></label>
            <asp:TextBox ID="txtAutoVal" value="$0" runat="server" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox><span> *</span> <a class="NormLink" href="http://www.edmunds.com/tmv/used" target="_blank">
                        <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|RYAE")%>&nbsp;&nbsp;</a><br />
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvAutoVal" runat="server" CssClass="error"
                EnableClientScript="true" Display="Dynamic" ControlToValidate="txtAutoVal" ErrorMessage="Automobiles required"
                Text="Enter Automobiles"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
            <label> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CV")%></label>
            <asp:TextBox ID="txtRetrVal" value="$0" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|CSA")%></label>
            <asp:TextBox ID="txtBnkAccVal" value="$0" runat="server" MaxLength="20" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|other")%></label>
            <asp:TextBox ID="txtOthrVal" value="$0" runat="server" MaxLength="50" onBlur="reformatCCCSCurrency(this);" onFocus="prepareField(this);"></asp:TextBox>
            </div>
       
    </div>
    <div class="clearboth"> </div>
</div>
    <!-- Liabilities -->
    <h1><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|PLTES")%></h1>
<div class="dvform2col dvformlblbig">
   
        <div class="dvform">
            <div class="dvrow">
            <label>&nbsp;</label>
            <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|Liabilities")%></p>
            
            </div>
            <div class="dvrow">
            <label> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|MB")%></label>
            <asp:TextBox ID="txtHomeOwe" value="$0" runat="server" MaxLength="50" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox><span> *</span>
            </div>
            <div class="dvrow">
            <label> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|IR")%></label>
            <asp:TextBox ID="txtInterRate" value="$0" runat="server" MaxLength="7" onBlur="PercentreformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox> <span>*</span>
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rftxtInterRate" runat="server" class="error"
                EnableClientScript="true" ControlToValidate="txtInterRate" ErrorMessage="Mortgage Balance required"
                Display="Dynamic" Text="Enter Mortgage Balance"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|AL")%></label>
            <asp:TextBox ID="txtAutoOwe" value="$0" runat="server" MaxLength="50" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox><span> *</span>
            <asp:RequiredFieldValidator SetFocusOnError="true" ID="rfvAutoOwe" runat="server" CssClass="error"
                EnableClientScript="true" ControlToValidate="txtAutoOwe" Display="Dynamic" ErrorMessage="Auto Loans required"
                Text="Enter Auto Loans"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|SD")%></label>
            <asp:TextBox ID="txtSecOwe" value="$0" runat="server" MaxLength="50" onBlur="reformatCCCSCurrency(this);" onFocus="prepareField(this);"></asp:TextBox>
            </div>
            <div class="dvrow">
            <label> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|OB")%></label>
            <asp:TextBox ID="txtOthrOwe" value="$0" runat="server" MaxLength="50" onBlur="reformatCCCSCurrency(this);"
                onFocus="prepareField(this);"></asp:TextBox>
            </div>
            <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|UD")%></label>
            <asp:Label ID="lblUnsecuredDebt" runat="server" Width="150" style="line-height:25px;color:#333; font-weight:bold; font-size:14px"></asp:Label>
            <a href="UserDebtListing.aspx">
                <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|INEED")%></a>
            </div>
       
    </div>
    <div class="clearboth"> </div>
</div>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|NetWorth")%></h1>
<div class="dvform2col dvformlblbig">
   
        <div class="dvform">
            <div class="dvrow">
            <label> <%= Cccs.Credability.Website.App.Translate("Credability|UnderstandingYourNetWorths|YNW")%></label>
            <asp:Label ID="lblNetWorth" runat="server" style="line-height:25px; color:#333; font-weight:bold; font-size:14px"></asp:Label>
            </div>
       
    </div>
    <div class="clearboth"> </div>
</div>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">

    <tr>
        <td class="NormTxt" align="right" valign="top">
           &nbsp;&nbsp;&nbsp;
        </td>
        <td height="35" align="left" valign="top">
            &nbsp;&nbsp;&nbsp;
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td height="30" colspan="3">
        </td>
    </tr>
</table>

<div class="dvbtncontainer">
<div class="lnkbutton">
           <asp:LinkButton ID="imgBtnUnderstandingNetWorthPrevious" runat="server" CssClass="previous"
                 onclick="imgBtnUnderstandingNetWorthPrevious_Click" ToolTip="Return To Previous Page" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="imgBtnUnderstandingNetWorthContinue" runat="server"
                 onclick="imgBtnUnderstandingNetWorthContinue_Click" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="imgBtnUnderstandingNetWorthToPrevious" runat="server"
                 onclick="imgBtnUnderstandingNetWorthToPrevious_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
        </div>

</div>

   