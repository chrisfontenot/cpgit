﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCAlwaysAvailableToHelp.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCAlwaysAvailableToHelp" %>
<table width="90%" align="center">
    <tr>
        <td>
            <asp:Image ID="Image1"  runat="server" ImageUrl="../../images/BT_werealwaysavailabletohelp.gif" width="500" height="110" border="0" />            
            <p>
            
            <%= Cccs.Credability.Website.App.Translate("Credability|AlwaysAvailableToHelp|TYFCC")%>
            
                
            </p>
            <p>
            
             <%= Cccs.Credability.Website.App.Translate("Credability|AlwaysAvailableToHelp|IWHAQ")%>
             
               
            </p>
            
            <%= Cccs.Credability.Website.App.Translate("Credability|AlwaysAvailableToHelp|Block")%>
             
           
            <p align="center">
                <a href="http://www.cccsatl.org/" class="external">Return to CCCS Homepage</a>
            </p>
        </td>
    </tr>
</table>
