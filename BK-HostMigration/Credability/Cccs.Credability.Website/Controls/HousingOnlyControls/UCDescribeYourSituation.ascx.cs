﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCDescribeYourSituation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShowContactDetailPageData();
                ShowExtraHubDetailPageData();
                ShowExtraQuestDetailPageData();


            }
        }
        private void ShowContactDetailPageData()
        {
            UserDecsribeYourSituationBCH userDecsribeYourSituationBCH = null;
            userDecsribeYourSituationBCH = App.Credability.UserDecsribeYourSituationBCHGet(SessionState.ClientNumber.Value);
            //userDecsribeYourSituationBCH = App.Credability.UserDecsribeYourSituationBCHGet(SessionState.ClientNumber.Value);

            if (userDecsribeYourSituationBCH != null)
            {
                txtComment.Text = userDecsribeYourSituationBCH.ContactComments;
                if (userDecsribeYourSituationBCH.ContactReason != null)
                {
                    ddlEvent.ClearSelection();
                    ddlEvent.SelectedValue = userDecsribeYourSituationBCH.ContactReason;
                }


                txtCurIntRate.Text = (userDecsribeYourSituationBCH.MortRate != null) ? userDecsribeYourSituationBCH.MortRate.ToString() : string.Empty;
                txtDateOfMortage.Text = (userDecsribeYourSituationBCH.MortDate != null) ? userDecsribeYourSituationBCH.MortDate.ToString() : string.Empty;
                txtHomeVal.Text = (userDecsribeYourSituationBCH.ValHome != null) ? userDecsribeYourSituationBCH.ValHome.ToString() : string.Empty;
                txtHouseholdSize.Text = (userDecsribeYourSituationBCH.SizeOfHouseHold != null) ? userDecsribeYourSituationBCH.SizeOfHouseHold.ToString() : string.Empty;
                txtHouseholdSizeRent.Text = (userDecsribeYourSituationBCH.SizeOfHouseHold != null) ? userDecsribeYourSituationBCH.SizeOfHouseHold.ToString() : string.Empty;
                //rblHousingSituation.SelectedValue = 
                if (!string.IsNullOrEmpty(userDecsribeYourSituationBCH.HousingType))
                {
                    if (userDecsribeYourSituationBCH.HousingType.ToString().Trim() == "B")
                    {
                        dvBuying.Visible = true;
                        rfvPriLoanServ.ValidationGroup = "userprofile";
                        RequiredFieldValidator1.ValidationGroup = "userprofile";
                        rfvddlMortageType.ValidationGroup = "userprofile";
                        rfvtxtMortageTerm.ValidationGroup = "userprofile";
                        dvAuthorizationCode.Visible = false;
                        rblHousingSituation.SelectedValue = userDecsribeYourSituationBCH.HousingType.ToString().Trim();
                    }
                    else
                    {
                        rfvPriLoanServ.ValidationGroup = "";
                        RequiredFieldValidator1.ValidationGroup = "";
                        rfvddlMortageType.ValidationGroup = "";
                        rfvtxtMortageTerm.ValidationGroup = "";
                        dvBuying.Visible = false;
                        dvAuthorizationCode.Visible = true;
                        rblHousingSituation.SelectedValue = userDecsribeYourSituationBCH.HousingType.ToString().Trim();
                    }

                }
                else
                {
                    dvBuying.Visible = false;
                    dvAuthorizationCode.Visible = true;
                }

                //(userDecsribeYourSituationBCH.MortCurrent != null) ? userDecsribeYourSituationBCH.MortCurrent.ToString() : string.Empty;

                txtMortLate.Text = (userDecsribeYourSituationBCH.MosDelinq != null) ? userDecsribeYourSituationBCH.MosDelinq.ToString() : string.Empty;
                txtLoanDesc.Text = (userDecsribeYourSituationBCH.LastContactDesc != null) ? userDecsribeYourSituationBCH.LastContactDesc.ToString() : string.Empty;
                txtPriLoanHold.Text = (userDecsribeYourSituationBCH.MortHolder != null) ? userDecsribeYourSituationBCH.MortHolder.ToString() : string.Empty;
                txtLoanNo.Text = (userDecsribeYourSituationBCH.LoanNumber != null) ? userDecsribeYourSituationBCH.LoanNumber.ToString() : string.Empty;
                txtLoanNo2.Text = (userDecsribeYourSituationBCH.LoanNumber2 != null) ? userDecsribeYourSituationBCH.LoanNumber2.ToString() : string.Empty;
                if (userDecsribeYourSituationBCH.MortType != null)
                {
                    ddlMortageType.ClearSelection();
                    ddlMortageType.SelectedValue = userDecsribeYourSituationBCH.MortType.ToString().Trim();
                }
                txtMortageTerm.Text = (userDecsribeYourSituationBCH.MortYears != null) ? userDecsribeYourSituationBCH.MortYears.ToString() : string.Empty;
                txtDateOfMortage.Text = (userDecsribeYourSituationBCH.MortDate != null) ? userDecsribeYourSituationBCH.MortDate.ToString() : string.Empty;
                if (userDecsribeYourSituationBCH.RateType != null)
                {
                    ddlIntType.ClearSelection();
                    ddlIntType.SelectedValue = userDecsribeYourSituationBCH.RateType.ToString().Trim();

                }

                txtStartBal.Text = (userDecsribeYourSituationBCH.OrigBal != null) ? userDecsribeYourSituationBCH.OrigBal.ToString() : "0";
                txtCurrentBal.Text = (userDecsribeYourSituationBCH.Owehome != null) ? userDecsribeYourSituationBCH.Owehome.ToString() : string.Empty;
                txtHomeVal.Text = (userDecsribeYourSituationBCH.ValHome != null) ? userDecsribeYourSituationBCH.ValHome.ToString() : "0";
                txtLoanPay.Text = (userDecsribeYourSituationBCH.MoPmt != null) ? userDecsribeYourSituationBCH.MoPmt.ToString() : "0";
                txtMoneyAvail.Text = (userDecsribeYourSituationBCH.AmtAvail != null) ? userDecsribeYourSituationBCH.AmtAvail.ToString() : "0";
                txtLoanDescDate.Text = (userDecsribeYourSituationBCH.LastContactDate != null) ? userDecsribeYourSituationBCH.LastContactDate.ToString() : string.Empty;
                rblPreDmp.SelectedValue = (userDecsribeYourSituationBCH.RepayPlan != null) ? userDecsribeYourSituationBCH.RepayPlan.ToString().Trim() : string.Empty;
                rblMortCur.SelectedValue = (userDecsribeYourSituationBCH.MortCurrent != null) ? userDecsribeYourSituationBCH.MortCurrent.ToString().Trim() : string.Empty;
                txtSecLoanHold.Text = (userDecsribeYourSituationBCH.SecondaryHolder != null) ? userDecsribeYourSituationBCH.SecondaryHolder.ToString() : "0";
                txtMtg2Amt.Text = (userDecsribeYourSituationBCH.SecondaryAmt != null) ? userDecsribeYourSituationBCH.SecondaryAmt.ToString() : "0";
                //txtMtg2Stat.Text = (userDecsribeYourSituationBCH.SecondaryStatus != null) ? userDecsribeYourSituationBCH.SecondaryStatus.ToString() :"0";
                txtMtg2Stat.Text = (userDecsribeYourSituationBCH.SecondaryStatus != null) ? userDecsribeYourSituationBCH.SecondaryStatus.ToString() : string.Empty;
                rblSellingProp.SelectedValue = (userDecsribeYourSituationBCH.PROP4Sale != null) ? userDecsribeYourSituationBCH.PROP4Sale.ToString() : "0";
                rblGotNotice.SelectedValue = (userDecsribeYourSituationBCH.NOTE4Close != null) ? userDecsribeYourSituationBCH.NOTE4Close.ToString() : "0";
                if (userDecsribeYourSituationBCH.WHOInHouse != null)
                {
                    ddlPropStat.ClearSelection();
                    ddlPropStat.SelectedValue = userDecsribeYourSituationBCH.WHOInHouse.ToString().Trim();

                }

                ddlPriLoanServ.ClearSelection();

                CompanyInfo[] companyInfo = App.Credability.CompanyListGet();
                if (companyInfo != null)
                {
                    ListItem lt = new ListItem();
                    lt.Value = "0";
                    lt.Text = "-- Select One --";
                    ddlPriLoanServ.DataTextField = "CompanyName";
                    ddlPriLoanServ.DataValueField = "ServID";

                    ddlPriLoanServ.DataSource = companyInfo;
                    ddlPriLoanServ.DataBind();
                    ddlPriLoanServ.Items.Insert(0, lt);
                    ddlPriLoanServ.ClearSelection();
                    if (!string.IsNullOrEmpty(userDecsribeYourSituationBCH.PRIServID.ToString()))
                        ddlPriLoanServ.SelectedValue = userDecsribeYourSituationBCH.PRIServID.ToString().Trim();


                    ListItem lt1 = new ListItem();
                    lt1.Value = "0";
                    lt1.Text = "-- Select One --";
                    ddlSecLoanServ.DataTextField = "CompanyName";
                    ddlSecLoanServ.DataValueField = "ServID";
                    ddlSecLoanServ.DataSource = companyInfo;
                    ddlSecLoanServ.DataBind();
                    ddlSecLoanServ.Items.Insert(0, lt1);
                    ddlSecLoanServ.ClearSelection();

                    if (userDecsribeYourSituationBCH.SECServID != 0)
                    {
                        rblSecondLoan.SelectedValue = "Yes";
                        pnlSeconLoanInfo.Visible = true;
                        ddlSecLoanServ.SelectedValue = userDecsribeYourSituationBCH.SECServID.ToString();
                    }
                    else
                    {
                        rblSecondLoan.SelectedValue = "No";
                        pnlSeconLoanInfo.Visible = false;
                    }
                    //if (!string.IsNullOrEmpty(userDecsribeYourSituationBCH.SECServID.ToString()))
                    //{

                    //    ddlSecLoanServ.SelectedValue = userDecsribeYourSituationBCH.SECServID.ToString();
                    //    rblSecondLoan.SelectedValue = "Yes";
                    //    pnlSeconLoanInfo.Visible = true;
                    //}
                    //else
                    //{
                    //    pnlSeconLoanInfo.Visible = false;
                    //    rblSecondLoan.SelectedValue = "No";
                    //}

                }


                rblAffHomeMod.SelectedValue = (userDecsribeYourSituationBCH.MAKEHomeAff != null) ? userDecsribeYourSituationBCH.MAKEHomeAff.ToString() : string.Empty;

                // txtInsPay.Text=userDecsribeYourSituationBCH

                //For ExtraQuest

            }
            else
            {
                Session["LogOutResn"] = "No Result Set";
            }



        }
        private void ShowExtraQuestDetailPageData()
        {
            UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest = null;
            userSituationDescriptionExtraQuest = App.Credability.UserSituationDescriptionExtraQuestGet(SessionState.ClientNumber.Value);
            //userSituationDescriptionExtraQuest = App.Credability.UserSituationDescriptionExtraQuestGet(SessionState.ClientNumber.Value);
            if (userSituationDescriptionExtraQuest != null)
            {
                if (userSituationDescriptionExtraQuest.SECEvent != null)
                {
                    ddlSecEvent.ClearSelection();
                    ddlSecEvent.SelectedValue = userSituationDescriptionExtraQuest.SECEvent.ToString();
                }
                rblMedExp.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MEDExp)) ? userSituationDescriptionExtraQuest.MEDExp.ToString().Trim() : string.Empty;
                rblLostSO.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.LOSTSO)) ? userSituationDescriptionExtraQuest.LOSTSO.ToString().Trim() : string.Empty;
                rblHealthWage.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.HEALTHWage)) ? userSituationDescriptionExtraQuest.HEALTHWage.ToString().Trim() : string.Empty;
                rblUnemployed.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.UNEmployed)) ? userSituationDescriptionExtraQuest.UNEmployed.ToString().Trim() : string.Empty;
                rblTaxLien.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.TAXLien)) ? userSituationDescriptionExtraQuest.TAXLien.ToString().Trim() : string.Empty;
                rblMortRate.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MORTRate)) ? userSituationDescriptionExtraQuest.MORTRate.ToString().Trim() : string.Empty;

            }
        }
        private void ShowExtraHubDetailPageData()
        {
            //For ExtraHub
            UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt = null;
            userSituationDescriptionExtraHudAbt = App.Credability.UserSituationDescriptionExtraHudAbtGet(SessionState.ClientNumber.Value);
            //userSituationDescriptionExtraHudAbt = App.Credability.UserSituationDescriptionExtraHudAbtGet(SessionState.ClientNumber.Value);
            if (userSituationDescriptionExtraHudAbt != null)
            {
                rblGovNpPro.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.GOVGrant)) ? userSituationDescriptionExtraHudAbt.GOVGrant.ToString().Trim() : string.Empty;
                txtOrgIntRate.Text = (userSituationDescriptionExtraHudAbt.STARTIntRate != null) ? userSituationDescriptionExtraHudAbt.STARTIntRate.ToString() : "0";
                txtMaxIntRate.Text = (userSituationDescriptionExtraHudAbt.TOPIntRate != null) ? userSituationDescriptionExtraHudAbt.TOPIntRate.ToString() : "0";
                txtTaxPay.Text = (userSituationDescriptionExtraHudAbt.ANPropTax != null) ? userSituationDescriptionExtraHudAbt.ANPropTax.ToString() : "0";
                txtInsPay.Text = (userSituationDescriptionExtraHudAbt.ANPropIns != null) ? userSituationDescriptionExtraHudAbt.ANPropIns.ToString() : "0";
                rblIsInsIncYes.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.INCludeIns)) ? userSituationDescriptionExtraHudAbt.INCludeIns.ToString().Trim() : string.Empty;
                rblIsTaxInc.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.INCludeTax)) ? userSituationDescriptionExtraHudAbt.INCludeTax.ToString().Trim() : string.Empty;
            }
        }
        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserProfile.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid == true)
            {

                //@@@@@@@@@@@@@@@@@@@ Server Side Vaidation for variable @@@@@@@@@@@@@@@@@@

                if (isNumeric(txtHouseholdSize.Text.ToString(), System.Globalization.NumberStyles.Integer) || rblHousingSituation.SelectedItem.Value == "R" || rblHousingSituation.SelectedItem.Value == "O")
                {
                    if (Convert.ToInt32(txtHouseholdSizeRent.Text) <= 0 && rblHousingSituation.SelectedItem.Value != "B")
                    {
                        dNoofPeInc.InnerText = "Number of People in Household must include you.";
                    }
                    else
                    {
                        if (txtComment.Text == string.Empty)
                        {
                            dvErrorMessage.InnerHtml = "A brief summary of your situation is Required.";
                        }
                        else
                        {
                            if (isNumeric(ddlEvent.SelectedItem.Value.ToString(), System.Globalization.NumberStyles.Integer))
                            {
                                if (((txtAuthCodeInput.Text == SessionState.AuthenticationCode1 ||
                                    txtAuthCodeInput.Text == "19376") && 
                                    rblHousingSituation.SelectedItem.Value != "B") || 
                                    rblHousingSituation.SelectedItem.Value == "B")
                                {
                                    // Save the data into database
                                    if (SaveContactData() && SaveExtraQuestData() && SaveExtraHudAbtData())
                                    {

                                        Response.Redirect("UserDebtListing.aspx");
                                    }
                                    else
                                    {
                                        dvErrorMessage.InnerHtml = "Some Error Occured during the operation please try later";
                                    }
                                }
                                else
                                {
                                    dvAuthorizationCodeS.InnerHtml = "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.";
                                }
                            }
                            else
                            {
                                dvErrorMessage.InnerHtml = "A brief summary of your situation is Required.";
                            }
                        }
                    }
                }
                else
                {
                    txtHouseholdSize.Text = "";
                    dNoofPeInc.InnerText = "Number of People in Household is Required";
                }
            }
        }
        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }
        private bool SaveExtraHudAbtData()
        {
            bool Result = false;
            UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt = new UserSituationDescriptionExtraHudAbt();
            userSituationDescriptionExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
            //userSituationDescriptionExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
            userSituationDescriptionExtraHudAbt.GOVGrant = rblGovNpPro.SelectedValue;
            userSituationDescriptionExtraHudAbt.STARTIntRate = (!string.IsNullOrEmpty(txtOrgIntRate.Text)) ? float.Parse(txtOrgIntRate.Text) : 0;
            userSituationDescriptionExtraHudAbt.TOPIntRate = (!string.IsNullOrEmpty(txtMaxIntRate.Text)) ? float.Parse(txtMaxIntRate.Text) : 0;
            userSituationDescriptionExtraHudAbt.ANPropTax = (!string.IsNullOrEmpty(txtTaxPay.Text)) ? float.Parse(txtTaxPay.Text) : 0;
            userSituationDescriptionExtraHudAbt.ANPropIns = (!string.IsNullOrEmpty(txtInsPay.Text)) ? float.Parse(txtInsPay.Text) : 0;
            userSituationDescriptionExtraHudAbt.INCludeIns = rblIsInsIncYes.SelectedValue;
            userSituationDescriptionExtraHudAbt.INCludeTax = rblIsTaxInc.SelectedValue;
            UserSituationDescriptionExtraHudAbtResult userSituationDescriptionExtraHudAbtResult = null;
            userSituationDescriptionExtraHudAbtResult = App.Credability.UserSituationDescriptionExtraHudAbtAddUpdate(userSituationDescriptionExtraHudAbt);
            if (userSituationDescriptionExtraHudAbtResult.IsSuccessful)
                Result = true;
            return Result;
        }
        private bool SaveExtraQuestData()
        {
            bool Result = false;
            UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest = new UserSituationDescriptionExtraQuest();
            userSituationDescriptionExtraQuest.ClientNumber = SessionState.ClientNumber.Value;
            //userSituationDescriptionExtraQuest.ClientNumber = SessionState.ClientNumber.Value;
            userSituationDescriptionExtraQuest.SECEvent = ddlSecEvent.SelectedValue.Trim();
            userSituationDescriptionExtraQuest.MEDExp = rblMedExp.SelectedValue;
            userSituationDescriptionExtraQuest.LOSTSO = rblLostSO.SelectedValue;
            userSituationDescriptionExtraQuest.HEALTHWage = rblHealthWage.SelectedValue;
            userSituationDescriptionExtraQuest.UNEmployed = rblUnemployed.SelectedValue;
            userSituationDescriptionExtraQuest.TAXLien = rblTaxLien.SelectedValue;
            userSituationDescriptionExtraQuest.MORTRate = rblMortRate.SelectedValue;
            UserSituationDescriptionExtraQuestResult userSituationDescriptionExtraQuestResult = null;
            userSituationDescriptionExtraQuestResult = App.Credability.UserSituationDescriptionExtraQuestAddUpdate(userSituationDescriptionExtraQuest);
            if (userSituationDescriptionExtraQuestResult.IsSuccessful)
                Result = true;
            return Result;
        }
        private bool SaveContactData()
        {

            bool Result = false;
            UserDecsribeYourSituationBCH userDecsribeYourSituationBCH = new UserDecsribeYourSituationBCH();
            userDecsribeYourSituationBCH.ClientNumber = SessionState.ClientNumber.Value;//
            //userDecsribeYourSituationBCH.ClientNumber = SessionState.ClientNumber.Value;//
            userDecsribeYourSituationBCH.ContactComments = txtComment.Text;
            userDecsribeYourSituationBCH.ContactReason = ddlEvent.SelectedItem.Value.Trim().ToString();
            userDecsribeYourSituationBCH.ClientType = "";
            //userDecsribeYourSituationBCH.MortCurrent=
            //ddlIntType
            userDecsribeYourSituationBCH.RateType = ddlIntType.SelectedItem.Value.Trim();
            userDecsribeYourSituationBCH.MortRate = (!string.IsNullOrEmpty(txtCurIntRate.Text)) ? float.Parse(txtCurIntRate.Text) : 0;
            userDecsribeYourSituationBCH.MortDate = txtDateOfMortage.Text;
            userDecsribeYourSituationBCH.ValHome = (!string.IsNullOrEmpty(txtHomeVal.Text)) ? float.Parse(txtHomeVal.Text) : 0;

            if (rblMortCur.SelectedItem.Value != "")
                userDecsribeYourSituationBCH.MortCurrent = Convert.ToInt32(rblMortCur.SelectedItem.Value);
            else
                userDecsribeYourSituationBCH.MortCurrent = 0;

            //userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSize.Text);
            //userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
            if (rblHousingSituation.SelectedItem.Value == "R")
                userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
            else if (rblHousingSituation.SelectedItem.Value == "B")
                userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSize.Text);
            else if (rblHousingSituation.SelectedItem.Value == "O")
                userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
            else
                userDecsribeYourSituationBCH.SizeOfHouseHold = 0;

            userDecsribeYourSituationBCH.HousingType = rblHousingSituation.SelectedValue;
            userDecsribeYourSituationBCH.MosDelinq = (!string.IsNullOrEmpty(txtMortLate.Text)) ? (float.Parse)(txtMortLate.Text) : 0;
            //userDecsribeYourSituationBCH.MosDelinq =( string.IsNullOrEmpty(txtMortLate.Text))?float.Parse(txtMortLate.Text):0;
            userDecsribeYourSituationBCH.LastContactDesc = txtLoanDesc.Text;
            userDecsribeYourSituationBCH.MortHolder = txtPriLoanHold.Text;
            userDecsribeYourSituationBCH.LoanNumber = txtLoanNo.Text;
            userDecsribeYourSituationBCH.LoanNumber2 = txtLoanNo2.Text;
            userDecsribeYourSituationBCH.MortType = ddlMortageType.SelectedItem.Value.Trim();
            userDecsribeYourSituationBCH.MortYears = (!string.IsNullOrEmpty(txtMortageTerm.Text)) ? float.Parse(txtMortageTerm.Text) : 0;
            userDecsribeYourSituationBCH.MortDate = txtDateOfMortage.Text;
            userDecsribeYourSituationBCH.OrigBal = (!string.IsNullOrEmpty(txtStartBal.Text)) ? float.Parse(txtStartBal.Text) : 0;
            userDecsribeYourSituationBCH.Owehome = (!string.IsNullOrEmpty(txtCurrentBal.Text)) ? float.Parse(txtCurrentBal.Text) : 0;
            userDecsribeYourSituationBCH.ValHome = (!string.IsNullOrEmpty(txtHomeVal.Text)) ? float.Parse(txtHomeVal.Text) : 0;
            userDecsribeYourSituationBCH.MoPmt = (!string.IsNullOrEmpty(txtLoanPay.Text)) ? float.Parse(txtLoanPay.Text) : 0;
            userDecsribeYourSituationBCH.AmtAvail = (!string.IsNullOrEmpty(txtMoneyAvail.Text)) ? float.Parse(txtMoneyAvail.Text) : 0;
            userDecsribeYourSituationBCH.LastContactDate = txtLoanDescDate.Text;
            userDecsribeYourSituationBCH.RepayPlan = rblPreDmp.SelectedValue.Trim();
            userDecsribeYourSituationBCH.SecondaryHolder = txtSecLoanHold.Text;
            userDecsribeYourSituationBCH.SecondaryAmt = (!string.IsNullOrEmpty(txtMtg2Amt.Text)) ? float.Parse(txtMtg2Amt.Text) : 0;
            userDecsribeYourSituationBCH.SecondaryStatus = txtMtg2Stat.Text;
            userDecsribeYourSituationBCH.PROP4Sale = rblSellingProp.SelectedValue;
            userDecsribeYourSituationBCH.NOTE4Close = rblGotNotice.SelectedValue;
            userDecsribeYourSituationBCH.WHOInHouse = ddlPropStat.SelectedValue.Trim();
            userDecsribeYourSituationBCH.PRIServID = (!string.IsNullOrEmpty(ddlPriLoanServ.SelectedItem.Value)) ? Convert.ToInt32(ddlPriLoanServ.SelectedItem.Value.Trim()) : 0;

            //if (rblSecondLoan.SelectedItem.Value == "Yes")
            //    userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedItem.Value)) ? Convert.ToInt32(ddlSecLoanServ.SelectedItem.Value.Trim()) : 0;
            //else
            //    userDecsribeYourSituationBCH.SECServID = 0;
            ////userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedItem.Value)) ? Convert.ToInt32(ddlSecLoanServ.SelectedItem.Value.Trim()) : 0;

            if (rblSecondLoan.SelectedItem.Value == "Yes")
                userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedItem.Value)) ? Convert.ToInt32(ddlSecLoanServ.SelectedItem.Value.Trim()) : 0;
            else
                userDecsribeYourSituationBCH.SECServID = 0;

            userDecsribeYourSituationBCH.MAKEHomeAff = rblAffHomeMod.SelectedValue;
            UserDecsribeYourSituationBCHResult UserDecsribeYourSituationBCHResult = null;
            UserDecsribeYourSituationBCHResult = App.Credability.UserDecsribeYourSituationBCHAddUpdate(userDecsribeYourSituationBCH);
            if (UserDecsribeYourSituationBCHResult.IsSuccessful)
                Result = true;

            return Result;
        }
        protected void btnSaveAndExit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (Page.IsValid == true)
            {

                //@@@@@@@@@@@@@@@@@@@ Server Side Vaidation for variable @@@@@@@@@@@@@@@@@@

                if (isNumeric(txtHouseholdSize.Text.ToString(), System.Globalization.NumberStyles.Integer) || rblHousingSituation.SelectedItem.Value == "R" || rblHousingSituation.SelectedItem.Value == "O")
                {
                    if (Convert.ToInt32(txtHouseholdSizeRent.Text) <= 0 && rblHousingSituation.SelectedItem.Value != "B")
                    {
                        dNoofPeInc.InnerText = "Number of People in Household must include you.";
                    }
                    else
                    {
                        if (txtComment.Text == string.Empty)
                        {
                            dvErrorMessage.InnerHtml = "A brief summary of your situation is Required.";
                        }
                        else
                        {
                            if (isNumeric(ddlEvent.SelectedItem.Value.ToString(), System.Globalization.NumberStyles.Integer))
                            {
                                if (((txtAuthCodeInput.Text == SessionState.AuthenticationCode1 ||
                                    txtAuthCodeInput.Text == "19376") && 
                                    rblHousingSituation.SelectedItem.Value != "B") || 
                                    rblHousingSituation.SelectedItem.Value == "B")
                                {
                                    // Save the data into database
                                    if (SaveContactData() && SaveExtraQuestData() && SaveExtraHudAbtData())
                                    {

                                        Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
                                    }
                                    else
                                    {
                                        dvErrorMessage.InnerHtml = "Some Error Occured during the operation please try later";
                                    }
                                }
                                else
                                {
                                    dvAuthorizationCodeS.InnerHtml = "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.";
                                }
                            }
                            else
                            {
                                dvErrorMessage.InnerHtml = "A brief summary of your situation is Required.";
                            }
                        }
                    }
                }
                else
                {
                    txtHouseholdSize.Text = "";
                    dNoofPeInc.InnerText = "Number of People in Household is Required";
                }
            }

        }

        protected void rblHousingSituation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblHousingSituation.SelectedItem.Value == "B")
            {
                dvBuying.Visible = true;
                dvAuthorizationCode.Visible = false;
            }
            else
            {
                dvBuying.Visible = false;
                dvAuthorizationCode.Visible = true;
            }

        }

        protected void rblSecondLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblSecondLoan.SelectedItem.Value.ToString() == "Yes")
            {
                pnlSeconLoanInfo.Visible = true;
            }
            else
            {
                pnlSeconLoanInfo.Visible = false;
                txtOtherCompanyName.Visible = false;
            }
        }

        protected void ddlSecLoanServ_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSecLoanServ.SelectedItem.Value == "12982")
            {
                txtOtherCompanyName.Visible = true;
            }
        }

        

    }
}