﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCFinancialSituationRecap : System.Web.UI.UserControl
    {

        double TotDebt = 0.0;
        double TotPay = 0.0;
        double TotalIncome = 0.0;
       double TotalExp = 0.0;
        //decimal TotalExp = 0.0;
        double TotDis = 0.0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionState.ClientNumber != null)
            {
                UserAnalyzingYourFinanicalSituationHousingOnly AnalyzedResult = null;
                AnalyzedResult = App.Credability.UserAnalyzingYourFinanicalSituationHousingOnlyGet(SessionState.ClientNumber.Value);
                TotDebt = App.Credability.CreditorBalanceGet(SessionState.ClientNumber.Value);
                TotPay = App.Credability.CreditorPaymentsGet(SessionState.ClientNumber.Value);
                TotalIncome += AnalyzedResult.MonthlyNetIncome + AnalyzedResult.MonthlyNetIncome + AnalyzedResult.MonthlyParttimeIncome + AnalyzedResult.MonthlyParttimeIncome2;
                TotalIncome += AnalyzedResult.MonthlyAlimonyIncome + AnalyzedResult.MonthlyAlimonyIncome2 + AnalyzedResult.MonthlyChildsupportIncome + AnalyzedResult.MonthlyChildsupportIncome2;
                TotalIncome += AnalyzedResult.MonthlyGovtassistIncome + AnalyzedResult.MonthlyGovtassistIncome2 + AnalyzedResult.MonthlyOtherIncome + AnalyzedResult.MonthlyOtherIncome2;

                // Totlal Expenses
               //ravi
                TotalExp += Convert.ToDouble(AnalyzedResult.Groceries+AnalyzedResult.RentMort + AnalyzedResult.EquityLoanTaxIns + AnalyzedResult.HomeMaintenance + AnalyzedResult.Utilities + AnalyzedResult.Telephone + AnalyzedResult.FoodAway);
                TotalExp += Convert.ToDouble(AnalyzedResult.VehiclePayments + AnalyzedResult.CarInsurance + AnalyzedResult.CarMaintenance + AnalyzedResult.PublicTransportation );
                TotalExp += Convert.ToDouble(AnalyzedResult.MedicalPrescription + AnalyzedResult.ChildSupport + AnalyzedResult.ChildElderCare + AnalyzedResult.Education + AnalyzedResult.Insurance);
                TotalExp += Convert.ToDouble(AnalyzedResult.Contributions + AnalyzedResult.Clothing + AnalyzedResult.Personalexp + AnalyzedResult.Laundry + AnalyzedResult.BeautyBarber + AnalyzedResult.Recreation + AnalyzedResult.ClubDues + AnalyzedResult.Gifts + AnalyzedResult.MiscExp);
              

               //original
                /*TotalExp += Convert.ToDouble(AnalyzedResult.RentMort + AnalyzedResult.EquityLoanTaxIns + AnalyzedResult.HomeMaintenance + AnalyzedResult.Utilities + AnalyzedResult.Telephone + AnalyzedResult.FoodAway);
                TotalExp += Convert.ToDouble(AnalyzedResult.Groceries + AnalyzedResult.VehiclePayments + AnalyzedResult.CarInsurance + AnalyzedResult.CarMaintenance + AnalyzedResult.PublicTransportation + AnalyzedResult.Insurance);
                TotalExp += Convert.ToDouble(AnalyzedResult.MedicalPrescription + AnalyzedResult.ChildSupport + AnalyzedResult.ChildElderCare + AnalyzedResult.Education + AnalyzedResult.Contributions);
                TotalExp += Convert.ToDouble(AnalyzedResult.Clothing + AnalyzedResult.Laundry + AnalyzedResult.BeautyBarber + AnalyzedResult.Recreation + AnalyzedResult.ClubDues + AnalyzedResult.Gifts + AnalyzedResult.MiscExp);
              */
                //one field is missing 'personal_exp'

                TotDis = TotalIncome - TotalExp - TotPay;

                TotalUnsecuredDebt.Text = "$ " + String.Format("{0:0,0.0}", TotDebt);
                lblTotalPayments.Text = "$ " + String.Format("{0:0,0.0}", TotPay);
                lblTotalPayments.Text = "$ " + String.Format("{0:0,0.0}", TotPay);
                lblTotalMonthlyIncome.Text = "$ " + String.Format("{0:0,0.0}", TotalIncome);
                lblTotalMonthlyExp.Text = "$ " + String.Format("{0:0,0.0}", TotalExp);
                lblDiscretionaryIncome.Text = "$ " + String.Format("{0:0,0.0}", TotDis);
            }
        }
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (txtAuthCodeInput.Text == SessionState.AuthenticationCode2 || 
                txtAuthCodeInput.Text == "54628")
            {
                Response.Redirect("UnderstandingYourNetWorth.aspx");
            }
            else
            {
                dvErrorMessage.InnerHtml = "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.";
            }

        }

        protected void btnPreviousPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("MonthlyExpenses.aspx");
        }

        protected void btnSaveExit_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
        }
        

    }
}