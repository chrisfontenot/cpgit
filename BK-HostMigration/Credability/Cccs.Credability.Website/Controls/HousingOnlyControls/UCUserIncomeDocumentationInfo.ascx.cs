﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCUserIncomeDocumentationInfo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                UserIncomeDocumentationInfoHousingOnly UserIncomeDocumentationInfoHousingOnly = null;
                UserIncomeDocumentationInfoHousingOnly = App.Credability.UserIncomeDocumentationInfoHousingOnlyGet(SessionState.ClientNumber.Value);
                if (UserIncomeDocumentationInfoHousingOnly != null)
                {
                    txtPriIncGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyGrossIncome.ToString();
                    txtSecIncGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyGrossIncome2.ToString();

                    txtPriIncPtGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyParttimeIncome.ToString();
                    txtSecIncPtGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyParttimeIncome2.ToString();

                    txtPriIncAlimonyGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome.ToString();
                    txtSecIncAlimonyGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome2.ToString();

                    txtPriIncChildSupportGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome.ToString();
                    txtSecIncChildSupportGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome2.ToString();

                    txtPriIncGovAstGrosstxt.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome.ToString();
                    txtSecIncGovAstGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome2.ToString();

                    txtPriIncOtherGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome.ToString();
                    txtSecIncOtherGross.Text = "$"+ UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome2.ToString();

                    Double TotalGrossIncomeFirst = UserIncomeDocumentationInfoHousingOnly.MonthlyGrossIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyParttimeIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome;
                    TotalGrossIncomeFirst += UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome;
                    dvTotalMonthlyGrossIncomePrimary.InnerHtml = "$ " + String.Format("{0:0,0.0}", TotalGrossIncomeFirst.ToString());

                    Double TotalGrossIncomeSec = UserIncomeDocumentationInfoHousingOnly.MonthlyGrossIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyParttimeIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome2;
                    TotalGrossIncomeSec += UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome2;
                    dvTotalMonthlyGrossIncomeSec.InnerHtml = "$ " + String.Format("{0:0,0.0}", TotalGrossIncomeSec.ToString());

                    //
                    txtPriIncNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyNetIncome.ToString();
                    txtSecIncNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyNetIncome2.ToString();
                    //


                    txtPriIncPtNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyPartNet.ToString();
                    txtSecIncPtNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyPartNet2.ToString();

                    txtPriIncAlimonyNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome.ToString();
                    txtSecIncAlimonyNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome2.ToString();

                    txtPriIncChildSupportNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome.ToString();
                    txtSecIncChildSupportNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome2.ToString();

                    txtPriIncGovAstNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome.ToString();
                    txtSecIncGovAstNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome2.ToString();

                    txtPriIncOtherNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome.ToString();
                    txtSecIncOtherNet.Text = "$" + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome2.ToString();

                    Double TotalNetHouseholdIncomePrimary = UserIncomeDocumentationInfoHousingOnly.MonthlyNetIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyPartNet + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome;
                    TotalNetHouseholdIncomePrimary += UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome;
                    dvNetHouseholdIncomePrimary.InnerHtml = "$ " + String.Format("{0:0,0.0}", TotalNetHouseholdIncomePrimary.ToString());

                    Double TotalNetHouseholdIncomeSec = UserIncomeDocumentationInfoHousingOnly.MonthlyGovtassistIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyPartNet2 + UserIncomeDocumentationInfoHousingOnly.MonthlyAlimonyIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyChildsupportIncome2;
                    TotalNetHouseholdIncomeSec += UserIncomeDocumentationInfoHousingOnly.MonthlyNetIncome2 + UserIncomeDocumentationInfoHousingOnly.MonthlyOtherIncome2;
                    dvNetHouseholdIncomeSec.InnerHtml = "$ " + String.Format("{0:0,0.0}", TotalNetHouseholdIncomeSec.ToString());

                }
            }

        }
        protected void imgBtnDocumentingPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserDebtListing.aspx");
        }

        protected void imgBtnDocumentingContinue_Click(object sender, EventArgs e)
        {
            UserIncomeDocumentationInfoHousingOnlyResult UserIncomeDocumentationInfoHousingOnlyResult = null;
            UserIncomeDocumentationInfoHousingOnly UserIncomeDocumentationInfoHousingOnlys = new UserIncomeDocumentationInfoHousingOnly();
            UserIncomeDocumentationInfoHousingOnlys.ClientNumber = SessionState.ClientNumber.Value;
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGrossIncome = float.Parse(txtPriIncGross.Text.Trim().Replace("$",""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyNetIncome = float.Parse(txtPriIncNet.Text.Trim().Replace("$",""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyAlimonyIncome = float.Parse(txtPriIncAlimonyGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyChildsupportIncome = float.Parse(txtPriIncChildSupportGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGovtassistIncome = float.Parse(txtPriIncGovAstGrosstxt.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyOtherIncome = float.Parse(txtPriIncOtherGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyParttimeIncome = float.Parse(txtPriIncPtGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGrossIncome2 = float.Parse(txtSecIncGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyNetIncome2 = float.Parse(txtSecIncNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyOtherIncome2 = float.Parse(txtSecIncOtherGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyParttimeIncome2 = float.Parse(txtSecIncPtGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyAlimonyIncome2 = float.Parse(txtSecIncAlimonyGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyChildsupportIncome2 = float.Parse(txtSecIncChildSupportGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGovtassistIncome2 = float.Parse(txtSecIncGovAstGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyPartNet = int.Parse(txtPriIncPtNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyPartNet2 = int.Parse(txtSecIncPtNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.ModForm3 = 1;

            UserIncomeDocumentationInfoHousingOnlyResult = App.Credability.UserIncomeDocumentationInfoHousingOnlyUpdate(UserIncomeDocumentationInfoHousingOnlys);
            if (UserIncomeDocumentationInfoHousingOnlyResult.IsSuccessful)
            {
                Response.Redirect("MonthlyExpenses.aspx");
            }
            else
            {
                //Show the messsage here 
            }


        }


        protected void imgBtnDocumentingReturnToPrevious_Click(object sender, EventArgs e)
        {

            UserIncomeDocumentationInfoHousingOnlyResult UserIncomeDocumentationInfoHousingOnlyResult = null;
            UserIncomeDocumentationInfoHousingOnly UserIncomeDocumentationInfoHousingOnlys = new UserIncomeDocumentationInfoHousingOnly();
            UserIncomeDocumentationInfoHousingOnlys.ClientNumber = SessionState.ClientNumber.Value;
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGrossIncome = float.Parse(txtPriIncGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyNetIncome = float.Parse(txtPriIncNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyAlimonyIncome = float.Parse(txtPriIncAlimonyGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyChildsupportIncome = float.Parse(txtPriIncChildSupportGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGovtassistIncome = float.Parse(txtPriIncGovAstGrosstxt.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyOtherIncome = float.Parse(txtPriIncOtherGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyParttimeIncome = float.Parse(txtPriIncPtGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGrossIncome2 = float.Parse(txtSecIncGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyNetIncome2 = float.Parse(txtSecIncNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyOtherIncome2 = float.Parse(txtSecIncOtherGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyParttimeIncome2 = float.Parse(txtSecIncPtGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyAlimonyIncome2 = float.Parse(txtSecIncAlimonyGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyChildsupportIncome2 = float.Parse(txtSecIncChildSupportGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyGovtassistIncome2 = float.Parse(txtSecIncGovAstGross.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyPartNet = int.Parse(txtPriIncPtNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.MonthlyPartNet2 = int.Parse(txtSecIncPtNet.Text.Trim().Replace("$", ""));
            UserIncomeDocumentationInfoHousingOnlys.ModForm3 = 1;

            UserIncomeDocumentationInfoHousingOnlyResult = App.Credability.UserIncomeDocumentationInfoHousingOnlyUpdate(UserIncomeDocumentationInfoHousingOnlys);
            if (UserIncomeDocumentationInfoHousingOnlyResult.IsSuccessful)
            {
                Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
            }
            else
            {
                //Show the messsage here 
            }


        }


    }
}