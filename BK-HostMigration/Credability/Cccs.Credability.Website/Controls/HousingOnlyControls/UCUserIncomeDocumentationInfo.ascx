﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCUserIncomeDocumentationInfo.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCUserIncomeDocumentationInfo" %>
<p><h1>
  <%= Cccs.Credability.Website.App.Translate("Credability|UserIncomeDocumentationInfoBCH|DYN")%></h1></p>
  
  
  <%= Cccs.Credability.Website.App.Translate("Credability|UserIncomeDocumentationInfoBCH|PCon")%> 
  
 
  <script language="JavaScript1.2">
var ReFormat = true, HoldField = "";
function SetUpCalc(Field)
{
	var sValue = Field.value;
	sValue = Number(stripCharsNotInBag (sValue, "0123456789."));
	if (sValue <= 0)
		sValue = '0.00';
	Field.value = sValue;
	ReFormat = false;
	Field.focus()
}
var ReFormat = true, HoldField = "";
function prepareField(theField)
{
	var sValue = theField.value;
	HoldField = sValue;
	sValue = Number(stripCharsNotInBag (sValue, "0123456789."));
	if (sValue <= 0)
		sValue = '';
	theField.value = sValue;
	theField.select();
}
function CheckMod(theField)
{
	if (HoldField != theField.value && HoldField != "")
	{
		//document.getElementById("ModForm").value = 1;
		HoldField = "";
	}
}
function reformatCCCSCurrency(theField)
{
	if(ReFormat)
	{
		var sValue = theField.value,OutTxt = "", Des;
		var NumStr, Len, i;
		sValue = stripCharsNotInBag (sValue, "0123456789.");
		if (sValue == 0) sValue = '0';
		NumStr = sValue - (sValue % 1);
		NumStr = String(NumStr);
		Len = NumStr.length - 1;
		for (i = 0; i <= Len; i++) 
		{
			OutTxt += NumStr.charAt(i);
			if (i < Len && (Len - i) % 3 == 0) //>
			{
				OutTxt += ",";
			}
		}
		sValue = '$' + OutTxt;
		theField.value = sValue;
		CheckMod(theField);
	}
	ReFormat = true;
}
function CalcMonthlyTotals()
{
      
	document.getElementById('<%=dvTotalMonthlyGrossIncomePrimary.ClientID %>').style.display='none';
	document.getElementById('PriIncTotalGross').value = Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGross.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncPtGross.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncAlimonyGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncChildSupportGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGovAstGrosstxt.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncOtherGross.ClientID %>').value, "0123456789."))
	document.getElementById('PriIncTotalGross').style.display='';
	reformatCCCSCurrency(document.getElementById('PriIncTotalGross'));
	
	<!--Second Person-->
	document.getElementById('<%=dvTotalMonthlyGrossIncomeSec.ClientID %>').style.display='none';
	document.getElementById('SecIncTotalGross').value = Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGross.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncPtGross.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncAlimonyGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncChildSupportGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGovAstGross.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncOtherGross.ClientID %>').value, "0123456789."))
	document.getElementById('SecIncTotalGross').style.display='';
	reformatCCCSCurrency(document.getElementById('SecIncTotalGross'));
	
	<!--Net Household Income -->
	
	document.getElementById('<%=dvNetHouseholdIncomePrimary.ClientID %>').style.display='none';
	document.getElementById('PriIncTotalNet').value = Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncPtNet.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncAlimonyNet.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncChildSupportNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncGovAstNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncOtherNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtPriIncNet.ClientID %>').value, "0123456789."))
	document.getElementById('PriIncTotalNet').style.display='';
	reformatCCCSCurrency(document.getElementById('PriIncTotalNet'));
	
	<!--Secondry Net Household Income -->
	
	document.getElementById('<%=dvNetHouseholdIncomeSec.ClientID %>').style.display='none';
	document.getElementById('SecIncTotalNet').value = Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncNet.ClientID %>').value, "0123456789.")) + Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncPtNet.ClientID %>').value, "0123456789.")) +  Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncAlimonyNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncChildSupportNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncGovAstNet.ClientID %>').value, "0123456789."))+ Number(stripCharsNotInBag (document.getElementById('<%=txtSecIncOtherNet.ClientID %>').value, "0123456789."))
	document.getElementById('SecIncTotalNet').style.display='';
	reformatCCCSCurrency(document.getElementById('SecIncTotalNet'));
	
}
function validateForm()
{
	var bReturn = true;
	var total_Gross = Number(stripCharsNotInBag (document.getElementById('PriIncTotalGross').value, "0123456789."));
	var total_Gross_Sec= Number(stripCharsNotInBag (document.getElementById('SecIncTotalGross').value, "0123456789."));
	var total_Net = Number(stripCharsNotInBag (document.getElementById('PriIncTotalNet').value, "0123456789."));
	var total_Net_Sec = Number(stripCharsNotInBag (document.getElementById('SecIncTotalNet').value, "0123456789."));
	if (total_Gross < 1.15 * total_Net)
	{
		bReturn = confirm("Your gross income should include the total of all monthly income, including your paycheck before taxes, child support, etc. Your monthly gross income will be greater than your monthly net income.  Do you wish to continue?");
	}
	if (total_Gross_Sec != 0)
	{
		if (total_Gross_Sec < 1.15 * total_Net_Sec) 
		{
			bReturn = confirm("Your gross income should include the total of all monthly income, including your paycheck before taxes, child support, etc. Your monthly gross income will be greater than your monthly net income.  Do you wish to continue?");
		}
	}
	if ((total_Gross >= 10000) || (total_Net >= 8000) || (total_Gross_Sec >= 10000) || (total_Net_Sec >= 8000))
	{
		bReturn = confirm("The income you entered is relatively high.  Do you wish to continue?");
	}
	if ((total_Gross == 0) && (total_Net == 0) && (total_Gross_Sec == 0) && (total_Net_Sec == 0))
	{
		bReturn = confirm("Both Gross and Net income that you entered is 0.  Do you wish to continue?");
	}
	else
	{
		if (total_Gross == 0)
		{
			bReturn = confirm("The Gross income you entered is 0.  Do you wish to continue?");
		}
		if (total_Net == 0)
		{
			bReturn = confirm("The Net income you entered is 0.  Do you wish to continue?");
		}
	}
	return bReturn;
}
function syncFields(Field)
{
	var sValue=Field.value;
	var sName = Field.name;
	var myRegExp = /Gross/;
	var matchPos1 = sName.search(myRegExp);
//check whether it's a 'Gross' field or a "Net' field
	if(matchPos1 != -1)
	{
		var snewName=sName.replace("Gross", "Net");
		
	}
	else
	{
		var snewName=sName.replace("Net", "Gross");
		
	}
	
	document.getElementById(snewName.replaceAll("$", "_")).value=sValue;
	reformatCCCSCurrency(document.getElementById(snewName.replaceAll("$", "_")));
}
// Replaces all instances of the given substring.
String.prototype.replaceAll = function( 
	strTarget, // The substring you want to replace
	strSubString // The string you want to replace in.
	){
	var strText = this;
	var intIndexOfMatch = strText.indexOf( strTarget );
	 
	// Keep looping while an instance of the target string
	// still exists in the string.
	while (intIndexOfMatch != -1){
		// Relace out the current instance.
		strText = strText.replace( strTarget, strSubString )
		 
		// Get the index of any next matching substring.
		intIndexOfMatch = strText.indexOf( strTarget );
	}
	 
	// Return the updated string with ALL the target strings
	// replaced out with the new substring.
	return( strText );
}
</script>

   <script src="../../Content/calculator.js" type="text/javascript" language="javascript"></script>
<p>&nbsp;</p>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GHI")%></h1>
 <div class="dvform2col dvformlblbig">
        <div class="colformlft">
            <div class="dvform">
                <div class="dvrow">
                <label>&nbsp;</label>
                <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|PP")%></p>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGI")%></label>                
            <asp:TextBox ID="txtPriIncGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGross.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGross.ClientID %>']);">
             <asp:Image ID="imgCal1" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" CssClass="error" ControlToValidate="txtPriIncGross" Operator="DataTypeCheck"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtPriIncGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGIF")%></label>
                <asp:TextBox ID="txtPriIncPtGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncPtGross.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncPtGross.ClientID %>'])">
                <%--<img width="15" height="13" border="0" alt="Click here to see the calculator" src="img/calc.gif">--%><asp:Image
                    ID="imgCal2" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
            <asp:CompareValidator SetFocusOnError="true" CssClass="error" ControlToValidate="txtPriIncPtGross"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncPtGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
                <asp:TextBox ID="txtPriIncAlimonyGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncAlimonyGross.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncAlimonyGross.ClientID %>'])">
                <asp:Image ID="imgCal3" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncAlimonyGross" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncAlimonyGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
                <asp:TextBox ID="txtPriIncChildSupportGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncChildSupportGross.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncChildSupportGross.ClientID %>'])">
                <asp:Image ID="imgCal4" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a><br />
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncChildSupportGross"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double" CssClass="error"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncChildSupportGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
                <asp:TextBox ID="txtPriIncGovAstGrosstxt" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGovAstGrosstxt.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGovAstGrosstxt.ClientID %>'])">
                <asp:Image ID="imgCal5" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncGovAstGrosstxt"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double" CssClass="error"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncGovAstGrosstxt"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
                 <asp:TextBox ID="txtPriIncOtherGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" onChange="syncFields(this)" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncOtherGross.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncOtherGross.ClientID %>'])">
                <asp:Image ID="imgCal6" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
           
            
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncOtherGross" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncOtherGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
               <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMGI")%></label>
                <input name="PriIncTotalGross" type="text" disabled="true" id="PriIncTotalGross"
                style="border: none; background-color: #e0e0e0; display: none;" size="14">
                <div id="dvTotalMonthlyGrossIncomePrimary" runat="server" style=" font-weight: bold;line-height:25px; font-size:14px; font-weight:bold">
            </div>
            <span id="test"></span>
                </div>
                </div>
                </div>
        <div class="colformrht">
            <div class="dvform">
                <div class="dvrow">
                <label>&nbsp;</label>
                <p class="col_title"> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|SP")%></p>
                </div>
                <div class="dvrow">
                 <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGI")%></label>                
            <asp:TextBox ID="txtSecIncGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true"  CssClass="error" ControlToValidate="txtSecIncGross" Operator="DataTypeCheck"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtSecIncGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MGIF")%></label>
                <asp:TextBox ID="txtSecIncPtGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" CssClass="error" ControlToValidate="txtSecIncPtGross"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncPtGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>
                <asp:TextBox ID="txtSecIncAlimonyGross" runat="server" MaxLength="7" onChange="syncFields(this)"
                onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
           
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncAlimonyGross"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double" CssClass="error"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncAlimonyGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>
                 <asp:TextBox ID="txtSecIncChildSupportGross" runat="server" MaxLength="7" onChange="syncFields(this)"
                onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
           
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncChildSupportGross"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double" CssClass="error"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncChildSupportGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                 <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>
                 <asp:TextBox ID="txtSecIncGovAstGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncGovAstGross" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncGovAstGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>
                <asp:TextBox ID="txtSecIncOtherGross" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
          
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncOtherGross" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncOtherGross"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMGI")%></label>
                <input name="SecIncTotalGross" type="text" disabled="true" id="SecIncTotalGross"
                style="border: none; background-color: #e0e0e0; display: none;" size="14" />
            <div id="dvTotalMonthlyGrossIncomeSec" runat="server" style="font-size:14px; font-weight:bold; font-weight: bold; line-height:25px">
            </div>
                </div>
                </div>
                </div>
                 <div class="clearboth">
        </div>
                </div>


<h1> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|NHI")%></h1>
<!-- Begin net income add form -->

<div class="dvform2col dvformlblbig">
        <div class="colformlft">
            <div class="dvform">
                <div class="dvrow">
                <label>&nbsp;</label>
                <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|PP")%></p>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFP")%></label>                
            <asp:TextBox ID="txtPriIncNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncNet.ClientID %>'])">
                <asp:Image ID="imgCal7" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncNet" Operator="DataTypeCheck" CssClass="error"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtPriIncNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFS")%></label>                
           <asp:TextBox ID="txtPriIncPtNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncPtNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncPtNet.ClientID %>'])">
                <asp:Image ID="imgCal8" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncPtNet" Operator="DataTypeCheck" CssClass="error"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtPriIncPtNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                 <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>              
            <asp:TextBox ID="txtPriIncAlimonyNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncAlimonyNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncAlimonyNet.ClientID %>'])">
                <asp:Image ID="imgCal9" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncAlimonyNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncAlimonyNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>                
           <asp:TextBox ID="txtPriIncChildSupportNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncChildSupportNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncChildSupportNet.ClientID %>'])">
                <asp:Image ID="imgCal10" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncChildSupportNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncChildSupportNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                 <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>               
           <asp:TextBox ID="txtPriIncGovAstNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncGovAstNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncGovAstNet.ClientID %>'])">
                <asp:Image ID="imgCal11" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncGovAstNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncGovAstNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                 <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>                  
           <asp:TextBox ID="txtPriIncOtherNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <a href="javascript:TCR.TCRPopup(document.forms[0].elements['<%=txtPriIncOtherNet.ClientID %>'])"
                onclick="SetUpCalc(document.forms[0].elements['<%=txtPriIncOtherNet.ClientID %>'])">
                <asp:Image ID="imgCal12" runat="server" ImageUrl="~/Images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtPriIncOtherNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtPriIncOtherNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMNI")%></label>                
           <input name="PriIncTotalNet" type="text" disabled="true" id="PriIncTotalNet" style="border: none;
                background-color: #e0e0e0; display: none;" size="14" />
            <div id="dvNetHouseholdIncomePrimary" runat="server" style="font-size:14px; font-weight:bold; font-weight: bold; line-height:25px">
            </div>
                </div>
                </div>
                </div>
        <div class="colformrht">
            <div class="dvform">
                <div class="dvrow">
                <label>&nbsp;</label>
                <p class="col_title"> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|SP")%></p>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFP")%></label>                
           <asp:TextBox ID="txtSecIncNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncNet" Operator="DataTypeCheck"  CssClass="error"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtSecIncNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|MNIFS")%></label>                
           <asp:TextBox ID="txtSecIncPtNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncPtNet" Operator="DataTypeCheck" CssClass="error"
                Display="Dynamic" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cvtxtSecIncPtNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|AR")%></label>                
           <asp:TextBox ID="txtSecIncAlimonyNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncAlimonyNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncAlimonyNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                 <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|CSR")%></label>               
           <asp:TextBox ID="txtSecIncChildSupportNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncChildSupportNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncChildSupportNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|GAR")%></label>                
           <asp:TextBox ID="txtSecIncGovAstNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncGovAstNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncGovAstNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label> <%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|OI")%></label>                
           <asp:TextBox ID="txtSecIncOtherNet" runat="server" MaxLength="7" onBlur="reformatCCCSCurrency (this);CalcMonthlyTotals();"
                onChange="syncFields(this)" onFocus="prepareField(this);" CssClass="txtBox"></asp:TextBox>
            <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtSecIncOtherNet" CssClass="error"
                Operator="DataTypeCheck" Display="Dynamic" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtSecIncOtherNet"></asp:CompareValidator>
                </div>
                <div class="dvrow">
                <label><%= Cccs.Credability.Website.App.Translate("Credability|IncomeDocumentationInfoRVM|TMNI")%></label>                
           <input name="SecIncTotalNet" type="text" disabled="true" id="SecIncTotalNet" style="border: none;
                background-color: #e0e0e0; display: none;" size="14" />
            <div id="dvNetHouseholdIncomeSec" runat="server" style="font-size:14px; font-weight:bold; font-weight: bold; line-height:25px">
            </div>
                </div>
                </div>
                </div>
                 <div class="clearboth"> </div>
                </div>

<div class="dvbtncontainer">
<div class="lnkbutton">
           <asp:LinkButton ID="imgBtnDocumentingPrevious" runat="server" CssClass="previous"
                 onclick="imgBtnDocumentingPrevious_Click" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="imgBtnDocumentingContinue" runat="server"
                 onclick="imgBtnDocumentingContinue_Click" ToolTip="Continue"  OnClientClick="return validateForm();"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="imgBtnDocumentingReturnToPrevious" runat="server"
                 onclick="imgBtnDocumentingReturnToPrevious_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
        </div>

</div>
    