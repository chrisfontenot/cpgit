﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCUnderstandingYourNetWorths : System.Web.UI.UserControl
    {
        Double Asset = 0.0;
        Double LibValue = 0.0;
        Double NetWorth = 0.0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserAssetNetWorthHousingOnly UserAssetNetWorthHousingOnlys = null;
                UserAssetNetWorthHousingOnlys = App.Credability.UserAssetNetWorthHousingOnlyGet(SessionState.ClientNumber.Value);
                Asset = UserAssetNetWorthHousingOnlys.ValHome;
                Asset += UserAssetNetWorthHousingOnlys.ValProp;
                Asset += UserAssetNetWorthHousingOnlys.ValCar;
                Asset += UserAssetNetWorthHousingOnlys.ValRet;
                Asset += UserAssetNetWorthHousingOnlys.ValSav;
                Asset += UserAssetNetWorthHousingOnlys.ValOther;


                LibValue += UserAssetNetWorthHousingOnlys.OweHome;
                LibValue += UserAssetNetWorthHousingOnlys.SecondaryAmt;
                LibValue += UserAssetNetWorthHousingOnlys.OweCar;
                LibValue += UserAssetNetWorthHousingOnlys.OweOther;
								Double CreaditBal = App.Credability.CreditorBalanceGet(SessionState.ClientNumber.Value);
                NetWorth = Math.Abs(Asset - (CreaditBal + LibValue));

                double Des = NetWorth % 1;
                NetWorth -= Des;
                lblUnsecuredDebt.Text = "$ " + String.Format("{0:0,0.0}", Asset);
                lblNetWorth.Text = "$ " + String.Format("{0:0,0.0}", NetWorth);

                txtHomeVal.Text = UserAssetNetWorthHousingOnlys.ValHome.ToString();
                txtAutoVal.Text = UserAssetNetWorthHousingOnlys.ValCar.ToString();
                txtOthrVal.Text = UserAssetNetWorthHousingOnlys.ValOther.ToString();
                txtHomeOwe.Text = UserAssetNetWorthHousingOnlys.OweHome.ToString();
                txtAutoOwe.Text = UserAssetNetWorthHousingOnlys.OweCar.ToString();
                txtSecOwe.Text = UserAssetNetWorthHousingOnlys.SecondaryAmt.ToString();
                txtOthrOwe.Text = UserAssetNetWorthHousingOnlys.OweOther.ToString();
                txtInterRate.Text = UserAssetNetWorthHousingOnlys.MortRate.ToString();
                txtPrptVal.Text = UserAssetNetWorthHousingOnlys.ValProp.ToString();
                txtRetrVal.Text = UserAssetNetWorthHousingOnlys.ValRet.ToString();
                txtBnkAccVal.Text = UserAssetNetWorthHousingOnlys.ValSav.ToString();
            }

        }
        protected void imgBtnUnderstandingNetWorthPrevious_Click(object sender, EventArgs e)
        {

            Response.Redirect("UserFinancialSituationRecap.aspx");
        }
        protected void imgBtnUnderstandingNetWorthToPrevious_Click(object sender, EventArgs e)
        {
            UserAssetNetWorthHousingOnly UserAssetNetWorthHousingOnlys = new UserAssetNetWorthHousingOnly();
            UserAssetNetWorthHousingOnlyResult UserAssetNetWorthHousingOnlyResults = null;
            UserAssetNetWorthHousingOnlys.ClientNumber = SessionState.ClientNumber.Value;
            UserAssetNetWorthHousingOnlys.ValHome = isNumeric(txtHomeVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtHomeVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValCar = isNumeric(txtAutoVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtAutoVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValOther = isNumeric(txtOthrVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtOthrVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweHome = isNumeric(txtHomeOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtHomeOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweCar = isNumeric(txtAutoOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtAutoOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.SecondaryAmt = isNumeric(txtSecOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtSecOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweOther = isNumeric(txtOthrOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtOthrOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.MortRate = isNumeric(txtInterRate.Text, System.Globalization.NumberStyles.Float) ? int.Parse(txtInterRate.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValProp = isNumeric(txtPrptVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtPrptVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValRet = isNumeric(txtRetrVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtRetrVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValSav = isNumeric(txtBnkAccVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtBnkAccVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ModForm5 = 1;
            UserAssetNetWorthHousingOnlyResults = App.Credability.UserAssetNetWorthHousingOnlyUpadte(UserAssetNetWorthHousingOnlys);
            if (UserAssetNetWorthHousingOnlyResults.IsSuccessful)
            {
                Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
            }

        }
        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }

        protected void imgBtnUnderstandingNetWorthContinue_Click(object sender, EventArgs e)
        {

            UserAssetNetWorthHousingOnly UserAssetNetWorthHousingOnlys = new UserAssetNetWorthHousingOnly();
            UserAssetNetWorthHousingOnlyResult UserAssetNetWorthHousingOnlyResults = null;
            UserAssetNetWorthHousingOnlys.ClientNumber = SessionState.ClientNumber.Value;
            UserAssetNetWorthHousingOnlys.ValHome = isNumeric(txtHomeVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtHomeVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValCar = isNumeric(txtAutoVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtAutoVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValOther = isNumeric(txtOthrVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtOthrVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweHome = isNumeric(txtHomeOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtHomeOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweCar = isNumeric(txtAutoOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtAutoOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.SecondaryAmt = isNumeric(txtSecOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtSecOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.OweOther = isNumeric(txtOthrOwe.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtOthrOwe.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.MortRate = isNumeric(txtInterRate.Text, System.Globalization.NumberStyles.Float) ? int.Parse(txtInterRate.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValProp = isNumeric(txtPrptVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtPrptVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValRet = isNumeric(txtRetrVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtRetrVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ValSav = isNumeric(txtBnkAccVal.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtBnkAccVal.Text.Trim()) : 0;
            UserAssetNetWorthHousingOnlys.ModForm5 = 1;
            UserAssetNetWorthHousingOnlyResults = App.Credability.UserAssetNetWorthHousingOnlyUpadte(UserAssetNetWorthHousingOnlys);
            if (UserAssetNetWorthHousingOnlyResults.IsSuccessful)
            {
                Response.Redirect("CommonOptionsforCrisis.aspx");
            }
        }


































    }
}