﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCUserDebtListing : System.Web.UI.UserControl
    {
        string ErrorMessageDelSucc = "Record deleted successfully.";
        string ErrorMessageNoRec = "No Record Deleted.";
        string ErrorMessageNoRecAvail = "No Record Available ! Please click below button to add new creditor detials.";
        UserDebtListingsResult UserDebtListingsResults = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get the user comment

            if (!Page.IsPostBack)
            {
                ShowUserDebtListingRecord();
                GetUserComment();
            }

        }
        private void GetUserComment()
        {
            CreditorComment creditorComment = App.Credability.CreditorCommentGet(SessionState.ClientNumber.Value);
            txtComment.Text = creditorComment.CreditorsComment;
        }
        protected void AddMoreRow_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void DeleteRow_Click(object sender, EventArgs e)
        {
            UserDebtListingsResult UserDebtListingsResults = null;
            bool Result = false;
            Int32 RowCount = 0;
            foreach (GridViewRow gvr in UserDebtListingGrid.Rows)
            {
                //Programmatically access the CheckBox from the TemplateField
                CheckBox cbkBoxGrid = (CheckBox)gvr.FindControl("chkId");
                Label lblCreditorId = (Label)gvr.FindControl("lblid");
                if (cbkBoxGrid.Checked == true && lblCreditorId.Text != string.Empty)
                {
                    //Delete row from datatable
                    UserDebtListingsResults = App.Credability.UserDebtListingsDelete(Convert.ToInt32(lblCreditorId.Text));
                    Result = UserDebtListingsResults.IsSuccessful;

                    //Delete the record from the Table in view state also 

                    DataTable NewRowAddedDebtListingTable = new DataTable();
                    NewRowAddedDebtListingTable = (DataTable)ViewState["CurrentTable"];
                    NewRowAddedDebtListingTable.Rows[RowCount].Delete();
                    ViewState["CurrentTable"] = null;
                    ViewState["CurrentTable"] = NewRowAddedDebtListingTable;

                    // End Code

                    RowCount -= 1;


                }
                if (cbkBoxGrid.Checked == true && lblCreditorId.Text == string.Empty)
                {
                    //Delete row from datatable
                    DataTable NewRowAddedDebtListingTable = new DataTable();
                    NewRowAddedDebtListingTable = (DataTable)ViewState["CurrentTable"];
                    NewRowAddedDebtListingTable.Rows[RowCount].Delete();
                    ViewState["CurrentTable"] = null;
                    ViewState["CurrentTable"] = NewRowAddedDebtListingTable;
                    Result = true;
                    RowCount -= 1;
                }
                RowCount += 1;
            }
            if (Result == true)
            {
                DataTable NewRowAddedDebtListingTable = new DataTable();
                NewRowAddedDebtListingTable = (DataTable)ViewState["CurrentTable"];
                UserDebtListingGrid.DataSource = NewRowAddedDebtListingTable;
                UserDebtListingGrid.DataBind();
                lblError.Text = ErrorMessageDelSucc;
            }
            else
            {
                lblError.Text = ErrorMessageNoRec;
            }
            //ShowUserDebtListingRecord();  
            //Refresh grid

            ShowUserDebtListingRecord();
            GetUserComment();
        }
        private void AddNewRowToGrid()
        {
            // int rowIndex;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable NewRowAddedDebtListingTable = new DataTable();
                NewRowAddedDebtListingTable = (DataTable)ViewState["CurrentTable"];

                int RowCount = 0;
                int GrindRowCount = UserDebtListingGrid.Rows.Count;
                foreach (GridViewRow GrdUserDebtListing in UserDebtListingGrid.Rows)
                {
                    //Get the valuse of grid controls
                    Label lblCreditorId = (Label)GrdUserDebtListing.FindControl("lblid");
                    TextBox txtCreditorNameGrd = (TextBox)GrdUserDebtListing.FindControl("txtCreditorName");
                    TextBox txtBalanceGrd = (TextBox)GrdUserDebtListing.FindControl("txtBalance");
                    TextBox txtInterestGrd = (TextBox)GrdUserDebtListing.FindControl("txtInterest");
                    TextBox txtmPaymentGrd = (TextBox)GrdUserDebtListing.FindControl("txtmPayment");
                    TextBox txtAccountNumberGrd = (TextBox)GrdUserDebtListing.FindControl("txtAccountNumber");
                    DropDownList ddlJiontAccountNumberGrd = (DropDownList)GrdUserDebtListing.FindControl("ddlJiontAccountNumber");
                    DropDownList ddlDebtPastDueGrd = (DropDownList)GrdUserDebtListing.FindControl("ddlDebtPastDue");
                    //Add Row data into table
                    NewRowAddedDebtListingTable.Rows[RowCount]["CreditorId"] = lblCreditorId.Text;
                    NewRowAddedDebtListingTable.Rows[RowCount]["PreAcntNoEncs"] = txtAccountNumberGrd.Text;
                    NewRowAddedDebtListingTable.Rows[RowCount]["CreditorBal"] = txtBalanceGrd.Text.Replace("$", "");
                    NewRowAddedDebtListingTable.Rows[RowCount]["CreditorIntrate"] = txtInterestGrd.Text.Replace("%", "");
                    NewRowAddedDebtListingTable.Rows[RowCount]["CreditorName"] = txtCreditorNameGrd.Text;
                    NewRowAddedDebtListingTable.Rows[RowCount]["CreditorPayments"] = txtmPaymentGrd.Text.Replace("$", "");

                    RowCount = RowCount + 1;
                }
                DataRow NewRow;
                NewRow = NewRowAddedDebtListingTable.NewRow();
                NewRowAddedDebtListingTable.Rows.Add(NewRow);

                DataRow NewRow1;
                NewRow1 = NewRowAddedDebtListingTable.NewRow();
                NewRowAddedDebtListingTable.Rows.Add(NewRow1);

                DataRow NewRow2;
                NewRow2 = NewRowAddedDebtListingTable.NewRow();
                NewRowAddedDebtListingTable.Rows.Add(NewRow2);

                DataRow NewRow3;
                NewRow3 = NewRowAddedDebtListingTable.NewRow();
                NewRowAddedDebtListingTable.Rows.Add(NewRow3);

                DataRow NewRow4;
                NewRow4 = NewRowAddedDebtListingTable.NewRow();
                NewRowAddedDebtListingTable.Rows.Add(NewRow4);

                ViewState["CurrentTable"] = NewRowAddedDebtListingTable;
                UserDebtListingGrid.DataSource = NewRowAddedDebtListingTable;
                UserDebtListingGrid.DataBind();
                lblError.Text = string.Empty;
            }
            else
            {
                Response.Write("ViewState is null");
            }

        }
        private void ShowUserDebtListingRecord()
        {
            UserDebtListings[] userDebtListing = App.Credability.UserDebtListingsGet(SessionState.ClientNumber.Value);
            if (userDebtListing != null)
            {
                DataTable DebtListingTable = new DataTable();
                DebtListingTable.Columns.Add("CreditorId");
                DebtListingTable.Columns.Add("PreAcntNoEncs");
                DebtListingTable.Columns.Add("CreditorBal");
                DebtListingTable.Columns.Add("CreditorIntrate");
                DebtListingTable.Columns.Add("CreditorPayments");
                DebtListingTable.Columns.Add("CreditorName");
                DebtListingTable.Columns.Add("CreditorJointAcct");
                DebtListingTable.Columns.Add("creditorPastDue");
                DataRow dr;
                for (int index = 0; index < userDebtListing.Length; index++)
                {
                    dr = DebtListingTable.NewRow();
                    dr["CreditorId"] = userDebtListing[index].CreditorId;
                    dr["CreditorName"] = userDebtListing[index].CreditorName;
                    dr["CreditorBal"] = userDebtListing[index].CreditorBal;
                    dr["CreditorIntrate"] = userDebtListing[index].CreditorIntrate;
                    dr["CreditorPayments"] = userDebtListing[index].CreditorPayments;
                    dr["CreditorJointAcct"] = userDebtListing[index].CreditorJointAcct;
                    dr["creditorPastDue"] = userDebtListing[index].creditorPastDue;
                    if (userDebtListing[index].PreAcntNoEncs != null)
                    {
                        dr["PreAcntNoEncs"] = userDebtListing[index].PreAcntNoEncs.ToString();

                    }
                    else
                    {
                        userDebtListing[index].PreAcntNoEncs = "0000000";
                    }
                    DebtListingTable.Rows.Add(dr);

                }
                if (ViewState["CurrentTable"] == null)
                {
                    ViewState["CurrentTable"] = null;
                    ViewState["CurrentTable"] = DebtListingTable;
                }
                else
                {
                    DebtListingTable = ViewState["CurrentTable"] as DataTable;
                }
                if (DebtListingTable.Rows.Count != 0)
                {
                    UserDebtListingGrid.DataSource = DebtListingTable;
                    UserDebtListingGrid.DataBind();
                }
                else
                {
                    for (int index = 0; index < 5; index++)
                    {
                        dr = DebtListingTable.NewRow();
                        dr["CreditorId"] = "";
                        dr["CreditorName"] = "";
                        dr["CreditorBal"] = "";
                        dr["CreditorIntrate"] = "";
                        dr["CreditorPayments"] = "";
                        dr["CreditorJointAcct"] = "";
                        dr["creditorPastDue"] = "";
                        dr["PreAcntNoEncs"] = "";
                        DebtListingTable.Rows.Add(dr);

                    }
                    ViewState["CurrentTable"] = DebtListingTable;
                    UserDebtListingGrid.DataSource = DebtListingTable;
                    UserDebtListingGrid.DataBind();
                    lblError.Text = ErrorMessageNoRecAvail;
                }

            }


        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (AddUpdateDebtListingData())
            {
                CreditorComment creditorComment = new CreditorComment();
                CreditorCommentResult Result = null;
								creditorComment.ClientNumber = SessionState.ClientNumber.Value;
                creditorComment.CreditorsComment = txtComment.Text.ToString().Trim();
                Result = App.Credability.CreaditorCommentsUpdate(creditorComment);
                if (Result.IsSuccessful)
                {
                    Response.Redirect("UserIncomeDocumentation.aspx");
                }
                else
                {
                    ShowUserDebtListingRecord();
                    //Error Message
                }
            }
        }


        private bool AddUpdateDebtListingData()
        {
            bool Result = false;

            foreach (GridViewRow GrdUserDebtListing in UserDebtListingGrid.Rows)
            {
                Label lblCreditorId = (Label)GrdUserDebtListing.FindControl("lblid");
                TextBox txtCreditorNameGrd = (TextBox)GrdUserDebtListing.FindControl("txtCreditorName");
                TextBox txtBalanceGrd = (TextBox)GrdUserDebtListing.FindControl("txtBalance");
                TextBox txtInterestGrd = (TextBox)GrdUserDebtListing.FindControl("txtInterest");
                TextBox txtmPaymentGrd = (TextBox)GrdUserDebtListing.FindControl("txtmPayment");
                TextBox txtAccountNumberGrd = (TextBox)GrdUserDebtListing.FindControl("txtAccountNumber");
                DropDownList ddlJiontAccountNumberGrd = (DropDownList)GrdUserDebtListing.FindControl("ddlJiontAccountNumber");
                DropDownList ddlDebtPastDueGrd = (DropDownList)GrdUserDebtListing.FindControl("ddlDebtPastDue");


                if (!String.IsNullOrEmpty(txtCreditorNameGrd.Text) && !String.IsNullOrEmpty(txtBalanceGrd.Text) && !String.IsNullOrEmpty(txtInterestGrd.Text) && !String.IsNullOrEmpty(txtmPaymentGrd.Text) && !String.IsNullOrEmpty(txtAccountNumberGrd.Text))
                {
                    UserDebtListings UsersDebtListings = new UserDebtListings();
                    if (lblCreditorId.Text == string.Empty)
                    {
                        UsersDebtListings.CreditorId = 0;
                    }
                    else
                    {
                        UsersDebtListings.CreditorId = Convert.ToInt32(lblCreditorId.Text);
                    }
                    UsersDebtListings.CreditorName = txtCreditorNameGrd.Text;
										UsersDebtListings.ClientNumber = SessionState.ClientNumber.Value;
                    UsersDebtListings.CreditorBal = float.Parse(txtBalanceGrd.Text.Replace("$", ""));
                    UsersDebtListings.CreditorPayments = float.Parse(txtmPaymentGrd.Text.Replace("$", ""));
                    UsersDebtListings.CreditorIntrate = float.Parse(txtInterestGrd.Text.Replace("%", ""));
                    UsersDebtListings.CreditorJointAcct = ddlJiontAccountNumberGrd.SelectedItem.Value.Trim();
                    UsersDebtListings.creditorPastDue = ddlDebtPastDueGrd.SelectedItem.Value.Trim();

                    //char[] values = txtAccountNumberGrd.Text.ToCharArray();
                    //string HexaDecimalAccountnumber = string.Empty;
                    //foreach (char letter in values)
                    //{
                    //    // Get the integral value of the character.
                    //    int value = Convert.ToInt32(letter);
                    //    // Convert the decimal value to a hexadecimal value in string form.
                    //    HexaDecimalAccountnumber += String.Format("{0:X}", value);


                    //}

                    //UsersDebtListings.CrAcntNoEncs = txtAccountNumberGrd.Text;
                    UsersDebtListings.PreAcntNoEncs = txtAccountNumberGrd.Text;
                    UsersDebtListings.LastModDTS = DateTime.Now;
                    UserDebtListingsResults = App.Credability.UserDebtListingsAddupdate(UsersDebtListings);
                    Result = UserDebtListingsResults.IsSuccessful;
                    //ddlJiontAccountNumberGrd
                    //UsersDebtListings.PreAcntNoEnc = System.Text.Encoding.Unicode.GetBytes("AS234TT");

                }
                else  // If all rows are eampty then also move to next page
                    Result = true;



            }

            //Check if there is no row in grid retrun true to navigate from DebtListing page to next page
            if (UserDebtListingGrid.Rows.Count == 0)
            {
                Result = true;
            }
            return Result;
            //UserDebtListingGrid.DataSource = dtCurrentTable;
            //UserDebtListingGrid.DataBind();

            //}



        }

        protected void btnSaveExit_Click(object sender, EventArgs e)
        {
            if (AddUpdateDebtListingData())
            {
                CreditorComment creditorComment = new CreditorComment();
                CreditorCommentResult Result = null;
								creditorComment.ClientNumber = SessionState.ClientNumber.Value;
                creditorComment.CreditorsComment = txtComment.Text.ToString().Trim();
                Result = App.Credability.CreaditorCommentsUpdate(creditorComment);
                if (Result.IsSuccessful)
                {
                    Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
                }
                else
                {
                    ShowUserDebtListingRecord();
                    //Error Message
                }
            }

        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("DescribeYourSituation.aspx");
        }

        protected void UserDebtListingGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;
            if (e.Row.RowType == DataControlRowType.DataRow && dr != null)
            {

                if (dr["CreditorBal"] != null)
                {
                    TextBox txtCreditorbalance = e.Row.Cells[2].FindControl("txtBalance") as TextBox;
                    if (!string.IsNullOrEmpty(txtCreditorbalance.Text))
                    {
                        txtCreditorbalance.Text = string.Empty;
                        txtCreditorbalance.Text = "$" + dr["CreditorBal"].ToString();
                    }

                }
                if (dr["CreditorIntrate"] != null)
                {
                    TextBox txtInterest = e.Row.Cells[3].FindControl("txtInterest") as TextBox;
                    if (!string.IsNullOrEmpty(txtInterest.Text))
                    {
                        txtInterest.Text = string.Empty;
                        txtInterest.Text = dr["CreditorIntrate"].ToString() + "%";
                    }

                }
                if (dr["CreditorPayments"] != null)
                {
                    TextBox txtPayment = e.Row.Cells[4].FindControl("txtmPayment") as TextBox;
                    if (!string.IsNullOrEmpty(txtPayment.Text))
                    {
                        txtPayment.Text = string.Empty;
                        txtPayment.Text = "$" + dr["CreditorPayments"].ToString();
                    }

                }
                if (dr["CreditorJointAcct"] != null)
                {
                    DropDownList ddljointAccountNumber = (DropDownList)e.Row.Cells[6].FindControl("ddlJiontAccountNumber");
                    ddljointAccountNumber.SelectedValue = dr["CreditorJointAcct"].ToString().Trim();
                }

                if (dr["creditorPastDue"] != null)
                {
                    DropDownList ddlcreditorPastDue = (DropDownList)e.Row.Cells[7].FindControl("ddlDebtPastDue");
                    ddlcreditorPastDue.SelectedValue = dr["creditorPastDue"].ToString().Trim();
                }
            }

        }
    }
}