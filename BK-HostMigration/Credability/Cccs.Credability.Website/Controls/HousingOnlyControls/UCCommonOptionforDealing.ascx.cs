﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCCommonOptionforDealing : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void click_btnReverseMortgagePrevious(object sender, EventArgs e)
        {
            Response.Redirect("CommonOptionsforCrisis.aspx");
        }

        protected void click_btnSendtoCounselorforFinalReview(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
        }
    }
}