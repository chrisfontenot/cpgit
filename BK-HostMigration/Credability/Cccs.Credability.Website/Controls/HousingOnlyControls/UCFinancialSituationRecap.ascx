﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFinancialSituationRecap.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCFinancialSituationRecap" %>
<h1>Analyzing Your Financial Situation</h1>
<p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|IOFYT")%></p>
<p><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysisDmp|LBIA")%></p>
<div class="dvfinance">
<div class="dvcollft">
<h1> <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Income")%></h1>
<table cellspacing="0" cellpadding="0" class="tblsummary">
<tbody><tr>
<td class="col1"><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TMI")%><br /> <a href="UserDebtListing.aspx" class="NormLink">
                            <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTM")%></a></td>
<td class="col2"><asp:Label ID="lblTotalMonthlyIncome" runat="server"></asp:Label></td>
</tr>

</tbody></table>

<p>&nbsp;</p>
<h1> <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Debt")%></h1>
<table cellspacing="0" cellpadding="0" class="tblsummary">
<tbody><tr>
<td class="col1"><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TUD")%></td>
<td class="col2"><asp:Label ID="TotalUnsecuredDebt" runat="server"></asp:Label></td>
</tr>
<tr>
<td class="col1"><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TP")%><br />
<a href="UserDebtListing.aspx" class="NormLink">
                            <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTM")%></a>
</td>
<td class="col2"> <asp:Label ID="lblTotalPayments" runat="server"></asp:Label></td>
</tr>

</tbody></table>

</div>
<div class="dvcolrht">
<h1><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|Expenses")%></h1>
<table cellspacing="0" cellpadding="0" class="tblsummary">
<tbody><tr>
<td class="col1"><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TME")%><br /><a href="UserDebtListing.aspx" class="NormLink">
                            <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|INTMA")%></a></td>
<td class="col2"><asp:Label ID="lblTotalMonthlyExp" runat="server"></asp:Label></td>
</tr>

</tbody></table>

<p>&nbsp;</p>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|BS")%></h1>
<table cellspacing="0" cellpadding="0" class="tblsummary">
<tbody><tr>
<td class="col1"> <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationRecap|TDI")%> </td>
<td class="col2"><asp:Label ID="lblDiscretionaryIncome" runat="server"></asp:Label></td>
</tr>
 <tr>
<td class="col1">&nbsp;<br />&nbsp;</td>
<td class="col2">&nbsp;</td>
</tr>
</tbody></table>
</div>
</div>
<p>&nbsp;</p>
<p><%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationDMP|NYCW")%> <a href="http://idhasoft.com"> Click here</a></p>

<p>&nbsp;</p>
<div id="dvErrorMessage" runat="server" style="text-align:center;color:Red;font-weight:bold;"></div>
<div class="dvform2col">
<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationDMP|WYARAc2")%>
 <asp:TextBox ID="txtAuthCodeInput" runat="server"></asp:TextBox>
 <%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationDMP|CVOT")%>
<div class="clearboth"> </div>
 </div>
 <p>&nbsp;</p>
 


<div class="dvbtncontainer">
<div class="lnkbutton">
<asp:LinkButton ID="btnPreviousPage" runat="server" CssClass="previous" OnClick="btnPreviousPage_Click"><span>Go To Previous Page</span></asp:LinkButton>
  </div>
  <div class="lnkbutton">
  <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ><span>Continue</span></asp:LinkButton>
</div>
<div class="lnkbutton">
  <asp:LinkButton ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click"><span>Save & Exit</span></asp:LinkButton>
 </div>
  </div>