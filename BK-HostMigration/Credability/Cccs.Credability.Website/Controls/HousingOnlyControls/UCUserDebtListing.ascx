﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCUserDebtListing.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCUserDebtListing" %>
<script language="javascript" src="../Content/FormChek.js"></script>
<script language="javascript" type="text/javascript">

var theCreditorCount,HoldField ="",CredRptCnt =10;
function prepareField(theField)
{
    var sValue = theField.value;
	HoldField = sValue;
	sValue = stripCharsNotInBag (sValue, "0123456789.");
	theField.value = sValue;
	theField.select();
}
function CheckMod(theField)
{
	if (HoldField != theField.value && HoldField != "")
	{
		HoldField = "";
	}
}
function reformatPercent(theField)
{
	var sValue = theField.value;
	sValue = Number(stripCharsNotInBag (sValue, "0123456789."));
	if (sValue == 0)
	{
		sValue = 0;
	}
	theField.value = String(sValue.toFixed(2)) + "%";
	CheckMod(theField);
}
function reformatCCCSCurrency (theField)
{
	var sValue = theField.value,OutTxt = "", Des;
	var NumStr, Len, i;
	sValue = stripCharsNotInBag (sValue, "0123456789.");
	if (sValue == 0)
	{	
		sValue = 0;
	}
	NumStr = sValue - (sValue % 1);
	NumStr = String(NumStr);
	Len = NumStr.length - 1;
	for (i = 0; i <= Len; i++) 
	{
		OutTxt +=  NumStr.charAt(i);
		if (i < Len && (Len - i) % 3 == 0) 
		{
			OutTxt += ",";
		}
	}
	sValue = '$' + OutTxt;
	theField.value = sValue;
	CheckMod(theField);
}
function clearDeleted(form) 
{
	var aCheckBoxes = document.getElementsByName("id");
	for (var x=0; x < aCheckBoxes.length; x++) //>
	{
		if (aCheckBoxes[x].checked) 
		{
			if (isEmpty(form.elements["creditor_" + x].value)) 
			{
				form.elements["name_" + x].value = "";
				form.elements["bal_" + x].value = "";
				form.elements["intrate_" + x].value = "";
				form.elements["payments_" + x].value = "";
				form.elements["pre_acctnbr_" + x].value = "";
				form.elements["jointacct_" + x].value = "";
				form.elements["pastdue_" + x].value = "";
				form.elements["acct_" + x].value = "";
				form.elements["cred_rpt_" + x].value = "";
			}
		}
	}
}
function IsGeaterThanZero(Value)
{
	Value = stripCharsNotInBag (Value, "0123456789.");
	if(Value == "")
	{
		return false
	}
	if(Number(Value) > 0)
	{
		return true;
	}
	return false;
}
function validateForm(form)
{
	var SndMsg = true, Back = true;
	if(CredRptCnt > 0)
	{
		SndMsg = false;
	}
	for (var x=0; (x <theCreditorCount) && SndMsg; x++) 
	{
		if (!isWhitespace(form.elements["name_" + x].value))
		{
			if(!isWhitespace(form.elements["bal_" + x].value))
			{
				SndMsg = false;
			}
		}
	}
	if (SndMsg)
		Back = confirm("You have not listed any creditors are you sure you wish to continue?");
	return Back;
}
</script>
<h1> <%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|Title")%></h1>
<p> <%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebtsRVM|GACA")%></p>
 <p><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|EIYS")%></p>
 <div id="TopContent">
 <p><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NrOrCl")%></p>
</div>


<div id="dvMessage" style="text-align:center;margin-bottom:10px;">
<asp:Label ID="lblError" runat="server" style="color:Red;font-weight:bold;"></asp:Label>
</div>
<div id="dvContent">
<asp:GridView ID="UserDebtListingGrid" runat="server" ShowFooter="false" AutoGenerateColumns="false" BorderWidth="1" 
    AllowPaging="false" CssClass="TblUserdebtListing" GridLines="None"  AlternatingRowStyle-CssClass="altrw"
         onrowdatabound="UserDebtListingGrid_RowDataBound">
    <Columns>
    <asp:TemplateField ItemStyle-CssClass="col1">
      <HeaderTemplate >
      <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|SelectforDelete")%>
      <div>&nbsp;<br />&nbsp;<br />&nbsp;</div>
  </HeaderTemplate>
  
  
   <ItemTemplate>
                <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# Bind("CreditorId")%>'></asp:Label>
                <asp:CheckBox ID="chkId" runat="server" />
            </ItemTemplate>
        </asp:TemplateField>
       <%-- <asp:TemplateField  HeaderText="<center>Creditor Name</center><center>(or collection agency)</center><center>Required</center><BR><BR>-------------------------------------<BR>EXAMPLE">--%>
     
     <asp:TemplateField ItemStyle-CssClass="col2">
       <HeaderTemplate >
      <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|CreditorsName")%>
       <span> <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
       <div>&nbsp;<br />&nbsp;<br /><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|EXAMPLE")%></div>
    
  </HeaderTemplate>
  
            <ItemTemplate >
                <asp:TextBox ID="txtCreditorName" runat="server" Text='<%# Bind("CreditorName") %>' MaxLength="60" CssClass="txtbx"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="rvtxtCreditorName" SetFocusOnError="true"  Display="Dynamic" runat="server" EnableClientScript="true" 
                 ControlToValidate="txtCreditorName"  Text="Creditor Name Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

                 
            </ItemTemplate>
        </asp:TemplateField>
    
        <asp:TemplateField ItemStyle-CssClass="col3">
       <HeaderTemplate>
       <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Balance")%><br />
        <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|UseCurrentBalance")%><br />
       <span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
       <div>&nbsp;<br />&nbsp;<br />$4,356</div>

     </HeaderTemplate>
    
            <ItemTemplate>
                <asp:TextBox ID="txtBalance" runat="server" Text='<%# Bind("CreditorBal") %>' MaxLength="7" onBlur="reformatCCCSCurrency(this)" onFocus="prepareField(this);" CssClass="txtbx"></asp:TextBox><br />
                 <asp:RequiredFieldValidator ID="rvtxtBalance" SetFocusOnError="true"  Display="Dynamic" runat="server" EnableClientScript="true" 
                 ControlToValidate="txtBalance"  Text="Balance Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

                <asp:CompareValidator  SetFocusOnError="true"    ControlToValidate="txtBalance" Operator="DataTypeCheck" display="Dynamic" ValidationGroup="userprofile" 
Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtBalance"></asp:CompareValidator>
            </ItemTemplate>
        </asp:TemplateField>
      
            <asp:TemplateField ItemStyle-CssClass="col4">
       <HeaderTemplate>
       
        <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|IntrestRate")%>
       <div>&nbsp;<br />&nbsp;<br />12.34%</div>
     </HeaderTemplate>
    
            <ItemTemplate>
                <asp:TextBox ID="txtInterest" runat="server" Text='<%# Bind("CreditorIntrate") %>' MaxLength="7" onBlur="reformatPercent(this);" onFocus="prepareField(this);" CssClass="txtbx"></asp:TextBox><br />
                  <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtInterest" Operator="DataTypeCheck" display="Dynamic" ValidationGroup="userprofile" 
Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtInterest"></asp:CompareValidator>
         
            </ItemTemplate>
        </asp:TemplateField>
           <asp:TemplateField ItemStyle-CssClass="col5">
       <HeaderTemplate>
       <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|MonthlyPayment")%>
       <span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
       <div>&nbsp;<br />&nbsp;<br />$78</div>
     </HeaderTemplate>
    <ItemTemplate>
                <asp:TextBox ID="txtmPayment" runat="server" Text='<%# Bind("CreditorPayments") %>' MaxLength="7" onBlur="reformatCCCSCurrency(this)" onFocus="prepareField(this);" CssClass="txtbx"></asp:TextBox><br />
                 <asp:RequiredFieldValidator ID="rvtxtmPayment" SetFocusOnError="true"  Display="Dynamic" runat="server" EnableClientScript="true" 
                 ControlToValidate="txtmPayment"  Text="Monthly Payment Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

                <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtmPayment" Operator="DataTypeCheck" display="Dynamic" ValidationGroup="userprofile" 
Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtmPayment"></asp:CompareValidator>
            </ItemTemplate>
        </asp:TemplateField>
       
          <asp:TemplateField ItemStyle-CssClass="col6">
        <HeaderTemplate>
       <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|AccountNumber")%>
        <span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>        
        <div> <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|FullAccountNumber")%><a class="Link" href="#" onClick="javascript: return false;" 
                    title="We need your full account number to accurately identify creditor concessions"><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Why")%>?</a></div>
     </HeaderTemplate>
     <ItemTemplate>
                <asp:TextBox ID="txtAccountNumber" runat="server" Text='<%# Bind("PreAcntNoEncs") %>' CssClass="txtbx"></asp:TextBox><br />
                 <asp:RequiredFieldValidator ID="rvtxtAccountNumber" SetFocusOnError="true"  Display="Dynamic" runat="server" EnableClientScript="true" 
                 ControlToValidate="txtAccountNumber"  Text="Account Number Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ItemStyle-CssClass="col7">
        
        
        <HeaderTemplate>
        <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|JointAct")%><br /><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
        <div>&nbsp;<br /><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|No")%> <br /><a class="Link" href="#" onClick="javascript: return false;" 
                    title="We need to determine if the account is a join account to determine payment obligations."><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Why")%>?</a></div>
  </HeaderTemplate>

         
            <ItemTemplate>
                <asp:DropDownList ID="ddlJiontAccountNumber" runat="server">
                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
         
         <asp:TemplateField ItemStyle-CssClass="col8">
       <HeaderTemplate>
       <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|DebtPastDue")%><br /><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Required")%></span>
       <div>&nbsp;<br />&nbsp;<br /> <%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|Current")%></div>
     
     </HeaderTemplate>
     <ItemTemplate>
                <asp:DropDownList ID="ddlDebtPastDue" runat="server">
                    <asp:ListItem Text="Current" Value="0"></asp:ListItem>
                    <asp:ListItem Text="< 30 days" Value=1"></asp:ListItem>
                    <asp:ListItem Text="30 days" Value="2"></asp:ListItem>
                    <asp:ListItem Text="60 days" Value="3"></asp:ListItem>
                    <asp:ListItem Text="90 days" Value="3"></asp:ListItem>
                    <asp:ListItem Text=">90 days" Value="4"></asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</div>
 <div class="dvbtncontainer">
<div class="lnkbutton">
           <asp:LinkButton ID="btnADDMoreCred" runat="server"
                 onclick="AddMoreRow_Click" ToolTip="Add More Creditors" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|AddMoreCreditors")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="btnDelete" runat="server"  OnClientClick="return confirm('Are you sure you want to delete this row ?');"
                 OnClick="DeleteRow_Click" ToolTip="Delete Selected"><span><%= Cccs.Credability.Website.App.Translate("Credability|UserDebtListing|DeleteSelected")%></span></asp:LinkButton>
        </div>

</div>
<div  class="clearboth"> </div>

 <p><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|NOTE")%></p>
<p><%= Cccs.Credability.Website.App.Translate("Credability|ListingYourDebts|Comments")%></p>


<div style="margin-bottom:20px;">
<asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine"  Width="100%" Rows="5" MaxLength="200"></asp:TextBox>
</div>


 
 <div class="dvbtncontainer">
<div class="lnkbutton">
           <asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous"
                 onclick="btnPrevious_Click" ToolTip="Return To Previous Page" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="btnContinue" runat="server"
                 onclick="btnContinue_Click" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="LinkButton1" runat="server"
                 onclick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
        </div>

</div>


