﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCMonthlyExpenses : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnMonthlyPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
            btnMonthlyExpensesCon.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            btnMonthlySaveExitPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
            if (!IsPostBack)
            {
                UserMonthlyExpensesBCH UserMonthlyExpensesBCH = null;
                UserMonthlyExpensesBCH = App.Credability.UserMonthlyExpensesBCHGet(SessionState.ClientNumber.Value);

                UserMonthlyExpensesExtraHudAbt UserMonthlyExpensesExtraHudAbt = null;
                UserMonthlyExpensesExtraHudAbt = App.Credability.UserMonthlyExpensesExtraHudAbtGet(SessionState.ClientNumber.Value);

                //For ContactDetails Table




                if (UserMonthlyExpensesBCH != null)
                {


                    txtRentmort.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Rentmort).ToString();
                    txtHomemaintenance.Text = "$" + Math.Round(UserMonthlyExpensesBCH.HomeMaintenance).ToString();
                    txtTelephone.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Telephone).ToString();
                    txtFoodaway.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Foodaway).ToString();
                    txtGroceries.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Groceries).ToString();
                    txtVehiclepayments.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Vehiclepayments).ToString();
                    if (UserMonthlyExpensesBCH.Carcurrent != null)
                    {
                        ddlPayAutoCur.ClearSelection();
                        ddlPayAutoCur.SelectedValue = UserMonthlyExpensesBCH.Carcurrent.ToString();

                    }
                    txtCarinsurance.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Carinsurance).ToString();
                    txtCarmaintenance.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Carmaintenance).ToString();
                    txtPublictransportation.Text = "$" + Math.Round(UserMonthlyExpensesBCH.PublicTransportation).ToString();
                    txtInsurance.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Insurance).ToString();
                    txtMedicalprescription.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Medicalprescription).ToString();
                    txtEducation.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Education).ToString();
                    txtChildsupport.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Childsupport).ToString();
                    txtChildeldercare.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Childsupport).ToString();
                    txtLaundry.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Laundry).ToString();
                    txtPersonalexp.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Personalexpenses).ToString();
                    txtClothing.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Clothing).ToString();
                    txtBeautybarber.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Beautybarbershop).ToString();
                    txtRecreation.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Recreation).ToString();
                    txtContributions.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Contributions).ToString();
                    txtClubdues.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Clubdues).ToString();
                    txtGifts.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Gifts).ToString();
                    txtMiscellaneous.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Miscellaneous).ToString();

                    //txtOtherLoans.Text = "$" + Math.Round(UserMonthlyExpensesBCH.OTHERLoans).ToString() ;
                    //txtCardPayments.Text = "$" + Math.Round(UserMonthlyExpensesBCH.CARDPayments).ToString() ;
                    txtOtherLoans.Text = (UserMonthlyExpensesBCH.OTHERLoans != null) ? (Convert.ToInt32(UserMonthlyExpensesBCH.OTHERLoans)).ToString() : "0";
                    txtCardPayments.Text = (UserMonthlyExpensesBCH.CARDPayments != null) ? (Convert.ToInt32(UserMonthlyExpensesBCH.CARDPayments)).ToString() : "0";



                }


                //if (UserMonthlyExpensesBCH != null)
                //{


                //    txtRentmort.Text = (UserMonthlyExpensesBCH.Rentmort != 0) ? (Convert.ToInt16(UserMonthlyExpensesBCH.Rentmort)).ToString() : "0";
                //    txtHomemaintenance.Text = (UserMonthlyExpensesBCH.HomeMaintenance != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.HomeMaintenance)).ToString() : "0";
                //    txtTelephone.Text = (UserMonthlyExpensesBCH.Telephone != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Telephone)).ToString() : "0";
                //    txtFoodaway.Text = (UserMonthlyExpensesBCH.Foodaway != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Foodaway)).ToString() : "0";
                //    txtGroceries.Text = (UserMonthlyExpensesBCH.Groceries != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Groceries)).ToString() : "0";
                //    txtVehiclepayments.Text = (UserMonthlyExpensesBCH.Vehiclepayments != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Vehiclepayments)).ToString() : "0";
                //    if (UserMonthlyExpensesBCH.Carcurrent != null)
                //    {
                //        ddlPayAutoCur.ClearSelection();
                //        ddlPayAutoCur.SelectedValue = UserMonthlyExpensesBCH.Carcurrent.ToString();

                //    }
                //    txtCarinsurance.Text =(UserMonthlyExpensesBCH.Carinsurance != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Carinsurance)).ToString() : "0";
                //    txtCarmaintenance.Text = (UserMonthlyExpensesBCH.Carmaintenance != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Carmaintenance)).ToString() : "0";
                //    txtPublictransportation.Text = (UserMonthlyExpensesBCH.PublicTransportation != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.PublicTransportation)).ToString() : "0";
                //    txtInsurance.Text = (UserMonthlyExpensesBCH.Insurance != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Insurance)).ToString() : "0";
                //    txtMedicalprescription.Text = (UserMonthlyExpensesBCH.Medicalprescription != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Medicalprescription)).ToString() : "0";
                //    txtEducation.Text = (UserMonthlyExpensesBCH.Education != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Education)).ToString() : "0";
                //    txtChildsupport.Text = (UserMonthlyExpensesBCH.Childsupport != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Childsupport)).ToString() : "0";
                //    txtChildeldercare.Text = (UserMonthlyExpensesBCH.Childsupport != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Childsupport)).ToString() : "0";
                //    txtLaundry.Text = (UserMonthlyExpensesBCH.Laundry != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Laundry)).ToString() : "0";
                //    txtPersonalexp.Text = (UserMonthlyExpensesBCH.Personalexpenses != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Personalexpenses)).ToString() : "0";
                //    txtClothing.Text = (UserMonthlyExpensesBCH.Clothing != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Clothing)).ToString() : "0";
                //    txtBeautybarber.Text = (UserMonthlyExpensesBCH.Beautybarbershop != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Beautybarbershop)).ToString() : "0";
                //    txtRecreation.Text = (UserMonthlyExpensesBCH.Recreation != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Recreation)).ToString() : "0";
                //    txtContributions.Text = (UserMonthlyExpensesBCH.Contributions != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Contributions)).ToString() : "0";
                //    txtClubdues.Text = (UserMonthlyExpensesBCH.Clubdues != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Clubdues)).ToString() : "0";
                //    txtGifts.Text = (UserMonthlyExpensesBCH.Gifts != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Gifts)).ToString() : "0";
                //    txtMiscellaneous.Text = (UserMonthlyExpensesBCH.Miscellaneous != null) ? (Convert.ToDecimal(UserMonthlyExpensesBCH.Miscellaneous)).ToString() : "0";
                //    txtOtherLoans.Text = (UserMonthlyExpensesBCH.OTHERLoans != null) ? (Convert.ToInt32(UserMonthlyExpensesBCH.OTHERLoans)).ToString() : "0";
                //    txtCardPayments.Text = (UserMonthlyExpensesBCH.CARDPayments != null) ? (Convert.ToInt32(UserMonthlyExpensesBCH.CARDPayments)).ToString() : "0";


                //}
                //For ExtraHudAbt Table

                if (UserMonthlyExpensesExtraHudAbt != null)
                {

                    if (UserMonthlyExpensesExtraHudAbt.includeFha != null)
                    {
                        ddlIsFhaInc.ClearSelection();
                        ddlIsFhaInc.SelectedValue = UserMonthlyExpensesExtraHudAbt.includeFha.ToString().Trim();
                    }

                    txtEducation.Text = "$" + Math.Round(UserMonthlyExpensesBCH.Education).ToString();



                    txtMoEquity.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.moEquity).ToString();
                    txtMo2ndMort.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.mo2ndMort).ToString();
                    txtMoFee.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.moFee).ToString();
                    txtUtlElectric.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.utlElectric).ToString();
                    txtUtlWater.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.utlWater).ToString();
                    txtUtlTv.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.utlTv).ToString();
                    txtUtlTrash.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.utlTv).ToString();
                    txtUtlGas.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.utlGas).ToString();
                    txtPayIns.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.AnPropIns / 12).ToString();
                    txtPayTax.Text = "$" + Math.Round(UserMonthlyExpensesExtraHudAbt.AnPropTax / 12).ToString();



                    //txtMoEquity.Text = (UserMonthlyExpensesExtraHudAbt.moEquity != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.moEquity)).ToString() : "0";
                    //txtMo2ndMort.Text = (UserMonthlyExpensesExtraHudAbt.mo2ndMort != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.mo2ndMort)).ToString() : "0";
                    //txtMoFee.Text = (UserMonthlyExpensesExtraHudAbt.moFee != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.moFee)).ToString() : "0";
                    //txtUtlElectric.Text = (UserMonthlyExpensesExtraHudAbt.utlElectric != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.utlElectric)).ToString() : "0";
                    //txtUtlWater.Text = (UserMonthlyExpensesExtraHudAbt.utlWater != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.utlWater)).ToString() : "0";
                    //txtUtlTv.Text = (UserMonthlyExpensesExtraHudAbt.utlTv != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.utlTv)).ToString() : "0";
                    //txtUtlTrash.Text = (UserMonthlyExpensesExtraHudAbt.utlTv != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.utlTv)).ToString() : "0";
                    //txtUtlGas.Text = (UserMonthlyExpensesExtraHudAbt.utlGas != null) ? (Convert.ToDecimal(UserMonthlyExpensesExtraHudAbt.utlGas)).ToString() : "0";
                    //txtPayIns.Text = (UserMonthlyExpensesExtraHudAbt.AnPropIns != null) ? (UserMonthlyExpensesExtraHudAbt.AnPropIns / 12).ToString() : "0";
                    //txtPayTax.Text = (UserMonthlyExpensesExtraHudAbt.AnPropTax != null) ? (UserMonthlyExpensesExtraHudAbt.AnPropTax / 12).ToString() : "0";
                    txtPayIns.Enabled = false;
                    txtPayTax.Enabled = false;

                }
                else
                {
                    txtMoEquity.Text = "0";
                    txtMo2ndMort.Text = "0";
                    txtMoFee.Text = "0";
                    txtUtlElectric.Text = "0";
                    txtUtlWater.Text = "0";
                    txtUtlTv.Text = "0";
                    txtUtlTrash.Text = "0";
                    txtUtlGas.Text = "0";
                    txtPayIns.Text = "0";
                    txtPayIns.Enabled = false;
                    txtPayTax.Enabled = false;
                    txtPayTax.Text = "0";

                }
            }

        }

        protected void btnMonthlyPreviousPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserIncomeDocumentation.aspx");
        }

        protected void btnMonthlySaveExitPage_Click(object sender, EventArgs e)
        {

            //Update The Information
            UserMonthlyExpensesBCHResult UserMonthlyExpensesBCHResults = null;
            UserMonthlyExpensesBCH userMonthlyExpensesBCH = new UserMonthlyExpensesBCH();

            UserMonthlyExpensesExtraHudAbtResult UserMonthlyExpensesExtraHudAbtResults = null;
            UserMonthlyExpensesExtraHudAbt userMonthlyExpensesExtraHudAbt = new UserMonthlyExpensesExtraHudAbt();



            //For ContactDetails Table



            userMonthlyExpensesBCH.ClientNumber = SessionState.ClientNumber.Value; 
            userMonthlyExpensesBCH.Rentmort = isNumeric(txtRentmort.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtRentmort.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.HomeMaintenance = isNumeric(txtHomemaintenance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtHomemaintenance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Telephone = isNumeric(txtTelephone.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtTelephone.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.Foodaway = isNumeric(txtFoodaway.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtFoodaway.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Groceries = isNumeric(txtGroceries.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtGroceries.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Vehiclepayments = isNumeric(txtVehiclepayments.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtVehiclepayments.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carcurrent = isNumeric(ddlPayAutoCur.SelectedValue, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(ddlPayAutoCur.SelectedValue.Trim()) : 0;
            userMonthlyExpensesBCH.Carinsurance = isNumeric(txtCarinsurance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtCarinsurance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carmaintenance = isNumeric(txtCarmaintenance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtCarmaintenance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.PublicTransportation = isNumeric(txtPublictransportation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtPublictransportation.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Insurance = isNumeric(txtInsurance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtInsurance.Text) : 0;
            userMonthlyExpensesBCH.Medicalprescription = isNumeric(txtMedicalprescription.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtMedicalprescription.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Education = isNumeric(txtEducation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtEducation.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Childsupport = isNumeric(txtChildsupport.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtChildsupport.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Childsupport = isNumeric(txtChildeldercare.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtChildeldercare.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Laundry = isNumeric(txtLaundry.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtLaundry.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Personalexpenses = isNumeric(txtPersonalexp.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtPersonalexp.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Clothing = isNumeric(txtClothing.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtClothing.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Beautybarbershop = isNumeric(txtBeautybarber.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtBeautybarber.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Recreation = isNumeric(txtRecreation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtRecreation.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.Contributions = isNumeric(txtContributions.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtContributions.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Clubdues = isNumeric(txtClubdues.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtClubdues.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Gifts = isNumeric(txtGifts.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtGifts.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Miscellaneous = isNumeric(txtMiscellaneous.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtMiscellaneous.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.OTHERLoans = isNumeric(txtOtherLoans.Text, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(txtOtherLoans.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.CARDPayments = isNumeric(txtCardPayments.Text, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(txtCardPayments.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carcurrent = isNumeric(ddlPayAutoCur.SelectedItem.Value, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(ddlPayAutoCur.SelectedItem.Value.Replace("$", "")) : 0;
            UserMonthlyExpensesBCHResults = App.Credability.UserMonthlyExpensesBCHAddUpdate(userMonthlyExpensesBCH);

            if (UserMonthlyExpensesBCHResults.IsSuccessful)
            {
                userMonthlyExpensesExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
                userMonthlyExpensesExtraHudAbt.includeFha = ddlIsFhaInc.SelectedItem.Value.Trim();
                userMonthlyExpensesExtraHudAbt.moEquity = isNumeric(txtMoEquity.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMoEquity.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.mo2ndMort = isNumeric(txtMo2ndMort.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMo2ndMort.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.moFee = isNumeric(txtMoFee.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMoFee.Text) : 0;
                userMonthlyExpensesExtraHudAbt.utlElectric = isNumeric(txtUtlElectric.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlElectric.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlWater = isNumeric(txtUtlWater.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlWater.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlTv = isNumeric(txtUtlTv.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlTv.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlTv = isNumeric(txtUtlTrash.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlTrash.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlGas = isNumeric(txtUtlGas.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlGas.Text.Replace("$", "")) : 0;
                UserMonthlyExpensesExtraHudAbtResults = App.Credability.UserMonthlyExpensesExtraHudAbtAddUpdate(userMonthlyExpensesExtraHudAbt);
                Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
            }
            else
            {
                //Response.Redirect("");
                // put error message here//
            }



        }
        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle,
                System.Globalization.CultureInfo.CurrentCulture, out result);
        }

        protected void btnMonthlyExpensesCon_Click(object sender, EventArgs e)
        {
            //Update The Information
            UserMonthlyExpensesBCHResult UserMonthlyExpensesBCHResults = null;
            UserMonthlyExpensesBCH userMonthlyExpensesBCH = new UserMonthlyExpensesBCH();

            UserMonthlyExpensesExtraHudAbtResult UserMonthlyExpensesExtraHudAbtResults = null;
            UserMonthlyExpensesExtraHudAbt userMonthlyExpensesExtraHudAbt = new UserMonthlyExpensesExtraHudAbt();



            //For ContactDetails Table
            userMonthlyExpensesBCH.ClientNumber = SessionState.ClientNumber.Value; 
            userMonthlyExpensesBCH.Rentmort = isNumeric(txtRentmort.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtRentmort.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.HomeMaintenance = isNumeric(txtHomemaintenance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtHomemaintenance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Telephone = isNumeric(txtTelephone.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtTelephone.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.Foodaway = isNumeric(txtFoodaway.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtFoodaway.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Groceries = isNumeric(txtGroceries.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtGroceries.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Vehiclepayments = isNumeric(txtVehiclepayments.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtVehiclepayments.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carcurrent = isNumeric(ddlPayAutoCur.SelectedValue, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(ddlPayAutoCur.SelectedValue.Trim()) : 0;
            userMonthlyExpensesBCH.Carinsurance = isNumeric(txtCarinsurance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtCarinsurance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carmaintenance = isNumeric(txtCarmaintenance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtCarmaintenance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.PublicTransportation = isNumeric(txtPublictransportation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtPublictransportation.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Insurance = isNumeric(txtInsurance.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtInsurance.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Medicalprescription = isNumeric(txtMedicalprescription.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtMedicalprescription.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Education = isNumeric(txtEducation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtEducation.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Childsupport = isNumeric(txtChildsupport.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtChildsupport.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Childsupport = isNumeric(txtChildeldercare.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtChildeldercare.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Laundry = isNumeric(txtLaundry.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtLaundry.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Personalexpenses = isNumeric(txtPersonalexp.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtPersonalexp.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Clothing = isNumeric(txtClothing.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtClothing.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Beautybarbershop = isNumeric(txtBeautybarber.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtBeautybarber.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Recreation = isNumeric(txtRecreation.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtRecreation.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.Contributions = isNumeric(txtContributions.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtContributions.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Clubdues = isNumeric(txtClubdues.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtClubdues.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Gifts = isNumeric(txtGifts.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtGifts.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Miscellaneous = isNumeric(txtMiscellaneous.Text, System.Globalization.NumberStyles.Float) ? Convert.ToDecimal(txtMiscellaneous.Text.Replace("$", "")) : 0;

            userMonthlyExpensesBCH.OTHERLoans = isNumeric(txtOtherLoans.Text, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(txtOtherLoans.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.CARDPayments = isNumeric(txtCardPayments.Text, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(txtCardPayments.Text.Replace("$", "")) : 0;
            userMonthlyExpensesBCH.Carcurrent = isNumeric(ddlPayAutoCur.SelectedItem.Value, System.Globalization.NumberStyles.Float) ? Convert.ToInt32(ddlPayAutoCur.SelectedItem.Value) : 0;
            UserMonthlyExpensesBCHResults = App.Credability.UserMonthlyExpensesBCHAddUpdate(userMonthlyExpensesBCH);

            if (UserMonthlyExpensesBCHResults.IsSuccessful)
            {
                userMonthlyExpensesExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
                userMonthlyExpensesExtraHudAbt.includeFha = ddlIsFhaInc.SelectedItem.Value.Trim();
                userMonthlyExpensesExtraHudAbt.moEquity = isNumeric(txtMoEquity.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMoEquity.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.mo2ndMort = isNumeric(txtMo2ndMort.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMo2ndMort.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.moFee = isNumeric(txtMoFee.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtMoFee.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlElectric = isNumeric(txtUtlElectric.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlElectric.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlWater = isNumeric(txtUtlWater.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlWater.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlTv = isNumeric(txtUtlTv.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlTv.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlTv = isNumeric(txtUtlTrash.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlTrash.Text.Replace("$", "")) : 0;
                userMonthlyExpensesExtraHudAbt.utlGas = isNumeric(txtUtlGas.Text, System.Globalization.NumberStyles.Float) ? float.Parse(txtUtlGas.Text.Replace("$", "")) : 0;
                UserMonthlyExpensesExtraHudAbtResults = App.Credability.UserMonthlyExpensesExtraHudAbtAddUpdate(userMonthlyExpensesExtraHudAbt);
                Response.Redirect("UserFinancialSituationRecap.aspx");
            }
            else
            {
                //Response.Redirect("");
                // put error message here//
            }


        }


    }

}