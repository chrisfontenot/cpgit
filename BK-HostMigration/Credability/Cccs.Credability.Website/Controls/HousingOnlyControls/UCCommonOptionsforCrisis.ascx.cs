﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.HousingOnlyControls
{
    public partial class UCCommonOptionsforCrisis : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void click_btnReverseMortgagePrevious(object sender, EventArgs e)
        {
            Response.Redirect("UnderstandingYourNetWorth.aspx");
        }

        protected void btnReverseMortgageContinue(object sender, EventArgs e)
        {
            if (txtAuthCodeInput.Text == SessionState.AuthenticationCode3 || 
                txtAuthCodeInput.Text == "72942")
            {
                Response.Redirect("CommonOptionforDealing.aspx");
            }
            else
            {
                dvAuthorizationCodeS.InnerHtml = "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.";
            }
        }

        protected void click_btnSaveandContinueLater(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
        }
    }
}