﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCMonthlyExpenses.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCMonthlyExpenses" %>
<script language="javascript" type="text/javascript">
    var ReFormat = true, HoldField = "";
    function prepareField(theField) {
        var sValue = theField.value;
        HoldField = sValue;
        sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
        if (sValue <= 0)
            sValue = '';
        theField.value = sValue;
        theField.select();
    }
    function CheckMod(theField) {
        if (HoldField != theField.value && HoldField != "") {
            //document.getElementById("ModForm").value = 1;
            HoldField = "";
        }
    }
    function reformatCCCSCurrency(theField) {
        if (ReFormat) {
            var sValue = theField.value, OutTxt = "", Des;
            var NumStr, Len, i;
            sValue = stripCharsNotInBag(sValue, "0123456789.");
            if (sValue == 0) sValue = '0';
            NumStr = sValue - (sValue % 1);
            NumStr = String(NumStr);
            Len = NumStr.length - 1;
            for (i = 0; i <= Len; i++) {
                OutTxt += NumStr.charAt(i);
                if (i < Len && (Len - i) % 3 == 0) //>
                {
                    OutTxt += ",";
                }
            }
            sValue = '$' + OutTxt;
            theField.value = sValue;
            CheckMod(theField);
        }
        ReFormat = true;
    }
</script>


<script runat="server">

    protected void btnMonthlyContinuePage_Click(object sender, EventArgs e)
    {

    }
</script>

<h1> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpenses|Title")%></h1>
<p><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|IFWT")%></p>
<p><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PETA")%></p>

<!-- Begin the expenses add form -->

<h1><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MFS")%></h1>
    <!-- Monthly Food & Shelter -->
<div class="dvform2col dvformlblbig">
<div class="dvform">
    <div class="dvrow">
    <label> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ROMP")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtRentmort"   runat="server" MaxLength="7"></asp:TextBox>
            <span>*</span><br />
            <asp:RequiredFieldValidator Display="Dynamic" ID="rfvRentmort" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtRentmort" Text="Rent or Mortgage payment is Required."
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtRentmort" Operator="DataTypeCheck" Display="Dynamic"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtRentmort"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|DTMM")%></label>
     <asp:DropDownList ID="ddlIsFhaInc" runat="server">
                <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="N"></asp:ListItem>
            </asp:DropDownList> <span > *</span><br />
            <asp:RequiredFieldValidator ID="rfvIsFhaInc" runat="server" EnableClientScript="true" Display="Dynamic" CssClass="error"
                 SetFocusOnError="true" InitialValue="0" ControlToValidate="ddlIsFhaInc" Text=" insurance or FHA insurance? is Required."
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
    </div>
    <div class="dvrow">
    <label> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MELoan")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtMoEquity" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtMoEquity" runat="server" EnableClientScript="true" Display="Dynamic" CssClass="error"
                InitialValue=""  SetFocusOnError="true"  ControlToValidate="txtMoEquity" Text=" insurance or FHA insurance? is Required."
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtMoEquity" Operator="DataTypeCheck" ValidationGroup="userprofile"
                Type="Double" ErrorMessage="Please enter a valid data" runat="server" Display="Dynamic"
                ID="cmpValtxtMoEquity"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|M2M")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);"  ID="txtMo2ndMort" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvMo2ndMort" runat="server" EnableClientScript="true" CssClass="error"
                InitialValue=""  SetFocusOnError="true"  ControlToValidate="txtMo2ndMort" Display="Dynamic" Text="Monthly 2nd Mortgage is Required."
                ValidationGroup="userprofile">
            </asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtMo2ndMort" Operator="DataTypeCheck" Display="Dynamic"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtMo2ndMort"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MPT")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtPayTax" runat="server"></asp:TextBox>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MHOI")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtPayIns" runat="server"></asp:TextBox>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MAF")%></label>
    <asp:TextBox onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtMoFee" runat="server"  MaxLength="7" ></asp:TextBox>
            <span>*</span><br />
            <asp:RequiredFieldValidator ID="rfvtxtMoFee" runat="server" EnableClientScript="true" Display="Dynamic" CssClass="error"
                InitialValue=""  SetFocusOnError="true"  ControlToValidate="txtMoFee" Text="Monthly Association Fees is Required."
                ValidationGroup="userprofile">
            </asp:RequiredFieldValidator><asp:CompareValidator SetFocusOnError="true"  ControlToValidate="txtMoFee"
                Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtMoFee"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|HM")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtHomemaintenance" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rgvtxtHomemaintenance" runat="server" Display="Dynamic" CssClass="error"
                EnableClientScript="true" InitialValue=""  SetFocusOnError="true"  ControlToValidate="txtHomemaintenance"
                Text="Home Maintenance: (repairs, lawn care,<br>
						pest control) is Required."></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtHomemaintenance"  Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtHomemaintenance"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Ele")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtUtlElectric" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span><br />
            <asp:RequiredFieldValidator ID="rfvtxtUtlElectric" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtUtlElectric" Display="Dynamic" CssClass="error" Text="Electric value is required."></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtUtlElectric" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                    ID="cmpValtxtUtlElectric"></asp:CompareValidator>
   </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|WS")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtUtlWater" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtUtlWater" runat="server" Display="Dynamic" CssClass="error"
                EnableClientScript="true"  SetFocusOnError="true"  ControlToValidate="txtUtlWater" Text="Utl Water Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator><br />
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtUtlWater" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtUtlWater"></asp:CompareValidator>
  </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Gas")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtUtlGas" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtUtlGas" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtUtlGas" Text="Utl Gas Required" Display="Dynamic" CssClass="error" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtUtlGas" Operator="DataTypeCheck" ValidationGroup="userprofile"
                Display="Dynamic" Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                ID="cmpValtxtUtlGas"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CS")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtUtlTv" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtUtlTv" runat="server" EnableClientScript="true"  Display="Dynamic" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtUtlTv" Text="Utl TV Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtUtlTv" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtUtlTv"></asp:CompareValidator>
    </div>   
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TC")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtUtlTrash" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span><asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtUtlTrash" CssClass="error"
                Display="Dynamic" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double"
                ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtUtlTrash"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TPI")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtTelephone" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtTelephone" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtTelephone" Display="Dynamic" Text="Tele Phone Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtTelephone" Operator="DataTypeCheck" ValidationGroup="userprofile"
                Display="Dynamic" Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                ID="cmpValtxtTelephone"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|FAFH")%></label>
     <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtFoodaway" runat="server"  MaxLength="7"></asp:TextBox >
            <span>*</span><br />
            <asp:RequiredFieldValidator ID="rvtxtFoodaway" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtFoodaway" Text="Food away from home  Required" Display="Dynamic"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtFoodaway" Display="Dynamic" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtFoodaway"></asp:CompareValidator>
   </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|FAH")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtGroceries" runat="server"></asp:TextBox>
            <span>*</span><br />
            <asp:RequiredFieldValidator ID="rvtxtGroceries" Display="Dynamic" runat="server" CssClass="error"
                EnableClientScript="true"  SetFocusOnError="true"  ControlToValidate="txtGroceries" Text="Food at home Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
    </div>
    
</div>
<div class="clearboth"> </div>
</div>

<h1><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MT")%></h1>
<!-- Monthly Transportation -->
<div class="dvform2col dvformlblbig">
<div class="dvform">
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AYCO")%></label>
    <asp:DropDownList ID="ddlPayAutoCur" runat="server">
                <asp:ListItem Value="13">-- Select One --</asp:ListItem>
                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="0"></asp:ListItem>
                <asp:ListItem Text="No Payment(s)" Value="2"></asp:ListItem>
            </asp:DropDownList>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AP")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtVehiclepayments" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtVehiclepayments" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtVehiclepayments" Display="Dynamic" Text="Vehicle paymentsr Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtVehiclepayments" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtVehiclepayments"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|AI")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtCarinsurance" runat="server"  MaxLength="7"></asp:TextBox> <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfv" runat="server" EnableClientScript="true" CssClass="error" SetFocusOnError="true"  ControlToValidate="txtCarinsurance"
                Display="Dynamic" Text="Car insurance Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtCarinsurance" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtCarinsurance"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|GAM")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtCarmaintenance" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtCarmaintenance" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtCarmaintenance" Display="Dynamic" Text="Car maintenance Required"
                ValidationGroup="userprofile"> </asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtCarmaintenance" Operator="DataTypeCheck" Display="Dynamic"
                    ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                    runat="server" ID="cmpValtxtCarmaintenance"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PTranp")%></label>
    <asp:TextBox onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtPublictransportation" runat="server"></asp:TextBox>
    </div>

    </div>
    <div class="clearboth"> </div>
    
</div>
  <!-- Monthly Insurance, Medical & Childcare  -->
<h1><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MIMC")%></h1>
<div class="dvform2col dvformlblbig">
<div class="dvform">
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ILMI")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtInsurance" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtInsurance" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtInsurance" Display="Dynamic" Text="Insurance Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtInsurance" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtInsurance" ></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MVAP")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtMedicalprescription" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span><br />
            <asp:RequiredFieldValidator ID="rfvtxtMedicalprescription" runat="server" Display="Dynamic" CssClass="error"
                EnableClientScript="true"  SetFocusOnError="true"  ControlToValidate="txtMedicalprescription" Text="Medical prescription Required"
                ValidationGroup="userprofile"> </asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtMedicalprescription" Operator="DataTypeCheck"
                    ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                    runat="server" ID="cmpValtxtMedicalprescription"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CSA")%></label>
     <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);"         style="text-align:right;"     ID="txtChildsupport" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtChildsupport" runat="server" Display="Dynamic" CssClass="error"
                EnableClientScript="true"  SetFocusOnError="true"  ControlToValidate="txtChildsupport" Text="Child support Required"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtChildsupport" Operator="DataTypeCheck" 
                    ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                    runat="server" ID="cmpValtxtChildsupport"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CCEC")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtChildeldercare" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtChildeldercare" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtChildeldercare" Text="Child elder care Required" Display="Dynamic"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtChildeldercare" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtChildeldercare"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|ERE")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtEducation" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtEducation" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtEducation" Text="Education Required" Display="Dynamic" CssClass="error"
                ValidationGroup="userprofile"> </asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtEducation" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtEducation"></asp:CompareValidator>
    </div>
    </div>
    <div class="clearboth"> </div>
    
</div>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MPME")%></h1>
    <!-- Monthly Personal & Miscellaneous Expenses -->
<div class="dvform2col dvformlblbig">
<div class="dvform">
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TCCP")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtCardPayments" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtCardPayments" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtCardPayments" Text="Card Payments Required" Display="Dynamic" CssClass="error"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtCardPayments" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Integer" ErrorMessage="Please enter a valid data" runat="server"
                    ID="cmpValtxtCardPayments"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|TPOO")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtOtherLoans" runat="server"  MaxLength="7"></asp:TextBox>
            <span style="color: Red; font-weight: bold;">*</span><asp:CompareValidator  SetFocusOnError="true" CssClass="error"  ControlToValidate="txtOtherLoans"
                Display="Dynamic" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Integer"
                ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtOtherLoans"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CCRO")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtContributions" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtContributions" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtContributions" Text="Contributions Required" Display="Dynamic"
                ValidationGroup="userprofile"> </asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtContributions" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                    ID="cmpValtxtContributions"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Clothing")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtClothing" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtClothing" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtClothing" Text="Clothing Required" Display="Dynamic" ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtClothing" Display="Dynamic" Operator="DataTypeCheck" ValidationGroup="userprofile"
                    Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtClothing"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|LDC")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtLaundry" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtLaundry" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtLaundry" Text="Laundry Required" Display="Dynamic" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtLaundry" Operator="DataTypeCheck" ValidationGroup="userprofile"
                Type="Double" ErrorMessage="Please enter a valid data" runat="server" ID="cmpValtxtLaundry"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|PE")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);"  ID="txtPersonalexp" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtPersonalexp" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtPersonalexp" Text="Personal exp Required" Display="Dynamic" CssClass="error"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtPersonalexp" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtPersonalexp"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|BBS")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtBeautybarber" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtBeautybarber" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtBeautybarber" Text="Beauty barber Required" Display="Dynamic"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtBeautybarber" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmptxtBeautybarber"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|RMV")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtRecreation" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtRecreation" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtRecreation" Text="Recreation Required" Display="Dynamic"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator><br />
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtRecreation" Operator="DataTypeCheck"
                ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data"
                runat="server" ID="cmpValtxtRecreation"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|CUD")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtClubdues" runat="server"  MaxLength="7"></asp:TextBox>
            <span style="color: Red; font-weight: bold;">*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtClubdues" runat="server" EnableClientScript="true" CssClass="error"
                 SetFocusOnError="true"  ControlToValidate="txtClubdues" Text="Clubdues Required" Display="Dynamic" ValidationGroup="userprofile"></asp:RequiredFieldValidator><br />
            <asp:CompareValidator  SetFocusOnError="true"  ControlToValidate="txtClubdues" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                ID="cmpValtxtClubdues"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|Gifts")%></label>
    <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtGifts" runat="server"  MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtGifts" runat="server" EnableClientScript="true" CssClass="error"
                Display="Dynamic"  SetFocusOnError="true"  ControlToValidate="txtGifts" Text="Gifts Required" ValidationGroup="userprofile"></asp:RequiredFieldValidator><asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtGifts" Operator="DataTypeCheck" ValidationGroup="userprofile"
                   Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                    ID="cmpValtxtGifts"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label> <%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|MBCM")%></label>
     <asp:TextBox   onBlur="reformatCCCSCurrency (this);"  onFocus="reformatCCCSCurrency(this);" ID="txtMiscellaneous"  runat="server" MaxLength="7"></asp:TextBox>
            <span>*</span>
            <br />
            <asp:RequiredFieldValidator ID="rfvtxtMiscellaneous" runat="server" EnableClientScript="true"
                 SetFocusOnError="true"  ControlToValidate="txtMiscellaneous" Text="Miscellaneous Required" Display="Dynamic" CssClass="error"
                ValidationGroup="userprofile"></asp:RequiredFieldValidator>
                <asp:CompareValidator
                     SetFocusOnError="true"  ControlToValidate="txtMiscellaneous" Operator="DataTypeCheck" ValidationGroup="userprofile" Type="Double" ErrorMessage="Please enter a valid data" runat="server"
                    ID="cmpValtxtMiscellaneous"></asp:CompareValidator>
    </div>
    <div class="dvrow">
    <label></label>
    </div>
    <div class="dvrow">
    <label></label>
    </div>
    <div class="dvrow">
    <label></label>
    </div>
    </div>
    <div class="clearboth"> </div>
    
</div>

<p><%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpensesBCH|WTPR")%></p>


 <div class="dvbtncontainer">
<div class="lnkbutton">
           <asp:LinkButton ID="btnMonthlyPreviousPage" runat="server" CssClass="previous"
                 onclick="btnMonthlyPreviousPage_Click"  CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="btnMonthlyExpensesCon" runat="server"
                 onclick="btnMonthlyExpensesCon_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
           <asp:LinkButton ID="btnMonthlySaveExitPage" runat="server"
                 onclick="btnMonthlySaveExitPage_Click" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
        </div>

</div>