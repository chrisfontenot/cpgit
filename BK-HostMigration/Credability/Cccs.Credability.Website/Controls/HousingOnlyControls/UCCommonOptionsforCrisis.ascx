﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCommonOptionsforCrisis.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCCommonOptionsforCrisis" %>
<h1>Common Option For Crisis</h1>
<p><%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisBCH|IATP")%></p>
<div>
<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|ULCont")%>
</div>
<p><%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|NYCW")%> link 
<a href="#" target='chat29009926' onclick="javascript:window.open('https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToChat&site=29009926&SESSIONVAR!skill=1042&SESSIONVAR!IN%20Number=12345&SESSIONVAR!AuthenticationCode1=&SESSIONVAR!AuthenticationCode2&SESSIONVAR!AuthenticationCode3=&imageUrl=https://server.iad.liveperson.nethttps://onlinecounsel.cccsatl.org/images/chat/&referrer='+escape(document.location),'chat29009926','width=475,height=550,resizable=yes');return false;"><%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|link")%></a> </p>
	
	<div id="dvAuthorizationCodeS" runat="server" style="color:Red;font-weight:bold;text-align:center;"></div>
	<div class="dvform2col">
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|WYARE")%>
	<asp:TextBox ID="txtAuthCodeInput" runat="server" ></asp:TextBox> <b><%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|CVOT")%></b>
	
	<p><%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|ATCC")%></p>
	<div class="clearboth"> </div>
 </div>
 <p>&nbsp;</p>
	



<div class="dvbtncontainer">
<div class="lnkbutton">
<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" OnClick="click_btnReverseMortgagePrevious"><span>Return To Previous Page</span></asp:LinkButton>
  </div>
  <div class="lnkbutton">
  <asp:LinkButton ID="btnReverseMortContinue" runat="server" OnClick="btnReverseMortgageContinue" ><span>Continue</span></asp:LinkButton>
</div>
<div class="lnkbutton">
  <asp:LinkButton ID="btnReverseMortSaveandContinueLater" runat="server" OnClick="click_btnSaveandContinueLater"><span>Save & Continue Later</span></asp:LinkButton>
 </div>
  </div>