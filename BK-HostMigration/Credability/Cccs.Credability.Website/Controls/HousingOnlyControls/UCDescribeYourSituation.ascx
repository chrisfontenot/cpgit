﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDescribeYourSituation.ascx.cs" Inherits="Cccs.Credability.Website.Controls.HousingOnlyControls.UCDescribeYourSituation" %>
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DUS")%>&nbsp;&nbsp;</h1>
<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold;">
</div>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|EPSI")%>&nbsp;&nbsp;
</p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary"
HeaderText="You must enter a value in the following fields:"
DisplayMode="BulletList"
EnableClientScript="true"
runat="server"/>
<div class="dvform2col dvformlblbig">
    <div class="dvform">
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|PTCA")%></label>
            <asp:DropDownList ID="ddlEvent" runat="server">
                <asp:ListItem Value="No" Text="-- Select One --"></asp:ListItem>
                <asp:ListItem Value="01" Text="Over Obligation"></asp:ListItem>
                <asp:ListItem Value="02" Text="Reduced Income"></asp:ListItem>
                <asp:ListItem Value="03" Text="Medical/Accident"></asp:ListItem>
                <asp:ListItem Value="04" Text="Death of a Family Member"></asp:ListItem>
                <asp:ListItem Value="05" Text="Divorce/Separation"></asp:ListItem>
                <asp:ListItem Value="06" Text="Unemployment"></asp:ListItem>
                <asp:ListItem Value="08" Text="Disability"></asp:ListItem>
                <asp:ListItem Value="09" Text="Gambling"></asp:ListItem>
                <asp:ListItem Value="12" Text="Child Support or Alimony"></asp:ListItem>
                <asp:ListItem Value="14" Text="Student Loans"></asp:ListItem>
                <asp:ListItem Value="07" Text="Other"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvEvent" runat="server" 
                Display="Dynamic"  InitialValue="No" 
                 ControlToValidate="ddlEvent" Text="*" ErrorMessage="Event Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TSE")%></label>
            <asp:DropDownList ID="ddlSecEvent" runat="server">
                <asp:ListItem Value="No" Text="-- Select One --"></asp:ListItem>
                <asp:ListItem Value="01" Text="Over Obligation"></asp:ListItem>
                <asp:ListItem Value="02" Text="Reduced Income"></asp:ListItem>
                <asp:ListItem Value="03" Text="Medical/Accident"></asp:ListItem>
                <asp:ListItem Value="04" Text="Death of a Family Member"></asp:ListItem>
                <asp:ListItem Value="05" Text="Divorce/Separation"></asp:ListItem>
                <asp:ListItem Value="06" Text="Unemployment"></asp:ListItem>
                <asp:ListItem Value="08" Text="Disability"></asp:ListItem>
                <asp:ListItem Value="09" Text="Gambling"></asp:ListItem>
                <asp:ListItem Value="12" Text="Child Support or Alimony"></asp:ListItem>
                <asp:ListItem Value="14" Text="Student Loans"></asp:ListItem>
                <asp:ListItem Value="07" Text="Other"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvSecEvent" runat="server" EnableClientScript="true"
                Display="Dynamic" InitialValue="No" 
               ControlToValidate="ddlSecEvent" Text="*" ErrorMessage="Secondary Event Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DTCT")%></label>
            <asp:TextBox ID="txtComment" runat="server" Rows="5" Width="300" TextMode="MultiLine"
                MaxLength="600"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvComment" runat="server" EnableClientScript="true"
                Display="Dynamic"  ControlToValidate="txtComment"
                Text="*" ErrorMessage="Comment Is Required." ></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <p class="col_title">
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IP2Y")%></p>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MEOT")%></label>
            <asp:RadioButtonList ID="rblMedExp" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"
                RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
           <asp:RequiredFieldValidator ID="rfvMedExp" runat="server" ErrorMessage="Credit Counseling Is Required."
                ControlToValidate="rblMedExp"  Display="Dynamic" Text="*"></asp:RequiredFieldValidator>
           
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DSOD")%></label>
            <asp:RadioButtonList ID="rblLostSO" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"
                RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
           <asp:RequiredFieldValidator ID="rfvLostSO" runat="server" EnableClientScript="true"
                Display="Dynamic" 
                ControlToValidate="rblLostSO" Text="*" ErrorMessage="Indicate Divorce, separation or death of a spouse."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|YOFW")%></label>
            <asp:RadioButtonList ID="rblHealthWage" runat="server" RepeatDirection="Horizontal"
                RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="rfvHealthWage" runat="server" EnableClientScript="true"
                Display="Dynamic" 
                ControlToValidate="rblHealthWage" Text="*" ErrorMessage= "You or family with health problems that caused loss of wages is Required.  
."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AEPO")%></label>
            <asp:RadioButtonList ID="rblUnemployed" runat="server" RepeatDirection="Horizontal"
                RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
           <asp:RequiredFieldValidator ID="rfvUnemployed" runat="server" EnableClientScript="true"
                Display="Dynamic" 
                ControlToValidate="rblUnemployed" Text="*" ErrorMessage="An extended period of unemployment (3 months or more) Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LPOA")%></label>
            <asp:RadioButtonList ID="rblTaxLien" runat="server" RepeatDirection="Horizontal"
                RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="rfvTaxLien" runat="server" EnableClientScript="true"
                Display="Dynamic" ControlToValidate="rblTaxLien"
                 Text="*" ErrorMessage="Lien placed on accounts or property for non-payment of taxes Yes No 
Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYEA")%></label>
            <asp:RadioButtonList ID="rblMortRate" runat="server" RepeatDirection="Horizontal"
                RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                <asp:ListItem Value="No">No</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="rfvMortRate" runat="server" EnableClientScript="true"
                Display="Dynamic" ControlToValidate="rblMortRate" 
                 Text="*" ErrorMessage="Have you experienced an adjustment in your mortgage interest rate Yes No Is Required. "></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
           <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TrWYAR")%></p>
        </div>
        <div class="dvrow">
        <label>&nbsp;</label>
            <asp:RadioButtonList ID="rblHousingSituation" runat="server" CellSpacing="5" RepeatLayout="Table" CssClass="DvradiolistAuto"
                            RepeatDirection="Vertical" AutoPostBack="True" OnSelectedIndexChanged="rblHousingSituation_SelectedIndexChanged">
                            <asp:ListItem Value="R">I am Renting.</asp:ListItem>
                            <asp:ListItem Value="B">I am Buying my Home.</asp:ListItem>
                            <asp:ListItem Value="O">I own my home.</asp:ListItem>
                        </asp:RadioButtonList>
        </div>
       
    </div>
    <div class="clearboth">
    </div>
</div>

<asp:Panel ID="dvBuying" runat="server" >
<div class="dvform2col dvformlblbig">
<div class="dvform">
 <div class="dvrow">
     <p><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OPTM")%></p>
        </div>
 <div class="dvrow">
            <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DYQFA")%></label>
            <asp:RadioButtonList ID="rblAffHomeMod" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="Dvradiolist">
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <asp:RequiredFieldValidator ID="rfvAffHomeMod" runat="server" EnableClientScript="true"
                       ControlToValidate="rblAffHomeMod" Text="*" ErrorMessage="Home Affordable Is Required"></asp:RequiredFieldValidator>
        </div>
  </div>
<div class="clearboth"> </div>
</div>

  <div class="dvform2col">
        <div class="colformlft">
        <div class="dvform">
         <div class="dvrow">
        <p class="col_title"> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AYML")%></p>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|NOC")%></label>
        <asp:DropDownList ID="ddlPriLoanServ" runat="server">
         </asp:DropDownList> 
          <br />
         <asp:RequiredFieldValidator ID="rfvPriLoanServ" runat="server" ControlToValidate="ddlPriLoanServ" Display="Dynamic" 
                                EnableClientScript="true" InitialValue="0"  Text="*" ErrorMessage="Name Of Company Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label id="PriLoan1" style="display: none">
         <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OSN")%>
         </label>
         <div id="PriLoan2" style="display: none; float:left">
                                <asp:TextBox ID="txtPriLoanHold" runat="server"></asp:TextBox>
                                <span>*</span>
                            </div>
        </div>
        <div class="dvrow">
        <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LN")%></label>
        <asp:TextBox ID="txtLoanNo" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ControlToValidate="txtLoanNo"
                                EnableClientScript="true" Text="*" ErrorMessage="Loan Number Is Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MT")%></label>
        <asp:DropDownList ID="ddlMortageType" runat="server">
                                <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                                <asp:ListItem Value="CONV">Conventional</asp:ListItem>
                                <asp:ListItem Value="FHA">FHA</asp:ListItem>
                                <asp:ListItem Value="VA">VA</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlMortageType" runat="server" ControlToValidate="ddlMortageType" Display="Dynamic"
                                EnableClientScript="true" InitialValue="0"  Text="*" ErrorMessage="Mortage Type IS Required."></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MTerm")%></label>
        <asp:TextBox ID="txtMortageTerm" runat="server" MaxLength="7" Width="90px"></asp:TextBox>(30/15YRS)
                            <asp:RequiredFieldValidator ID="rfvtxtMortageTerm" runat="server" ControlToValidate="txtMortageTerm" 
                                Display="Dynamic" EnableClientScript="true" Text="*"  ErrorMessage="Mortage Term Is  Required."></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="rvtxtMortageTerm" runat="server" ControlToValidate="txtMortageTerm" 
                                Display="Dynamic" ErrorMessage="Please enter a valid data" Text="*" Operator="DataTypeCheck"
                                 Type="Double"></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DOM")%></label>
        <asp:TextBox ID="txtDateOfMortage" runat="server" MaxLength="20" Width="80px"></asp:TextBox> (MM/DD/YY)
                            <br />
                            <asp:RequiredFieldValidator ID="rvtxtDateOfMortage" runat="server" ControlToValidate="txtDateOfMortage"
                                Display="Dynamic" EnableClientScript="true"  Text="*" ErrorMessage="Date Of Mortage IS Required."></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revtxtDateOfMortage" runat="server" ControlToValidate="txtDateOfMortage"
                                Display="Dynamic" ErrorMessage="Your date must be in the format: MM/DD/YYYY" Text="*"
                                SetFocusOnError="true" ValidationExpression="^([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$" />
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DYMU")%></label>
        <asp:RadioButtonList ID="rblGovNpPro" runat="server" RepeatDirection="Horizontal" CssClass="Dvradiolist"
                                RepeatLayout="Table" RepeatColumns="2">
                                <asp:ListItem Value="YES">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList>
                           
                            <asp:RequiredFieldValidator ID="rvrblGovNpPro" runat="server" ControlToValidate="rblGovNpPro" Display="Dynamic" 
                                EnableClientScript="true"  Text="*" ErrorMessage="Payment Option Required"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IType")%></label>
        <asp:DropDownList ID="ddlIntType" runat="server">
                                <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                                <asp:ListItem Text="Fixed" Value="F">Fixed</asp:ListItem>
                                <asp:ListItem Text="Standard Arm" Value="A">Standard Arm</asp:ListItem>
                                <asp:ListItem Text="Interest Only" Value="I">Interest Only</asp:ListItem>
                                <asp:ListItem Text="Option Arm" Value="O">Option Arm</asp:ListItem>
                            </asp:DropDownList> 
                            <asp:RequiredFieldValidator ID="rfvIntType" runat="server" ControlToValidate="ddlIntType" InitialValue="0"  Display="Dynamic"
                                EnableClientScript="true"  Text="*" ErrorMessage="Interest Type Required" ></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdCurInt")%></label>
         <asp:TextBox ID="txtCurIntRate" runat="server" MaxLength="7"></asp:TextBox>
                           
                            <asp:RequiredFieldValidator ID="rfvCurIntRate" runat="server" ControlToValidate="txtCurIntRate" 
                                Display="Dynamic" EnableClientScript="true"  Text="*" ErrorMessage="Interest Rate Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtCurIntRate" runat="server" ControlToValidate="txtCurIntRate" Text="*"
                                Display="Dynamic" ErrorMessage="Please Enter Proper Interest Rate." Operator="DataTypeCheck"
                                Type="Double" ></asp:CompareValidator>
                            <br />
                            <span id="IntRate2" style="display: none">
                                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OIR")%>&nbsp;&nbsp;
                            </span>
                            <div id="IntRate3" style="display: none; float:left">
                                <asp:TextBox ID="txtOrgIntRate" runat="server" MaxLength="7"></asp:TextBox>
                                <span>*</span>
                                <asp:CompareValidator ID="cmptxtOrgIntRate" runat="server" ControlToValidate="txtOrgIntRate"
                                    ErrorMessage="Please enter a valid data" Operator="DataTypeCheck" SetFocusOnError="true"
                                    Type="Double" ></asp:CompareValidator>
                            </div>
                            <div id="IntRate5" style="display: none; float:left">
                                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdWhatInt")%>&nbsp;&nbsp;
                            </div>
                            <div id="IntRate6" style="display: none; float:left">
                                <asp:TextBox ID="txtMaxIntRate" runat="server" MaxLength="7"></asp:TextBox>
                                <span class="ReqTxt"> <span>*</span></span><br />
                            </div>
                            <asp:CompareValidator ID="cmptxtMaxIntRate" runat="server" ControlToValidate="txtMaxIntRate"
                                ErrorMessage="Please enter a valid data" Operator="DataTypeCheck" SetFocusOnError="true"
                                Type="Double" ></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OLB")%></label>
        <asp:TextBox ID="txtStartBal" runat="server" MaxLength="7"></asp:TextBox> 
                           
                            <asp:RequiredFieldValidator ID="rfvStartBal" runat="server" ControlToValidate="txtStartBal" CssClass="error"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="Original Load Balance Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtStartBal" runat="server" ControlToValidate="txtStartBal"
                                Display="Dynamic" ErrorMessage="Please enter a valid data" Operator="DataTypeCheck"
                                SetFocusOnError="true" Type="Double" ></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CurBal")%></label>
        <asp:TextBox ID="txtCurrentBal" runat="server" MaxLength="7"></asp:TextBox> 
                           
                            <asp:RequiredFieldValidator ID="rfvCurrentBal" runat="server" ControlToValidate="txtCurrentBal" CssClass="error"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="Current Balance Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtCurrentBal" runat="server" ControlToValidate="txtCurrentBal"
                                Display="Dynamic" ErrorMessage="Please enter a valid data" Operator="DataTypeCheck"
                                SetFocusOnError="true" Type="Double" ></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|EVOH")%></label>
        <asp:TextBox ID="txtHomeVal" runat="server" MaxLength="7"></asp:TextBox>
                           
                            <asp:RequiredFieldValidator ID="rfvHomeVal" runat="server" ControlToValidate="txtHomeVal" CssClass="error"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="Home Value Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtHomeVal" runat="server" ControlToValidate="txtHomeVal"
                                Display="Dynamic" ErrorMessage="Please enter a valid data" Operator="DataTypeCheck"
                                SetFocusOnError="true" Type="Double" ></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SMPA")%></label>
        <asp:TextBox ID="txtLoanPay" runat="server" MaxLength="7"></asp:TextBox> 
                           
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtLoanPay" CssClass="error"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="Monthly Payment Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtLoanPay" runat="server" ControlToValidate="txtLoanPay"
                                Display="Dynamic" ErrorMessage="Please enter a valid data" Operator="DataTypeCheck"
                                SetFocusOnError="true" Type="Double" ></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|ATII")%></label>
        <asp:RadioButtonList ID="rblIsTaxInc" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CssClass="Dvradiolist"
                                RepeatLayout="Table">
                                <asp:ListItem Value="YES">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList>
                           
                            <asp:RequiredFieldValidator ID="rfvIsTaxInc" runat="server" ControlToValidate="rblIsTaxInc" Display="Dynamic"
                                EnableClientScript="true"  Text="*" ErrorMessage="Tax Inc Is Required." ></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WITA")%></label>
         <asp:TextBox ID="txtTaxPay" runat="server" MaxLength="7"></asp:TextBox>
                           
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtTaxPay" Display="Dynamic" CssClass="error"
                                EnableClientScript="true"  Text="*" ErrorMessage="Tax Pay Is Required." ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmptxtTaxPay" runat="server" ControlToValidate="txtTaxPay"
                                ErrorMessage="Please Enter Proper Tax Pay." Operator="DataTypeCheck" Text="*"
                                Type="Double" ></asp:CompareValidator>
        </div>
       
        
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IIII")%></label>
        <asp:RadioButtonList ID="rblIsInsIncYes" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" RepeatLayout="Table" CssClass="Dvradiolist">
                                <asp:ListItem Value="YES">Yes</asp:ListItem>
                                <asp:ListItem Value="No">No</asp:ListItem>
                            </asp:RadioButtonList> 
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="rblIsInsIncYes"
                                EnableClientScript="true"  Text="*" ErrorMessage="Insurance Payment Is Required" Display="Dynamic" 
                                ></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WhatInsu")%></label>
        <asp:TextBox ID="txtInsPay" runat="server" MaxLength="7"></asp:TextBox>
                           
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtInsPay"
                                EnableClientScript="true" Text="*" ErrorMessage="Annual Homeowner Insurance Payment Is Required." Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpValtxtInsPay" runat="server" ControlToValidate="txtInsPay"
                                ErrorMessage="Please enter a valid data" Text="*" Operator="DataTypeCheck" SetFocusOnError="true"
                                Type="Double"></asp:CompareValidator>
        </div>
        <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdNumThat")%></label>
        <asp:TextBox ID="txtHouseholdSize" runat="server" MaxLength="7"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rftxtHouseholdSize" runat="server" ControlToValidate="txtHouseholdSize" CssClass="error"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="HouseholdSize Required"
                                ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvtxtHouseholdSize" runat="server" ControlToValidate="txtHouseholdSize"
                                Display="Dynamic" ErrorMessage="Please enter a valid data." Text="*" Operator="DataTypeCheck"
                                Type="Integer" ></asp:CompareValidator>
        </div>
        
        </div>
        </div>
        <div class="colformrht">
        <div class="dvform">
         <div class="dvrow">
         <p class="col_title"><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CLStatus")%></p>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IYPF")%></label>
         <asp:RadioButtonList ID="rblSellingProp" runat="server" RepeatDirection="Horizontal" CssClass="Dvradiolist"
                                    RepeatLayout="Table" RepeatColumns="2">
                                    <asp:ListItem Value="YES">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="rvfSellingProp" runat="server" EnableClientScript="true" Display="Dynamic" 
                                    ControlToValidate="rblSellingProp" Text="*" ErrorMessage="Selling Property Is Required."></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYRAF")%></label>
         <asp:RadioButtonList ID="rblGotNotice" runat="server" RepeatDirection="Horizontal" CssClass="Dvradiolist"
                                    RepeatLayout="Table" RepeatColumns="2">
                                    <asp:ListItem Value="YES">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList> 
                               
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" EnableClientScript="true"
                                    ControlToValidate="rblGotNotice" Text="*" ErrorMessage="Got Notice Is Required." Display="Dynamic"></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYEBI")%></label>
         <asp:RadioButtonList ID="rblPreDmp" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                    <asp:ListItem Value="YES">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList> 
                               
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" Display="Dynamic" 
                                     ControlToValidate="rblPreDmp" Text="*" ErrorMessage="Repayment Plan Is Required."
                                    ></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AYCO")%></label>
          <asp:RadioButtonList ID="rblMortCur" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                </asp:RadioButtonList>
                               <asp:RequiredFieldValidator ID="rfvrblMortCur" runat="server" EnableClientScript="true"  Display="Dynamic"
                                    SetFocusOnError="true" ControlToValidate="rblMortCur" Text="*" ErrorMessage="Mort Current Is Required."
                                   ></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|INHM")%></label>
         <asp:TextBox ID="txtMortLate" runat="server" MaxLength="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtMortLate" runat="server" EnableClientScript="true" Display="Dynamic" CssClass="error"
                                    SetFocusOnError="true" ControlToValidate="txtMortLate" Text="Mort Late Required"
                                    ></asp:RequiredFieldValidator>
                                <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtMortLate" Operator="DataTypeCheck"
                                    Display="Dynamic"  Type="Integer" ErrorMessage="Please enter a valid data"
                                    runat="server" ID="cvtxtMortLate"></asp:CompareValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HMDYC")%></label>
         <asp:TextBox ID="txtMoneyAvail" runat="server" MaxLength="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMoneyAvail" runat="server" EnableClientScript="true" Display="Dynamic" CssClass="error"
                                     ControlToValidate="txtMoneyAvail" Text="*" ErrorMessage="Money Avail Is Required."></asp:RequiredFieldValidator>
                                <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtMoneyAvail" Operator="DataTypeCheck"
                                    Display="Dynamic"  Type="Double" Text="*" ErrorMessage="Please enter a valid data."
                                    runat="server" ID="cvtxtMoneyAvail"></asp:CompareValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WDYL")%></label>
         <asp:TextBox ID="txtLoanDescDate" runat="server" MaxLength="20"></asp:TextBox> (MM/DD/YYYY)
                                <asp:RequiredFieldValidator ID="rfvLoanDescDate" runat="server" EnableClientScript="true"
                                    ControlToValidate="txtLoanDescDate" Text="*" ErrorMessage="When did you last speak with your mortgage company is required." 
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="revtxtLoanDescDate" ControlToValidate="txtLoanDescDate"
                                    ValidationExpression="^([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$"
                                    ErrorMessage="Your date must be in the format: MM/DD/YYYY" Text="*"
                                    Display="Dynamic" />
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WWTO")%></label>
         <asp:TextBox ID="txtLoanDesc" runat="server" TextMode="MultiLine" Width="200px" Rows="3"></asp:TextBox>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|PS")%></label>
         <asp:DropDownList ID="ddlPropStat" runat="server">
                                    <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                                    <asp:ListItem Text="Owner Property Occupied">Owner Property Occupied</asp:ListItem>
                                    <asp:ListItem Text="Owner Property Vacant">Owner Property Vacant</asp:ListItem>
                                    <asp:ListItem Text="Investor Property Occupied">Investor Property Occupied</asp:ListItem>
                                    <asp:ListItem Text="Investor Property Vacant">Investor Property Vacant</asp:ListItem>
                                </asp:DropDownList> 
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" EnableClientScript="true"
                                     ControlToValidate="ddlPropStat" Text="*" ErrorMessage="Property State Is Required." Display="Dynamic" ></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <p class="col_title"> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SHEL")%></p>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DYHA")%></label>
         <asp:RadioButtonList ID="rblSecondLoan" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CssClass="Dvradiolist"
                                    AutoPostBack="True" OnSelectedIndexChanged="rblSecondLoan_SelectedIndexChanged">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:RadioButtonList><br />
                                <asp:RequiredFieldValidator ID="rfvSecondLoan" runat="server" EnableClientScript="true" Display="Dynamic"
                                   ControlToValidate="rblSecondLoan" Text="*" ErrorMessage="Second Loan Is Required."></asp:RequiredFieldValidator>
         </div>
         
         <asp:Panel ID="pnlSeconLoanInfo" runat="server" Visible="false">
          <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|NOC")%></label>
         <asp:DropDownList ID="ddlSecLoanServ" runat="server" Width="150px" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlSecLoanServ_SelectedIndexChanged">
                                    </asp:DropDownList> 
                                    <asp:RequiredFieldValidator ID="rfvSecLoanServ" runat="server" EnableClientScript="true"  Display="Dynamic"
                                        ControlToValidate="ddlSecLoanServ" Text="*" ErrorMessage="Second Loan Is Required."
                                         InitialValue="0"></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow" id="txtOtherCompanyName" runat="server" visible="false">
         <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OSN")%></label>
         <asp:TextBox ID="txtSecLoanHold" runat="server" MaxLength="50"></asp:TextBox> 
                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" Display="Dynamic" 
                                         ControlToValidate="txtSecLoanHold" Text="*" ErrorMessage="Other Company Name IS Required."
                                       ></asp:RequiredFieldValidator>
         
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LN")%></label>
          <asp:TextBox ID="txtLoanNo2" runat="server" MaxLength="50"></asp:TextBox> 
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" Display="Dynamic"
                                        ControlToValidate="txtLoanNo2" Text="*" ErrorMessage="Loan Number Required"
                                       ></asp:RequiredFieldValidator>
         </div>
         <div class="dvrow">
         <label> <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LB")%></label>
         <asp:TextBox ID="txtMtg2Amt" runat="server" MaxLength="7"></asp:TextBox>  
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                        ControlToValidate="txtMtg2Amt" Text="*" ErrorMessage="Loan Balance Required" Display="Dynamic" CssClass="error"
                                        ></asp:RequiredFieldValidator>
                                    <asp:CompareValidator  ControlToValidate="txtMtg2Amt" Operator="DataTypeCheck"
                                        Display="Dynamic"  Type="Double" ErrorMessage="Please enter a valid data" Text="*"
                                        runat="server" ID="cvtxtMtg2Amt"></asp:CompareValidator>
         </div>
         <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LS")%></label>
         <asp:TextBox ID="txtMtg2Stat" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
             ControlToValidate="txtMtg2Stat" Text="*" ErrorMessage="Loan Status Required" Display="Dynamic" 
            ></asp:RequiredFieldValidator>
         </div>
         
         <div class="dvrow">
         <label></label>
         
         </div>
         
         
         
                            <tr>
                                <td valign="top" align="right">
                                    &nbsp;&nbsp;
                                </td>
                                <td valign="top" align="left">
                                    <br />
                                </td>
                            </tr>
                            <tr >
                                <td align="right" valign="top" class="NormTxt">
                                   &nbsp;&nbsp;
                                </td>
                                <td class="ReqTxt" align="left" valign="top">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" class="NormTxt">
                                    &nbsp;&nbsp;
                                </td>
                                <td class="ReqTxt" align="left" valign="top">
                                   
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top">
                                   &nbsp;&nbsp;
                                </td>
                                <td class="ReqTxt" align="left" valign="top">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top" class="NormTxt">
                                    &nbsp;&nbsp;
                                </td>
                                <td class="ReqTxt" align="left" valign="top">
                                    
                                </td>
                            </tr>
                        </asp:Panel>
         
         </div>
         </div>

<div class="clearboth">
        </div>
    </div>  
 
 
 
</asp:Panel>
<asp:Panel ID="dvAuthorizationCode" runat="server">

<div class="dvform2col dvformlblbig">
<div class="dvform">
<div class="dvrow">
<label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdNumInc")%></label>
<asp:TextBox ID="txtHouseholdSizeRent" runat="server" MaxLength="7"></asp:TextBox> 
                <span id="dNoofPeInc" runat="server"> </span>
                <asp:RequiredFieldValidator ID="rfvtxtHouseholdSizeRent" runat="server" EnableClientScript="true" 
                    ControlToValidate="txtHouseholdSizeRent" Text="*" ErrorMessage="Number of People in Household is Required."
                    Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtHouseholdSizeRent" class="error" Operator="DataTypeCheck" Display="Dynamic"  Type="Integer"
                    ErrorMessage="Please enter a valid data" runat="server" ID="cvtxtHouseholdSizeRent"></asp:CompareValidator>
</div>
<div class="dvrow">
 <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|NYCW")%>&nbsp;&nbsp;
                <a href="#" target='chat29009926' onclick="javascript:window.open('https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToChat&site=29009926&SESSIONVAR!skill=1042&SESSIONVAR!IN%20Number=12345&SESSIONVAR!AuthenticationCode1=&SESSIONVAR!AuthenticationCode2&SESSIONVAR!AuthenticationCode3=&imageUrl=https://server.iad.liveperson.nethttps://onlinecounsel.cccsatl.org/images/chat/&referrer='+escape(document.location),'chat29009926','width=475,height=550,resizable=yes');return false;">
                    Click Here</a>

</div>
<div class="dvrow">
<label><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WYARE")%></label>
 <asp:TextBox ID="txtAuthCodeInput" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvtxtAuthCodeInput" runat="server" EnableClientScript="true" Display="Dynamic" 
                     ControlToValidate="txtAuthCodeInput"  Text="*" ErrorMessage="You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed."
                   ></asp:RequiredFieldValidator>
                    <span id="dvAuthorizationCodeS" runat="server" style="color: Red; font-weight: bold;
                    text-align: center;"> </span>
                </div>
<div class="clearboth"> </div>
                    
</div>

<div class="clearboth"> </div>
</div>

 

</asp:Panel>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click"
            ToolTip="Return To Previous" CausesValidation="false"><span>Return To Previous</span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" 
            ToolTip="Continue" CausesValidation="true"><span>Continue</span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveAndExit" runat="server"  ToolTip="Save &amp; Exit"
            OnClick="btnSaveAndExit_Click"><span>Save &amp; Exit</span></asp:LinkButton>
    </div>
</div>

