﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCGetJointSsn.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCGetJointSsn" %>
<style type="text/css">
  .style1
  {
    width: 545px;
  }
  .style2
  {
    width: 211px;
  }
</style>

<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
  function ReSetPhone(theField) {
    var Val = stripCharsNotInBag(theField.value, "1234567890");
    if (Val == "") {
      theField.value = ""
    }
    else {
      theField.value = reformatUSPhone(Val);
    }
  }
  function ReSetSSN(theField) {
    var Val = stripCharsNotInBag(theField.value, "1234567890");
    if (Val == "") {
      theField.value = ""
    }
    else {
      theField.value = reformatSSN(Val);
    }
  }
  function ReSetLastName(theField) {
    var LastName = theField.value;
    LastName = LastName.replace("-", " ");
    theField.value = LastName;
    CleanField(theField);
  }
  function ReSetLastName(theField) {
    var LastName = theField.value;
    LastName = LastName.replace("-", " ");
    theField.value = LastName;
    CleanField(theField);
  }
  function CleanField(theField) {
    var theValue = theField.value;
    theValue = stripCharsNotInBag(theValue, digits + lowercaseLetters + uppercaseLetters + whitespace);
    theField.value = theValue;
  }

  function CheckSsn(sender, args)
  {
    args.IsValid = args.Value.length == 9;
}
  
  function CheckprimaryPhone(sender, args) {

    if (args.Value.length > 14 || args.Value.length < 14 || args.Value.length == 10) {
      args.IsValid = false;
    }
    else {
      args.IsValid = true;
    }

    //args.Value;

  }
</script>

<p align="center">
  <%= Cccs.Credability.Website.App.Translate("Credability|BKC|HDY")%><br />
  <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TFF")%>
</p>
<asp:validationsummary id="DesValidationSummary" cssclass="DvErrorSummary" headertext="You must enter a value in the following fields:"
  displaymode="BulletList" enableclientscript="true" runat="server" forecolor="#A50000" />
<div class="error" id="dvTopErrorMessage" runat="server">
</div>
<div class="dvform2col dvformlblbig">
  <div class="dvform">
    <div class="dvrow">
      <asp:radiobuttonlist id="rblFiling" runat="server" cellspacing="5" repeatdirection="Vertical" autopostback="True"
        onselectedindexchanged="rblFiling_SelectedIndexChanged" cssclass="rdolistsw100">
      </asp:radiobuttonlist>
      <asp:requiredfieldvalidator class="validationMessage" id="RequiredFieldValidator1" runat="server" controltovalidate="rblFiling"
        display="Dynamic" text="" errormessage="Please indicate how you are filing for bankruptcy" setfocusonerror="True"
        cssclass="errexlsgn">
      </asp:requiredfieldvalidator>
    </div>
    <div class="clrbth">
    </div>
  </div>
  <asp:panel id="dvJointForm" runat="server">
    <div class="dvform2col dvformlblbig">
      <div class="dvform">
        <div class="dvrow">
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TUS")%><br />
          <span style="background: red; margin: 15px 0; color: #fff; font-size: 14px; float: left">
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|RE")%></span><br />
          <label style="width: 600px !important; text-align: left; padding: 0; margin: 0">
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TAB")%>
          </label>
        </div>
        <div class="dvrow">
          <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SSNU")%>
          <asp:textbox id="txtjointssn" runat="server" MaxLength="9">
          </asp:textbox><span style="color: Red; font-size: large;">&nbsp;*</span>
          <div class="clrbth">
          </div>
          <span style="padding: 0 0 0 148px; color: #333">
            Note :<%= Cccs.Credability.Website.App.Translate("Credability|BKC|9D")%></span>
          <asp:requiredfieldvalidator class="validationMessage" id="RequiredFieldValidator2" runat="server" controltovalidate="txtjointssn"
            display="Dynamic" text=""  setfocusonerror="True">
          </asp:requiredfieldvalidator>
          <asp:customvalidator class="validationMessage" id="cfvSSN" runat="server" enableclientscript="true" text="" errormessage="Invalid SSN Number.Valid Format xxx-xx-xxxx"
            clientvalidationfunction="CheckSsn" controltovalidate="txtjointssn" display="Dynamic" setfocusonerror="true">
          </asp:customvalidator>
          <p>
          </p>
        </div>
      </div>
    </div>
  </asp:panel>
</div>
<div class="dvbtncontainer">
  <div class="lnkbutton">
    <asp:linkbutton id="btnGetJointSsnContinue" runat="server" onclick="click_btnGetJointSsnContinue">
      <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:linkbutton>
  </div>
</div>
