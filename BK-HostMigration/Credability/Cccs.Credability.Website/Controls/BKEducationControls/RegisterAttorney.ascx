﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterAttorney.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.RegisterAttorney" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
function popwindow(url, width, height) 
{
	var Win = window.open(url,"popupWindow",'width=' + width + ',height=' + height + ',resizable=0,scrollbars=yes');
}
</script>
<asp:validationsummary id="DesValidationSummary" cssclass="DvErrorSummary" displaymode="BulletList"
  EnableClientScript="true" runat="server" forecolor="#A50000" />
<div id="dvErrorMessage" class="DvErrorSummary" runat="server" visible="false">
</div>
<p><b><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|PET")%>1- 2. </b></p>
<p>
  <span style="font-weight: bold;">1.</span> <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YSS")%>
</p>
<%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%>:
  <asp:textbox id="txtSocialSecurityNumber" runat="server" maxlength="9" cssclass="mgrT5">
  </asp:textbox>
  <span class="requiredField">&nbsp;*</span>
 &nbsp; <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|9")%>
 <asp:RequiredFieldValidator class="validationMessage" ID="rfvSocialSecurityNumber" runat="server" ControlToValidate="txtSocialSecurityNumber"
                        EnableClientScript="true" Text="" ErrorMessage=""></asp:RequiredFieldValidator>
  <asp:regularexpressionvalidator class="validationMessage" runat="server" id="RegularExpressionSocialSecurityNumber" EnableClientScript="true"
    controltovalidate="txtSocialSecurityNumber" validationexpression="^[0-9]{9}$" text="" errormessage=""
    display="Dynamic"  />
 

<p>
  <span style="font-weight: bold;">2.</span> <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Y7")%> 
						<a href="javascript:popwindow('CaseNumberWhy.htm',460,120);" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|W")%></a><br><br>
	<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TIN")%><%=DateTime.Now.Year.ToString()%><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TF")%> <%=DateTime.Now.Year.ToString().Substring(2, 2)%>.
						<br><br>
	<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TYI")%>
						<br><br>
	<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|FE")%>
						<span class="MedHead"><%=DateTime.Now.Year.ToString().Substring(2,2)%></span><span  style="font-weight: bold;">-VW-</span><span class="MedHead">12345</span>
						<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|ENR")%> <span style="font-weight: bold;"><%=DateTime.Now.Year.ToString().Substring(2, 2)%>-12345</span>. 
						<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WH")%><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TSO")%>
						<a href="javascript:popwindow('CaseNumberExample.aspx',250,185);" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CH")%></a>.
<p>
<p>
<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|BCN")%>:
  <asp:textbox id="txtBankruptcyCaseNumber" runat="server" maxlength="8" cssclass="mgrT5">
  </asp:textbox>
  <span class="requiredField">&nbsp;*</span>
 &nbsp; <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|7D")%>
 <asp:RequiredFieldValidator class="validationMessage" ID="rfvBankruptcyCaseNumber" runat="server" ControlToValidate="txtBankruptcyCaseNumber"
                        EnableClientScript="true" Text="" ErrorMessage=""></asp:RequiredFieldValidator>
    <asp:HiddenField ID="ConfirmCaseNo" runat="server" />
  </p>  
</p>
<div class="clrbth">
</div>

<div class="dvbtncontainer">
  <div class="lnkbutton">
	  <%--<asp:LinkButton ID="btnRegistration" runat="server" OnClick="btnRegistration_Click"  ValidationGroup="user"><span>Continue</span></asp:LinkButton></div>--%>
    <asp:linkbutton id="btnRegistration" runat="server" onclick="btnRegistration_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:linkbutton>
        <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
            <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
                <ProgressTemplate>
                    <div style="position: relative; top: 30%; text-align: center;">
                        <img src="../images/loading.gif" style="vertical-align: middle" alt="Processing" />
                        <%= Cccs.Credability.Website.App.Translate("Processing")%> ...
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
            BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </div>
</div>

<script type="text/javascript" language="javascript">
    var ModalProgress = '<%= ModalProgress.ClientID %>';
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

    function beginReq(sender, args) {
        // shows the Popup     
        $find(ModalProgress).show();
    }

    function endReq(sender, args) {
        //  shows the Popup
        $find(ModalProgress).hide();
    }
</script>