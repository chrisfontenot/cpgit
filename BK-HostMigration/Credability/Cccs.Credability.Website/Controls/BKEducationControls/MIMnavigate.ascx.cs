﻿using System;
using System.Text;
using System.Data;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
	public partial class MIMnavigate : System.Web.UI.UserControl
	{
		String UID = (SessionState.Username != null) ? SessionState.Username : null;
		Int32 TotalHrs = 0, tpage =0, LastPage =0;
		Int32[] strQuiz = new Int32[8];
		string[] ChTitle = new string[8];
		String nextpath = "", nextname = "";
		StringBuilder sbChap = new StringBuilder();
		StringBuilder sbNextPath = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			ChTitle[0] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TKT"); //"The Key to Getting Ahead";
			ChTitle[1] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|DA");  //"Developing a Budget to Get What You Want";
			ChTitle[2] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|FT");  //"Financial Tools to Help You";
			ChTitle[3] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|CCE"); //"Credit Can Enhance Your Life";
			ChTitle[4] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YCS"); //"Your Credit Score - A Number to Know";
			ChTitle[5] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WYN"); //"What You Need To Know About Contracts";
			ChTitle[6] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|CLT"); //"Consumer Laws to Protect You";
			ChTitle[7] = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WTD"); //"What to Do When Things Go Wrong";

			var dsUsers = App.Credability.MySQLUserRecordGet(SessionState.Username);
			if(Request["savesession"] != null)
			{
				tpage = Convert.ToInt32(Request.QueryString["savesession"].ToString());
				if(dsUsers.Tables[0].Rows.Count > 0)
				{
					DataRow drUsers = dsUsers.Tables[0].Rows[0];
					LastPage = Convert.ToInt32(drUsers["lastpage"].ToString());
				}
				Response.Redirect("Intintion.aspx");
			}

			if(dsUsers.Tables[0].Rows.Count > 0)
			{
				DataRow drUsers2 = dsUsers.Tables[0].Rows[0];
				LastPage = Convert.ToInt32(drUsers2["lastpage"].ToString());
				TotalHrs = Convert.ToInt32(drUsers2["totalhrs"].ToString());
				for(int i = 1; i <= 8; i++)
				{
					strQuiz[i - 1] = Convert.ToInt32(drUsers2["Q" + i.ToString()].ToString());
				}

				var totalTimeLeft = 115 - TotalHrs;
				if(totalTimeLeft < 0)
				{
					totalTimeLeft = 0;
				}
				lblTotHrs1.Text = totalTimeLeft.ToString();

				if(TotalHrs < 115 && LastPage == 18 && Convert.ToInt32(strQuiz[7].ToString()) > -1)
					pnl1.Visible = true;
				else
					pnl2.Visible = true;

				if(LastPage >= 11)
				{
					tpage = LastPage - 10;
					if(tpage > 8)
						tpage = 8;

					for(int j = 1; j <= (tpage - 1); j++)
					{
						sbChap.AppendFormat("<a href='{0}/students/chapters/chapter{1}.php?uid={2}' class='NormLink'>{3}{1}</a>", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), j, UID, Cccs.Credability.Website.App.Translate("Credability|RVMButtons|RC"));
						sbChap.Append("&nbsp;<span style='color:#008000;'><strong>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Completed") + "</strong></span>");
						sbChap.Append("&nbsp;&nbsp;&nbsp;<span class='SmallTxt'>" + ChTitle[j - 1].ToString() + "</span><span class='TnyTxt'><br><br></span>");
					}
					if(strQuiz[tpage - 1] > -1)
					{
						sbChap.AppendFormat("<a href='{0}/students/chapters/chapter{1}.php?uid={2}' class='NormLink'>{3}{1}</a>", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), tpage, UID, Cccs.Credability.Website.App.Translate("Credability|RVMButtons|RC"));
						sbChap.Append("&nbsp;<span style='color:#008000;'><strong>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Completed") + "</strong></span>");
						sbChap.Append("&nbsp;&nbsp;&nbsp;<span class='SmallTxt'>" + ChTitle[tpage - 1].ToString() + "</span><span class='TnyTxt'><br><br></span>");
					}
					lblPhpChapterInfo.Text = sbChap.ToString();
				}

				if(LastPage == 0)
				{
					nextpath = String.Format("{0}/students/tests/pretest.php?uid={1}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), UID);
					nextname = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TPT"); //"The Pre-Test";
				}
				else if(LastPage == 1)
				{
					nextpath = String.Format("{0}/students/chapters/chapter1.php?uid={1}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), UID);
					nextname = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|1"); //"Chapter - 1";
				}
				else if(LastPage >= 11 && LastPage <= 17)
				{
					if(strQuiz[LastPage - 11] > -1)
						tpage = LastPage - 9;
					else
						tpage = LastPage - 10;
					nextpath = String.Format("{0}/students/chapters/chapter{1}.php?uid={2}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), tpage, UID);
					nextname = String.Format("{0}{1}", Cccs.Credability.Website.App.Translate("Credability|RVMButtons|C-"), (tpage));
				}
				else if(LastPage == 18)
				{
					if(strQuiz[7] > -1)
					{
						nextpath = "LoginVal.aspx?redirectURL=students/tests/congrats.php";
						nextname = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TFT"); //"The Final Test";
					}
					else
					{
						nextpath = String.Format("{0}/students/chapters/chapter8.php?uid={1}", CommonFunction.PHPMIMSiteUrl().TrimEnd('/'), UID);
						nextname = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|8"); //"Chapter - 8";
					}
				}

				if(nextpath == "")
					sbNextPath.Append("<h3>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IA") + "</h3>");//Your Next task is Please review the chapters above for another 
				else
				{
					if(TotalHrs < 115 && LastPage == 18 && strQuiz[7] > -1)
					{
						Int32 NextPathHrs = 115 - TotalHrs;
						sbNextPath.Append("<h3>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YN") + "" + nextname + " &nbsp;&nbsp;<span class='ReqTxt'>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|PR") + "" + NextPathHrs.ToString() + " " + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|MIN") + "</span></h3>");
					}
					else
						sbNextPath.Append("<h3>" + Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YN") + "<a href=" + nextpath + " class='BigLink' >" + nextname + "</a></h3>");
				}
				lblNextPath.Text = sbNextPath.ToString();
			}
		}
	}
}