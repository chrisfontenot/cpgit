﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_IntroNew.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UC_IntroNew" %>
<p style="text-align:right;">
    <a href="testimonials.html" target="_blank"><%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|Testimonials")%></a>
</p>
<p>
    <h2><%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|APOPDBE")%></h2>
</p>
<p>
   <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|TYFCCAYG")%>
</p>
<p>
   <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|TMIMYO")%>
</p>
<p>
   <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|T30QPITD")%>
</p>
<p>
				 <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|TCRE")%>
				
					<ul>
						<li>
							<%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|ETYIYPUW")%>
						</li>
						<li>
						    <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|MTYVUSM")%>
							
						</li>
						<li>
						    <%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|ETYOA")%>
							
						</li>
					</ul>
				
</p>  
<p >
					<%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|WRYWN")%>
				</p>
				<p >
					<%= Cccs.Credability.Website.Controls.BKEducationControls.UC_IntroNewPage.GetDisclaimer()%>
				</p>
				<p >
					<%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|IUHAQ")%>
					
				</p>
<!--				<p >
				<%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|ATCIC")%>					
				</p>-->
				
				<p style="text-align:center;">
				<span style="color:#0000FF; font-weight: bold;">
				<%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|CIRW")%>
						
						</span>
				</p>
				
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click"
            ValidationGroup="continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>