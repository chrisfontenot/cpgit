﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCWaiverPath.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCWaiverPath" %>
<h1>
   <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PTS")%></h1>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TYFCS")%>
</p>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSSDI" runat="server" OnClick="btnSSDI_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IQFA")%></span></asp:LinkButton>
    </div>
    <p>
        &nbsp;</p>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnIqualifyforafee" runat="server" OnClick="btnIqualifyforafee_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IQFAF")%></span></asp:LinkButton>
    </div>
    <p>
        &nbsp;</p>
    <div class="lnkbutton fNone">
        <asp:LinkButton ID="btnIwouldliketoapply" runat="server" OnClick="btnIwouldliketoapply_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IWLT")%></span></asp:LinkButton>
    </div>
    <p>
        &nbsp;</p>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnNoIdonot" runat="server" OnClick="btnNoIdonot_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|NIDN")%></span></asp:LinkButton>
    </div>
</div>
