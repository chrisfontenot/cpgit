﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDisclosure.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCDisclosure" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
</div>
<h1>
    Paying for Your Session</h1>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PSY")%></p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="user" />
<div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false"></div>    
<div class="dvform2col dvformlblbig">
    <div class="dvform">
        <div class="dvrow">
            <asp:RadioButtonList ID="rblPaymentType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblPaymentType_SelectedIndexChanged"
                CssClass="rdolists">
                <asp:ListItem Value="C">Draft my Bank Account to pay for my session.</asp:ListItem>
                <asp:ListItem Value="D">Charge my Debit Card to pay for my session.</asp:ListItem>
                <asp:ListItem Value="W">I have paid/would like to pay via Western Union Quick Collect.</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="clearboth">
    </div>
</div>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PCAP")%></p>
<asp:HiddenField ID="hdnCurVancoRef" runat="server" />
<asp:HiddenField ID="hdnPaymentType" runat="server" />
<asp:Panel ID="pnl1" runat="server" Visible="false">
    <p align="center">
        <img border="0" src="../images/blankcheck.gif" height="240" width="390">
    </p>
    <div class="dvform2col dvformlblbig">
        <div class="dvform">
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|RNumber")%></label>
                <asp:TextBox ID="txtabanumber" runat="server" MaxLength="9"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator  class="validationMessage"ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtabanumber"
                    Display="Dynamic" Text="" ErrorMessage=" Routing Number Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" SetFocusOnError="true"
                    ControlToValidate="txtabanumber" ValidationExpression="^[0-9]{9}$" Text="" ErrorMessage="Enter valid Routing Number must be 9 digits"
                    Display="Dynamic" ValidationGroup="user" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|NObank")%></label>
                <asp:TextBox ID="txtbankname" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YANum")%></label>
                <asp:TextBox ID="txtacctnumber" runat="server"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtacctnumber"
                    Display="Dynamic" Text="" ErrorMessage=" Account Number Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BPNum")%></label>
                <asp:TextBox ID="txtbankphone" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOT")%></label>
                <asp:TextBox ID="txtnameoncheck" runat="server"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtnameoncheck"
                    Display="Dynamic" Text="" ErrorMessage=" Name on the Check Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
</asp:Panel>
<!-- Western Union Quick Connect -->
<asp:Panel ID="pnl3" runat="server" Visible="false">
    <table border="0" width="90%" cellpadding="10">
        <tr>
            <td class="NormTxt">
                <p>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTP")%>
                </p>
                <p>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YWN")%>
                    <br />
                    <ol class="NormTxt">
                        <li>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|LFT")%>
                        </li>
                        <li>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PTA")%>
                        </li>
                        <li>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CC")%>
                        </li>
                        <li>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SG")%>
                        </li>
                        <li>
                            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SAN")%>
                        </li>
                    </ol>
                    <p>
                    </p>
                    <p>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHN")%>
                    </p>
                    <p>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYAMY")%>
                    </p>
                    <div class="dvform2col dvformlblbig">
                        <div class="dvform">
                            <div class="dvrow">
                                <label>
                                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|MTCNN")%></label>
                                <asp:TextBox ID="txtmtcn" runat="server" MaxLength="10"></asp:TextBox>
                                <span class="requiredField">&nbsp;*</span>
                                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtmtcn"
                                    Display="Dynamic" Text="" ErrorMessage="MTCN Number Required" SetFocusOnError="True"
                                    ValidationGroup="user"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="clearboth">
                        </div>
                    </div>
                    <p align="center">
                        <br>
                        <img alt="Western Union Example" src="../images/WU2.gif"> </img>
                        <br>
                    </p>
            </td>
        </tr>
    </table>
</asp:Panel>
<!-- Put everything in here for Debit Card info -->
<asp:Panel ID="pnl2" runat="server" Visible="false">
    <div class="dvform2col dvformlblbig">
        <div class="dvform">
            <div>
                <p class="col_title">
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FWB")%></p>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Card")%></label>
                <asp:TextBox ID="txtdcfnameoncard" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtdcmnameoncard" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtdclnameoncard" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtdcfnameoncard"
                    Display="Dynamic" Text="" ErrorMessage=" First Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdcmnameoncard"
                    Display="Dynamic" Text="" ErrorMessage=" Middle Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtdclnameoncard"
                    Display="Dynamic" Text="" ErrorMessage=" Last Name Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator><span
                        style="color: Red; font-size: large">&nbsp;*</span>
            </div>
            <div class="dvrow">
                <p class="col_title">
                    <b>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Billing")%></b></p>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Street")%></label>
                <asp:TextBox ID="txtdcbilladdr1" runat="server" MaxLength="100"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtdcbilladdr1"
                    Display="Dynamic" Text="" ErrorMessage=" Address Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Address")%></label>
                <asp:TextBox ID="txtdcbilladdr2" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|City")%></label>
                <asp:TextBox ID="txtdcbillcity" runat="server" MaxLength="25"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtdcbillcity"
                    Display="Dynamic" Text="" ErrorMessage=" City Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|State")%>
                </label>
                <asp:DropDownList ID="ddlState" runat="server">
                    <asp:ListItem Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="AA">AA</asp:ListItem>
                    <asp:ListItem Value="AE">AE</asp:ListItem>
                    <asp:ListItem Value="AK">AK</asp:ListItem>
                    <asp:ListItem Value="AL">AL</asp:ListItem>
                    <asp:ListItem Value="AP">AP</asp:ListItem>
                    <asp:ListItem Value="AR">AR</asp:ListItem>
                    <asp:ListItem Value="AS">AS</asp:ListItem>
                    <asp:ListItem Value="AZ">AZ</asp:ListItem>
                    <asp:ListItem Value="CA">CA</asp:ListItem>
                    <asp:ListItem Value="CO">CO</asp:ListItem>
                    <asp:ListItem Value="CT">CT</asp:ListItem>
                    <asp:ListItem Value="DC">DC</asp:ListItem>
                    <asp:ListItem Value="DE">DE</asp:ListItem>
                    <asp:ListItem Value="FL">FL</asp:ListItem>
                    <asp:ListItem Value="FM">FM</asp:ListItem>
                    <asp:ListItem Value="GA">GA</asp:ListItem>
                    <asp:ListItem Value="GU">GU</asp:ListItem>
                    <asp:ListItem Value="HI">HI</asp:ListItem>
                    <asp:ListItem Value="IA">IA</asp:ListItem>
                    <asp:ListItem Value="ID">ID</asp:ListItem>
                    <asp:ListItem Value="IL">IL</asp:ListItem>
                    <asp:ListItem Value="IN">IN</asp:ListItem>
                    <asp:ListItem Value="KS">KS</asp:ListItem>
                    <asp:ListItem Value="KY">KY</asp:ListItem>
                    <asp:ListItem Value="LA">LA</asp:ListItem>
                    <asp:ListItem Value="MA">MA</asp:ListItem>
                    <asp:ListItem Value="MD">MD</asp:ListItem>
                    <asp:ListItem Value="ME">ME</asp:ListItem>
                    <asp:ListItem Value="MH">MH</asp:ListItem>
                    <asp:ListItem Value="MI">MI</asp:ListItem>
                    <asp:ListItem Value="MN">MN</asp:ListItem>
                    <asp:ListItem Value="MO">MO</asp:ListItem>
                    <asp:ListItem Value="MP">MP</asp:ListItem>
                    <asp:ListItem Value="MS">MS</asp:ListItem>
                    <asp:ListItem Value="MT">MT</asp:ListItem>
                    <asp:ListItem Value="NC">NC</asp:ListItem>
                    <asp:ListItem Value="ND">ND</asp:ListItem>
                    <asp:ListItem Value="NE">NE</asp:ListItem>
                    <asp:ListItem Value="NH">NH</asp:ListItem>
                    <asp:ListItem Value="NJ">NJ</asp:ListItem>
                    <asp:ListItem Value="NM">NM</asp:ListItem>
                    <asp:ListItem Value="NV">NV</asp:ListItem>
                    <asp:ListItem Value="NY">NY</asp:ListItem>
                    <asp:ListItem Value="OH">OH</asp:ListItem>
                    <asp:ListItem Value="OK">OK</asp:ListItem>
                    <asp:ListItem Value="OR">OR</asp:ListItem>
                    <asp:ListItem Value="PA">PA</asp:ListItem>
                    <asp:ListItem Value="PR">PR</asp:ListItem>
                    <asp:ListItem Value="PW">PW</asp:ListItem>
                    <asp:ListItem Value="RI">RI</asp:ListItem>
                    <asp:ListItem Value="SC">SC</asp:ListItem>
                    <asp:ListItem Value="SD">SD</asp:ListItem>
                    <asp:ListItem Value="TN">TN</asp:ListItem>
                    <asp:ListItem Value="TX">TX</asp:ListItem>
                    <asp:ListItem Value="UT">UT</asp:ListItem>
                    <asp:ListItem Value="VA">VA</asp:ListItem>
                    <asp:ListItem Value="VI">VI</asp:ListItem>
                    <asp:ListItem Value="VT">VT</asp:ListItem>
                    <asp:ListItem Value="WA">WA</asp:ListItem>
                    <asp:ListItem Value="WI">WI</asp:ListItem>
                    <asp:ListItem Value="WV">WV</asp:ListItem>
                    <asp:ListItem Value="WY">WY</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="" ErrorMessage=" Select State" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtdcbillzip" runat="server" MaxLength="5"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="" ErrorMessage=" Select State" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtdcbillzip"
                    Display="Dynamic" Text="" ErrorMessage=" Zip Code Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="RegularExpressionValidator1" SetFocusOnError="true"
                    ControlToValidate="txtdcbillzip" ValidationExpression="^[0-9]{5}$" Text="" ErrorMessage="Enter valid Zip Code must be 5 digits"
                    Display="Dynamic" ValidationGroup="user" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Account")%>
                </label>
                <asp:TextBox ID="txtdcacctnum" runat="server" MaxLength="20"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtdcacctnum"
                    Display="Dynamic" Text="" ErrorMessage=" Account Number Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Exp")%>
                </label>
                <asp:DropDownList ID="ddlCardExMnD" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="">--</asp:ListItem>
                </asp:DropDownList>
                <span class="MedTxt"><font style="color: #000">/</font></span>
                <asp:DropDownList ID="ddlCardExYrD" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="">--</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
</asp:Panel>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ValidationGroup="user"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
