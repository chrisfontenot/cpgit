﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UC_IntroNew : System.Web.UI.UserControl
    {
        private void ShowPageTitle()
        {
            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
               lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|IntroNew|Overview");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowPageTitle();
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("RegisterAttorney.aspx");
        }
    }
}