﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFeeDisclosure.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCFeeDisclosure" %>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TYFC")%>
</p>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BWC")%>
</p>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FDS")%>
</p>
<div>
    <ul>
        <li>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|OFI")%></li>
        <li>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SFCCCS")%></li>
        <li>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|OCA")%>
        </li>
    </ul>
</div>
<p>
    &nbsp;</p>
<div id="dvErrorCont" runat="server" style="display: none" class="ErrorCont">
    <div id="dvError" runat="server">
    </div>
</div>
<div class="dvform2col">
    <div class="dvform">
        <table class="DvradiolistAuto">
            <tr>
                <td style="width:600px">
                    <asp:CheckBox ID="chkUnderstand" runat="server" />
                    <label for="<%=chkUnderstand.ClientID %>" style="width:600px">
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHR")%>
                       <em style="color:#FF0000;font-size:large; font-style:normal;">*</em>
                    </label> 
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div class="clearboth">
    </div>
</div>
</p>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnbkFeeDisclosureContinue" runat="server" OnClick="click_btnbkFeeDisclosureContinue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>