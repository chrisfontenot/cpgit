﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCAppSucess : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnIwouldlike.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }
            SessionState.UserPageArrivalTime = DateTime.Now;
            //CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
            if (SessionState.WaiverType != null && SessionState.WaiverType == "SSD")
                pnlSSD.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "IW")
                pnlIW.Visible = true;

            if (SessionState.WaiverType != null && SessionState.WaiverType == "PB")
                pnlPB.Visible = true;
            lblInNumber.Text = String.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|FJSE"), SessionState.Clid);
            Label lblHeading = new Label();
            lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = App.Translate("AppSuccess|Heading");
        }
        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
        }

        protected void btnIwouldlike_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverWaiver.aspx");
        }
    }
}