﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UC_IntroNewPage : System.Web.UI.UserControl
    {
        public string CharAmount = string.Empty;
        static string showDisclaimer { get; set; }

        private void ShowPageTitle()
        {
            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|BKC|BWB");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowPageTitle();
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (!ZipCodeManager.ZipCodeAvailable(textZipCode.Text.Trim()))
            {
                ValidationError.InnerHtml = "The zip code is invalid. Please correct.";
                return;
            }

            string AttnyName = string.Empty;
            string AttnyEmail = string.Empty;
            SessionState.Escrow = "N";
            SessionState.Escrow6 = "N";
            SessionState.PrimaryZipCode = textZipCode.Text.Trim();

            RegisterAttorney regAttorney = new RegisterAttorney();

            int firmCode = 9999;

            if (!textFirmCode.Text.Trim().IsNullOrEmpty())
            {
                if (int.TryParse(textFirmCode.Text, out firmCode))
                {
                    if (!regAttorney.CheckFirmID(firmCode))
                    {
                        ValidationError.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                        ValidationError.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|TRW") + textFirmCode.Text + App.Translate("Credability|UserProfileBCH|WV") + "</li></ul>";
                        return;
                    }
                    SessionState.FirmID = textFirmCode.Text;
                }
                else
                {
                    ValidationError.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                    ValidationError.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|TRW") + textFirmCode.Text + App.Translate("Credability|UserProfileBCH|WV") + "</li></ul>";
                    return;
                }
            }
            else
            {
                SessionState.FirmID = string.Empty;
                AttnyName = txtattyname.Text.Clean();
                AttnyEmail = txtattyemail.Text.Clean();
            }

            var regNum = regAttorney.CreateMySQLRecord();
            var clid = regAttorney.CreateHostRecord(regNum);
            if (SessionState.SecondaryFlag != "S")
                regAttorney.CreateFeeWaiver(clid);

            // Bankruptcy Conversion
            //App.Host.MimSaveRegisterAttorney(clid, SessionState.CaseNumber, SessionState.ContactSsn, SessionState.FirmID, AttnyName, AttnyEmail, SessionState.Username);
            App.Debtplus.MimSaveRegisterAttorney(regNum, SessionState.CaseNumber, SessionState.ContactSsn, SessionState.FirmID, AttnyName, AttnyEmail, SessionState.Username);

            Account WsAcnt = App.Identity.AccountGet(SessionState.UserId.Value, "WS.CAM");
            if (WsAcnt != null)
            {
                WsAcnt.HostId = clid;
                WsAcnt.InternetId = Convert.ToInt64(clid);
                App.Identity.AccountSave(WsAcnt, SessionState.Username);
            }
            if (SessionState.SecondaryFlag == "S")
                Response.Redirect("ProvideYourPersonalInformation.aspx");
            else
            {
                //they don't have an atty code, so use the default
                if (String.IsNullOrEmpty(textFirmCode.Text))
                {
                    SessionState.FirmID = "9999";
                }
                //Bankruptcy conversion-Seethal
                //double char_amt = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "W").Value;

                double char_amt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "W");
                SessionState.BKFee = char_amt;
                SessionState.ChargeAmount = char_amt.ToString();

                showDisclaimer = App.Translate("Credability|BKC|BothSpouses");

                Response.Redirect("IntroNew.aspx");
            }
        }

        public static string GetDisclaimer()
        {
            return (showDisclaimer);
        }
    }
}