﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MIMnavigate.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.MIMnavigate" %>
<table width="90%" border="0" align="center" cellpadding="6" cellspacing="0">
	<tr>
		<td valign="top" width="6%">
			<br/>
			<p align="center" class="MasiveTxtB">
				<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WB")%>
			</p>
			<hr width="100%"><br/>
			<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="NormTxt"> 
						<h3><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|ThankY")%></h3>
					</td>
				</tr>
			</table>
			<table width="63%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="NormTxt">
						<p>
							<%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|WKT")%>
						<p>
						<asp:panel ID="pnl1" runat="server" Visible="false">
						    <p>
						        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|YH")%> <span class='ReqTxt'>
                                    <asp:Label ID="lblTotHrs1" runat="server" Text=""></asp:Label></span> <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|MT")%><br/>
		                        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TYW")%>
						    </p>
						</asp:panel>
						<asp:panel ID="pnl2" runat="server" Visible="false">
						    <p><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|PC")%><br/></p>
						</asp:panel>
<%--<%                  
	If totalhrs < 115 And lastpage = 18 And Quiz(8) > -1 Then
		Response.Write "You have not spent the required 115 minutes on the education course.<br/> "
		Response.Write "Please review the chapters below for another <span class='ReqTxt'>" & 115 - totalhrs &"</span> minutes to meet the required time.<br/>"
		Response.Write "Then you will be able to take the Final Test and survey to receive your certificate."
	Else
%>--%>
							
<%--<% 
	End If
%>
--%>						<br/><br/>
					</td>
				</tr>
			</table>
			<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="NormTxt"> 
						<h3><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|TRY")%>:</h3>
					</td>
				</tr>
			</table>
			<table width="63%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="NormTxt">
                        <asp:Label ID="lblPhpChapterInfo" runat="server" Text=""></asp:Label>
<%--<%
'chapter overviews + tests -- 8 of 'em
If lastpage >= "11" Then
	tpage = lastpage - 10
	If tpage > 8 Then tpage = 8
	For i=1 to (tpage-1)
		Response.Write "<a href='" & BaseUrl & "/students/chapters/chapter" & i & ".php?uid=" & Session("uid") &"' class='NormLink'>Review Chapter " & i & "</a>"
		Response.Write "&nbsp;<span style='color:#008000;'><strong>Completed</strong></span>"
		Response.Write "&nbsp;&nbsp;&nbsp;<span class='SmallTxt'>" & ChTitle(i) & "</span><span class='TnyTxt'><br/><br/></span>"
	Next
	If Quiz(tpage) > -1 Then
		Response.Write "<a href='" & BaseUrl & "/students/chapters/chapter" & i & ".php?uid=" & Session("uid") &"' class='NormLink'>Review Chapter " & i & "</a>"
		Response.Write "&nbsp;<span style='color:#008000;'><strong>Completed</strong></span>"
		Response.Write "&nbsp;&nbsp;&nbsp;<span class='SmallTxt'>" & ChTitle(i) & "</span><span class='TnyTxt'><br/><br/></span>"
	End If
End If
If totalhrs < 115 And lastpage = 18 And Quiz(8) > -1 Then
'	Response.Write "<span class='NormTxt'>The Final Test&nbsp;&nbsp;</span><span class='ReqTxt'>Incomplete</span><br/>"
End If

nextpath = ""
nextname = ""
If lastpage < "1" Then '>
	nextpath = BaseUrl & "/students/tests/pretest.php?uid=" & Session("uid")
	nextname = "The Pre-Test"
Else
	If lastpage = 18 Then
		If Quiz(8) > -1 Then
			nextpath = BaseUrl & "/students/tests/test.php?uid=" & Session("uid")
			nextname = "The Final Test"
		Else
			nextpath = BaseUrl & "/students/chapters/chapter8.php?uid=" & Session("uid")
			nextname = "Chapter - 8"
		End If
	Else
' next is chapter-test logic
		If lastpage >= 11 Then
			If lastpage <= 17 Then 
				If Quiz(lastpage - 10) > -1 Then
					tpage = lastpage - 9
				Else
					tpage = lastpage - 10
				End If
				nextpath = BaseUrl & "/students/chapters/chapter" & tpage & ".php?uid=" & Session("uid")
				nextname = "Chapter - " & tpage
			End If
		End If
		If lastpage = 1 Then
			nextpath = BaseUrl & "/students/chapters/chapter1.php?uid=" & Session("uid")
			nextname = "Chapter - 1"
		End If
	End If
End If
%>--%>
						<br/><br/>
					</td>
				</tr>
			</table>
			<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="NormTxt"> 
					<asp:Label ID="lblNextPath" runat="server" Text=""></asp:Label>
 
					
<%--<%
If nextpath = "" Then
	Response.Write	"<h3>It appears that you have completed everything.</h3>"
Else 
	If totalhrs < 115 And lastpage = 18 And Quiz(8) > -1 Then
		Response.Write "<h3>Your Next task is " & nextname & " &nbsp;&nbsp;<span class='ReqTxt'>Please review the chapters above for another " & 115 - totalhrs &" minutes</span></h3>"	
	Else
		Response.Write "<h3>Your Next task is <a href=" & nextpath & " class='BigLink' >" & nextname & "</a></h3>"
	End If
End If
%>--%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
