﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UC_IntroNewPage.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UC_IntroNewPage" %>
<div style="width: 100%; color: Red; font-weight: bold" id="ValidationError" runat="server">
</div>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<p style="text-align:right;">
    <a href="testimonials.html" target="_blank"><%= Cccs.Credability.Website.App.Translate("Credability|IntroNew|Testimonials")%></a>
</p>

    <h2><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|PET")%>
    <span > (2).</span></h2>
    <br />
    <p><span>1. </span>
    <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IYA")%></span></p>

<p>
   <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AFC")%>
   <asp:textbox runat="server" id="textFirmCode" style="width: 149px; height: 11px;"></asp:textbox>
   <span>(4-digits)</span>
</p>
<h2>Or</h2>
<p>
   <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CertificateText")%>
</p>	
<div class="dvform2col dvformlblbig">
   <div class="dvform">
      <div class="dvrow">
         <label><%= Cccs.Credability.Website.App.Translate("Credability|BKC|AName")%></label>
         <asp:textbox id="txtattyname" runat="server" maxlength="50"></asp:textbox>
      </div>
      <div class="dvrow">
        <label><%= Cccs.Credability.Website.App.Translate("Credability|BKC|AEA")%></label>
        <asp:textbox id="txtattyemail" runat="server" maxlength="100"></asp:textbox><br/>
        <asp:regularexpressionvalidator class="validationMessage" id="RegularExpressionValidator1" runat="server" EnableClientScript="true" errormessage="Invalid Email Address"
          text="" controltovalidate="txtattyemail" display="Dynamic"  cssclass="error"
          validationexpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
        </asp:regularexpressionvalidator>
      </div>
    </div>
   <div class="clearboth">
  </div>
</div>
<p>2. 
   <%=Cccs.Credability.Website.App.Translate("Credability|BKC|HOMEZIPCODE") %>
   <span><asp:textbox id="textZipCode" runat="server" maxlength="100"></asp:textbox><br/></span>
</p>
	
		
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click"
            ValidationGroup="continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>