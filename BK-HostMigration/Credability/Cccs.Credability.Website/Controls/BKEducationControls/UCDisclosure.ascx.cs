﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCDisclosure : System.Web.UI.UserControl
    {
        private readonly NLog.Logger logger;

        public UCDisclosure()
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            logger.Debug("UCDisclosure Page Load");

            if (!IsPostBack)
            {
                logger.Debug("UCDisclosure Page Load - Not Postback");

                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");

                logger.Debug("UCDisclosure Page Load - Calling UserDisclosureBKCGet");
                UserDisclosureBKCGet(Convert.ToInt32(SessionState.ClientNumber));
                logger.Debug("UCDisclosure Page Load - Call to UserDisclosureBKCGet complete");

                logger.Debug("UCDisclosure Page Load - Calling BindExpdate");
                BindExpdate();
                logger.Debug("UCDisclosure Page Load - Call to BindExpdate complete");
            }

        }
        private void BindExpdate()
        {

            for (int i = 1; i <= 12; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                ddlCardExMnD.Items.Add(li);
            }
            ddlCardExMnD.DataBind();

            for (int CurYear = DateTime.Now.Year; CurYear <= DateTime.Now.Year + 10; CurYear++)
            {
                ListItem liYear = new ListItem();
                liYear.Text = CurYear.ToString().Substring(2);
                liYear.Value = CurYear.ToString().Substring(2);
                ddlCardExYrD.Items.Add(liYear);
            }
            ddlCardExYrD.DataBind();

        }
        public DisclosureContactDetailsBKCounseling UserDisclosureBKCGet(Int32 ClientNumber)
        {
            DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounseling = null;

            DisclosureContactDetailsBKCounseling = App.Credability.DisclosureContactDetailsBKCounselingGet(ClientNumber);

            if (DisclosureContactDetailsBKCounseling != null)
            {
                hdnCurVancoRef.Value = DisclosureContactDetailsBKCounseling.CurVancoRef.ToString();
                rblPaymentType.SelectedValue = (DisclosureContactDetailsBKCounseling.paymentTYPE != null) ? Convert.ToString(DisclosureContactDetailsBKCounseling.paymentTYPE.Trim()) : String.Empty;

                if (rblPaymentType.SelectedValue == "C")
                {
                    txtabanumber.Text = (DisclosureContactDetailsBKCounseling.abaNUMBER != null) ? DisclosureContactDetailsBKCounseling.abaNUMBER : String.Empty;
                    txtbankname.Text = (DisclosureContactDetailsBKCounseling.ADSbankname != null) ? DisclosureContactDetailsBKCounseling.ADSbankname : String.Empty;
                    txtacctnumber.Text = (DisclosureContactDetailsBKCounseling.acctNUMBER != null) ? DisclosureContactDetailsBKCounseling.acctNUMBER : String.Empty;
                    txtbankphone.Text = (DisclosureContactDetailsBKCounseling.ADSbankphone != null) ? DisclosureContactDetailsBKCounseling.ADSbankphone : String.Empty;
                    txtnameoncheck.Text = (DisclosureContactDetailsBKCounseling.adsNAMEONCHECK != null) ? DisclosureContactDetailsBKCounseling.adsNAMEONCHECK : String.Empty;
                }
                else if (rblPaymentType.SelectedValue == "D")
                {
                    DisclosureVancoRefBKCounseling DisclosureVancoRefBKCounseling = null;
                    DisclosureVancoRefBKCounseling = App.Credability.DisclosureVancoRefBKCounselingGet(DisclosureContactDetailsBKCounseling.CurVancoRef);
                    if (DisclosureVancoRefBKCounseling != null)
                    {
                        txtdcfnameoncard.Text = (DisclosureVancoRefBKCounseling.CARDFirstNameEnc != null) ? DisclosureVancoRefBKCounseling.CARDFirstNameEnc : String.Empty;
                        txtdcmnameoncard.Text = (DisclosureVancoRefBKCounseling.CARDMidNameEnc != null) ? DisclosureVancoRefBKCounseling.CARDMidNameEnc : String.Empty;
                        txtdclnameoncard.Text = (DisclosureVancoRefBKCounseling.CARDLastNameEnc != null) ? DisclosureVancoRefBKCounseling.CARDLastNameEnc : String.Empty;
                        txtdcbilladdr1.Text = (DisclosureVancoRefBKCounseling.CARDAddrEnc != null) ? DisclosureVancoRefBKCounseling.CARDAddrEnc : String.Empty;
                        txtdcbilladdr2.Text = (DisclosureVancoRefBKCounseling.CARDAddr2Enc != null) ? DisclosureVancoRefBKCounseling.CARDAddr2Enc : String.Empty;
                        txtdcbillcity.Text = (DisclosureVancoRefBKCounseling.CARDCityEnc != null) ? DisclosureVancoRefBKCounseling.CARDCityEnc : String.Empty;
                        txtdcbillzip.Text = (DisclosureVancoRefBKCounseling.CARDZipEnc != null) ? DisclosureVancoRefBKCounseling.CARDZipEnc : String.Empty;
                        txtdcacctnum.Text = (DisclosureVancoRefBKCounseling.CARDNumberEnc != null) ? DisclosureVancoRefBKCounseling.CARDNumberEnc : String.Empty;
                        ddlState.SelectedValue = (DisclosureVancoRefBKCounseling.CARDSTEnc != null) ? DisclosureVancoRefBKCounseling.CARDSTEnc : String.Empty;
                        ddlCardExMnD.SelectedValue = (DisclosureVancoRefBKCounseling.CARDExpMonEnc != null) ? DisclosureVancoRefBKCounseling.CARDExpMonEnc : String.Empty;
                        ddlCardExYrD.SelectedValue = (DisclosureVancoRefBKCounseling.CARDExpYearEnc != null) ? DisclosureVancoRefBKCounseling.CARDExpYearEnc : String.Empty;
                    }
                }
                else if (rblPaymentType.SelectedValue == "W")
                {
                    txtmtcn.Text = (DisclosureContactDetailsBKCounseling.mtcn != null) ? DisclosureContactDetailsBKCounseling.mtcn : String.Empty;
                }

                if (rblPaymentType.SelectedValue == "C")
                {
                    pnl1.Visible = true;
                    pnl2.Visible = false;
                    pnl3.Visible = false;
                }
                else if (rblPaymentType.SelectedValue == "D")
                {
                    pnl1.Visible = false;
                    pnl2.Visible = true;
                    pnl3.Visible = false;
                }
                else if (rblPaymentType.SelectedValue == "W")
                {
                    pnl1.Visible = false;
                    pnl2.Visible = false;
                    pnl3.Visible = true;
                }

            }

            return DisclosureContactDetailsBKCounseling;
        }
        private DisclosureContactDetailsBKCounselingResult SaveContactAccountDetails(Int32 ClientNumber)
        {

            DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounseling = new DisclosureContactDetailsBKCounseling();
            DisclosureContactDetailsBKCounselingResult Result = null;

            DisclosureContactDetailsBKCounseling.ClientNumber = ClientNumber;
            DisclosureContactDetailsBKCounseling.adsNAMEONCHECK = txtnameoncheck.Text;
            DisclosureContactDetailsBKCounseling.ADSbankname = txtbankname.Text;
            DisclosureContactDetailsBKCounseling.ADSbankphone = txtbankphone.Text;
            DisclosureContactDetailsBKCounseling.abaNUMBER = txtabanumber.Text.Trim();
            DisclosureContactDetailsBKCounseling.acctNUMBER = txtacctnumber.Text.Trim();
            DisclosureContactDetailsBKCounseling.paymentTYPE = rblPaymentType.SelectedValue;
            DisclosureContactDetailsBKCounseling.dcFNAMEONCARD = txtdcfnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcLNAMEONCARD = txtdclnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcMNAMEONCARD = txtdcmnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcBILLADDR = txtdcbilladdr1.Text;
            DisclosureContactDetailsBKCounseling.dcBILLADDR2 = txtdcbilladdr2.Text;
            DisclosureContactDetailsBKCounseling.dcBILLCITY = txtdcbillcity.Text;
            DisclosureContactDetailsBKCounseling.dcBILLSTATE = ddlState.SelectedValue;
            DisclosureContactDetailsBKCounseling.dcBILLZIP = txtdcbillzip.Text;
            DisclosureContactDetailsBKCounseling.dcACCTNUM = txtdcacctnum.Text;
            DisclosureContactDetailsBKCounseling.dcEXPDATE = ddlCardExMnD.SelectedValue + "/" + ddlCardExYrD.SelectedValue;
            DisclosureContactDetailsBKCounseling.mtcn = txtmtcn.Text;
            DisclosureContactDetailsBKCounseling.CurVancoRef = Convert.ToInt32(hdnCurVancoRef.Value);


            Result = App.Credability.DisclosureContactDetailsBKCounselingAddUpdate(DisclosureContactDetailsBKCounseling);

            return Result;
        }

        private IVancoRef SaveVancoRefDetails(Int32 RegInNum)
        {
            var DisclosureVancoRefBKCounseling = new DisclosureVancoRefBKCounseling()
            {
                REGInNum = RegInNum,
                CARDFirstNameEnc = txtdcfnameoncard.Text,
                CARDMidNameEnc = txtdcmnameoncard.Text,
                CARDLastNameEnc = txtdclnameoncard.Text,
                CARDAddrEnc = txtdcbilladdr1.Text.Trim(),
                CARDAddr2Enc = txtdcbilladdr2.Text.Trim(),
                CARDCityEnc = txtdcbillcity.Text,
                CARDSTEnc = ddlState.SelectedValue,
                CARDZipEnc = txtdcbillzip.Text,
                CARDNumberEnc = txtdcacctnum.Text,
                CARDExpMonEnc = ddlCardExMnD.SelectedValue,
                CARDExpYearEnc = ddlCardExYrD.SelectedValue,
                CREATEDTS = DateTime.Now,
            };

            var vancoRef = App.Credability.DisclosureVancoRefAddUpdate(DisclosureVancoRefBKCounseling);

            return vancoRef;
        }
        protected void rblPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblPaymentType.SelectedValue == "C")
            {
                pnl1.Visible = true;
                pnl2.Visible = false;
                pnl3.Visible = false;
            }
            else if (rblPaymentType.SelectedValue == "D")
            {
                pnl1.Visible = false;
                pnl2.Visible = true;
                pnl3.Visible = false;
            }
            else if (rblPaymentType.SelectedValue == "W")
            {
                pnl1.Visible = false;
                pnl2.Visible = false;
                pnl3.Visible = true;
            }
        }


        protected void btnContinue_Click(object sender, EventArgs e)
        {
            DisclosureContactDetailsBKCounselingResult Result = null;
            if (rblPaymentType.SelectedValue == "C")
            {

                abadetailBKCounseling abadetailBKCounseling = null;

                abadetailBKCounseling = App.Credability.AbaDetailsGet(txtabanumber.Text.Trim());
                String NameofBank = "";
                String BankPhoneNumber = "";
                if (abadetailBKCounseling != null)
                {
                    NameofBank = (abadetailBKCounseling.ABAname != null) ? abadetailBKCounseling.ABAname : String.Empty;
                    BankPhoneNumber = (abadetailBKCounseling.ABAphone != null) ? abadetailBKCounseling.ABAphone : String.Empty;

                    txtbankname.Text = NameofBank;
                    txtbankphone.Text = BankPhoneNumber;

                    Result = SaveContactAccountDetails(Convert.ToInt32(SessionState.ClientNumber));
                    if (Result.IsSuccessful == false)
                        dvErrorMessage.InnerHtml = Result.Exception.ToString();
                    else
                        Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/'));
                }
                else
                {
                    dvErrorMessage.InnerHtml = "The Routing# is not found in the national database.";
                }
            }
            else if (rblPaymentType.SelectedValue == "D")
            {
                IVancoRef vancoRef = SaveVancoRefDetails(Convert.ToInt32(hdnCurVancoRef.Value));
                Result = SaveContactAccountDetails(Convert.ToInt32(SessionState.ClientNumber));
                if (Result.IsSuccessful == false)
                    dvErrorMessage.InnerHtml = Result.Exception.ToString();
                else
                    Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/'));

            }
            else if (rblPaymentType.SelectedValue == "W")
            {

                try
                {
                    if (WesternUnion.IsMCTNValid(txtmtcn.Text))
                    {
                        Result = SaveContactAccountDetails(Convert.ToInt32(SessionState.ClientNumber));
                        if (Result.IsSuccessful == false)
                            dvErrorMessage.InnerHtml = Result.Exception.ToString();
                        else
                            Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/'));
                    }
                    else
                        dvErrorMessage.InnerHtml = "Invalid MTCN Number";
                }
                catch (Exception ex)
                {
                    dvErrorMessage.InnerHtml = ex.Message;
                }
              
            }
        }
    }
}