﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
//using Cccs.Host;
using Cccs.Identity;
using Cccs.Web;
using Cccs.Translation;
using System.Web;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCProvideYourPersonalInfo : System.Web.UI.UserControl
    {
        //String CaseNumber = String.Empty;
        private string EmailAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateEmail.");                      // TODO: DONE
        private string SSNAlreadyExistMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|DuplicateSSN.");
        // TODO: DONE
        #region ViewState
        private string SSN_Number_With_Hyphen
        {
            get { return ViewState.ValueGet<string>("SSN_Number_With_Hyphen"); }
            set { ViewState.ValueSet("SSN_Number_With_Hyphen", value); }
        }
        #endregion

        private void ShowPageTitle()
        {
            Label lblHeading = new Label();
            lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|BKE|PI");
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (HttpContext.Current.Request.RawUrl.ToLower().Contains("counseling.credability.org"))
            {
                displayHostId.Visible = false;
            }
            else
            {
                displayHostId.Visible = true;
                displayHostId.InnerText = SessionState.Clid == null ? String.Empty : SessionState.Clid.ToString();
            }

            ShowPageTitle();

            var vancoRef = App.Credability.CurVancoMimRefGet(Convert.ToInt64(SessionState.RegNum));
            if (vancoRef != null && vancoRef.Status != null && vancoRef.Status.Trim() == VancoRef.STATUS_PAYED)
            {
                Response.Redirect("MIMnavigate.aspx");
            }

            // Bankruptcy Conversion
            //var mimData = App.Host.Mim_Disclosure_LoadData(SessionState.Clid);
            var mimData = App.Debtplus.Mim_Disclosure_LoadData(SessionState.Clid);
            if (mimData != null && (!String.IsNullOrEmpty(mimData.AbaNumber) || !String.IsNullOrEmpty(mimData.Mtcn)))
            {
                Response.Redirect("MIMnavigate.aspx");
            }

            if (!IsPostBack)
            {
                InitializeTranslations();
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                btnSaveContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
                LoadFormData();
            }
        }

        private void InitializeTranslations()
        {
            GetRacePickList();
            GetMaritalSataus();
            GetSexPickList();
            GetHispanicPickList();
            GetStatePickList();
            GetNumberInHousePickList();
            GetEducationPickList();
            GetIncomeRangePickList();
            GetBankruptcyTypePickList();
            GetContactAgePickList();
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|FNR");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|UserProfileBCH|LNR");
            RequiredFieldValidator3.ErrorMessage = App.Translate("Credability|UserProfileBCH|AGE");
            RequiredFieldValidator4.ErrorMessage = App.Translate("Credability|UserProfileBCH|MSR");
            RequiredFieldValidator5.ErrorMessage = App.Translate("Credability|UserProfileBCH|UAR");
            RequiredFieldValidator6.ErrorMessage = App.Translate("Credability|UserProfileBCH|UCR");
            RequiredFieldValidator8.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
            RequiredFieldValidator10.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCR");
            rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|ZCNPFR");
            RequiredFieldValidator7.ErrorMessage = App.Translate("Credability|UserProfileBCH|DAYTIME");
            cfvPphone.ErrorMessage = App.Translate("Credability|UserProfileBCH|DAYTIMEPHONE");
            RequiredFieldValidator9.ErrorMessage = App.Translate("Credability|UserProfileBCH|BANK");
            RequiredFieldValidator11.ErrorMessage = App.Translate("Credability|UserProfileBCH|BCRR");
            RequiredFieldValidator12.ErrorMessage = App.Translate("Credability|UserProfileBCH|SSNR");
            RequiredFieldValidator13.ErrorMessage = App.Translate("Credability|BKC|Bankruptcynumber");
        }

        private void GetMaritalSataus()
        {
            MaritalStatus[] maritalStatus = App.Identity.MaritalStatusesGet(SessionState.LanguageCode);
            ddlContactMarital.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
            if ((maritalStatus != null) && (maritalStatus.Length > 0))
            {
                for (int i = 0; i < (maritalStatus.Length - 3); i++)
                {
                    ddlContactMarital.Items.Add(new ListItem(maritalStatus[i].Text, maritalStatus[i].MaritalStatusCode.ToString() ?? maritalStatus[i].MaritalStatusCode.ToString())); // Uses Text if Value is null
                }
            }
        }

        private void GetSexPickList()
        {
            ddlContactSex.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            CommonFunction.AddItems(ddlContactSex.Items, "Sex", SessionState.LanguageCode);
        }

        private void GetRacePickList()
        {
            Race[] race = App.Identity.RacesGet(SessionState.LanguageCode);
            ddlContactRace.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            if ((race != null) && (race.Length > 0))
            {
                for (int i = 0; i < race.Length; i++)
                {
                    ddlContactRace.Items.Add(new ListItem(race[i].Text, race[i].RaceCode.ToString() ?? race[i].RaceCode.ToString())); // Uses Text if Value is null
                }
            }
        }

        private void GetHispanicPickList()
        {
            ddlContactHispanic.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "2"));
            CommonFunction.AddItems(ddlContactHispanic.Items, "Hispanic", SessionState.LanguageCode);
        }

        private void GetStatePickList()
        {
            ddlState.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            string[] statesCodes = App.Geographics.StateCodesGet();
            if (statesCodes != null)
            {
                foreach (string s in statesCodes)
                {
                    ddlState.Items.Add(s);
                }
            }
        }

        private void GetContactAgePickList()
        {
            ddlContactAge.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            ddlContactAge.Items.Add(new ListItem("15-25", "1"));
            ddlContactAge.Items.Add(new ListItem("26-35", "2"));
            ddlContactAge.Items.Add(new ListItem("36-45", "3"));
            ddlContactAge.Items.Add(new ListItem("46-55", "4"));
            ddlContactAge.Items.Add(new ListItem("56-65", "5"));
            ddlContactAge.Items.Add(new ListItem("66+", "6"));
        }

        private void GetNumberInHousePickList()
        {
            ddlNumberInHouse.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            ddlNumberInHouse.Items.Add(new ListItem("1", "1"));
            ddlNumberInHouse.Items.Add(new ListItem("2", "2"));
            ddlNumberInHouse.Items.Add(new ListItem("3", "3"));
            ddlNumberInHouse.Items.Add(new ListItem("4", "4"));
            ddlNumberInHouse.Items.Add(new ListItem("5", "5"));
            ddlNumberInHouse.Items.Add(new ListItem("6", "6"));
            ddlNumberInHouse.Items.Add(new ListItem("7", "7"));
            ddlNumberInHouse.Items.Add(new ListItem("8", "8"));
            ddlNumberInHouse.Items.Add(new ListItem("9", "9"));
            ddlNumberInHouse.Items.Add(new ListItem("10", "10"));
            ddlNumberInHouse.Items.Add(new ListItem("11", "11"));
            ddlNumberInHouse.Items.Add(new ListItem("12", "12"));
        }

        private void GetEducationPickList()
        {
            ddlEducation.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            CommonFunction.AddItems(ddlEducation.Items, "BKEducationOptions", SessionState.LanguageCode);
        }

        private void GetIncomeRangePickList()
        {
            ddlIncomeRange.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            ddlIncomeRange.Items.Add(new ListItem("0-10,000", "1"));
            ddlIncomeRange.Items.Add(new ListItem("10,001 - 20,000", "2"));
            ddlIncomeRange.Items.Add(new ListItem("20,001 - 30,000", "3"));
            ddlIncomeRange.Items.Add(new ListItem("30,001 - 40,000", "4"));
            ddlIncomeRange.Items.Add(new ListItem("40,001 - 50,000", "5"));
            ddlIncomeRange.Items.Add(new ListItem("50,001 - 60,000", "6"));
            ddlIncomeRange.Items.Add(new ListItem("60,001 - 70,000", "7"));
            ddlIncomeRange.Items.Add(new ListItem("70,001+", "8"));
        }

        private void GetBankruptcyTypePickList()
        {
            ddlBankruptcyType.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            CommonFunction.AddItems(ddlBankruptcyType.Items, "BKEducationChapterOptions", SessionState.LanguageCode);
        }

        private long? SecondaryUserDetailId
        {
            get { return ViewState.ValueGet<long?>("SecondaryUserDetailId"); }
            set { ViewState.ValueSet("SecondaryUserDetailId", value); }
        }
        private long? UserAddressId
        {
            get { return ViewState.ValueGet<long?>("UserAddressID"); }
            set { ViewState.ValueSet("UserAddressID", value); }
        }

        private long? PrimaryUserDetailId
        {
            get { return ViewState.ValueGet<long?>("PrimaryUserDetailId"); }
            set { ViewState.ValueSet("PrimaryUserDetailId", value); }
        }

        private void LoadFormData()
        {
            GetUserDetailsFromIDM(SessionState.UserId.Value);

            // Bankruptcy Conversion
            //Cccs.Host.MimForm7LoadDataResult resultLoadData = new Cccs.Host.MimForm7LoadDataResult();
            //resultLoadData = App.Host.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
            var resultLoadData = App.Debtplus.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);

            if (resultLoadData.IsSuccessful)
            {
                txtUid.Text = SessionState.Username;

                //Mel - 05/17/2013 
                //  If the bk case number is blank, use the number that was given on
                //  the "RegisterAttorney" page.
                if (resultLoadData.casenumber.IsNullOrEmpty() && SessionState.CaseNumber.IsNullOrEmpty())
                    EnableBKCaseNumField();
                else if (resultLoadData.casenumber.IsNullOrEmpty())
                    txtCaseNumber.Text = SessionState.CaseNumber;
                else
                    txtCaseNumber.Text = resultLoadData.casenumber;

                txtContactZip.Text = SessionState.PrimaryZipCode;

                //First, see if the firm info were entered in the previous page. If true, use them.
                if (!SessionState.FirmName.IsNullOrEmpty())
                    txtAttyname.Text = SessionState.FirmName;
                else
                    txtAttyname.Text = resultLoadData.attyname;

                if (!SessionState.AttorneyEmail.IsNullOrEmpty())
                    txtAttyemail.Text = SessionState.AttorneyEmail;
                else
                    txtAttyemail.Text = resultLoadData.attyemail;

                //First, see if the firm code was entered in the previous page. If true, use it.
                if (!SessionState.FirmID.IsNullOrEmpty())
                    txtFirmCode.Text = SessionState.FirmID;
                else
                {
                    if (resultLoadData.firmcode == "9999")
                        txtFirmCode.Text = String.Empty;
                    else
                        txtFirmCode.Text = resultLoadData.firmcode;
                }

                ddlBankruptcyType.SelectedValue = resultLoadData.bankruptcy_type;
                if (SessionState.ContactSsn.IsNullOrEmpty())
                    EnableSSNField();
                else
                    ShowSSNNumber();

                if (String.IsNullOrEmpty(SessionState.CaseNumber))
                    SessionState.CaseNumber = resultLoadData.casenumber;
                ddlNumberInHouse.SelectedValue = resultLoadData.numberinhouse;
                ddlContactSex.SelectedValue = resultLoadData.contact_sex;
            }
            else
            {
                txtUid.Text = SessionState.Username;
                ShowSSNNumber();
                txtCaseNumber.Text = SessionState.CaseNumber;
            }

            var dsUsers = App.Credability.UserinfoMySqlGet(SessionState.RegNum.ToString());
            if (dsUsers.Tables[0].Rows.Count != 0)
            {
                DataRow drUsers = dsUsers.Tables[0].Rows[0];
                ddlEducation.SelectedValue = drUsers["educationlvl"].ToString();
                ddlContactAge.SelectedValue = drUsers["age"].ToString();
                ddlIncomeRange.SelectedValue = drUsers["income"].ToString();
                ddlNumberInHouse.SelectedValue = drUsers["numhousehold"].ToString();
            }
        }

        private void ShowSSNNumber()
        {
            string SSNNumber = string.Empty;
            SSN_Number_With_Hyphen = string.Empty;

            char[] Ssn = SessionState.ContactSsn.Replace("-", "").ToCharArray();
            for (int count = 0; count < SessionState.ContactSsn.Replace("-", "").Length; count++)
            {
                if (count <= 4)
                {

                    if (count % 2 == 0 && count != 0)
                    {
                        SSN_Number_With_Hyphen += Ssn[count].ToString() + "-";
                        SSNNumber += "*" + "-";
                    }
                    else
                    {
                        SSNNumber += "*";
                        SSN_Number_With_Hyphen += Ssn[count].ToString();
                    }

                }
                else
                {
                    SSNNumber += Ssn[count].ToString();
                    SSN_Number_With_Hyphen += Ssn[count].ToString();
                }
            }

            txtContactSsn.Text = SSNNumber;//SessionState.ContactSsn;
        }

        public void GetUserDetailsFromIDM(long user_id)
        {
            // Address
            //
            GetUserAddressFromIDM(user_id);

            // Primary UserDetail
            //
            UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);

            if (user_detail_primary != null)
            {
                PrimaryUserDetailId = user_detail_primary.UserDetailId;

                txtContactFirstname.Text = user_detail_primary.FirstName.Clean();
                txtContactInitial.Text = user_detail_primary.MiddleName.Clean();
                txtContactLastname.Text = user_detail_primary.LastName.Clean();
                if (!string.IsNullOrEmpty(user_detail_primary.Ssn))
                {
                    SessionState.ContactSsn = user_detail_primary.Ssn;
                    ShowSSNNumber();
                }
                //txtDayPhone.Text = App.DateString(user_detail_primary.BirthDate);
                txtDayPhone.Text = user_detail_primary.PhoneHome.Clean();
                txtNitePhone.Text = user_detail_primary.PhoneWork.Clean();
                txtContactEmail.Text = user_detail_primary.Email.Clean();
                //by ravi

                switch (user_detail_primary.IsHispanic)
                {
                    case true:
                        ddlContactHispanic.SelectedIndex = 1;
                        break;
                    case false:
                        ddlContactHispanic.SelectedIndex = 2;
                        break;
                    default: //null
                        ddlContactHispanic.SelectedIndex = 0;
                        break;

                }

                switch (user_detail_primary.IsMale)
                {
                    case true:
                        ddlContactSex.SelectedIndex = 1;
                        break;
                    case false:
                        ddlContactSex.SelectedIndex = 2;
                        break;
                    default: //null
                        ddlContactSex.SelectedIndex = 0;
                        break;

                }
                ddlContactMarital.SelectedValue = (user_detail_primary.MaritalStatus != null) ? user_detail_primary.MaritalStatus.MaritalStatusCode.ToString() : string.Empty;
                ddlContactRace.SelectedValue = (user_detail_primary.Race != null) ? user_detail_primary.Race.RaceCode.ToString() : string.Empty;
            }
        }

        private void GetUserAddressFromIDM(long user_id)
        {
            Address[] address = App.Identity.AddressesGet(user_id);

            if ((address != null) && (address.Length != 0))
            {
                //UserAddressId = address[0].AddressId;
                txtContactAddress.Text = address[0].StreetLine1.Clean();
                txtContactAddress2.Text = address[0].StreetLine2.Clean();
                txtContactCity.Text = address[0].City.Clean();
                txtContactZip.Text = address[0].Zip.Clean();
                ddlState.SelectedValue = address[0].State.Clean();

                SessionState.PrimaryZipCode = address[0].Zip.Clean();
            }
            //UserInfoFromContactDetails(Convert.ToInt32(SessionState.ClientNumber));
        }

        public void UserInfoFromContactDetails(int ClientNumber)
        {
            UserContactDetails UserContactDetail = App.Credability.UserInfoGet(ClientNumber);
            if (UserContactDetail != null)
            {
                txtBirthCity.Text = UserContactDetail.BirthOfCity.Clean();
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            ProcessClick(sender, e, false);
        }

        protected void btnSaveExit_Click(object sender, EventArgs e)
        {
            ProcessClick(sender, e, true);
        }

        protected void ProcessClick(object sender, EventArgs e, bool saveExit)
        {
            //Validate all inputs first to avoid sql injection.
            string buf;
            bool flag = true;

            if (!ValidateInput(txtContactFirstname.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactLastname.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactInitial.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactSsn.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactAddress.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactAddress2.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactCity.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtBirthCity.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtContactEmail.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtDayPhone.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtNitePhone.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtAttyname.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtAttyemail.Text, out buf))
                flag = false;
            else if (!ValidateInput(txtFirmCode.Text, out buf))
                flag = false;

            if (!flag)
            {
                CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, "Invalid Character in input: { " + buf + " }", true);
                return;
            }

            App.SetEscrowInfo();
            string CharType = String.Empty, CharAmt = String.Empty;

            #region Get The Char Type And Set them in Session for secondry Person
            // Bankruptcy Conversion
            //MimForm7MainLineResult ResultCharType = App.Host.Mim_Form7_MainLine(ddlState.SelectedItem.Value.ToString().Trim());
            var ResultCharType = App.Debtplus.Mim_Form7_MainLine(ddlState.SelectedItem.Value.ToString().Trim());
            CharType = ResultCharType.CharType;
            CharAmt = ResultCharType.CharAmt;
            if (string.IsNullOrEmpty(CharType) || CharType == "p")
                CharType = "H";
            if (string.IsNullOrEmpty(CharAmt))
                CharAmt = "50.00";
            #endregion

            if (!txtContactSsn.ReadOnly)
                SessionState.ContactSsn = txtContactSsn.Text;

            //SessionState.ChargeAmount = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode,
            //          App.WebsiteCode == Cccs.Identity.Website.BKDE ? "W" : "B").Value.ToString();
            SessionState.ChargeAmount = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode,
                      App.WebsiteCode == Cccs.Identity.Website.BKDE ? "W" : "B").ToString();

            if (SessionState.CanJoinSSN)
                //Bankruptcy conversion-Seethal

                //App.Host.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
                App.Debtplus.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);

            //06/24/2013 Mel
            // Attorney's code can not be blank, less than or greater than 4 digit. Else substitute it for 9999.
            if (txtFirmCode.Text.IsNullOrEmpty() || (txtFirmCode.Text.Length != 4))
                txtFirmCode.Text = "9999";

            SessionState.FirmID = txtFirmCode.Text;

            //06/24/2013 Mel
            //Update the user detail info before send it to host.
            long? clNo = SessionState.UserId;
            SaveUserDetailPrimary((long)SessionState.UserId);

            //Update host
            var hostLanguage = SessionState.LanguageCode == Language.EN ? Constants.HostLanguage.English : Constants.HostLanguage.Spanish;

            // Bankruptcy Conversion
            //Result resSave = App.Host.Mim_Form7_SaveData(SessionState.Clid, txtContactLastname.Text
            //    , txtContactFirstname.Text, txtContactInitial.Text, SessionState.ContactSsn
            //    , txtContactAddress.Text, txtContactAddress2.Text, txtContactCity.Text
            //    , ddlState.SelectedItem.Value, SessionState.PrimaryZipCode, ddlContactMarital.SelectedItem.Value
            //    , ddlContactSex.SelectedItem.Value, ddlContactRace.SelectedItem.Value
            //    , ddlContactHispanic.SelectedItem.Value, txtBirthCity.Text, txtContactEmail.Text
            //    , txtDayPhone.Text, txtNitePhone.Text, txtAttyname.Text, txtAttyemail.Text
            //    , SessionState.FirmID, SessionState.Escrow, SessionState.SecondaryFlag, CharType
            //    , txtCaseNumber.Text, ddlBankruptcyType.SelectedItem.Value
            //    , ddlIncomeRange.SelectedItem.Value, ddlNumberInHouse.SelectedItem.Value, hostLanguage, SessionState.ChargeAmount);
            var resSave = App.Debtplus.Mim_Form7_SaveData(SessionState.Clid, txtContactLastname.Text
                , txtContactFirstname.Text, txtContactInitial.Text, SessionState.ContactSsn
                , txtContactAddress.Text, txtContactAddress2.Text, txtContactCity.Text
                , ddlState.SelectedItem.Value, SessionState.PrimaryZipCode, ddlContactMarital.SelectedItem.Value
                , ddlContactSex.SelectedItem.Value, ddlContactRace.SelectedItem.Value
                , ddlContactHispanic.SelectedItem.Value, txtBirthCity.Text, txtContactEmail.Text
                , txtDayPhone.Text, txtNitePhone.Text, txtAttyname.Text, txtAttyemail.Text
                , SessionState.FirmID, SessionState.Escrow, SessionState.SecondaryFlag, CharType
                , txtCaseNumber.Text, ddlBankruptcyType.SelectedItem.Value
                , ddlIncomeRange.SelectedItem.Value, ddlNumberInHouse.SelectedItem.Value, hostLanguage, SessionState.ChargeAmount);

            if (CharType == "H" && SessionState.SecondaryFlag == "S")
            {
                // Bankruptcy Conversion
                //Result resSaveWriteOff = App.Host.Mim_Form7_SaveWriteOff(SessionState.Clid, SessionState.RegNum, SessionState.ChargeAmount);//CharAmt.ToString());
                var resSaveWriteOff = App.Debtplus.Mim_Form7_SaveWriteOff(SessionState.Clid, SessionState.RegNum, SessionState.ChargeAmount);//CharAmt.ToString());
            }
            if (updateMySQlB())
            {
                if (UserAddressUpdateIntoIDM(SessionState.UserId.Value) && SaveUserDetailPrimary(SessionState.UserId.Value))//&& UserInfoAddUpdate()
                {
                    if (!saveExit)
                    {
                        if (SessionState.FirmID != "9999")
                        {
                            Response.Redirect("RegistrationVerify.aspx");
                        }
                        else
                        {
                            if (SessionState.Escrow == "Y")
                                Response.Redirect("WaiverWaiver.aspx");
                            else
                                Response.Redirect("AprWait.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
                        Response.Redirect("http.//credability.org");
                    }
                }
            }
        }

        private bool updateMySQlB()
        {
            bool result = false;
            MySqlUsersInfoUpdateResult results = null;
            MySqlUsersInfoUpdate UsersInfoUpdate = new MySqlUsersInfoUpdate();
            UsersInfoUpdate.RegNum = Convert.ToInt32(SessionState.RegNum);
            UsersInfoUpdate.fname = txtContactFirstname.Text.Trim();
            UsersInfoUpdate.lname = txtContactLastname.Text.Trim();
            UsersInfoUpdate.phone = txtDayPhone.Text.Trim();
            UsersInfoUpdate.email = txtContactEmail.Text.Trim();
            UsersInfoUpdate.educationlvl = ddlEducation.SelectedItem.Value.Trim();
            UsersInfoUpdate.gender = ddlContactSex.SelectedItem.Value.Trim();
            UsersInfoUpdate.mstatus = ddlContactMarital.SelectedItem.Value.Trim();
            UsersInfoUpdate.numhousehold = ddlNumberInHouse.SelectedItem.Value.Trim();
            UsersInfoUpdate.age = ddlContactAge.SelectedItem.Value.ToString();
            UsersInfoUpdate.income = ddlIncomeRange.SelectedItem.Value.ToString();

            results = App.Credability.UserInfoMySqlUpdate(UsersInfoUpdate);
            if (results.IsSuccessful)
                result = true;
            return result;
        }


        private bool UserAddressUpdateIntoIDM(long user_id)
        {
            bool is_successful = false;

            Address address = null;

            if (UserAddressId != null)
            {
                Address[] addresses = App.Identity.AddressesGet(user_id);

                if ((addresses != null) && (addresses.Length > 0))
                {
                    address = addresses[0];
                }
            }

            if (address == null)
            {
                address = new Address { UserId = user_id, };
            }

            address.AddressType = "HOME";
            address.StreetLine1 = txtContactAddress.Text.Clean();
            address.StreetLine2 = txtContactAddress2.Text.Clean();
            address.City = txtContactCity.Text.Clean();
            address.Zip = txtContactZip.Text.Clean();
            address.State = !string.IsNullOrEmpty(ddlState.SelectedValue) ? ddlState.SelectedValue : null;

            SessionState.PrimaryZipCode = address.Zip;

            string AuditorName = txtContactFirstname.Text.Trim() + " " + txtContactLastname.Text.Trim();

            AddressSaveResult result = App.Identity.AddressSave(address, AuditorName);

            is_successful = result.IsSuccessful;

            if (is_successful)
            {
                UserAddressId = result.AddressId;
            }
            else
            {
                if (result.IsInvalidZipCode == true)
                {
                    string ErrorMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|InValidState") + " " + result.StateExpectedForZip;     // TODO: Translate

                    CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, ErrorMessage, true);
                    //CommonFunction.ShowErrorMessageAtPagebottom(dvErrorMessage, ErrorMessage, true);
                }

                else if (result.IsInvalidZipCode == false)
                {
                    string ErrorMessage = Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|InValidState") + " " + result.StateExpectedForZip;     // TODO: Translate

                    CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, ErrorMessage, true);
                    //CommonFunction.ShowErrorMessageAtPagebottom(dvErrorMessage, ErrorMessage, true);
                }
                else
                    if (!string.IsNullOrEmpty(result.ExceptionStr))
                {
                    CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, result.ExceptionStr, true);
                    //CommonFunction.ShowErrorMessageAtPagebottom(dvErrorMessage, result.ExceptionStr, true);
                }
            }
            return is_successful;
        }


        public bool ValidateInput(string data, out string invalidChar)
        {
            if (data.Contains("'"))        //Character data string delimiter
                invalidChar = "'";
            else if (data.Contains(";"))   //Query delimiter
                invalidChar = ";";
            else if (data.Contains("--"))  //Comment delimiter
                invalidChar = "--";
            else if (data.Contains("/*"))  // Comment delimiters. Text between /* and */ is not evaluated by the server.
                invalidChar = "/*";
            else if (data.Contains("*/"))  // Comment delimiters. Text between /* and */ is not evaluated by the server.
                invalidChar = "*/";
            else if (data.Contains("xp_"))   //Used at the start of the name of catalog-extended stored procedures, such as xp_cmdshell.
                invalidChar = "xp_";
            else
            {
                invalidChar = "";
                return (true);
            }

            return (false);
        }

        public bool SaveUserDetailPrimary(long user_id)
        {
            bool is_successful = false;

            UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);

            if (user_detail_primary != null)
            {
                user_detail_primary.FirstName = txtContactFirstname.Text.Clean();
                user_detail_primary.LastName = txtContactLastname.Text.Clean();
                user_detail_primary.MiddleName = txtContactInitial.Text.Clean();
                user_detail_primary.Ssn = SSN_Number_With_Hyphen;//SessionState.ContactSsn;//txtContactSsn.Text.Clean();
                user_detail_primary.Email = txtContactEmail.Text.Clean();

                user_detail_primary.PhoneHome = txtDayPhone.Text.Clean();
                user_detail_primary.PhoneWork = txtNitePhone.Text.Clean();
                //by ravi

                switch (ddlContactHispanic.SelectedIndex)
                {
                    case 0:
                        user_detail_primary.IsHispanic = null;
                        break;
                    case 1:
                        user_detail_primary.IsHispanic = true;
                        break;
                    case 2:
                        user_detail_primary.IsHispanic = false;
                        break;
                }

                switch (ddlContactSex.SelectedIndex)
                {
                    case 0:
                        user_detail_primary.IsMale = null;
                        break;
                    case 1:
                        user_detail_primary.IsMale = true;
                        break;
                    case 2:
                        user_detail_primary.IsMale = false;
                        break;

                }
                //ended by ravi

                if (!string.IsNullOrEmpty(ddlContactMarital.SelectedValue))
                {
                    user_detail_primary.MaritalStatus = new MaritalStatus { MaritalStatusCode = ddlContactMarital.SelectedValue[0] };

                }

                if (!string.IsNullOrEmpty(ddlContactRace.SelectedValue))
                {
                    user_detail_primary.Race = new Race { RaceCode = ddlContactRace.SelectedValue[0] };
                }

                string AuditorName = txtContactFirstname.Text.Clean() + " " + txtContactLastname.Text.Clean(); // TODO: SessionState.Username

                UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_primary, AuditorName);

                is_successful = user_detail_save_result.IsSuccessful;

                if (!is_successful)
                {
                    if (user_detail_save_result.IsDuplicateEmail == true)
                    {
                        CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, EmailAlreadyExistMessage, true);
                        //CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, EmailAlreadyExistMessage, true);
                    }

                    if (user_detail_save_result.IsDuplicateSsn == true)
                    {
                        CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, SSNAlreadyExistMessage, true);
                        //CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
                    }

                    if (!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
                    {
#if DEBUG
                        CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, user_detail_save_result.ExceptionStr, true);
                        //CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, SSNAlreadyExistMessage, true);
#endif
                    }
                }
            }

            return is_successful;
        }

        private bool CheckFirmID(int FirmCode)
        {
            bool Back = true;
            string HostServiceError = String.Empty;
            // Bankruptcy Conversion
            //Result<XmlDocument> result = App.Host.GetEscrow(FirmCode);
            var result = App.Debtplus.GetEscrow(FirmCode, App.WebsiteCode);

            if (result != null)
            {
                SessionState.EscrowProbonoValue = result.EscrowProBono;
                SessionState.Escrow = result.Escrow;
                /*if (result.IsLegalAid)
                {
                    SessionState.Escrow6 = "Y";
                }
                if (result.Escrow == 1)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 2 && App.WebsiteCode == Cccs.Identity.Website.BKC)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 3 && App.WebsiteCode == Cccs.Identity.Website.BKDE)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 6)
                {
                    SessionState.Escrow = "Y";
                    SessionState.Escrow6 = "Y";
                }
                else
                {
                    SessionState.Escrow = "N";
                }*/

                SessionState.FirmID = FirmCode.ToString();
                SessionState.FirmName = result.FirmName;
            }
            else
            {
                Back = false;
            }

            //XmlNodeList nodeListError = result.Value.SelectNodes("//Error") as XmlNodeList;
            //if (nodeListError != null && nodeListError.Count != 0)
            //{
            //    HostServiceError = nodeListError.Item(0).InnerText;
            //    if (HostServiceError.Contains("This Record was not found"))
            //        Back = false;
            //}
            //if (Back)
            //{
            //    string Escrow = String.Empty;
            //    XmlNodeList nodeListEscrow = result.Value.SelectNodes("//Escrow") as XmlNodeList;
            //    if (nodeListEscrow != null && nodeListEscrow.Count != 0)
            //    {
            //        switch (nodeListEscrow.Item(0).InnerText.Trim())
            //        {
            //            case "1":
            //                SessionState.Escrow = "Y";
            //                break;
            //            case "3":
            //                SessionState.Escrow = "Y";
            //                break;
            //            case "6":
            //                SessionState.Escrow = "Y";
            //                SessionState.Escrow6 = "Y";
            //                break;
            //            default:
            //                SessionState.Escrow = "N";
            //                break;
            //        }
            //    }
            //    XmlNodeList nodeListFirmName = result.Value.SelectNodes("//Name") as XmlNodeList;
            //    if (nodeListFirmName != null && nodeListFirmName.Count != 0)
            //    {
            //        SessionState.FirmName = nodeListFirmName.Item(0).InnerText.Trim();
            //    }

            /*
            XmlNodeList nodeListChargeAmount = result.Value.SelectNodes("//ChargeAmount") as XmlNodeList;
            if (nodeListChargeAmount != null && nodeListChargeAmount.Count != 0)
            {
                SessionState.ChargeAmount = nodeListChargeAmount.Item(0).InnerText.Trim();
            }
            */
            //}
            return Back;
        }

        protected void EnableSSNField()
        {
            txtContactSsn.BackColor = System.Drawing.Color.White;
            txtContactSsn.ReadOnly = false;
            txtContactSsn.MaxLength = 9;
        }

        protected void EnableBKCaseNumField()
        {
            txtCaseNumber.BackColor = System.Drawing.Color.White;
            txtCaseNumber.ReadOnly = false;
            txtCaseNumber.MaxLength = 8;
        }

    }
}