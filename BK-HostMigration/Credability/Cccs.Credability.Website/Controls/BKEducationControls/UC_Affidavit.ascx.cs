﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UC_Affidavit : System.Web.UI.UserControl
    {
        private void ShowPageTitle()
        {
            var lblHeading = Page.Master.FindControl("lblHeading") as Label;
            if (lblHeading != null)
                lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|Affidavit|Affidavit");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ShowPageTitle();
            chkConfirmation.Text = Cccs.Credability.Website.App.Translate("Credability|Affidavit|I_Agree");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (chkConfirmation.Checked)
                Response.Redirect("RegisterAttorney.aspx");
            else
            {
                CommonFunction.ShowErrorMessageAtPagebottom(dvBottomServerSideErrorMessage, Cccs.Credability.Website.App.Translate("Credability|Affidavit|YMAIOT"), true);
                dvBottomServerSideErrorMessage.Visible = true;
            }
        }
    }
}