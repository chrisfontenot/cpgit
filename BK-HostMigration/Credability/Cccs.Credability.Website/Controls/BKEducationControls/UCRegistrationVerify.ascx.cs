﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCRegistrationVerify : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                btnRegistrationVerify.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                SessionState.UserPageArrivalTime = DateTime.Now;
                if(SessionState.FirmID != null)
                    lblFirmID.Text = SessionState.FirmID.ToString();

                if(SessionState.FirmName != null)
                    lblFirmName.Text = SessionState.FirmName.ToString();
            }

        }

        protected void btnRegistrationVerify_Click(object sender, EventArgs e)
        {
            if(SessionState.Escrow6 == "Y")
                Response.Redirect("SetTrans.aspx");
            else
                Response.Redirect("AprWait.aspx");

        }
    }
}