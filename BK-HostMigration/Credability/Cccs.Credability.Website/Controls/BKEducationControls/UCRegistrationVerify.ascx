﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCRegistrationVerify.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCRegistrationVerify" %>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AIV")%>
</p>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YAF")%>
    <asp:Label ID="lblFirmID" runat="server" Text=""></asp:Label>&nbsp;&nbsp;
    <asp:Label ID="lblFirmName" runat="server" Text=""></asp:Label>
</p>
<p>
<%= Cccs.Credability.Website.App.Translate("Credability|BKC|WWS")%>
</p>
<p>
<a href="ProvideYourPersonalInformation.aspx">
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|ITIN")%></a>
</p>
<div class="dvbtncontainer">
    <div class="lnkbutton">
    <asp:LinkButton ID="btnRegistrationVerify" runat="server" OnClick="btnRegistrationVerify_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>

