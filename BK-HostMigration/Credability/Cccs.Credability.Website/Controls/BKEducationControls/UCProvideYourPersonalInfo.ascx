﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCProvideYourPersonalInfo.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCProvideYourPersonalInfo" %>
<h1 runat="server" ID="displayHostId" ></h1>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
    function ReSetPhone(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatUSPhone(Val);
        }
    }
    function ReSetSSN(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatSSN(Val);
        }
    }
    function ReSetLastName(theField) {
        var LastName = theField.value;
        LastName = LastName.replace("-", " ");
        theField.value = LastName;
        CleanField(theField);
    }
    function ReSetLastName(theField) {
        var LastName = theField.value;
        LastName = LastName.replace("-", " ");
        theField.value = LastName;
        CleanField(theField);
    }
    function CleanField(theField) {
        var theValue = theField.value;
        theValue = stripCharsNotInBag(theValue, digits + lowercaseLetters + uppercaseLetters + whitespace);
        theField.value = theValue;
    }

    function CheckSsn(sender, args) {

        if (args.Value.length > 11 || args.Value.length < 11 || args.Value.length == 0) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }

    }
    function CheckprimaryPhone(sender, args) {

        if (args.Value.length > 14 || args.Value.length < 14 || args.Value.length == 10) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>

<p align="center" class="MasiveTxt" style="color: #003366">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|PI")%><strong></strong>
</p>
<hr width="100%">
<p class="NormTxt">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|EYC")%>
</p>
<p class="NormTxt">
   
        <%= Cccs.Credability.Website.App.Translate("Credability|BKE|CI")%>
        <span class="ReqTxt">*</span>
        <br />
</p>
<p>
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
</p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="user" />
<div id="dvErrorMessage" class="DvErrorSummary" runat="server" visible="false">
</div>    
<div class="dvform2col dvformlblbig">
    
        <div class="dvform">
            <div class="dvrow">
                <p>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|YI")%>
                </p>
               <div class="c-body c-small">
		            <table style="background-color: #FFF2CB;" width="100%">
                     <tr style="font-size:medium">
                        <td> <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Privacy_disclaimer")%>
                        </td>
                     </tr>
                  </table>
               </div>
<%--               
               <div><span style="border:1px; margin-left:80px; margin-right:80px; color: red">
                  Note : <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Privacy_disclaimer")%> 
              </span></div>--%>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|UI")%></label>
                <asp:TextBox ID="txtUid" runat="server" MaxLength="50" BackColor="Silver" ReadOnly="True"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|FirstName")%></label>
                <asp:TextBox ID="txtContactFirstname" runat="server" MaxLength="50"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtContactFirstname"
                    Display="Dynamic" Text="" ErrorMessage=" First Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Initial")%></label>
                <asp:TextBox ID="txtContactInitial" runat="server" MaxLength="50"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|LastName")%></label>
                <asp:TextBox ID="txtContactLastname" runat="server" MaxLength="50"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContactLastname"
                    Display="Dynamic" Text="!" ErrorMessage=" Last Name Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Ssn")%></label>
                <asp:TextBox ID="txtContactSsn" runat="server" MaxLength="50" BackColor="Silver"
                    ReadOnly="True"></asp:TextBox>
                <span style="color: Red; font-size: large;">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtContactSsn"
                    Display="Dynamic" Text="!" ErrorMessage=" Social Security Number Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
           </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|Age")%></label>
                <asp:DropDownList ID="ddlContactAge" runat="server" />
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlContactAge"
                    Display="Dynamic" Text="" ErrorMessage=" Select Age" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|MaritalStatus")%></label>
                <asp:DropDownList ID="ddlContactMarital" runat="server">
                </asp:DropDownList>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlContactMarital"
                    Display="Dynamic" InitialValue="0" Text="!" ErrorMessage=" Select Marital Status" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Sex")%></label>
                <asp:DropDownList ID="ddlContactSex" runat="server">
                </asp:DropDownList>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Race")%></label>
                <asp:DropDownList ID="ddlContactRace" runat="server">
                </asp:DropDownList>
                <span style="color:#333">&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|Hispanic")%>?:</span>
                <asp:DropDownList ID="ddlContactHispanic" runat="server">
                </asp:DropDownList>
                <span><a href="#" onclick="javascript: return false;" title='<%= Cccs.Credability.Website.App.Translate("WhyLink")%>'><%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|W")%></a></span>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress")%></label>
                <asp:TextBox ID="txtContactAddress" runat="server" MaxLength="100"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtContactAddress"
                    Display="Dynamic" Text="" ErrorMessage=" Address Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|StreetAddress1")%></label>
                <asp:TextBox ID="txtContactAddress2" runat="server" MaxLength="100"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|City")%></label>
                <asp:TextBox ID="txtContactCity" runat="server" MaxLength="50"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtContactCity"
                    Display="Dynamic" Text="!" ErrorMessage=" City Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|State")%></label>
                <asp:DropDownList ID="ddlState" runat="server" />
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="!" ErrorMessage=" Select State" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|ZipCode")%></label>
                <asp:TextBox ID="txtContactZip" runat="server" MaxLength="5" Width="128px"></asp:TextBox><span style="color: Red; font-size: large;">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtContactZip"
                    Display="Dynamic" Text="" ErrorMessage=" Zip Code Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" SetFocusOnError="true"
                    ControlToValidate="txtContactZip" ValidationExpression="^[0-9]{5}$" Text="" ErrorMessage="Enter  valid Zip Code must be 5 numeric digits"
                    Display="Dynamic" ValidationGroup="user" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|EADD")%></label>
                <asp:TextBox ID="txtContactEmail" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContactEmail"
                    Display="Dynamic" Text="" ErrorMessage="Invalid Email Address" SetFocusOnError="True"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="userprofile"></asp:RegularExpressionValidator>
                &nbsp;&nbsp;&nbsp;&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|BKE|Required")%>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|NIH")%></label>
                <asp:DropDownList ID="ddlNumberInHouse" runat="server" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|EL")%></label>
                <asp:DropDownList ID="ddlEducation" runat="server" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AHI")%></label>
                <asp:DropDownList ID="ddlIncomeRange" runat="server">
                </asp:DropDownList>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|BC")%></label>
                <asp:TextBox ID="txtCaseNumber" runat="server" MaxLength="15" BackColor="Silver"
                    ReadOnly="True"></asp:TextBox>
                <span style="color: Red; font-size: large;">&nbsp;*</span>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKE|EX")%>
                <span class="NormHead">
                    <%=DateTime.Now.Year.ToString().Substring(2)%></span><%= Cccs.Credability.Website.App.Translate("Credability|BKE|NR")%>80842
                <%= Cccs.Credability.Website.App.Translate("Credability|BKE|enter")%> <%=DateTime.Now.Year.ToString().Substring(2)%>-80842.
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtCaseNumber"
                    Display="Dynamic" Text="!" ErrorMessage=" Case Number Required" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|DH")%></label>
                <asp:TextBox ID="txtDayPhone" runat="server" MaxLength="15" onBlur="ReSetPhone(this)"></asp:TextBox>
                <span style="color: Red; font-size: large;">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDayPhone"
                    Display="Dynamic" Text="" ErrorMessage=" Daytime Phone Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:CustomValidator class="validationMessage" ID="cfvPphone" runat="server" ClientValidationFunction="CheckprimaryPhone"
                    ControlToValidate="txtDayPhone" Display="Dynamic" EnableClientScript="true" SetFocusOnError="True"
                    Text="" ErrorMessage="Invalid Phone Number Number." ValidationGroup="user"> 
                </asp:CustomValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|EP")%></label>
                <asp:TextBox ID="txtNitePhone" runat="server" MaxLength="15" onBlur="ReSetPhone(this)"></asp:TextBox>
                <asp:CustomValidator class="validationMessage" ID="CustomValidator1" runat="server" ClientValidationFunction="CheckprimaryPhone"
                    ControlToValidate="txtNitePhone" Display="Dynamic" EnableClientScript="true"
                    SetFocusOnError="True" Text="" ErrorMessage="Invalid Phone Number Number." ValidationGroup="user"> 
                </asp:CustomValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AN")%></label>
                <asp:TextBox ID="txtAttyname" runat="server" MaxLength="50"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AEA")%></label>
                <asp:TextBox ID="txtAttyemail" runat="server" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator class="validationMessage" ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAttyemail"
                    Display="Dynamic" Text="" ErrorMessage="Invalid Email Address" SetFocusOnError="True"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="userprofile"></asp:RegularExpressionValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AFC")%></label>
                <asp:TextBox ID="txtFirmCode" runat="server" MaxLength="15"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|FB")%></label>
                <asp:DropDownList ID="ddlBankruptcyType" runat="server" />
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlBankruptcyType"
                    Display="Dynamic" Text="" ErrorMessage=" Select Bankruptcy" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <p>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|Credit")%>
                </p>
            </div>
            <div class="dvrow">
                <p>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|AAPO")%>
                </p>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKE|PPY")%></label>
                <asp:TextBox ID="txtBirthCity" runat="server" MaxLength="50"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtBirthCity"
                    Display="Dynamic" Text="" ErrorMessage=" Birth City Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
        </div>
    <div class="clearboth">
    </div>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" Text="Continue" ValidationGroup="user"
            CssClass="Button" OnClick="btnContinue_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveContinue" runat="server" Text="Save and Exit" ValidationGroup="user"
            CssClass="Button" OnClick="btnSaveExit_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinuelater")%></span></asp:LinkButton>
    </div>
</div>
