﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCGetJointSsn : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|UserProfileBCH|SSNR");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");

            if (!IsPostBack)
            {
                btnGetJointSsnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                CommonFunction.AddItems(rblFiling.Items, "FillingOptionsRadioButton", SessionState.LanguageCode);
                dvJointForm.Visible = false;
            }
        }

        protected void rblFiling_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblFiling.SelectedValue == "Joint")
            {
                dvJointForm.Visible = true;
            }
            else
            {
                dvJointForm.Visible = false;
            }
        }

        protected void click_btnGetJointSsnContinue(object sender, EventArgs e)
        {
            SessionState.SecondaryFlag = string.Empty;
            SessionState.CanJoinSSN = false;
            SessionState.JointSsn = string.Empty;

            if (rblFiling.SelectedValue == "Joint")
                SessionState.JointSsn = txtjointssn.Text.Clean();


            if (SessionState.ContactSsn.IsNullOrEmpty())
            {
                SessionState.CanJoinSSN = true;
            }
            else
            {
                // Bankruptcy Conversion
                //App.Host.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
                App.Debtplus.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
            }

            Response.Redirect("ProvideYourPersonalInformation.aspx");

            //if (rblFiling.SelectedValue == "Joint")
            //{
            //   SessionState.SecondaryFlag = "";
            //   SessionState.JointSsn = txtjointssn.Text.Clean();
            //   App.Host.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
            //}
            //else
            //{
            //   SessionState.SecondaryFlag = "";
            //   SessionState.JointSsn = string.Empty;
            //   App.Host.MimSaveXRef(SessionState.ContactSsn, string.Empty);
            //}
        }

        public void JoinClientSpouse(bool bJoint)
        {
            if (bJoint)
            {
                SessionState.SecondaryFlag = "S";
            }
            else
            {
                SessionState.SecondaryFlag = "";
            }

            // Bankruptcy Conversion
            //App.Host.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
            App.Debtplus.MimSaveXRef(SessionState.ContactSsn, SessionState.JointSsn);
        }
    }
}