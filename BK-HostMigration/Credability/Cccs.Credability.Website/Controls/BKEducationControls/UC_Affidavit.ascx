﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Affidavit.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UC_Affidavit" %>
<p style="text-align:center">
    <h2><%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|COGA")%></h2>
</p>
<p>
<br />
<%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|DFES")%>
</p>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|SINCE1964")%>
    
</p>
<p>
<ul>
				<li style="margin-bottom: 15px;">
               <%= ((SessionState.BKFee.CompareTo(50.00) == 0)?
                     string.Format(Cccs.Credability.Website.App.Translate("Credability|BKE|OFRF"),"$"+SessionState.BKFee):
                     string.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|OFI4"), SessionState.BKFee, 50- SessionState.BKFee))%>	
				</li>
				<li style="margin-bottom: 15px;">
				  <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|YOUMAYBEELIGIBL")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				   <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|Our_Counselors_Are")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				    <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|CCCS_Does_Not_Pay")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				    <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|CCCS_Is_obligated_To")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				    <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|CCCS_Will_Not_Disclose")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				     <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|The_Executive_Office_For")%>
					
				</li>
				<li style="margin-bottom: 15px;">
				    <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|Upon_Successful_Completion_Of")%>
					
				</li>
</ul>
<br /><br />
</p>
<p>
<%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|We_Appreciate_The_Confidence")%>
</p>
<p>
    <br /><br /><b><%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|Affidavit")%></b><br />
</p>
<p>
 <div class="DvErrorSummary" id="dvBottomServerSideErrorMessage" runat="server" visible="false"></div>
 </p>
 <div class="dvform2col dvform2colchk">
        <table cellpadding="0" cellspacing="0" border="0" summary="" style="width:100%;">
                <tr>
                        <td style="width:20%;vertical-align:middle;padding-left:30px;"><asp:CheckBox ID="chkConfirmation" runat="server"/></td>
                        <td style="width:80%;"> <%= Cccs.Credability.Website.App.Translate("Credability|Affidavit|I_Certify_That_I")%></td>
                </tr>
        </table>
        
        <div class="clearboth">
        </div>
    </div>
    <div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click"
            ValidationGroup="continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>