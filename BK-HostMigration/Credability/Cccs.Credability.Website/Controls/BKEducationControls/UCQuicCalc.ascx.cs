﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Cccs.Host;
namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCQuicCalc : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                GetStatePickList();
                ValidationControlTranslation();
                LoadQuickCalcData();
                btnReturnToPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
            }

        }
      private void GetStatePickList()
        {
            ddlLiveIn.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
            string[] statesCodes = App.Geographics.StateCodesGet();

            if (statesCodes != null)
            {
                foreach (string s in statesCodes)
                {
                    ddlLiveIn.Items.Add(s);
                }
            }

        }
        private void LoadQuickCalcData()
        {
            // Bankruptcy Conversion
            //MimQuicCalcLoadDataResult Result = null;
            //Result = App.Host.Mim_QuicCalc_LoadData(SessionState.Clid, SessionState.RegNum);
            var Result = App.Debtplus.Mim_QuicCalc_LoadData(SessionState.Clid, SessionState.RegNum);
            if (Result.IsSuccessful)
            {
                ddlLiveIn.SelectedValue = Result.ContactState;
                txtGrossInc.Text = Result.MonthlyGrossIncome.ToString();
                ddlTotDep.SelectedValue = Result.SizeOfHousehold.ToString(); ;
            }
        }
        private void ValidationControlTranslation()
        {
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|USR");
            RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|GIR");
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
        }

        

        protected void btnIwouldlike_Click(object sender, EventArgs e)
        {
            Session["WaiverType"] = "IW";

            // Bankruptcy Conversion
            //Result Results = App.Host.Mim_QuicCalc_SaveData(SessionState.Clid,ddlLiveIn.SelectedItem.Value.Trim(),txtGrossInc.Text.Trim(),ddlTotDep.SelectedItem.Value);
            var Results = App.Debtplus.Mim_QuicCalc_SaveData(SessionState.Clid, ddlLiveIn.SelectedItem.Value.Trim(), txtGrossInc.Text.Trim(), ddlTotDep.SelectedItem.Value);
            if (Results.IsSuccessful == true)
            {
                Response.Redirect("WaverApp.aspx");
            }
            else
                DvError.InnerHtml = "Exception Occured. Please Try Later." + Results.Messages.ToString(); 

        }

        protected void btnIdonotwish_Click(object sender, EventArgs e)
        {
            // Bankruptcy Conversion
            //Result Results = App.Host.Mim_QuicCalc_SaveData(SessionState.Clid, ddlLiveIn.SelectedItem.Value.Trim(), txtGrossInc.Text.Trim(), ddlTotDep.SelectedItem.Value);
            var Results = App.Debtplus.Mim_QuicCalc_SaveData(SessionState.Clid, ddlLiveIn.SelectedItem.Value.Trim(), txtGrossInc.Text.Trim(), ddlTotDep.SelectedItem.Value);

            if (Results.IsSuccessful == true)
            {
                Response.Redirect("WaiverWaiver.aspx");
            }
            else
                DvError.InnerHtml = "Exception Occured. Please Try Later.<BR /><BR />" + Results.Messages.ToString(); 

        }

        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");

        }
    }
}