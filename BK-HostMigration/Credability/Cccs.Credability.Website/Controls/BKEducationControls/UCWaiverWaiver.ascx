﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCWaiverWaiver.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCWaiverWaiver" %>

<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<div style="width: 100%; color: Red; font-weight: bold" id="DvError" runat="server">
</div>
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WOAP")%></h1>
<asp:Panel ID="pnlEscrow" runat="server" Visible="true">
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BYH")%><br />
     <div class="lnkbutton">
        <asp:LinkButton ID="lnkFeeWaiver" runat="server" OnClick="lnkFeeWaiver_Click"
            CausesValidation="False"><span><%= Cccs.Credability.Website.App.Translate("Credability|BKC|IUT")%></span>
        </asp:LinkButton>
    </div>
</p>
<br />
</asp:Panel>    
    
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YHCTY")%>
</p>
<p class="col_title">
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTC")%>
</p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="continue" />
<div class="dvform2col dvformlblbig">
    <div class="dvform">
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|AYF")%>
            </label>
            <asp:RadioButtonList ID="rblJointFile" runat="server" RepeatDirection="Horizontal"
                AutoPostBack="True" OnSelectedIndexChanged="rblJointFile_SelectedIndexChanged" CssClass="rdotbllsts mgrT7 fL">
                
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator1" runat="server" ControlToValidate="rblJointFile"
                Display="Dynamic" Text="" ErrorMessage="one of the Option is Required" SetFocusOnError="True"
                ValidationGroup="continue" CssClass="fL padT5"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YZC")%></label>
            <asp:TextBox ID="txtPriZipEnc" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPriZipEnc"
                Display="Dynamic" Text="" ErrorMessage="Zip Code is Required" SetFocusOnError="True"
                ValidationGroup="continue"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|L4")%></label>
            <asp:TextBox ID="txtPri4SsnEnc" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPri4SsnEnc"
                Display="Dynamic" Text="" ErrorMessage="SSN is Required" SetFocusOnError="True" ValidationGroup="continue"></asp:RequiredFieldValidator>
        </div>
        <asp:Panel ID="pnlSpouse" runat="server" Visible="false">
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SZ")%></label>
                    <asp:TextBox ID="txtSecZipEnc" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSecZipEnc"
                        Display="Dynamic" Text="!" ErrorMessage="Zip Code is Required" SetFocusOnError="True"
                        ValidationGroup="continue"></asp:RequiredFieldValidator>
                
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|L4D")%></label>
                <asp:TextBox ID="txtSec4SsnEnc" runat="server" MaxLength="4"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSec4SsnEnc"
                    Display="Dynamic" Text="!" ErrorMessage="SSN is Required" SetFocusOnError="True" ValidationGroup="continue"></asp:RequiredFieldValidator>
            </div>
        </asp:Panel>
    </div>
    <div class="clearboth">
    </div>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturn" runat="server" CssClass="previous" OnClick="btnReturn_Click"
            CausesValidation="False"><span>Return to previous page</span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" CssClass="next" OnClick="btnContinue_Click"
            ValidationGroup="continue"><span>Continue</span></asp:LinkButton>
    </div>
</div>
