﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCQuicCalc.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKEducationControls.UCQuicCalc" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
    function Qualify()
    {
	    var St, Dep, GrossInc, PovLev, i
	    //						All	   AK	  HI
	    var BaseAmt = new Array(10400, 13000, 11960);
	    var StepAmt = new Array(3600,  4500,  4140);
	    LiveIn = document.getElementById('<%=ddlLiveIn.ClientID%>')
	    St = LiveIn.options[LiveIn.selectedIndex].text;
	    Dep = document.getElementById('<%=ddlTotDep.ClientID%>').selectedIndex;
	    GrossInc = stripCharsNotInBag(document.getElementById('<%=txtGrossInc.ClientID%>').value, "0123456789.");
	    document.getElementById('<%=txtGrossInc.ClientID%>').value = GrossInc
	    if(isNaN(GrossInc) || GrossInc == "")
	    {
		    alert('<%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCVIA")%>');
		    return false;
	    }
	    if(St.length != 2)
	    {
	        alert('<%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCSR")%>');    
		    //alert("Please select your state of residence.");
		    return false;
	    }
	    switch(St)
	    {
		    case "AK":
			    i = 1;
			    break;

		    case "HI":
			    i = 2;
			    break;

		    default:
			    i = 0;
			    break;
	    }
	    PovLev = BaseAmt[i] + (Dep * StepAmt[i]);
	    //alert('PovLev: ' + PovLev);
	    //alert('GrossInc: ' + GrossInc);
	    if(GrossInc <= PovLev)
	    {
		    document.getElementById("Continue").style.display = 'block';
		    document.getElementById("Deny").style.display = 'none';
		    document.getElementById("Approve").style.display = 'block';
            //alert("Aprove");
	    }
	    else
	    {
		    document.getElementById("Continue").style.display = 'block';
		    document.getElementById("Deny").style.display = 'block';
		    document.getElementById("Approve").style.display = 'none';
            //alert("Deny");
	    }
    }


</script>
<h1><%= Cccs.Credability.Website.App.Translate("Credability|BKC|AEFW")%></h1>
<div class="error" id="DvError" runat="server">
</div>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTQ")%></p>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTE")%></p>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" 
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="required" />
<div class="dvform2col dvformlblbig">
    <div class="dvform">
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WIY")%></label>
            <asp:DropDownList ID="ddlLiveIn" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlLiveIn"
                Display="Dynamic" ValidationGroup="required" Text="!" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|WIYG")%>
            </label>
            <asp:TextBox ID="txtGrossInc" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGrossInc"
                Display="Dynamic" ValidationGroup="required" Text="!" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="dvrow">
            <p>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|GII")%></p>
        </div>
        <div class="dvrow">
            <label>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|TDLI")%></label>
            <asp:DropDownList ID="ddlTotDep" runat="server">
                <asp:ListItem Value="1">1</asp:ListItem>
                <asp:ListItem Value="2">2</asp:ListItem>
                <asp:ListItem Value="3">3</asp:ListItem>
                <asp:ListItem Value="4">4</asp:ListItem>
                <asp:ListItem Value="5">5</asp:ListItem>
                <asp:ListItem Value="6">6</asp:ListItem>
                <asp:ListItem Value="7">7</asp:ListItem>
                <asp:ListItem Value="8">8</asp:ListItem>
                <asp:ListItem Value="9">9</asp:ListItem>
                <asp:ListItem Value="10">10</asp:ListItem>
                <asp:ListItem Value="11">11</asp:ListItem>
                <asp:ListItem Value="12">12</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="clearboth">
    </div>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturnToPrevious" runat="server" OnClick="btnReturnToPrevious_Click"
            CausesValidation="False" CssClass="previous"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnCalculate" runat="server" OnClientClick="Qualify();return false;" ValidationGroup="required"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Calculate")%></span></asp:LinkButton>
    </div>
</div>
<div id="Approve" style="display: none;">
    <br /><br /> 
	<table align="center" width="95%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
			<p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|QCAFICF")%></p>
				<%--It appears you may qualify for a fee waiver.  If would like to continue with the fee waiver process, please click “I would like to apply for a fee waiver” below.--%>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
	</table>
</div>
<div id="Deny" style="display: none;">
    <br /><br />
	<table align="center" width="85%" cellpadding="0" cellspacing="0" border="0" class="NormTxt">
		<tr>
			<td>
				<p><%= Cccs.Credability.Website.App.Translate("Credability|BKC|PIY")%></p>
			</td>
		</tr>
		<tr>
			<td height="10"></td>
		</tr>
	</table>
</div>
<div id="Continue" style="display: none;">
    
    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="btnIwouldlike" runat="server" OnClick="btnIwouldlike_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IWTA")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
            <asp:LinkButton ID="btnIdonotwish" runat="server" OnClick="btnIdonotwish_Click"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|IDN")%></span></asp:LinkButton>
        </div>
    </div>
</div>
