﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCAprWait : System.Web.UI.UserControl
    {
        public string CounsNotes = string.Empty;
        public Int32 SignUp;
        public Int32 Aproved;
        public Int32 FeePolicy;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (
                String.IsNullOrEmpty(SessionState.FirmID)
                || String.IsNullOrEmpty(SessionState.PrimaryZipCode)
            )
            {
                var myUrl = HttpContext.Current.Request.Url.AbsolutePath;
                Response.Redirect(ResolveUrl("~/GetRequiredFeeInformation.aspx?redirect=" + myUrl));
            }

            if (!IsPostBack)
            {
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                btnContinueDisApproved.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }

            //reload client zipcode and firmcode so that we can retrieve the correct fee
            //this is necessary when a client has abandoned the middle of a session and is now logging back in to pay
            if (
                (   SessionState.PrimaryZipCode == SessionState.FirmID  
                    || String.IsNullOrEmpty(SessionState.PrimaryZipCode)
                    || String.IsNullOrEmpty(SessionState.FirmID)
                )
                && !String.IsNullOrEmpty(SessionState.Clid) 
                && !String.IsNullOrEmpty(SessionState.RegNum)) {

                // Bankruptcy Conversion
                //var data = App.Host.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
                var data = App.Debtplus.Mim_Form7_LoadData(SessionState.Clid, SessionState.RegNum);
                SessionState.PrimaryZipCode = data.contact_zip;
                SessionState.FirmID = data.firmcode;
            }

            //Bankruptcy conversion-Seethal
            //var result = App.Host.MimDisclosureGetChargeAmount(SessionState.PrimaryZipCode,SessionState.FirmID);
            var result = App.Debtplus.MimDisclosureGetChargeAmount(SessionState.PrimaryZipCode, SessionState.FirmID);
            GetCharAmt1.Text = result.ToString("#0.00");
            GetCharAmt2.Text = GetCharAmt1.Text;

            if (!IsPostBack)
                ApprovalState();
        }

        private void ApprovalState()
        {
            if (SessionState.FwID == null && SessionState.SecondaryFlag == "S")
            {
                string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
                Response.Redirect(Url);
            }

            if (SessionState.FwID != null)
            {
                var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(SessionState.FwID.Value);
                CounsNotes = feeWaiver.CounsNotes;
                SignUp = feeWaiver.SignUp.HasValue ? feeWaiver.SignUp.Value : 2;
                Aproved = feeWaiver.Aproved.HasValue ? feeWaiver.Aproved.Value : 2;
                if (SignUp == 1)
                {
                    if (Aproved == 2)
                        pnlWait.Visible = true;
                    else
                    {
                        if(SessionState.SecondaryFlag == "S")
                        {
                            string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
                            Response.Redirect(Url);
                        }
                        if(Aproved == 1)
                            pnlApproved.Visible = true;
                        else
                            pnlDisApproved.Visible = true;
                    }
                }
                else if(SignUp == 0)
                {
                    if(SessionState.SecondaryFlag == "S")
                    {
                        string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
                        Response.Redirect(Url);
                    }

                    var vancoRef = App.Credability.CurVancoMimRefGet(Convert.ToInt64(SessionState.RegNum));
                    if(vancoRef != null && vancoRef.Status != null && vancoRef.Status.Trim() == VancoRef.STATUS_PAYED)
                    {
                        //Response.Redirect("MIMnavigate.aspx"); use the login.php to start clock
                        Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/login.php?uid=" + SessionState.Username);
                    }

                    // Bankruptcy Conversion
                    //var mimData = App.Host.Mim_Disclosure_LoadData(SessionState.Clid);
                    var mimData = App.Debtplus.Mim_Disclosure_LoadData(SessionState.Clid);
                    if (mimData != null && (!String.IsNullOrEmpty(mimData.AbaNumber) || !String.IsNullOrEmpty(mimData.Mtcn)))
                    {
                        //Response.Redirect("MIMnavigate.aspx"); use the login.php to start clock
                        Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/login.php?uid=" + SessionState.Username);
                    }

                    Response.Redirect("Disclosure.aspx");
                }
                else
                {
                    if(SessionState.Escrow == "Y")
                        Response.Redirect("WaiverWaiver.aspx");
                    else
                        Response.Redirect("WaiverPath.aspx");
                }
            }
        }

        protected void btnAbandonSession_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverWaiver.aspx");
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
        }

        protected void btnContinueDisApproved_Click(object sender, EventArgs e)
        {
            if(SessionState.Escrow == "Y")
            {
                string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
                Response.Redirect(Url);
            }
            else
                Response.Redirect("Disclosure.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            string Url = CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username;
            Response.Redirect(Url);
        }
    }
}