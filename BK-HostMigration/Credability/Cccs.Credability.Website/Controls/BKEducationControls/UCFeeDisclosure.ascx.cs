﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCFeeDisclosure : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnbkFeeDisclosureContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }
        }

		public void UpdateFeePolicy()
		{
			var feeWaiver = App.FeeWaiver.GetByFeeWaiverId((int)SessionState.FwID);
			if(feeWaiver.FeePolicyDTS == null)
			{
				feeWaiver.FeePolicyDTS = DateTime.Now;
				feeWaiver.FeePolicy = 1;
				feeWaiver = App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
			}
		}

        protected void click_btnbkFeeDisclosureContinue(object sender, EventArgs e)
        {
            if (chkUnderstand.Checked)
            {
                UpdateFeePolicy();
                Response.Redirect("GetJointSsn.aspx");
            }
            else
            {
                dvErrorCont.Attributes.Add("style", "display:block");
							dvError.InnerHtml = "You must agree to the CredAbility fee policy before continuing.  Please read the policy and check the box indicating you have read and understand the fee policy.";
            }
        }
    }
}