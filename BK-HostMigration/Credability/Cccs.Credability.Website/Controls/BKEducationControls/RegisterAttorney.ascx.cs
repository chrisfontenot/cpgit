﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Cccs.Identity;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
	public partial class RegisterAttorney : System.Web.UI.UserControl
    {
        private NLog.Logger logger;

		protected void Page_Load(object sender, EventArgs e)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();
			//log4net.ThreadContext.Properties["UserId"] = SessionState.UserId;

			var lblHeading = Page.Master.FindControl("lblHeading") as Label;
			if(lblHeading != null)
				lblHeading.Text = App.Translate("Credability|BKE|RegisterationHead");

			if(!IsPostBack)
			{
				btnRegistration.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				SessionState.UserPageArrivalTime = DateTime.Now;

				ValidationControlTranslation();
			}
		}

		private void ValidationControlTranslation()
		{
            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
			rfvSocialSecurityNumber.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ISS");
			RegularExpressionSocialSecurityNumber.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ISSN");
        }

		protected void btnRegistration_Click(object sender, EventArgs e)
		{
			var bankruptcyNumber = String.Empty;
            
			if(!ValidateBankruptcyNumber(txtBankruptcyCaseNumber.Text, out bankruptcyNumber))
			{
				dvErrorMessage.InnerHtml = String.Format("<ul><li>{0}</li></ul>", App.Translate("Credability|DescYourSituRVM|IB"));
				dvErrorMessage.Visible = true;
				return;
			}            
			SessionState.CaseNumber = bankruptcyNumber;

            // Bankruptcy Conversion
            //var foundSsnResult = App.Host.Mim_CreateLogin_ValidateData(txtSocialSecurityNumber.Text.Trim());
            var foundSsnResult = App.Debtplus.Mim_CreateLogin_ValidateData(txtSocialSecurityNumber.Text.Trim());

            if (!foundSsnResult.IsSuccessful)
			{
				dvErrorMessage.InnerHtml = "<ul><li>An error occurred when trying to validate the SSN.</li></ul>";
				dvErrorMessage.Visible = true;
				return;
			}

            if (foundSsnResult.IsSsnAlreadyOnFile
                && !Context.Request.Url.AbsoluteUri.Contains("localhost:")
                && !Context.Request.Url.AbsoluteUri.Contains("qacounseling.test.credability.local")
                )
            {
               dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
               dvErrorMessage.InnerHtml += "<li>Social Security Number is already on file.</li>";
               dvErrorMessage.Visible = true;
               return;
            }
            else
            {
               SessionState.SecondaryFlag = foundSsnResult.SecondaryFlag;
               SessionState.ContactSsn = txtSocialSecurityNumber.Text.Clean();

               logger.DebugFormat("SecondaryFlag: {0}", foundSsnResult.SecondaryFlag);
               logger.DebugFormat("SpUvId: {0}", foundSsnResult.SpUvId);

               if ((foundSsnResult != null) && !foundSsnResult.SecondaryFlag.IsNullOrEmpty() && !foundSsnResult.SpUvId.IsNullOrEmpty())
               {
                  if ((foundSsnResult.SecondaryFlag.ToUpper().CompareTo(@"S") == 0) &&
                      (!foundSsnResult.SpUvId.Trim().IsNullOrEmpty()))
                  {
                     SetFWIDSession(Convert.ToInt32(foundSsnResult.SpUvId.Replace("W", "")));
                  }
               }
            }

            //Update host   
            var hostLanguage = SessionState.LanguageCode == Cccs.Translation.Language.EN ? Constants.HostLanguage.English : Constants.HostLanguage.Spanish;

            // Bankruptcy Conversion
            //Result resSave = App.Host.Mim_Form7_SaveData(SessionState.Clid, "", "", "", SessionState.ContactSsn, "","",  "",
            //    "", SessionState.PrimaryZipCode, "",  "", "",  "", "",  "", "",  "", "", "",
            //    SessionState.FirmID, SessionState.Escrow, SessionState.SecondaryFlag, "",  SessionState.CaseNumber, "", "", "", hostLanguage, 
            //    SessionState.ChargeAmount);
            var resSave = App.Debtplus.Mim_Form7_SaveData(SessionState.Clid, "", "", "", SessionState.ContactSsn, "", "", "",
                "", SessionState.PrimaryZipCode, "", "", "", "", "", "", "", "", "", "",
                SessionState.FirmID, SessionState.Escrow, SessionState.SecondaryFlag, "", SessionState.CaseNumber, "", "", "", hostLanguage,
                SessionState.ChargeAmount);


            SessionState.Escrow = "N";
		   SessionState.Escrow6 = "N";

         var regNum = CreateMySQLRecord();
         Account WsAcnt = App.Identity.AccountGet(SessionState.UserId.Value, "WS.CAM");
 
			if(SessionState.SecondaryFlag == "S")
				Response.Redirect("ProvideYourPersonalInformation.aspx");
			else
				Response.Redirect("GetJointSsn.aspx");
		}

		private bool ValidateBankruptcyNumber(string number, out string validationNumber)
		{
			validationNumber = String.Empty;
			foreach(var character in number)
			{
				if(Char.IsNumber(character))
					validationNumber += character;
			}
			if(validationNumber.Length != 7)
			{
				return false;
			}
			if(validationNumber != ConfirmCaseNo.Value)
			{
				string Year = validationNumber.Substring(0, 2);
				if(!(Year == DateTime.Now.Year.ToString().Substring(2, 2) || Year == DateTime.Now.AddYears(-1).Year.ToString().Substring(2, 2)))
				{
					ConfirmCaseNo.Value = validationNumber;
					return false;
				}
				string[] invalidBankruptcyNumbers = new string[] { "01234", "12345", "23456", "34567", "45678", "56789", "67890", "00000", "11111", "22222", "33333", "44444", "55555", "66666", "77777", "88888", "99999" };
				var last5Digits = validationNumber.Substring(2, 5);
				foreach(var invalidNumber in invalidBankruptcyNumbers)
				{
					if(invalidNumber == last5Digits)
					{
						ConfirmCaseNo.Value = validationNumber;
						return false;
					}
				}
			}
			validationNumber = String.Format("{0}-{1}", validationNumber.Substring(0, 2), validationNumber.Substring(2, validationNumber.Length - 2));
			return true;
		}

		private void SetFWIDSession(int hostId)
		{
			var feeWaiver = App.FeeWaiver.GetByEducationClient(hostId);
			SessionState.FwID = feeWaiver.FwID;
		}

		public string CreateMySQLRecord()
		{
			var dsUser = App.Credability.RegNumGet(SessionState.Username.ToString());
			if(dsUser == null || dsUser.Tables.Count == 0 || dsUser.Tables[0].Rows.Count == 0)
			{
				var UserNamePassword = new UserNamePasswordRegnum()
				{
					username = SessionState.Username,
					password = SessionState.Username
				};
				App.Credability.CreateNewRegNumInMySql(UserNamePassword);

				dsUser = App.Credability.RegNumGet(SessionState.Username.ToString());
				SessionState.RegNum = dsUser.Tables[0].Rows[0]["regnum"].ToString();
			}
			else
			{
				var regNum = dsUser.Tables[0].Rows[0]["regnum"].ToString();
				SessionState.RegNum = regNum;
			}
			return SessionState.RegNum;
		}

		public string CreateHostRecord(string regNum)
		{
            // Bankruptcy Conversion
            //var possibleClids = App.Host.Mim_HostID_ReturningUser_Get(regNum);
            var possibleClids = App.Debtplus.Mim_HostID_ReturningUser_Get(regNum);
            if (String.IsNullOrEmpty(possibleClids))
			{
                //SessionState.Clid = App.Host.Mim_NewClsID_Get(regNum);
                SessionState.Clid = App.Debtplus.Mim_NewClsID_Get(regNum);
            }
			else
			{
				SessionState.Clid = possibleClids;
			}
			return SessionState.Clid;
		}

		public int CreateFeeWaiver(string clid)
		{
			var hostId = Convert.ToInt32(clid.Replace("W", String.Empty));
			var feeWaiver = App.FeeWaiver.GetByEducationClient(hostId);
			if(feeWaiver == null)
			{
				feeWaiver = new FeeWaiver()
				{
					Language = SessionState.LanguageCode == Cccs.Translation.Language.EN ? "Eng" : "Esp",
					client_number = hostId,
					ClientType = "Edu",
				};
				feeWaiver = App.FeeWaiver.AddFeeWaiver(feeWaiver);
			}

			SessionState.FwID = feeWaiver.FwID;
			return SessionState.FwID.Value;
		}

		public bool CheckFirmID(int FirmCode)
		{
			bool Back = true;
			string HostServiceError = String.Empty;
            // Bankruptcy Conversion
            //Result<XmlDocument> result = App.Host.GetEscrow(FirmCode);
            var result = App.Debtplus.GetEscrow(FirmCode, App.WebsiteCode);

            if (result != null)
            {
                SessionState.EscrowProbonoValue = result.EscrowProBono;
                SessionState.Escrow = result.Escrow;

                if (result.IsLegalAid)
                {
                    SessionState.Escrow6 = "Y";
                }

                /*if (result.Escrow == 1)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 2 && App.WebsiteCode == Cccs.Identity.Website.BKC)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 3 && App.WebsiteCode == Cccs.Identity.Website.BKDE)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 6)
                {
                    SessionState.Escrow = "Y";
                    SessionState.Escrow6 = "Y";
                }
                else
                {
                    SessionState.Escrow = "N";
                }*/

                SessionState.FirmID = FirmCode.ToString();
                SessionState.FirmName = result.FirmName;
                SessionState.ChargeAmount = result.ChargeAmount > 0 ? "0.00" : result.ChargeAmount.ToString(); ;
            }

            //            XmlNodeList nodeListError = result.Value.SelectNodes("//Error") as XmlNodeList;
            //			if(nodeListError != null && nodeListError.Count != 0)
            //			{
            //				HostServiceError = nodeListError.Item(0).InnerText;
            //				if(HostServiceError.Contains("This Record was not found"))
            //					Back = false;
            //			}
            //			if(Back)
            //			{
            //				string Escrow = String.Empty;
            //				XmlNodeList nodeListEscrow = result.Value.SelectNodes("//Escrow") as XmlNodeList;
            //				if(nodeListEscrow != null && nodeListEscrow.Count != 0)
            //				{
            //					switch(nodeListEscrow.Item(0).InnerText.Trim())
            //					{
            //						case "1":
            //							SessionState.Escrow = "Y";
            //							break;
            //						case "3":
            //							SessionState.Escrow = "Y";
            //							break;
            //						case "6":
            //							SessionState.Escrow = "Y";
            //							SessionState.Escrow6 = "Y";
            //							break;
            //						default:
            //							SessionState.Escrow = "N";
            //							break;
            //					}
            //				}
            //				XmlNodeList nodeListFirmName = result.Value.SelectNodes("//Name") as XmlNodeList;
            //				if(nodeListFirmName != null && nodeListFirmName.Count != 0)
            //				{
            //					SessionState.FirmName = nodeListFirmName.Item(0).InnerText.Trim();
            //				}
            //				SessionState.FirmID = FirmCode.ToString();

            //                XmlNodeList nodeListChargeAmount = result.Value.SelectNodes("//ChargeAmount") as XmlNodeList;
            //                /*
            //                if (nodeListChargeAmount != null && nodeListChargeAmount.Count != 0)
            //                {
            //                    SessionState.ChargeAmount = nodeListChargeAmount.Item(0).InnerText.Trim();
            //                }
            //                */
            //                //SessionState.ChargeAmount = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode,
            ////                    App.WebsiteCode == Cccs.Identity.Website.BKDE ? "W" : "B").Value.ToString();
            //			}
            return Back;
		}

      protected void txtfirmid_TextChanged(object sender, EventArgs e)
      {

      }

      protected void txtattyname_TextChanged(object sender, EventArgs e)
      {

      }
	}
}