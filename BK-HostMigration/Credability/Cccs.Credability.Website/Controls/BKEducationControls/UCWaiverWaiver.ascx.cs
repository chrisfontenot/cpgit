﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Dal;

namespace Cccs.Credability.Website.Controls.BKEducationControls
{
    public partial class UCWaiverWaiver : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadioButtonTransaltion();
                pnlEscrow.Visible = SessionState.Escrow == "Y";
            }
        }

        private void RadioButtonTransaltion()
        {
            CommonFunction.AddItems(rblJointFile.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
        }

        private void SaveWaiverInfo(Int32 FeeWaiverID)
        {
            var feeWaiver = App.FeeWaiver.GetByFeeWaiverId(FeeWaiverID);
            feeWaiver.SignUp = 0;
            if (!String.IsNullOrEmpty(rblJointFile.SelectedValue))
            {
                feeWaiver.JointFile = Convert.ToInt32(rblJointFile.SelectedValue);
            }
            feeWaiver.Pri4SsnEnc = FeeWaiverManager.EncryptText(txtPri4SsnEnc.Text.Trim());
            feeWaiver.PriZipEnc = FeeWaiverManager.EncryptText(txtPriZipEnc.Text.Trim());
            feeWaiver.Sec4SsnEnc = FeeWaiverManager.EncryptText(txtSec4SsnEnc.Text.Trim());
            feeWaiver.SecZipEnc = FeeWaiverManager.EncryptText(txtSecZipEnc.Text.Trim());

            feeWaiver = App.FeeWaiver.UpdateFeeWaiver(feeWaiver);
        }

        protected void lnkFeeWaiver_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Int32 FeeWaiverID = Convert.ToInt32(SessionState.FwID);
            Boolean PrimaryZipcodeFlag = true, SecondaryZipcodeFlag = true;
            if (rblJointFile.SelectedValue == "1")
            {
                PrimaryZipcodeFlag = ZipCodeManager.ZipCodeAvailable(txtPriZipEnc.Text.Trim());
                SecondaryZipcodeFlag = ZipCodeManager.ZipCodeAvailable(txtPriZipEnc.Text.Trim());
                if (PrimaryZipcodeFlag)
                {
                    if(SecondaryZipcodeFlag)
                    {
                        DvError.InnerHtml = "";
                        SaveWaiverInfo(FeeWaiverID);
//                        if(SessionState.Escrow == "Y")
//                            Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username);
//                        else
                            Response.Redirect("Disclosure.aspx");
                    }
                    else
                    {
                        DvError.InnerHtml = Cccs.Credability.Website.App.Translate("Credability|FeaWaiver|ISZC");
                    }
                }
                else
                {
                    DvError.InnerHtml = Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|IZC");
                }
            }
            else if (rblJointFile.SelectedValue == "0")
            {
                bool zipCodeFound = ZipCodeManager.ZipCodeAvailable(txtPriZipEnc.Text.Trim());
                if (zipCodeFound)
                {
                    DvError.InnerHtml = "";
                    SaveWaiverInfo(FeeWaiverID);
//                    if(SessionState.Escrow == "Y")
//                        Response.Redirect(CommonFunction.PHPMIMSiteUrl().TrimEnd('/') + "/students/tests/pretest.php?uid=" + SessionState.Username);
//                    else
                        Response.Redirect("Disclosure.aspx");
                }
                else
                {
                    DvError.InnerHtml = Cccs.Credability.Website.App.Translate("Credability|UserProfileBCH|IZC");
                }
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("WaiverPath.aspx");
        }

        protected void rblJointFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblJointFile.SelectedValue == "1")
                pnlSpouse.Visible = true;
            else
                pnlSpouse.Visible = false;
        }
    }
}