﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDisclosure.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.BKCounselingControls.UCDisclosure" %>
<span id="spnBKETitle" runat="server" visible="false">
<h1><%= Cccs.Credability.Website.App.Translate("Credability|BKE|PFY")%></h1>
<p>
    <font color="red"><%= Cccs.Credability.Website.App.Translate("Credability|BKE|ACCA")%></font>
</p>
</span>

<p>
    <asp:Label ID="lblFirstPara" runat="server" Text=""></asp:Label>
</p>
<ul>
    <% if (!IsEnglish)
       {%>
        <li><b><%= Cccs.Credability.Website.App.Translate("Credability|BKC|OCFC1")%></b></li>
    <% }%>
 
  <asp:Label ID="lblLi1" runat="server" Text=""></asp:Label>
</ul>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">

    function strip_lspaces(element) {
        if (element != '') {
            while ('' + element.charAt(0) == ' ') {
                element = element.substring(1, element.length);
            }
        }
        return element;
    }
</script>

<asp:HiddenField ID="hdnCharAmt" Value="" runat="server" />
<p class="col_title" id="PleaseSelectPaymentInstruction1" runat="server">
 <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PSY")%></p>
<asp:ValidationSummary ID="DesValidationSummary" ValidationGroup="user" CssClass="DvErrorSummary"
    DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" />
<div class="DvErrorSummary" id="dvErrorMessage" runat="server" visible="false">
</div>
<div class="dvform2col dvformlblbig" id="PleaseSelectPaymentInstruction" runat="server">
 <div class="dvform">
  <div class="dvrow">
    <asp:RadioButtonList ID="rblPaymentType" runat="server" AutoPostBack="true" RepeatDirection="Vertical"
    OnSelectedIndexChanged="rblPaymentType_SelectedIndexChanged" CssClass="rdolists">
   </asp:RadioButtonList>
   <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator14" runat="server" ControlToValidate="rblPaymentType"
                Display="Dynamic" Text="" ErrorMessage=" Select one of the Payment options."
                SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator><br />
            <br />
            <div align="center">
                <%= Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|PC")%></div>
  </div>
 </div>
 <div class="clearboth">
 </div>
</div>
<asp:HiddenField ID="hdnCurVancoRef" runat="server" />
<asp:HiddenField ID="hdnPaymentType" runat="server" />
<asp:Panel ID="pnl1" runat="server" Visible="false">
    <p align="center">
        <img border="0" src="../images/blankcheck.gif" height="240" width="390">
    </p>
    <div class="dvform2col dvformlblbig">
        <div class="dvform">
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|RNumber")%></label>
                <asp:TextBox ID="txtabanumber" runat="server" MaxLength="9"></asp:TextBox>
                <span class="requiredField">&nbsp;*</span><br />
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtabanumber"
                    Display="Dynamic" Text="" ErrorMessage=" Routing Number Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" SetFocusOnError="true"
                    ControlToValidate="txtabanumber" ValidationExpression="^[0-9]{9}$" Text="" ErrorMessage="Enter valid Routing Number must be 9 digits"
                    Display="Dynamic" ValidationGroup="user" />
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|NObank")%></label>
                <asp:TextBox ID="txtbankname" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YANum")%></label>
                <asp:TextBox ID="txtacctnumber" runat="server"></asp:TextBox>
                <span class="fieldValidator">&nbsp;*</span><br />
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtacctnumber"
                    Display="Dynamic" Text="" ErrorMessage=" Account Number Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|BPNum")%></label>
                <asp:TextBox ID="txtbankphone" runat="server"></asp:TextBox>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|NOT")%></label>
                <asp:TextBox ID="txtnameoncheck" runat="server"></asp:TextBox>
                <span class="fieldValidator">&nbsp;*</span><br />
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtnameoncheck"
                    Display="Dynamic" Text="" ErrorMessage=" Name on the Check Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnl2" runat="server" Visible="false">
    <div class="dvform2col dvformlblbig">
 <div class="dvform">
 <div>
                <p class="col_title">
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|FWB")%></p>
 </div>
  <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Card")%></label>
  <asp:TextBox ID="txtdcfnameoncard" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtdcfnameoncard"
                    Display="Dynamic" Text="" ErrorMessage=" First Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
  <asp:TextBox ID="txtdcmnameoncard" runat="server"></asp:TextBox>
                <asp:TextBox ID="txtdclnameoncard" runat="server" MaxLength="50"></asp:TextBox><span
                    class="requiredField">&nbsp;*&nbsp;</span>
  <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtdclnameoncard"
                    Display="Dynamic" Text="" ErrorMessage=" Last Name Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
  <asp:HiddenField ID="hdnDCFName" runat="server" Value="" />
  <asp:HiddenField ID="hdnDCMName" runat="server" Value="" />
  <asp:HiddenField ID="hdnDCLName" runat="server" Value="" />
  </div>
    <div class="dvrow">
                <p class="col_title">
                    <b>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Billing")%></b></p>
  </div>
    <div class="dvrow">
                <label>
                   <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Street")%></label>
  <asp:TextBox ID="txtdcbilladdr1" runat="server" MaxLength="100"></asp:TextBox>
      <span class="requiredField">&nbsp;*&nbsp;</span>
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtdcbilladdr1"
                    Display="Dynamic" Text="!" ErrorMessage=" Address Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
  </div>
    <div class="dvrow">
                <label>
                    <% if (IsEnglish)
                       {%> <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Address")%><%}%></label>
  <asp:TextBox ID="txtdcbilladdr2" runat="server"></asp:TextBox>
  </div>
    <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|City")%></label>
  <asp:TextBox ID="txtdcbillcity" runat="server" MaxLength="25"></asp:TextBox>
  <span class="requiredField">&nbsp;*&nbsp;</span>
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtdcbillcity"
                    Display="Dynamic" Text="" ErrorMessage=" City Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
  </div>
   <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|State")%></label>
   <asp:DropDownList ID="ddlState" runat="server">
        <asp:ListItem Value="">--select--</asp:ListItem>
        <asp:ListItem Value="AA">AA</asp:ListItem>
        <asp:ListItem Value="AE">AE</asp:ListItem>
        <asp:ListItem Value="AK">AK</asp:ListItem>
        <asp:ListItem Value="AL">AL</asp:ListItem>
        <asp:ListItem Value="AP">AP</asp:ListItem>
        <asp:ListItem Value="AR">AR</asp:ListItem>
        <asp:ListItem Value="AS">AS</asp:ListItem>
        <asp:ListItem Value="AZ">AZ</asp:ListItem>
        <asp:ListItem Value="CA">CA</asp:ListItem>
        <asp:ListItem Value="CO">CO</asp:ListItem>
        <asp:ListItem Value="CT">CT</asp:ListItem>
        <asp:ListItem Value="DC">DC</asp:ListItem>
        <asp:ListItem Value="DE">DE</asp:ListItem>
        <asp:ListItem Value="FL">FL</asp:ListItem>
        <asp:ListItem Value="FM">FM</asp:ListItem>
        <asp:ListItem Value="GA">GA</asp:ListItem>
        <asp:ListItem Value="GU">GU</asp:ListItem>
        <asp:ListItem Value="HI">HI</asp:ListItem>
        <asp:ListItem Value="IA">IA</asp:ListItem>
        <asp:ListItem Value="ID">ID</asp:ListItem>
        <asp:ListItem Value="IL">IL</asp:ListItem>
        <asp:ListItem Value="IN">IN</asp:ListItem>
        <asp:ListItem Value="KS">KS</asp:ListItem>
        <asp:ListItem Value="KY">KY</asp:ListItem>
        <asp:ListItem Value="LA">LA</asp:ListItem>
        <asp:ListItem Value="MA">MA</asp:ListItem>
        <asp:ListItem Value="MD">MD</asp:ListItem>
        <asp:ListItem Value="ME">ME</asp:ListItem>
        <asp:ListItem Value="MH">MH</asp:ListItem>
        <asp:ListItem Value="MI">MI</asp:ListItem>
        <asp:ListItem Value="MN">MN</asp:ListItem>
        <asp:ListItem Value="MO">MO</asp:ListItem>
        <asp:ListItem Value="MP">MP</asp:ListItem>
        <asp:ListItem Value="MS">MS</asp:ListItem>
        <asp:ListItem Value="MT">MT</asp:ListItem>
        <asp:ListItem Value="NC">NC</asp:ListItem>
        <asp:ListItem Value="ND">ND</asp:ListItem>
        <asp:ListItem Value="NE">NE</asp:ListItem>
        <asp:ListItem Value="NH">NH</asp:ListItem>
        <asp:ListItem Value="NJ">NJ</asp:ListItem>
        <asp:ListItem Value="NM">NM</asp:ListItem>
        <asp:ListItem Value="NV">NV</asp:ListItem>
        <asp:ListItem Value="NY">NY</asp:ListItem>
        <asp:ListItem Value="OH">OH</asp:ListItem>
        <asp:ListItem Value="OK">OK</asp:ListItem>
        <asp:ListItem Value="OR">OR</asp:ListItem>
        <asp:ListItem Value="PA">PA</asp:ListItem>
        <asp:ListItem Value="PR">PR</asp:ListItem>
        <asp:ListItem Value="PW">PW</asp:ListItem>
        <asp:ListItem Value="RI">RI</asp:ListItem>
        <asp:ListItem Value="SC">SC</asp:ListItem>
        <asp:ListItem Value="SD">SD</asp:ListItem>
        <asp:ListItem Value="TN">TN</asp:ListItem>
        <asp:ListItem Value="TX">TX</asp:ListItem>
        <asp:ListItem Value="UT">UT</asp:ListItem>
        <asp:ListItem Value="VA">VA</asp:ListItem>
        <asp:ListItem Value="VI">VI</asp:ListItem>
        <asp:ListItem Value="VT">VT</asp:ListItem>
        <asp:ListItem Value="WA">WA</asp:ListItem>
        <asp:ListItem Value="WI">WI</asp:ListItem>
        <asp:ListItem Value="WV">WV</asp:ListItem>
        <asp:ListItem Value="WY">WY</asp:ListItem>
       </asp:DropDownList>
    <span class="requiredField">&nbsp;*&nbsp;</span>
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="" ErrorMessage=" Select State" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
       <asp:TextBox ID="txtdcbillzip" runat="server" MaxLength="5"></asp:TextBox>
       <span class="requiredField">&nbsp;*&nbsp;</span>
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlState"
                    Display="Dynamic" Text="" ErrorMessage=" Select State" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtdcbillzip"
                    Display="Dynamic" Text="" ErrorMessage=" Zip Code Required" SetFocusOnError="True"
                    ValidationGroup="user"></asp:RequiredFieldValidator>
       <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="RegularExpressionValidator1" SetFocusOnError="true"
        ControlToValidate="txtdcbillzip" ValidationExpression="^[0-9]{5}$" Text="" ErrorMessage="Zip Code must be 5 digits"
        Display="Dynamic" ValidationGroup="user" />
   </div>
    <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Account")%></label>
   <asp:TextBox ID="txtdcacctnum" runat="server" MaxLength="20"></asp:TextBox>
   <span class="requiredField">&nbsp;*&nbsp;</span>
       <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtdcacctnum"
        Display="Dynamic" Text="" ErrorMessage=" Account Number Required" SetFocusOnError="True"
        ValidationGroup="user"></asp:RequiredFieldValidator>
   </div>
   <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|BKC|Exp")%></label>
   <asp:DropDownList ID="ddlCardExMnD" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Value="">--</asp:ListItem>
       </asp:DropDownList>
       <asp:DropDownList ID="ddlCardExYrD" runat="server" AppendDataBoundItems="true">
        <asp:ListItem Value="">--</asp:ListItem>
       </asp:DropDownList>
       <span class="requiredField">&nbsp;*&nbsp;</span>
   </div>
 </div>
 <div class="clearboth">
 </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnl3" runat="server" Visible="false">
      <p>
        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IOTP")%></p>
    <p>

           <asp:Label ID="lblMtcnAmt" runat="server" Text=""></asp:Label>
           <br />
           <ol class="NormTxt">
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|LFT")%>
            </li>
            <% if (Cccs.Credability.Website.App.WebsiteCode == Cccs.Identity.Website.BKDE) { %>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKDE|PTA")%>
            </li>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKDE|CC")%>
            </li>
            <% } else { %>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PTA")%>
            </li>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|CC")%>
            </li>
            <% } %>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SG")%>
            </li>
            <li>
                <%= Cccs.Credability.Website.App.Translate("Credability|BKC|SAN")%>
                <asp:Label ID="lblClientNumber" runat="server" Text=""></asp:Label>
            </li>
           </ol>
        <p>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IHN")%></p>
        <p>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|IYAMY")%></p>
      <div class="dvform2col dvformlblbig">
 <div class="dvform">
  <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|BKC|MTCNN")%></label>
                    <asp:TextBox ID="txtmtcn" runat="server" MaxLength="10"></asp:TextBox>
                    <span class="requiredField">&nbsp;*&nbsp;</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtmtcn"
     Display="Dynamic" Text="" SetFocusOnError="True" ValidationGroup="user"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator class="validationMessage" runat="server" ID="RegularExpressionValidator2" SetFocusOnError="true"
                        ControlToValidate="txtmtcn" ValidationExpression="^[0-9]{10}$" Text="" Display="Dynamic"
                        ValidationGroup="user" />
  </div>
 </div>
 <div class="clearboth">
 </div>
        </div>
        <p align="center">
            <br />
            <img alt="Western Union Example" src="../images/WU2.gif"> </img>
            <br />
        </p>
</asp:Panel>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ValidationGroup="user">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
        </asp:LinkButton>
        <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
            <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
                <ProgressTemplate>
                    <div style="position: relative; top: 30%; text-align: center;">
                        <img src="../images/loading.gif" style="vertical-align: middle" alt="Processing" />
                        <%= Cccs.Credability.Website.App.Translate("Processing")%> ...
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </asp:Panel>
        <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress"
            BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
    </div>
</div>

<script type="text/javascript" language="javascript">
    var ModalProgress = '<%= ModalProgress.ClientID %>';
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);

    function beginReq(sender, args) {
        // shows the Popup     
        $find(ModalProgress).show();
    }

    function endReq(sender, args) {
        //  shows the Popup
        $find(ModalProgress).hide();
    }
</script>
