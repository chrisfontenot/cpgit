﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;
using Cccs.Web;
using System.Text;
using Cccs.Picklist;
using System.Xml;
using Cccs.Vanco;
using Cccs.Credability.Dal;
using MMI.FirstData.Dto;
using MMI.FirstData;
using System.Configuration;

namespace Cccs.Credability.Website.Controls.BKCounselingControls
{
    public partial class UCDisclosure : System.Web.UI.UserControl
    {
        #region Control Parameters

        public string RedirectOnContinue
        {
            get
            {
                string url = ViewState.ValueGet<string>("RedirectOnContinue");

                return !string.IsNullOrEmpty(url) ? url : "ProvideYourPersonalInformation.aspx";
            }

            set
            {
                ViewState.ValueSet("RedirectOnContinue", value);
            }
        }


        #endregion

        public string escrow = string.Empty;
        public bool IsEnglish = true;

        private NLog.Logger logger;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();

            logger.Debug("UCDisclosure Page Load");

            logger.Debug("UCDisclosure Page Load FirmID: " + (string.IsNullOrEmpty(SessionState.FirmID) ? "Null" : SessionState.FirmID));

            logger.Debug("UCDisclosure Page Load PrimaryZipCode: " + (string.IsNullOrEmpty(SessionState.PrimaryZipCode) ? "Null" : SessionState.PrimaryZipCode));

            if (String.IsNullOrEmpty(SessionState.FirmID) || String.IsNullOrEmpty(SessionState.PrimaryZipCode))
            {
                var myUrl = HttpContext.Current.Request.Url.AbsolutePath;

                logger.Debug("UCDisclosure Page Load URL: " + myUrl);

                Response.Redirect(ResolveUrl("~/GetRequiredFeeInformation.aspx?redirect=" + myUrl));
            }

            if (!IsPostBack)
            {
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }

            if (SessionState.LanguageCode == "ES")
                IsEnglish = false;

            //Result<Double> AmtResult;
            Double charAmt = 0;

            //rate now varies for both counseling and debtor ed., so we need to pass a code in to "GetCharAmt"
            try
            {
                logger.Debug("UCDisclosure Page Load - calling GetCharAmt");

                try
                {
                    //Bankruptcy conversion-Seethal
                    //AmtResult = App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode,
                    //App.WebsiteCode == Cccs.Identity.Website.BKDE ? "W" : "B");

                    charAmt = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode,
                   App.WebsiteCode == Cccs.Identity.Website.BKDE ? "W" : "B");

                    logger.Debug("UCDisclosure Page Load - call to GetCharAmt complete");

                    logger.Debug("UCDisclosure Page Load - AmtResult value : " + charAmt.ToString());

                    SessionState.ChargeAmount = charAmt.ToString();

                    if (charAmt > 0)
                    {
                        charAmt = Convert.ToDouble(charAmt.ToString());
                    }
                }
                catch (Exception ex)
                {
                    logger.Debug("UCDisclosure Page Load - Exception Message: " + ex.Message);
                    logger.Debug("UCDisclosure Page Load - Exception StackTrace: " + ex.StackTrace);

                    if (ex.InnerException != null)
                    {
                        logger.Debug("UCDisclosure Page Load - Inner Exception Message: " + ex.InnerException.Message);
                        logger.Debug("UCDisclosure Page Load - Inner Exception StackTrace: " + ex.InnerException.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogException(NLog.LogLevel.Fatal, "Exception while trying to get fee from Host", ex);
            }

            if (App.WebsiteCode == Cccs.Identity.Website.BKC)
            {
                lblFirstPara.Text = string.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|PBA"), charAmt.ToString("C"));
                lblLi1.Text = "<li>" + string.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|OCFC"), charAmt.ToString("C")) + "</li>";
                lblLi1.Text += "<li>" + App.Translate("Credability|BKC|TRAC") + "</li>";
            }
            else
            {
                lblFirstPara.Text = string.Format(Cccs.Credability.Website.App.Translate("Credability|BKE|PBA"), charAmt.ToString("C"));
                lblLi1.Text = "<li>" + string.Format(Cccs.Credability.Website.App.Translate("Credability|BKE|OCFC"), charAmt.ToString("C")) + "</li>";
            }

            lblMtcnAmt.Text = string.Format(Cccs.Credability.Website.App.Translate("Credability|BKC|YWN"), charAmt.ToString("C"));
            lblClientNumber.Text = SessionState.ClientNumber.ToString();

            if (!IsPostBack)
            {
                SessionState.UserPageArrivalTime = DateTime.Now;
                //AddupdatetimeStampValue();
                CommonFunction.AddItems(rblPaymentType.Items, "PaymentOptionsRadioButton", SessionState.LanguageCode);
                UserDisclosureBKCGet(Convert.ToInt32(SessionState.ClientNumber));
                BindExpdate();
                ValidationControlTranslation();


                HasPayed();
                if (SessionState.Escrow == "Y")
                {
                    PleaseSelectPaymentInstruction.Visible = false;
                    PleaseSelectPaymentInstruction1.Visible = false;
                    RequiredFieldValidator14.Enabled = false;
                }
                else
                {
                    PleaseSelectPaymentInstruction.Visible = true;
                    PleaseSelectPaymentInstruction1.Visible = true;
                    RequiredFieldValidator14.Enabled = true;
                }
            }

        }

        public void HasPayed()
        {
            App.SetEscrowInfo();

            String Status = "";
            Int32 CurVanCoRef = 0, Completed = 0, Processed = 0;

            if (rblPaymentType.SelectedValue == "D")
            {
                DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounseling = null;

                DisclosureContactDetailsBKCounseling = App.Credability.DisclosureContactDetailsBKCounselingGet(Convert.ToInt32(SessionState.ClientNumber));

                if (DisclosureContactDetailsBKCounseling != null)
                {
                    CurVanCoRef = DisclosureContactDetailsBKCounseling.CurVancoRef;
                    Completed = DisclosureContactDetailsBKCounseling.Completed;
                    Processed = DisclosureContactDetailsBKCounseling.Processed;

                    if (Completed == 1 && Processed != 2)
                        Response.Redirect(RedirectOnContinue);
                }

                DisclosureVancoRefBKCounseling DisclosureVancoRefBKCounseling = null;
                DisclosureVancoRefBKCounseling = App.Credability.DisclosureVancoRefBKCounselingGet(Convert.ToInt32(hdnCurVancoRef.Value));
                if (DisclosureVancoRefBKCounseling != null)
                {
                    Status = DisclosureVancoRefBKCounseling.Status;
                    if (Status == "Payed")
                        Response.Redirect(RedirectOnContinue);
                }
            }
        }

        public void AddupdatetimeStampValue()
        {
            CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
        }

        private void ValidationControlTranslation()
        {
            RequiredFieldValidator14.ErrorMessage = App.Translate("Credability|BKC|SPO");
            RequiredFieldValidator3.ErrorMessage = App.Translate("Credability|DescYourSituRVM|RNR");
            rfvtxtZip.ErrorMessage = App.Translate("Credability|UserProfileBCH|EVR");
            RequiredFieldValidator4.ErrorMessage = App.Translate("Credability|UserProfileBCH|ANR");
            RequiredFieldValidator5.ErrorMessage = App.Translate("Credability|UserProfileBCH|NOT");
            RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|UserProfileBCH|FINR");

            RequiredFieldValidator6.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LANR");
            RequiredFieldValidator7.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ADR");
            RequiredFieldValidator8.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CIRE");
            RequiredFieldValidator10.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SS");

            RequiredFieldValidator11.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ZCR");
            RegularExpressionValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ZCM");
            RequiredFieldValidator13.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ACCN");
            RequiredFieldValidator12.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MTCN");
            RegularExpressionValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IMCTN");
            RequiredFieldValidator9.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SES");

            DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
        }

        private void BindExpdate()
        {

            for (int i = 1; i <= 12; i++)
            {
                ListItem li = new ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                ddlCardExMnD.Items.Add(li);
            }
            ddlCardExMnD.DataBind();

            for (int CurYear = DateTime.Now.Year; CurYear <= DateTime.Now.Year + 10; CurYear++)
            {
                ListItem liYear = new ListItem();
                liYear.Text = CurYear.ToString().Substring(2);
                liYear.Value = CurYear.ToString().Substring(2);
                ddlCardExYrD.Items.Add(liYear);
            }
            ddlCardExYrD.DataBind();

        }

        public void GetUserDetailsFromIDM(long user_id)
        {

            // Primary UserDetail
            //
            UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);

            if (user_detail_primary != null)
            {
                hdnDCFName.Value = user_detail_primary.FirstName.Clean();
                hdnDCMName.Value = user_detail_primary.MiddleName.Clean();
                hdnDCLName.Value = user_detail_primary.LastName.Clean();
            }

            // Secondary UserDetail
            //
            UserDetail user_detail_secondary = App.Identity.UserDetailGet(user_id, false);
        }

        public DisclosureContactDetailsBKCounseling UserDisclosureBKCGet(Int32 ClientNumber)
        {
            DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounseling = null;

            DisclosureContactDetailsBKCounseling = App.Credability.DisclosureContactDetailsBKCounselingGet(ClientNumber);

            if (DisclosureContactDetailsBKCounseling != null)
            {
                // If a payment has already been made, then continue to the next page.
                if (!String.IsNullOrEmpty(DisclosureContactDetailsBKCounseling.paymentTYPE))
                {
                    Response.Redirect(RedirectOnContinue);
                }

                hdnCurVancoRef.Value = DisclosureContactDetailsBKCounseling.CurVancoRef.ToString();

                if (SessionState.Escrow == "Y")
                {
                    pnl1.Visible = false;
                    pnl2.Visible = false;
                    pnl3.Visible = false;
                }
                else if (rblPaymentType.SelectedValue == "C")
                {
                    pnl1.Visible = true;
                    pnl2.Visible = false;
                    pnl3.Visible = false;
                }
                else if (rblPaymentType.SelectedValue == "D")
                {
                    pnl1.Visible = false;
                    pnl2.Visible = true;
                    pnl3.Visible = false;
                }
                else if (rblPaymentType.SelectedValue == "W")
                {
                    pnl1.Visible = false;
                    pnl2.Visible = false;
                    pnl3.Visible = true;
                }

            }

            return DisclosureContactDetailsBKCounseling;
        }

        private DisclosureContactDetailsBKCounselingResult SaveContactAccountDetails(Int32 ClientNumber)
        {
            logger.Info("Begin SaveContactAccountDetails");
            logger.DebugFormat("ClientNumber: {0}", ClientNumber);

            DisclosureContactDetailsBKCounseling DisclosureContactDetailsBKCounseling = new DisclosureContactDetailsBKCounseling();
            DisclosureContactDetailsBKCounselingResult Result = null;

            DisclosureContactDetailsBKCounseling.ClientNumber = ClientNumber;
            DisclosureContactDetailsBKCounseling.adsNAMEONCHECK = txtnameoncheck.Text;
            DisclosureContactDetailsBKCounseling.ADSbankname = txtbankname.Text;
            DisclosureContactDetailsBKCounseling.ADSbankphone = txtbankphone.Text;
            DisclosureContactDetailsBKCounseling.abaNUMBER = txtabanumber.Text.Trim();
            DisclosureContactDetailsBKCounseling.acctNUMBER = txtacctnumber.Text.Trim();
            DisclosureContactDetailsBKCounseling.paymentTYPE = rblPaymentType.SelectedValue;
            DisclosureContactDetailsBKCounseling.dcFNAMEONCARD = txtdcfnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcLNAMEONCARD = txtdclnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcMNAMEONCARD = txtdcmnameoncard.Text;
            DisclosureContactDetailsBKCounseling.dcBILLADDR = txtdcbilladdr1.Text;
            DisclosureContactDetailsBKCounseling.dcBILLADDR2 = txtdcbilladdr2.Text;
            DisclosureContactDetailsBKCounseling.dcBILLCITY = txtdcbillcity.Text;
            DisclosureContactDetailsBKCounseling.dcBILLSTATE = ddlState.SelectedValue;
            DisclosureContactDetailsBKCounseling.dcBILLZIP = txtdcbillzip.Text;
            DisclosureContactDetailsBKCounseling.dcACCTNUM = txtdcacctnum.Text;
            DisclosureContactDetailsBKCounseling.dcEXPDATE = ddlCardExMnD.SelectedValue + "/" + ddlCardExYrD.SelectedValue;
            DisclosureContactDetailsBKCounseling.mtcn = txtmtcn.Text;
            DisclosureContactDetailsBKCounseling.CurVancoRef = Convert.ToInt32(hdnCurVancoRef.Value);

            Result = App.Credability.DisclosureContactDetailsBKCounselingAddUpdate(DisclosureContactDetailsBKCounseling);

            logger.DebugFormat("DisclosureContactDetailsBKCounselingAddUpdate Success: {0}", Result.IsSuccessful);
            logger.Info("End SaveContactAccountDetails");

            return Result;
        }

        private IVancoRef SaveVancoRefDetails(Int32 clientNumber)
        {
            logger.Info("Begin SaveVancoRefDetails");
            logger.DebugFormat("ClientNumber: {0}", clientNumber);

            var DisclosureVancoRefBKCounseling = new DisclosureVancoRefBKCounseling()
            {
                REGInNum = clientNumber,
                CARDFirstNameEnc = txtdcfnameoncard.Text,
                CARDMidNameEnc = txtdcmnameoncard.Text,
                CARDLastNameEnc = txtdclnameoncard.Text,
                CARDAddrEnc = txtdcbilladdr1.Text.Trim(),
                CARDAddr2Enc = txtdcbilladdr2.Text.Trim(),
                CARDCityEnc = txtdcbillcity.Text,
                CARDSTEnc = ddlState.SelectedValue,
                CARDZipEnc = txtdcbillzip.Text,
                CARDNumberEnc = txtdcacctnum.Text,
                CARDExpMonEnc = ddlCardExMnD.SelectedValue,
                CARDExpYearEnc = ddlCardExYrD.SelectedValue,
                CREATEDTS = DateTime.Now,
                Status = "Unpayed",
            };

            var vancoRef = App.Credability.DisclosureVancoRefAddUpdate(DisclosureVancoRefBKCounseling);

            logger.DebugFormat("DisclosureVancoRefAddUpdate ReqId: {0}", vancoRef.ReqID);
            logger.Info("End SaveVancoRefDetails");

            return vancoRef;
        }

        private IVancoRef SaveVancoMimRefDetails(Int32 RegInNum)
        {
            logger.Info("Begin SaveVancoMimRefDetails");
            logger.DebugFormat("RegInNum: {0}", RegInNum);

            var DisclosureVancoRefBKCounseling = new DisclosureVancoRefBKCounseling()
            {
                REGInNum = RegInNum,
                CARDFirstNameEnc = txtdcfnameoncard.Text,
                CARDMidNameEnc = txtdcmnameoncard.Text,
                CARDLastNameEnc = txtdclnameoncard.Text,
                CARDAddrEnc = txtdcbilladdr1.Text.Trim(),
                CARDAddr2Enc = txtdcbilladdr2.Text.Trim(),
                CARDCityEnc = txtdcbillcity.Text,
                CARDSTEnc = ddlState.SelectedValue,
                CARDZipEnc = txtdcbillzip.Text,
                CARDNumberEnc = txtdcacctnum.Text,
                CARDExpMonEnc = ddlCardExMnD.SelectedValue,
                CARDExpYearEnc = ddlCardExYrD.SelectedValue,
                CREATEDTS = DateTime.Now,
                Status = "Unpayed",
            };

            var vancoRef = App.Credability.DisclosureVancoMimRefAddUpdate(DisclosureVancoRefBKCounseling);

            logger.DebugFormat("DisclosureVancoMimRefAddUpdate ReqId: {0}", vancoRef.ReqID);
            logger.Info("End SaveVancoMimRefDetails");

            return vancoRef;
        }

        protected void rblPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dvErrorMessage.Visible = false;
            if (SessionState.Escrow == "Y")
            {
                pnl1.Visible = false;
                pnl2.Visible = false;
                pnl3.Visible = false;
            }
            else if (rblPaymentType.SelectedValue == "C")
            {
                hdnPaymentType.Value = "C";
                pnl1.Visible = true;
                pnl2.Visible = false;
                pnl3.Visible = false;
            }
            else if (rblPaymentType.SelectedValue == "D")
            {
                hdnPaymentType.Value = "D";
                pnl1.Visible = false;
                pnl2.Visible = true;
                pnl3.Visible = false;
            }
            else if (rblPaymentType.SelectedValue == "W")
            {
                hdnPaymentType.Value = "W";
                pnl1.Visible = false;
                pnl2.Visible = false;
                pnl3.Visible = true;
            }
        }

        private bool WesternUnionCheck()
        {
            bool isValid = false;
            try
            {
                if (WesternUnion.IsMCTNValid(txtmtcn.Text))
                {
                    SaveBkeWesternUnionDetails();
                    isValid = true;
                    dvErrorMessage.Visible = false;
                }
                else
                {
                    dvErrorMessage.Visible = true;
                    dvErrorMessage.InnerHtml = "Invalid MTCN Number";
                }
            }
            catch (Exception ex)
            {

                logger.ErrorFormat("Exception in SaveBkeDebitDetails", ex.Message);
            }

            return isValid;
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            if (SessionState.Escrow == "Y")
            {
                Response.Redirect(RedirectOnContinue);
            }

            if (!String.IsNullOrEmpty(txtacctnumber.Text) && txtacctnumber.Text.Length < 4)
            {
                dvErrorMessage.Visible = true;
                dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|ANR");
                return;
            }

            logger.Info("Starting payment.");

            logger.DebugFormat("CharAmt: {0}", hdnCharAmt.Value);
            logger.DebugFormat("Payment Type: {0}", rblPaymentType.SelectedValue);
            logger.DebugFormat("Check Account Number Last 4: {0}", !String.IsNullOrEmpty(txtacctnumber.Text) ? txtacctnumber.Text.Substring(txtacctnumber.Text.Length - 4, 4) : String.Empty);
            logger.DebugFormat("Check Name on check: {0}", txtnameoncheck.Text);
            logger.DebugFormat("Check Bank Name: {0}", txtbankname.Text);
            logger.DebugFormat("Check Bank Phone: {0}", txtbankphone.Text);
            logger.DebugFormat("Debit Card First Name on card: {0}", txtdcfnameoncard.Text);
            logger.DebugFormat("Debit Card Middle Name on card: {0}", txtdcmnameoncard.Text);
            logger.DebugFormat("Debit Card Last Name on card: {0}", txtdclnameoncard.Text);
            logger.DebugFormat("Debit Card Billing Address 1: {0}", txtdcbilladdr1.Text);
            logger.DebugFormat("Debit Card Billing Address 2: {0}", txtdcbilladdr2.Text);
            logger.DebugFormat("Debit Card Billing City: {0}", txtdcbillcity.Text);
            logger.DebugFormat("Debit Card Billing State: {0}", ddlState.SelectedValue);
            logger.DebugFormat("Debit Card Billing Zip: {0}", txtdcbillzip.Text);
            logger.DebugFormat("Debit Card Account Number Last 4: {0}", !String.IsNullOrEmpty(txtdcacctnum.Text) ? txtdcacctnum.Text.Substring(txtdcacctnum.Text.Length - 4, 4) : String.Empty);
            logger.DebugFormat("Debit Card Expire Month: {0}", ddlCardExMnD.SelectedValue);
            logger.DebugFormat("Debit Card Expire Year: {0}", ddlCardExYrD.SelectedValue);
            logger.DebugFormat("Western Union MTCN Number: {0}", txtmtcn.Text);

            if (rblPaymentType.SelectedValue == "D")
            {
                int month;
                int year;
                if (!Int32.TryParse(ddlCardExMnD.SelectedValue, out month) || !Int32.TryParse(ddlCardExYrD.SelectedValue, out year))
                {
                    dvErrorMessage.Visible = true;
                    dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                    dvErrorMessage.InnerHtml += "<li>" + App.Translate("SelectDebitExpiration") + "</li></ul>"; //Select one of the Payment options.
                    return;
                }
            }

            if (rblPaymentType.SelectedValue != "C" && rblPaymentType.SelectedValue != "D" && rblPaymentType.SelectedValue != "W")
            {
                dvErrorMessage.Visible = true;
                dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|BKC|SPO") + "</li></ul>"; //Select one of the Payment options.

                logger.ErrorFormat("Error Condition 1: {0}", dvErrorMessage.InnerHtml);
            }
            else
            {
                #region BKE
                if (App.WebsiteCode == Cccs.Identity.Website.BKDE)
                {
                    bool isValid = true;
                    dvErrorMessage.Visible = false;
                    switch (rblPaymentType.SelectedValue)
                    {
                        case "C":
                            isValid = SaveBkeCheckDetails();
                            break;

                        case "D":
                            isValid = SaveBkeDebitDetails();
                            break;

                        case "W":
                            isValid = WesternUnionCheck();
                            break;

                        default:
                            isValid = false;
                            throw new InvalidOperationException("Unrecognized payment type.");
                    }
                    if (isValid) Response.Redirect(RedirectOnContinue);
                }
                #endregion

                #region BKC
                if (App.WebsiteCode == Cccs.Identity.Website.BKC)
                {
                    dvErrorMessage.InnerHtml = "";
                    dvErrorMessage.Visible = false;
                    DisclosureContactDetailsBKCounselingResult Result = null;

                    if (rblPaymentType.SelectedValue == "C")
                    {
                        logger.DebugFormat("Check ABA Number: {0}", txtabanumber.Text);

                        String abaFirstChar = txtabanumber.Text.Substring(0, 1);
                        if ((abaFirstChar != "0") && (abaFirstChar != "1") && (abaFirstChar != "2") && (abaFirstChar != "3"))
                        {
                            dvErrorMessage.Visible = true;
                            dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                            dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|BKC|RB0") + "</li></ul>"; //The Routing# must begin with 0,1,2 or 3 

                            logger.ErrorFormat("Error Condition 2: {0}", dvErrorMessage.InnerHtml);
                        }
                        else
                        {
                            abadetailBKCounseling abadetailBKCounseling = null;

                            abadetailBKCounseling = App.Credability.AbaDetailsGet(txtabanumber.Text.Trim());
                            if (abadetailBKCounseling != null)
                            {
                                var NameofBank = (abadetailBKCounseling.ABAname != null) ? abadetailBKCounseling.ABAname : String.Empty;
                                var BankPhoneNumber = (abadetailBKCounseling.ABAphone != null) ? abadetailBKCounseling.ABAphone : String.Empty;

                                txtbankname.Text = NameofBank;
                                txtbankphone.Text = BankPhoneNumber;

                                Result = SaveContactAccountDetails(Convert.ToInt32(SessionState.ClientNumber));
                                if (Result.IsSuccessful == false)
                                {
                                    dvErrorMessage.Visible = true;
                                    dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                                    if (Result.Exception != null)
                                    {
                                        dvErrorMessage.InnerHtml += "<li>" + Result.Exception.ToString() + "</li></ul>";
                                        logger.Error(String.Format("Error Condition 3: {0}", dvErrorMessage.InnerHtml), Result.Exception);
                                    }
                                    else
                                    {
                                        logger.ErrorFormat("Error Condition 4: {0}", dvErrorMessage.InnerHtml);
                                    }
                                }
                            }
                            else
                            {
                                dvErrorMessage.Visible = true;
                                dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                                dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|BKC|RNDB") + "</li></ul>"; //The Routing# is not found in the national database.

                                logger.ErrorFormat("Error Condition 5: {0}", dvErrorMessage.InnerHtml);
                            }
                        }

                    }
                    else if (rblPaymentType.SelectedValue == "D")
                    {
                        IVanco vanco = App.Credability.GetVancoManager(true, IsEnglish);
                        IVancoRef vancoRef = SaveVancoRefDetails(SessionState.ClientNumber.Value);
                        Result = SaveContactAccountDetails(SessionState.ClientNumber.Value);
                        if (Result.IsSuccessful == false)
                        {
                            dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                            dvErrorMessage.InnerHtml += "<li>" + Result.Exception.ToString() + "</li>";
                            logger.ErrorFormat("Error Condition 6: {0}", dvErrorMessage.InnerHtml);
                        }

                        //double charAmount = App.Host.GetCharAmt(SessionState.ChargeAmount).Value;
                        double charAmount = App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B");
                        var clientNumber = SessionState.ClientNumber.Value;
                        VancoDraft vanco_draft = GetVancoDraft(charAmount, clientNumber);

                        string refCode = ConfigurationManager.AppSettings["BKCounselingReferenceCode"];
                        string memoValue = ConfigurationManager.AppSettings["BKCounselingMemo"];
                        string referenceCode = string.IsNullOrEmpty(refCode) ? "BKCounseling" : refCode;
                        string memo = string.IsNullOrEmpty(memoValue) ? "Web" : memoValue;

                        // Using the Vanco table structure for FirstData and replace only the payment API
                        SaveVancoDraft(vanco, vanco_draft, vancoRef, referenceCode, memoValue);

                        App.Credability.DoHostTransactionUpdate(vanco_draft, vancoRef);
                        App.Credability.VancoRefSave(vancoRef);
                    }
                    else if (rblPaymentType.SelectedValue == "W")
                    {
                        try
                        {
                            if (WesternUnion.IsMCTNValid(txtmtcn.Text))
                            {
                                Result = SaveContactAccountDetails(Convert.ToInt32(SessionState.ClientNumber));
                                if (Result.IsSuccessful == false)
                                {
                                    dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                                    dvErrorMessage.InnerHtml += "<li>" + Result.Exception.ToString() + "</li>";
                                    logger.ErrorFormat("Error Condition 8: {0}", dvErrorMessage.InnerHtml);
                                }
                            }
                            else
                                dvErrorMessage.InnerHtml = "Invalid MTCN Number";
                        }

                        catch (Exception ex)
                        {
                            dvErrorMessage.InnerHtml = ex.Message;
                        }
                    }

                    if (dvErrorMessage.InnerHtml == "")
                    {
                        AddupdatetimeStampValue();

                        logger.InfoFormat("Redirect: {0}", RedirectOnContinue);
                        Response.Redirect(RedirectOnContinue);
                    }
                    else
                    {
                        dvErrorMessage.Visible = true;
                    }
                }
                #endregion
            }

            if (dvErrorMessage.InnerHtml.Length > 0)
            {
                logger.ErrorFormat("Error Condition Final: {0}", dvErrorMessage.InnerHtml);
            }
        }

        private void SaveBkeWesternUnionDetails()
        {
            //Bankruptcy conversion-Seethal
            //var result = App.Host.MimDisclosureSaveMtcn(SessionState.Clid, txtmtcn.Text, SessionState.ChargeAmount);
            var result = App.Debtplus.MimDisclosureSaveMtcn(SessionState.Clid, txtmtcn.Text, SessionState.ChargeAmount);
            
            if (result.IsSuccessful == false)
            {
                dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                dvErrorMessage.InnerHtml += "<li>" + result.Exception.ToString() + "</li>";
                logger.ErrorFormat("Error Condition 8: {0}", dvErrorMessage.InnerHtml);
            }
        }

        private bool SaveBkeDebitDetails()
        {
            bool isValid = false;
            try
            {
                logger.Debug("Tracing SaveBkeDebitDetails...");

                logger.Debug("calling SaveVancoMimRefDetails...");
                var vancoRef = SaveVancoMimRefDetails(Convert.ToInt32(SessionState.RegNum));
                logger.Debug("SaveVancoMimRefDetails complete...");

                logger.Debug("calling GetMimSequenceNumber...");
                // Bankruptcy Conversion
                //var sequenceNumber = App.Host.GetMimSequenceNumber(SessionState.Clid);
                var sequenceNumber = App.Debtplus.GetMimSequenceNumber(SessionState.Clid);
                logger.DebugFormat("Sequence Number: {0}", sequenceNumber);

                logger.Debug("calling MimDisclosureGetChargeAmount...");
                //Bankruptcy conversion-Seethal
                //var charAmount = App.Host.MimDisclosureGetChargeAmount(SessionState.PrimaryZipCode, SessionState.FirmID);
                var charAmount = App.Debtplus.MimDisclosureGetChargeAmount(SessionState.PrimaryZipCode, SessionState.FirmID);
                logger.DebugFormat("Charge Amount: {0}", charAmount);

                var clientNumber = Convert.ToInt32(SessionState.RegNum);
                logger.DebugFormat("Client Number: {0}", clientNumber);

                logger.Debug("calling GetVancoManager...");
                var vanco = App.Credability.GetVancoManager(false, IsEnglish);
                logger.DebugFormat("Vanco Deails: ", vanco.LoginDts);

                logger.Debug("calling GetVancoDraft...");
                var vanco_draft = GetVancoDraft(charAmount, clientNumber);
                logger.DebugFormat("Vanco Draft Details: {0}", vanco_draft.ClientNumber);

                logger.Debug("calling SaveVancoDraft...");
                string refCode = ConfigurationManager.AppSettings["BKEducationReferenceCode"];
                string memoValue = ConfigurationManager.AppSettings["BKEducationMemo"];
                string referenceCode = string.IsNullOrEmpty(refCode) ? "BKEducation" : refCode;
                string memo = string.IsNullOrEmpty(memoValue) ? "Web" : memoValue;

                // Using the Vanco table structure for FirstData and replace only the payment API
                SaveVancoDraft(vanco, vanco_draft, vancoRef, referenceCode, memo);

                logger.Debug("calling VancoMimRefSave...");
                App.Credability.VancoMimRefSave(vancoRef);

                logger.Debug("calling Mim_Disclosure_SaveTransaction...");

                // Bankruptcy Conversion
                //var result = App.Host.Mim_Disclosure_SaveTransaction(SessionState.Clid
                //    , txtdcfnameoncard.Text
                //    , txtdcmnameoncard.Text
                //    , txtdclnameoncard.Text
                //    , txtdcbilladdr1.Text
                //    , txtdcbillcity.Text
                //    , ddlState.SelectedValue
                //    , txtdcbillzip.Text
                //    , txtdcacctnum.Text
                //    , String.Format("{0}/{1}", ddlCardExMnD.SelectedValue, ddlCardExYrD.SelectedValue)
                //    , String.Format("WS{0}-{1}", SessionState.RegNum, sequenceNumber.ToString())
                //    , sequenceNumber.ToString()
                //    , vancoRef.VanTransRef
                //    //get it from session state so that we have only one way to retrieve this information 
                //    , SessionState.ChargeAmount
                //);

                var result = App.Debtplus.Mim_Disclosure_SaveTransaction(SessionState.Clid
                    , txtdcfnameoncard.Text
                    , txtdcmnameoncard.Text
                    , txtdclnameoncard.Text
                    , txtdcbilladdr1.Text
                    , txtdcbillcity.Text
                    , ddlState.SelectedValue
                    , txtdcbillzip.Text
                    , txtdcacctnum.Text
                    , String.Format("{0}/{1}", ddlCardExMnD.SelectedValue, ddlCardExYrD.SelectedValue)
                    , String.Format("WS{0}-{1}", SessionState.RegNum, sequenceNumber.ToString())
                    , sequenceNumber.ToString()
                    , vancoRef.VanTransRef
                    //get it from session state so that we have only one way to retrieve this information 
                    , SessionState.ChargeAmount
                );

                if (result == null)
                {
                    logger.Error("Result is null");
                }
                else
                {
                    isValid = result.IsSuccessful;
                    if (result.IsSuccessful == false)
                    {
                        dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                        dvErrorMessage.InnerHtml += "<li>" + result.Exception.ToString() + "</li>";
                        logger.ErrorFormat("Error Condition 6: {0}", dvErrorMessage.InnerHtml);
                    }
                }
            }
            catch (Exception ex) { logger.ErrorFormat("Exception in SaveBkeDebitDetails Message: {0} StackTrace: {1}", ex.Message, ex.StackTrace); }
            return isValid;
        }

        private bool SaveBkeCheckDetails()
        {
            bool isValid = false;
            try
            {

                logger.DebugFormat("Check ABA Number: {0}", txtabanumber.Text);

                String abaFirstChar = txtabanumber.Text.Substring(0, 1);
                if ((abaFirstChar != "0") && (abaFirstChar != "1") && (abaFirstChar != "2") && (abaFirstChar != "3"))
                {
                    dvErrorMessage.Visible = true;
                    dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                    dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|BKC|RB0") + "</li></ul>"; //The Routing# must begin with 0,1,2 or 3 

                    logger.ErrorFormat("Error Condition 2: {0}", dvErrorMessage.InnerHtml);
                }
                else
                {
                    var abadetailBKCounseling = App.Credability.AbaDetailsGet(txtabanumber.Text.Trim());
                    if (abadetailBKCounseling != null)
                    {
                        var NameofBank = (abadetailBKCounseling.ABAname != null) ? abadetailBKCounseling.ABAname : String.Empty;
                        var BankPhoneNumber = (abadetailBKCounseling.ABAphone != null) ? abadetailBKCounseling.ABAphone : String.Empty;

                        txtbankname.Text = NameofBank;
                        txtbankphone.Text = BankPhoneNumber;

                        //Bankruptcy conversion-Seethal
                        //var result = App.Host.MimDisclosureSaveAch(SessionState.Clid, NameofBank, txtnameoncheck.Text, txtabanumber.Text, txtacctnumber.Text, SessionState.ChargeAmount);
                        var result = App.Debtplus.MimDisclosureSaveAch(SessionState.Clid, NameofBank, txtnameoncheck.Text, txtabanumber.Text, txtacctnumber.Text, SessionState.ChargeAmount);
                        isValid = result.IsSuccessful;
                        if (!result.IsSuccessful)
                        {
                            dvErrorMessage.Visible = true;
                            dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                            if (result.Exception != null)
                            {
                                dvErrorMessage.InnerHtml += "<li>" + result.Exception.ToString() + "</li></ul>";
                                logger.Error(String.Format("Error Condition 3: {0}", dvErrorMessage.InnerHtml), result.Exception);
                            }
                            else
                            {
                                logger.ErrorFormat("Error Condition 4: {0}", dvErrorMessage.InnerHtml);
                            }
                        }
                    }
                    else
                    {
                        dvErrorMessage.Visible = true;
                        dvErrorMessage.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
                        dvErrorMessage.InnerHtml += "<li>" + App.Translate("Credability|BKC|RNDB") + "</li></ul>"; //The Routing# is not found in the national database.

                        logger.ErrorFormat("Error Condition 5: {0}", dvErrorMessage.InnerHtml);
                    }
                }
            }
            catch (Exception ex) { logger.ErrorFormat("Exception in SaveBkeCheckDetails", ex.Message); }
            return isValid;
        }

        private void SaveVancoDraft(IVanco vanco, VancoDraft vanco_draft, IVancoRef vancoRef, string referenceCode, string memo)
        {
            logger.Info("Begin SaveVancoDraft");

            //logger.Debug("calling VancoDraftSave..");

            //VancoDraftSaveResult result = App.Credability.VancoDraftSave(vanco, vanco_draft, vancoRef);

            var cardData = new CardDetail()
                {
                AccountNumber = vanco_draft.DcAcctNum,
                ExpirationMonth = vanco_draft.CardExpMon,
                ExpirationYear = (vanco_draft.CardExpYear < 2000) ? 2000 + vanco_draft.CardExpYear : vanco_draft.CardExpYear,
                FirstName = vanco_draft.DcFNameOnCard,
                MiddleName = vanco_draft.DcMNameOnCard,
                LastName = vanco_draft.DcLNameOnCard,
                Address1 = vanco_draft.DcBillAddr1,
                Address2 = vanco_draft.DcBillAddr2,
                City = vanco_draft.DcBillCity,
                State = vanco_draft.DcBillState,
                ZipCode = vanco_draft.DcBillZip
            };

            string internetID = null;

            if (SessionState.AccountId.HasValue)
                    {
                var account = App.Identity.AccountGetById(SessionState.AccountId.Value);

                if (account != null && account.InternetId.HasValue)
                {
                    internetID = account.InternetId.ToString();
                }
                    }

            if(!string.IsNullOrEmpty(internetID))
            {
                // Unique Reference to be used with FirstData
                referenceCode = string.Format("{0}-{1}", referenceCode, internetID);
                }

            var result = App.EPay.AddTransaction(cardData, Convert.ToDecimal(vanco_draft.CharAmount), referenceCode, memo);

            if (result.StatusCode == MMI.Payment.Status.Code.SUCCESS)
                {
                var response = (SuccessResult)result;
                vancoRef.VanTransRef = response.ReferenceCode;
                vancoRef.VanPayMethRef = response.TransactionID;
                vancoRef.Status = "Payed";
                vancoRef.TransDTS = DateTime.Now;
                }
            else
            {
                var response = (FailError)result;
                vancoRef.LastErrCode = (int)response.StatusCode;
                vancoRef.ErrorOn = response.Message;
            }

            //if (result.IsSuccessful == true)
            //{
            //    dvErrorMessage.InnerHtml = "";
            //}
            //else
            //{
            //    logger.Debug("VancoDraftSave result unsuccessful");
            //    String szErrorMsg = String.Empty;

            //    if (result.Messages != null && result.Messages.Length > 0)
            //    {
            //        int iMsgCount = result.Messages.Count();

            //        if (iMsgCount > 0)
            //            szErrorMsg = result.Messages[0];

            //        if (!szErrorMsg.Contains("434|") && !szErrorMsg.Contains("Duplicate Transaction"))
            //        {
            //            dvErrorMessage.InnerHtml = "";
            //            dvErrorMessage.Visible = true;
            //            dvErrorMessage.InnerHtml += "<ul><li>" + szErrorMsg + "</li></ul>";

            //            logger.ErrorFormat("SaveVancoDraft Error (not 434|Duplicate Transaction): {0}", dvErrorMessage.InnerHtml);
            //        }

            //        logger.ErrorFormat("SaveVancoDraft Error: {0}", szErrorMsg);
            //    }
            //    else
            //    {
            //        logger.Debug("VancoDraftSave result - Messages null");
            //    }
            //}

            logger.Info("End SaveVancoDraft");
        }

        private VancoDraft GetVancoDraft(double charAmount, int clientNumber)
        {
            VancoDraft vanco_draft = new VancoDraft
            {
                ClientNumber = clientNumber,
                DcFNameOnCard = txtdcfnameoncard.Text,
                DcMNameOnCard = txtdcmnameoncard.Text,
                DcLNameOnCard = txtdclnameoncard.Text,
                DcBillAddr1 = txtdcbilladdr1.Text.Trim(),
                DcBillAddr2 = txtdcbilladdr2.Text.Trim(),
                DcBillCity = txtdcbillcity.Text,
                DcBillState = ddlState.SelectedValue,
                DcBillZip = txtdcbillzip.Text,
                DcAcctNum = txtdcacctnum.Text,
                CardExpMon = Convert.ToInt32(ddlCardExMnD.SelectedValue),
                CardExpYear = Convert.ToInt32(ddlCardExYrD.SelectedValue),
                CharAmount = charAmount
            };
            return vanco_draft;
        }
    }
}