﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Cccs.Credability.Website.Controls.BKCounselingControls
{
    public partial class BreadCrumControl : System.Web.UI.UserControl
    {
        public enum Tab
        {
            TellUs ,
            Budget ,
            Debt ,
            Summary ,
            Profile
        };

        protected void Page_Load(object sender , EventArgs e)
        {
            if (!IsPostBack)
            {
                lblBchProgressTracker.Width = System.Web.UI.WebControls.Unit.Percentage(App.PercentComplete * 0.97);

                // Enable based on Percent complete
                //
                int percent_complete = App.PercentComplete;

                if (App.WebsiteCode == Cccs.Identity.Website.BKC)
                {
                    if (percent_complete >= Cccs.Credability.Website.BKCounseling.PercentComplete.USER_PROFILE)
                    {
                        aProfile.HRef = AppRelativePathReplace("ProvideYourPersonalInformation.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.BKCounseling.PercentComplete.DESCRIBE_YOUR_SITUATION)
                    {
                        aTellUs.HRef = AppRelativePathReplace("DescribingYourSituation.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.BKCounseling.PercentComplete.USER_DEBT_LISTING)
                    {
                        aDebt.HRef = AppRelativePathReplace("ListingYourDebts.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.BKCounseling.PercentComplete.USER_INCOME_DOCUMENTATION)
                    {
                        aBudget.HRef = AppRelativePathReplace("UserIncomeDocumentation.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.BKCounseling.PercentComplete.ANALYZING_YOUR_FINANCIAL_SITUATION)
                    {
                        aSummary.HRef = AppRelativePathReplace("UserFinancialSituationAnalysis.aspx");
                    }
                }
            }
        }

        private string AppRelativePathReplace(string url)
        {
            return VirtualPathUtility.GetDirectory(Request.AppRelativeCurrentExecutionFilePath) + url;
        }

        public void ActiveTabSet(Tab tab)
        {
            HtmlAnchor href = FindControl(string.Format("a{0}" , tab)) as HtmlAnchor;

            if (href != null)
            {
                href.Attributes["class"] += "active";
                Visible = true;
            }
        }

    }
}