﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcHecmExample2 : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			}
			base.Page_Load(sender, e);
		}

		protected void click_btnHECMContinue(object sender, EventArgs e)
		{
			NavigateContinue();
		}
	}
}