﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcEligibility : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				btnReverseMortPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
				btnReverseMortContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnReverseMortSaveandContinueLater.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
			}
			base.Page_Load(sender, e);
		}

		protected void click_btnReverseMortgagePrevious(object sender, EventArgs e)
		{
			NavigatePrevious();

		}

		protected void btnReverseMortgageContinue(object sender, EventArgs e)
		{
			NavigateContinue();
		}

		protected void click_btnSaveandContinueLater(object sender, EventArgs e)
		{
			NavigateExit();
		}
	}
}