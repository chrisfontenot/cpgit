﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcCreditGrowth : CounselingUcBase
    {
        protected new void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnHECMPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
            }
			base.Page_Load(sender, e);
        }

        protected void click_btnHECMPrevious(object sender, EventArgs e)
        {
			NavigatePrevious();
        }
    }
}