﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAging.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcAging" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|Agencies")%></h1>
<div class="dvfinance">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|YSB")%>&nbsp;&nbsp;<a href="http://www.n4a.org/about-n4a/?fa=aaa-title-VI" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|wwwe")%></a></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|TA")%></p>
	<ul>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|EA")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|HC")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|HH")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|PD")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|MP")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|H")%>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|TAM")%>
		</li>
	</ul>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
</div>