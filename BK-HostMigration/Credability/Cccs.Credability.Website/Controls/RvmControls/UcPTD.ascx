﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcPTD.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcPTD" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|PTD|Property")%></h1>
<div class="dvfinance">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|PTD|SS")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|PTD|NAS")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|PTD|LDP")%></p>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
</div>