﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcRepaymentRules.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcRepaymentRules" %>

<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton 
            ID="btnRepaymentRulesP" 
            runat="server" 
            CssClass="previous" 
            ValidationGroup="userprofile" 
            OnClick="click_btnRepaymentRulesPrevious" 
            ToolTip="Return To Previous Page">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span>
		</asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton 
            ID="btnRepaymentRulesS" 
            runat="server" 
            ValidationGroup="userprofile" 
            OnClick="click_btnRepaymentRulesSaveContinue" 
            ToolTip="Save & Continue Later">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span>
		</asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton 
            ID="btnRepaymentRulesC" 
            runat="server" 
            ValidationGroup="userprofile" 
            OnClick="click_btnRepaymentRulesContinue" 
            ToolTip="Continue">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
		</asp:LinkButton>
	</div>
</div>