﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAlternitives.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcAlternitives" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|AAR")%></h1>
<div>
	<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|BY")%>
	<ul class="NormTxt">
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|SYH")%>
		</li>
		<li class="blacktext">
			<a href="DPL.aspx" class="NormLink">
				<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|DP")%></a>
			<ul class="NormTxt">
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|YLO")%>
				</li>
			</ul>
		</li>
	</ul>
	<ul>
		<li>
			<a href="PTD.aspx" class="NormLink">
				<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|PT")%></a>
			<ul class="NormTxt">
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|YLOS")%>
				</li>
			</ul>
		</li>
		<li class="blacktext">
			<a href="PublicBenefits.aspx" class="NormLink">
				<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|PB")%></a>
			<ul class="NormTxt">
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|SSA")%>
				</li>
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|MM")%>
				</li>
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|MPD")%>
				</li>
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|OH")%>
				</li>
			</ul>
		</li>
		<li class="blacktext">
			<a href="Aging.aspx" class="NormLink">
				<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|THA")%></a>
			<ul class="NormTxt">
				<li>
					<a href="http://www.eldercare.gov" class="NormLink">
						<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|WWW")%></a>
				</li>
			</ul>
		</li>
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|Alternitives|CM")%>
		</li>
	</ul>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortSaveandContinueLater" runat="server" ValidationGroup="userprofile" OnClick="click_btnSaveandContinueLater" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortContinue" runat="server" ValidationGroup="userprofile" OnClick="btnReverseMortgageContinue" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
