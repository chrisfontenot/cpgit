﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDPL.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcDPL" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|DPL|Deferred")%></h1>
<div class="dvfinance">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|Many")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|DPLs")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|EC")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|DCB")%></p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|TB")%></p>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
</div>