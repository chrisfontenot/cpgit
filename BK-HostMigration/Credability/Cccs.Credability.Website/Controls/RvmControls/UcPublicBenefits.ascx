﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcPublicBenefits.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcPublicBenefits" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|DPL|IOP")%></h1>
<div class="dvfinance">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|DPL|AR")%></p>
	<ul class="NormTxt">
		<li class="blacktext">
			<%= Cccs.Credability.Website.App.Translate("Credability|DPL|ABS")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|DPL|SSI")%>
			<ol>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|DPL|IG")%>:<a href="http://www.ssa.gov" target="_blank" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|DPL|www")%></a>
				</li>
				<li class="blacktext">
					<%= Cccs.Credability.Website.App.Translate("Credability|DPL|IAB")%>:<a href="http://www.ssa.gov" target="_blank" class="NormLink"><%= Cccs.Credability.Website.App.Translate("Credability|DPL|wwwg")%></a>
				</li>
			</ol>
		</li>
	</ul>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
</div>