﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcUserSituationDescription.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcUserSituationDescription" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
	function CheckForNull(theField)
	{
		var Val = theField.value;
		if (Val == "")
		{
			theField.value = "0";
		}
	}
</script>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|DYSI")%></h1>
<p class="ReqTxt">
	<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|Pleaseindicate")%></p>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|EPSIU")%>
</p>
<div id="dvError" runat="server" class="DvErrorSummary" visible="false">
</div>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
<div class="dvform2col">
	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|PTCOMRTCARM")%>
				</label>
				<span>
					<asp:DropDownList ID="ddlTheEvent" runat="server">
					</asp:DropDownList>
				</span>
				<span class="requiredField">&nbsp;*</span>
				<asp:RequiredFieldValidator class="validationMessage" ID="ddlTheEventRequired" runat="server" InitialValue="--Select--" ControlToValidate="ddlTheEvent" Text="!" Display="Dynamic"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|DTCTLYTCRM")%></label>
				<asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Width="230px" Rows="4" MaxLength="200"></asp:TextBox><span>&nbsp;*</span>
				<asp:RequiredFieldValidator class="validationMessage" ID="txtCommentRequired" runat="server" EnableClientScript="true" ControlToValidate="txtComment" Text="!" Display="Dynamic"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">
				<label>
				</label>
			</div>
		</div>
	</div>
	<div class="colformrht">
		<div class="dvform">
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|HYMAMPITPSM")%></label>
				<table>
					<tr>
						<td>
							<asp:RadioButtonList ID="rbtLateDebt" CellSpacing="0" RepeatLayout="Table" CellPadding="10" CssClass="Dvradiolist" runat="server" RepeatDirection="Horizontal" ToolTip='Have you missed any mortgage payments in the past six months? '>
								<asp:ListItem Value="0">&nbsp;Yes</asp:ListItem>
								<asp:ListItem Value="1">&nbsp;No</asp:ListItem>
							</asp:RadioButtonList>
							<asp:RequiredFieldValidator class="validationMessage" ID="rfvRentmort" runat="server" CssClass="error" ControlToValidate="rbtLateDebt" ErrorMessage="Field Required." Text="!"></asp:RequiredFieldValidator>
						</td>
						<td>
							<span class="requiredField">&nbsp;*</span>
						</td>
					</tr>
				</table>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|IYHMMHYM")%></label>
				<asp:TextBox ID="txtRentLate" runat="server" Width="20" MaxLength="2" OnBlur="CheckForNull(this)"></asp:TextBox><%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|NOM")%>
				<asp:RangeValidator class="validationMessage" ID="rfvRentLate" runat="server" MaximumValue="100" MinimumValue="0" Text="!" Type="Integer" ErrorMessage="Please enter a valid data" ControlToValidate="txtRentLate"></asp:RangeValidator>
			</div>
			<div class="dvrow">
				<label>
					<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituation|NOPLIH")%></label>
				<asp:TextBox ID="txtHouseholdSize" runat="server" MaxLength="3"></asp:TextBox>
				<asp:RequiredFieldValidator class="validationMessage" ID="txtHouseholdSizeRequired" runat="server" ControlToValidate="txtHouseholdSize" InitialValue="0" Text="!" EnableClientScript="true"></asp:RequiredFieldValidator>
				<asp:CompareValidator class="validationMessage" ControlToValidate="txtHouseholdSize" Operator="DataTypeCheck" Type="Integer" ErrorMessage="Please enter a valid data" Text="!" runat="server" ID="CompareValidator1"></asp:CompareValidator>
			</div>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<div class="DvErrorSummary" id="dvBottomServerSideErrorMessage" runat="server" visible="false">
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" CssClass="previous" CausesValidation="false" OnClick="btnPrevious_Click" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>