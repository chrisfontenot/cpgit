﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcHecmExample1.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcHecmExample1" %>
<%= Cccs.Credability.Website.App.Translate("Credability|Example1|Example1")%>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" ValidationGroup="userprofile" OnClick="click_btnHECMContinue" ToolTip="Continue" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>