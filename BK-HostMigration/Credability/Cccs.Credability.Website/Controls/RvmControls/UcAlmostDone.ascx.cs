﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcAlmostDone : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			SubmitToQueue(true);
			NavigatePage(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
		}
	}
}