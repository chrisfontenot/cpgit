﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcEligibility.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcEligibility" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|RM")%>&nbsp;&nbsp;</h1>
<div>
	<p class="col_title">
		<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|ER")%></p>
	<ul>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|YHMB")%><br />
			<a href="#" onclick="JavaScript: FlipFlop('HomesQualify'); return false;" class="NormLink">
				<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|CHTF")%></a>
		</li>
	</ul>
	<div id="HomesQualify" style="display: none; background: #c2c2c2; padding: 0 10px; margin: 10px 0">
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|YHM")%></p>
	</div>
	<ul>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|ACOM")%></li></ul>
	<ul>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|EligibilityRequirement|YHMW")%></li></ul>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnReverseMortgagePrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortSaveandContinueLater" runat="server" ValidationGroup="userprofile" OnClick="click_btnSaveandContinueLater" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortContinue" runat="server" ValidationGroup="userprofile" OnClick="btnReverseMortgageContinue" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
<script type="text/javascript">
	function FlipFlop(Flop)
	{
		var Flip = document.getElementById(Flop).style.display
		if (Flip == 'none')
		{
			Flip = 'block';
		}
		else
		{
			Flip = 'none';
		}
		document.getElementById(Flop).style.display = Flip;
	}
</script>
