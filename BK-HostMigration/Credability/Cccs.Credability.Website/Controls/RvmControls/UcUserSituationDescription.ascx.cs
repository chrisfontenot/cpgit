﻿using System;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcUserSituationDescription : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			InitializeTranslation();

			if(!IsPostBack)
			{
				UserSituationDescription userSituationDescriptionResult = App.Credability.UserSituationDescriptionGet(SessionState.ClientNumber.Value);

				txtComment.Text = userSituationDescriptionResult.ContactComments;
				txtRentLate.Text = userSituationDescriptionResult.MosDelinq.ToString();
				if(!userSituationDescriptionResult.ContactReason.IsNullOrWhiteSpace())
				{
					ddlTheEvent.SelectedValue = userSituationDescriptionResult.ContactReason.Trim();
					ddlTheEvent.SelectedItem.Selected = true;
				}
				if(userSituationDescriptionResult.MortCurrent == 0)
				{
					rbtLateDebt.SelectedValue = "0";
				}
				else
				{
					rbtLateDebt.SelectedValue = "1";
				}
				if(userSituationDescriptionResult.SizeOfHouseHold > 0)
				{
					txtHouseholdSize.Text = userSituationDescriptionResult.SizeOfHouseHold.ToString();
				}
				else
				{
					txtHouseholdSize.Text = "1";
				}
			}
			base.Page_Load(sender, e);
		}

		private void InitializeTranslation()
		{
			btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");

			ddlTheEvent.Items.Add(new ListItem(App.Translate("Credability|PickList|Select")));
			CommonFunction.AddItems(ddlTheEvent.Items, "TheEvent", SessionState.LanguageCode);

			ddlTheEventRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSTE");
			txtCommentRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CIR");
			txtHouseholdSizeRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPLIHMIY");
			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		private bool UpdateAddUserSituation()
		{
			UserSituationDescription UserSituationDescription = new UserSituationDescription();
			UserSituationDescription.ClientNumber = SessionState.ClientNumber.Value;
			UserSituationDescription.ContactComments = txtComment.Text.Trim();
			UserSituationDescription.ContactReason = ddlTheEvent.SelectedValue.ToString();
			UserSituationDescription.HousingType = "1";
			UserSituationDescription.MosDelinq = txtRentLate.Text.ToInt(0);
			UserSituationDescription.MortCurrent = rbtLateDebt.SelectedValue.ToInt(0);
			UserSituationDescription.SizeOfHouseHold = txtHouseholdSize.Text.ToInt(0);
			UserSituationDescriptionResult UserSituationDescriptionResults = App.Credability.UserSituationDescriptionUpdate(UserSituationDescription);
			return UserSituationDescriptionResults.IsSuccessful;
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(ValidatePage())
			{
				UpdateAddUserSituation();
				NavigateContinue();
			}
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			if(ValidatePage())
			{
				UpdateAddUserSituation();
				NavigateExit();
			}
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected bool ValidatePage()
		{
			bool IsValid = true;
			int RentLate = txtRentLate.Text.ToInt(0);
			if(txtHouseholdSize.Text.ToInt(0) < 1)
			{
				IsValid = false;
				CommonFunction.ShowErrorMessageAtPageTop(dvError, App.Translate("Credability|WebSite|BCHErrorNOPLIHHMIY"), true);
			}
			if(rbtLateDebt.SelectedValue == "1" && RentLate > 0)
			{
				IsValid = false;
				CommonFunction.ShowErrorMessageAtPageTop(dvError, App.Translate("Credability|WebSite|BCHErrorBehindMort"), true);
			}
			if(rbtLateDebt.SelectedValue == "0" && RentLate == 0)
			{
				IsValid = false;
				CommonFunction.ShowErrorMessageAtPageTop(dvError, App.Translate("Credability|WebSite|BCHErrorMortMissPay"), true);
			}
			return IsValid;
		}
	}
}