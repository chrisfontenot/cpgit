﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAlmostDone.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcAlmostDone" %>
<script language="JavaScript1.2">
function validateForm(form)
{
	return true;
}
function DoMulti()
{
	window.open('https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToTalk&site=29009926&SESSIONVAR!skill=<%=Cccs.Credability.Website.App.SkillCode %>&SESSIONVAR!IN%20Number=SessionState.ClientNumber.Value&SESSIONVAR!AuthenticationCode1=11111&SESSIONVAR!AuthenticationCode2=111111&SESSIONVAR!AuthenticationCode3=11111&imageUrl=https://onlinecounsel.cccsatl.org/images/chat&referrer='+escape(document.location),'call29009926','width=472,height=320,resizable=yes');
    return false;
}
</script>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|YAAD")%></h1>
<div class="dvfinance">
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|TFTP")%>
	</p>
	<p>
		<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|OYHS")%></p>
	<ul>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|YIRM")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|ASOY")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|YRM")%>
		</li>
		<li>
			<%= Cccs.Credability.Website.App.Translate("Credability|AlmostDone|YCR")%>
		</li>
	</ul>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnBookScheduleWithCounseller" runat="server" OnClientClick="DoMulti();" OnClick="btnContinue_Click" ToolTip="Schedule an Appointment with Your Counselor"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|ScheduleAppointment")%></span></asp:LinkButton>
	</div>
</div>