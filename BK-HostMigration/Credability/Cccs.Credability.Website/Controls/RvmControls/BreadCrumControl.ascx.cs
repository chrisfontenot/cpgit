﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;
using Cccs.Credability.Website.Rvm;

namespace Cccs.Credability.Website.Controls.RvmControls
{
	public partial class BreadCrumControl : System.Web.UI.UserControl
	{
		public enum Tab
		{
			AboutUs,
			TellUs,
			Budget,
			Debt,
			Summary,
			Profile
		};

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				lblProgressTracker.Width = System.Web.UI.WebControls.Unit.Percentage(App.PercentComplete * 0.97);

				// Enable based on Percent complete
				//
				int percent_complete = App.PercentComplete;

				if (percent_complete >= PercentComplete.USER_PROFILE)
				{
					aProfile.HRef = AppRelativePathReplace("UserProfile.aspx");
				}

				if (percent_complete >= PercentComplete.ELIGIBILITY)
				{
					aAboutUs.HRef = AppRelativePathReplace("Eligibility.aspx");
				}

				if (percent_complete >= PercentComplete.DESCRIBE_YOUR_SITUATION)
				{
					aTellUs.HRef = AppRelativePathReplace("DescribeYourSituation.aspx");
				}

				if (percent_complete >= PercentComplete.USER_INCOME_DOCUMENTATION)
				{
					aBudget.HRef = AppRelativePathReplace("UserIncomeDocumentation.aspx");
				}

				if (percent_complete >= PercentComplete.USER_DEBT_LISTING)
				{
					aDebt.HRef = AppRelativePathReplace("ListingYourDebts.aspx");
				}

				if (percent_complete >= PercentComplete.USER_FINANCIAL_SITUATION_ANALYSIS)
				{
					aSummary.HRef = AppRelativePathReplace("UserFinancialSituationAnalysis.aspx");
				}

			}
		}

		private string AppRelativePathReplace(string url)
		{
			return VirtualPathUtility.GetDirectory(Request.AppRelativeCurrentExecutionFilePath) + url;
		}

		public void ActiveTabSet(Tab tab)
		{
			HtmlAnchor href = FindControl(string.Format("a{0}", tab)) as HtmlAnchor;

			if (href != null)
			{
				href.Attributes["class"] += "active";
				Visible = true;
			}
		}
	}
}