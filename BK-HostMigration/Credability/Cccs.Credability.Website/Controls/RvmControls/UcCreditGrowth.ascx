﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcCreditGrowth.ascx.cs" Inherits="Cccs.Credability.Website.Controls.UcCreditGrowth" %>
<%= Cccs.Credability.Website.App.Translate("Credability|CreditlineGrowth|Growth")%>
<div class="ButtonParaGraph">
	<br />
	<div class="dvbtncontainer">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnHECMPrevious" runat="server" CssClass="previous" ValidationGroup="userprofile" OnClick="click_btnHECMPrevious" ToolTip="Return To Previous Page"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
		</div>
	</div>
</div>