﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls
{
	public partial class UcRepaymentRules : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                btnRepaymentRulesP.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnRepaymentRulesS.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
                btnRepaymentRulesC.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            }
			base.Page_Load(sender, e);
		}

		protected void click_btnRepaymentRulesPrevious(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void click_btnRepaymentRulesContinue(object sender, EventArgs e)
		{
			NavigateContinue();
		}

		protected void click_btnRepaymentRulesSaveContinue(object sender, EventArgs e)
		{
			NavigateExit();
		}
	}
}