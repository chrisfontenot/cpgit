﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumControl.ascx.cs" Inherits="Cccs.Credability.Website.Controls.RecControls.BreadCrumControl" %>
<p>&nbsp</p>
<div class="dvtab-ctrl">
	<ul>
		<li><a ID="aSummary" runat="server" class="last"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Summary")%></span></a></li>
		<li><a ID="aBudget" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Budget")%></span></a></li>
		<li><a ID="aDebt" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|Debt")%></span></a></li>
		<li><a ID="aTellUs" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|TellUs")%></span></a></li>
		<li><a ID="aProfile" runat="server" class="first"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMBreadcrumButton|UserProfile")%></span></a></li>
	</ul>
	<div class="clear"></div>
	<div class="dvsiteTracker" style="width:414px">
		<asp:Label ID="lblBchProgressTracker" runat="server" Width="100%">&nbsp;</asp:Label>
	</div> 
</div>
<div class="clear"></div>