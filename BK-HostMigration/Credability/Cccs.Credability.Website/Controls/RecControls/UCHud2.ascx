﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCHud2.ascx.cs" Inherits="Cccs.Credability.Website.Controls.RecControls.UCHud2" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|BCH|COI")%></h1>
<br />
<%= Cccs.Credability.Website.App.Translate("Credability|BCH|IMSS")%>
<br />
<br />
<%= Cccs.Credability.Website.App.Translate("Credability|BCH|LAN")%>
<br />
<br />
<%= Cccs.Credability.Website.App.Translate("Credability|BCH|BIA")%>
<br />
<br />
</p>
<ol>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|R")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|GTB")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|RPL")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|MYR")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|Forbearance")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|TLA")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|Modification")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|TLAT")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|Refinance")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|TOA")%>
    </li>
</ol>
<%= Cccs.Credability.Website.App.Translate("Credability|BCH|IIB")%>
<ol>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|SS")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|SYH")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|DIL")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|GTH")%>
    </li>
    <li><strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|Bankruptcyt")%></strong>
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|ELO")%>
    </li>
</ol>
<strong>
    <%= Cccs.Credability.Website.App.Translate("Credability|BCH|OR")%></strong>
<%= Cccs.Credability.Website.App.Translate("Credability|BCH|HASL")%><br />
<blockquote>
    <a href="<%= Cccs.Credability.Website.App.Translate("Credability|BCH|WWWH")%>" target="_blank" class="NormLink">
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|WWWH")%></a><br />
    <a href="<%= Cccs.Credability.Website.App.Translate("Credability|BCH|WWWHI")%>" target="_blank"
        class="NormLink">
        <%= Cccs.Credability.Website.App.Translate("Credability|BCH|WWWHI")%></a><!--<%= Cccs.Credability.Website.App.Translate("Credability|BCH|WWWC")%>-->
</blockquote>
<br />
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" />
<div class="dvform2col">
<Uc:AuthCode ID="UcAuthCode" runat="server" ValidationGroupSet=""></Uc:AuthCode>
</div>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReturnToPreviousPage" runat="server" CssClass="previous" OnClick="click_btnReturnToPreviousPage"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveExit" runat="server" OnClick="click_btnSaveExit"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="click_btnContinue" ><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
