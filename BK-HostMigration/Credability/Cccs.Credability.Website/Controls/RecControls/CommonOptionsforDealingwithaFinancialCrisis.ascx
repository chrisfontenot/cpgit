﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionsforDealingwithaFinancialCrisis.ascx.cs" Inherits="Cccs.Credability.Website.Controls.RecControls.CommonOptionsforDealingwithaFinancialCrisis" %>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|BCH|IADD")%></p>
<p>
	<ul>
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|HI")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|IYH")%>
		</li>
		<br />
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|UYA")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|UTE")%>
		</li>
		<br />
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|EIAD")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|CCCSO")%>
			<br />
			<br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|AFE")%><br />
		</li>
		<br />
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|NDS")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|CMBW")%>
		</li>
		<br />
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|FFB")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|WSU")%>
		</li>
		<br />
		<li>
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|BCH|WOT")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|BCH|TSOF")%>
			<br />
			<br />
			<ol>
				<li>
					<b>
						<%= Cccs.Credability.Website.App.Translate("Credability|BCH|PLA")%></b><br />
					<%= Cccs.Credability.Website.App.Translate("Credability|BCH|FATW")%>
				</li>
				<br />
				<li>
					<b>
						<%= Cccs.Credability.Website.App.Translate("Credability|BCH|PCO")%></b><br />
					<%= Cccs.Credability.Website.App.Translate("Credability|BCH|ATP")%>
				</li>
			</ol>
		</li>
	</ul>
</p>
<div class="DvErrorSummary" id="dvErrorMessage" runat="server" visible="false">
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReturnToPreviousPage" runat="server" CssClass="previous" OnClick="click_btnReturnToPreviousPage"><span>Return To Previous Page</span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="click_btnSaveExit"><span>Save & Exit</span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="click_btnContinue"><span>Continue</span></asp:LinkButton>
	</div>
</div>
