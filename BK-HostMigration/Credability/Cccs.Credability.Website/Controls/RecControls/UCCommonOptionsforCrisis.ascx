﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCommonOptionsforCrisis.ascx.cs" Inherits="Cccs.Credability.Website.Controls.RecControls.UCCommonOptionsforCrisis" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<%--<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisBCH|CO")%></h1>--%>
<p>
    Thank you for your service. You will be contacted by a counselor within the next 48 hours.
</p>
<p>
    Service members, those in the Reserves and National Guard and military veterans can also work with a CredAbility financial advisor to build long-term financial security. As part of a new program funded with a grant from JPMorgan Chase and Co., you can sign up to meet periodically with a financial advisor over a six-month time frame. The grant covers the cost of this program, so there is no cost to you.
</p>
<p>
    In addition to helping you build a budget and learn more about money management, the advisor can help you develop a plan to address other financial concerns and help build long-term financial security. To schedule a free, confidential session with a CredAbility financial advisor, call CredAbility toll-free at 888-808-7285.
</p>
<%--	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisBCH|IATP")%></p>
<div>
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|ULCont")%>
</div>
<p>--%>
	<%-- <%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|NYCW")%>
	link
	<%= Cccs.Credability.Website.CommonFunction.CreateChatLink(Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|link")) %>--%>

<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000"  />
<%--<Uc:AuthCode ID="UcAuthCode" runat="server" ValidationGroupSet=""></Uc:AuthCode>--%>
<p>
	&nbsp;</p>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" OnClick="click_btnReverseMortgagePrevious"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortSaveandContinueLater" runat="server" OnClick="click_btnSaveandContinueLater"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortContinue" runat="server" OnClick="btnReverseMortgageContinue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
