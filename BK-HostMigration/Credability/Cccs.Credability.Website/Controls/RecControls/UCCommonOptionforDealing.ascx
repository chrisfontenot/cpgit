﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCommonOptionforDealing.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.RecControls.UCCommonOptionforDealing" %>
<asp:ValidationSummary ID="DesValidationSummary" ValidationGroup="user" CssClass="DvErrorSummary"
    HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList"
    EnableClientScript="true" runat="server" ForeColor="#A50000" />
<div class="DvErrorSummary" id="dvErrorMessage" runat="server" visible="false">
</div>
<script type="text/javascript">
    function ReSetPhone(theField) {
        var Val = stripCharsNotInBag(theField.value, "1234567890");
        if (Val == "") {
            theField.value = ""
        }
        else {
            theField.value = reformatUSPhone(Val);
        }
    }
</script>
<%--<script type="text/javascript">
    function FlipFlopBox1() {
        var AmPm = document.getElementById("AmPm");
        var ContTime = document.getElementById("ContactDay");
        var DmpDate = document.getElementById("DmpDate");
        var ContactMe = document.getElementById("ContactMe");
        if (document.getElementById("DmpFlagYes").checked) {
            DmpDate.disabled = false;
            DmpDate.style.backgroundColor = "#FFFFFF";
            ContactMe.disabled = false;
            Contact();
        }
        else {
            ContactMe.disabled = true;
            DmpDate.disabled = true;
            DmpDate.style.backgroundColor = "#CCCCCC";
            var Phone = document.getElementById("ContactPhone");
            var Email = document.getElementById("ContactEmail");
            ContTime.disabled = true;
            AmPm.disabled = true;
            document.getElementById("ContactMethoidPhone").disabled = true;
            document.getElementById("ContactMethoidEmail").disabled = true;
            Email.disabled = true;
            Phone.disabled = true;
            Phone.style.backgroundColor = "#CCCCCC";
            Email.style.backgroundColor = "#CCCCCC";
            ContTime.style.backgroundColor = "#CCCCCC";
            AmPm.style.backgroundColor = "#CCCCCC";
        }
    }
</script>--%>
<h1>
    <%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionforDealingBCH|COFD")%></h1>
<p>
    <%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionforDealingBCH|TrCont")%></p>
<p>
    <asp:Image ID="Image1" ImageUrl="../../Images/Graph.gif" runat="server" />
</p>
<p>
    &nbsp;</p>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" OnClick="click_btnReverseMortgagePrevious"
            CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span>
        </asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSendtoCounselorforFinalReview" runat="server" OnClick="click_btnDone"
            ValidationGroup="user"><span><%= Cccs.Credability.Website.App.Translate("SENDCOUNSELORFOR")%></span></asp:LinkButton>
    </div>
</div>
