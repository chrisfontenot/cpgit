﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.RecControls
{
	public partial class CommonOptionsforDealingwithaFinancialCrisis : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				SessionState.UserPageArrivalTime = DateTime.Now;
			}
		}
		
		protected void click_btnReturnToPreviousPage(object sender, EventArgs e)
		{
			Response.Redirect("CommonOptionsforCrisis.aspx");
		}

		protected void click_btnContinue(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			//Response.Redirect("CommonOptionforDealing.aspx");
            //changed 5.15.13
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
		}

		protected void click_btnSaveExit(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
	}
}