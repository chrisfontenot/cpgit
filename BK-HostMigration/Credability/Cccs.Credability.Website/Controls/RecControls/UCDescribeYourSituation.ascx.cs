﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.CcrcHpf;
using Cccs.Translation;
using System.Web.Services;

namespace Cccs.Credability.Website.Controls.RecControls
{
	public partial class UCDescribeYourSituation : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
			{
                InitializeControl();

            }
        }

        public void InitializeControl()
        {
				SessionState.UserPageArrivalTime = DateTime.Now;

            InitializeTranslation();
				ShowContactDetailPageData();
				ShowExtraHubDetailPageData();
				ShowExtraQuestDetailPageData();
				SetInterestRateText();
            TimelineEventsPanel.Visible = false;
            ClientGoalsPanel.Visible = false;
            SetHudOption();
          
            if (App.WebsiteCode == Cccs.Identity.Website.BKC)
					ShowHideBKCControls();
                else if (App.WebsiteCode == Cccs.Identity.Website.DMP)
                    ShowHideDMPControls();
                else
                {
                    dvAMM.Visible = false;
                    dvGLM.Visible = false;
                }

			}

        public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
        {
            Double result;
            return Double.TryParse(val, NumberStyle, System.Globalization.CultureInfo.CurrentCulture, out result);
		}

		public void SetDMPHeader()
		{
			headingAll.Visible = false;
			headingDMP.Visible = true;
            dvAMM.Visible = false;
            dvGLM.Visible = false;
		}

		protected void ShowHideBKCControls()
		{
			introdSpan.Visible = false;
			introdBKCSpan.Visible = true;
			primaryEventSpan.Visible = false;
			secondEventSpan.Visible = false;
			describeCircumSpan.Visible = false;
			PnlBch.Visible = false;
			bkcPanelWYAB.Visible = true;
			rblHousingSituation.SelectedValue = "B";
			PnlHouseOnly.Visible = false;
			pnlHudOption_1.Visible = false;
			pnlHudOption_2.Visible = false;
			areYouCurOnPaySpan.Visible = false;
			noofPeopleSpan.Visible = false;
            dvAMM.Visible = true;
            dvGLM.Visible = true;
            UcAuthCode.Visible = false;
			SetHousingSituation();
		}

		protected void ShowHideDMPControls()
        {
         
			introdSpan.Visible = false;
			introdBKCSpan.Visible = false;
			introDMPSpan.Visible = true;
            UcAuthCode.Visible = false;
            dvFelony.Visible = false;
            dvMortgageOrHamp.Visible = false;
            dvBusinessOrPersonalMortgage.Visible = false;

            SetHousingSituation();
		}

        private void SetHudOption()
        {
            switch (App.ClientType)
            {
                case Constants.ClientType.HUD:
                    pnlHudOption_1.Visible = false;
                    pnlHudOption_2.Visible = false;
                    rblHousingSituation.SelectedValue = "B";
                    PnlHouseOnly.Visible = true;
                    PnlBch.Visible = false;
                    TimelineEventsPanel.Visible = true;
                    ClientGoalsPanel.Visible = true;
                    break;

                default: // BCH
                    pnlHudOption_1.Visible = true;
                    pnlHudOption_2.Visible = true;
                    PnlHouseOnly.Visible = false;
                    PnlBch.Visible = true;
                    UcAuthCode.Visible = false;
                    break;
            }
            SetHousingSituation();
        }

        public bool IsSubmitButtonVisible
        {
            get
            {
                return btnSubmit.Visible;
            }
            set
            {
                btnSubmit.Visible = value;
            }
        }

        private void SetHousingSituation()
        {
            if (rblHousingSituation.SelectedValue == "B" && !HideHousingPanel)
            {
                dvBuying.Visible = true;


                dvHouseHoldRentSize.Visible = false;
            }
            else
            {
                dvBuying.Visible = false;

                dvHouseHoldRentSize.Visible = true;
              
            }
        }

   
     

        private void SetInterestRateText()
        {
            if (ddlIntType.SelectedValue == "A" || ddlIntType.SelectedValue == "O")
            {
                lblIntRate.Text = Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdCurInt");
                pnlArmInrest.Visible = true;
            }
            else
            {
                // TODO: add "Interest Rate" to translations
                lblIntRate.Text = Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdInt");
                pnlArmInrest.Visible = false;
            }
        }

        private void ResetHousingSituationValues()
        {
           if(!dvBuying.Visible)
			{
            
                //rblAffHomeMod.ClearSelection();
                ddlPriLoanServ.ClearSelection();
                txtPriLoanHold.Text = string.Empty;
                txtLoanNo.Text = string.Empty;
                ddlMortageType.ClearSelection();
                txtMortageTerm.Text = string.Empty;
                txtDateOfMortage.Text = string.Empty;
                rblGovNpPro.ClearSelection();
                ddlIntType.ClearSelection();
                txtCurIntRate.Text = string.Empty;
                txtOrgIntRate.Text = string.Empty;
                txtMaxIntRate.Text = string.Empty;
                txtStartBal.Text = string.Empty;
                txtCurrentBal.Text = string.Empty;
                txtHomeVal.Text = string.Empty;
                txtLoanPay.Text = string.Empty;
                rblIsTaxInc.ClearSelection();
                txtTaxPay.Text = string.Empty;
                rblIsInsIncYes.ClearSelection();
                txtInsPay.Text = string.Empty;
                txtHouseholdSize.Text = string.Empty;
                rblSellingProp.ClearSelection();
                rblGotNotice.ClearSelection();
                rblPreDmp.ClearSelection();
                rblMortCur.ClearSelection();
                rblLoanModification.ClearSelection();
                rblAssistanceModify.ClearSelection();
                txtMortLate.Text = string.Empty;
                txtMoneyAvail.Text = string.Empty;
                txtLoanDescDate.Text = string.Empty;
                txtLoanDesc.Text = string.Empty;
                ddlPropStat.ClearSelection();
                rblSecondLoan.ClearSelection();
                ddlSecLoanServ.ClearSelection();
                txtSecLoanHold.Text = string.Empty;
                txtLoanNo2.Text = string.Empty;
                txtMtg2Amt.Text = string.Empty;
                txtMtg2Stat.Text = string.Empty;
            }
        }

		private void InitializeTranslation()
		{
			btnReturnToPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			btnSaveAndExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");

			ValidationControlTranslation();
			rfvEvent.InitialValue = App.Translate("Credability|PickList|Select");
			ddlEvent.Items.Clear();
			ddlEvent.Items.Add(new ListItem(App.Translate("Credability|PickList|Select")));
			CommonFunction.AddItems(ddlEvent.Items, "TheEventBch", SessionState.LanguageCode);

			//Secondary Event Pick List
			ddlSecEvent.Items.Clear();
			ddlSecEvent.Items.Add(new ListItem(App.Translate("Credability|PickList|Select")));
			rfvSecEvent.InitialValue = App.Translate("Credability|PickList|Select");
			CommonFunction.AddItems(ddlSecEvent.Items, "TheEventBch", SessionState.LanguageCode);

            RadioButtonTranslation();
			PickListtransaltion();
		}

		public void PickListtransaltion()
		{
            
			ddlPropStat.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
			CommonFunction.AddItems(ddlPropStat.Items, "PropertyStatusPickList", SessionState.LanguageCode);

			ddlIntType.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
			CommonFunction.AddItems(ddlIntType.Items, "InteresttypePickList", SessionState.LanguageCode);

			ddlMortageType.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
			CommonFunction.AddItems(ddlMortageType.Items, "MortgageTypePickList", SessionState.LanguageCode);

            // Harp & Hamp questions

            ddlHousing.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
            CommonFunction.AddItems(ddlHousing.Items, "HousingType", SessionState.LanguageCode);
            
            ddlMortgageInvestor.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), "0"));
            CommonFunction.AddItems(ddlMortgageInvestor.Items, "MortgageInvestorPickList", SessionState.LanguageCode);


		}

        private void RadioButtonTranslation()
		{
			CommonFunction.AddItems(rblLostSO.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblMedExp.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblHealthWage.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblUnemployed.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblTaxLien.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblMortRate.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            //CommonFunction.AddItems(rblAffHomeMod.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblGovNpPro.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblIsTaxInc.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblIsInsIncYes.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblSellingProp.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblGotNotice.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblPreDmp.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblMortCur.Items, "YesNoRadioButtonsValue1and2", SessionState.LanguageCode);
            CommonFunction.AddItems(rblAssistanceModify.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblLoanModification.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblSecondLoan.Items, "YesNoRadioButtons", SessionState.LanguageCode);
			CommonFunction.AddItems(rblHousingSituation.Items, "RentingBuyingOwnHomeRadioButtonList", SessionState.LanguageCode);

            // Harp, Hamp and Short Sale Questions
            CommonFunction.AddItems(rblFelony.Items, "YesNoRadioButtons", SessionState.LanguageCode);

            CommonFunction.AddItems(rblModifiedHamp.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblBusiness.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblSoldToFM.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblFirstLienFreddie.Items, "YesNoRadioButtons", SessionState.LanguageCode);

            CommonFunction.AddItems(rblEscrow.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblRefinance.Items, "YesNoRadioButtons", SessionState.LanguageCode);
            CommonFunction.AddItems(rblLiensOnProperty.Items, "YesNoRadioButtons", SessionState.LanguageCode);

			// Removing the "I own my home" Option from Housing Situation Radio Button List.
            if (App.WebsiteCode == Cccs.Identity.Website.DMP)
			{
				ListItem li = new ListItem();
				li.Text = "I own my home";
				li.Value = "O";
				rblHousingSituation.Items.Remove(li);
			}
		}

		private void ValidationControlTranslation()
		{
			rfvEvent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSTE");
			rfvSecEvent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SEIR");
			rfvComment.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CIR");
			rfvMedExp.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CC");
			rfvLostSO.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ID");
			rfvHealthWage.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YOF");
			rfvUnemployed.ErrorMessage = App.Translate("Credability|DescYourSituRVM|AEP");
			rfvTaxLien.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LP");
			rfvMortRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|HY");
            //rfvAffHomeMod.ErrorMessage = App.Translate("Credability|DescYourSituRVM|HA");
            rfvLoanNo.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LM");
			rfvPriLoanServ.ErrorMessage = App.Translate("Credability|DescYourSituRVM|HA");
			rfvddlMortageType.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MTI");
			rfvtxtMortageTerm.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MTIR");
			rvMortageTerm.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MTS");
			rvtxtDateOfMortage.ErrorMessage = App.Translate("Credability|DescYourSituRVM|DOM");
			revtxtDateOfMortage.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YDM");
			rvrblGovNpPro.ErrorMessage = App.Translate("Credability|DescYourSituRVM|POR");
			rfvIntType.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IT");
			rfvddlMortageType.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MTI");
			rfvCurIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IRR");
			rcCurIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IRS");
			//rfvtxtOrgIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OIR");
			rvtxtOrgIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OIRS");
			//rfvtxtMaxIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MA");
			rvtxtMaxIntRate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MAI");
			rfvStartBal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OL");
			rvtxtStartBal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OLB");
			rfvCurrentBal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CL");
			rvtxtCurrentBal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|CLB");
			rfvHomeVal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|HV");
			rvtxtHomeVal.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ES");
            rfvLoanPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MP");
			rvtxtLoanPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MPS");
			rfvIsTaxInc.ErrorMessage = App.Translate("Credability|DescYourSituRVM|TI");
            rfvTaxPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|AP");
			rvtxtTaxPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|APT");
            rfvIsInsInc.ErrorMessage = App.Translate("Credability|DescYourSituRVM|IP");
            rfvInsPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|AMO");
			rvtxtInsPay.ErrorMessage = App.Translate("Credability|DescYourSituRVM|AMOI");
			rftxtHouseholdSize.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NO");
			rvtxtHouseholdSize.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOP");
			rvfSellingProp.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SP");
			RequiredFieldValidator13.ErrorMessage = App.Translate("Credability|DescYourSituRVM|GN");
			RequiredFieldValidator14.ErrorMessage = App.Translate("Credability|DescYourSituRVM|RP");
			rfvrblMortCur.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MCI");
            rfvrblAssistanceModify.ErrorMessage = App.Translate("Credability|DescYourSituRVM|AMM");
            rfvrblLoanModification.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LMG");
			
            rfvtxtMortLate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|ML");
			cvtxtMortLate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PE");
			//	rfvMoneyAvail.ErrorMessage = App.Translate("Credability|DescYourSituRVM|MoneyA");
			cvtxtMoneyAvail.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PEA");
			rfvLoanDescDate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|WD");
			revtxtLoanDescDate.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YD");
			RequiredFieldValidator16.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PS");
			rfvSecLoanServ.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SL");
			RequiredFieldValidator2.ErrorMessage = App.Translate("Credability|DescYourSituRVM|OCM");
			RequiredFieldValidator3.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SLNR");
			RequiredFieldValidator4.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LBR");
			rvtxtMtg2Amt.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SLB");
			RequiredFieldValidator5.ErrorMessage = App.Translate("Credability|DescYourSituRVM|LS");
			rfvtxtHouseholdSizeRent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPI");
            rgvHousholdSizeRent.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPIH");
			//rvtxtAuthCodeInput.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YNTE");
			rfvSecondLoan.ErrorMessage = App.Translate("Credability|DescYourSituRVM|SLR");
			RequiredFieldValidatorrblHousingSituation.ErrorMessage = App.Translate("Credability|DescYourSituRVM|RentOwnBuy");
			rfvrblHousingSituation.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PIB");
			//RequiredFieldValidator1.ErrorMessage = App.Translate("Credability|DescYourSituRVM|NOPLIHMIY");

			//rfvRentLate1.ErrorMessage = App.Translate("Credability|Credability|DescYourSituRVM|IYHMMBYR");
            
            //HARp & HAMP questions

            rfvBusiness.ErrorMessage = App.Translate("HarpQuestionError2");
            rfvClientGoals.ErrorMessage = App.Translate("HarpQuestionError3");
            rfvEscrow.ErrorMessage = App.Translate("HarpQuestionError10");
            rfvTimeLineEvents.ErrorMessage = App.Translate("HarpQuestionError4");
            rfvFelony.ErrorMessage = App.Translate("HarpQuestionError1");
            rfvFirstLienFreddie.ErrorMessage = App.Translate("HarpQuestionError8");
            rfvModifiedHamp.ErrorMessage = App.Translate("HarpQuestionError11");
            rfvLiensOnProperty.ErrorMessage = App.Translate("HarpQuestionError12");
            rfvHousing.ErrorMessage = App.Translate("HarpQuestionError5");
            rfvRefinance.ErrorMessage = App.Translate("HarpQuestionError9");
            rfvMortgageInvestor.ErrorMessage = App.Translate("HarpQuestionError6");
            rfvSoldToFM.ErrorMessage = App.Translate("HarpQuestionError7");      

			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		private void ShowContactDetailPageData()
		{
			UserDecsribeYourSituationBCH userDecsribeYourSituationBCH = null;
			userDecsribeYourSituationBCH = App.Credability.UserDecsribeYourSituationBCHGet(SessionState.ClientNumber.Value);


            if (userDecsribeYourSituationBCH != null)
			{
				txtComment.Text = userDecsribeYourSituationBCH.ContactComments;
                if (userDecsribeYourSituationBCH.ContactReason != null)
				{
					ddlEvent.ClearSelection();
					ddlEvent.SelectedValue = userDecsribeYourSituationBCH.ContactReason;
				}

				txtCurIntRate.Text = (userDecsribeYourSituationBCH.MortRate / 100).ToString("P");
				txtDateOfMortage.Text = userDecsribeYourSituationBCH.MortDate.Clean();
				txtHomeVal.Text = userDecsribeYourSituationBCH.ValHome.ToString("C0");
				txtHouseholdSize.Text = userDecsribeYourSituationBCH.SizeOfHouseHold.ToString();
				txtHouseholdSizeRent.Text = userDecsribeYourSituationBCH.SizeOfHouseHold.ToString();


                if (!string.IsNullOrEmpty(userDecsribeYourSituationBCH.HousingType))
				{
                    if (userDecsribeYourSituationBCH.HousingType.ToString().Trim() == "B")
					{
                    
                        //HomeAffordable.Visible = true; // 06/07/2011 CREDA-1464 - Making Home Affordable for Housing Type "I Have a Mortgage".

						rblHousingSituation.SelectedValue = userDecsribeYourSituationBCH.HousingType.ToString().Trim();
					}
					else
					{
                        //HomeAffordable.Visible = false;  // 06/07/2011 CREDA-1464 - Making Home Affordable for Housing Type "I Have a Mortgage".
                   


						rblHousingSituation.SelectedValue = userDecsribeYourSituationBCH.HousingType.ToString().Trim();
					}
				}
                
                SetHousingSituation();
                 
				txtMortLate.Text = userDecsribeYourSituationBCH.MosDelinq.ToString();
				txtLoanDesc.Text = userDecsribeYourSituationBCH.LastContactDesc.Clean();
				txtLoanNo.Text = userDecsribeYourSituationBCH.LoanNumber.Clean();
				txtLoanNo2.Text = userDecsribeYourSituationBCH.LoanNumber2.Clean();
				ddlMortageType.ClearSelection();
				ddlMortageType.SelectedValue = userDecsribeYourSituationBCH.MortType.Clean();
				txtMortageTerm.Text = userDecsribeYourSituationBCH.MortYears.ToString();
				txtDateOfMortage.Text = userDecsribeYourSituationBCH.MortDate.Clean();
				ddlIntType.ClearSelection();
				ddlIntType.SelectedValue = userDecsribeYourSituationBCH.RateType.Clean();
				SetInterestRateText();

				txtStartBal.Text = userDecsribeYourSituationBCH.OrigBal.ToString("C0");
				txtCurrentBal.Text = userDecsribeYourSituationBCH.Owehome.ToString("C0");
				txtHomeVal.Text = userDecsribeYourSituationBCH.ValHome.ToString("C0");
				txtLoanPay.Text = userDecsribeYourSituationBCH.MoPmt.ToString("C0");
				txtMoneyAvail.Text = userDecsribeYourSituationBCH.AmtAvail.ToString("C0");
				txtLoanDescDate.Text = userDecsribeYourSituationBCH.LastContactDate.Clean();
				rblPreDmp.SelectedValue = userDecsribeYourSituationBCH.RepayPlan.Clean();
                if (userDecsribeYourSituationBCH.MortCurrent != null)
				{
					rblMortCur.SelectedValue = userDecsribeYourSituationBCH.MortCurrent.ToString();
				}
				txtSecLoanHold.Text = userDecsribeYourSituationBCH.SecondaryHolder.Clean();
				txtMtg2Amt.Text = userDecsribeYourSituationBCH.SecondaryAmt.ToString("C0");
				txtMtg2Stat.Text = userDecsribeYourSituationBCH.SecondaryStatus.Clean();
				rblSellingProp.SelectedValue = userDecsribeYourSituationBCH.PROP4Sale.Clean();
				rblGotNotice.SelectedValue = userDecsribeYourSituationBCH.NOTE4Close.Clean();
				ddlPropStat.ClearSelection();
				ddlPropStat.SelectedValue = userDecsribeYourSituationBCH.WHOInHouse.Clean();

				ddlPriLoanServ.ClearSelection();

				CompanyInfo[] companyInfo = App.Credability.CompanyListGet();
                if (companyInfo != null)
				{
					ListItem lt = new ListItem();
					lt.Value = "0";
					lt.Text = App.Translate("Credability|PickList|Select");
					ddlPriLoanServ.DataTextField = "CompanyName";
					ddlPriLoanServ.DataValueField = "ServID";

					ddlPriLoanServ.DataSource = companyInfo;
					ddlPriLoanServ.DataBind();
					ddlPriLoanServ.Items.Insert(0, lt);
					ddlPriLoanServ.ClearSelection();
                    if (userDecsribeYourSituationBCH.PRIServID > 0)
					{
						ddlPriLoanServ.SelectedValue = userDecsribeYourSituationBCH.PRIServID.ToString();
                        if (App.Credability.IsOtherServicer(userDecsribeYourSituationBCH.PRIServID))
						{
							PriLoan1.Visible = true;
							PriLoan2.Visible = true;
							txtPriLoanHold.Visible = true;
							txtPriLoanHold.Text = userDecsribeYourSituationBCH.MortHolder.Clean();
						}
						else
						{
							PriLoan1.Visible = false;
							PriLoan2.Visible = false;
							txtPriLoanHold.Visible = false;
							txtPriLoanHold.Text = string.Empty;
						}
					}

					ListItem lt1 = new ListItem();
					lt1.Value = "0";
					lt1.Text = App.Translate("Credability|PickList|Select");
					ddlSecLoanServ.DataTextField = "CompanyName";
					ddlSecLoanServ.DataValueField = "ServID";
					ddlSecLoanServ.DataSource = companyInfo;
					ddlSecLoanServ.DataBind();
					ddlSecLoanServ.Items.Insert(0, lt1);
					ddlSecLoanServ.ClearSelection();

                    if (userDecsribeYourSituationBCH.SECServID != 0)
					{
						rblSecondLoan.SelectedValue = "Yes";
						pnlSeconLoanInfo.Visible = true;
						ddlSecLoanServ.SelectedValue = userDecsribeYourSituationBCH.SECServID.ToString();
                        if (App.Credability.IsOtherServicer(userDecsribeYourSituationBCH.SECServID))
						{
							dvOtherCompanyName.Visible = true;
							txtSecLoanHold.Text = userDecsribeYourSituationBCH.SecondaryHolder;
						}
						else
						{
							dvOtherCompanyName.Visible = false;
							txtSecLoanHold.Text = string.Empty;
						}
					}
					else
					{
						rblSecondLoan.SelectedValue = "No";
						pnlSeconLoanInfo.Visible = false;
					}

				}
                //rblAffHomeMod.SelectedValue = (userDecsribeYourSituationBCH.MAKEHomeAff != null) ? userDecsribeYourSituationBCH.MAKEHomeAff.ToString() : string.Empty;
			}
		}

		private void ShowExtraQuestDetailPageData()
		{
			UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest = null;
			userSituationDescriptionExtraQuest = App.Credability.UserSituationDescriptionExtraQuestGet(SessionState.ClientNumber.Value);
			//userSituationDescriptionExtraQuest = App.Credability.UserSituationDescriptionExtraQuestGet(SessionState.ClientNumber.Value);
            if (userSituationDescriptionExtraQuest != null)
			{
                if (userSituationDescriptionExtraQuest.SECEvent != null)
				{
					ddlSecEvent.ClearSelection();
					ddlSecEvent.SelectedValue = userSituationDescriptionExtraQuest.SECEvent.ToString();
				}
                
				rblMedExp.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MEDExp)) ? userSituationDescriptionExtraQuest.MEDExp.ToString().Trim() : string.Empty;
				rblLostSO.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.LOSTSO)) ? userSituationDescriptionExtraQuest.LOSTSO.ToString().Trim() : string.Empty;
				rblHealthWage.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.HEALTHWage)) ? userSituationDescriptionExtraQuest.HEALTHWage.ToString().Trim() : string.Empty;
				rblUnemployed.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.UNEmployed)) ? userSituationDescriptionExtraQuest.UNEmployed.ToString().Trim() : string.Empty;
				rblTaxLien.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.TAXLien)) ? userSituationDescriptionExtraQuest.TAXLien.ToString().Trim() : string.Empty;
				rblMortRate.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MORTRate)) ? userSituationDescriptionExtraQuest.MORTRate.ToString().Trim() : string.Empty;
                
                rblAssistanceModify.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.ASSISTModify)) ? userSituationDescriptionExtraQuest.ASSISTModify.ToString().Trim() : string.Empty;
                rblLoanModification.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.LOANModify)) ? userSituationDescriptionExtraQuest.LOANModify.ToString().Trim() : string.Empty;
                
                // Hamp Questions
                rblModifiedHamp.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MortgageModifiedHamp)) ? userSituationDescriptionExtraQuest.MortgageModifiedHamp.ToString().Trim() : string.Empty;
                ddlMortgageInvestor.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.MortgageInvestor)) ? userSituationDescriptionExtraQuest.MortgageInvestor.ToString().Trim() : string.Empty;

                rblBusiness.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.IsBusinessOrPersonalMortgage)) ? userSituationDescriptionExtraQuest.IsBusinessOrPersonalMortgage.ToString().Trim() : string.Empty;
                ddlHousing.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.HousingType)) ? userSituationDescriptionExtraQuest.HousingType.ToString().Trim() : string.Empty;

                rblEscrow.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.AgreeEscrowAccount)) ? userSituationDescriptionExtraQuest.AgreeEscrowAccount.ToString().Trim() : string.Empty;
                rblFelony.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.ConvictedFelony)) ? userSituationDescriptionExtraQuest.ConvictedFelony.ToString().Trim() : string.Empty;


                rblFirstLienFreddie.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.IsLoanFirstLienFMorFMae)) ? userSituationDescriptionExtraQuest.IsLoanFirstLienFMorFMae.ToString().Trim() : string.Empty;
                rblSoldToFM.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.LoanSoldFMorFmae)) ? userSituationDescriptionExtraQuest.LoanSoldFMorFmae.ToString().Trim() : string.Empty;

                rblRefinance.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.LoanRefinanceProgram)) ? userSituationDescriptionExtraQuest.LoanRefinanceProgram.ToString().Trim() : string.Empty;
                rblLiensOnProperty.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraQuest.AnyLiens)) ? userSituationDescriptionExtraQuest.AnyLiens.ToString().Trim() : string.Empty;

                txtClientGoals.Text = userSituationDescriptionExtraQuest.ClientGoals ?? "";
                txtTimeLineEvents.Text = userSituationDescriptionExtraQuest.TimeLineEvents ?? "";    
              

			}
		}

		private void ShowExtraHubDetailPageData()
		{
			//For ExtraHub
			UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt = null;
			userSituationDescriptionExtraHudAbt = App.Credability.UserSituationDescriptionExtraHudAbtGet(SessionState.ClientNumber.Value);
			//userSituationDescriptionExtraHudAbt = App.Credability.UserSituationDescriptionExtraHudAbtGet(SessionState.ClientNumber.Value);
            if (userSituationDescriptionExtraHudAbt != null)
			{
				rblGovNpPro.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.GOVGrant)) ? userSituationDescriptionExtraHudAbt.GOVGrant.ToString().Trim() : string.Empty;
				txtOrgIntRate.Text = (userSituationDescriptionExtraHudAbt.STARTIntRate / 100).ToString("P");
				txtMaxIntRate.Text = (userSituationDescriptionExtraHudAbt.TOPIntRate / 100).ToString("P");
				txtTaxPay.Text = userSituationDescriptionExtraHudAbt.ANPropTax.ToString("C0");
				txtInsPay.Text = userSituationDescriptionExtraHudAbt.ANPropIns.ToString("C0");
				rblIsInsIncYes.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.INCludeIns)) ? userSituationDescriptionExtraHudAbt.INCludeIns.ToString().Trim() : string.Empty;
				rblIsTaxInc.SelectedValue = (!string.IsNullOrEmpty(userSituationDescriptionExtraHudAbt.INCludeTax)) ? userSituationDescriptionExtraHudAbt.INCludeTax.ToString().Trim() : string.Empty;
			}
		}

		private bool SaveExtraHudAbtData()
		{
			bool Result = false;
			UserSituationDescriptionExtraHudAbt userSituationDescriptionExtraHudAbt = new UserSituationDescriptionExtraHudAbt();
			userSituationDescriptionExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
			//userSituationDescriptionExtraHudAbt.ClientNumber = SessionState.ClientNumber.Value;
			userSituationDescriptionExtraHudAbt.GOVGrant = rblGovNpPro.SelectedValue;
			userSituationDescriptionExtraHudAbt.STARTIntRate = txtOrgIntRate.Text.ToFloat(0);
			userSituationDescriptionExtraHudAbt.TOPIntRate = txtMaxIntRate.Text.ToFloat(0);
			userSituationDescriptionExtraHudAbt.ANPropTax = txtTaxPay.Text.ToFloat(0);
			userSituationDescriptionExtraHudAbt.ANPropIns = txtInsPay.Text.ToFloat(0);

			userSituationDescriptionExtraHudAbt.INCludeIns = rblIsInsIncYes.SelectedValue;
			userSituationDescriptionExtraHudAbt.INCludeTax = rblIsTaxInc.SelectedValue;
			UserSituationDescriptionExtraHudAbtResult userSituationDescriptionExtraHudAbtResult = null;
			userSituationDescriptionExtraHudAbtResult = App.Credability.UserSituationDescriptionExtraHudAbtAddUpdate(userSituationDescriptionExtraHudAbt);
            if (userSituationDescriptionExtraHudAbtResult.IsSuccessful)
				Result = true;
			return Result;
		}

		private bool SaveExtraQuestData()
		{
			bool Result = false;
			UserSituationDescriptionExtraQuest userSituationDescriptionExtraQuest = new UserSituationDescriptionExtraQuest();
			userSituationDescriptionExtraQuest.ClientNumber = SessionState.ClientNumber.Value;
			//userSituationDescriptionExtraQuest.ClientNumber = SessionState.ClientNumber.Value;
			userSituationDescriptionExtraQuest.SECEvent = ddlSecEvent.SelectedValue.Trim();
			userSituationDescriptionExtraQuest.MEDExp = rblMedExp.SelectedValue;
			userSituationDescriptionExtraQuest.LOSTSO = rblLostSO.SelectedValue;
			userSituationDescriptionExtraQuest.HEALTHWage = rblHealthWage.SelectedValue;
			userSituationDescriptionExtraQuest.UNEmployed = rblUnemployed.SelectedValue;
			userSituationDescriptionExtraQuest.TAXLien = rblTaxLien.SelectedValue;
			userSituationDescriptionExtraQuest.MORTRate = rblMortRate.SelectedValue;


            if (rblLoanModification != null && rblLoanModification.SelectedItem != null && rblLoanModification.SelectedValue != "")
                userSituationDescriptionExtraQuest.LOANModify = rblLoanModification.SelectedValue;

            if (rblAssistanceModify != null && rblAssistanceModify.SelectedItem != null && rblAssistanceModify.SelectedValue != "")
                userSituationDescriptionExtraQuest.ASSISTModify = rblAssistanceModify.SelectedValue;

            // Hamp Questions

            userSituationDescriptionExtraQuest.MortgageModifiedHamp = (!string.IsNullOrEmpty(rblModifiedHamp.SelectedValue)) ? rblModifiedHamp.SelectedValue : "";

            userSituationDescriptionExtraQuest.MortgageInvestor = (!string.IsNullOrEmpty(ddlMortgageInvestor.SelectedValue)) ? ddlMortgageInvestor.SelectedValue : "";
            userSituationDescriptionExtraQuest.IsBusinessOrPersonalMortgage = (!string.IsNullOrEmpty(rblBusiness.SelectedValue)) ? rblBusiness.SelectedValue : "";
            userSituationDescriptionExtraQuest.HousingType = (!string.IsNullOrEmpty(ddlHousing.SelectedValue)) ? ddlHousing.SelectedValue : "";
            userSituationDescriptionExtraQuest.AgreeEscrowAccount = (!string.IsNullOrEmpty(rblEscrow.SelectedValue)) ? rblEscrow.SelectedValue : "";

            // Hamp 2 tier Questions
            userSituationDescriptionExtraQuest.ConvictedFelony = (!string.IsNullOrEmpty(rblFelony.SelectedValue)) ? rblFelony.SelectedValue : "";
            userSituationDescriptionExtraQuest.IsLoanFirstLienFMorFMae = (!string.IsNullOrEmpty(rblFirstLienFreddie.SelectedValue)) ? rblFirstLienFreddie.SelectedValue : "";

            // Harp Questions
            userSituationDescriptionExtraQuest.LoanSoldFMorFmae = (!string.IsNullOrEmpty(rblSoldToFM.SelectedValue)) ? rblSoldToFM.SelectedValue : "";
            userSituationDescriptionExtraQuest.LoanRefinanceProgram = (!string.IsNullOrEmpty(rblRefinance.SelectedValue)) ? rblRefinance.SelectedValue : "";

            // Short Sale/Deed in Lieu


            userSituationDescriptionExtraQuest.AnyLiens = (!string.IsNullOrEmpty(rblLiensOnProperty.SelectedValue)) ? rblLiensOnProperty.SelectedValue : "";

            // Fp - Housing only questions
            userSituationDescriptionExtraQuest.ClientGoals = txtClientGoals.Text;
            userSituationDescriptionExtraQuest.TimeLineEvents = txtTimeLineEvents.Text;



			UserSituationDescriptionExtraQuestResult userSituationDescriptionExtraQuestResult = null;
           
			userSituationDescriptionExtraQuestResult = App.Credability.UserSituationDescriptionExtraQuestAddUpdate(userSituationDescriptionExtraQuest);
            if (userSituationDescriptionExtraQuestResult.IsSuccessful)
				Result = true;
			return Result;
		}

		private bool SaveContactData(int? HpfId)
		{
			bool Result = false;
			ResetHousingSituationValues();
			UserDecsribeYourSituationBCH userDecsribeYourSituationBCH = new UserDecsribeYourSituationBCH();
			userDecsribeYourSituationBCH.ClientNumber = SessionState.ClientNumber.Value;
			userDecsribeYourSituationBCH.ContactComments = txtComment.Text.Clean();
			userDecsribeYourSituationBCH.ContactReason = ddlEvent.SelectedValue.Trim().ToString();
			userDecsribeYourSituationBCH.ClientType = "";
			//userDecsribeYourSituationBCH.MortCurrent=
			//ddlIntType
			userDecsribeYourSituationBCH.RateType = ddlIntType.SelectedValue.Trim();
			//Code added by anil singh 
            if (ddlIntType.SelectedItem.Value != "F" || ddlIntType.SelectedItem.Value != "I")
				userDecsribeYourSituationBCH.MortRate = txtCurIntRate.Text.ToFloat(0);
			else
				userDecsribeYourSituationBCH.MortRate = txtOrgIntRate.Text.ToFloat(0);

			userDecsribeYourSituationBCH.MortDate = txtDateOfMortage.Text;
			userDecsribeYourSituationBCH.ValHome = txtHomeVal.Text.ToFloat(0);

            if (rblMortCur != null && rblMortCur.SelectedItem != null && rblMortCur.SelectedValue != "")
				userDecsribeYourSituationBCH.MortCurrent = Convert.ToInt32(rblMortCur.SelectedValue);
        

			//userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSize.Text);
			//userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
            if (rblHousingSituation.SelectedValue == "R" || HideHousingPanel)
				userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
            else if (rblHousingSituation.SelectedValue == "B")
				userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSize.Text);
            else if (rblHousingSituation.SelectedValue == "O")
				userDecsribeYourSituationBCH.SizeOfHouseHold = Convert.ToInt32(txtHouseholdSizeRent.Text);
			else
				userDecsribeYourSituationBCH.SizeOfHouseHold = 0;

			userDecsribeYourSituationBCH.HousingType = rblHousingSituation.SelectedValue;
			userDecsribeYourSituationBCH.MosDelinq = (!string.IsNullOrEmpty(txtMortLate.Text)) ? (float.Parse)(txtMortLate.Text) : 0;
            if (App.ClientType == Constants.ClientType.BK)
			{
				userDecsribeYourSituationBCH.ClientType = Constants.ClientType.BK;
			}
            else if (App.ClientType == Constants.ClientType.DMP)
			{
				userDecsribeYourSituationBCH.ClientType = Constants.ClientType.DMP;
			}
			else
			{
                if (App.ClientType != Constants.ClientType.HUD)
				{
                    if (userDecsribeYourSituationBCH.MosDelinq > 0)
                    {
                        userDecsribeYourSituationBCH.ClientType = Constants.ClientType.REC;
                        //userDecsribeYourSituationBCH.ClientType = Constants.ClientType.BCH;
                    }
                    else
                    {
                        userDecsribeYourSituationBCH.ClientType = Constants.ClientType.REC;
                        //userDecsribeYourSituationBCH.ClientType = Constants.ClientType.BC;
                    }
				}
				else
				{
					userDecsribeYourSituationBCH.ClientType = Constants.ClientType.HUD;
				}
			}
			//userDecsribeYourSituationBCH.MosDelinq =( string.IsNullOrEmpty(txtMortLate.Text))?float.Parse(txtMortLate.Text):0;
			userDecsribeYourSituationBCH.LastContactDesc = txtLoanDesc.Text.Clean();
			userDecsribeYourSituationBCH.MortHolder = (ddlPriLoanServ.SelectedValue == "12982") ? txtPriLoanHold.Text.Clean() : String.Empty;
			//userDecsribeYourSituationBCH.MortHolder = txtPriLoanHold.Text.Clean();
			userDecsribeYourSituationBCH.LoanNumber = txtLoanNo.Text.Clean();
			userDecsribeYourSituationBCH.LoanNumber2 = txtLoanNo2.Text.Clean();
			userDecsribeYourSituationBCH.MortType = ddlMortageType.SelectedValue.Trim();

			userDecsribeYourSituationBCH.MortYears = txtMortageTerm.Text.ToFloat(0);
			userDecsribeYourSituationBCH.MortDate = txtDateOfMortage.Text.Clean();
			userDecsribeYourSituationBCH.OrigBal = txtStartBal.Text.ToFloat(0);
			userDecsribeYourSituationBCH.Owehome = txtCurrentBal.Text.ToFloat(0);
			userDecsribeYourSituationBCH.ValHome = txtHomeVal.Text.ToFloat(0);
			userDecsribeYourSituationBCH.MoPmt = txtLoanPay.Text.ToFloat(0);
			userDecsribeYourSituationBCH.AmtAvail = txtMoneyAvail.Text.ToFloat(0);

			userDecsribeYourSituationBCH.LastContactDate = txtLoanDescDate.Text.Clean();
			userDecsribeYourSituationBCH.RepayPlan = rblPreDmp.SelectedValue.Trim();
			userDecsribeYourSituationBCH.SecondaryHolder = (ddlSecLoanServ.SelectedValue == "12982") ? txtSecLoanHold.Text.Clean() : String.Empty;
			userDecsribeYourSituationBCH.SecondaryAmt = txtMtg2Amt.Text.ToFloat(0);
			userDecsribeYourSituationBCH.SecondaryStatus = txtMtg2Stat.Text.Clean();
			userDecsribeYourSituationBCH.PROP4Sale = rblSellingProp.SelectedValue.Clean();
			userDecsribeYourSituationBCH.NOTE4Close = rblGotNotice.SelectedValue.Clean();
			userDecsribeYourSituationBCH.WHOInHouse = ddlPropStat.SelectedValue.Trim();
			userDecsribeYourSituationBCH.PRIServID = ddlPriLoanServ.SelectedValue.ToInt(0);
			userDecsribeYourSituationBCH.HpfID = HpfId;

			//if (rblSecondLoan.SelectedValue == "Yes")
			//    userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedValue)) ? Convert.ToInt32(ddlSecLoanServ.SelectedValue.Trim()) : 0;
			//else
			//    userDecsribeYourSituationBCH.SECServID = 0;
			////userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedValue)) ? Convert.ToInt32(ddlSecLoanServ.SelectedValue.Trim()) : 0;

            if (rblSecondLoan.SelectedValue == "Yes")
				userDecsribeYourSituationBCH.SECServID = (!string.IsNullOrEmpty(ddlSecLoanServ.SelectedValue)) ? Convert.ToInt32(ddlSecLoanServ.SelectedValue.Trim()) : 0;
			else
				userDecsribeYourSituationBCH.SECServID = 0;

            userDecsribeYourSituationBCH.MAKEHomeAff = string.Empty;
            //rblAffHomeMod.SelectedValue;
			UserDecsribeYourSituationBCHResult UserDecsribeYourSituationBCHResult = null;
			UserDecsribeYourSituationBCHResult = App.Credability.UserDecsribeYourSituationBCHAddUpdate(userDecsribeYourSituationBCH);
            if (UserDecsribeYourSituationBCHResult.IsSuccessful)
				Result = true;

			return Result;
		}

        public int? SaveHpfCase()
        {
            SaveHpfData SaveHpf = new SaveHpfData();
            PersonalInfoData ProfileData = PersonalInfoData.GetData(SessionState.UserId.Value);
            CcrcHpfData HpfData = App.Credability.CcrcHpfDataGet(SessionState.ClientNumber.Value);

            SaveHpf.PriFName = ProfileData.PrimaryUserDetail.FirstName;
            SaveHpf.PriLName = ProfileData.PrimaryUserDetail.LastName;
            SaveHpf.PriMName = ProfileData.PrimaryUserDetail.MiddleName;
            SaveHpf.PriSsn = ProfileData.PrimaryUserDetail.Ssn;
            SaveHpf.PriDob = ProfileData.PrimaryUserDetail.BirthDate;

            if (ProfileData.SecondUserDetail != null)
            {
                SaveHpf.SecFName = ProfileData.SecondUserDetail.FirstName;
                SaveHpf.SecLName = ProfileData.SecondUserDetail.LastName;
                SaveHpf.SecMName = ProfileData.SecondUserDetail.MiddleName;
                SaveHpf.SecSsn = ProfileData.SecondUserDetail.Ssn;
                SaveHpf.SecDob = ProfileData.SecondUserDetail.BirthDate;
            }

            SaveHpf.Addr1 = ProfileData.PrimaryUserAddress.StreetLine1;
            SaveHpf.Addr2 = ProfileData.PrimaryUserAddress.StreetLine2;
            SaveHpf.City = ProfileData.PrimaryUserAddress.City;
            SaveHpf.State = ProfileData.PrimaryUserAddress.State;
            SaveHpf.ZipCode = ProfileData.PrimaryUserAddress.Zip;

            SaveHpf.Phone1 = ProfileData.PrimaryUserDetail.PhoneHome;
            SaveHpf.Phone2 = ProfileData.PrimaryUserDetail.PhoneCell;
            SaveHpf.eMail = ProfileData.PrimaryUserDetail.Email;

            SaveHpf.HpfID = HpfData.HpfID;
            SaveHpf.CcrcAprov = HpfData.CcrcAprov;
            SaveHpf.CreateDTS = HpfData.CreateDTS;

            SaveHpf.InNo = SessionState.ClientNumber.Value;
            SaveHpf.HomeVal = txtHomeVal.Text.ToDouble(0);
            SaveHpf.WhoInHouse = ddlPropStat.SelectedValue.Trim();

            SaveHpf.PriServID = ddlPriLoanServ.SelectedValue.ToInt(0);
            SaveHpf.PriHolder = (ddlPriLoanServ.SelectedValue == "12982") ? txtPriLoanHold.Text.Clean() : String.Empty;
            SaveHpf.PriAcctNo = txtLoanNo.Text.Clean();

            if (rblSecondLoan.SelectedValue == "Yes")
            {
                SaveHpf.SecondLoan = "Y";
                SaveHpf.SecServID = ddlSecLoanServ.SelectedValue.ToInt(0);
                SaveHpf.SecHolder = (ddlSecLoanServ.SelectedValue == "12982") ? txtSecLoanHold.Text.Clean() : String.Empty;
                SaveHpf.SecAcctNo = txtLoanNo2.Text.Clean();
            }
            else
            {
                SaveHpf.SecondLoan = "N";
            }

            CcrcHpf.Impl.CcrcHpf CheckHpf = new CcrcHpf.Impl.CcrcHpf();
            SaveHpfResult TheResult = CheckHpf.SubmitHpfCase(SaveHpf);

            var CurLanguage = SessionState.LanguageCode == Language.EN ? Constants.HostLanguage.English : Constants.HostLanguage.Spanish;
            //Bankruptcy conversion-Seethal
            //App.Host.SendHpfDisclosure(SessionState.ClientNumber.Value, CurLanguage, ProfileData.PrimaryUserDetail.Email, ProfileData.PrimaryUserDetail.FirstName, ProfileData.PrimaryUserDetail.LastName, ProfileData.PrimaryUserAddress.StreetLine1, ProfileData.PrimaryUserAddress.StreetLine2, ProfileData.PrimaryUserAddress.City, ProfileData.PrimaryUserAddress.State, ProfileData.PrimaryUserAddress.Zip);
            App.Debtplus.SendHpfDisclosure(SessionState.ClientNumber.Value, CurLanguage, ProfileData.PrimaryUserDetail.Email, ProfileData.PrimaryUserDetail.FirstName, ProfileData.PrimaryUserDetail.LastName, ProfileData.PrimaryUserAddress.StreetLine1, ProfileData.PrimaryUserAddress.StreetLine2, ProfileData.PrimaryUserAddress.City, ProfileData.PrimaryUserAddress.State, ProfileData.PrimaryUserAddress.Zip);
            return TheResult.HpfID;
        }

		protected void rblHousingSituation_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetHousingSituation();
			rblHousingSituation.Focus();
		}

        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            string redirectUrl = String.Empty;
            if (App.WebsiteCode == Cccs.Identity.Website.BKC)
            {
                redirectUrl = "DescribingYourSituation.aspx";
            }
            else if (App.WebsiteCode == Cccs.Identity.Website.DMP)
		{
                redirectUrl = "ListingYourDebts.aspx";
            }
            else
			{
                redirectUrl = "UserProfile.aspx";
            }

            Page.Validate();
             
            if (Page.IsValid == true)
            {
                if (isNumeric(txtHouseholdSize.Text.ToString(), System.Globalization.NumberStyles.Integer)
                    || rblHousingSituation.SelectedValue == "R" || rblHousingSituation.SelectedValue == "O")
                {
                    if (Convert.ToInt32(txtHouseholdSizeRent.Text) <= 0 && rblHousingSituation.SelectedValue != "B")
                    {
                        CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household must include you.", true);
                    }
                    else
                    {
                        if (txtComment.Text == string.Empty)
                        {
                            CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
                        }
                        else
                        {
                            if (isNumeric(ddlEvent.SelectedValue.ToString(), System.Globalization.NumberStyles.Integer))
                            {
                                //if (((!HideChatAuthorization && UcAuthCode.IsChatCodeValid ||(HideChatAuthorization))
                                //    //e(ChatCodeOption.Session1, txtAuthCodeInput.Text) 
                                //                               && rblHousingSituation.SelectedValue != "B") || rblHousingSituation.SelectedValue == "B")
                                //{
                                SessionState.CompanyName = ddlPriLoanServ.SelectedItem.Text.ToString().Trim();
                                SessionState.SecCompanyName = ddlSecLoanServ.SelectedItem.Text.ToString().Trim();
                                // Save the data into database
                                if (rblMortCur != null &&
                                                rblMortCur.SelectedItem != null &&
                                                rblMortCur.SelectedValue == "1" && (rblHousingSituation.SelectedValue == "B") &&
                                                txtMortLate.Text.ToInt(0) > 0 && App.WebsiteCode != Cccs.Identity.Website.BKC)
                                {
                                    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, App.Translate("Credability|CredabilityWebSite|YHI"), true);
                                }
                                else if (rblMortCur != null &&
                                                rblMortCur.SelectedItem != null &&
                                                rblMortCur.SelectedValue == "0" && (rblHousingSituation.SelectedValue == "B") &&
                                                Convert.ToInt32(txtMortLate.Text.Trim()) == 0 && App.WebsiteCode != Cccs.Identity.Website.BKC)
                                {
                                    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, App.Translate("Credability|CredabilityWebSite|YHIT"), true);
			}
			else
			{
                                    int? HpfID = null;
                                    if (SessionState.RefCode == "ccrc/620")
                                        HpfID = SaveHpfCase();
                                    if (SaveContactData(HpfID) && SaveExtraQuestData() && SaveExtraHudAbtData())
                                    {
                                        string clientType = App.ClientType;
                                        if (App.ClientType == Constants.ClientType.HUD)
                                        {
                                            SessionState.ClientType = Constants.ClientType.HUD;
                                            CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
                                        }
             
                                        if (rblHousingSituation.SelectedValue != "B")
                                        {
                                            if (clientType != Constants.ClientType.HUD)
                                            {
                                                clientType = Constants.ClientType.REC;
                                                //clientType = Constants.ClientType.BC;
                                            }
                                        }
                                        else
                                        {
                                            if (rblHousingSituation.SelectedValue == "B" && rblMortCur.SelectedValue == "1" && txtMortLate.Text.Trim() == "0")
                                            {
                                                if (clientType != Constants.ClientType.HUD)
                                                {
                                                    clientType = Constants.ClientType.REC;
                                                    //clientType = Constants.ClientType.BC;
                                                }
                                            }
                                            else
                                            {
                                                if (clientType != Constants.ClientType.HUD)
                                                {
                                                    clientType = Constants.ClientType.REC;
                                                    //clientType = Constants.ClientType.BCH;
                                                }
                                            }
                                        }
                                        SessionState.ClientType = clientType;
                                        CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
                                        Response.Redirect(redirectUrl);
                                    }
                                    else
                                    {
                                        CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Some Error Occured during the operation please try later.", true);
                                    }
			}
                                //}
                                //else
                                //{
                                //    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.", true);
                                //}
		}
                            else
		{
                                CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
                            }
                        }
                    }
                }
                else
			{
                    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household is Required.", true);
                }
			}
		}

		protected void rblSecondLoan_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (rblSecondLoan.SelectedValue.ToString() == "Yes")
			{
				pnlSeconLoanInfo.Visible = true;
			}
			else
			{
				pnlSeconLoanInfo.Visible = false;
				dvOtherCompanyName.Visible = false;
			}
			rblSecondLoan.Focus();
		}

		protected void ddlSecLoanServ_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (App.Credability.IsOtherServicer(ddlSecLoanServ.SelectedValue.ToInt(0)))
			{
				dvOtherCompanyName.Visible = true;
				txtSecLoanHold.Focus();
			}
			else
			{
				dvOtherCompanyName.Visible = false;
				ddlSecLoanServ.Focus();
			}
		}

		// 05/05/2010 CREDA-382 - Interest type error on BCH and BC site
		protected void ddlIntType_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetInterestRateText();
			ddlIntType.Focus();
		}

		// 05/05/2010 CREDA-382 - Interest type error on BCH and BC site

		protected void ddlPriLoanServ_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (App.Credability.IsOtherServicer(ddlPriLoanServ.SelectedValue.ToInt(0)))
			{
				PriLoan1.Visible = true;
				PriLoan2.Visible = true;
				txtPriLoanHold.Visible = true;
				//txtPriLoanHold.Focus();
				ddlPriLoanServ.Focus();
			}
			else
			{
				PriLoan1.Visible = false;
				PriLoan2.Visible = false;
				txtPriLoanHold.Visible = false;
				ddlPriLoanServ.Focus();
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			Page.Validate();
            if (Page.IsValid)
			{
                if (isNumeric(txtHouseholdSize.Text.ToString(), System.Globalization.NumberStyles.Integer)
					|| rblHousingSituation.SelectedValue == "R" || rblHousingSituation.SelectedValue == "O")
				{
                    if (Convert.ToInt32(txtHouseholdSizeRent.Text) <= 0 && rblHousingSituation.SelectedValue != "B")
					{
						CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household must include you.", true);
					}
					else
					{
                        if (txtComment.Text == string.Empty)
						{
							CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
						}
						else
						{
                            if (isNumeric(ddlEvent.SelectedValue.ToString(), System.Globalization.NumberStyles.Integer))
							{
                                //if (((!HideChatAuthorization && UcAuthCode.IsChatCodeValid || (HideChatAuthorization))
                                //    && rblHousingSituation.SelectedValue != "B") || rblHousingSituation.SelectedValue == "B")
                                //{
									SessionState.CompanyName = ddlPriLoanServ.SelectedItem.Text.ToString().Trim();
									SessionState.SecCompanyName = ddlSecLoanServ.SelectedItem.Text.ToString().Trim();
									// Save the data into database
                                if (rblMortCur != null && rblMortCur.SelectedItem != null
										&& rblMortCur.SelectedValue == "1" && (rblHousingSituation.SelectedValue == "B")
										&& txtMortLate.Text.ToInt(0) > 0 && App.WebsiteCode != Cccs.Identity.Website.BKC)
									{
										CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, App.Translate("Credability|CredabilityWebSite|YHI"), true);
									}
                                else if (rblMortCur != null && rblMortCur.SelectedItem != null
										&& rblMortCur.SelectedValue == "0" && (rblHousingSituation.SelectedValue == "B")
										&& Convert.ToInt32(txtMortLate.Text.Trim()) == 0
										&& App.WebsiteCode != Cccs.Identity.Website.BKC)
									{
										CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, App.Translate("Credability|CredabilityWebSite|YHIT"), true);
									}
									else
									{
										int? HpfID = null;
                                    if (SessionState.RefCode == "ccrc/620")
											HpfID = SaveHpfCase();
                                    if (SaveContactData(HpfID) && SaveExtraQuestData() && SaveExtraHudAbtData())
										{
											string redirectUrl = "ListingYourDebts.aspx";
											string clientType = App.ClientType;
                                        if (App.ClientType == Constants.ClientType.HUD)
											{
												SessionState.ClientType = Constants.ClientType.HUD;
												CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
											}
                                        if (App.WebsiteCode == Cccs.Identity.Website.DMP)
											{
												redirectUrl = "UserIncomeDocumentation.aspx";
											}


                                        else if (rblHousingSituation.SelectedValue != "B")
											{
                                            if (clientType != Constants.ClientType.HUD)
												{
                                                    clientType = Constants.ClientType.REC;
													//clientType = Constants.ClientType.BC;
												}
												redirectUrl = "ListingYourDebts.aspx";
											}
											else
											{
                                            if (App.WebsiteCode == Cccs.Identity.Website.BKC)
												{
													redirectUrl = "CommonOptions.aspx";
												}
                                            else if (rblHousingSituation.SelectedValue == "B" && rblMortCur.SelectedValue == "1" && txtMortLate.Text.Trim() == "0")
												{
                                                if (clientType != Constants.ClientType.HUD)
													{
                                                        clientType = Constants.ClientType.REC;
														//clientType = Constants.ClientType.BC;
														redirectUrl = "PostPurchace.aspx";
													}
													else
													{
														redirectUrl = "Hud1.aspx";
													}
												}
												else
												{
                                                if (clientType != Constants.ClientType.HUD)
													{
                                                        clientType = Constants.ClientType.REC;
														//clientType = Constants.ClientType.BCH;
													}
													redirectUrl = "Hud1.aspx";
												}
											}
											SessionState.ClientType = clientType;
											CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
											Response.Redirect(redirectUrl);
										}
										else
										{
											CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Some Error Occured during the operation please try later.", true);
										}
									}
								//}
                                //else
                                //{
                                //    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.", true);
                                //}
							}
							else
							{
								CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
							}
						}
					}
				}
				else
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household is Required.", true);
				}
			}
		}

        protected void btnSaveAndExitAndSubmitToQueue_Click(object sender, EventArgs e)
			{
            CommonFunction.SendToCounselor(false);
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}

		protected void btnSaveAndExit_Click(object sender, EventArgs e)
		{
			Page.Validate();

            if (Page.IsValid == true)
			{
                if (isNumeric(txtHouseholdSize.Text.ToString(), System.Globalization.NumberStyles.Integer) ||
						rblHousingSituation.SelectedValue == "R" ||
						rblHousingSituation.SelectedValue == "O")
				{
                    if (Convert.ToInt32(txtHouseholdSizeRent.Text) <= 0 && rblHousingSituation.SelectedValue != "B")
					{
						CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household must include you.", true);
					}
					else
					{
                        if (txtComment.Text == string.Empty)
						{
							CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
						}
						else
						{
                            if (isNumeric(ddlEvent.SelectedValue.ToString(), System.Globalization.NumberStyles.Integer))
							{
                               // if(((!HideChatAuthorization && UcAuthCode.IsChatCodeValid || (HideChatAuthorization))
                               //&& rblHousingSituation.SelectedValue != "B") || rblHousingSituation.SelectedValue == "B")
                               // {
									SessionState.CompanyName = ddlPriLoanServ.SelectedItem.Text.ToString().Trim();
									SessionState.SecCompanyName = ddlSecLoanServ.SelectedItem.Text.ToString().Trim();
									// Save the data into database
									int? HpfID = null;
                                if (SessionState.RefCode == "ccrc/620")
										HpfID = SaveHpfCase();
                                if (SaveContactData(HpfID) && SaveExtraQuestData() && SaveExtraHudAbtData())
									{
										CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
										Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
									}
									else
									{
										CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Some Error Occured during the operation please try later.", true);
									}
                                //}
                                //else
                                //{
                                //    CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.", true);
                                //}
							}
							else
							{
								CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "A brief summary of your situation is Required.", true);
							}
						}
					}
				}
				else
				{
					txtHouseholdSize.Text = "";
					CommonFunction.ShowErrorMessageAtPageTop(dvTopErrorMessage, "Number of People in Household is Required.", true);
				}
			}
			}

        public bool HideHousingPanel { get; set; }



	}
}