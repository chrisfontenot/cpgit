﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDescribeYourSituation.ascx.cs"
    Inherits="Cccs.Credability.Website.Controls.RecControls.UCDescribeYourSituation" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc"
    TagName="AuthCode" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<style type="text/css">
    .style1 {
        width: 9%;
    }

    .style2 {
        width: 12%;
    }

    .dvrow {
        width: 1078px;
    }

    .style3 {
        width: 85px;
    }

    .style4 {
        width: 11%;
    }

    .style5 {
        width: 118px;
    }

    .style6 {
        width: 906px;
    }

    .style7 {
        width: 10%;
    }

    .style8 {
        width: 141px;
    }

    .style9 {
        width: 11%;
    }

    .style10 {
        width: 75px;
    }

    .style11 {
        width: 10%;
    }

    .style12 {
        width: 11%;
    }

    .style13 {
        width: 126px;
    }

    .style14 {
        width: 11%;
    }

    .style15 {
        width: 69px;
    }

    .style16 {
        width: 7%;
    }

    .style17 {
        width: 5%;
    }

    .style18 {
        width: 8%;
    }

    .style19 {
        width: 9%;
    }

    .style20 {
        width: 103px;
    }

    .style21 {
        width: 983px;
    }

    .style22 {
        width: 9%;
    }
</style>

<script type="text/javascript">

    function ValIntRate(thefield) {
        var HoldIntRate = thefield //document.getElementById('<%=txtCurIntRate.ClientID %>');

            ReFormatPercent(HoldIntRate)
            var IntRateValue = Number(HoldIntRate.value.replace("%", ""));

            if (IntRateValue < 1.0) {
                if (!confirm("Is this amount correct?")) {
                    setTimeout("HoldIntRate.focus();HoldIntRate.select();", 10);
                }
            }
            else {
                if (IntRateValue > 15.0) {
                    if (!confirm("Is this amount correct?")) {
                        setTimeout("HoldIntRate.focus();HoldIntRate.select();", 10);
                    }
                }
            }
        }

        function CleanField(theField) {
            var sValue = theField.value;
            sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
            if (sValue < 0)
                sValue = "";
            theField.value = String(sValue);
        }

</script>

<h1 visible="false" id="headingDMP" runat="server">
    <%= Cccs.Credability.Website.App.Translate("Credability|UserIncomeDocumentationInfo|StepTitle")%>&nbsp;&nbsp;
</h1>
<h1 visible="true" id="headingAll" runat="server">
    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DUS")%>&nbsp;&nbsp;</h1>

<span id="introdSpan" runat="server">
    <p>
        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|EPSI")%>&nbsp;&nbsp;
    </p>
</span><span id="introdBKCSpan" visible="false" runat="server">
    <p>
        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBKC|TEAA")%>&nbsp;&nbsp;
    </p>
</span><span id="introDMPSpan" visible="false" runat="server">
    <p>
        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationDMP|TEAA")%>&nbsp;&nbsp;
    </p>
</span>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:"
    DisplayMode="BulletList" EnableClientScript="true" runat="server" />
<div id="dvTopErrorMessage" cssclass="DvErrorSummary" runat="server" visible="false"
    style="color: Red;">
</div>
<div class="dvform2col">
    <div>
        <div class="dvform">
            <span id="primaryEventSpan" runat="server">
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|PTCA")%></label>
                    <span>
                        <asp:DropDownList ID="ddlEvent" runat="server">
                        </asp:DropDownList>
                    </span><span style="color: Red; font-size: large">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvEvent" runat="server" Display="Dynamic" ControlToValidate="ddlEvent"
                        Text="!">
                    </asp:RequiredFieldValidator>
                </div>
            </span><span id="secondEventSpan" runat="server">
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TSE")%></label>
                    <span>
                        <asp:DropDownList ID="ddlSecEvent" runat="server">
                        </asp:DropDownList>
                    </span><span style="color: Red; font-size: large">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvSecEvent" runat="server" EnableClientScript="true"
                        Display="Dynamic" ControlToValidate="ddlSecEvent" Text="!">
                    </asp:RequiredFieldValidator>
                </div>
            </span><span id="describeCircumSpan" runat="server">
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DTCT")%></label>
                    <asp:TextBox ID="txtComment" runat="server" Rows="5" Width="300" TextMode="MultiLine"
                        MaxLength="600">
                    </asp:TextBox>
                    <span style="color: Red; font-size: large">&nbsp;*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvComment" runat="server" EnableClientScript="true"
                        Display="Dynamic" ControlToValidate="txtComment" Text="!">
                    </asp:RequiredFieldValidator>
                </div>
            </span>
        </div>
        <div class="dvrow">
            <p class="col_title">
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IP2Y")%>
            </p>
        </div>
    </div>
    <div class="colformlft">
        <div class="dvform">

            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MEOT")%></label>
                <table class="style1">
                    <tr>
                        <td>
                            <span>
                                <asp:RadioButtonList ID="rblMedExp" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"
                                    RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvMedExp" runat="server" ControlToValidate="rblMedExp" Display="Dynamic"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DSOD")%></label>
                <table class="style2">
                    <tr>
                        <td class="style3">
                            <span>
                                <asp:RadioButtonList ID="rblLostSO" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"
                                    RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvLostSO" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="rblLostSO"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|YOFW")%></label>
                <table class="style4">
                    <tr>
                        <td class="style5">
                            <span>
                                <asp:RadioButtonList ID="rblHealthWage" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td class="style6">
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvHealthWage" runat="server" EnableClientScript="true" Display="Dynamic"
                                ControlToValidate="rblHealthWage" Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dvrow" id="dvFelony" runat="server">
                <label>
                   <!-- Have you been convicted of a felony larceny, theft, fraud, forgery, money laundering or tax evasion in connection with a mortgage within the last 10 years?-->
                         <%= Cccs.Credability.Website.App.Translate("HARPQuestion1")%></label>
               
                <table class="style4">
                    <tr>
                        <td class="style5">
                            <span>
                                <asp:RadioButtonList ID="rblFelony" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                 
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td class="style6">
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvFelony" runat="server" EnableClientScript="true" Display="Dynamic"
                                ControlToValidate="rblFelony" Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AEPO")%></label>
                <table class="style7">
                    <tr>
                        <td class="style8">
                            <span>
                                <asp:RadioButtonList ID="rblUnemployed" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvUnemployed" runat="server" EnableClientScript="true" Display="Dynamic"
                                ControlToValidate="rblUnemployed" Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            
        </div>
    </div>
    <div class="colformlft">
        <div class="dvform">
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LPOA")%></label>
                <table class="style9">
                    <tr>
                        <td class="style10">
                            <span>
                                <asp:RadioButtonList ID="rblTaxLien" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvTaxLien" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="rblTaxLien"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="dvrow">
                <label>
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYEA")%></label>
                <table class="style11">
                    <tr>
                        <td>
                            <span>
                                <asp:RadioButtonList ID="rblMortRate" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvMortRate" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="rblMortRate"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>

            
            <div class="dvrow" runat="server" id="dvBusinessOrPersonalMortgage">
                <label><!--    Is this a personal mortgage?-->
                     <%= Cccs.Credability.Website.App.Translate("HARPQuestion2")%>
                </label>
                <table class="style11">
                    <tr>
                        <td>
                            <span>
                                <asp:RadioButtonList ID="rblBusiness" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                               
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvBusiness" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="rblBusiness"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="clearboth">
    </div>
    <div class="dvrow">
        <p>
            <asp:Panel ID="PnlHouseOnly" runat="server">
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TrWYARH")%>
            </asp:Panel>
            <asp:Panel ID="PnlBch" runat="server">
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TrWYAR")%>
            </asp:Panel>
        </p>
        <asp:Panel ID="pnlHudOption_1" runat="server">
            <p>
                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|PLIBW")%>
                <span class="requiredField">*</span>
                <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidatorrblHousingSituation" runat="server"
                    ControlToValidate="rblHousingSituation" Text="!" />
            </p>
        </asp:Panel>
    </div>


    <asp:Panel ID="pnlHudOption_2" runat="server">
        <div class="dvrow">
            <label>
                &nbsp;</label>
            <asp:RadioButtonList ID="rblHousingSituation" runat="server" CellSpacing="5" RepeatLayout="Table" AutoPostBack="true"
                CssClass="DvradiolistAuto" RepeatDirection="Vertical" OnSelectedIndexChanged="rblHousingSituation_SelectedIndexChanged">
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator class="validationMessage" ID="rfvrblHousingSituation" runat="server" ControlToValidate="rblHousingSituation"
                Display="Dynamic" EnableClientScript="true" Text="!">
            </asp:RequiredFieldValidator>
        </div>
    </asp:Panel>






    <asp:Panel ID="dvBuying" runat="server" CssClass="dvBuying">
        <div class="dvform2col dvformlblbig">
            <div class="dvform">

                <asp:Panel ID="bkcPanelWYAB" Visible="false" runat="server">
                    <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TrWYARH")%>
                </asp:Panel>
            </div>
            <div class="clearboth">
            </div>
        </div>
        <div class="dvform2col">
            <div class="colformlft">
                <div class="dvform">
                    <div class="dvrow">
                        <p class="col_title">
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AYML")%>
                        </p>
                    </div>
                    <div class="dvrow" runat="server" id="ClientGoalsPanel">
                      
                     
                        <label>
                           <!-- Client Goals-->
                               <%= Cccs.Credability.Website.App.Translate("HARPQuestion3")%></label>
                       
                   
                        <asp:Image style="vertical-align:top;margin-top:5px" ID="imgHelpClient" runat="server" ImageUrl="~/Images/help_16.gif" ToolTip="Client Goals Example: Sell my home. Modify My Mortgage. Refinance my Mortgage." />
                        <asp:TextBox TextMode="MultiLine" ID="txtClientGoals" runat="server" MaxLength="150" />
                       
                        <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvClientGoals"
                            runat="server" ControlToValidate="txtClientGoals" Display="Dynamic" EnableClientScript="true"
                            Text="!"></asp:RequiredFieldValidator></span>

                    </div>
                    <div class="dvrow" runat="server" id="TimelineEventsPanel">
                        <label>
                         <!--   Timeline of Events-->
                               <%= Cccs.Credability.Website.App.Translate("HARPQuestion4")%></label>
                    
                       
                        <asp:Image style="vertical-align:top;margin-top:5px" ID="imgHelpTimeline" runat="server" ImageUrl="~/Images/help_16.gif" 
                            ToolTip="Timeline of Events Example: I lost my job in August 2012. I used my savings and withdrew funds from my 401(k) to remain current through November 2012. After exhausting all of my financial resources, I missed my first payment in December 2012. I am now 1 month past due on the mortgage."
                            />

                        <asp:TextBox TextMode="MultiLine" ID="txtTimeLineEvents" runat="server" MaxLength="150" />

                        <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvTimeLineEvents"
                            runat="server" ControlToValidate="txtTimeLineEvents" Display="Dynamic" EnableClientScript="true"
                            Text="!"></asp:RequiredFieldValidator></span>

                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|NOC")%></label>
                        <asp:DropDownList ID="ddlPriLoanServ" runat="server" Width="250" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlPriLoanServ_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvPriLoanServ" runat="server" ControlToValidate="ddlPriLoanServ"
                            Display="Dynamic" EnableClientScript="true" InitialValue="0" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label id="PriLoan1" runat="server" visible="false">
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OSN")%>
                        </label>
                        <div id="PriLoan2" runat="server" style="float: left" visible="false">
                            <asp:TextBox ID="txtPriLoanHold" runat="server">
                            </asp:TextBox>
                            <span>*</span>
                        </div>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LN")%></label>
                        <asp:TextBox ID="txtLoanNo" runat="server" MaxLength="20" onfocus="SetUpField(this);">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvLoanNo" Display="Dynamic" runat="server"
                            ControlToValidate="txtLoanNo" EnableClientScript="true" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MT")%></label>
                        <asp:DropDownList ID="ddlMortageType" runat="server">
                        </asp:DropDownList>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvddlMortageType" runat="server" ControlToValidate="ddlMortageType"
                            Display="Dynamic" EnableClientScript="true" InitialValue="0" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                        <!--    Type of Housing-->
                              <%= Cccs.Credability.Website.App.Translate("HARPQuestion5")%>
                        </label>
                        <asp:DropDownList ID="ddlHousing" runat="server" />
                          
                     
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvHousing" runat="server" ControlToValidate="ddlHousing"
                            Display="Dynamic" EnableClientScript="true" InitialValue="0" Text="!">
                        </asp:RequiredFieldValidator>

                    </div>
                    <div class="dvrow">
                        <label>
                            <!--Mortgage Investor-->
                              <%= Cccs.Credability.Website.App.Translate("HARPQuestion6")%>

                        </label>
                        <asp:DropDownList ID="ddlMortgageInvestor" runat="server"/>
                            
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvMortgageInvestor" runat="server" ControlToValidate="ddlMortgageInvestor"
                            Display="Dynamic" EnableClientScript="true" InitialValue="0" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>

                    <div class="dvrow">
                        <label>
                          <!--  Was the loan sold to Freddie Mac or Fannie Mae on or before May 31, 2009?-->
                              <%= Cccs.Credability.Website.App.Translate("HARPQuestion7")%>
                        </label>
                        <table class="style14">
                            <tr>
                                <td class="style15">
                                    <asp:RadioButtonList ID="rblSoldToFM" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                      
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvSoldToFM"
                                        runat="server" ControlToValidate="rblSoldToFM" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                          <!--  Is the loan a first lien, conventional mortgage backed by Freddie Mac or Fannie Mae?-->
                              <%= Cccs.Credability.Website.App.Translate("HARPQuestion8")%>
                        </label>
                        <table class="style14">
                            <tr>
                                <td class="style15">
                                    <asp:RadioButtonList ID="rblFirstLienFreddie" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                 
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvFirstLienFreddie"
                                        runat="server" ControlToValidate="rblFirstLienFreddie" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|MTerm")%></label>
                        <asp:TextBox ID="txtMortageTerm" runat="server" MaxLength="2" Width="90px" onfocus="SetUpField(this);">
                        </asp:TextBox>&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|30")%>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvtxtMortageTerm" runat="server" ControlToValidate="txtMortageTerm"
                            Display="Dynamic" EnableClientScript="true" Text="!">
                        </asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rvMortageTerm" runat="server" MinimumValue="1" Type="Integer"
                            MaximumValue="100" ControlToValidate="txtMortageTerm" Text="!">
                        </asp:RangeValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DOM")%></label>
                        <asp:TextBox ID="txtDateOfMortage" runat="server" MaxLength="20" Width="80px">
                        </asp:TextBox>&nbsp;<%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rvtxtDateOfMortage" runat="server" ControlToValidate="txtDateOfMortage"
                            Display="Dynamic" EnableClientScript="true" Text="!">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="revtxtDateOfMortage" runat="server" ControlToValidate="txtDateOfMortage"
                            Display="Dynamic" Text="!" SetFocusOnError="true" ValidationExpression="^([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DYMU")%></label>
                        <table class="style14">
                            <tr>
                                <td class="style15">
                                    <asp:RadioButtonList ID="rblGovNpPro" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rvrblGovNpPro"
                                        runat="server" ControlToValidate="rblGovNpPro" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IType")%></label>
                        <asp:DropDownList ID="ddlIntType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlIntType_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvIntType" runat="server" ControlToValidate="ddlIntType"
                            InitialValue="0" Display="Dynamic" EnableClientScript="true" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <asp:Label ID="lblIntRate" runat="server" Text=""></asp:Label></label>
                        <asp:TextBox ID="txtCurIntRate" runat="server" Text="0.00%" onblur="ValIntRate(this);"
                            onfocus="SetUpField(this);">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvCurIntRate" runat="server" ControlToValidate="txtCurIntRate"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="0.00 %">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rcCurIntRate" runat="server" ControlToValidate="txtCurIntRate"
                            Display="Dynamic" Text="!" ValidationExpression="^\d{0,2}(\.\d{1,4})? *%?$" />
                    </div>
                    <asp:Panel ID="pnlArmInrest" runat="server">
                        <%-- Original interest rate --%>
                        <div id="IntRate2" class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OIR")%>
                            </label>
                            <asp:TextBox ID="txtOrgIntRate" runat="server" Text="0.00%" onblur="ValIntRate(this);"
                                onfocus="SetUpField(this);">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtOrgIntRate" runat="server" ControlToValidate="txtOrgIntRate"
                                Display="Dynamic" Text="!" ValidationExpression="^\d{0,2}(\.\d{1,4})? *%?$" />
                        </div>
                        <%-- Maximum interest rate --%>
                        <div id="IntRate5" class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdWhatInt")%>
                            </label>
                            <asp:TextBox ID="txtMaxIntRate" runat="server" Text="0.00%" onblur="ValIntRate(this);"
                                onfocus="SetUpField(this);">
                            </asp:TextBox>
                            <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtMaxIntRate" runat="server" ControlToValidate="txtMaxIntRate"
                                Display="Dynamic" Text="!" ValidationExpression="^\d{0,2}(\.\d{1,4})? *%?$" />
                        </div>
                    </asp:Panel>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OLB")%></label>
                        <asp:TextBox ID="txtStartBal" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvStartBal" runat="server" ControlToValidate="txtStartBal"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtStartBal" runat="server" ControlToValidate="txtStartBal"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CurBal")%></label>
                        <asp:TextBox ID="txtCurrentBal" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvCurrentBal" runat="server" ControlToValidate="txtCurrentBal"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtCurrentBal" runat="server" ControlToValidate="txtStartBal"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|EVOH")%></label>
                        <asp:TextBox ID="txtHomeVal" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvHomeVal" runat="server" ControlToValidate="txtHomeVal"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtHomeVal" runat="server" ControlToValidate="txtHomeVal"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SMPA")%></label>
                        <asp:TextBox ID="txtLoanPay" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvLoanPay" runat="server" ControlToValidate="txtLoanPay"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtLoanPay" runat="server" ControlToValidate="txtLoanPay"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>

                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|ATII")%></label>
                        <table class="style19">
                            <tr>
                                <td class="style20">
                                    <asp:RadioButtonList ID="rblIsTaxInc" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                    </asp:RadioButtonList>
                                </td>
                                <td class="style21">
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvIsTaxInc"
                                        runat="server" ControlToValidate="rblIsTaxInc" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                           <!-- Have you agreed to set-up an escrow account, if one does not exist?-->
                                 <%= Cccs.Credability.Website.App.Translate("HARPQuestion10")%>
                        </label>
                        <table class="style19">
                            <tr>
                                <td class="style20">
                                    <asp:RadioButtonList ID="rblEscrow" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                      
                                    </asp:RadioButtonList>
                                </td>
                                <td class="style21">
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvEscrow"
                                        runat="server" ControlToValidate="rblEscrow" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WITA")%></label>
                        <asp:TextBox ID="txtTaxPay" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvTaxPay" runat="server" ControlToValidate="txtTaxPay"
                            Display="Dynamic" EnableClientScript="true" Text="!" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtTaxPay" runat="server" ControlToValidate="txtTaxPay"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IIII")%></label>
                        <table class="style19">
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblIsInsIncYes" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvIsInsInc"
                                        runat="server" ControlToValidate="rblIsInsIncYes" Display="Dynamic" EnableClientScript="true"
                                        Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WhatInsu")%></label>
                        <asp:TextBox ID="txtInsPay" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvInsPay" runat="server" ControlToValidate="txtInsPay"
                            EnableClientScript="true" Text="!" Display="Dynamic" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtInsPay" runat="server" ControlToValidate="txtInsPay"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>

                    <span id="noofPeopleSpan" runat="server">
                        <div class="dvrow">
                            <label>
                                <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdNumThat")%></label>
                            <asp:TextBox ID="txtHouseholdSize" runat="server" MaxLength="2" onfocus="SetUpField(this);">
                            </asp:TextBox>
                            <span class="requiredField">*</span>
                            <asp:RequiredFieldValidator class="validationMessage" ID="rftxtHouseholdSize" runat="server" ControlToValidate="txtHouseholdSize"
                                Display="Dynamic" EnableClientScript="true" SetFocusOnError="true" Text="!">
                            </asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rvtxtHouseholdSize" runat="server" MinimumValue="1" Type="Integer"
                                MaximumValue="100" ControlToValidate="txtHouseholdSize" Text="!" Display="Dynamic">
                            </asp:RangeValidator>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="colformrht">
            <div class="dvform">
                <div class="dvrow">
                    <p class="col_title">
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|CLStatus")%>
                    </p>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IYPF")%></label>
                    <table class="style14">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblSellingProp" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                    RepeatDirection="Horizontal" RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rvfSellingProp"
                                    runat="server" ControlToValidate="rblSellingProp" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYRAF")%></label>
                    <table class="style16">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblGotNotice" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                    RepeatDirection="Horizontal" RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator13"
                                    runat="server" ControlToValidate="rblGotNotice" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HYEBI")%></label>
                    <table class="style17">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblPreDmp" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                    RepeatDirection="Horizontal" RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator14"
                                    runat="server" ControlToValidate="rblPreDmp" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow">
                    <label>
                       <!-- Was the loan refinanced under HARP previously?-->
                          <%= Cccs.Credability.Website.App.Translate("HARPQuestion9")%>
                    </label>
                    <table class="style17">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblRefinance" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                    RepeatDirection="Horizontal" RepeatLayout="Table">
                                   
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvRefinance"
                                    runat="server" ControlToValidate="rblRefinance" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow" id="dvMortgageOrHamp" runat="server">
                <label>
                    <!--Has your mortgage been previously modified under HAMP or have you ever been on a trial modification?-->
                      <%= Cccs.Credability.Website.App.Translate("HARPQuestion11")%>
                </label>
                <table class="style11">
                    <tr>
                        <td>
                            <span>
                                <asp:RadioButtonList ID="rblModifiedHamp" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table" RepeatColumns="2" CssClass="Dvradiolist">
                                
                                </asp:RadioButtonList>
                            </span>
                        </td>
                        <td>
                            <span><span class="requiredField">*</span><asp:RequiredFieldValidator
                                ID="rfvModifiedHamp" runat="server" EnableClientScript="true" Display="Dynamic" ControlToValidate="rblModifiedHamp"
                                Text="!"></asp:RequiredFieldValidator></span>
                        </td>
                    </tr>
                </table>
            </div>
                <span id="areYouCurOnPaySpan" runat="server">
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AYCO")%></label>
                        <table class="style18">
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblMortCur" runat="server" CssClass="Dvradiolist" RepeatColumns="2"
                                        RepeatDirection="Horizontal" RepeatLayout="Table">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvrblMortCur"
                                        runat="server" ControlToValidate="rblMortCur" Display="Dynamic" EnableClientScript="true"
                                        SetFocusOnError="true" Text="!"></asp:RequiredFieldValidator></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|INHM")%></label>
                        <asp:TextBox ID="txtMortLate" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="CleanField(this)">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvtxtMortLate" runat="server" EnableClientScript="true"
                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMortLate" Text="!">
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator SetFocusOnError="true" ControlToValidate="txtMortLate" Operator="DataTypeCheck"
                            Display="Dynamic" Type="Integer" runat="server" ID="cvtxtMortLate">
                        </asp:CompareValidator>
                    </div>
                </span>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|HMDYC")%></label>
                    <asp:TextBox ID="txtMoneyAvail" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                        onblur="ReFormatCurrency(this)" Text="$0">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator class="validationMessage" ID="cvtxtMoneyAvail" runat="server" ControlToValidate="txtMoneyAvail"
                        Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WDYL")%></label>
                    <asp:TextBox ID="txtLoanDescDate" runat="server" MaxLength="20">
                    </asp:TextBox>
                    <%= Cccs.Credability.Website.App.Translate("Credability|UserProfile|DDMMYYYY")%><span
                        style="color: Red; font-size: large">*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="rfvLoanDescDate" runat="server" EnableClientScript="true"
                        ControlToValidate="txtLoanDescDate" Text="!" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="revtxtLoanDescDate" ControlToValidate="txtLoanDescDate"
                        ValidationExpression="^([1-9]|0[1-9]|1[012])[/]([1-9]|0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d$"
                        Text="!" Display="Dynamic" />
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|WWTO")%></label>
                    <asp:TextBox ID="txtLoanDesc" runat="server" TextMode="MultiLine" Width="200px" Rows="3">
                    </asp:TextBox>
                </div>
                <div class="dvrow" runat="server" id="dvAMM">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|AMM")%></label>
                    <table class="style18">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblAssistanceModify" runat="server" CssClass="Dvradiolist"
                                    RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvrblAssistanceModify"
                                    runat="server" ControlToValidate="rblAssistanceModify" Display="Dynamic" EnableClientScript="true"
                                    SetFocusOnError="true" Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow" runat="server" id="dvGLM">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|GLM")%></label>
                    <table class="style18">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblLoanModification" runat="server" CssClass="Dvradiolist"
                                    RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvrblLoanModification"
                                    runat="server" ControlToValidate="rblLoanModification" Display="Dynamic" EnableClientScript="true"
                                    SetFocusOnError="true" Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|PS")%></label>
                    <asp:DropDownList ID="ddlPropStat" runat="server" Style="width: 182px">
                    </asp:DropDownList>
                    <span class="requiredField">*</span>
                    <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true"
                        ControlToValidate="ddlPropStat" Text="!" InitialValue="0" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="dvrow">
                    <p class="col_title">
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SHEL")%>
                    </p>
                </div>
                <div class="dvrow">
                    <label>
                        <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DYHA")%></label>
                    <table class="style22">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblSecondLoan" runat="server" AutoPostBack="True" CssClass="Dvradiolist"
                                    OnSelectedIndexChanged="rblSecondLoan_SelectedIndexChanged" RepeatColumns="2"
                                    RepeatDirection="Horizontal">
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvSecondLoan"
                                    runat="server" ControlToValidate="rblSecondLoan" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dvrow">
               
                    <label>
                        <!--Are there any liens on the property-->
                          <%= Cccs.Credability.Website.App.Translate("HARPQuestion12")%>
                    </label>
                    <table class="style22">
                        <tr>
                            <td>
                                <asp:RadioButtonList ID="rblLiensOnProperty" runat="server" AutoPostBack="True" CssClass="Dvradiolist"
                                    RepeatColumns="2"
                                    RepeatDirection="Horizontal">
                                   
                                </asp:RadioButtonList>
                            </td>
                            <td>
                                <span style="color: Red; font-size: large">*<asp:RequiredFieldValidator class="validationMessage" ID="rfvLiensOnProperty"
                                    runat="server" ControlToValidate="rblLiensOnProperty" Display="Dynamic" EnableClientScript="true"
                                    Text="!"></asp:RequiredFieldValidator></span>
                            </td>
                        </tr>
                    </table>
                </div>

                <asp:Panel ID="pnlSeconLoanInfo" runat="server" Visible="false">
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|NOC")%></label>
                        <asp:DropDownList ID="ddlSecLoanServ" runat="server" AutoPostBack="True" Width="250"
                            OnSelectedIndexChanged="ddlSecLoanServ_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvSecLoanServ" runat="server" EnableClientScript="true"
                            Display="Dynamic" ControlToValidate="ddlSecLoanServ" Text="!" InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow" id="dvOtherCompanyName" runat="server" visible="false">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|OSN")%></label>
                        <asp:TextBox ID="txtSecLoanHold" runat="server" MaxLength="50" onfocus="SetUpField(this);">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                            Display="Dynamic" ControlToValidate="txtSecLoanHold" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LN")%></label>
                        <asp:TextBox ID="txtLoanNo2" runat="server" MaxLength="50" onfocus="SetUpField(this);">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                            Display="Dynamic" ControlToValidate="txtLoanNo2" Text="!">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LB")%></label>
                        <asp:TextBox ID="txtMtg2Amt" runat="server" MaxLength="7" onfocus="SetUpField(this);"
                            onblur="ReFormatCurrency(this)" Text="$0">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                            ControlToValidate="txtMtg2Amt" Text="!" Display="Dynamic" CssClass="error" InitialValue="$0">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator class="validationMessage" ID="rvtxtMtg2Amt" runat="server" ControlToValidate="txtMtg2Amt"
                            Display="Dynamic" Text="!" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" />
                    </div>
                    <div class="dvrow">
                        <label>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|LS")%></label>
                        <asp:TextBox ID="txtMtg2Stat" runat="server" onfocus="SetUpField(this);">
                        </asp:TextBox>
                        <span class="requiredField">*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                            ControlToValidate="txtMtg2Stat" Text="!" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                    <div class="dvrow">
                        <label>
                        </label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="clearboth">
        </div>
    </asp:Panel>
    <asp:Panel ID="dvHouseHoldRentSize" runat="server" CssClass="dvHouseHoldRentSize" >
        <div class="dvform2col dvformlblbig">
            <div class="dvform">
                <div class="dvrow">
                    <label>
                        <p>
                            <%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|TdNumInc")%>
                        </p>
                    </label>
                    <span>
                        <asp:TextBox ID="txtHouseholdSizeRent" runat="server" MaxLength="2">
                        </asp:TextBox>
                        <span style="color: Red; font-size: large">&nbsp;*</span>
                        <asp:RequiredFieldValidator class="validationMessage" ID="rfvtxtHouseholdSizeRent" runat="server" EnableClientScript="true"
                            ControlToValidate="txtHouseholdSizeRent" Text="!" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvHousholdSizeRent" runat="server" MinimumValue="1" Type="Integer"
                            MaximumValue="100" ControlToValidate="txtHouseholdSizeRent" Text="!" Display="Dynamic">
                        </asp:RangeValidator>
                    </span>
                </div>

                <Uc:AuthCode ID="UcAuthCode" runat="server" ChatCode="Session1"></Uc:AuthCode>

                <div class="clearboth">
                </div>
            </div>
            <div class="clearboth">
            </div>
        </div>
    </asp:Panel>
    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
            <asp:LinkButton ID="btnSubmit" class="save" Visible="false" runat="server" ToolTip="Submit"
                CausesValidation="false" OnClick="btnSaveAndExitAndSubmitToQueue_Click">      
      <span>
            <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SendToCounselor")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
            <asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
        </div>
        <div class="lnkbutton">
            <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"
                CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
        </div>
    </div>
</div>
