﻿using System;

namespace Cccs.Credability.Website.Controls.RecControls
{
	public partial class UCHud1 : System.Web.UI.UserControl
	{
		Int32 ClientNumber;

		protected void Page_Load(object sender, EventArgs e)
		{
			ClientNumber = SessionState.ClientNumber.Value;
			btnReturnToPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
			btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
			if(!IsPostBack)
			{
				InitializeTranslation();

				SessionState.UserPageArrivalTime = DateTime.Now;
				GetCommanOptions(ClientNumber);
			}
		}

		private void ValidationControlTranslation()
		{
			//rfvtxtAuthCodeInput.ErrorMessage = App.Translate("Credability|BadAuthCode");
			//DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
		}

		private void InitializeTranslation()
		{
			ValidationControlTranslation();
		}

		public void GetCommanOptions(Int32 ClientNumber)
		{
			CommonOptionBKCounseling CommonOptionBKCounselings = null;

			CommonOptionBKCounselings = App.Credability.CommonOptionBKCounselingContactDetailsGet(ClientNumber);

			if(CommonOptionBKCounselings != null)
			{
				//txtMortHolder.Text = CommonOptionBKCounselings.MortHolder.Clean();
				txtMortHolder.Text = (SessionState.CompanyName != null) ? SessionState.CompanyName : string.Empty;
				txtLoanNumber.Text = CommonOptionBKCounselings.LoanNumber.Clean();
				ShowMortType(CommonOptionBKCounselings.MortType);

				txtMortYears.Text = CommonOptionBKCounselings.MortYears.ToString("F0") + " years";
				txtMortDate.Text = CommonOptionBKCounselings.MortDate.Clean();
				txtMortRate.Text = (CommonOptionBKCounselings.MortRate / 100).ToString("P");
				ShowInterestType(CommonOptionBKCounselings.RateType);

				SetInterestRateText();

				txtOrigBal.Text = CommonOptionBKCounselings.OrigBal.ToString("C0");
				txtOweHome.Text = CommonOptionBKCounselings.OweHome.ToString("C0");
				txtValHome.Text = CommonOptionBKCounselings.ValHome.ToString("C0");
				txtMopmt.Text = CommonOptionBKCounselings.MoPmt.ToString("C0");
				txtAmtavail.Text = CommonOptionBKCounselings.AmtAvail.ToString("C0");
				txtLastcontactdate.Text = CommonOptionBKCounselings.LastContactDate.Clean();
				txtLastcontactdesc.Text = CommonOptionBKCounselings.LastContactDesc.Clean();
				txtRepayplan.Text = CommonOptionBKCounselings.RepayPlan.Clean();

				txtSecMort.Text = GetHasSecondaryServicerText(CommonOptionBKCounselings.SecondaryHolder);
				if(!string.IsNullOrEmpty(CommonOptionBKCounselings.SecondaryHolder.Clean()))
				{
					pnlScondaryMort.Visible = true;
					txtSecondaryAm.Text = CommonOptionBKCounselings.SecondaryAmt.ToString("C0");
					txtSecondarystatus.Text = CommonOptionBKCounselings.SecondaryStatus.Clean();
					txtSecondaryholder.Text = CommonOptionBKCounselings.SecondaryHolder.Clean();
				}
				else
				{
					pnlScondaryMort.Visible = false;
					txtSecondaryAm.Text = String.Empty;
					txtSecondarystatus.Text = String.Empty;
					txtSecondaryholder.Text = String.Empty;
				}
				txtProp4Sale.Text = CommonOptionBKCounselings.Prop4Sale.Clean();
				//Showpropert4Sale(CommonOptionBKCounselings.Prop4Sale);
				txtNote4Close.Text = CommonOptionBKCounselings.Note4Close.Clean();
				// ShowNotet4Close(CommonOptionBKCounselings.Note4Close);
				txtSizeofhousehold.Text = CommonOptionBKCounselings.SizeofHouseHold.ToString();
				ShowMortCurrent(CommonOptionBKCounselings.MortCurrent);
				txtMosdelinq.Text = CommonOptionBKCounselings.MosDelinq.ToString("F0");
				;
				//txtWhoInHouse.Text = CommonOptionBKCounselings.WhoInHouse.Clean();
				ShowWhoInHouse(CommonOptionBKCounselings.WhoInHouse);
			}

			CommonOptionBKCounseling CommonOptionExtraHudBKCounselings = null;
			CommonOptionExtraHudBKCounselings = App.Credability.CommonOptionBKCounselingExtraHudGet(ClientNumber);
			if(CommonOptionExtraHudBKCounselings != null)
			{
				txtGovGrant.Text = CommonOptionExtraHudBKCounselings.GovGrant.Clean();
				txtIncludeIns.Text = CommonOptionExtraHudBKCounselings.IncludeIns.Clean();
				//ShowincludeInc(CommonOptionExtraHudBKCounselings.IncludeIns);
				txtIncludeTax.Text = CommonOptionExtraHudBKCounselings.IncludeTax.Clean();
				// ShowIncludeTax(CommonOptionExtraHudBKCounselings.IncludeTax);
				txtStartIntRate.Text = (CommonOptionExtraHudBKCounselings.StartIntRate / 100).ToString("P");
				txtTopIntRate.Text = (CommonOptionExtraHudBKCounselings.TopIntRate / 100).ToString("P");
				//by ravi
				txtMortRate1.Text = (CommonOptionBKCounselings.MortRate / 100).ToString("P");
				//end ravi
				txtAnPropTax.Text = CommonOptionExtraHudBKCounselings.AnPropTax.ToString("C0");
				txtAnPropIns.Text = CommonOptionExtraHudBKCounselings.AnPropIns.ToString("C0");
			}
		}

		private string GetHasSecondaryServicerText(string value)
		{
			return string.IsNullOrEmpty(value.Clean()) ? Cccs.Credability.Website.App.Translate("No") : Cccs.Credability.Website.App.Translate("Yes");
		}

		private void ShowIncludeTax(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtIncludeTax.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case "N":
					txtIncludeTax.Text = Cccs.Credability.Website.App.Translate("No");
					break;

				default:
					txtIncludeTax.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowGovGrant(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtGovGrant.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case "N":
					txtGovGrant.Text = Cccs.Credability.Website.App.Translate("No");
					break;
				default:
					txtGovGrant.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowincludeInc(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtIncludeIns.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case "N":
					txtIncludeIns.Text = Cccs.Credability.Website.App.Translate("No");
					break;
				default:
					txtIncludeIns.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowNotet4Close(string value)
		{
			switch(value.Clean())
			{
				case "Y":
					txtNote4Close.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case "N":
					txtNote4Close.Text = Cccs.Credability.Website.App.Translate("No");
					break;
				default:
					txtNote4Close.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void Showpropert4Sale(string value)
		{
			switch(value.Trim())
			{
				case "Y":
					txtProp4Sale.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case "N":
					txtProp4Sale.Text = Cccs.Credability.Website.App.Translate("No");
					break;
				default:
					txtProp4Sale.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowWhoInHouse(string value)
		{
			switch(value.Clean())
			{
				case "01":
					txtWhoInHouse.Text = Cccs.Credability.Website.App.Translate("Owner Property Occupied");
					break;
				case "02":
					txtWhoInHouse.Text = Cccs.Credability.Website.App.Translate("Owner Property Vacant");
					break;
				case "03":
					txtWhoInHouse.Text = Cccs.Credability.Website.App.Translate("Investor Property Occupied");
					break;
				case "04":
					txtWhoInHouse.Text = Cccs.Credability.Website.App.Translate("Investor Property Vacant");
					break;
				default:
					txtWhoInHouse.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowMortCurrent(int value)
		{
			switch(value)
			{
				case 1:
					txtMortcurrent.Text = Cccs.Credability.Website.App.Translate("Yes");
					break;
				case 0:
					txtMortcurrent.Text = Cccs.Credability.Website.App.Translate("No");
					break;
				default:
					txtMortType.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowMortType(string value)
		{
			switch(value.Trim())
			{
				case "CONV":
					txtMortType.Text = Cccs.Credability.Website.App.Translate("Conventional");
					break;
				case "FHA":
					txtMortType.Text = Cccs.Credability.Website.App.Translate("FHA");
					break;
				case "VA":
					txtMortType.Text = Cccs.Credability.Website.App.Translate("VA");
					break;
				default:
					txtMortType.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		private void ShowInterestType(string value)
		{
			switch(value.Trim())
			{
				case "F":
					txtRateType.Text = Cccs.Credability.Website.App.Translate("Fixed");
					break;
				case "A":
					txtRateType.Text = Cccs.Credability.Website.App.Translate("Standard Arm");
					break;
				case "I":
					txtRateType.Text = Cccs.Credability.Website.App.Translate("Interest Only");
					break;
				case "O":
					txtRateType.Text = Cccs.Credability.Website.App.Translate("Option Arm");
					break;
				default:
					txtRateType.Text = Cccs.Credability.Website.App.Translate("None Selected");
					break;
			}
		}

		protected void click_btnReturnToPreviousPage(object sender, EventArgs e)
		{
			if(App.WebsiteCode == Cccs.Identity.Website.DMP)
			{
				Response.Redirect("DescribingYourSituation.aspx");
			}
			else
			{
				Response.Redirect("DescribeYourSituation.aspx");
			}
		}

		protected void click_btnContinue(object sender, EventArgs e)
		{
			if (Page.IsValid)
			{
				CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
				Response.Redirect("ListingYourDebts.aspx");
			}
			
		}

		protected void click_btnSaveExit(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}

		private void SetInterestRateText()
		{
			if(txtRateType.Text == "Fixed" || txtRateType.Text == "Interest Only")
			{
				pnlArmInrest.Visible = false;
				pnlFixed.Visible = true;
			}
			else
			{
				pnlArmInrest.Visible = true;
				pnlFixed.Visible = false;
			}
		}
	}
}