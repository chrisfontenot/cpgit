﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Credability.Website.Code;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.Navigation
{
    public abstract class BreadCrumbTemplate : System.Web.UI.UserControl
    {
        public enum Tab
        {
            TellUs,
            Budget,
            Debt,
            Summary,
            Profile,

        };
        protected List<NavigationEntity> NavigationList;

        protected override void OnLoad(EventArgs e)
        {
            if (!IsPostBack)
            {

                NavigationManager manager = new NavigationManager(NavigationList);
                NavigationEntity CurrentPage = manager.GetCurrentPage(Request.PhysicalPath);
                if (CurrentPage != null && App.WebsiteCode=="DMP")
                {
                    //  Label lblStep = ((Label)FindControl("lblStep"));

                    //  if (lblStep != null)
                    //  {
                    //  lblStep.Text = string.Format("Step {0} of {1}",
                    //   manager.GetCurrentStep(Request.PhysicalPath).ToString(), manager.GetNumberOfSteps());
                    //   }
                    List<NavigationEntity> list = manager.GetListOfActiveTabs(CurrentPage);



                    foreach (NavigationEntity entity in list)
                    {
                        HtmlAnchor href = FindControl(string.Format("a{0}", entity.tab)) as HtmlAnchor;
                        href.HRef = AppRelativePathReplace(entity.URL);
                        if (entity.tab!=null) ActiveTabSet(entity.tab);
                    }
                }
            }
            base.OnLoad(e);
        }


        private string AppRelativePathReplace(string url)
        {

            return VirtualPathUtility.GetDirectory(Request.AppRelativeCurrentExecutionFilePath) + url;
        }

        public void ActiveTabSet(Tab? tab)
        {
            HtmlAnchor href = FindControl(string.Format("a{0}", tab)) as HtmlAnchor;

            if (href != null)
            {
                if (href.Attributes["class"] == null || href.Attributes["class"].ToString().IndexOf("active") == -1)
                    href.Attributes["class"] += "active";
                Visible = true;
            }
        }
    }
}