﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Credability.Website.Controls;

namespace Cccs.Credability.Website.Controls.Navigation
{
    
    public class NavigationEntity
    {
   //     public string Title { get; set; }
        public string URL { get; set; }
        public int Order { get; set; }
   //     public bool Active { get; set; }
        public byte PercentComplete { get; set; }
        public BreadCrumbTemplate.Tab? tab { get; set; }
        public bool FirstPageInTab { get; set; }
    }

}