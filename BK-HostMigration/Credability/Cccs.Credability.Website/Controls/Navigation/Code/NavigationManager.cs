﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Cccs.Credability.Website.Controls.Navigation
{   
  

    public class NavigationManager
    {
        public NavigationManager(List<NavigationEntity> list)
        {
            NavigationList=list;

        }
        private List<NavigationEntity> NavigationList = new List<NavigationEntity>();

        public int GetCurrentStep(string url)
        {
            url = Path.GetFileName(url);
            int step = 0;
            try
            {
                step= NavigationList.FirstOrDefault(x => x.URL.Equals(url,StringComparison.CurrentCultureIgnoreCase)).Order;
            }
            catch {}
            return step;
        }
        public NavigationEntity GetCurrentPage(string url)
        {
            url = Path.GetFileName(url);
            NavigationEntity entity = null;
            try
            {
                entity = NavigationList.FirstOrDefault(x => x.URL.Equals(url, StringComparison.CurrentCultureIgnoreCase));
            }
            catch { }
            return entity;
          
        }
        public int GetNumberOfSteps()
        {
            return NavigationList.Where(x=>x.Order!=0).GroupBy(x => x.Order).Count();
        }

        public List<NavigationEntity> GetListOfActiveTabs(NavigationEntity currentPage)
        {
            var list = NavigationList.Where(x => x.PercentComplete <= currentPage.PercentComplete && x.FirstPageInTab == true && currentPage.PercentComplete!=100).ToList();
          
            return list;
        }
    }
}