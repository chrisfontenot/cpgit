﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCommonOptionsforCrisis.ascx.cs" Inherits="Cccs.Credability.Website.Controls.BchControls.UCCommonOptionsforCrisis" %>
<%@ Register Src="~/Controls/Shared/Components/UcAuthorizationCode.ascx" TagPrefix="Uc" TagName="AuthCode" %>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisBCH|CO")%></h1>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisBCH|IATP")%></p>
<div>
	<%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|ULCont")%>
</div>
<p>
	<%-- <%= Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|NYCW")%>
	link
	<%= Cccs.Credability.Website.CommonFunction.CreateChatLink(Cccs.Credability.Website.App.Translate("Credability|CommonOptionsforCrisisDMP|link")) %>--%></p>

<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000"  />
<Uc:AuthCode ID="UcAuthCode" runat="server" ValidationGroupSet=""></Uc:AuthCode>
<p>
	&nbsp;</p>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortPrevious" runat="server" CssClass="previous" OnClick="click_btnReverseMortgagePrevious"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortSaveandContinueLater" runat="server" OnClick="click_btnSaveandContinueLater"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnReverseMortContinue" runat="server" OnClick="btnReverseMortgageContinue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
