﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Cccs.Picklist;

namespace Cccs.Credability.Website.Controls.BchControls
{
    public partial class UCCommonOptionforDealing : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeTranslation();
                btnReverseMortPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                CommonFunction.UserProgressSave(95, SessionState.Username);
                DateTime Today = DateTime.Now.AddDays(10);
                SessionState.UserPageArrivalTime = DateTime.Now;

                CommonOptionforDealingBCH CommonOptionforDealingBCHs = null;
                CommonOptionforDealingBCHs = App.Credability.CommonOptionforDealingBCHGet(SessionState.ClientNumber.Value);
            }
        }

        private void InitializeTranslation()
        {
            btnSendtoCounselorforFinalReview.ToolTip = Cccs.Credability.Website.App.Translate("SENDCOUNSELORFOR");
            btnReverseMortPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
        }

        protected void click_btnReverseMortgagePrevious(object sender, EventArgs e)
        {
            Response.Redirect("CommonOptionsforCrisis.aspx");
        }

        protected void click_btnDone(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
        }
    }
}