﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BchControls
{
	public partial class UCHud2 : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            btnReturnToPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
            btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
			
            if (!IsPostBack)
			{
				SessionState.UserPageArrivalTime = DateTime.Now;
			}
		}

		protected void click_btnReturnToPreviousPage(object sender, EventArgs e)
		{
			Response.Redirect("UnderstandingYourNetWorth.aspx");
		}

		protected void click_btnContinue(object sender, EventArgs e)
		{
            if (Page.IsValid)
            {
                //if (CommonFunction.IsValidChatCode(ChatCodeOption.Session3, txtAuthCodeInput.Text))
                //{
                    CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
                    Response.Redirect("CommonOptionsforDealingwithaFinancialCrisis.aspx");
                //}
                //else
                //{
                //    dvErrorMessage.InnerHtml = App.Translate("Credability|BadAuthCode");
                //    dvErrorMessage.Visible = true;
                //}
            }
		}

		protected void click_btnSaveExit(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);

			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
	}
}