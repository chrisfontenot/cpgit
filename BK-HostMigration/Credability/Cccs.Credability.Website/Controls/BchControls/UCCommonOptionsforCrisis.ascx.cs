﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.BchControls
{
	public partial class UCCommonOptionsforCrisis : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            btnReverseMortPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
            btnReverseMortContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
            btnReverseMortSaveandContinueLater.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
            if (!IsPostBack)
			{
				SessionState.UserPageArrivalTime = DateTime.Now;
			}
		}

		protected void click_btnReverseMortgagePrevious(object sender, EventArgs e)
		{
			Response.Redirect("UnderstandingYourNetWorth.aspx");
		}

		protected void btnReverseMortgageContinue(object sender, EventArgs e)
		{
            if (Page.IsValid)
            {
                CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
                Response.Redirect("CommonOptionforDealing.aspx");
            }
            //if (CommonFunction.IsValidChatCode(ChatCodeOption.Session3, txtAuthCodeInput.Text))
            //{
            //    CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
            //    Response.Redirect("CommonOptionforDealing.aspx");
            //}
            //else
            //{
            //    dvErrorMessage.InnerHtml = "";
            //    dvErrorMessage.InnerHtml += "<b>Error ! </b>";
            //    dvErrorMessage.InnerHtml += "<ul><li>You need to enter an acceptable authorization code. Please ask your Chat Counselor for assistance if needed.</ul></li>";
            //    dvErrorMessage.Visible = true;
            //}
		}

		protected void click_btnSaveandContinueLater(object sender, EventArgs e)
		{
			CommonFunction.AddUpdateTimeStampvalue(Convert.ToInt32(SessionState.ClientNumber), string.Empty);
			Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
		}
	}
}