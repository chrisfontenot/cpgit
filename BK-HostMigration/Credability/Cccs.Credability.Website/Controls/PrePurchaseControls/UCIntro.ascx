﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCIntro.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchasing.Introduction" %>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center">
			<h1>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseIntro|TYFC")%></h1>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseIntro|BAHIABD")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseIntro|WACTP")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td width="30">
					</td>
					<td colspan="2" align="left">
						<b>
							<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseIntro|YWNTFTB")%></b>
					</td>
				</tr>
				<tr>
					<td width="30">
					</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseIntro|DOYMI")%>
								</td>
							</tr>
							<%--<tr>
                                <td align="center">
                                    <asp:Button ID="btnContinue" runat="server" Text="Continue" 
                                        onclick="btnContinue_Click" CssClass="Button" />
                                </td>
                            </tr>--%>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>