﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap3C.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap3C" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3C|CLT")%></h2>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3C|CLTAV")%>
			<a class="DefWord" onmouseover="TagToTip('FHA')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('VA1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('RHS')" onmouseout="UnTip()"></a>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3C|HYANLG")%>
			<a class="DefWord" onmouseover="TagToTip('ARM')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('Fix1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('Con1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('Rehab1')" onmouseout="UnTip()"></a>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3C|TBAYTT")%>
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>