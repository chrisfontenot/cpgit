﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.PrePurchasing
{
	public partial class UcMonthlyExpensesNew : CounselingUcBase
	{
		private static readonly string _CONFIRMED = "_CONFIRMED";

		private void ShowPageTitle()
		{
			Label lblHeading = new Label();
			lblHeading = Page.Master.FindControl("lblHeading") as Label;
			if(lblHeading != null)
				lblHeading.Text = Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonthlyEx|Entering_Your_Monthly_Expenses");
		}

		protected new void Page_Load(object sender, EventArgs e)
		{
			ShowPageTitle();
			if(!IsPostBack)
			{
				btnMonthlyPreviousPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
				btnMonthlyExpensesCon.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnMonthlySaveExitPage.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
				ValidationControlTranslation();
				CommonFunction.AddItems(ddlCarCurrent.Items, "CurrentOnAutoPayments", SessionState.LanguageCode);

				if(SessionState.ClientNumber != null)
				{
					FormDataSet();
				}
				else
					Response.Redirect("Introduction.aspx");

				//GetMonthlyExpensesByClientNumber(ClientNumber);
			}
			base.Page_Load(sender, e);
		}

		private void ValidationControlTranslation()
		{
			ddlCarCurrentRequired.ErrorMessage = App.Translate("Credability|MonthlyExpenses|PSIY");
		}

		public bool isNumeric(string val, System.Globalization.NumberStyles NumberStyle)
		{
			Double result;
			return Double.TryParse(val, NumberStyle,
				System.Globalization.CultureInfo.CurrentCulture, out result);
		}

		private void FormDataSet()
		{
			try
			{
				UserMonthlyExpenses MonthlyExpenses = App.Credability.UserMonthlyExpensesGet((int)SessionState.ClientNumber);
				UserMonthlyExpenses NewMonthleyExpenses = App.Credability.NewMonthlyExpensesPreHudGet((int)SessionState.ClientNumber);

				//For PreHud Table
				if(NewMonthleyExpenses != null)
				{
					txtRentMortNew.Text = NewMonthleyExpenses.RentMort.ToString(App.CurrencyFormat);
					txtMoEquityNew.Text = NewMonthleyExpenses.MoEquity.ToString(App.CurrencyFormat);
					txtMo2ndMortNew.Text = NewMonthleyExpenses.Mo2ndMort.ToString(App.CurrencyFormat);
					txtMoPropTaxNew.Text = NewMonthleyExpenses.MoPropTax.ToString(App.CurrencyFormat);
					txtMoPropInsNew.Text = NewMonthleyExpenses.MoPropIns.ToString(App.CurrencyFormat);
					txtMoFeeNew.Text = NewMonthleyExpenses.MoFee.ToString(App.CurrencyFormat);
					txtHomeMaintenanceNew.Text = NewMonthleyExpenses.HomeMaintenance.ToString(App.CurrencyFormat);
					txtUtlElectricNew.Text = NewMonthleyExpenses.UtlElectric.ToString(App.CurrencyFormat);
					txtUtlWaterNew.Text = NewMonthleyExpenses.UtlWater.ToString(App.CurrencyFormat);
					txtUtlGasNew.Text = NewMonthleyExpenses.UtlGas.ToString(App.CurrencyFormat);
					txtUtlTvNew.Text = NewMonthleyExpenses.UtlTv.ToString(App.CurrencyFormat);
					txtUtlTrashNew.Text = NewMonthleyExpenses.UtlTrash.ToString(App.CurrencyFormat);
					txtTelephoneNew.Text = NewMonthleyExpenses.Telephone.ToString(App.CurrencyFormat);
					txtFoodAwayNew.Text = NewMonthleyExpenses.FoodAway.ToString(App.CurrencyFormat);
					txtGroceriesNew.Text = NewMonthleyExpenses.Groceries.ToString(App.CurrencyFormat);

					txtCarPaymentsNew.Text = NewMonthleyExpenses.CarPayments.ToString(App.CurrencyFormat);
					txtCarInsuranceNew.Text = NewMonthleyExpenses.CarInsurance.ToString(App.CurrencyFormat);
					txtCarMaintenanceNew.Text = NewMonthleyExpenses.CarMaintenance.ToString(App.CurrencyFormat);
					txtPublicTransportationNew.Text = NewMonthleyExpenses.PublicTransportation.ToString(App.CurrencyFormat);

					txtInsuranceNew.Text = NewMonthleyExpenses.Insurance.ToString(App.CurrencyFormat);
					txtMedicalPrescriptionNew.Text = NewMonthleyExpenses.MedicalPrescription.ToString(App.CurrencyFormat);
					txtChildSupportAlimonyNew.Text = NewMonthleyExpenses.ChildSupportAlimony.ToString(App.CurrencyFormat);
					txtChildElderCareNew.Text = NewMonthleyExpenses.ChildElderCare.ToString(App.CurrencyFormat);
					txtEducationNew.Text = NewMonthleyExpenses.Education.ToString(App.CurrencyFormat);

					txtOtherLoansNew.Text = NewMonthleyExpenses.OtherLoans.ToString(App.CurrencyFormat);
					txtContributionsNew.Text = NewMonthleyExpenses.Contributions.ToString(App.CurrencyFormat);
					txtClothingNew.Text = NewMonthleyExpenses.Clothing.ToString(App.CurrencyFormat);
					txtLaundryNew.Text = NewMonthleyExpenses.Laundry.ToString(App.CurrencyFormat);
					txtPersonalExpensesNew.Text = NewMonthleyExpenses.PersonalExpenses.ToString(App.CurrencyFormat);
					txtBeautyBarberNew.Text = NewMonthleyExpenses.BeautyBarber.ToString(App.CurrencyFormat);
					txtRecreationNew.Text = NewMonthleyExpenses.Recreation.ToString(App.CurrencyFormat);
					txtClubDuesNew.Text = NewMonthleyExpenses.ClubDues.ToString(App.CurrencyFormat);
					txtGiftsNew.Text = NewMonthleyExpenses.Gifts.ToString(App.CurrencyFormat);
					txtMiscellaneousNew.Text = NewMonthleyExpenses.Miscellaneous.ToString(App.CurrencyFormat);
				}
				else
				{
					txtRentMortNew.Text = "$0";
					txtMoEquityNew.Text = "$0";
					txtMo2ndMortNew.Text = "$0";
					txtMoPropTaxNew.Text = "$0";
					txtMoPropInsNew.Text = "$0";
					txtMoFeeNew.Text = "$0";
					txtHomeMaintenanceNew.Text = "$0";
					txtUtlElectricNew.Text = "$0";
					txtUtlWaterNew.Text = "$0";
					txtUtlGasNew.Text = "$0";
					txtUtlTvNew.Text = "$0";
					txtUtlTrashNew.Text = "$0";
					txtTelephoneNew.Text = "$0";
					txtFoodAwayNew.Text = "$0";
					txtGroceriesNew.Text = "$0";

					txtCarPaymentsNew.Text = "$0";
					txtCarInsuranceNew.Text = "$0";
					txtCarMaintenanceNew.Text = "$0";
					txtPublicTransportationNew.Text = "$0";

					txtInsuranceNew.Text = "$0";
					txtMedicalPrescriptionNew.Text = "$0";
					txtChildSupportAlimonyNew.Text = "$0";
					txtChildElderCareNew.Text = "$0";
					txtEducationNew.Text = "$0";

					txtOtherLoansNew.Text = "$0";
					txtContributionsNew.Text = "$0";
					txtClothingNew.Text = "$0";
					txtLaundryNew.Text = "$0";
					txtPersonalExpensesNew.Text = "$0";
					txtBeautyBarberNew.Text = "$0";
					txtRecreationNew.Text = "$0";
					txtClubDuesNew.Text = "$0";
					txtGiftsNew.Text = "$0";
					txtMiscellaneousNew.Text = "$0";
				}

				//For ContactDetails Table
				if(MonthlyExpenses != null)
				{
					txtRentMort.Text = MonthlyExpenses.RentMort.ToString(App.CurrencyFormat);
					txtMoEquity.Text = MonthlyExpenses.MoEquity.ToString(App.CurrencyFormat);
					txtMo2ndMort.Text = MonthlyExpenses.Mo2ndMort.ToString(App.CurrencyFormat);
					txtMoPropTax.Text = MonthlyExpenses.MoPropTax.ToString(App.CurrencyFormat);
					txtMoPropIns.Text = MonthlyExpenses.MoPropIns.ToString(App.CurrencyFormat);
					txtMoFee.Text = MonthlyExpenses.MoFee.ToString(App.CurrencyFormat);
					txtHomeMaintenance.Text = MonthlyExpenses.HomeMaintenance.ToString(App.CurrencyFormat);
					txtUtlElectric.Text = MonthlyExpenses.UtlElectric.ToString(App.CurrencyFormat);
					txtUtlWater.Text = MonthlyExpenses.UtlWater.ToString(App.CurrencyFormat);
					txtUtlGas.Text = MonthlyExpenses.UtlGas.ToString(App.CurrencyFormat);
					txtUtlTv.Text = MonthlyExpenses.UtlTv.ToString(App.CurrencyFormat);
					txtUtlTrash.Text = MonthlyExpenses.UtlTrash.ToString(App.CurrencyFormat);
					txtTelephone.Text = MonthlyExpenses.Telephone.ToString(App.CurrencyFormat);
					txtFoodAway.Text = MonthlyExpenses.FoodAway.ToString(App.CurrencyFormat);
					txtGroceries.Text = MonthlyExpenses.Groceries.ToString(App.CurrencyFormat);

					ddlCarCurrent.ClearSelection();
					ddlCarCurrent.SelectedValue = MonthlyExpenses.CarCurrent.ToString();
					txtCarPayments.Text = MonthlyExpenses.CarPayments.ToString(App.CurrencyFormat);
					txtCarInsurance.Text = MonthlyExpenses.CarInsurance.ToString(App.CurrencyFormat);
					txtCarMaintenance.Text = MonthlyExpenses.CarMaintenance.ToString(App.CurrencyFormat);
					txtPublicTransportation.Text = MonthlyExpenses.PublicTransportation.ToString(App.CurrencyFormat);

					txtInsurance.Text = MonthlyExpenses.Insurance.ToString(App.CurrencyFormat);
					txtMedicalPrescription.Text = MonthlyExpenses.MedicalPrescription.ToString(App.CurrencyFormat);
					txtChildSupportAlimony.Text = MonthlyExpenses.ChildSupportAlimony.ToString(App.CurrencyFormat);
					txtChildElderCare.Text = MonthlyExpenses.ChildElderCare.ToString(App.CurrencyFormat);
					txtEducation.Text = MonthlyExpenses.Education.ToString(App.CurrencyFormat);

					txtOtherLoans.Text = MonthlyExpenses.OtherLoans.ToString(App.CurrencyFormat);
					txtContributions.Text = MonthlyExpenses.Contributions.ToString(App.CurrencyFormat);
					txtClothing.Text = MonthlyExpenses.Clothing.ToString(App.CurrencyFormat);
					txtLaundry.Text = MonthlyExpenses.Laundry.ToString(App.CurrencyFormat);
					txtPersonalExpenses.Text = MonthlyExpenses.PersonalExpenses.ToString(App.CurrencyFormat);
					txtBeautyBarber.Text = MonthlyExpenses.BeautyBarber.ToString(App.CurrencyFormat);
					txtRecreation.Text = MonthlyExpenses.Recreation.ToString(App.CurrencyFormat);
					txtClubDues.Text = MonthlyExpenses.ClubDues.ToString(App.CurrencyFormat);
					txtGifts.Text = MonthlyExpenses.Gifts.ToString(App.CurrencyFormat);
					txtMiscellaneous.Text = MonthlyExpenses.Miscellaneous.ToString(App.CurrencyFormat);
				}
				else
				{
					txtRentMort.Text = "$0";
					txtMoEquity.Text = "$0";
					txtMo2ndMort.Text = "$0";
					txtMoPropTax.Text = "$0";
					txtMoPropIns.Text = "$0";
					txtMoFee.Text = "$0";
					txtHomeMaintenance.Text = "$0";
					txtUtlElectric.Text = "$0";
					txtUtlWater.Text = "$0";
					txtUtlGas.Text = "$0";
					txtUtlTv.Text = "$0";
					txtUtlTrash.Text = "$0";
					txtTelephone.Text = "$0";
					txtFoodAway.Text = "$0";
					txtGroceries.Text = "$0";

					txtCarPayments.Text = "$0";
					txtCarInsurance.Text = "$0";
					txtCarMaintenance.Text = "$0";
					txtPublicTransportation.Text = "$0";

					txtInsurance.Text = "$0";
					txtMedicalPrescription.Text = "$0";
					txtChildSupportAlimony.Text = "$0";
					txtChildElderCare.Text = "$0";
					txtEducation.Text = "$0";

					txtOtherLoans.Text = "$0";
					txtContributions.Text = "$0";
					txtClothing.Text = "$0";
					txtLaundry.Text = "$0";
					txtPersonalExpenses.Text = "$0";
					txtBeautyBarber.Text = "$0";
					txtRecreation.Text = "$0";
					txtClubDues.Text = "$0";
					txtGifts.Text = "$0";
					txtMiscellaneous.Text = "$0";
				}
			}
			catch(Exception ex)
			{
				dvErrorMessage.InnerHtml = ex.ToString();
			}
		}

		private bool SaveMonthlyExpenses()
		{
			bool errorFlag = true;
			//Update The Information
			UserReferenceDetailsResults NewSaveResult = App.Credability.NewMonthlyExpensesPreHudAddUpdate(FormDataGetNewMonthlyExpenses());
			if(!NewSaveResult.IsSuccessful)
			{
				errorFlag = false;
				dvErrorMessage.InnerHtml = NewSaveResult.Exception.ToString();
			}

			UserMonthlyExpensesResult OldSaveResult = App.Credability.UserMonthlyExpensesUpdate(FormDataGetOldMonthlyExpenses());
			if(!OldSaveResult.IsSuccessful)
			{
				errorFlag = false;
				dvErrorMessage.InnerHtml += OldSaveResult.Exception.ToString();
			}
			return errorFlag;
		}

		public void EnableDisableAutoPayments()
		{
			if(ddlCarCurrent.SelectedValue == "3")
			{
				txtCarPayments.Enabled = false;
				txtCarPayments.Text = "$0";
			}
			else
			{
				txtCarPayments.Enabled = true;
			}
		}

		protected void btnMonthlyPreviousPage_Click(object sender, EventArgs e)
		{
			SaveMonthlyExpenses();
			NavigatePrevious();
		}

		protected void btnMonthlyExpensesCon_Click(object sender, EventArgs e)
		{
			if(ConfirmZerosForContinue())
			{
				if(SaveMonthlyExpenses())
				{
					NavigateContinue();
				}
				else
				{
					dvErrorMessage.Visible = true;
				}
			}
		}

		protected void btnMonthlySaveExitPage_Click(object sender, EventArgs e)
		{
			if(SaveMonthlyExpenses())
			{
				NavigateExit();
			}
			else
			{
				dvErrorMessage.Visible = true;
			}
		}

		protected void ddlCarCurrent_SelectedIndexChanged(object sender, EventArgs e)
		{
			EnableDisableAutoPayments();
			//PayAutoCur.Focus();
		}

		private bool ConfirmZerosForContinue()
		{
			return !RequireRegisterConfirmZerosScript(btnMonthlyExpensesCon.UniqueID);
		}

		private bool RequireRegisterConfirmZerosScript(string ctrlName)
		{
			bool ret = false;
			string[] vals = Request.Params.GetValues(_CONFIRMED);
			// Check if the hidden value has been set
			if(vals == null || vals.Length == 0 || string.IsNullOrEmpty(vals[0]))
			{
				// if not, build the validation dialog.
				string expText = BuildConfirmZerosText(FormDataGetNewMonthlyExpenses());
				if(!string.IsNullOrEmpty(expText))
				{
					//string script = string.Format(_ScriptTemplate, expText, ctrlName);
					string script = "if(confirm('" + Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|SHE") + "\\r\\n" +
									expText +
									"\\r\\n\\r\\n" + Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|IT") + "')){window.setTimeout(\"document.getElementById('" +
									_CONFIRMED +
									"').value = 'Y';__doPostBack('" +
									ctrlName +
									"','');\", 1)}";
					ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ConfirmZeroScript", script, true);
					ret = true;
				}
			}
			return ret;
		}

		private string BuildConfirmZerosText(UserMonthlyExpenses NewMonthlyExpenses)
		{
			StringBuilder builder = new StringBuilder();

			if(NewMonthlyExpenses.RentMort == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ROMP"));
			if(NewMonthlyExpenses.MoEquity == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MELoan"));
			if(NewMonthlyExpenses.Mo2ndMort == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|M2M"));
			if(NewMonthlyExpenses.MoPropTax == 0)
				AppendListItem(builder, App.Translate("Credability|PrePurchaseMonExp|PT"));
			//MoPropIns is missing
			if(NewMonthlyExpenses.MoFee == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MAF"));
			if(NewMonthlyExpenses.HomeMaintenance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|HOM"));
			if(NewMonthlyExpenses.UtlElectric == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Ele"));
			if(NewMonthlyExpenses.UtlWater == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|WS"));
			if(NewMonthlyExpenses.UtlGas == 0)
				AppendListItem(builder, App.Translate("Credability|PrePurchaseMonExp|NGOFO"));
			if(NewMonthlyExpenses.UtlTv == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CS"));
			if(NewMonthlyExpenses.UtlTrash == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|TC"));
			if(NewMonthlyExpenses.Telephone == 0)
				AppendListItem(builder, "Telephone");
			if(NewMonthlyExpenses.FoodAway == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|FAFH"));
			if(NewMonthlyExpenses.Groceries == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|FAH"));

			if(txtCarPaymentsNew.Enabled)
			{
				if(NewMonthlyExpenses.CarPayments == 0)
					AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|AP"));
			}
			if(NewMonthlyExpenses.CarInsurance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|AI"));
			if(NewMonthlyExpenses.CarMaintenance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|GAM"));
			if(NewMonthlyExpenses.PublicTransportation == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|PTranp"));

			if(NewMonthlyExpenses.Insurance == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ILMI"));
			if(NewMonthlyExpenses.MedicalPrescription == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|MVAP"));
			if(NewMonthlyExpenses.ChildSupportAlimony == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CSA"));
			if(NewMonthlyExpenses.ChildElderCare == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CCEC"));
			if(NewMonthlyExpenses.Education == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|ERE"));

			if(NewMonthlyExpenses.OtherLoans == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|TPOO"));
			if(NewMonthlyExpenses.Contributions == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CO"));
			if(NewMonthlyExpenses.Clothing == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Clothing"));
			if(NewMonthlyExpenses.Laundry == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|LDC"));
			if(NewMonthlyExpenses.PersonalExpenses == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|PEE"));
			if(NewMonthlyExpenses.BeautyBarber == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|BBS"));
			if(NewMonthlyExpenses.Recreation == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|RE"));
			if(NewMonthlyExpenses.ClubDues == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|CUD"));
			if(NewMonthlyExpenses.Gifts == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Gifts"));
			if(NewMonthlyExpenses.Miscellaneous == 0)
				AppendListItem(builder, App.Translate("Credability|MonthlyExpensesBCH|Miscellaneous"));

			return builder.ToString();
		}

		public UserMonthlyExpenses FormDataGetNewMonthlyExpenses()
		{
			UserMonthlyExpenses NewMonthlyExpenses = new UserMonthlyExpenses();
			NewMonthlyExpenses.ClientNumber = SessionState.ClientNumber.Value;

			NewMonthlyExpenses.RentMort = txtRentMortNew.Text.ToDecimal(0);
			NewMonthlyExpenses.MoEquity = txtMoEquityNew.Text.ToFloat(0);
			NewMonthlyExpenses.Mo2ndMort = txtMo2ndMortNew.Text.ToFloat(0);
			NewMonthlyExpenses.MoPropTax = txtMoPropTaxNew.Text.ToFloat(0);
			NewMonthlyExpenses.MoPropIns = txtMoPropInsNew.Text.ToFloat(0);
			NewMonthlyExpenses.MoFee = txtMoFeeNew.Text.ToFloat(0);
			NewMonthlyExpenses.HomeMaintenance = txtHomeMaintenanceNew.Text.ToDecimal(0);
			NewMonthlyExpenses.UtlElectric = txtUtlElectricNew.Text.ToFloat(0);
			NewMonthlyExpenses.UtlWater = txtUtlWaterNew.Text.ToFloat(0);
			NewMonthlyExpenses.UtlGas = txtUtlGasNew.Text.ToFloat(0);
			NewMonthlyExpenses.UtlTv = txtUtlTvNew.Text.ToFloat(0);
			NewMonthlyExpenses.UtlTrash = txtUtlTrashNew.Text.ToFloat(0);
			NewMonthlyExpenses.Telephone = txtTelephoneNew.Text.ToDecimal(0);
			NewMonthlyExpenses.FoodAway = txtFoodAwayNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Groceries = txtGroceriesNew.Text.ToDecimal(0);

			NewMonthlyExpenses.CarPayments = txtCarPaymentsNew.Text.ToDecimal(0);
			NewMonthlyExpenses.CarInsurance = txtCarInsuranceNew.Text.ToDecimal(0);
			NewMonthlyExpenses.CarMaintenance = txtCarMaintenanceNew.Text.ToDecimal(0);
			NewMonthlyExpenses.PublicTransportation = txtPublicTransportationNew.Text.ToDecimal(0);

			NewMonthlyExpenses.Insurance = txtInsuranceNew.Text.ToDecimal(0);
			NewMonthlyExpenses.MedicalPrescription = txtMedicalPrescriptionNew.Text.ToDecimal(0);
			NewMonthlyExpenses.ChildSupportAlimony = txtChildSupportAlimonyNew.Text.ToDecimal(0);
			NewMonthlyExpenses.ChildElderCare = txtChildElderCareNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Education = txtEducationNew.Text.ToDecimal(0);

			NewMonthlyExpenses.OtherLoans = txtOtherLoansNew.Text.ToInt(0);
			NewMonthlyExpenses.Contributions = txtContributionsNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Clothing = txtClothingNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Laundry = txtLaundryNew.Text.ToDecimal(0);
			NewMonthlyExpenses.PersonalExpenses = txtPersonalExpensesNew.Text.ToDecimal(0);
			NewMonthlyExpenses.BeautyBarber = txtBeautyBarberNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Recreation = txtRecreationNew.Text.ToDecimal(0);
			NewMonthlyExpenses.ClubDues = txtClubDuesNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Gifts = txtGiftsNew.Text.ToDecimal(0);
			NewMonthlyExpenses.Miscellaneous = txtMiscellaneousNew.Text.ToDecimal(0);

			return NewMonthlyExpenses;
		}

		public UserMonthlyExpenses FormDataGetOldMonthlyExpenses()
		{
			UserMonthlyExpenses OldMonthlyExpenses = new UserMonthlyExpenses();
			OldMonthlyExpenses.ClientNumber = SessionState.ClientNumber.Value;

			OldMonthlyExpenses.RentMort = txtRentMort.Text.ToDecimal(0);
			OldMonthlyExpenses.MoEquity = txtMoEquity.Text.ToFloat(0);
			OldMonthlyExpenses.Mo2ndMort = txtMo2ndMort.Text.ToFloat(0);
			OldMonthlyExpenses.MoPropTax = txtMoPropTax.Text.ToFloat(0);
			OldMonthlyExpenses.MoPropIns = txtMoPropIns.Text.ToFloat(0);
			OldMonthlyExpenses.MoFee = txtMoFee.Text.ToFloat(0);
			OldMonthlyExpenses.HomeMaintenance = txtHomeMaintenance.Text.ToDecimal(0);
			OldMonthlyExpenses.UtlElectric = txtUtlElectric.Text.ToFloat(0);
			OldMonthlyExpenses.UtlWater = txtUtlWater.Text.ToFloat(0);
			OldMonthlyExpenses.UtlGas = txtUtlGas.Text.ToFloat(0);
			OldMonthlyExpenses.UtlTv = txtUtlTv.Text.ToFloat(0);
			OldMonthlyExpenses.UtlTrash = txtUtlTrash.Text.ToFloat(0);
			OldMonthlyExpenses.Telephone = txtTelephone.Text.ToDecimal(0);
			OldMonthlyExpenses.FoodAway = txtFoodAway.Text.ToDecimal(0);
			OldMonthlyExpenses.Groceries = txtGroceries.Text.ToDecimal(0);

			OldMonthlyExpenses.CarCurrent = ddlCarCurrent.SelectedValue.ToInt(0);
			OldMonthlyExpenses.CarPayments = txtCarPayments.Text.ToDecimal(0);
			OldMonthlyExpenses.CarInsurance = txtCarInsurance.Text.ToDecimal(0);
			OldMonthlyExpenses.CarMaintenance = txtCarMaintenance.Text.ToDecimal(0);
			OldMonthlyExpenses.PublicTransportation = txtPublicTransportation.Text.ToDecimal(0);

			OldMonthlyExpenses.Insurance = txtInsurance.Text.ToDecimal(0);
			OldMonthlyExpenses.MedicalPrescription = txtMedicalPrescription.Text.ToDecimal(0);
			OldMonthlyExpenses.ChildSupportAlimony = txtChildSupportAlimony.Text.ToDecimal(0);
			OldMonthlyExpenses.ChildElderCare = txtChildElderCare.Text.ToDecimal(0);
			OldMonthlyExpenses.Education = txtEducation.Text.ToDecimal(0);

			OldMonthlyExpenses.OtherLoans = txtOtherLoans.Text.ToInt(0);
			OldMonthlyExpenses.Contributions = txtContributions.Text.ToDecimal(0);
			OldMonthlyExpenses.Clothing = txtClothing.Text.ToDecimal(0);
			OldMonthlyExpenses.Laundry = txtLaundry.Text.ToDecimal(0);
			OldMonthlyExpenses.PersonalExpenses = txtPersonalExpenses.Text.ToDecimal(0);
			OldMonthlyExpenses.BeautyBarber = txtBeautyBarber.Text.ToDecimal(0);
			OldMonthlyExpenses.Recreation = txtRecreation.Text.ToDecimal(0);
			OldMonthlyExpenses.ClubDues = txtClubDues.Text.ToDecimal(0);
			OldMonthlyExpenses.Gifts = txtGifts.Text.ToDecimal(0);
			OldMonthlyExpenses.Miscellaneous = txtMiscellaneous.Text.ToDecimal(0);

			return OldMonthlyExpenses;
		}

		private void AppendListItem(StringBuilder builder, string item)
		{
			builder.Append("\\r\\n    ").Append(item);
			//builder.Append(item);
		}
	}
}