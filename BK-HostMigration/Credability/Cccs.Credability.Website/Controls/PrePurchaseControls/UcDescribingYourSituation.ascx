﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDescribingYourSituation.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchasing.UcDescribingYourSituation" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|DescribeYourSituationBCH|DUS")%>&nbsp;&nbsp;</h1>
<div id="dvErrorMessage" runat="server" style="color: Red; font-weight: bold; text-align: center;">
</div>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="You must enter a value in the following fields:" DisplayMode="BulletList" EnableClientScript="true" runat="server" ValidationGroup="user" />
<div class="DvErrorSummary" id="dvServerSideValidation" runat="server" visible="false">
</div>
<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IOTBAS")%>
<p class="col_title">
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IOTB")%>
	<font color="red">*</font>
</p>
<div class="dvform2col">
	<div class="colformlft">
		<div class="dvform">
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<table class="DvradiolistAuto" cellpadding="5">
					<tr>
						<td>
							<asp:CheckBox ID="chkAtBeg" runat="server"></asp:CheckBox><label for="<%=chkAtBeg.ClientID %>">
								<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IAATVBOTP")%></label>
						</td>
					</tr>
					<tr>
						<td>
							<asp:CheckBox ID="chkDnPayAst" runat="server"></asp:CheckBox><label for="<%=chkDnPayAst.ClientID%>"><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IHAFDPA")%></label>
						</td>
					</tr>
					<tr>
						<td>
							<asp:CheckBox ID="chkPreAprv" runat="server"></asp:CheckBox><label for="<%=chkPreAprv.ClientID %>">
								<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IHABPA")%>
							</label>
						</td>
					</tr>
					<tr>
						<td>
							<asp:CheckBox ID="chkCloDate" runat="server" ValidationGroup="user"></asp:CheckBox><label for="<%=chkCloDate.ClientID %>">
								<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|IAHACDO")%></label><br />
							<asp:TextBox ID="txtDateOf" runat="server" MaxLength="10" CssClass="txtBox" Width="150px" ValidationGroup="user"></asp:TextBox><br />
							&nbsp;<asp:CustomValidator class="validationMessage" ID="txtDateOfCustom" runat="server" Text="!" ControlToValidate="txtDateOf" ValidationGroup="user" OnServerValidate="txtDateOfCustom_ServerValidate" ValidateEmptyText="true"></asp:CustomValidator>
						</td>
					</tr>
				</table>
			</div>
			<div class="dvrow">
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|POOTF")%>
                <span class="requiredField">*</span>
				<asp:RequiredFieldValidator class="validationMessage" ID="rbtPreHomeRequired" runat="server" ValidationGroup="user" Text="!" ControlToValidate="rbtPreHome" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
			</div>
			<div class="dvrow">
				<label>
					&nbsp;</label>
				<asp:RadioButtonList ID="rbtPreHome" runat="server" CssClass="DvradiolistAuto" CellPadding="5">
				</asp:RadioButtonList>
			</div>
			<div class="dvrow">
				<label>
					<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|HYALAH")%></label>
				<asp:DropDownList ID="ddlFoundHome" runat="server" OnSelectedIndexChanged="ddlFoundHome_SelectedIndexChanged" AutoPostBack="True">
				</asp:DropDownList>
				<span class="requiredField">*</span>
				<asp:RequiredFieldValidator class="validationMessage" ID="ddlFoundHomeRequired" runat="server" ControlToValidate="ddlFoundHome" Display="Dynamic" ValidationGroup="user" Text="!" SetFocusOnError="True"></asp:RequiredFieldValidator>
			</div>
			<asp:Panel ID="dvShowNewHome" runat="server" Visible="false">
				<p class="col_title">
					<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|PTUAI")%>
				</p>
				<div class="dvrow">
					<label>
						<%=Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|SA")%>
					</label>
					<asp:TextBox ID="txtNewAdr" MaxLength="75" runat="server"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:RequiredFieldValidator class="validationMessage" ID="txtNewAdrRequired" runat="server" ControlToValidate="txtNewAdr" Display="Dynamic" ValidationGroup="user" Text="!" SetFocusOnError="True"></asp:RequiredFieldValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|CI")%></label>
					<asp:TextBox ID="txtNewCity" MaxLength="25" runat="server"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:RequiredFieldValidator class="validationMessage" ID="txtNewCityRequired" runat="server" ControlToValidate="txtNewCity" Display="Dynamic" ValidationGroup="user" Text="!" SetFocusOnError="True"></asp:RequiredFieldValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|ST")%></label>
					<asp:DropDownList ID="ddlStates" runat="server">
						<asp:ListItem Value="">-- Select --</asp:ListItem>
						<asp:ListItem Value="AA">AA</asp:ListItem>
						<asp:ListItem Value="AE">AE</asp:ListItem>
						<asp:ListItem Value="AK">AK</asp:ListItem>
						<asp:ListItem Value="AL">AL</asp:ListItem>
						<asp:ListItem Value="AP">AP</asp:ListItem>
						<asp:ListItem Value="AR">AR</asp:ListItem>
						<asp:ListItem Value="AS">AS</asp:ListItem>
						<asp:ListItem Value="AZ">AZ</asp:ListItem>
						<asp:ListItem Value="CA">CA</asp:ListItem>
						<asp:ListItem Value="CO">CO</asp:ListItem>
						<asp:ListItem Value="CT">CT</asp:ListItem>
						<asp:ListItem Value="DC">DC</asp:ListItem>
						<asp:ListItem Value="DE">DE</asp:ListItem>
						<asp:ListItem Value="FL">FL</asp:ListItem>
						<asp:ListItem Value="FM">FM</asp:ListItem>
						<asp:ListItem Value="GA">GA</asp:ListItem>
						<asp:ListItem Value="GU">GU</asp:ListItem>
						<asp:ListItem Value="HI">HI</asp:ListItem>
						<asp:ListItem Value="IA">IA</asp:ListItem>
						<asp:ListItem Value="ID">ID</asp:ListItem>
						<asp:ListItem Value="IL">IL</asp:ListItem>
						<asp:ListItem Value="IN">IN</asp:ListItem>
						<asp:ListItem Value="KS">KS</asp:ListItem>
						<asp:ListItem Value="KY">KY</asp:ListItem>
						<asp:ListItem Value="LA">LA</asp:ListItem>
						<asp:ListItem Value="MA">MA</asp:ListItem>
						<asp:ListItem Value="MD">MD</asp:ListItem>
						<asp:ListItem Value="ME">ME</asp:ListItem>
						<asp:ListItem Value="MH">MH</asp:ListItem>
						<asp:ListItem Value="MI">MI</asp:ListItem>
						<asp:ListItem Value="MN">MN</asp:ListItem>
						<asp:ListItem Value="MO">MO</asp:ListItem>
						<asp:ListItem Value="MP">MP</asp:ListItem>
						<asp:ListItem Value="MS">MS</asp:ListItem>
						<asp:ListItem Value="MT">MT</asp:ListItem>
						<asp:ListItem Value="NC">NC</asp:ListItem>
						<asp:ListItem Value="ND">ND</asp:ListItem>
						<asp:ListItem Value="NE">NE</asp:ListItem>
						<asp:ListItem Value="NH">NH</asp:ListItem>
						<asp:ListItem Value="NJ">NJ</asp:ListItem>
						<asp:ListItem Value="NM">NM</asp:ListItem>
						<asp:ListItem Value="NV">NV</asp:ListItem>
						<asp:ListItem Value="NY">NY</asp:ListItem>
						<asp:ListItem Value="OH">OH</asp:ListItem>
						<asp:ListItem Value="OK">OK</asp:ListItem>
						<asp:ListItem Value="OR">OR</asp:ListItem>
						<asp:ListItem Value="PA">PA</asp:ListItem>
						<asp:ListItem Value="PR">PR</asp:ListItem>
						<asp:ListItem Value="PW">PW</asp:ListItem>
						<asp:ListItem Value="RI">RI</asp:ListItem>
						<asp:ListItem Value="SC">SC</asp:ListItem>
						<asp:ListItem Value="SD">SD</asp:ListItem>
						<asp:ListItem Value="TN">TN</asp:ListItem>
						<asp:ListItem Value="TX">TX</asp:ListItem>
						<asp:ListItem Value="UT">UT</asp:ListItem>
						<asp:ListItem Value="VA">VA</asp:ListItem>
						<asp:ListItem Value="VI">VI</asp:ListItem>
						<asp:ListItem Value="VT">VT</asp:ListItem>
						<asp:ListItem Value="WA">WA</asp:ListItem>
						<asp:ListItem Value="WI">WI</asp:ListItem>
						<asp:ListItem Value="WV">WV</asp:ListItem>
						<asp:ListItem Value="WY">WY</asp:ListItem>
					</asp:DropDownList>
					<span class="requiredField">*</span>
					<asp:RequiredFieldValidator class="validationMessage" ID="ddlStatesRequired" runat="server" ControlToValidate="ddlStates" Display="Dynamic" ValidationGroup="user" Text="!" SetFocusOnError="True"></asp:RequiredFieldValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|ZC")%>
					</label>
					<asp:TextBox ID="txtNewZip" MaxLength="5" runat="server"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:RequiredFieldValidator class="validationMessage" ID="txtNewZipRequired" runat="server" ControlToValidate="txtNewZip" Display="Dynamic" ValidationGroup="user" Text="!" SetFocusOnError="True"></asp:RequiredFieldValidator>
					<asp:RegularExpressionValidator class="validationMessage" runat="server" ID="rfvtxtZip" SetFocusOnError="true" ControlToValidate="txtNewZip" ValidationExpression="^[0-9]{5}$" Display="Dynamic" ValidationGroup="user" Text="!" />
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDesYourSit|EPP")%></label>
					<asp:TextBox ID="txtPriceEst" MaxLength="9" runat="server" onBlur="ReFormatCurrency(this);" onFocus="SetUpField(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtPriceEstCustom" runat="server" Text="!" ControlToValidate="txtPriceEst" ValidationGroup="user" OnServerValidate="txtPriceEstCustom_ServerValidate"></asp:CustomValidator>
				</div>
			</asp:Panel>
		</div>
	</div>
	<div class="clearboth">
	</div>
</div>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%=Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" ValidationGroup="user" OnClick="btnSaveAndExit_Click">
      <span>
        <%=Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" ValidationGroup="user" CausesValidation="true">
      <span>
        <%=Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
