﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap3E.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap3E" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3E|UW")%></h2>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3E|TPOTU")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3E|DTAV")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="MedTxtB">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>