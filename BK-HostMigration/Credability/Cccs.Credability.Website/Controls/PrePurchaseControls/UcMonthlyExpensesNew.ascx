﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcMonthlyExpensesNew.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchasing.UcMonthlyExpensesNew" %>
<script type="text/javascript" src="/Content/ErrorHandling.js"></script>
<script type="text/javascript">
	function CheckMaxNumberLarge(sender, args)
	{
		CheckMaxNumber(args, 9999);
	}

	function CheckMaxNumberSmall(sender, args)
	{
		CheckMaxNumber(args, 999);
	}

	function CheckMaxNumber(args, MaxValue)
	{
		var CurValue = String(args.Value)
		if (parseInt(stripCharsNotInBag(CurValue, NoChr)) > MaxValue)
		{
			args.IsValid = confirm('<%= Cccs.Credability.Website.App.Translate("Credability|MonthlyExpenses|PleaseConfirm")%>' + CurValue + "?")
		}
	}
</script>
<asp:ValidationSummary ID="DesValidationSummary" CssClass="DvErrorSummary" HeaderText="" DisplayMode="BulletList" EnableClientScript="true" runat="server" ForeColor="#A50000" ValidationGroup="userprofile" />
<div id="dvErrorMessage" class="DvErrorSummary" runat="server" visible="false">
</div>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|AANH")%></p>
<%--Monthly Food & Shelter Starts--%>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MFS")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<%--Left Col Start--%>
		<div class="dvform">
			<div class="colformlft">
				<div class="dvrow">
					<label>
						&nbsp;</label>
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CE")%></p>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ROMP")%></label>
					<asp:TextBox ID="txtRentMort" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtRentMortCustom" runat="server" ControlToValidate="txtRentMort" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|EL")%></label>
					<asp:TextBox ID="txtMoEquity" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoEquityCustom" runat="server" ControlToValidate="txtMoEquity" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|2M")%></label>
					<asp:TextBox ID="txtMo2ndMort" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMo2ndMortCustom" runat="server" ControlToValidate="txtMo2ndMort" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|PT")%></label>
					<asp:TextBox ID="txtMoPropTax" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoPropTaxCustom" runat="server" ControlToValidate="txtMoPropTax" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|HOI")%></label>
					<asp:TextBox ID="txtMoPropIns" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoPropInsCustom" runat="server" ControlToValidate="txtMoPropIns" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|HAF")%></label>
					<asp:TextBox ID="txtMoFee" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoFeeCustom" runat="server" ControlToValidate="txtMoFee" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|HMPC")%>
					</label>
					<asp:TextBox ID="txtHomeMaintenance" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtHomeMaintenanceCustom" runat="server" ControlToValidate="txtHomeMaintenance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ELE")%></label>
					<asp:TextBox ID="txtUtlElectric" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlElectricCustom" runat="server" ControlToValidate="txtUtlElectric" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|WS")%>
					</label>
					<asp:TextBox ID="txtUtlWater" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlWaterCustom" runat="server" ControlToValidate="txtUtlWater" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|NGOFO")%></label>
					<asp:TextBox ID="txtUtlGas" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlGasCustom" runat="server" ControlToValidate="txtUtlGas" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CS")%></label>
					<asp:TextBox ID="txtUtlTv" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlTvCustom" runat="server" ControlToValidate="txtUtlTv" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|TC")%></label>
					<asp:TextBox ID="txtUtlTrash" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlTrashCustom" runat="server" ControlToValidate="txtUtlTrash" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|TP")%></label>
					<asp:TextBox ID="txtTelephone" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtTelephoneCustom" runat="server" ControlToValidate="txtTelephone" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|FAFH")%></label>
					<asp:TextBox ID="txtFoodAway" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtFoodAwayCustom" runat="server" ControlToValidate="txtFoodAway" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|FAH")%></label>
					<asp:TextBox ID="txtGroceries" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtGroceriesCustom" runat="server" ControlToValidate="txtGroceries" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
			<div class="colformrht">
				<div class="dvrow" style="padding-left: 0px; text-align: left;">
					<p class="col_title">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|EWNH")%></p>
				</div>
				<div class="dvrow">
					<%--<label><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ROMP")%></label>--%>
					<asp:TextBox ID="txtRentMortNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Principal and Interest Only"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtRentMortNewCustom" runat="server" ControlToValidate="txtRentMortNew" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMoEquityNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoEquityNewCustom" runat="server" ControlToValidate="txtMoEquityNew" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMo2ndMortNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMo2ndMortNewCustom" runat="server" ControlToValidate="txtMo2ndMortNew" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMoPropTaxNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoPropTaxNewCustom" runat="server" ControlToValidate="txtMoPropTaxNew" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMoPropInsNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoPropInsNewCustom" runat="server" ControlToValidate="txtMoPropInsNew" Text="!" ClientValidationFunction="CheckMaxNumberlarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMoFeeNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMoFeeNewCustom" runat="server" ControlToValidate="txtMoFeeNew" Text="!" ClientValidationFunction="CheckMaxNumberlarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtHomeMaintenanceNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="$50 Minimum Recommended"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtHomeMaintenanceNewCustom" runat="server" ControlToValidate="txtHomeMaintenanceNew" Text="!" ClientValidationFunction="CheckMaxNumberLarge"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtUtlElectricNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlElectricNewCustom" runat="server" ControlToValidate="txtUtlElectricNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtUtlWaterNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator ID="txtUtlWaterNewCustom" runat="server" ControlToValidate="txtUtlWaterNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtUtlGasNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator ID="txtUtlGasNewCustom" runat="server" ControlToValidate="txtUtlGasNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtUtlTvNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlTvNewCustomValidator23" runat="server" ControlToValidate="txtUtlTvNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtUtlTrashNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtUtlTrashNewCustom" runat="server" ControlToValidate="txtUtlTrashNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtTelephoneNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Check with the current owner for estimated utility cost."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtTelephoneNewCustom" runat="server" ControlToValidate="txtTelephoneNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtFoodAwayNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtFoodAwayNewCustom" runat="server" ControlToValidate="txtFoodAwayNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtGroceriesNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtGroceriesNewCustom" runat="server" ControlToValidate="txtGroceriesNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
		</div>
		<%--Left Col End--%>
	</div>
	<div class="clearboth">
	</div>
</div>
<%--Monthly Food & Shelter Ends--%>
<%--Monthly Transport Starts--%>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MT")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<%--Left Col Start--%>
		<div class="dvform">
			<div class="colformlft">
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|AYC")%></label>
					<asp:DropDownList ID="ddlCarCurrent" runat="server" OnSelectedIndexChanged="ddlCarCurrent_SelectedIndexChanged" AutoPostBack="true">
					</asp:DropDownList>
					<asp:RequiredFieldValidator class="validationMessage" ID="ddlCarCurrentRequired" runat="server" EnableClientScript="true" Display="Dynamic" SetFocusOnError="true" InitialValue="0" ControlToValidate="ddlCarCurrent" Text="!" ValidationGroup="userprofile"></asp:RequiredFieldValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|AP")%></label>
					<asp:TextBox ID="txtCarPayments" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarPaymentsCustom" runat="server" ControlToValidate="txtCarPayments" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|AI")%></label>
					<asp:TextBox ID="txtCarInsurance" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarInsuranceCustom" runat="server" ControlToValidate="txtCarInsurance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|GAM")%></label>
					<asp:TextBox ID="txtCarMaintenance" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarMaintenanceCustom" runat="server" ControlToValidate="txtCarMaintenance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|PTR")%></label>
					<asp:TextBox ID="txtPublicTransportation" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtPublicTransportationCustom" runat="server" ControlToValidate="txtPublicTransportation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
			<div class="colformrht">
				<div class="dvrow">
					<label>
						&nbsp;</label>
					<br />
					<br />
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtCarPaymentsNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarPaymentsNewCustom" runat="server" ControlToValidate="txtCarPaymentsNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtCarInsuranceNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="Ask your insurance agent for a new quote."></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarInsuranceNewCustom" runat="server" ControlToValidate="txtCarInsuranceNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtCarMaintenanceNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);" ToolTip="will your commute be Longer or Shorter?"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtCarMaintenanceNewCustom" runat="server" ControlToValidate="txtCarMaintenanceNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtPublicTransportationNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtPublicTransportationNewCustom" runat="server" ControlToValidate="txtPublicTransportationNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
		</div>
		<%--Left Col End--%>
	</div>
	<div class="clearboth">
	</div>
</div>
<%--Monthly Transport Ends--%>
<%--Monthly Insurance, Medical Childcare Starts--%>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MIMC")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<%--Left Col Start--%>
		<div class="dvform">
			<div class="colformlft">
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ILM")%></label>
					<asp:TextBox ID="txtInsurance" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtInsuranceCustom" runat="server" ControlToValidate="txtInsurance" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MV&PC")%></label>
					<asp:TextBox ID="txtMedicalPrescription" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMedicalPrescriptionCustom" runat="server" ControlToValidate="txtMedicalPrescription" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CSA")%></label>
					<asp:TextBox ID="txtChildSupportAlimony" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtChildSupportAlimonyCustom" runat="server" ControlToValidate="txtChildSupportAlimony" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CCEC")%></label>
					<asp:TextBox ID="txtChildElderCare" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtChildElderCareCustom" runat="server" ControlToValidate="txtChildElderCare" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ERE")%></label>
					<asp:TextBox ID="txtEducation" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtEducationCustom" runat="server" ControlToValidate="txtEducation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
			<div class="colformrht">
				<div class="dvrow">
					<%--<label><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|ILM")%></label>--%>
					<asp:TextBox ID="txtInsuranceNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtInsuranceNewCustom" runat="server" ControlToValidate="txtInsuranceNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMedicalPrescriptionNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMedicalPrescriptionNewCustom" runat="server" ControlToValidate="txtMedicalPrescriptionNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<%--<label><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CSA")%></label>--%>
					<asp:TextBox ID="txtChildSupportAlimonyNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span>
						*</span>
					<asp:CustomValidator ID="txtChildSupportAlimonyNewCustom" runat="server" ControlToValidate="txtChildSupportAlimonyNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<%--<label> <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CCEC")%></label>--%>
					<asp:TextBox ID="txtChildElderCareNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span>
						*</span>
					<asp:CustomValidator ID="txtChildElderCareNewCustom" runat="server" ControlToValidate="txtChildElderCareNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtEducationNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtEducationNewCustom" runat="server" ControlToValidate="txtEducationNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
		</div>
		<%--Left Col End--%>
	</div>
	<div class="clearboth">
	</div>
</div>
<%--Monthly Insurance, Medical Childcare Ends--%>
<%--Monthly Personal & Miscellaneous Expenses Starts--%>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MPME")%></h1>
<div class="dvform2col dvformlblbig">
	<div class="dvform">
		<%--Left Col Start--%>
		<div class="dvform">
			<div class="colformlft">
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|TPOOIL")%></label>
					<asp:TextBox ID="txtOtherLoans" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtOtherLoansCustom" runat="server" ControlToValidate="txtOtherLoans" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CONTR")%></label>
					<asp:TextBox ID="txtContributions" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtContributionsCustom" runat="server" ControlToValidate="txtContributions" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CLO")%></label>
					<asp:TextBox ID="txtClothing" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtClothingCustom" runat="server" ControlToValidate="txtClothing" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|LDC")%></label>
					<asp:TextBox ID="txtLaundry" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtLaundryCustom" runat="server" ControlToValidate="txtLaundry" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|PETLBW")%></label>
					<asp:TextBox ID="txtPersonalExpenses" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtPersonalExpensesCustom" runat="server" ControlToValidate="txtPersonalExpenses" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|BBS")%></label>
					<asp:TextBox ID="txtBeautyBarber" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtBeautyBarberCustom" runat="server" ControlToValidate="txtBeautyBarber" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|RE")%>
					</label>
					<asp:TextBox ID="txtRecreation" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtRecreationCustom" runat="server" ControlToValidate="txtRecreation" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|CUD")%>
					</label>
					<asp:TextBox ID="txtClubDues" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtClubDuesCustom" runat="server" ControlToValidate="txtClubDues" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|GIF")%>
					</label>
					<asp:TextBox ID="txtGifts" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtGiftsCustom" runat="server" ControlToValidate="txtGifts" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<label>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|MBCMO")%></label>
					<asp:TextBox ID="txtMiscellaneous" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMiscellaneousCustom" runat="server" ControlToValidate="txtMiscellaneous" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
			<div class="colformrht">
				<div class="dvrow">
					<asp:TextBox ID="txtOtherLoansNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtOtherLoansNewCustom" runat="server" ControlToValidate="txtOtherLoansNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtContributionsNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtContributionsNewCustom" runat="server" ControlToValidate="txtContributionsNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtClothingNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtClothingNewCustom" runat="server" ControlToValidate="txtClothingNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtLaundryNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtLaundryNewCustom" runat="server" ControlToValidate="txtLaundryNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtPersonalExpensesNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtPersonalExpensesNewCustom" runat="server" ControlToValidate="txtPersonalExpensesNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtBeautyBarberNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtBeautyBarberNewCustom" runat="server" ControlToValidate="txtBeautyBarberNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtRecreationNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtRecreationNewCustom" runat="server" ControlToValidate="txtRecreationNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtClubDuesNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtClubDuesNewCustom" runat="server" ControlToValidate="txtClubDuesNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtGiftsNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtGiftsNewCustom" runat="server" ControlToValidate="txtGiftsNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
				<div class="dvrow">
					<asp:TextBox ID="txtMiscellaneousNew" runat="server" MaxLength="6" onFocus="SetUpField(this);" onBlur="ReFormatCurrency(this);"></asp:TextBox>
					<span class="requiredField">*</span>
					<asp:CustomValidator class="validationMessage" ID="txtMiscellaneousNewCustom" runat="server" ControlToValidate="txtMiscellaneousNew" Text="!" ClientValidationFunction="CheckMaxNumberSmall"></asp:CustomValidator>
				</div>
			</div>
		</div>
		<%--Left Col End--%>
	</div>
	<div class="clearboth">
	</div>
</div>
<%--Monthly Personal & Miscellaneous Expenses Ends--%>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseMonExp|WTPRYCS")%></p>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnMonthlyPreviousPage" runat="server" ToolTip="Return To Previous Page" OnClick="btnMonthlyPreviousPage_Click" CssClass="previous"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span>
		</asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnMonthlySaveExitPage" runat="server" ToolTip="Save & Continue Later" OnClick="btnMonthlySaveExitPage_Click" ValidationGroup="userprofile"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnMonthlyExpensesCon" runat="server" OnClick="btnMonthlyExpensesCon_Click" ValidationGroup="userprofile" ToolTip="Continue">
            <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
<input type="hidden" id="_CONFIRMED" name="_CONFIRMED" value="" />