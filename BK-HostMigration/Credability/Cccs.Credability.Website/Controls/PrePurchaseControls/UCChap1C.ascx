﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap1C.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap1C" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|IHOFM")%></b>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|WAYTAPA")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|TAOHO")%></b>
			<ul class="NormTxt">
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|APTCYO")%>
					<br />
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|FIYCG")%>
				</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1C|OYHCSYH")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
