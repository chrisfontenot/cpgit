﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCCounsStart.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCCounsStart" %>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td align="left">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseCounStart|YPTHO")%>
				<br />
			</h2>
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
		</td>
	</tr>
	<tr>
		<td align="left">
			<br />
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseCounStart|PFHC")%>
		</td>
	</tr>
	<tr>
		<td>
			<table align="center" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="20">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseCounStart|PRAR")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td height="55" width="100%">
			<table align="Left" width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center" width="40%">
					</td>
					<td width="20%">
						<asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" CssClass="Button" />
					</td>
					<td align="center" width="40%">
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
