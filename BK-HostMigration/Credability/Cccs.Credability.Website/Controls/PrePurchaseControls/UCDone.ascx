﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDone.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCDone" %>
<p align="center">
	<h2>
		<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDone|WAAATHY")%></h2>
</p>
<p class="NormTxt">
	<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDone|TYF")%>
</p>
<blockquote>
</blockquote>
<p class="NormTxt">
	<strong></strong>
</p>
<p align="center">
	<h1>
		<a href="FollowUp.aspx" class="MasiveLink">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDone|FUA")%></a></h1>
</p>
<p align="center">
	<h2>
		<a href="http://www.cccsatl.org?Done.aspx" class="BigLink">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseDone|RR")%></a></h2>
</p>
