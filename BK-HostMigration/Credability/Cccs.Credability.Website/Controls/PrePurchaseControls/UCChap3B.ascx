﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap3B.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap3B" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td align="center" class="GiantTxtB">
            <h2>
                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|SFAL")%></h2>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|SFA")%><br />
            <br />
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|SOM")%>
            <a class="DefWord" onmouseover="TagToTip('Int1')" onmouseout="UnTip()"></a>
        </td>
    </tr>
    <tr>
        <td align="left" class="MedTxtB">
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|MAAFANO")%>
            <a class="DefWord" onmouseover="TagToTip('REA1')" onmouseout="UnTip()"></a>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="MedTxtB">
            <h2>
                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|BOPL")%></h2>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap3B|AMLATR")%>
            <a href="http://www.federalreserve.gov/Pubs/riskyhomeloans/default.htm?Chap3B.aspx"
                class="NormLink" target="_blank">www.federalreserve.gov/Pubs/riskyhomeloans/default.htm</a>.
        </td>
        <td align="left" class="NormTxt">
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td>
            <!-- copy and paste. Modify height and width if desired. -->
            <object id="scPlayer" width="400" height="318" type="application/x-shockwave-flash"
                data="http://content.screencast.com/users/RCox/folders/certificate/media/0d43d6f2-91fb-49dc-b0b0-8a6c1972f376/flvplayer.swf">
                <param name="movie" value="http://content.screencast.com/users/RCox/folders/certificate/media/0d43d6f2-91fb-49dc-b0b0-8a6c1972f376/flvplayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#FFFFFF" />
                <param name="flashVars" value="thumb=http://content.screencast.com/users/RCox/folders/certificate/media/0d43d6f2-91fb-49dc-b0b0-8a6c1972f376/FirstFrame.jpg&containerwidth=400&containerheight=318&content=http://content.screencast.com/users/RCox/folders/certificate/media/0d43d6f2-91fb-49dc-b0b0-8a6c1972f376/CCCS_Loan_Officer_FINAL.flv&blurover=false" />
                <param name="allowFullScreen" value="true" />
                <param name="scale" value="showall" />
                <param name="allowScriptAccess" value="always" />
                <param name="base" value="http://content.screencast.com/users/RCox/folders/certificate/media/0d43d6f2-91fb-49dc-b0b0-8a6c1972f376/" />
                Unable to display content. Adobe Flash is required.
            </object>
        </td>
    </tr>
</table>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"
            CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
