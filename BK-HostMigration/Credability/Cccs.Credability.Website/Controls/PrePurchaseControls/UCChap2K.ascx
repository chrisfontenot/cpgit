﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap2K.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap2K" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td align="center" class="GiantTxtB">
            <b>
                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2K|THI")%></b>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2K|AWN")%>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2K|YWOC")%>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <asp:Label ID="lblThirdParagraph" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td>
            <!-- copy and paste. Modify height and width if desired. -->
            <object id="scPlayer" width="400" height="318" type="application/x-shockwave-flash"
                data="http://content.screencast.com/users/RCox/folders/certificate/media/6eb41251-b2f5-4c03-8c57-6418ed031253/flvplayer.swf">
                <param name="movie" value="http://content.screencast.com/users/RCox/folders/certificate/media/6eb41251-b2f5-4c03-8c57-6418ed031253/flvplayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#FFFFFF" />
                <param name="flashVars" value="thumb=http://content.screencast.com/users/RCox/folders/certificate/media/6eb41251-b2f5-4c03-8c57-6418ed031253/FirstFrame.jpg&containerwidth=400&containerheight=318&content=http://content.screencast.com/users/RCox/folders/certificate/media/6eb41251-b2f5-4c03-8c57-6418ed031253/CCCS_Private_Home_Inspector_FINAL.flv&blurover=false" />
                <param name="allowFullScreen" value="true" />
                <param name="scale" value="showall" />
                <param name="allowScriptAccess" value="always" />
                <param name="base" value="http://content.screencast.com/users/RCox/folders/certificate/media/6eb41251-b2f5-4c03-8c57-6418ed031253/" />
                Unable to display content. Adobe Flash is required.
            </object>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
</table>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"
            CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
