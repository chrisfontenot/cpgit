﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Cccs.Credability.Website.Controls.PrePurchaseControls
{
    public partial class BreadCrumControl : System.Web.UI.UserControl
    {
        public enum Tab
        {
            Chap1 ,
            Chap2 ,
            Chap3 ,
            Chap4 ,
            Chap5
        };


        protected void Page_Load(object sender , EventArgs e)
        {
            if (!IsPostBack)
            {
                lblBchProgressTracker.Width = System.Web.UI.WebControls.Unit.Percentage(App.PercentComplete * 0.97);

                // Enable based on Percent complete
                //
                int percent_complete = App.PercentComplete;

                if (App.WebsiteCode == Cccs.Identity.Website.PRP)
                {
                    if (percent_complete >= Cccs.Credability.Website.PrePurchase.PercentComplete.USER_PROFILE)
                    {
                        aChap1.HRef = AppRelativePathReplace("ProvideYourPersonalInfo.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.PrePurchase.PercentComplete.CHAP_2A)
                    {
                        aChap2.HRef = AppRelativePathReplace("Chap2A.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.PrePurchase.PercentComplete.CHAP_3A)
                    {
                        aChap3.HRef = AppRelativePathReplace("Chap3A.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.PrePurchase.PercentComplete.CHAP_4A)
                    {
                        aChap4.HRef = AppRelativePathReplace("Chap4A.aspx");
                    }

                    if (percent_complete >= Cccs.Credability.Website.PrePurchase.PercentComplete.CHAP_5A)
                    {
                        aChap5.HRef = AppRelativePathReplace("Chap5A.aspx");
                    }
                }
            }
        }

        private string AppRelativePathReplace(string url)
        {
            return VirtualPathUtility.GetDirectory(Request.AppRelativeCurrentExecutionFilePath) + url;
        }

        public void ActiveTabSet(Tab tab)
        {
            HtmlAnchor href = FindControl(string.Format("a{0}" , tab)) as HtmlAnchor;

            if (href != null)
            {
                href.Attributes["class"] += "active";
                Visible = true;
            }
        }

    }
}