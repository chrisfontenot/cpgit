﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap1B.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap1B" %>
<%--<script language="javascript" type="text/javascript">
    function FloatingForms(theField) 
    {
        var DefList = theField.value;
        DefList(0) = "Bor1";
        DefList(1) = "Crd1";
        DefList(2) = "Crd2";
        DefList(3) = "Crd3";
        DefList(4) = "FICO";
        DefList(5) = "Util1";
        DefList(6) = "SavAc1";
        theField.value = DefList;
    }
</script>
--%>
<table width="55%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|WDLLAQ")%></b>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<ul class="NormTxt">
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|YOBPO")%>
				</li>
				<li style="margin-bottom: 10px;">
					<a class="DefWord" onmouseover="TagToTip('Bor1')" onmouseout="UnTip()"></a>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|YCREP")%>
				</li>
			</ul>
		</td>
	</tr>
	<%--				<tr> 
					<td height="10"></td> 
				</tr> --%>
	<tr>
		<td align="left">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|CYCR")%>
			<ul class="NormTxt">
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|EQU")%>
					<a target="_blank" href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.equifax.com">www.equifax.com</a>
				</li>
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|EXP")%>
					<a target="_blank" href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.experian.com">www.experian.com</a>
				</li>
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|TRANS")%>
					<a target="_blank" href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.transunion.com">www.transunion.com</a>
				</li>
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|OR")%>
					<a target="_blank" class="NormLink" href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.annualcreditreport.com">www.annualcreditreport.com</a>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|FMI")%>
				</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<a class="DefWord" onmouseover="TagToTip('Crd1')" onmouseout="UnTip()"></a>
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|CSIASM")%>
			<ul class="NormTxt">
				<li style="margin-bottom: 10px;">
					<a class="DefWord" onmouseover="TagToTip('FICO')" onmouseout="UnTip()"></a>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|35POYP")%>
				</li>
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|30POHMC")%>
				</li>
				<li style="margin-bottom: 10px;">
					<a class="DefWord" onmouseover="TagToTip('Util1')" onmouseout="UnTip()"></a>
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|15PIB")%>
				</li>
				<li style="margin-bottom: 10px;">
					<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|10PIN")%>
				</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|IYDNH")%>
			<a class="DefWord" onmouseover="TagToTip('SavAc1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('Crd2')" onmouseout="UnTip()"></a>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1B|IYCNI")%>
		</td>
	</tr>
	<tr>
		<td height="15">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>