﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap1E.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap1E" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|SAYRFH")%></b>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="MedTxt">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|THYD")%></b>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left">
			<ol>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|AYS")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|DYS")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|DYHE")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|WYD")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|HYSM")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|HYAF")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|HYAA")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|ITFDYP")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|HYRAA")%><br />
				<br />
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1E|DYHML")%><br />
				<br />
			</ol>
		</td>
	</tr>
	<tr>
		<td height="35">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>