﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap4C.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap4C" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td align="center" class="GiantTxtB">
            <h2>
                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4C|S")%></h2>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4C|TLMR")%>
        </td>
    </tr>
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td class="NormTxt">
            <br>
            <br>
            <!-- copy and paste. Modify height and width if desired. -->
            <object id="scPlayer" width="720" height="498" type="application/x-shockwave-flash"
                data="http://content.screencast.com/users/RCox/folders/certificate/media/32de147a-b0a8-4147-b40a-8269add2e60d/flvplayer.swf">
                <param name="movie" value="http://content.screencast.com/users/RCox/folders/certificate/media/32de147a-b0a8-4147-b40a-8269add2e60d/flvplayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#FFFFFF" />
                <param name="flashVars" value="thumb=http://content.screencast.com/users/RCox/folders/certificate/media/32de147a-b0a8-4147-b40a-8269add2e60d/FirstFrame.jpg&containerwidth=720&containerheight=498&content=http://content.screencast.com/users/RCox/folders/certificate/media/32de147a-b0a8-4147-b40a-8269add2e60d/CCCS_INSURANCE_REVISED_CORRECT.flv&blurover=false" />
                <param name="allowFullScreen" value="true" />
                <param name="scale" value="showall" />
                <param name="allowScriptAccess" value="always" />
                <param name="base" value="http://content.screencast.com/users/RCox/folders/certificate/media/32de147a-b0a8-4147-b40a-8269add2e60d/" />
                Unable to display content. Adobe Flash is required.
            </object>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4C|YLWRTY")%>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
</table>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"
            CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
