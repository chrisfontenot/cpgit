﻿using System;
using System.Web.UI.WebControls;
using Cccs.Web;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.PrePurchasing
{
	public partial class UcDescribingYourSituation : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				InitializeTranslation();
				GetDescribeYourSelfInfo();
			}
			base.Page_Load(sender, e);
		}

		private void InitializeTranslation()
		{
			btnReturnToPrevious.ToolTip = App.Translate("Credability|RVMButtons|Previous");
			btnContinue.ToolTip = App.Translate("Credability|RVMButtons|Continue");
			btnSaveAndExit.ToolTip = App.Translate("Credability|RVMButtons|SaveContinueLater");

			ddlFoundHome.Items.Add(new ListItem(App.Translate("Credability|PickList|Select"), ""));
			CommonFunction.AddItems(ddlFoundHome.Items, "YesNo", SessionState.LanguageCode);
			CommonFunction.AddItems(rbtPreHome.Items, "PreHome", SessionState.LanguageCode);

			ValidationControlTranslation();
		}

		protected void ValidationControlTranslation()
		{
			DesValidationSummary.HeaderText = App.Translate("ValidationSummary|YouMustEnterAValue");
			ddlFoundHomeRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSWY");
			txtNewAdrRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PESA");//"Please Enter Street Address";
			txtNewCityRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PEC");//"Please Enter City";
			ddlStatesRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSST");//"Please Select State";
			txtNewZipRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PEZ");//"Please Enter Zip Code";
			txtPriceEstCustom.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PEE");//"Please Enter Estimated Purchase Price";
			rbtPreHomeRequired.ErrorMessage = App.Translate("Credability|DescYourSituRVM|PSWYH");//"plesse state whether you have owned a home or not";
			txtDateOfCustom.ErrorMessage = App.Translate("Credability|DescYourSituRVM|YDM");
		}

		private void GetDescribeYourSelfInfo()
		{
			UserReferenceDetails userReferenceDetails = App.Credability.UserReferenceDetailsGet(SessionState.ClientNumber.Value);
			if(userReferenceDetails != null)
			{
				chkAtBeg.Checked = (userReferenceDetails.BegPro == true) ? true : false;
				chkDnPayAst.Checked = (userReferenceDetails.AstPay == true) ? true : false;
				chkPreAprv.Checked = (userReferenceDetails.BenAprov == true) ? true : false;
				chkCloDate.Checked = (userReferenceDetails.HaveCloDate == true) ? true : false;
				if(userReferenceDetails.TheCloDate != String.Empty && userReferenceDetails.HaveCloDate)
				{
					DateTime CloseDate;
					CloseDate = Convert.ToDateTime(userReferenceDetails.TheCloDate);
					txtDateOf.Text = String.Format("{0:d}", CloseDate);
				}

				ddlFoundHome.SelectedValue = (userReferenceDetails.FndHouse != null) ? userReferenceDetails.FndHouse.Trim() : String.Empty;
				if(ddlFoundHome.SelectedValue == "Y")
					dvShowNewHome.Visible = true;
				txtNewAdr.Text = (userReferenceDetails.HouseAdr != null) ? userReferenceDetails.HouseAdr.Trim() : String.Empty;
				txtNewCity.Text = (userReferenceDetails.HouseCity != null) ? userReferenceDetails.HouseCity.Trim() : String.Empty;
				txtNewZip.Text = (userReferenceDetails.HouseZip != null) ? userReferenceDetails.HouseZip.Trim() : String.Empty;
				txtPriceEst.Text = userReferenceDetails.HousePrice.ToString(App.CurrencyFormat);
				ddlStates.SelectedValue = (userReferenceDetails.HouseSt != null) ? userReferenceDetails.HouseSt.Trim() : String.Empty;
				if(!userReferenceDetails.HadHouse.IsNullOrWhiteSpace())
					rbtPreHome.SelectedValue = userReferenceDetails.HadHouse.Trim();
			}
		}

		private UserReferenceDetailsResults SaveDescribeYourSelfInfo()
		{
			UserReferenceDetails userReferenceDetails = new UserReferenceDetails();

			userReferenceDetails.ClientNumber = SessionState.ClientNumber.Value;
			userReferenceDetails.BegPro = (chkAtBeg.Checked == true) ? true : false;
			userReferenceDetails.AstPay = (chkDnPayAst.Checked == true) ? true : false;
			userReferenceDetails.BenAprov = (chkPreAprv.Checked == true) ? true : false;
			userReferenceDetails.HaveCloDate = (chkCloDate.Checked == true) ? true : false;
			userReferenceDetails.TheCloDate = txtDateOf.Text;
			userReferenceDetails.FndHouse = ddlFoundHome.SelectedValue;
			userReferenceDetails.HadHouse = rbtPreHome.SelectedValue;
			if(ddlFoundHome.SelectedValue == "Y")
			{
				userReferenceDetails.HouseAdr = txtNewAdr.Text.Trim();
				userReferenceDetails.HouseCity = txtNewCity.Text.Trim();
				userReferenceDetails.HouseSt = ddlStates.SelectedValue;
				userReferenceDetails.HouseZip = txtNewZip.Text;
				userReferenceDetails.HousePrice = txtPriceEst.Text.ToDecimal(0);
			}
			else
			{
				userReferenceDetails.HouseAdr = String.Empty;
				userReferenceDetails.HouseCity = String.Empty;
				userReferenceDetails.HouseSt = String.Empty;
				userReferenceDetails.HouseZip = String.Empty;
				userReferenceDetails.HousePrice = 0;
			}
			return App.Credability.DescribeYourSelfPreHudAddUpdate(userReferenceDetails);
		}

		protected void ddlFoundHome_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(ddlFoundHome.SelectedValue == "Y")
			{
				dvShowNewHome.Visible = true;
				txtNewAdr.Focus();
			}
			else
			{
				dvShowNewHome.Visible = false;
				ddlFoundHome.Focus();
			}
		}

		protected void btnReturnToPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			if(ValidateZipState())
			{
				UserReferenceDetailsResults userReferenceDetailsResults = SaveDescribeYourSelfInfo();
				if(userReferenceDetailsResults.IsSuccessful)
				{
					NavigateContinue();
				}
				else
				{
					dvErrorMessage.InnerHtml = userReferenceDetailsResults.Exception.ToString();  //"Error in Updating Information.";
				}
			}
		}

		protected void btnSaveAndExit_Click(object sender, EventArgs e)
		{
			UserReferenceDetailsResults userReferenceDetailsResults = SaveDescribeYourSelfInfo();
			if(userReferenceDetailsResults.IsSuccessful)
			{
				NavigateExit();
			}
			else
			{
				dvErrorMessage.InnerHtml = "Error in Updating Information.";
			}
		}

		protected void txtPriceEstCustom_ServerValidate(object source, ServerValidateEventArgs args)
		{
			if(txtPriceEst.Text.ToInt(0) == 0 && ddlFoundHome.SelectedValue == "Y")
			{
				args.IsValid = false;
			}
			else
			{
				args.IsValid = true;
			}
		}

		protected void txtDateOfCustom_ServerValidate(object source, ServerValidateEventArgs args)
		{
			if(chkCloDate.Checked)
			{
				if(txtDateOf.Text.ToDateTime(null) == null)
				{
					args.IsValid = false;
				}
				else
				{
					args.IsValid = true;
				}
			}
			else
			{
				args.IsValid = true;
			}
		}

		protected bool ValidateZipState()
		{
			bool IsPageValid = Page.IsValid;
			if(ddlFoundHome.SelectedValue == "Y" && IsPageValid)
			{
				string state_code = App.Geographics.StateCodeGetByZip(txtNewZip.Text.Clean());
				dvServerSideValidation.InnerHtml = App.Translate("Credability|UserProfileBCH|PCTFI") + ":<ul>";
				if(string.IsNullOrEmpty(state_code))
				{
					dvServerSideValidation.InnerHtml += "<li>" + App.Translate("Credability|UserProfileBCH|IZC") + "</li></ul>"; // TODO: Translate
					IsPageValid = false;
					dvServerSideValidation.Visible = true;

				}
				else if(string.Compare(state_code, ddlStates.SelectedValue, true) != 0)
				{
					dvServerSideValidation.InnerHtml += string.Format("<li>" + App.Translate("Credability|UserProfileBCH|SEFZI") + "</li></ul>", txtNewZip.Text, state_code); // TODO: Translate
					IsPageValid = false;
					dvServerSideValidation.Visible = true;
				}
			}
			return IsPageValid;
		}
	}
}
