﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChapter1.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChapter1" %>
<script language="javascript" type="text/javascript">
	//var DefList;
	function FloatingForms(theField)
	{
		var DefList = theField.value;
		DefList(0) = "Crd1";
		DefList(1) = "Equ1";
		//DefPull DefList;
		theField.value = DefList;
	}
</script>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1|C1")%>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="BigTxtB">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap1|MPWT")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>
