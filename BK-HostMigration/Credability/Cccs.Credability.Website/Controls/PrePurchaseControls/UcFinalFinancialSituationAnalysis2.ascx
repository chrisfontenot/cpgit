﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcFinalFinancialSituationAnalysis2.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UcFinalFinancialSituationAnalysis2" %>
<div style="width: 100%; color: Red; font-weight: bold" id="DvError" runat="server">
</div>
<h1>
	<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|UFSA")%></h1>
<p>
	<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|BATIY")%></p>
<div class="dvfinance">
	<div class="dvcollft">
		<h1>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|IES")%></h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|MI")%>
				</td>
				<td class="col2">
					<asp:Label ID="lblNetMonthlyIncome" runat="server"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|MLE")%>
				</td>
				<td class="col2">
					<asp:Label ID="lblMonthlyLivingExpenses" runat="server"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|MDP")%>
				</td>
				<td class="col2">
					<asp:Label ID="MonthlyDebtPayments" runat="server"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|Cash")%>
				</td>
				<td class="col2">
					<asp:Label ID="lblDisposableIncome" runat="server"></asp:Label>
				</td>
			</tr>
		</table>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|AnalyzeYourFinancialSituation2|If_You_Have_A_Negative")%></p>
		<p>
			<b><a href="MonthlyExpensesNew.aspx?back=<%=ReturnTo%><%--UFS2--%>">
				<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|RTME")%></a></b> &nbsp; &nbsp; &nbsp;<b><a href="UserIncomeDocumentation.aspx?back=<%=ReturnTo%><%--UFS2--%>">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|RTMI")%></a></b></p>
		<asp:TextBox ID="txtReasonHelp" runat="server" TextMode="MultiLine" Width="98%" MaxLength="1000" Rows="3"></asp:TextBox>
	</div>
	<div class="dvcolrht">
		<h1>
			&nbsp;</h1>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|NetWorth")%>
				</td>
				<td class="col2">
					<asp:Label ID="lblNetWorth" runat="server"></asp:Label>
				</td>
			</tr>
		</table>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|IYHANN")%></p>
		<p>
			<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|IYHAP")%></p>
		<table class="tblsummary" cellpadding="0" cellspacing="0">
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|PPCS")%>
				</td>
				<td class="col2">
					<asp:Label ID="PrimaryCreditScoreLiteral" runat="server"></asp:Label>
				</td>
			</tr>
			<tr>
				<td class="col1">
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|SPCS")%>
				</td>
				<td class="col2">
					<asp:Label ID="SecondaryCreditScoreLiteral" runat="server"></asp:Label>
				</td>
			</tr>
		</table>
		<div>
			<ul>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|YCSIAN")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|CSRF")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|THTS")%>
				</li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|SAT700")%></li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|SB600")%></li>
				<li>
					<%= Cccs.Credability.Website.App.Translate("Credability|FinancialSituationAnalysis|TCSP")%></li>
			</ul>
		</div>
	</div>
</div>
<asp:HiddenField ID="hdnTotalExp" runat="server" />
<asp:HiddenField ID="hdnDispIncome" runat="server" />
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPrevious_Click" ToolTip="Return To Previous Page" CssClass="previous" CausesValidation="false"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveExit" runat="server" OnClick="btnSaveExit_Click" ToolTip="Save & Continue Later"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"><span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>