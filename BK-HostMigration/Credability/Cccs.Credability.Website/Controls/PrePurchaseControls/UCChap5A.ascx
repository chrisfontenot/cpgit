﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap5A.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap5A" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5A|C5")%></h2>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="BigTxtB">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5A|LAH")%></b>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5A|COBAH")%>
			<a class="DefWord" onmouseover="TagToTip('Bud1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('For1')" onmouseout="UnTip()"></a>
		</td>
	</tr>
	<tr>
		<td height="50">
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>