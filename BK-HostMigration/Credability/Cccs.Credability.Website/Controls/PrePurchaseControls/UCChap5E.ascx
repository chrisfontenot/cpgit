﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap5E.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap5E" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|AF")%></h2>
			<a class="DefWord" onmouseover="TagToTip('For1')" onmouseout="UnTip()"></a>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|NTYH")%><br />
			<br />
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|NTY")%>
			(<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.CredAbility.org" class="NormLink" target="_blank">www.CredAbility.org</a>)
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|A1800")%>
			(<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.nfcc.org" class="NormLink" target="_blank">www.nfcc.org</a>)
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|WHT")%><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|NOTE")%><br />
			<br />
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|BO")%></b><br />
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap5E|USA")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<a class="DefWord" onmouseover="TagToTip('Int1')" onmouseout="UnTip()"></a><a class="DefWord" onmouseover="TagToTip('Crd1')" onmouseout="UnTip()"></a>
		</td>
	</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>