﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.PrePurchaseControls
{
	public partial class UcFinalFinancialSituationAnalysis2 : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				btnPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
				btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
				btnSaveExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
				if(SessionState.ClientNumber != null)
				{
					CalculateFinancialSituation(SessionState.ClientNumber.Value);
				}
			}
			base.Page_Load(sender, e);
		}

		private void CalculateFinancialSituation(int ClientNumber)
		{
			FinanicalSituationDetails AnalyzedResult = GetNewValues(ClientNumber);
			if(AnalyzedResult != null)
			{
				lblNetMonthlyIncome.Text = AnalyzedResult.Income.NetIncomeTotalBoth.ToString(App.CurrencyFormat);
				lblMonthlyLivingExpenses.Text = AnalyzedResult.MonthlyExpenses.ExpensesTotal.ToString(App.CurrencyFormat);
				MonthlyDebtPayments.Text = AnalyzedResult.AssetsLiabilities.CreditorTotals.Payments.ToString(App.CurrencyFormat);
				lblNetWorth.Text = AnalyzedResult.AssetsLiabilities.NetWorth.ToString(App.CurrencyFormat);
				if(AnalyzedResult.AssetsLiabilities.NetWorth >= 0)
				{
					lblNetWorth.Style["color"] = "#333";
				}
				else
				{
					lblNetWorth.Style["color"] = "Red";
				}

				lblDisposableIncome.Text = AnalyzedResult.DisposableIncome.ToString(App.CurrencyFormat);
				if(AnalyzedResult.DisposableIncome >= 0)
				{
					lblDisposableIncome.Style["color"] = "#333";
				}
				else
				{
					lblDisposableIncome.Style["color"] = "Red";
				}
				txtReasonHelp.Text = AnalyzedResult.ReasonForhelp;

                // Bankruptcy Conversion
                //var creditScoreResult = App.Host.GetCredScore(SessionState.ClientNumber.Value);
                var creditScoreResult = App.Debtplus.GetCredScore(SessionState.ClientNumber.Value);
                if (creditScoreResult.IsSuccessful)
				{
					var creditScores = creditScoreResult;
					PrimaryCreditScoreLiteral.Text = creditScores.PrimaryScore;
					SecondaryCreditScoreLiteral.Text = String.IsNullOrEmpty(creditScores.SecondaryScore) ? "N/A" : creditScores.SecondaryScore;
				}
			}
		}

		protected void btnContinue_Click(object sender, EventArgs e)
		{
			NavigateContinue();
		}

		protected void btnPrevious_Click(object sender, EventArgs e)
		{
			NavigatePrevious();
		}

		protected void btnSaveExit_Click(object sender, EventArgs e)
		{
			NavigateExit();
		}

		protected FinanicalSituationDetails GetNewValues(int ClientNumber)
		{
			FinanicalSituationDetails Details = App.Credability.UserFinalFinanicalSituationGet(ClientNumber);
			if(Details != null)
			{
				Details.MonthlyExpenses = App.Credability.NewMonthlyExpensesPreHudGet(ClientNumber);
			}
			return Details;
		}
	}
}