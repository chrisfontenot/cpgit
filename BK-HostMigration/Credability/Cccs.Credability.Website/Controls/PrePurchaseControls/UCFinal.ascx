﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFinal.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCFinal" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h2>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFinal|CONGT")%></h2>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFinal|YHC")%>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td class="NormTxt">
			<table width="85%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="NormTxt">
						<ul class="NormTxt">
						</ul>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="15">
		</td>
	</tr>
	<%--<tr> 
					<td align="center"> 
            <asp:Button ID="btnContinue" runat="server" Text="Send to Counselor for Final Review" 
                            CssClass="Button" onclick="btnContinue_Click" />
        &nbsp;</td> 
				</tr> --%>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSendtoCounselorforFinalReview" runat="server" OnClick="click_btnSendtoCounselorforFinalReview" ValidationGroup="user"><span><%= Cccs.Credability.Website.App.Translate("SENDCOUNSELORFOR")%></span></asp:LinkButton>
	</div>
</div>