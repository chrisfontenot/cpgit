﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Controls.PrePurchaseControls
{
    public partial class UCChap3D : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnReturnToPrevious.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous");
                btnContinue.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue");
                btnSaveAndExit.ToolTip = Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater");
            }
        }

        protected void btnReturnToPrevious_Click(object sender, EventArgs e)
        {
            Response.Redirect("Chap3C.aspx");
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            Response.Redirect("Chap3E.aspx");
        }

        protected void btnSaveAndExit_Click(object sender, EventArgs e)
        {
            Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
        }
    }
}