﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap4D.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap4D" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td align="center" class="GiantTxtB">
            <h2>
                <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4D|CTBD")%></h2>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4D|DTC")%>
        </td>
    </tr>
    <tr>
        <td height="10">
        </td>
    </tr>
    <tr>
        <td class="NormTxt">
            <br>
            <br>
            <!-- copy and paste. Modify height and width if desired. -->
            <object id="scPlayer" width="400" height="318" type="application/x-shockwave-flash"
                data="http://content.screencast.com/users/RCox/folders/certificate/media/785520b3-3bd3-40c3-abca-b75423f6681f/flvplayer.swf">
                <param name="movie" value="http://content.screencast.com/users/RCox/folders/certificate/media/785520b3-3bd3-40c3-abca-b75423f6681f/flvplayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#FFFFFF" />
                <param name="flashVars" value="thumb=http://content.screencast.com/users/RCox/folders/certificate/media/785520b3-3bd3-40c3-abca-b75423f6681f/FirstFrame.jpg&containerwidth=400&containerheight=318&content=http://content.screencast.com/users/RCox/folders/certificate/media/785520b3-3bd3-40c3-abca-b75423f6681f/CCCS_Real_Estate_Agent_FINAL.flv&blurover=false" />
                <param name="allowFullScreen" value="true" />
                <param name="scale" value="showall" />
                <param name="allowScriptAccess" value="always" />
                <param name="base" value="http://content.screencast.com/users/RCox/folders/certificate/media/785520b3-3bd3-40c3-abca-b75423f6681f/" />
                Unable to display content. Adobe Flash is required.
            </object>
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
    <tr>
        <td align="left" class="NormTxt">
            <%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap4D|BSYRC")%>
            <a class="DefWord" onmouseover="TagToTip('Hom1')" onmouseout="UnTip()"></a>.
        </td>
    </tr>
    <tr>
        <td height="20">
        </td>
    </tr>
</table>
<div class="dvbtncontainer">
    <div class="lnkbutton">
        <asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
    </div>
    <div class="lnkbutton">
        <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue"
            CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
    </div>
</div>
