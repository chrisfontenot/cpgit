﻿using System;
using Cccs.Credability.Website.Code;

namespace Cccs.Credability.Website.Controls.PrePurchaseControls
{
	public partial class UCFinal : CounselingUcBase
	{
		protected new void Page_Load(object sender, EventArgs e)
		{
			base.Page_Load(sender, e);
		}

		protected void click_btnSendtoCounselorforFinalReview(object sender, EventArgs e)
		{
			SubmitToQueue(true);
			NavigatePage(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
		}
	}
}