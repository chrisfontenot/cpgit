﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCFollowUp.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCFollowUp" %>
<table width="55%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<h1>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|FUA")%></h1>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|FAI")%>
			<a href="http://www.credabilityu.org?FollowUp.aspx" class="NormLink" target="_blank">www.credabilityu.org</a>.
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|IP")%>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center">
			<table>
				<tr>
					<td align="left">
						<ul class="NormTxt" style="list-style-type: none;">
							<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|WTB")%>
						</ul>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="20">
		</td>
	</tr>
	<tr>
		<td align="left" class="NormTxt">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|TGMC")%>
			<ul class="NormTxt">
				<a href="http://www.myfico.com?FollowUp.aspx" class="NormLink" target="_blank">www.myfico.com</a>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|TMBAF")%>
				<a href="http://www.annualcreditreport.com?FollowUp.aspx" class="NormLink" target="_blank">www.annualcreditreport.com</a>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|OCTCB")%>
			</ul>
			<table align="center" class="NormTxt" cellspacing="0">
				<tr>
					<td>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|EQ")%>
					</td>
					<td>
						<a href="http://www.equifax.com?FollowUp.aspx" class="NormLink" target="_blank">www.equifax.com</a>
					</td>
					<td>
						800.685.1111
					</td>
				</tr>
				<tr>
					<td>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|EX")%>
					</td>
					<td>
						<a href="http://www.experian.com?FollowUp.aspx" class="NormLink" target="_blank">www.experian.com</a>
					</td>
					<td>
						888.397.3742
					</td>
				</tr>
				<tr>
					<td>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|TR")%>
					</td>
					<td>
						<a href="http://www.transunion.com?FollowUp.aspx" class="NormLink" target="_blank">www.transunion.com</a>
					</td>
					<td>
						877.322.8228
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="40">
		</td>
	</tr>
	<tr>
		<td align="left">
			<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|TMTC")%>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="left">
			<blockquote class="NormTxt">
			</blockquote>
		</td>
	</tr>
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="left">
			<blockquote class="NormTxt">
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|HUD")%>
				<a href="http://portal.hud.gov/portal/page/portal/HUD/topics/buying_a_home" class="NormLink" target="_blank">http://portal.hud.gov/portal/page/portal/HUD/topics/buying_a_home</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|HBI")%>
				<a href="http://www.homefair.com?FollowUp.aspx" class="NormLink" target="_blank">www.homefair.com</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|BR")%>
				<a href="http://www.bankrate.com/brm/mortgage-calculator.asp" class="NormLink" target="_blank">www.bankrate.com/brm/mortgage-calculator.asp</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|FM")%>
				<a href="http://www.government-mortgages.com?FollowUp.aspx" class="NormLink" target="_blank">www.government-mortgages.com</a><br>
				<!-- <strong>Fannie Mae - </strong><a href="ReDirExt.asp?ReDir=www.mortgagecontent.net/scApplication/fanniemae/affordability.do" class="NormLink" target="_blank">www.mortgagecontent.net/scApplication/fanniemae/affordability.do</a><br> -->
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|FMC")%>
				<a href="http://www.freddiemac.com/corporate/buyown/english/calcs_tools?FollowUp.aspx" class="NormLink" target="_blank">www.freddiemac.com/corporate/buyown/english/calcs_tools</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|HL")%>
				<a href="http://www.realtor.com/" class="NormLink" target="_blank">www.realtor.com</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|HI")%>
				<a href="http://www.ashi.org?FollowUp.aspx" class="NormLink" target="_blank">www.ashi.org</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|URL")%>
				<a href="http://www.federalreserve.gov/Pubs/riskyhomeloans/default.htm?FollowUp.aspx" class="NormLink" target="_blank">www.federalreserve.gov/Pubs/riskyhomeloans/default.htm</a><br>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseFollow|URL")%>
				<a href="http://www.energysavers.gov?FollowUp.aspx" class="NormLink" target="_blank">www.energysavers.gov</a><br>
			</blockquote>
		</td>
	</tr>
</table>