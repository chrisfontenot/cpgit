﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCChap2C.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.UCChap2C" %>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="10">
		</td>
	</tr>
	<tr>
		<td align="center" class="GiantTxtB">
			<b>
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|HMOAHCYA")%></b>
		</td>
		<tr>
			<td align="left" class="NormTxt">
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|TFWHC")%>
				<ul class="NormTxt">
					<li style="margin-bottom: 10px;">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|BR")%><br />
						<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.bankrate.com/brm/mortgage-calculator.asp" target="_blank">www.bankrate.com/brm/mortgage-calculator.asp</a>
					</li>
					<li style="margin-bottom: 10px;">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|FM")%><br />
						<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.government-mortgages.com" target="_blank">www.government-mortgages.com</a>
					</li>
					<li style="margin-bottom: 10px;">
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|FMA")%><br />
						<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.freddiemac.com/corporate/buyown/english/calcs_tools" target="_blank">www.freddiemac.com/corporate/buyown/english/calcs_tools</a>
					</li>
					<li>
						<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|NFFCC")%><br />
						<a href="https://onlinecounsel.cccsatl.org/prp/scripts/ReDirExt.asp?ReDir=www.nfcc.org/FinancialEducation" target="_blank">www.nfcc.org/FinancialEducation</a>
					</li>
				</ul>
			</td>
		</tr>
		<tr>
			<td align="left" class="NormTxt">
				<%= Cccs.Credability.Website.App.Translate("Credability|PrePurchaseChap2C|GPQF")%>
			</td>
		</tr>
		<tr>
			<td height="40">
			</td>
		</tr>
</table>
<div class="dvbtncontainer">
	<div class="lnkbutton">
		<asp:LinkButton runat="server" ID="btnReturnToPrevious" CssClass="previous" OnClick="btnReturnToPrevious_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Previous")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnSaveAndExit" runat="server" ToolTip="Save &amp; Exit" OnClick="btnSaveAndExit_Click">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|SaveContinueLater")%></span></asp:LinkButton>
	</div>
	<div class="lnkbutton">
		<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue" CausesValidation="true">
      <span>
        <%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span></asp:LinkButton>
	</div>
</div>