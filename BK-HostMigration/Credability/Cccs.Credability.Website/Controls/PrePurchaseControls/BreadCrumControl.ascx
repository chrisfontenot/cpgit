﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadCrumControl.ascx.cs" Inherits="Cccs.Credability.Website.Controls.PrePurchaseControls.BreadCrumControl" %>
<p>&nbsp</p>
<div class="dvtab-ctrl">
	<ul>
		<li><a ID="aChap5" runat="server" class="last"><span><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchase|Chap5")%></span></a></li>
		<li><a ID="aChap4" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchase|Chap4")%></span></a></li>
		<li><a ID="aChap3" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchase|Chap3")%></span></a></li>
		<li><a ID="aChap2" runat="server"><span><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchase|Chap2")%></span></a></li>
		<li><a ID="aChap1" runat="server" class="first"><span><%= Cccs.Credability.Website.App.Translate("Credability|PrePurchase|Chap1")%></span></a></li>
	</ul>
	<div class="clear"></div>
	<div class="dvsiteTracker" style="width:360px">
		<asp:Label ID="lblBchProgressTracker" runat="server" Width="100%">&nbsp;</asp:Label>
	</div> 
</div>
<div class="clear"></div>