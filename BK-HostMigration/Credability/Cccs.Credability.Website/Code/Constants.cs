﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website
{
    public static class Constants
    {
        public static class ClientType
        {
            public const string REC = "REC";
            public const string BCH = "BCH";
            public const string BC = "BC";
            public const string HUD = "HUD";
            public const string BKC = "BKC";
            public const string BK = "BK";
            public const string RVM = "RVM";
            public const string BKE = "BKE";
            public const string DMP = "DMP";
        }

        public static class SkillCode
        {
            public const string BCH = "BCH";
            public const string BC = "FCO/DMP%20Internet%20Counselor";
            public const string InternetCounselor = "Internet%20Counselor";
            public const string BankruptcyEducation = "BKeducation";
            public const string HUD = "HUD";
            public const string Housing = "Housing";
            public const string REC = "Reconnect-Chase";
        }

        public static class Servicer
        {
            public const int OtherServicer = 12982;
        }

        public static class HostLanguage
        {
            public const string English = "E";
            public const string Spanish = "S";
        }
    }
}