﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace Cccs.Credability.Website
{
	public class WesternUnion
    {
        private static readonly NLog.Logger logger;

		private const string LOGONURL = "https://trackpayments.westernunion.com/tally/Logon.do";
		private const string VALIDATEURL = "https://trackpayments.westernunion.com/tally/PaymentInquiry.do";
		private static string LOGOUTURL = "https://trackpayments.westernunion.com/tally/Logout.do";

		static WesternUnion()
        {
            logger = NLog.LogManager.GetCurrentClassLogger();
		}

		public static bool IsMCTNValid(string mctn)
		{
			bool isValid = false;
			string SessionID = "";
			Cookie JSessionID = null;
			string userid = ConfigurationManager.AppSettings["WestUnionLogIn"];
			string password = ConfigurationManager.AppSettings["WestUnionPw"];
			string clid = ConfigurationManager.AppSettings["WestUnionClId"];
			try
			{
				WesternUnion.Login(userid, password, clid, out SessionID, out JSessionID);

				if(!(string.IsNullOrEmpty(SessionID) && JSessionID != null))
				{
					isValid = WesternUnion.ValidateMCTN(mctn, SessionID, userid, password, clid, JSessionID);

					WesternUnion.Logout(SessionID, JSessionID);

				}
				return isValid;
			}
			catch(WesternUnionException ex)
			{
				logger.Error(string.Format("Error in isMCTNValid {0}", mctn), ex);
				throw;
			}
			catch(Exception ex)
			{
				logger.Error(string.Format("Error in isMCTNValid {0}", mctn), ex);
				throw new WesternUnionException("Unknown Error while validating MCTN Number. Please try again.");
			}

		}

		/// <summary>
		/// Login to the western union website
		/// </summary>
		/// <param name="userID"></param>
		/// <param name="password"></param>
		/// <param name="Clid"></param>
		/// <param name="sessionID"></param>
		/// <param name="Jsession"></param>
		private static void Login(string userID, string password, string Clid, out string sessionID, out Cookie Jsession)
		{
			sessionID = "";
			Jsession = null;
			// Create parameter list
			Dictionary<string, string> parameters = new Dictionary<string, string>()
        {  
        {"UserID", userID},
        {"Password", password},
        {"ClientID", Clid},
        {"LoginStatus", "N"}
        };

			CookieCollection cookieCollection;
			var cookies = new CookieContainer();
			// Create request

			string str = SendHttpRequest(parameters, LOGONURL, cookies, out  cookieCollection);

			// Get Session ID
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(str);
			HtmlNode node = doc.DocumentNode.SelectSingleNode(@"//input[@name='SessionID']");
			if(node != null)
			{
				sessionID = node.Attributes["value"].Value;
				Jsession = cookieCollection["JSessionID"];
			}
			else
			{
				throw new WesternUnionException("Error validating MCTN Number. Please try again in 15 minutes");
			}





		}

		/// <summary>
		/// Validate Western Union Number
		/// </summary>
		/// <param name="mtcn"></param>
		/// <param name="SessionID"></param>
		/// <param name="UserID"></param>
		/// <param name="password"></param>
		/// <param name="CLID"></param>
		/// <param name="JsessionID"></param>
		/// <returns></returns>
		private static bool ValidateMCTN(string mtcn, string SessionID, string UserID, string password, string CLID, Cookie JsessionID)
		{
			bool valid = false;
			// create parameter list      

			Dictionary<string, string> parameters = new Dictionary<string, string>()
        {   {"LoginStatus", "Y"},
            {"LogonID", UserID},
            {"LogonAcct", CLID},
            {"SwtAcctID", CLID},
            {"SessionID", SessionID},
            {"Sys", string.Empty},
            {"JspID", string.Empty},
            {"GroupID", string.Empty},
            {"ConsNumber", string.Empty},
            {"MTCN", mtcn},
            {"Amount", string.Empty},
            {"FromDate", string.Empty},
            {"ToDate", string.Empty},
            {"FromTime", string.Empty},
            {"ToTime", string.Empty},
            {"LastName", string.Empty},
            {"FirstName", string.Empty},
            {"Status", "All"},
            {"Flag", "A"},
            {"QueryFlag", "R"},
            {"PageNo", "1"},
            {"ReportFlag", "P"},
            {"CCID", CLID},
            {"Submit", string.Empty}};

			// Create request
			var cookies = new CookieContainer();
			CookieCollection cookieCollection;
			cookies.Add(JsessionID);
			string str = SendHttpRequest(parameters, VALIDATEURL, cookies, out cookieCollection);

			// Get MTCN
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(str);

			HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(@"//table/tr/td/a[contains(@href,'DetailedTxn')]");

			if(nodes != null)
			{
				foreach(HtmlNode n in nodes)
				{
					if(n.InnerText.Trim().IndexOf(mtcn) == 0)
					{
						valid = true;
						break;
					}
				}
			}

			return valid;

		}


		/// <summary>
		/// Logout
		/// </summary>
		/// <param name="SessionID"></param>
		/// <param name="jSessionID"></param>
		private static void Logout(string SessionID, Cookie jSessionID)
		{
			Dictionary<string, string> parameters = new Dictionary<string, string>(){           
            {"LoginStatus", "Y"},
            {"SessionID", SessionID}           
           };

			// Create request
			var cookies = new CookieContainer();
			CookieCollection cookieCollection;
			cookies.Add(jSessionID);
			SendHttpRequest(parameters, LOGOUTURL, cookies, out cookieCollection);
		}

		private static string SendHttpRequest(Dictionary<string, string> parameters, string url, CookieContainer cookies, out CookieCollection cookieCollection)
		{
			string postData = CreatePostData(parameters);
			ASCIIEncoding encoding = new ASCIIEncoding();
			byte[] data = encoding.GetBytes(postData);
			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
			request.ContentType = "application/x-www-form-urlencoded";
			request.Method = "POST";
			request.ContentLength = data.Length;
			request.CookieContainer = cookies;
			Stream requestStream = request.GetRequestStream();
			try
			{

				using(StreamWriter swOut = new StreamWriter(requestStream))
				{
					requestStream = null;
					swOut.Write(postData);

				}
			}
			finally
			{
				if(requestStream != null)
					requestStream.Dispose();

			}

			return GetWebResponse(request, out cookieCollection);

		}

		private static string GetWebResponse(HttpWebRequest request, out CookieCollection cookies)
		{
			// Get response

			using(HttpWebResponse response = (HttpWebResponse) request.GetResponse())
			{
				if(response.StatusCode != HttpStatusCode.OK)
					throw new InvalidOperationException("Unable to retrieve web response successfully.");
				cookies = response.Cookies;
				Stream responseStream = response.GetResponseStream();
				try
				{
					using(StreamReader reader = new StreamReader(responseStream))
					{
						responseStream = null;
						string str = reader.ReadToEnd();
						return str;
					}
				}
				finally
				{
					if(responseStream != null)
						responseStream.Dispose();
				}

			}
		}

		private static string CreatePostData(Dictionary<string, string> param)
		{
			StringBuilder s = new StringBuilder();

			foreach(var key in param.Keys)
			{
				s.AppendFormat("{0}={1}&", key, param[key]);

			}
			s.Remove(s.Length - 1, 1);
			return s.ToString();
		}



	}
}