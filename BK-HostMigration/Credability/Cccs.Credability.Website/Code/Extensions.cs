﻿using System;
using System.Web;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Cccs.Credability.Website
{
	public static class Extensions
	{
		public static string Clean(this string str)
		{
			return (str != null) ? str.Trim() : String.Empty;
		}

		public static string Scrub(this string str)
		{
			string scrubbed = str;

			if(scrubbed != null)
			{
				scrubbed = new Regex // Use regular expression to remove "javascript"
				(
					"([a-z]*)[\\x00-\\x20]*=[\\x00-\\x20]*([\\`\\\'\\\\\"]*)[\\x00-\\x20]*j[\\x00-\\x20]*a[\\x00-\\x20]*v[\\x0" + "0-\\x20]*a[\\x00-\\x20]*s[\\x00-\\x20]*c[\\x00-\\x20]*r[\\x00-\\x20]*i[\\x00-\\x20]*p[\\x00-\\x20]*t[\\x00-\\x20]*",
					RegexOptions.IgnoreCase
				).Replace(scrubbed, string.Empty);

				scrubbed = new Regex // Use regular expression to remove risky tags
				(
					"</*(table|drop|delete|applet|meta|xml|blink|link|style|script|embed|object|iframe|frame|frameset|ilayer|layer|bgsound|title|base|body)[^>]*>",
					RegexOptions.IgnoreCase
				).Replace(scrubbed, string.Empty);


				scrubbed = HttpContext.Current.Server.HtmlEncode(scrubbed);

				scrubbed = scrubbed.Replace("&amp;", "&");  // Ampersand
				scrubbed = scrubbed.Replace("&#169;", "©"); // Copyright 
				scrubbed = scrubbed.Replace("&#174;", "®"); // Registered
				scrubbed = scrubbed.Replace("&#153;", "™"); // Trademark

				scrubbed = scrubbed.Trim();
			}

			return scrubbed;
		}

		public static decimal ToDecimal(this string str, decimal def)
		{
			decimal val;
			if(decimal.TryParse(str.Replace("%", String.Empty), NumberStyles.Currency, CultureInfo.CurrentCulture, out val))
			{
				return val;
			}
			else
			{
				return def;
			}
		}

		public static int ToInt(this string str, int def)
		{
			int val;
			if(int.TryParse(str.Replace("%", String.Empty), NumberStyles.Currency, CultureInfo.CurrentCulture, out val))
			{
				return val;
			}
			else
			{
				return def;
			}
		}

		public static float ToFloat(this string str, float def)
		{
			float val;
			if(float.TryParse(str.Replace("%", String.Empty), NumberStyles.Currency, CultureInfo.CurrentCulture, out val))
			{
				return val;
			}
			else
			{
				return def;
			}
		}

		public static double ToDouble(this string str, double def)
		{
			double val;
			if(double.TryParse(str.Replace("%", String.Empty), NumberStyles.Currency, CultureInfo.CurrentCulture, out val))
			{
				return val;
			}
			else
			{
				return def;
			}
		}

		public static DateTime? ToDateTime(this string str, DateTime? def)
		{
			DateTime result;
			if(DateTime.TryParse(str, out result))
			{
				return result;
			}
			else
			{
				return def;
			}
		}
	}
}