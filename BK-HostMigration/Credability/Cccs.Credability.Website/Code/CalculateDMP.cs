﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Credability.Certificates;
using StructureMap;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Credability.Website
{
    public class CalculateDMP
    {
        private readonly List<CreditorDmpPayment> dmpProposedList = new List<CreditorDmpPayment>();
        private readonly List<CreditorDmpPayment> creditorPayments = new List<CreditorDmpPayment>();
        private readonly UserDebtListings[] userDebtListing;

        const decimal defaultDmpPrincipalPayment = .0225m;
        const decimal defaultDmpInterestRate = .09m;

        public CalculateDMP(int ClientNumber)
        {

            userDebtListing = App.Credability.UserDebtListingsGet(ClientNumber);
            IDmpCalculationService service = ObjectFactory.GetInstance<IDmpCalculationService>();

            foreach (var item in userDebtListing)
            {
                var creditorPayment = new CreditorDmpPayment()
                {
                    Balance = ((decimal)item.CreditorBal).CeilingByDecimals(2),
                    MonthlyPayment = ((decimal)item.CreditorPayments).CeilingByDecimals(2),
                    MonthlyInterestRate = (decimal)item.CreditorIntrate / 100 / 12

                };
                creditorPayments.Add(creditorPayment);
            }


            var DmpProposed = new CreditorDmpPayment
             {
                 MonthlyPayment = MonthlyDMPPayment,
                 Balance = Balance,
                 MonthlyInterestRate = defaultDmpInterestRate / 12m,
                 MonthsToRepay = 0

             };
            dmpProposedList.Add(DmpProposed);
            service.CalculateMonthsToRepay(creditorPayments);
            service.CalculateMonthsToRepay(dmpProposedList);

        }

        public decimal Balance
        {
            get
            {
                return (decimal)(from c in userDebtListing select c.CreditorBal).Sum();
            }
        }
        public bool isDMP
        {
            get
            {
                return (TotalDMPPeriod < TotalPeriod);

            }
        }

        public decimal MonthlyPayment
        {
            get
            {
                return creditorPayments.Sum(x => x.MonthlyPayment);
            }
        }

        public decimal TotalPayment
        {
            get
            {
                return MonthlyPayment * creditorPayments.Max(x => x.MonthsToRepay);
            }
        }

        public int TotalPeriod
        {
            get
            {
                return creditorPayments.Max(x => x.MonthsToRepay); ;
            }
        }

        public decimal MonthlyDMPPayment
        {
            get
            {
                return ((decimal)Balance * defaultDmpPrincipalPayment).CeilingByDecimals(2); ;
            }
        }

        public int TotalDMPPeriod
        {
            get
            { return dmpProposedList.Max(x => x.MonthsToRepay); }
        }

        public int TotalDMPPeriodDifference
        {
            get
            { return TotalPeriod - TotalDMPPeriod; }
        }

        public decimal MonthlyDMPSaved
        {
            get
            {
                return TotalPayment - (MonthlyDMPPayment * TotalDMPPeriod);
            }
        }

        public decimal InterestDMPSaved
        {
            get
            {
                decimal interestPaidProposed = (dmpProposedList[0].MonthlyPayment *
                    TotalDMPPeriod) -Balance;
                decimal interestPaid = TotalPayment - Balance;
                return interestPaid - interestPaidProposed;
            }
        }



    }

}