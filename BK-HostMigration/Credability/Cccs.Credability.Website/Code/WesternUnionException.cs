﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website
{
    public class WesternUnionException:ApplicationException
    {

        public WesternUnionException(string message)
            : base(message)
        {
        }
    }
}