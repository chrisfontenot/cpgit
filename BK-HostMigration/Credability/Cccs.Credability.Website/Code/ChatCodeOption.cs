﻿namespace Cccs.Credability.Website
{
	public enum ChatCodeOption
	{
		NoChat = 0,
		Session1 = 1,
		Session2,
		Session3,
        UnnumberedChat
	}
}