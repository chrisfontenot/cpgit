﻿using System;
using Cccs.Web;

// make sure there are RedirectOnPrevious, RedirectOnContinue atributes in the UserControl tag of aspx page.
// also make sure there is base.Page_Load(sender, e); in Page_Load of inherited UaerControl.
namespace Cccs.Credability.Website.Code
{
	public class CounselingUcBase : System.Web.UI.UserControl
	{
		/// <summary>
		/// the page to continue to insted of defualt
		/// </summary>
		public string PageContinue = String.Empty;
		/// <summary>
		/// the privious page insted of defualt
		/// </summary>
		public string PagePrevious = String.Empty;
		//public string PageContinue { get; set; }
		//public string PagePrevious{get;set;}

		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				SessionState.UserPageArrivalTime = DateTime.Now;
			}
		}

		/// <summary>
		/// redirects to PageNext
		/// </summary>
		/// <param name="PageNext">Url to navagate to</param>
		protected void NavigatePage(string PageNext)
		{
			CommonFunction.AddUpdateTimeStampvalue(SessionState.ClientNumber.Value, string.Empty);
			Response.Redirect(PageNext);
		}

		/// <summary>
		/// Redirects to defualt Previous page or value of PagePrevious 
		/// </summary>
		protected void NavigatePrevious()
		{
			if(!PagePrevious.IsNullOrWhiteSpace())
				NavigatePage(PagePrevious);
			else
				NavigatePage(RedirectOnPrevious);
		}

		/// <summary>
		/// Redirects to defualt Continue page or value of PageContinue 
		/// </summary>
		protected void NavigateContinue()
		{
			if(!Request.QueryString["back"].IsNullOrWhiteSpace())
				NavigatePage(Request.QueryString["back"] + ".aspx");
			else if(!PageContinue.IsNullOrWhiteSpace())
				NavigatePage(PageContinue);
			else
				NavigatePage(RedirectOnContinue);
		}

		/// <summary>
		/// redirects to MyAccount
		/// </summary>
		protected void NavigateExit()
		{
			NavigatePage(CommonFunction.GetThankYouCoreSiteUrl());
		}

		/// <summary>
		/// submits client to counslors queue
		/// </summary>
		protected void SubmitToQueue()
		{
			SubmitToQueue(false);
		}

		/// <summary>
		/// submits client to counslors queue
		/// </summary>
		protected void SubmitToQueue(bool IsComplete)
		{
			CommonFunction.SendToCounselor(IsComplete);
            if (App.WebsiteCode == Cccs.Identity.Website.DMP) {
                if (IsComplete)
                {
                    NavigatePage("ThankYou.aspx");
                }
                else {
                    NavigatePage("ScheduleCall.aspx");
                }
            }
				
		}

		/// <summary>
		/// the defualt continue
		/// </summary>
		public string RedirectOnContinue
		{
			get
			{
				return ViewState.ValueGet<string>("RedirectOnContinue");
			}

			set
			{
				ViewState.ValueSet("RedirectOnContinue", value);
			}
		}

		/// <summary>
		/// the defualt privous
		/// </summary>
		public string RedirectOnPrevious
		{
			get
			{
				return ViewState.ValueGet<string>("RedirectOnPrevious");
			}

			set
			{
				ViewState.ValueSet("RedirectOnPrevious", value);
			}
		}

		/// <summary>
		/// the name of this page.
		/// </summary>
		public string ReturnTo
		{
			get
			{
				return ViewState.ValueGet<string>("ReturnTo");
			}

			set
			{
				ViewState.ValueSet("ReturnTo", value);
			}
		}
	}
}