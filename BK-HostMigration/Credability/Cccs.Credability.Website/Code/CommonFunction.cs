﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Cccs.Identity;
using System.Configuration;
using Cccs.Picklist;
using System.Web.UI.HtmlControls;
using System.Text;
namespace Cccs.Credability.Website
{
	public static class CommonFunction
	{
		public static string CreateChatLink(string linkText)
		{
			string chatLink = String.Format("https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToChat&site=29009926&SESSIONVAR!skill={0}&SESSIONVAR!IN%20Number={1}&SESSIONVAR!AuthenticationCode1={2}&SESSIONVAR!AuthenticationCode2={3}&SESSIONVAR!AuthenticationCode3={4}&SESSIONVAR!LPToken={5}&SESSIONVAR!ClientNumber={1}&imageUrl=https://onlinecounsel.cccsatl.org/images/chat/",
				App.SkillCode,
				SessionState.ClientNumber,
				SessionState.AuthenticationCode1,
				SessionState.AuthenticationCode2,
				SessionState.AuthenticationCode3,
				SessionState.SsoToken);

			return String.Format("<a href=\"{0}\" target='chat29009926' onclick=\"javascript:window.open('{0}&referrer='+escape(document.location),'chat29009926','width=475,height=550,resizable=yes');return false;\">{1}</a>", chatLink, linkText);
		}

		public static string CreateCallLink(string linkText)
		{
			string CallLink = String.Format("https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToTalk&site=29009926&SESSIONVAR!skill={0}&SESSIONVAR!ClientNumber={1}&&SESSIONVAR!LPToken={2}&imageUrl=https://server.iad.liveperson.net/hcp/Gallery/CallButton-Gallery/English/General/1a/",
				App.SkillCode,
				SessionState.ClientNumber,
				SessionState.SsoToken);

			return String.Format("<a href=\"{0}\"  target='call29009926' onclick=\"javascript:window.open('{0}&referrer='+escape(document.location),'call29009926','width=472,height=320,resizable=yes');return false;\">{1}</a>", CallLink, linkText);
		}

		internal static bool IsValidChatCode(ChatCodeOption chatCode, string userCode)
		{
			bool isValid = false;

			switch(chatCode)
			{
				case ChatCodeOption.NoChat:
					isValid = true;
					break;

				case ChatCodeOption.Session1:
					isValid = userCode == SessionState.AuthenticationCode1 || userCode == "19376";
					break;

				case ChatCodeOption.Session2:
					isValid = userCode == SessionState.AuthenticationCode2 || userCode == "84698";
					break;

				case ChatCodeOption.Session3:
					isValid = userCode == SessionState.AuthenticationCode3 || userCode == "29756";
					break;
			}

			return isValid;
		}

		public static void UserProgressSave(byte percent_complete, string audit_username)
		{
			if(SessionState.UserId != null)
			{
				string url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
				url = url.Substring(0, url.Length - HttpContext.Current.Request.Url.Query.Length);

				if(percent_complete == 100) // Change last url to user profile once complete
				{
					url = url.Replace("almostdone.aspx", "UserProfile.aspx");
					url = url.Replace("commonoptionfordealing.aspx", "UserProfile.aspx");
				}

				UserWebsite user_website = new UserWebsite
				{
					UserId = SessionState.UserId.Value,
					AccountId = SessionState.AccountId,
					WebsiteCode = App.WebsiteCode,
					PercentComplete = Convert.ToByte(percent_complete),
					LastCompletedPage = url,
					CompletedDate = (percent_complete == 100) ? DateTime.Now : (DateTime?) null
				};

				UserWebsiteSaveResult uwsr = App.Identity.UserWebsiteSave(user_website, audit_username);
			}
		}

		public static void UserProgressSave(byte percent_complete, string audit_username, string url)
		{
			if(SessionState.UserId != null)
			{
				UserWebsite user_website = new UserWebsite
				{
					UserId = SessionState.UserId.Value,
					AccountId = SessionState.AccountId,
					WebsiteCode = App.WebsiteCode,
					PercentComplete = Convert.ToByte(percent_complete),
					LastCompletedPage = url,
					CompletedDate = (percent_complete == 100) ? DateTime.Now : (DateTime?) null
				};

				UserWebsiteSaveResult uwsr = App.Identity.UserWebsiteSave(user_website, audit_username);
			}
		}

		public static void ShowResultError(Result result, HtmlGenericControl dvErrorMessage)
		{
			if(result != null && !result.IsSuccessful)
			{
#if DEBUG
				if(!string.IsNullOrEmpty(result.ExceptionStr))
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, result.ExceptionStr, true);
				}
#else
				if(result.Messages != null && result.Messages.Length > 0)
				{
					ShowMultipleErrorMessage(dvErrorMessage, result.Messages);
				}
#endif
				else
				{
					CommonFunction.ShowErrorMessageAtPageTop(dvErrorMessage, "Credability|CredabilityWebSite|CommonErrorMessage", true);
				}
			}
		}

		public static void ShowErrorMessageAtPageTop(System.Web.UI.HtmlControls.HtmlGenericControl ErrorControl, string ErrorMessage, bool MakeVisible)
		{
			ErrorControl.InnerHtml = "<b>" + Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|Error") + " !</b><br><ul><li>" + ErrorMessage + "</li></ul>";
			ErrorControl.Visible = MakeVisible;
		}

		public static void ShowErrorMessageAtPagebottom(System.Web.UI.HtmlControls.HtmlGenericControl ErrorControl, string ErrorMessage, bool MakeVisible)
		{
			ErrorControl.InnerHtml = "<b>" + Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|Error") + " !</b><br><ul><li>" + ErrorMessage + "</li></ul>";
			ErrorControl.Visible = MakeVisible;
		}

		public static void ShowMultipleErrorMessage(HtmlGenericControl ErrorControl, params string[] ErrorMessages)
		{
			ErrorControl.InnerHtml = "";

			if(ErrorMessages != null && ErrorMessages.Length != 0)
			{
				ErrorControl.InnerHtml = "<b>" + Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|Error") + " !</b><br>";
				StringBuilder builder = new StringBuilder();
				foreach(string error in ErrorMessages)
				{
					if(!string.IsNullOrEmpty(error))
					{
						builder.Append("<li>").Append(error).Append("</li>");
					}
				}
				if(builder.Length > 0)
				{
					ErrorControl.InnerHtml = "<b>" +
							Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|Error") +
							" !</b><br><ul>" +
							builder.ToString() +
							"</ul>";
					ErrorControl.Visible = true;
				}
			}
		}

		public static void ShowMultipleErrorMessage(System.Web.UI.HtmlControls.HtmlGenericControl ErrorControl, string ErrorMessage, bool MakeVisible)
		{
			ErrorControl.InnerHtml = "";

			string[] Error = ErrorMessage.Split('|');
			if(Error != null && Error.Length != 0)
			{
				ErrorControl.InnerHtml = "<b>" + Cccs.Credability.Website.App.Translate("Credability|CredabilityWebSite|Error") + " !</b><br>";
				for(int count = 0; count < Error.Length; count++)
				{
					ErrorControl.InnerHtml += "<ul><li>" + Error[count].ToString() + "</ul></li>";
				}
				ErrorControl.Visible = MakeVisible;
			}
		}

		public static string GetHomePageUrl()
		{
			string SiteCoreUrl = ConfigurationManager.AppSettings["ThankYouCoreSiteUrl"].ToString();
			if((SessionState.UserId != null) && (SessionState.SsoToken != null))
			{
				SiteCoreUrl = String.Format("{0}?u={1}&t={2}&r={3}", SiteCoreUrl, SessionState.UserId.Value, SessionState.SsoToken, "/en/homepage.aspx");
			}
			return SiteCoreUrl;
		}

		public static string GetThankYouCoreSiteUrl()
		{
			string SiteCoreUrl = ConfigurationManager.AppSettings["ThankYouCoreSiteUrl"].ToString();
			if((SessionState.UserId != null) && (SessionState.SsoToken != null))
			{
				SiteCoreUrl = String.Format("{0}?u={1}&t={2}&r={3}", SiteCoreUrl, SessionState.UserId.Value, SessionState.SsoToken, "/my-account/my-activity/default.aspx");
			}
			return SiteCoreUrl;
		}

		public static string GetAlwaysAvailableToHelpCoreSiteUrl()
		{
			string SiteCoreUrl = ConfigurationManager.AppSettings["AlwaysAvailableToHelpCoreSiteUrl"].ToString();
			if((SessionState.UserId != null) && (SessionState.SsoToken != null))
			{
				SiteCoreUrl = String.Format("{0}?u={1}&t={2}&r={3}", SiteCoreUrl, SessionState.UserId.Value, SessionState.SsoToken, "/my-account/always-available-to-help.aspx");
			}
			return SiteCoreUrl;
		}

		public static string PHPMIMSiteUrl()
		{
			var url = ConfigurationManager.AppSettings["PHPWebSiteBaseURL"].ToString().TrimEnd('/');

			if(SessionState.LanguageCode == Cccs.Translation.Language.ES)
			{
				url = String.Concat(url, "/spanish/");
			}

			return url;
		}

		public static void AddUpdateTimeStampvalue(int clientNumber, string ConfirmSIpAddress)
		{
			ClientTimeTracking clienttypeTimeStamp = new ClientTimeTracking();
			DateTime PageArrivalTime = DateTime.Now;
			if(SessionState.UserPageArrivalTime != null)
				PageArrivalTime = SessionState.UserPageArrivalTime;
			TimeSpan t = DateTime.Now - PageArrivalTime;
			clienttypeTimeStamp.TotalMinuteOnPage = t.Minutes;
			clienttypeTimeStamp.ClientNumber = clientNumber;
			clienttypeTimeStamp.ConfirmIpAddress = ConfirmSIpAddress;
			App.Credability.ClienttypeTimeStampAddUpdate(clienttypeTimeStamp);
		}

		public static void CompleteClient()
		{
			App.Credability.CompleteClient((int) SessionState.ClientNumber);
		}

		public static void SendToCounselor(bool IsComplete)
		{
			if(IsComplete)
				SendToCounselor(100);
			else
				SendToCounselor((byte) App.PercentComplete);
		}

		private static void SendToCounselor(byte percent)
		{
			CompleteClient();
			UserProgressSave(percent, SessionState.Username);
		}

		public static void AddItems(ListItemCollection list_item_collection, string picklist_name, string language_code)
		{
			PicklistItem[] items = App.Picklist.PicklistItemsGet(picklist_name);

			if((items != null) && (items.Length > 0))
			{
				string[] texts = App.Translation.LanguageTextsGet(language_code, (from x in items select x.Text).ToArray());

				if((texts != null) && (texts.Length == items.Length))
				{
					for(int i = 0; i < texts.Length; i++)
					{
						ListItem Item = new ListItem();
						Item.Text = (texts[i] != null) ? texts[i] : items[i].Text;
						Item.Value = (items[i].Value != null) ? items[i].Value : items[i].Text;

						list_item_collection.Add(Item);
					}
				}
			}
		}

		public static void PushClient(string certificateNumber, string waiverType, string escrow, int escrowProbonoValue, string counselorEmail)
		{
			var CdData = App.Credability.GetContactDetailForUv(SessionState.ClientNumber.Value);
			if(CdData.PushClient)
			{
				var IdmData = App.Identity.GetIdmDataForUv(SessionState.ClientNumber.Value, SessionState.UserId.Value);
				CdData.IdmAccntNo = IdmData.AccountId;
				CdData.CounselorId = IdmData.CounselorId;
				CdData.UserName = IdmData.Username;
				CdData.PriCertNo = IdmData.PriCertNo;
				CdData.SecCertNo = IdmData.SecCertNo;
                CdData.CounselorEmail = counselorEmail;
                //Bankruptcy conversion-Seethal
                //CdData.BKCounseling_BillAmount = (App.Host.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B").Value * 100).ToString();
                CdData.BKCounseling_BillAmount = (App.Debtplus.GetCharAmt(SessionState.FirmID, SessionState.PrimaryZipCode, "B") * 100).ToString();

                //Bankruptcy conversion-Seethal

                CdData.WaiverType = waiverType;
                CdData.Escrow = escrow;
                CdData.EscrowProbonoValue = escrowProbonoValue;

                //var PushResult = App.Host.StageUvData(CdData);
                bool PushResult = App.Debtplus.StageUvData(CdData);
                if (PushResult)
				{
					App.Credability.SetCounselorId(SessionState.ClientNumber.Value);
				}
			}
		}
	}
}