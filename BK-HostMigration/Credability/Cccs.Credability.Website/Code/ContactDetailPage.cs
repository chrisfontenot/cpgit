﻿using System;

namespace Cccs.Credability.Website
{
	public class ContactDetailPage : DefaultPage
	{
		private string UserPageArrivalViewState = "UserPageArrival";
		protected DateTime? UserPageArrival
		{
			set { ViewState[UserPageArrivalViewState] = value; }
			get
			{
				if(ViewState[UserPageArrivalViewState] == null)
					return null;

				return Convert.ToDateTime(ViewState[UserPageArrivalViewState]);
			}
		}

		protected override object SaveViewState()
		{
			UserPageArrival = DateTime.Now;

			return base.SaveViewState();
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);

			if(UserPageArrival.HasValue)
			{
				UpdateTimeSpent(SessionState.ClientNumber.Value, UserPageArrival.Value);
			}
		}

		protected static void UpdateTimeSpent(int clientNumber, DateTime userPageArrival)
		{
			var timespan = DateTime.Now.Subtract(userPageArrival);

			var timestamp = new ClientTimeTracking();
			timestamp.ClientNumber = clientNumber;
			timestamp.TotalMinuteOnPage = timespan.Minutes;

			App.Credability.ClienttypeTimeStampAddUpdate(timestamp);
		}

		protected static void UpdateTimeSpent(int clientNumber, string ipAddress, DateTime userPageArrival)
		{
			var timespan = DateTime.Now.Subtract(userPageArrival);

			var timestamp = new ClientTimeTracking();
			timestamp.ClientNumber = clientNumber;
			timestamp.ConfirmIpAddress = ipAddress;
			timestamp.TotalMinuteOnPage = timespan.Minutes;

			App.Credability.ClienttypeTimeStampAddUpdate(timestamp);
		}
	}
}