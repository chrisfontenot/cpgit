﻿using System;
using System.Web;

namespace Cccs.Credability.Website
{
    public class DefaultPage : System.Web.UI.Page
    {
        public void ShowLanguageCodes()
        {
            if (HttpContext.Current.Request.QueryString["ShowLanguageCodes"] != null)
            {
                if (HttpContext.Current.Request.QueryString["ShowLanguageCodes"].ToString() == "1")
                {
                    SessionState.MustShowLanguageCodes = true;
                }

                if (HttpContext.Current.Request.QueryString["ShowLanguageCodes"].ToString() == "0")
                {
                    SessionState.MustShowLanguageCodes = false;
                }
            }
        }
    }
}