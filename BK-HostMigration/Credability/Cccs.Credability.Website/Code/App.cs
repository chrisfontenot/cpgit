﻿using System;
using System.Web;
using Cccs.Translation;
using Cccs.Identity;
using Cccs.Geographics;
using Cccs.Configuration;
using Cccs.Picklist;
//using Cccs.Host;
using System.Configuration;
using Cccs.Vanco;
using System.Diagnostics;
using System.Text;
using System.Xml;
using Cccs.Debtplus;
using Cccs.Debtplus.Data;
using MMI.FirstData;
using MMI.FirstData.Impl;

namespace Cccs.Credability.Website
{
	public static class App
	{
		private static IConfiguration m_configuration;
		private static ITranslation m_translation;
		private static IIdentity m_identity;
		private static ICredability m_credability;
		private static IGeographics m_Geographics;
		//private static IHost m_Host;
        private static IDebtplus m_Debtplus;
		private static IPicklist m_Picklist;
		private static FeeWaiverManager m_feeWaiverManager;
		private static ZipCodeManager m_zipCodeManager;  
        //private static ICredability m_credabilityCache;
        private static IEPay m_payment;
    
		public const string CurrencyFormat = "C0";
    
		static App()
		{
			// PickList
			//			
			m_Picklist = new Cccs.Picklist.Impl.Picklist();

			// Identity
			//
			m_configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());
			m_translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());
			m_Geographics = new Cccs.Geographics.Impl.Geographics();
			m_identity = new Cccs.Identity.Impl.Identity(m_configuration, m_translation, m_Geographics);

			// Host
			//
            // Bankruptcy Service
			//m_Host = new Cccs.Host.Impl.Host(m_identity);

            // Debtplus
            //
            m_Debtplus = new Cccs.Debtplus.Impl.Debtplus(m_identity);

			// Vanco: English
			// 
			string vanco_uri_en = ConfigurationManager.AppSettings["VancoUriEn"];
			string vanco_login_en = ConfigurationManager.AppSettings["VancoLoginEn"];
			string vanco_password_en = ConfigurationManager.AppSettings["VancoPasswordEn"];
			string vanco_client_id_en = ConfigurationManager.AppSettings["VancoClientIdEn"];
			string vance_fund_en = ConfigurationManager.AppSettings["VancoFundEn"];
			IVanco vanco_en = new Cccs.Vanco.Impl.Vanco(vanco_uri_en, vanco_login_en, vanco_password_en, vanco_client_id_en, vance_fund_en);

			// Vanco: Spanish
			// 
			string vanco_uri_es = ConfigurationManager.AppSettings["VancoUriEs"];
			string vanco_login_es = ConfigurationManager.AppSettings["VancoLoginEs"];
			string vanco_password_es = ConfigurationManager.AppSettings["VancoPasswordEs"];
			string vanco_client_id_es = ConfigurationManager.AppSettings["VancoClientIdEs"];
			string vance_fund_es = ConfigurationManager.AppSettings["VancoFundEs"];
			IVanco vanco_es = new Cccs.Vanco.Impl.Vanco(vanco_uri_es, vanco_login_es, vanco_password_es, vanco_client_id_es, vance_fund_es);

			// BKE Vanco: English
			// 
			string bke_vanco_uri_en = ConfigurationManager.AppSettings["BkeVancoUriEn"];
			string bke_vanco_login_en = ConfigurationManager.AppSettings["BkeVancoLoginEn"];
			string bke_vanco_password_en = ConfigurationManager.AppSettings["BkeVancoPasswordEn"];
			string bke_vanco_client_id_en = ConfigurationManager.AppSettings["BkeVancoClientIdEn"];
			string bke_vanco_fund_en = ConfigurationManager.AppSettings["BkeVancoFundEn"];
			IVanco bke_vanco_en = new Cccs.Vanco.Impl.Vanco(bke_vanco_uri_en, bke_vanco_login_en, bke_vanco_password_en, bke_vanco_client_id_en, bke_vanco_fund_en);

			// BKE Vanco: Spanish
			// 
			string bke_vanco_uri_es = ConfigurationManager.AppSettings["BkeVancoUriEs"];
			string bke_vanco_login_es = ConfigurationManager.AppSettings["BkeVancoLoginEs"];
			string bke_vanco_password_es = ConfigurationManager.AppSettings["BkeVancoPasswordEs"];
			string bke_vanco_client_id_es = ConfigurationManager.AppSettings["BkeVancoClientIdEs"];
			string bke_vanco_fund_es = ConfigurationManager.AppSettings["BkeVancoFundEs"];
			IVanco bke_vanco_es = new Cccs.Vanco.Impl.Vanco(bke_vanco_uri_es, bke_vanco_login_es, bke_vanco_password_es, bke_vanco_client_id_es, bke_vanco_fund_es);

            // Credability
            //
            // Bankruptcy Conversion
            //m_credability = new Cccs.Credability.Impl.Credability(m_identity, m_Host, vanco_en, vanco_es, bke_vanco_en, bke_vanco_es);
            m_credability = new Cccs.Credability.Impl.Credability(m_identity, m_Debtplus, vanco_en, vanco_es, bke_vanco_en, bke_vanco_es);
            m_feeWaiverManager = new FeeWaiverManager();
			m_zipCodeManager = new ZipCodeManager();
          
            //FirstData
            m_payment = new EPay();

           
		}

        public static bool SetEscrowInfo() {
            var toReturn = true;
            String Escrow = "", FirmName = "", HostServiceError = "";
            //Result<XmlDocument> result = null;
            EscrowResult result = null;
            // This needs to be update from Host Services once the Services are ready to integrate.
            if (!string.IsNullOrEmpty(SessionState.FirmID))
            {
                int firmId;
                if (Int32.TryParse(SessionState.FirmID, out firmId))
                {

                    //result = App.Host.GetEscrow(firmId);
                    result = App.Debtplus.GetEscrow(firmId, App.WebsiteCode);
                }
            }
            
            if (result != null)
            {
                /*XmlNodeList nodeListError = result.Value.SelectNodes("//Error") as XmlNodeList;
                if (nodeListError != null && nodeListError.Count != 0)
                {
                    HostServiceError = nodeListError.Item(0).InnerText;
                    if (HostServiceError.Contains("This Record was not found")) {
                        toReturn = false;
                    }
                }

                if (HostServiceError == "")
                {
                    XmlNodeList nodeListEscrow = result.Value.SelectNodes("//Escrow") as XmlNodeList;
                    if (nodeListEscrow != null && nodeListEscrow.Count != 0)
                    {
                        Escrow = nodeListEscrow.Item(0).InnerText;
                    }

                    SessionState.Escrow6 = "N";

                    if (Escrow == "1") SessionState.Escrow = "Y";
                    else if (Escrow == "2" && App.WebsiteCode == Cccs.Identity.Website.BKC) SessionState.Escrow = "Y";
                    else if (Escrow == "3" && App.WebsiteCode == Cccs.Identity.Website.BKDE) SessionState.Escrow = "Y";
                    else if (Escrow == "6")
                    {
                        SessionState.Escrow = "Y";
                        SessionState.Escrow6 = "Y";
                    }
                    else
                        SessionState.Escrow = "N";

                    XmlNodeList nodeListFirmName = result.Value.SelectNodes("//Name") as XmlNodeList;
                    if (nodeListFirmName != null && nodeListFirmName.Count != 0)
                    {
                        SessionState.FirmName = nodeListFirmName.Item(0).InnerText.Trim();
                    }
                }*/

                SessionState.EscrowProbonoValue = result.EscrowProBono;
                SessionState.Escrow = result.Escrow;

                if (result.IsLegalAid)
                {
                    SessionState.Escrow6 = "Y";
                }

                /*if (result.Escrow == "Y")
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 2 && App.WebsiteCode == Cccs.Identity.Website.BKC)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 3 && App.WebsiteCode == Cccs.Identity.Website.BKDE)
                {
                    SessionState.Escrow = "Y";
                }
                else if (result.Escrow == 6)
                {
                    SessionState.Escrow = "Y";
                    SessionState.Escrow6 = "Y";
                }
                else
                {
                    SessionState.Escrow = "N";
                }*/

                SessionState.FirmName = result.FirmName;
            }

            return toReturn;
        }

		public static int CalculateAge(DateTime dob)
		{
			int age = DateTime.Today.Year - dob.Year;

			if(dob.AddYears(age) > DateTime.Today)
			{
				age--;
			}

			return age;
		}

		public static ITranslation Translation
		{
			get { return m_translation; }
		}

		public static IIdentity Identity
		{
			[DebuggerStepThrough]
			get { return m_identity; }
		}

		public static IGeographics Geographics
		{
			[DebuggerStepThrough]
			get { return m_Geographics; }
		}

		public static ICredability Credability
		{
			[DebuggerStepThrough]
			get { return m_credability; }
		}

        public static IEPay EPay
        {
            [DebuggerStepThrough]
            get { return m_payment; }
        }

		public static FeeWaiverManager FeeWaiver
		{
			[DebuggerStepThrough]
			get { return m_feeWaiverManager; }
		}

		public static ZipCodeManager ZipCodes
		{
			[DebuggerStepThrough]
			get { return m_zipCodeManager; }
		}

		//public static IHost Host
		//{
		//	[DebuggerStepThrough]
		//	get { return m_Host; }
		//}

        public static IDebtplus Debtplus
        {
            [DebuggerStepThrough]
            get
            { return m_Debtplus; }
        }

        public static IPicklist Picklist
		{
			[DebuggerStepThrough]
			get { return m_Picklist; }
		}

        public static bool AuthorizationChatCodeState
        {
            [DebuggerStepThrough]
            get
            {
                return m_credability.isChatCodeActive(App.WebsiteCode, SessionState.LanguageCode);             
               
            }

        }

		public static string Translate(string language_text_code)
		{
			if(SessionState.MustShowLanguageCodes)
			{
				return "<span style=\"color: red;\">[" + language_text_code + "]</span>" + m_translation.LanguageTextGet(SessionState.LanguageCode, language_text_code);
			}
			else
			{
				return m_translation.LanguageTextGet(SessionState.LanguageCode, language_text_code);
			}
		}

		public static string Translate(string language_text_code, string arg1)
		{
			return string.Format(m_translation.LanguageTextGet(SessionState.LanguageCode, language_text_code), arg1);
		}

		public static string DateString(DateTime? date)
		{
			return (date != null) ? DateString(date.Value) : string.Empty;
		}

		public static string DateString(DateTime date)
		{
			return (date != DateTime.MinValue) ? date.ToString("MM/dd/yyyy") : string.Empty;
		}

        private static string SniffUrl() {
          
            //string url = HttpContext.Current.Request.Url.AbsolutePath.ToUpper();
            string url = HttpContext.Current.Request.Url.ToString().ToUpper();

            if (url.Contains("GETREQUIREDFEEINFORMATION.ASPX".ToUpper()))
            {
                url = HttpContext.Current.Request["redirect"].ToUpper();
            }

            return url;
        }

		public static string WebsiteCode
		{
			get
			{
				string website_code = null;

                string url = SniffUrl();

				if(url.Contains("/BCH/"))
				{
					website_code = Cccs.Identity.Website.BCH;
				}
                else if (url.Contains("/REC/"))
                {
                    website_code = Cccs.Identity.Website.REC;
                }
				else if(url.Contains("/HOUSINGONLY/"))
				{
					website_code = Cccs.Identity.Website.HUD;
				}
				else if(url.Contains("/BKCOUNSELING/"))
				{
					website_code = Cccs.Identity.Website.BKC;
				}
				else if(url.Contains("/BKEDUCATION/"))
				{
					website_code = Cccs.Identity.Website.BKDE;
				}
				else if(url.Contains("/DMPONLY"))
				{
					website_code = Cccs.Identity.Website.DMP;
				}
				else if(url.Contains("/PREPURCHASE/"))
				{
					website_code = Cccs.Identity.Website.PRP;
				}
				else if(url.Contains("/RVM/"))
				{
					website_code = Cccs.Identity.Website.RVM;
				}
				else if(url.Contains("/CLIENTACCESS/"))
				{
					website_code = Cccs.Identity.Website.CA;
				}
				else
				{
					throw new NotImplementedException("App.WebsiteCode");
				}

				return website_code;
			}
		}

		public static string AccountTypeCode
		{
			get
			{
				string account_type_code = null;

                string url = SniffUrl();

                if (url.Contains("/REC/") || url.Contains("/BCH/") || url.Contains("/RVM/") || url.Contains("/HOUSINGONLY/") || url.Contains("/PREPURCHASE/") || url.Contains("/DMPONLY"))
				{
					account_type_code = Cccs.Identity.AccountType.CAM;
				}
				else if(url.Contains("/BKCOUNSELING/"))
				{
					account_type_code = Cccs.Identity.AccountType.BR_CAM;
				}
				else if(url.Contains("/BKEDUCATION/"))
				{
					account_type_code = Cccs.Identity.AccountType.WS_CAM;
				}
				else if(url.Contains("/CLIENTACCESS/"))
				{
					account_type_code = Cccs.Identity.AccountType.CM;
				}
				else
				{
					throw new NotImplementedException("App.WebsiteCode");
				}

				return account_type_code;
			}
		}

		public static string SkillCode
		{
			get
			{
				string skillCode = null;

				if(SessionState.LanguageCode == Cccs.Translation.Language.ES && ClientType == Constants.ClientType.RVM)
				{
					skillCode = "Spanish%20Housing";
				}
				if(SessionState.LanguageCode == Cccs.Translation.Language.ES && ClientType != Constants.ClientType.BKE)
				{
					skillCode = "Spanish%20Internet%20Counselor";
				}
				else
				{
					string clientType = ClientType;
					switch(clientType)
					{
                        case Constants.ClientType.REC:
                            skillCode = Constants.SkillCode.REC;
                            break;
						case Constants.ClientType.BC:
							skillCode = Constants.SkillCode.BC;
							break;
						case Constants.ClientType.BCH:
							skillCode = Constants.SkillCode.BCH;
							break;
						case Constants.ClientType.BK:
							skillCode = Constants.SkillCode.InternetCounselor;
							break;
						case Constants.ClientType.BKE:
							skillCode = Constants.SkillCode.BankruptcyEducation;
							break;
						case Constants.ClientType.HUD:
						case Constants.ClientType.RVM:
							skillCode = Constants.SkillCode.Housing;
							break;
						default:
							skillCode = clientType;
							break;
					}
				}
				return skillCode.Trim();
			}
		}

		public static string ClientType // TODO: This should be determined in some other manner than url sniffing in the ui
		{
			get
			{
				string client_type = null;
                string url = SniffUrl();

				if(url.Contains("/BCH/"))
				{
					client_type = DetermineBchClientType();
				}
                else if (url.Contains("/REC/"))
                {
                    client_type = DetermineRecClientType();
                }
				else if(url.Contains("/BKCOUNSELING/"))
				{
					client_type = "BK";
				}
				else if(url.Contains("/BKEDUCATION/"))
				{
					client_type = Constants.ClientType.BKE;
				}
				else if(url.Contains("/DMPONLY"))
				{
					client_type = "DMP";
				}
				else if(url.Contains("/HOUSINGONLY/"))
				{
					client_type = "HUD";
				}
				else if(url.Contains("/PREPURCHASE/"))
				{
					client_type = "PrP";
				}
				else if(url.Contains("/RVM/"))
				{
					client_type = "RVM";
				}
				else if(url.Contains("/CLIENTACCESS/"))
				{
					client_type = "CA";
				}
				else
				{
					throw new NotImplementedException("App.WebsiteCode");
				}
				return client_type;
			}
			set
			{
				SessionState.ClientType = value;
				int clientNbr = SessionState.ClientNumber.Value;
				if(clientNbr > 0)
				{
					UserContactDetails contact = Credability.UserInfoGet(clientNbr);
					if(contact != null)
					{
						contact.ClientType = value;
					}
					else
					{
						contact = new UserContactDetails
						{
							ClientNumber = clientNbr,
							ClientType = value,
						};
					}
					Credability.UserInformationUpdate(contact);
				}
			}
		}

		private static string DetermineBchClientType()
		{
			string client_type = Constants.ClientType.BC;
			int clientNbr = SessionState.ClientNumber.GetValueOrDefault(0);
			if(clientNbr > 0)
			{
				UserContactDetails contact = Credability.UserInfoGet(clientNbr);
				if(contact != null && !string.IsNullOrEmpty(contact.ClientType.Clean()))
				{
					client_type = contact.ClientType.Trim();
				}
			}
			return client_type;
		}

        private static string DetermineRecClientType()
        {
            string client_type = Constants.ClientType.REC;
            int clientNbr = SessionState.ClientNumber.GetValueOrDefault(0);
            if (clientNbr > 0)
            {
                UserContactDetails contact = Credability.UserInfoGet(clientNbr);
                if (contact != null && !string.IsNullOrEmpty(contact.ClientType.Clean()))
                {
                    client_type = contact.ClientType.Trim();
                }
            }
            return client_type;
        }

		public static string LivePersonVariableScriptBlock(long userId, int clientNumber)
		{
			const string JScriptAssignment = "lpMTagConfig.visitorVar[lpMTagConfig.visitorVar.length]='{0}={1}';\n";

			StringBuilder builder = new StringBuilder();

			var primaryUserDetail = Identity.UserDetailGet(userId, true);
			if(primaryUserDetail != null)
			{
				builder.AppendFormat(JScriptAssignment, "ClientNumber", SessionState.ClientNumber.Value);
				builder.AppendFormat(JScriptAssignment, "LPToken", SessionState.SsoToken);
				builder.AppendFormat(JScriptAssignment, "PrimaryFirstName", primaryUserDetail.FirstName);
				builder.AppendFormat(JScriptAssignment, "PrimaryLastName", primaryUserDetail.LastName);
			}

			var secondaryUserDetail = Identity.UserDetailGet(userId, false);
			if(secondaryUserDetail != null)
			{
				builder.AppendFormat(JScriptAssignment, "SecondaryFirstName", secondaryUserDetail.FirstName ?? "-");
				builder.AppendFormat(JScriptAssignment, "SecondaryLastName", secondaryUserDetail.LastName ?? "-");
			}
			else
			{
				builder.AppendFormat(JScriptAssignment, "SecondaryFirstName", "-");
				builder.AppendFormat(JScriptAssignment, "SecondaryLastName", "-");
			}

			var attorneyInfo = App.Credability.RegistrationBKCounselingGet(clientNumber);
			if(attorneyInfo != null)
			{
				builder.AppendFormat(JScriptAssignment, "AttorneyFirm", attorneyInfo.FIRMname ?? "-");
				builder.AppendFormat(JScriptAssignment, "AttorneyName", attorneyInfo.ATTYname ?? "-");
			}
			else
			{
				builder.AppendFormat(JScriptAssignment, "AttorneyFirm", "-");
				builder.AppendFormat(JScriptAssignment, "AttorneyName", "-");
			}

			var extraDetail = App.Credability.BKCUserDescribeYourSituationExtraQuestGet(clientNumber);
			var housingDetail = App.Credability.UserDecsribeYourSituationHousingOnlyGet(clientNumber);
			var commonOptions = App.Credability.CommonOptionBKCounselingContactDetailsGet(clientNumber);
			var commonOptionsExtra = App.Credability.CommonOptionBKCounselingExtraHudGet(clientNumber);
			if(housingDetail != null)
			{
				builder.AppendFormat(JScriptAssignment, "MainEvent", CheckForNullOrWhitespace(housingDetail.ContactReason));
				builder.AppendFormat(JScriptAssignment, "SecondaryEvent", extraDetail != null ? CheckForNullOrWhitespace(extraDetail.SecEvent) : "-");
				builder.AppendFormat(JScriptAssignment, "Reason", CheckForNullOrWhitespace(housingDetail.ContactComments));
				builder.AppendFormat(JScriptAssignment, "Housing", CheckForNullOrWhitespace(housingDetail.HousingType));

				var loanNumber = (housingDetail.LoanNumber ?? String.Empty).Trim();
				builder.AppendFormat(JScriptAssignment, "MortgageLoanNumber", loanNumber == String.Empty ? "-" : loanNumber);
				builder.AppendFormat(JScriptAssignment, "MortgageCurrent", loanNumber == String.Empty ? "-" : (housingDetail.MortCurrent == 0 ? "No" : "Yes"));

				builder.AppendFormat(JScriptAssignment, "NumberOfMonthsMissed", housingDetail.MosDelinq > 0 ? housingDetail.MosDelinq.ToString("#,##0") : "-");
				builder.AppendFormat(JScriptAssignment, "NumberOfPeopleInHousehold", housingDetail.SizeOfHouseHold > 0 ? housingDetail.SizeOfHouseHold.ToString("#,##0") : "-");
				builder.AppendFormat(JScriptAssignment, "QualifyForAffordableModProgram", CheckForNullOrWhitespace(housingDetail.MAKEHomeAff));
				builder.AppendFormat(JScriptAssignment, "MortgageCompany", CheckForNullOrWhitespace(housingDetail.MortHolder));
				builder.AppendFormat(JScriptAssignment, "InterestType", ShowInterestType(commonOptions.RateType));
				builder.AppendFormat(JScriptAssignment, "InterestRate", housingDetail.MortRate > 0 ? String.Format("{0:##.##}%", housingDetail.MortRate) : "-");
				builder.AppendFormat(JScriptAssignment, "OriginalLoanBalance", housingDetail.OrigBal > 0 ? String.Format("{0:C0}", housingDetail.OrigBal) : "-");
				builder.AppendFormat(JScriptAssignment, "CurrentBalance", housingDetail.Owehome > 0 ? String.Format("{0:C0}", housingDetail.Owehome) : "-");
				builder.AppendFormat(JScriptAssignment, "EstimatedValueOfHome", housingDetail.ValHome > 0 ? String.Format("{0:C0}", housingDetail.ValHome) : "-");
				builder.AppendFormat(JScriptAssignment, "ScheduledMonthlyPayment", housingDetail.MoPmt > 0 ? String.Format("{0:C0}", housingDetail.MoPmt) : "-");
			}
			if(commonOptions != null)
			{
				builder.AppendFormat(JScriptAssignment, "PropertyInForclosure", CheckForNullOrWhitespace(commonOptions.Note4Close));
				builder.AppendFormat(JScriptAssignment, "PropertyForSale", CheckForNullOrWhitespace(commonOptions.Prop4Sale));
				builder.AppendFormat(JScriptAssignment, "EverInRepaymentPlan", CheckForNullOrWhitespace(commonOptions.RepayPlan));
				builder.AppendFormat(JScriptAssignment, "LastSpeakToLender", CheckForNullOrWhitespace(commonOptions.LastContactDate));
				builder.AppendFormat(JScriptAssignment, "LenderDiscussionOutcome", CheckForNullOrWhitespace(commonOptions.LastContactDesc));
				builder.AppendFormat(JScriptAssignment, "SecondLoan", commonOptions.SecServID == 0 ? "No" : "Yes");
				builder.AppendFormat(JScriptAssignment, "SecondLender", CheckForNullOrWhitespace(commonOptions.SecondaryHolder));
				builder.AppendFormat(JScriptAssignment, "SecondLoanBalance", commonOptions.SecondaryAmt > 0 ? commonOptions.SecondaryAmt.ToString("C") : "-");
				builder.AppendFormat(JScriptAssignment, "SecondLoanStatus", CheckForNullOrWhitespace(commonOptions.SecondaryStatus));
			}
			if(commonOptionsExtra != null)
			{
				builder.AppendFormat(JScriptAssignment, "TaxesIncluded", CheckForNullOrWhitespace(commonOptionsExtra.IncludeTax));
				builder.AppendFormat(JScriptAssignment, "InsurancePaymentIncluded", CheckForNullOrWhitespace(commonOptionsExtra.IncludeIns));
			}
			else
			{
				builder.AppendFormat(JScriptAssignment, "TaxesIncluded", "-");
				builder.AppendFormat(JScriptAssignment, "InsurancePaymentIncluded", "-");
			}

			FinanicalSituationDetails FinanicalDetails = App.Credability.UserFinalFinanicalSituationGet(clientNumber);
			if(FinanicalDetails != null)
			{
				builder.AppendFormat(JScriptAssignment, "TotalBalanceOfDebts", FinanicalDetails.AssetsLiabilities.CreditorTotals.Balance > 0 ? FinanicalDetails.AssetsLiabilities.CreditorTotals.Balance.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "TotalPaymentOfDebts", FinanicalDetails.AssetsLiabilities.CreditorTotals.Payments > 0 ? FinanicalDetails.AssetsLiabilities.CreditorTotals.Payments.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "TotalNumberOfDebts", FinanicalDetails.AssetsLiabilities.CreditorTotals.Count > 0 ? FinanicalDetails.AssetsLiabilities.CreditorTotals.Count.ToString("#,##0") : "-");

				builder.AppendFormat(JScriptAssignment, "TotalNetIncome", FinanicalDetails.Income.NetIncomeTotalBoth > 0 ? FinanicalDetails.Income.NetIncomeTotalBoth.ToString(CurrencyFormat) : "-");
                
				builder.AppendFormat(JScriptAssignment, "RentMortgageExpense", FinanicalDetails.MonthlyExpenses.RentMort > 0 ? FinanicalDetails.MonthlyExpenses.RentMort.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "EquitySecondMortExpense", FinanicalDetails.MonthlyExpenses.Mort2nd > 0 ? FinanicalDetails.MonthlyExpenses.Mort2nd.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "HomeMaintenanceExpense", FinanicalDetails.MonthlyExpenses.HomeMaintenance > 0 ? FinanicalDetails.MonthlyExpenses.HomeMaintenance.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "UtilitiesExpense", FinanicalDetails.MonthlyExpenses.Utilities > 0 ? FinanicalDetails.MonthlyExpenses.Utilities.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "PhoneInternetExpense", FinanicalDetails.MonthlyExpenses.Telephone > 0 ? FinanicalDetails.MonthlyExpenses.Telephone.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "FoodOutExpense", FinanicalDetails.MonthlyExpenses.FoodAway > 0 ? FinanicalDetails.MonthlyExpenses.FoodAway.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "FoodInExpense", FinanicalDetails.MonthlyExpenses.Groceries > 0 ? FinanicalDetails.MonthlyExpenses.Groceries.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "AutoPaymentsExpense", FinanicalDetails.MonthlyExpenses.CarPayments > 0 ? FinanicalDetails.MonthlyExpenses.CarPayments.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "AutoInsuranceExpense", FinanicalDetails.MonthlyExpenses.CarInsurance > 0 ? FinanicalDetails.MonthlyExpenses.CarInsurance.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "GasExpense", FinanicalDetails.MonthlyExpenses.CarMaintenance > 0 ? FinanicalDetails.MonthlyExpenses.CarMaintenance.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "PublicTransportExpense", FinanicalDetails.MonthlyExpenses.PublicTransportation > 0 ? FinanicalDetails.MonthlyExpenses.PublicTransportation.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "InsuranceExpense", FinanicalDetails.MonthlyExpenses.Insurance > 0 ? FinanicalDetails.MonthlyExpenses.Insurance.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "MedicalExpense", FinanicalDetails.MonthlyExpenses.MedicalPrescription > 0 ? FinanicalDetails.MonthlyExpenses.MedicalPrescription.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "ChildAlimonyExpense", FinanicalDetails.MonthlyExpenses.ChildSupportAlimony > 0 ? FinanicalDetails.MonthlyExpenses.ChildSupportAlimony.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "EducationExpense", FinanicalDetails.MonthlyExpenses.Education > 0 ? FinanicalDetails.MonthlyExpenses.Education.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "ContributionsExpense", FinanicalDetails.MonthlyExpenses.Contributions > 0 ? FinanicalDetails.MonthlyExpenses.Contributions.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "ClothingExpense", FinanicalDetails.MonthlyExpenses.Clothing > 0 ? FinanicalDetails.MonthlyExpenses.Clothing.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "LaundryExpense", FinanicalDetails.MonthlyExpenses.Laundry > 0 ? FinanicalDetails.MonthlyExpenses.Laundry.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "PersonalExpenses", FinanicalDetails.MonthlyExpenses.PersonalExpenses > 0 ? FinanicalDetails.MonthlyExpenses.PersonalExpenses.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "BeautyBarberExpense", FinanicalDetails.MonthlyExpenses.BeautyBarber > 0 ? FinanicalDetails.MonthlyExpenses.BeautyBarber.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "RecreationExpense", FinanicalDetails.MonthlyExpenses.Recreation > 0 ? FinanicalDetails.MonthlyExpenses.Recreation.ToString(CurrencyFormat) : "-");
                builder.AppendFormat(JScriptAssignment, "ClubExpense", FinanicalDetails.MonthlyExpenses.ClubDues > 0 ? FinanicalDetails.MonthlyExpenses.ClubDues.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "GiftsExpense", FinanicalDetails.MonthlyExpenses.Gifts > 0 ? FinanicalDetails.MonthlyExpenses.Gifts.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "MiscExpense", FinanicalDetails.MonthlyExpenses.Miscellaneous > 0 ? FinanicalDetails.MonthlyExpenses.Miscellaneous.ToString(CurrencyFormat) : "-");
				builder.AppendFormat(JScriptAssignment, "MonthlyExpenseTotal", FinanicalDetails.MonthlyExpenses.ExpensesTotal > 0 ? FinanicalDetails.MonthlyExpenses.ExpensesTotal.ToString(CurrencyFormat) : "-");

				builder.AppendFormat(JScriptAssignment, "CashAtMonthEnd", FinanicalDetails.DisposableIncome.ToString(CurrencyFormat));
			}
			return String.Format("<script language=\"JavaScript1.2\">{0}</script>", builder.ToString());
		}

		private static string ShowMortType(string value)
		{
			switch(value.Trim())
			{
				case "CONV":
					return Cccs.Credability.Website.App.Translate("Conventional");
				case "FHA":
					return Cccs.Credability.Website.App.Translate("FHA");
				case "VA":
					return Cccs.Credability.Website.App.Translate("VA");
				default:
					return Cccs.Credability.Website.App.Translate("None Selected");
			}
		}

		private static string ShowInterestType(string value)
		{
			switch(value.Trim())
			{
				case "F":
					return Cccs.Credability.Website.App.Translate("Fixed");
				case "A":
					return Cccs.Credability.Website.App.Translate("Standard Arm");
				case "I":
					return Cccs.Credability.Website.App.Translate("Interest Only");
				case "O":
					return Cccs.Credability.Website.App.Translate("Option Arm");
				default:
					return Cccs.Credability.Website.App.Translate("None Selected");
			}
		}

		public static int PercentComplete
		{
			get
			{
				int percentage_complete = 5;

				UserWebsite user_website = App.Identity.UserWebsiteGet(SessionState.UserId.Value, App.WebsiteCode);

				if(user_website != null)
				{
					percentage_complete = user_website.PercentComplete;
				}

				return percentage_complete;
			}
		}

		public static bool IsSpanishSupported
		{
			get
			{
				return true;
			}
		}

		private static string CheckForNullOrWhitespace(string candidate)
		{
			return candidate.IsNullOrWhiteSpace() ? "-" : candidate;
		}
	}
}