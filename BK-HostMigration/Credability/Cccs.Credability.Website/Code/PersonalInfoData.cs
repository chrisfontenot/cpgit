﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Identity;
using System.Xml;
using System.Globalization;

namespace Cccs.Credability.Website
{
    public class PersonalInfoData
    {
        public UserDetail PrimaryUserDetail { get; set; }
        public UserDetail SecondUserDetail { get; set; }
        public Address PrimaryUserAddress { get; set; }
        public string PrimaryUserEducation { get; set; }
        public string PrimaryUserEmployment { get; set; }
        public string SecondUserEmployment { get; set; }
        public bool DiscardData { get; set; }
        public bool ValidateAges { get; set; }

        public IEnumerable<string> ValidateData()
        {
            List<string> errs = new List<string>();
            string state_code = App.Geographics.StateCodeGetByZip(PrimaryUserAddress.Zip);
            if (string.IsNullOrEmpty(state_code))
            {
                errs.Add(App.Translate("Credability|UserProfileBCH|IZC"));
            }
            else if (string.Compare(state_code, PrimaryUserAddress.State, true, CultureInfo.InvariantCulture) != 0)
            {
                errs.Add(string.Format(App.Translate("Credability|UserProfileBCH|SEFZI"), PrimaryUserAddress.Zip, state_code));
            }
            if (ValidateAges)
            {
                int age = App.CalculateAge(PrimaryUserDetail.BirthDate.GetValueOrDefault());
                if (age < 13 || age > 108)
                {
                    errs.Add(App.Translate("Credability|UserProfileBCH|PriAge"));
                }
            }
            if (!string.IsNullOrEmpty(SecondUserDetail.FirstName) ||
                    !string.IsNullOrEmpty(SecondUserDetail.LastName))
            {
                if (string.IsNullOrEmpty(SecondUserDetail.LastName))
                {
                    errs.Add(App.Translate("Credability|UserProfileBCH|COLNR"));
                }
                if (string.IsNullOrEmpty(SecondUserDetail.Ssn))
                {
                    errs.Add(App.Translate("Credability|UserProfileBCH|COSSNR"));
                }
                if (!SecondUserDetail.BirthDate.HasValue)
                {
                    errs.Add(App.Translate("Credability|UserProfileBCH|CODOBR"));
                }
                else if (ValidateAges)
                {
                    int age = App.CalculateAge(SecondUserDetail.BirthDate.GetValueOrDefault());
                    if (age < 13 || age > 108)
                    {
                        errs.Add(App.Translate("Credability|UserProfileBCH|CoAge"));
                    }
                }
            }
            return errs;
        }

        public IEnumerable<string> SaveData(long user_id, string auditorName)
        {
            List<string> coll = new List<string>();
            if (!DiscardData)
            {
                SavePrimaryUserDetail(user_id, auditorName);
                if (coll.Count == 0)
                {
                    coll = SavePrimaryUserAddress(user_id, auditorName);
                    if (coll.Count == 0)
                    {
                        coll = SaveSecondUserDetail(user_id, auditorName);
                    }
                }
            }
            return coll;
        }

        public IEnumerable<string> RequestCreditReport(int clientNbr, string clientType, string refCode)
        {
            List<string> errs = new List<string>();

            UserContactDetails user_contact_details = App.Credability.UserInfoGet(SessionState.ClientNumber.Value);

            if (user_contact_details != null)
            {
                //
                /*Result<XmlDocument> result = App.Host.RequestCreditReport
				(
					clientNbr,
					clientType,
					refCode,
					user_contact_details.BirthOfCity,
					user_contact_details.CosignBirthOfCity,
					PrimaryUserDetail.FirstName,
					PrimaryUserDetail.LastName,
					PrimaryUserDetail.Ssn,
					PrimaryUserDetail.BirthDate,
					PrimaryUserAddress.StreetLine1,
					PrimaryUserAddress.City,
					PrimaryUserAddress.State,
					PrimaryUserAddress.Zip,
					SecondUserDetail.FirstName,
					SecondUserDetail.LastName,
					SecondUserDetail.Ssn,
					SecondUserDetail.BirthDate
				);
                
                 /*if (!result.IsSuccessful)
				{
	#if DEBUG
									if (result.Exception != null)
									{
											errs.Add(result.Exception.Message);
									}
	#endif
					if (errs.Count == 0)
					{
						errs.Add(App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
					}*/

                try
                {
                    App.Debtplus.RequestCreditReport
                    (
                        clientNbr,
                        clientType,
                        refCode,
                        user_contact_details.BirthOfCity,
                        user_contact_details.CosignBirthOfCity,
                        PrimaryUserDetail.FirstName,
                        PrimaryUserDetail.LastName,
                        PrimaryUserDetail.Ssn,
                        PrimaryUserDetail.BirthDate,
                        PrimaryUserAddress.StreetLine1,
                        PrimaryUserAddress.City,
                        PrimaryUserAddress.State,
                        PrimaryUserAddress.Zip,
                        SecondUserDetail.FirstName,
                        SecondUserDetail.LastName,
                        SecondUserDetail.Ssn,
                        SecondUserDetail.BirthDate
                    );
                }
                catch (Exception ex)
                {
                    errs.Add(ex.Message);
                }
            }

            if (errs.Count == 0)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
            }

            return errs;
		}

    public static PersonalInfoData GetData(long user_id)
    {
        PersonalInfoData pdata = new PersonalInfoData();
        pdata.PrimaryUserDetail = App.Identity.UserDetailGet(user_id, true);
        pdata.SecondUserDetail = App.Identity.UserDetailGet(user_id, false);
        Address[] address = App.Identity.AddressesGet(user_id);
        if ((address != null) && (address.Length != 0))
        {
            pdata.PrimaryUserAddress = address[0];
        }
        return pdata;
    }

    private List<string> SavePrimaryUserDetail(long user_id, string auditorName)
    {
        List<string> errs = new List<string>();
        UserDetail user_detail_primary = App.Identity.UserDetailGet(user_id, true);
        if (user_detail_primary != null)
        {
            user_detail_primary.FirstName = PrimaryUserDetail.FirstName;
            user_detail_primary.LastName = PrimaryUserDetail.LastName;
            user_detail_primary.MiddleName = PrimaryUserDetail.MiddleName;
            user_detail_primary.Ssn = PrimaryUserDetail.Ssn;
            user_detail_primary.BirthDate = PrimaryUserDetail.BirthDate;
            if (PrimaryUserDetail.Email != null)
            {
                user_detail_primary.Email = PrimaryUserDetail.Email;
            }
            user_detail_primary.PhoneHome = PrimaryUserDetail.PhoneHome;
            user_detail_primary.PhoneWork = PrimaryUserDetail.PhoneWork;
            user_detail_primary.IsHispanic = PrimaryUserDetail.IsHispanic;
            user_detail_primary.IsMale = PrimaryUserDetail.IsMale;
            user_detail_primary.IsMale = PrimaryUserDetail.IsMale;
            user_detail_primary.MaritalStatus = PrimaryUserDetail.MaritalStatus;
            user_detail_primary.Race = PrimaryUserDetail.Race;
        }
        else
        {
            user_detail_primary = PrimaryUserDetail;
        }

        UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_primary, auditorName);
        if (!user_detail_save_result.IsSuccessful)
        {
            if (user_detail_save_result.IsDuplicateEmail == true)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|DuplicateEmail."));
            }
            if (user_detail_save_result.IsDuplicateSsn == true)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|DuplicateSSN."));
            }
#if DEBUG
            if (!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
            {
                errs.Add(user_detail_save_result.ExceptionStr);
            }
#endif
            if (errs.Count == 0)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
            }
        }
        return errs;
    }

    private List<string> SavePrimaryUserAddress(long user_id, string auditorName)
    {
        List<string> errs = new List<string>();

        Address address = null;
        if (PrimaryUserAddress.AddressId > 0)
        {
            address = App.Identity.AddressGet(PrimaryUserAddress.AddressId);
        }
        if (address == null)
        {
            Address[] addresses = App.Identity.AddressesGet(user_id);
            if ((addresses != null) && (addresses.Length > 0))
            {
                address = addresses[0];
            }
        }
        if (address == null)
        {
            address = new Address { UserId = user_id, };
        }

        address.AddressType = "HOME";
        address.StreetLine1 = PrimaryUserAddress.StreetLine1;
        address.StreetLine2 = PrimaryUserAddress.StreetLine2;
        address.City = PrimaryUserAddress.City;
        address.Zip = PrimaryUserAddress.Zip;
        address.State = PrimaryUserAddress.State;

        AddressSaveResult result = App.Identity.AddressSave(address, auditorName);
        if (result.IsSuccessful)
        {
            PrimaryUserAddress.AddressId = result.AddressId.GetValueOrDefault(0);
        }
        else
        {
            if (result.IsInvalidZipCode == true)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|InValidState") + " " + result.StateExpectedForZip);
            }
#if DEBUG
            if (!string.IsNullOrEmpty(result.ExceptionStr))
            {
                errs.Add(result.ExceptionStr);
            }
#endif
            if (errs.Count == 0)
            {
                errs.Add(App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
            }
        }
        return errs;
    }

    private List<string> SaveSecondUserDetail(long userId, string auditorName)
    {
        List<string> errs = new List<string>();

        if (!string.IsNullOrEmpty(SecondUserDetail.LastName))
        {
            UserDetail user_detail_secondary = App.Identity.UserDetailGet(userId, false);
            if (user_detail_secondary == null) // For new Secondry User
            {
                user_detail_secondary = new UserDetail { UserId = userId, IsPrimary = false, };
            }

            user_detail_secondary.FirstName = SecondUserDetail.FirstName;
            user_detail_secondary.MiddleName = SecondUserDetail.MiddleName;
            user_detail_secondary.LastName = SecondUserDetail.LastName;
            user_detail_secondary.Ssn = SecondUserDetail.Ssn;
            user_detail_secondary.BirthDate = SecondUserDetail.BirthDate;

            UserDetailSaveResult user_detail_save_result = App.Identity.UserDetailSave(user_detail_secondary, auditorName);

            if (user_detail_save_result.IsSuccessful)
            {
                SecondUserDetail.UserDetailId = user_detail_save_result.UserDetailId.GetValueOrDefault(0);
            }
            else
            {
                if (user_detail_save_result.IsDuplicateEmail == true)
                {
                    errs.Add(App.Translate("Credability|CredabilityWebSite|DuplicateEmail."));
                }
                if (user_detail_save_result.IsDuplicateSsn == true)
                {
                    errs.Add(App.Translate("Credability|CredabilityWebSite|DuplicateSSN."));
                }
#if DEBUG
                if (!string.IsNullOrEmpty(user_detail_save_result.ExceptionStr))
                {
                    errs.Add(user_detail_save_result.ExceptionStr);
                }
#endif
                if (errs.Count == 0)
                {
                    errs.Add(App.Translate("Credability|CredabilityWebSite|CommonErrorMessage"));
                }
            }
        }
        else
        {
            App.Identity.UserDetailDeleteSecondary(userId);
        }
        return errs;
    }
}
}
