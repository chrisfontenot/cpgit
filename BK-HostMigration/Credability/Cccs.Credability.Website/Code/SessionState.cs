﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Web;
using Cccs.Identity;
namespace Cccs.Credability.Website
{
	public static class SessionState
	{
		#region Constants

		private const string USER_ID = "UserId";
		private const string USERNAME = "Username";
		private const string SSO_TOKEN = "SsoToken";
		private const string ACCOUNT_ID = "AccountId";
		private const string LANGUAGE_CODE = "LanguageCode";
		private const string REF_CODE = "ref_code";
        private const string WAIVER_TYPE = "WaiverType";
        
		private const string AUTH_CODE1 = "authCode1";
		private const string AUTH_CODE2 = "authCode2";
		private const string AUTH_CODE3 = "authCode3";

		private const string COMPANY_NAME = "CompanyName";
		private const string SECCOMPANY_NAME = "SecCompanyName";
		private const string CLIENT_NUMBER = "ClientNumber";
        private const string COUNSELOR_ID = "CounselorID";
		private const string PROGRESSBAR_VALUE = "ProgressStatusValue";
		private const string FWID = "FwID";
		private const string DELQTYPE = "DelqType";
        private const string escrow = "Escrow";
        private const string escrowProbonoValue = "EscrowProbonoValue";
        private const string escrow6 = "Escrow6";
        private const string FIRM_ID = "FirmID";
        private const string FIRM_NAME = "FirmName";
        private const string CASENUMBER = "CaseNumber";
        private const string SECONDARY_FLAG = "SecondaryFlag";
        private const string SESSION_COMPLTED = "SessionCompleted";
		private const string CLIENT_TYPE = "ClientType";
        private const string REG_NUM = "RegNum";
        private const string CL_ID = "Clid";
        private const string Attorney_Name = "AttorneyName";
        private const string Attorney_Email = "AttorneyEmail";
        private const string Edit_Attorney = "EditAttorney";
		private const string IS_SECPERSON_MAIDEN_NAME_REQUIRED = "IsSecPersonMaidenNameRequired";
		private const string MOD_FORM = "1";
		private const string USERPAGEARRIVAL_TIME = "UserPageArrivalTime";
        private const string CONTACT_SSN = "ContactSsn";
        private const string Joint_Ssn = "JointSsn";
        private const string secondary_flag = "SecondaryFlag";
        private const string MUST_SHOW_LANGUAGE_CODES = "MustShowLanguageCode";
        private const string CHARGE_AMOUNT = "ChargeAmount";
        private const string PRIMARY_ZIPCODE = "PrimaryZipCode";
        private const string STATE = "State";
		#endregion

        public static string State
        {
            get { return HttpContext.Current.Session.ValueGet<string>(STATE); }
            set { HttpContext.Current.Session.ValueSet(STATE, value); }
        }
        public static string PrimaryZipCode
        {
            get { return HttpContext.Current.Session.ValueGet<string>(PRIMARY_ZIPCODE); }
            set { HttpContext.Current.Session.ValueSet(PRIMARY_ZIPCODE, value); }
        }
        public static string ChargeAmount
        {
            get { 
                var toReturn = HttpContext.Current.Session.ValueGet<string>(CHARGE_AMOUNT);

                return toReturn == null ? String.Empty : toReturn; 
            }
            set { 
                HttpContext.Current.Session.ValueSet(CHARGE_AMOUNT, value); 
            }
        }
		public static string RefCode
		{
			get { return HttpContext.Current.Session.ValueGet<string>(REF_CODE); }
			set { HttpContext.Current.Session.ValueSet(REF_CODE, value); }
		}
        public static string JointSsn
        {
            get { return HttpContext.Current.Session.ValueGet<string>(Joint_Ssn); }
            set { HttpContext.Current.Session.ValueSet(Joint_Ssn, value); }
        }
        public static string RegNum
		{
            get { return HttpContext.Current.Session.ValueGet<string>(REG_NUM); }
            set { HttpContext.Current.Session.ValueSet(REG_NUM, value); }
		}
        public static string ContactSsn
        {
            get { return HttpContext.Current.Session.ValueGet<string>(CONTACT_SSN); }
            set { HttpContext.Current.Session.ValueSet(CONTACT_SSN, value); }
        }
        
       /*DEPRECATED, WILL BE DELETED SOON - bw
         public static string FecondaryFlag
        {
            get { return HttpContext.Current.Session.ValueGet<string>(secondary_flag); }
            set { HttpContext.Current.Session.ValueSet(secondary_flag, value); }
        }
        */


         public static string AttorneyName
         {
             get { return HttpContext.Current.Session.ValueGet<string>(Attorney_Name); }
             set { HttpContext.Current.Session.ValueSet(Attorney_Name, value); }
         }
         public static string AttorneyEmail
         {
             get { return HttpContext.Current.Session.ValueGet<string>(Attorney_Email); }
             set { HttpContext.Current.Session.ValueSet(Attorney_Email, value); }
         }

         public static string EditAttorney
        {
            get { return HttpContext.Current.Session.ValueGet<string>(Edit_Attorney); }
            set { HttpContext.Current.Session.ValueSet(Edit_Attorney , value); }
        }

        public static string Clid
		{
            get { return HttpContext.Current.Session.ValueGet<string>(CL_ID); }
            set { HttpContext.Current.Session.ValueSet(CL_ID, value); }
		}
        
		public static string AuthenticationCode1
		{
			get { return HttpContext.Current.Session.ValueGet<string>(AUTH_CODE1); }
			set { HttpContext.Current.Session.ValueSet(AUTH_CODE1, value); }
		}

		public static string AuthenticationCode2
		{
			get { return HttpContext.Current.Session.ValueGet<string>(AUTH_CODE2); }
			set { HttpContext.Current.Session.ValueSet(AUTH_CODE2, value); }
		}

		public static string AuthenticationCode3
		{
			get { return HttpContext.Current.Session.ValueGet<string>(AUTH_CODE3); }
			set { HttpContext.Current.Session.ValueSet(AUTH_CODE3, value); }
		}

		public static string LanguageCode
		{
			get
			{
				string language_code = HttpContext.Current.Session.ValueGet<string>(LANGUAGE_CODE);

				if (language_code != Cccs.Translation.Language.ES)
				{
					language_code = Cccs.Translation.Language.EN;
				}

				return language_code;
			}

			set
			{
				HttpContext.Current.Session.ValueSet(LANGUAGE_CODE, value);
			}
		}

		public static long? UserId
		{
			get { return HttpContext.Current.Session.ValueGet<long?>(USER_ID); }
			set { HttpContext.Current.Session.ValueSet(USER_ID, value); }
		}

		public static string Username
		{
			get { return HttpContext.Current.Session.ValueGet<string>(USERNAME); }
			set { HttpContext.Current.Session.ValueSet(USERNAME, value); }
		}

		public static int? ClientNumber
		{
			get { return HttpContext.Current.Session.ValueGet<int?>(CLIENT_NUMBER); }
			set { HttpContext.Current.Session.ValueSet(CLIENT_NUMBER, value); }
		}

        public static int? CounselorID
        {
            get { return HttpContext.Current.Session.ValueGet<int?>(COUNSELOR_ID); }
            set { HttpContext.Current.Session.ValueSet(COUNSELOR_ID, value); }
        }

		public static long? AccountId
		{
			get { return HttpContext.Current.Session.ValueGet<long?>(ACCOUNT_ID); }
			set { HttpContext.Current.Session.ValueSet(ACCOUNT_ID, value); }
		}

		public static int? FwID
		{
			get { return HttpContext.Current.Session.ValueGet<int?>(FWID); }
			set { HttpContext.Current.Session.ValueSet(FWID, value); }
		}

		public static string ClientType
		{
			get { return HttpContext.Current.Session.ValueGet<string>(CLIENT_TYPE); }
			set { HttpContext.Current.Session.ValueSet(CLIENT_TYPE, value); }
		}

        public static string Escrow
        {
            get { return HttpContext.Current.Session.ValueGet<string>(escrow); }
            set { HttpContext.Current.Session.ValueSet(escrow , value); }
        }

        public static int EscrowProbonoValue
        {
            get { return HttpContext.Current.Session.ValueGet<int>(escrowProbonoValue); }
            set { HttpContext.Current.Session.ValueSet(escrowProbonoValue, value); }
        }

        public static string Escrow6
        {
            get { return HttpContext.Current.Session.ValueGet<string>(escrow6); }
            set { HttpContext.Current.Session.ValueSet(escrow6 , value); }
        }

        public static string FirmName
        {
            get { return HttpContext.Current.Session.ValueGet<string>(FIRM_NAME); }
            set { HttpContext.Current.Session.ValueSet(FIRM_NAME , value); }
        }

        public static string CaseNumber
        {
            get { return HttpContext.Current.Session.ValueGet<string>(CASENUMBER); }
            set { HttpContext.Current.Session.ValueSet(CASENUMBER , value); }
        }

        public static string SecondaryFlag
        {
            get { return HttpContext.Current.Session.ValueGet<string>(SECONDARY_FLAG); }
            set { HttpContext.Current.Session.ValueSet(SECONDARY_FLAG , value); }
        }

        public static int? SessionCompleted
        {
            get { return HttpContext.Current.Session.ValueGet<int?>(SESSION_COMPLTED); }
            set { HttpContext.Current.Session.ValueSet(SESSION_COMPLTED , value); }
        }

        public static string FirmID
        {
            get { return HttpContext.Current.Session.ValueGet<string>(FIRM_ID); }
            set { HttpContext.Current.Session.ValueSet(FIRM_ID , value); }
        }

		public static int? ModForm
		{
			get { return HttpContext.Current.Session.ValueGet<int?>(MOD_FORM); }
			set { HttpContext.Current.Session.ValueSet(MOD_FORM, value); }
		}

		public static string CompanyName
		{
			get { return HttpContext.Current.Session.ValueGet<string>(COMPANY_NAME); }
			set { HttpContext.Current.Session.ValueSet(COMPANY_NAME, value); }
		}

		public static string SecCompanyName
		{
			get { return HttpContext.Current.Session.ValueGet<string>(SECCOMPANY_NAME); }
			set { HttpContext.Current.Session.ValueSet(SECCOMPANY_NAME, value); }
		}

		public static string DelqType
		{
			get { return HttpContext.Current.Session.ValueGet<string>(DELQTYPE); }
			set { HttpContext.Current.Session.ValueSet(DELQTYPE, value); }
		}

		public static DateTime UserPageArrivalTime
		{
			get { return HttpContext.Current.Session.ValueGet<DateTime>(USERPAGEARRIVAL_TIME); }
			set { HttpContext.Current.Session.ValueSet(USERPAGEARRIVAL_TIME, value); }
		}

		public static string SsoToken
		{
			get { return HttpContext.Current.Session.ValueGet<string>(SSO_TOKEN); }
			set { HttpContext.Current.Session.ValueSet(SSO_TOKEN, value); }
		}
        public static string WaiverType
		{
            get { return HttpContext.Current.Session.ValueGet<string>(WAIVER_TYPE); }
            set { HttpContext.Current.Session.ValueSet(WAIVER_TYPE, value); }
		}
        
		public static bool IsSecPersonMaidenNameRequired
		{
			get { return HttpContext.Current.Session.ValueGet<bool>(IS_SECPERSON_MAIDEN_NAME_REQUIRED); }
			set { HttpContext.Current.Session.ValueSet(IS_SECPERSON_MAIDEN_NAME_REQUIRED, value); }
		}

        public static bool MustShowLanguageCodes
        {
            get { return HttpContext.Current.Session.ValueGet<bool>(MUST_SHOW_LANGUAGE_CODES); }
            set { HttpContext.Current.Session.ValueSet(MUST_SHOW_LANGUAGE_CODES, value); }
        }

        public static bool isRegistration { get; set; }

        //public static double BKEducationFee { get; set; }

        public static double BKFee { get; set; }

        public static bool CanJoinSSN { get; set; }
   }
}
