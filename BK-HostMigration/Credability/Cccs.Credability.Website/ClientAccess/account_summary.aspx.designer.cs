﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cccs.Credability.Website.ClientAccess {
    
    
    public partial class account_summary {
        
        /// <summary>
        /// UcAccountSummary control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Cccs.Credability.Website.Controls.ClientAccessControls.AccountSummary UcAccountSummary;
        
        /// <summary>
        /// Master property.
        /// </summary>
        /// <remarks>
        /// Auto-generated property.
        /// </remarks>
        public new Cccs.Credability.Website.MastePages.Master Master {
            get {
                return ((Cccs.Credability.Website.MastePages.Master)(base.Master));
            }
        }
    }
}
