﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="account_summary.aspx.cs" Inherits="Cccs.Credability.Website.ClientAccess.account_summary" MasterPageFile="~/MasterPages/Master.Master" Title="Account Summary"%>
<%@ Register Src="~/Controls/ClientAccessControls/AccountSummary.ascx" TagPrefix="Uc" TagName="AccountSumm" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:AccountSumm ID="UcAccountSummary" runat="server"></Uc:AccountSumm >
</asp:Content>
