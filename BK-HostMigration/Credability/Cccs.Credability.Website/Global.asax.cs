﻿using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Cccs.Credability.Website.BootstrapperTasks;
using StructureMap;
using Cccs.Credability.Certificates.Email;

namespace Cccs.Credability.Website
{
    public class Global : System.Web.HttpApplication
    {
        private const string AsposeLicense = "Cccs.Credability.Website.Aspose.Total.lic";
        private const string PreFilingPollKey = "PreFilingPoll";

        protected void Application_Start(object sender, EventArgs e)
        {            
            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("Counsites Application Started");
            
            var assembly = Assembly.GetExecutingAssembly();
            var licenseStream = assembly.GetManifestResourceStream(AsposeLicense);

            var wordLicense = new Aspose.Words.License();
            licenseStream.Position = 0;
            wordLicense.SetLicense(licenseStream);

            var barcodeLicense = new Aspose.BarCode.License();
            licenseStream.Position = 0;
            barcodeLicense.SetLicense(licenseStream);

            var reportLicense = new Aspose.Report.License();
            licenseStream.Position = 0;
            reportLicense.SetLicense(licenseStream);

            ConfigureDependencyInjection();
            ConfigureAutoMapper();

            // Long running threads are usually discouraged in ASP.NET, 
            // but there isn't an application server for running a windows service.
            // StartEmailProcessingThread();
        }

        private void ConfigureDependencyInjection()
        {
            ObjectFactory.Initialize(r => 
                {
                    r.AddRegistry<RegisterServices>();
                    r.AddRegistry<RegisterCertificateRepositories>();
                });
        }

        private void ConfigureAutoMapper()
        {
            // Get the list of referenced assemblies that may have Profiles defined.
            // Exclude the AutoMapper assembly.
            var referencedAssemblyNames = typeof(ContactDetailPage).Assembly.GetReferencedAssemblies()
                .Where(x => x.FullName != typeof(Profile).Assembly.FullName);

            // Scan each referenced assembly and add any defined Profile.
            var configuration = Mapper.Configuration;
            foreach (var assemblyName in referencedAssemblyNames)
            {
                var assembly = Assembly.Load(assemblyName);
                AddProfileClasses(configuration, assembly);
            }

            // Add any profile classes in the current assembly.
            AddProfileClasses(configuration, this.GetType().Assembly);

            Mapper.AssertConfigurationIsValid();
        }

        private static void AddProfileClasses(IConfiguration configuration, Assembly assembly)
        {
            var profileTypes = assembly.GetExportedTypes()
                .Where(x => x.IsSubclassOf(typeof(Profile)));

            foreach (var type in profileTypes)
            {
                var profileInstance = Activator.CreateInstance(type) as Profile;
                configuration.AddProfile(profileInstance);
            }
        }

        //private void StartEmailProcessingThread()
        //{
        //    var poll = ObjectFactory.GetInstance<PreFilingPoll>();
        //    poll.Start();
        //    Application[PreFilingPollKey] = poll;
        //}

        //private void StopEmailProcessThread()
        //{
        //    var poll = Application[PreFilingPollKey] as PreFilingPoll;
        //    if (poll != null && poll.IsRunning)
        //    {
        //        poll.Stop();
        //    }
        //}

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            //StopEmailProcessThread();
        }
    }
}