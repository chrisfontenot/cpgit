﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfileCheck.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.UserProfileCheck" MasterPageFile="~/MasterPages/Master.Master" Title="Contact Information" %>
<%@ Register Src="~/Controls/Shared/Pages/PersonalInfo.ascx" TagName="PersonalInfo" TagPrefix="uc1" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<script type="text/javascript" language="javascript" src="../Content/FormChek.js"></script>
	<uc1:PersonalInfo ID="PersonalInfo1" runat="server" IsPrimaryEmploymentVisible="false" ValidateAges="true" IsPrimaryEmailVisible="false" IsSecondaryEmploymentVisible="false" IsSecondaryExtraVisible="false" />
	<div class="clearboth">
	</div>
	<div class="dvform2col dvform2colchk">
	</div>
	<div class="dvbtncontainer">
		<div class="lnkbutton">
			<asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click" ToolTip="Continue">
			<span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
			</asp:LinkButton>
		</div>
	</div>
</asp:Content>