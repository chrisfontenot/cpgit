﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserFinancialSituationRecap.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.UserFinancialSituationRecap" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility User Financial Situation Summary HUD"%>
<%@ Register Src="~/Controls/Shared/Pages/UcFinancialRecap.ascx" TagPrefix="Uc" TagName="UcFinancialSituationRecap" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UcFinancialSituationRecap id="UcFinancialSituationRecap" runat="server" RedirectOnContinue="UnderstandingYourNetWorth.aspx" RedirectOnPrevious="MonthlyExpenses.aspx" ReturnTo="UserFinancialSituationRecap"></Uc:UcFinancialSituationRecap> 
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>