﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserFinancialSituationRecap.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.UserFinancialSituationRecap"  MasterPageFile="~/MasterPages/Master.Master" Title="CCCS Analyzing Your Financial Situation HousingOnly"%>
<%@ Register Src="~/Controls/HousingOnlyControls/UCFinancialSituationRecap.ascx" TagPrefix="Uc"  TagName="UCFinancialSituationRecap"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCFinancialSituationRecap id="UcUCFinancialSituationRecap" runat="server"></Uc:UCFinancialSituationRecap>
<!-- Credibility Google Analytics Code -->
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-15166403-1");
        pageTracker._setDomainName(".credibility.org");
        pageTracker._trackPageview();
    } catch (err) { }
</script>
</asp:Content>
