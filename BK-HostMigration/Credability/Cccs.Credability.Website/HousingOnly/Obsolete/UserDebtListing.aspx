﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserDebtListing.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.UserDebtListing"  MasterPageFile="~/MasterPages/Master.Master" Title="CCCS User Debt Listing HousingOnly"%>
<%@ Register Src="~/Controls/HousingOnlyControls/UCUserDebtListing.ascx" TagPrefix="Uc"  TagName="UCUserDebtListing"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCUserDebtListing id="UcUCUserDebtListing" runat="server"></Uc:UCUserDebtListing>
<!-- Credibility Google Analytics Code -->
<script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-15166403-1");
        pageTracker._setDomainName(".credibility.org");
        pageTracker._trackPageview();
    } catch (err) { }
</script>
</asp:Content>
