﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionsforDealingwithaFinancialCrisis.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.CommonOptionsforDealingwithaFinancialCrisis" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Common Options for Dealing with a Financial Crisis HUD"%>
<%@ Register Src="~/Controls/BchControls/CommonOptionsforDealingwithaFinancialCrisis.ascx" TagPrefix="Uc"  TagName="CommonOptionsforDealingwithaFinancialCrisis"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:CommonOptionsforDealingwithaFinancialCrisis id="UcCommonOptionsforDealingwithaFinancialCrisis" runat="server"></Uc:CommonOptionsforDealingwithaFinancialCrisis> 
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>