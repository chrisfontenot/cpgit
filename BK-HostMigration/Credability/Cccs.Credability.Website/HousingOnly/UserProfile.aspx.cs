﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class UserProfile : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Profile);
				UserProfile1.ShowHideControl();

                CommonFunction.UserProgressSave(PercentComplete.USER_PROFILE, SessionState.Username);
			}
		}
	}
}
