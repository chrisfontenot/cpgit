﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientCounseled.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.ClientCounseled" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility ClientCounseled HUD" %>
<%@ Register Src="~/Controls/HousingOnly/UcClientCounseled.ascx" TagPrefix="Uc" TagName="UcClientCounseled" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcClientCounseled ID="UcStuff" runat="server"></Uc:UcClientCounseled>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder"
	runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>