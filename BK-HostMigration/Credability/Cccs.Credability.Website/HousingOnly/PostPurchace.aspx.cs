﻿using System;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class PostPurchace : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUCPostPurchace.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode=ChatCodeOption.Session1;
			}
			if(!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.TellUs);

				CommonFunction.UserProgressSave(PercentComplete.POST_PURCHASE, SessionState.Username);
			}
		}
	}
}
