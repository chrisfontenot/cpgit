﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class CommonOptionsforCrisis : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcUCommonOption.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
            if (AuthCode != null)
            {
                AuthCode.ChatCode=ChatCodeOption.Session3;
            }

			if (!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Summary);

                CommonFunction.UserProgressSave(PercentComplete.COMMON_OPTIONS_FOR_CRISIS, SessionState.Username);
			}
		}
	}
}
