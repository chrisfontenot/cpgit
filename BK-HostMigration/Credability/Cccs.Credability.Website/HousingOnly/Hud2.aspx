﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Hud2.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.Hud2" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility HUD2 HUD" %>
<%@ Register Src="~/Controls/BchControls/UCHud2.ascx" TagPrefix="Uc" TagName="UCHud2" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCHud2 ID="UcUCHud2" runat="server"></Uc:UCHud2>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>