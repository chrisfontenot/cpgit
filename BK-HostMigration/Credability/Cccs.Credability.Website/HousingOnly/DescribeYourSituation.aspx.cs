﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class DescribeYourSituation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.TellUs);

                CommonFunction.UserProgressSave(PercentComplete.DESCRIBE_YOUR_SITUATION, SessionState.Username);
			}
		}
	}
}
