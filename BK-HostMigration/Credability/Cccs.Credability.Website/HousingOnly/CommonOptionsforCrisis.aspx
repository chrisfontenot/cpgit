﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionsforCrisis.aspx.cs" Inherits="Cccs.Credability.Website.HousingOnly.CommonOptionsforCrisis" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Common Option for Crisis HUD" %>
<%@ Register Src="~/Controls/BchControls/UCCommonOptionsforCrisis.ascx" TagPrefix="Uc"  TagName="UCommonOption"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCommonOption id="UcUCommonOption" runat="server"></Uc:UCommonOption>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>