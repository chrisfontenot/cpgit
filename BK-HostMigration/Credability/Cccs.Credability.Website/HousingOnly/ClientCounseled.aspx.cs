﻿using System;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class ClientCounseled : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbBch.Visible = false;

				CommonFunction.UserProgressSave(PercentComplete.AFS, SessionState.Username);
			}
		}
	}
}