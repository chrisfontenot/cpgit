﻿using System;

namespace Cccs.Credability.Website.HousingOnly
{
	public partial class AFS : DefaultPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				this.ShowLanguageCodes();

				Master.BreadCrumbBch.Visible = false;

				CommonFunction.UserProgressSave(PercentComplete.AFS, SessionState.Username);
			}
		}
	}
}
