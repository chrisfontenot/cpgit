﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="GetRequiredFeeInformation.aspx.cs" Inherits="Cccs.Credability.Website.Controls.Shared.Pages.GetRequiredFeeInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <asp:HiddenField ID="RedirectLocation" runat="server" />   

    <div style="background-color:#ECECEC; float:left; width:100%;">       

            <h1>We need some information please</h1>

            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|PET")%>
            <asp:textbox id="txtAttorneyCode" runat="server" maxlength="4" cssclass="mgrT5">
            </asp:textbox>
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|4digits")%> OR<br />

            <label style="float:left;"><%= "I do not have an attorney code" %></label>
            <asp:CheckBox ID="NoAttorneyCode" runat="server" TextAlign="Left" />
            <br /><br />
        
            <%= Cccs.Credability.Website.App.Translate("Credability|BKC|YZC")%>
            <asp:TextBox ID="txtZipCode" runat="server" cssclass="mgrT5"></asp:TextBox><br />
        
    </div>    

    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click">
                <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
</asp:Content>
