-- Create Database Master Key 
CREATE MASTER KEY ENCRYPTION BY
PASSWORD = 'S3@5&hFRwQ9*7' 
GO

-- Create Encryption Certificate 
CREATE CERTIFICATE CccsCertificate
WITH SUBJECT = 'Certificate for CCCS Application' 
GO

-- Create Symmetric Key
CREATE SYMMETRIC KEY CcCs_Key WITH
IDENTITY_VALUE = 'Key for CCCS application',
ALGORITHM = AES_256, 
KEY_SOURCE = 'cccs application'
ENCRYPTION BY CERTIFICATE CccsCertificate;