﻿// Generic Field Modifiers
var ReFormat = true;// this is so the calculator can get the # w/o $
var NoChr = "0123456789.";

function SetUpField(theField)
{
	var sValue = theField.value;
	sValue = Number(stripCharsNotInBag(sValue, NoChr));
	if (sValue <= 0)
		sValue = '';
	theField.value = sValue;
	theField.select();
}

function ReFormatCurrency(theField)
{
	if(ReFormat)
	{
		var sValue = theField.value, OutTxt = "", Des;
		var NumStr, Len, i;
		sValue = stripCharsNotInBag(sValue, NoChr);
		if (sValue == 0) sValue = '0';
		NumStr = sValue - (sValue % 1);
		NumStr = String(NumStr);
		Len = NumStr.length - 1;
		for (i = 0; i <= Len; i++)
		{
			OutTxt += NumStr.charAt(i);
			if (i < Len && (Len - i) % 3 == 0) //>
			{
				OutTxt += ",";
			}
		}
		sValue = '$' + OutTxt;
		theField.value = sValue;
	}
	ReFormat = true;
}

function ReFormatPercent(theField)
{
	var sValue = theField.value;
	sValue = Number(stripCharsNotInBag(sValue, NoChr));
	if (sValue == 0)
	{
		sValue = 0;
	}
	theField.value = String(sValue.toFixed(2)) + "%";
}

function SetUpCalc(Field)
{
	var sValue = Field.value;
	sValue = Number(stripCharsNotInBag(sValue, NoChr));
	if (sValue <= 0)
		sValue = '0.00';
	Field.value = sValue;
	ReFormat = false;
	Field.focus()
}

///////////////////////////////////////////////
// Font Size related Functions
///////////////////////////////////////////////
var CurFont = GetFontCookie();
function FontChange(Dir)
{
	switch (Dir)
	{
		case '+':
			if (CurFont < 3) //>
			{
				CurFont++;
				SetFontCookie()
			}
			else
			{
				return false;
			}
			break;

		case '-':
			if (CurFont > 0)
			{
				CurFont--;
				SetFontCookie()
			}
			else
			{
				return false;
			}
			break;
		default:
			break;
	}
	//alert(CurFont);
	if (CurFont == 3)
	{

		document.getElementById("FontUp").innerHTML = '<img src="/images/FontUpG.gif" border="0">';
	}
	else
	{
		document.getElementById("FontUp").innerHTML = '<a href="#" onclick="javascript: FontChange(\'+\'); return false;" class="BigLink"><img src="/images/FontUp.gif" border="0"></a>';
	}
	if (CurFont == 0)
	{
		document.getElementById("FontDown").innerHTML = '<img src="/images/FontDnG.gif" border="0">';
	}
	else
	{
		document.getElementById("FontDown").innerHTML = '<a href="#" onclick="javascript: FontChange(\'-\'); return false;" class="BigLink"><img src="/images/FontDn.gif" border="0"></a>';
	}
	for (i = 0; i < 4; i++)
	{
	    if (document.getElementById("ctl00_fontSize" + i) !== null) {
	        document.getElementById("ctl00_fontSize" + i).disabled = true;
	    }
	}
	document.getElementById("ctl00_fontSize" + CurFont).disabled = false;
	return false;
}

function SetFontCookie()
{
	var EXdate = new Date();
	EXdate.setDate(EXdate.getDate() + 20)
	document.cookie = "CccsCurFontSize=" + CurFont + ";expires=" + EXdate.toGMTString();

}

function GetFontCookie()
{
	CookieMon = "CccsCurFontSize=";
	if (document.cookie.length > 0)
	{
		Start = document.cookie.indexOf(CookieMon)
		if (Start != -1)
		{
			Start = Start + CookieMon.length;
			End = document.cookie.indexOf(";", Start);
			if (End == -1)
				End = document.cookie.length;
			return Number(document.cookie.substring(Start, End));
		}
	}
	return 0
}

function SaveWarning(TabTitle, CurTab)
{
	if (CurTab != '0' && document.getElementById("ModForm").value != '0')
	{
		return confirm("By navigating through these tabs any changes you may have made will be lost.\nPlease use the navigation buttons at the bottom of the page.\nDo you wish to continue on to " + TabTitle + "?");
	}
	else
	{
		return true;
	}
}