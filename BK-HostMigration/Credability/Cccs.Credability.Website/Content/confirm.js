function pageLoad(sender, args) {
    $(document).ready(function () {

        $('.lnkbutton .save').click(function (e) {
            var theHREF = $(this).attr("href");
            e.preventDefault();
            var message = "Click the send button below to move forward with sending your current uncompleted session to one of our certified counselors. We will contact you via phone to complete the remainder of the counseling session. Click cancel below to return to your online counseling session and finalize the remainder of information we need to complete your counseling session. The more information for your counseling session you submit to us, the faster we will be able to complete our review and analysis and provide you with assistance.";
            confirmPopUp(message, function (response) {
                if (response) {
                    window.location.href = theHREF;

                }
            });
        });

    });
}


function confirmPopUp(message, callback) {
    $('#confirm').modal({
        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
        position: ["20%", ],
        overlayId: 'confirm-overlay',
        containerId: 'confirm-container',
        containerCss: { width: 550, height: 280 },
        //  appendTo: 'form',
        onShow: function (dialog) {
            var modal = this;

            $('.message', dialog.data[0]).append(message);

            // if the user clicks "yes"
            $('.buttons div', dialog.data[0]).click(function () {
                var link = $(this);

                // call the callback
                if ($.isFunction(callback)) {
                    callback.apply(this, [link.hasClass('yes') ? true : false]);
                    // callback.apply();
                }
                // close the dialog
                modal.close(); // or $.modal.close();
            });

        }
    });
}

