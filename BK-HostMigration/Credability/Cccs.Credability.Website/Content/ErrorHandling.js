﻿(function () {
    var needsHighlighting = function (validator, control) {
        var hasDoNotHighlight = $(validator).hasClass('doNotHighlight');

        return (typeof validator.isvalid !== 'undefined' && !validator.isvalid) ||
            (control.type == 'text' && control.value === '' && !hasDoNotHighlight);
    };

    WebForm_OnSubmit = function () {
        //hide ALL validation summary blocks
        $('[id$=DesValidationSummary]').hide();
//        ctl00_ContentPlaceHolderBody_UcUCDescribeYourSituation_DesValidationSummary

        if (typeof (ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) {
            for (var i in Page_Validators) {
                try {
                    var control = document.getElementById(Page_Validators[i].controltovalidate);

                    if (needsHighlighting(Page_Validators[i], control)) {
                        control.className = "ErrorControl";
                    } else {
                        control.className = "";
                    }
                } catch (e) { }
            }
            return false;
        }
        return true;
    }

})();