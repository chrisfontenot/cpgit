﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Example1.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.Example1" MasterPageFile="~/MasterPages/Master.Master" Title="Reverse Mortgage Introduction" %>
<%@ Register Src="~/Controls/RvmControls/UcHecmExample1.ascx" TagPrefix="Uc" TagName="HecmExample" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:HecmExample ID="UcHecmExample" runat="server" RedirectOnContinue="Example2.aspx"></Uc:HecmExample>
</asp:Content>