﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Cccs.Identity;
using Cccs.Credability.Website.Controls.RvmControls;
using Cccs.Credability.Website.Rvm;
namespace Cccs.Credability.Website
{
	public partial class UserProfile : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Master.BreadCrumbRvm.ActiveTabSet(BreadCrumControl.Tab.Profile);

                CommonFunction.UserProgressSave(PercentComplete.USER_PROFILE, SessionState.Username);
			
				System.Web.UI.HtmlControls.HtmlGenericControl Rvmspan = UcUserInfo.FindControl("spnRvmRow") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if (Rvmspan != null)
				{
					Rvmspan.Visible = true;
				}
			}
		}
	}
}
