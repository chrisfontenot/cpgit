﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Eligibility.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.ReverseMortgageCosts" MasterPageFile ="~/MasterPages/Master.Master" Title="CredAbility Eligibility" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/RvmControls/UcEligibility.ascx" TagPrefix="Uc" TagName="UCEligibility" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCEligibility ID="UcUCEligibility" runat="server" RedirectOnContinue="RepaymentRules.aspx" RedirectOnPrevious="UserProfile.aspx"></Uc:UCEligibility> 
</asp:Content>