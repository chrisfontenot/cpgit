﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PublicBenefits.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.PublicBenefits" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility PublicBenefits RVM" %>
<%@ Register Src="~/Controls/RvmControls/UcPublicBenefits.ascx" TagPrefix="Uc" TagName="PublicBenefits" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:PublicBenefits ID="UcPublicBenefits" runat="server" RedirectOnPrevious="Alternitives.aspx"></Uc:PublicBenefits>
</asp:Content>