﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AFS.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.AFS" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility AFS RVM" %>
<%@ Register Src="~/Controls/Shared/Pages/UcAfs.ascx" TagPrefix="Uc" TagName="UCAFS" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<%--	<Uc:UCAFS ID="UcUCAFS" runat="server" RedirectOnContinue="UserProfile.aspx" RedirectOnNeedAuthorization="DescribeYourSituation.aspx"></Uc:UCAFS>--%>
    <Uc:UCAFS ID="UcUCAFS" runat="server" RedirectOnContinue="WhatYouNeed_RVM.aspx" RedirectOnNeedAuthorization="DescribeYourSituation.aspx"></Uc:UCAFS>
</asp:Content>