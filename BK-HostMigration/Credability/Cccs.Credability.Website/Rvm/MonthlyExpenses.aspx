﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpenses.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.MonthlyExpenses" MasterPageFile="~/MasterPages/Master.Master" Title ="CredAbility Monthly Expenses"%>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyExpenses.ascx" TagPrefix="Uc" TagName="MonthlyExpenses" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="javascript" src="../Content/FormChek.js"></script>
	<Uc:MonthlyExpenses ID="UcMonthlyExpenses" runat="server" RedirectOnContinue="UserFinancialSituationRecap.aspx" RedirectOnPrevious="UserIncomeDocumentation.aspx"></Uc:MonthlyExpenses>
</asp:Content>