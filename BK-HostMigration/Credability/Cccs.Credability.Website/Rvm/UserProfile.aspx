﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="Cccs.Credability.Website.UserProfile" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility User Profile" %>
<%@ Register Src="~/Controls/Shared/Pages/UserProfile.ascx" TagPrefix="Uc" TagName="UserInfo" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UserInfo ID="UcUserInfo" runat="server" IsEmailVisible="true" WebsiteCode="RVM" RedirectOnContinue="RvmRegulation.aspx" RedirectOnNeedAuthorization="AFS.aspx"></Uc:UserInfo>
</asp:Content>