﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HECMBenefits.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.HECMBenefits"  MasterPageFile ="~/MasterPages/Master.Master" Title="CredAbility HECM Benefits"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/RvmControls/UcRepaymentRules.ascx" TagPrefix="Uc" TagName="UCRepaymentRules" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <%= Cccs.Credability.Website.App.Translate("Credability|Rvm|~Rvm~HECMBenefits.aspx") %>

    <Uc:UCRepaymentRules 
        ID="UcUCRepaymentRules" 
        runat="server" 
        RedirectOnContinue="ReverseMortgageCosts.aspx" 
        RedirectOnPrevious="RepaymentRules.aspx">
    </Uc:UCRepaymentRules>

</asp:Content>