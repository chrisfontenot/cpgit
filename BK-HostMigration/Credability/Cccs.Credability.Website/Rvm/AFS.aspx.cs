﻿using System;

namespace Cccs.Credability.Website.Rvm
{
	public partial class AFS : DefaultPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
                this.ShowLanguageCodes();
                CommonFunction.UserProgressSave(PercentComplete.AFS, SessionState.Username);
			}
		}
	}
}
