﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Aging.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.Aging" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Aging RVM" %>
<%@ Register Src="~/Controls/RvmControls/UcAging.ascx" TagPrefix="Uc" TagName="UCAging" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCAging ID="UcUCAging" runat="server" RedirectOnPrevious="Alternitives.aspx"></Uc:UCAging>
</asp:Content>