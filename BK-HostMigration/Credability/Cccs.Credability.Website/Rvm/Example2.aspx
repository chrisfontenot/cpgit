﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Example2.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.Example2" MasterPageFile="~/MasterPages/Master.Master" Title="Reverse Mortgage Introduction" %>
<%@ Register Src="~/Controls/RvmControls/UcHecmExample2.ascx" TagPrefix="Uc" TagName="HecmExample" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:HecmExample ID="UcHecmExample" runat="server" RedirectOnContinue="ReverseMortgageCosts.aspx"></Uc:HecmExample>
</asp:Content>