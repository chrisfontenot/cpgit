﻿using System;
using Cccs.Credability.Dal;
using Cccs.Identity;

namespace Cccs.Credability.Website.Rvm
{
	public partial class RvmRegulation : System.Web.UI.Page
	{
		protected string BlockMessage = String.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			string ReDirPage = "Eligibility.aspx";
			Address[] ClntAdr = App.Identity.AddressesGet(SessionState.UserId.Value);
			if(ClntAdr != null)
			{
				RegulatedState StReg = App.Credability.RegulatedStateGet(ClntAdr[0].State);
				if(StReg != null)
				{
					if(StReg.RvmBan)
					{
						BlockMessage = String.Format(App.Translate("Credability|RVM|BadState"), StReg.State);
					}
					else
					{
						Response.Redirect(ReDirPage);
					}
				}
				else
				{
					Response.Redirect(ReDirPage);
				}
			}
			else
			{
				Response.Redirect(ReDirPage);
			}
		}

		protected void btnExit_Click(object sender, EventArgs e)
		{
			Response.Redirect(CommonFunction.GetAlwaysAvailableToHelpCoreSiteUrl());
		}
	}
}