﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website.Rvm
{
	public static class PercentComplete
	{
		public const byte AFS = 5;
		public const byte USER_PROFILE = 15;
		public const byte ELIGIBILITY = 24;
		public const byte REPAYMENT_RULES = 28;
		public const byte HECM_BENEFITS = 32;
		public const byte REVERSE_MORTGAGE_COSTS = 34;
		public const byte ALTERNATIVES = 38;
		public const byte DESCRIBE_YOUR_SITUATION = 45;
		public const byte USER_INCOME_DOCUMENTATION = 60;
		public const byte MONTHLY_EXPENSES = 64;
		public const byte UNDERSTANDING_YOUR_NET_WORTH = 68;
		public const byte USER_DEBT_LISTING = 75;
		public const byte USER_FINANCIAL_SITUATION_ANALYSIS = 85;
		public const byte USER_FINANCIAL_SITUATION_RECAP = 90;
		public const byte ALMOST_DONE = 95;
		public const byte COMPLETE = 100;
	}
}
