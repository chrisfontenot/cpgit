﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreditGrowth.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.CreditGrowth"  MasterPageFile="~/MasterPages/Master.Master" Title="Reverse Mortgage Introductions"%>
<%@ Register Src="~/Controls/RvmControls/UcCreditGrowth.ascx" TagPrefix="Uc" TagName="CreditGrowths" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:CreditGrowths ID="CreditGrowths" runat="server" RedirectOnPrevious="HECMBenefits.aspx"></Uc:CreditGrowths> 
</asp:Content>