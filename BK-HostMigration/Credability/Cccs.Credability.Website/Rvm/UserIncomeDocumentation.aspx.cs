﻿using System;
using Cccs.Credability.Website.Controls.RvmControls;
using Cccs.Credability.Website.Rvm;

namespace Cccs.Credability.Website
{
	public partial class UserIncomeDocumentation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbRvm.ActiveTabSet(BreadCrumControl.Tab.Budget);
				CommonFunction.UserProgressSave(PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
				System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = UcUserIncome.FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(HeadSpan != null)
				{
					HeadSpan.Visible = true;
				}
				System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(introText != null)
				{
					introText.Visible = true;
				}
			}
		}
	}
}
