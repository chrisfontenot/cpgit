﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserIncomeDocumentation.aspx.cs" Inherits="Cccs.Credability.Website.UserIncomeDocumentation" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Describe your Situation"%>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyIncome.ascx" TagPrefix="Uc" TagName="UcMonthlyIncome" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UcMonthlyIncome id="UcUserIncome" RedirectOnPrevious="DescribeYourSituation.aspx" RedirectOnContinue="MonthlyExpenses.aspx" runat="server"></Uc:UcMonthlyIncome>
</asp:Content>