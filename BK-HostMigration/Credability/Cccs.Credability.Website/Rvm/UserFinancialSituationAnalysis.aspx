﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserFinancialSituationAnalysis.aspx.cs" Inherits="Cccs.Credability.Website.UserFinancialSituationAnalysis" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Monthly Expenses" %>
<%@ Register Src="~/Controls/Shared/Pages/UcFinancialAnalysis.ascx" TagPrefix="Uc" TagName="UcFinalFinancialSituationAnalysis" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<img src="../Images/BT_analyzingyoursituation[1].gif" alt="User Financial Situation Analysis" />
	<Uc:UcFinalFinancialSituationAnalysis ID="UcFinalFinancialSituationAnalysis" runat="server" RedirectOnContinue="UserFinancialSituationRecap.aspx" RedirectOnPrevious="ListingYourDebts.aspx" ReturnTo="UserFinancialSituationAnalysis" />
</asp:Content>