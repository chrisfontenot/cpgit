﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alternitives.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.Alternitives" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Alternitives RVM"%>
<%@ Register Src="~/Controls/RvmControls/UcAlternitives.ascx" TagPrefix="Uc" TagName="Alternitives" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:Alternitives ID="UcAlternitives" runat="server" RedirectOnContinue="DescribeYourSituation.aspx" RedirectOnPrevious="ReverseMortgageCosts.aspx"></Uc:Alternitives> 
</asp:Content>