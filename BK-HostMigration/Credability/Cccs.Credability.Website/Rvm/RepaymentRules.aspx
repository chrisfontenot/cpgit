﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RepaymentRules.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.RepaymentRules" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Repayment Rules" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/RvmControls/UcRepaymentRules.ascx" TagPrefix="Uc" TagName="UCRepaymentRules" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">       
    
    <%= Cccs.Credability.Website.App.Translate("Credability|Rvm|~Rvm~RvmRepaymentRules.aspx") %>

    <Uc:UCRepaymentRules 
        ID="UcUCRepaymentRules" 
        runat="server" 
        RedirectOnContinue="HECMBenefits.aspx" 
        RedirectOnPrevious="Eligibility.aspx">
    </Uc:UCRepaymentRules>
</asp:Content>