﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReverseMortgageCosts.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.ReverseMortgageCosts1"  MasterPageFile ="~/MasterPages/Master.Master" Title ="CredAbility Reverse Mortgage Costs"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/RvmControls/UcRepaymentRules.ascx" TagPrefix="Uc" TagName="UCRepaymentRules" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <%= Cccs.Credability.Website.App.Translate("Credability|Rvm|~Rvm~ReverseMortgageCosts.aspx") %>

    <Uc:UCRepaymentRules 
        ID="UcUCRepaymentRules" 
        runat="server" 
        RedirectOnContinue="Alternitives.aspx" 
        RedirectOnPrevious="HECMBenefits.aspx">
    </Uc:UCRepaymentRules>

</asp:Content>