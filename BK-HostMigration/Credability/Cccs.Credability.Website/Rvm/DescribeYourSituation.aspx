﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribeYourSituation.aspx.cs" Inherits="Cccs.Credability.Website.Rvm.DescribeYourSituation" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Describe your Situation"%>
<%@ Register Src="~/Controls/RvmControls/UcUserSituationDescription.ascx" TagPrefix="Uc" TagName="UserSituationDesc" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UserSituationDesc ID="UcUserSituationDesc" runat="server" RedirectOnContinue="UserIncomeDocumentation.aspx" RedirectOnPrevious="Alternitives.aspx"></Uc:UserSituationDesc> 
</asp:Content>