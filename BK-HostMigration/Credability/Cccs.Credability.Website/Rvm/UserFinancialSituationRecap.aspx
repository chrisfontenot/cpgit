﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserFinancialSituationRecap.aspx.cs" Inherits="Cccs.Credability.Website.UserFinancialSituationRecap" MasterPageFile="~/MasterPages/Master.Master" Title ="CredAbility Monthly Expenses"%>
<%@ Register Src="~/Controls/Shared/Pages/UcFinancialRecap.ascx" TagPrefix="Uc" TagName="UcFinancialSituationRecap" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<img src="../Images/BT_analyzingyoursituation[1].gif" alt="User Financial Situation Analysis" />
<Uc:UcFinancialSituationRecap id="UcFinancialSituationRecap" runat="server" RedirectOnContinue="AlmostDone.aspx" RedirectOnPrevious="UserFinancialSituationAnalysis.aspx" ReturnTo="UserFinancialSituationRecap"></Uc:UcFinancialSituationRecap>
</asp:Content>