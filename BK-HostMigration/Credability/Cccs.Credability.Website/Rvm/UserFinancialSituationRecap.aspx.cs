﻿using System;
using Cccs.Credability.Website.Controls.RvmControls;
using Cccs.Credability.Website.Rvm;

namespace Cccs.Credability.Website
{
	public partial class UserFinancialSituationRecap : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbRvm.ActiveTabSet(BreadCrumControl.Tab.Summary);
				CommonFunction.UserProgressSave(PercentComplete.USER_FINANCIAL_SITUATION_RECAP, SessionState.Username);
			}
		}
	}
}