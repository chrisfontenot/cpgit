﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.RvmControls;

namespace Cccs.Credability.Website.Rvm
{
	public partial class Example2 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Master.BreadCrumbRvm.ActiveTabSet(BreadCrumControl.Tab.AboutUs);
			}
		}
	}
}
