﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Rvm
{
	public partial class AlmostDone : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
                CommonFunction.UserProgressSave(PercentComplete.ALMOST_DONE, SessionState.Username);
			}
		}
	}
}
