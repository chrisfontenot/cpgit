﻿using System;
using Cccs.Credability.Website.Controls.RvmControls;
using Cccs.Credability.Website.Rvm;

namespace Cccs.Credability.Website
{
	public partial class UnderstandingYourNetWorth : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbRvm.ActiveTabSet(BreadCrumControl.Tab.Budget);
				CommonFunction.UserProgressSave(PercentComplete.UNDERSTANDING_YOUR_NET_WORTH, SessionState.Username);
			}
		}
	}
}