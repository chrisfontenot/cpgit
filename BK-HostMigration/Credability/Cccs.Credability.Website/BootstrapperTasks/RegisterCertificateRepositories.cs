﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap.Configuration.DSL;
using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.Repositories;

namespace Cccs.Credability.Website.BootstrapperTasks
{
    public class RegisterCertificateRepositories : Registry
    {
        public RegisterCertificateRepositories()
        {
            For<IAccountRepository>().Use<AccountRepository>();
            For<IContactDetailRepository>().Use<ContactDetailRepository>();
            For<ISnapshotRepository>().Use<SnapshotRepository>();
            For<IUserRepository>().Use<UserRepository>();
            For<IExpenseRepository>().Use<ExpenseRepository>();
            For<IPredischargeCertificateRepository>().Use<PredischargeCertificateRepository>();
        }
    }
}