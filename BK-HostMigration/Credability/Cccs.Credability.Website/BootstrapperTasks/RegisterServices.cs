﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap.Configuration.DSL;
using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.PreFiling;
using AutoMapper;
//using Cccs.Host;
using Cccs.Credability.Certificates.Email;
using Cccs.Credability.Certificates.Education;
using Cccs.Debtplus;

namespace Cccs.Credability.Website.BootstrapperTasks
{
    public class RegisterServices : Registry
    {
        public RegisterServices()
        {
            For<ILoggingService>().Use<Log4NetLoggingService>()
                .OnCreation(x => LogUserId(x));
            For<IMappingEngine>().Use(Mapper.Engine);
            For<IPreFilingCertificateService>().Use<PreFilingCertificateService>();
            For<ICounselingCertificateDownload>().Use<CounselingCertificateDownload>();
            //For<IHost>().Use(App.Host);
            For<IDebtplus>().Use(App.Debtplus);
            For<PreFilingPoll>().Use<PreFilingPoll>();
            For<IDmpCalculationService>().Use<DMPCalculationService>();
            For<IEducationCertificateService>().Use<EducationCertificateService>(); ;
        }

        private void LogUserId(Log4NetLoggingService service)
        {
            if (SessionState.UserId.HasValue)
            {
                service.SetUserId(SessionState.UserId.Value);
            }
        }
    }
}