﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;
using System.Globalization;
using System.Threading;

namespace Cccs.Credability.Website.MastePages
{
    public partial class SynchronousMaster : System.Web.UI.MasterPage
    {
        public Cccs.Credability.Website.Controls.BchControls.BreadCrumControl BreadCrumbBch
        {
            get { return UcBreadcrumBch; }
        }

        public Cccs.Credability.Website.Controls.BKCounselingControls.BreadCrumControl BreadCrumbBKC
        {
            get { return UcBreadcrumBKC; }
        }

        public Cccs.Credability.Website.Controls.RvmControls.BreadCrumControl BreadCrumbRvm
        {
            get { return UcBreadcrum; }
        }

        public Cccs.Credability.Website.Controls.PrePurchaseControls.BreadCrumControl BreadCrumbPrp
        {
            get { return UcBreadcrumPRP; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(SessionState.LanguageCode);

            if (!IsPostBack)
            {
                HrefLogo.HRef = CommonFunction.GetHomePageUrl();

                // Handle SSO Token authentication
                //
                if ((Request != null) && (Request.QueryString != null) && !string.IsNullOrEmpty(Request.QueryString["u"]) && !string.IsNullOrEmpty(Request.QueryString["t"]))
                {
                    Session.Clear();
                    long user_id = 0;

                    if (long.TryParse(Request.QueryString["u"], out user_id))
                    {
                        string sso_token = Request.QueryString["t"];

                        if (App.Identity.UserSsoTokenValidate(user_id, sso_token))
                        {
                            User user = App.Identity.UserGet(user_id);

                            SessionState.UserId = user_id;
                            SessionState.SsoToken = sso_token;
                            SessionState.Username = user.Username;
                            if (App.IsSpanishSupported)
                            {
                                SessionState.LanguageCode = user.LanguageCode ?? Cccs.Translation.Language.EN;
                            }

                            GenerateAuthenticationCode(sso_token);
                        }
                    }
                }
            }
            else if ((SessionState.UserId != null) && !string.IsNullOrEmpty(SessionState.SsoToken))
            {
                if (!App.Identity.UserSsoTokenValidate(SessionState.UserId.Value, SessionState.SsoToken))
                {
                    Session.Clear();
                }
            }

            // Redirect out of site if unauthenticated
            //
            if (SessionState.UserId == null)
            {
                Response.Redirect(CommonFunction.GetThankYouCoreSiteUrl());
            }

            if (!IsPostBack)
            {
                // TODO: Move this "logic" to App and call from AFS page?	////////////////////////////////////////////////

                string website_code = App.WebsiteCode;

                string refCode = Request.QueryString["r"];
                if (!string.IsNullOrEmpty(refCode))
                {
                    refCode = HttpUtility.UrlDecode(refCode);
                }

                UserWebsite user_website = App.Identity.UserWebsiteGet(SessionState.UserId.Value, website_code);

                if (user_website == null)
                {
                    // Add a new ContactDetail record.  TODO: Wait until AFS page
                    //
                    UserContactDetails contact_details = new UserContactDetails();

                    contact_details.ClientType = App.ClientType;

                    if (refCode != null)
                    {
                        contact_details.RefCode = refCode;
                    }

                    UserContactDetailsResult ucdr = App.Credability.UserInformationUpdate(contact_details);

                    // Add the user website entry
                    //
                    if (ucdr.IsSuccessful)
                    {
                        SessionState.ClientNumber = ucdr.ClientNumber;

                        Account account = new Account { UserId = SessionState.UserId.Value, AccountTypeCode = App.AccountTypeCode, InternetId = SessionState.ClientNumber.Value };

                        AccountSaveResult asr = App.Identity.AccountSave(account, SessionState.Username);

                        if (asr.IsSuccessful)
                        {
                            SessionState.AccountId = asr.AccountId;

                            user_website = new UserWebsite { UserId = SessionState.UserId.Value, WebsiteCode = website_code, AccountId = asr.AccountId };

                            App.Identity.UserWebsiteSave(user_website, SessionState.Username);
                        }
                    }
                    SessionState.RefCode = refCode;
                }
                else if (user_website.AccountId == null) // User has visited website but has no known Account/ContactDetail/IN#
                {
                    // Add a new ContactDetail record.  TODO: Wait until AFS page
                    //
                    UserContactDetails contact_details = new UserContactDetails();
                    if (refCode != null)
                    {
                        contact_details.RefCode = refCode;
                    }
                    UserContactDetailsResult ucdr = App.Credability.UserInformationUpdate(contact_details);

                    // Add the user website entry
                    //
                    if (ucdr.IsSuccessful)
                    {
                        SessionState.ClientNumber = ucdr.ClientNumber;

                        Account account = new Account { UserId = SessionState.UserId.Value, AccountTypeCode = App.AccountTypeCode, InternetId = SessionState.ClientNumber.Value };

                        AccountSaveResult asr = App.Identity.AccountSave(account, SessionState.Username);

                        if (asr.IsSuccessful)
                        {
                            SessionState.AccountId = asr.AccountId;

                            user_website.AccountId = asr.AccountId;

                            App.Identity.UserWebsiteSave(user_website, SessionState.Username);
                        }
                    }
                    SessionState.RefCode = refCode;
                }
                else
                {
                    Account account = App.Identity.AccountGetById(user_website.AccountId.Value);

                    if (account != null)
                    {
                        SessionState.AccountId = account.AccountId;

                        if (account.InternetId != null)
                        {
                            SessionState.ClientNumber = (int)account.InternetId.Value;
                        }
                    }

                    if (string.IsNullOrEmpty(SessionState.RefCode) || refCode != null)
                    {
                        refCode = UpdateReferralCode(SessionState.ClientNumber.GetValueOrDefault(), refCode);
                        SessionState.RefCode = refCode;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void GenerateAuthenticationCode(string token)
        {
            int seed;
            if (!string.IsNullOrEmpty(token))
            {
                seed = token.GetHashCode();
            }
            else
            {
                seed = DateTime.Now.Millisecond;
            }
            Random rand = new Random(seed);
            int min = Properties.Settings.Default.AuthCodeMin;
            int max = Properties.Settings.Default.AuthCodeMax;
            SessionState.AuthenticationCode1 = rand.Next(min, max).ToString();
            SessionState.AuthenticationCode2 = rand.Next(min, max).ToString();
            SessionState.AuthenticationCode3 = rand.Next(min, max).ToString();
        }

        private string UpdateReferralCode(int clientNbr, string refCode)
        {
            UserContactDetails contact = App.Credability.UserInfoGet(clientNbr);
            if (contact != null)
            {
                if (refCode == null)
                {
                    refCode = contact.RefCode;
                }
                else if (string.Compare(contact.RefCode.Trim(), refCode.Trim(), true, CultureInfo.InvariantCulture) != 0)
                {
                    contact.RefCode = refCode;
                    App.Credability.UserInformationUpdate(contact);
                }
            }
            return refCode;
        }
    }
}
