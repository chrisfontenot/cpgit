﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProvideYourPersonalInfo.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchasing.ProvideYourPersonalInfo" Title="CredAbility Pre Purching Provide Your Personal Information" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UserProfile.ascx" TagName="ProvideYourPersonalInfo" TagPrefix="uc1" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<uc1:ProvideYourPersonalInfo ID="ProvideYourPersonalInfo1" runat="server" IsEmailVisible="true" RedirectOnContinue="Chapter1.aspx" RedirectOnNeedAuthorization="AFS.aspx" />
</asp:Content>

