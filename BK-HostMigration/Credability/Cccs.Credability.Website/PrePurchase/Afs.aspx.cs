﻿using System;

namespace Cccs.Credability.Website.PrePurchase
{
	public partial class Afs : DefaultPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				this.ShowLanguageCodes();

				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl BCHTopText = UcUCAfs.FindControl("BCHTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BCHTopText != null)
			{
				BCHTopText.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl PRPTopText = UcUCAfs.FindControl("PRPTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PRPTopText != null)
			{
				PRPTopText.Visible = true;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl BCHPriPerson = UcUCAfs.FindControl("BCHPriPerson") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BCHPriPerson != null)
			{
				BCHPriPerson.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl PRPPriPerson = UcUCAfs.FindControl("PRPPriPerson") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PRPPriPerson != null)
			{
				PRPPriPerson.Visible = true;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl BCHSecPerson = UcUCAfs.FindControl("BCHSecPerson") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BCHSecPerson != null)
			{
				BCHSecPerson.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl PRPSecPerson = UcUCAfs.FindControl("PRPSecPerson") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PRPSecPerson != null)
			{
				PRPSecPerson.Visible = true;
			}
		}
	}
}
