﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
	public partial class PrePurchaseAnalyzingYourFinSitRecap : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcFinancialSituationRecap.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
				AuthCode.ChatCode= ChatCodeOption.Session1;
			}
			if(!IsPostBack)
			{
				Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);
				CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.ANALYZING_YOUR_FINANCIAL_SITUATION_RECAP, SessionState.Username);
				System.Web.UI.HtmlControls.HtmlGenericControl PrpText = UcFinancialSituationRecap.FindControl("PrpText") as System.Web.UI.HtmlControls.HtmlGenericControl;
				if(PrpText != null)
				{
					PrpText.Visible = true;
				}
			}
		}
	}
}