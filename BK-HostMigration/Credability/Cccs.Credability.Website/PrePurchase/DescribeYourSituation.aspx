﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribeYourSituation.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.PrePurchaseDescribeYourSelf" Title="CredAbility PrePurchase Describe YourS elf" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="../Controls/PrePurchaseControls/UcDescribingYourSituation.ascx" TagName="DescribingYourSituation" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<uc1:DescribingYourSituation ID="DescribingYourSituation1" runat="server" RedirectOnContinue="Chap1C.aspx" RedirectOnPrevious="Chap1B.aspx" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>