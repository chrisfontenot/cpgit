﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
	public partial class UserDocYourIncome : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);
			CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
			System.Web.UI.HtmlControls.HtmlGenericControl introTextPrp = UcUserIncome.FindControl("introTextPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(introTextPrp != null)
			{
				introTextPrp.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = UcUserIncome.FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(HeadSpan != null)
			{
				HeadSpan.Visible = true;
			}
		}
	}
}