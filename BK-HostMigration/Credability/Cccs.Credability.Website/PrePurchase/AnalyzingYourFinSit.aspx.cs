﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
	public partial class PrePurchaseAnalyzingYourFinSit : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);

			CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.ANALYZING_YOUR_FINANCIAL_SITUATION, SessionState.Username);
			if(!IsPostBack)
			{
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl spnBKCTopText = UcFinalFinancialSituationAnalysis.FindControl("spnBKCTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnBKCTopText != null)
			{
				spnBKCTopText.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl spnPRPTopText = UcFinalFinancialSituationAnalysis.FindControl("spnPRPTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnPRPTopText != null)
			{
				spnPRPTopText.Visible = true;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl spnRVMFirstPannelText = UcFinalFinancialSituationAnalysis.FindControl("spnRVMFirstPannelText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnRVMFirstPannelText != null)
			{
				spnRVMFirstPannelText.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl spnPRPFirstPannelText = UcFinalFinancialSituationAnalysis.FindControl("spnPRPFirstPannelText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnPRPFirstPannelText != null)
			{
				spnPRPFirstPannelText.Visible = true;
			}
		}
	}
}