﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FollowUp.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.FollowUp" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility FollowUp" %>
<%@ Register Src="~/Controls/PrePurchaseControls/UCFollowUp.ascx" TagPrefix="Uc"  TagName="UCFollowUp"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCFollowUp id="UcUCFollowUp" runat="server"></Uc:UCFollowUp>
</asp:Content>
