﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnalyzingYourFinSitRecap.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchasing.PrePurchaseAnalyzingYourFinSitRecap" Title="CredAbility Pre Purching Analyzing Your Financial Situation Recap" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="../Controls/Shared/Pages/UcFinancialRecap.ascx" TagName="UcFinancialSituationRecap" TagPrefix="Uc" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcFinancialSituationRecap ID="UcFinancialSituationRecap" runat="server" RedirectOnContinue="Chap1B.aspx" RedirectOnPrevious="AnalyzingYourFinSit.aspx" ReturnTo="AnalyzingYourFinSitRecap" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>