﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
	public partial class PrePurchaseUnderstandingYourNetWorths : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);
			CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.UNDERSTANDING_YOUR_NET_WORTH, SessionState.Username);
			if(!IsPostBack)
			{
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl spnBCHTopText = UcUnderstandingYourNetWorth.FindControl("spnBCHTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnBCHTopText != null)
			{
				spnBCHTopText.Visible = false;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl spnPRPTopText = UcUnderstandingYourNetWorth.FindControl("spnPRPTopText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(spnPRPTopText != null)
			{
				spnPRPTopText.Visible = true;
			}
		}
	}
}