﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="WhatYouNeed_PP.aspx.cs" Inherits="Cccs.Credability.Website.Controls.Shared.Pages.WhatYouNeed_PP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <div style="background-color:#ECECEC; float:left; width:100%;">       

            <h1>What you will need to finish this session:</h1>

            <ul>
                <li>60 minutes of time (this is the average amount of time it takes to complete a session)</li>
                <li>Information on income (i.e. W-2s/paystubs, SS Payments, Alimony, and child support)</li>
                <li>Information on your expenses (things like your mortgage or rent payments, utilities, transportation expenses)</li>
                    <ul>
                        <li>Another good resource for expenses is your bank statement or checkbook which can provide further insight into where you money is spent each month</li>
                    </ul>                
                <li>A list of the debts you owe (including statements for credit cards, student loans, auto loans, and loans against your 401K)</li>                                   
            </ul>
        
    </div>    

    <div class="dvbtncontainer">
        <div class="lnkbutton">
            <asp:LinkButton ID="btnContinue" runat="server" OnClick="btnContinue_Click">
                <span><%= Cccs.Credability.Website.App.Translate("Credability|RVMButtons|Continue")%></span>
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
</asp:Content>