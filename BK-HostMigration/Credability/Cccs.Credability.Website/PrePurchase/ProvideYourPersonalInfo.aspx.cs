﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
    public partial class ProvideYourPersonalInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);

            CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.USER_PROFILE, SessionState.Username);

            if (!IsPostBack)
            {
                ShowHideSpanControls();
            }


        }

        public void ShowHideSpanControls()
        {
            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPRow = ProvideYourPersonalInfo1.FindControl("spnPRPRow") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnPRPRow != null)
            {
                spnPRPRow.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl PrePurchasespan = ProvideYourPersonalInfo1.FindControl("spnPrePurchase") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (PrePurchasespan != null)
            {
                PrePurchasespan.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnBottomAuthorizationRow = ProvideYourPersonalInfo1.FindControl("spnBottomAuthorizationRow") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (PrePurchasespan != null)
            {
                spnBottomAuthorizationRow.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPBottomAuthorizationRow = ProvideYourPersonalInfo1.FindControl("spnPRPBottomAuthorizationRow") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (PrePurchasespan != null)
            {
                spnPRPBottomAuthorizationRow.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnRVMPri = ProvideYourPersonalInfo1.FindControl("spnRVMPri") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnRVMPri != null)
            {
                spnRVMPri.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPPri = ProvideYourPersonalInfo1.FindControl("spnPRPPri") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnPRPPri != null)
            {
                spnPRPPri.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnRVMSec = ProvideYourPersonalInfo1.FindControl("spnRVMSec") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnRVMSec != null)
            {
                spnRVMSec.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPSec = ProvideYourPersonalInfo1.FindControl("spnPRPSec") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnPRPSec != null)
            {
                spnPRPSec.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnRVMPriInfo = ProvideYourPersonalInfo1.FindControl("spnRVMPriInfo") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnRVMPriInfo != null)
            {
                spnRVMPriInfo.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPPriInfo = ProvideYourPersonalInfo1.FindControl("spnPRPPriInfo") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnPRPPriInfo != null)
            {
                spnPRPPriInfo.Visible = true;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnRVMSecInfo = ProvideYourPersonalInfo1.FindControl("spnRVMSecInfo") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnRVMSecInfo != null)
            {
                spnRVMSecInfo.Visible = false;
            }

            System.Web.UI.HtmlControls.HtmlGenericControl spnPRPSecInfo = ProvideYourPersonalInfo1.FindControl("spnPRPSecInfo") as System.Web.UI.HtmlControls.HtmlGenericControl;
            if (spnPRPSecInfo != null)
            {
                spnPRPSecInfo.Visible = true;
            }

        }

    }
}
