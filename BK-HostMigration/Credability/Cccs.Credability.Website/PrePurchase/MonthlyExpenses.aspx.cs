﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchasing
{
	public partial class PrePurchasingMonthlyExpenses : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);
				CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.MONTHLY_EXPENSES, SessionState.Username);
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcMonthlyExpenses.FindControl("TopTextPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}

			System.Web.UI.HtmlControls.HtmlGenericControl FhaQuestion = UcMonthlyExpenses.FindControl("FhaQuestion") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(FhaQuestion != null)
			{
				FhaQuestion.Visible = false;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Equity = UcMonthlyExpenses.FindControl("EquityPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Equity != null)
			{
				Equity.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Mort2nd = UcMonthlyExpenses.FindControl("Mort2ndPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Mort2nd != null)
			{
				Mort2nd.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl PayTax = UcMonthlyExpenses.FindControl("PayTaxPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PayTax != null)
			{
				PayTax.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl PayIns = UcMonthlyExpenses.FindControl("PayInsPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PayIns != null)
			{
				PayIns.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl AssocFee = UcMonthlyExpenses.FindControl("AssocFeePrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(AssocFee != null)
			{
				AssocFee.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Gas = UcMonthlyExpenses.FindControl("GasPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Gas != null)
			{
				Gas.Visible = true;
			}
			System.Web.UI.WebControls.TextBox MoPropIns = UcMonthlyExpenses.FindControl("txtMoPropIns") as System.Web.UI.WebControls.TextBox;
			if(MoPropIns != null)
			{
				MoPropIns.Enabled = true;
			}
			System.Web.UI.WebControls.TextBox MoPropTax = UcMonthlyExpenses.FindControl("txtMoPropTax") as System.Web.UI.WebControls.TextBox;
			if(MoPropTax != null)
			{
				MoPropTax.Enabled = true;
			}
		}
	}
}