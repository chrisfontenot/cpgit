﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnalyzingYourFinSit.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchasing.PrePurchaseAnalyzingYourFinSit" Title="CredAbility Pre Purching AFS" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UcFinancialAnalysis.ascx" TagName="UcFinalFinancialSituationAnalysis" TagPrefix="Uc" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcFinalFinancialSituationAnalysis ID="UcFinalFinancialSituationAnalysis" runat="server" RedirectOnContinue="AnalyzingYourFinSitRecap.aspx" RedirectOnPrevious="UnderstandingYourNetWorth.aspx" ReturnTo="AnalyzingYourFinSit" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>