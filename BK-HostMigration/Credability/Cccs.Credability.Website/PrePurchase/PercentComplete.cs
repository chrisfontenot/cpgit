﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website.PrePurchase
{
    public class PercentComplete
    {
        // Chapter-1 
        public const byte USER_PROFILE = 2;
        public const byte CHAP_1 = 2;
        public const byte USER_INCOME_DOCUMENTATION = 4;
        public const byte MONTHLY_EXPENSES = 6;
        public const byte USER_DEBT_LISTING = 8;
        public const byte UNDERSTANDING_YOUR_NET_WORTH = 10;
        public const byte ANALYZING_YOUR_FINANCIAL_SITUATION = 12;
        public const byte ANALYZING_YOUR_FINANCIAL_SITUATION_RECAP = 14;
        public const byte CHAP_1B = 14;
        public const byte DESCRIBE_YOUR_SITUATION = 16;
        public const byte CHAP_1C = 16;
        public const byte CHAP_1D = 17;
        public const byte CHAP_1E = 18;
        public const byte CHAP_1END = 18;

        // Chapter-2
        public const byte CHAP_2A = 20;
        public const byte CHAP_2B = 22;
        public const byte CHAP_2C = 24;
        public const byte CHAP_2D = 26;
        public const byte CHAP_2E = 26;
        public const byte CHAP_2F = 28;
        public const byte CHAP_2G = 30;
        public const byte CHAP_2H = 32;
        public const byte CHAP_2I = 34;
        public const byte CHAP_2J = 36;
        public const byte CHAP_2K = 38;
        public const byte CHAP_2END = 39;

        // Chapter-3
        public const byte CHAP_3A = 41;
        public const byte CHAP_3B = 44;
        public const byte CHAP_3C = 47;
        public const byte CHAP_3D = 50;
        public const byte CHAP_3E = 53;
        public const byte CHAP_3F = 56;
        public const byte CHAP_3END = 59;

        // Chapter-4
        public const byte CHAP_4A = 63;
        public const byte CHAP_4B = 66;
        public const byte CHAP_4C = 69;
        public const byte CHAP_4D = 72;
        public const byte CHAP_4E = 76;
        public const byte CHAP_4F = 68;
        public const byte CHAP_4END = 80;

        // Chapter-5
        public const byte CHAP_5A = 82;
        public const byte CHAP_5B = 84;
        public const byte CHAP_5C = 86;
        public const byte CHAP_5D = 88;
        public const byte MONTHLY_EXPENSES_2 = 90;
        public const byte ANALYZING_YOUR_FINANCIAL_SITUATION_2 = 92;
        public const byte CHAP_5E = 94;
        public const byte CHAP_5F = 96;
        public const byte CHAP_5END = 98;
        public const byte FINAL = 100;
    }
}
