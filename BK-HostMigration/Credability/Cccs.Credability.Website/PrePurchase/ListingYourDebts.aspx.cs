﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchase
{
	public partial class PrePurchaseListingYourDebts : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap1);
				CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.USER_DEBT_LISTING, SessionState.Username);
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl PageHead = UcDebtList.FindControl("PageHeadGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PageHead != null)
			{
				PageHead.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcDebtList.FindControl("TopTextPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl BottomText = UcDebtList.FindControl("BottomTextPrp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BottomText != null)
			{
				BottomText.Visible = true;
			}
		}
	}
}
