﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchase
{
    public partial class Chap3End : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap3);

            CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.CHAP_3END, SessionState.Username);
        }
    }
}
