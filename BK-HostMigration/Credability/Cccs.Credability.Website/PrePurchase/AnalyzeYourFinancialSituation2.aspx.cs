﻿using System;
using Cccs.Credability.Website.Controls.PrePurchaseControls;

namespace Cccs.Credability.Website.PrePurchase
{
	public partial class AnalyzeYourFinancialSituation2 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Master.BreadCrumbPrp.ActiveTabSet(BreadCrumControl.Tab.Chap5);

			CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.ANALYZING_YOUR_FINANCIAL_SITUATION_2, SessionState.Username);
		}
	}
}