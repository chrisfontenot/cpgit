﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Done.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.Done" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Done" %>
<%@ Register Src="~/Controls/PrePurchaseControls/UCDone.ascx" TagPrefix="Uc" TagName="UCDone" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCDone ID="UcUCDone" runat="server"></Uc:UCDone>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>