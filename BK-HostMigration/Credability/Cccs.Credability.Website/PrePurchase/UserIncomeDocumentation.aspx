﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserIncomeDocumentation.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchasing.UserDocYourIncome" Title="CredAbility Pre Purching User Documenting Your Income" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyIncome.ascx" TagPrefix="Uc" TagName="UcMonthlyIncome" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcMonthlyIncome ID="UcUserIncome" RedirectOnPrevious="Chapter1.aspx" RedirectOnContinue="MonthlyExpenses.aspx" runat="server"></Uc:UcMonthlyIncome>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>