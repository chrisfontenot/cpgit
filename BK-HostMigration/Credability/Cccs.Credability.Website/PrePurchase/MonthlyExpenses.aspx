<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpenses.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchasing.PrePurchasingMonthlyExpenses" Title="CredAbility Monthly Expenses" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyExpenses.ascx" TagPrefix="Uc" TagName="MonthlyExpenses" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
	<script language="javascript" src="../Content/FormChek.js"></script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:MonthlyExpenses ID="UcMonthlyExpenses" runat="server" RedirectOnContinue="ListingYourDebts.aspx" RedirectOnPrevious="UserIncomeDocumentation.aspx"></Uc:MonthlyExpenses>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>