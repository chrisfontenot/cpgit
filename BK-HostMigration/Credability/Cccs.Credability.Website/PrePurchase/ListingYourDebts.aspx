﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListingYourDebts.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.PrePurchaseListingYourDebts" Title="CredAbility Pre Purching AFS" MasterPageFile="~/MasterPages/Master.Master"%>
<%@ Register Src="~/Controls/Shared/Pages/UcDebtListing.ascx" TagName="DebtList" TagPrefix="Uc" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:DebtList ID="UcDebtList" runat="server" RedirectOnContinue="UnderstandingYourNetWorth.aspx" RedirectOnPrevious="MonthlyExpenses.aspx" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>