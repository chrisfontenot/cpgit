﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnalyzeYourFinancialSituation2.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.AnalyzeYourFinancialSituation2" Title="CredAbility Pre Purching Analyze Your Financial Situation" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="../Controls/PrePurchaseControls/UcFinalFinancialSituationAnalysis2.ascx" TagName="UcFinalFinancialSituationAnalysis2" TagPrefix="Uc" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcFinalFinancialSituationAnalysis2 ID="UcFinalFinancialSituationAnalysis2" runat="server" RedirectOnContinue="Chap5E.aspx" RedirectOnPrevious="MonthlyExpensesNew.aspx" ReturnTo="AnalyzeYourFinancialSituation2" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>