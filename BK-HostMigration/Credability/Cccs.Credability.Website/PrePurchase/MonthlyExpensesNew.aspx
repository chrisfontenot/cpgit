﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpensesNew.aspx.cs" Inherits="Cccs.Credability.Website.PrePurchase.MonthlyExp" Title="CredAbility Pre Purching Mothly Expenses" MasterPageFile="~/MasterPages/Master.Master" %>
<%@ Register Src="../Controls/PrePurchaseControls/UcMonthlyExpensesNew.ascx" TagName="UcMonthlyExpensesNew" TagPrefix="Uc" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
	<script language="javascript" src="../Content/FormChek.js"></script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcMonthlyExpensesNew ID="UcMonthlyExpensesNew" runat="server" RedirectOnContinue="AnalyzeYourFinancialSituation2.aspx" RedirectOnPrevious="Chap5D.aspx" />
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>