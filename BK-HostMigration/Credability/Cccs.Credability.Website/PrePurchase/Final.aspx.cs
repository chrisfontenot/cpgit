﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.PrePurchase
{
    public partial class Final : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CommonFunction.UserProgressSave(Cccs.Credability.Website.PrePurchase.PercentComplete.FINAL, SessionState.Username);
        }
    }
}
