﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
     <script language="CS" runat="server">
             protected void Page_Load(object sender, EventArgs e)
             {
                 if (!IsPostBack)
                 {
                   // txtUsername.Value = ""; // Guid.NewGuid().ToString();
                    txtPassword.Value = ""; // "aajaax15";
                 }
             }
     
			 protected void ButtonSubmit_Click(object sender, EventArgs e)
			 {
				 Cccs.Identity.UserRegistration user_registration = new Cccs.Identity.UserRegistration
				 {
                Username = txtUsername.Value,
                Password = txtPassword.Value,
                PasswordConfirm = txtPassword.Value,
 					 FirstName = txtFirstName.Value,
                LastName = txtLastName.Value,
					 Email = "noemail@rbxglobal.com",
					 SecurityQuestionAnswer = new Cccs.Identity.SecurityQuestionAnswer()
    				 {
						SecurityQuestionId = 1,
						SecurityAnswer = "Chocolate",
	    			 },
					 LanguageCode = "EN",
				 };

				 Cccs.Identity.UserRegisterResult result = Cccs.Credability.Website.App.Identity.UserRegister(user_registration);

				 if (result.IsSuccessful)
				 {
                     Cccs.Identity.UserAuthenticateResult uar = Cccs.Credability.Website.App.Identity.UserAuthenticate(user_registration.Username, user_registration.Password, "1.2.3.4");
                                          
					 if (uar.IsSuccessful)
					 {
						 string url = string.Format("{0}?u={1}&t={2}", DropDownListRedirect.SelectedValue, result.UserId.Value, uar.SsoToken);
						 Response.Redirect(url);
					 }
				 }
			 }   
     	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
			 <asp:DropDownList ID="DropDownListRedirect" runat="server">
			   <asp:ListItem Text="Rvm/default.aspx" />
			   <asp:ListItem Text="Bch/default.aspx" />
               <asp:ListItem Text="Rec/default.aspx" />
			   <asp:ListItem Text="BKCounseling/default.aspx" />
			   <asp:ListItem Text="HousingOnly/default.aspx" />
			   <asp:ListItem Text="BKEducation/default.aspx" />
			   <asp:ListItem Text="DMPOnly/default.aspx" />
			   <asp:ListItem Text="PrePurchase/default.aspx" />
			 </asp:DropDownList>
             <br />
             <br />
             <label>First Name:</label>
             <br />
            <input runat="server" ID="txtFirstName" />
             <br />
             <br />
             <label>Last Name:</label>
             <br />
            <input runat="server" ID="txtLastName" />
             <br />
             <br />
             <label>User name:</label>
             <br />
             <input runat="server" ID="txtUsername" />
             <br />
             <br />
             <label>Password:</label>
             <br />
            <input runat="server" ID="txtPassword" />
             <br />
             <br />


			 <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" onclick="ButtonSubmit_Click" />
    </div>
    </form>
</body>
</html>

