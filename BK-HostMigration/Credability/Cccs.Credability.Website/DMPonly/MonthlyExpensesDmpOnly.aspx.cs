﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.BchControls;

namespace Cccs.Credability.Website.DmpOnly
{
    public partial class MonthlyExpensesDmpOnly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Master.BreadCrumbBch.ActiveTabSet(BreadCrumControl.Tab.Budget);

            CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.MONTHLY_EXPENSES , SessionState.Username);
        }
    }
}
