﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserIncomeDocumentation.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.UserIncomeDocumentationDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs Income Documentation" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyIncome.ascx" TagPrefix="Uc" TagName="UcMonthlyIncome" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonPhoneCall.ascx" TagPrefix="Uc" TagName="LPPhoneCall" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UcMonthlyIncome ID="UcUserIncome" runat="server" IsSubmitButtonVisible="true" RedirectOnContinue="~/Controls/Shared/Pages/EnterPayrollDeductions.aspx?flow=/DMPONLY/" RedirectOnPrevious="DescribingYourSituation.aspx"></Uc:UcMonthlyIncome>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>