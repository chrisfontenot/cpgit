﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="CreditorScreenwb.aspx.cs" Inherits="Cccs.Credability.Website.DMPonly.CreditorScreenWithBenefits" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="~/Controls/DmpOnlyControls/UcCreditorScreenWithBenefits.ascx" TagName="UcCreditorScreenWithBenefits" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonPhoneCall.ascx" TagPrefix="Uc" TagName="LPPhoneCall" %>
<%@ Register Src="~/Controls/Shared/Components/UcNavigationControl.ascx" TagName="UcNavigationControl" TagPrefix="uc1" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<uc1:UcCreditorScreenWithBenefits ID="UcCreditorScreenWithBenefits1" runat="server" />
	<br />
	<uc1:UcNavigationControl ID="UcNavigationControl1" runat="server" RedirectOnContinue="DescribingYourSituation.aspx" RedirectOnPrevious="ListingYourDebts.aspx" SaveAndSubmitToQueueButtonIsVisible="true" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>