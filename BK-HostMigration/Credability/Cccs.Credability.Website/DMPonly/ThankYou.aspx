﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ThankYou.aspx.cs" Inherits="Cccs.Credability.Website.DMPonly.ThankYou" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Thank You BCH" %>
<%@ Register Src="~/Controls/DmpOnlyControls/UCThanksUser.ascx" TagPrefix="Uc" TagName="UCThanksUser" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/Shared/Components/UcNavigationControl.ascx" TagName="UcNavigationControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCThanksUser ID="UcUCThanksUser" runat="server"></Uc:UCThanksUser>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>