﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.DMPonly
{
    public partial class AfsDmpOnly : DefaultPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowLanguageCodes();
        }
    }
}
