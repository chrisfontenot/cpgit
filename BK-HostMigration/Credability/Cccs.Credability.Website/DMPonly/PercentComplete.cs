﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cccs.Credability.Website.DMPonly
{
    public class PercentComplete
    {
        public const byte AFS = 5;
        public const byte USER_PROFILE = 10;
        public const byte USER_DEBT_LISTING = 30;     
        public const byte CREDITORSCREEN = 50;
        public const byte DESCRIBE_YOUR_SITUATION = 65;
        public const byte USER_INCOME_DOCUMENTATION = 70;
        public const byte MONTHLY_EXPENSES = 75;
        public const byte UNDERSTANDING_YOUR_NET_WORTH = 80;
        public const byte POINTS_TO_REMEMBER = 90;  
        public const byte COMPLETE = 100;
     
    }
}
