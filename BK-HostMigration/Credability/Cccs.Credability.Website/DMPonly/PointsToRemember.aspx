﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="PointsToRemember.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.PointsToRemember" Title="Points to Remember" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="../Controls/DmpOnlyControls/UcPointsToRemember.ascx" TagName="UcPointsToRemember" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/UcNavigationControl.ascx" TagName="UcNavigationControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<uc1:UcPointsToRemember ID="UcPointsToRemember1" runat="server" />
	<uc2:UcNavigationControl ID="UcNavigationControl1" SaveButtonIsVisible="false" SaveAndSubmitToQueueButtonIsVisible="false" runat="server" RedirectOnPrevious="UnderstandingYourNetWorth.aspx" RedirectOnContinue="ThankYou.aspx" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>