﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribingYourSituation.aspx.cs"
    Inherits="Cccs.Credability.Website.DmpOnly.DescribingYourSituation" MasterPageFile="~/MasterPages/Master.Master"
    Title="Cccs Describing Your Situation" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>

<%@ Register Src="~/Controls/BchControls/UCDescribeYourSituation.ascx" TagPrefix="Uc"
    TagName="UCDescribeYourSituation" %>

<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <Uc:UCDescribeYourSituation ID="UcUCDescribeYourSituation" runat="server" HideChatAuthorization="true"
        HideHousingPanel="true" HideNavigationButtons="true" IsSubmitButtonVisible=true></Uc:UCDescribeYourSituation>
  
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>