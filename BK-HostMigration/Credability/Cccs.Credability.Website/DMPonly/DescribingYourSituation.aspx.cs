﻿using System;

namespace Cccs.Credability.Website.DmpOnly
{
	public partial class DescribingYourSituation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//    Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.TellUs);

			CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.DESCRIBE_YOUR_SITUATION, SessionState.Username);
			UcUCDescribeYourSituation.SetDMPHeader();
		}
	}
}