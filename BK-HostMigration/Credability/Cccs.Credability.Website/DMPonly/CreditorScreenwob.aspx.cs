﻿using System;

namespace Cccs.Credability.Website.DMPonly
{
	public partial class CreditorScreenWithoutBenefits : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcCreditorScreenWithoutBenefits1.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode = ChatCodeOption.NoChat;			    
			}
			if(!IsPostBack)
			{
				CommonFunction.UserProgressSave(PercentComplete.CREDITORSCREEN, SessionState.Username);
				UcNavigationControl1.OnContinue += new Website.Controls.UcNavigationControl.delegateOnContinue(UcNavigationControl1_eventOncontinue);
			}
		}

		protected void UcNavigationControl1_eventOncontinue()
		{
			UcCreditorScreenWithoutBenefits1.onContinue();
		}
	}
}