﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.UserProfileDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs User Profile Dmp Only" %>
<%@ Register Src="../Controls/Shared/Pages/UserProfile.ascx" TagPrefix="Uc" TagName="UCIncome" %>
    <%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>

<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="javascript" src="../Content/FormChek.js"></script>
	<Uc:UCIncome id="UcUCIncome" runat="server" IsPriEmploymentEducationVisible="true" IsEmailVisible="false" IsCounselingVisible="true" IsCoEmploymentVisible="true" IsAuthorizeShareParticipateVisible="false" WebsiteCode="BCH" RedirectOnContinue="ListingYourDebts.aspx" RedirectOnNeedAuthorization="Afs.aspx">
</Uc:UCIncome>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
  <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>