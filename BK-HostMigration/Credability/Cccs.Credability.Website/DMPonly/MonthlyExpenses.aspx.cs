﻿using System;

namespace Cccs.Credability.Website.DmpOnly
{
	public partial class MonthlyExpensesDmpOnly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.MONTHLY_EXPENSES, SessionState.Username);
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcMonthlyExpenses.FindControl("TopTextDmp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl FhaQuestion = UcMonthlyExpenses.FindControl("FhaQuestion") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(FhaQuestion != null)
			{
				FhaQuestion.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Equity = UcMonthlyExpenses.FindControl("EquityGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Equity != null)
			{
				Equity.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Mort2nd = UcMonthlyExpenses.FindControl("Mort2ndGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Mort2nd != null)
			{
				Mort2nd.Visible = true;
			}
			//System.Web.UI.HtmlControls.HtmlGenericControl PayTax = UcMonthlyExpenses.FindControl("PayTaxGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			//if(PayTax != null)
			//{
			//    PayTax.Visible = true;
			//}
			//System.Web.UI.HtmlControls.HtmlGenericControl PayIns = UcMonthlyExpenses.FindControl("PayInsGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			//if(PayIns != null)
			//{
			//    PayIns.Visible = true;
			//}
			System.Web.UI.HtmlControls.HtmlGenericControl HideTaxIns = UcMonthlyExpenses.FindControl("HideTaxIns") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(HideTaxIns != null)
			{
				HideTaxIns.Visible = false;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl AssocFee = UcMonthlyExpenses.FindControl("AssocFeeGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(AssocFee != null)
			{
				AssocFee.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl Gas = UcMonthlyExpenses.FindControl("GasGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(Gas != null)
			{
				Gas.Visible = true;
			}
		}
	}
}
