﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpenses.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.MonthlyExpensesDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs Monthly Expenses for DmpOnly" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyExpenses.ascx" TagPrefix="Uc" TagName="MonthlyExpenses" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<script language="javascript" src="../Content/FormChek.js"></script>
	<Uc:MonthlyExpenses ID="UcMonthlyExpenses" runat="server" IsSubmitButtonVisible="true" RedirectOnContinue="UnderstandingYourNetWorth.aspx" RedirectOnPrevious="~/Controls/Shared/Pages/EnterPayrollDeductions.aspx?flow=/DMPONLY/"></Uc:MonthlyExpenses>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>