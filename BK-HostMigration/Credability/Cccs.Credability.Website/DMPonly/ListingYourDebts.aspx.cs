﻿using System;

namespace Cccs.Credability.Website.DmpOnly
{
	public partial class UserDebtListingDmpOnly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Debt);
			CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.USER_DEBT_LISTING, SessionState.Username);
			if(!IsPostBack)
			{
				ShowHideSpanControls();
			}
		}

		public void ShowHideSpanControls()
		{
			System.Web.UI.HtmlControls.HtmlGenericControl PageHead = UcDebtList.FindControl("PageHeadDmp") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(PageHead != null)
			{
				PageHead.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl TopText = UcDebtList.FindControl("TopTextGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(TopText != null)
			{
				TopText.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl BottomText = UcDebtList.FindControl("BottomTextGenral") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(BottomText != null)
			{
				BottomText.Visible = true;
			}
		}
	}
}