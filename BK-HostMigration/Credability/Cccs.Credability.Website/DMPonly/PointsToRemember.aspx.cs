﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.DmpOnlyControls;
using  Cccs.Credability.Website.Code;
using Cccs.Credability.Website.Controls.Navigation;

namespace Cccs.Credability.Website.DmpOnly
{
    public partial class PointsToRemember : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         //   Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Summary);
       
            CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.POINTS_TO_REMEMBER, SessionState.Username);
            UcNavigationControl1.OnContinue += new Website.Controls.UcNavigationControl.delegateOnContinue(UcNavigationControl1_eventOncontinue);
     
        }
        public void UcNavigationControl1_eventOncontinue()
        {
            UcPointsToRemember1.OnContinue();

        }
    }

  
}