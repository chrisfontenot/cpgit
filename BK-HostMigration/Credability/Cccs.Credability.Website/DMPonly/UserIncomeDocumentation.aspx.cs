﻿using System;

namespace Cccs.Credability.Website.DmpOnly
{
	public partial class UserIncomeDocumentationDmpOnly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//  Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Budget);
			CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.USER_INCOME_DOCUMENTATION, SessionState.Username);
			System.Web.UI.HtmlControls.HtmlGenericControl HeadSpan = UcUserIncome.FindControl("PageHeadDmpOnly") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(HeadSpan != null)
			{
				HeadSpan.Visible = true;
			}
			System.Web.UI.HtmlControls.HtmlGenericControl introText = UcUserIncome.FindControl("introText") as System.Web.UI.HtmlControls.HtmlGenericControl;
			if(introText != null)
			{
				introText.Visible = true;
			}
		}
	}
}
