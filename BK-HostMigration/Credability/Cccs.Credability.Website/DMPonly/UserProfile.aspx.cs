﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Credability.Website.Controls.DmpOnlyControls;
using Cccs.Credability.Website.Code;
using Cccs.Credability.Website.Controls.Navigation;

namespace Cccs.Credability.Website.DmpOnly
{
    public partial class UserProfileDmpOnly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

         //   Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Profile);

            CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.USER_PROFILE, SessionState.Username);
            UcUCIncome.PageHeading = Cccs.Credability.Website.App.Translate("Credability|UserProfile|StepTitle");
          
        }
    }
}
