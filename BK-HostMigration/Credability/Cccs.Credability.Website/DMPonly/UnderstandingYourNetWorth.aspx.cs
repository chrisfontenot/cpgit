﻿using System;

namespace Cccs.Credability.Website.DmpOnly
{
	public partial class UnderstandingYourNetWorthDmpOnly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Budget);

			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcFinancialAnalysis.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
                AuthCode.ChatCode = ChatCodeOption.UnnumberedChat;
                AuthCode.CodeLabel = "Credability|UnderstandingYourNetWorthDMP|ChatLabel";
			}
			CommonFunction.UserProgressSave(Cccs.Credability.Website.DMPonly.PercentComplete.UNDERSTANDING_YOUR_NET_WORTH, SessionState.Username);
		}
	}
}