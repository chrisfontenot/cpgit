﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnderstandingYourNetWorth.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.UnderstandingYourNetWorthDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs Understanding Your NetWorth DmpOnly" %>
<%@ Register Src="~/Controls/Shared/Pages/UcNetWorth.ascx" TagPrefix="Uc" TagName="UcUnderstandingYourNetWorth" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<script language="javascript" src="../Content/FormChek.js"></script>
	<Uc:UcUnderstandingYourNetWorth ID="UcFinancialAnalysis" runat="server" ChatAuthorizationVisible="true" IsSubmitButtonVisible="true" RedirectOnContinue="PointsToRemember.aspx" RedirectOnPrevious="MonthlyExpenses.aspx"></Uc:UcUnderstandingYourNetWorth>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>