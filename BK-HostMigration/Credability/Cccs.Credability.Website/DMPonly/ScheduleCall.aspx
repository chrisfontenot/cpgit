﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="ScheduleCall.aspx.cs" Inherits="Cccs.Credability.Website.DMPonly.ScheduleCall" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<div>
		<p />
		<p />
			<%=App.Translate("Credability|ScheduleCall|TopTxt")%>
		<p />
		<!-- BEGIN LivePerson Button Code -->
		<div class="lnkbutton">
			<a id="_lpChatBtn" href='https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToTalk&site=29009926&byhref=1&SESSIONVAR!skill=DMP%20Client&imageUrl=https://server.iad.liveperson.net/hcp/Gallery/CallButton-Gallery/English/General/1a/' target='call29009926' onclick="javascript:window.open('https://server.iad.liveperson.net/hc/29009926/?cmd=file&file=visitorWantsToTalk&site=29009926&SESSIONVAR!skill=DMP%20Client&imageUrl=https://server.iad.liveperson.net/hcp/Gallery/CallButton-Gallery/English/General/1a/&referrer='+escape(document.location),'call29009926','width=472,height=320,resizable=yes');return false;">
				<span style="font: normal 11px Arial, Helvetica, sans-serif; color: #FFFFFF">
					<%=App.Translate("Credability|ScheduleCall|ScheduleBtn")%></span></a>
			<!-- END LivePerson Button code -->
		</div>
		<p />
		<p />
		<p />
			<%=App.Translate("Credability|ScheduleCall|BottomTxt")%>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>