﻿using System;

namespace Cccs.Credability.Website.DMPonly
{
	public partial class CreditorScreenWithBenefits : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Cccs.Credability.Website.Controls.UcAuthorizationCode AuthCode = UcCreditorScreenWithBenefits1.FindControl("UcAuthCode") as Cccs.Credability.Website.Controls.UcAuthorizationCode;
			if(AuthCode != null)
			{
//				AuthCode.ChatCode=ChatCodeOption.Session1;
                AuthCode.ChatCode = ChatCodeOption.NoChat;
			}
			if(!IsPostBack)
			{
				// Master.BreadCrumbDmp.ActiveTabSet(BreadCrumbTemplate.Tab.Debt);
				CommonFunction.UserProgressSave(PercentComplete.CREDITORSCREEN, SessionState.Username);
				UcNavigationControl1.OnContinue += new Website.Controls.UcNavigationControl.delegateOnContinue(UcNavigationControl1_eventOncontinue);
			}
		}

		protected void UcNavigationControl1_eventOncontinue()
		{
			UcCreditorScreenWithBenefits1.onContinue();
		}
	}
}