﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Afs.aspx.cs" Inherits="Cccs.Credability.Website.DMPonly.AfsDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs Afs Dmp Only" %>
<%@ Register Src="~/Controls/Shared/Pages/UcAfs.ascx" TagPrefix="Uc" TagName="UCAfs" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <Uc:UCAfs ID="UcUCAfs" runat="server" RedirectOnContinue="WhatYouNeed_DMP.aspx" RedirectOnNeedAuthorization="DescribingYourSituation.aspx"></Uc:UCAfs>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>