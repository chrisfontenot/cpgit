﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpensesDmpOnly.aspx.cs" Inherits="Cccs.Credability.Website.DmpOnly.MonthlyExpensesDmpOnly" MasterPageFile="~/MasterPages/Master.Master" Title="Cccs Monthly Expenses for DmpOnly" %>
<%@ Register Src="~/Controls/BchControls/UCMonthlyExpenses.ascx" TagPrefix="Uc"  TagName="UCMonth"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="javascript" src="../Content/FormChek.js"></script>
<Uc:UCMonth id="UcUCMonth" runat="server" RedirectOnContinue="UserFinancialSituationRecap.aspx" RedirectOnPrevious="UserIncomeDocumentation.aspx"></Uc:UCMonth> 
</asp:Content>