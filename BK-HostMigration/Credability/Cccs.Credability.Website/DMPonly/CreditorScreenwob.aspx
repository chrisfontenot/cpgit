﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Master.Master" AutoEventWireup="true" CodeBehind="CreditorScreenwob.aspx.cs" Inherits="Cccs.Credability.Website.DMPonly.CreditorScreenWithoutBenefits" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ Register Src="../Controls/DmpOnlyControls/UcCreditorScreenWithoutBenefits.ascx" TagName="UcCreditorScreenWithoutBenefits" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Shared/Components/UcNavigationControl.ascx" TagName="UcNavigationControl" TagPrefix="uc1" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<uc1:UcCreditorScreenWithoutBenefits ID="UcCreditorScreenWithoutBenefits1" runat="server" RedirectOnContinue="DescribingYourSituation.aspx" RedirectOnPrevious="ListingYourDebts.aspx" />
	<br />
	<uc1:UcNavigationControl ID="UcNavigationControl1" runat="server" RedirectOnContinue="DescribingYourSituation.aspx" RedirectOnPrevious="ListingYourDebts.aspx" SaveAndSubmitToQueueButtonIsVisible="true" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>