﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Credability.Website.Rec
{
    public partial class UserProfileCheck : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            PersonalInfoData data = this.PersonalInfo1.ProcessData();
            if (data != null)
            {
                Response.Redirect("ListingYourDebts.aspx");
            }
        }
    }
}
