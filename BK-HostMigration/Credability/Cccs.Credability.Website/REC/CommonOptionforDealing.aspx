﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CommonOptionforDealing.aspx.cs" Inherits="Cccs.Credability.Website.Rec.CommonOptionforDealing"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Common Option for Dealing REC"%>
<%@ Register Src="~/Controls/RecControls/UCCommonOptionforDealing.ascx" TagPrefix="Uc"  TagName="UCCommonOptionforDealing"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
        <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCCommonOptionforDealing id="UcUCCommonOptionforDealing" runat="server"></Uc:UCCommonOptionforDealing>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>