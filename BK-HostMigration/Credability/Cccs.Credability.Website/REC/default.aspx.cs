﻿using System;

namespace Cccs.Credability.Website.Rec
{
	public partial class _default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Credability.AFS Afs = null;
			Afs = App.Credability.AfsGet(SessionState.UserId.Value, SessionState.ClientNumber.Value);
			if(Afs == null || String.IsNullOrEmpty(Afs.PrimaryPerson))
			{
				Response.Redirect("AFS.aspx");
			}
			else if(Afs.IsSecondaryPersonNeeded && String.IsNullOrEmpty(Afs.SecondaryPerson))
			{
				Response.Redirect("AFS.aspx");
			}
			else
			{
				Response.Redirect("UserProfile.aspx");
			}
		}
	}
}