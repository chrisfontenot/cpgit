﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Cccs.Credability.Website.Rec._default" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Welcome Back"%>
<%@ Register Src="~/Controls/Shared/Pages/UcDefault.ascx" TagPrefix="Uc" TagName="WelcomeBack"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
        <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:WelcomeBack id="UcUserInfo" runat="server"></Uc:WelcomeBack>
</asp:Content>
