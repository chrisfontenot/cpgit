﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribeYourSituation.aspx.cs" Inherits="Cccs.Credability.Website.Rec.DescribeYourSituation" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Describe Your Situation" %>
<%@ Register Src="~/Controls/RecControls/UCDescribeYourSituation.ascx" TagPrefix="Uc"  TagName="UCDescribeYourSituation"%>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
        <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UCDescribeYourSituation id="UcUCDescribeYourSituation" runat="server"></Uc:UCDescribeYourSituation>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>