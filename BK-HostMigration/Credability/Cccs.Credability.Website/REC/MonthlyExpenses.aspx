﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonthlyExpenses.aspx.cs" Inherits="Cccs.Credability.Website.Rec.MonthlyExpenses" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Monthly Expenses REC" %>
<%@ Register Src="~/Controls/Shared/Pages/UcMonthlyExpenses.ascx" TagPrefix="Uc" TagName="MonthlyExpenses" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
        <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script language="javascript" src="../Content/FormChek.js"></script>
	<Uc:MonthlyExpenses ID="UcMonthlyExpenses" runat="server" RedirectOnContinue="UserFinancialSituationRecap.aspx" RedirectOnPrevious="~/Controls/Shared/Pages/EnterPayrollDeductions.aspx?flow=/REC/"></Uc:MonthlyExpenses>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>