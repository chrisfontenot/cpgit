﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnderstandingYourNetWorth.aspx.cs" Inherits="Cccs.Credability.Website.Rec.UnderstandingYourNetWorth"  MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility Understanding Your Net Worth REC"%>
<%@ Register Src="~/Controls/Shared/Pages/UcNetWorth.ascx" TagPrefix="Uc" TagName="UcUnderstandingYourNetWorth" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc"  TagName="LPVariables"%>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="../Content/FormChek.js"></script>
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<Uc:UcUnderstandingYourNetWorth id="UcUnderstandingYourNetWorth" runat="server" RedirectOnContinue="Hud2.aspx" RedirectOnPrevious="UserFinancialSituationRecap.aspx" ReturnTo="UnderstandingYourNetWorth"></Uc:UcUnderstandingYourNetWorth>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder" runat="server">
    <Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>