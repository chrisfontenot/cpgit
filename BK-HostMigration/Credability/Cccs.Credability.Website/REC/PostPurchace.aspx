﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PostPurchace.aspx.cs" Inherits="Cccs.Credability.Website.Rec.PostPurchace" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility PostPurchace REC" %>
<%@ Register Src="~/Controls/Shared/Pages/UcPostPurchace.ascx" TagPrefix="Uc" TagName="UCPostPurchace" %>
<%@ Register Src="~/Controls/Shared/Components/LivePersonVariables.ascx" TagPrefix="Uc" TagName="LPVariables" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<Uc:UCPostPurchace ID="UcUCPostPurchace" runat="server" RedirectOnContinue="ListingYourDebts.aspx" RedirectOnPrevious="DescribeYourSituation.aspx"></Uc:UCPostPurchace>
</asp:Content>
<asp:Content ID="UCLPPlaceHolder" ContentPlaceHolderID="livePersonMonitorPlaceHolder"
	runat="server">
	<Uc:LPVariables ID="UCVariables" runat="server" />
</asp:Content>