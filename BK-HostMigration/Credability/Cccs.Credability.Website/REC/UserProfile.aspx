﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="Cccs.Credability.Website.Rec.UserProfile" MasterPageFile="~/MasterPages/Master.Master" Title="CredAbility REC User Profile" %>
<%@ Register Src="../Controls/Shared/Pages/UserProfile.ascx" TagName="UserProfile" TagPrefix="uc1" %>
<%@ MasterType TypeName="Cccs.Credability.Website.MastePages.Master" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    <link href="../Content/Rec.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
	<script type="text/javascript" language="javascript" src="../Content/FormChek.js"></script>
	<uc1:UserProfile ID="UserProfile1" runat="server" IsPriEmploymentEducationVisible="true" 
	IsEmailVisible="false" IsCounselingVisible="true" IsMilitaryServiceVisible="true" IsCoEmploymentVisible="true" WebsiteCode="BCH" RedirectOnContinue="DescribeYourSituation.aspx" RedirectOnNeedAuthorization="AFS.aspx"/>
</asp:Content>