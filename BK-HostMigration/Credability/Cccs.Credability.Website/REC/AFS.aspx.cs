﻿using System;

namespace Cccs.Credability.Website.Rec
{
	public partial class AFS : DefaultPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
                this.ShowLanguageCodes();

				Master.BreadCrumbRvm.Visible = false;
				Master.BreadCrumbBch.Visible = false;

				CommonFunction.UserProgressSave(PercentComplete.AFS, SessionState.Username);
			}
		}
	}
}
