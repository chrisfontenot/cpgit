﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Vanco
{
	public class VancoResult : Result<string>
	{
		private VancoError[] m_errors;

		public VancoError[] Errors 
		{
			get { return m_errors; }

			set
			{
				m_errors = value;

				Messages = (from e in m_errors select string.Format("{0}|{1}", e.ErrorCode, e.ErrorDescription)).ToArray();
			}
		}
	}
}
