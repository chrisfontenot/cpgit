﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Vanco
{
	public class VancoError
	{
		#region ErrorCode Constants

		public const int MISSING_CUSTOMER = 166;
		public const int MISSING_PAYMENT_METHOD = 167;
		public const int NOT_A_DEBIT_CARD = 179;
		public const int MISSING_CUSTOMER2 = 231;
		public const int NOT_LOGGED_IN = 380;
		public const int DENIED = 434;
		public const int INVALID_EXPIRATION_DATE = 183;
		public const int INVALID_CARD_NUMBER = 232;
		public const int INCOMPLETE_BILLING_ADDRESS = 184;
		public const int INVALID_STATE = 198;
		public const int INVALID_CUSTOMER_ID = 158;
		public const int CUSTOMER_ALREADY_EXISTS = 160;

		#endregion

		public int ErrorCode { get; set; }

		public string ErrorDescription { get; set; }
	}
}
