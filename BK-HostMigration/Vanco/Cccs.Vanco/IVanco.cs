﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Cccs.Vanco
{
	public interface IVanco
	{
		DateTime? LoginDts { get; }

		VancoResult Login(int cur_vanco_ref);

		VancoResult AddEditCustomer(int cur_vanco_ref, string van_cust_ref, int client_number, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_addr2, string dc_bill_city, string dc_bill_state, string dc_bill_zip);

		VancoResult AddEditPaymentMethod(int cur_vanco_ref, string van_cust_ref, int client_number, string van_pay_meth_ref, string dc_acctnum, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, int card_exp_mon, int card_exp_year);

		VancoResult AddTransaction(int cur_vanco_ref, string cur_inv_no, string van_cust_ref, string van_pay_meth_ref, double char_amt);
	}
}
