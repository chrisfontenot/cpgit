﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace Cccs.Vanco.Impl
{
	public class Vanco : IVanco
	{
		private const string THE_BOUNDRY = "*tH3-bOundRy#";

		private string m_vanco_uri;
		private string m_login;
		private string m_password;
		private string m_client_id;
		private string m_session_id;
        private string m_fund;
		private DateTime? m_login_dts;

        private readonly NLog.Logger logger;

        public Vanco(string vanco_uri, string login, string password, string client_id, string fund)
		{
			m_vanco_uri = vanco_uri;
			m_login = login;
			m_password = password;
			m_client_id = client_id;
            m_fund = fund;

            this.logger = NLog.LogManager.GetCurrentClassLogger();
        }

		private string VancoDateStr
		{
			get { return DateTime.Now.ToString("yyyy-MM-dd"); }
		}

		private string VancoDateTimeStr
		{
			get { return DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"); }
		}

		private static VancoError[] ParseErrorMessages(XDocument response_xml)
		{
			return 
		  (
				from e in response_xml.Descendants("Error") 
				select new VancoError
				{
					ErrorCode = int.Parse(e.Element("ErrorCode").Value),
					ErrorDescription = e.Element("ErrorDescription").Value
				}
			).ToArray();
		}

		public DateTime? LoginDts
		{
			get { return m_login_dts; }
		}

		public VancoResult Login(int cur_vanco_ref)
		{
			VancoResult result = new VancoResult();

			try
			{
				StringBuilder request_str = new StringBuilder();
				request_str.Append("--" + THE_BOUNDRY);
				request_str.AppendLine();
				request_str.Append("Content-Disposition: form-data; name=\"xml\"");
				request_str.AppendLine();
				request_str.AppendLine();
				request_str.Append("<VancoWS>");
				request_str.Append( "<Auth>");
				request_str.Append(  "<RequestType>Login</RequestType>");
				request_str.Append(  "<RequestID>LogIn" + cur_vanco_ref + "</RequestID>");
				request_str.Append(  "<RequestTime>" + VancoDateTimeStr + "</RequestTime>");
				request_str.Append( "</Auth>");
				request_str.Append("<Request>");
				request_str.Append( "<RequestVars>");
				request_str.Append(  "<UserID>" + m_login + "</UserID>");
				request_str.Append(  "<Password>" + m_password + "</Password>");
				request_str.Append(  "<ProductID>EFT</ProductID>");
				request_str.Append(  "</RequestVars>");
				request_str.Append( "</Request>");
				request_str.Append("</VancoWS> ");
				request_str.AppendLine();
				request_str.Append("--" + THE_BOUNDRY + "--");
				request_str.AppendLine();
				request_str.AppendLine();


				HttpWebRequest web_request = (HttpWebRequest) WebRequest.Create(m_vanco_uri);
				web_request.Method = "POST";
				web_request.ContentType = "multipart/form-data; boundary=" + THE_BOUNDRY;
				
				byte[] request_bytes = Encoding.ASCII.GetBytes(request_str.ToString());
				web_request.ContentLength = request_bytes.Length;

				using (Stream request_stream = web_request.GetRequestStream())
				{
					request_stream.Write(request_bytes, 0, request_bytes.Length);
				}

				WebResponse web_response = web_request.GetResponse();

				using (Stream response_stream = web_response.GetResponseStream())
				{
					using (StreamReader response_stream_reader = new StreamReader(response_stream))
					{
						string response_str = response_stream_reader.ReadToEnd();

						if (!string.IsNullOrEmpty(response_str))
						{
							XDocument response_xml = XDocument.Parse(response_str);

							result.Errors = ParseErrorMessages(response_xml);

							if (result.Errors.Length == 0)
							{
								result.Value = (from s in response_xml.Descendants("SessionID") select s.Value).FirstOrDefault();

								if (!string.IsNullOrEmpty(result.Value))
								{
									m_session_id = result.Value;
									m_login_dts = DateTime.Now;

									result.IsSuccessful = true;
								}
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
			}

			return result;
		}

		public VancoResult AddEditCustomer(int cur_vanco_ref, string van_cust_ref, int client_number, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_addr2, string dc_bill_city, string dc_bill_state, string dc_bill_zip)
		{
			VancoResult result = new VancoResult();

			try
			{
				StringBuilder request_str = new StringBuilder();
				request_str.Append("--" + THE_BOUNDRY);
                request_str.AppendLine();
                request_str.Append("Content-Disposition: form-data; name=\"xml\"");
				request_str.AppendLine();
				request_str.AppendLine();
				request_str.Append("<VancoWS>");
				request_str.Append( "<Auth>");
				request_str.Append(  "<RequestType>EFTAddEditCustomer</RequestType>");
				request_str.Append(  "<RequestID>LogIn" + cur_vanco_ref + "." + DateTime.Now.ToString("mmss") + "</RequestID>");
				request_str.Append(  "<RequestTime>" + VancoDateTimeStr + "</RequestTime>");
				request_str.Append(  "<SessionID>" + m_session_id + "</SessionID>");
				request_str.Append(  "<Version>1</Version>");
				request_str.Append( "</Auth>");
				request_str.Append( "<Request>");
				request_str.Append(  "<RequestVars>");
				request_str.Append(   "<ClientID>" + m_client_id + "</ClientID>");
				request_str.Append(   "<CustomerRef>" + van_cust_ref + "</CustomerRef>");
				request_str.Append(   "<CustomerID>" + client_number + "</CustomerID>");
				request_str.Append(   "<CustomerName>" + dc_fnameoncard + " " + dc_mnameoncard + " " + dc_lnameoncard + "</CustomerName>");
				request_str.Append(   "<CustomerAddress1>" + dc_bill_addr1 + "</CustomerAddress1>");
				request_str.Append(   "<CustomerAddress2>" + dc_bill_addr2 + "</CustomerAddress2>");
				request_str.Append(   "<CustomerCity>" + dc_bill_city + "</CustomerCity>");
				request_str.Append(   "<CustomerState>" + dc_bill_state + "</CustomerState>");
				request_str.Append(   "<CustomerZip>" + dc_bill_zip + "</CustomerZip>");
				request_str.Append(   "<CustomerPhone></CustomerPhone>");
				request_str.Append(  "</RequestVars>");
				request_str.Append( "</Request>");
				request_str.Append("</VancoWS> ");
				request_str.AppendLine();
				request_str.Append("--" + THE_BOUNDRY + "--");
				request_str.AppendLine();
				request_str.AppendLine();

                logger.DebugFormat("Web Request Data: {0}", request_str.ToString());
                logger.DebugFormat("Web Request to AddEditCustomer Url:  {0}", m_vanco_uri);

                ServicePointManager.ServerCertificateValidationCallback += 
                    delegate (
                        object sender,
                        X509Certificate certificate,
                        X509Chain chain,
                        SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(m_vanco_uri);
				web_request.Method = "POST";
				web_request.ContentType = "multipart/form-data; boundary=" + THE_BOUNDRY;

				byte[] request_bytes = Encoding.ASCII.GetBytes(request_str.ToString());
				web_request.ContentLength = request_bytes.Length;

				using (Stream request_stream = web_request.GetRequestStream())
				{
					request_stream.Write(request_bytes, 0, request_bytes.Length);
				}

                logger.Debug("getting response...");
				WebResponse web_response = web_request.GetResponse();

				using (Stream response_stream = web_response.GetResponseStream())
				{
					using (StreamReader response_stream_reader = new StreamReader(response_stream))
					{
						string response_str = response_stream_reader.ReadToEnd();

						if (!string.IsNullOrEmpty(response_str))
						{
                            logger.DebugFormat("Response string: {0}", response_str);
							XDocument response_xml = XDocument.Parse(response_str);

							result.Errors = ParseErrorMessages(response_xml);

                            if (result.Errors != null && result.Errors.Length == 0)
                            {
                                logger.Debug("No errors in response. setting call successful");
                                result.Value = (from s in response_xml.Descendants("CustomerRef") select s.Value).FirstOrDefault();
                                result.IsSuccessful = !string.IsNullOrEmpty(result.Value);
                            }
                            else
                            {
                                logger.DebugFormat("Number of errors: {0}", result.Errors.Length);
                            }
						}
                        else
                        {
                            logger.Debug("Response is blank or null.");
                        }
					}
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;

                logger.DebugFormat("AddEditCustomer: Error occured. Message: {0}, StackTrace: {1}", exception.Message, exception.StackTrace);
			}

			return result;
		}

		public VancoResult AddEditPaymentMethod(int cur_vanco_ref, string van_cust_ref, int client_number, string van_pay_meth_ref, string dc_acctnum, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, int card_exp_mon, int card_exp_year)
		{
			VancoResult result = new VancoResult();

			try
			{
				StringBuilder request_str = new StringBuilder();
				request_str.Append("--" + THE_BOUNDRY);
                request_str.AppendLine();
                request_str.Append("Content-Disposition: form-data; name=\"xml\"");
				request_str.AppendLine();
				request_str.AppendLine();
				request_str.Append("<VancoWS>");
				request_str.Append( "<Auth>");
				request_str.Append(  "<RequestType>EFTAddEditPaymentMethod</RequestType>");
				request_str.Append(  "<RequestID>LogIn" + cur_vanco_ref + "." + DateTime.Now.ToString("mmss") + "</RequestID>");
				request_str.Append(  "<RequestTime>" + VancoDateTimeStr + "</RequestTime>");
				request_str.Append(  "<SessionID>" + m_session_id + "</SessionID>");
				request_str.Append(  "<Version>1</Version>");
				request_str.Append( "</Auth>");
				request_str.Append( "<Request>");
				request_str.Append(  "<RequestVars>");
				request_str.Append(   "<ClientID>" + m_client_id + "</ClientID>");
				request_str.Append(   "<CustomerRef>" + van_cust_ref + "</CustomerRef>");
				request_str.Append(   "<CustomerID>" + client_number + "</CustomerID>");
				request_str.Append(   "<PaymentMethodRef>" + van_pay_meth_ref + "</PaymentMethodRef>");
				request_str.Append(   "<AccountType>CC</AccountType>");
				request_str.Append(   "<AccountNumber>" + dc_acctnum + "</AccountNumber>");
				request_str.Append(   "<SameCCBillingAddrAsCust>Yes</SameCCBillingAddrAsCust>");
				request_str.Append(   "<CardBillingName>" + dc_fnameoncard + " " + dc_mnameoncard + " " + dc_lnameoncard + "</CardBillingName>");
				request_str.Append(   "<CardExpMonth>" + card_exp_mon + "</CardExpMonth>");
				request_str.Append(   "<CardExpYear>" + card_exp_year + "</CardExpYear>");
				request_str.Append(  "</RequestVars>");
				request_str.Append( "</Request>");
				request_str.Append("</VancoWS> ");
				request_str.AppendLine();
				request_str.Append("--" + THE_BOUNDRY + "--");
				request_str.AppendLine();
				request_str.AppendLine();


				HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(m_vanco_uri);
				web_request.Method = "POST";
				web_request.ContentType = "multipart/form-data; boundary=" + THE_BOUNDRY;

				byte[] request_bytes = Encoding.ASCII.GetBytes(request_str.ToString());
				web_request.ContentLength = request_bytes.Length;

				using (Stream request_stream = web_request.GetRequestStream())
				{
					request_stream.Write(request_bytes, 0, request_bytes.Length);
				}

				WebResponse web_response = web_request.GetResponse();

				using (Stream response_stream = web_response.GetResponseStream())
				{
					using (StreamReader response_stream_reader = new StreamReader(response_stream))
					{
						string response_str = response_stream_reader.ReadToEnd();

						if (!string.IsNullOrEmpty(response_str))
						{
							XDocument response_xml = XDocument.Parse(response_str);

							result.Errors = ParseErrorMessages(response_xml);

							if (result.Errors.Length == 0)
							{
								result.Value = (from s in response_xml.Descendants("PaymentMethodRef") select s.Value).FirstOrDefault();
								result.IsSuccessful = !string.IsNullOrEmpty(result.Value);
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
			}

			return result;
		}

		public VancoResult AddTransaction(int cur_vanco_ref, string cur_inv_no, string van_cust_ref, string van_pay_meth_ref, double char_amt)
		{
			VancoResult result = new VancoResult();

			try
			{
				StringBuilder request_str = new StringBuilder();
				request_str.Append("--" + THE_BOUNDRY);
                request_str.AppendLine();
                request_str.Append("Content-Disposition: form-data; name=\"xml\"");
				request_str.AppendLine();
				request_str.AppendLine();
				request_str.Append("<VancoWS>");
				request_str.Append( "<Auth>");
				request_str.Append(  "<RequestType>EFTAddTransaction</RequestType>");
				request_str.Append(  "<RequestID>LogIn" + cur_inv_no + "</RequestID>");
				request_str.Append(  "<RequestTime>" + VancoDateTimeStr + "</RequestTime>");
				request_str.Append(  "<SessionID>" + m_session_id + "</SessionID>");
				request_str.Append(  "<Version>1</Version>");
				request_str.Append( "</Auth>");
				request_str.Append( "<Request>");
				request_str.Append(  "<RequestVars>");
				request_str.Append(   "<ClientID>" + m_client_id + "</ClientID>");
				request_str.Append(   "<CustomerRef>" + van_cust_ref + "</CustomerRef>");
				request_str.Append(   "<PaymentMethodRef>" + van_pay_meth_ref + "</PaymentMethodRef>");
				request_str.Append(   "<StartDate>" + VancoDateStr + "</StartDate>");
				request_str.Append(   "<FrequencyCode>O</FrequencyCode>");
				request_str.Append(   "<TransactionTypeCode>WEB</TransactionTypeCode>");
				request_str.Append(   "<Funds>");
				request_str.Append(    "<Fund>");
				request_str.Append(     "<FundID>" + m_fund + "</FundID>");
				request_str.Append(     "<FundAmount>" + char_amt + "</FundAmount>");
				request_str.Append(    "</Fund>");
				request_str.Append(   "</Funds>");
				request_str.Append(  "</RequestVars>");
				request_str.Append( "</Request>");
				request_str.Append("</VancoWS> ");
				request_str.AppendLine();
				request_str.Append("--" + THE_BOUNDRY + "--");
				request_str.AppendLine();
				request_str.AppendLine();


				HttpWebRequest web_request = (HttpWebRequest)WebRequest.Create(m_vanco_uri);
				web_request.Method = "POST";
				web_request.ContentType = "multipart/form-data; boundary=" + THE_BOUNDRY;

				byte[] request_bytes = Encoding.ASCII.GetBytes(request_str.ToString());
				web_request.ContentLength = request_bytes.Length;

				using (Stream request_stream = web_request.GetRequestStream())
				{
					request_stream.Write(request_bytes, 0, request_bytes.Length);
				}

				WebResponse web_response = web_request.GetResponse();

				using (Stream response_stream = web_response.GetResponseStream())
				{
					using (StreamReader response_stream_reader = new StreamReader(response_stream))
					{
						string response_str = response_stream_reader.ReadToEnd();

						if (!string.IsNullOrEmpty(response_str))
						{
							XDocument response_xml = XDocument.Parse(response_str);

							result.Errors = ParseErrorMessages(response_xml);

							if (result.Errors.Length == 0)
							{
								result.Value = (from s in response_xml.Descendants("TransactionRef") select s.Value).FirstOrDefault();
								result.IsSuccessful = !string.IsNullOrEmpty(result.Value);
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
			}

			return result;
		}
	}
}
