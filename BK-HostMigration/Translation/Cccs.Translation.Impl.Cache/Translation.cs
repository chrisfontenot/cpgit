﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Caching;

namespace Cccs.Translation.Impl.Cache
{
	public class Translation : ITranslation
	{
		private ITranslation m_translation;
		private ICacheManager m_cache;

		public Translation(ITranslation translation)
		{
			m_translation = translation;
			m_cache = CacheFactory.GetCacheManager();
		}

		#region Language

		public Language[] LanguagesGet()
		{
			Language[] languages = m_cache.GetData(LANGUAGES) as Language[];

			if (languages == null) // Cache miss
			{
				languages = m_translation.LanguagesGet();

				if (languages != null) 
				{
					m_cache.Add(LANGUAGES, languages); // Cache the complete set of languages

					foreach (Language language in languages) // Cache each language individually
					{
						string cache_key = LanguageCacheKey(language.LanguageCode);

						if (!m_cache.Contains(cache_key))
						{
							m_cache.Add(cache_key, language);
						}
					}
				}
			}			

			return languages;
		}

		public Language LanguageGet(string language_code)
		{
			string cache_key = LanguageCacheKey(language_code);

			Language language = m_cache.GetData(cache_key) as Language;

			if (language == null)	// Cache miss
			{
				language = m_translation.LanguageGet(language_code);

				if (language != null)	// Cache language
				{
					m_cache.Add(cache_key, language);
				}
			}

			return language;
		}

		#endregion

		#region LanguageText

		public string LanguageTextGet(string language_code, string language_text_code)
		{
			string cache_key = LanguageTextCacheKey(language_code, language_text_code);
 
			string language_text = m_cache.GetData(cache_key) as string;

			if (language_text == null)
			{
				language_text = m_translation.LanguageTextGet(language_code, language_text_code);

				if (language_text != null) // Cache language text
				{
					m_cache.Add(cache_key, language_text);
				}
			}

			return language_text;
		}

		public string[] LanguageTextsGet(string language_code, string[] language_text_codes)
		{
			string cache_key = LanguageTextCacheKey(language_code, language_text_codes);

			string[] language_texts = m_cache.GetData(cache_key) as string[];

			if (language_texts == null)
			{
				language_texts = m_translation.LanguageTextsGet(language_code, language_text_codes);

				if (language_texts != null) // Cache language text
				{
					m_cache.Add(cache_key, language_texts);
				}
			}

			return language_texts;
		}

		#endregion

		#region Cache Keys
			
		private static string LANGUAGES = "Languages";

		private static string LanguageCacheKey(string language_code)
		{
			return string.Format("{0}_{1}", LANGUAGES, language_code);
		}

		private static string LanguageTextCacheKey(string language_code, string language_text_code)
		{
			return string.Format("{0}_{1}_{2}", LANGUAGES, language_code, language_text_code);
		}

		private static string LanguageTextCacheKey(string language_code, string[] language_text_codes)
		{
			StringBuilder cache_key = new StringBuilder(string.Format("{0}_{1}", LANGUAGES, language_code));

			foreach (string language_text_code in language_text_codes)
			{
				cache_key.Append('_');
				cache_key.Append(language_text_code);
			}

			return cache_key.ToString();
		}

		#endregion
	}
}
