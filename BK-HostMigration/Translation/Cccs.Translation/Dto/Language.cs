﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Translation
{
	[DataContract]
	public class Language
	{
		#region Constants
			public const string EN = "EN";
			public const string ES = "ES";
		#endregion

		[DataMember]
		public string LanguageCode { get; set; }

		[DataMember]
		public string LanguageName { get; set; }
	}
}
