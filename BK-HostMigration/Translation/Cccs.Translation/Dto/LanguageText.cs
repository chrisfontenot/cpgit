﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Translation
{
	[DataContract]
	public class LanguageText
	{
		[DataMember]
		public int LanguageTextId { get; set; }

		[DataMember]
		public string LanguageTextCode { get; set; }

		[DataMember]
		public string LanguageCode { get; set; }

		[DataMember]
		public string Text { get; set; }
	}
}
