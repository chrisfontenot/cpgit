﻿using System.ServiceModel;

namespace Cccs.Translation
{
	[ServiceContract]
	public interface ITranslation
	{
		#region Language

		[OperationContract]
		Language[] LanguagesGet();

		[OperationContract]
		Language LanguageGet(string language_code);

		#endregion

		#region LanguageText

		[OperationContract]
		string LanguageTextGet(string language_code, string language_text_code);

		[OperationContract]
		string[] LanguageTextsGet(string language_code, string[] language_text_codes);

		#endregion
	}
}
