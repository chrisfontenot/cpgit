﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Translation.Test
{
	class TranslationManager
	{
		private static ITranslation m_translation_basic = null;
		private static ITranslation m_translation_cache = null;
		private static Random m_random = new Random();

		public static ITranslation TranslationBasic
		{
			get 
			{	
				return m_translation_basic ?? (m_translation_basic = new Cccs.Translation.Impl.Translation()); 
			}
		}

		public static ITranslation TranslationCache
		{
			get 
			{ 
				return m_translation_cache ?? (m_translation_cache = new Cccs.Translation.Impl.Cache.Translation(TranslationBasic)); 
			}
		}

		public static ITranslation TranslationRandom
		{
			get { return ((m_random.Next() & 1) == 1) ? TranslationBasic : TranslationCache; }
		}
	}
}
