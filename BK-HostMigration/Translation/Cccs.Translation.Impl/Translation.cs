﻿using System;
using System.Linq;
using System.Data;
using Cccs.Translation.Dal;

namespace Cccs.Translation.Impl
{
	public class Translation : ITranslation
	{
		#region Language

		public Language[] LanguagesGet()
		{
			Language[] languages = null;

			using (TranslationDataContext entities = new TranslationDataContext())
			{
				languages =
				(
					from entity in entities.Languages
					select new Language
					{
						LanguageCode = entity.LanguageCode,
						LanguageName = entity.LanguageName,
					}
				).ToArray();
			}

			return languages;
		}

		public Language LanguageGet(string language_code)
		{
			Language language = null;

			using (TranslationDataContext entities = new TranslationDataContext())
			{
				language =
				(
					from entity in entities.Languages
					where (string.Compare(entity.LanguageCode, language_code, true) == 0) 
					select new Language
					{
						LanguageCode = entity.LanguageCode,
						LanguageName = entity.LanguageName,
					}
				).FirstOrDefault();
			}

			return language;
		}

		#endregion

		#region LanguageText

		public string LanguageTextGet(string language_code, string language_text_code)
		{
			string language_text = null;

			using (TranslationDataContext entities = new TranslationDataContext())
			{
				language_text =
				(
					from entity in entities.LanguageTexts
					where ((string.Compare(entity.Language.LanguageCode, language_code, true) == 0) && (string.Compare(entity.LanguageTextCode, language_text_code, true) == 0)) 
					select entity.Text
				).FirstOrDefault();
			}

			return language_text;
		}

		public string[] LanguageTextsGet(string language_code, string[] language_text_codes)
		{
			string[] language_texts = null;

			using (TranslationDataContext entities = new TranslationDataContext())
			{
				language_texts = new string[language_text_codes.Length];

				for (int c = 0; c < language_text_codes.Length; c++)
				{
					string language_text_code = language_text_codes[c];

					language_texts[c] =
					(
						from entity in entities.LanguageTexts
						where
						(
							(string.Compare(entity.Language.LanguageCode, language_code, true) == 0) && 
							(string.Compare(language_text_code, entity.LanguageTextCode) == 0)
						)
						select entity.Text
					).FirstOrDefault();
				}
			}

			return language_texts;
		}

		#endregion

	}
}