﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class Website
	{
		#region Database Constants
            public const string REC = "REC";
			public const string BCH  = "BCH";
			public const string BKC	 = "BKC";
			public const string BKDE = "BKDE";
			public const string CA	 = "CA";
			public const string DMP	 = "DMP";
			public const string HUD	 = "HUD";
			public const string PRP	 = "PRP";
			public const string RVM	 = "RvM";
            public const string BK = "BK"; //For sending the information to counsellor 
		#endregion

		[DataMember]
		public string WebsiteCode { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public string Url { get; set; }
	}
}

