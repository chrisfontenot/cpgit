﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class SecurityQuestion
	{
		[DataMember(IsRequired=true)]
		public byte SecurityQuestionId { get; set; }

		[DataMember]
		public string Text { get; set; }
	}
}
