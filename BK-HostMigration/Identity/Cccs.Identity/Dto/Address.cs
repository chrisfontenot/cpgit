﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class Address
	{
		[DataMember(IsRequired = true)]
		public long AddressId { get; set; }

		[DataMember(IsRequired = true)]
		public long UserId { get; set; }

		[DataMember]
		public string AddressType { get; set; }

		[DataMember]
		public string StreetLine1 { get; set; }

		[DataMember]
		public string StreetLine2 { get; set; }

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public string State { get; set; }

		[DataMember]
		public string Zip { get; set; }
	}
}
