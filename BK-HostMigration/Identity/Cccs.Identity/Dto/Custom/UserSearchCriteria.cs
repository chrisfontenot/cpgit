﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserSearchCriteria
	{
		[DataMember]
		public string Username { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public string Ssn { get; set; }

		[DataMember]
		public DateTime? BirthDate { get; set; }

		[DataMember]
		public string Zip { get; set; }

		[DataMember]
		public bool? IsLocked { get; set; }

		[DataMember]
		public string Phone { get; set; }

		[DataMember]
		public string InternetId { get; set; }

		[DataMember]
		public string HostId { get; set; }


		public bool IsEmpty
		{
			get
			{
				return
				(
					((Username   == null) || string.IsNullOrEmpty(Username.Trim())) &&
					((FirstName  == null) || string.IsNullOrEmpty(FirstName.Trim())) &&
					((LastName   == null) || string.IsNullOrEmpty(LastName.Trim())) &&
					((Email      == null) || string.IsNullOrEmpty(Email.Trim())) &&
					((Ssn        == null) || string.IsNullOrEmpty(Ssn.Trim())) &&
					(BirthDate   == null) &&
					((Zip        == null) || string.IsNullOrEmpty(Zip.Trim())) &&
					((Phone      == null) || string.IsNullOrEmpty(Phone.Trim())) &&
					((InternetId == null) || string.IsNullOrEmpty(InternetId.Trim())) &&
					((HostId     == null) || string.IsNullOrEmpty(HostId.Trim())) &&
					(IsLocked     == null)
				);
			}
		}
	}
}
