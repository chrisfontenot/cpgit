﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserRegistration
	{
		[DataMember]
		public string Username { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public string Password { get; set; }

		[DataMember]
		public string PasswordConfirm { get; set; }

		[DataMember]
		public SecurityQuestionAnswer SecurityQuestionAnswer { get; set; }

		[DataMember]
		public string LanguageCode { get; set; }
	}
}
