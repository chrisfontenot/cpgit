﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserProfile : User
	{
		[DataMember]
		public UserDetail UserDetailPrimary { get; set; }

		[DataMember]
		public UserDetail UserDetailSecondary { get; set; }
		
		[DataMember]
		public Address[] Addresses { get; set; }

		[DataMember]
		public Account[] Accounts { get; set; }

		[DataMember]
		public UserWebsite[] UserWebsites { get; set; }
	}
}
