﻿using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class NewPasswordErrors
	{
		[DataMember]
		public bool? IsTooShort { get; set; }

		[DataMember]
		public bool? IsTooLong { get; set; }

		[DataMember]
		public bool? ContainsUsername { get; set; }

		[DataMember]
		public bool? ContainsFirstName { get; set; }

		[DataMember]
		public bool? ContainsLastName { get; set; }

		[DataMember]
		public bool? ContainsPassword { get; set; }

		[DataMember]
		public bool? ContainsSequence { get; set; }

		[DataMember]
		public bool? ContainsInvalidChar { get; set; }

		[DataMember]
		public bool? ContainsNoLetter { get; set; }

		[DataMember]
		public bool? ContainsNoNumber { get; set; }


	}
}
