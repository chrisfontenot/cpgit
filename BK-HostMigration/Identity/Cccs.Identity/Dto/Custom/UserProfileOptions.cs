﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserProfileOptions
	{
		[DataMember(IsRequired = true)]
		public bool IncludeUserDetails { get; set; }

		[DataMember(IsRequired = true)]
		public bool IncludeSecurityQuestionAnswers { get; set; }

		[DataMember(IsRequired = true)]
		public bool IncludeAddresses { get; set; }

		[DataMember(IsRequired = true)]
		public bool IncludeAccounts { get; set; }

		[DataMember(IsRequired = true)]
		public bool IncludeUserWebsites { get; set; }
	}
}
