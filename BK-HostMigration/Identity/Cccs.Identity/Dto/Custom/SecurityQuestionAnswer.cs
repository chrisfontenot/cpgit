﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class SecurityQuestionAnswer : SecurityQuestion
	{
		[DataMember]
		public string SecurityAnswer { get; set; }
	}
}
