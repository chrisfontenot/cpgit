﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserDetail
	{
		[DataMember(IsRequired = true)]
		public long UserDetailId { get; set; }

		[DataMember(IsRequired = true)]
		public long UserId { get; set; }

		[DataMember(IsRequired = true)]
		public bool IsPrimary { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string MiddleName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string Ssn { get; set; }

		[DataMember]
		public DateTime? BirthDate { get; set; }

		[DataMember]
		public bool? IsMale { get; set; }

		[DataMember]
		public Race Race { get; set; }

		[DataMember]
		public bool? IsHispanic { get; set; }

		[DataMember]
		public MaritalStatus MaritalStatus { get; set; }

		[DataMember]
		public string Email { get; set; }

		[DataMember]
		public string PhoneCell { get; set; }

		[DataMember]
		public string PhoneHome { get; set; }

      [DataMember]
      public string PhoneWork { get; set; }

      [DataMember]
      public string Address1 { get; set; }

      [DataMember]
      public string Address2 { get; set; }

      [DataMember]
      public string City { get; set; }

      [DataMember]
      public string State { get; set; }

      [DataMember]
      public string Zipcode { get; set; }

   }
}
