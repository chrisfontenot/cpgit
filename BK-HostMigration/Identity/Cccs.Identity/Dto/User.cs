﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class User
	{
		[DataMember(IsRequired = true)]
		public long UserId { get; set; }

		[DataMember]
		public string Username { get; set; }

		[DataMember]
		public string NewPassword { get; set; }

		[DataMember]
		public string NewPasswordConfirm { get; set; }

		[DataMember]
		public DateTime? PasswordResetDate { get; set; }

		[DataMember(IsRequired = true)]
		public byte LoginFailures { get; set; }

		[DataMember]
		public DateTime? LastLoginDate { get; set; }

		[DataMember]
		public DateTime? LastLoginFailureDate { get; set; }

		[DataMember]
		public string LastLoginIpAddress { get; set; }

		[DataMember]
		public DateTime? LockedDate { get; set; }

		[DataMember]
		public SecurityQuestionAnswer SecurityQuestionAnswer { get; set; }

		[DataMember]
		public string LanguageCode { get; set; }

		[DataMember(IsRequired = true)]
		public bool IsMigratedUser { get; set; }

		[DataMember(IsRequired = true)]
		public bool IsFullProfile { get; set; }

		[DataMember(IsRequired = true)]
		public bool IsProfileUpdateRequired { get; set; }

		[DataMember]
		public string Avatar { get; set; }
	}
}
