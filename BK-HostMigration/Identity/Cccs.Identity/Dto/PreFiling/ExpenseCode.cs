﻿using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class ExpenseCode
    {
        [DataMember]
        public int ExpenseCategoryId { get; set; }
        [DataMember]
        public int ExpenseCodeId { get; set; }
        [DataMember]
        public string CodeName { get; set; }
        [DataMember]
        public string DisplayText { get; set; }
        [DataMember]
        public byte DisplayOrder { get; set; }
        [DataMember]
        public decimal SessionAmount { get; set; }
    }
}
