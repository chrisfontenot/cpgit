﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class ReIssueSearchResult
    {
        [DataMember]
        public long UserDetailId { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public List<ReIssueSearchResultSnapshot> Snapshots { get; set; }
    }
}
