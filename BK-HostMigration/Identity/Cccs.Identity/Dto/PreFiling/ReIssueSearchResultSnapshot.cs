﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class ReIssueSearchResultSnapshot
    {
        [DataMember]
        public long PreFilingSnapshotId { get; set; }
        [DataMember]
        public string CertificateNumber { get; set; }
        [DataMember]
        public DateTime CourseCompleted { get; set; }
    }
}
