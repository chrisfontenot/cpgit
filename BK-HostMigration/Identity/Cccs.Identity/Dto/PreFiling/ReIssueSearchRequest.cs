﻿using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class ReIssueSearchRequest
    {
        [DataMember]
        public long ClientNumber { get; set; }
        [DataMember]
        public string HostId { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string SSN { get; set; }
        [DataMember]
        public string CertificateNumber { get; set; }

        public bool IsEmpty
        {
            get
            {
                return ClientNumber == 0 && HostId.IsNullOrWhiteSpace()
                    && FirstName.IsNullOrWhiteSpace() && LastName.IsNullOrWhiteSpace()
                    && Email.IsNullOrWhiteSpace() && SSN.IsNullOrWhiteSpace()
                    && CertificateNumber.IsNullOrWhiteSpace();
            }
        }
    }
}
