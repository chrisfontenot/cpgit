﻿using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class Creditor
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public byte DisplayOrder { get; set; }
        [DataMember]
        public decimal InterestRate { get; set; }
        [DataMember]
        public decimal Balance { get; set; }
        [DataMember]
        public decimal MonthlyPayment { get; set; }
        [DataMember]
        public short MonthsToRepay { get; set; }
    }
}
