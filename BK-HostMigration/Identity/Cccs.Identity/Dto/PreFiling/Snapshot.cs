﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class Snapshot
    {
        [DataMember]
        public long PreFilingSnapshotId { get; set; }
        [DataMember]
        public long PreFilingActionPlanId { get; set; }
        [DataMember]
        public long UserDetailId { get; set; }
        [DataMember]
        public int ClientNumber { get; set; }
        [DataMember]
        public string CertificateNumber { get; set; }
        [DataMember]
        public string LanguageCode { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string JudicialDistrict { get; set; }
        [DataMember]
        public DateTime CourseCompleted { get; set; }
        [DataMember]
        public DateTime CertificateGenerated { get; set; }
        [DataMember]
        public string CertificateVersionType { get; set; }
        [DataMember]
        public string SsnLast4 { get; set; }
        [DataMember]
        public short CreditScore { get; set; }
        [DataMember]
        public string StreetLine1 { get; set; }
        [DataMember]
        public string StreetLine2 { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public decimal IncomeAfterTaxes { get; set; }
        [DataMember]
        public decimal LivingExpenses { get; set; }
        [DataMember]
        public decimal DebtPayment { get; set; }
        [DataMember]
        public decimal CashAtMonthEnd { get; set; }
        [DataMember]
        public decimal TotalAssets { get; set; }
        [DataMember]
        public decimal TotalLiabilities { get; set; }
        [DataMember]
        public decimal NetWorth { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public string CounselorName { get; set; }
        [DataMember]
        public string CounselorTitle { get; set; }
        [DataMember]
        public byte ArchiveStatus { get; set; }
        [DataMember]
        public decimal DmpInterestRate { get; set; }
        [DataMember]
        public decimal DmpBalance { get; set; }
        [DataMember]
        public decimal DmpMonthlyPayment { get; set; }
        [DataMember]
        public short DmpMonthsToRepay { get; set; }

        [DataMember]
        public IList<Creditor> Creditors { get; set; }
        [DataMember]
        public IList<ExpenseCategory> ExpenseCategories { get; set; }
        [DataMember]
        public IList<ExpenseCode> ExpenseCodes { get; set; }

        public Snapshot()
        {
            Creditors = new List<Creditor>();
            ExpenseCategories = new List<ExpenseCategory>();
            ExpenseCodes = new List<ExpenseCode>();
        }
    }
}
