﻿using System.Runtime.Serialization;

namespace Cccs.Identity.Dto.PreFiling
{
    [DataContract]
    public class ExpenseCategory
    {
        [DataMember]
        public int ExpenseCategoryId { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public string DisplayText { get; set; }
        [DataMember]
        public byte DisplayOrder { get; set; }
        [DataMember]
        public decimal SessionAmount { get; set; }
        [DataMember]
        public decimal PercentOfTotal { get; set; }
        [DataMember]
        public decimal RecommendedPercentage { get; set; }
        [DataMember]
        public decimal RecommendedTotal { get; set; }
    }
}
