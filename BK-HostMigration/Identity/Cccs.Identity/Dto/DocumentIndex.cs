﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Identity.Dto
{
    public class DocumentIndex
    {
        public string ID { get; set; }
        public string Type { get; set; }
		public DateTime Date { get; set; }
		public DateTime CertificateGenerated { get; set; }
		public string Url { get; set; }
    }
}
