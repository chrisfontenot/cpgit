﻿using System;
using System.Runtime.Serialization;
using System.Collections;

namespace Cccs.Identity
{
	[DataContract]
	public class Account
	{
		#region Database Constants

		public const string BKC = "BR.CAM";
		public const string BCH = "CAM";
		public const string CM  = "CM";
		public const string BKE = "WS.CAM";

		#endregion

		[DataMember(IsRequired = true)]
		public long AccountId { get; set; }

		[DataMember(IsRequired = true)]
		public long UserId { get; set; }

		[DataMember]
		public string AccountTypeCode { get; set; }

		[DataMember]
		public long? InternetId { get; set; }

		[DataMember]
		public string HostId { get; set; }
	}

	public static class AccountExtensions
	{
		public static Account FindByAccountTypeCode(this ICollection accounts, string account_type_code)
		{
			Account account = null;

			if (accounts != null)
			{
				foreach (Account acct in accounts)
				{
					if (string.Compare(acct.AccountTypeCode.Trim(), account_type_code.Trim(), true) == 0)
					{
						account = acct;
						break;
					}
				}
			}

			return account;
		}
	}
}
