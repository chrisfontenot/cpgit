﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class AccountType
	{
		#region Database Constants
			public const string BR_CAM = "BR.CAM";
			public const string CAM    = "CAM";
			public const string CM     = "CM";
			public const string WS_CAM = "WS.CAM";
		#endregion

		[DataMember]
		public string AccountTypeCode { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public string Description { get; set; }
	}
}
