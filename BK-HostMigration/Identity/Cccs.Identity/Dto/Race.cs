﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class Race
	{
		[DataMember(IsRequired = true)]
		public char RaceCode { get; set; }

		[DataMember]
		public string Text { get; set; }
	}
}
