﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class MaritalStatus
	{
		[DataMember(IsRequired = true)]
		public char MaritalStatusCode { get; set; }

		[DataMember]
		public string Text { get; set; }
	}
}
