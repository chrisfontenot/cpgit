﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserWebsite
	{
		[DataMember(IsRequired = true)]
		public long UserId { get; set; }

		[DataMember]
		public string WebsiteCode { get; set; }

		[DataMember]
		public long? AccountId { get; set; }

		[DataMember]
		public string LastCompletedPage { get; set; }

		[DataMember(IsRequired = true)]
		public byte PercentComplete { get; set; }

		[DataMember]
		public DateTime? CompletedDate { get; set; }
	}
}
