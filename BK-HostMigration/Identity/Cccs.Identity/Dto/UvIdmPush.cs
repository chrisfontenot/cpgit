﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Identity.Dto
{
	public class UvIdmPush
	{
		public long AccountId { get; set; }
		public string CounselorId { get; set; }
		public string Username { get; set; }
		public string PriCertNo { get; set; }
		public string SecCertNo { get; set; }
	}
}
