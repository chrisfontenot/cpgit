﻿using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserSaveResult : UserPasswordChangeResult
	{
		public UserSaveResult()
		{
		}

		public UserSaveResult(UserSaveResult result) : base(result)
		{
			UserId = result.UserId;
			IsUsernameMissing = result.IsUsernameMissing;
			IsInvalidUsername = result.IsInvalidUsername;
			IsDuplicateUsername = result.IsDuplicateUsername;
		}

		[DataMember]
		public long? UserId { get; set; }

		[DataMember]
		public bool? IsUsernameMissing { get; set; }

		[DataMember]
		public bool? IsInvalidUsername { get; set; }

		[DataMember]
		public bool? IsDuplicateUsername { get; set; }
	}
}
