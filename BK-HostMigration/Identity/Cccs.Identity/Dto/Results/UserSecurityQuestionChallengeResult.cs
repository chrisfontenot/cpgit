﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserSecurityQuestionChallengeResult : Result
	{
		[DataMember]
		public long? UserId { get; set; }	// User.UserId if the username was valid and the user is active

		[DataMember]
		public bool? IsInvalidUsername { get; set; } // A valid and active user was not found

		[DataMember]
		public bool? IsUserLocked { get; set; }	// User is locked and the lock has not yet expired

		[DataMember]
		public bool? AreUserAnswersMissing { get; set; } // User does not contain security question answer(s)

		[DataMember]
		public bool? IsIncorrectAnswer { get; set; }	// Incorrect answer to the user's first security question 
	}
}
