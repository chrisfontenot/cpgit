﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class AccountSaveResult : Result
	{
		[DataMember]
		public long? AccountId { get; set; }

		[DataMember]
		public bool? IsIncompleteWithCompletedDate { get; set; }

		[DataMember]
		public bool? IsCompleteWithoutCompletedDate { get; set; }
	}
}
