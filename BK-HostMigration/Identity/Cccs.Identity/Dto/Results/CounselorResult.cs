﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Cccs.Identity
{
    [DataContract]
    public class CounselorResult : Result
    {
        [DataMember]
        public int CounselorID { get; set; }

        [DataMember]
        public string CounselorCode { get; set; }

        [DataMember]
        public string CounselorName { get; set; }

        [DataMember]
        public string CounselorEmail { get; set; }
    }
}
