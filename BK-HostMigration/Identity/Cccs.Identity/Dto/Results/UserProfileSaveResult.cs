﻿using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserProfileSaveResult : UserSaveResult
	{
		public UserProfileSaveResult()
		{
		}

		public UserProfileSaveResult(UserSaveResult user_save_result)
		: base(user_save_result)
		{
		}

		[DataMember]
		public AddressSaveResult[] AddressSaveResults { get; set; }

		[DataMember]
		public UserDetailSaveResult UserDetailSaveResultPrimary { get; set; }

		[DataMember]
		public UserDetailSaveResult UserDetailSaveResultSecondary { get; set; }

		[DataMember]
		public AccountSaveResult[] AccountSaveResults { get; set; }

		[DataMember]
		public UserWebsiteSaveResult[] UserWebsiteSaveResults { get; set; }
	}
}
