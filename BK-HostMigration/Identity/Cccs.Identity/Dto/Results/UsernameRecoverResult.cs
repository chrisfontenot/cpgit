﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UsernameRecoverResult : Result
	{
		[DataMember]
		public string Username { get; set; } // Username if the email was valid and the user is active

		[DataMember]
		public long? UserId { get; set; }	// UserId if the email was valid and the user is active

		[DataMember]
		public bool? IsInvalidEmail { get; set; }	// No active user was found for the given email address

		[DataMember]
		public bool? IsUserLocked { get; set; }	// User is locked and lock has not yet expired

		[DataMember]
		public bool? AreUserAnswersMissing { get; set; } // User does not contain security question answer(s)

		[DataMember]
		public bool? IsIncorrectAnswer { get; set; }	// Incorrect answer to the user's first security question 
	}
}
