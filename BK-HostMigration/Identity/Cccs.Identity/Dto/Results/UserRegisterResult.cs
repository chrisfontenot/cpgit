﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserRegisterResult : UserSaveResult
	{
		[DataMember]
		public string SsoToken { get; set; } // Encrypted token for single-sign-on

		[DataMember]
		public bool? IsDuplicateEmail { get; set; }

		[DataMember]
		public bool? IsFirstNameMissing { get; set; }

		[DataMember]
		public bool? IsLastNameMissing { get; set; }

	}
}
