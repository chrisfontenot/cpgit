﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserPasswordChangeResult : Result
	{
		public UserPasswordChangeResult()
		{
		}

		public UserPasswordChangeResult(UserPasswordChangeResult result)
		: base(result)
		{
			IsInvalidUserId = result.IsInvalidUserId;
			IsUserLocked = result.IsUserLocked;
			IsInvalidOldPassword = result.IsInvalidOldPassword; 
			IsInvalidNewPasswordConfirm = result.IsInvalidNewPasswordConfirm; 
			IsUnchangedNewPassword = result.IsUnchangedNewPassword; 
			NewPasswordErrors = result.NewPasswordErrors; 
		}

		[DataMember]
		public bool? IsInvalidUserId { get; set; }
		
		[DataMember]
		public bool? IsUserLocked { get; set; }	// User is locked and lock has not yet expired

		[DataMember]
		public bool? IsInvalidOldPassword { get; set; }

		[DataMember]
		public bool? IsInvalidNewPasswordConfirm { get; set; }

		[DataMember]
		public bool? IsUnchangedNewPassword { get; set; }
		
		[DataMember]
		public NewPasswordErrors NewPasswordErrors { get; set; }
	}
}
