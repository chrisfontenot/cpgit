﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserAuthenticateResult : Result
	{
		[DataMember]
		public long? UserId { get; set; }	// User.UserId if the username was valid and the user is active

		[DataMember]
		public string SsoToken { get; set; } // Encrypted token for single-sign-on

		[DataMember]
		public bool? IsInvalidUsername { get; set; } // A valid and active user was not found

		[DataMember]
		public bool? IsInvalidPassword { get; set; } // The password did not match

		[DataMember]
		public DateTime? LastLoginDate { get; set; } // Previous Login Date

		[DataMember]
		public bool? IsUserLocked { get; set; }	// User is locked and lock has not yet expired
	}
}
