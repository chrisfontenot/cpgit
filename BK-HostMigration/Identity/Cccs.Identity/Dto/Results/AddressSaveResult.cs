﻿using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class AddressSaveResult : Result
	{
		[DataMember]
		public long? AddressId { get; set; }

		[DataMember]
		public bool? IsInvalidZipCode { get; set; }

		[DataMember]
		public string StateExpectedForZip { get; set; }	// Will be set when provided ZipCode did not match expected state
	}
}
