﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity
{
	[DataContract]
	public class UserDetailSaveResult : Result
	{
		[DataMember]
		public long? UserDetailId { get; set; }

		[DataMember]
		public bool? IsDuplicateEmail { get; set; }

		[DataMember]
		public bool? IsInvalidSsn { get; set; }

		[DataMember]
		public bool? IsDuplicateSsn { get; set; }

		[DataMember]
		public bool? IsFirstNameMissing { get; set; }

		[DataMember]
		public bool? IsLastNameMissing { get; set; }
	}
}
