﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Identity.Dto
{
    public class Certificate
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public bool HasPdfVersion { get; set; }
        public bool HasExpired { get; set; }
        public string Url { get; set; }
    }
}
