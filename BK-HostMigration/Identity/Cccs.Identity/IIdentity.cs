﻿using System.ServiceModel;
using System;
using Cccs.Identity.Dto;
using System.Collections.Generic;
using Cccs.Identity.Dto.PreFiling;

namespace Cccs.Identity
{
    [ServiceContract]
    public interface IIdentity
    {
        #region Authentication

        [OperationContract]
        UserAuthenticateResult UserAuthenticate(string username, string password, string ip_address);

        [OperationContract]
        bool UserSsoTokenValidate(long user_id, string sso_token);

        [OperationContract]
        void UserLogout(long user_id);

        [OperationContract]
        UsernameRecoverResult UsernameRecover(string email, string security_question_answer);

        [OperationContract]
        UserSecurityQuestionChallengeResult UserSecurityQuestionChallenge(string username, string security_question_answer);

        [OperationContract]
        UserPasswordChangeResult UserPasswordChange(long user_id, string old_password, string new_password, string new_password_confirm);

        [OperationContract]
        UserPasswordChangeResult UserPasswordReset(long user_id, string new_password, string new_password_confirm, string audit_username);

        [OperationContract]
        string UserPasswordGenerate();

        [OperationContract]
        Result UserUnlock(long user_id, string audit_username);

        string CreateSsoToken(long user_id, Guid session_guid, DateTime last_login_date);
        #endregion

        #region	SecurityQuestion

        [OperationContract]
        SecurityQuestion[] SecurityQuestionsGet(string language_code);

        [OperationContract]
        SecurityQuestion SecurityQuestionGetByUsername(string username);

        [OperationContract]
        SecurityQuestion SecurityQuestionGetByEmail(string email);

        #endregion

        #region User

        [OperationContract]
        long? UserIdGetByUsername(string username);

        [OperationContract]
        long[] UserIdsGetByEmail(string email);

        [OperationContract]
        long? UserIdGetByHostId(string host_id);

        [OperationContract]
        long? UserIdGetBySsn(string ssn);

        [OperationContract]
        User UserGet(long user_id);

        [OperationContract]
        UserSaveResult UserSave(User user, string audit_username);

        [OperationContract]
        UserRegisterResult UserRegister(UserRegistration user_registration);

        [OperationContract]
        UserSearchResult UserSearch(UserSearchCriteria user_search_criteria);

        [OperationContract]
        byte[] DisplayCertificate(string certificateNumber);

        [OperationContract]
        byte[] DisplayCombinedCertificate(string certificateNumber);

        [OperationContract]
        byte[] DisplayActionPlan(string certificateNumber);

        [OperationContract]
        byte[] DisplayActionPlanById(long actionPlanId);

        [OperationContract]
        byte[] DisplayDebtAnalysis(long actionPlanId);

        [OperationContract]
        List<Certificate> GetCertificates(long userId);

        [OperationContract]
        DocumentList GetClientDocuments(long userId);

        [OperationContract]
        Snapshot GetSnapshot(long actionPlanId);

		[OperationContract]
		Snapshot GetCurrentSnapshot(long userId);

		[OperationContract]
		Snapshot GetCurrentSnapshotForUserDetail(long UserDetailId);

        [OperationContract]
        Snapshot GetCertificateSnapshot(string certificateNumber);

        [OperationContract]
        void ReIssueCertificate(Snapshot currentSnapshot, string attorneyEmail);

        [OperationContract]
        void ReTryCertificateDownload(long preFilingSnapshotId, string attorneyEmail);

        [OperationContract]
        List<ReIssueSearchResult> SearchReIssueSnapshots(ReIssueSearchRequest request);

        #endregion

        #region UserDetail

        [OperationContract]
        UserDetail[] UserDetailsGet(long user_id);

        [OperationContract]
        UserDetail UserDetailGet(long user_id, bool is_primary);

        [OperationContract]
        UserDetailSaveResult UserDetailSave(UserDetail user_detail, string audit_username);

        [OperationContract]
        Result UserDetailDeleteSecondary(long user_id);

        #endregion

        #region UserProfile

        [OperationContract]
        UserProfile UserProfileGet(long user_id, UserProfileOptions user_profile_options);

        [OperationContract]
        UserProfileSaveResult UserProfileSave(UserProfile user_profile, string audit_username);

        #endregion

        #region Address

        [OperationContract]
        Address[] AddressesGet(long user_id);

        [OperationContract]
        Address AddressGet(long address_id);

        [OperationContract]
        AddressSaveResult AddressSave(Address addr, string audit_username);

        #endregion

        #region Account

        [OperationContract]
        Account[] AccountsGet(long user_id);

        [OperationContract]
        Account AccountGet(long user_id, string account_type_code);

        [OperationContract]
        Account AccountGetById(long account_id);

        [OperationContract]
        AccountSaveResult AccountSave(Account account, string audit_username);

        #endregion

        #region AccountType

        [OperationContract]
        AccountType[] AccountTypesGet();

        [OperationContract]
        AccountType AccountTypeGet(string account_type_code);

        #endregion

        #region Race

        [OperationContract]
        Race[] RacesGet(string language_code);

        [OperationContract]
        Race RaceGet(char race_code, string language_code);

        #endregion

        #region MaritalStatus

        [OperationContract]
        MaritalStatus[] MaritalStatusesGet(string language_code);

        [OperationContract]
        MaritalStatus MaritalStatusGet(char marital_status_code, string language_code);

        #endregion

        #region Website

        [OperationContract]
        Website[] WebsitesGet();

        #endregion

        #region UserWebsite

        [OperationContract]
        UserWebsite[] UserWebsitesGet(long user_id);

        [OperationContract]
        UserWebsite UserWebsiteGet(long user_id, string website_code);

        [OperationContract]
        UserWebsiteSaveResult UserWebsiteSave(UserWebsite user_website, string audit_username);

        #endregion

		[OperationContract]
		UvIdmPush GetIdmDataForUv(int InNo, long UserId);

        [OperationContract]
        string GetEducationCertificateNumber(long accountID);
        [OperationContract]
        CounselorResult GetCounselorByCode(string counselorCode);

        [OperationContract]
        CounselorResult GetCounselorByID(int counselorID);
    }
}