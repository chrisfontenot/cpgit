﻿using System;
using System.Linq;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		private const string VALID_USERNAME_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_-@.";

		public User UserGet(long user_id)
		{
			User user = null;

			using(IdentityDataContext entities = new IdentityDataContext())
			{
				entities.OpenEncryptionKey();

				var user_tmp =
				(
					from entity in entities.Users
					where ((entity.IsActive) && (entity.UserId == user_id))
					select new
					{
						User = new User
						{
							UserId = entity.UserId,
							Username = entity.Username,
							PasswordResetDate = entity.PasswordResetDate,
							LoginFailures = entity.LoginFailures,
							LastLoginDate = entity.LastLoginDate,
							LastLoginFailureDate = entity.LastLoginFailureDate,
							LastLoginIpAddress = entity.LastLoginIpAddress,
							LockedDate = entity.LockedDate,
							LanguageCode = entity.LanguageCode,
							IsMigratedUser = entity.IsMigratedUser,
							IsFullProfile = entity.IsFullProfile,
							IsProfileUpdateRequired = entity.IsProfileUpdateRequired,
							Avatar = entity.Avatar,
						},
						SecurityQuestion = entity.SecurityQuestion,
						SecurityAnswer = entities.DecryptText(entity.SecurityAnswer),
					}
				).FirstOrDefault();

				if(user_tmp != null)
				{
					user = user_tmp.User;

					if(user_tmp.SecurityQuestion != null)
					{
						user.SecurityQuestionAnswer = new SecurityQuestionAnswer()
						{
							SecurityQuestionId = user_tmp.SecurityQuestion.SecurityQuestionId,
							Text = m_translation.LanguageTextGet(user.LanguageCode, user_tmp.SecurityQuestion.Text),
							SecurityAnswer = user_tmp.SecurityAnswer
						};
					}
				}
			}

			return user;
		}

		public long? UserIdGetByUsername(string username)
		{
			long? user_id = null;

			using(IdentityDataContext context = new IdentityDataContext())
			{
				user_id = UserIdGetByUsername(username, context);
			}

			return user_id;
		}

		private long? UserIdGetByUsername(string username, IdentityDataContext context)
		{
			long? user_id = null;

			var found_user_id =
			(
				from entity in context.Users
				where (entity.IsActive && (string.Compare(entity.Username.Trim(), username.Trim(), true) == 0))
				select entity.UserId
			);

			if(found_user_id.Count() > 0)
			{
				user_id = found_user_id.First();
			}

			return user_id;
		}

		public long[] UserIdsGetByEmail(string email)
		{
			long[] user_id = null;

			using(IdentityDataContext context = new IdentityDataContext())
			{
				user_id = UserIdsGetByEmail(email, context);
			}

			return user_id;
		}

		private long[] UserIdsGetByEmail(string email, IdentityDataContext context)
		{
			long[] user_id = null;

			context.OpenEncryptionKey();

			user_id =
			(
				from entity in context.UserDetails
				where (entity.IsActive && entity.User.IsActive && (string.Compare(entity.Email.Trim(), email.Trim(), true) == 0))
				select entity.UserId
			).Distinct().ToArray();

			return user_id;
		}

		public long? UserIdGetBySsn(string ssn)
		{
			long? user_id = null;

			using(IdentityDataContext context = new IdentityDataContext())
			{
				user_id = UserIdGetBySsn(ssn, context);
			}

			return user_id;
		}

		private long? UserIdGetBySsn(string ssn, IdentityDataContext context)
		{
			long? user_id = null;

			context.OpenEncryptionKey();

			var found_user_id =
			(
				from entity in context.UserDetails
				where (entity.IsActive && entity.User.IsActive && (string.Compare(context.DecryptText(entity.Ssn).Trim(), ssn.Trim(), true) == 0))
				select entity.UserId
			).FirstOrDefault();

			if(found_user_id > 0)
			{
				user_id = found_user_id;
			}

			return user_id;
		}

		public long? UserIdGetByHostId(string host_id)
		{
			long? user_id = null;

			using(IdentityDataContext entities = new IdentityDataContext())
			{
				entities.OpenEncryptionKey();

				var found_user_id =
				(
					from entity in entities.Accounts
					where (entity.IsActive && entity.User.IsActive && (entity.HostId != null) && (string.Compare(entity.HostId.Trim(), host_id.Trim(), true) == 0))
					select entity.UserId
				).FirstOrDefault();

				if(found_user_id > 0)
				{
					user_id = found_user_id;
				}
			}

			return user_id;
		}

		public UserRegisterResult UserRegister(UserRegistration user_registration)
		{
			UserRegisterResult result = new UserRegisterResult();

			try
			{
				result.IsUsernameMissing = ((user_registration.Username == null) || (string.IsNullOrEmpty(user_registration.Username.Trim())));
				result.IsFirstNameMissing = ((user_registration.FirstName == null) || (string.IsNullOrEmpty(user_registration.FirstName.Trim())));
				result.IsLastNameMissing = ((user_registration.LastName == null) || (string.IsNullOrEmpty(user_registration.LastName.Trim())));
				result.IsInvalidUsername = !IsValidUsername(user_registration.Username);

				result.IsDuplicateUsername = ((!string.IsNullOrEmpty(user_registration.Username)) && (UserIdGetByUsername(user_registration.Username) != null));

				// TODO: PROD
				//long[] user_ids = !string.IsNullOrEmpty(user_registration.Email) ? UserIdsGetByEmail(user_registration.Email) : null;
				//result.IsDuplicateEmail = ((user_ids != null) && (user_ids.Length > 0));
				result.IsDuplicateEmail = false;

				if
				(
					(result.IsUsernameMissing == false) && (result.IsFirstNameMissing == false) && (result.IsLastNameMissing == false) &&
					(result.IsInvalidUsername == false) && (result.IsDuplicateUsername == false) /* && (result.IsDuplicateEmail == false)	*/
				)
				{
					using(IdentityDataContext entities = new IdentityDataContext())
					{
						// Create new User
						//
						Cccs.Identity.Dal.User new_user = new Cccs.Identity.Dal.User
						{
							LastLoginDate = DateTime.Now,
							SessionGuid = Guid.NewGuid(),
							CreatedDate = DateTime.Now,
							IsActive = true,
						};

						User user = new User
						{
							Username = user_registration.Username,
							NewPassword = user_registration.Password,
							NewPasswordConfirm = user_registration.PasswordConfirm,
							SecurityQuestionAnswer = user_registration.SecurityQuestionAnswer,
							LanguageCode = user_registration.LanguageCode,
							IsMigratedUser = false,
							IsFullProfile = false,
							IsProfileUpdateRequired = false,
						};


						if(DalUserUpdate(result, entities, new_user, user, user_registration.Username))
						{
							// Update UserDetail
							//
							Cccs.Identity.Dal.UserDetail new_user_detail = new Cccs.Identity.Dal.UserDetail
							{
								IsPrimary = true,
								FirstName = user_registration.FirstName.Replace("\"", String.Empty),
								LastName = user_registration.LastName.Replace("\"", String.Empty),
								Email = user_registration.Email,
								CreatedDate = DateTime.Now,
								ModifiedDate = DateTime.Now,
								ModifiedBy = user_registration.Username,
								IsActive = true,
								User = new_user,
							};

							entities.Users.InsertOnSubmit(new_user);
							entities.UserDetails.InsertOnSubmit(new_user_detail);
							entities.SubmitChanges();

							result.UserId = new_user.UserId;
							result.SsoToken = CreateSsoToken(user.UserId, new_user.SessionGuid.Value, new_user.LastLoginDate.Value);

							result.IsSuccessful = true;
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to register new User." };
			}

			return result;
		}

		public UserSaveResult UserSave(User user, string audit_username)
		{
			UserSaveResult result = new UserSaveResult();

			try
			{
				using(IdentityDataContext context = new IdentityDataContext())
				{
					UserSave(user, audit_username, context, result);
				}
			}
			catch(Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save User." };
			}

			return result;
		}

		private void UserSave(User user, string audit_username, IdentityDataContext context, UserSaveResult result)
		{
			try
			{
				result.IsUsernameMissing = ((user.Username == null) || (string.IsNullOrEmpty(user.Username.Trim())));

				if(result.IsUsernameMissing == false)
				{
					result.IsInvalidUsername = !IsValidUsername(user.Username);

					if(result.IsInvalidUsername == false)
					{
						long? user_id_by_username = UserIdGetByUsername(user.Username, context);
						result.IsDuplicateUsername = ((user_id_by_username != null) && (user_id_by_username != user.UserId));

						if(result.IsDuplicateUsername == false)
						{
							if(user.UserId == 0) // Insert New User
							{
								Cccs.Identity.Dal.User dal_user = new Cccs.Identity.Dal.User
								{
									CreatedDate = DateTime.Now,
									IsActive = true,
								};

								if(DalUserUpdate(result, context, dal_user, user, audit_username))
								{
									context.Users.InsertOnSubmit(dal_user);
									context.SubmitChanges();

									result.UserId = user.UserId = dal_user.UserId;
									result.IsSuccessful = true;
								}
							}
							else // Update existing user
							{
								long user_id = user.UserId;

								Cccs.Identity.Dal.User dal_user =
								(
									from entity in context.Users
									where (entity.IsActive && (entity.UserId == user_id))
									select entity
								).FirstOrDefault();

								if(dal_user != null)
								{
									if(DalUserUpdate(result, context, dal_user, user, audit_username))
									{
										context.SubmitChanges();

										result.UserId = dal_user.UserId;
										result.IsSuccessful = true;
									}
								}
								else
								{
									result.Messages = new string[] { string.Format("User #{0} not found.", user_id) };
								}
							}
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save User." };
			}
		}

		public UserSearchResult UserSearch(UserSearchCriteria user_search_criteria)
		{
			UserSearchResult result = new UserSearchResult();

			try
			{
				result.IsCriteriaMissing = user_search_criteria.IsEmpty;

				if(result.IsCriteriaMissing == false)
				{
					using(IdentityDataContext entities = new IdentityDataContext())
					{
						user_search_criteria.Username = string.IsNullOrEmpty(user_search_criteria.Username) ? string.Empty : user_search_criteria.Username.Trim().ToUpper();
						user_search_criteria.FirstName = string.IsNullOrEmpty(user_search_criteria.FirstName) ? string.Empty : user_search_criteria.FirstName.Trim().ToUpper();
						user_search_criteria.LastName = string.IsNullOrEmpty(user_search_criteria.LastName) ? string.Empty : user_search_criteria.LastName.Trim().ToUpper();
						user_search_criteria.Email = string.IsNullOrEmpty(user_search_criteria.Email) ? string.Empty : user_search_criteria.Email.Trim().ToUpper();
						user_search_criteria.Ssn = string.IsNullOrEmpty(user_search_criteria.Ssn) ? string.Empty : user_search_criteria.Ssn.Trim().ToUpper();
						user_search_criteria.Zip = string.IsNullOrEmpty(user_search_criteria.Zip) ? string.Empty : user_search_criteria.Zip.Trim().ToUpper();
						user_search_criteria.Phone = string.IsNullOrEmpty(user_search_criteria.Phone) ? string.Empty : user_search_criteria.Phone.Trim().ToUpper();
						user_search_criteria.InternetId = string.IsNullOrEmpty(user_search_criteria.InternetId) ? string.Empty : user_search_criteria.InternetId.Trim().ToUpper();
						user_search_criteria.HostId = string.IsNullOrEmpty(user_search_criteria.HostId) ? string.Empty : user_search_criteria.HostId.Trim().ToUpper();

						entities.OpenEncryptionKey();

						result.UserMatches =
						(
							from user in entities.Users
							join user_detail in entities.UserDetails on user equals user_detail.User into ud
							join address in entities.Addresses on user equals address.User into ad
							join account in entities.Accounts on user equals account.User into ac
							from user_detail in ud.DefaultIfEmpty()
							from address in ad.DefaultIfEmpty()
							from account in ac.DefaultIfEmpty()
							where
							(
								(user.IsActive) &&
								((user_search_criteria.IsLocked == null) || ((user.LockedDate != null) == user_search_criteria.IsLocked)) &&
								(string.IsNullOrEmpty(user_search_criteria.Username) || user.Username.Trim().ToUpper().Contains(user_search_criteria.Username)) &&
								(string.IsNullOrEmpty(user_search_criteria.FirstName) || ((user_detail.FirstName != null) && user_detail.FirstName.Trim().ToUpper().Contains(user_search_criteria.FirstName))) &&
								(string.IsNullOrEmpty(user_search_criteria.LastName) || ((user_detail.LastName != null) && user_detail.LastName.Trim().ToUpper().Contains(user_search_criteria.LastName))) &&
								(string.IsNullOrEmpty(user_search_criteria.Email) || ((user_detail.Email != null) && user_detail.Email.Trim().ToUpper().Contains(user_search_criteria.Email))) &&
								(string.IsNullOrEmpty(user_search_criteria.Ssn) || ((entities.DecryptText(user_detail.Ssn) != null) && entities.DecryptText(user_detail.Ssn).Trim().ToUpper().Contains(user_search_criteria.Ssn))) &&
								((user_search_criteria.BirthDate == null) || (user_detail.BirthDate == user_search_criteria.BirthDate)) &&
								(string.IsNullOrEmpty(user_search_criteria.Zip) || ((address.Zip != null) && address.Zip.Trim().ToUpper().Contains(user_search_criteria.Zip))) &&
								(string.IsNullOrEmpty(user_search_criteria.InternetId) || ((account.InternetId != null) && account.InternetId.ToString().Contains(user_search_criteria.InternetId))) &&
								(string.IsNullOrEmpty(user_search_criteria.HostId) || account.HostId.Trim().ToUpper().Contains(user_search_criteria.HostId)) &&
								(string.IsNullOrEmpty(user_search_criteria.Phone) ||
								(
									((user_detail.PhoneHome != null) && user_detail.PhoneHome.Trim().ToUpper().Contains(user_search_criteria.Phone)) ||
									((user_detail.PhoneCell != null) && user_detail.PhoneCell.Trim().ToUpper().Contains(user_search_criteria.Phone)) ||
									((user_detail.PhoneWork != null) && user_detail.PhoneWork.Trim().ToUpper().Contains(user_search_criteria.Phone))
								))
							)
							select new UserSearchResult.UserMatch
							{
								UserId = user.UserId,
								Username = user.Username,
								FirstName = user_detail.FirstName,
								MiddleName = user_detail.MiddleName,
								LastName = user_detail.LastName,
								City = address.City,
								State = address.State,
								Zip = address.Zip,
								Email = user_detail.Email,
								StreetLine1 = address.StreetLine1,
								PhoneHome = user_detail.PhoneHome,
								PhoneCell = user_detail.PhoneCell,
								PhoneWork = user_detail.PhoneWork,
								Ssn = entities.DecryptText(user_detail.Ssn),
								IsPrimary = user_detail.IsPrimary,
								IsLocked = (user.LockedDate != null),
							}
						).Distinct().ToArray();

						result.TotalMatches = (result.UserMatches != null) ? result.UserMatches.Length : 0;

						if(result.TotalMatches > MaxSearchResults)
						{
							UserSearchResult.UserMatch[] matches = result.UserMatches;

							Array.Resize(ref matches, MaxSearchResults);

							result.UserMatches = matches;
						}

						result.IsSuccessful = true;
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}


		#region Helper methods

		private bool DalUserUpdate(UserSaveResult result, IdentityDataContext entities, Cccs.Identity.Dal.User dal_user, User user, string audit_username)
		{
			bool is_successful = false;

			bool has_answers = (user.SecurityQuestionAnswer != null);

			dal_user.Username = user.Username;
			dal_user.LockedDate = user.LockedDate;
			dal_user.PasswordResetDate = user.PasswordResetDate;
			dal_user.SecurityAnswer = has_answers ? entities.EncryptText(user.SecurityQuestionAnswer.SecurityAnswer) : null;
			dal_user.LanguageCode = user.LanguageCode;
			dal_user.IsFullProfile = user.IsFullProfile;
			dal_user.IsProfileUpdateRequired = user.IsProfileUpdateRequired;
			dal_user.Avatar = user.Avatar;
			dal_user.ModifiedBy = audit_username;
			dal_user.ModifiedDate = DateTime.Now;

			if(string.IsNullOrEmpty(user.NewPassword) || DoPasswordChange(result, entities, dal_user, null, user.NewPassword, user.NewPasswordConfirm, audit_username))
			{
				if(has_answers) // Don't delete security question answers if they were not provided
				{
					dal_user.SecurityQuestionId = user.SecurityQuestionAnswer.SecurityQuestionId;
				}

				is_successful = true;
			}

			return is_successful;
		}

		private bool IsValidUsername(string username)
		{
			bool is_valid_username = false;

			if(!string.IsNullOrEmpty(username))
			{
				is_valid_username = true;

				foreach(char c in username)
				{
					if(!VALID_USERNAME_CHARS.Contains(c))
					{
						is_valid_username = false;
						break;
					}
				}
			}

			return is_valid_username;
		}

		#endregion
	}
}