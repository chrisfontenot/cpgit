﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Data;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public Account[] AccountsGet(long user_id)
		{
			Account[] accounts = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				accounts =
				(
					from entity in entities.Accounts
					where ((entity.IsActive) && (entity.User.IsActive) && (entity.UserId == user_id))
					orderby entity.ModifiedDate descending
					select new Account
					{
						AccountId = entity.AccountId,
						UserId = user_id,
						AccountTypeCode = entity.AccountType.AccountTypeCode,
						InternetId = entity.InternetId,
						HostId = entity.HostId,
					}
				).ToArray();
			}

			return accounts;
		}

		public Account AccountGet(long user_id, string account_type_code)
		{
			Account account = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				account =
				(
					from entity in entities.Accounts
					where 
					(
					  entity.IsActive && (string.Compare(entity.AccountTypeCode.Trim(), account_type_code.Trim(), true) == 0) && 
						(entity.User.IsActive) && (entity.UserId == user_id) 
					)
					select new Account
					{
						AccountId = entity.AccountId,
						UserId = user_id,
						AccountTypeCode = entity.AccountTypeCode,
						InternetId = entity.InternetId,
						HostId = entity.HostId,
					}
				).FirstOrDefault();
			}

			return account;
		}

		public Account AccountGetById(long account_id)
		{
			Account account = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				account =
				(
					from entity in entities.Accounts
					where	(	entity.IsActive && (entity.AccountId == account_id)	)
					select new Account
					{
						AccountId = account_id,
						UserId = entity.UserId,
						AccountTypeCode = entity.AccountTypeCode,
						InternetId = entity.InternetId,
						HostId = entity.HostId,
					}
				).FirstOrDefault();
			}

			return account;
		}
		public AccountSaveResult AccountSave(Account account, string audit_username)
		{
			AccountSaveResult result = new AccountSaveResult();

			try
			{
				using (IdentityDataContext context = new IdentityDataContext())
				{
					AccountSave(account, audit_username, context, result);
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save." };
			}

			return result;
		} 

		private void AccountSave(Account account, string audit_username, IdentityDataContext context, AccountSaveResult result)
		{
			try
			{
				if (account.AccountId == 0) // Insert
				{
					DateTime now = DateTime.Now;

					Cccs.Identity.Dal.Account dal_account = new Cccs.Identity.Dal.Account
					{
						UserId = account.UserId,
						AccountTypeCode = account.AccountTypeCode,
						InternetId = (int?)account.InternetId,
						HostId = account.HostId,
						CreatedDate = now,
						ModifiedDate = now,
						ModifiedBy = audit_username,
						IsActive = true,
					};

					context.Accounts.InsertOnSubmit(dal_account);
					context.SubmitChanges();

					result.AccountId = account.AccountId = dal_account.AccountId;
					result.IsSuccessful = true;
				}
				else // Update
				{
					long account_id = account.AccountId;

					Cccs.Identity.Dal.Account dal_account =
					(
						from entity in context.Accounts
						where (entity.AccountId == account_id)
						select entity
					).FirstOrDefault();

					if (dal_account != null)
					{
						dal_account.AccountTypeCode = account.AccountTypeCode;
						dal_account.InternetId = (int?)account.InternetId;
						dal_account.HostId = account.HostId;
						dal_account.ModifiedBy = audit_username;
						dal_account.ModifiedDate = DateTime.Now;

						context.SubmitChanges();

						result.AccountId = dal_account.AccountId;
						result.IsSuccessful = true;
					}
					else
					{
						result.Messages = new string[] { string.Format("Account #{0} not found.", account_id) };
					}
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save." };
			}
		}
	}
}
