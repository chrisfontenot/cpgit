﻿using System;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		private int UserLockoutMinutes
		{
			get
			{
				int user_lockout_minutes = 60; // Default to 1 hour
				if(m_configuration != null)
				{
					string user_lockout_minutes_str = m_configuration.ValueGet("UserLockoutMinutes");
					if(!string.IsNullOrEmpty(user_lockout_minutes_str))
					{
						if(int.TryParse(user_lockout_minutes_str, out user_lockout_minutes))
						{
							user_lockout_minutes = Math.Max(0, user_lockout_minutes);
						}
					}
				}
				return user_lockout_minutes;
			}
		}

		private int UserLoginFailureMinutes
		{
			get
			{
				int user_login_failure_minutes = 60; // Default to 1 hour
				if(m_configuration != null)
				{
					string user_login_failure_minutes_str = m_configuration.ValueGet("UserLoginFailureMinutes");
					if(!string.IsNullOrEmpty(user_login_failure_minutes_str))
					{
						if(int.TryParse(user_login_failure_minutes_str, out user_login_failure_minutes))
						{
							user_login_failure_minutes = Math.Max(0, user_login_failure_minutes);
						}
					}
				}
				return user_login_failure_minutes;
			}
		}

		private int UserLoginFailureLimit
		{
			get
			{
				int user_login_failure_limit = 6; // Default to 6
				if(m_configuration != null)
				{
					string user_login_failure_limit_str = m_configuration.ValueGet("UserLoginFailureLimit");
					if(!string.IsNullOrEmpty(user_login_failure_limit_str))
					{
						if(int.TryParse(user_login_failure_limit_str, out user_login_failure_limit))
						{
							user_login_failure_limit = Math.Max(0, user_login_failure_limit);
						}
					}
				}
				return user_login_failure_limit;
			}
		}

		private int UserPasswordLengthMin
		{
			get
			{
				int user_password_length_min = 8; // Default to a minimum of 8
				if(m_configuration != null)
				{
					string user_password_length_min_str = m_configuration.ValueGet("UserPasswordLengthMin");
					if(!string.IsNullOrEmpty(user_password_length_min_str))
					{
						if(int.TryParse(user_password_length_min_str, out user_password_length_min))
						{
							user_password_length_min = Math.Max(4, user_password_length_min);	// Hardcode hard min limit of 4
						}
					}
				}
				return user_password_length_min;
			}
		}

		private int UserPasswordLengthMax
		{
			get
			{
				int user_password_length_max = 20; // Default to 20
				if(m_configuration != null)
				{
					string user_password_length_max_str = m_configuration.ValueGet("UserPasswordLengthMax");
					if(!string.IsNullOrEmpty(user_password_length_max_str))
					{
						if(int.TryParse(user_password_length_max_str, out user_password_length_max))
						{
							user_password_length_max = Math.Min(32, user_password_length_max);	// Hardcode hard max of 32
						}
					}
				}
				return user_password_length_max;
			}
		}

		private int UserPasswordResetDays
		{
			get
			{
				int user_password_reset_days = 365; // Default to 365
				if(m_configuration != null)
				{
					string user_password_reset_days_str = m_configuration.ValueGet("UserPasswordResetDays");
					if(!string.IsNullOrEmpty(user_password_reset_days_str))
					{
						if(int.TryParse(user_password_reset_days_str, out user_password_reset_days))
						{
							user_password_reset_days = Math.Max(0, user_password_reset_days);
						}
					}
				}
				return user_password_reset_days;
			}
		}

		private int MaxSearchResults
		{
			get
			{
				int max_search_results = 20; // Default to 20
				if(m_configuration != null)
				{
					string max_search_results_str = m_configuration.ValueGet("MaxSearchResults");
					if(!string.IsNullOrEmpty(max_search_results_str))
					{
						if(int.TryParse(max_search_results_str, out max_search_results))
						{
							max_search_results = Math.Max(0, max_search_results);
						}
					}
				}
				return max_search_results;
			}
		}

		private string DbEncryptionKey
		{
			get
			{
				string db_encryption_key = null;
				if(m_configuration != null)
				{
					db_encryption_key = m_configuration.ValueGet("DbEncryptionKey");
				}
				return db_encryption_key;
			}
		}

		private string DbEncryptionPassword
		{
			get
			{
				string db_encryption_password = null;
				if(m_configuration != null)
				{
					db_encryption_password = m_configuration.ValueGet("DbEncryptionPassword");
				}
				return db_encryption_password;
			}
		}
	}
}