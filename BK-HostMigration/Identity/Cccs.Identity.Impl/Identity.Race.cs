﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public Race[] RacesGet(string language_code)
		{
			Race[] races = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				races =
				(
					from entity in entities.Races
					orderby entity.SortOrder
					select new Race
					{
						RaceCode = entity.RaceCode,
						Text = entity.Text,
					}
				).ToArray();

				if (races != null) // Translate Race.Text
				{
					foreach (Race race in races)
					{
						race.Text = m_translation.LanguageTextGet(language_code, race.Text);
					}
				}
			}

			return races;
		}

		public Race RaceGet(char race_code, string language_code)
		{
			Race race = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				race =
				(
					from entity in entities.Races
					where (entity.RaceCode == race_code) 
					select new Race
					{
						RaceCode = entity.RaceCode,
						Text = entity.Text,
					}
				).FirstOrDefault();

				if (race != null) // Translate Race.Text
				{
					race.Text = m_translation.LanguageTextGet(language_code, race.Text);
				}
			}

			return race;
		}
	}
}
