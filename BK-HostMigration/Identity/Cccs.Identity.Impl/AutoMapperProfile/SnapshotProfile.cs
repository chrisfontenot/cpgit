﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Cccs.Identity.Dto.PreFiling;
using Cccs.Credability.Certificates.DataTransferObjects;

namespace Cccs.Identity.Impl.AutoMapperProfile
{
    public class SnapshotProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "SnapshotProfile";
            }
        }

        protected override void Configure()
        {
            CreateMap<PreFilingSnapshotDto, Snapshot>();
            CreateMap<Snapshot, PreFilingSnapshotDto>();

            CreateMap<PreFilingSnapshotCreditorDto, Creditor>();
            CreateMap<Creditor, PreFilingSnapshotCreditorDto>();

            CreateMap<PreFilingSnapshotExpenseCategoryDto, ExpenseCategory>()
                .ForMember(d => d.CategoryName, o => o.MapFrom(s => s.ExpenseCategory));
            CreateMap<ExpenseCategory, PreFilingSnapshotExpenseCategoryDto>()
                .ForMember(d => d.ExpenseCategory, o => o.MapFrom(s => s.CategoryName));

            CreateMap<PreFilingSnapshotExpenseCodeDto, ExpenseCode>()
                .ForMember(d => d.CodeName, o => o.MapFrom(s => s.ExpenseCode));
            CreateMap<ExpenseCode, PreFilingSnapshotExpenseCodeDto>()
                .ForMember(d => d.ExpenseCode, o => o.MapFrom(s => s.CodeName));
        }
    }
}
