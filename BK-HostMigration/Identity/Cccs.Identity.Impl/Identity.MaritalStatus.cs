﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public MaritalStatus[] MaritalStatusesGet(string language_code)
		{
			MaritalStatus[] marital_statuses = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				marital_statuses =
				(
					from entity in entities.MaritalStatus
					orderby entity.SortOrder
					select new MaritalStatus
					{
						MaritalStatusCode = entity.MaritalStatusCode,
						Text = entity.Text,
					}
				).ToArray();

				if (marital_statuses != null)	// Translate MaritalStatus.Text
				{
					foreach (MaritalStatus marital_status in marital_statuses)
					{
						marital_status.Text = m_translation.LanguageTextGet(language_code, marital_status.Text);
					}
				}
			}

			return marital_statuses;
		}

		public MaritalStatus MaritalStatusGet(char marital_status_code, string language_code)
		{
			MaritalStatus marital_status = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				marital_status =
				(
					from entity in entities.MaritalStatus
					where (entity.MaritalStatusCode == marital_status_code )
					select new MaritalStatus
					{
						MaritalStatusCode = entity.MaritalStatusCode,
						Text = entity.Text,
					}
				).FirstOrDefault();

				if (marital_status != null)	// Translate MaritalStatus.Text
				{
					marital_status.Text = m_translation.LanguageTextGet(language_code, marital_status.Text);
				}
			}

			return marital_status;
		}
	}
}
