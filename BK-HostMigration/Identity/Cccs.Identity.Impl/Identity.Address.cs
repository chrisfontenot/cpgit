﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Data;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		private const string ADDRESS_TYPE = "Home";

		public Address[] AddressesGet(long user_id)
		{
			Address[] addresses = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				addresses =
				(
					from entity in entities.Addresses
					where ((entity.IsActive) && (entity.UserId == user_id))
					select new Address
					{
						AddressId = entity.AddressId,
						UserId = user_id,
						AddressType = ADDRESS_TYPE, // TODO: entity.AddressType,
						StreetLine1 = entity.StreetLine1,
						StreetLine2 = entity.StreetLine2,
						City = entity.City,
						State = entity.State,
						Zip = entity.Zip,
					}
				).ToArray();
			}

			return addresses;
		}

		public Address AddressGet(long address_id)
		{
			Address address = null;

			using (IdentityDataContext entities = new IdentityDataContext() )
			{
				entities.OpenEncryptionKey();

				address =
				(
					from entity in entities.Addresses
					where ((entity.IsActive) && (entity.AddressId == address_id))
					select new Address
					{
						AddressId = address_id,
						UserId = entity.UserId,
						AddressType = ADDRESS_TYPE, // TODO: entity.AddressType,
						StreetLine1 = entity.StreetLine1,
						StreetLine2 = entity.StreetLine2,
						City = entity.City,
						State = entity.State,
						Zip = entity.Zip,
					}
				).FirstOrDefault();
			}

			return address;
		}

		public AddressSaveResult AddressSave(Address address, string audit_username)
		{
			AddressSaveResult result = new AddressSaveResult();

			try
			{
				using (IdentityDataContext context = new IdentityDataContext())
				{
					AddressSave(address, audit_username, context, result);
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save." };
			}

			return result;
		}

		private void AddressSave(Address address, string audit_username, IdentityDataContext context, AddressSaveResult result)
		{
			string state_code = m_geographics.StateCodeGetByZip(address.Zip);

			result.IsInvalidZipCode    = string.IsNullOrEmpty(state_code);
			result.StateExpectedForZip = (!string.IsNullOrEmpty(state_code) && string.Compare(address.State.Trim(), state_code.Trim(), true) != 0) ? state_code : null;
			
			if ((result.IsInvalidZipCode == false) && string.IsNullOrEmpty(result.StateExpectedForZip))
			{
				// Assume User<->Address is 1-to-1 for now
				address.AddressId = (from a in context.Addresses where (a.UserId == address.UserId) select a.AddressId).FirstOrDefault();
				
				if (address.AddressId == 0) // Insert
				{
					DateTime now = DateTime.Now;

					Cccs.Identity.Dal.Address dal_address = new Cccs.Identity.Dal.Address
					{
						UserId = address.UserId,
						AddressType = ADDRESS_TYPE, // TODO: address.AddressType,
						StreetLine1 = address.StreetLine1,
						StreetLine2 = address.StreetLine2,
						City = address.City,
						State = address.State,
						Zip = address.Zip,
						CreatedDate = now,
						ModifiedDate = now,
						ModifiedBy = audit_username,
						IsActive = true,
					};

					context.Addresses.InsertOnSubmit(dal_address);
					context.SubmitChanges();

					result.AddressId = address.AddressId = dal_address.AddressId;
					result.IsSuccessful = true;
				}
				else // Update
				{
					long addr_id = address.AddressId;

					Cccs.Identity.Dal.Address dal_address =
					(
						from entity in context.Addresses
						where (entity.AddressId == addr_id)
						select entity
					).FirstOrDefault();

					if (dal_address != null)
					{
						dal_address.AddressType = address.AddressType;
						dal_address.StreetLine1 = address.StreetLine1;
						dal_address.StreetLine2 = address.StreetLine2;
						dal_address.City = address.City;
						dal_address.State = address.State;
						dal_address.Zip = address.Zip;
						dal_address.ModifiedBy = audit_username;
						dal_address.ModifiedDate = DateTime.Now;

						context.SubmitChanges();

						result.AddressId = dal_address.AddressId;
						result.IsSuccessful = true;
					}
					else
					{
						result.Messages = new string[] { "Address not found." };
					}
				}
			}
		}
	}
}
