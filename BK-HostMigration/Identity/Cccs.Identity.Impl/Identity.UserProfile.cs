﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Data.Common;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public UserProfile UserProfileGet(long user_id, UserProfileOptions user_profile_options)
		{
			UserProfile user_profile = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				user_profile =
				(
					from entity in entities.Users
					where ((entity.IsActive) && (entity.UserId == user_id))
					select new UserProfile
					{
						UserId = entity.UserId,
						Username = entity.Username,
						PasswordResetDate = entity.PasswordResetDate,
						LoginFailures = entity.LoginFailures,
						LastLoginDate = entity.LastLoginDate,
						LockedDate = entity.LockedDate,
						LanguageCode = entity.LanguageCode,
						IsMigratedUser = entity.IsMigratedUser,
						IsFullProfile = entity.IsFullProfile,
						IsProfileUpdateRequired = entity.IsProfileUpdateRequired,
						Avatar = entity.Avatar,
					}
				).FirstOrDefault();

				if (user_profile != null)
				{
					if (((user_profile_options == null) || (user_profile_options.IncludeSecurityQuestionAnswers)))
					{
						user_profile.SecurityQuestionAnswer = SecurityQuestionAnswerGet(user_id, user_profile.LanguageCode);
					}

					if ((user_profile_options == null) || (user_profile_options.IncludeAddresses))
					{
						user_profile.Addresses = AddressesGet(user_id);
					}

					if ((user_profile_options == null) || (user_profile_options.IncludeUserDetails))
					{
						user_profile.UserDetailPrimary = UserDetailGet(user_id, true);
						user_profile.UserDetailSecondary = UserDetailGet(user_id, false);
					}

					if ((user_profile_options == null) || (user_profile_options.IncludeAccounts))
					{
						user_profile.Accounts = AccountsGet(user_id);
					}

					if ((user_profile_options == null) || (user_profile_options.IncludeUserWebsites))
					{
						user_profile.UserWebsites = UserWebsitesGet(user_id);
					}
				}
			}

			return user_profile;
		}

		public UserProfileSaveResult UserProfileSave(UserProfile user_profile, string audit_username)
		{
			UserProfileSaveResult result = new UserProfileSaveResult();

			try
			{
				using (IdentityDataContext context = new IdentityDataContext())
				{
					context.Connection.Open();

					using (DbTransaction transaction = context.Connection.BeginTransaction())
					{
						context.Transaction = transaction;

						try
						{
							UserSave(user_profile, audit_username, context, result);

							if (result.IsSuccessful)
							{
								// Save Addresses
								//
								if (user_profile.Addresses != null)
								{
									List<AddressSaveResult> results = new List<AddressSaveResult>(user_profile.Addresses.Length);

									foreach (Address address in user_profile.Addresses)
									{
										address.UserId = result.UserId.Value;

										AddressSaveResult asr = new AddressSaveResult();
										results.Add(asr);
											
										AddressSave(address, audit_username, context, asr);

										if ((asr == null) || !asr.IsSuccessful)
										{
											result.IsSuccessful = false;
										}
									}

									result.AddressSaveResults = results.ToArray();
								}

								// User Details
								//
								if (user_profile.UserDetailPrimary != null)
								{
									user_profile.UserDetailPrimary.UserId = result.UserId.Value;

									result.UserDetailSaveResultPrimary = new UserDetailSaveResult(); 
										
									UserDetailSave(user_profile.UserDetailPrimary, audit_username, context, result.UserDetailSaveResultPrimary);

									if (!result.UserDetailSaveResultPrimary.IsSuccessful)
									{
										result.IsSuccessful = false;
									}
								}

								if (user_profile.UserDetailSecondary != null)
								{
									user_profile.UserDetailSecondary.UserId = result.UserId.Value;

									result.UserDetailSaveResultSecondary = new UserDetailSaveResult(); 
										
									UserDetailSave(user_profile.UserDetailSecondary, audit_username, context, result.UserDetailSaveResultSecondary);

									if (!result.UserDetailSaveResultSecondary.IsSuccessful)
									{
										result.IsSuccessful = false;
									}
								}

								// Accounts
								//
								if (user_profile.Accounts != null)
								{
									List<AccountSaveResult> results = new List<AccountSaveResult>(user_profile.Accounts.Length);

									foreach (Account account in user_profile.Accounts)
									{
										account.UserId = result.UserId.Value;

										AccountSaveResult asr = new AccountSaveResult();
										results.Add(asr);
											
										AccountSave(account, audit_username, context, asr);

										if ((asr == null) || !asr.IsSuccessful)
										{
											result.IsSuccessful = false;
										}
									}

									result.AccountSaveResults = results.ToArray();
								}


								// UserWebsites
								//
								if (user_profile.UserWebsites != null)
								{
									List<UserWebsiteSaveResult> results = new List<UserWebsiteSaveResult>(user_profile.Accounts.Length);

									foreach (UserWebsite user_website in user_profile.UserWebsites)
									{
										user_website.UserId = result.UserId.Value;

										UserWebsiteSaveResult uwsr = new UserWebsiteSaveResult();
										results.Add(uwsr);
											
										UserWebsiteSave(user_website, audit_username, context, uwsr);

										if ((uwsr == null) || !uwsr.IsSuccessful)
										{
											result.IsSuccessful = false;
										}
									}

									result.UserWebsiteSaveResults = results.ToArray();
								}
							}

							if (result.IsSuccessful)
							{
								transaction.Commit();
							}
							else
							{
								result.UserId = null;
								transaction.Rollback();
							}
						}
						catch
						{
							result.UserId = null;
							transaction.Rollback();
							throw;
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.UserId = null;
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to register UserProfile." };
			}

			return result;
		}

	}
}
