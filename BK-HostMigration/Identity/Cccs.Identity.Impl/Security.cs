﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Cccs.Identity.Impl
{
	internal class Security
	{
		public static string GenerateSalt()
		{
			byte[] buf = new byte[8];

			new RNGCryptoServiceProvider().GetBytes(buf);

			return Convert.ToBase64String(buf);
		}

		public static string HashPassword(string password, string salt)
		{
			if (!string.IsNullOrEmpty(salt))
			{
				password += salt;
			}

			byte[] hashed_password = new SHA512Managed().ComputeHash(Encoding.UTF8.GetBytes(password));

			return Convert.ToBase64String(hashed_password);
		}

		public static bool CheckPassword(string password, string password_hash, string password_salt)
		{
			string check = string.IsNullOrEmpty(password_salt) ? password.Trim() : Security.HashPassword(password.Trim(), password_salt); // No Salt means clear-text password

			return (check == password_hash);
		}

	}
}
