﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Cccs.Credability.Certificates;
using Cccs.Credability.Certificates.DataTransferObjects;
using Cccs.Credability.Certificates.PreFiling;
using Cccs.Geographics;
using Cccs.Identity.Dal;
using Cccs.Identity.Dto;
using Cccs.Identity.Dto.PreFiling;
using Cccs.Translation;
using StructureMap;
using Cccs.Credability.Certificates.PreFiling.Queues;
using CuttingEdge.Conditions;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		private Cccs.Configuration.IConfiguration m_configuration = null;
		private ITranslation m_translation = null;
		private IGeographics m_geographics = null;

		public Identity(Cccs.Configuration.IConfiguration configuration, ITranslation translation, IGeographics geographics)
		{
			m_configuration = configuration;
			m_translation = translation;
			m_geographics = geographics;

			IdentityDataContext.SetEncryption(DbEncryptionKey, DbEncryptionPassword);
		}

		public byte[] DisplayCertificate(string certificateNumber)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var data = service.GetSnapshotByCertificateNumber(certificateNumber);

			var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
			var generatorType = assembly.GetType(data.CertificateVersionType);

			var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
			using(var stream = generator.CreateCertificate(data))
			{
				return stream.GetBuffer();
			}
		}

		public byte[] DisplayCombinedCertificate(string certificateNumber)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var primaryData = service.GetSnapshotByCertificateNumber(certificateNumber);
			var secondaryData = service.GetAssociatedSnapshotByCertificateNumber(certificateNumber);

			var assembly = typeof(IPreFilingCertificateGenerator).Assembly;
			var generatorType = assembly.GetType(primaryData.CertificateVersionType);

			var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
			using(var stream = generator.CreateCombinedCertificate(primaryData, secondaryData))
			{
				return stream.GetBuffer();
			}
		}

		public byte[] DisplayActionPlan(string certificateNumber)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var userRepository = ObjectFactory.GetInstance<IUserRepository>();
			var certificateData = service.GetSnapshotByCertificateNumber(certificateNumber);
			var associatedCertificateData = service.GetAssociatedSnapshotByCertificateNumber(certificateNumber);

			var userDetail = userRepository.GetDetail(certificateData.UserDetailId);
			var primaryData = userDetail.IsPrimary ? certificateData : associatedCertificateData;
			var secondaryData = !userDetail.IsPrimary ? certificateData : associatedCertificateData;

			var assembly = typeof(IPreFilingCertificateService).Assembly;
			var generatorType = assembly.GetType(certificateData.CertificateVersionType);

			var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
			using(var stream = generator.CreateActionPlan(primaryData, secondaryData))
			{
				return stream.GetBuffer();
			}
		}

		public byte[] DisplayActionPlanById(long actionPlanId)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var actionPlan = service.GetActionPlan(actionPlanId);
			if(actionPlan == null)
				throw new InvalidOperationException(String.Format("The specified action plan does not exist. {0}", actionPlanId));

			var primaryData = actionPlan.Snapshots[0];
			var secondaryData = null as PreFilingSnapshotDto;
			if(actionPlan.Snapshots.Count > 1)
			{
				secondaryData = actionPlan.Snapshots[1];
			}

			var assembly = typeof(IPreFilingCertificateService).Assembly;
			var generatorType = assembly.GetType(primaryData.CertificateVersionType);

			var generator = Activator.CreateInstance(generatorType) as IPreFilingCertificateGenerator;
			using(var stream = generator.CreateActionPlan(primaryData, secondaryData))
			{
				return stream.GetBuffer();
			}
		}

		public byte[] DisplayDebtAnalysis(long actionPlanId)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var actionPlan = service.GetActionPlan(actionPlanId);
			if(actionPlan == null)
				throw new InvalidOperationException(String.Format("The specified action plan does not exist. {0}", actionPlanId));

			var primaryData = actionPlan.Snapshots.OrderByDescending(s => s.PreFilingSnapshotId).First();
			var assembly = typeof(IDebtAnalysisGenerator).Assembly;
			var generatorType = assembly.GetType(primaryData.CertificateVersionType);

			var generator = Activator.CreateInstance(generatorType) as IDebtAnalysisGenerator;
			if(generator == null)
				throw new InvalidOperationException(String.Format("This action plan does not support Debt Analysis. {0}", actionPlanId));

			using(var stream = generator.CreateDebtAnalysis(primaryData))
			{
				return stream.GetBuffer();
			}
		}

		public List<Certificate> GetCertificates(long userId)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var certificateList = service.GetCertificateListForUser(userId);

			var list = new List<Certificate>();
			certificateList.ForEach(c =>
			{
				var item = new Certificate
				{
					Id = c.CertificateNumber,
					Name = c.CertificateNumber,
					Description = c.CertificateNumber,
					Date = DateTime.Parse(c.CourseCompleted),
					HasPdfVersion = true,
					HasExpired = false,
					Url = c.CertificateNumber,
				};
				list.Add(item);
			});

			return list;
		}

		public Snapshot GetSnapshot(long actionPlanId)
		{
			var repository = ObjectFactory.GetInstance<ISnapshotRepository>();
			var dto = repository.GetSnapshotByActionPlan(actionPlanId);

			var mapper = ObjectFactory.GetInstance<IMappingEngine>();
			var snapshot = mapper.Map<PreFilingSnapshotDto, Snapshot>(dto);

			return snapshot;
		}

		public Snapshot GetCertificateSnapshot(string certificateNumber)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var snapshotDto = service.GetSnapshotByCertificateNumber(certificateNumber);

			var snapshot = Mapper.Map<PreFilingSnapshotDto, Snapshot>(snapshotDto);
			return snapshot;
		}

		public Snapshot GetCurrentSnapshot(long userId)
		{
			var userRepository = ObjectFactory.GetInstance<IUserRepository>();
			var userDetail = userRepository.GetDetail(userId, true);
			if(userDetail == null)
				throw new UserDetailNotFoundException(String.Format("Unable to retrieve the primary UserDetail record for UserId '{0}'", userId.ToString()));

			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var dto = service.GetCurrentSnapshotForUserDetail(userDetail.UserDetailId);

			var mapper = ObjectFactory.GetInstance<IMappingEngine>();
			var snapshot = mapper.Map<PreFilingSnapshotDto, Snapshot>(dto);

			return snapshot;
		}

		public Snapshot GetCurrentSnapshotForUserDetail(long UserDetailId)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var dto = service.GetCurrentSnapshotForUserDetail(UserDetailId);

			var mapper = ObjectFactory.GetInstance<IMappingEngine>();
			var snapshot = mapper.Map<PreFilingSnapshotDto, Snapshot>(dto);

			return snapshot;
		}

		public List<ReIssueSearchResult> SearchReIssueSnapshots(ReIssueSearchRequest request)
		{
			Condition.Requires(request, "request").IsNotNull()
				.Evaluate(r => !r.IsEmpty);
			request.FirstName = request.FirstName.IsNullOrWhiteSpace() ? String.Empty : request.FirstName.Trim().ToUpper();
			request.LastName = request.LastName.IsNullOrWhiteSpace() ? String.Empty : request.LastName.Trim().ToUpper();

			using(var context = new IdentityDataContext())
			{
				IQueryable<Dal.UserDetail> userQuery = context.UserDetails;
				if(request.ClientNumber > 0)
					userQuery = userQuery.Where(d => d.User.Accounts.Where(a => a.InternetId == request.ClientNumber).Any());
				if(!request.HostId.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => d.User.Accounts.Where(a => a.HostId == request.HostId).Any());
				if(!request.CertificateNumber.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => d.PreFilingSnapshots.Where(s => s.CertificateNumber == request.CertificateNumber).Any());
				if(!request.Email.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => d.Email == request.Email);
				if(!request.FirstName.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => d.FirstName.ToUpper().Contains(request.FirstName));
				if(!request.LastName.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => d.LastName.ToUpper().Contains(request.LastName));
				if(!request.SSN.IsNullOrWhiteSpace())
					userQuery = userQuery.Where(d => context.DecryptText(d.Ssn) == request.SSN);

				// Only the last 100 users that match this request.
				userQuery = userQuery.OrderByDescending(d => d.UserDetailId).Take(100);

				context.OpenEncryptionKey();
				var queryResult = (from d in userQuery
								   select d).ToList();

				var list = Mapper.Map<IList<Dal.UserDetail>, List<ReIssueSearchResult>>(queryResult);
				return list;
			}
		}

		public void ReIssueCertificate(Snapshot currentSnapshot, string attorneyEmail)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var snapshotDto = Mapper.Map<Snapshot, PreFilingSnapshotDto>(currentSnapshot);
			snapshotDto = service.ReIssueSnapshotData(snapshotDto);

			var userRepository = ObjectFactory.GetInstance<IUserRepository>();
			var userDetail = userRepository.GetDetail(currentSnapshot.UserDetailId);

			var actionPlan = service.GetActionPlan(currentSnapshot.PreFilingActionPlanId);
			var associatedSnapshot = actionPlan.Snapshots.Where(s => s.PreFilingSnapshotId != currentSnapshot.PreFilingSnapshotId).FirstOrDefault();
			var primarySnapshot = userDetail.IsPrimary ? snapshotDto : associatedSnapshot;
			var secondarySnapshot = !userDetail.IsPrimary ? snapshotDto : associatedSnapshot;

			var preFilingActionPlan = service.CreateActionPlan(primarySnapshot, secondarySnapshot);
			service.AssignCertificateToSnapshot(snapshotDto.PreFilingSnapshotId);

			SendClientActionPlan(preFilingActionPlan.PreFilingActionPlanId, userDetail.Email);
			SendAttorneyCertificate(preFilingActionPlan.PreFilingActionPlanId, attorneyEmail);
		}

		public void ReTryCertificateDownload(long preFilingSnapshotId, string attorneyEmail)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			service.AssignCertificateToSnapshot(preFilingSnapshotId);

			var snapshot = service.GetSnapshot(preFilingSnapshotId);
			var userRepository = ObjectFactory.GetInstance<IUserRepository>();
			var userDetail = userRepository.GetDetail(snapshot.UserDetailId);

			SendClientActionPlan(snapshot.PreFilingActionPlanId, userDetail.Email);
			SendAttorneyCertificate(snapshot.PreFilingActionPlanId, attorneyEmail);
		}

		private void SendClientActionPlan(long preFilingActionPlanId, string clientEmail)
		{
			if(clientEmail.IsNullOrWhiteSpace())
				return;

			var clientMessage = new ClientActionPlanQueueMessage()
			{
				PreFilingActionPlanId = preFilingActionPlanId,
				EmailAddress = clientEmail,
			};

			var logger = ObjectFactory.GetInstance<ILoggingService>();
			using(var queue = new ClientActionPlanQueue(logger))
			{
				queue.SendActionPlan(clientMessage);
			}
		}

		private void SendAttorneyCertificate(long preFilingActionPlanId, string attorneyEmail)
		{
			if(attorneyEmail.IsNullOrWhiteSpace())
				return;

			var attorneyMessage = new AttorneyCertificateQueueMessage()
			{
				PreFilingActionPlanId = preFilingActionPlanId,
				EmailAddress = attorneyEmail,
			};

			var logger = ObjectFactory.GetInstance<ILoggingService>();
			using(var queue = new AttorneyCertificateQueue(logger))
			{
				queue.SendAttorneyCertificate(attorneyMessage);
			}
		}

		public DocumentList GetClientDocuments(long userId)
		{
			var service = ObjectFactory.GetInstance<IPreFilingCertificateService>();
			var certificateList = service.GetCertificateListForUser(userId);

			var list = new DocumentList();
			certificateList.ForEach(c =>
			{
				var item = new DocumentIndex
				{
					ID = c.CertificateNumber,
					Date = DateTime.Parse(c.CourseCompleted),
					CertificateGenerated = DateTime.Parse(c.CertificateGenerated),
					Type = "MyCertificate.pdf",
					Url = c.CertificateNumber,
				};
				list.Add(item);
			});

			var assembly = typeof(IDebtAnalysisGenerator).Assembly;
			var actionPlanList = service.GetActionPlanList(userId);
			actionPlanList.ForEach(a =>
			{
				var latestCertificate = a.Certificates.OrderByDescending(c => c.PreFilingSnapshotId).First();
				var item = new DocumentIndex
				{
					ID = a.PreFilingActionPlanId.ToString(),
					Date = DateTime.Parse(latestCertificate.CourseCompleted),
					CertificateGenerated = DateTime.Parse(latestCertificate.CertificateGenerated),
					Type = "MyActionPlan.pdf",
					Url = latestCertificate.CertificateNumber,
				};
				list.Add(item);

				var generatorType = assembly.GetType(latestCertificate.CertificateVersionType);
				var generator = Activator.CreateInstance(generatorType) as IDebtAnalysisGenerator;
				if(generator != null)
				{
					var debt = new DocumentIndex
					{
						ID = String.Format("D{0}", a.PreFilingActionPlanId.ToString()),
						Date = DateTime.Parse(latestCertificate.CourseCompleted),
						CertificateGenerated = DateTime.Parse(latestCertificate.CertificateGenerated),
						Type = "MyDebt.pdf",
						Url = latestCertificate.CertificateNumber,
					};
					list.Add(debt);
				}
			});
			return list;
		}

		public UvIdmPush GetIdmDataForUv(int InNo, long UserId)
		{
			UvIdmPush Result = new UvIdmPush();
			Result.PriCertNo = String.Empty;
			Result.SecCertNo = String.Empty;
			using(var DataConn = new IdentityDataContext())
			{
				var AccntData =
					(from Rec in DataConn.Accounts
					 where Rec.UserId == UserId && Rec.InternetId == InNo
					 select Rec).FirstOrDefault();
				if(AccntData != null)
				{
					Result.AccountId = AccntData.AccountId;
					Result.CounselorId = (AccntData.CounselorId != null) ? AccntData.CounselorId.Trim() : String.Empty;
				}

				var UserData =
					(from Rec in DataConn.Users
					 where Rec.UserId == UserId
					 select Rec).FirstOrDefault();
				if(UserData != null)
				{
					Result.Username = (UserData.Username != null) ? UserData.Username.Trim() : String.Empty;
				}

				var UserDtl =
					(from Rec in DataConn.UserDetails
					 where Rec.UserId == UserId
					 select Rec).ToList();
				foreach(var CurRec in UserDtl)
				{
					var CertData =
						(from Rec in DataConn.PreFilingSnapshots
						 where Rec.ClientNumber == InNo && Rec.UserDetailId == CurRec.UserDetailId
						 select Rec).OrderByDescending(CurSnap => CurSnap.CertificateGenerated).FirstOrDefault();

					if(CertData != null)
					{
						if(CurRec.IsPrimary)
							Result.PriCertNo = (CertData.CertificateNumber != null) ? CertData.CertificateNumber.Trim() : String.Empty;
						else
							Result.SecCertNo = (CertData.CertificateNumber != null) ? CertData.CertificateNumber.Trim() : String.Empty;
					}
				}
			}
			return Result;
		}
	}
}