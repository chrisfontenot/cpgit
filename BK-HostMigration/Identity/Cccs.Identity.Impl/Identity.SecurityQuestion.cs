﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Data.EntityClient;

namespace Cccs.Identity.Impl
{
    public partial class Identity : IIdentity
    {
        public SecurityQuestion[] SecurityQuestionsGet(string language_code)
        {
            SecurityQuestion[] security_questions = null;

            using (IdentityDataContext entities = new IdentityDataContext())
            {
                var security_questions_dal =
                (
                    from entity in entities.SecurityQuestions
                    orderby entity.SortOrder
                    select new
                    {
                        entity.SecurityQuestionId,
                        entity.Text
                    }
                );

                if (security_questions_dal != null)
                {
                    List<SecurityQuestion> security_question_list = new List<SecurityQuestion>();

                    foreach (var security_question_dal in security_questions_dal)
                    {
                        security_question_list.Add
                        (
                            new SecurityQuestion
                            {
                                SecurityQuestionId = security_question_dal.SecurityQuestionId,
                                Text = m_translation.LanguageTextGet(language_code, security_question_dal.Text)
                            }
                        );
                    }

                    security_questions = security_question_list.ToArray();
                }
            }

            return security_questions;
        }

        public SecurityQuestion SecurityQuestionGetByUsername(string username)
        {
            using (IdentityDataContext entities = new IdentityDataContext())
            {
                Cccs.Identity.Dal.User user =
                (
                    from entity in entities.Users
                    where (entity.IsActive && (string.Compare(entity.Username.Trim(), username.Trim(), true) == 0))
                    select entity
                ).FirstOrDefault();

                if ((user != null) && (user.SecurityQuestion != null))
                {
                    var securityQuestion = new SecurityQuestion()
                    {
                        SecurityQuestionId = user.SecurityQuestion.SecurityQuestionId,
                        Text = m_translation.LanguageTextGet(user.LanguageCode, user.SecurityQuestion.Text)
                    };

                    return securityQuestion;
                }
            }

            return null;
        }

        public SecurityQuestion SecurityQuestionGetByEmail(string email)
        {
            using (IdentityDataContext context = new IdentityDataContext())
            {
                long[] user_ids = UserIdsGetByEmail(email, context);

                if ((user_ids != null) && (user_ids.Length == 1))
                {
                    context.OpenEncryptionKey();

                    Cccs.Identity.Dal.User user =
                    (
                        from entity in context.UserDetails
                        where (entity.IsActive && entity.User.IsActive && (entity.UserId == user_ids[0]))
                        select entity.User
                    ).FirstOrDefault();

                    if ((user != null) && (user.SecurityQuestion != null))
                    {
                        var securityQuestion = new SecurityQuestion()
                        {
                            SecurityQuestionId = user.SecurityQuestion.SecurityQuestionId,
                            Text = m_translation.LanguageTextGet(user.LanguageCode, user.SecurityQuestion.Text)
                        };

                        return securityQuestion;
                    }
                }
            }

            return null;
        }

        private SecurityQuestionAnswer SecurityQuestionAnswerGet(long user_id, string language_code)
        {
            using (IdentityDataContext entities = new IdentityDataContext())
            {

                var security_question_answer_dal =
                (
                    from entity in entities.Users
                    where (entity.IsActive && (entity.UserId == user_id))
                    select new
                    {
                        SecurityQuestionId = (entity.SecurityQuestion == null) ? (byte?)null : entity.SecurityQuestion.SecurityQuestionId,
                        TranslationTextCode = (entity.SecurityQuestion == null) ? null : entity.SecurityQuestion.Text,
                        SecurityAnswer = entity.SecurityAnswer,
                    }
                ).FirstOrDefault();

                if ((security_question_answer_dal != null) && (security_question_answer_dal.SecurityQuestionId != null))
                {
                    entities.OpenEncryptionKey();
                    var answer = new SecurityQuestionAnswer()
                    {
                        SecurityQuestionId = security_question_answer_dal.SecurityQuestionId.Value,
                        Text = m_translation.LanguageTextGet(language_code, security_question_answer_dal.TranslationTextCode),
                        SecurityAnswer = entities.DecryptText(security_question_answer_dal.SecurityAnswer),
                    };

                    return answer;
                }
            }

            return null;
        }
    }
}
