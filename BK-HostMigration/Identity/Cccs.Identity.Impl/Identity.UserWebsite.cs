﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public UserWebsite[] UserWebsitesGet(long user_id)
		{
			UserWebsite[] user_websites = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				user_websites =
				(
					from entity in entities.UserWebsites
					where (entity.IsActive && (entity.UserId == user_id))
					orderby (entity.LastCompletedPage == null), ((entity.PercentComplete == 100) && (entity.CompletedDate != null)), entity.ModifiedDate descending
					select new UserWebsite
					{
						UserId = user_id,
						WebsiteCode = entity.WebsiteCode,
						AccountId = entity.AccountId,
						LastCompletedPage = entity.LastCompletedPage,
						PercentComplete = entity.PercentComplete,
						CompletedDate = entity.CompletedDate,
					}
				).ToArray();  
			}

			return user_websites;
		}

		public UserWebsite UserWebsiteGet(long user_id, string website_code)
		{
			UserWebsite user_website = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				user_website =
				(
					from entity in entities.UserWebsites
					where (entity.IsActive && (entity.UserId == user_id) && (string.Compare(entity.WebsiteCode.Trim(), website_code.Trim(), true) == 0))
					select new UserWebsite
					{
						UserId = user_id,
						WebsiteCode = website_code,
						AccountId = entity.AccountId,
						LastCompletedPage = entity.LastCompletedPage,
						PercentComplete = entity.PercentComplete,
						CompletedDate = entity.CompletedDate,
					}
				).FirstOrDefault();
			}

			return user_website;
		}

		public UserWebsiteSaveResult UserWebsiteSave(UserWebsite user_website, string audit_username)
		{
			UserWebsiteSaveResult result = new UserWebsiteSaveResult();

			try
			{
				using (IdentityDataContext context = new IdentityDataContext())
				{
					UserWebsiteSave(user_website, audit_username, context, result);
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save." };
			}

			return result;
		}


		public void UserWebsiteSave(UserWebsite user_website, string audit_username, IdentityDataContext context, UserWebsiteSaveResult result)
		{
			try
			{
				// Valadation
				//
				bool is_valid = true;

				user_website.PercentComplete = Math.Min((byte)100, user_website.PercentComplete);

				if ((user_website.PercentComplete < 100) && (user_website.CompletedDate != null))
				{
					result.IsIncompleteWithCompletedDate = true;
					is_valid = false;
				}
				else
				{
					result.IsIncompleteWithCompletedDate = false;
				}

				if ((user_website.PercentComplete == 100) && (user_website.CompletedDate == null))
				{
					result.IsCompleteWithoutCompletedDate = true;
					is_valid = false;
				}
				else
				{
					result.IsCompleteWithoutCompletedDate = false;
				}

				// Update Database
				//
				if (is_valid)
				{
					Cccs.Identity.Dal.UserWebsite dal_user_website =
					(
						from entity in context.UserWebsites
						where ((entity.UserId == user_website.UserId) && (string.Compare(entity.WebsiteCode.Trim(), user_website.WebsiteCode.Trim()) == 0))
						select entity
					).FirstOrDefault();	

					if (dal_user_website == null) // Insert
					{
						DateTime now = DateTime.Now;

						dal_user_website = new Cccs.Identity.Dal.UserWebsite
						{
							UserId = user_website.UserId,
							WebsiteCode = user_website.WebsiteCode.Trim(),
							AccountId = user_website.AccountId,
							PercentComplete = user_website.PercentComplete,
							LastCompletedPage = user_website.LastCompletedPage,
							CompletedDate = user_website.CompletedDate,
							CreatedDate = now,
							ModifiedDate = now,
							ModifiedBy = audit_username,
							IsActive = true,
						};

						context.UserWebsites.InsertOnSubmit(dal_user_website);
						context.SubmitChanges();

						result.IsSuccessful = true;
					}
					else // Update
					{
						if (user_website.PercentComplete > dal_user_website.PercentComplete) // Avoid progress rollback
						{
							dal_user_website.PercentComplete = user_website.PercentComplete;
							dal_user_website.LastCompletedPage = user_website.LastCompletedPage;
							dal_user_website.CompletedDate = user_website.CompletedDate;
						}
						dal_user_website.AccountId = user_website.AccountId;
						dal_user_website.ModifiedBy = audit_username;
						dal_user_website.ModifiedDate = DateTime.Now;

						context.SubmitChanges();

						result.IsSuccessful = true;
					}
				}
			}
			catch (Exception exception)
			{
				result.IsSuccessful = false;
				result.Exception = exception;
				result.Messages = new string[] { "An error occured attempting to save." };
			}
		}
	}
}
