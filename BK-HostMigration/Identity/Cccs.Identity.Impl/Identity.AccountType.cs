﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public AccountType[] AccountTypesGet()
		{
			AccountType[] account_types = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				account_types =
				(
					from entity in entities.AccountTypes
					select new AccountType
					{
						AccountTypeCode = entity.AccountTypeCode,
						Name = entity.Name,
						Description = entity.Description,
					}
				).ToArray();
			}

			return account_types;
		}

		public AccountType AccountTypeGet(string account_type_code)
		{
			AccountType account_type = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				account_type =
				(
					from entity in entities.AccountTypes
					where (string.Compare(entity.AccountTypeCode.Trim(), account_type_code.Trim(), true) == 0)
					select new AccountType
					{
						AccountTypeCode = entity.AccountTypeCode,
						Name = entity.Name,
						Description = entity.Description,
					}
				).FirstOrDefault();
			}

			return account_type;
		}
	}
}
