﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Data;
using System.Data.Linq;

namespace Cccs.Identity.Impl
{
    public partial class Identity : IIdentity
    {
        public UserDetail[] UserDetailsGet(long user_id)
        {
            UserDetail[] user_details = null;

            using (IdentityDataContext entities = new IdentityDataContext())
            {
                entities.OpenEncryptionKey();

                var user_details_tmp = from entity in entities.UserDetails
                                       where (entity.IsActive && entity.User.IsActive && (entity.User.UserId == user_id))
                                       select new
                                       {
                                           UserDetail = new UserDetail
                                           {
                                               UserDetailId = entity.UserDetailId,
                                               UserId = user_id,
                                               IsPrimary = entity.IsPrimary,
                                               FirstName = entity.FirstName,
                                               MiddleName = entity.MiddleName,
                                               LastName = entity.LastName,
                                               Ssn = entities.DecryptText(entity.Ssn),
                                               BirthDate = entity.BirthDate,
                                               IsMale = entity.IsMale,
                                               Email = entity.Email,
                                               PhoneCell = entity.PhoneCell,
                                               PhoneHome = entity.PhoneHome,
                                               PhoneWork = entity.PhoneWork,
                                               IsHispanic = entity.IsHispanic,
                                           },
                                           RaceCode = entity.RaceCode,
                                           RaceText = (entity.Race == null) ? null : entity.Race.Text,
                                           MaritalStatusCode = entity.MaritalStatusCode,
                                           MaritalStatusText = (entity.MaritalStatus == null) ? null : entity.MaritalStatus.Text,
                                           LanguageCode = entity.User.LanguageCode
                                       };

                if (user_details_tmp != null)
                {
                    user_details = (from u in user_details_tmp select u.UserDetail).OrderByDescending(x => x.IsPrimary).ToArray();

                    int i = 0;
                    foreach (var user_detail_tmp in user_details_tmp)
                    {
                        if (user_detail_tmp.RaceCode != null) // Translate Race.Text
                        {
                            user_details[i].Race = new Race
                            {
                                RaceCode = user_detail_tmp.RaceCode.Value,
                                Text = m_translation.LanguageTextGet(user_detail_tmp.LanguageCode, user_detail_tmp.RaceText)
                            };
                        }

                        if (user_detail_tmp.MaritalStatusCode != null) // Translate MaritalStatus.Text
                        {
                            user_details[i].MaritalStatus = new MaritalStatus
                            {
                                MaritalStatusCode = user_detail_tmp.MaritalStatusCode.Value,
                                Text = m_translation.LanguageTextGet(user_detail_tmp.LanguageCode, user_detail_tmp.MaritalStatusText)
                            };
                        }
                        i++;
                    }
                }
            }

            return user_details;
        }

        public UserDetail UserDetailGet(long user_id, bool is_primary)
        {
            UserDetail user_detail = null;

            using (IdentityDataContext entities = new IdentityDataContext())
            {
                entities.OpenEncryptionKey();

                var user_detail_tmp =
                (
                    from entity in entities.UserDetails
                    where ((entity.IsActive) && (entity.User.UserId == user_id) && (entity.IsPrimary == is_primary))
                    select new
                    {
                        UserDetail = new UserDetail
                        {
                            UserDetailId = entity.UserDetailId,
                            UserId = user_id,
                            IsPrimary = entity.IsPrimary,
                            FirstName = entity.FirstName,
                            MiddleName = entity.MiddleName,
                            LastName = entity.LastName,
                            Ssn = entities.DecryptText(entity.Ssn),
                            BirthDate = entity.BirthDate,
                            IsMale = entity.IsMale,
                            Email = entity.Email,
                            PhoneCell = entity.PhoneCell,
                            PhoneHome = entity.PhoneHome,
                            PhoneWork = entity.PhoneWork,
                            IsHispanic = entity.IsHispanic
                        },
                        TestEmail = entity.Email,
                        RaceCode = entity.RaceCode,
                        RaceText = (entity.Race == null) ? null : entity.Race.Text,
                        MaritalStatusCode = entity.MaritalStatusCode,
                        MaritalStatusText = (entity.MaritalStatus == null) ? null : entity.MaritalStatus.Text,
                        LanguageCode = entity.User.LanguageCode
                    }
                ).FirstOrDefault();

                if (user_detail_tmp != null)
                {
                   Address[] address = AddressesGet(user_id);

                   if ((address != null) && (address.Length != 0))
                   {
                      user_detail_tmp.UserDetail.Address1 = address[0].StreetLine1;
                      user_detail_tmp.UserDetail.Address2 = address[0].StreetLine2;
                      user_detail_tmp.UserDetail.City = address[0].City;
                      user_detail_tmp.UserDetail.State = address[0].State;
                      user_detail_tmp.UserDetail.Zipcode = address[0].Zip;
                   }

                   if (user_detail_tmp.RaceCode != null) // Translate Race.Text
                    {
                        user_detail_tmp.UserDetail.Race = new Race
                        {
                            RaceCode = user_detail_tmp.RaceCode.Value,
                            Text = m_translation.LanguageTextGet(user_detail_tmp.LanguageCode, user_detail_tmp.RaceText)
                        };
                    }

                    if (user_detail_tmp.MaritalStatusCode != null) // Translate MaritalStatus.Text
                    {
                        user_detail_tmp.UserDetail.MaritalStatus = new MaritalStatus
                        {
                            MaritalStatusCode = user_detail_tmp.MaritalStatusCode.Value,
                            Text = m_translation.LanguageTextGet(user_detail_tmp.LanguageCode, user_detail_tmp.MaritalStatusText)
                        };
                    }

                    user_detail = user_detail_tmp.UserDetail;
                }
            }

            return user_detail;
        }

        public UserDetailSaveResult UserDetailSave(UserDetail user_detail, string audit_username)
        {
            UserDetailSaveResult result = new UserDetailSaveResult();

            try
            {
                using (IdentityDataContext context = new IdentityDataContext())
                {
                    UserDetailSave(user_detail, audit_username, context, result);
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
                result.Messages = new string[] { "An error occured attempting to save." };
            }

            return result;
        }

        private void UserDetailSave(UserDetail user_detail, string audit_username, IdentityDataContext context, UserDetailSaveResult result)
        {
            try
            {
                result.IsInvalidSsn = IsInvalidSsn(user_detail.Ssn);

                result.IsFirstNameMissing = ((user_detail.FirstName == null) || (string.IsNullOrEmpty(user_detail.FirstName.Trim())));
                result.IsLastNameMissing = ((user_detail.LastName == null) || (string.IsNullOrEmpty(user_detail.LastName.Trim())));

                if ((result.IsInvalidSsn == false) && (result.IsFirstNameMissing == false) && (result.IsLastNameMissing == false) /*&& (result.IsDuplicateEmail == false) && (result.IsDuplicateSsn == false)*/ )
                {
                    if (user_detail.UserDetailId == 0) // Insert
                    {
                        DateTime now = DateTime.Now;

                        Cccs.Identity.Dal.UserDetail dal_user_detail = new Cccs.Identity.Dal.UserDetail
                        {
                            UserId = user_detail.UserId,
                            IsPrimary = user_detail.IsPrimary,
                            FirstName = user_detail.FirstName,
                            MiddleName = user_detail.MiddleName,
                            LastName = user_detail.LastName,
                            Ssn = context.EncryptText(user_detail.Ssn),
                            BirthDate = user_detail.BirthDate,
                            IsMale = user_detail.IsMale,
                            RaceCode = (user_detail.Race != null) ? (char?)user_detail.Race.RaceCode : null,
                            IsHispanic = user_detail.IsHispanic,
                            MaritalStatusCode = (user_detail.MaritalStatus != null) ? (char?)user_detail.MaritalStatus.MaritalStatusCode : null,
                            Email = user_detail.Email,
                            PhoneCell = user_detail.PhoneCell,
                            PhoneHome = user_detail.PhoneHome,
                            PhoneWork = user_detail.PhoneWork,
                            CreatedDate = now,
                            ModifiedDate = now,
                            ModifiedBy = audit_username,
                            IsActive = true,
                        };

                        context.UserDetails.InsertOnSubmit(dal_user_detail);
                        context.SubmitChanges();

                        result.UserDetailId = user_detail.UserDetailId = dal_user_detail.UserDetailId;
                        result.IsSuccessful = true;
                    }
                    else // Update
                    {
                        Cccs.Identity.Dal.UserDetail dal_user_detail =
                        (
                            from entity in context.UserDetails
                            where (entity.UserDetailId == user_detail.UserDetailId)
                            select entity
                        ).FirstOrDefault();

                        if (dal_user_detail != null)
                        {
                            dal_user_detail.IsPrimary = user_detail.IsPrimary;
                            dal_user_detail.FirstName = user_detail.FirstName;
                            dal_user_detail.MiddleName = user_detail.MiddleName;
                            dal_user_detail.LastName = user_detail.LastName;
                            dal_user_detail.Ssn = context.EncryptText(user_detail.Ssn);
                            dal_user_detail.BirthDate = user_detail.BirthDate;
                            dal_user_detail.IsMale = user_detail.IsMale;
                            dal_user_detail.RaceCode = (user_detail.Race != null) ? (char?)user_detail.Race.RaceCode : null;
                            dal_user_detail.IsHispanic = user_detail.IsHispanic;
                            dal_user_detail.MaritalStatusCode = (user_detail.MaritalStatus != null) ? (char?)user_detail.MaritalStatus.MaritalStatusCode : null;
                            dal_user_detail.Email = user_detail.Email;
                            dal_user_detail.PhoneCell = user_detail.PhoneCell;
                            dal_user_detail.PhoneHome = user_detail.PhoneHome;
                            dal_user_detail.PhoneWork = user_detail.PhoneWork;
                            dal_user_detail.ModifiedBy = audit_username;
                            dal_user_detail.ModifiedDate = DateTime.Now;

                            context.SubmitChanges();

                            result.UserDetailId = dal_user_detail.UserDetailId;
                            result.IsSuccessful = true;
                        }
                        else
                        {
                            result.Messages = new string[] { "UserDetail not found." };
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
                result.Messages = new string[] { "An error occured attempting to save." };
            }
        }

        public Result UserDetailDeleteSecondary(long user_id)
        {
            Result result = new Result();

            try
            {
                using (IdentityDataContext context = new IdentityDataContext())
                {
                    Cccs.Identity.Dal.UserDetail user_detail =
                    (
                        from entity in context.UserDetails
                        where ((entity.User.UserId == user_id) && (entity.IsPrimary == false))
                        select entity
                    ).FirstOrDefault();

                    if (user_detail != null)
                    {
                        context.UserDetails.DeleteOnSubmit(user_detail);

                        context.SubmitChanges();

                        result.IsSuccessful = true;
                    }
                }
            }
            catch (Exception exception)
            {
                result.IsSuccessful = false;
                result.Exception = exception;
            }

            return result;
        }

        private static bool IsInvalidSsn(string ssn)
        {
            if (String.IsNullOrEmpty(ssn)) return false;

            return (ssn.Length != 11) ||
                (
                    !char.IsDigit(ssn[0]) || !char.IsDigit(ssn[1]) || !char.IsDigit(ssn[2]) || (ssn[3] != '-') ||
                    !char.IsDigit(ssn[4]) || !char.IsDigit(ssn[5]) || (ssn[6] != '-') ||
                    !char.IsDigit(ssn[7]) || !char.IsDigit(ssn[8]) || !char.IsDigit(ssn[9]) || !char.IsDigit(ssn[10])
                );
        }
    }
}
