﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		public Website[] WebsitesGet()
		{
			Website[] websites = null;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				websites =
				(
					from entity in entities.Websites
					select new Website
					{
						WebsiteCode = entity.WebsiteCode,
						Name = entity.Name,
						Url = entity.Url
					}
				).ToArray();
			}

			return websites;
		}
	}
}
