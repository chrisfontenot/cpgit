﻿using Cccs.Identity.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Identity.Impl
{
    public partial class Identity : IIdentity
    {
        public CounselorResult GetCounselorByCode(string counselorCode)
        {
            CounselorResult result = new CounselorResult();

            using (IdentityDataContext idc = new IdentityDataContext())
            {
                try
                {
                    var counselor = idc.Counselors.Where(c => string.Compare(c.Code, counselorCode, true) == 0).FirstOrDefault();

                    if (counselor != null)
                    {
                        result.CounselorCode = counselor.Code;
                        result.CounselorEmail = counselor.Email;
                        result.CounselorID = counselor.ID;
                        result.CounselorName = counselor.Name;
                        result.IsSuccessful = true;
                    }
                }
                catch (Exception ex)
                {
                    result.Exception = ex;
                    result.IsSuccessful = false;
                }
            }

            return result;
        }

        public CounselorResult GetCounselorByID(int counselorID)
        {
            CounselorResult result = new CounselorResult();

            using (IdentityDataContext idc = new IdentityDataContext())
            {
                try
                {
                    var counselor = idc.Counselors.Where(c => c.ID == counselorID).FirstOrDefault();

                    if (counselor != null)
                    {
                        result.CounselorCode = counselor.Code;
                        result.CounselorEmail = counselor.Email;
                        result.CounselorID = counselor.ID;
                        result.CounselorName = counselor.Name;
                        result.IsSuccessful = true;
                    }
                }
                catch (Exception ex)
                {
                    result.Exception = ex;
                    result.IsSuccessful = false;
                }
            }

            return result;
        }
    }
}
