﻿using Cccs.Identity.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Identity.Impl
{
    public partial class Identity : IIdentity
    {
        public string GetEducationCertificateNumber(long accountID)
        {
            var certificateNumber = string.Empty;

            using (IdentityDataContext idc = new IdentityDataContext())
            {
                var data = idc.PreDischargeCertificates.Where(p => p.AccountId == accountID).FirstOrDefault();

                if (data != null)
                {
                    certificateNumber = data.CertificateNumber;
                }
            }

            return certificateNumber;
        }
    }
}
