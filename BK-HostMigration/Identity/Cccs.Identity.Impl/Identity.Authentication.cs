﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using System.Security.Cryptography;

namespace Cccs.Identity.Impl
{
	public partial class Identity : IIdentity
	{
		private const string PASSWORD_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		private const string PASSWORD_CHARS_RANDOM_LETTERS = "abcdefghijkmnopqrstuvwxyz";	// Remove	l1O0
		private const string PASSWORD_CHARS_RANDOM_DIGITS = "23456789";	// Remove	l1O0
		private const string PASSWORD_CHARS_RANDOM = PASSWORD_CHARS_RANDOM_LETTERS + PASSWORD_CHARS_RANDOM_DIGITS;	

		public UserAuthenticateResult UserAuthenticate(string username, string password, string ip_address)
		{
			UserAuthenticateResult result = new UserAuthenticateResult();

			try
			{
				using (IdentityDataContext entities = new IdentityDataContext())
				{
					Cccs.Identity.Dal.User user =
					(
						from entity in entities.Users
						where (entity.IsActive && (string.Compare(entity.Username.Trim(), username.Trim(), true) == 0))
						select entity
					).FirstOrDefault();

					result.IsInvalidUsername = (user == null);

					if (user != null)
					{
						result.UserId = user.UserId;
						result.IsUserLocked = IsLocked(user.LockedDate);

						if (result.IsUserLocked == false) // Handle un/non-locked users
						{
							string password_hash = string.IsNullOrEmpty(user.PasswordSalt) ? password.Trim() : Security.HashPassword(password.Trim(), user.PasswordSalt); // No Salt means clear-text password

							result.IsInvalidPassword = (user.Password != password_hash);

							if (result.IsInvalidPassword == false) // Check password
							{
								result.LastLoginDate = user.LastLoginDate;

								user.LoginFailures = 0;
								user.LastLoginFailureDate = null;
								user.LockedDate = null;
								user.LastLoginDate = DateTime.Now;
								user.LastLoginIpAddress = ip_address;
								user.SessionGuid = Guid.NewGuid();

								result.SsoToken = CreateSsoToken(user.UserId, user.SessionGuid.Value, user.LastLoginDate.Value);
								result.IsSuccessful = true;
							}
							else // Bad Password
							{
								AuthenticationFailure(user);
								result.IsUserLocked = (user.LockedDate != null);
							}

							
							user.ModifiedBy = username;
							user.ModifiedDate = DateTime.Now;

							entities.SubmitChanges();
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public bool UserSsoTokenValidate(long user_id, string sso_token)
		{
			bool result = false;

			using (IdentityDataContext entities = new IdentityDataContext())
			{
				Cccs.Identity.Dal.User user =
				(
					from entity in entities.Users
					where (entity.IsActive && (entity.UserId == user_id))
					select entity
				).FirstOrDefault();

				if ((user != null) && (user.SessionGuid != null) && (user.LastLoginDate != null) && (user.LastLoginDate.Value.AddDays(1) > DateTime.Now))
				{
					result = (sso_token == CreateSsoToken(user_id, user.SessionGuid.Value, user.LastLoginDate.Value));
				}
			}

			return result;
		}

		public void UserLogout(long user_id)
		{
			using (IdentityDataContext entities = new IdentityDataContext())
			{
				Cccs.Identity.Dal.User user =
				(
					from entity in entities.Users
					where (entity.IsActive && (entity.UserId == user_id))
					select entity
				).FirstOrDefault();

				if (user != null)
				{
					user.SessionGuid = null;
					user.ModifiedBy = user.Username;
					user.ModifiedDate = DateTime.Now;

					entities.SubmitChanges();
				}
			}
		}

		public UserSecurityQuestionChallengeResult UserSecurityQuestionChallenge(string username, string security_question_answer)
		{
			UserSecurityQuestionChallengeResult result = new UserSecurityQuestionChallengeResult();

			try
			{
				using (IdentityDataContext entities = new IdentityDataContext())
				{
					entities.OpenEncryptionKey();

					Cccs.Identity.Dal.User user =
					(
						from entity in entities.Users
						where ((entity.IsActive) && (string.Compare(entity.Username.Trim(), username.Trim(), true) == 0))
						select entity
					).FirstOrDefault();

					result.IsInvalidUsername = (user == null);

					if (user != null)
					{
						result.UserId = user.UserId;
						result.IsUserLocked = IsLocked(user.LockedDate);
						result.AreUserAnswersMissing = (user.SecurityAnswer == null);

						if ((result.IsUserLocked == false) && (result.AreUserAnswersMissing == false))
						{
							entities.OpenEncryptionKey();
							result.IsIncorrectAnswer = (string.Compare(entities.DecryptText(user.SecurityAnswer).Trim(), security_question_answer.Trim(), true) != 0);

							result.IsSuccessful = (result.IsIncorrectAnswer == false);

							if (result.IsSuccessful == false)	
							{
								AuthenticationFailure(user);
								
								user.ModifiedBy = username;
								user.ModifiedDate = DateTime.Now;

								entities.SubmitChanges();
							}
							
							result.IsUserLocked = (user.LockedDate != null);
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserPasswordChangeResult UserPasswordChange(long user_id, string old_password, string new_password, string new_password_confirm)
		{
			UserPasswordChangeResult result = new UserPasswordChangeResult();

			try
			{
				using (IdentityDataContext entities = new IdentityDataContext())
				{
					Cccs.Identity.Dal.User user =
					(
						from entity in entities.Users
						where (entity.IsActive && (entity.UserId == user_id))
						select entity
					).FirstOrDefault();

					result.IsInvalidUserId = (user == null);

					if (user != null)
					{
						if (DoPasswordChange(result, entities, user, old_password, new_password, new_password_confirm, user.Username))
						{
							entities.SubmitChanges();

							result.IsSuccessful = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserPasswordChangeResult UserPasswordReset(long user_id, string new_password, string new_password_confirm, string audit_username)
		{
			UserPasswordChangeResult result = new UserPasswordChangeResult();

			try
			{
				using (IdentityDataContext entities = new IdentityDataContext())
				{
					Cccs.Identity.Dal.User user =
					(
						from entity in entities.Users
						where (entity.IsActive && (entity.UserId == user_id))
						select entity
					).FirstOrDefault();

					result.IsInvalidUserId = (user == null);

					if (user != null)
					{
						if (DoPasswordChange(result, entities, user, null, new_password, new_password_confirm, audit_username))
						{
							entities.SubmitChanges();

							result.IsSuccessful = true;
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UsernameRecoverResult UsernameRecover(string email, string security_question_answer)
		{
			UsernameRecoverResult result = new UsernameRecoverResult();

			try
			{
				using (IdentityDataContext context = new IdentityDataContext())
				{
					long[] user_ids = UserIdsGetByEmail(email, context);

					if ((user_ids == null) || (user_ids.Length > 1))
					{
						result.IsInvalidEmail = true;
					}
					else
					{
						context.OpenEncryptionKey();

						var user_detail =
						(
							from entity in context.UserDetails
							where ((entity.IsActive) && (entity.User.IsActive) && (entity.UserId == user_ids[0]))
							select new
							{
								entity.UserId,
								entity.User.Username,
								SecurityAnswer = context.DecryptText(entity.User.SecurityAnswer),
								entity.User.LockedDate,
							}
						).FirstOrDefault();

						result.IsInvalidEmail = (user_detail == null);

						if (user_detail != null)
						{
							result.UserId = user_detail.UserId;
							result.IsUserLocked = IsLocked(user_detail.LockedDate);
							result.AreUserAnswersMissing = (user_detail.SecurityAnswer == null);

							if ((result.AreUserAnswersMissing == false) && (result.IsUserLocked == false))
							{
								result.IsIncorrectAnswer = (string.Compare(user_detail.SecurityAnswer.Trim(), security_question_answer.Trim(), true) != 0);
								result.IsSuccessful = (result.IsIncorrectAnswer == false);

								if (result.IsSuccessful)
								{
									result.Username = user_detail.Username; // Did not want to provide unless successful, since IsSuccessful may not be honored...
								}
								else // Bad Answers
								{
									Cccs.Identity.Dal.User user =
									(
										from entity in context.Users
										where (entity.IsActive && (entity.UserId == user_detail.UserId))
										select entity
									).FirstOrDefault();

									AuthenticationFailure(user);

									user.ModifiedBy = user.Username;
									user.ModifiedDate = DateTime.Now;

									result.IsUserLocked = (user.LockedDate != null);

									context.SubmitChanges();
								}
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public Result UserUnlock(long user_id, string audit_username)
		{
			Result result = new Result();
			
			try
			{
				using (IdentityDataContext entities = new IdentityDataContext())
				{
					Cccs.Identity.Dal.User user =
					(
						from entity in entities.Users
						where (entity.IsActive && (entity.UserId == user_id))
						select entity
					).FirstOrDefault();

					if (user != null)
					{
						user.LockedDate = null;
						user.LoginFailures = 0;
						user.LastLoginFailureDate = null;
						user.ModifiedBy = audit_username;
						user.ModifiedDate = DateTime.Now;

						entities.SubmitChanges();
						
						result.IsSuccessful = true;
					}
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#region Helper Methods

		public string CreateSsoToken(long user_id, Guid session_guid, DateTime last_login_date)
		{
			string sso_token_str = string.Format("{0}_{1}_{2}", user_id, session_guid.ToString(), last_login_date.ToString("yyMdhms"));

			byte[] hashed_token = new SHA512Managed().ComputeHash(Encoding.UTF8.GetBytes(sso_token_str));

			string hex = BitConverter.ToString(hashed_token).Replace("-", string.Empty);

			if (hex.Length > 64)
			{
				hex = hex.Substring(0, 64);
			}

			return hex;
		}

		private bool DoPasswordChange(UserPasswordChangeResult result, IdentityDataContext entities, Cccs.Identity.Dal.User dal_user, string old_password, string new_password, string new_password_confirm, string audit_username)
		{
			bool is_successful = false;

			result.IsUserLocked = IsLocked(dal_user.LockedDate);

			result.IsInvalidNewPasswordConfirm = string.IsNullOrEmpty(new_password_confirm) || (string.Compare(new_password.Trim(), new_password_confirm.Trim(), true) != 0);

			if ( !string.IsNullOrEmpty(old_password) )
			{
				result.IsInvalidOldPassword = !Security.CheckPassword(old_password, dal_user.Password, dal_user.PasswordSalt);
			}
			
			result.IsUnchangedNewPassword = (Security.HashPassword(new_password, dal_user.PasswordSalt) == dal_user.Password);
			
			result.NewPasswordErrors = NewPasswordValidate(entities, dal_user, new_password);

			if 
			( 
				((string.Compare(dal_user.Username.Trim(), audit_username.Trim(), true) != 0) || (result.IsUserLocked == false)) &&	 // Locked users cannot change own password
				(result.IsInvalidNewPasswordConfirm == false) &&
				(string.IsNullOrEmpty(old_password) || (result.IsInvalidOldPassword != true)) && 
				(result.IsUnchangedNewPassword == false) && 
				(result.NewPasswordErrors == null)
			)
			{
				dal_user.PasswordSalt = Security.GenerateSalt();
				dal_user.Password = Security.HashPassword(new_password, dal_user.PasswordSalt);
				dal_user.PasswordResetDate = DateTime.Now.AddDays(UserPasswordResetDays);
				dal_user.LoginFailures = 0;
				dal_user.LastLoginFailureDate = null;
				dal_user.LockedDate = null;
				dal_user.ModifiedDate = DateTime.Now;
				dal_user.ModifiedBy = audit_username;

				is_successful = true;
			}

			return is_successful;
		}

		private NewPasswordErrors NewPasswordValidate(IdentityDataContext entities, Cccs.Identity.Dal.User user, string new_password)
		{
			NewPasswordErrors errors = null;

			new_password = new_password.ToUpper(); // Convert to upper-case for substring searches

			// Check Minumum Length
			//
			if ( new_password.Length < UserPasswordLengthMin	)
			{
				errors = errors ?? new NewPasswordErrors();
				errors.IsTooShort = true;
			}

			// Check Maximum Length
			//
			if ( new_password.Length > UserPasswordLengthMax	)
			{
				errors = errors ?? new NewPasswordErrors();
				errors.IsTooLong = true;
			}

			// Check if contains username
			//
			if (new_password.Contains(user.Username.ToUpper()))
			{
				errors = errors ?? new NewPasswordErrors();
				errors.ContainsUsername = true;
			}

			// Check if contains the word "password"
			//

			if (new_password.Contains("PASSWORD"))
			{
				errors = errors ?? new NewPasswordErrors();
				errors.ContainsPassword = true;
			}

			// Check if contains first or last name
			//
			foreach ( Cccs.Identity.Dal.UserDetail user_detail in user.UserDetails )
			{
				string first_name = user_detail.FirstName;
				if (new_password.Contains(first_name.ToUpper()))
				{
					errors = errors ?? new NewPasswordErrors();
					errors.ContainsFirstName = true;
				}

				string last_name = user_detail.LastName;
				if (!String.IsNullOrEmpty(last_name) && new_password.Contains(last_name.ToUpper()))
				{
					errors = errors ?? new NewPasswordErrors();
					errors.ContainsLastName = true;
				}
			}

			// Check for repeated characters
			//
			char last_char = char.MinValue;
			int repeated = 0;
			bool contains_letter = false;
			bool contains_digit = false;

			foreach (char c in new_password)
			{
				if (c == last_char)
				{
					repeated++;

					if (repeated > 3) // TODO: 3 or configurable?
					{
						errors = errors ?? new NewPasswordErrors();
						errors.ContainsSequence = true;
					}
				}
				else
				{
					last_char = c;
					repeated = 0;
				}

				if (char.IsLetter(c))
				{
					contains_letter = true;
				}
				else if (char.IsDigit(c))
				{
					contains_digit = true;
				}
				else // non-AlphaNumeric character
				{
					errors = errors ?? new NewPasswordErrors();
					errors.ContainsInvalidChar = true;
				}
			}

			// Check for at least one letter
			//
			if (!contains_letter)
			{
				errors = errors ?? new NewPasswordErrors();
				errors.ContainsNoLetter = true;
			}

			// Check for at least one digit
			//
			if (!contains_digit)
			{
				errors = errors ?? new NewPasswordErrors();
				errors.ContainsNoNumber = true;
			}

			return errors;
		}

		public string UserPasswordGenerate()
		{
			int password_length = UserPasswordLengthMin;
			StringBuilder password = new StringBuilder(password_length);
			Random random = new Random();

			password.Append(PASSWORD_CHARS_RANDOM_LETTERS[random.Next(0, PASSWORD_CHARS_RANDOM_LETTERS.Length - 1)]);	// Make sure there is one letter
			for (int i = 0; i < password_length - 2; i++)
			{
				password.Append(PASSWORD_CHARS_RANDOM[random.Next(0, PASSWORD_CHARS_RANDOM.Length - 1)]);
			}
			password.Append(PASSWORD_CHARS_RANDOM_DIGITS[random.Next(0, PASSWORD_CHARS_RANDOM_DIGITS.Length - 1)]);	// Make sure there is one digit

			return password.ToString();
		}

		private bool IsLocked(DateTime? locked_date)
		{
			return ((locked_date != null) && (DateTime.Now.Subtract(locked_date.Value).TotalMinutes < UserLockoutMinutes));
		}

		private void AuthenticationFailure(Cccs.Identity.Dal.User user)
		{
			if ((user.LastLoginFailureDate != null) && (DateTime.Now.Subtract(user.LastLoginFailureDate.Value).TotalMinutes > UserLoginFailureMinutes))
			{
				user.LoginFailures = 0;
				user.LockedDate = null;
				user.LastLoginFailureDate = DateTime.Now;
			}
			else if (user.LoginFailures == 0)
			{
				user.LastLoginFailureDate = DateTime.Now;
			}

			user.LoginFailures++;
			user.SessionGuid = null;

			if (user.LoginFailures >= UserLoginFailureLimit) // Check whether or not to lock user due to excessive failed logins
			{
				user.LockedDate = DateTime.Now;
			}
		}


		#endregion
	}
}
