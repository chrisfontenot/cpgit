﻿using System;
using System.Collections.Generic;
using Cccs.Identity.Dto;
using Cccs.Identity.Dto.PreFiling;

namespace Cccs.Identity.Service.Client
{
	public class Identity : IIdentity
	{
		#region Authentication

		public UserAuthenticateResult UserAuthenticate(string username, string password, string ip_address)
		{
			UserAuthenticateResult result = new UserAuthenticateResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserAuthenticate(username, password, ip_address);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public bool UserSsoTokenValidate(long user_id, string sso_token)
		{
			bool result = false;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserSsoTokenValidate(user_id, sso_token);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public void UserLogout(long user_id)
		{
			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					client.UserLogout(user_id);
				}
			}
			catch 
			{
				throw;
			}
		}

		public UsernameRecoverResult UsernameRecover(string email, string security_question_answer)
		{
			UsernameRecoverResult result = new UsernameRecoverResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UsernameRecover(email, security_question_answer);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserSecurityQuestionChallengeResult UserSecurityQuestionChallenge(string username, string security_question_answer)
		{
			UserSecurityQuestionChallengeResult result = new UserSecurityQuestionChallengeResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserSecurityQuestionChallenge(username, security_question_answer);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserPasswordChangeResult UserPasswordChange(long user_id, string old_password, string new_password, string new_password_confirm)
		{
			UserPasswordChangeResult result = new UserPasswordChangeResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserPasswordChange(user_id, old_password, new_password, new_password_confirm);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserPasswordChangeResult UserPasswordReset(long user_id, string new_password, string new_password_confirm, string audit_username)
		{
			UserPasswordChangeResult result = new UserPasswordChangeResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserPasswordReset(user_id, new_password, new_password_confirm, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public string UserPasswordGenerate()
		{
			string result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserPasswordGenerate();
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public Result UserUnlock(long user_id, string audit_username)
		{
			Result result = new Result();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserUnlock(user_id, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#endregion

		#region	SecurityQuestion

		public SecurityQuestion[] SecurityQuestionsGet(string language_code)
		{
			SecurityQuestion[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.SecurityQuestionsGet(language_code);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public SecurityQuestion SecurityQuestionGetByUsername(string username)
		{
			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					var result = client.SecurityQuestionGetByUsername(username);
                    return result;
				}
			}
			catch 
			{
				throw;
			}
		}

		public SecurityQuestion SecurityQuestionGetByEmail(string email)
		{
			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					var result = client.SecurityQuestionGetByEmail(email);
                    return result;
				}
			}
			catch 
			{
				throw;
			}
		}

		#endregion

		#region User

		public long? UserIdGetByUsername(string username)
		{
			long? result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserIdGetByUsername(username);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public long[] UserIdsGetByEmail(string email)
		{
			long[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserIdsGetByEmail(email);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public long? UserIdGetBySsn(string ssn)
		{
			long? result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserIdGetBySsn(ssn);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public long? UserIdGetByHostId(string host_id)
		{
			long? result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserIdGetByHostId(host_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public User UserGet(long user_id)
		{
			User result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserGet(user_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public UserSaveResult UserSave(User user, string audit_username)
		{
			UserSaveResult result = new UserSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserSave(user, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserRegisterResult UserRegister(UserRegistration user_registration)
		{
			UserRegisterResult result = new UserRegisterResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserRegister(user_registration);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public UserSearchResult UserSearch(UserSearchCriteria user_search_criteria)
		{
			UserSearchResult result = new UserSearchResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserSearch(user_search_criteria);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

        public byte[] DisplayActionPlan(string certificateNumber)
        {
            throw new NotImplementedException();
        }

        public byte[] DisplayActionPlanById(long actionPlanId)
        {
            throw new NotImplementedException();
        }

        public byte[] DisplayDebtAnalysis(long actionPlanId)
        {
            throw new NotImplementedException();
        }

        public byte[] DisplayCertificate(string certificateNumber)
        {
            throw new NotImplementedException();
        }

        public byte[] DisplayCombinedCertificate(string certificateNumber)
        {
            throw new NotImplementedException();
        }

        public List<Certificate> GetCertificates(long userId)
        {
            throw new NotImplementedException();
        }

        public DocumentList GetClientDocuments(long userId)
        {
            throw new NotImplementedException();
        }

        public Snapshot GetSnapshot(long actionPlanId)
        {
            throw new NotImplementedException();
        }

        public Snapshot GetCurrentSnapshot(long userId)
        {
            throw new NotImplementedException();
        }

		public Snapshot GetCurrentSnapshotForUserDetail(long UserDetailId)
		{
			throw new NotImplementedException();
		}
		
		public Snapshot GetCertificateSnapshot(string certificateNumber)
        {
            throw new NotImplementedException();
        }

        public void ReIssueCertificate(Snapshot currentSnapshot, string attorneyEmail)
        {
            throw new NotImplementedException();
        }

        public void ReTryCertificateDownload(long preFilingSnapshotId, string attorneyEmail)
        {
            throw new NotImplementedException();
        }

        public List<ReIssueSearchResult> SearchReIssueSnapshots(ReIssueSearchRequest request)
        {
            throw new NotImplementedException();
        }

		#endregion

		#region UserDetail

		public UserDetail[] UserDetailsGet(long user_id)
		{
			UserDetail[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserDetailsGet(user_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public UserDetail UserDetailGet(long user_id, bool is_primary)
		{
			UserDetail result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserDetailGet(user_id, is_primary);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public UserDetailSaveResult UserDetailSave(UserDetail user_detail, string audit_username)
		{
			UserDetailSaveResult result = new UserDetailSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserDetailSave(user_detail, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public Result UserDetailDeleteSecondary(long user_id)
		{
			Result result = new Result();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserDetailDeleteSecondary(user_id);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}


		#endregion

		#region UserProfile

		public UserProfile UserProfileGet(long user_id, UserProfileOptions user_profile_options)
		{
			UserProfile result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserProfileGet(user_id, user_profile_options);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public UserProfileSaveResult UserProfileSave(UserProfile user_profile, string audit_username)
		{
			UserProfileSaveResult result = new UserProfileSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserProfileSave(user_profile, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#endregion

		#region Address

		public Address[] AddressesGet(long user_id)
		{
			Address[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AddressesGet(user_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public Address AddressGet(long address_id)
		{
			Address result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AddressGet(address_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public AddressSaveResult AddressSave(Address addr, string audit_username)
		{
			AddressSaveResult result = new AddressSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AddressSave(addr, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#endregion

		#region Account

        public String CreateSsoToken(long lngUserID, System.Guid guid, System.DateTime dteLastLoggedIn)
        {
            throw new NotImplementedException();
        }

		public Account[] AccountsGet(long user_id)
		{
			Account[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountsGet(user_id);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public Account AccountGet(long user_id, string account_type_code)
		{
			Account result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountGet(user_id, account_type_code);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public Account AccountGetById(long account_id)
		{
			Account result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountGetById(account_id);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public AccountSaveResult AccountSave(Account account, string audit_username)
		{
			AccountSaveResult result = new AccountSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountSave(account, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#endregion

		#region AccountType

		public AccountType[] AccountTypesGet()
		{
			AccountType[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountTypesGet();
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public AccountType AccountTypeGet(string account_type_code)
		{
			AccountType result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.AccountTypeGet(account_type_code);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		#endregion

		#region Race

		public Race[] RacesGet(string language_code)
		{
			Race[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.RacesGet(language_code);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public Race RaceGet(char race_code, string language_code)
		{
			Race result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.RaceGet(race_code, language_code);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		#endregion

		#region MaritalStatus

		public MaritalStatus[] MaritalStatusesGet(string language_code)
		{
			MaritalStatus[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.MaritalStatusesGet(language_code);
				}
			}
			catch
			{
				throw;
			}

			return result;
		}

		public MaritalStatus MaritalStatusGet(char marital_status_code, string language_code)
		{
			MaritalStatus result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.MaritalStatusGet(marital_status_code, language_code);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		#endregion

		#region Website

		public Website[] WebsitesGet()
		{
			Website[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.WebsitesGet();
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		#endregion

		#region UserWebsite

		public UserWebsite[] UserWebsitesGet(long user_id)
		{
			UserWebsite[] result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserWebsitesGet(user_id);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public UserWebsite UserWebsiteGet(long user_id, string website_code)
		{
			UserWebsite result = null;

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserWebsiteGet(user_id, website_code);
				}
			}
			catch 
			{
				throw;
			}

			return result;
		}

		public UserWebsiteSaveResult UserWebsiteSave(UserWebsite user_website, string audit_username)
		{
			UserWebsiteSaveResult result = new UserWebsiteSaveResult();

			try
			{
				using (IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					result = client.UserWebsiteSave(user_website, audit_username);
				}
			}
			catch (Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		#endregion

		#region Push
		public UvIdmPush GetIdmDataForUv(int InNo, long UserId)
		{
			UvIdmPush Result = new UvIdmPush();

			try
			{
				using(IdentityService.IdentityClient client = new IdentityService.IdentityClient())
				{
					client.Open();

					Result = client.GetIdmDataForUv(InNo, UserId);
				}
			}
			catch
			{
				throw;
			}
	
			return Result;
		}

        public string GetEducationCertificateNumber(long accountID)
        {
            throw new NotImplementedException();
        }

        public CounselorResult GetCounselorByCode(string counselorCode)
        {
            throw new NotImplementedException();
        }

        public CounselorResult GetCounselorByID(int counselorID)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}