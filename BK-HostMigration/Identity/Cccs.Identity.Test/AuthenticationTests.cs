﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for Authentication
    /// </summary>
    [TestClass]
    public class AuthenticationTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void PasswordsTest()
        {
            string username = Guid.NewGuid().ToString();
            string password = App.Identity.UserPasswordGenerate();

            User user = new User
            {
                Username = username,
                NewPassword = password,
                NewPasswordConfirm = password,
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");


            string new_password = "P2345678";
            UserPasswordChangeResult upcr = App.Identity.UserPasswordChange(result.UserId.Value, password, new_password, new_password);

            password = new_password;
            new_password += "9";
            upcr = App.Identity.UserPasswordChange(result.UserId.Value, password, new_password, new_password);

            password = new_password;
            new_password += "01234567890";
            upcr = App.Identity.UserPasswordChange(result.UserId.Value, password, new_password, new_password);

            password = new_password;
            new_password += "X";
            upcr = App.Identity.UserPasswordChange(result.UserId.Value, password, new_password, new_password);

            password = new_password;
            new_password = "X234567";
            upcr = App.Identity.UserPasswordChange(result.UserId.Value, password, new_password, new_password);



        }


        [TestMethod]
        public void AuthenticationTest4815162342()
        {
            UserAuthenticateResult result = App.Identity.UserAuthenticate("johnlocke", "Test9876", "1.2.3.4");

        }

        [TestMethod]
        public void AuthenticationTestDavid63()
        {
            UserAuthenticateResult result = App.Identity.UserAuthenticate("david63", "test1234", "1.2.3.4");

            string url = string.Format("http://172.16.151.98:8731/counseling/Rvm/RepaymentRules.aspx?u={0}&t={1}", result.UserId.Value, result.SsoToken);
        }

        [TestMethod]
        public void AuthenticationTest()
        {
            string username = Guid.NewGuid().ToString();
            string password = App.Identity.UserPasswordGenerate();

            User user = new User
            {
                Username = username,
                NewPassword = password,
                NewPasswordConfirm = password,
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");

            UserDetail user_detail = new UserDetail
            {
                UserId = result.UserId.Value,
                IsPrimary = true,
                FirstName = "Randall",
                MiddleName = "John",
                LastName = "Miller",
                Ssn = "123-45-" + new Random().Next(1000, 9999).ToString(),
                BirthDate = new DateTime(1974, 8, 1),
                IsMale = true,
                Race = new Race { RaceCode = 'W' },
                MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                Email = Guid.NewGuid().ToString().Substring(0, 4) + "@credability.org",
                PhoneCell = "111-222-3333",
                PhoneHome = "123-456-7890",
                PhoneWork = "999-888-7777",
            };

            UserDetailSaveResult udsr = App.Identity.UserDetailSave(user_detail, "Rastre");


            App.Identity.UsernameRecover(user_detail.Email, user.SecurityQuestionAnswer.SecurityAnswer);


            UserAuthenticateResult uar = App.Identity.UserAuthenticate(username, password, "1.1.1.1");

            string new_password = App.Identity.UserPasswordGenerate();

            UserPasswordChangeResult upcr = App.Identity.UserPasswordChange(user.UserId, password, new_password, new_password);

            upcr = App.Identity.UserPasswordReset(user.UserId, "Abc123", "Abc123", "Rastre");

            bool is_valid = App.Identity.UserSsoTokenValidate(result.UserId.Value, uar.SsoToken);

            App.Identity.UserLogout(user.UserId);

            is_valid = App.Identity.UserSsoTokenValidate(user.UserId, uar.SsoToken);



            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #1
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #2
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #3
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #4
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #5
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #6
            uar = App.Identity.UserAuthenticate(username, "asdasdad", "1.1.1.1"); // Fail #7

        }

        [TestMethod]
        public void UsernameRecoverTest()
        {
            string username = Guid.NewGuid().ToString();
            string password = App.Identity.UserPasswordGenerate();

            User user = new User
            {
                Username = username,
                NewPassword = password,
                NewPasswordConfirm = password,
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");

            UserDetail user_detail = new UserDetail
            {
                UserId = result.UserId.Value,
                IsPrimary = true,
                FirstName = "Randall",
                MiddleName = "John",
                LastName = "Miller",
                Ssn = "123-45-" + new Random().Next(1000, 9999).ToString(),
                BirthDate = new DateTime(1974, 8, 1),
                IsMale = true,
                Race = new Race { RaceCode = 'W' },
                MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                Email = Guid.NewGuid().ToString().Substring(0, 4) + "@credability.org",
                PhoneCell = "111-222-3333",
                PhoneHome = "123-456-7890",
                PhoneWork = "999-888-7777",
            };

            UserDetailSaveResult udsr = App.Identity.UserDetailSave(user_detail, "Rastre");


            UsernameRecoverResult urr = App.Identity.UsernameRecover(user_detail.Email, user.SecurityQuestionAnswer.SecurityAnswer);

            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #1
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #2
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #3
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #4
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #5
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #6
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #7

            App.Identity.UserUnlock(result.UserId.Value, "Rastre");
            urr = App.Identity.UsernameRecover(user_detail.Email, "adasd"); // Fail #3




        }

        [TestMethod]
        public void PasswordGenerate()
        {
            //for (int i = 0; i < 50; i++)
            //{
            //  string password = App.Identity.UserPasswordGenerate();
            //}

            //using (IdentityWebService.Identity identity = new Cccs.Identity.Test.IdentityWebService.Identity())
            //{
            //  string password = identity.PasswordGenerate();
            //}
        }
    }
}
