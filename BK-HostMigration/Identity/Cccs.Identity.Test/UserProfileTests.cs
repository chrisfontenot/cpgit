﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for UserProfileTests
    /// </summary>
    [TestClass]
    public class UserProfileTests
    {
        public UserProfileTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



        [TestMethod]
        public void TestMethod2()
        {
            UserProfile up = App.Identity.UserProfileGet(83715, null);
        }

        [TestMethod]
        public void UserProfileTestMethod1()
        {
            UserProfile user_profile = new UserProfile
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "a1b2c3d4",
                NewPasswordConfirm = "a1b2c3d4",
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },

                Addresses = new Address[] 
				{
					new Address
					{
						AddressType = "Home",
						StreetLine1 = "123 Main St.",
						StreetLine2 = "Suite 1",
						City = "Atlanta",
						State = "GA",
						Zip = "30350"
					}
				},

                UserDetailPrimary = new UserDetail
                {
                    IsPrimary = true,
                    FirstName = "Randall",
                    MiddleName = "John",
                    LastName = "Miller",
                    Ssn = "123-45-" + new Random().Next(1000, 9999),
                    BirthDate = new DateTime(1974, 8, 1),
                    IsMale = true,
                    Race = new Race { RaceCode = 'W' },
                    MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                    Email = Guid.NewGuid().ToString() + "@credability.org",
                    PhoneCell = "111-222-3333",
                    PhoneHome = "123-456-7890",
                    PhoneWork = "999-888-7777",
                },

                UserDetailSecondary = new UserDetail
                {
                    IsPrimary = false,
                    FirstName = "Holly",
                    MiddleName = "Suzannah",
                    LastName = "Miller",
                    Ssn = "987-65-" + new Random().Next(1000, 9999),
                    BirthDate = new DateTime(1976, 8, 18),
                    IsMale = false,
                    Race = new Race { RaceCode = 'W' },
                    MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                    Email = Guid.NewGuid().ToString() + "@credability.org",
                    PhoneCell = "111-222-3333",
                    PhoneHome = "123-456-7890",
                    PhoneWork = "999-888-7777",
                },

                Accounts = new Account[]
				{
					new Account
					{
						AccountTypeCode = "BR.CAM",
						InternetId = 1,
						HostId = "RastreHost",
					},
					new Account
					{
						AccountTypeCode = "WS.CAM",
						InternetId = 1,
						HostId = "RastreHost",
					}
				},

            };

            UserProfileSaveResult result = App.Identity.UserProfileSave(user_profile, "Rastre");

            UserProfile user_profile_get = App.Identity.UserProfileGet(result.UserId.Value, null);







        }
    }
}
