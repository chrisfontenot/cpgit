﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for UserTest
    /// </summary>
    [TestClass]
    public class UserTests
    {
        public UserTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void UserIdGetByHostIdTest1()
        {
            long? user_id = App.Identity.UserIdGetByHostId("B678444");
        }

        [TestMethod]
        public void UserNullExceptionTest()
        {
            var user = new User
            {
                LanguageCode = "EN",
                LastLoginDate = DateTime.Parse("10/7/2010 1:07:14 PM"),
                LastLoginIpAddress = String.Empty,
                LoginFailures = 0,
                NewPassword = "123456789a",
                NewPasswordConfirm = "123456789a",
                PasswordResetDate = DateTime.Parse("6/2/2010 2:30:48 PM"),
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "born"
                },
                UserId = 90009,
                Username = "testdude12",
            };

            var result = App.Identity.UserSave(user, "Rastre");
        }

        [TestMethod]
        public void UserTest1()
        {
            User user = new User
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "Passw0rd",
                NewPasswordConfirm = "Passw0rd",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");

            User user_get = App.Identity.UserGet(result.UserId.Value);

            result = App.Identity.UserSave(user, "Rastre");
        }

        [TestMethod]
        public void UserGetByUserId()
        {
            var user = App.Identity.UserGet(92112);
            Assert.IsNotNull(user);
            Assert.IsNotNull(user.SecurityQuestionAnswer);
        }

        [TestMethod]
        public void UserServiceGetByUserId()
        {
            var identity = new IdentityServiceReference.IdentityClient();
            var user = identity.UserGet(92112);
            
            Assert.IsNotNull(user);
        }
    }
}
