﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for AccountTest
    /// </summary>
    [TestClass]
    public class AccountTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AccountTest1()
        {
            User user = new User
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "Passw0rd",
                NewPasswordConfirm = "Passw0rd",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");

            Account account = new Account
            {
                UserId = result.UserId.Value,
                AccountTypeCode = "BR.CAM",
                InternetId = 1,
                HostId = "RastreHost",
            };

            AccountSaveResult asr = App.Identity.AccountSave(account, "Rastre");

            Account account_get = App.Identity.AccountGet(account.UserId, account.AccountTypeCode);


        }
    }
}
