﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for UserRegisterTest
    /// </summary>
    [TestClass]
    public class UserRegisterTests
    {
        public UserRegisterTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void UserRegisterTest1()
        {
            UserRegistration user_registration = new UserRegistration
            {
                Username = Guid.NewGuid().ToString(),
                Password = "Passw0rd",
                PasswordConfirm = "Passw0rd",
                FirstName = "Randall",
                LastName = "Miller",
                Email = Guid.NewGuid().ToString() + "@r.com",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
            };

            UserRegisterResult result = App.Identity.UserRegister(user_registration);

            User user_get = App.Identity.UserGet(result.UserId.Value);
            UserDetail user_detail_get = App.Identity.UserDetailGet(result.UserId.Value, true);
        }
    }
}
