﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for UserWebsiteTests
    /// </summary>
    [TestClass]
    public class UserWebsiteTests
    {
        public UserWebsiteTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void UserWebsiteTest()
        {
            User user = new User
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "Passw0rd",
                NewPasswordConfirm = "Passw0rd",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult result = App.Identity.UserSave(user, "Rastre");

            Website[] websites = App.Identity.WebsitesGet();

            UserWebsite user_website = new UserWebsite
            {
                UserId = result.UserId.Value,
                WebsiteCode = websites[1].WebsiteCode,
                PercentComplete = 15,
                LastCompletedPage = "Home1.aspx",
            };
            UserWebsiteSaveResult uwsr = App.Identity.UserWebsiteSave(user_website, "Rastre");

            user_website = new UserWebsite
            {
                UserId = result.UserId.Value,
                WebsiteCode = websites[0].WebsiteCode,
                PercentComplete = 100,
                LastCompletedPage = "Home2.aspx",
                CompletedDate = DateTime.Now,
            };
            uwsr = App.Identity.UserWebsiteSave(user_website, "Rastre");

            user_website = new UserWebsite
            {
                UserId = result.UserId.Value,
                PercentComplete = 1,
                WebsiteCode = websites[2].WebsiteCode,
            };
            uwsr = App.Identity.UserWebsiteSave(user_website, "Rastre");


            user_website = App.Identity.UserWebsiteGet(result.UserId.Value, websites[0].WebsiteCode);


            UserWebsite[] user_websites = App.Identity.UserWebsitesGet(result.UserId.Value);
        }
    }
}
