﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
	/// <summary>
	/// Summary description for MaritalStatusTests
	/// </summary>
	[TestClass]
	public class MaritalStatusTests
	{
		public MaritalStatusTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion


		[TestMethod]
		public void MaritalStatusTest_Ws()
		{
			using (IdentityReference.Identity service = new Cccs.Identity.Test.IdentityReference.Identity())
			{

				//IdentityReference.MaritalStatus[] marital_status_en = service.MaritalStatusesGet("EN");


				//IdentityReference.MaritalStatus[] marital_status_es = App.Identity.MaritalStatusesGet("ES");

				//foreach (IdentityReference.MaritalStatus marital_status in marital_status_en)
				//{
				//  IdentityReference.MaritalStatus ms_en = App.Identity.MaritalStatusGet(marital_status.MaritalStatusCode, "EN");
				//  IdentityReference.MaritalStatus ms_es = App.Identity.MaritalStatusGet(marital_status.MaritalStatusCode, "ES");
				//}
			}
		}

		[TestMethod]
		public void MaritalStatusTest()
		{
			MaritalStatus[] marital_status_en = App.Identity.MaritalStatusesGet("EN");
			MaritalStatus[] marital_status_es = App.Identity.MaritalStatusesGet("ES");

			foreach (MaritalStatus marital_status in marital_status_en)
			{
				MaritalStatus ms_en = App.Identity.MaritalStatusGet(marital_status.MaritalStatusCode, "EN");
				MaritalStatus ms_es = App.Identity.MaritalStatusGet(marital_status.MaritalStatusCode, "ES");
			}
		}
	}
}
