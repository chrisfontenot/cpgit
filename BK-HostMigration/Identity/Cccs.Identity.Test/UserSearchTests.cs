﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
	/// <summary>
	/// Summary description for UserSearchTest
	/// </summary>
	[TestClass]
	public class UserSearchTests
	{
		public UserSearchTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void UserSearchTest1()
		{
			UserSearchCriteria user_search_criteria = new UserSearchCriteria
			{
				Username = "prasad123",
			};

			UserSearchResult result = App.Identity.UserSearch(user_search_criteria);


			user_search_criteria = new UserSearchCriteria
			{
				InternetId = "74468"
			};

			result = App.Identity.UserSearch(user_search_criteria);


			user_search_criteria = new UserSearchCriteria
			{
				HostId = "B678456"
			};

			result = App.Identity.UserSearch(user_search_criteria);




			user_search_criteria = new UserSearchCriteria
			{
				LastName = "Ra"
			};

			result = App.Identity.UserSearch(user_search_criteria);


			user_search_criteria = new UserSearchCriteria
			{
				FirstName = "Ra"
			};

			result = App.Identity.UserSearch(user_search_criteria);


			user_search_criteria = new UserSearchCriteria
			{
				Zip = "30350"
			};

			result = App.Identity.UserSearch(user_search_criteria);
		}

        [TestMethod]
        public void UserJimboSearch()
        {
            var client = new JimboServiceReference.JimboClient();
            var criteria = new UserSearchCriteria()
            {
                Username = "Shi2"
            };

            var result = client.UserSearch(criteria);

            Assert.IsNotNull(result);
        }
    }
}
