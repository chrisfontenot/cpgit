﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
	/// <summary>
	/// Summary description for AddressTest
	/// </summary>
	[TestClass]
	public class AddressTest
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void AddressTest1()
		{
			User user = new User
			{
				Username = Guid.NewGuid().ToString(),
				NewPassword = "Passw0rd",
				NewPasswordConfirm = "Passw0rd",
				LanguageCode = "EN",
				IsMigratedUser = false,
				IsFullProfile = false,
				IsProfileUpdateRequired = false,
				Avatar = "My Avatar",
			};

			UserSaveResult r = App.Identity.UserSave(user, "Rastre");


			Address address = new Address
			{
				UserId = r.UserId.Value,
				AddressType = "Home",
				StreetLine1 = "123 Main St.",
				StreetLine2 = "Suite 1",
				City = "Atlanta",
				State = "GA",
				Zip = "30309"
			};

			AddressSaveResult result = App.Identity.AddressSave(address, "Rastre");


			Address[] addresses_get = App.Identity.AddressesGet(r.UserId.Value);

			Address address_get = App.Identity.AddressGet(result.AddressId.Value);


			// Invalid Zip Test
			address = new Address
			{
				AddressId = result.AddressId.Value,
				UserId = r.UserId.Value,
				AddressType = "Home",
				StreetLine1 = "123 Main St.",
				StreetLine2 = "Suite 1",
				City = "Atlanta",
				State = "GA",
				Zip = "30350"
			};

			result = App.Identity.AddressSave(address, "Rastre");

			// Bad State for Zip test
			address.AddressId = result.AddressId.Value;
			address.Zip = "00000";


			result = App.Identity.AddressSave(address, "Rastre");

		}
	}
}
