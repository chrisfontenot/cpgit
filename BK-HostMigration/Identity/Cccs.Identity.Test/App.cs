﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Configuration;
using Cccs.Translation;
using Cccs.Geographics;
using System.Configuration;

namespace Cccs.Identity.Test
{
	static class App
	{
		private static IIdentity m_identity;

		static App()
		{
			string use_service = ConfigurationManager.AppSettings["UseService"];

			if ((use_service != null) && (use_service.ToLower() == "true"))
			{
				m_identity = new Cccs.Identity.Service.Client.Identity();
			}
			else
			{
				IConfiguration configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());
				ITranslation translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());
				IGeographics geographics = new Cccs.Geographics.Impl.Cache.Geographics(new Cccs.Geographics.Impl.Geographics());
				
				m_identity = new Cccs.Identity.Impl.Identity(configuration, translation, geographics);			
			}
		}

		public static IIdentity Identity
		{
			get { return m_identity; }
		}
	}
}
