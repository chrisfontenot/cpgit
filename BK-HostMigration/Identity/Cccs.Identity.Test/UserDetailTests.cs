﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Identity.Test
{
    /// <summary>
    /// Summary description for UserDetailTest
    /// </summary>
    [TestClass]
    public class UserDetailTests
    {
        public UserDetailTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void UserDetailTest1()
        {
            //for (int i = 0; i < 100; i++)
            {

                User user = new User
                {
                    Username = Guid.NewGuid().ToString(),
                    NewPassword = "a1b2c3d4",
                    NewPasswordConfirm = "a1b2c3d4",
                    SecurityQuestionAnswer = new SecurityQuestionAnswer()
                    {
                        SecurityQuestionId = 1,
                        SecurityAnswer = "Answer 1",
                    },
                    LanguageCode = "EN",
                    IsMigratedUser = false,
                    IsFullProfile = false,
                    IsProfileUpdateRequired = false,
                    Avatar = "My Avatar",
                };

                UserSaveResult user_save_result = App.Identity.UserSave(user, "Rastre");

                if (user_save_result.IsSuccessful)
                {
                    UserDetail user_detail = new UserDetail
                    {
                        UserId = user_save_result.UserId.Value,
                        IsPrimary = true,
                        FirstName = "Randall",
                        MiddleName = "John",
                        LastName = "Miller",
                        Ssn = "000-00-" + new Random().Next(1000, 9999).ToString(),
                        BirthDate = new DateTime(1974, 8, 1),
                        IsMale = true,
                        Race = new Race { RaceCode = 'W' },
                        MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                        Email = Guid.NewGuid().ToString() + "@credability.org",
                        PhoneCell = "111-222-3333",
                        PhoneHome = "123-456-7890",
                        PhoneWork = "999-888-7777",
                    };

                    UserDetailSaveResult result = App.Identity.UserDetailSave(user_detail, "Rastre");
                    UserDetail user_detail_get = App.Identity.UserDetailGet(user_detail.UserId, user_detail.IsPrimary);

                    UserDetail user_detail2 = new UserDetail
                    {
                        UserId = user_save_result.UserId.Value,
                        IsPrimary = false,
                        FirstName = "Holly",
                        MiddleName = "S",
                        LastName = "Miller",
                        Ssn = user_detail.Ssn, // "123-45-" + new Random().Next(1000, 9999).ToString(),
                        BirthDate = new DateTime(1974, 8, 18),
                        IsMale = false,
                        Race = new Race { RaceCode = 'W' },
                        MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                        Email = Guid.NewGuid().ToString() + "@credability.org",
                        PhoneCell = "0123456789012345678901234567890123456789012345678901234567890123456789",
                        PhoneHome = "123-456-7890",
                        PhoneWork = "999-888-7777",
                    };
                    result = App.Identity.UserDetailSave(user_detail2, "Rastre");

                    UserDetail[] user_details = App.Identity.UserDetailsGet(user_detail.UserId);

                    Result r = App.Identity.UserDetailDeleteSecondary(user_detail.UserId);

                    user_details = App.Identity.UserDetailsGet(user_detail.UserId);

                }
            }

        }

        [TestMethod]
        public void UserDetailSSNTest()
        {
            User user = new User
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "a1b2c3d4",
                NewPasswordConfirm = "a1b2c3d4",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult user_save_result = App.Identity.UserSave(user, "Rastre");

            if (user_save_result.IsSuccessful)
            {
                UserDetail user_detail = new UserDetail
                {
                    UserId = user_save_result.UserId.Value,
                    IsPrimary = true,
                    FirstName = "Randall",
                    MiddleName = "John",
                    LastName = "Miller",
                    Ssn = "123-45-123X",
                    BirthDate = new DateTime(1974, 8, 1),
                    IsMale = true,
                    Race = new Race { RaceCode = 'W' },
                    MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                    Email = Guid.NewGuid().ToString() + "@credability.org",
                    PhoneCell = "111-222-3333",
                    PhoneHome = "123-456-7890",
                    PhoneWork = "999-888-7777",
                };

                UserDetailSaveResult result = App.Identity.UserDetailSave(user_detail, "Rastre");
            }

        }

        [TestMethod]
        public void UserDetailChangeSSNTest()
        {
            User user = new User
            {
                Username = Guid.NewGuid().ToString(),
                NewPassword = "a1b2c3d4",
                NewPasswordConfirm = "a1b2c3d4",
                SecurityQuestionAnswer = new SecurityQuestionAnswer()
                {
                    SecurityQuestionId = 1,
                    SecurityAnswer = "Answer 1",
                },
                LanguageCode = "EN",
                IsMigratedUser = false,
                IsFullProfile = false,
                IsProfileUpdateRequired = false,
                Avatar = "My Avatar",
            };

            UserSaveResult user_save_result = App.Identity.UserSave(user, "Rastre");

            if (user_save_result.IsSuccessful)
            {
                UserDetail user_detail = new UserDetail
                {
                    UserId = user_save_result.UserId.Value,
                    IsPrimary = true,
                    FirstName = "Randall",
                    MiddleName = "John",
                    LastName = "Miller",
                    Ssn = "123-45-" + new Random().Next(1000, 9999).ToString(),
                    BirthDate = new DateTime(1974, 8, 1),
                    IsMale = true,
                    Race = new Race { RaceCode = 'W' },
                    MaritalStatus = new MaritalStatus { MaritalStatusCode = 'M' },
                    Email = Guid.NewGuid().ToString() + "@credability.org",
                    PhoneCell = "111-222-3333",
                    PhoneHome = "123-456-7890",
                    PhoneWork = "999-888-7777",
                };

                UserDetailSaveResult result = App.Identity.UserDetailSave(user_detail, "Rastre");

                // Change SSN
                //
                user_detail.UserDetailId = result.UserDetailId.Value;
                user_detail.Ssn = "123-45-" + new Random().Next(1000, 9999).ToString();
                result = App.Identity.UserDetailSave(user_detail, "Rastre");

                UserDetail ud = App.Identity.UserDetailGet(user_save_result.UserId.Value, true);
            }

        }

    }
}
