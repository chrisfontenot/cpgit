﻿using System.Data.Linq.Mapping;
using System.Reflection;
using System.Configuration;

namespace Cccs.Identity.Dal
{
	public partial class IdentityDataContext : System.Data.Linq.DataContext
	{
		private static string m_db_encryption_key;
		private static string m_db_encryption_password;

		public IdentityDataContext() :
			base(ConfigurationManager.ConnectionStrings["Cccs.Identity.Dal.Properties.Settings.Cccs_IdentityConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}

		public static void SetEncryption(string db_encryption_key, string db_encryption_password)
		{
			m_db_encryption_key = db_encryption_key;
			m_db_encryption_password = db_encryption_password;
		}

		public void OpenEncryptionKey()
		{
			if(!string.IsNullOrEmpty(m_db_encryption_key) && !string.IsNullOrEmpty(m_db_encryption_password))
			{
				this.ExecuteQuery<string>(string.Format("OPEN SYMMETRIC KEY {0} DECRYPTION BY PASSWORD = '{1}' ", m_db_encryption_key, m_db_encryption_password), new object[] { });
			}
		}

		// Do not delete the following method when the dbml is regenerated - delete the one in Identity.designer.cs instead.
		//
		[Function(Name = "dbo.EncryptText", IsComposable = true)]
		public System.Data.Linq.Binary EncryptText([Parameter(Name = "ClearText", DbType = "NVarChar(MAX)")] string clearText)
		{
			System.Data.Linq.Binary encrypted_text = null;

			if(!string.IsNullOrEmpty(clearText))
			{
				OpenEncryptionKey();

				encrypted_text = ((System.Data.Linq.Binary) (this.ExecuteMethodCall(this, ((MethodInfo) (MethodInfo.GetCurrentMethod())), clearText).ReturnValue));
			}

			return encrypted_text;
		}
	}
}