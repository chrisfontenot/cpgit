﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity.Service
{
    [DataContract]
    public class JimboUserSearchResult : Result
    {
        [DataMember]
        public bool IsCriteriaMissing { get; set; }

        [DataMember]
        public long TotalMatches { get; set; }
        
        [DataMember]
        public UserMatch[] UserMatches { get; set; }

        [DataContract]
        public class UserMatch
        {
            [DataMember(IsRequired = true)]
            public long UserId { get; set; }

            [DataMember]
            public string Username { get; set; }

            [DataMember]
            public string FirstName { get; set; }
            
            [DataMember]
            public string MiddleName { get; set; }
            
            [DataMember]
            public string LastName { get; set; }
            
            [DataMember]
            public string StreetLine1 { get; set; }
            
            [DataMember]
            public string City { get; set; }
            
            [DataMember]
            public string State { get; set; }
            
            [DataMember]
            public string Zip { get; set; }
            
            [DataMember]
            public string Email { get; set; }
            
            [DataMember]
            public string PhoneHome { get; set; }
            
            [DataMember]
            public string PhoneCell { get; set; }
            
            [DataMember]
            public string PhoneWork { get; set; }

            [DataMember]
            public string Ssn { get; set; }

            [DataMember(IsRequired = true)]
            public bool IsPrimary { get; set; }

            [DataMember(IsRequired = true)]
            public bool IsLocked { get; set; }

            [DataMember]
            public DateTime CreateDate { get; set; }
       }
    }
}
