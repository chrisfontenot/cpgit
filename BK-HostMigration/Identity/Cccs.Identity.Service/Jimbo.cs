﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity.Dal;
using Cccs.Configuration;
using System.ServiceModel;
using System.IO;
using Cccs.Credability.Certificates;
using System.Configuration;
using Cccs.Credability.Certificates.PreFiling;

namespace Cccs.Identity.Service
{
	[ServiceContract]
	public class Jimbo
	{
		private static IConfiguration m_configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());

		public Jimbo()
		{
			var key = ConfigurationManager.AppSettings["IdmKey"];
			var password = ConfigurationManager.AppSettings["IdmPassword"];

			IdentityDataContext.SetEncryption(key, password);
		}

		private int MaxSearchResults
		{
			get
			{
				var maximumResults = 20; // Default to 20

				if(m_configuration != null)
				{
					var maximumResultsConfiguration = m_configuration.ValueGet("MaxSearchResults");
					if(!String.IsNullOrEmpty(maximumResultsConfiguration))
					{
						if(Int32.TryParse(maximumResultsConfiguration, out maximumResults))
						{
							maximumResults = Math.Max(0, maximumResults);
						}
					}
				}

				return maximumResults;
			}
		}

		[OperationContract]
		public JimboUserSearchResult UserSearch(UserSearchCriteria criteria)
		{
			var result = new JimboUserSearchResult();

			try
			{
				result.IsCriteriaMissing = criteria.IsEmpty;

				if(result.IsCriteriaMissing)
					return result;

				using(var entities = new IdentityDataContext())
				{
					criteria.Username = String.IsNullOrEmpty(criteria.Username) ? String.Empty : criteria.Username.Trim().ToUpper();
					criteria.FirstName = String.IsNullOrEmpty(criteria.FirstName) ? String.Empty : criteria.FirstName.Trim().ToUpper();
					criteria.LastName = String.IsNullOrEmpty(criteria.LastName) ? String.Empty : criteria.LastName.Trim().ToUpper();
					criteria.Email = String.IsNullOrEmpty(criteria.Email) ? String.Empty : criteria.Email.Trim().ToUpper();
					criteria.Ssn = String.IsNullOrEmpty(criteria.Ssn) ? String.Empty : criteria.Ssn.Trim().ToUpper();
					criteria.Zip = String.IsNullOrEmpty(criteria.Zip) ? String.Empty : criteria.Zip.Trim().ToUpper();
					criteria.Phone = String.IsNullOrEmpty(criteria.Phone) ? String.Empty : criteria.Phone.Trim().ToUpper();
					criteria.InternetId = String.IsNullOrEmpty(criteria.InternetId) ? String.Empty : criteria.InternetId.Trim().ToUpper();
					criteria.HostId = String.IsNullOrEmpty(criteria.HostId) ? String.Empty : criteria.HostId.Trim().ToUpper();

					entities.OpenEncryptionKey();

					result.UserMatches =
					(
						from user in entities.Users
						join user_detail in entities.UserDetails on user equals user_detail.User into ud
						join address in entities.Addresses on user equals address.User into ad
						join account in entities.Accounts on user equals account.User into ac
						from user_detail in ud.DefaultIfEmpty()
						from address in ad.DefaultIfEmpty()
						from account in ac.DefaultIfEmpty()
						where
						(
							(user.IsActive) &&
							((criteria.IsLocked == null) || ((user.LockedDate != null) == criteria.IsLocked)) &&
							(String.IsNullOrEmpty(criteria.Username) || user.Username.Trim().ToUpper().Contains(criteria.Username)) &&
							(String.IsNullOrEmpty(criteria.FirstName) || ((user_detail.FirstName != null) && user_detail.FirstName.Trim().ToUpper().Contains(criteria.FirstName))) &&
							(String.IsNullOrEmpty(criteria.LastName) || ((user_detail.LastName != null) && user_detail.LastName.Trim().ToUpper().Contains(criteria.LastName))) &&
							(String.IsNullOrEmpty(criteria.Email) || ((user_detail.Email != null) && user_detail.Email.Trim().ToUpper().Contains(criteria.Email))) &&
							(String.IsNullOrEmpty(criteria.Ssn) || ((entities.DecryptText(user_detail.Ssn) != null) && entities.DecryptText(user_detail.Ssn).Trim().ToUpper().Contains(criteria.Ssn))) &&
							((criteria.BirthDate == null) || (user_detail.BirthDate == criteria.BirthDate)) &&
							(String.IsNullOrEmpty(criteria.Zip) || ((address.Zip != null) && address.Zip.Trim().ToUpper().Contains(criteria.Zip))) &&
							(String.IsNullOrEmpty(criteria.InternetId) || ((account.InternetId != null) && account.InternetId.ToString().Contains(criteria.InternetId))) &&
							(String.IsNullOrEmpty(criteria.HostId) || account.HostId.Trim().ToUpper().Contains(criteria.HostId)) &&
							(String.IsNullOrEmpty(criteria.Phone) ||
							(
								((user_detail.PhoneHome != null) && user_detail.PhoneHome.Trim().ToUpper().Contains(criteria.Phone)) ||
								((user_detail.PhoneCell != null) && user_detail.PhoneCell.Trim().ToUpper().Contains(criteria.Phone)) ||
								((user_detail.PhoneWork != null) && user_detail.PhoneWork.Trim().ToUpper().Contains(criteria.Phone))
							))
						)
						select new JimboUserSearchResult.UserMatch
						{
							UserId = user.UserId,
							Username = user.Username,
							FirstName = user_detail.FirstName,
							MiddleName = user_detail.MiddleName,
							LastName = user_detail.LastName,
							City = address.City,
							State = address.State,
							Zip = address.Zip,
							Email = user_detail.Email,
							StreetLine1 = address.StreetLine1,
							PhoneHome = user_detail.PhoneHome,
							PhoneCell = user_detail.PhoneCell,
							PhoneWork = user_detail.PhoneWork,
							Ssn = entities.DecryptText(user_detail.Ssn),
							IsPrimary = user_detail.IsPrimary,
							IsLocked = (user.LockedDate != null),
							CreateDate = user.CreatedDate,
						}
					).Distinct().Take(MaxSearchResults).ToArray();

					result.TotalMatches = (result.UserMatches != null) ? result.UserMatches.Length : 0;
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		[OperationContract]
		public JimboAccount[] AccountsGet(long user_id)
		{
			using(IdentityDataContext entities = new IdentityDataContext())
			{
				var accounts =
                (
					from entity in entities.Accounts
					where ((entity.IsActive) && (entity.User.IsActive) && (entity.UserId == user_id))
					orderby entity.ModifiedDate descending
					select new JimboAccount
					{
						AccountId = entity.AccountId,
						UserId = user_id,
						AccountTypeCode = entity.AccountType.AccountTypeCode,
						InternetId = entity.InternetId,
						HostId = entity.HostId,
						CreateDate = entity.CreatedDate,
					}
				).ToArray();

				return accounts;
			}
		}

        [OperationContract]
        public Certificates GetCounselingCertificates(int clientNumber)
        {
            throw new NotImplementedException();
            //var service = new Cccs.Credability.Certificates.PreFiling.CounselingCertificate();
            //var dto = service.GetCertificatesByInternetId(clientNumber);

            //var certificates = new Certificates()
            //{
            //    ClientNumber = dto.ClientNumber,
            //    PrimaryCertificateNumber = dto.PrimaryCertificateNumber,
            //    SecondaryCertificateNumber = dto.SecondaryCertificateNumber,
            //};

            //return certificates;
		}

		[OperationContract]
		public byte[] GenerateActionPlan(string certificateNumber)
		{
            throw new NotImplementedException();
            //var languageCode = "ES";
            //using(var context = new IdentityDataContext())
            //{
            //    languageCode = (from c in context.CounselingCertificates
            //                    join d in context.UserDetails on c.UserDetailId equals d.UserDetailId
            //                    join u in context.Users on d.UserId equals u.UserId
            //                    select u.LanguageCode).First();
            //}

            //var service = new Cccs.Credability.Certificates.PreFiling.CounselingCertificate();
            //var dataArray = service.GetAllCertificateData(certificateNumber);

            //var primaryData = dataArray[0];
            //var secondaryData = dataArray.Length > 1 ? dataArray[1] : null;
            //var assembly = typeof(ICertificateGenerator).Assembly;
            //var generatorType = assembly.GetType(primaryData.CertificateVersionType);

            //var generator = Activator.CreateInstance(generatorType) as ICertificateGenerator;
            //var stream = generator.CreateActionPlan(languageCode, primaryData, secondaryData);
            //stream.Position = 0;

            //return stream.GetBuffer();
		}

        [OperationContract]
        public byte[] GenerateCounselingCertificate(string certificateNumber)
        {
            throw new NotImplementedException();
            //var service = new Cccs.Credability.Certificates.PreFiling.CounselingCertificate();
            //var data = service.GetCertificateData(certificateNumber);

            //var assembly = typeof(ICertificateGenerator).Assembly;
            //var generatorType = assembly.GetType(data.CertificateVersionType);

            //var generator = Activator.CreateInstance(generatorType) as ICertificateGenerator;
            //var stream = generator.CreateCertificate(data);
            //stream.Position = 0;

            //return stream.GetBuffer();
		}

		[OperationContract]
		public void UpdateAccountHostId(long accountId, string hostId)
		{
			using(IdentityDataContext entities = new IdentityDataContext())
			{
				var account = (from a in entities.Accounts
							   where a.AccountId == accountId
							   select a).First();

				account.HostId = hostId;

				entities.SubmitChanges();
			}
		}
	}
}