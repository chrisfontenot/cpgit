﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity.Service
{
    [DataContract]
    public class JimboAccount
    {
        [DataMember(IsRequired = true)]
        public long AccountId { get; set; }

        [DataMember(IsRequired = true)]
        public long UserId { get; set; }

        [DataMember]
        public string AccountTypeCode { get; set; }

        [DataMember]
        public long? InternetId { get; set; }

        [DataMember]
        public string HostId { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }
    }
}
