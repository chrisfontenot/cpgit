﻿using Cccs.Configuration;
using Cccs.Translation;
using Cccs.Geographics;

namespace Cccs.Identity.Service
{
	public class Identity : Cccs.Identity.Impl.Identity
	{
		private static IConfiguration m_configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());
		private static ITranslation m_translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());
		private static IGeographics m_geographics = new Cccs.Geographics.Impl.Cache.Geographics(new Cccs.Geographics.Impl.Geographics());

		public Identity()	: base(m_configuration, m_translation, m_geographics)
		{
		}
	}
}
