﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Cccs.Identity.Service
{
    [DataContract]
    public class Certificates
    {
        [DataMember]
        public int ClientNumber { get; set; }

        [DataMember]
        public string PrimaryCertificateNumber { get; set; }

        [DataMember]
        public string SecondaryCertificateNumber { get; set; }
    }
}