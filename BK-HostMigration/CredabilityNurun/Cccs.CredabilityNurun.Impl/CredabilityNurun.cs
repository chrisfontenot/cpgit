﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.CredabilityNurun.Dal;

namespace Cccs.CredabilityNurun.Impl
{
	public class CredabilityNurun : ICredabilityNurun
	{
		public FeeWaiverInfo FeeWaiverInfoGet(long user_id, string account_type_code)
		{
			FeeWaiverInfo fee_waiver_info = null;

			if (string.Compare(account_type_code.Trim(), "WS.CAM", true) == 0)
			{
				account_type_code = "Edu";
			}
			else if (string.Compare(account_type_code.Trim(), "BR.CAM", true) == 0)
			{
				account_type_code = "Coun";
			}

			using (CredabilityNurunDataContext data_context = new CredabilityNurunDataContext())
			{
				fee_waiver_info =
				(
					from entity in data_context.FeeWaivers
					where ((entity.client_number == user_id) && (string.Compare(account_type_code.Trim(), entity.ClientType.Trim()) == 0)) 
					select new FeeWaiverInfo
					{
						IsApproved = (entity.Aproved == null) ? null : (bool?)(entity.Aproved == 1),
						IsJointFile = (entity.JointFile == null) ? null : (bool?)(entity.JointFile == 1),
					}
				).FirstOrDefault();
			}

			return fee_waiver_info;
		}
	}
}
