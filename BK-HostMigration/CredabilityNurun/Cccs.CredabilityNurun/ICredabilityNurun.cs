﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Cccs.CredabilityNurun
{
	[ServiceContract]
	public interface ICredabilityNurun
	{
		[OperationContract]
		FeeWaiverInfo FeeWaiverInfoGet(long user_id, string account_type_code);
	}
}
