﻿using System;
using System.Runtime.Serialization;

namespace Cccs.CredabilityNurun
{
	[DataContract]
	public class FeeWaiverInfo
	{
		[DataMember]
		public bool? IsApproved { get; set; }

		[DataMember]
		public bool? IsJointFile { get; set; }
	}
}
