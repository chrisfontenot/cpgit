﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Picklist.Dal;

namespace Cccs.Picklist.Impl
{
	public class Picklist	: IPicklist
	{
		public PicklistItem[] PicklistItemsGet(string name)
		{
			Cccs.Picklist.PicklistItem[] picklist_items = null;

			using (PicklistDataContext entities = new PicklistDataContext())
			{
				picklist_items =
				(
					from entity in entities.PicklistItems
					where (entity.IsActive && (string.Compare(entity.Picklist.Name.Trim(), name.Trim(), true) == 0))
					orderby entity.SortOrder
					select new Cccs.Picklist.PicklistItem
					{
						PicklistItemId = entity.PicklistItemId,
						Text = entity.Text,
						Value = entity.Value,
					}
				).ToArray();
			}

			return picklist_items;
		}

		public PicklistItem[] PicklistSubItemsGet(int parent_picklist_item_id)
		{
			Cccs.Picklist.PicklistItem[] picklist_items = null;

			using (PicklistDataContext entities = new PicklistDataContext())
			{
				picklist_items =
				(
					from entity in entities.PicklistItems
					join parent in entities.PicklistItemSets on entity.PicklistItemId equals parent.PickListItemId
					where (parent.ParentPicklistItemId == parent_picklist_item_id)
					select new Cccs.Picklist.PicklistItem
					{
						PicklistItemId = entity.PicklistItemId,
						Text = entity.Text,
						Value = entity.Value,
					}
				).ToArray();
			}

			return picklist_items;
		}
	}
}
