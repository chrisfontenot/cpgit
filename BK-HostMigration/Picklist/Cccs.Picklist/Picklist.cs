﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Picklist
{
	[DataContract]
	public class Picklist
	{
		[DataMember]
		public int PicklistId { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public string Description { get; set; }
	}
}
