﻿using System.ServiceModel;

namespace Cccs.Picklist
{
	[ServiceContract]
	public interface IPicklist
	{
		[OperationContract]
		PicklistItem[] PicklistItemsGet(string picklist_name);

		[OperationContract]
		PicklistItem[] PicklistSubItemsGet(int parent_picklist_item_id);
	}
}
