﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Picklist
{
	[DataContract]
	public class PicklistItem
	{
		[DataMember]
		public int PicklistItemId { get; set; }

		[DataMember]
		public string Text { get; set; }

		[DataMember]
		public string Value { get; set; }
	}
}
