﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.UI.WebControls;

namespace Cccs.Picklist.Test
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class PicklistTests
	{
		public PicklistTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void TestMethod1()
		{
			PicklistItem[] pick_list_items_makes = App.Picklist.PicklistItemsGet("Make");
			PicklistItem[] pick_list_items_models = App.Picklist.PicklistItemsGet("model");
			PicklistItem[] pick_list_items_years = App.Picklist.PicklistItemsGet("Year");

			foreach (PicklistItem make in pick_list_items_makes)
			{
				PicklistItem[] models = App.Picklist.PicklistSubItemsGet(make.PicklistItemId);
			}
		}

		[TestMethod]
		public void TranslatedPicklistTest()
		{				 
			DropDownList ddl_en = new DropDownList();
			ddl_en.Items.Add(new ListItem("- Please Select -"));
			AddItems(ddl_en.Items, "Make", "EN");

			DropDownList ddl_es = new DropDownList();
			ddl_es.Items.Add(new ListItem("- Selecto Gracias -"));
			AddItems(ddl_es.Items, "Make", "ES");
		}

		public void AddItems(ListItemCollection list_item_collection, string picklist_name, string language_code)
		{
			PicklistItem[] items = App.Picklist.PicklistItemsGet(picklist_name);

			if ((items != null) && (items.Length > 0) )
			{
				string[] texts = App.Translation.LanguageTextsGet(language_code, (from x in items select x.Text).ToArray());

				if ((texts != null) && (texts.Length == texts.Length))
				{
					for (int i = 0; i < texts.Length; i++)
					{
						list_item_collection.Add(new ListItem(texts[i], items[i].Value ?? items[i].Text)); // Uses Text if Value is null
					}
				}
			}
		}
	}
}
