﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Translation;

namespace Cccs.Picklist.Test
{
	public static class App
	{
		static IPicklist m_picklist = new Cccs.Picklist.Impl.Picklist();
		static ITranslation m_translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());
		
		public static IPicklist Picklist 
		{ 
			get { return m_picklist; } 
		}

		public static ITranslation Translation
		{
			get { return m_translation; }
		}
	}
}
