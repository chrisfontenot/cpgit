-- Create Database Master Key 
CREATE MASTER KEY ENCRYPTION BY
PASSWORD = '2145FSfgrewq%#$246fbvIFe0nxQ#iMQdsEUI34%^dkEXm0$!dsD4eF'
GO

-- created for Counseling database
CREATE SYMMETRIC KEY CounselingKey WITH
IDENTITY_VALUE = 'Encryption Key for Counseling',
ALGORITHM = AES_256, 
KEY_SOURCE = 'k`]Jl{$*1_0:DCBo]aor{93#TmFBg&Cl^N[N!BS1/s@rb$6Q-#i#1_TL&9W_w,wm'
ENCRYPTION BY PASSWORD = 'S3@5&hFRwQ9*7'


-- created for Identity database
CREATE SYMMETRIC KEY IdentityKey WITH
IDENTITY_VALUE = 'Encryption Key for Counseling',
ALGORITHM = AES_256, 
KEY_SOURCE = 'k`]Jl{$*1_0:DCBo]aor{93#TmFBg&Cl^N[N!BS1/s@rb$6Q-#i#1_TL&9W_w,wm'
ENCRYPTION BY PASSWORD = 'fhD)429(OhGr$xQa'
