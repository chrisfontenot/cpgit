﻿using Cccs.Debtplus.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.DebtPlus;

namespace Cccs.Debtplus
{
    public interface IDebtplus
    {
        string GetCounselorName(string counselorID);

        EscrowResult GetEscrow(int firmID, string websiteCode);

        string ReqCredRpt(long userID, int clientNumber);

        string RequestCreditReport(int clientNumber, string clientType, string refCode, string primaryBirthCity, string secondaryBirthCity, string primaryFirstName, string primaryLastName, string primarySsn, DateTime? primaryBirthDate, string streetLine, string city, string state, string zip, string secondaryFirstName, string secondaryLastName, string secondarySsn, DateTime? secondaryBirthDate);

        Result GetCredRpt(int clientNumber);

        CreditScoreData GetCredScore(int clientNumber);

        bool SaveTrans(int clientNumber, int vanTranRef, string invoiceNumber, float charAmount);

        int GetNextSeqNo(int clientNumber);

        object HaveCredRpt(int clientNumber);

        object UpdateOCS(int clientNumber, int stat);

        object LoadDmp(int clientNumber);

        double GetCharAmt(string attorneyCode, string zipCode, string lineOfService);

        DateTime GetUvDts();

        //string StageUvData(UvPushData UvData);

        string SavePayrollDeductions(int InternetNumber, List<PayrollDeduction> Deductions);

        List<PayrollDeduction> ReadPayrollDeductions(int internetNumber);

        bool StageUvData(DebtplusPushData UvData);

        JudicialDistrict GetJudicialDistrict(string zipCode);

        string GetEoustJudicialDistrictByName(string districtName);

        EoustLoginData GetEoustLogin();

        string GetFirmEmail(string FirmID);

        bool SetCertNo(CertReIssue reIssueData);

        MimClientLoginData Mim_LoginClient(string RegNum, string websiteCode);

        MimCreateLoginValidateData Mim_CreateLogin_ValidateData(string ssn);

        MimDisclosureLoadData Mim_Disclosure_LoadData(string clientNumber);

        DebtplusResult Mim_Disclosure_SaveTransaction(string clientNumber, string dc_fnameoncard, string dc_mnameoncard
           , string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state
           , string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no
           , string van_trans_ref, string char_amt);

        MimForm7LoadData Mim_Form7_LoadData(string clientNumber, string regNum);

        DebtplusResult Mim_Form7_SaveData(string clientNumber, string contact_lastname, string contact_firstname,
            string contact_initial, string contact_ssn, string contact_address, string contact_address2,
            string contact_city, string contact_state, string contact_zip, string contact_marital,
            string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity,
            string cemail, string dayphone, string nitephone, string attyname, string attyemail,
            string firm_id, string escrow, string secondary_flag, string char_type, string casenumber,
            string bankruptcy_type, string income_range, string number_in_house, string languageCode, string char_amt);

        DebtplusResult Mim_Form7_SaveWriteOff(string clientNumber, string regnum, string char_amt);

        DebtplusResult MimCompleteCourse(string clientNumber, string prescore, string postscore, string TestDTS);

        MimQuicCalcLoadData Mim_QuicCalc_LoadData(string clientNumber, string regnum);

        DebtplusResult Mim_QuicCalc_SaveData(string clientNumber, string contact_state, string monthly_gross_income, string size_of_household);

        string Mim_HostID_ReturningUser_Get(string reg_num);

        DebtplusResult MimSaveRegisterAttorney(string clientNumber, string CaseNo, string Ssn, string FirmId, string AttnyName, string AttnyEmail, string UserName);

        DebtplusResult Mim_WaiverWaiver_SaveUv(string ClNo);

        DebtplusResult MimSavePreTest(string ClNo, int PreTest);

        DebtplusResult MimDisclosureSaveMtcn(string ClNo, string mtcn, string char_amt);

        DebtplusResult MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number, string char_amt);

        double MimDisclosureGetChargeAmount(string ZipCode, string FirmCode);

        DebtplusResult MimSaveXRef(string PriSsn, string SecSsn);

        int GetMimSequenceNumber(string clientNumber);

        MimForm7MainLineData Mim_Form7_MainLine(string contact_state);

        MimSetTransSaveWriteData Mim_SetTrans_SaveWriteOff(string clientNumber, string regnum);

        MimSetTransSaveWriteData Mim_SetTrans_SaveWriteOff(string clientNumber, string regnum, string char_amt);

        void SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip);
        string Mim_NewClsID_Get(string reg_num);

        void SendDataToDebtplus(int clientNumber, long accountNumber, string firmID, string zipCode, string certificateNumber, string waiverType, string escrow, string websiteCode, int escrowProbonoValue, string seecondaryFlag);
    }
}
