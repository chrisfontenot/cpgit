using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class PayrollDeduction
    {
        public String Description { get; set; }
        public decimal Amount { get; set; }
    }
}

