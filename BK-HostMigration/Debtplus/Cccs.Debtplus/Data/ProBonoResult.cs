﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class ProBonoResult
    {
        public string FirmCode { get; set; }

        public string FirmName { get; set; }

        public int ProBono { get; set; }

        public decimal ChargeAmount { get; set; }
    }
}
