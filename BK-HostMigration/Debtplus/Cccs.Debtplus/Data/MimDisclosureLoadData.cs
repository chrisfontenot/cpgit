﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimDisclosureLoadData
    {
        public string BankName { get; set; }
        public string NameOnCheck { get; set; }
        public string PaymentType { get; set; }
        public string AbaNumber { get; set; }
        public string AcctNumber { get; set; }
        public string DcFNameOncard { get; set; }
        public string DcMNameOnCard { get; set; }
        public string DcLNameOnCard { get; set; }
        public string DcBillAddr1 { get; set; }
        public string DcBillAddr2 { get; set; }
        public string DcBillCity { get; set; }
        public string DcBillState { get; set; }
        public string DcBillZip { get; set; }
        public string DcAcctNum { get; set; }
        public string CardExp { get; set; }
        public string Mtcn { get; set; }
        public bool DataFromDb { get; set; }
        public bool IsSuccessful { get; set; }
    }
}
