﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class EscrowResult
    {
        public string FirmCode { get; set; }

        public string FirmName { get; set; }

        public string Escrow { get; set; }

        public int EscrowProBono { get; set; }

        public bool IsLegalAid { get; set; }

        public decimal ChargeAmount { get; set; }
    }
}
