﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class DebtplusResult
    {
        private Exception m_exception = null;

        public DebtplusResult()
        {
        }

        public DebtplusResult(DebtplusResult result)
        {
            IsSuccessful = result.IsSuccessful;
            Messages = result.Messages;
            Exception = result.Exception;
        }

        public bool IsSuccessful { get; set; }

        public string[] Messages { get; set; }

        public string ExceptionStr { get; set; }

        public Exception Exception
        {
            get
            {
                return m_exception;
            }
            set
            {
                m_exception = value;
                SetExceptionStr(value);
            }
        }

        private void SetExceptionStr(Exception exception)
        {
            StringBuilder sb = new StringBuilder();

            if (exception != null)
            {
                for (Exception ex = exception; ex != null; ex = ex.InnerException)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append("  Inner exception: ");
                    }

                    sb.Append(ex.GetType().ToString());
                    sb.Append(": '");
                    sb.Append(ex.Message);
                    sb.AppendLine("'.");
                }

                sb.AppendLine();
                sb.AppendLine("StackTrace: ");
                sb.Append(exception.StackTrace);

                ExceptionStr = sb.ToString();
            }
        }
    }
}
