﻿using Cccs.Debtplus.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus
{
    public class DebtplusCredResult : DebtplusResult
    {
        public String TotalOriginalDebt{ get; set; }
        public String TotalPTD{ get; set; }
        public String TotalPTM{ get; set; }
        public String TotalEstBal{ get; set; }
        public String DueDate{ get; set; }
        public String DueAmount{ get; set; }
        public String AutoPay{ get; set; }
        public String PlanStartDate{ get; set; }
        public CaCreditor[] Creditor{ get; set; }
    }

    public class CaCreditor
    {
        public String AccountID{ get; set; }
        public String CreditorName{ get; set; }
        public String AccountNumber{ get; set; }
        public String OriginalBalance{ get; set; }
        public String PaidToDate{ get; set; }
        public String ScheduledPayment{ get; set; }
        public String PaidThisMonth{ get; set; }
        public String Balance{ get; set; }
        public String InterestRate{ get; set; }
        public String PayoutDate{ get; set; }
        public String VerifiedBalance{ get; set; }
        public String VerifiedDate{ get; set; }
        public bool VerifyCan{ get; set; }
        public String ProposalOnFile{ get; set; }
    }
}
