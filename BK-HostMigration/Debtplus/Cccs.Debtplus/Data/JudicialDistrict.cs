﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.DebtPlus
{
	public class JudicialDistrict
	{
		public string DistrictCode { get; set; }
		public string DistrictName { get; set; }
	}
}
