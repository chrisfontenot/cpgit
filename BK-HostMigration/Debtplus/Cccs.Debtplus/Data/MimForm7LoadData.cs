﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimForm7LoadData
    {
        public string contact_firstname { get; set; }
        public string contact_initial { get; set; }
        public string contact_lastname { get; set; }
        public string contact_address { get; set; }
        public string contact_address2 { get; set; }
        public string contact_city { get; set; }
        public string contact_state { get; set; }
        public string contact_zip { get; set; }
        public string contact_email { get; set; }
        public string casenumber { get; set; }
        public string dayphone { get; set; }
        public string nitephone { get; set; }
        public string attyname { get; set; }
        public string attyemail { get; set; }
        public string firmcode { get; set; }
        public string bankruptcy_type { get; set; }
        public string contact_marital { get; set; }
        public string contact_sex { get; set; }
        public string contact_race { get; set; }
        public string contact_hispanic { get; set; }
        public string income_range { get; set; }
        public string numberinhouse { get; set; }
        public bool IsSuccessful { get; set; }
        public Exception exception { get; set; }
    }
}
