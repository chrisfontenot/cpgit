﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimCreateLoginValidateData
    {
        public bool IsValid { get; set; }

        public bool IsSsnAlreadyOnFile { get; set; }

        public string JointSsn { get; set; }

        public string SecondaryFlag { get; set; }

        public string SpUvId { get; set; }

        public bool IsSuccessful { get; set; }
    }
}
