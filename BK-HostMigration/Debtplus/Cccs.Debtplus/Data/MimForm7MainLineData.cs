﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimForm7MainLineData
    {
        public string CharType { get; set; }
        public string CharAmt { get; set; }
        public bool IsSuccessful { get; set; }
        public Exception exception { get; set; }
    }
}
