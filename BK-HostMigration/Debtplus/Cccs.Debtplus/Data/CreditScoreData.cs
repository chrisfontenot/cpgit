﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class CreditScoreData
    {
        public string PrimaryScore { get; set; }
        public string SecondaryScore { get; set; }
        public bool IsSuccessful { get; set; }
        public Exception exception { get; set; }
    }
}
