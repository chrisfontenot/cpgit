﻿
namespace Cccs.Debtplus.Data
{
	public class DebtplusPushCreditor
	{
		public string Name { get; set; }
		public float Balance { get; set; }
		public float Payment { get; set; }
		public float IntRate { get; set; }
		public string PriAcctHolder { get; set; }
		public string PastDue { get; set; }
		public string PreAcctNo { get; set; }
		public string CrAcctNo { get; set; }
	}
}
