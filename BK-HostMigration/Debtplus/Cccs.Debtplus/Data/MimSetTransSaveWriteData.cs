﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimSetTransSaveWriteData
    {
        public string State { get; set; }
        public string CurSeqNo { get; set; }
        public string LastTrans { get; set; }
        public string CharAmt { get; set; }
        public string CurInvNo { get; set; }
        public string TranId { get; set; }
        public bool IsSuccessful { get; set; }
        public Exception exception { get; set; }
    }
}
