﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class MimQuicCalcLoadData
    {
        public string ContactState { get; set; }
        public int SizeOfHousehold { get; set; }
        public decimal MonthlyGrossIncome { get; set; }
        public bool IsSuccessful { get; set; }
        public Exception exception { get; set; }

    }
}
