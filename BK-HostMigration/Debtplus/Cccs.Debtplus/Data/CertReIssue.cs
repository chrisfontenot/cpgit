﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Data
{
    public class CertReIssue
    {
        public string ClNo;
        public string CertNo;
        public DateTime CertIssue;
        public bool IsPrimary;
    }
}
