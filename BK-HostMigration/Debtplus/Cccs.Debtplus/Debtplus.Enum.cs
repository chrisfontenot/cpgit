﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Enum
{
    public enum PaymentType
    {
        None = 0,
        DebitCard = 1,
        ACH = 2,
        Escrow = 3,
        FeeWaiver = 4
    }

    public enum FeeWaiverType
    {
        None = 0,
        Income = 1,
        SSDI = 2,
        ProBono = 3,
        GoodWill = 4,
        LegalAid = 5
    }
}
