﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Cccs.Debtplus.Dal;
using System.Configuration;
using Cccs.Debtplus.Data;
using Cccs.Identity;
using Cccs.Credability.Dal;
using Cccs.Debtplus.Enum;

namespace Cccs.Debtplus.Impl
{
    public partial class Debtplus : IDebtplus
    {
        public double GetCharAmt(string attorneyCode, string zipCode, string lineOfService)
        {
            decimal amount = 0;
            using (var bc = new Dal.DebtplusDataContext())
            {
                int productID = 0;
                switch (lineOfService)
                {
                    case "B":
                        productID = PreFilingProductID;
                        break;
                    case "W":
                        productID = PostDischargeProductID;
                        break;
                    default:
                        break;
                }
                // TODO : Add code the convert lineofService to Product
                var product = bc.products.Where(s => s.oID == productID).FirstOrDefault();

                var state = bc.ZipCodeSearches.Where(s => s.ZIPCode == zipCode).FirstOrDefault();

                if (state != null)
                {
                    var productFee = bc.product_states.Where(ps => ps.productID == productID && ps.stateID == state.State).FirstOrDefault();

                    if (productFee != null && productFee.Amount != 0.00m)
                    {
                        amount = productFee.Amount;
                    }
                }
                else
                {
                    amount = product.Amount;
                }
            }

            return Convert.ToDouble(amount);
        }

        public string GetCounselorName(string counselorID)
        {
            string counselorName = string.Empty;
            using (var dc = new DebtplusDataContext())
            {
                // TODO: May be we need to map the LivePersonID to CounselorID
                var counselor = dc.counselors.Where(c => c.CounselorID == Convert.ToInt32(counselorID)).FirstOrDefault();

                if (counselor != null)
                {
                    // get the counselor name record using nameid
                    var name = dc.Names.Where(n => n.ID == counselor.NameID).FirstOrDefault();

                    if (name != null)
                    {
                        // get the counselor name from the names table
                        counselorName = string.Format("{0} {1}", name.First, name.Last);
                    };
                }
            }
            return counselorName;
        }

        public EscrowResult GetEscrow(int firmID, string websiteCode)
        {
            EscrowResult result = new EscrowResult(); ;

            using (var dc = new DebtplusDataContext())
            {
                // TODO: Check which table has the escrow flag.
                var vendor = dc.vendors.Where(v => v.oID == firmID).FirstOrDefault();

                vendor_product product = null;
                //vendor_product postProduct = null;

                if (vendor != null)
                {
                    result.FirmName = vendor.Name;

                    var vendorProducts = dc.vendor_products.Where(vp => vp.vendor == vendor.oID).ToList();

                    foreach (var vp in vendorProducts)
                    {
                        if (websiteCode == Cccs.Identity.Website.BKC && vp.product == PreFilingProductID)
                        {
                            product = vp;
                        }
                        else if (websiteCode == Cccs.Identity.Website.BKDE && vp.product == PostDischargeProductID)
                        {
                            product = vp;
                        }
                    }
                    // No Escrow
                    result.Escrow = "N";
                    if (product != null && (product.EscrowProBono == 1 || product.EscrowProBono == 2))
                    {
                        result.Escrow = "Y";
                        /* set escrow Probono value to the the value of vendor_product value, use this to differentiate between Escrow (1) or Probono (2) */
                        result.EscrowProBono = product.EscrowProBono;
                    }
                    /*if (postProduct != null && (postProduct.EscrowProBono == 1 || postProduct.EscrowProBono == 2))
                    {
                        result.Escrow = 3;
                        result.EscrowProBono = postProduct.EscrowProBono;
                    }
                    if (preFilingProduct != null && postProduct != null &&
                        ((preFilingProduct.EscrowProBono == 1 && postProduct.EscrowProBono == 1) ||
                        (preFilingProduct.EscrowProBono == 2 && postProduct.EscrowProBono == 2)))
                    {
                        result.Escrow = 1;
                        result.EscrowProBono = preFilingProduct.EscrowProBono;
                    }*/
                    if (vendor.IsLegalAid == true)
                    {
                        result.Escrow = "Y";
                        result.IsLegalAid = true;
                    }
                }
            }

            return result;
        }

        //public ProBonoResult GetProBono(int firmID)
        //{
        //    ProBonoResult result = new ProBonoResult(); ;

        //    using (var dc = new DebtplusDataContext())
        //    {
        //        // TODO: Check which table has the escrow flag.
        //        var vendor = dc.vendors.Where(v => v.Label == firmID.ToString()).FirstOrDefault();

        //        vendor_product preFilingProduct = null;
        //        vendor_product postProduct = null;

        //        if (vendor != null)
        //        {
        //            result.FirmName = vendor.Name;

        //            var vendorProducts = dc.vendor_products.Where(vp => vp.vendor == vendor.oID).ToList();

        //            foreach (var vp in vendorProducts)
        //            {
        //                if (vp.product == PreFilingProductID)
        //                {
        //                    preFilingProduct = vp;
        //                }
        //                else if (vp.product == PostDischargeProductID)
        //                {
        //                    postProduct = vp;
        //                }
        //            }
        //            // No ProBono
        //            result.ProBono = 0;
        //            if (preFilingProduct != null && preFilingProduct.EscrowProBono == 2)
        //            {
        //                result.ProBono = 2;
        //            }
        //            if (postProduct != null && postProduct.EscrowProBono == 2)
        //            {
        //                result.ProBono = 3;
        //            }
        //            if (preFilingProduct != null && postProduct != null &&
        //                preFilingProduct.EscrowProBono == 2 && postProduct.EscrowProBono == 2)
        //            {
        //                result.ProBono = 1;
        //            }
        //            if (vendor.IsLegalAid == true)
        //            {
        //                result.ProBono = 6;
        //            }
        //        }
        //    }

        //    return result;
        //}

        private static string FinalId(int client_number, string client_type, string ref_code)
        {
            string final_id;

            if ((client_type == "HUD") || ((client_type == "WEL") && (ref_code == "WFH/360")))
            {
                final_id = string.Format("H{0}", client_number);
            }
            else
            {
                final_id = string.Format("IN{0}", client_number);
            }

            return final_id;
        }

        public string ReqCredRpt(long user_id, int client_number)
        {
            string dmpAvailable = string.Empty;
            var options = new UserProfileOptions { IncludeUserDetails = true, IncludeAddresses = true, IncludeAccounts = true };

            UserProfile user_profile = identity.UserProfileGet(user_id, options);

            if ((user_profile != null) && (user_profile.UserDetailPrimary != null) && (user_profile.Addresses != null) && (user_profile.Addresses.Length > 0))
            {
                string client_type = null;
                string ref_code = null;
                string birth_city_primary = null;
                string birth_city_secondary = null;

                using (CredabilityDataContext data_context = new CredabilityDataContext())
                {
                    var contact_detail =
                    (
                        from c in data_context.contactdetails
                        where (c.client_number == client_number)
                        select new
                        {
                            ClientType = c.ClientType,
                            RefCode = c.refcode,
                            BirthCityPrimary = c.cred_rpt_bcity,
                            BirthCitySecondary = c.cred_rpt_bcity2,
                        }
                    ).FirstOrDefault();

                    if (contact_detail != null)
                    {
                        client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
                        ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
                        birth_city_primary = (contact_detail.BirthCityPrimary != null) ? contact_detail.BirthCityPrimary.Trim() : null;
                        birth_city_secondary = (contact_detail.BirthCitySecondary != null) ? contact_detail.BirthCitySecondary.Trim() : null;
                    }
                }

                if (!string.IsNullOrEmpty(birth_city_primary))
                {
                    string final_id = FinalId(client_number, client_type, ref_code);

                    var crq = new CreditReportQueue()
                    {
                        FirstName = user_profile.UserDetailPrimary.FirstName.Trim(),
                        LastName = user_profile.UserDetailPrimary.LastName.Trim(),
                        SSN = user_profile.UserDetailPrimary.Ssn.Trim(),
                        BirthDate = user_profile.UserDetailPrimary.BirthDate.HasValue ? Convert.ToDateTime(user_profile.UserDetailPrimary.BirthDate.Value) as DateTime? : null,
                        Street = user_profile.Addresses[0].StreetLine1.Trim(),
                        City = user_profile.Addresses[0].City.Trim(),
                        State = user_profile.Addresses[0].State.Trim(),
                        Zipcode = user_profile.Addresses[0].Zip.Trim(),
                        IsPrimary = string.IsNullOrEmpty(birth_city_primary.Trim()) ? false : true,
                        FinalID = final_id
                    };

                    if ((user_profile.UserDetailSecondary != null) && !string.IsNullOrEmpty(birth_city_secondary))
                    {
                        final_id += "*S";
                        crq.FirstName = user_profile.UserDetailSecondary.FirstName.Trim();
                        crq.LastName = user_profile.UserDetailSecondary.LastName.Trim();
                        crq.SSN = user_profile.UserDetailSecondary.Ssn.Trim();
                        crq.BirthDate = user_profile.UserDetailSecondary.BirthDate.HasValue ? Convert.ToDateTime(user_profile.UserDetailSecondary.BirthDate.Value) as DateTime? : null;
                        var isSecondary = string.IsNullOrEmpty(birth_city_secondary.Trim()) ? false : true;
                        crq.IsPrimary = !isSecondary; // This is secondary user data, set IsPrimary to False
                    }

                    using (var dc = new DebtplusDataContext())
                    {
                        dc.CreditReportQueues.InsertOnSubmit(crq);
                        dc.SubmitChanges();
                    }

                    /*if (user_profile.Accounts.FindByAccountTypeCode(Account.BKE) == null)
                    {

                    }*/

                }
            }
            return dmpAvailable;
        }

        public string RequestCreditReport(int client_number, string client_type, string ref_code, string primary_birth_city, string secondary_birth_city, string primary_first_name, string primary_last_name, string primary_ssn, DateTime? primary_birth_date, string street_line1, string city, string state, string zip, string secondary_first_name, string secondary_last_name, string secondary_ssn, DateTime? secondary_birth_date)
        {
            string final_id = FinalId(client_number, client_type, ref_code);

            var crq = new CreditReportQueue()
            {
                FirstName = primary_first_name,
                LastName = primary_last_name,
                SSN = primary_ssn,
                BirthDate = primary_birth_date.HasValue ? Convert.ToDateTime(primary_birth_date.Value) as DateTime? : null,
                Street = street_line1,
                City = city,
                State = state,
                Zipcode = zip,
                IsPrimary = Convert.ToBoolean(primary_birth_city),
                FinalID = final_id
            };

            if (!string.IsNullOrEmpty(secondary_birth_city))
            {
                final_id += "*S";
                crq.FirstName = secondary_first_name;
                crq.LastName = secondary_last_name;
                crq.SSN = secondary_ssn;
                crq.BirthDate = secondary_birth_date.HasValue ? Convert.ToDateTime(secondary_birth_date.Value) as DateTime? : null;
                crq.IsPrimary = Convert.ToBoolean(secondary_birth_city.Trim());
            }

            using (var dc = new DebtplusDataContext())
            {
                dc.CreditReportQueues.InsertOnSubmit(crq);
                dc.SubmitChanges();
            }

            return null;
        }

        public string SavePayrollDeductions(int InternetNumber, List<PayrollDeduction> Deductions)
        {
            List<PayrollDeduct> lstProduct = new List<PayrollDeduct>();

            foreach (var PayrollDeduction in Deductions)
            {
                PayrollDeduct objProduct = new PayrollDeduct();
                objProduct.description = PayrollDeduction.Description;
                objProduct.Amount = PayrollDeduction.Amount;
                lstProduct.Add(objProduct);
            }
            using (var dc = new DebtplusDataContext())
            {
                dc.PayrollDeducts.InsertAllOnSubmit(lstProduct);
                dc.SubmitChanges();
            }
            return "";
        }

        public List<PayrollDeduction> ReadPayrollDeductions(int internetNumber)
        {
            using (var dc = new DebtplusDataContext())
            {
                var PayrollDeducts = dc.PayrollDeducts.Where(obj => obj.ID == internetNumber);

                List<PayrollDeduction> lstPayrollDeduction = new List<PayrollDeduction>();

                if (PayrollDeducts != null)
                {
                    PayrollDeduction objPDeduction = null;
                    foreach (var PayrollDeduction in PayrollDeducts)
                    {
                        objPDeduction = new Data.PayrollDeduction();
                        objPDeduction.Description = PayrollDeduction.description;
                        objPDeduction.Amount = PayrollDeduction.Amount;
                        lstPayrollDeduction.Add(objPDeduction);
                    }
                }
                return lstPayrollDeduction;
            }
        }

        public Result GetCredRpt(int clientNumber)
        {
            var result = new Result();
            CreditorAccountQueue creditorAccount = null;
            try
            {
                bool clear_credit_report_pull = false;

                string client_type = null;
                string ref_code = null;
                using (CredabilityDataContext data_context = new CredabilityDataContext())
                {
                    var contact_detail =
                    (
                        from c in data_context.contactdetails
                        where (c.client_number == clientNumber)
                        select new
                        {
                            ClientType = c.ClientType,
                            RefCode = c.refcode,
                        }
                    ).FirstOrDefault();

                    if (contact_detail != null)
                    {
                        client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
                        ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
                    }
                }

                int i = 1;
                string final_id = FinalId(clientNumber, client_type, ref_code);
                bool done = false;

                try
                {
                    using (var dc = new DebtplusDataContext())
                    {
                        var finalID = final_id + "*" + i.ToString();
                        creditorAccount = dc.CreditorAccountQueues.Where(ca => ca.FinalID == finalID).FirstOrDefault();

                        if (creditorAccount != null)
                        {
                            using (CredabilityDataContext data_context = new CredabilityDataContext())
                            {
                                var creditor_details = from c in data_context.creditordetails
                                                       where ((c.client_number == clientNumber) && (string.Compare(c.cr_flag, "y", true) == 0))
                                                       select c;

                                data_context.creditordetails.DeleteAllOnSubmit(creditor_details);
                                data_context.SubmitChanges();
                            }
                        }
                        else
                        {
                            done = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    done = true;
                }

                while (!done)
                {
                    clear_credit_report_pull = true;
                    try
                    {
                        int creditor_bal = 0;
                        try
                        {
                            //creditor_bal = (int)float.Parse(CleanData(creditorAccount.CreditorBalance, NoChr + "."));
                            creditor_bal = (int)creditorAccount.CreditorBalance;
                        }
                        catch (FormatException)
                        {
                            creditor_bal = 0;
                        }

                        int creditor_payments = 0;
                        try
                        {
                            //creditor_payments = (int)float.Parse(CleanData(UvRec.Extract(6).ToString(), NoChr + "."));
                            creditor_payments = (int)creditorAccount.CreditorPayment;
                        }
                        catch (FormatException)
                        {
                            creditor_payments = 0;
                        }

                        //string creditor_name = CleanData(UvRec.Extract(1).ToString(), AlphaNoChr);
                        //string creditor_jointacct = CleanData(UvRec.Extract(5).ToString(), AlphaNoChr);
                        //string CrAcntNoEnc = CleanData(UvRec.Extract(3).ToString(), AlphaNoChr);
                        //string PreAcntNoEnc = CleanData(UvRec.Extract(3).ToString(), AlphaNoChr);

                        string creditor_name = creditorAccount.CreditorName;
                        string creditor_jointacct = creditorAccount.CreditorJointAccount;
                        string CrAcntNoEnc = creditorAccount.CreditorAccountNumberEnc;
                        string PreAcntNoEnc = creditorAccount.PreAccountNumberEnc;

                        using (CredabilityDataContext data_context = new CredabilityDataContext())
                        {
                            creditordetail creditor_detail = new creditordetail()
                            {
                                creditor_intrate = 0.0f,
                                creditor_past_due = "0",
                                cr_flag = "y",
                                client_number = clientNumber,
                                creditor_name = creditor_name,
                                creditor_bal = creditor_bal,
                                creditor_payments = creditor_payments,
                                creditor_jointacct = creditor_jointacct,
                                CrAcntNoEnc = data_context.EncryptText(CrAcntNoEnc),
                                PreAcntNoEnc = data_context.EncryptText(PreAcntNoEnc),
                            };

                            data_context.creditordetails.InsertOnSubmit(creditor_detail);
                            data_context.SubmitChanges();
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        //UvFile.DeleteRecord(string.Format("{0}*{1}", final_id, i));
                        using (var dc = new DebtplusDataContext())
                        {
                            var finalID = string.Format("{0}*{1}", final_id, i);
                            var rec = dc.CreditorAccountQueues.Where(ca => ca.FinalID == finalID).FirstOrDefault();

                            if (rec != null)
                            {
                                dc.CreditorAccountQueues.DeleteOnSubmit(rec);
                                dc.SubmitChanges();
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    i++;
                    try
                    {
                        //UvRec = UvFile.Read(string.Format("{0}*{1}", final_id, i));
                        var finalID = string.Format("{0}*{1}", final_id, i);
                        using (var dc = new DebtplusDataContext())
                        {
                            creditorAccount = dc.CreditorAccountQueues.Where(ca => ca.FinalID == finalID).FirstOrDefault();
                        }
                    }
                    catch (Exception)
                    {
                        done = true;
                    }
                }

                if (clear_credit_report_pull)
                {
                    using (CredabilityDataContext data_context = new CredabilityDataContext())
                    {
                        var contact_detail = from c in data_context.contactdetails
                                             where (c.client_number == clientNumber)
                                             select c;

                        foreach (var c in contact_detail)
                        {
                            c.cred_rpt_pull = null;
                        }

                        data_context.SubmitChanges();
                    }
                }
                result.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public CreditScoreData GetCredScore(int clientNumber)
        {
            // TODO:  Discuss the logic to get the credit score for the user
            var result = new CreditScoreData()
            {
                PrimaryScore = "Test",
                SecondaryScore = "Test",
                IsSuccessful = true
            };
            return result;
        }


        public int GetNextSeqNo(int clientNumber)
        {
            int sequenceNumber = 0;

            String recID = "IN" + clientNumber.ToString();

            try
            {
                int clNumber = 0;
                // TODO: Call function to convert IN number to ClientNumber
                //int clNumber = GetClientNumber();

                if (clNumber != 0)
                {
                    using (var dc = new DebtplusDataContext())
                    {
                        var record = dc.WEB_BRCAMs.Where(cam => cam.ClientNumber == clNumber.ToString()).FirstOrDefault();

                        if (record != null)
                        {
                            if (record.SeqNo.HasValue && record.SeqNo > 0)
                            {
                                sequenceNumber = (int)record.SeqNo;
                            }
                        }
                        else
                        {
                            sequenceNumber = 0;
                        }
                    }
                }
                else
                {
                    sequenceNumber = 1;
                }
            }
            catch (Exception ex)
            {
                sequenceNumber = 1;
            }

            return sequenceNumber;
        }

        public DateTime GetUvDts()
        {
            return DateTime.Now;
        }

        public object HaveCredRpt(int clientNumber)
        {
            throw new NotImplementedException();
        }

        public object LoadDmp(int clientNumber)
        {
            throw new NotImplementedException();
        }



        public bool SaveTrans(int clientNumber, int vanTranRef, string invoiceNumber, float charAmount)
        {
            String recID = "IN" + clientNumber.ToString();
            bool result = false;
            try
            {
                int clNumber = 0;
                DateTime uvDts = GetUvDts();
                // TODO: Call function to convert IN number to ClientNumber
                //int clNumber = GetClientNumber();

                if (clNumber != 0)
                {
                    using (var dc = new DebtplusDataContext())
                    {
                        var record = dc.WEB_BRCAMs.Where(cam => cam.ClientNumber == clNumber.ToString()).FirstOrDefault();

                        if (record != null)
                        {
                            try
                            {
                                int sequenceNumber = Convert.ToInt32(invoiceNumber.Substring(invoiceNumber.LastIndexOf('-') + 1));
                                record.InvoiceNumber = invoiceNumber;
                                record.IPostDate = uvDts; // TODO: Confirm if 213 is IPostDate
                                record.SeqNo = sequenceNumber;

                                var transRecord = new WEB_TRAN();
                                transRecord.BillDate = uvDts;
                                transRecord.PayType = "D";
                                transRecord.BillAmount = Convert.ToDecimal(charAmount);
                                transRecord.ClientNo = clNumber.ToString();
                                transRecord.PaymentRefNo = vanTranRef.ToString();
                                transRecord.TransType = "PMT";
                                transRecord.InvoiceNo = invoiceNumber;
                                dc.WEB_TRANs.InsertOnSubmit(transRecord);
                                dc.SubmitChanges();
                                result = true;
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        dc.SubmitChanges();
                    }
                }
                else
                {
                    using (var dc = new DebtplusDataContext())
                    {
                        var transRecord = new WEB_TEMP_TRAN();
                        transRecord.BillDate = uvDts;
                        transRecord.PayType = "D";
                        transRecord.BillAmount = Convert.ToDecimal(charAmount);
                        transRecord.ClientNo = clNumber.ToString();
                        transRecord.PaymentRefNo = vanTranRef.ToString();
                        transRecord.TransType = "PMT";
                        transRecord.InvoiceNo = invoiceNumber;
                        dc.WEB_TEMP_TRANs.InsertOnSubmit(transRecord);
                        dc.SubmitChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public object UpdateOCS(int clientNumber, int stat)
        {
            throw new NotImplementedException();
        }

        public bool StageUvData(DebtplusPushData UvData)
        {
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    CreditorDetail objCreditor = new CreditorDetail();
                    int i = 0;
                    string RecId = "";
                    //START:Delete Exisiting Data
                    bool Done = false;
                    while (!Done)
                    {
                        RecId = String.Format("{0}*{1}", UvData.InNo, (++i).ToString());
                        var objCred = dc.CreditorDetails.Where(obj => obj.InNo == RecId).FirstOrDefault();
                        if (objCred != null)
                        {
                            objCreditor.InNo = RecId;
                            dc.CreditorDetails.DeleteOnSubmit(objCred);
                        }
                        else
                        {
                            Done = true;
                        }
                    }

                    //END:Delete Exisiting Data

                    //START:Inserting Data
                    i = 0;
                    CreditorDetail CreditorData = null;
                    foreach (var CurCred in UvData.Creditors)
                    {
                        CreditorData = new CreditorDetail();
                        //CreditorData.InNo = String.Format("{0}*{1}", UvData.InNo, (++i).ToString());
                        CreditorData.InNo = UvData.InNo.ToString();
                        CreditorData.Name = CurCred.Name;
                        CreditorData.Balance = CurCred.Balance;
                        CreditorData.IntRate = CurCred.IntRate;
                        CreditorData.Payment = CurCred.Payment;
                        CreditorData.PreAcctNo = CurCred.PreAcctNo;
                        CreditorData.PriAcctHolder = CurCred.PriAcctHolder;
                        CreditorData.PastDue = CurCred.PastDue;
                        CreditorData.CrAcctNo = CurCred.CrAcctNo;
                        dc.CreditorDetails.InsertOnSubmit(CreditorData);
                        dc.SubmitChanges();
                    }
                    //END:Inserting Data

                    //Get Contact Details
                    var contactObj = dc.ContactDetails.Where(obj => obj.InNo == UvData.InNo.ToString()).FirstOrDefault();
                    if (contactObj == null)
                    {
                        contactObj = new ContactDetail();
                    }

                    var contactObj2 = dc.ContactDetails2s.Where(obj => obj.InNo == UvData.InNo.ToString()).FirstOrDefault();

                    if (contactObj2 == null)
                    {
                        contactObj2 = new ContactDetails2();
                    }

                    contactObj.InNo = RecId;
                    contactObj.FNAME = UvData.PriFirstName;
                    contactObj.MINITIAL = UvData.PriMidName;
                    contactObj.LNAME = UvData.PriLastName;
                    contactObj.ADDRESS = UvData.Addr1;
                    contactObj.CITY = UvData.City;
                    contactObj.STATE = UvData.State;
                    contactObj.ZIP = UvData.Zip;
                    contactObj.CONF_MAIDEN = UvData.PriConfirmMaiden;
                    contactObj.SSN = UvData.PriSsn;
                    contactObj.DOB = UvData.PriDob;
                    contactObj.SEX = UvData.PriSex;
                    contactObj.MARITAL = UvData.PriMarital;
                    contactObj.PHONE = UvData.Phone1;
                    contactObj.EMAIL = UvData.eMail;
                    contactObj.CO_FNAME = UvData.SecFirstName;
                    contactObj.CO_MINITIAL = UvData.SecMidName;
                    contactObj.CO_LNAME = UvData.SecLastName;
                    contactObj.CO_SSN = UvData.SecSsn;
                    contactObj.CO_DOB = UvData.SecDob;
                    contactObj.CO_SEX = UvData.SecSex;
                    contactObj.CO_HISPANIC = UvData.SecHispanic;
                    contactObj.CO_RACE = UvData.SecRace;
                    contactObj.MORT_CURRENT = UvData.MortCurrent;
                    contactObj.MORT_INVESTOR = UvData.MortgageInvestor;
                    contactObj.VAL_HOME = UvData.ValHome;
                    contactObj.VAL_CAR = UvData.ValCar;
                    contactObj.OWE_CAR = UvData.OweCar;
                    contactObj.OWE_OTHER = UvData.OweOther;
                    contactObj.RENT_MORT = UvData.RentMort;
                    contactObj.EQUITY_LOAN_TAX_INS = UvData.EquityLoanTaxIns;
                    contactObj.HOME_MAINTENANCE = UvData.HomeMaintenance;
                    contactObj.INSURANCE = UvData.MedicalInsurance;
                    contactObj.VEHICLE_PAYMENTS = UvData.CardPayments;//Not assigning any value in Credibitily.UvPush.cs
                    contactObj.CAR_INSURANCE = UvData.CarInsurance;
                    contactObj.UTILITIES = UvData.Utilities;
                    contactObj.TELEPHONE = UvData.Telephone;
                    contactObj.GROCERIES = UvData.Groceries;
                    contactObj.FOOD_AWAY = UvData.FoodAway;
                    contactObj.CAR_MAINTENANCE = UvData.CarMaintenance;
                    contactObj.PUBLIC_TRANSPORTATION = UvData.PublicTransportation;
                    contactObj.MEDICAL_PRESCRIPTION = UvData.MedicalPrescription;
                    contactObj.CHILD_SUPPORT = UvData.ChildSupportAlimony;
                    contactObj.CHILD_ELDER_CARE = UvData.ChildElderCare;
                    contactObj.MISC_EXP = UvData.Miscellaneous;
                    contactObj.EDUCATION = UvData.Education;
                    contactObj.LAUNDRY = UvData.Laundry;
                    contactObj.CLOTHING = UvData.Clothing;
                    contactObj.BEAUTY_BARBER = UvData.BeautyBarber;
                    contactObj.PERSONAL_EXP = UvData.PersonalExpenses;
                    contactObj.CLUB_DUES = UvData.ClubDues;
                    contactObj.CONTRIBUTIONS = UvData.Contributions;
                    contactObj.RECREATION = UvData.Recreation;
                    contactObj.GIFTS = UvData.Gifts;
                    contactObj.SAVINGS = UvData.ValSavings;
                    var Income = UvData.GrossIncome + UvData.PartTimeGrossIncome + UvData.AlimonyIncome;
                    Income += UvData.ChildSupportIncome + UvData.GovtAssistIncome + UvData.OtherIncome;
                    contactObj.MONTHLY_GROSS_INCOME = Income;
                    Income = UvData.NetIncome + UvData.PartTimeNetIncome;
                    contactObj.MONTHLY_NET_INCOME = Income;
                    contactObj.SIZE_OF_HOUSEHOLD = UvData.SizeOfHousehold;
                    contactObj.VAL_OTHER = UvData.ValOther;
                    contactObj.OWE_HOME = UvData.OweHome;
                    contactObj.DMP_FCO_PCP_FLAG = UvData.DmpFcoPcpFlag;
                    contactObj.DMP_FCO_PCP_DATE = UvData.DmpFcoPcpDate;
                    contactObj.CONTACT_REASON = UvData.ContactReason;
                    contactObj.CONTACT_HISPANIC = UvData.PriHispanic;
                    contactObj.CONTACT_RACE = UvData.PriRace;
                    contactObj.BANKRUPT = UvData.Bankrupt;
                    contactObj.MORT_RATE = UvData.MortIntrestRate;
                    var HousingType = UvData.HousingType;
                    contactObj.CLIENT_TYPE = UvData.ClientType;
                    if (UvData.ClientType == "PrP")
                        HousingType = "B";
                    contactObj.HOUSING_TYPE = HousingType;
                    if (UvData.ClientType == "BK")
                    {
                        //I HAVE A DOUBT HERE--  UvRec.Replace(69, "y");//
                    }

                    contactObj.CRED_RPT_PULL = UvData.CredRptPull;
                    contactObj.CRED_RPT_BCITY = UvData.PriBirthCity;
                    contactObj.FIRM_ID = UvData.FirmCode;
                    contactObj.FIRM_NAME = UvData.FirmName;
                    contactObj.ATTY_NAME = UvData.AttyName;
                    contactObj.ABANUMBER = UvData.BankAbaNo;
                    contactObj.ACCTNUMBER = UvData.BankAcctNo;
                    contactObj.REAL_EMAIL = UvData.eMail;
                    contactObj.MONTHLY_PARTTIME_INCOME = UvData.PartTimeGrossIncome;
                    contactObj.MONTHLY_ALIMONY_INCOME = UvData.AlimonyIncome;
                    contactObj.MONTHLY_CHILDSUPPORT_INCOME = UvData.ChildSupportIncome;
                    contactObj.MONTHLY_GOVTASSIST_INCOME = UvData.GovtAssistIncome;
                    contactObj.CONTACT_COMMENTS = UvData.ContactComments;
                    contactObj.CREDITOR_COMMENTS = UvData.CreditorComments;
                    contactObj.PAYTYPE = UvData.PaymentType;
                    contactObj.DC_FNAMEONCARD = UvData.DebitCardFirstName;
                    contactObj.DC_MNAMEONCARD = UvData.DebitCardMidName;
                    contactObj.DC_LNAMEONCARD = UvData.DebitCardLastName;
                    contactObj.DC_BILLADDR = UvData.DebitCardBillAddr1;
                    contactObj.DC_BILLCITY = UvData.DebitCardBillCity;
                    contactObj.DC_BILLSTATE = UvData.DebitCardBillState;
                    contactObj.DC_BILLZIP = UvData.DebitCardBillZip;
                    contactObj.DC_ACCTNUM = UvData.DebitCardAcctNo;
                    contactObj.DC_EXPDATE = UvData.DebitCardExpDate;
                    contactObj.MTCN = UvData.MTCN;
                    contactObj.MORT_HOLDER = UvData.MortHolder;
                    contactObj.LOAN_NUMBER = UvData.MortLoanNo;
                    contactObj.MORT_TYPE = UvData.MortType;
                    contactObj.RATE_TYPE = UvData.MortRateType;
                    contactObj.MORT_DATE = UvData.MortDate;
                    if (!string.IsNullOrEmpty(UvData.MortLastContactDate))
                    {
                        contactObj.MORT_LAST_CONTACT_DATE = Convert.ToDateTime(UvData.MortLastContactDate);
                    }
                    contactObj.MORT_INVESTOR = UvData.MortgageInvestor;
                    contactObj.ORIG_BAL = UvData.MortOrignalBal;
                    contactObj.MOS_DELINQ = UvData.MosDelinq;
                    contactObj.AMT_AVAIL = UvData.MortAmtAvail;
                    contactObj.REPAY_PLAN = UvData.RepayPlan;
                    contactObj.FORECLOSURE_DATE = UvData.ForeclosureDate;
                    contactObj.LAST_CONTACT_DATE = UvData.MortLastContactDate;
                    contactObj.SECONDARY_HOLDER = UvData.MortSecHolder;
                    contactObj.SECONDARY_AMT = UvData.MortSecAmt;
                    contactObj.SECONDARY_STATUS = UvData.MortSecStatus;
                    contactObj.FLAG = "Y";
                    contactObj.COUNSELOR = "intcouns";
                    contactObj.CONFIRM_DATE = UvData.ConfirmDateTime;
                    contactObj.LAST_CONTACT_DESC = UvData.MortLastContactDesc;
                    contactObj.MORT_YEARS = UvData.MortYears;
                    if (UvData.SpanishFlag.ToUpper() == "Y")
                        contactObj.LANGUAGEs = "S";
                    contactObj.REFCODE = UvData.RefCode;
                    contactObj.ATTY_EMAIL = UvData.FirmEmail;
                    contactObj.CONTACT_ADDRESS2 = UvData.Addr2;
                    contactObj.CLIENT_LOGIN = UvData.UserName;
                    contactObj.PUSH_DATETIME = DateTime.Now;
                    if ((UvData.AttySit == "NoAt" || UvData.AttySit == "Self") && UvData.BsrpStAlowed)
                        contactObj.SENDREF = "Y";
                    if (UvData.DebitCardTransDTS != null)
                        contactObj.PAYDATE = UvData.DebitCardTransDTS;
                    contactObj.WEB_LOGIN = UvData.UserName;
                    contactObj.CONFIRMATION_NO = UvData.DebitCardVanTransRef;
                    contactObj.CO_COUNSEL = UvData.CoCounsPres;
                    contactObj.PRI_COUNSEL = UvData.PriorCouns;
                    contactObj.PRIOR_DMP = UvData.PriorDMP;
                    contactObj.RECENT_DMP = UvData.RecentDMP;
                    contactObj.EDUCATION_LEVEL = UvData.PriEducation;
                    contactObj.CL_EMPLOYED = UvData.PriEmployed;
                    contactObj.SP_EMPLOYED = UvData.SecEmployed;
                    contactObj.CONTACT_REASON2 = UvData.ContactReason2;
                    contactObj.MED_EXP_PROB = UvData.MedExp;
                    contactObj.SO_LOSS_PROB = UvData.LostSO;
                    contactObj.HEALTH_PROB = UvData.HealthWage;
                    contactObj.UNEMPLOY_PROB = UvData.Unemployed;
                    contactObj.TAX_PROB = UvData.TaxLien;
                    contactObj.INTRATE_PROB = UvData.MortRate;
                    contactObj.CCRC_FLAG = UvData.CcrcAprov;
                    if (UvData.SignUp == 1)
                        contactObj.OPT_STAT = "A";
                    if (UvData.SignUpDTS != null)
                        contactObj.OPT_DATE = UvData.SignUpDTS;
                    if (UvData.JointFile == 1)
                    {
                        contactObj.STATE = "J";
                        if (UvData.GotSec1040DTS != null)
                            contactObj.SP_FORM1 = UvData.GotSec1040DTS;
                        if (UvData.GotSecPayStubDTS != null)
                            contactObj.SP_FORM2 = UvData.GotSecPayStubDTS;
                        if (UvData.GotScheduleIDTS != null)
                            contactObj.SP_FORM3 = UvData.GotScheduleIDTS;
                        if (UvData.GotSsdiDTS != null)
                            contactObj.SP_FORM4 = UvData.GotSsdiDTS;
                        if (UvData.GotProBonoDTS != null)
                            contactObj.CL_FORM5 = UvData.GotProBonoDTS;
                        if (UvData.FeePolicyDTS != null)
                        {
                            contactObj.ACK1_DATE = UvData.FeePolicyDTS;
                        }
                    }

                    contactObj.TYPE = UvData.WaiverType;
                    if (UvData.Aproved == 1)
                        contactObj.APPR_STAT = "E";
                    else
                        contactObj.APPR_STAT = "N";
                    if (UvData.DecisionDTS != null)
                        contactObj.APPR_DATE = UvData.DecisionDTS;
                    if (UvData.GotPri1040DTS != null)
                        contactObj.CL_FORM1 = UvData.GotPri1040DTS;
                    if (UvData.GotPriPayStubDTS != null)
                        contactObj.CL_FORM2 = UvData.GotPriPayStubDTS;
                    if (UvData.GotScheduleIDTS != null)
                        contactObj.CL_FORM3 = UvData.GotScheduleIDTS;
                    if (UvData.GotSsdiDTS != null)
                        contactObj.CL_FORM4 = UvData.GotSsdiDTS;
                    if (UvData.GotProBonoDTS != null)
                        contactObj.CL_FORM5 = UvData.GotProBonoDTS;
                    if (UvData.FeePolicyDTS != null)
                    {
                        contactObj.ACK1_DATE = UvData.FeePolicyDTS;
                    }
                    contactObj.CCMIN_PMT = UvData.CardPayments;
                    contactObj.INSTALL_AMT = UvData.OtherLoans;
                    contactObj.PROP4SALE = UvData.Prop4Sale;
                    contactObj._4CLOSEIND = UvData.Note4Close;
                    contactObj.REFSOURCE = "08";
                    string Contat30 = "N";
                    DateTime ContactDate;
                    if (DateTime.TryParse(UvData.MortLastContactDate, out ContactDate))
                        if (DateTime.Compare(UvData.ConfirmDateTime, ContactDate) <= 30)
                            Contat30 = "Y";
                    contactObj._30DAYCONTACT = Contat30;
                    contactObj.PROPSTATUS = UvData.WhoInHouse;
                    contactObj.LOAN_NUMBER2 = UvData.MortSecLoanNo;
                    contactObj.RET_AMT = UvData.ValRetirement;
                    contactObj.PERSPROP_AMT = UvData.ValProperty;
                    if (UvData.BkForPrv == 1)
                        contactObj.BKFOR_PRV = "Y";
                    else
                        contactObj.BKFOR_PRV = "N";
                    if (UvData.HpfID != null)
                        contactObj.FCID = UvData.HpfID;
                    contactObj.MONTHLY_PARTTIME_NET = UvData.PartTimeNetIncome;
                    contactObj.UTRASH = UvData.UtlTrash;
                    contactObj.UTV = UvData.UtlTv;
                    contactObj.UGAS = UvData.UtlGas;
                    contactObj.UPHONE = UvData.Telephone;
                    contactObj.UWATER = UvData.UtlWater;
                    contactObj.UELECT = UvData.UtlElectric;
                    contactObj.PROP_DUES = UvData.MoFee;
                    contactObj.PMI_INC = UvData.IncludeFha;
                    contactObj.PROP_TAXES = UvData.AnPropTax;
                    contactObj.TAX_INC = UvData.IncludeTax;
                    contactObj.PROP_INSURE = UvData.AnPropIns;
                    contactObj.INS_INC = UvData.IncludeIns;
                    contactObj.ORIG_MRATE = UvData.StartIntRate;
                    contactObj.DP_ASSIST = UvData.AstPay;
                    contactObj.EQLOAN_AMT = UvData.MoEquity;
                    contactObj._2ND_AMT = UvData.Mo2ndMort;
                    contactObj.HAMP_HARP = UvData.MakeHomeAff;

                    if (UvData.ConThem.ToLower() == "true")
                    {
                        int Days;
                        if (int.TryParse(UvData.ConDay, out Days))
                        {
                            string ConMe = "Please Contact in the {0} on {1} at {2}";
                            contactObj.CONTACT_ME = String.Format(ConMe, UvData.ConTime, UvData.ConfirmDateTime.AddDays(Days), UvData.ConInfo);
                        }
                    }

                    contactObj2.CLIENT__TYPE = UvData.ClientType;
                    if (UvData.ClientType == "PrP")
                    {
                        contactObj2.NEWUTLTRASH = UvData.NewUtlTrash;
                        contactObj2.NEWUTLTV = UvData.NewUtlTv;
                        contactObj2.NEWUTLGAS = UvData.NewUtlGas;
                        contactObj2.NEWUTLWATER = UvData.NewUtlWater;
                        contactObj2.NEWUTLELECTRIC = UvData.NewUtlElectric;
                        contactObj2.NEWMOFEE = UvData.NewMoFee;
                        contactObj2.NEWMOPROPINS = UvData.NewMoPropIns;
                        contactObj2.NEWMOPROPTAX = UvData.NewMoPropTax;
                        contactObj2.NEWMO2NDMORT = UvData.NewMo2ndMort;
                        contactObj2.NEWMOEQUITY = UvData.NewMoEquity;
                        contactObj2.NEWCARDPAYMENTS = UvData.NewOtherLoans;
                        contactObj2.NEWMISCEXP = UvData.NewMiscExp;
                        contactObj2.NEWGIFTS = UvData.NewGifts;
                        contactObj2.NEWCONTRIBUTIONS = UvData.NewContributions;
                        contactObj2.NEWPUBLICTRANS = UvData.NewPublicTrans;
                        contactObj2.NEWCLUBDUES = UvData.NewClubDues;
                        contactObj2.NEWCARMAINT = UvData.NewCarMaint;
                        contactObj2.NEWCARINS = UvData.NewCarIns;
                        contactObj2.NEWVEHICLEPAY = UvData.NewVehiclePay;
                        contactObj2.NEWBEAUTY = UvData.NewBeauty;
                        contactObj2.NEWPERDONALEXP = UvData.PersonalExpenses;
                        contactObj2.NEWELDERCARE = UvData.NewElderCare;
                        contactObj2.NEWCLOTHING = UvData.NewClothing;
                        contactObj2.NEWLAUNDRY = UvData.NewLaundry;
                        contactObj2.NEWCHILDSUPPORT = UvData.NewChildSupport;
                        contactObj2.NEWEDU = UvData.NewEdu;
                        contactObj2.NEWMED = UvData.NewMed;
                        contactObj2.NEWRECREATION = UvData.NewRecreation;
                        contactObj2.NEWINSURANCE = UvData.NewInsurance;
                        contactObj2.NEWFOODAWAY = UvData.NewFoodAway;
                        contactObj2.NEWGROCERIES = UvData.NewGroceries;
                        contactObj2.NEWPHONE = UvData.NewPhone;
                        contactObj2.NEWMAINT = UvData.NewMaint;
                        contactObj2.NEWMORT = UvData.NewMort;
                        contactObj2.HOUSEADR = UvData.HouseAdr;
                        contactObj2.HOUSECITY = UvData.HouseCity;
                        contactObj2.HOUSEST = UvData.HouseSt;
                        contactObj2.HOUSEZIP = UvData.HouseZip;
                        contactObj2.HOUSEPRICE = UvData.HousePrice;
                        contactObj2.FNDHOUSE = UvData.FndHouse;
                        contactObj2.HADHOUSE = UvData.HadHouse;
                        if (UvData.TheCloDate != null && UvData.HaveCloDate)
                        {
                            contactObj2.THECLODATE = UvData.TheCloDate;
                            contactObj2.HAVECLODATE = "Y";

                        }
                        else
                        {
                            contactObj2.HAVECLODATE = "N";
                        }
                        if (UvData.BenAprov)
                            contactObj2.BENAPROV = "Y";
                        else
                            contactObj2.BENAPROV = "N";
                        if (UvData.AstPay)
                            contactObj2.ASTPAY = "Y";
                        else
                            contactObj2.ASTPAY = "N";
                        if (UvData.BegPro)
                            contactObj2.BEGPRO = "Y";
                        else
                            contactObj2.BEGPRO = "Y";
                        contactObj2.REFEMAIL = UvData.RefEmail;
                        contactObj2.REFNAME = UvData.RefName;
                        contactObj2.REFINST = UvData.RefInst;
                        contactObj2.MOPROPINS = UvData.MoPropIns;
                        contactObj2.MOPROPTAX = UvData.MoPropTax;
                    }
                    contactObj2.ACCTID = Convert.ToInt32(UvData.IdmAccntNo);
                    contactObj2.CL_CERTNO = UvData.PriCertNo;
                    contactObj2.SP_CERTNO = UvData.SecCertNo;
                    contactObj2.MONTHLY_NET_INCOME2 = UvData.NetIncome2;
                    contactObj2.MONTHLY_GROSS_INCOME2 = UvData.GrossIncome2;
                    contactObj2.MONTHLY_GROSS_INCOME2 = UvData.GrossIncome2;
                    contactObj2.MONTHLY_PARTTIME_INCOME2 = UvData.PartTimeGrossIncome2;
                    contactObj2.MONTHLY_ALIMONY_INCOME2 = UvData.AlimonyIncome2;
                    contactObj2.MONTHLY_CHILDSUPPORT_INCOME2 = UvData.ChildSupportIncome2;
                    contactObj2.MONTHLY_GOVTASSIST_INCOME2 = UvData.GovtAssistIncome2;
                    contactObj2.ASSISTANCE = UvData.AssistModify;
                    contactObj2.GUARANTEE = UvData.LoanModify;
                    contactObj2.BKCOUNSELING_BILLAMOUNT = UvData.BKCounseling_BillAmount;
                    contactObj2.MortgageModifiedHamp = UvData.MortgageModifiedHamp;
                    contactObj2.MortgageInvestor = UvData.MortgageInvestor;
                    contactObj2.IsBusinessOrPersonalMortgage = UvData.IsBusinessOrPersonalMortgage;
                    contactObj2.HouseType = UvData.HouseType;
                    contactObj2.AgreeEscrowAccount = UvData.AgreeEscrowAccount;
                    contactObj2.ConvictedFelony = UvData.ConvictedFelony;
                    contactObj2.IsLoanFirstLienFMorFMae = UvData.IsLoanFirstLienFMorFMae;
                    contactObj2.LoanSoldFMorFmae = UvData.LoanSoldFMorFmae;
                    contactObj2.LoanRefinanceProgram = UvData.LoanRefinanceProgram;
                    contactObj2.AnyLiens = UvData.AnyLiens;
                    contactObj2.ActiveBankruptcy = UvData.ActiveBankruptcy;

                    if (UvData.CounselorId.IsNullOrWhiteSpace())
                        UvData.CounselorId = "IA";
                    contactObj2.COUNSELOR_ID = UvData.CounselorId;



                    // Insert the object if it is not created already
                    if (contactObj.ID == 0)
                    {
                        dc.ContactDetails.InsertOnSubmit(contactObj);
                    }
                    if (contactObj2.ID == 0)
                    {
                        dc.ContactDetails2s.InsertOnSubmit(contactObj2);
                    }

                    dc.SubmitChanges();

                    var fees = Convert.ToDecimal(GetCharAmt(UvData.FirmCode, UvData.Zip, "B"));

                    PaymentType paymentType = PaymentType.None;

                    FeeWaiverType feeWaiverType = FeeWaiverType.None;

                    bool isLegalAid = false;

                    var vendor = dc.vendors.Where(v => v.oID == Convert.ToInt32(UvData.FirmCode)).FirstOrDefault();

                    if (vendor != null)
                    {
                        isLegalAid = vendor.IsLegalAid.HasValue ? (bool)vendor.IsLegalAid : false;
                    }

                    ProcessPaymentAndFeeWaiverType(UvData.DebitCardAcctNo, UvData.DebitCardExpDate, UvData.BankAcctNo, UvData.BankAbaNo, UvData.WaiverType, isLegalAid, UvData.Escrow, UvData.EscrowProbonoValue, out paymentType, out feeWaiverType);

                    dc.create_bk_client(contactObj.ID, contactObj2.ID, UvData.InNo.ToString(), fees, UvData.PriCertNo, (int)paymentType, (int)feeWaiverType, UvData.CounselorEmail);
                }
            }
            catch (Exception ex)
            {
                logger.Debug(() => "Error Occured in StageUvData. Message: " + ex.Message);
                logger.Debug(() => "Stack Trace: " + ex.StackTrace.ToString());

                if (ex.InnerException != null)
                {
                    var iex = ex.InnerException;
                    logger.Debug(() => "Error Occured in StageUvData. Inner Exception Message: " + iex.Message);
                    logger.Debug(() => "Stack Trace: " + iex.StackTrace.ToString());
                }
            }
            return true;
        }
    }
}
