﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Cccs.Debtplus.Dal;
using System.Configuration;
using Cccs.Debtplus.Data;
using Cccs.Identity;
using Cccs.Credability.Dal;
using Cccs.DebtPlus;

namespace Cccs.Debtplus.Impl
{
    public partial class Debtplus : IDebtplus
    {
        public DebtPlus.JudicialDistrict GetJudicialDistrict(string zipCode)
        {
            DebtPlus.JudicialDistrict clientDistrict = new DebtPlus.JudicialDistrict();

            using (var dc = new DebtplusDataContext())
            {
                var districtDetails = dc.DistrictDetails.Where(obj => obj.ZipCode == zipCode).FirstOrDefault();
                if (districtDetails != null && !string.IsNullOrEmpty(districtDetails.DistrictName))
                {
                    clientDistrict.DistrictName = districtDetails.DistrictName;

                    var judicialDistricts = dc.JudicialDistricts.Where(obj => obj.DistrictName == clientDistrict.DistrictName).FirstOrDefault();

                    if (judicialDistricts != null && !string.IsNullOrEmpty(judicialDistricts.DistrictName))
                    {
                        clientDistrict.DistrictCode = judicialDistricts.DistrictCode;
                    }
                }
            }
            return clientDistrict;
        }

        public string GetEoustJudicialDistrictByName(string districtName)
        {
            string disName = "";
            using (var dc = new DebtplusDataContext())
            {
                var judicialDistricts = dc.JudicialDistricts.Where(obj => obj.DistrictName == districtName).FirstOrDefault();
                if (judicialDistricts != null && !string.IsNullOrEmpty(judicialDistricts.DistrictName))
                {
                    disName = judicialDistricts.DistrictCode;
                }
            }
            return disName;
        }

        public EoustLoginData GetEoustLogin()
        {
            using (var dc = new DebtplusDataContext())
            {
                var record = dc.EoustLogins.Where(e => e.Username == eoustUser).FirstOrDefault();

                if (record == null)
                {
                    return null;
                }
                else
                {
                    var loginData = new EoustLoginData()
                    {
                        EoustID = record.EoustID,
                        Username = record.Username,
                        Password = record.Password
                    };
                    return loginData;
                }
            }
        }

        public string GetFirmEmail(string firmID)
        {
            string email = null;
            using (var dc = new DebtplusDataContext())
            {
                var vendorContacts = (from v in dc.vendors
                              join vc in dc.vendor_contacts on v.oID equals vc.vendor
                              where v.oID == Convert.ToInt32(firmID)
                              select vc).ToList();

                // There can be multiple vendor contacts
                // select one 
                foreach (var item in vendorContacts)
                {
                    if (item.emailID.HasValue) {
                        var emailAddress = dc.EmailAddresses.Where(e => e.ID == item.emailID).FirstOrDefault();

                        if (emailAddress != null)
                        {
                            email = emailAddress.Address;
                            return email;
                        }
                    }
                }
            }

            return email;
        }

        public bool SetCertNo(CertReIssue reIssueData)
        {
            bool back = false;

            using (var dc = new DebtplusDataContext())
            {
                var record = dc.WEB_BRCAMs.Where(b => b.ClientNumber == reIssueData.ClNo).FirstOrDefault();

                if (record != null)
                {
                    record.Cert_No = reIssueData.CertNo;
                    record.Cert_Iss_Date = reIssueData.CertIssue;
                    record.IsPrimary = reIssueData.IsPrimary;

                    dc.SubmitChanges();
                    back = true;
                }
            }

            return back;
        }
    }
}