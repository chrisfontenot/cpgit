﻿using Cccs.Debtplus.Dal;
using Cccs.Debtplus.Data;
using Cccs.Debtplus.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Impl
{
    public partial class Debtplus : IDebtplus
    {
        public MimClientLoginData Mim_LoginClient(string RegNum, string websiteCode)
        {
            var loginData = new MimClientLoginData();

            loginData.Escrow = "N";
            loginData.Escrow6 = "N";
            loginData.ReDirTarget = "introNew.aspx";

            // TODO : Check for the implemenration of GET.CLNO.SUB

            using (var dc = new DebtplusDataContext())
            {
                var record = dc.WEB_WSCAMs.Where(ws => ws.RegNum == "WS" + RegNum).FirstOrDefault();

                if (record != null)
                {
                    loginData.ClId = record.ClientNumber.ToString();
                    loginData.UvStat = record.Status;
                    loginData.ContactSsn = record.SSN;
                    loginData.FirmId = record.FirmCode;
                    loginData.ZipCode = record.Zip;
                    loginData.LoginMimOk = true;
                    loginData.ReDirTarget = "AprWait.aspx";

                    if (String.IsNullOrEmpty(record.LName) || String.IsNullOrEmpty(record.FName))
                    {
                        loginData.ReDirTarget = "ProvideYourPersonalInformation.aspx";
                    }

                    try
                    {
                        var xRef = dc.WEB_CASEXREFs.Where(x => x.PrimarySSN == record.SSN).FirstOrDefault();

                        if (xRef != null)
                        {
                            loginData.JointSsn = xRef.Secondary;
                            loginData.SecondaryFlag = xRef.JointSSN;

                            if (!string.IsNullOrEmpty(xRef.Secondary))
                            {
                                var jointRec = dc.WEB_WSCAMs.Where(x => x.SSN == xRef.Secondary).FirstOrDefault();

                                if (jointRec != null)
                                {
                                    loginData.SpUvId = jointRec.ClientNumber.ToString();
                                }
                            }
                        }
                        else
                        {
                            loginData.ReDirTarget = "GetJointSsn.aspx";
                        }
                    }
                    catch (Exception ex)
                    {
                        loginData.ReDirTarget = "GetJointSsn.aspx";
                    }

                    try
                    {
                        var result = GetEscrow(Convert.ToInt32(loginData.FirmId), websiteCode);

                        if (result != null)
                        {
                            loginData.Escrow = "Y";

                            if (result.IsLegalAid)
                            {
                                loginData.Escrow6 = "Y";
                            }
                        }
                        else
                        {
                            loginData.Escrow = "N";
                            loginData.Escrow6 = "N";
                        }

                        /*switch (result.Escrow)
                        {
                            case 1:
                                loginData.Escrow = "Y";
                                break;
                            case 3:
                                loginData.Escrow = "Y";
                                break;
                            case 6:
                                loginData.Escrow = "Y";
                                loginData.Escrow6 = "Y";
                                break;
                        }*/
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            return loginData;
        }

        public MimCreateLoginValidateData Mim_CreateLogin_ValidateData(string ssn)
        {
            var result = new MimCreateLoginValidateData();

            using (var dc = new DebtplusDataContext())
            {
                /* this login in Host is implemented using the Client Number and that is never executed asthe Clno returned is always blank
                 instead check for the ssn number directly on the CaseXref table*/
                //var record = dc.WEB_WSCAMs.Where(ws => ws.SSN == ssn).FirstOrDefault();

                //if (record != null)
                //{
                //    result.IsSsnAlreadyOnFile = (record.Smeth == "I");
                //}

                //if (!result.IsSsnAlreadyOnFile)
                //{
                    result.IsValid = true;

                    var xRef = dc.WEB_CASEXREFs.Where(x => x.PrimarySSN == ssn).FirstOrDefault();

                    if (xRef != null)
                    {
                        result.JointSsn = xRef.Secondary;
                        result.SecondaryFlag = xRef.JointSSN;

                        if (!string.IsNullOrEmpty(xRef.Secondary))
                        {
                            var jointRec = dc.WEB_WSCAMs.Where(x => x.SSN == xRef.Secondary).FirstOrDefault();

                            if (jointRec != null)
                            {
                                result.SpUvId = jointRec.ClientNumber.ToString();
                            }
                        }
                    }
                    result.IsSuccessful = true;
                //}
                //else
                //{
                //    result.IsValid = true;
                //    result.IsSuccessful = true;
                //}
            }
            result.IsValid = true;
            result.IsSuccessful = true;

            return result;
        }

        public MimDisclosureLoadData Mim_Disclosure_LoadData(string clientNumber)
        {
            var result = new MimDisclosureLoadData();

            using (var dc = new DebtplusDataContext())
            {
                var record = dc.WEB_WSCAMs.Where(ws => ws.ClientNumber == clientNumber).FirstOrDefault();

                if (record != null)
                {
                    result.BankName = record.BankName;
                    result.NameOnCheck = record.NameOnCheck;
                    result.PaymentType = (record.PayMethod == "P") ? "C" : record.PayMethod;
                    result.AbaNumber = record.ABANumber;
                    result.AcctNumber = record.AcctNumber;
                    result.DcFNameOncard = record.DCFNameOnCard;
                    result.DcMNameOnCard = record.DCMNameOnCard;
                    result.DcLNameOnCard = record.DCLNameOnCard;
                    result.DcBillAddr1 = record.DCBillAddr;
                    result.DcBillAddr2 = record.DCBillAddr2;
                    result.DcBillCity = record.DCBillCity;
                    result.DcBillState = record.DCBillState;
                    result.DcBillZip = record.DCBillZip;
                    result.DcAcctNum = record.DCAcctNum;
                    result.CardExp = record.DCExpiryDate;
                    result.Mtcn = record.MTCN;
                    result.DataFromDb = true;
                    result.IsSuccessful = true;
                }
            }

            return result;
        }

        public DebtplusResult Mim_Disclosure_SaveTransaction(string clientNumber, string dc_fnameoncard, string dc_mnameoncard
            , string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state
            , string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no
            , string van_trans_ref, string char_amt)
        {
            var result = new DebtplusResult();

            try
            {

                using (var dc = new DebtplusDataContext())
                {
                    var record = dc.WEB_WSCAMs.Where(ws => ws.ClientNumber == clientNumber).FirstOrDefault();

                    if (record != null)
                    {
                        record.PayMethod = "D";
                        record.BillDate = DateTime.Today;
                        record.DCFNameOnCard = dc_fnameoncard;
                        record.DCMNameOnCard = dc_mnameoncard;
                        record.DCLNameOnCard = dc_lnameoncard;
                        record.DCBillAddr = dc_bill_addr1;
                        record.DCBillCity = dc_bill_city;
                        record.DCBillState = dc_bill_state;
                        record.DCBillZip = dc_bill_zip;
                        record.DCAcctNum = dc_acctnum;
                        record.DCExpiryDate = dc_exp_date;
                        record.AcctNumber = dc_acctnum;
                        record.LastTrans = cur_inv_no;
                        record.SeqNo = Convert.ToInt32(cur_seq_no);
                        record.BillAmt = Convert.ToDecimal(char_amt);
                    }

                    var ccpmt_log = dc.WEB_CCPMT_LOGs.Where(l => l.ClientNumber == Convert.ToInt32(clientNumber)).FirstOrDefault();

                    if (ccpmt_log != null)
                    {
                        ccpmt_log.BillAmount = Convert.ToDecimal(char_amt);
                        ccpmt_log.TransRef = String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim();
                        //ccpmt_log.PayType = UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D");
                    }
                    else
                    {
                        ccpmt_log = new WEB_CCPMT_LOG();
                        ccpmt_log.BillAmount = Convert.ToDecimal(char_amt);
                        ccpmt_log.TransRef = String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim();
                        //ccpmt_log.PayType = UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D");
                        dc.WEB_CCPMT_LOGs.InsertOnSubmit(ccpmt_log);
                    }

                    var wstrans = new WEB_WSTRAN();
                    wstrans.BillDate = DateTime.Today;
                    wstrans.PayType = "D";
                    wstrans.BillAmount = Convert.ToInt32(char_amt);
                    wstrans.ClientNumber = Convert.ToInt32(clientNumber);
                    wstrans.TransReference = String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim();
                    wstrans.TransType = "PMT";
                    wstrans.InvoiceNumber = cur_inv_no;

                    dc.WEB_WSTRANs.InsertOnSubmit(wstrans);

                    result.IsSuccessful = true;

                    dc.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public MimForm7LoadData Mim_Form7_LoadData(string clientNumber, string regNum)
        {
            var result = new MimForm7LoadData();

            try
            {

                using (var dc = new DebtplusDataContext())
                {
                    var record = dc.WEB_WSCAMs.Where(ws => ws.ClientNumber == clientNumber).FirstOrDefault();

                    if (record != null)
                    {
                        if (record.InNo == "WS" + clientNumber)
                        {
                            result.contact_firstname = record.FName;
                            result.contact_initial = record.MName;
                            result.contact_lastname = record.LName;
                            result.contact_address = record.Addr;
                            result.contact_address2 = record.Addr2;
                            result.contact_city = record.City;
                            result.contact_state = record.State;
                            result.contact_zip = record.Zip;
                            result.contact_email = record.Email;
                            result.casenumber = record.CaseNumber;
                            if (!string.IsNullOrEmpty(result.casenumber) && result.casenumber.Length > 3)
                            {
                                result.casenumber = String.Format("{0}-{1}", result.casenumber.Substring(0, 2), result.casenumber.Substring(2));
                            }
                            result.dayphone = record.DayPhone;
                            result.nitephone = record.NitePhone;
                            result.attyname = record.AttyName;// WsRec.Extract(233).ToString().Trim();
                            result.attyemail = record.NonEscrowEMA; // WsRec.Extract(254).ToString().Trim();
                            result.firmcode = record.FirmCode;
                            result.bankruptcy_type = record.BankruptcyType;

                            try
                            {
                                var wsdemog = dc.WEB_WSDEMOGs.Where(d => d.CLIENTNUMBER == Convert.ToInt32(clientNumber)).FirstOrDefault();

                                if (wsdemog != null)
                                {
                                    result.contact_marital = wsdemog.MARITAL;
                                    result.contact_sex = wsdemog.SEX;
                                    result.contact_race = wsdemog.RACE;
                                    result.contact_hispanic = wsdemog.HISP;
                                    result.numberinhouse = wsdemog.NUMINHOUSE.ToString();
                                    result.income_range = wsdemog.INCOME_RANGE;
                                }
                            }
                            catch (Exception ex)
                            {
                                result.contact_marital = string.Empty;
                                result.contact_sex = string.Empty;
                                result.contact_race = string.Empty;
                                result.contact_hispanic = string.Empty;
                                result.numberinhouse = "0";
                                result.income_range = string.Empty;
                            }
                            result.IsSuccessful = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public DebtplusResult Mim_Form7_SaveData(string clientNumber, string contact_lastname, string contact_firstname,
            string contact_initial, string contact_ssn, string contact_address, string contact_address2,
            string contact_city, string contact_state, string contact_zip, string contact_marital,
            string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity,
            string cemail, string dayphone, string nitephone, string attyname, string attyemail,
            string firm_id, string escrow, string secondary_flag, string char_type, string casenumber,
            string bankruptcy_type, string income_range, string number_in_house, string languageCode, string char_amt)
        {
            var result = new DebtplusResult();

            firm_id = string.IsNullOrEmpty(firm_id) ? "9999" : firm_id.Trim();
            var ssn = (!string.IsNullOrEmpty(contact_ssn)) ? contact_ssn.Replace("-", "").Trim() : "";
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var crq = dc.CreditReportQueues.Where(c => c.ClientNumber == clientNumber).FirstOrDefault();

                    if (crq != null)
                    {
                        crq.FirstName = contact_firstname;
                        crq.LastName = contact_lastname;
                        crq.SSN = ssn;
                        crq.Street = contact_address;
                        crq.City = contact_city;
                        crq.State = contact_state;
                        crq.Zipcode = contact_zip;
                        crq.BirthCity = credrpt_birthcity;
                    }
                    else
                    {
                        crq = new CreditReportQueue();
                        crq.ClientNumber = clientNumber;
                        crq.FirstName = contact_firstname;
                        crq.LastName = contact_lastname;
                        crq.SSN = ssn;
                        crq.Street = contact_address;
                        crq.City = contact_city;
                        crq.State = contact_state;
                        crq.Zipcode = contact_zip;
                        crq.BirthCity = credrpt_birthcity;
                        crq.IsPrimary = true;
                        dc.CreditReportQueues.InsertOnSubmit(crq);
                    }

                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    ws.FullName = string.Format("{0} {1} {2}", contact_firstname.Trim(), contact_initial.Trim(), contact_lastname.Trim());
                    ws.Addr = contact_address.Trim();
                    ws.Addr2 = contact_address2.Trim();
                    ws.CSZ = string.Format("{0} {1} {2}", contact_city.Trim(), contact_state.Trim(), contact_zip.Trim()).ToUpper();
                    ws.LFName = string.Format("{0} {1}", contact_lastname.Trim(), contact_firstname.Trim()).ToUpper();
                    ws.DayPhone = dayphone.Trim();
                    ws.NitePhone = nitephone.Trim();
                    ws.ApptDate = DateTime.Today;
                    ws.ApptType = "W";
                    ws.Status = "PE";
                    ws.ModDate = DateTime.Today;
                    ws.SSN = ssn;
                    ws.LName = contact_lastname.Trim().ToUpper();
                    ws.FName = contact_firstname.Trim().ToUpper();
                    ws.MName = contact_initial.Trim().ToUpper();
                    ws.City = contact_city.Trim().ToUpper();
                    ws.State = contact_state.Trim().ToUpper();
                    ws.Zip = contact_zip.Trim();
                    ws.Zip134 = contact_zip.Trim();
                    ws.Email = cemail.Trim();
                    ws.Smeth = "I";
                    ws.FirmCode = firm_id;
                    ws.CaseNumber = casenumber.Replace("-", "").Trim();
                    if ((escrow == "Y") && (secondary_flag != "S") && (char_type == "H"))
                    {
                        ws.PayType = "A";
                        ws.BillAmt = Convert.ToDecimal(char_amt);
                    }
                    ws.HType = "B";
                    ws.BankruptcyType = bankruptcy_type.Trim();

                    if (firm_id == "9999")
                    {
                        ws.AttyName = attyname.Trim();
                        ws.NonEscrowEMA = attyemail.Trim(); //	NON_ESCROW_EMAIL
                    }
                    else
                    {
                        ws.AttyName = string.Empty;
                        ws.NonEscrowEMA = string.Empty;
                    }

                    var wsdemog = dc.WEB_WSDEMOGs.Where(d => d.CLIENTNUMBER == Convert.ToInt32(clientNumber)).FirstOrDefault();

                    if (wsdemog != null)
                    {
                        wsdemog.MARITAL = contact_marital.Trim();
                        wsdemog.SEX = contact_sex.Trim();
                        wsdemog.RACE = contact_race.Trim();
                        wsdemog.LANGUAGE = languageCode;
                        wsdemog.HISP = contact_hispanic.Trim();
                        wsdemog.NUMINHOUSE = string.IsNullOrEmpty(number_in_house) ? 0 : Convert.ToInt32(number_in_house.Trim());
                        wsdemog.INCOME_RANGE = string.IsNullOrEmpty(income_range) ? "" : income_range.Trim();
                    }
                    else
                    {
                        wsdemog = new WEB_WSDEMOG();
                        wsdemog.CLIENTNUMBER = Convert.ToInt32(clientNumber);
                        wsdemog.MARITAL = contact_marital.Trim();
                        wsdemog.SEX = contact_sex.Trim();
                        wsdemog.RACE = contact_race.Trim();
                        wsdemog.LANGUAGE = languageCode;
                        wsdemog.HISP = contact_hispanic.Trim();
                        wsdemog.NUMINHOUSE = string.IsNullOrEmpty(number_in_house) ? 0 : Convert.ToInt32(number_in_house.Trim());
                        wsdemog.INCOME_RANGE = string.IsNullOrEmpty(income_range) ? "" : income_range.Trim();
                        dc.WEB_WSDEMOGs.InsertOnSubmit(wsdemog);
                    }
                    
                    var ssnXref = dc.WEB_APPT_SSN_XREFs.Where(x => x.SSN == ssn).FirstOrDefault();

                    if (ssnXref == null)
                    {
                        ssnXref = new WEB_APPT_SSN_XREF();
                        ssnXref.SSN = ssn;
                        ssnXref.ClientNumber = clientNumber;
                        dc.WEB_APPT_SSN_XREFs.InsertOnSubmit(ssnXref);
                    }

                    if (!string.IsNullOrEmpty(cemail))
                    {
                        var emailXref = dc.WEB_APPT_EMAIL_XREFs.Where(e => e.Email == cemail.Trim()).FirstOrDefault();

                        if (emailXref != null)
                        {
                            emailXref.ClientNumber = clientNumber.Trim();
                        }
                        else
                        {
                            emailXref = new WEB_APPT_EMAIL_XREF();
                            emailXref.ClientNumber = clientNumber.Trim();
                            emailXref.Email = cemail.Trim();
                            dc.WEB_APPT_EMAIL_XREFs.InsertOnSubmit(emailXref);
                        }
                    }

                    dc.SubmitChanges();
                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }


            return result;
        }

        public DebtplusResult Mim_Form7_SaveWriteOff(string clientNumber, string regnum, string char_amt)
        {
            var result = new DebtplusResult();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    string CurInvNo = string.Empty;
                    int sequence = 1;
                    if (int.TryParse(ws.SeqNo.ToString(), out sequence))
                    {
                        sequence++;
                    }
                    CurInvNo = string.Format("WS{0}-{1}", regnum, sequence);

                    ws.BillDate = DateTime.Today;
                    ws.PayType = "N9";
                    ws.LastTrans = CurInvNo;
                    ws.SeqNo = sequence;
                    ws.BillAmt = Convert.ToDecimal(char_amt);

                    //BEGIN:WEB.CAM.CONTROL is a table with column WS.TRANS.SEQ.Getting the value of that colum and incrementing it and sending back to that column.
                    //Not implemente it.
                    //int TranId = 1;
                    //using (UniFile CamControl = UvSess.CreateUniFile("WEB.CAM.CONTROL"))
                    //using (UniDynArray ContRec = CamControl.Read("WS.TRANS.SEQ"))
                    //{
                    //    if (int.TryParse(ContRec.Extract(1).ToString(), out TranId))
                    //    {
                    //        TranId++;
                    //    }
                    //    ContRec.Replace(1, TranId.ToString());
                    //    CamControl.Write("WS.TRANS.SEQ", ContRec);
                    //}
                    //END

                    var wstrans = new WEB_WSTRAN();
                    wstrans.BillDate = DateTime.Today;
                    wstrans.PayType = "N9";
                    wstrans.BillAmount = Convert.ToDecimal(char_amt);
                    wstrans.ClientNumber = Convert.ToInt32(clientNumber.Trim());
                    wstrans.TransType = "WOF";
                    wstrans.InvoiceNumber = CurInvNo.Trim();

                    dc.WEB_WSTRANs.InsertOnSubmit(wstrans);

                    dc.SubmitChanges();

                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public DebtplusResult MimCompleteCourse(string clientNumber, string prescore, string postscore, string TestDTS)
        {
            var result = new DebtplusResult();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.PreScore = Convert.ToInt32(prescore.Trim());
                        ws.PostScore = Convert.ToInt32(postscore.Trim());
                        ws.TestDate = Convert.ToDateTime(TestDTS);
                        ws.ConfirmationDateTime = Convert.ToDateTime(TestDTS);
                        ws.Status = "CO";
                        dc.SubmitChanges();
                        result.IsSuccessful = true;
                    }
                    else
                    {
                        result.IsSuccessful = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public MimQuicCalcLoadData Mim_QuicCalc_LoadData(string clientNumber, string regnum)
        {
            var result = new MimQuicCalcLoadData();

            using (var dc = new DebtplusDataContext())
            {
                var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                if (ws != null)
                {
                    result.ContactState = ws.State;
                }

                var demo = dc.WEB_WSDEMOGs.Where(d => d.CLIENTNUMBER == Convert.ToInt32(clientNumber)).FirstOrDefault();

                if (demo != null)
                {
                    result.SizeOfHousehold = demo.NUMINHOUSE.HasValue ? Convert.ToInt32(demo.NUMINHOUSE) : 0;
                    result.MonthlyGrossIncome = demo.GROSS.HasValue ? Convert.ToDecimal(demo.GROSS) : 0;
                    result.IsSuccessful = true;
                }
                else
                {
                    result.IsSuccessful = false;
                }
            }

            return result;
        }

        public DebtplusResult Mim_QuicCalc_SaveData(string clientNumber, string contact_state, string monthly_gross_income, string size_of_household)
        {
            var result = new DebtplusResult();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.State = contact_state.Trim(); ;
                    }

                    var demo = dc.WEB_WSDEMOGs.Where(d => d.CLIENTNUMBER == Convert.ToInt32(clientNumber)).FirstOrDefault();

                    if (demo != null)
                    {
                        demo.NUMINHOUSE = Convert.ToInt32(size_of_household);
                        demo.GROSS = Convert.ToDecimal(monthly_gross_income);
                    }
                    else
                    {
                        demo = new WEB_WSDEMOG();
                        demo.CLIENTNUMBER = Convert.ToInt32(clientNumber);
                        demo.NUMINHOUSE = Convert.ToInt32(size_of_household);
                        demo.GROSS = Convert.ToDecimal(monthly_gross_income);
                        dc.WEB_WSDEMOGs.InsertOnSubmit(demo);
                    }

                    dc.SubmitChanges();

                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public string Mim_NewClsID_Get(string reg_num)
        {
            string clientNumber = string.Empty;
            /*using (UniSession UvSess = OpenUniSession())
            {
                using (UniSubroutine UvSub = UvSess.CreateUniSubroutine("CRE8.CAM.SUB", 3))// the sec pram is the # of prams
                {
                    try
                    {
                        //the # is a 0 base pram #
                        UvSub.SetArg(1, "WS" + reg_num);// the regnum from MySql with a WS in front
                        UvSub.SetArg(2, "W");// the cam file to be inserted into
                        UvSub.Call();
                        ClNo = UvSub.GetArg(0);//the out pram will have the clno as a string. if blank insert failed
                    }
                    catch
                    {
                    }
                }
            }*/
            using (var dc = new DebtplusDataContext())
            {
                var wsCam = new WEB_WSCAM()
                {
                    RegNum = "WS" + reg_num
                };

                dc.WEB_WSCAMs.InsertOnSubmit(wsCam);
                dc.SubmitChanges();

                string newClientNumber = wsCam.ID.ToString();
                wsCam.ClientNumber = newClientNumber;
                wsCam.InNo = "WS" + newClientNumber;
                dc.SubmitChanges();

                clientNumber = newClientNumber;
            }
            return clientNumber;
        }

        public string Mim_HostID_ReturningUser_Get(string reg_num)
        {
            string clientID = string.Empty;

            using (var dc = new DebtplusDataContext())
            {
                var ws = dc.WEB_WSCAMs.Where(w => w.RegNum == "WS" + reg_num).FirstOrDefault();

                if (ws != null && !string.IsNullOrEmpty(ws.ClientNumber))
                {
                    clientID = ws.ClientNumber.ToString();
                }
            }

            return clientID;
        }

        public DebtplusResult MimSaveRegisterAttorney(string clientNumber, string CaseNo, string Ssn, string FirmId, string AttnyName, string AttnyEmail, string UserName)
        {
            var result = new DebtplusResult();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.SSN = string.IsNullOrEmpty(Ssn) ? "" : Ssn.Replace("-", String.Empty).Trim();
                        ws.FirmCode = FirmId.Trim();
                        ws.CaseNumber = string.IsNullOrEmpty(CaseNo) ? "" : CaseNo.Replace("-", String.Empty).Trim();
                        ws.WebLogin = UserName.Trim();

                        if (FirmId == "9999")
                        {
                            ws.AttyName = AttnyName.Trim();
                            ws.NonEscrowEMA = AttnyEmail.Trim();
                        }
                        else
                        {
                            ws.AttyName = string.Empty;
                            ws.NonEscrowEMA = string.Empty;
                        }

                        dc.SubmitChanges();
                    }
                }

                result.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public DebtplusResult Mim_WaiverWaiver_SaveUv(string ClNo)
        {
            var result = new DebtplusResult();
            try
            {
                string UvDts = DateTime.Now.ToString("D");
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_FEEWAIVERs.Where(w => w.ClNo == Convert.ToInt32(ClNo)).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.OPT_STAT = "D";
                        ws.OPT_DATE = UvDts;
                    }
                    else
                    {
                        ws = new WEB_FEEWAIVER();
                        ws.ClNo = Convert.ToInt32(ClNo);
                        ws.OPT_STAT = "D";
                        ws.OPT_DATE = UvDts;
                        dc.WEB_FEEWAIVERs.InsertOnSubmit(ws);
                    }
                    dc.SubmitChanges();
                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public DebtplusResult MimSavePreTest(string ClNo, int PreTest)
        {
            var result = new DebtplusResult();
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ID == Convert.ToInt32(ClNo)).FirstOrDefault();

                    ws.PreScore = PreTest;

                    dc.SubmitChanges();

                    result.IsSuccessful = true;
                }
            }

            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        private String correctChargeAmount(String original, String code)
        {
            var toReturn = original;

            if ((new List<String> { "D", "W", "P", "A", "N3", "N9" }).Contains(code))
            {
                toReturn = pad(original);
            }

            return toReturn;
        }

        private String pad(String original)
        {
            var toReturn = original;

            if (original.Length == 2)
            {
                return original + "00";
            }

            return toReturn;
        }

        public DebtplusResult MimDisclosureSaveMtcn(string ClNo, string mtcn, string char_amt)
        {
            var result = new DebtplusResult();
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ID == Convert.ToInt32(ClNo)).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.PayType = "W";
                        ws.MTCN = mtcn;
                        ws.BillAmt = Convert.ToDecimal(correctChargeAmount(char_amt, "W").Trim());
                        dc.SubmitChanges();
                        result.IsSuccessful = true;
                    }
                    else
                    {
                        result.IsSuccessful = false;
                    }
                }
            }

            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public DebtplusResult MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number, string char_amt)
        {
            var result = new DebtplusResult();
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == ClNo).FirstOrDefault();

                    if (ws != null)
                    {
                        ws.BankName = bank_name;
                        ws.NameOnCheck = name_on_check;
                        ws.ABANumber = aba_number;
                        ws.AcctNumber = acct_number;
                        ws.PayType = "P";
                        ws.BillAmt = Convert.ToDecimal(correctChargeAmount(char_amt, "P").Trim());
                        dc.SubmitChanges();
                    }
                    result.IsSuccessful = true;
                }
            }

            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public double MimDisclosureGetChargeAmount(string ZipCode, string FirmCode)
        {
            return this.GetCharAmt(FirmCode, ZipCode, "W");
        }

        public DebtplusResult MimSaveXRef(string PriSsn, string SecSsn)
        {
            var result = new DebtplusResult();
            var primarySSN = PriSsn.Trim().Replace("-", String.Empty);
            var secondarySSN = SecSsn.Trim().Replace("-", String.Empty);
            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var primary = dc.WEB_CASEXREFs.Where(w => string.Compare(w.PrimarySSN, primarySSN, true) == 0).FirstOrDefault();
                    if (primary == null)
                    {
                        WEB_CASEXREF xref = new WEB_CASEXREF();
                        xref.PrimarySSN = primarySSN;
                        xref.Secondary = secondarySSN;
                        xref.JointSSN = string.Empty;
                        dc.WEB_CASEXREFs.InsertOnSubmit(xref);
                        dc.SubmitChanges();
                    }
                    if (!string.IsNullOrEmpty(secondarySSN))
                    {
                        var secondary = dc.WEB_CASEXREFs.Where(w => string.Compare(w.PrimarySSN, secondarySSN, true) == 0).FirstOrDefault();
                        if (secondary == null)
                        {
                            WEB_CASEXREF xref = new WEB_CASEXREF();
                            xref.PrimarySSN = secondarySSN;
                            xref.JointSSN = "S";
                            xref.Secondary = primarySSN;
                            dc.WEB_CASEXREFs.InsertOnSubmit(xref);
                            dc.SubmitChanges();
                        }
                    }
                }
                result.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public int GetMimSequenceNumber(string clientNumber)
        {
            int back = 1;

            using (var dc = new DebtplusDataContext())
            {
                var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                if (ws != null)
                {
                    int.TryParse(ws.SeqNo.ToString(), out back);
                    back++;
                }
            }

            return back;
        }

        public MimForm7MainLineData Mim_Form7_MainLine(string contact_state)
        {
            var result = new MimForm7MainLineData();
            decimal amount = 0;

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    //var state = dc.WEB_STATE_FEEs.Where(s => s.State == contact_state).FirstOrDefault();

                    //if (state != null)
                    //{
                    //    result.CharAmt = state.CharAmount.HasValue ? state.CharAmount.ToString() : "50.00";
                    //    result.CharType = !string.IsNullOrEmpty(state.CharType) ? state.CharType : "H";
                    //}
                    //else
                    //{
                    //    result.CharAmt = "50.00";
                    //    result.CharType = "H";
                    //}

                    var product = dc.products.Where(s => s.oID == PostDischargeProductID).FirstOrDefault();

                    var state = dc.states.Where(s => s.MailingCode.ToLower() == contact_state.ToLower()).FirstOrDefault();

                    if (state != null)
                    {
                        var productFee = dc.product_states.Where(ps => ps.productID == PostDischargeProductID && ps.stateID == state.state1).FirstOrDefault();

                        if (productFee != null && productFee.Amount != 0.00m)
                        {
                            amount = productFee.Amount;
                        }
                        else
                        {
                            amount = 50.00m;
                        }
                    }
                    else
                    {
                        amount = product.Amount;
                    }

                    result.CharAmt = amount.ToString();
                    result.CharType = "H";
                }
            }
            catch (Exception ex)
            {
                result.exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public MimSetTransSaveWriteData Mim_SetTrans_SaveWriteOff(string clientNumber, string regnum)
        {
            var result = new MimSetTransSaveWriteData();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    string CurInvNo = string.Empty;
                    result.CharAmt = "50.00";
                    if (ws != null)
                    {
                        result.State = ws.State;
                        result.LastTrans = ws.LastTrans;

                        int sequence = 0;
                        int.TryParse(ws.SeqNo.ToString(), out sequence);
                        result.CurSeqNo = sequence + 1.ToString();

                        CurInvNo = string.Format("WS{0}-{1}", regnum, sequence);

                        var state = dc.WEB_STATE_FEEs.Where(s => s.State == result.State).FirstOrDefault();

                        if (state != null)
                        {
                            result.CharAmt = state.CharAmount.ToString();
                        }
                        else
                        {
                            result.CharAmt = "50.00";
                        }

                        result.CurInvNo = CurInvNo;

                        ws.BillDate = DateTime.Today;
                        ws.PayType = "N3";
                        ws.LastTrans = CurInvNo;
                        ws.SeqNo = sequence;
                        //ws.BillAmt = Convert.ToDecimal(result.CharAmt);
                    }

                    var wstrans = new WEB_WSTRAN();
                    wstrans.BillDate = DateTime.Today;
                    wstrans.PayType = "N3";
                    wstrans.BillAmount = Convert.ToDecimal(result.CharAmt);
                    wstrans.ClientNumber = Convert.ToInt32(clientNumber.Trim());
                    wstrans.TransType = "WOF";
                    wstrans.InvoiceNumber = CurInvNo.Trim();

                    dc.WEB_WSTRANs.InsertOnSubmit(wstrans);

                    dc.SubmitChanges();
                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public MimSetTransSaveWriteData Mim_SetTrans_SaveWriteOff(string clientNumber, string regnum, string char_amt)
        {
            var result = new MimSetTransSaveWriteData();

            try
            {
                using (var dc = new DebtplusDataContext())
                {
                    var ws = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber).FirstOrDefault();

                    string CurInvNo = string.Empty;
                    result.CharAmt = char_amt;
                    if (ws != null)
                    {
                        result.State = ws.State;
                        result.LastTrans = ws.LastTrans;

                        int sequence = 1;
                        if (int.TryParse(ws.SeqNo.ToString(), out sequence))
                        {
                            sequence++;
                        }

                        result.CurSeqNo = sequence.ToString();

                        CurInvNo = string.Format("WS{0}-{1}", regnum, sequence);

                        ws.BillDate = DateTime.Today;
                        ws.PayType = "N3";
                        ws.LastTrans = CurInvNo;
                        ws.SeqNo = sequence;
                        ws.BillAmt = Convert.ToDecimal(result.CharAmt);
                    }

                    var wstrans = new WEB_WSTRAN();
                    wstrans.BillDate = DateTime.Today;
                    wstrans.PayType = "N3";
                    wstrans.BillAmount = Convert.ToDecimal(result.CharAmt);
                    wstrans.ClientNumber = Convert.ToInt32(clientNumber.Trim());
                    wstrans.TransType = "WOF";
                    wstrans.InvoiceNumber = CurInvNo.Trim();

                    dc.WEB_WSTRANs.InsertOnSubmit(wstrans);

                    dc.SubmitChanges();
                    result.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                result.exception = ex;
                result.IsSuccessful = false;
            }

            return result;
        }

        public void SendDataToDebtplus(int clientNumber, long accountNumber, string firmID, string zipCode, string certificateNumber, string waiverType, string escrow, string websiteCode, int escrowProbonoValue, string secondaryFlag)
        {
            double charAmt = 0;
            if (string.IsNullOrEmpty(secondaryFlag))
            {
                charAmt = GetCharAmt(firmID, zipCode, "W");
            }

            var isLegalAid = false;

            PaymentType paymentType = PaymentType.None;
            FeeWaiverType feeWaiverType = FeeWaiverType.None;

            using (var dc = new DebtplusDataContext())
            {
                // TODO: Change the Label to OID
                var vendor = dc.vendors.Where(v => v.oID == Convert.ToInt32(firmID)).FirstOrDefault();

                if (vendor != null)
                {
                    isLegalAid = vendor.IsLegalAid.HasValue ? (bool)vendor.IsLegalAid : false;
                }

                var camData = dc.WEB_WSCAMs.Where(w => w.ClientNumber == clientNumber.ToString()).FirstOrDefault();

                //var xref = dc.WEB_CASEXREFs.Where().FirstOrDefault();

                string debitCardNumber = string.Empty;
                string debitCardExpiryDate = string.Empty;
                string acctNumber = string.Empty;
                string routingNumber = string.Empty;
                if (camData != null)
                {
                    debitCardNumber = camData.DCAcctNum;
                    debitCardExpiryDate = camData.DCExpiryDate;
                    acctNumber = camData.AcctNumber;
                    routingNumber = camData.ABANumber;
                }

                ProcessPaymentAndFeeWaiverType(debitCardNumber, debitCardExpiryDate, acctNumber, routingNumber, waiverType, isLegalAid, escrow, escrowProbonoValue, out paymentType, out feeWaiverType);
                
                dc.create_edu_client(clientNumber.ToString(), accountNumber, Convert.ToDecimal(charAmt), certificateNumber, (int)paymentType, (int)feeWaiverType);
            }
        }

        private void ProcessPaymentAndFeeWaiverType(string debitCardNumber, string debitCardExpiryDate, string acctNumber, string routingNumber, string waiverType, bool isLegalAid, string escrow, int escrowProbonoValue, out PaymentType paymentType, out FeeWaiverType feeWaiverType)
        {
            paymentType = PaymentType.None;

            feeWaiverType = FeeWaiverType.None;

            var isEscrow = false;
            bool probono = false;
            if (!string.IsNullOrEmpty(escrow) && escrow == "Y")
            {
                if (escrowProbonoValue == 1)
                {
                    isEscrow = true;
                }
                else if (escrowProbonoValue == 2)
                {
                    probono = true;
                }
            }


            if (probono == true)
            {
                paymentType = PaymentType.FeeWaiver;

                /* if the attorney is ProBono (the Waiver Type is null), set FeeWaiver to Probono */
                if (waiverType == null || waiverType == "PB")
                {
                    feeWaiverType = FeeWaiverType.ProBono;
                }
            }
            else if (isLegalAid == true)
            {
                paymentType = PaymentType.FeeWaiver;

                feeWaiverType = FeeWaiverType.LegalAid;
            }
            //else if (!string.IsNullOrEmpty(debitCardNumber) && !string.IsNullOrEmpty(debitCardExpiryDate))
            else if (!string.IsNullOrEmpty(acctNumber) && !string.IsNullOrEmpty(debitCardExpiryDate))
            {
                paymentType = PaymentType.DebitCard;
            }
            else if (!string.IsNullOrEmpty(acctNumber) && !string.IsNullOrEmpty(routingNumber))
            {
                paymentType = PaymentType.ACH;
            }
            else if (isEscrow == true)
            {
                paymentType = PaymentType.Escrow;
            }
            else
            {
                paymentType = PaymentType.FeeWaiver;

                if (waiverType == "IW")
                {
                    feeWaiverType = FeeWaiverType.Income;
                }
                else if (waiverType == "SSD")
                {
                    feeWaiverType = FeeWaiverType.SSDI;
                }
            }
        }
    }
}
