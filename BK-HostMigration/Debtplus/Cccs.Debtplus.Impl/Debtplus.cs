﻿using Cccs.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Cccs.Debtplus.Impl
{
    public partial class Debtplus : IDebtplus
    {
        private readonly IIdentity identity;
        private readonly NLog.Logger logger;
        private string eoustUser;
        private int PreFilingProductID;
        private int PostDischargeProductID;

        public Debtplus(IIdentity identity)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();
            this.identity = identity;

            //SetHostConfiguration();
            SetConfiguration();
            //SetEncryption();
        }

        private void SetConfiguration()
        {
            var configurationValid = true;

            this.eoustUser = ConfigurationManager.AppSettings["EoustUser"];
            if (String.IsNullOrEmpty(eoustUser))
            {
                logger.Fatal("Unable to retrieve Eoust User configuration. Verify appSettings 'Account' key has been configured correctly.");
                configurationValid = false;
            }

            if (!configurationValid)
                throw new ConfigurationErrorsException("Unable to configure Host settings.");

            var prefilingProduct = ConfigurationManager.AppSettings["PreFilingProductID"];
            var postfilingProduct = ConfigurationManager.AppSettings["PostDischargeProductID"];

            if (!string.IsNullOrEmpty(prefilingProduct))
            {
                PreFilingProductID = 1;
            }
            else
            {
                if (!int.TryParse(prefilingProduct, out PreFilingProductID))
                {
                    PreFilingProductID = 1;
                }
            }

            if (!string.IsNullOrEmpty(postfilingProduct))
            {
                PostDischargeProductID = 2;
            }
            else
            {
                if (!int.TryParse(postfilingProduct, out PostDischargeProductID))
                {
                    PostDischargeProductID = 2;
                }
            }
        }
    }
}
