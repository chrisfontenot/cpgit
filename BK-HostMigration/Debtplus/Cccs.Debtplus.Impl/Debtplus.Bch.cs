﻿using Cccs.Debtplus.Dal;
using System;
using System.Linq;

namespace Cccs.Debtplus.Impl
{
    public partial class Debtplus : IDebtplus
    {

        public void SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip)
        {
            using (var dc = new DebtplusDataContext())
            {
                string RecId = String.Format("INH{0}", InNo.ToString());

                RecId = String.Format("HPF_PRIVACY*{0}", RecId);

                var TranInfo = dc.WEB_TRANS_INFOs.Where(obj => obj.Id == RecId).FirstOrDefault();

                if (TranInfo != null)
                {
                    WEB_TRANS_INFO objTrans = new WEB_TRANS_INFO();
                    objTrans.Id = RecId;
                    objTrans.Type="I";
                    objTrans.Language = Lang;
                    objTrans.Email = eMail;
                    objTrans.FullName = String.Format("{0} {1}", FirstName, LastName);
                    objTrans.Address1 = Addr1;
                    objTrans.Address2 = Addr2;
                    objTrans.Address3 = String.Format("{0} {1} {2}", City, State, Zip);
                    dc.WEB_TRANS_INFOs.InsertOnSubmit(objTrans);
                    dc.SubmitChanges();
                }

            }
        }
    }
}