﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cccs.Response
{
    [DataContract]
    public class DataResponse<T> : StatusResponse
    {
        [DataMember]
        public T Payload { get; set; }

        //private bool _isSuccessful = false; // set to false by default, until we explicitly know we are successful

        //[DataMember]
        //public bool IsSuccessful
        //{
        //    get { return this._isSuccessful; }
        //    set { this._isSuccessful = value; }
        //}

        //[DataMember]
        //public List<string> ErrorMessages { get; set; }

        //public string ErrorMessagesToHtml()
        //{
        //    string htmlString = "<ul>";
        //    foreach (string s in ErrorMessages)
        //    {
        //        htmlString = "<li>" + s + "</li>";
        //    }
        //    htmlString += "</ul>";
        //    return htmlString;
        //}
    }
}

