﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs
{
    public static class NLogExtensions
    {
        public static void DebugFormat(this NLog.Logger self,string format, params object[] args){
            self.Debug(format, args);
        }

        public static void WarnFormat(this NLog.Logger self,string format, params object[] args){
            self.Warn(format, args);
        }

        public static void ErrorFormat(this NLog.Logger self, string format, params object[] args)
        {
            self.Warn(format, args);
        }

        public static void InfoFormat(this NLog.Logger self, string format, params object[] args)
        {
            self.Warn(format, args);
        }
				//logger.WarnFormat("CounselorName not found for LivePersonId: {0}", livePersonId);

    }
}
