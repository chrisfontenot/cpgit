﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs
{
    public static class ExceptionExtensions
    {
        public static void LogAndThrow(this Exception ex, ILoggingService logger)
        {
            logger.Error(ex.Message, ex);

            throw ex;
        }
    }
}
