﻿using System;
using System.Reflection;
//using log4net;
using NLog;

namespace Cccs
{
    public class Log4NetLoggingService : ILoggingService
    {

        private readonly NLog.Logger logger;
        public Log4NetLoggingService()
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();
        }


        public void SetUserId(long userId)
        {
            //ThreadContext.Properties["UserId"] = userId;
        }

        public void Debug(Func<string> func)
        {
            if (!logger.IsDebugEnabled) return;

            var message = func();
            logger.Debug(message);
        }

        public void DebugParameter<T>(string name, T value) where T : struct
        {
            if (!logger.IsDebugEnabled) return;

            logger.DebugFormat("Parameter: {0} = {1}", name, value.ToString());
        }

        public void Error(string message, Exception exception)
        {
            if (!logger.IsErrorEnabled) return;

            logger.Error(message, exception);
        }

        public void Error(Func<string> func, Exception exception)
        {
            if (!logger.IsErrorEnabled) return;

            var message = func();
            logger.Error(message, exception);
        }

        public void Fatal(string message, Exception exception)
        {
            if (!logger.IsFatalEnabled) return;

            logger.Fatal(message, exception);
        }

        public void Info(Func<string> func)
        {
            if (!logger.IsInfoEnabled) return;

            var message = func();
            logger.Info(message);
        }

        public void Warn(Func<string> func)
        {
            if (!logger.IsWarnEnabled) return;

            var message = func();
            logger.Warn(message);
        }

    }
}
