﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs
{
    public static class GenericExtensions
    {
        public static TResult ReturnIfNotNull<T, TResult>(this T item, TResult defaultValue, Func<T, TResult> func)
        {
            if (item == null)
                return defaultValue;

            return func(item);
        }

        public static bool ConditionIfNotNull<T>(this T item, Func<T, bool> func)
        {
            if (item == null)
                return false;

            return func(item);
        }
    }
}
