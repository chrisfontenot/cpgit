﻿using System;

namespace Cccs
{
    public interface ILoggingService
    {
        void SetUserId(long userId);
        void Debug(Func<string> func);
        void DebugParameter<T>(string name, T value) where T : struct;
        void Error(string message, Exception exception);
        void Error(Func<string> func, Exception exception);
        void Fatal(string message, Exception exception);
        void Info(Func<string> func);
        void Warn(Func<string> func);
    }
}
