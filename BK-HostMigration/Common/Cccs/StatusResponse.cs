﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Cccs.Response
{
    [DataContract]
    public class StatusResponse
    {
        public StatusResponse() { }

        private bool _isSuccessful = false; // set to false by default, until we explicitly know we are successful

        [DataMember]
        public bool IsSuccessful
        {
            get { return this._isSuccessful; }
            set { this._isSuccessful = value; }
        }

        [DataMember]
        public List<string> ErrorMessages { get; private set; }

        [DataMember]
        public List<StatusException> StatusExceptions { get; private set; }

        public string DisplayErrorMessagesAsHtml()
        {
            string htmlString = "<ul>";
            foreach (string s in ErrorMessages)
            {
                htmlString = "<li>" + s + "</li>";
            }
            htmlString += "</ul>";
            return htmlString;
        }

        public string DisplayErrorMessagesAsText()
        {
            string textString = "\r\n";
            foreach (string s in ErrorMessages)
            {
                textString = s + "\r\n";
            }
            textString += "\r\n";
            return textString;
        }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public DateTime StopTime { get; set; }

        public TimeSpan Duration
        {
            get { return StopTime.Subtract(StartTime); }
        }

        public void StartTimer()
        {
            this.StartTime = DateTime.Now;

            // we are assigning the start time to the stop time here,
            // that way if we get a 0 duration, we know something went wrong
            this.StopTime = this.StartTime;
        }

        public void StopTimer()
        {
            this.StopTime = DateTime.Now;
        }

        public string DisplayDurationInSeconds()
        {
            return Duration.TotalSeconds.ToString();
        }

        public void AddErrorMessage(string errorMessage)
        {
            if (this.ErrorMessages == null)
            {
                this.ErrorMessages = new List<string>();
            }

            this.ErrorMessages.Add(errorMessage);
        }

        public void AddErrorMessage(List<string> errorMessages)
        {
            if (this.ErrorMessages == null)
            {
                this.ErrorMessages = new List<string>();
            }

            this.ErrorMessages.AddRange(errorMessages);
        }

        public void AddStatusException(StatusException statusException)
        {
            if (this.StatusExceptions == null)
            {
                this.StatusExceptions = new List<StatusException>();
            }

            this.StatusExceptions.Add(statusException);
        }

        public void AddException(Exception ex)
        {
            this.AddErrorMessage(ex.Message + ex.StackTrace);
        }
    }
}

