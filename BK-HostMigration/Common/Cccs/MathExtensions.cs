﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs
{
    public static class MathExtensions
    {
        public static decimal CeilingByDecimals(this decimal d, int numberOfDecimals)
        {
            var multiplier = numberOfDecimals * 10m;
            var ceilingValue = Math.Ceiling(d * multiplier) / multiplier;

            return ceilingValue;
        }

        public static decimal MinimumMonthlyPayment(this decimal d, decimal annualInterestRate)
        {
            if (annualInterestRate <= 0)
                return 0;

            //if (principalPayoffPercent <= 0)
            //    return 0;

            if (annualInterestRate >= 1)
            {
                annualInterestRate /= 100;
            }
            //if (principalPayoffPercent >= 1)
            //{
            //    principalPayoffPercent /= 100;
            //}

            var monthlyInterestRate = annualInterestRate / 12;
            var firstMonthInterest = (d * monthlyInterestRate).CeilingByDecimals(2);
            //  var payment = (d * principalPayoffPercent).CeilingByDecimals(2) + firstMonthInterest;
            var payment = firstMonthInterest;
            return payment;
        }
    }
}
