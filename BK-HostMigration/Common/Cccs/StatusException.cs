﻿using System.Runtime.Serialization;

namespace Cccs.Response
{
    [DataContract(Name="StatusException")]
    public enum StatusException
    {
        [EnumMember]
        CertificateGenerationFailure,

        [EnumMember]
        CertificateSMTPFailure
    }
}
