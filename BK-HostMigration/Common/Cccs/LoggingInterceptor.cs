﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.DynamicProxy;

namespace Cccs
{
    public class LoggingInterceptor : ILoggingInterceptor, IInterceptor
    {
        private readonly ILoggingService service;
        public LoggingInterceptor(ILoggingService service)
        {
            this.service = service;
        }

        public void Intercept(IInvocation invocation)
        {
            service.Info(() => 
                String.Format("Entering {0}.{1}", 
                invocation.TargetType.FullName, 
                invocation.Method.Name));
            if (invocation.Arguments.Length > 0)
            {
                service.Debug(() =>
                    {
                        var argumentList = new List<string>();
                        foreach (var item in invocation.Arguments)
                        {
                            argumentList.Add(item.ToString());
                        }

                        return String.Join(", ", argumentList.ToArray());
                    });
            }

            invocation.Proceed();

            service.Debug(() => String.Format("Return Value: {0}", invocation.ReturnValue));
            service.Info(() =>
                String.Format("Exiting {0}.{1}",
                invocation.TargetType.FullName,
                invocation.Method.Name));
        }
    }
}
