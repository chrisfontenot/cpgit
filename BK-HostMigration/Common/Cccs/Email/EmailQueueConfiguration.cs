﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Cccs.Email
{
    public class EmailQueueConfiguration
    {
        public PreFilingSettings PreFiling { get; set; }

        public EmailQueueConfiguration()
        {
            PreFiling = new PreFilingSettings();
        }

        public static EmailQueueConfiguration GetConfiguration()
        {
            var configuration = ConfigurationManager.GetSection(EmailQueueConfigurationSectionHandler.SectionName) as EmailQueueConfiguration;
            return configuration;
        }
    }
}
