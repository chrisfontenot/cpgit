﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using CuttingEdge.Conditions;
using System.Xml;

namespace Cccs.Email
{
    public class EmailQueueConfigurationSectionHandler : IConfigurationSectionHandler
    {
        internal const string SectionName = "EmailQueue";

        const string PreFilingElement = "PreFiling";
        const string ClientActionPlanElement = "ClientActionPlan";
        const string CounselorActionPlanElement = "CounselorActionPlan";
        const string AttorneyCertificateElement = "AttorneyCertificate";
        const string QueuePathAttribute = "queuePath";
        const string EnvironmentAttribute = "Environment";
        const string DefaultEmailAddressAttribute = "DefaultEmailAddress";
        const string PollFrequencyAttribute = "pollFrequencySeconds";
        const string QueueTimeoutAttribute = "timeoutSeconds";
        const string FailedMessageQueuePathAttribute = "failedMessageQueuePath";

        /// <summary>
        /// Creates a configuration section handler.
        /// </summary>
        /// <param name="parent">The parent object.</param>
        /// <param name="configContext">The configuration context object.</param>
        /// <param name="section">The xml node containing the configuration section.</param>
        /// <returns>The configuration object.</returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            var requiredNodes = new Dictionary<string, bool>();
            requiredNodes.Add(PreFilingElement, false);
            ValidateNodes(section, requiredNodes);

            var nodeHandlers = new Dictionary<string, Action<XmlNode, EmailQueueConfiguration>>();
            nodeHandlers.Add(PreFilingElement, PreFilingElementHandler);

            var configuration = new EmailQueueConfiguration();
            foreach (XmlNode node in section.ChildNodes)
            {
                var action = nodeHandlers[node.Name];
                action(node, configuration);
            }

            return configuration;
        }

        private void ValidateNodes(XmlNode parent, Dictionary<string, bool> requiredNodes)
        {
            foreach (XmlNode node in parent.ChildNodes)
            {
                if (!requiredNodes.ContainsKey(node.Name))
                    throw new ConfigurationErrorsException(String.Format("Unrecognized configuration node. '{0}'", node.Name));

                requiredNodes[node.Name] = true;
            }

            foreach (var item in requiredNodes)
            {
                if (!item.Value)
                    throw new ConfigurationErrorsException(String.Format("Missing required configuration node. '{0}'", item.Key));
            }
        }

        private void PreFilingElementHandler(XmlNode preFilingNode, EmailQueueConfiguration configuration)
        {
            var preFilingSettings = configuration.PreFiling;
            ValidatePreFilingSettings(preFilingNode, preFilingSettings);

            var requiredNodes = new Dictionary<string, bool>();
            requiredNodes.Add(ClientActionPlanElement, false);
            requiredNodes.Add(AttorneyCertificateElement, false);
            requiredNodes.Add(CounselorActionPlanElement, false);
            ValidateNodes(preFilingNode, requiredNodes);

            var clientNode = preFilingNode.SelectSingleNode(ClientActionPlanElement);
            ValidateQueueSettings(clientNode, configuration.PreFiling.ClientActionPlanSettings);

            var attorneyNode = preFilingNode.SelectSingleNode(AttorneyCertificateElement);
            ValidateQueueSettings(attorneyNode, configuration.PreFiling.AttorneyCertificateSettings);

            var counselorNode = preFilingNode.SelectSingleNode(CounselorActionPlanElement);
            ValidateQueueSettings(counselorNode, configuration.PreFiling.CounselorActionPlanSettings);
        }

        private static void ValidatePreFilingSettings(XmlNode preFilingNode, PreFilingSettings settings)
        {
            var pollFrequencySecondsAttribute = preFilingNode.Attributes[PollFrequencyAttribute];
            if (pollFrequencySecondsAttribute == null || pollFrequencySecondsAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", PollFrequencyAttribute, preFilingNode.Name));
            var pollFrequencySeconds = 0d;
            if (!Double.TryParse(pollFrequencySecondsAttribute.Value, out pollFrequencySeconds))
                throw new ConfigurationErrorsException(String.Format("Attribute '{0}' on node '{1}' must contain a valid numeric value.", PollFrequencyAttribute, preFilingNode.Name));

            var environmentAttribute = preFilingNode.Attributes[EnvironmentAttribute];
            if (environmentAttribute == null || environmentAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", EnvironmentAttribute, preFilingNode.Name));

            var defaultEmailAddressAttribute = preFilingNode.Attributes[DefaultEmailAddressAttribute];
            if (defaultEmailAddressAttribute == null || defaultEmailAddressAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", DefaultEmailAddressAttribute, preFilingNode.Name));

            settings.Environment = environmentAttribute.Value;
            settings.DefaultEmailAddress = defaultEmailAddressAttribute.Value;
            settings.PollFrequencySeconds = pollFrequencySeconds;
        }

        private static void ValidateQueueSettings(XmlNode queueSettingsNode, QueueSettings settings)
        {
            var pathAttribute = queueSettingsNode.Attributes[QueuePathAttribute];
            if (pathAttribute == null || pathAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", QueuePathAttribute, queueSettingsNode.Name));

            var failedPathAttribute = queueSettingsNode.Attributes[FailedMessageQueuePathAttribute];
            if (failedPathAttribute == null || failedPathAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", FailedMessageQueuePathAttribute, queueSettingsNode.Name));

            var timeoutAttribute = queueSettingsNode.Attributes[QueueTimeoutAttribute];
            if (timeoutAttribute == null || timeoutAttribute.Value.IsNullOrWhiteSpace())
                throw new ConfigurationErrorsException(String.Format("Missing attribute '{0}' on node '{1}'", QueueTimeoutAttribute, queueSettingsNode.Name));
            var timeoutSeconds = 0d;
            if (!Double.TryParse(timeoutAttribute.Value, out timeoutSeconds))
                throw new ConfigurationErrorsException(String.Format("Attribute '{0}' on node '{1}' must contain a valid numeric value.", QueueTimeoutAttribute, queueSettingsNode.Name));

            settings.QueuePath = pathAttribute.Value;
            settings.TimeoutSeconds = timeoutSeconds;
            settings.FailedMessageQueuePath = failedPathAttribute.Value;
        }
    }
}
