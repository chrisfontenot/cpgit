﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Email
{
    public class PreFilingSettings
    {
        public double PollFrequencySeconds { get; internal set; }
        public string Environment { get; internal set; }
        public string DefaultEmailAddress { get; internal set; }

        public QueueSettings ClientActionPlanSettings { get; private set; }
        public QueueSettings AttorneyCertificateSettings { get; private set; }
        public QueueSettings CounselorActionPlanSettings { get; private set; }

        public PreFilingSettings()
        {
            ClientActionPlanSettings = new QueueSettings();
            AttorneyCertificateSettings = new QueueSettings();
            CounselorActionPlanSettings = new QueueSettings();
        }

        public bool UseDefaultEmailAddress()
        {
            return (!((Environment != null) && (Environment == "PROD")));
        }
    }
}
