﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Email
{
    public class QueueSettings
    {
        public string QueuePath { get; internal set; }
        public double TimeoutSeconds { get; internal set; }
        public string FailedMessageQueuePath { get; internal set; }
    }
}
