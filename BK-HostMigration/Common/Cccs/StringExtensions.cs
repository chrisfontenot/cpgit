﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Cccs
{
    public static class StringExtensions
    {
        public static string FormatWith(this string format, params object[] args)
        {
            return String.Format(format, args);
        }

        public static bool IsNullOrEmpty(this string s)
        {
            return String.IsNullOrEmpty(s);
        }

        public static bool IsNullOrWhiteSpace(this string s)
        {
            if (s.IsNullOrEmpty()) return true;

            if (s.Trim().IsNullOrEmpty()) return true;

            return false;
        }

        public static string IsNullOrWhiteSpace(this string s, string defaultValue)
        {
            if (s.IsNullOrWhiteSpace())
                return defaultValue;

            return s;
        }

        public static string TrimOrNull(this string s)
        {
            if (s.IsNullOrWhiteSpace())
                return s;

            return s.Trim();
        }

        public static string ToTitleCase(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;

            var textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;
            var output = textInfo.ToTitleCase(s);

            return output;
        }
    }
}
