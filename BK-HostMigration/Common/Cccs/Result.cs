﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace Cccs
{
	[DataContract]
	public class Result
	{
		private Exception m_exception = null;
		
		public Result()
		{
		}

		public Result(Result result)
		{
			IsSuccessful = result.IsSuccessful;
			Messages = result.Messages;
			Exception = result.Exception;
		}

		[DataMember]
		public bool IsSuccessful { get; set; }

		[DataMember]
		public string[] Messages { get; set; }

		[DataMember]
		public string ExceptionStr { get; set; }

		public Exception Exception 
		{ 
			get 
			{ 
				return m_exception; 
			} 
			set 
			{ 
				m_exception = value; 
				SetExceptionStr(value); 
			}
		}

		private void SetExceptionStr(Exception exception)
		{
			StringBuilder sb = new StringBuilder();

			if (exception != null)
			{
				for (Exception ex = exception; ex != null; ex = ex.InnerException)
				{
					if (sb.Length > 0)
					{
						sb.Append("  Inner exception: ");
					}

					sb.Append(ex.GetType().ToString());
					sb.Append(": '");
					sb.Append(ex.Message);
					sb.AppendLine("'.");
				}

				sb.AppendLine();
				sb.AppendLine("StackTrace: ");
				sb.Append(exception.StackTrace);

				ExceptionStr = sb.ToString();
			}
		}
	}

	public class Result<T> : Result
	{
		public Result() : base() { }

		public Result(T value) : base() { Value = value; }

		public T Value { get; set; }
	}

	//public class Result<T, S> : Result
	//{
	//  public T Value1 { get; set; }
	//  public S Value2 { get; set; }
	//}
}