﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;
using StructureMap.Configuration.DSL;
using Castle.DynamicProxy;

namespace Cccs.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class LoggingTests
    {
        public LoggingTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /*
        [TestMethod]
        public void Logging_Instance_Sets_UserId()
        {
            var container = MockHelper.CreateAutoMockContainer();

            ObjectFactory.Initialize(r => 
            {
                r.AddRegistry<LoggingRegistry>();
            });

            var service = ObjectFactory.GetInstance<ILoggingService>();
        }

        public class LoggingRegistry : Registry
        {
            public LoggingRegistry()
            {
                For<ILoggingService>().Use<Log4NetLoggingService>()
                    .OnCreation(x => LogUserId(x, 1));
            }

            private void LogUserId(Log4NetLoggingService service, long userId)
            {
                service.SetUserId(userId);
            }
        }

        */
    }
}
