﻿using System;
using Moq;

namespace Cccs.Tests
{
    public static class MockHelper
    {
        public static AutoMockContainer CreateAutoMockContainer()
        {
            var repository = new MockRepository(MockBehavior.Loose);
            var container = new AutoMockContainer(repository);

            return container;
        }
    }
}
