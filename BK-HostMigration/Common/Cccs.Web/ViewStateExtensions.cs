﻿using System.Web.UI;

namespace Cccs.Web
{
	public static class ViewStateExtensions
	{
		public static T ValueGet<T>(this StateBag view_state_bag, string key)
		{
			T value = default(T);

			object value_obj = view_state_bag[key];

			if (value_obj != null)
			{
				value = (T)value_obj;
			}

			return value;
		}

		public static T ValueGet<T>(this StateBag view_state_bag, string key, T nullValue)
		{
			T value = default(T);

			object value_obj = view_state_bag[key];

			if (value_obj != null)
			{
				value = (T)value_obj;
			}
			else
			{
				value = nullValue;
			}

			return value;
		}

		public static void ValueSet<T>(this StateBag view_state_bag, string key, T value)
		{
			if (!object.Equals(value, default(T)))
			{
				view_state_bag[key] = value;
			}
			else
			{
				view_state_bag.Remove(key);
			}
		}
	}
}
