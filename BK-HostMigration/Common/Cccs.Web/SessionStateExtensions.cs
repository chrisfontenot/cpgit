﻿using System.Web.SessionState;

namespace Cccs.Web
{
	public static class SessionStateExtensions
	{
		public static T ValueGet<T>(this HttpSessionState session, string key)
		{
			T value = default(T);

			if (session != null)
			{
				object value_obj = session[key];

				if (value_obj != null)
				{
					value = (T)value_obj;
				}
			}

			return value;
		}

		public static void ValueSet<T>(this HttpSessionState session, string key, T value)
		{
			if (session != null)
			{
				if (!object.Equals(value, default(T)))
				{
					session[key] = value;
				}
				else
				{
					session.Remove(key);
				}
			}
		}
	}
}
