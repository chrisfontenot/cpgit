﻿using System;
using System.Web;
using System.Web.UI;

namespace Cccs.Web
{
	public class ParamsBase
	{
		public static void ValueSet(string param_name, string value)
		{
			((Page)HttpContext.Current.Handler).Form.Attributes[param_name] = value;
		}

		public static string ValueGet(string param_name)
		{
			string value = ((Page)HttpContext.Current.Handler).Form.Attributes[param_name];

			if (string.IsNullOrEmpty(value))
			{
				value = ((Page)HttpContext.Current.Handler).Request.Params[param_name];

				if (!string.IsNullOrEmpty(value))
				{
					ValueSet(param_name, value);
				}
			}

			return value;
		}

		public static int ValueGetAsInt(string param_name)
		{
			return Convert.ToInt32(ValueGet(param_name));
		}

		public static short ValueGetAsShort(string param_name)
		{
			return Convert.ToInt16(ValueGet(param_name));
		}

		public static Guid ValueGetAsGuid(string param_name)
		{
			Guid value = Guid.Empty;

			try
			{
				Guid guid = new Guid(param_name);
				value = guid;
			}
			catch
			{
			}

			return value;
		}
	}
}
