﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using Cccs.Geographics.Dal;

namespace Cccs.Geographics.Impl
{
	public class Geographics : IGeographics
	{
		public string[] StateCodesGet()
		{
			string[] state_codes = null;

			using (GeographicsDataContext entities = new GeographicsDataContext())
			{
				state_codes =
				(
					from entity in entities.zipcodes
					select entity.state
				).Distinct().OrderBy(s => s).ToArray();
			}

			return state_codes;	
		}

		public string StateCodeGetByZip(string zip_code)
		{
			string state_code = null;

			using (GeographicsDataContext entities = new GeographicsDataContext())
			{
				state_code =
				(
					from entity in entities.zipcodes
					where (string.Compare(entity.id.Trim(), zip_code.Trim(), true) == 0)
					select entity.state
				).FirstOrDefault();
			}

			return state_code;
		}

		public string[] ZipCodesGetByState(string state_code)
		{
			string[] zip_codes = null;

			using (GeographicsDataContext entities = new GeographicsDataContext())
			{
				zip_codes =
				(
					from entity in entities.zipcodes
					where (string.Compare(entity.state.Trim(), state_code.Trim(), true) == 0)
					select entity.id
				).Distinct().OrderBy(z => z).ToArray();
			}

			return zip_codes;
		}
	}
}
