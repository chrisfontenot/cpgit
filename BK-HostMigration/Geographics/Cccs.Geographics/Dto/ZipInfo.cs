﻿using System;
using System.Runtime.Serialization;

namespace Cccs.Geographics
{
	[DataContract]
	public class ZipInfo
	{
		[DataMember]
		public string ZipCode { get; set; }

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public string State { get; set; }

		[DataMember]
		public double? Lat { get; set; }

		[DataMember]
		public double? Lon { get; set; }
	}
}
