﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Geographics
{
	public interface IGeographics
	{
		string[] StateCodesGet();

		string StateCodeGetByZip(string zip_code);

		string[] ZipCodesGetByState(string state_code);
	}
}
