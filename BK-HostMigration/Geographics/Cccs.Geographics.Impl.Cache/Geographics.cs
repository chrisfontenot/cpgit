﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Caching;

namespace Cccs.Geographics.Impl.Cache
{
	public class Geographics : IGeographics
	{
		private IGeographics m_geographics;
		private ICacheManager m_cache;

		public Geographics(IGeographics geographics)
		{
			m_geographics = geographics;
			m_cache = CacheFactory.GetCacheManager();
		}


		public string[] StateCodesGet()
		{
			string cache_key = "StateCodesGet";

			string[] state_codes = m_cache.GetData(cache_key) as string[];

			if (state_codes == null)	// Cache miss
			{
				state_codes = m_geographics.StateCodesGet();

				if (state_codes != null)	// Cache value
				{
					m_cache.Add(cache_key, state_codes);
				}
			}

			return state_codes;
		}

		public string StateCodeGetByZip(string zip_code)
		{
			string cache_key = string.Format("StateCodeGetByZip_{0}", zip_code.Trim());

			string state_code = m_cache.GetData(cache_key) as string;

			if (state_code == null)	// Cache miss
			{
				state_code = m_geographics.StateCodeGetByZip(zip_code);

				if (state_code != null)	// Cache value
				{
					m_cache.Add(cache_key, state_code);
				}
			}

			return state_code;
		}

		public string[] ZipCodesGetByState(string state_code)
		{
			string cache_key = string.Format("ZipCodesGetByState_{0}", state_code.Trim());

			string[] zip_codes = m_cache.GetData(cache_key) as string[];

			if (zip_codes == null)	// Cache miss
			{
				zip_codes = m_geographics.ZipCodesGetByState(state_code);

				if (zip_codes != null)	// Cache value
				{
					m_cache.Add(cache_key, zip_codes);
				}
			}

			return zip_codes;
		}
	}
}
