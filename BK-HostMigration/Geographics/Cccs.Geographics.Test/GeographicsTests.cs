﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cccs.Geographics.Test
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class GeographicsTests
	{
		public GeographicsTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void StateCodesTest()
		{
			IGeographics geographics = new Cccs.Geographics.Impl.Cache.Geographics(new Cccs.Geographics.Impl.Geographics());


			string[] state_codes = geographics.StateCodesGet();

			foreach (string state_code in state_codes)
			{
				string[] zip_codes = geographics.ZipCodesGetByState(state_code);
			  /*
				foreach (string zip_code in zip_codes)
				{
					state_codes = geographics.StateCodesGetByZip(zip_code);

					if (state_codes.Length > 1)
					{
						bool bp = true;
					}
				}
				*/
			}


		}
	}
}
