﻿CREATE PROCEDURE [dbo].[GetUnarchivedCertificateUser]
AS
BEGIN
	DECLARE @UserId bigint
	SELECT TOP 1
		@UserId = U.UserId
	FROM
		dbo.[User] U
		JOIN dbo.UserDetail D ON D.UserId = U.UserId
		JOIN dbo.CounselingCertificate C ON C.UserDetailId = D.UserDetailId
	WHERE
		C.ArchiveStatus IS NULL
		OR C.ArchiveStatus = 0
		
	IF (@UserId IS NOT NULL)
	BEGIN
		-- Mark the user as in process so archiving is thread safe.
		UPDATE
			C
		SET
			ArchiveStatus = 2
		FROM
			dbo.UserDetail D
			JOIN dbo.CounselingCertificate C ON C.UserDetailId = D.UserDetailId
		WHERE
			D.UserId = @UserId
			AND (C.ArchiveStatus IS NULL OR C.ArchiveStatus = 0)
	END

	SELECT @UserId 'UserId'
END
