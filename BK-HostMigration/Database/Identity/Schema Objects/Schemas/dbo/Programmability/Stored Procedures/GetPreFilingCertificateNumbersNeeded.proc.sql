﻿CREATE PROCEDURE [dbo].[GetPreFilingCertificateNumbersNeeded]
AS
BEGIN
	DECLARE @PreFilingSnapshotId bigint
	SELECT TOP 1
		@PreFilingSnapshotId = S.PreFilingSnapshotId
	FROM
		dbo.UserDetail D
		JOIN dbo.PreFilingSnapshot S ON S.UserDetailId = D.UserDetailId
	WHERE
		(S.ArchiveStatus IS NULL OR S.ArchiveStatus = 0)
		AND (S.CertificateNumber IS NULL OR S.CertificateNumber = '')
		
	IF (@PreFilingSnapshotId IS NOT NULL)
	BEGIN
		-- Mark the snapshot as in process so maintenance will ignore it.
		UPDATE
			S
		SET
			ArchiveStatus = 2
		FROM
			dbo.PreFilingSnapshot S
		WHERE
			S.PreFilingSnapshotId = @PreFilingSnapshotId
	END

	SELECT @PreFilingSnapshotId 'PreFilingSnapshotId'
END
