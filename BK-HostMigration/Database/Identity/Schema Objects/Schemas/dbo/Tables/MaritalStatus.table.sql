﻿CREATE TABLE [dbo].[MaritalStatus]
(
	MaritalStatusCode nchar(1) NOT NULL,
	Text nvarchar(50) NOT NULL,
	SortOrder tinyint NULL
)
