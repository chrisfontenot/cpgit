﻿CREATE TABLE [dbo].[PreFilingSnapshotCreditor]
(
	PreFilingSnapshotId bigint NOT NULL,
	Name nvarchar(60) NOT NULL,
	DisplayOrder tinyint NOT NULL,
	InterestRate decimal(9, 3) NOT NULL,
	Balance money NOT NULL,
	MonthlyPayment money NOT NULL,
	MonthsToRepay smallint NOT NULL
)
