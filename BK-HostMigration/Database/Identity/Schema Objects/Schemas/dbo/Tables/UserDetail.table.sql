﻿CREATE TABLE [dbo].[UserDetail]
(
	UserDetailId bigint identity(1,1) NOT NULL,
	UserId bigint NOT NULL,
	IsPrimary bit NOT NULL,
	FirstName nvarchar(100) NOT NULL,
	MiddleName nvarchar(100) NULL,
	LastName nvarchar(100) NOT NULL,
	Ssn varbinary(max) NULL,
	BirthDate datetime NULL,
	IsMale bit NULL,
	RaceCode nchar(1) NULL,
	IsHispanic bit NULL,
	MaritalStatusCode nchar(1) NULL,
	Email nvarchar(320) NULL,
	PhoneCell nvarchar(30) NULL,
	PhoneHome nvarchar(30) NULL,
	PhoneWork nvarchar(30) NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL,
	IsActive bit NOT NULL
)
