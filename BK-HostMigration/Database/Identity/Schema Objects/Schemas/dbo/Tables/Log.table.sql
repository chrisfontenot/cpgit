﻿CREATE TABLE [dbo].[Log]
(
	LogId bigint identity(1,1) NOT NULL,
	UserId bigint NULL,
	Date datetime NOT NULL,
	Thread varchar(255) NOT NULL,
	Level varchar(50) NOT NULL,
	Logger varchar(255) NOT NULL,
	Message varchar(max) NOT NULL,
	Exception varchar(max) NULL
)
