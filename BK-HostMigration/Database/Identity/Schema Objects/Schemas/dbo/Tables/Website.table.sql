﻿CREATE TABLE [dbo].[Website]
(
	WebsiteCode nvarchar(4) NOT NULL,
	Name nvarchar(50) NOT NULL,
	Url nvarchar(200) NOT NULL,
	IsActive bit NOT NULL
)
