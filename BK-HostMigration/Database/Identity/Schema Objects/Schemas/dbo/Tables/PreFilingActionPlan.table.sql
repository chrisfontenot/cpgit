﻿CREATE TABLE [dbo].[PreFilingActionPlan]
(
	PreFilingActionPlanId bigint identity(1,1) NOT NULL, 
	PrimarySnapshotId bigint NOT NULL,
	SecondarySnapshotId bigint NULL
)
