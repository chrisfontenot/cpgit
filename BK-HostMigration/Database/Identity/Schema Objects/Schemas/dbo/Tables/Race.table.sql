﻿CREATE TABLE [dbo].[Race]
(
	RaceCode nchar(1) NOT NULL,
	Text nvarchar(50) NULL,
	SortOrder tinyint NULL
)
