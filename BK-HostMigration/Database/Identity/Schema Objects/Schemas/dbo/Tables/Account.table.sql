﻿CREATE TABLE [dbo].[Account]
(
	AccountId bigint identity(1,1) NOT NULL, 
	UserId bigint NOT NULL,
	AccountTypeCode nvarchar(8) NOT NULL,
	InternetId int NULL,
	HostId nvarchar(50) NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL,
	IsActive bit NOT NULL,
	EmployeeId nvarchar(200) NULL
)
