﻿CREATE TABLE [dbo].[HoldAcntNo]
(
	UserId bigint NOT NULL,
	InternetId bigint NULL,
	HostId nvarchar(50) NULL,
	Ssn varbinary(max) NULL
)
