﻿CREATE TABLE [dbo].[AccountType]
(
	AccountTypeCode nvarchar(8) NOT NULL,
	Name nvarchar(50) NOT NULL,
	Description nvarchar(300) NOT NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL
)
