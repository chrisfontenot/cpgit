﻿CREATE TABLE [dbo].[UserWebsite]
(
	UserId bigint NOT NULL,
	WebsiteCode nvarchar(4) NOT NULL,
	AccountId bigint NULL,
	LastCompletedPage nvarchar(200) NULL,
	PercentComplete tinyint NOT NULL,
	CompletedDate datetime NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL,
	IsActive bit NOT NULL
)
