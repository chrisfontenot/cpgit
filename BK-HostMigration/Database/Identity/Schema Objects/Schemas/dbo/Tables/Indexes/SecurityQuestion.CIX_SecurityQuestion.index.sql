﻿CREATE CLUSTERED INDEX [CIX_SecurityQuestion]
    ON [dbo].[SecurityQuestion]
	(SortOrder, SecurityQuestionId)
