﻿CREATE TABLE [dbo].[PreFilingSnapshotExpenseCategory]
(
	PreFilingSnapshotId bigint NOT NULL,
	ExpenseCategoryId int NOT NULL,
	ExpenseCategory nvarchar(100) NOT NULL,
	DisplayText nvarchar(150) NOT NULL,
	DisplayOrder tinyint NOT NULL,
	SessionAmount money NOT NULL,
	PercentOfTotal decimal(9, 2) NOT NULL,
	RecommendedPercentage decimal(9, 2) NOT NULL,
	RecommendedAmount decimal(9, 2) NOT NULL
)
