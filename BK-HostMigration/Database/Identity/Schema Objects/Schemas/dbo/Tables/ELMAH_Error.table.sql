﻿CREATE TABLE [dbo].[ELMAH_Error]
(
	ErrorId uniqueidentifier NOT NULL,
	Application nvarchar(60) NOT NULL,
	Host nvarchar(50) NOT NULL,
	Type nvarchar(100) NOT NULL,
	Source nvarchar(60) NOT NULL,
	Message nvarchar(500) NOT NULL,
	[User] nvarchar(50) NOT NULL,
	StatusCode int NOT NULL,
	TimeUtc datetime NOT NULL,
	Sequence int identity(1,1) NOT NULL,
	AllXml ntext NOT NULL
)
