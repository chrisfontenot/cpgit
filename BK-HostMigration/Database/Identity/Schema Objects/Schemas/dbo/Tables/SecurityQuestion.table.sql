﻿CREATE TABLE [dbo].[SecurityQuestion]
(
	SecurityQuestionId tinyint identity(1,1) NOT NULL, 
	[Text] nvarchar(50) NOT NULL,
	SortOrder tinyint NOT NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL
)
