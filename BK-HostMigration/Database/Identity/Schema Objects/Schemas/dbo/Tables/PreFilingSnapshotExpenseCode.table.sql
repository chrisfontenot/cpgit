﻿CREATE TABLE [dbo].[PreFilingSnapshotExpenseCode]
(
	PreFilingSnapshotId bigint NOT NULL,
	ExpenseCategoryId int NOT NULL,
	ExpenseCodeId int NOT NULL,
	ExpenseCode nvarchar(100) NOT NULL,
	DisplayText nvarchar(150) NOT NULL,
	DisplayOrder tinyint NOT NULL,
	SessionAmount money NOT NULL
)
