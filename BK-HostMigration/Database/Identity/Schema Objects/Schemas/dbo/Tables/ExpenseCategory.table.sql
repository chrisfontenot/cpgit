﻿CREATE TABLE [dbo].ExpenseCategory
(
	ExpenseCategoryId int NOT NULL,
	Name nvarchar(100) NOT NULL,
	LookupCode nvarchar(50) NOT NULL,
	RecommendedPercentage decimal(9, 2) NOT NULL,
	DisplayOrder tinyint NOT NULL
)
