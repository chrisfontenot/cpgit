﻿CREATE TABLE [dbo].[Address]
(
	AddressId bigint identity(1,1) NOT NULL,
	UserId bigint NOT NULL,
	AddressType nvarchar(50) NOT NULL,
	StreetLine1 nvarchar(200) NOT NULL,
	StreetLine2 nvarchar(200) NULL,
	City nvarchar(200) NOT NULL,
	State nvarchar(5) NOT NULL,
	Zip nvarchar(10) NOT NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL,
	IsActive bit NOT NULL
)
