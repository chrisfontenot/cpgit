﻿CREATE TABLE [dbo].ExpenseCode
(
	ExpenseCodeId int NOT NULL,
	ExpenseCategoryId int NOT NULL,
	Name nvarchar(100) NOT NULL,
	LookupCode nvarchar(50) NOT NULL,
	DisplayOrder tinyint NOT NULL
)
