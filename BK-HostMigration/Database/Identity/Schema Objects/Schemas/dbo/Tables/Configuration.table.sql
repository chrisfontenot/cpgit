﻿CREATE TABLE [dbo].[Configuration]
(
	ConfigurationCode nvarchar(50) NOT NULL,
	Value nvarchar(max) NOT NULL,
	Description nvarchar(max) NULL,
	CreatedDate datetime NOT NULL,
	ModifiedDate datetime NOT NULL,
	ModifiedBy nvarchar(200) NOT NULL
)
