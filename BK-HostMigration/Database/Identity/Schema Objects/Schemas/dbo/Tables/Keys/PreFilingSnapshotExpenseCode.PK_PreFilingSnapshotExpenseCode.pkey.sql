﻿ALTER TABLE [dbo].[PreFilingSnapshotExpenseCode]
	ADD CONSTRAINT [PK_PreFilingSnapshotExpenseCode]
	PRIMARY KEY (PreFilingSnapshotId, ExpenseCodeId)