﻿ALTER TABLE [dbo].[PreFilingSnapshotCreditor]
	ADD CONSTRAINT [PK_PreFilingSnapshotCreditor]
	PRIMARY KEY (PreFilingSnapshotId, DisplayOrder)