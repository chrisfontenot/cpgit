﻿ALTER TABLE [dbo].[PreFilingSnapshotExpenseCategory]
	ADD CONSTRAINT [PK_PreFilingSnapshotExpenseCategory]
	PRIMARY KEY (PreFilingSnapshotId, ExpenseCategoryId)