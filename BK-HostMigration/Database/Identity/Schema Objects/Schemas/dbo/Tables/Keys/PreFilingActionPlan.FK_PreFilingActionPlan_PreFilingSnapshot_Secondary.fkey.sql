﻿ALTER TABLE [dbo].[PreFilingActionPlan]
	ADD CONSTRAINT [FK_PreFilingActionPlan_PreFilingSnapshot_Secondary] 
	FOREIGN KEY (SecondarySnapshotId)
	REFERENCES dbo.PreFilingSnapshot (PreFilingSnapshotId)	

