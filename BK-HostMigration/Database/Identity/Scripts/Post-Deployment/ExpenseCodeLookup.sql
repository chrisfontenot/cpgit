﻿-- =============================================
-- Script Template
-- =============================================

/* Housing Lookups */
DECLARE @ExpenseCategoryId int
SELECT @ExpenseCategoryId = 1
IF (NOT EXISTS(SELECT ExpenseCategoryId FROM dbo.ExpenseCategory WHERE ExpenseCategoryId = @ExpenseCategoryId))
BEGIN
	INSERT INTO dbo.ExpenseCategory (ExpenseCategoryId, Name, LookupCode, RecommendedPercentage, DisplayOrder)
		SELECT @ExpenseCategoryId, 'Housing', 'Housing', 0.38, 1

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 1, @ExpenseCategoryId, 'Rent or Mortgage Payment', 'RentOrMortgage', 1

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 2, @ExpenseCategoryId, 'Second Mortgage', 'SecondMortgage', 2

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 3, @ExpenseCategoryId, 'HomeMaintenance', 'HomeMaintenance', 3

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 4, @ExpenseCategoryId, 'Utilities', 'Utilities', 4
END
GO

/* Transportation Lookups */
DECLARE @ExpenseCategoryId int
SELECT @ExpenseCategoryId = 2
IF (NOT EXISTS(SELECT ExpenseCategoryId FROM dbo.ExpenseCategory WHERE ExpenseCategoryId = @ExpenseCategoryId))
BEGIN
	INSERT INTO dbo.ExpenseCategory (ExpenseCategoryId, Name, LookupCode, RecommendedPercentage, DisplayOrder)
		SELECT @ExpenseCategoryId, 'Transportation', 'Transportation', 0.15, 2

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 5, @ExpenseCategoryId, 'Auto Payments', 'AutoPayments', 1

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 6, @ExpenseCategoryId, 'Auto Insurance', 'AutoInsurance', 2

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 7, @ExpenseCategoryId, 'Auto Maintenance', 'AutoMaintenance', 3

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 8, @ExpenseCategoryId, 'Public Transportation', 'PublicTransportation', 4
END
GO

/* Food Lookups */
DECLARE @ExpenseCategoryId int
SELECT @ExpenseCategoryId = 3
IF (NOT EXISTS(SELECT ExpenseCategoryId FROM dbo.ExpenseCategory WHERE ExpenseCategoryId = @ExpenseCategoryId))
BEGIN
	INSERT INTO dbo.ExpenseCategory (ExpenseCategoryId, Name, LookupCode, RecommendedPercentage, DisplayOrder)
		SELECT @ExpenseCategoryId, 'Food', 'Food', 0.12, 3

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 9, @ExpenseCategoryId, 'Food Out', 'FoodOut', 1

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 10, @ExpenseCategoryId, 'Food In', 'FoodIn', 2
END
GO

/* Other Expenses Lookups */
DECLARE @ExpenseCategoryId int
SELECT @ExpenseCategoryId = 4
IF (NOT EXISTS(SELECT ExpenseCategoryId FROM dbo.ExpenseCategory WHERE ExpenseCategoryId = @ExpenseCategoryId))
BEGIN
	INSERT INTO dbo.ExpenseCategory (ExpenseCategoryId, Name, LookupCode, RecommendedPercentage, DisplayOrder)
		SELECT @ExpenseCategoryId, 'Other Expenses', 'OtherExpenses', 0.35, 4

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 11, @ExpenseCategoryId, 'Telephone', 'Telephone', 1

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 12, @ExpenseCategoryId, 'Insurance', 'Insurance', 2

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 13, @ExpenseCategoryId, 'Medical', 'Medical', 3

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 14, @ExpenseCategoryId, 'Child Support and Alimony', 'ChildSupportAlimony', 4

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 15, @ExpenseCategoryId, 'Child/Elder Care', 'ChildElderCare', 5

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 16, @ExpenseCategoryId, 'Education', 'Education', 6

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 17, @ExpenseCategoryId, 'Credit Card Payments', 'CreditCard', 7

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 18, @ExpenseCategoryId, 'Other Installment Loans', 'OtherInstallmentLoans', 8

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 19, @ExpenseCategoryId, 'Charity', 'Charity', 9

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 20, @ExpenseCategoryId, 'Clothing', 'Clothing', 10

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 21, @ExpenseCategoryId, 'Laundry', 'Laundry', 11

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 22, @ExpenseCategoryId, 'Personal Expenses', 'Personal', 12

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 23, @ExpenseCategoryId, 'Beauty', 'Beauty', 13

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 24, @ExpenseCategoryId, 'Recreation', 'Recreation', 14

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 25, @ExpenseCategoryId, 'Club/Union Dues', 'ClubUnionDues', 15

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 26, @ExpenseCategoryId, 'Gifts', 'Gifts', 16

	INSERT INTO dbo.ExpenseCode (ExpenseCodeId, ExpenseCategoryId, Name, LookupCode, DisplayOrder)
		SELECT 27, @ExpenseCategoryId, 'Miscellaneous', 'Miscellaneous', 17
END
GO
