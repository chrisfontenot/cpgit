﻿-- =============================================
-- Script Template
-- =============================================

IF (NOT EXISTS(SELECT TOP 1 PreFilingActionPlanId FROM dbo.PreFilingActionPlan))
BEGIN
	-- Solo Primary Snapshots
	INSERT INTO dbo.PreFilingActionPlan(PrimarySnapshotId, SecondarySnapshotId)
		SELECT
			PS.PreFilingSnapshotId, NULL
		FROM
			dbo.PreFilingSnapshot PS
			JOIN dbo.UserDetail PD ON PD.UserDetailId = PS.UserDetailId
				AND PD.IsPrimary = 1
			LEFT JOIN dbo.UserDetail SD ON SD.UserId = PD.UserId
				AND SD.IsPrimary = 0
		WHERE
			SD.UserDetailId IS NULL

	-- Primary Snapshots with a Secondary Snapshot on the same day
	INSERT INTO dbo.PreFilingActionPlan(PrimarySnapshotId, SecondarySnapshotId)
		SELECT
			PS.PreFilingSnapshotId,  SS.PreFilingSnapshotId
		FROM
			dbo.PreFilingSnapshot PS
			JOIN dbo.UserDetail PD ON PD.UserDetailId = PS.UserDetailId
				AND PD.IsPrimary = 1
			JOIN dbo.UserDetail SD ON SD.UserId = PD.UserId
				AND SD.IsPrimary = 0
			JOIN dbo.PreFilingSnapshot SS ON SS.UserDetailId = SD.UserDetailId
				AND DATEADD(dd, DATEDIFF(dd, 0, PS.CertificateGenerated), 0) = DATEADD(dd, DATEDIFF(dd, 0, SS.CertificateGenerated), 0)

	-- Secondary Snapshot with a Primary Snapshot on a previous day, but within the last 6 months.
	INSERT INTO dbo.PreFilingActionPlan(PrimarySnapshotId, SecondarySnapshotId)
		SELECT
			PS.PreFilingSnapshotId,  SS.PreFilingSnapshotId
		FROM
			dbo.PreFilingSnapshot PS
			JOIN dbo.UserDetail PD ON PD.UserDetailId = PS.UserDetailId
				AND PD.IsPrimary = 1
			JOIN dbo.UserDetail SD ON SD.UserId = PD.UserId
				AND SD.IsPrimary = 0
			JOIN dbo.PreFilingSnapshot SS ON SS.UserDetailId = SD.UserDetailId
				AND DATEADD(dd, DATEDIFF(dd, 0, SS.CertificateGenerated), 0) > DATEADD(dd, DATEDIFF(dd, 0, PS.CertificateGenerated), 0)
				AND DATEDIFF(mm, DATEADD(dd, DATEDIFF(dd, 0, PS.CertificateGenerated), 0), DATEADD(dd, DATEDIFF(dd, 0, SS.CertificateGenerated), 0)) <= 6

	-- Primary Snapshot with a Secondary Snapshot on a previous day, but within the last 6 months.
	INSERT INTO dbo.PreFilingActionPlan(PrimarySnapshotId, SecondarySnapshotId)
		SELECT
			PS.PreFilingSnapshotId,  SS.PreFilingSnapshotId
		FROM
			dbo.PreFilingSnapshot PS
			JOIN dbo.UserDetail PD ON PD.UserDetailId = PS.UserDetailId
				AND PD.IsPrimary = 1
			JOIN dbo.UserDetail SD ON SD.UserId = PD.UserId
				AND SD.IsPrimary = 0
			JOIN dbo.PreFilingSnapshot SS ON SS.UserDetailId = SD.UserDetailId
				AND DATEADD(dd, DATEDIFF(dd, 0, PS.CertificateGenerated), 0) > DATEADD(dd, DATEDIFF(dd, 0, SS.CertificateGenerated), 0)
				AND DATEDIFF(mm, DATEADD(dd, DATEDIFF(dd, 0, SS.CertificateGenerated), 0), DATEADD(dd, DATEDIFF(dd, 0, PS.CertificateGenerated), 0)) <= 6
END