﻿-- =============================================
-- Script Template
-- =============================================

IF (NOT EXISTS(SELECT TOP 1 PreFilingSnapshotId FROM dbo.PreFilingSnapshot))
BEGIN
	INSERT INTO dbo.PreFilingSnapshot(UserDetailId, ClientNumber, LanguageCode, CertificateNumber, FirstName,
		MiddleName, LastName, JudicialDistrict, CourseCompleted, CertificateGenerated, CertificateVersionType,
		SsnLast4, CreditScore, StreetLine1, StreetLine2, City, State, PostalCode, IncomeAfterTaxes, LivingExpenses,
		DebtPayment, CashAtMonthEnd, TotalAssets, TotalLiabilities, NetWorth, Reason, CounselorName, CounselorTitle,
		ArchiveStatus)
	SELECT
		C.UserDetailId, C.ClientNumber, U.LanguageCode, C.CertificateNumber, C.FirstName,
		C.MiddleInitial, C.LastName, C.JudicialDistrict, C.CourseCompleted, C.CertificateGenerated, C.CertificateVersionType,
		C.SsnLast4, NULL, C.Address1, C.Address2, C.City, C.State, C.PostalCode, C.IncomeAfterTaxes, C.LivingExpenses,
		C.DebtPayment, C.CashAtMonthEnd, C.TotalAssets, C.TotalLiabilities, C.NetWorth, C.Reason, 'Brian Young', 'Vice President of Counseling',
		C.ArchiveStatus
	FROM
		dbo.CounselingCertificate C
		JOIN dbo.UserDetail D ON D.UserDetailId = C.UserDetailId
		JOIN dbo.[User] U ON U.UserId = D.UserId
END