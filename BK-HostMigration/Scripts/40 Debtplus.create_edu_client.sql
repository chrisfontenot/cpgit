
/****** Object:  StoredProcedure [dbo].[create_edu_client]    Script Date: 4/4/2017 4:02:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[create_edu_client]
( 
	@clientNumber as varchar(150),
	@accountNumber as bigint,
	@fees as money = null,
	@certificateNumber varchar(50),
	@paymentType as int = null,
	@feeWaiverType as int = null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @primaryFirstName varchar(50)
			,@primaryMiddleName varchar(50)
			,@primaryLastName varchar(50)
			--,@primaryNameID int
			,@address varchar(100)
			,@city varchar(50)
			,@state varchar(50)
			,@zipcode varchar(10)
			,@addressid int
			,@attn varchar(50)
			,@phone varchar(20)
			,@phoneid int
			,@activestatuscode varchar(10)
			,@officeid int
			,@peoplecount int
			,@gender1 varchar(10)
			,@gender2 varchar(10)
			,@birthdate1 datetime
			,@birthdate2 datetime
			,@grossincome1 varchar(10)
			,@grossincome2 varchar(10)
			,@netincome1 varchar(10)
			,@netincome2 varchar(10)
			,@languageid int
			,@race varchar(10)
			--,@alimony varchar(15)
			--,@childsupport varchar(15)
			--,@socialsecurity varchar(15)
			--,@savings varchar(15)
			--,@rentalincome varchar(15)
			--,@selfemployment varchar(15)
			--,@parttime varchar(15)
			--,@foodstamp varchar(15)
			--,@otherincome varchar(15)
			--,@ethnicity varchar(15)
			,@maritalstatus varchar(5)
			,@grossincome1out MONEY
			,@grossincome2out MONEY
			,@netincome1out MONEY
			,@netincome2out MONEY
			--,@counselor varchar(50)
			,@counselorid int
			,@maritalstatusid INT
			,@programmonths INT
			,@estatement INT
			,@ecorrespondence INT
			,@emailaddress varchar(50)
			,@hasspouse int
			,@householdid int
			,@femaleheaded varchar(10)
			,@dependents int
			,@defaultcounty int
			,@defaultregion int
			,@activestatusdate datetime
			,@defaultpreferrredcontact int
			,@clientid int
			--,@oldclientid varchar(20)
			,@applicantnameid int
			,@emailid int
			--,@educationcode varchar(15)
			,@ssn1 varchar(10)
			,@ssn2 varchar(10)
			,@defaultmilitarystatus INT
			,@defaultmilitaryservice INT
			,@defaultmilitarygrade int
			,@defaultmilitarydependent int
			,@defaultfrequency int
			,@defaultnoficoscore int
			,@defaultdisabledtype int
			,@relationself int
			,@raceid int
			,@defaultrace int
			,@defaultethnicitytype int
			,@ethnicityid int
			,@defaulteducation int
			,@secondaryfirstname varchar(20)
			,@secondarymidname varchar(20)
			,@secondarylastname varchar(20)
			,@coapplicantnameid int
			--,@servicerid int
			,@educationid int
			,@defaultrelation int
			,@referredby int
			,@ficoscore decimal
			,@financialproblemcause int
			,@firstcontactid int
			--,@carval varchar(10)
			--,@homeval varchar(10)
			--,@otherval varchar(10)
			--,@homemaintenance varchar(10)
		 --   ,@medicalinsurance varchar(10)
		 --   ,@vehiclepayment varchar(10)
		 --   ,@carinsurance varchar(10)
		 --   ,@utilities varchar(10) -- ??
		 --   ,@telephone varchar(10)
		 --   ,@grocery varchar(10)
		 --   ,@foodaway varchar(10)
		 --   ,@carmaintenance varchar(10)
		 --   ,@publictransportation varchar(10)
		 --   ,@medicalprescription varchar(10)
		 --   ,@miscellaneous varchar(10)
		 --   ,@education varchar(10)
		 --   ,@childsupportalimony varchar(10)
		 --   ,@laundry varchar(10)
		 --   ,@clothing varchar(10)
		 --   ,@beautybarber varchar(10)
		 --   ,@personalexpense varchar(10)
		 --   ,@clubdues varchar(10)
		 --   ,@contributions varchar(10)
		 --   ,@recreation varchar(10)
		 --   ,@gifts varchar(10)
		 --   ,@exp_savings varchar(10)
		 --   ,@trash varchar(10)
		 --   ,@gas varchar(10)
			,@FirstNameOnCard varchar(50)
			,@MiddleNameOnCard varchar(50)
			,@LastNameOnCard varchar(50)
			,@DCAccountNumber varchar(20)
			,@ExpiryDate varchar(10)
			,@BillAddress varchar(100)
			,@BillCity varchar(50)
			,@BillState varchar(50)
			,@BillZip varchar(10)
			,@person1 int
			,@person2 int
			--,@servicer varchar(50)
			--,@servicerLoan varchar(50)
			--,@moPastDue int
			,@clientFee money
			,@firmid varchar(50)
			,@vendorid int
			,@productid int
			,@clienttype varchar(10)
			--,@vendorproductid int
			,@clientproductid int
			,@isdebitcardpayment bit
			,@abanumber varchar(10)
			--,@PROBONO int
			--,@INCOME int
			--,@SSDI int
			--,@LEGAL_AID int
			--,@TO_BE_DECIDED int
			--,@productDisputeType int
			,@isachpayment bit
			,@tenderedAmount money
			,@discountAmount money
			,@feeWaiverText varchar(50)
			,@ethnicity varchar(10)
			,@caseNumber varchar(50)
			,@bankruptcyChapter varchar(50)
			,@bankruptcyChapterID varchar(50)

	SELECT @activestatuscode = 'I'
		  ,@hasspouse = 0
		  ,@femaleheaded = 'N'
		  ,@dependents = 0
		  ,@defaultcounty = 1
		  ,@defaultregion = 7
		  ,@activestatusdate = '1/1/1901'
		  ,@defaultpreferrredcontact = 8
		  ,--  [unknown] 
	       @defaultmilitarystatus = 0
		  ,-- [Not In Military]
		   @defaultmilitaryservice = 0
		  ,-- [N/A]
		   @defaultmilitarygrade = 0
		  ,-- [N/A]
		   @defaultmilitarydependent = 0
		  ,-- Unspecified
		   @defaultfrequency = 4
		  ,-- [N/A]
		   @defaultnoficoscore = 3
		  ,-- type 2 [unspecified]
		   @defaultdisabledtype = 0
		  ,@relationself = 1
		  , -- refused to disclose
		   @defaultrace = 15
		  ,@defaultethnicitytype = 2
		  ,-- Unknown education
		  @defaulteducation = 1
		  ,@defaultrelation = 0
		  ,@financialproblemcause = 160
		  , --Not Specified
		  @firstcontactid = 1
		  ,-- Internet Counselor
		  @counselorid = 1820
		  ,@isdebitcardpayment = 0
		  ,@clientFee = @fees
		  ,@clienttype='BKE' -- Bankruptcy Education
		  ,@tenderedAmount = 0
		  ,@isachpayment = 0
		  ,@discountAmount = 0
	-- set office to atlanta branch
	SELECT @officeid = 354

	print 'get client details'

	select @primaryFirstName = FNAME
	       ,@primaryMiddleName = null
		   ,@primaryLastName = LNAME
		   ,@address  = [Addr]
		   ,@city = CITY
		   ,@state = [STATE]
		   ,@zipcode = ZIP
		   ,@phone = DayPhone
		   ,@emailaddress = EMAIL
		   --,@birthdate1 = CASE WHEN [DOB] IS NULL THEN NULL WHEN len([DOB]) = 0 THEN NULL WHEN [DOB] LIKE '%|%' THEN CASE WHEN isdate(left([DOB], charindex('|', [DOB]) - 1)) = 1 THEN convert(DATETIME, left([DOB], charindex('|', [DOB]) - 1)) ELSE NULL END ELSE CASE WHEN isdate([DOB]) = 1 THEN convert(DATETIME, [DOB]) ELSE NULL END END
		   --,@birthdate2 = CASE WHEN [CO_DOB] IS NULL THEN NULL WHEN len(CO_DOB) = 0 THEN NULL WHEN CO_DOB LIKE '%|%' THEN CASE WHEN isdate(substring(CO_DOB, charindex('|', CO_DOB) + 1, len(CO_DOB))) = 1 THEN convert(DATETIME, substring(CO_DOB, charindex('|', CO_DOB) + 1, len(CO_DOB))) ELSE NULL END ELSE NULL END
		   --,@netincome1 = MONTHLY_NET_INCOME
		   --,@alimony = isnull(MONTHLY_ALIMONY_INCOME, '')
		   --,@childsupport = isnull([CHILD_SUPPORT], '')
		   --,@socialsecurity = isnull([MONTHLY_GOVTASSIST_INCOME], '')
		   --,@savings = isnull([SAVINGS], '')
		   --,@rentalincome = 0
		   --,@selfemployment = isnull(NULL, '')
		   --,@parttime = isnull([MONTHLY_PARTTIME_NET], '')
		   --,@foodstamp = 0
		   --,@otherincome = 0
		   --,@ethnicity = isnull(CONTACT_HISPANIC, '')
		   --,@educationcode = EDUCATION_LEVEL
		   ,@ssn1 = SSN
		   ,@ssn2 = COSSN
		   ,@secondaryfirstname = COFNAME
		   ,@secondarymidname = COMNAME
		   ,@secondarylastname = COLNAME
		   --,@carval = VAL_CAR
		   --,@homeval = VAL_HOME
		   --,@otherval = VAL_OTHER
		   --,@homemaintenance = HOME_MAINTENANCE
		   --,@medicalinsurance = INSURANCE
		   --,@vehiclepayment = VEHICLE_PAYMENTS
		   --,@carinsurance = CAR_INSURANCE
		   --,@utilities =  EQUITY_LOAN_TAX_INS
		   --,@telephone = TELEPHONE
		   --,@grocery = GROCERIES
		   --,@foodaway = FOOD_AWAY
		   --,@carmaintenance = CAR_MAINTENANCE
		   --,@publictransportation = PUBLIC_TRANSPORTATION
		   --,@medicalprescription = MEDICAL_PRESCRIPTION
		   --,@miscellaneous = MISC_EXP 
		   --,@education = EDUCATION
		   --,@childsupportalimony = CHILD_SUPPORT
		   --,@laundry = LAUNDRY
		   --,@clothing = CLOTHING
		   --,@beautybarber = BEAUTY_BARBER
		   --,@personalexpense = PERSONAL_EXP
		   --,@clubdues = CLUB_DUES
		   --,@contributions =  CONTRIBUTIONS
		   --,@recreation = RECREATION
		   --,@gifts = GIFTS
		   --,@exp_savings = SAVINGS
		   --,@trash = UTRASH
		   --,@gas = UGAS
		   ,@FirstNameOnCard = DCFNAMEONCARD
           ,@MiddleNameOnCard = DCMNAMEONCARD
           ,@LastNameOnCard = DCLNAMEONCARD
           ,@DCAccountNumber = DCACCTNUM
           ,@ExpiryDate = DCEXPIRYDATE
           ,@BillAddress = DCBILLADDR
           ,@BillCity = DCBILLCITY
           ,@BillState = DCBILLSTATE
           ,@BillZip = DCBILLZIP
		   --,@servicer = MORT_HOLDER
		   --,@servicerLoan = LOAN_NUMBER
		   --,@moPastDue = MOS_DELINQ
		   ,@firmid = FIRMCODE
		   --,@clienttype = CLIENT_TYPE
		   ,@abanumber = ABANUMBER
		   ,@accountnumber = ACCTNUMBER
		   ,@caseNumber = CaseNumber
		   ,@bankruptcyChapter = BankruptcyType
	  from WEB_WSCAM
	 where ClientNumber = @clientNumber;

	 select @peoplecount = CASE isnumeric(replace([NUMINHOUSE], '.', '')) WHEN 1 THEN replace([NUMINHOUSE], '.', '') ELSE 0 END
		   ,@gender1 = [sex]
		   ,@grossincome1 = GROSS
		   ,@race = RACE
		   ,@languageid = CASE [LANGUAGE] WHEN 'S' THEN 2 ELSE 1 END
		   ,@maritalstatus = isnull([MARITAL], '')
	   from WEB_WSDEMOG
	  where CLIENTNUMBER = @clientNumber

	 --select @grossincome2 = MONTHLY_GROSS_INCOME2
		--   ,@netincome2 = MONTHLY_NET_INCOME2
		--   ,@counselor = COUNSELOR_ID
	 -- from ContactDetails2
	 --where ID = @secondaryContactDetailID;

	--insert address
	IF len(@address) > 0 OR len(@city) > 0 OR len(@state) > 0 OR len(@zipcode) > 0
	BEGIN
		EXEC dbo.insert_address @address
			,null
			,null
			,@city
			,@state
			,@zipcode
			,@addressid OUTPUT
			,@attn OUTPUT
	END

	EXEC create_telephonenumber @phone
				,NULL
				,NULL
				,NULL
				,@phoneid OUTPUT


	PRINT 'processing gross income and net income'

	EXEC dbo.process_gross_net_income @grossincome1
                                     ,@netincome1
                                     ,@grossincome2
                                     ,@netincome2
                                     ,@grossincome1out OUTPUT
                                     ,@netincome1out OUTPUT
                                     ,@grossincome2out OUTPUT
                                     ,@netincome2out OUTPUT

    PRINT 'getting counselor id...'
	--SELECT @counselorid = CredAbilityInput.dbo.map_counselor(@counselor);

	--IF len(ltrim(rtrim(isnull(@maritalstatus, '')))) > 0
	--BEGIN
	--	IF @maritalstatus = 'N' OR @maritalstatus = 'O' OR @maritalstatus = 'Z'
	--	BEGIN
	--		-- if spouse name is provided set the marital status to married
	--		IF @hasspouse = 1
	--		BEGIN
	--			SELECT @maritalstatus = 'M'
	--		END
	--				-- otherwise set the marital status to single
	--		ELSE
	--		BEGIN
	--			SELECT @maritalstatus = 'S'
	--		END
	--	END
	--END

	PRINT 'getting marital status'

	SELECT @maritalstatusid = case @maritalstatus 
	                            when 'D' then 3
							    when 'M' then 2
							    when 'S' then 1
							    when 'W' then 4
							    when 'X' then 5
							  end;

	SELECT @programmonths = 0
		  ,@estatement = CASE ISNULL(@emailaddress, '0') WHEN '0' THEN 0 ELSE 1 END
		  ,@ecorrespondence = @estatement

	PRINT 'get household id...'

	SELECT @householdid = dbo.get_household_info(@femaleheaded, @gender1, @dependents, @hasspouse, @maritalstatus);

	
	print 'inserting client data...'

	INSERT INTO [clients] (
				[addressid]
				,[hometelephoneid]
				,[county]
				,[region]
				,[active_status]
				,[active_status_date]
				,[client_status]
				,[disbursement_date]
				,[start_date]
				,[referred_by]
				,[cause_fin_problem1]
				,[office]
				,[counselor]
				,[hold_disbursements]
				,[personal_checks]
				,[ach_active]
				,[stack_proration]
				,[mortgage_problems]
				,[intake_agreement]
				,[electroniccorrespondence]
				,[electronicstatements]
				,[bankruptcy_class]
				,[fed_tax_owed]
				,[state_tax_owed]
				,[local_tax_owed]
				,[fed_tax_months]
				,[state_tax_months]
				,[local_tax_months]
				,[held_in_trust]
				,[deposit_in_trust]
				,[reserved_in_trust]
				,[marital_status]
				,[language]
				,[dependents]
				,[household]
				,[method_first_contact]
				,[config_fee]
				,[first_appt]
				,[first_kept_appt]
				,[first_resched_appt]
				,[program_months]
				,[preferred_contact]
				,[created_by]
				,[date_created]
				,[people]
				,[payout_total_debt]
				,[payout_months_to_payout]
				,[payout_total_fees]
				,[payout_total_interest]
				,[payout_total_payments]
				,[payout_deposit_amount]
				,[payout_monthly_fee]
				,[payout_cushion_amount]
				)
			VALUES (
				 @addressid
				,@phoneid
				,@defaultcounty
				,@defaultregion
				,@activestatuscode
				,@activestatusdate
				,'BK'
				,1
				,NULL
				,@referredby
				,@financialproblemcause
				,@officeid
				,@counselorid
				,0
				,0
				,0
				,0
				,0
				,0
				,@ecorrespondence
				,@estatement
				,0
				,0.00
				,0.00
				,0.00
				,0
				,0
				,0
				,0
				,0.00
				,0.00
				,@maritalstatusid
				,@languageid
				,@dependents
				,@householdid
				,@firstcontactid
				,1.11
				,NULL
				,NULL
				,NULL
				,@programmonths
				,@defaultpreferrredcontact
				,'Bankruptcy'
				,getdate()
				,@peoplecount
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				);

	set @clientid = SCOPE_IDENTITY();

	insert into client_addkeys(client, additional, date_created, created_by) values (@clientid, @clientNumber, getdate(), 'Bankruptcy')

	PRINT 'PEOPLE TABLE...'
	PRINT 'gettig applicant name id'
	EXEC dbo.create_name @primaryFirstName
                        ,@primaryMiddleName
                        ,@primaryLastName
                        ,@applicantnameid OUTPUT;

	PRINT 'getting mail id'

	EXEC dbo.create_email @emailaddress, @emailid OUTPUT;

	--SELECT @educationid = CredAbilityInput.dbo.map_education(@educationcode)

	-- remove dash from ssn
	if @ssn1 is not null and len(@ssn1) > 1 
	begin

		select @ssn1 = replace(@ssn1, '-', '')

	end

	select @raceid = case @race when 'W' then 3 when 'X' then 2 when 'A' then 1 when 'M' then 17 end
	select @ethnicityid = case @ethnicity when 1 then 0 when 2 then 1 else null end

	if not @bankruptcyChapter is null and @bankruptcyChapter <> ''
	begin
		select @bankruptcyChapterID = case @bankruptcyChapter when '7' then 1 when '13' then 2 else null end;
	end

	print 'insert people table relation 1'
	INSERT INTO [people] (
		[Client]
		,[Relation]
		,[NameID]
		,[EmailID]
		,[SSN]
		,[Gender]
		,[Race]
		,[Ethnicity]
		,[Disabled]
		,[Education]
		,[Birthdate]
		,[FICO_Score]
		,[CreditAgency]
		,[no_fico_score_reason]
		,[gross_income]
		,[net_income]
		,[Frequency]
		,[created_by]
		,[date_created]
		,MilitaryStatusID
		,[MilitaryServiceID]
		,[MilitaryGradeID]
		,[MilitaryDependentID]
		,[bkCaseNumber]
		,bkchapter
		)
	VALUES (
		@clientid
		,@relationself
		,@applicantnameid
		,@emailid
		,CASE @ssn1 WHEN '' THEN NULL WHEN NULL THEN NULL ELSE CASE len(@ssn1) WHEN 9 THEN @ssn1 ELSE NULL END END
		,case @gender1 when 'M' then 1 when 'F' then 2 else 1 end --isnull(@gender1, 1)
		,isnull(@raceid, @defaultrace)
		,isnull(@ethnicityid, @defaultethnicitytype)
		,@defaultdisabledtype
		,isnull(@educationid, @defaulteducation)
		,@birthdate1
		,isnull(@ficoscore, 0)
		,'EQUIFAX'
		,@defaultnoficoscore
		,@grossincome1out
		,@netincome1out
		,@defaultfrequency
		,'Bankruptcy'
		,getdate()
		,@defaultmilitarystatus
		,@defaultmilitaryservice
		,@defaultmilitarygrade
		,@defaultmilitarydependent
		,@caseNumber
		,@bankruptcyChapterID
		);

		set @person1 = SCOPE_IDENTITY();

		EXEC create_name @secondaryfirstname
					    ,@secondarymidname
					    ,@secondarylastname
					    ,@coapplicantnameid OUTPUT;

		if not @coapplicantnameid is null
		begin

			PRINT 'inserting co-applicant record'
				PRINT 'ssn2 : ' + CASE @ssn2 WHEN '' THEN 'null' WHEN NULL THEN 'null' ELSE substring(@ssn2, 1, 9) END
				
				INSERT INTO [people] (
					[client]
					,[relation]
					,[nameid]
					,[emailid]
					,[former]
					,[ssn]
					,[gender]
					,[race]
					,[ethnicity]
					,[disabled]
					,[education]
					,[birthdate]
					,[fico_score]
					,[creditagency]
					,[no_fico_score_reason]
					,[gross_income]
					,[net_income]
					,[frequency]
					,[created_by]
					,[date_created]
					,MilitaryStatusID
					,[MilitaryServiceID]
					,[MilitaryGradeID]
					,[MilitaryDependentID]
					)
				VALUES (
					@clientid
					,@defaultrelation
					,@coapplicantnameid
					,NULL
					,NULL
					,CASE @ssn2 WHEN '' THEN NULL WHEN NULL THEN NULL ELSE CASE len(@ssn2) WHEN 9 THEN @ssn2 ELSE NULL END END
					,isnull(@gender2, 1)
					,@defaultrace
					,@defaultethnicitytype
					,@defaultdisabledtype
					,@defaulteducation
					,@birthdate2
					,0
					,'EQUIFAX'
					,@defaultnoficoscore
					,@grossincome2out
					,@netincome2out
					,@defaultfrequency
					,'Bankruptcy'
					,getdate()
					,@defaultmilitarystatus
					,@defaultmilitaryservice
					,@defaultmilitarygrade
					,@defaultmilitarydependent
					);

					set @person2 = SCOPE_IDENTITY();

					
		end
		
		
		if @DCAccountNumber is not null and @DCAccountNumber <> '' 
		begin
		
			select @isdebitcardpayment = 1
		end

		-- if the payment is not made by debt card and has valid aba number and bank account
		if @isdebitcardpayment = 0 and @abanumber is not null and @abanumber <> '' and @accountnumber is not null and @accountnumber <> ''
		begin
			select @isachpayment = 1
		end

		if @firmid is not null and @firmid <> ''
		begin
		
			if @clienttype is not null and @clienttype <> ''
			begin

				select @productid = case @clienttype when 'BK' then 1 when 'BKE' then 2 else null end

				select @vendorid = oid from vendors where oid = @firmid;
				
				if @vendorid is not null
				begin
				
					if @feewaiverType is not null
					begin

						select @feeWaiverText = case @feeWaiverType
														when 1 then 'INCOME' -- income
														when 2 then 'SSDI' -- SSDI
														when 3 then 'PROBONO' -- Pro Bono
														when 5 then 'LEGAL_AID' -- Legal Aid
														else 'UNKNOWN'
														end;
					end

					if @isdebitcardpayment = 1 or @isachpayment = 1
					begin
						select @tenderedAmount = @clientFee
					end

					if @feeWaiverType is not null and @feeWaiverType > 0
					begin
						select @discountAmount = @clientFee
					end

					INSERT INTO [dbo].[client_products]([client], [vendor], [product], [cost], [discount], [tendered], [date_created], [created_by], InNumber, PrimaryCertificate, PaymentType, FeeWaiverType)
						VALUES (@clientid, @vendorid, @productid, @clientFee, @discountAmount, @tenderedAmount, getdate(), 'Bankruptcy', @clientNumber, @certificateNumber, @paymentType, @feeWaiverType)

					select @clientproductid = SCOPE_IDENTITY()

					INSERT INTO [dbo].[product_transactions]([vendor],[product],[client_product],[transaction_type],[cost],[discount],[payment],[disputed],[note],[date_created],[created_by])
					VALUES(@vendorid, @productid, @clientproductid, 'BB', @clientFee, 0, 0, 0, 'Bankruptcy Transaction', getdate(), 'Bankruptcy')

					-- if there is a fee waiver create transaction with Transaction Type 'DC' Discount Amount
					if (@feeWaiverType is not null and @feeWaiverType > 0)
					begin
						INSERT INTO [dbo].[product_transactions]([vendor], [product], [client_product], [transaction_type], [cost], [discount], [payment], [disputed], [note], [date_created], [created_by])
						VALUES(@vendorid, @productid, @clientproductid, 'DC', 0, @discountAmount, 0, 0, 'Fee Waiver Type: ' + isnull(@feeWaiverText, ''), getdate(), 'Bankruptcy')
					end

					-- Add another transaction for DebitCard payment since there is confirmation on the payment with Tranaction type 'DC'
					if @isdebitcardpayment = 1
					begin
						INSERT INTO [dbo].[product_transactions]([vendor], [product], [client_product], [transaction_type], [cost], [discount], [payment], [disputed], [note], [date_created], [created_by])
						VALUES(@vendorid, @productid, @clientproductid, 'RC', 0, 0, @clientFee, 0, null, getdate(), 'Bankruptcy')
					end
					
					if @isdebitcardpayment = 1
					begin
						print 'insert credit card payment details...'
						print 'client: ' + convert(varchar, isnull(@clientid, 0))
						print 'client_product: ' + convert(varchar, isnull(@clientproductid, 0))
						print '[payment_medium]: ' + 'CreditCard'
						print '[account_number]: ' + isnull(@DCAccountNumber, '')
						print '[expiration_date]: ' + isnull(@ExpiryDate, '01/01/1901')
						print '[credit_card_type]: ' + isnull(null, '<null>')
						print '[card_status_flag]: ' + 'Present'
						print '[amount]: ' + convert(varchar, isnull(@clientFee, '0.0'))
						print 'avs_full_name: ' + isnull(@FirstNameOnCard + ' ' + @LastNameOnCard, '')
						print '[avs_address]: ' + isnull(@BillAddress, '')
						print '[avs_city]: ' + isnull(@BillCity, '')
						print '[avs_state]: ' + isnull(@BillState, '')
						print '[avs_zipcode]: ' + isnull(@BillZip, '')
						print '[@phone]: ' + isnull(@phone, '')
						print '[created_by]: ' + 'Bankruptcy'
						insert into [credit_card_payment_details] (
								client
								,client_product
								, [payment_medium]
								,[account_number]
								,[expiration_date]
								,[credit_card_type]
								,[card_status_flag]
								,[amount]
								,[avs_full_name]
								,[avs_address]
								,[avs_city]
								,[avs_state]
								,[avs_zipcode]
								,[avs_phone]
								,[settlement_submission_date]
								,[submission_date]
								,[date_created]
								,[created_by])
						select 
								@clientid
								,@clientproductid
								,'CreditCard'
								, null--@DCAccountNumber -- Not setting the Account number 
								,null--case @ExpiryDate when null then null when '' then null else convert(datetime, '1/' + @ExpiryDate, 3) end -- convert d/mm/yy to datetime
								,NULL
								,'Present'
								,@clientFee
								,@FirstNameOnCard + ' ' + @LastNameOnCard
								,@BillAddress
								,@BillCity
								,@BillState
								,@BillZip
								,case @phone when null then null when '' then null else replace(replace(replace(replace(@phone, '-', ''), '(', ''), ')', ''), ' ', '') end
								, getdate() -- settlement submission date
								, getdate() -- submission date
								, getdate()
								, 'Bankruptcy'
					end

					if @isachpayment = 1
					begin

						INSERT INTO [dbo].[ach_onetimes]([client]
										,bank
										,[ABA]
										,[AccountNumber]
										,[CheckingSavings]
										,[Amount]
										,[batch_type]
										,[EffectiveDate]
										,[client_product]
										,[date_created]
										,[created_by])
									VALUES
										(@clientid
										, 85 -- For Bankruptcy and Credit Card Payment BOA Ach Products
										,@abanumber
										,@accountnumber
										,'C'
										,@clientfee
										,'WEB'
										,getdate()
										,@clientproductid
										,getdate()
										,'Bankruptcy')

					end

				end

			end

		end

		--print 'insert client housing'
		--INSERT INTO client_housing (
		--	client
		--	,housing_status
		--	,HUD_fair_housing
		--	,HUD_colonias
		--	,HUD_migrant_farm_worker
		--	,HUD_predatory_lending
		--	,HUD_FirstTimeHomeBuyer
		--	,HUD_DiscriminationVictim
		--	,HUD_LoanScam
		--	,OtherHUDFunding
		--	,NFMCP_level
		--	,NFMCP_privacy_policy
		--	,NFMCP_decline_authorization
		--	,HUD_home_ownership_voucher
		--	,HUD_housing_voucher
		--	,housing_type
		--	,HUD_grant)
		--SELECT @clientid
		--	,4
		--	,0
		--	,0
		--	,0
		--	,0
		--	,0
		--	,0
		--	,0
		--	,0.00
		--	,''
		--	,0
		--	,0
		--	,0
		--	,0
		--	,8
		--	,0;

		---- get servicer
		--declare @calcServicerId int 
		--set @calcServicerId = COALESCE((select top 1 oID from Housing_lender_servicers [servicers] 
  --      where servicers.[description] = @servicer),442)

		--declare @calcServicerName varchar(50)
		--set @calcServicerName = case when @calcServicerId = 442 then 'Other or Unknown' else @servicer end

		--declare @property int
		--set @property = NULL
		
		--print 'insert housing properties'
		--insert into Housing_properties(OwnerID, CoOwnerID, UseHomeAddress, HousingID, Residency, PropertyType)
		--values(@person1, @person2, 1, @clientid, 0, 8)
  
		--set @property = SCOPE_IDENTITY()

		--print 'insert housing lenders'
		--declare @lender int
		--set @lender = NULL
		--insert into Housing_lenders(ServicerName, ServicerID, AcctNum, InvestorID, InvestorAccountNumber)
		--values(@calcServicerName
		--     , @calcServicerId
		--	 , @servicerLoan -- Servicer Loan ??
		--	 , NULL -- Investor ??
		--	 , NULL -- Investor Number ??
		--	 )
		--set @lender = SCOPE_IDENTITY()

		--print 'housing loans'
		--declare @loan int
		--set @loan = NULL
		--insert into Housing_loans(PropertyID,CurrentLenderID,UseOrigLenderID,IntakeSameAsCurrent,Loan1st2nd,UseInReports,LoanDelinquencyMonths)
		--values(@property,@lender,0,1,1,1,@moPastDue)
		--set @loan = SCOPE_IDENTITY() 

		---- ASSET TABLE
		--	PRINT 'inserting data to asset table'

		--	EXEC dbo.create_asset @clientid
		--		,@alimony
		--		,@childsupport
		--		,@socialsecurity
		--		,@savings
		--		,@selfemployment
		--		,@parttime
		--		,@foodstamp
		--		,@otherincome
		--		,@rentalincome

		

	--print 'insert secured properties'
	---- insert secured properties
	--EXEC create_bk_secured_properties @clientid, @carval, @homeval, @otherval

	--print 'insert budgets'
	--exec insert_bk_budgets @clientid
	--					  ,@homemaintenance
	--					  ,@medicalinsurance
	--					  ,@vehiclepayment
	--					  ,@carinsurance
	--					  ,@utilities
	--					  ,@telephone
	--					  ,@grocery
	--					  ,@foodaway
	--					  ,@carmaintenance
	--					  ,@publictransportation
	--					  ,@medicalprescription
	--					  ,@miscellaneous
	--					  ,@education
	--					  ,@childsupportalimony
	--					  ,@laundry
	--					  ,@clothing
	--					  ,@beautybarber
	--					  ,@personalexpense
	--					  ,@clubdues
	--					  ,@contributions
	--					  ,@recreation
	--					  ,@gifts
	--					  ,@exp_savings
	--					  ,@trash
	--					  ,@gas

	-- Insert Client IN Number mapping
	--print 'insert client IN Number mapping'
	--INSERT INTO [dbo].[ClientInNumberMapping]([ClientID], [InNumber])
    -- VALUES (@clientID, @clientNumber)
				
END
