USE [cccsIdentity]
GO

/****** Object:  Table [dbo].[PreDischargeCertificate]    Script Date: 3/3/2017 12:23:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PreDischargeCertificate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[CertificateNumber] [varchar](50) NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PreDischangeCertificate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


