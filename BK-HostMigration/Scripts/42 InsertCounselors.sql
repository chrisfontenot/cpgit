insert into names (First, Last) values ('Test', 'Counselor');
insert into EmailAddresses (Address) values ('nandakishore.sharma@infocalyx.com');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Test' and last='Counselor'), (select top(1) Email from emailaddresses where address='nandakishore.sharma@infocalyx.com'), 'cccsatl\nsharma1', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('Greg', 'Leonard');
insert into EmailAddresses (Address) values ('Gregory.Leonard@Clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Greg' and last='Leonard'), (select top(1) Email from emailaddresses where address='Gregory.Leonard@Clearpointccs.org'), 'cccsatl\gLeonard', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('MeekaAila', 'Adams');
insert into EmailAddresses (Address) values ('MeekaAila.Adams@Clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='MeekaAila' and last='Adams'), (select top(1) Email from emailaddresses where address='MeekaAila.Adams@Clearpointccs.org'), 'cccsatl\mAdams', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('Joji', 'Varghese');
insert into EmailAddresses (Address) values ('Joji.Varghese@Clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Joji' and last='Varghese'), (select top(1) Email from emailaddresses where address='Joji.Varghese@Clearpointccs.org'), 'cccsatl\jVarghese', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('Curtrina', 'Mapp');
insert into EmailAddresses (Address) values ('Curtrina.Mapp@Clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Curtrina' and last='Mapp'), (select top(1) Email from emailaddresses where address='Curtrina.Mapp@Clearpointccs.org'), 'cccsatl\cMapp', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('Rita', 'Ramos');
insert into EmailAddresses (Address) values ('Rita.Ramos@Clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Rita' and last='Ramos'), (select top(1) Email from emailaddresses where address='Rita.Ramos@Clearpointccs.org'), 'cccsatl\rRamos', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

insert into names (First, Last) values ('Test', 'Counselor2');
insert into EmailAddresses (Address) values ('varsha.nair@clearpointccs.org');
insert into counselors(nameid, emailid, person, [default], ActiveFlag, office, menu_level, billing_mode, emp_start_date, date_created, created_by) values
((select top(1) name from names where first='Test' and last='Counselor2'), (select top(1) Email from emailaddresses where address='varsha.nair@clearpointccs.org'), 'cccsatl\vnair1', 0, 1, 99, 3, 'FIXED', getdate(), getdate(), 'Bankruptcy')

--Test Counselor	nandakishore.sharma@gmail.com
--Greg Leonard	Gregory.Leonard@Clearpointccs.org
--MeekaAila Adams	MeekaAila.Adams@Clearpointccs.org
--Joji Varghese	Joji.Varghese@Clearpointccs.org
--Curtrina Mapp	Curtrina.Mapp@Clearpointccs.org
--Rita Ramos	Rita.Ramos@Clearpointccs.org
--Test Counselor2	varsha.nair@clearpointccs.org