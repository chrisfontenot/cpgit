SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[create_email](
	@email	varchar(100),
	@emailid	int output
) as 
begin

select @email = case @email when 'none' then 'NOT SPECIFIED'  when '' then 'NOT SPECIFIED'  else @email end;

insert into emailaddresses(address, validation)
		select @email, case @email when 'none' then 3 when '' then 3 else 1 end;

	select @emailid = scope_identity();

end;


