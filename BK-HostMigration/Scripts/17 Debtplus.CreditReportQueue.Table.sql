
/****** Object:  Table [dbo].[CreditReportQueue]    Script Date: 2/20/2017 6:51:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CreditReportQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FinalID] [varchar](10) NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[SSN] [varchar](20) NULL,
	[BirthDate] [datetime] NULL,
	[Street] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zipcode] [varchar](10) NULL,
	[IsPrimary] [bit] NOT NULL,
	[ClientNumber] [varchar](10) NULL,
	[BirthCity] [varchar](50) NULL,
 CONSTRAINT [PK_CreditReportQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

