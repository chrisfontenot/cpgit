
/****** Object:  Table [dbo].[WEB_WSTRANS]    Script Date: 1/24/2017 5:04:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WEB_WSTRANS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BillDate] [datetime] NULL,
	[PayType] [varchar](10) NULL,
	[BillAmount] [decimal](18, 0) NULL,
	[ClientNumber] [int] NULL,
	[TransReference] [varchar](20) NULL,
	[TransType] [varchar](10) NULL,
	[InvoiceNumber] [varchar](10) NULL,
 CONSTRAINT [PK_WEB_WSTRANS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


