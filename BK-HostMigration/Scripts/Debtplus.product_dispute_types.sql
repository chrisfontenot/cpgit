
select * from product_dispute_types

set identity_insert product_dispute_types on;

if Not exists (select 1 from product_dispute_types where description = 'To Be Decided')
begin
	insert into product_dispute_types(Oid, [description], [activeflag], [default], [date_created], [created_by]) values (7, 'To Be Decided', 1, 0, getdate(), 'sa');
end
if Not exists (select 1 from product_dispute_types where description = 'Income')
begin
	insert into product_dispute_types(Oid, [description], [activeflag], [default], [date_created], [created_by]) values (8, 'Income', 1, 0, getdate(), 'sa');
end
if Not exists (select 1 from product_dispute_types where description = 'SSDI')
begin
	insert into product_dispute_types(Oid, [description], [activeflag], [default], [date_created], [created_by]) values (9, 'SSDI', 1, 0, getdate(), 'sa');
end
if Not exists (select 1 from product_dispute_types where description = 'Legal Aid')
begin
	insert into product_dispute_types(Oid, [description], [activeflag], [default], [date_created], [created_by]) values (10, 'Legal Aid', 1, 0, getdate(), 'sa');
end
if Not exists (select 1 from product_dispute_types where description = 'Good Will')
begin
	insert into product_dispute_types(Oid, [description], [activeflag], [default], [date_created], [created_by]) values (11, 'Good Will', 1, 0, getdate(), 'sa');
end
set identity_insert product_dispute_types off;