SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[create_asset](
	@clientid		int,
	@alimony		varchar(15),
	@childsupport	varchar(15),
	@socialsecurity	varchar(15),
	@savings		varchar(15),
	@selfemployment	varchar(15),
	@parttime		varchar(15),
	@foodstamp		varchar(15),
	@otherincome	varchar(15),
	@rentalincome   varchar(15)
) as 
begin

	declare	@childsupporttype	int
	select  @childsupporttype = 2

	declare	@socialsecuritytype	int
	select  @socialsecuritytype = 3

	declare	@foodstamptype	int
	select  @foodstamptype = 4

	declare	@selfemploymenttype	int
	select  @selfemploymenttype = 5

	declare	@parttimetype	int
	select  @parttimetype = 6

	declare	@alimonytype	int
	select  @alimonytype = 7

	declare	@otherincometype	int
	select  @otherincometype = 8

	declare	@savingstype	int
	select  @savingstype = 9

	declare	@rentaltype	int
	select  @rentaltype = 12

	declare @childsupport1		money
	declare @childsupport2		money
	declare @socialsecurity1	money
	declare @socialsecurity2	money
	declare @foodstamp1			money
	declare @foodstamp2			money
	declare @selfemployment1	money
	declare @selfemployment2	money
	declare @parttime1			money
	declare @parttime2			money
	declare @alimony1			money
	declare @alimony2			money
	declare @otherincome1		money
	declare @otherincome2		money
	declare @rentalincome1		money
	declare @rentalincome2		money
	declare @savings1			money
	declare @savings2			money

	declare @childsupportamount	money
	declare @socialsecurityamount	money
	declare @foodstampamount	money
	declare @selfemploymentamount	money
	declare @parttimeamount	money
	declare @alimonyamount	money
	declare @otherincomeamount	money
	declare @rentalincomeamount	money
	declare @savingsamount	money

	print 'insert asset : parse null data'
	select @childsupport   = ltrim(rtrim(replace(isnull(@childsupport,''), ' ', '')))
	select @socialsecurity = ltrim(rtrim(replace(isnull(@socialsecurity,''), ' ', '')))
	select @foodstamp      = ltrim(rtrim(replace(isnull(@foodstamp,''), ' ', '')))
	select @selfemployment = ltrim(rtrim(replace(isnull(@selfemployment,''), ' ', '')))
	select @parttime       = ltrim(rtrim(replace(isnull(@parttime,''), ' ', '')))
	select @alimony        = ltrim(rtrim(replace(isnull(@alimony,''), ' ', '')))
	select @otherincome    = ltrim(rtrim(replace(isnull(@otherincome,''), ' ', '')))
	select @savings        = ltrim(rtrim(replace(isnull(@savings,''), ' ', '')))
	select @rentalincome   = ltrim(rtrim(replace(isnull(@rentalincome,''), ' ', '')))

	--print 'Client ID        : ' + convert(varchar, @clientid)
	--print 'Child Support    : ' + isnull(@childsupport,'data is null')
	--print 'Social Security  : ' + isnull(@socialsecurity,'data is null')
	--print 'Food Stamp       : ' + isnull(@foodstamp,'data is null')
	--print 'Self Employment  : ' + isnull(@selfemployment,'data is null')
	--print 'Part Time        : ' + isnull(@parttime,'data is null')
	--print 'Alimony          : ' + isnull(@alimony,'data is null')
	--print 'Other Income     : ' + isnull(@otherincome,'data is null')
	--print 'Savings          : ' + isnull(@savings,'data is null')

	if @childsupport is not null and @childsupport != ''
	begin
		select @childsupportamount = isnull(@childsupport, 0.00)

		if @childsupportamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @childsupporttype, @childsupportamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Child Support data is empty'

	if @socialsecurity is not null and @socialsecurity != '' 
	begin
		select  @socialsecurityamount = isnull(@socialsecurity, 0.00)

		if @socialsecurityamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @socialsecuritytype, @socialsecurityamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Social Security data is empty'


	if @foodstamp != '' 
	begin
		select  @foodstampamount = isnull(@foodstamp, 0.00)

		if @foodstampamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @foodstamptype, @foodstampamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Food Stamp data is empty'

	if @selfemployment != ''
	begin
		select  @selfemploymentamount = isnull(@selfemployment, 0.00)

		if @selfemploymentamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @selfemploymenttype, @selfemploymentamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Self Employment data is empty'

	if @parttime != ''
	begin
		select  @parttimeamount = isnull(@parttime, 0.00)

		if @parttimeamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @parttimetype, @parttimeamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Part Time data is empty'

	if @alimony != ''
	begin
		select  @alimonyamount = isnull(@alimony, 0.00)

		if @alimonyamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @alimonytype, @alimonyamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Alimony data is empty'

	if @otherincome != ''
	begin
		select  @otherincomeamount = isnull(@otherincome, 0.00)

		if @otherincomeamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @otherincometype, @otherincomeamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Other Income data is empty'

	
	if @rentalincome != ''
	begin
		select  @rentalincomeamount = isnull(@rentalincome, 0.00)

		if @rentalincomeamount > 0.00
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @rentaltype, @rentalincomeamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Rental Income data is empty'

	if @savings != ''
	begin
		select  @savingsamount = isnull(@savings, 0.00)

		if @savingsamount > 0.00 
		begin
			insert into assets(client, asset_id, amount, frequency, date_created, created_by)
				   values(@clientid, @savingstype, @savingsamount, 4, getdate(), 'Bankruptcy')
		end
	end
	else print 'Savings data is empty'

end



