
/****** Object:  Table [dbo].[WEB_BRCAM]    Script Date: 1/16/2017 8:45:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WEB_BRCAM](
	[ID] [int] NOT NULL,
	[2BillFlag] [int] NULL,
	[Appr_Status] [int] NULL,
	[Appt_Date] [datetime] NULL,
	[BillAmt] [decimal](18, 0) NULL,
	[Cert_Iss_Date] [datetime] NULL,
	[Cert_No] [varchar](50) NULL,
	[DisputeCode] [varchar](50) NULL,
	[DisputeDate] [datetime] NULL,
	[FirmCode] [varchar](20) NULL,
	[IApptDate] [datetime] NULL,
	[ICert_Iss_Date] [datetime] NULL,
	[IPostDate] [datetime] NULL,
	[LFName] [varchar](50) NULL,
	[LName] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[PayType] [varchar](20) NULL,
	[PcpLocal] [varchar](10) NULL,
	[PostDate] [datetime] NULL,
	[SSN] [varchar](20) NULL,
	[Stat] [varchar](20) NULL,
	[SeqNo] [int] NULL,
	[ClientNumber] [varchar](20) NULL,
	[InvoiceNumber] [varchar](20) NULL,
	[IsPrimary] [bit] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


