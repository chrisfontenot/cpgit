/****** Object:  Table [dbo].[JudicialDistricts]    Script Date: 1/31/2017 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JudicialDistricts](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DistrictName] [varchar](100) NULL,
	[DistrictCode] [varchar](10) NULL,
 CONSTRAINT [PK_JudicialDistricts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[JudicialDistricts] ON 

INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (1, N'District of Alaska', N'1')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (2, N'District of Arizona', N'2')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (3, N'Eastern District of Arkansas', N'3')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (4, N'Western District of Arkansas', N'86')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (5, N'Central District of California', N'4')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (6, N'Eastern District of California', N'5')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (7, N'Northern District of California', N'6')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (8, N'Southern District of California', N'7')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (9, N'District of Colorado', N'8')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (10, N'District of Connecticut', N'9')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (11, N'District of Columbia', N'10')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (12, N'District of Delaware', N'11')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (13, N'Middle District of Florida', N'12')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (14, N'Northern District of Florida', N'13')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (15, N'Southern District of Florida', N'14')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (16, N'Middle District of Georgia', N'15')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (17, N'Northern District of Georgia', N'16')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (18, N'Southern District of Georgia', N'17')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (19, N'District of Guam', N'18')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (20, N'District of Hawaii', N'19')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (21, N'District of Idaho', N'20')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (22, N'Central District of Illinois', N'87')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (23, N'Northern District of Illinois', N'21')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (24, N'Southern District of Illinois', N'88')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (25, N'Northern District of Indiana', N'22')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (26, N'Southern District of Indiana', N'23')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (27, N'Northern District of Iowa', N'24')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (28, N'Southern District of Iowa', N'25')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (29, N'District of Kansas', N'26')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (30, N'Eastern District of Kentucky', N'27')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (31, N'Western District of Kentucky', N'28')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (32, N'Western District of Louisiana', N'31')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (33, N'Eastern District of Louisiana', N'29')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (34, N'Middle District of Louisiana', N'30')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (35, N'District of Maine', N'32')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (36, N'District of Maryland', N'33')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (37, N'District of Massachusetts', N'34')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (38, N'Eastern District of Michigan', N'35')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (39, N'Western District of Michigan', N'36')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (40, N'District of Minnesota', N'37')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (41, N'Northern District of Mississippi', N'38')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (42, N'Southern District of Mississippi', N'39')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (43, N'Eastern District of Missouri', N'40')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (44, N'Western District of Missouri', N'41')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (45, N'District of Montana', N'42')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (46, N'District of Nebraska', N'43')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (47, N'District of Nevada', N'44')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (48, N'District of New Hampshire', N'45')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (49, N'District of New Jersey', N'46')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (50, N'District of New Mexico', N'47')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (51, N'Eastern District of New York', N'48')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (52, N'Northern District of New York', N'49')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (53, N'Southern District of New York', N'50')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (54, N'Western District of New York', N'51')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (55, N'District of North Dakota', N'52')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (56, N'District of Northern Mariana Islands', N'53')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (57, N'Northern District of Ohio', N'54')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (58, N'Southern District of Ohio', N'55')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (59, N'Eastern District of Oklahoma', N'56')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (60, N'Northern District of Oklahoma', N'57')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (61, N'Western District of Oklahoma', N'58')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (62, N'District of Oregon', N'59')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (63, N'Eastern District of Pennsylvania', N'60')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (64, N'Middle District of Pennsylvania', N'61')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (65, N'Western District of Pennsylvania', N'62')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (66, N'District of Puerto Rico', N'63')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (67, N'District of Rhode Island', N'64')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (68, N'District of South Carolina', N'65')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (69, N'District of South Dakota', N'66')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (70, N'Eastern District of Tennessee', N'67')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (71, N'Middle District of Tennessee', N'68')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (72, N'Western District of Tennessee', N'69')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (73, N'Eastern District of Texas', N'70')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (74, N'Northern District of Texas', N'71')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (75, N'Southern District of Texas', N'72')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (76, N'Western District of Texas', N'73')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (77, N'District of Utah', N'74')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (78, N'District of Vermont', N'75')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (79, N'Eastern District of Virginia', N'77')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (80, N'Western District of Virginia', N'78')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (81, N'District of Virgin Islands', N'76')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (82, N'Eastern District of Washington', N'79')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (83, N'Western District of Washington', N'80')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (84, N'Northern District of West Virginia', N'81')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (85, N'Southern District of West Virginia', N'82')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (86, N'Eastern District of Wisconsin', N'83')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (87, N'Western District of Wisconsin', N'84')
INSERT [dbo].[JudicialDistricts] ([ID], [DistrictName], [DistrictCode]) VALUES (88, N'District of Wyoming', N'85')
SET IDENTITY_INSERT [dbo].[JudicialDistricts] OFF
