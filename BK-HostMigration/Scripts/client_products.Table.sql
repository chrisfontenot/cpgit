
/****** Object:  Table [dbo].[client_products]    Script Date: 2/26/2017 2:23:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[client_products](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_key] NOT NULL,
	[vendor_product] [dbo].[typ_key] NOT NULL,
	[cost] [money] NOT NULL,
	[discount] [money] NOT NULL,
	[tendered] [money] NOT NULL,
	[disputed_amount] [money] NOT NULL,
	[disputed_status] [varchar](10) NULL,
	[disputed_note] [varchar](512) NULL,
	[resolution_note] [varchar](512) NULL,
	[date_performed] [datetime] NULL,
	[vendor_invoice] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL,
	[invoice_status] [int] NULL,
 CONSTRAINT [PK_client_products] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[client_products]  WITH CHECK ADD  CONSTRAINT [FK_client_products_clients] FOREIGN KEY([client])
REFERENCES [dbo].[clients] ([client])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[client_products] CHECK CONSTRAINT [FK_client_products_clients]
GO

ALTER TABLE [dbo].[client_products]  WITH CHECK ADD  CONSTRAINT [FK_client_products_vendor_products] FOREIGN KEY([vendor_product])
REFERENCES [dbo].[vendor_products] ([oID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[client_products] CHECK CONSTRAINT [FK_client_products_vendor_products]
GO


