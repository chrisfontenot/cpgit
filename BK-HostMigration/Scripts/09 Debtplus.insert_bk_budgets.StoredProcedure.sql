SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_bk_budgets]
(
	@clientID int
	,@homemaintenance varchar(10)
	,@medicalinsurance varchar(10)
	,@vehiclepayment varchar(10) -- ?? Auto Payment 710
	,@carinsurance varchar(10)
	,@utilities varchar(10) -- ??
	,@telephone varchar(10)
	,@grocery varchar(10)
	,@foodaway varchar(10)
	,@carmaintenance varchar(10)
	,@publictransportation varchar(10)
	,@medicalprescription varchar(10)
	,@miscellaneous varchar(10)
	,@education varchar(10)
	,@childsupportalimony varchar(10)
	,@laundry varchar(10)
	,@clothing varchar(10)
	,@beautybarber varchar(10)
	,@personalexpense varchar(10)
	,@clubdues varchar(10)
	,@contributions varchar(10)
	,@recreation varchar(10)
	,@gifts varchar(10)
	,@savings varchar(10)
	,@trash varchar(10)
	,@gas varchar(10)
)
AS
BEGIN

	declare @budgetID int
	       ,@homemaintenancevalue money
		   ,@medicalinsurancevalue money
		   ,@carinsurancevalue money
		   ,@telephonevalue money
		   ,@groceryvalue money
		   ,@foodawayvalue money
		   ,@carmaintenancevalue money
		   ,@publictransportationvalue money
		   ,@medicalprescriptionvalue money
		   ,@vehiclepaymentvalue money
		   ,@utilitiesvalue money
		   ,@miscellaneousvalue money
		   ,@educationvalue money
		   ,@childsupportalimonyvalue money
		   ,@laundryvalue money
		   ,@clothingvalue money
		   ,@beautybarbervalue money
		   ,@personalexpensevalue money
		   ,@clubduesvalue money
		   ,@contributionsvalue money
		   ,@recreationvalue money
		   ,@giftsvalue money
		   ,@savingsvalue money
		   ,@trashvalue money
		   ,@gasvalue money

	INSERT INTO BUDGETS (CLIENT, STARTING_DATE, ending_date, budget_type, date_created, created_by)
	SELECT @clientID, getdate(), null, 'I', getdate(), 'Bankruptcy';

	select @budgetID = SCOPE_IDENTITY()

	select @homemaintenancevalue = case when isnumeric(@homemaintenance) > 0 then convert(MONEY, @homemaintenance) else 0.00 end
	      ,@medicalinsurancevalue = case when isnumeric(@medicalinsurance) > 0 then convert(MONEY, @medicalinsurance) else 0.00 end
		  ,@carinsurancevalue = case when isnumeric(@carinsurance) > 0 then convert(MONEY, @carinsurance) else 0.00 end
		  ,@telephonevalue = case when isnumeric(@telephone) > 0 then convert(MONEY, @telephone) else 0.00 end
		  ,@groceryvalue = case when isnumeric(@grocery) > 0 then convert(MONEY, @grocery) else 0.00 end
		  ,@foodawayvalue = case when isnumeric(@foodaway) > 0 then convert(MONEY, @foodaway) else 0.00 end
		  ,@carmaintenancevalue = case when isnumeric(@carmaintenance) > 0 then convert(MONEY, @carmaintenance) else 0.00 end
		  ,@publictransportationvalue = case when isnumeric(@publictransportation) > 0 then convert(MONEY, @publictransportation) else 0.00 end
		  ,@medicalprescriptionvalue = case when isnumeric(@medicalprescription) > 0 then convert(MONEY, @medicalprescription) else 0.00 end
		  ,@vehiclepaymentvalue = case when isnumeric(@vehiclepayment) > 0 then convert(MONEY, @vehiclepayment) else 0.00 end
		  ,@utilitiesvalue = case when isnumeric(@utilities) > 0 then convert(MONEY, @utilities) else 0.00 end
		  ,@miscellaneousvalue = case when isnumeric(@miscellaneous) > 0 then convert(MONEY, @miscellaneous) else 0.00 end
		  ,@educationvalue = case when isnumeric(@education) > 0 then convert(MONEY, @education) else 0.00 end
		  ,@childsupportalimonyvalue = case when isnumeric(@childsupportalimony) > 0 then convert(MONEY, @childsupportalimony) else 0.00 end
		  ,@laundryvalue = case when isnumeric(@laundry) > 0 then convert(MONEY, @laundry) else 0.00 end
		  ,@clothingvalue = case when isnumeric(@clothing) > 0 then convert(MONEY, @clothing) else 0.00 end
		  ,@beautybarbervalue = case when isnumeric(@beautybarber) > 0 then convert(MONEY, @beautybarber) else 0.00 end
		  ,@personalexpensevalue = case when isnumeric(@personalexpense) > 0 then convert(MONEY, @personalexpense) else 0.00 end
		  ,@clubduesvalue = case when isnumeric(@clubdues) > 0 then convert(MONEY, @clubdues) else 0.00 end
		  ,@contributionsvalue = case when isnumeric(@contributions) > 0 then convert(MONEY, @contributions) else 0.00 end
		  ,@recreationvalue = case when isnumeric(@recreation) > 0 then convert(MONEY, @recreation) else 0.00 end
		  ,@giftsvalue = case when isnumeric(@gifts) > 0 then convert(MONEY, @gifts) else 0.00 end
		  ,@savingsvalue = case when isnumeric(@savings) > 0 then convert(MONEY, @savings) else 0.00 end
		  ,@trashvalue = case when isnumeric(@trash) > 0 then convert(MONEY, @trash) else 0.00 end
		  ,@gasvalue = case when isnumeric(@gas) > 0 then convert(MONEY, @gas) else 0.00 end

	--print 'insert budget detail 145'
	if @homemaintenancevalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 145, @homemaintenancevalue, @homemaintenancevalue, getdate(), 'Bankruptcy'; 
	end

	--print 'insert budget detail 540'
	if @medicalinsurancevalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 540, @medicalinsurancevalue, @medicalinsurancevalue, getdate(), 'Bankruptcy'; 
	end

	--print 'insert budget detail 710'
	if @vehiclepaymentvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 710, @vehiclepaymentvalue, @vehiclepaymentvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 530'
	if @carinsurancevalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 530, @carinsurancevalue, @carinsurancevalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 130'
	if @utilitiesvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 130, @utilitiesvalue, @utilitiesvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 311'
	if @telephonevalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 311, @telephonevalue, @telephonevalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 920'
	if @groceryvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 920, @groceryvalue, @groceryvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 925'
	if @foodawayvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 925, @foodawayvalue, @foodawayvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 740'
	if @carmaintenance > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 740, @carmaintenance, @carmaintenance, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 720'
	if @publictransportationvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 720, @publictransportationvalue, @publictransportationvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 960'
	if @medicalprescription > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 960, @medicalprescription, @medicalprescription, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 910'
	if @childsupportalimonyvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 910, @childsupportalimonyvalue, @childsupportalimonyvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 1000'
	if @miscellaneousvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 1000, @miscellaneousvalue, @miscellaneousvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 912'
	if @educationvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 912, @educationvalue, @educationvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 945'
	if @laundryvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 945, @laundryvalue, @laundryvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 940'
	if @clothingvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 940, @clothingvalue, @clothingvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 950'
	if @beautybarbervalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 950, @beautybarbervalue, @beautybarbervalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 965'
	if @personalexpensevalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 965, @personalexpensevalue, @personalexpensevalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 985'
	if @clubduesvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 985, @clubduesvalue, @clubduesvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 980'
	if @contributionsvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 980, @contributionsvalue, @contributionsvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 995'
	if @recreationvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 995, @recreationvalue, @recreationvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 975'
	if @giftsvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 975, @giftsvalue, @giftsvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 220'
	if @savingsvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 220, @savingsvalue, @savingsvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 135'
	if @trashvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 135, @trashvalue, @trashvalue, getdate(), 'Bankruptcy';
	end

	--print 'insert budget detail 131'
	if @gasvalue > 0 
	begin
		INSERT INTO budget_detail (budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		select @budgetID, 131, @gasvalue, @gasvalue, getdate(), 'Bankruptcy';
	end
	
END
