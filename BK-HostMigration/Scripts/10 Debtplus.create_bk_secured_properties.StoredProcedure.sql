SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[create_bk_secured_properties]
(
	 @clientid int
	,@valcar varchar(10) = null
	,@valhome varchar(10) = null
	,@valother varchar(10) = null
	,@valrealestate varchar(10) = null
	,@valsavings varchar(10) = null
	,@valretirement varchar(10) = null
)
AS
BEGIN
	DECLARE @createdby VARCHAR(50)

	SET @createdby = 'Bankruptcy'

	declare @carvalue money
	       ,@homevalue money
		   ,@othervalue money
		   ,@realestatevalue money
		   ,@savingsvalue money
		   ,@retirementvalue money


	select @carvalue = case when isnumeric(@valcar) > 0 then convert(MONEY, @valcar) else 0.00 end
	      ,@homevalue = case when isnumeric(@valhome) > 0 then convert(MONEY, @valhome) else 0.00 end
		  ,@othervalue = case when isnumeric(@valother) > 0 then convert(MONEY, @valother) else 0.00 end
		  ,@realestatevalue = case when isnumeric(@valrealestate) > 0 then convert(MONEY, @valrealestate) else 0.00 end
		  ,@retirementvalue = case when isnumeric(@valretirement) > 0 then convert(MONEY, @valretirement) else 0.00 end
		  ,@savingsvalue = case when isnumeric(@valsavings) > 0 then convert(MONEY, @valsavings) else 0.00 end

	-- Auto value
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 400, @carvalue, @createdby

	--home
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 500, @homevalue, @createdby

	--other
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 130, @othervalue, @createdby

	-- 401K
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 310, @retirementvalue, @createdby

	-- Checking Saving
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 100, @savingsvalue, @createdby

	-- Land / Other Real Estate
	INSERT INTO secured_properties (client, secured_type, current_value, created_by)
	SELECT @clientid, 510, @realestatevalue, @createdby

	--FROM CredAbilityInput..Demographic
	--WHERE (isnumeric([valcar]) > 0 AND convert(MONEY, [valcar]) > 0) OR (isnumeric([auto loans]) > 0 AND convert(MONEY, [auto loans]) > 0)

	
	---- Insert loans for the car as needed
	--INSERT INTO ConvertedDebtplus..secured_loans (
	--	secured_property
	--	,balance
	--	,created_by
	--	)
	--SELECT p.secured_property
	--	,[AUTO LOANS]
	--	,@createdby
	--FROM CredAbilityInput..Demographic d
	--INNER JOIN ConvertedDebtPlus..secured_properties p ON d.[client#] = p.client AND 1008 = p.secured_type
	--WHERE isnumeric(d.[AUTO LOANS]) > 0

	---- Insert the savings
	--INSERT INTO ConvertedDebtplus..secured_properties (
	--	client
	--	,secured_type
	--	,current_value
	--	,created_by
	--	)
	--SELECT [client#]
	--	,1002
	--	,[valsavings]
	--	,@createdby
	--FROM CredAbilityInput..Demographic
	--WHERE isnumeric([valsavings]) > 0

	---- Insert the retirement
	--INSERT INTO ConvertedDebtplus..secured_properties (
	--	client
	--	,secured_type
	--	,current_value
	--	,created_by
	--	)
	--SELECT [client#]
	--	,1007
	--	,[valretamt]
	--	,@createdby
	--FROM CredAbilityInput..Demographic
	--WHERE isnumeric([valretamt]) > 0

	---- Insert the Jewelry
	--INSERT INTO ConvertedDebtplus..secured_properties (
	--	client
	--	,secured_type
	--	,current_value
	--	,created_by
	--	)
	--SELECT [client#]
	--	,1013
	--	,[VALPERSPROP]
	--	,@createdby
	--FROM CredAbilityInput..Demographic
	--WHERE isnumeric([VALPERSPROP]) > 0

	---- Insert the secured loans
	--INSERT INTO ConvertedDebtplus..secured_properties (
	--	client
	--	,secured_type
	--	,current_value
	--	,created_by
	--	)
	--SELECT [client#]
	--	,1004
	--	,0
	--	,@createdby
	--FROM CredAbilityInput..Demographic
	--WHERE (isnumeric([valother]) > 0 AND convert(MONEY, [valother]) > 0) OR (isnumeric([SEC AMT]) > 0 AND convert(MONEY, [sec amt]) > 0)

	---- Update the value other property
	--UPDATE ConvertedDebtplus..secured_properties
	--SET current_value = convert(MONEY, [valother])
	--FROM ConvertedDebtplus..secured_properties p
	--INNER JOIN CredAbilityInput..Demographic d ON d.[client#] = p.[client]
	--WHERE p.secured_type = 1004 AND isnumeric(d.[valother]) > 0

	---- Insert the other loans
	--INSERT INTO ConvertedDebtplus..secured_loans (
	--	secured_property
	--	,balance
	--	,created_by
	--	)
	--SELECT p.secured_property
	--	,[SEC AMT]
	--	,@createdby
	--FROM CredAbilityInput..Demographic d
	--INNER JOIN ConvertedDebtPlus..secured_properties p ON d.[client#] = p.client AND 1004 = p.secured_type
	--WHERE isnumeric(d.[SEC AMT]) > 0

	---- Student loan
	--INSERT INTO ConvertedDebtplus..secured_properties (
	--	client
	--	,secured_type
	--	,current_value
	--	,created_by
	--	)
	--SELECT [client#]
	--	,1014
	--	,0
	--	,@createdby
	--FROM CredAbilityInput..Demographic
	--WHERE isnumeric([INSTALLMENT LOAN BAL]) > 0

	--INSERT INTO ConvertedDebtplus..secured_loans (
	--	secured_property
	--	,balance
	--	,created_by
	--	)
	--SELECT p.secured_property
	--	,[INSTALLMENT LOAN BAL]
	--	,@createdby
	--FROM CredAbilityInput..Demographic d
	--INNER JOIN ConvertedDebtPlus..secured_properties p ON d.[client#] = p.client AND 1014 = p.secured_type
	--WHERE isnumeric([INSTALLMENT LOAN BAL]) > 0 AND [installment loan bal] LIKE '[0-9]%'

END

