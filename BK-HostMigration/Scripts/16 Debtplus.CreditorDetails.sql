
CREATE TABLE [dbo].[CreditorDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InNo] [varchar](150) NULL,
	[Name] [varchar](150) NULL,
	[Balance] [float] NULL,
	[IntRate] [float] NULL,
	[Payment] [float] NULL,
	[PreAcctNo] [varchar](50) NULL,
	[PriAcctHolder] [varchar](50) NULL,
	[PastDue] [varchar](50) NULL,
	[CrAcctNo] [varchar](50) NULL,
 CONSTRAINT [PK_CreditorDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


