
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE create_bk_client
( 
	@primaryContactDetailID as int,
	@secondaryContactDetailID as int = null,
	@clientINNumber as varchar(10) = null,
	@fees as money = null,
	@certificateNumber as varchar(50) = null,
	@paymentType as int = null,
	@feeWaiver as int = null,
	@counselorEmail as varchar(50) = null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @primaryFirstName varchar(50)
			,@primaryMiddleName varchar(50)
			,@primaryLastName varchar(50)
			--,@primaryNameID int
			,@address varchar(100)
			,@city varchar(50)
			,@state varchar(50)
			,@zipcode varchar(10)
			,@addressid int
			,@attn varchar(50)
			,@phone varchar(20)
			,@phoneid int
			,@activestatuscode varchar(10)
			,@officeid int
			,@peoplecount int
			,@gender1 varchar(10)
			,@gender2 varchar(10)
			,@birthdate1 datetime
			,@birthdate2 datetime
			,@grossincome1 varchar(10)
			,@grossincome2 varchar(10)
			,@netincome1 varchar(10)
			,@netincome2 varchar(10)
			,@languageid int
			,@race varchar(10)
			,@race2 varchar(10)
			,@alimony varchar(15)
			,@childsupport varchar(15)
			,@socialsecurity varchar(15)
			,@savings varchar(15)
			,@rentalincome varchar(15)
			,@selfemployment varchar(15)
			,@parttime varchar(15)
			,@foodstamp varchar(15)
			,@otherincome varchar(15)
			,@ethnicity varchar(15)
			,@ethnicity2 varchar(15)
			,@maritalstatus varchar(5)
			,@grossincome1out MONEY
			,@grossincome2out MONEY
			,@netincome1out MONEY
			,@netincome2out MONEY
			,@counselor varchar(50)
			,@counselorid int
			,@maritalstatusid INT
			,@programmonths INT
			,@estatement INT
			,@ecorrespondence INT
			,@emailaddress varchar(50)
			,@hasspouse int
			,@householdid int
			,@femaleheaded varchar(10)
			,@dependents int
			,@defaultcounty int
			,@defaultregion int
			,@activestatusdate datetime
			,@defaultpreferrredcontact int
			,@clientid int
			--,@oldclientid varchar(20)
			,@applicantnameid int
			,@emailid int
			,@educationcode varchar(15)
			,@ssn1 varchar(15)
			,@ssn2 varchar(15)
			,@defaultmilitarystatus INT
			,@defaultmilitaryservice INT
			,@defaultmilitarygrade int
			,@defaultmilitarydependent int
			,@defaultfrequency int
			,@defaultnoficoscore int
			,@defaultdisabledtype int
			,@relationself int
			,@raceid int
			,@raceid2 int
			,@defaultrace int
			,@defaultethnicitytype int
			,@ethnicityid int
			,@ethnicityid2 int
			,@defaulteducation int
			,@secondaryfirstname varchar(20)
			,@secondarymidname varchar(20)
			,@secondarylastname varchar(20)
			,@coapplicantnameid int
			,@servicerid int
			,@educationid int
			,@defaultrelation int
			,@referredby int
			,@ficoscore decimal
			,@financialproblemcause int
			,@firstcontactid int
			,@carval varchar(10)
			,@homeval varchar(10)
			,@otherval varchar(10)
			,@homemaintenance varchar(10)
		    ,@medicalinsurance varchar(10)
		    ,@vehiclepayment varchar(10)
		    ,@carinsurance varchar(10)
		    ,@utilities varchar(10) -- ??
		    ,@telephone varchar(10)
		    ,@grocery varchar(10)
		    ,@foodaway varchar(10)
		    ,@carmaintenance varchar(10)
		    ,@publictransportation varchar(10)
		    ,@medicalprescription varchar(10)
		    ,@miscellaneous varchar(10)
		    ,@education varchar(10)
		    ,@childsupportalimony varchar(10)
		    ,@laundry varchar(10)
		    ,@clothing varchar(10)
		    ,@beautybarber varchar(10)
		    ,@personalexpense varchar(10)
		    ,@clubdues varchar(10)
		    ,@contributions varchar(10)
		    ,@recreation varchar(10)
		    ,@gifts varchar(10)
		    ,@exp_savings varchar(10)
		    ,@trash varchar(10)
		    ,@gas varchar(10)
			,@FirstNameOnCard varchar(50)
			,@MiddleNameOnCard varchar(50)
			,@LastNameOnCard varchar(50)
			,@DCAccountNumber varchar(20)
			,@ExpiryDate varchar(10)
			,@BillAddress varchar(100)
			,@BillCity varchar(50)
			,@BillState varchar(50)
			,@BillZip varchar(10)
			,@person1 int
			,@person2 int
			,@servicer varchar(50)
			,@servicerLoan varchar(50)
			,@moPastDue int
			,@clientFee money
			,@firmid varchar(50)
			,@vendorid int
			,@productid int
			,@clienttype varchar(10)
			--,@vendorproductid int
			,@clientproductid int
			,@isdebitcardpayment bit
			,@isachpayment bit
			,@tenderedAmount money
			,@discountAmount money
			,@abanumber varchar(10)
			,@accountnumber varchar(20)
			--,@PROBONO int
			--,@INCOME int
			--,@SSDI int
			--,@LEGAL_AID int
			--,@TO_BE_DECIDED int
			,@productDisputeType int
			,@feeWaiverText varchar(50)
			,@originalbalance money
			,@interestrate decimal
			,@loanoriginaldate datetime
			,@intakeid int
			,@currentbalance money
			,@monthlypayment money
			,@financetype varchar(50)
			,@financetypeid int
			,@mortlastcontactdate datetime
			,@mortgageinvestor varchar(10)
			,@mortgageinvestorid int
			,@propertyTax money
			,@isProperyTaxIncluded varchar(10)
			,@isProperyTaxIncludedid int
			,@propertyInsurance money
			,@isProperyInsuranceIncluded varchar(10)
			,@isProperyInsuranceIncludedid int
			,@realestatevalue varchar(10)
			,@savingsvalue varchar(10)
			,@retirementvalue varchar(10)

	SELECT @activestatuscode = 'I'
		  ,@hasspouse = 0
		  ,@femaleheaded = 'N'
		  ,@dependents = 0
		  ,@defaultcounty = 1
		  ,@defaultregion = 7
		  ,@activestatusdate = '1/1/1901'
		  ,@defaultpreferrredcontact = 8
		  ,--  [unknown] 
	       @defaultmilitarystatus = 0
		  ,-- [Not In Military]
		   @defaultmilitaryservice = 0
		  ,-- [N/A]
		   @defaultmilitarygrade = 0
		  ,-- [N/A]
		   @defaultmilitarydependent = 0
		  ,-- Unspecified
		   @defaultfrequency = 4
		  ,-- [N/A]
		   @defaultnoficoscore = 3
		  ,-- type 2 [unspecified]
		   @defaultdisabledtype = 0
		  ,@relationself = 1
		  , -- refused to disclose
		   @defaultrace = 15
		  ,@defaultethnicitytype = 2
		  ,-- Unknown education
		  @defaulteducation = 1
		  ,@defaultrelation = 11 -- other family
		  ,@financialproblemcause = 160
		  , --Not Specified
		  @firstcontactid = 1
		  -- Internet Counselor
		  --,@counselorid = 1820
		  ,@isdebitcardpayment = 0
		  ,@tenderedAmount = 0
		  ,@isachpayment = 0
		  ,@discountAmount = 0
		  ,@clientFee = @fees
		  ,@originalbalance = 0
		  ,@interestrate = 0
	-- set office to atlanta branch
	SELECT @officeid = 354

	print 'get client details'

	select @primaryFirstName = FNAME
	       ,@primaryMiddleName = MINITIAL
		   ,@primaryLastName = LNAME
		   ,@address  = [ADDRESS]
		   ,@city = CITY
		   ,@state = [STATE]
		   ,@zipcode = ZIP
		   ,@phone = PHONE
		   ,@emailaddress = EMAIL
		   ,@peoplecount = CASE isnumeric(replace([SIZE_OF_HOUSEHOLD], '.', '')) WHEN 1 THEN replace([SIZE_OF_HOUSEHOLD], '.', '') ELSE 0 END
		   ,@gender1 = [sex]
		   ,@gender2 = [co_sex]
		   ,@birthdate1 = CASE WHEN [DOB] IS NULL THEN NULL WHEN len([DOB]) = 0 THEN NULL WHEN [DOB] LIKE '%|%' THEN CASE WHEN isdate(left([DOB], charindex('|', [DOB]) - 1)) = 1 THEN convert(DATETIME, left([DOB], charindex('|', [DOB]) - 1)) ELSE NULL END ELSE CASE WHEN isdate([DOB]) = 1 THEN convert(DATETIME, [DOB]) ELSE NULL END END
		   ,@birthdate2 = CASE WHEN [CO_DOB] IS NULL THEN NULL WHEN len([CO_DOB]) = 0 THEN NULL WHEN [CO_DOB] LIKE '%|%' THEN CASE WHEN isdate(left([CO_DOB], charindex('|', [CO_DOB]) - 1)) = 1 THEN convert(DATETIME, left([CO_DOB], charindex('|', [CO_DOB]) - 1)) ELSE NULL END ELSE CASE WHEN isdate([CO_DOB]) = 1 THEN convert(DATETIME, [CO_DOB]) ELSE NULL END END
		   ,@grossincome1 = MONTHLY_GROSS_INCOME
		   ,@netincome1 = MONTHLY_NET_INCOME
		   ,@languageid = CASE LANGUAGES WHEN 'S' THEN 2 ELSE 1 END
		   ,@race = CONTACT_RACE
		   ,@race2 = CO_RACE
		   ,@alimony = isnull(MONTHLY_ALIMONY_INCOME, '')
		   ,@childsupport = isnull([CHILD_SUPPORT], '')
		   ,@socialsecurity = isnull([MONTHLY_GOVTASSIST_INCOME], '')
		   ,@savings = isnull([SAVINGS], '')
		   ,@rentalincome = 0
		   ,@selfemployment = isnull(NULL, '')
		   ,@parttime = isnull([MONTHLY_PARTTIME_NET], '')
		   ,@foodstamp = 0
		   ,@otherincome = 0
		   ,@ethnicity = isnull(CONTACT_HISPANIC, '')
		   ,@ethnicity2 = isnull(CO_HISPANIC, '')
		   ,@maritalstatus = isnull([MARITAL], '')
		   ,@educationcode = EDUCATION_LEVEL
		   ,@ssn1 = SSN
		   ,@ssn2 = CO_SSN
		   ,@secondaryfirstname = CO_FNAME
		   ,@secondarymidname = CO_MINITIAL
		   ,@secondarylastname = CO_LNAME
		   ,@carval = VAL_CAR
		   ,@homeval = VAL_HOME
		   ,@otherval = VAL_OTHER
		   ,@homemaintenance = HOME_MAINTENANCE
		   ,@medicalinsurance = INSURANCE
		   ,@vehiclepayment = VEHICLE_PAYMENTS
		   ,@carinsurance = CAR_INSURANCE
		   ,@utilities =  EQUITY_LOAN_TAX_INS
		   ,@telephone = TELEPHONE
		   ,@grocery = GROCERIES
		   ,@foodaway = FOOD_AWAY
		   ,@carmaintenance = CAR_MAINTENANCE
		   ,@publictransportation = PUBLIC_TRANSPORTATION
		   ,@medicalprescription = MEDICAL_PRESCRIPTION
		   ,@miscellaneous = MISC_EXP 
		   ,@education = EDUCATION
		   ,@childsupportalimony = CHILD_SUPPORT
		   ,@laundry = LAUNDRY
		   ,@clothing = CLOTHING
		   ,@beautybarber = BEAUTY_BARBER
		   ,@personalexpense = PERSONAL_EXP
		   ,@clubdues = CLUB_DUES
		   ,@contributions =  CONTRIBUTIONS
		   ,@recreation = RECREATION
		   ,@gifts = GIFTS
		   ,@exp_savings = SAVINGS
		   ,@trash = UTRASH
		   ,@gas = UGAS
		   ,@FirstNameOnCard = DC_FNAMEONCARD
           ,@MiddleNameOnCard = DC_MNAMEONCARD
           ,@LastNameOnCard = DC_LNAMEONCARD
           ,@DCAccountNumber = DC_ACCTNUM
           ,@ExpiryDate = DC_EXPDATE
           ,@BillAddress = DC_BILLADDR
           ,@BillCity = DC_BILLCITY
           ,@BillState = DC_BILLSTATE
           ,@BillZip = DC_BILLZIP
		   ,@servicer = MORT_HOLDER
		   ,@servicerLoan = LOAN_NUMBER
		   ,@moPastDue = MOS_DELINQ
		   ,@firmid = FIRM_ID
		   ,@clienttype = CLIENT_TYPE
		   ,@abanumber = ABANUMBER
		   ,@accountnumber = ACCTNUMBER
		   ,@originalbalance = isnull(ORIG_BAL, 0)
		   ,@interestrate = isnull(MORT_RATE, 0)
		   ,@loanoriginaldate = CASE WHEN [MORT_DATE] IS NULL THEN NULL WHEN len([MORT_DATE]) = 0 THEN NULL WHEN [MORT_DATE] LIKE '%|%' THEN CASE WHEN isdate(left([MORT_DATE], charindex('|', [MORT_DATE]) - 1)) = 1 THEN convert(DATETIME, left([MORT_DATE], charindex('|', [MORT_DATE]) - 1)) ELSE NULL END ELSE CASE WHEN isdate([MORT_DATE]) = 1 THEN convert(DATETIME, [MORT_DATE]) ELSE NULL END END
		   ,@currentbalance = isnull(OWE_HOME, 0)
		   ,@monthlypayment = isnull(RENT_MORT, 0)
		   ,@financetype = MORT_TYPE
		   ,@mortlastcontactdate = CASE WHEN MORT_LAST_CONTACT_DATE IS NULL THEN NULL WHEN len(MORT_LAST_CONTACT_DATE) = 0 THEN NULL WHEN MORT_LAST_CONTACT_DATE LIKE '%|%' THEN CASE WHEN isdate(left(MORT_LAST_CONTACT_DATE, charindex('|', MORT_LAST_CONTACT_DATE) - 1)) = 1 THEN convert(DATETIME, left(MORT_LAST_CONTACT_DATE, charindex('|', MORT_LAST_CONTACT_DATE) - 1)) ELSE NULL END ELSE CASE WHEN isdate(MORT_LAST_CONTACT_DATE) = 1 THEN convert(DATETIME, MORT_LAST_CONTACT_DATE) ELSE NULL END END
		   ,@mortgageinvestor = MORT_INVESTOR
		   ,@propertyTax = isnull([PROP.TAXES], 0)
		   ,@isProperyTaxIncluded = isnull([TAX.INC], 'No')
		   ,@propertyInsurance = isnull([PROP.INSURE], 0)
		   ,@isProperyInsuranceIncluded = isnull([INS.INC], 'No')
		   ,@realestatevalue = PERSPROP_AMT
		   ,@savingsvalue = SAVINGS
		   ,@retirementvalue = RET_AMT
	  from ContactDetails
	 where ID = @primaryContactDetailID;

	 select @grossincome2 = MONTHLY_GROSS_INCOME2
		   ,@netincome2 = MONTHLY_NET_INCOME2
		   ,@counselor = COUNSELOR_ID
	  from ContactDetails2
	 where ID = @secondaryContactDetailID;

	--insert address
	IF len(@address) > 0 OR len(@city) > 0 OR len(@state) > 0 OR len(@zipcode) > 0
	BEGIN
		EXEC dbo.insert_address @address
			,null
			,null
			,@city
			,@state
			,@zipcode
			,@addressid OUTPUT
			,@attn OUTPUT
	END

	EXEC create_telephonenumber @phone
				,NULL
				,NULL
				,NULL
				,@phoneid OUTPUT


	PRINT 'processing gross income and net income'

	EXEC dbo.process_gross_net_income @grossincome1
                                     ,@netincome1
                                     ,@grossincome2
                                     ,@netincome2
                                     ,@grossincome1out OUTPUT
                                     ,@netincome1out OUTPUT
                                     ,@grossincome2out OUTPUT
                                     ,@netincome2out OUTPUT

    PRINT 'getting counselor id...'
	--SELECT @counselorid = CredAbilityInput.dbo.map_counselor(@counselor);

	--IF len(ltrim(rtrim(isnull(@maritalstatus, '')))) > 0
	--BEGIN
	--	IF @maritalstatus = 'N' OR @maritalstatus = 'O' OR @maritalstatus = 'Z'
	--	BEGIN
	--		-- if spouse name is provided set the marital status to married
	--		IF @hasspouse = 1
	--		BEGIN
	--			SELECT @maritalstatus = 'M'
	--		END
	--				-- otherwise set the marital status to single
	--		ELSE
	--		BEGIN
	--			SELECT @maritalstatus = 'S'
	--		END
	--	END
	--END

	PRINT 'getting marital status'

	SELECT @maritalstatusid = case @maritalstatus 
	                            when 'D' then 3
							    when 'M' then 2
							    when 'S' then 1
							    when 'W' then 4
							    when 'X' then 5
								else null
							  end;

	SELECT @programmonths = 0
		  ,@estatement = CASE ISNULL(@emailaddress, '0') WHEN '0' THEN 0 ELSE 1 END
		  ,@ecorrespondence = @estatement

	PRINT 'get household id...'

	SELECT @householdid = dbo.get_household_info(@femaleheaded, @gender1, @dependents, @hasspouse, @maritalstatus);

	print 'get counselor id'
	select top(1) @counselorid = counselor from counselors where emailid in (select email from EmailAddresses where Address = @counselorEmail)
	
	print 'inserting client data...'

	INSERT INTO [clients] (
				[addressid]
				,[hometelephoneid]
				,[county]
				,[region]
				,[active_status]
				,[active_status_date]
				,[client_status]
				,[disbursement_date]
				,[start_date]
				,[referred_by]
				,[cause_fin_problem1]
				,[office]
				,[counselor]
				,[hold_disbursements]
				,[personal_checks]
				,[ach_active]
				,[stack_proration]
				,[mortgage_problems]
				,[intake_agreement]
				,[electroniccorrespondence]
				,[electronicstatements]
				,[bankruptcy_class]
				,[fed_tax_owed]
				,[state_tax_owed]
				,[local_tax_owed]
				,[fed_tax_months]
				,[state_tax_months]
				,[local_tax_months]
				,[held_in_trust]
				,[deposit_in_trust]
				,[reserved_in_trust]
				,[marital_status]
				,[language]
				,[dependents]
				,[household]
				,[method_first_contact]
				,[config_fee]
				,[first_appt]
				,[first_kept_appt]
				,[first_resched_appt]
				,[program_months]
				,[preferred_contact]
				,[created_by]
				,[date_created]
				,[people]
				,[payout_total_debt]
				,[payout_months_to_payout]
				,[payout_total_fees]
				,[payout_total_interest]
				,[payout_total_payments]
				,[payout_deposit_amount]
				,[payout_monthly_fee]
				,[payout_cushion_amount]
				)
			VALUES (
				 @addressid
				,@phoneid
				,@defaultcounty
				,@defaultregion
				,@activestatuscode
				,@activestatusdate
				,'BK'
				,1
				,NULL
				,@referredby
				,@financialproblemcause
				,@officeid
				,@counselorid
				,0
				,0
				,0
				,0
				,0
				,0
				,@ecorrespondence
				,@estatement
				,0
				,0.00
				,0.00
				,0.00
				,0
				,0
				,0
				,0
				,0.00
				,0.00
				,@maritalstatusid
				,@languageid
				,@dependents
				,@householdid
				,@firstcontactid
				,1.11
				,NULL
				,NULL
				,NULL
				,@programmonths
				,@defaultpreferrredcontact
				,'Bankruptcy'
				,getdate()
				,@peoplecount
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				);

	set @clientid = SCOPE_IDENTITY();

	insert into client_addkeys(client, additional, date_created, created_by) values (@clientid, @clientINNumber, getdate(), 'Bankruptcy')

	PRINT 'PEOPLE TABLE...'
	PRINT 'gettig applicant name id'
	EXEC dbo.create_name @primaryFirstName
                        ,@primaryMiddleName
                        ,@primaryLastName
                        ,@applicantnameid OUTPUT;

	PRINT 'getting mail id'

	EXEC dbo.create_email @emailaddress, @emailid OUTPUT;

	--SELECT @educationid = CredAbilityInput.dbo.map_education(@educationcode)

	print 'ssn before correction : ' + isnull(@ssn1, 'ssn is null')

	-- remove dash from ssn
	if @ssn1 is not null and len(@ssn1) > 1 
	begin
		select @ssn1 = isnull(replace(@ssn1, '-', ''), '')
		--print 'ssn value after replacing hypen : ' + isnull(@ssn1, 'ssn value is null after correction')
		select @ssn1 = CASE @ssn1 
						WHEN NULL THEN null
						WHEN '' THEN null
					    ELSE 
							CASE len(@ssn1) 
								WHEN 9 THEN @ssn1 
								ELSE 'c' 
							END 
					   END
		--print 'ssn value after check : ' + isnull(@ssn1, '')
	end
	else
	begin
		select @ssn1 = ''
	end

	print 'ssn : ' + isnull(@ssn1, '')

	select @raceid = case @race when 'W' then 3 when 'X' then 2 when 'A' then 1 when 'M' then 17 end
	
	print 'insert people table relation 1'
	INSERT INTO [people] (
		[Client]
		,[Relation]
		,[NameID]
		,[EmailID]
		,[SSN]
		,[Gender]
		,[Race]
		,[Ethnicity]
		,[Disabled]
		,[Education]
		,[Birthdate]
		,[FICO_Score]
		,[CreditAgency]
		,[no_fico_score_reason]
		,[gross_income]
		,[net_income]
		,[Frequency]
		,[created_by]
		,[date_created]
		,MilitaryStatusID
		,[MilitaryServiceID]
		,[MilitaryGradeID]
		,[MilitaryDependentID]
		)
	VALUES (
		@clientid
		,@relationself
		,@applicantnameid
		,@emailid
		,@ssn1
		,case @gender1 when 'M' then 1 when 'F' then 2 else 1 end --isnull(@gender1, 1)
		,isnull(@raceid, @defaultrace)
		,isnull(@ethnicity, @defaultethnicitytype)
		,@defaultdisabledtype
		,isnull(@educationid, @defaulteducation)
		,@birthdate1
		,isnull(@ficoscore, 0)
		,'EQUIFAX'
		,@defaultnoficoscore
		,@grossincome1out
		,@netincome1out
		,@defaultfrequency
		,'Bankruptcy'
		,getdate()
		,@defaultmilitarystatus
		,@defaultmilitaryservice
		,@defaultmilitarygrade
		,@defaultmilitarydependent
		);

		set @person1 = SCOPE_IDENTITY();

		if not @secondaryfirstname is null and @secondaryfirstname <> '' and not @secondarylastname is null and @secondarylastname <> ''
		begin
			print 'inserting coapplicant name'
			EXEC create_name @secondaryfirstname
						    ,@secondarymidname
						    ,@secondarylastname
						    ,@coapplicantnameid OUTPUT;
		end

		if not @coapplicantnameid is null
		begin

			PRINT 'inserting co-applicant record'
				--PRINT 'ssn2 : ' + CASE @ssn2 WHEN '' THEN 'null' WHEN NULL THEN 'null' ELSE substring(@ssn2, 1, 9) END

				if @ssn2 is not null and len(@ssn2) > 1 
				begin
					select @ssn2 = isnull(replace(@ssn2, '-', ''), '')
					--print 'ssn value after replacing hypen : ' + isnull(@ssn2, 'ssn value is null after correction')
					select @ssn2 = CASE @ssn2 
									WHEN NULL THEN null
									WHEN '' THEN null
									ELSE 
										CASE len(@ssn2) 
											WHEN 9 THEN @ssn2 
											ELSE null
										END 
								   END
					--print 'ssn value after check : ' + isnull(@ssn2, '')
				end
				else
				begin
					select @ssn2 = ''
				end

				select @raceid2 = case @race2 when 'W' then 3 when 'X' then 2 when 'A' then 1 when 'M' then 17 end

				INSERT INTO [people] (
					[client] 
					,[relation]
					,[nameid]
					,[emailid]
					,[former]
					,[ssn]
					,[gender]
					,[race]
					,[ethnicity]
					,[disabled]
					,[education]
					,[birthdate]
					,[fico_score]
					,[creditagency]
					,[no_fico_score_reason]
					,[gross_income]
					,[net_income]
					,[frequency]
					,[created_by]
					,[date_created]
					,MilitaryStatusID
					,[MilitaryServiceID]
					,[MilitaryGradeID]
					,[MilitaryDependentID]
					)
				VALUES (
					@clientid
					,case @maritalstatus when 'M' then 2 else @defaultrelation end -- if marital status is M, then spouse otherwise other family
					,@coapplicantnameid
					,NULL
					,NULL
					,@ssn2
					,case @gender2 when 'M' then 1 when 'F' then 2 else 1 end --isnull(@gender1, 1)
					,isnull(@raceid2, @defaultrace)
					,isnull(@ethnicity2, @defaultethnicitytype)
					,@defaultdisabledtype
					,@defaulteducation
					,@birthdate2
					,0
					,'EQUIFAX'
					,@defaultnoficoscore
					,@grossincome2out
					,@netincome2out
					,@defaultfrequency
					,'Bankruptcy'
					,getdate()
					,@defaultmilitarystatus
					,@defaultmilitaryservice
					,@defaultmilitarygrade
					,@defaultmilitarydependent
					);

					set @person2 = SCOPE_IDENTITY();
		end

		if @DCAccountNumber is not null and @DCAccountNumber <> '' 
		begin
			select @isdebitcardpayment = 1
		end

		-- if the payment is not made by debt card and has valid aba number and bank account
		if @isdebitcardpayment = 0 and @abanumber is not null and @abanumber <> '' and @accountnumber is not null and @accountnumber <> ''
		begin
			select @isachpayment = 1
		end

		if @firmid is not null and @firmid <> ''
		begin
		
			if @clienttype is not null and @clienttype <> ''
			begin

				select @productid = case @clienttype when 'BK' then 1 when 'BKE' then 2 else null end

				if @productid is not null
				begin

					select @vendorid = oid from vendors where oid = @firmid;

					if @vendorid is not null
					begin

						if @feeWaiver is not null
						begin

							select @feeWaiverText = case @feeWaiver
															when 1 then 'INCOME' -- income
															when 2 then 'SSDI' -- SSDI
															when 3 then 'PROBONO' -- Pro Bono
															when 5 then 'LEGAL_AID' -- Legal Aid
															else 'UNKNOWN'
														 end;
						end

						if @isdebitcardpayment = 1 
						begin
							select @tenderedAmount = @clientFee
						end

						if @feeWaiver is not null and @feeWaiver > 0
						begin
							select @discountAmount = @clientFee
						end

						INSERT INTO [dbo].[client_products]([client], [vendor], [product], [cost], [discount], [tendered], [date_created], [created_by], InNumber, PrimaryCertificate, PaymentType, FeeWaiverType)
						VALUES (@clientid, @vendorid, @productid, @clientFee, @discountAmount, @tenderedAmount, getdate(), 'Bankruptcy', @clientINNumber, @certificateNumber, @paymentType, @feeWaiver)

						select @clientproductid = SCOPE_IDENTITY()

						INSERT INTO [dbo].[product_transactions]([vendor],[product],[client_product],[transaction_type],[cost],[discount],[payment],[disputed],[note],[date_created],[created_by])
						VALUES(@vendorid, @productid, @clientproductid, 'BB', @clientFee, 0, 0, 0, 'Bankruptcy Transaction', getdate(), 'Bankruptcy')

						
						-- if there is a fee waiver create transaction with Transaction Type 'DC' Discount Amount
						if (@feeWaiver is not null and @feeWaiver > 0)
						begin
							INSERT INTO [dbo].[product_transactions]([vendor], [product], [client_product], [transaction_type], [cost], [discount], [payment], [disputed], [note], [date_created], [created_by])
							VALUES(@vendorid, @productid, @clientproductid, 'DC', 0, @discountAmount, 0, 0, 'Fee Waiver Type: ' + isnull(@feeWaiverText, ''), getdate(), 'Bankruptcy')
						end

						-- Add another transaction for DebitCard payment since there is confirmation on the payment with Tranaction type 'DC'
						if @isdebitcardpayment = 1
						begin
							INSERT INTO [dbo].[product_transactions]([vendor], [product], [client_product], [transaction_type], [cost], [discount], [payment], [disputed], [note], [date_created], [created_by])
							VALUES(@vendorid, @productid, @clientproductid, 'RC', 0, 0, @clientFee, 0, null, getdate(), 'Bankruptcy')
						end

						if @isdebitcardpayment = 1
						begin
							print 'insert credit card payment details...'
							print 'client: ' + convert(varchar, isnull(@clientid, 0))
							print 'client_product: ' + convert(varchar, isnull(@clientproductid, 0))
							print '[payment_medium]: ' + 'CreditCard'
							print '[account_number]: ' + isnull(@DCAccountNumber, '')
							print '[expiration_date]: ' + isnull(@ExpiryDate, '01/01/1901')
							print '[credit_card_type]: ' + isnull(null, '<null>')
							print '[card_status_flag]: ' + 'Present'
							print '[amount]: ' + convert(varchar, isnull(@clientFee, '0.0'))
							print 'avs_full_name: ' + isnull(@FirstNameOnCard + ' ' + @LastNameOnCard, '')
							print '[avs_address]: ' + isnull(@BillAddress, '')
							print '[avs_city]: ' + isnull(@BillCity, '')
							print '[avs_state]: ' + isnull(@BillState, '')
							print '[avs_zipcode]: ' + isnull(@BillZip, '')
							print '[@phone]: ' + isnull(@phone, '')
							print '[created_by]: ' + 'Bankruptcy'
							insert into [credit_card_payment_details] (
									 client
									,client_product
									,[payment_medium]
									,[account_number]
									,[expiration_date]
									,[credit_card_type]
									,[card_status_flag]
									,[amount]
									,[avs_full_name]
									,[avs_address]
									,[avs_city]
									,[avs_state]
									,[avs_zipcode]
									,[avs_phone]
									,[settlement_submission_date]
									,[submission_date]
									,[date_created]
									,[created_by])
							select 
									@clientid
									,@clientproductid
									,'CreditCard'
									, null--@DCAccountNumber -- Not setting the Account number 
									,null--case @ExpiryDate when null then null when '' then null else convert(datetime, '1/' + @ExpiryDate, 3) end -- convert d/mm/yy to datetime
									,NULL
									,'Present'
									,@clientFee
									,@FirstNameOnCard + ' ' + @LastNameOnCard
									,@BillAddress
									,@BillCity
									,@BillState
									,@BillZip
									,case @phone when null then null when '' then null else replace(replace(replace(replace(@phone, '-', ''), '(', ''), ')', ''), ' ', '') end
									, getdate() -- settlement submission date
									, getdate() -- submission date
									, getdate()
									, 'Bankruptcy'
						end

						if @isachpayment = 1
						begin

							INSERT INTO [dbo].[ach_onetimes]([client]
											,bank
											,[ABA]
											,[AccountNumber]
											,[CheckingSavings]
											,[Amount]
											,[batch_type]
											,[EffectiveDate]
											,[client_product]
											,[date_created]
											,[created_by])
										VALUES
											(@clientid
											, 85 -- For Bankruptcy and Credit Card Payment BOA Ach Products
											,@abanumber
											,@accountnumber
											,'C'
											,@clientfee
											,'WEB'
											,getdate()
											,@clientproductid
											,getdate()
											,'Bankruptcy')

						end

					end

				end

			end

		end

		print 'insert client housing'
		INSERT INTO client_housing (
			client
			,housing_status
			,HUD_fair_housing
			,HUD_colonias
			,HUD_migrant_farm_worker
			,HUD_predatory_lending
			,HUD_FirstTimeHomeBuyer
			,HUD_DiscriminationVictim
			,HUD_LoanScam
			,OtherHUDFunding
			,NFMCP_level
			,NFMCP_privacy_policy
			,NFMCP_decline_authorization
			,HUD_home_ownership_voucher
			,HUD_housing_voucher
			,housing_type
			,HUD_grant)
		SELECT @clientid
			,4
			,0
			,0
			,0
			,0
			,0
			,0
			,0
			,0.00
			,''
			,0
			,0
			,0
			,0
			,8
			,0;

		-- get servicer
		declare @calcServicerId int 
		set @calcServicerId = COALESCE((select top 1 oID from Housing_lender_servicers [servicers] 
        where servicers.[description] = @servicer),442)

		declare @calcServicerName varchar(50)
		set @calcServicerName = case when @calcServicerId = 442 then 'Other or Unknown' else @servicer end

		declare @property int
		set @property = NULL
		
		select @isProperyTaxIncludedid = case @isProperyTaxIncluded when 'Yes' then 1 when 'No' then 2 else null end;
		select @isProperyInsuranceIncludedid = case @isProperyInsuranceIncluded when 'Yes' then 1 when 'No' then 2 else null end;

		print 'insert housing properties'
		insert into Housing_properties(OwnerID, CoOwnerID, UseHomeAddress, HousingID, Residency, PropertyType, annual_property_tax, annual_ins_amount, property_tax, homeowner_ins, ImprovementsValue)
		values(@person1, @person2, 1, @clientid, 0, 8, @propertyTax, @propertyInsurance, @isProperyTaxIncludedid, @isProperyInsuranceIncludedid, @homeval)
  
		set @property = SCOPE_IDENTITY()

		if @mortgageinvestor is not null or @mortgageinvestor <> ''
		begin

			select @mortgageinvestorid = case @mortgageinvestor 
											when '02' then 3 -- Freddie Mac
											when '01' then 2 -- Fannie Mae
											when '03' then 442 -- Other
											when '04' then 1 -- Not SUre
											else null
										 end;

		end

		print 'incvestor id ' + convert(varchar, isnull(@mortgageinvestorid,''))
		print 'contact lender date: ' + convert(varchar, isnull(@mortlastcontactdate, '01/01/1901'))

		print 'insert housing lenders'
		declare @lender int
		set @lender = NULL
		insert into Housing_lenders(ServicerName, ServicerID, AcctNum, InvestorID, InvestorAccountNumber, ContactLenderDate)
		values(@calcServicerName
		     , @calcServicerId
			 , @servicerLoan -- Servicer Loan ??
			 , @mortgageinvestorid
			 , NULL -- Investor Number ??
			 , @mortlastcontactdate)
		set @lender = SCOPE_IDENTITY()

		select @financetypeid = case @financetype when 'CONV' then 1 when 'FHA' then 2 when 'VA' then 5 when 'USDA' then 4 else null end;

		insert into Housing_loan_details(interestrate, Hybrid_ARM_Loan, Option_ARM_Loan, Interest_Only_Loan, FHA_VA_Insured_Loan, ARM_Reset, Privately_Held_Loan, Payment, FinanceTypeCD)
		values (@interestrate, 0, 0, 0, 0, 0, 0, @monthlypayment, @financetypeid)
		set @intakeid = SCOPE_IDENTITY()

		print 'housing loans'
		declare @loan int
		set @loan = NULL
		insert into Housing_loans(PropertyID,CurrentLenderID,UseOrigLenderID, OrigLoanAmt,IntakeSameAsCurrent,Loan1st2nd,UseInReports,LoanDelinquencyMonths, IntakeDetailID, LoanOriginationDate, CurrentLoanBalanceAmt)
		values(@property, @lender, 0, @originalbalance, 1, 1, 1, @moPastDue, @intakeid, @loanoriginaldate, @currentbalance)
		set @loan = SCOPE_IDENTITY() 

		-- ASSET TABLE
			PRINT 'inserting data to asset table'

			EXEC dbo.create_asset @clientid
				,@alimony
				,@childsupport
				,@socialsecurity
				,@savings
				,@selfemployment
				,@parttime
				,@foodstamp
				,@otherincome
				,@rentalincome

		

	print 'insert secured properties'
	-- insert secured properties
	EXEC create_bk_secured_properties @clientid, @carval, @homeval, @otherval, @realestatevalue, @savingsvalue, @retirementvalue

	print 'insert budgets'
	exec insert_bk_budgets @clientid
						  ,@homemaintenance
						  ,@medicalinsurance
						  ,@vehiclepayment
						  ,@carinsurance
						  ,@utilities
						  ,@telephone
						  ,@grocery
						  ,@foodaway
						  ,@carmaintenance
						  ,@publictransportation
						  ,@medicalprescription
						  ,@miscellaneous
						  ,@education
						  ,@childsupportalimony
						  ,@laundry
						  ,@clothing
						  ,@beautybarber
						  ,@personalexpense
						  ,@clubdues
						  ,@contributions
						  ,@recreation
						  ,@gifts
						  ,@exp_savings
						  ,@trash
						  ,@gas

	-- Insert Client IN Number mapping
	--print 'insert client IN Number mapping'
	--INSERT INTO [dbo].[ClientInNumberMapping]([ClientID], [InNumber])
    -- VALUES (@clientID, @clientINNumber)


		--select @servicerid = creditor_id from creditors where creditor_name = (select name from CreditorDetails where ID = @creditorDetailID)
--INSERT INTO [dbo].[client_creditor_balances]
--           ([client_creditor]
--           ,[orig_balance]
--           ,[orig_balance_adjustment]
--           ,[total_payments]
--           ,[total_interest]
--           ,[current_sched_payment]
--           ,[total_sched_payment]
--           ,[payments_month_0]
--           ,[payments_month_1]
--           ,[zero_bal_status]
--           ,[zero_bal_letter_date]
--           ,[created_by]
--           ,[date_created])
--     VALUES
--           (<client_creditor, [dbo].[typ_key],>
--           ,<orig_balance, money,>
--           ,<orig_balance_adjustment, money,>
--           ,<total_payments, money,>
--           ,<total_interest, money,>
--           ,<current_sched_payment, money,>
--           ,<total_sched_payment, money,>
--           ,<payments_month_0, money,>
--           ,<payments_month_1, money,>
--           ,<zero_bal_status, int,>
--           ,<zero_bal_letter_date, datetime,>
--           ,<created_by, [dbo].[typ_counselor],>
--           ,<date_created, [dbo].[typ_date],>)
--GO


		--INSERT INTO [dbo].[client_creditor]
  --         ([client_creditor_balance]
  --         ,[client]
  --         ,[creditor]
  --         ,[creditor_name]
  --         ,[line_number]
  --         ,[priority]
  --         ,[drop_date]
  --         ,[drop_reason]
  --         ,[drop_reason_sent]
  --         ,[person]
  --         ,[client_name]
  --         ,[non_dmp_payment]
  --         ,[non_dmp_interest]
  --         ,[disbursement_factor]
  --         ,[date_disp_changed]
  --         ,[orig_dmp_payment]
  --         ,[months_delinquent]
  --         ,[start_date]
  --         ,[dmp_interest]
  --         ,[dmp_payout_interest]
  --         ,[last_stmt_date]
  --         ,[irs_form_on_file]
  --         ,[student_loan_release]
  --         ,[balance_verification_release]
  --         ,[last_payment_date_b4_dmp]
  --         ,[expected_payout_date]
  --         ,[rpps_client_type_indicator]
  --         ,[terms]
  --         ,[percent_balance]
  --         ,[client_creditor_proposal]
  --         ,[contact_name]
  --         ,[payments_this_creditor]
  --         ,[returns_this_creditor]
  --         ,[interest_this_creditor]
  --         ,[sched_payment]
  --         ,[proposal_prenote_date]
  --         ,[send_bal_verify]
  --         ,[verify_request_date]
  --         ,[balance_verify_date]
  --         ,[balance_verify_by]
  --         ,[first_payment]
  --         ,[last_payment]
  --         ,[account_number]
  --         ,[message]
  --         ,[hold_disbursements]
  --         ,[send_drop_notice]
  --         ,[last_communication]
  --         ,[reassigned_debt]
  --         ,[fairshare_pct_check]
  --         ,[fairshare_pct_eft]
  --         ,[prenote_date]
  --         ,[check_payments]
  --         ,[payment_rpps_mask]
  --         ,[payment_rpps_mask_timestamp]
  --         ,[rpps_mask]
  --         ,[rpps_mask_timestamp]
  --         ,[created_by]
  --         ,[date_created])
  --   VALUES
  --         (@clientid
  --         ,<creditor, [dbo].[typ_creditor],>
  --         ,<creditor_name, varchar(80),>
  --         ,<line_number, int,>
  --         ,<priority, int,>
  --         ,<drop_date, datetime,>
  --         ,<drop_reason, [dbo].[typ_key],>
  --         ,<drop_reason_sent, datetime,>
  --         ,<person, int,>
  --         ,<client_name, varchar(255),>
  --         ,<non_dmp_payment, money,>
  --         ,<non_dmp_interest, [dbo].[typ_fairshare_rate],>
  --         ,<disbursement_factor, money,>
  --         ,<date_disp_changed, datetime,>
  --         ,<orig_dmp_payment, money,>
  --         ,<months_delinquent, int,>
  --         ,<start_date, datetime,>
  --         ,<dmp_interest, float,>
  --         ,<dmp_payout_interest, [dbo].[typ_fairshare_rate],>
  --         ,<last_stmt_date, datetime,>
  --         ,<irs_form_on_file, bit,>
  --         ,<student_loan_release, bit,>
  --         ,<balance_verification_release, bit,>
  --         ,<last_payment_date_b4_dmp, datetime,>
  --         ,<expected_payout_date, datetime,>
  --         ,<rpps_client_type_indicator, varchar(1),>
  --         ,<terms, int,>
  --         ,<percent_balance, float,>
  --         ,<client_creditor_proposal, [dbo].[typ_key],>
  --         ,<contact_name, varchar(255),>
  --         ,<payments_this_creditor, money,>
  --         ,<returns_this_creditor, money,>
  --         ,<interest_this_creditor, money,>
  --         ,<sched_payment, money,>
  --         ,<proposal_prenote_date, datetime,>
  --         ,<send_bal_verify, int,>
  --         ,<verify_request_date, datetime,>
  --         ,<balance_verify_date, datetime,>
  --         ,<balance_verify_by, varchar(50),>
  --         ,<first_payment, [dbo].[typ_key],>
  --         ,<last_payment, [dbo].[typ_key],>
  --         ,<account_number, [dbo].[typ_client_account],>
  --         ,<message, [dbo].[typ_message],>
  --         ,<hold_disbursements, bit,>
  --         ,<send_drop_notice, bit,>
  --         ,<last_communication, [dbo].[typ_trace_number],>
  --         ,<reassigned_debt, bit,>
  --         ,<fairshare_pct_check, float,>
  --         ,<fairshare_pct_eft, float,>
  --         ,<prenote_date, datetime,>
  --         ,<check_payments, int,>
  --         ,<payment_rpps_mask, [dbo].[typ_key],>
  --         ,<payment_rpps_mask_timestamp, binary(8),>
  --         ,<rpps_mask, [dbo].[typ_key],>
  --         ,<rpps_mask_timestamp, binary(8),>
  --         ,<created_by, [dbo].[typ_counselor],>
  --         ,<date_created, [dbo].[typ_date],>)


				
END
GO
