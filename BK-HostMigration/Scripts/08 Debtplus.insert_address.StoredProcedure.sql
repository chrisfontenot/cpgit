/****** Object:  StoredProcedure [dbo].[insert_address]    Script Date: 2/6/2017 7:12:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





create procedure [dbo].[insert_address](
	@address1 varchar(50),
	@address2 varchar(50),
	@address3 varchar(50),
	@city	  varchar(20),
	@statecode varchar(10),
	@zipcode   varchar(15),
	@addressid int output,
	@attn	  varchar(50) output
)
as
begin

	declare @creditorprefix1	varchar(100)
	declare @creditorprefix2	varchar(100)
	declare @house				varchar(50)
	declare @direction			varchar(50)
	declare @suite				varchar(50)
	declare @suitevalue			varchar(50)
	declare @suffix				varchar(50)
	declare @street				varchar(50)
	declare @addressline2		varchar(100)
	--declare @city				varchar(50)
	declare @stateid			int
	declare @suffixabbreviation	varchar(20)
	declare @directionabbreviation	varchar(20)

	set @address1 = dbo.removespaces(@address1)
	set @address2 = dbo.removespaces(@address2)
	--set @address3 = CredAbilityInput.dbo.removespaces(@address3)

	select @creditorprefix1  = ''
	select @creditorprefix2  = ''
	select @house = ''
	select @direction = ''
	select @suite = ''
	select @suitevalue = ''
	select @suffix = ''
	select @street = ''
	select @addressline2 = ''
	--select @city = ''
	select @stateid = null
	select @suffixabbreviation = null

	if len(@address1) = 0
	begin
		if @address2 LIKE '%BOX%'
		begin
			select @street       = @address2
		end
		else
		begin
			exec parse_address_with_house @address2, @house output, @direction output, @street output, @suffix output, @suite output, @suitevalue output
		end
	end
	else
	begin

		if @address1 LIKE '%BOX%'
		begin
			select @street = @address1				

			if @address2 LIKE '%ATTN%'
			begin
				--select @attn = ltrim(rtrim( replace( replace(@address2, 'ATTN', ''), ':', '') ))
				select @creditorprefix1 = ltrim(rtrim(isnull(LEFT(@address2, charindex('ATTN', @address2)-1),'')))

				select @attn = ltrim(rtrim(isnull(replace(RIGHT(@address2, len(@address2) - (charindex('ATTN', @address2)+3)), ':',''),'')))
			end
			else
			begin
				select @addressline2 = @address2
			end

		end
		else
		begin

			-- if the address start with ATTN, assing the name to creditor prefix 1
			-- parse other details from addressline2
			if @address1 like '%ATTN%'
			begin

				select @creditorprefix1 = ltrim(rtrim(isnull(LEFT(@address1, charindex('ATTN', @address1)-1),'')))

				select @attn = ltrim(rtrim(isnull(replace(RIGHT(@address1, len(@address1) - (charindex('ATTN', @address1)+3)), ':',''),'')))

				if @address2 LIKE '%BOX%'
				begin
					select @street = @address2
				end
				else
				begin
					exec parse_address_with_house @address2, @house output, @direction output, @street output, @suffix output, @suite output, @suitevalue output	
				end

			end
			else
			begin
				
				-- if the address starts with a number process the house number
				if @address1 LIKE '[0-9]%'
				begin
					----print 'start with number'
					exec parse_address_with_house @address1, @house output, @direction output, @street output, @suffix output, @suite output, @suitevalue output	

					if @address2 LIKE '%ATTN%'
					begin
						--select @attn = ltrim(rtrim( replace( replace(@address2, 'ATTN', ''), ':', '') ))
						select @creditorprefix1 = ltrim(rtrim(isnull(LEFT(@address2, charindex('ATTN', @address2)-1),'')))

						select @attn = ltrim(rtrim(isnull(replace(RIGHT(@address2, len(@address2) - (charindex('ATTN', @address2)+3)), ':',''),'')))
					end
					else
					begin
						select @addressline2 = isnull(@address2, '')
					end
				end
				else
				begin
					--print 'does not start with number'					

					if @address2 LIKE '%BOX%'
					begin
						select @creditorprefix1 = @address1
						select @street = @address2
					end
					else
					begin
						if @address2 LIKE '%ATTN%'
						begin
							--select @attn = ltrim(rtrim( replace( replace(@address2, 'ATTN', ''), ':', '') ))
							select @creditorprefix1 = ltrim(rtrim(isnull(LEFT(@address2, charindex('ATTN', @address2)-1),'')))

							select @attn = ltrim(rtrim(isnull(replace(RIGHT(@address2, len(@address2) - (charindex('ATTN', @address2)+3)), ':',''),'')))

							-- if the creditor prefix was already assgined from address line 2 
							-- skip this
							if len(@creditorprefix1) > 0
							begin
								select @creditorprefix2 = @address1
							end
						end
						else
						begin

							if @address2 LIKE '[0-9]%'
							begin
								select @creditorprefix1 = @address1
								exec parse_address_with_house @address2, @house output, @direction output, @street output, @suffix output, @suite output, @suitevalue output
							end
							else
							begin
								exec parse_address_with_house @address1, @house output, @direction output, @street output, @suffix output, @suite output, @suitevalue output
								select @addressline2 = isnull(@address2, '')
							end							
						end
					end
					
				end
			end			
		end

	end

	if len(@address1) = 0 and len(@address2) = 0 and len(@address3) > 0
	begin
		if @address3 LIKE '%BOX%'
		begin
			select @street = @address3
		end
	end

	select @street = ltrim(rtrim(isnull(@street,'')))

	select @stateid = state
	  from states
	 where MailingCode = @statecode
	
	if LEN(@suffix) > 0 
	begin
		-- check if the suffix description match and get the abbreviation
		select @suffixabbreviation = abbreviation
		  from postal_abbreviations 
		 where type=2 
		   and description <> '' 
		   and abbreviation <> '' 
		   and description = @suffix
		
		-- if the description does not match 
		-- insert the abbreviation if it exists in debtplus table
		if LEN(ltrim(rtrim(isnull(@suffixabbreviation,'')))) = 0 -- @suffixabbreviation is null
		begin
			select @suffixabbreviation = abbreviation
		      from postal_abbreviations 
		     where type=2 
		       and description <> '' 
		       and abbreviation <> '' 
		       and abbreviation = @suffix
		end			
	end

	if LEN(@direction) > 0 
	begin
		-- check if the direction description match and get the abbreviation
		select @directionabbreviation = abbreviation
		  from postal_abbreviations 
		 where type=1 
		   and description <> '' 
		   and abbreviation <> '' 
		   and description = @direction
		
		-- if the description does not match 
		-- insert the abbreviation if it exists in debtplus table
		if LEN(ltrim(rtrim(isnull(@directionabbreviation,'')))) = 0 -- @directionabbreviation is null
		begin
			select @directionabbreviation = abbreviation
		      from postal_abbreviations 
		     where type=1
		       and description <> '' 
		       and abbreviation <> '' 
		       and abbreviation = @direction
		end			
	end
	

	insert into addresses(creditor_prefix_1, creditor_prefix_2, house, direction, street, suffix, modifier, modifier_value, address_line_2, address_line_3, city, state, PostalCode)
	values(isnull(@creditorprefix1,''), isnull(@creditorprefix2,''), isnull(@house,''), isnull(@directionabbreviation,''), isnull(@street,''), isnull(@suffixabbreviation,''), isnull(@suite,''), isnull(@suitevalue,''), isnull(@addressline2,''), '', isnull(@city,''), isnull(@stateid,''), isnull(@zipcode,''))

	select @addressid = SCOPE_IDENTITY()
		
	print '-------------------------------------------'
	print 'actual address : ' + isnull( @address1, '') + ' ' + isnull(@address2, '') + ' ' + isnull(@city, '') + ' ' + isnull(@statecode, '') + ' ' + isnull(@zipcode, '')
	print dbo.removespaces('parsed address : ' + isnull(@attn,'') + ' ' + isnull(@house,'') + ' ' + isnull(@directionabbreviation,'') + ' ' + rtrim(ltrim(isnull(@street, ''))) + ' ' + isnull(@suffixabbreviation, '') + ' ' + isnull(@suite, '') + ' ' + isnull(@suitevalue, '') + ' ' + isnull(@addressline2, '') + ' ' + isnull(@city, '') + ' ' + isnull(convert(varchar, @stateid), '') + ' ' + isnull(@zipcode, ''))
	print 'actual address : '
	print 'A1: ' + isnull( @address1, '')
	print 'A2: ' + isnull(@address2, '')
	print 'A3: ' +  isnull(@city, '') + ' ' + isnull(@statecode, '') + ' ' + isnull(@zipcode, '')
	print 'parsed address : '
	print 'Prefix    : ' + isnull(@attn,'') 
	print 'House     : ' + isnull(@house,'') 
	print 'Direction : ' + isnull(@direction,'') 
	print 'Street    : ' + rtrim(ltrim(isnull(@street, ''))) 
	print 'Suffix    : ' + isnull(@suffixabbreviation, '')
	print 'Suite     : ' + isnull(@suite, '')
	print 'Suiteval  : ' + isnull(@suitevalue, '')
	print 'Address 2 : ' + isnull(@addressline2, '')
	print 'City      : ' + isnull(@city, '')
	print 'State     : ' + isnull(convert(varchar, @stateid), '')
	print 'Zip Code  : ' + isnull(@zipcode, '')
	print '  '
end

