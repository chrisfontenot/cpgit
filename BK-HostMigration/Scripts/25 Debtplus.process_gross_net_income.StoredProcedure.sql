/****** Object:  StoredProcedure [dbo].[process_gross_net_income]    Script Date: 2/7/2017 10:18:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[process_gross_net_income](
	@grossincome1	varchar(10),
	@netincome1		varchar(10),
	@grossincome2	varchar(10),
	@netincome2		varchar(10),
	@giout1			money	output,
	@niout1			money	output,
	@giout2			money	output,
	@niout2			money	output	
) as
begin

	declare @g1	float
	declare @n1	float
	declare	@g2	float
	declare	@n2	float

	select @grossincome1 = ltrim(rtrim(isnull(@grossincome1,'')))
	select @netincome1   = ltrim(rtrim(isnull(@netincome1,'')))
	select @grossincome2 = ltrim(rtrim(isnull(@grossincome2,'')))
	select @netincome2   = ltrim(rtrim(isnull(@netincome2,'')))

	--print '' print 'processing gross income and net income, input values'
	--print 'gross income 1 : ' + @grossincome1
	--print 'net income 1   : ' + @netincome1
	--print 'gross income 2 : ' + @grossincome2
	--print 'net income 2   : ' + @netincome2

	select @g1 = case isnumeric(@grossincome1) when 1 then CONVERT(money, @grossincome1) else CONVERT(money, 0.00) end
	select @n1 = case isnumeric(@netincome1)   when 1 then CONVERT(money, @netincome1)   else CONVERT(money, 0.00) end
	select @g2 = case isnumeric(@grossincome2) when 1 then CONVERT(money, @grossincome2) else CONVERT(money, 0.00) end
	select @n2 = case isnumeric(@netincome2)   when 1 then CONVERT(money, @netincome2)   else CONVERT(money, 0.00) end
	
	select @giout1 = @g1
	select @niout1 = @n1
	select @giout2 = @g2
	select @niout2 = @n2

	if @g1 = 0.00 and @n1 > 0.00
	begin
		select @giout1 = @n1
	end

	-- if gross income 2 is null and net income 2 is > 0, assign net income 2 to gross income 1
	-- and subtract gross income 2 from gross income 1
	if @n2 > 0.00 and @g2 = 0.00 
	begin
		select @giout2 = @n2
		select @giout1 = @giout1 - @giout2
	end

	select @giout1 = isnull(@giout1,0.00)
	select @niout1 = isnull(@niout1,0.00)
	select @giout2 = isnull(@giout2,0.00)
	select @niout2 = isnull(@niout2,0.00)

	--print '' print 'processing gross income and net income, output values'
	--print 'gross income 1 : ' + convert(varchar,isnull(@giout1,''))
	--print 'net income 1   : ' + convert(varchar,isnull(@niout1,''))
	--print 'gross income 2 : ' + convert(varchar,isnull(@giout2,''))
	--print 'net income 2   : ' + convert(varchar,isnull(@niout2,''))
end
