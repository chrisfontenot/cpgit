SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[create_name](
	@firstname	varchar(50),
	@middlename	varchar(50),
	@lastname	varchar(50),
	@nameid		int output
)
as 
begin
	insert into names(first, middle, last)
	select @firstname, @middlename, @lastname;

	select @nameid = scope_identity();
end;

