SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[get_household_info](
	@femaleheaded		varchar(10),
	@applicantgender	varchar(10),
	@dependentcount		int,
	@hasspouse			bit,
	@applicantmaritalstatus	varchar(5)	
) returns int as 
begin

	declare @householdid		int	
	select @dependentcount = isnull(@dependentcount, 0)
	select @femaleheaded   = isnull(@femaleheaded, 'N')

	if @femaleheaded = 'N' and @dependentcount = 0 and @applicantmaritalstatus != 'M' and @hasspouse = 0
	begin
		-- 1 : Single Adult
		select @householdid = 1
	end
	else 
	begin
		if @femaleheaded = 'Y'
		begin
			-- 2 : Female-headed single parent household
			select @householdid = 2
		end
		else
		begin
			if @applicantgender = 'M' and @applicantmaritalstatus != 'M' and @dependentcount > 0
			begin
				-- 3 : Male-headed single parent household
				select @householdid = 3
			end
			else
			begin
				if @applicantmaritalstatus = 'M' 
				begin
					if @dependentcount > 0
					begin
						-- 5 : Married with dependents
						select @householdid = 5
					end
					else
					begin
						-- 4 : Married without dependents
						select @householdid = 4
					end
				end
				else
				begin
					-- if the co-applicant is preset and they are not married
					if @hasspouse = 1 
					begin
						-- 6 : Two or more unrelated adults
						select @householdid = 6
					end
				end
			end
		end
	end

	-- 7 : Other
	select @householdid = isnull(@householdid, 7)
	return @householdid
end
