
/****** Object:  Table [dbo].[credit_card_payment_details]    Script Date: 2/23/2017 3:19:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[credit_card_payment_details](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_product] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NULL,
	[payment_medium] [varchar](20) NOT NULL,
	[account_number] [varchar](50) NULL,
	[expiration_date] [datetime] NULL,
	[cvv] [varchar](10) NULL,
	[credit_card_type] [varchar](10) NULL,
	[card_status_flag] [varchar](10) NULL,
	[amount] [money] NOT NULL,
	[avs_full_name] [varchar](50) NULL,
	[avs_address] [varchar](50) NULL,
	[avs_city] [varchar](50) NULL,
	[avs_state] [varchar](2) NULL,
	[avs_zipcode] [varchar](10) NULL,
	[avs_phone] [varchar](10) NULL,
	[return_code] [varchar](10) NULL,
	[confirmation_number] [varchar](10) NULL,
	[settlement_submission_date] [datetime] NULL,
	[result_message] [varchar](256) NULL,
	[request] [xml] NULL,
	[response] [xml] NULL,
	[submission_date] [dbo].[typ_date] NOT NULL,
	[submission_batch] [uniqueidentifier] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_credit_card_payment_details] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[credit_card_payment_details] ADD  CONSTRAINT [DF_Table_1_transaction_type]  DEFAULT ('CreditCard') FOR [payment_medium]
GO

ALTER TABLE [dbo].[credit_card_payment_details] ADD  CONSTRAINT [DF_credit_card_payment_details_submission_batch]  DEFAULT (newid()) FOR [submission_batch]
GO

ALTER TABLE [dbo].[credit_card_payment_details]  WITH CHECK ADD  CONSTRAINT [FK_credit_card_payment_details_client_products] FOREIGN KEY([client_product])
REFERENCES [dbo].[client_products] ([oID])
GO

ALTER TABLE [dbo].[credit_card_payment_details] CHECK CONSTRAINT [FK_credit_card_payment_details_client_products]
GO

ALTER TABLE [dbo].[credit_card_payment_details]  WITH CHECK ADD  CONSTRAINT [FK_credit_card_payment_details_clients] FOREIGN KEY([client])
REFERENCES [dbo].[clients] ([client])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[credit_card_payment_details] CHECK CONSTRAINT [FK_credit_card_payment_details_clients]
GO


