
/****** Object:  Table [dbo].[WEB_TRANS]    Script Date: 1/10/2017 12:24:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WEB_TRANS](
	[ID] [int] NOT NULL,
	[BillDate] [datetime] NULL,
	[PayType] [varchar](10) NULL,
	[BillAmount] [decimal](18, 0) NULL,
	[ClientNo] [varchar](20) NULL,
	[PaymentRefNo] [varchar](20) NULL,
	[TransType] [varchar](10) NULL,
	[InvoiceNo] [varchar](20) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

