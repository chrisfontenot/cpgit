
/****** Object:  Table [dbo].[WEB_WSCAM]    Script Date: 2/21/2017 8:52:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WEB_WSCAM](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [varchar](50) NULL,
	[Addr] [varchar](50) NULL,
	[Addr2] [varchar](50) NULL,
	[CSZ] [varchar](50) NULL,
	[CO_FullName] [varchar](50) NULL,
	[LFName] [varchar](50) NULL,
	[DayPhone] [varchar](20) NULL,
	[NitePhone] [varchar](20) NULL,
	[ApptDate] [datetime] NULL,
	[ApptType] [varchar](10) NULL,
	[Status] [varchar](10) NULL,
	[ModDate] [datetime] NULL,
	[Zip29] [varchar](10) NULL,
	[RegNum38] [varchar](20) NULL,
	[SSID] [varchar](10) NULL,
	[SSN] [varchar](10) NULL,
	[CoSSN] [varchar](10) NULL,
	[LName] [varchar](50) NULL,
	[FName] [varchar](50) NULL,
	[MName] [varchar](50) NULL,
	[CoLName] [varchar](50) NULL,
	[CoFName] [varchar](50) NULL,
	[CoMName] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[ST] [varchar](10) NULL,
	[State] [varchar](20) NULL,
	[Zip] [varchar](10) NULL,
	[Zip134] [varchar](10) NULL,
	[Email] [varchar](50) NULL,
	[InNo] [varchar](10) NULL,
	[RegNum] [varchar](10) NULL,
	[RNum] [varchar](10) NULL,
	[State161] [varchar](10) NULL,
	[Smeth] [varchar](10) NULL,
	[FirmCode] [varchar](10) NULL,
	[CaseNumber] [varchar](20) NULL,
	[PayMethod] [varchar](10) NULL,
	[PayType] [varchar](10) NULL,
	[BillDate] [datetime] NULL,
	[IPayDate] [datetime] NULL,
	[PayDate] [datetime] NULL,
	[MTCN] [varchar](10) NULL,
	[AcctNumber] [varchar](20) NULL,
	[ABANumber] [varchar](20) NULL,
	[AttyName] [varchar](50) NULL,
	[HType] [varchar](10) NULL,
	[PreScore] [int] NULL,
	[PostScore] [int] NULL,
	[BankName] [varchar](50) NULL,
	[NameOnCheck] [varchar](50) NULL,
	[BankruptcyType] [varchar](10) NULL,
	[TestDate] [datetime] NULL,
	[DCFNameOnCard] [varchar](50) NULL,
	[DCMNameOnCard] [varchar](50) NULL,
	[DCLNameOnCard] [varchar](50) NULL,
	[DCBillAddr] [varchar](50) NULL,
	[DCBillAddr2] [varchar](50) NULL,
	[DCBillCity] [varchar](50) NULL,
	[DCBillState] [varchar](50) NULL,
	[DCBillZip] [varchar](10) NULL,
	[DCAcctNum] [varchar](20) NULL,
	[DCExpiryDate] [varchar](10) NULL,
	[WebLogin] [varchar](20) NULL,
	[WebPassword] [varchar](50) NULL,
	[NonEscrowEMA] [varchar](20) NULL,
	[ConfirmationDateTime] [datetime] NULL,
	[MimPostScore] [float] NULL,
	[LastTrans] [varchar](10) NULL,
	[RcvdDate] [datetime] NULL,
	[SeqNo] [int] NULL,
	[CompletionStatus] [varchar](10) NULL,
	[ClassComplete] [varchar](10) NULL,
	[BillAmt] [decimal](18, 0) NULL,
	[ClientNumber] [varchar](15) NULL,
 CONSTRAINT [PK_WEB_WSCAM] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

