
/****** Object:  Table [dbo].[WEB_APPT_SSN_XREF]    Script Date: 1/24/2017 6:39:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WEB_APPT_SSN_XREF](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientNumber] [varchar](10) NULL,
	[SSN] [varchar](10) NULL,
 CONSTRAINT [PK_WEB_APPT_SSN_XREF] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


