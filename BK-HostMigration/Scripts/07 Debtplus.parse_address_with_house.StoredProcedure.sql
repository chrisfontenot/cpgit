/****** Object:  StoredProcedure [dbo].[parse_address_with_house]    Script Date: 2/6/2017 7:23:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[parse_address_with_house](
	@addressline	varchar(150),
	@house			varchar(50) output,
	@direction		varchar(50) output,
	@street			varchar(50) output,
	@suffix			varchar(50) output,
	@suite			varchar(50) output,
	@suitevalue		varchar(50) output
) as 
begin

	declare @addr	  CURSOR
	
	declare @ishouse	bit
	select @ishouse = 0

	-- flag for addresses that have direction on the street name N West Hamilton
	declare @isdirectionassigned	bit
	select @isdirectionassigned = 0
	
	declare @issuite	bit
	select @issuite = 0

	declare @hasampersand		bit

	declare @item		  varchar(50)
	declare @rowdata	  varchar(50)
	declare @itemindex	int
	declare @itemcount	int
	declare @X1 xml 

	-- if there is ampersand in the input replace it with &amp; to avoid xml parsing issue
	if @addressline LIKE '%&%'
	begin
		select @hasampersand = 1

		set @addressline = replace(@addressline, '&', '&amp;')

	end
	
	-- remove additional spaces
	SET @addressline = dbo.removespaces(@addressline)

	select @X1 = cast('<row>'+replace(@addressline, ' ', '</row><row>')+'</row>' as xml);
	
	SET @addr = CURSOR FOR
	select n.r.value('.', 'varchar(20)') as item
	from @X1.nodes('row') as n(r);

	select @itemcount = count(n.r.value('.', 'varchar(20)'))
	from @X1.nodes('row') as n(r);

	select @itemindex = 1
	select @ishouse	= 1
	OPEN @addr
	FETCH NEXT
	FROM @addr INTO @rowdata
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		-- clean the data, remove dots and spaces
		select @item = ltrim(rtrim(replace(isnull(@rowdata, ''), '.', '')))

		if @item LIKE '[0-9]%' and @itemindex = 1 
		begin
			--print 'assigning house...' + @item
			select @house = @item
			select @ishouse = 1
		end
		else 
		begin 
			if @issuite = 1 
			begin
				--print 'assigning suitevalue...' + @item
				select @suitevalue = isnull(@suitevalue, '') + ' ' + @item

				-- reset the suite flag
				--select @issuite = 0
			end
			else
			begin
				if @item LIKE 'STE%' or @item LIKE 'SUITE%' or @item LIKE 'BLDG%' or @item LIKE '%#%' or @item LIKE '%[0-9]-%' or @item LIKE '%-[0-9]%'
				begin
					--print 'assigning suite...' + @item
					select @suite = @item

					-- set the suite flag to get the suite value
					-- in the next iteration
					select @issuite = 1
				end
				else
				begin
					--select @issuite = 0

					if @item = 'ST' or @item = 'STREET' or @item = 'BLVD' or @item = 'BOULEVARD' or @item = 'RD' or @item = 'ROAD' 
					or @item = 'DR' or @item = 'DRIVE' or @item = 'AVE' or @item = 'AVENUE' or @item = 'LANE' or @item = 'PKWY' or @item = 'PARKWAY'
					or @item = 'BV' or @item = 'TRACE' or @item = 'CR' or @item = 'CRESCENT'
					begin
						--print 'assigning suffix...' + @item
						select @suffix = @item
					end
					else 
					begin
						
						-- parse direction
						if @item = 'N' or @item = 'NORTH' or @item = 'S' or @item = 'SOUTH' or @item = 'W' or @item = 'WEST' or 
							@item = 'E' or @item = 'EAST' or @item = 'NE' or @item = 'NW' or @item = 'SE' or @item = 'SW'
						begin							

							if @isdirectionassigned = 0
							begin
								--print 'assinging direction ' + @item
								select @direction = @item
								select @isdirectionassigned = 1
							end
							else
							begin
								--print 'assinging street since direction is already assigned ' + @item
								select @street = isnull(@street, '') + ' ' + @item
							end
						end
						else
						begin
							--print 'assigning street...' + @item
							-- append rest of the data to the street
							select @street = isnull(@street, '') + ' ' + @item

							--print 'street value : ' + isnull(@street, 'null')
						end
					end						
				end
			end			
		end;		

		FETCH NEXT
		FROM @addr INTO @rowdata

		select @itemindex = @itemindex + 1
	END

	-- if there was a ampersand replacement in the address, replace it back
	if @hasampersand = 1
	begin

		select @house       = case ltrim(rtrim(isnull(@house,''))) when '' then '' else replace(@house, '&amp;', '&') end
		select @direction   = case ltrim(rtrim(isnull(@direction,''))) when '' then '' else replace(@direction, '&amp;', '&') end
		select @street      = case ltrim(rtrim(isnull(@street,''))) when '' then '' else replace(@street, '&amp;', '&') end
		select @suffix      = case ltrim(rtrim(isnull(@suffix,''))) when '' then '' else replace(@suffix, '&amp;', '&') end
		select @suite       = case ltrim(rtrim(isnull(@suite,''))) when '' then '' else replace(@suite, '&amp;', '&') end
		select @suitevalue  = case ltrim(rtrim(isnull(@suitevalue,''))) when '' then '' else replace(@suitevalue, '&amp;', '&') end

	end

	----print 'actual address : ' + @address1
	-- --print 'parsed address : ' + isnull(@house,'') + ' ' + isnull(@direction,'') + ' ' + rtrim(ltrim(isnull(@street, ''))) + ' ' + isnull(@suffix, '') + ' ' + isnull(@suite, '') + ' ' + isnull(@suitevalue, '')

	CLOSE @addr
	DEALLOCATE @addr
end

