SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FeeWaiverType](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](4) NULL,
	[rpps] [varchar](4) NULL,
	[epay] [varchar](4) NULL,
	[note] [dbo].[typ_message] NULL,
	[hud_9902_section] [varchar](256) NULL,
	[hpf] [varchar](50) NULL,
	[default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_FeeWaiverType] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

insert into [FeeWaiverType](description, [default], activeflag, created_by, date_created) values ('Income', 0, 1, 'sa', getdate())
insert into [FeeWaiverType](description, [default], activeflag, created_by, date_created) values ('SSDI', 0, 1, 'sa', getdate())
insert into [FeeWaiverType](description, [default], activeflag, created_by, date_created) values ('ProBono', 0, 1, 'sa', getdate())
insert into [FeeWaiverType](description, [default], activeflag, created_by, date_created) values ('GoodWill', 0, 1, 'sa', getdate())
insert into [FeeWaiverType](description, [default], activeflag, created_by, date_created) values ('LegalAid', 0, 1, 'sa', getdate())


