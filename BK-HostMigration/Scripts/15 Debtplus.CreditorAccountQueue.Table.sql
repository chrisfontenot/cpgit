
/****** Object:  Table [dbo].[CreditorAccountQueue]    Script Date: 12/30/2016 7:00:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CreditorAccountQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FinalID] [varchar](50) NOT NULL,
	[CreditorBalance] [decimal](18, 0) NOT NULL,
	[CreditorPayment] [decimal](18, 0) NOT NULL,
	[CreditorName] [varchar](50) NULL,
	[CreditorJointAccount] [varchar](50) NULL,
	[CreditorAccountNumberEnc] [varchar](100) NULL,
	[PreAccountNumberEnc] [varchar](100) NULL,
 CONSTRAINT [PK_CreditorAccountQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


