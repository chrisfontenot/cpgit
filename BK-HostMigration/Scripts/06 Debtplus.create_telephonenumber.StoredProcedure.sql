/****** Object:  StoredProcedure [dbo].[create_telephonenumber]    Script Date: 2/6/2017 8:08:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[create_telephonenumber](
	@completenumber		varchar(50),
	@phoneareacode  	varchar(50),
	@phonenumber     	varchar(50),
	@extension	     	varchar(10),
	@phoneid		    int output
) as
begin

	declare	@areacode		varchar(5);
	declare	@number			varchar(10);

	declare @areacodehadthenumber bit
	select @areacodehadthenumber = 0

	declare @areacodeandnumberwerevalid bit
	select @areacodeandnumberwerevalid = 0

	declare @uscountrycode	int;
	select @uscountrycode = 1

	-- filter numbers that have space, dot and hypen
	select @completenumber  = ltrim(rtrim(replace(replace(replace(replace(replace(isnull(@completenumber,''),'-',''),' ',''),'.',''), ')', ''), '(', '')))
	select @phoneareacode   = ltrim(rtrim(replace(replace(replace(replace(replace(isnull(@phoneareacode,''),'-',''),' ',''),'.',''), ')', ''), '(', '')))
	select @phonenumber     = ltrim(rtrim(replace(replace(replace(replace(replace(isnull(@phonenumber,''),'-',''),' ',''),'.',''), ')', ''), '(', '')))
	select @extension       = ltrim(rtrim(replace(replace(replace(replace(replace(isnull(@extension,''),'-',''),' ',''),'.',''), ')', ''), '(', '')))

	-- remove all alphabets
	SELECT @completenumber = LEFT(SUBSTRING(@completenumber, PATINDEX('%[0-9]%', @completenumber), 8000),
           PATINDEX('%[^0-9]%', SUBSTRING(@completenumber, PATINDEX('%[0-9]%', @completenumber), 8000) + 'X') -1)

	print 'complete number after trim : ' + isnull(@completenumber, 'null')

	-- @phoneareacode    : contains the area code of the phone number in most cases
	-- @phonenumber      : contains the phone number without area code
	-- @phonefullnumber  : contains the complete 10 digit phone number, process this if the area code 
	--                     and phone number are invalid or empty

	-- in some cases area code has the complete phone number and the phone number is empty
	if len(@phoneareacode) = 10 and isnumeric(@phoneareacode) = 1 and len(@phonenumber) = 0
	begin
		select @areacode = left(@phoneareacode, 3),
		       @number   = substring(@phoneareacode, 4,3) + '-' + substring(@phoneareacode, 7,4) 

		select @areacodehadthenumber = 1
	end

	-- process area code and phone number
	if (len(@phoneareacode) = 3 and isnumeric(@phoneareacode) = 1) or (len(@phonenumber) = 7 and isnumeric(@phonenumber) = 1)
	begin
		-- select the area code
		if len(@phoneareacode) = 3 and isnumeric(@phoneareacode) = 1
		begin
			select @areacode = @phoneareacode
		end

		-- select the phone number
		if len(@phonenumber) = 7 and isnumeric(@phonenumber) = 1
		begin
			select @number   = substring(@phonenumber, 1,3) + '-' + substring(@phonenumber, 4,4) 
		end

		select @areacodeandnumberwerevalid = 1
	end

	if @areacodehadthenumber = 0 and @areacodeandnumberwerevalid = 0
	begin
		if len(@completenumber) = 10 and ISNUMERIC(@completenumber) = 1
		begin
			select @areacode = left(@completenumber, 3),
		           @number   = substring(@completenumber, 4,3) + '-' + substring(@completenumber, 7,4) 
		end
		else 
		begin
			-- allow the phone number to be inserted if the length is 7 digits
			if len(@completenumber) = 7 
			begin
				-- insert the 7 digit phone number and area code as 000
				select @areacode = '000', @number = @completenumber
			end
		end
	end
	
	-- edit : allow the phone number to be inserted even if the area code is empty
	-- if @areacode != '' and @areacode != '000' and @number != '' and @number != '000-0000'
	if @number != '' and @number != '000-0000'
	begin
		insert into telephonenumbers(country, acode, number, Ext)
		select @uscountrycode, @areacode, @number, @extension;

		select @phoneid = scope_identity();
	end
	else
	begin
		select @phoneid = null

		print 'phone number is invalid or empty'
	end
end
