﻿using System;
using System.Collections.Generic;
using System.Text;
using IBMU2.UODOTNET;
using Cccs.Host.Dal;
using Cccs.Host.Dto;

namespace Cccs.Host.Impl
{
	public partial class Host : IHost
	{
		public JudicialDistrict GetJudicialDistrict(string zipCode)
		{
			JudicialDistrict ClientDist = new JudicialDistrict();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile JudDis = UvSess.CreateUniFile("WEB.DISTRICT.ZIP"))
				using(UniDynArray DistRec = JudDis.Read(zipCode))
				{
					ClientDist.DistrictName = DistRec.Extract(1).ToString().Trim();
					using(UniFile EoustCont = UvSess.CreateUniFile("WEB.EOUST.CONTROL"))
					using(UniDynArray ContRec = EoustCont.Read("JUDICIAL.DISTRICTS"))
					{
						for(int i = 0; i < ContRec.Dcount(); i++)
						{
							if(ContRec.Extract(i, 1).ToString().IndexOf(ClientDist.DistrictName, 0) > 1)
							{
								ClientDist.DistrictCode = ContRec.Extract(i, 1).ToString().Split('>')[0];
								i = ContRec.Dcount();
							}
						}
					}
				}
			}
			catch
			{
			}
			return ClientDist;
		}

		public string GetEoustJudicialDistrictByName(string districtName)
		{
			try
			{
				using(var session = OpenUniSession())
				using(var file = session.CreateUniFile("WEB.EOUST.CONTROL"))
				using(var record = file.Read("JUDICIAL.DISTRICTS"))
				{
					for(int i = 0; i < record.Dcount(); i++)
					{
						if(record.Extract(i, 1).StringValue.IndexOf(districtName, 0) > 1)
						{
							return record.Extract(i, 1).StringValue.Split('>')[0];
						}
					}
				}
			}
			catch
			{
			}

			return null;
		}

		public EoustLogin GetEoustLogin()
		{
			EoustLogin CounsLogIn = new EoustLogin();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile LogInFile = UvSess.CreateUniFile("WEB.EOUST.CONTROL"))
				using(UniDynArray LogInRec = LogInFile.Read("LOGINS"))
				{
					for(int i = 3; i < LogInRec.Dcount(); i++)
					{
						if(LogInRec.Extract(i, 1).ToString() == eoustUser)
						{
							CounsLogIn.Username = LogInRec.Extract(i, 2).ToString().Trim();
							CounsLogIn.Password = LogInRec.Extract(i, 3).ToString().Trim();
							CounsLogIn.EoustId = LogInRec.Extract(i, 4).ToString().Trim();
							i = LogInRec.Dcount();
						}
					}
				}
			}
			catch
			{
			}
			return CounsLogIn;
		}

		public string GetFirmEmail(string FirmID)
		{
			string FirmEmail = "";
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile AttyFirm = UvSess.CreateUniFile("WEB_ATTY_FIRM"))
				using(UniDynArray FirmRec = AttyFirm.Read(FirmID))
				{
					FirmEmail = FirmRec.Extract(11).ToString();
				}
			}
			catch
			{
			}
			return FirmEmail;
		}

		public bool SetCertNo(CertReIssue ReIssueData)
		{
			bool Back = false;
			int SpSpot;
			if(ReIssueData.IsPrimary)
				SpSpot = 1;
			else
				SpSpot = 2;
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile BrCam = UvSess.CreateUniFile("WEB.BRCAM"))
				using(UniDynArray ClntRec = BrCam.Read(ReIssueData.ClNo))
				{
					ClntRec.Replace(216, SpSpot, ReIssueData.CertNo);
					ClntRec.Replace(218, SpSpot, 1, UvSess.Iconv(ReIssueData.CertIssue.ToShortDateString(), "D"));
					ClntRec.Replace(218, SpSpot, 2, ReIssueData.CertIssue.ToShortTimeString());
					BrCam.Write(ReIssueData.ClNo, ClntRec);
					Back = true;
				}
			}
			catch
			{
			}
			return Back;
		}
	}
}