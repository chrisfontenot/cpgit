﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IBMU2.UODOTNET;


namespace Cccs.Host.Impl
{
	public partial class Host : IHost
	{
        public CaAccountTransactionsResult CaTrans(String ClientNo,String TransOpt)
        {
            CaAccountTransactionsResult result = new CaAccountTransactionsResult();
            bool WriteRec;
            double DepVal,PayVal;
            String TypeTxt;
            try
            {
                using(UniSession UvSess = OpenUniSession())
                {
                    using(UniSubroutine SubClTf = UvSess.CreateUniSubroutine("CALL.GET.LTRANS.SUB", 2))
                    {
                        SubClTf.SetArg(0, ClientNo);
                        SubClTf.Call();
                        String[] LastStuff = SubClTf.GetArg(1).Split('~');
                        result.LastDepositDate = LastStuff[0].Replace('-', '/');
                        result.LastDepositAmount = LastStuff[1];
                        result.LastDisbursementDate = LastStuff[2].Replace('-', '/');
                        result.LastDisbursementAmount = LastStuff[3];
                    }
                    UniFile UvTrans = UvSess.CreateUniFile("WEB.CLTF");
                    UniDynArray TransRec = UvTrans.Read(ClientNo);
                    CaActivity[] HoldTrans;
                    int CurCnt = 0,MaxCnt = 50;
                    if(TransRec.Dcount() < MaxCnt)
                        HoldTrans = new CaActivity[MaxCnt];
                    else
                        HoldTrans = new CaActivity[TransRec.Dcount()];
                    for(int i = TransRec.Dcount(); i >= 0; i--)
                    {
                        TypeTxt = "";
                        DepVal = 0;
                        PayVal = 0;
                        WriteRec = true;
                        switch(TransRec.Extract(i, 2).ToString().Trim())
                        {
                            case "RF":
                                double.TryParse(TransRec.Extract(i, 4).ToString().Trim(), out DepVal);
                                TypeTxt = "Client Refund";
                                if(TransOpt == "Dis")
                                    WriteRec = false;
                                break;
                            case "AD":
                                double.TryParse(TransRec.Extract(i, 5).ToString().Trim(), out PayVal);
                                TypeTxt = "Disbursement";
                                if(TransOpt == "Dep")
                                    WriteRec = false;
                                break;
                            case "CM":
                                double.TryParse(TransRec.Extract(i, 5).ToString().Trim(), out PayVal);
                                TypeTxt = "Disbursement";
                                if(TransOpt == "Dep")
                                    WriteRec = false;
                                break;
                            case "VD":
                                double.TryParse(TransRec.Extract(i, 4).ToString().Trim(), out DepVal);
                                TypeTxt = "Returned From Creditor";
                                if(TransOpt == "Dis")
                                    WriteRec = false;
                                break;
                            case "CR":
                                double.TryParse(TransRec.Extract(i, 4).ToString().Trim(), out DepVal);
                                TypeTxt = "Returned From Creditor";
                                if(TransOpt == "Dis")
                                    WriteRec = false;
                                break;
                            case "DP5":
                                double.TryParse(TransRec.Extract(i, 4).ToString().Trim(), out DepVal);
                                if(DepVal > 0)
                                    TypeTxt = "Autodraft";
                                else
                                    TypeTxt = "Insufficient Funds";
                                if(TransOpt == "Dis")
                                    WriteRec = false;
                                break;
                            default:
                                if(TransRec.Extract(i, 2).ToString().Trim().StartsWith("DP"))
                                {
                                    double.TryParse(TransRec.Extract(i, 4).ToString().Trim(), out PayVal);
                                    TypeTxt = "Deposit";
                                    if(TransOpt == "Dis")
                                        WriteRec = false;
                                }
                                break;
                        }
                        if(!(DepVal == 0 && PayVal == 0) && WriteRec)
                        {
                            HoldTrans[CurCnt].Date = UvSess.Oconv(TransRec.Extract(i, 1).ToString().Trim(), "D4/");
                            HoldTrans[CurCnt].Type = TypeTxt;
                            HoldTrans[CurCnt].Payment = PayVal;
                            HoldTrans[CurCnt].Deposit = DepVal;
                            CurCnt++;
                            if(CurCnt >= MaxCnt)
                            {
                                result.MaxMsg = "Only the last " + MaxCnt.ToString() + " transactions can be displayed at this time. If you need further transaction history contact: Client Services at info@cccsatl.org or 800.251.2227";
                                i = -1;
                            }
                        }
                    }
                    result.Transaction = new CaActivity[CurCnt];
                    for(int i = 0; i < CurCnt; i++)
                        result.Transaction[i] = HoldTrans[i];
                    result.IsSuccessful = true;
                }
            }
            catch(Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public CaCredResult CaCreds(String ClientNo)
        {
            CaCredResult result = new CaCredResult();
            bool CanVer, IsGood = true;
            try
            {
                using(UniSession UvSess = OpenUniSession())
                {
                    using(UniFile ClntMast = UvSess.CreateUniFile("WEB.CM"))
                    {
                        try
                        {
                            UniDynArray CmRec = ClntMast.Read(ClientNo);
                            result.PlanStartDate = UvSess.Oconv(CmRec.Extract(39).ToString().Trim(), "D4/");
                            result.DueDate = UvSess.Oconv(CmRec.Extract(27).ToString().Trim(), "D4/");
                            result.DueAmount = CmRec.Extract(29).ToString().Trim();
                            CmRec.Dispose();
                        }
                        catch(UniFileException ex)
                        {
                            result.Exception = ex;
                            IsGood = false;
                        }
                        ClntMast.Close();
                    }
                    if(IsGood)
                    {
                        using(UniFile UvAch = UvSess.CreateUniFile("WEB.ACHFILE"))
                        {
                            try
                            {
                                UniDynArray AchRec = UvAch.Read(ClientNo);
                                result.AutoPay = AchRec.Extract(5).ToString().Trim();
                                if(result.AutoPay == "")
                                {
                                    result.AutoPay = "N";
                                }
                                AchRec.Dispose();
                            }
                            catch(UniFileException)
                            {
                                result.AutoPay = "N";
                            }
                            UvAch.Close();
                        }
                        using(UniSubroutine UvCcf = UvSess.CreateUniSubroutine("GET.CCF.AMTS", 5))
                        {
                            UvCcf.SetArg(0, ClientNo);
                            UvCcf.Call();
                            result.TotalPTD = UvCcf.GetArg(1).Trim();
                            result.TotalOriginalDebt = UvCcf.GetArg(2).Trim();
                            result.TotalPTM = UvCcf.GetArg(3).Trim();
                            result.TotalEstBal = UvCcf.GetArg(4).Trim();
                        }
                        String[] MasterRec, CredId, CredName, AccntNo, OBal, PtdAmt, SchPay, PtmAmt, EstBal, IntRate
                            , EstPayDTS, BalVer, VerDTS, PropOnFile, DispFact;
                        Char FldMrk = (Char)254, ValMrk = (Char)253;
                        using(UniSubroutine CcfDet = UvSess.CreateUniSubroutine("GET.CCF.DET.AMTS", 2))
                        {
                            CcfDet.SetArg(0, ClientNo);
                            CcfDet.Call();
                            MasterRec = CcfDet.GetArg(1).Split(FldMrk);
                        }
                        CredId = MasterRec[0].Split(ValMrk);
                        CredName = MasterRec[1].Split(ValMrk);
                        AccntNo = MasterRec[2].Split(ValMrk);
                        OBal = MasterRec[3].Split(ValMrk);
                        PtdAmt = MasterRec[4].Split(ValMrk);
                        SchPay = MasterRec[5].Split(ValMrk);
                        PtmAmt = MasterRec[6].Split(ValMrk);
                        EstBal = MasterRec[7].Split(ValMrk);
                        IntRate = MasterRec[8].Split(ValMrk);
                        EstPayDTS = MasterRec[9].Split(ValMrk);
                        BalVer = MasterRec[10].Split(ValMrk);
                        VerDTS = MasterRec[11].Split(ValMrk);
                        PropOnFile = MasterRec[12].Split(ValMrk);
                        DispFact = MasterRec[13].Split(ValMrk);
                        result.Creditor = new CaCreditor[CredId.Length];
                        float TestDis;
                        for(int i = 0; i < CredId.Length; i++)
                        {
                                result.Creditor[i] = new CaCreditor();
                                result.Creditor[i].AccountID = CredId[i].Trim();
                                result.Creditor[i].CreditorName = CredName[i].Trim();
                                result.Creditor[i].AccountNumber = AccntNo[i].Trim();
                                result.Creditor[i].OriginalBalance = OBal[i].Trim();
                                result.Creditor[i].PaidToDate = PtdAmt[i].Trim();
                                result.Creditor[i].ScheduledPayment = SchPay[i].Trim();
                                result.Creditor[i].PaidThisMonth = PtmAmt[i].Trim();
                                result.Creditor[i].Balance = EstBal[i];
                                result.Creditor[i].InterestRate = IntRate[i].Trim();
                                result.Creditor[i].PayoutDate = EstPayDTS[i].Trim().Replace('-', '/');
                                result.Creditor[i].VerifiedBalance = BalVer[i].Trim();
                                result.Creditor[i].VerifiedDate = VerDTS[i].Trim().Replace('-', '/');
                                result.Creditor[i].ProposalOnFile = PropOnFile[i].Trim();

                                CanVer = false;
                                TestDis = 0;
                                if (float.TryParse(DispFact[i].Trim(), out TestDis))
                                {
                                    if (TestDis > 0)
                                    {
                                        if (AccntNo[i].IndexOf("tran") > 0 || CredId[i].IndexOf("C0093") > 0 || CredId[i].IndexOf("C4635") > 0 || CredId[i].IndexOf("C5859") > 0)
                                            CanVer = false;
                                        else
                                            CanVer = true;
                                    }
                                }
                                result.Creditor[i].VerifyCan = CanVer;
                           

                        }
                    }
                    result.IsSuccessful = true;
                }
            }
            catch(Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public CaMailPreferenceResult CaPerfGet(String ClientNo)
        {
            CaMailPreferenceResult result = new CaMailPreferenceResult();
            try
            {
                using(UniSession UvSess = OpenUniSession())
                {
                    using(UniFile UvEmail = UvSess.CreateUniFile("WEB.EMAIL.HISTORY"))
                    {
                        UniDynArray EmailRec = UvEmail.Read(ClientNo);
                        if(EmailRec.Extract(1).ToString().Trim() == "E")
                            result.Letter = "Email";
                        else
                            result.Letter = "U.S. Mail";
                        if(EmailRec.Extract(2).ToString().Trim() == "E")
                            result.Statement = "Email";
                        else
                            result.Statement = "U.S. Mail";
                    }
                    result.IsSuccessful = true;
                }
            }
            catch(Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public Result CaPerfSet(String ClientNo, String LetterPref, String StatmentPref)
        {
            Result result = new Result();
            try
            {
                using(UniSession UvSess = OpenUniSession())
                {
                        using(UniFile UvEmail = UvSess.CreateUniFile("WEB.EMAIL.HISTORY"))
                        {
                            UniDynArray EmailRec = UvEmail.Read(ClientNo);
                        LetterPref = CleanData(LetterPref.ToUpper(), "PE");
                            if(LetterPref != "")
                                EmailRec.Replace(1, LetterPref);
                        StatmentPref = CleanData(StatmentPref.ToUpper(), "PE");
                            if(StatmentPref != "")
                                EmailRec.Replace(2, StatmentPref);
                            using(UniCommand UvCmd = UvSess.CreateUniCommand())
                            {
                                UvCmd.Command = "RUN WEB.BP GET.UV.DATE";
                                UvCmd.Execute();
                                EmailRec.Replace(11, UvCmd.Response.Trim());
                            }
                            UvEmail.Write(ClientNo, EmailRec);
                            result.IsSuccessful = true;
                        }
                    }
                    }
            catch(Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public Result CaInfoSet(String ClientNo, String HomePhone, String WorkPhone, String Addr1
            , String Addr2, String City, String State, String Zip, String FirstName, String LastName
           , String Email, String EmailW)
        {
            HomePhone = HomePhone.ToUpper().Trim();
            WorkPhone = WorkPhone.ToUpper().Trim();
            Addr1 = Addr1.ToUpper().Trim();
            Addr2 = Addr2.ToUpper().Trim();
            City = City.ToUpper().Trim();
            State = State.ToUpper().Trim();
            Zip = Zip.ToUpper().Trim();
            FirstName = FirstName.ToUpper().Trim();
            LastName = LastName.ToUpper().Trim();
            Email = Email.Trim();
            EmailW = EmailW.Trim();
            Result result = new Result();
            try
            {
                using(UniSession UvSess = OpenUniSession())
                {
                    result.IsSuccessful = true;
                    try
                    {
                        using(UniFile UvCm = UvSess.CreateUniFile("WEB.CM"))
                        {
                            String OldFirstName, OldLastName, OldAddr1, OldAddr2, OldCity, OldState, OldZip
                                , OldHomePhone, OldWorkPhone, OldEmail, OldEmailW, ErrMsg;
                            UniDynArray CmRec = UvCm.Read(ClientNo);
                            CmRec.Replace(1, FirstName + " " + LastName);
                            OldAddr1 = CmRec.Extract(2).ToString().ToUpper().Trim();
                            CmRec.Replace(2, Addr1);
                            OldAddr2 = CmRec.Extract(3).ToString().ToUpper().Trim();
                            CmRec.Replace(3, Addr2);
                            CmRec.Replace(4, City + ", " + State + " " + Zip);
                            OldHomePhone = CmRec.Extract(5).ToString().ToUpper().Trim();
                            CmRec.Replace(5, HomePhone);
                            OldWorkPhone = CmRec.Extract(6).ToString().ToUpper().Trim();
                            CmRec.Replace(6, WorkPhone);
                            CmRec.Replace(10, WorkPhone.Substring(0, 3));
                            CmRec.Replace(33, LastName);
                            CmRec.Replace(56, Zip);
                            CmRec.Replace(119, HomePhone.Substring(0, 3));
                            OldLastName = CmRec.Extract(120).ToString().ToUpper().Trim();
                            CmRec.Replace(120, LastName);
                            OldFirstName = CmRec.Extract(121).ToString().ToUpper().Trim();
                            CmRec.Replace(121, FirstName);
                            CmRec.Replace(124, LastName);
                            OldCity = CmRec.Extract(128).ToString().ToUpper().Trim();
                            CmRec.Replace(128, City);
                            OldState = CmRec.Extract(129).ToString().ToUpper().Trim();
                            CmRec.Replace(129, State);
                            OldZip = CmRec.Extract(130).ToString().ToUpper().Trim();
                            CmRec.Replace(130, Zip);
                            CmRec.Replace(137, City);
                            CmRec.Replace(138, State);
                            CmRec.Replace(139, Zip);
                            OldEmail = CmRec.Extract(144).ToString().Trim();
                            CmRec.Replace(144, Email);
                            CmRec.Replace(149, WorkPhone.Substring(0, 3));
                            CmRec.Replace(152, Zip);
                            CmRec.Replace(153, WorkPhone.Substring(0, 3));
                            CmRec.Replace(161, HomePhone.Substring(3));
                            CmRec.Replace(162, WorkPhone.Substring(3));
                            OldEmailW = CmRec.Extract(166).ToString().Trim();
                            CmRec.Replace(166, EmailW);
                            CmRec.Replace(182, City);
                            CmRec.Replace(183, State);
                            CmRec.Replace(184, Zip);
                            CmRec.Replace(200, City + ", " + State + " " + Zip);
                            UvCm.Write(ClientNo, CmRec);
                            using(UniSubroutine UvNote = UvSess.CreateUniSubroutine("CNM.PUPD.SUB", 3))
                            {
                                if(OldFirstName != FirstName || OldLastName != LastName)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldName:" + OldFirstName + " " + OldLastName + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldAddr1 != Addr1)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldAddr1:" + OldAddr1 + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldAddr2 != Addr2)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldAddr2:" + OldAddr2 + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldCity != City || OldState != State || OldZip != Zip)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldCSZ:" + OldCity + ", " + OldState + " " + OldZip + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldHomePhone != HomePhone)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldPhone:" + OldHomePhone + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldWorkPhone != WorkPhone)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldWorkPhone:" + OldWorkPhone + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldEmail != Email)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldEmail:" + OldEmail + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                                if(OldEmailW != EmailW)
                                {
                                    UvNote.SetArg(0, ClientNo);
                                    UvNote.SetArg(1, "OldWorkEmail:" + OldEmailW + " CltWebSite");
                                    UvNote.Call();
                                    ErrMsg = UvNote.GetArg(2);
                                    if(ErrMsg != "0")
                                    {
                                        result.ExceptionStr = ErrMsg;
                                        result.IsSuccessful = false;
                                    }
                                }
                            }
                        }
                        using(UniFile UvCam = UvSess.CreateUniFile("WEB.CAM"))
                        {
                            UniDynArray CamRec = UvCam.Read(ClientNo);
                            CamRec.Replace(1, FirstName + " " + LastName);
                            CamRec.Replace(2, Addr1);
                            CamRec.Replace(3, Addr2);
                            CamRec.Replace(4, City + ", " + State + " " + Zip);
                            CamRec.Replace(6, LastName);
                            CamRec.Replace(7, HomePhone);
                            CamRec.Replace(8, WorkPhone);
                            CamRec.Replace(29, Zip);
                            CamRec.Replace(119, HomePhone.Substring(0, 3));
                            CamRec.Replace(120, LastName);
                            CamRec.Replace(121, FirstName);
                            CamRec.Replace(128, City);
                            CamRec.Replace(129, State);
                            CamRec.Replace(130, Zip);
                            CamRec.Replace(134, Zip);
                            CamRec.Replace(144, Email);
                            CamRec.Replace(146, LastName);
                            CamRec.Replace(149, HomePhone.Substring(0, 3));
                            CamRec.Replace(153, WorkPhone.Substring(0, 3));
                            CamRec.Replace(162, WorkPhone);
                            CamRec.Replace(166, EmailW);
                            UvCam.Write(ClientNo, CamRec);
                        }
                    }
                    catch(Exception ex)
                    {
                        result.Exception = ex;
                        result.IsSuccessful = false;
                    }
                }
            }
            catch(Exception ex)
            {
                result.Exception = ex;
                result.IsSuccessful = false;
            }
            return result;
        }

        public Result CaCredBalSet(String AccountID, float NewBal)
        {
            Result result = new Result();
            String OldBal, ErrMsg = "0";
            float TestDis;
            if(!(AccountID.IndexOf("tran") > 0 || AccountID.IndexOf("C0093") > 0 || AccountID.IndexOf("C4635") > 0 || AccountID.IndexOf("C5859") > 0))
            {
                try
                {
                    using(UniSession UvSess = OpenUniSession())
                    {
                        using(UniFile UvCcf = UvSess.CreateUniFile("WEB.CCF"))
                        {
                            UniDynArray CcfRec = UvCcf.Read(AccountID);
                            OldBal = CcfRec.Extract(6).ToString().Trim();
                            if(float.TryParse(CcfRec.Extract(8).ToString().Trim(), out TestDis))
                            {
                                if(TestDis > 0)
                                {
                                    CcfRec.Replace(6, NewBal.ToString());
                                    using(UniCommand UvCmd = UvSess.CreateUniCommand())
                                    {
                                        UvCmd.Command = "RUN WEB.BP GET.UV.DATE";
                                        UvCmd.Execute();
                                        CcfRec.Replace(16, UvCmd.Response.Trim());
                                    }
                                    CcfRec.Replace(31, "Clt Website");
                                    UvCcf.Write(AccountID, CcfRec);
                                    using(UniSubroutine UvNote = UvSess.CreateUniSubroutine("CNM.PUPD.SUB", 3))
                                    {
                                        UvNote.SetArg(0, AccountID.Split('*')[0]);
                                        UvNote.SetArg(1, "CURR BALANCE CHG FROM " + OldBal + " TO " + NewBal.ToString() + " (CltWebSite)");
                                        UvNote.Call();
                                        ErrMsg = UvNote.GetArg(2);
                                        if(ErrMsg != "0")
                                        {
                                            result.ExceptionStr = ErrMsg;
                                            result.IsSuccessful = false;
                                        }
                                    }
                                }
                                else
                                {
                                    result.ExceptionStr = "The balance for this creditor can not be updated online.";
                                    result.IsSuccessful = false;
                                }
                            }

                            CcfRec.Dispose();
                        }
                    }
                }
                catch(Exception ex)
                {
                    result.Exception = ex;
                    result.IsSuccessful = false;
                }
            }
            else
            {
                result.ExceptionStr = "The balance for this creditor can not be updated online.";
                result.IsSuccessful = false;
            }
            return result;
        }
    }
}