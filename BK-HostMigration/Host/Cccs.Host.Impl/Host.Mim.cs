﻿using System;
using IBMU2.UODOTNET;
using Cccs.Host.Dal;
using System.Collections.Generic;

namespace Cccs.Host.Impl
{
	public partial class Host : IHost
	{
		private string SelectClNo(string InClNo)
		{
			string OutClNo = InClNo;
			if(InClNo.IndexOf('~') > 0)
			{
				string[] TestClNo = InClNo.Split('~');
				OutClNo = TestClNo[0];
				long CurNo, MaxNo = 0;
				int MaxIndex = 0, i;
				for(i = 0; i < TestClNo.Length; i++)
				{
					if(TestClNo[i].StartsWith("W") && long.TryParse(TestClNo[i].Replace("W", string.Empty), out CurNo))
					{
						if(CurNo > MaxNo)
						{
							MaxNo = CurNo;
							MaxIndex = i;
						}
					}
				}
				OutClNo = TestClNo[MaxIndex];
			}
			return OutClNo;
		}

		public MimLoginClientResult Mim_LoginClient(string RegNum)
		{
			MimLoginClientResult result = new MimLoginClientResult();
			result.Escrow = "N";
			result.Escrow6 = "N";
			result.ReDirTarget = "introNew.aspx";
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniSubroutine UvClNo = UvSess.CreateUniSubroutine("GET.CLNO.SUB", 2))
					{
						UvClNo.SetArg(0, "WS" + RegNum);
						UvClNo.Call();
						string ClNo = SelectClNo(UvClNo.GetArg(1).Trim());
						if(ClNo != "")
						{
							using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
							{
								using(UniDynArray WsRec = WsCam.Read(ClNo))
								{
									result.ClId = ClNo;                                         // ID
									result.UvStat = WsRec.Extract(21).ToString().Trim();		// STATUS
									result.ContactSsn = WsRec.Extract(42).ToString().Trim();	// SSN
									result.FirmId = WsRec.Extract(210).ToString().Trim();		// FIRMCODE
                                    result.ZipCode = WsRec.Extract(130).ToString().Trim();      //ZIPCODE
									result.LoginMimOk = true;
									result.ReDirTarget = "AprWait.aspx";                                   
									if(String.IsNullOrEmpty(WsRec.Extract(120).ToString().Trim()) || String.IsNullOrEmpty(WsRec.Extract(121).ToString().Trim()))
										result.ReDirTarget = "ProvideYourPersonalInformation.aspx";
									using(UniFile CaseXRef = UvSess.CreateUniFile("WEB.CASEXREF"))
									{
										try
										{
											using(UniDynArray XRefRec = CaseXRef.Read(result.ContactSsn))
											{
												result.JointSsn = XRefRec.Extract(1).ToString();        // SSN
												result.SecondaryFlag = XRefRec.Extract(2).ToString();   // SECONDARY
											}
											using(UniSubroutine SpClNo = UvSess.CreateUniSubroutine("GET.SSN2CLN.SUB", 3))
											{
												SpClNo.SetArg(1, result.JointSsn);
												SpClNo.SetArg(2, "W");
												SpClNo.Call();
												result.SpUvId = SelectClNo(SpClNo.GetArg(0).Trim());
											}
										}
										catch
										{
											result.ReDirTarget = "GetJointSsn.aspx";
										}
									}
									using(UniFile AttnyFirm = UvSess.CreateUniFile("WEB_ATTY_FIRM"))
									{
										try
										{
											using(UniDynArray FirmRec = AttnyFirm.Read(result.FirmId))
											{
												switch(FirmRec.Extract(21).ToString().Trim())
												{
													case "1":
														result.Escrow = "Y";
														break;
													case "3":
														result.Escrow = "Y";
														break;
													case "6":
														result.Escrow = "Y";
														result.Escrow6 = "Y";
														break;
												}
											}
										}
										catch
										{
										}
									}
									result.IsSuccessful = true;
								}
							}
						}
						else // Record not found
						{
							result.ErrorMsg = "There was an error retrieving your data. Please try again."; // TODO: Don't return, or return TranslationKey instead.
							result.IsSuccessful = true;
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public string Mim_NewClsID_Get(string reg_num)
		{
			string ClNo = string.Empty;
			using(UniSession UvSess = OpenUniSession())
			{
				using(UniSubroutine UvSub = UvSess.CreateUniSubroutine("CRE8.CAM.SUB", 3))// the sec pram is the # of prams
				{
					try
					{
						//the # is a 0 base pram #
						UvSub.SetArg(1, "WS" + reg_num);// the regnum from MySql with a WS in front
						UvSub.SetArg(2, "W");// the cam file to be inserted into
						UvSub.Call();
						ClNo = UvSub.GetArg(0);//the out pram will have the clno as a string. if blank insert failed
					}
					catch
					{
					}
				}
			}
			return ClNo;
		}

		public string Mim_HostID_ReturningUser_Get(string reg_num)
		{
			string HostID = string.Empty;
			using(UniSession UvSess = OpenUniSession())
			{
				using(UniSubroutine UvClNo = UvSess.CreateUniSubroutine("GET.CLNO.SUB", 2))
				{

					UvClNo.SetArg(0, "WS" + reg_num);// this time the first is input regnum
					UvClNo.Call();
					HostID = SelectClNo(UvClNo.GetArg(1).Trim());// and the second is the output
				}
			}
			return HostID;
		}

		public MimCreateLoginValidateDataResult Mim_CreateLogin_ValidateData(string ssn)
		{
			MimCreateLoginValidateDataResult result = new MimCreateLoginValidateDataResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniSubroutine GetClNo = UvSess.CreateUniSubroutine("GET.SSN2CLN.SUB", 3))
					{
						GetClNo.SetArg(1, ssn.Trim());
						GetClNo.SetArg(2, "W");
						GetClNo.Call();
						string ClNo = SelectClNo(GetClNo.GetArg(0).Trim());
						using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
						{
							if(!string.IsNullOrEmpty(ClNo))
							{
								try
								{
									using(UniDynArray WsRec = WsCam.Read(ClNo))
									{
										result.IsSsnAlreadyOnFile = (WsRec.Extract(201).ToString().Trim() == "I");//SMETH
										if(!result.IsSsnAlreadyOnFile)
										{
											result.IsValid = true;
										}
									}
								}
								catch
								{
									result.IsValid = true;
								}
							}
							else
							{
								result.IsValid = true;
							}
						}
						if(!result.IsSsnAlreadyOnFile)
						{
							using(UniFile CaseXRef = UvSess.CreateUniFile("WEB.CASEXREF"))
							{
								try
								{
									using(UniDynArray XRefRec = CaseXRef.Read(ssn))
									{
										result.JointSsn = XRefRec.Extract(1).ToString();        // SSN
										result.SecondaryFlag = XRefRec.Extract(2).ToString();   // SECONDARY
										if(result.SecondaryFlag == "S")
										{
											GetClNo.SetArg(1, result.JointSsn);
											GetClNo.SetArg(2, "W");
											GetClNo.Call();
											result.SpUvId = SelectClNo(GetClNo.GetArg(0).Trim());
										}
										else
										{
											result.SpUvId = String.Empty;
										}
									}
								}
								catch
								{
								}
							}
						}
					}
				}
				result.IsSuccessful = true;
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public MimDisclosureLoadDataResult Mim_Disclosure_LoadData(string ClNo)
		{
			MimDisclosureLoadDataResult result = new MimDisclosureLoadDataResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					{
						using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
						{
							result.BankName = WsRec.Extract(231).ToString().Trim();
							result.NameOnCheck = WsRec.Extract(232).ToString().Trim();
							result.PaymentType = (WsRec.Extract(212).ToString().Trim() == "P") ? "C" : WsRec.Extract(212).ToString().Trim();
							result.AbaNumber = WsRec.Extract(220).ToString().Trim();
							result.AcctNumber = WsRec.Extract(219).ToString().Trim();
							result.DcFNameOncard = WsRec.Extract(240).ToString().Trim();
							result.DcMNameOnCard = WsRec.Extract(241).ToString().Trim();
							result.DcLNameOnCard = WsRec.Extract(242).ToString().Trim();
							result.DcBillAddr1 = WsRec.Extract(243).ToString().Trim();
							result.DcBillAddr2 = WsRec.Extract(244).ToString().Trim();
							result.DcBillCity = WsRec.Extract(245).ToString().Trim();
							result.DcBillState = WsRec.Extract(246).ToString().Trim();
							result.DcBillZip = WsRec.Extract(247).ToString().Trim();
							result.DcAcctNum = WsRec.Extract(248).ToString().Trim();
							result.CardExp = WsRec.Extract(249).ToString().Trim();
							result.Mtcn = WsRec.Extract(214).ToString().Trim();
							result.DataFromDb = true;
							result.IsSuccessful = true;
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result Mim_Disclosure_SaveTransaction(string ClNo, string dc_fnameoncard, string dc_mnameoncard
			, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state
			, string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no
			, string van_trans_ref, string char_amt)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
					{
						WsRec.Replace(212, "D");
						WsRec.Replace(213, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
						WsRec.Replace(240, dc_fnameoncard.ToUpper().Trim());
						WsRec.Replace(241, dc_mnameoncard.ToUpper().Trim());
						WsRec.Replace(242, dc_lnameoncard.ToUpper().Trim());
						WsRec.Replace(243, dc_bill_addr1.ToUpper().Trim());
						WsRec.Replace(245, dc_bill_city.ToUpper().Trim());
						WsRec.Replace(246, dc_bill_state.ToUpper().Trim());
						WsRec.Replace(247, dc_bill_zip.ToUpper().Trim());
						WsRec.Replace(248, dc_acctnum.Trim());
						WsRec.Replace(249, dc_exp_date.Trim());
						WsRec.Replace(276, cur_inv_no.Trim());
						WsRec.Replace(278, cur_seq_no.Trim());
                        WsRec.Replace(314, correctChargeAmount(char_amt,"D").Trim());

						WsCam.Write(ClNo.Trim(), WsRec);
					}

					using(UniFile CcPmtLog = UvSess.CreateUniFile("WEB.CCPMT.LOG"))
					{
						try
						{
							using(UniDynArray LogRec = CcPmtLog.Read(ClNo.Trim()))
							{
								LogRec.Replace(5, UvSess.Iconv(char_amt.Trim(), "MD2"));
								LogRec.Replace(1, String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim());
								LogRec.Replace(2, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
								CcPmtLog.Write(ClNo.Trim(), LogRec);
							}
						}
						catch // Record does not exist, add new
						{
							using(UniDynArray LogRec = new UniDynArray(UvSess))
							{
								//LogRec.Insert(0, ClNo);
								LogRec.Insert(1, String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim());
								LogRec.Insert(2, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
								LogRec.Insert(5, UvSess.Iconv(char_amt.Trim(), "MD2"));
								CcPmtLog.Write(ClNo.Trim(), LogRec);
							}
						}
					}
					int next_seqno = 0;
					using(UniFile CamControl = UvSess.CreateUniFile("WEB.CAM.CONTROL"))
					using(UniDynArray ConRec = CamControl.Read("WS.TRANS.SEQ"))
					{
						if(int.TryParse(ConRec.Extract(1).ToString(), out next_seqno))
						{
							next_seqno++;
							ConRec.Replace(1, next_seqno.ToString());
							CamControl.Write("WS.TRANS.SEQ", ConRec);
						}
					}

					using(UniFile WsTrans = UvSess.CreateUniFile("WEB.WSTRANS"))
					using(UniDynArray TranRec = new UniDynArray(UvSess))
					{
						string id = string.Format("I{0}", next_seqno);
						TranRec.Insert(1, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
						TranRec.Insert(2, "D");
						TranRec.Insert(3, char_amt.Trim());
						TranRec.Insert(4, ClNo.Trim());
						TranRec.Insert(10, String.IsNullOrEmpty(van_trans_ref) ? String.Empty : van_trans_ref.Trim());
						TranRec.Insert(11, "PMT");
						TranRec.Insert(12, cur_inv_no.Trim());

						WsTrans.Write(id, TranRec);
						result.IsSuccessful = true;
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public MimForm7LoadDataResult Mim_Form7_LoadData(string ClNo, string regnum)
		{
			MimForm7LoadDataResult result = new MimForm7LoadDataResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					if(WsRec.Extract(146).ToString().Trim() == "WS" + regnum)
					{
						result.contact_firstname = WsRec.Extract(121).ToString().Trim();
						result.contact_initial = WsRec.Extract(122).ToString().Trim();
						result.contact_lastname = WsRec.Extract(120).ToString().Trim();
						result.contact_address = WsRec.Extract(2).ToString().Trim();
						result.contact_address2 = WsRec.Extract(3).ToString().Trim();
						result.contact_city = WsRec.Extract(128).ToString().Trim();
						result.contact_state = WsRec.Extract(129).ToString().Trim();
						result.contact_zip = WsRec.Extract(130).ToString().Trim();
						result.contact_email = WsRec.Extract(144).ToString().Trim();
						result.casenumber = WsRec.Extract(211).ToString().Trim();
						if(result.casenumber.Length > 3)
							result.casenumber = String.Format("{0}-{1}", result.casenumber.Substring(0, 2), result.casenumber.Substring(2));
						result.dayphone = WsRec.Extract(7).ToString().Trim();
						result.nitephone = WsRec.Extract(8).ToString().Trim();
						result.attyname = WsRec.Extract(233).ToString().Trim();
						result.attyemail = WsRec.Extract(254).ToString().Trim();
						result.firmcode = WsRec.Extract(210).ToString().Trim();
						result.bankruptcy_type = WsRec.Extract(233).ToString().Trim();
						try
						{
							using(UniFile WsDemog = UvSess.CreateUniFile("WEB.WSDEMOG"))
							{
								using(UniDynArray DmRec = WsDemog.Read(ClNo.Trim()))
								{
									result.contact_marital = DmRec.Extract(1).ToString().Trim();
									result.contact_sex = DmRec.Extract(18).ToString().Trim();
									result.contact_race = DmRec.Extract(22).ToString().Trim();
									result.contact_hispanic = DmRec.Extract(98).ToString().Trim();
									result.numberinhouse = DmRec.Extract(114).ToString().Trim();
									result.income_range = DmRec.Extract(115).ToString().Trim();
								}
							}
						}
						catch
						{
							result.contact_marital = string.Empty;
							result.contact_sex = string.Empty;
							result.contact_race = string.Empty;
							result.contact_hispanic = string.Empty;
							result.numberinhouse = "0";
							result.income_range = string.Empty;
						}
						result.IsSuccessful = true;
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result Mim_Form7_SaveData(string ClNo, string contact_lastname, string contact_firstname,
			string contact_initial, string contact_ssn, string contact_address, string contact_address2,
			string contact_city, string contact_state, string contact_zip, string contact_marital,
			string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity,
			string cemail, string dayphone, string nitephone, string attyname, string attyemail,
			string firm_id, string escrow, string secondary_flag, string char_type, string casenumber,
            string bankruptcy_type, string income_range, string number_in_house, string languageCode, string char_amt)
		{
			Result result = new Result();
			firm_id = string.IsNullOrEmpty(firm_id) ? "9999" : firm_id.Trim();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile web_cred_rpt_hdr = UvSess.CreateUniFile("WEB.CRED.RPT.HDR"))
					{
						try
						{
							using(UniDynArray web_cred_rpt_hdr_rec = web_cred_rpt_hdr.Read(ClNo.Trim()))
							{
							}
						}
						catch // Insert New
						{
							using(UniFile CredRptQue = UvSess.CreateUniFile("CREDRPT_QUE"))
							{
								try
								{
									using(UniDynArray CredRptRec = CredRptQue.Read(ClNo.Trim()))
									{
										CredRptRec.Replace(1, contact_firstname.Trim());
										CredRptRec.Replace(2, contact_lastname.Trim());
										CredRptRec.Replace(3, contact_ssn.Trim());
										CredRptRec.Replace(5, contact_address.Trim());
										CredRptRec.Replace(8, contact_city.Trim());
										CredRptRec.Replace(9, contact_state.Trim());
										CredRptRec.Replace(10, contact_zip.Trim());
										CredRptRec.Replace(11, credrpt_birthcity.Trim());

										CredRptQue.Write(ClNo.Trim(), CredRptRec);
									}
								}
								catch(UniFileException) // Insert New
								{
									using(UniDynArray CredRptRec = new UniDynArray(UvSess))
									{
										//CredRptRec.Insert(0, ClNo.Trim());
										CredRptRec.Insert(1, contact_firstname.Trim());
										CredRptRec.Insert(2, contact_lastname.Trim());
										CredRptRec.Insert(3, contact_ssn.Trim());
										CredRptRec.Insert(5, contact_address.Trim());
										CredRptRec.Insert(8, contact_city.Trim());
										CredRptRec.Insert(9, contact_state.Trim());
										CredRptRec.Insert(10, contact_zip.Trim());
										CredRptRec.Insert(11, credrpt_birthcity.Trim());

										CredRptQue.Write(ClNo.Trim(), CredRptRec);
									}
								}
							}
						}
					}

					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					{
						using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
						{
							WsRec.Replace(1, string.Format("{0} {1} {2}", contact_firstname.Trim(), contact_initial.Trim(), contact_lastname.Trim()).ToUpper()); //	FULLNAME
							WsRec.Replace(2, contact_address.Trim().ToUpper()); //	ADDR
							WsRec.Replace(3, contact_address2.Trim().ToUpper()); //	ADDR2
							WsRec.Replace(4, string.Format("{0} {1} {2}", contact_city.Trim(), contact_state.Trim(), contact_zip.Trim()).ToUpper()); //	CSZ
							WsRec.Replace(6, string.Format("{0} {1}", contact_lastname.Trim(), contact_firstname.Trim()).ToUpper()); //	LFNAME
							WsRec.Replace(7, dayphone.Trim()); //	DAYPHONE
							WsRec.Replace(8, nitephone.Trim()); //	NITEPHONE
							//web_wscam_rec.Replace(13, DateTime.Now.ToShortDateString()); //	APPT_DATE
							WsRec.Replace(13, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D")); //	APPT_DATE
							WsRec.Replace(15, "W"); //	APPT.TYPE
							WsRec.Replace(21, "PE"); //	STATUS
							WsRec.Replace(27, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D")); //	MOD.DATE
                            WsRec.Replace(42, contact_ssn);
							WsRec.Replace(120, contact_lastname.Trim().ToUpper()); //	LNAME
							WsRec.Replace(121, contact_firstname.Trim().ToUpper()); //	FNAME
							WsRec.Replace(122, contact_initial.Trim().ToUpper()); //	MNAME
							WsRec.Replace(128, contact_city.Trim().ToUpper()); //	CITY
							WsRec.Replace(129, contact_state.Trim().ToUpper()); //	STATE
							WsRec.Replace(130, contact_zip.Trim()); //	ZIP
							WsRec.Replace(134, contact_zip.Trim()); //	ZIP.134
							WsRec.Replace(144, cemail.Trim()); //	EMAIL
							WsRec.Replace(201, "I"); //	SMETH
							WsRec.Replace(210, firm_id); //	FIRMCODE
							WsRec.Replace(211, casenumber.Replace("-", "").Trim()); //	CASENUMBER
							if((escrow == "Y") && (secondary_flag != "S") && (char_type == "H"))
							{
								WsRec.Replace(212, "A"); //	PAYTYPE
                                WsRec.Replace(314, correctChargeAmount(char_amt, "A").Trim());
							}
							WsRec.Replace(228, "B"); //	HTYPE
							WsRec.Replace(233, bankruptcy_type.Trim()); //	BANKRUPTCY_TYPE
							if(firm_id == "9999")
							{
								WsRec.Replace(223, attyname.Trim());					//	ATTYNAME
								WsRec.Replace(254, attyemail.Trim());					//	NON_ESCROW_EMAIL
							}
							else
							{
								WsRec.Replace(223, String.Empty);						//	ATTYNAME
								WsRec.Replace(254, String.Empty);						//	NON_ESCROW_EMAIL
							}
							WsCam.Write(ClNo.Trim(), WsRec);
						}
					}

					using(UniFile WsDemog = UvSess.CreateUniFile("WEB.WSDEMOG"))
					{
						try
						{
							using(UniDynArray DemogRec = WsDemog.Read(ClNo.Trim()))
							{
								DemogRec.Replace(17, contact_marital.Trim());	// MARITAL
								DemogRec.Replace(18, contact_sex.Trim());	// SEX
								DemogRec.Replace(22, contact_race.Trim());	// RACE
								DemogRec.Replace(97, languageCode);	// LANGUAGE
								DemogRec.Replace(98, contact_hispanic.Trim());	// HISP
								DemogRec.Replace(114, number_in_house.Trim());	// NUMINHOUSE
								DemogRec.Replace(115, income_range.Trim());	// INCOME_RANGE

								WsDemog.Write(ClNo.Trim(), DemogRec);
							}
						}
						catch	// Insert New
						{
							using(UniDynArray DemogRec = new UniDynArray(UvSess))
							{
								//DemogRec.Insert(0, ClNo.Trim()); // ID
								DemogRec.Insert(17, contact_marital.Trim());	// MARITAL
								DemogRec.Insert(18, contact_sex.Trim());	// SEX
								DemogRec.Insert(22, contact_race.Trim());	// RACE
								DemogRec.Insert(97, languageCode);	// LANGUAGE
								DemogRec.Insert(98, contact_hispanic.Trim());	// HISP
								DemogRec.Insert(114, number_in_house.Trim());	// NUMINHOUSE
								DemogRec.Insert(115, income_range.Trim());	// INCOME_RANGE

								WsDemog.Write(ClNo.Trim(), DemogRec);
							}
						}
					}

					using(UniFile SsnXRef = UvSess.CreateUniFile("WEB.APPT.SSN.XREF"))
					{
						try
						{
							using(UniDynArray SsnXRefRec = SsnXRef.Read(contact_ssn.Trim()))
							{
							}
						}
						catch // Insert New
						{
							using(UniDynArray SsnXRefRec = new UniDynArray(UvSess))
							{
								//SsnXRefRec.Insert(0, contact_ssn.Trim());
								SsnXRefRec.Insert(1, ClNo.Trim());
								SsnXRef.Write(contact_ssn.Trim(), SsnXRefRec);
							}
						}
					}
					if(!string.IsNullOrEmpty(cemail.Trim()))
					{
						using(UniFile eMailXRef = UvSess.CreateUniFile("WEB.APPT.EMAIL.XREF"))
						{
							try
							{
								using(UniDynArray eMailXRefRec = eMailXRef.Read(cemail.Trim()))
								{
									eMailXRefRec.Replace(1, ClNo.Trim());
									eMailXRef.Write(cemail.Trim(), eMailXRefRec);
								}
							}
							catch
							{
								using(UniDynArray eMailXRefRec = new UniDynArray(UvSess))
								{
									//eMailXRefRec.Insert(0, cemail.Trim());
									eMailXRefRec.Insert(1, ClNo.Trim());
									eMailXRef.Write(cemail.Trim(), eMailXRefRec);
								}
							}
						}
					}
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result Mim_Form7_SaveWriteOff(string ClNo, string regnum, string char_amt)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					string CurInvNo = string.Empty;
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
					{
						string LastTrans = WsRec.Extract(276).ToString();
						int CurSeqNo = 1;

						if(int.TryParse(WsRec.Extract(278).ToString(), out CurSeqNo))
						{
							CurSeqNo++;
						}
						CurInvNo = string.Format("WS{0}-{1}", regnum, CurSeqNo);

						WsRec.Replace(213, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
						WsRec.Replace(212, "N9");
						WsRec.Replace(276, CurInvNo);
						WsRec.Replace(278, CurSeqNo.ToString());
                        WsRec.Replace(314, correctChargeAmount(char_amt,"N9").Trim());

						WsCam.Write(ClNo.Trim(), WsRec);
					}
					int TranId = 1;
					using(UniFile CamControl = UvSess.CreateUniFile("WEB.CAM.CONTROL"))
					using(UniDynArray ContRec = CamControl.Read("WS.TRANS.SEQ"))
					{
						if(int.TryParse(ContRec.Extract(1).ToString(), out TranId))
						{
							TranId++;
						}
						ContRec.Replace(1, TranId.ToString());
						CamControl.Write("WS.TRANS.SEQ", ContRec);
					}
					using(UniFile WsTrans = UvSess.CreateUniFile("WEB.WSTRANS"))
					using(UniDynArray TranRec = new UniDynArray(UvSess))
					{
						string id = string.Format("I{0}", TranId);
						//TranRec.Insert(0, id);
						TranRec.Insert(1, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));
						TranRec.Insert(2, "N9");
						TranRec.Insert(3, char_amt.Trim());
						TranRec.Insert(4, ClNo.Trim());
						TranRec.Insert(11, "WOF");
						TranRec.Insert(12, CurInvNo);
						WsTrans.Write(id, TranRec);
					}
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public MimForm7MainLineResult Mim_Form7_MainLine(string contact_state)
		{
			MimForm7MainLineResult result = new MimForm7MainLineResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile StateFees = UvSess.CreateUniFile("WEB.STATE.FEES"))
				{
					try
					{
						using(UniDynArray rec = StateFees.Read(contact_state.Trim()))
						{
							result.CharType = rec.Extract(36).ToString();
							result.CharAmt = rec.Extract(39).ToString();

							if(string.IsNullOrEmpty(result.CharType))
							{
								result.CharType = "H";
							}

							if(string.IsNullOrEmpty(result.CharAmt))
							{
								result.CharAmt = "50.00";
							}
						}
					}
					catch
					{
						result.CharType = "H";
						result.CharAmt = "50.00";
					}
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}


        public MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum)
        {
            MimSetTransSaveWriteResult result = new MimSetTransSaveWriteResult();
            try
            {
                using (UniSession UvSess = OpenUniSession())
                using (UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
                using (UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
                {
                    result.State = WsRec.Extract(129).ToString();
                    result.LastTrans = WsRec.Extract(276).ToString();

                    int cur_seq_no = 0;
                    int.TryParse(WsRec.Extract(278).ToString(), out cur_seq_no);
                    result.CurSeqNo = (cur_seq_no + 1).ToString();

                    string UvDts = UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D");

                    using (UniFile StateFees = UvSess.CreateUniFile("WEB.STATE.FEES"))
                    {
                        try
                        {
                            using (UniDynArray FeeRec = StateFees.Read(result.State))
                            {
                                result.CharAmt = FeeRec.Extract(39).ToString();
                            }
                        }
                        catch
                        {
                            result.CharAmt = "50.00";
                        }
                    }

                    result.CurInvNo = string.Format("WS{0}-{1}", regnum.Trim(), result.CurSeqNo);
                    WsRec.Replace(213, UvDts);
                    WsRec.Replace(212, "N3");
                    WsRec.Replace(276, result.CurInvNo);
                    WsRec.Replace(278, result.CurSeqNo);


                    WsCam.Write(ClNo.Trim(), WsRec);
                    using (UniFile CamControl = UvSess.CreateUniFile("WEB.CAM.CONTROL"))
                    using (UniDynArray ConRec = CamControl.Read("WS.TRANS.SEQ"))
                    {
                        int tranid = 0;
                        int.TryParse(ConRec.Extract(1).ToString(), out tranid);
                        result.TranId = (tranid + 1).ToString();
                        ConRec.Replace(1, result.TranId);
                        CamControl.Write("WS.TRANS.SEQ", ConRec);

                        using (UniFile WsTrans = UvSess.CreateUniFile("WEB.WSTRANS"))
                        using (UniDynArray TranRec = new UniDynArray(UvSess))
                        {
                            string id = string.Format("I{0}", result.TranId);
                            TranRec.Insert(0, id);
                            TranRec.Insert(1, UvDts);
                            TranRec.Insert(2, "N3");
                            TranRec.Insert(3, result.CharAmt);
                            TranRec.Insert(4, ClNo.ToString());
                            TranRec.Insert(11, "WOF");
                            TranRec.Insert(12, result.CurInvNo);

                            WsTrans.Write(id, TranRec);
                            result.IsSuccessful = true;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                result.Exception = exception;
                result.IsSuccessful = false;
            }
            return result;
        }

		public MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum, string char_amt)
		{
			MimSetTransSaveWriteResult result = new MimSetTransSaveWriteResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					result.State = WsRec.Extract(129).ToString();
					result.LastTrans = WsRec.Extract(276).ToString();

					int cur_seq_no = 0;
					int.TryParse(WsRec.Extract(278).ToString(), out cur_seq_no);
					result.CurSeqNo = (cur_seq_no + 1).ToString();

					string UvDts = UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D");
					result.CurInvNo = string.Format("WS{0}-{1}", regnum.Trim(), result.CurSeqNo);
					WsRec.Replace(213, UvDts);
					WsRec.Replace(212, "N3");
					WsRec.Replace(276, result.CurInvNo);
					WsRec.Replace(278, result.CurSeqNo);
                    WsRec.Replace(314, correctChargeAmount(char_amt,"N3").Trim());

					WsCam.Write(ClNo.Trim(), WsRec);
					using(UniFile CamControl = UvSess.CreateUniFile("WEB.CAM.CONTROL"))
					using(UniDynArray ConRec = CamControl.Read("WS.TRANS.SEQ"))
					{
						int tranid = 0;
						int.TryParse(ConRec.Extract(1).ToString(), out tranid);
						result.TranId = (tranid + 1).ToString();
						ConRec.Replace(1, result.TranId);
						CamControl.Write("WS.TRANS.SEQ", ConRec);

						using(UniFile WsTrans = UvSess.CreateUniFile("WEB.WSTRANS"))
						using(UniDynArray TranRec = new UniDynArray(UvSess))
						{
							string id = string.Format("I{0}", result.TranId);
							TranRec.Insert(0, id);
							TranRec.Insert(1, UvDts);
							TranRec.Insert(2, "N3");
							TranRec.Insert(3, result.CharAmt);
							TranRec.Insert(4, ClNo.ToString());
							TranRec.Insert(11, "WOF");
							TranRec.Insert(12, result.CurInvNo);

							WsTrans.Write(id, TranRec);
							result.IsSuccessful = true;
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result MimCompleteCourse(string ClNo, string prescore, string postscore, string TestDTS)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					WsRec.Replace(229, prescore.Trim());  // PRESCORE
					WsRec.Replace(230, postscore.Trim()); // POSTSCORE
					WsRec.Replace(239, UvSess.Iconv(TestDTS.Split(' ')[0], "D"));//test_date.Trim());	// TEST_DATE
					WsRec.Replace(256, TestDTS);//test_dts.Trim());  // CONF_DATETIME
					WsRec.Replace(21, "CO");							// STATUS

					WsCam.Write(ClNo.Trim(), WsRec);
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public MimQuicCalcLoadDataResult Mim_QuicCalc_LoadData(string clid, string regnum)
		{
			MimQuicCalcLoadDataResult result = new MimQuicCalcLoadDataResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					using(UniDynArray WsRec = WsCam.Read(clid.Trim()))
					{
						result.ContactState = WsRec.Extract(129).ToString();     // STATE
					}
					try
					{
						using(UniFile WsDemog = UvSess.CreateUniFile("WEB.WSDEMOG"))
						using(UniDynArray DemogRec = WsDemog.Read(clid.Trim()))
						{
							result.SizeOfHousehold = DemogRec.Extract(114).ToString();  // NUMINHOUSE
							result.MonthlyGrossIncome = DemogRec.Extract(1).ToString(); // GROSS

							result.IsSuccessful = true;
						}
					}
					catch
					{
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public Result Mim_QuicCalc_SaveData(string ClNo, string contact_state, string monthly_gross_income, string size_of_household)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
					{
						WsRec.Replace(129, contact_state.Trim());
					}
					using(UniFile WsDemog = UvSess.CreateUniFile("WEB.WSDEMOG"))
					{
						try
						{
							using(UniDynArray DemogRec = WsDemog.Read(ClNo.Trim()))
							{
								DemogRec.Replace(1, monthly_gross_income);
								DemogRec.Replace(114, size_of_household);

								WsDemog.Write(ClNo.Trim(), DemogRec);
								result.IsSuccessful = true;
							}
						}
						catch	// Insert new
						{
							using(UniDynArray DemogRec = new UniDynArray(UvSess))
							{
								DemogRec.Insert(0, ClNo.Trim());
								DemogRec.Insert(1, monthly_gross_income);
								DemogRec.Insert(114, size_of_household);

								WsDemog.Write(ClNo.Trim(), DemogRec);
								result.IsSuccessful = true;
							}
						}
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result Mim_WaiverWaiver_SaveUv(string ClNo)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile FeeWaiver = UvSess.CreateUniFile("WEB.FEEWAIVER"))
				{
					string UvDts = UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D");
					try
					{
						using(UniDynArray FwRec = FeeWaiver.Read(ClNo.Trim()))
						{
							FwRec.Replace(2, "D");			// OPT.STAT
							FwRec.Replace(3, UvDts);		// OPT.DATE
							FeeWaiver.Write(ClNo.Trim(), FwRec);
						}
					}
					catch // Insert new
					{
						using(UniDynArray FwRec = new UniDynArray(UvSess))
						{
							//web_feewaiver_rec.Insert(0, clid.Trim());
							FwRec.Insert(2, "D");
							FwRec.Insert(3, UvDts);
							FeeWaiver.Write(ClNo.Trim(), FwRec);
						}
					}
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = true;
			}
			return result;
		}

		public MimWaverAppLoadDataResult Mim_WaverApp_LoadData(string ClNo, string regnum)
		{
			MimWaverAppLoadDataResult result = new MimWaverAppLoadDataResult();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					result.ContactFirstName = WsRec.Extract(121).ToString().Trim(); // FNAME
					result.ContactLastName = WsRec.Extract(120).ToString().Trim();  // LNAME
					result.ContactAddress = WsRec.Extract(2).ToString().Trim();     // ADDR
					result.ContactAddress2 = WsRec.Extract(3).ToString().Trim();    // ADDR2
					result.ContactCity = WsRec.Extract(128).ToString().Trim();      // CITY
					result.ContactState = WsRec.Extract(129).ToString().Trim();     // STATE
					result.ContactZip = WsRec.Extract(130).ToString().Trim();       // ZIP
					result.ContactTelephone = WsRec.Extract(7).ToString().Trim();   // DAYPHONE
					result.RealEmail = WsRec.Extract(144).ToString().Trim();        // EMAIL

					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result Mim_WaverApp_SaveData(string ClNo, string contact_firstname, string contact_lastname, string contact_initial, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string real_email, string contact_telephone)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
					using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
					{
						WsRec.Replace(121, contact_firstname.Trim().ToUpper());
						WsRec.Replace(120, contact_lastname.Trim().ToUpper());
						WsRec.Replace(1, string.Format("{0} {1} {2}", contact_firstname.Trim(), contact_initial.Trim(), contact_lastname.Trim()).ToUpper());
						WsRec.Replace(6, string.Format("{0} {1}", contact_lastname.Trim(), contact_firstname.Trim()).ToUpper());
						WsRec.Replace(2, contact_address.Trim().ToUpper());
						WsRec.Replace(3, contact_address2.Trim().ToUpper());
						WsRec.Replace(128, contact_city.Trim().ToUpper());
						WsRec.Replace(129, contact_state.Trim().ToUpper());
						WsRec.Replace(130, contact_zip.Trim().ToUpper());
						WsRec.Replace(134, contact_zip.Trim().ToUpper());
						WsRec.Replace(4, string.Format("{0} {1} {2}", contact_city, contact_state, contact_zip));
						WsRec.Replace(144, real_email.Trim());
						WsRec.Replace(7, contact_telephone.Trim());

						WsCam.Write(ClNo.Trim(), WsRec);
					}
					using(UniFile FeeWaiver = UvSess.CreateUniFile("WEB.FEEWAIVER"))
					{
						try
						{
							using(UniDynArray FwRec = FeeWaiver.Read(ClNo.Trim()))
							{
								FwRec.Replace(2, "A");
								FwRec.Replace(3, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));

								FeeWaiver.Write(ClNo.Trim(), FwRec);
							}
						}
						catch // Insert New
						{
							using(UniDynArray FwRec = new UniDynArray(UvSess))
							{
								FwRec.Insert(2, "A");
								FwRec.Insert(3, UvSess.Iconv(DateTime.Today.ToString().Split(' ')[0], "D"));

								FeeWaiver.Write(ClNo.Trim(), FwRec);
							}
						}

						result.IsSuccessful = true;
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public Result MimSaveRegisterAttorney(string ClNo, string CaseNo, string Ssn, string FirmId
			, string AttnyName, string AttnyEmail, string UserName)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					WsRec.Replace(42, Ssn.Replace("-", String.Empty).Trim());		//	SSN
					WsRec.Replace(210, FirmId.Trim());								//	FIRMCODE
					WsRec.Replace(211, CaseNo.Replace("-", String.Empty).Trim());	//	CASENUMBER
					WsRec.Replace(252, UserName.Trim());							//  WEB_LOGIN
					if(FirmId == "9999")
					{
						WsRec.Replace(223, AttnyName.Trim());						//	ATTYNAME
						WsRec.Replace(254, AttnyEmail.Trim());						//	NON_ESCROW_EMAIL
					}
					else
					{
						WsRec.Replace(223, String.Empty);						//	ATTYNAME
						WsRec.Replace(254, String.Empty);						//	NON_ESCROW_EMAIL
					}


					WsCam.Write(ClNo.Trim(), WsRec);
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

		public int GetMimSequenceNumber(string ClNo)
		{
			int Back = 0;
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					int.TryParse(WsRec.Extract(278).ToString(), out Back);
					Back++;
				}
			}
			catch
			{
				Back = 1;
			}
			return Back;
		}

		public Result MimSavePreTest(string ClNo, int PreTest)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					WsRec.Replace(229, PreTest.ToString());                  //	PRESCORE

					WsCam.Write(ClNo.Trim(), WsRec);
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

        public Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number)
        {
            Result result = new Result();
            try
            {
                using (UniSession UvSess = OpenUniSession())
                using (UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
                using (UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
                {
                    WsRec.Replace(231, bank_name.Trim());
                    WsRec.Replace(232, name_on_check.Trim());
                    WsRec.Replace(220, aba_number.Trim());
                    WsRec.Replace(219, acct_number.Trim());
                    WsRec.Replace(212, "P");

                    WsCam.Write(ClNo.Trim(), WsRec);
                    result.IsSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                result.Exception = exception;
                result.IsSuccessful = false;
            }

            return result;
        }

		public Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number, string char_amt)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					WsRec.Replace(231, bank_name.Trim());
					WsRec.Replace(232, name_on_check.Trim());
					WsRec.Replace(220, aba_number.Trim());
					WsRec.Replace(219, acct_number.Trim());
					WsRec.Replace(212, "P");
                    WsRec.Replace(314, correctChargeAmount(char_amt,"P").Trim());
					WsCam.Write(ClNo.Trim(), WsRec);
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

        public Result MimDisclosureSaveMtcn(string ClNo, string mtcn)
        {
            Result result = new Result();
            try
            {
                using (UniSession UvSess = OpenUniSession())
                using (UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
                using (UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
                {
                    WsRec.Replace(212, "W");
                    WsRec.Replace(214, mtcn.Trim());

                    WsCam.Write(ClNo.Trim(), WsRec);
                    result.IsSuccessful = true;
                }
            }
            catch (Exception exception)
            {
                result.Exception = exception;
                result.IsSuccessful = false;
            }
            return result;
        }

		public Result MimDisclosureSaveMtcn(string ClNo, string mtcn, string char_amt)
		{
			Result result = new Result();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				using(UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
				using(UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
				{
					WsRec.Replace(212, "W");
					WsRec.Replace(214, mtcn.Trim());
                    WsRec.Replace(314, correctChargeAmount(char_amt,"W").Trim());
					WsCam.Write(ClNo.Trim(), WsRec);
					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}
			return result;
		}

        /// <summary>
        /// Gets client data from HOST and then does a lookup to see how much to charge the customer
        /// </summary>
        /// <remarks>I may want/need to hook in here to actually retrieve the </remarks>
        /// <param name="ClNo"></param>
        /// <returns></returns>
//		public double MimDisclosureGetChargeAmount(string ClNo)
        public double MimDisclosureGetChargeAmount(string ZipCode, string FirmCode)
		{
            return this.GetCharAmt(FirmCode, ZipCode, "W").Value;
		}

        public double MimDisclosureGetChargeAmount(string ClNo) {
            string Back = "50.00";
            try
            {
                using (UniSession UvSess = OpenUniSession())
                using (UniFile WsCam = UvSess.CreateUniFile("WEB.WSCAM"))
                using (UniDynArray WsRec = WsCam.Read(ClNo.Trim()))
                using (UniFile StateFees = UvSess.CreateUniFile("WEB.STATE.FEES"))
                using (UniDynArray SfRec = StateFees.Read(WsRec.Extract(129).ToString().Trim()))
                {
                    Back = UvSess.Oconv(SfRec.Extract(39).ToString().Trim(), "MD2");
                }
            }
            catch
            {
                Back = "50.00";
            }
            return Convert.ToDouble(Back);
        }

		public Result MimSaveXRef(string PriSsn, string SecSsn)
		{
			var primarySSN = PriSsn.Trim().Replace("-", String.Empty);
			var secondarySSN = SecSsn.Trim().Replace("-", String.Empty);
			Result result = new Result();
			using(UniSession UvSess = OpenUniSession())
			using(UniFile CaseXRef = UvSess.CreateUniFile("WEB.CASEXREF"))
			{
				UniDynArray xRec;
                try
                {
                    xRec = CaseXRef.Read(primarySSN);
                }
                catch
                {
                    xRec = new UniDynArray(UvSess);
                    xRec.Insert(1, secondarySSN);
                    xRec.Insert(2, string.Empty);
                    CaseXRef.Write(primarySSN, xRec);
                }

                xRec.Dispose();                
				xRec = null;
				if(!string.IsNullOrEmpty(secondarySSN))
				{
					try
					{
						xRec = CaseXRef.Read(secondarySSN);
					}
					catch
					{
						xRec = new UniDynArray(UvSess);
						xRec.Insert(1, primarySSN);
						xRec.Insert(2, "S");
						CaseXRef.Write(secondarySSN, xRec);
					}
				}
				result.IsSuccessful = true;
			}
			return result;
		}

		public string MimSaveJointXRef(string PriSsn, string SecSsn)
		{
			string Back = string.Empty;
			UniDynArray PriRec = null, SecRec = null;
			using(UniSession UvSess = OpenUniSession())
			using(UniFile CaseXRef = UvSess.CreateUniFile("WEB.CASEXREF"))
			{
				try
				{
					PriRec = CaseXRef.Read(PriSsn);
				}
				catch
				{
					PriRec = null;
				}
				try
				{
					SecRec = CaseXRef.Read(SecSsn);
				}
				catch
				{
					SecRec = null;
				}

				if(SecRec == null)
				{
					if(PriRec == null)
					{
						PriRec = new UniDynArray(UvSess);
						PriRec.Insert(1, SecSsn.Trim());
						PriRec.Insert(2, string.Empty);
						CaseXRef.Write(PriSsn.Trim(), PriRec);

						SecRec = new UniDynArray(UvSess);
						SecRec.Insert(1, PriSsn.Trim());
						SecRec.Insert(2, "S");
						CaseXRef.Write(SecSsn.Trim(), SecRec);
					}
					else
					{
						PriRec.Replace(1, SecSsn.Trim());
						PriRec.Replace(2, string.Empty);
						CaseXRef.Write(PriSsn.Trim(), PriRec);

						SecRec = new UniDynArray(UvSess);
						SecRec.Insert(1, PriSsn.Trim());
						SecRec.Insert(2, "S");
						CaseXRef.Write(SecSsn.Trim(), SecRec);
					}
				}
				else
				{
					if(PriRec == null)
					{
						if(string.IsNullOrEmpty(SecRec.Extract(1).ToString().Trim()))
						{
							SecRec.Replace(1, PriSsn);
							CaseXRef.Write(SecSsn.Trim(), SecRec);

							PriRec = new UniDynArray(UvSess);
							PriRec.Insert(1, SecSsn.Trim());
							PriRec.Insert(2, "S");
							CaseXRef.Write(PriSsn.Trim(), PriRec);
							Back = "S";
						}
						else
						{
							PriRec = new UniDynArray(UvSess);
							PriRec.Insert(1, string.Empty);
							PriRec.Insert(2, string.Empty);
							CaseXRef.Write(PriSsn.Trim(), PriRec);
						}
					}
					else
					{// still needs work?
						Back = PriRec.Extract(2).ToString().Trim();
						//if(string.IsNullOrEmpty(SecRec.Extract(1).ToString().Trim()))
						//{
						//    SecRec.Replace(1, PriSsn);
						//    CaseXRef.Write(SecSsn.Trim(), SecRec);

						//    PriRec.Replace(1, SecSsn.Trim());
						//    PriRec.Replace(2, "S");
						//    CaseXRef.Write(PriSsn.Trim(), PriRec);
						//    Back = "S";
						//}
						//else
						//{
						//    PriRec = new UniDynArray(UvSess);
						//    PriRec.Insert(1, string.Empty);
						//    PriRec.Insert(2, string.Empty);
						//    CaseXRef.Write(PriSsn.Trim(), PriRec);
						//}
					}
				}
			}
			return Back;
		}

        private String correctChargeAmount(String original,String code) {
            var toReturn = original;

            if ((new List<String> { "D", "W", "P", "A", "N3", "N9" }).Contains(code)) {
                toReturn = pad(original);
            }

            return toReturn;
        }

        private String pad(String original) {
            var toReturn = original;

            if (original.Length == 2) {
                return original + "00";
            }

            return toReturn;
        }
    }
}