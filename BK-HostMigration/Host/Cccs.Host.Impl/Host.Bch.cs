﻿using System;
using IBMU2.UODOTNET;

namespace Cccs.Host.Impl
{
	public partial class Host : IHost
	{
		public Result Bch_HomeSaver_SaveData(string need_help, string help_type, string contact_time, string loan_number, string first_name, string last_name, string address_line1, string address_line2, string city, string state, string zip, string phone, string email)
		{
			Result result = new Result();

			try
			{
				string act_code = string.Empty;
				string status = string.Empty;
				string contact_method = string.Empty;
				string que = string.Empty;

				if(string.Compare(need_help.Trim(), "Good", true) == 0)
				{
					que = "I";
					act_code = "NI";
					status = "CT";
					contact_method = "Good";
				}
				else if(string.Compare(help_type.Trim(), "HelpPhone", true) == 0)
				{
					que = "C";
					act_code = "CL";
					status = "IC";
					contact_method = "Phone";
				}

				using(UniSession uni_session = OpenUniSession())
				{
					string uv_dts = string.Empty;
					using(UniCommand cmd = uni_session.CreateUniCommand())
					{
						cmd.Command = "RUN WEB.BP GET.UV.DATE";
						cmd.Execute();
						uv_dts = cmd.Response.Trim();
					}

					using(UniCommand cmd = uni_session.CreateUniCommand())
					{
						cmd.Command = string.Format("SELECT WEB.OCS WITH LOAN.NO = {0}", loan_number.Trim());
						cmd.Execute();

						if(!string.IsNullOrEmpty(cmd.Response))
						{
							using(UniSelectList uni_select_list = uni_session.CreateUniSelectList(0))
							{
								string id = uni_select_list.Next();

								using(UniFile uni_file = uni_session.CreateUniFile("WEB.OCS"))
								{
									using(UniDynArray uni_dyn_array = uni_file.Read(id))
									{
										string contact_attempts = uni_dyn_array.Extract(23).ToString();

										int num_contact_attempts = 1;
										if(int.TryParse(contact_attempts, out num_contact_attempts))
										{
											num_contact_attempts++;
										}

										uni_dyn_array.Replace(5, first_name.Trim());
										uni_dyn_array.Replace(4, last_name.Trim());
										uni_dyn_array.Replace(10, string.Format("{0} {1}", address_line1.Trim(), address_line2.Trim()));
										uni_dyn_array.Replace(11, city.Trim());
										uni_dyn_array.Replace(12, state.Trim());
										uni_dyn_array.Replace(13, zip.Trim());
										uni_dyn_array.Replace(14, phone.Trim());
										uni_dyn_array.Replace(21, uv_dts.Trim());
										uni_dyn_array.Replace(23, num_contact_attempts.ToString());
										uni_dyn_array.Replace(32, email.Trim());
										uni_dyn_array.Replace(33, uv_dts.Trim());
										uni_dyn_array.Replace(35, contact_method.Trim());
										uni_dyn_array.Replace(37, status.Trim());
										uni_dyn_array.Replace(38, uv_dts.Trim());
										uni_dyn_array.Replace(39, act_code.Trim());
										uni_dyn_array.Replace(40, phone.Trim());
										uni_dyn_array.Replace(41, "Internet Contact (HomeSaver)");

										if(need_help == "Good")
										{
											uni_dyn_array.Replace(22, uv_dts.Trim());
										}
										else if(help_type == "HelpPhone")
										{
											uni_dyn_array.Replace(36, contact_time);
										}

										uni_file.Write(id, uni_dyn_array);

										result.IsSuccessful = true;
									}
								}
							}
						}
						else // Insert New
						{
							using(UniFile uni_file = uni_session.CreateUniFile("WEB.OCS"))
							{
								using(UniDynArray uni_dyn_array = new UniDynArray(uni_session))
								{
									uni_dyn_array.Insert(3, loan_number.Trim());
									uni_dyn_array.Insert(4, last_name.Trim());
									uni_dyn_array.Insert(5, first_name.Trim());
									uni_dyn_array.Insert(10, string.Format("{0} {1}", address_line1.Trim(), address_line2.Trim()));
									uni_dyn_array.Insert(11, city.Trim());
									uni_dyn_array.Insert(12, state.Trim());
									uni_dyn_array.Insert(13, zip.Trim());
									uni_dyn_array.Insert(14, phone.Trim());
									uni_dyn_array.Insert(21, uv_dts.Trim());
									uni_dyn_array.Insert(23, "1");
									uni_dyn_array.Insert(32, email.Trim());
									uni_dyn_array.Insert(33, uv_dts.Trim());
									uni_dyn_array.Insert(35, contact_method.Trim());
									uni_dyn_array.Insert(37, status.Trim());
									uni_dyn_array.Insert(38, uv_dts.Trim());
									uni_dyn_array.Insert(39, act_code.Trim());
									uni_dyn_array.Insert(40, phone.Trim());
									uni_dyn_array.Insert(41, "Internet Contact (HomeSaver)");

									if(need_help == "Good")
									{
										uni_dyn_array.Replace(22, uv_dts.Trim());
									}
									else if(help_type == "HelpPhone")
									{
										uni_dyn_array.Replace(36, contact_time);
									}

									uni_file.Write(string.Format("SLN{0}", loan_number), uni_dyn_array);

									result.IsSuccessful = true;
								}
							}
						}
					}

				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public void SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip)
		{
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					string RecId = String.Format("INH{0}", InNo.ToString());
					UniDynArray DisRec;
					using(UniFile PermFile = UvSess.CreateUniFile("WEB.TRANS.INFO.LOG"))
					{
						try
						{//if read sucessful info allready sent nothing more to do
							DisRec = PermFile.Read(RecId);
							DisRec.Dispose();
							return;
						}
						catch(UniFileException)
						{//if read fails record not sent continue
						}
					}
					using(UniFile TempFile = UvSess.CreateUniFile("WEB.TRANS.INFO"))
					{
						RecId = String.Format("HPF_PRIVACY*{0}", RecId);
						try
						{//get curent record if exists
							DisRec = TempFile.Read(RecId);
						}
						catch(UniFileException)
						{//if record not found start new record
							DisRec = UvSess.CreateUniDynArray();
							DisRec.Insert(9, " ");// size record  (Must use Non Empty String)
						}

						DisRec.Replace(1, UvSess.Iconv(DateTime.Now.ToString("MM/dd/yyyy"), "D"));
						DisRec.Replace(2, "I");
						DisRec.Replace(3, Lang);
						DisRec.Replace(4, eMail);
						DisRec.Replace(5, String.Format("{0} {1}", FirstName, LastName));
						DisRec.Replace(6, Addr1);
						DisRec.Replace(7, Addr2);
						DisRec.Replace(8, String.Format("{0} {1} {2}", City, State, Zip));

						TempFile.Write(RecId, DisRec);
						DisRec.Dispose();
					}
				}
			}
			catch
			{
			}
		}
	}
}