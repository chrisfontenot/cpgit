﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity;
using IBMU2.UODOTNET;
using Cccs.Host.Dal;
using System.Configuration;

namespace Cccs.Host.Impl
{
    public partial class Host : IHost
    {
        private readonly IIdentity identity;
        private readonly NLog.Logger logger;
        private string hostName;
        private string userId;
        private string password;
        private string account;
        private string eoustUser;

        public Host(IIdentity identity)
        {
            this.logger = NLog.LogManager.GetCurrentClassLogger();
            this.identity = identity;

            SetHostConfiguration();
            SetEncryption();
        }

        private UniSession OpenUniSession()
        {
            return UniObjects.OpenSession(hostName, userId, password, account);
        }

        private void CloseUniSession(UniSession UvSess)
        {
            if (UvSess != null && UvSess.IsActive)
                UniObjects.CloseSession(UvSess);
        }

        private void SetHostConfiguration()
        {
            var configurationValid = true;

            this.hostName = ConfigurationManager.AppSettings["HostName"];
            if (String.IsNullOrEmpty(hostName))
            {
                logger.Fatal("Unable to retrieve Host server name configuration. Verify appSettings 'HostName' key has been configured correctly.");
                configurationValid = false;
            }

            this.userId = ConfigurationManager.AppSettings["UserId"];
            if (String.IsNullOrEmpty(hostName))
            {
                logger.Fatal("Unable to retrieve Host user configuration. Verify appSettings 'UserId' key has been configured correctly.");
                configurationValid = false;
            }

            this.password = ConfigurationManager.AppSettings["Password"];
            if (String.IsNullOrEmpty(hostName))
            {
                logger.Fatal("Unable to retrieve Host password configuration. Verify appSettings 'Password' key has been configured correctly.");
                configurationValid = false;
            }

            this.account = ConfigurationManager.AppSettings["Account"];
            if (String.IsNullOrEmpty(hostName))
            {
                logger.Fatal("Unable to retrieve Host account configuration. Verify appSettings 'Account' key has been configured correctly.");
                configurationValid = false;
            }

            this.eoustUser = ConfigurationManager.AppSettings["EoustUser"];
            if (String.IsNullOrEmpty(eoustUser))
            {
                logger.Fatal("Unable to retrieve Eoust User configuration. Verify appSettings 'Account' key has been configured correctly.");
                configurationValid = false;
            }

            if (!configurationValid)
                throw new ConfigurationErrorsException("Unable to configure Host settings.");
        }

        private void SetEncryption()
        {
            var configurationValid = true;

            var encryptionKey = ConfigurationManager.AppSettings["EnKey"];
            if (encryptionKey == null)
            {
                logger.Fatal("Unable to retrieve CredabilityHostDataContext encryption key. Verify appSettings 'EnKey' key has been configured correctly.");
                configurationValid = false;
            }

            var encryptionPassword = ConfigurationManager.AppSettings["EnPassword"];
            if (encryptionPassword == null)
            {
                logger.Fatal("Unable to retrieve CredabilityHostDataContext encryption password. Verify appSettings 'EnPassword' key has been configured correctly.");
                configurationValid = false;
            }

            if (!configurationValid)
                throw new ConfigurationErrorsException("Unable to configure encryption settings.");

            CredabilityHostDataContext.SetEncryption(encryptionKey, encryptionPassword);
        }
    }
}