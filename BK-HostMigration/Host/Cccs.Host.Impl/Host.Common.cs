﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using IBMU2.UODOTNET;
using System.Xml;
using Cccs.Identity;
using Cccs.Host.Dal;
using Cccs.Host.Dto;

namespace Cccs.Host.Impl
{
	public partial class Host : IHost
	{
		#region Legacy

		private const char FldDlim = (char) 254;
		private const string AlphaNoChr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		private const string NoChr = "0123456789";

		private string CleanData(string dirty_str, string AlowedChr)
		{
			StringBuilder clean_str = new StringBuilder(dirty_str.Length);

			for(int i = 0; i < dirty_str.Length; i++)
			{
				if(AlowedChr.Contains(dirty_str[i]))
				{
					clean_str.Append(dirty_str[i]);
				}
			}

			return clean_str.ToString();
		}

		private string CleanZeros(string dirty_str)
		{
			StringBuilder clean_str = new StringBuilder(dirty_str.Length);

			int i = 0;
			for(; (i < dirty_str.Length) && (dirty_str[i] == '0'); i++)
			{
			}

			if(i < dirty_str.Length)
			{
				for(; i < dirty_str.Length; i++)
				{
					clean_str.Append(dirty_str[i]);
				}
			}

			return clean_str.ToString();
		}

		#endregion

		public string GetCounselorName(string counselorId)
		{
			try
			{
				using(var session = OpenUniSession())
				using(var file = session.CreateUniFile("WEB.COUNSELOR"))
				using(var record = file.Read(counselorId))
				{
					var counselorName = record.Extract(1).StringValue.Trim();
					return counselorName;
				}
			}
			catch
			{
			}

			return null;
		}

		private static string FinalId(int client_number, string client_type, string ref_code)
		{
			string final_id;

			if((client_type == "HUD") || ((client_type == "WEL") && (ref_code == "WFH/360")))
			{
				final_id = string.Format("H{0}", client_number);
			}
			else
			{
				final_id = string.Format("IN{0}", client_number);
			}

			return final_id;
		}

		public Result<XmlDocument> GetEscrow(int firm_id)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			XmlDocument XmlDoc = new XmlDocument();
			XmlElement Response = XmlDoc.CreateElement("Response");

			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile UvFile = UvSess.CreateUniFile("WEB_ATTY_FIRM"))
					{
						using(UniDynArray UvRec = UvFile.Read(firm_id.ToString()))
						{
							XmlElement FirmCode = XmlDoc.CreateElement("FirmCode");
							FirmCode.InnerText = firm_id.ToString();
							Response.AppendChild(FirmCode);

							XmlElement Name = XmlDoc.CreateElement("Name");
							Name.InnerText = UvRec.Extract(1).ToString();
							Response.AppendChild(Name);

							XmlElement Escrow = XmlDoc.CreateElement("Escrow");
							Escrow.InnerText = UvRec.Extract(21).ToString();
							Response.AppendChild(Escrow);

                            XmlElement ChargeAmount = XmlDoc.CreateElement("ChargeAmount");
							ChargeAmount.InnerText = UvRec.Extract(64).ToString();
							Response.AppendChild(ChargeAmount);

							result.IsSuccessful = true;
						}
					}

					UniObjects.CloseSession(UvSess);
				}
			}
			catch(Exception ex)
			{
				XmlElement Error = XmlDoc.CreateElement("Error");
				Error.InnerText = ex.Message;
				Response.AppendChild(Error);

				result.Exception = ex;
				result.IsSuccessful = false;
			}

			XmlDoc.AppendChild(Response);
			result.Value = XmlDoc;

			return result;
		}

		public Result<XmlDocument> ReqCredRpt(long user_id, int client_number)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			XmlDocument XmlDoc = new XmlDocument();
			XmlElement Response = XmlDoc.CreateElement("Response");

			try
			{
				UserProfile user_profile = identity.UserProfileGet(user_id, new UserProfileOptions { IncludeUserDetails = true, IncludeAddresses = true, IncludeAccounts = true });

				if((user_profile != null) && (user_profile.UserDetailPrimary != null) && (user_profile.Addresses != null) && (user_profile.Addresses.Length > 0))
				{
					string client_type = null;
					string ref_code = null;
					string birth_city_primary = null;
					string birth_city_secondary = null;

					using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
					{
						var contact_detail =
						(
							from c in data_context.contactdetails
							where (c.client_number == client_number)
							select new
							{
								ClientType = c.ClientType,
								RefCode = c.refcode,
								BirthCityPrimary = c.cred_rpt_bcity,
								BirthCitySecondary = c.cred_rpt_bcity2,
							}
						).FirstOrDefault();

						if(contact_detail != null)
						{
							client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
							ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
							birth_city_primary = (contact_detail.BirthCityPrimary != null) ? contact_detail.BirthCityPrimary.Trim() : null;
							birth_city_secondary = (contact_detail.BirthCitySecondary != null) ? contact_detail.BirthCitySecondary.Trim() : null;
						}
					}

					if(!string.IsNullOrEmpty(birth_city_primary))
					{
						using(UniSession UvSess = OpenUniSession())
						{
							using(UniFile UvFile = UvSess.CreateUniFile("CREDRPT_QUE"))
							{
								using(UniDynArray UvRec = new UniDynArray(UvSess))
								{
									string final_id = FinalId(client_number, client_type, ref_code);

									UvRec.Insert(1, user_profile.UserDetailPrimary.FirstName.Trim());
									UvRec.Insert(2, user_profile.UserDetailPrimary.LastName.Trim());
									UvRec.Insert(3, user_profile.UserDetailPrimary.Ssn.Trim());
									UvRec.Insert(4, user_profile.UserDetailPrimary.BirthDate.HasValue ? user_profile.UserDetailPrimary.BirthDate.Value.ToShortDateString() : string.Empty);
									UvRec.Insert(5, user_profile.Addresses[0].StreetLine1.Trim());
									UvRec.Insert(8, user_profile.Addresses[0].City.Trim());
									UvRec.Insert(9, user_profile.Addresses[0].State.Trim());
									UvRec.Insert(10, user_profile.Addresses[0].Zip.Trim());
									UvRec.Insert(11, birth_city_primary.Trim());
									UvFile.Write(final_id, UvRec);

									if((user_profile.UserDetailSecondary != null) && !string.IsNullOrEmpty(birth_city_secondary))
									{
										final_id += "*S";
										UvRec.Replace(1, user_profile.UserDetailSecondary.FirstName.Trim());
										UvRec.Replace(2, user_profile.UserDetailSecondary.LastName.Trim());
										UvRec.Replace(3, user_profile.UserDetailSecondary.Ssn.Trim());
										UvRec.Replace(4, user_profile.UserDetailSecondary.BirthDate.HasValue ? user_profile.UserDetailPrimary.BirthDate.Value.ToShortDateString() : string.Empty);
										UvRec.Replace(11, birth_city_secondary.Trim());
										UvFile.Write(final_id, UvRec);
									}

									if(user_profile.Accounts.FindByAccountTypeCode(Account.BKE) == null) // if (ClientType != "BK")
									{
										XmlElement DmpAvail = XmlDoc.CreateElement("DmpAvail");

										using(UniFile StFee = UvSess.CreateUniFile("WEB.STATEFEES"))
										{
											try
											{
												DmpAvail.InnerText = StFee.ReadField(user_profile.Addresses[0].State.Trim(), 16).ToString();
											}
											catch(UniFileException)
											{
												DmpAvail.InnerText = "";
											}
										}
										Response.AppendChild(DmpAvail);
									}

									result.IsSuccessful = true;
								}
							}

							UniObjects.CloseSession(UvSess);
						}
					}
				}
			}
			catch(Exception ex)
			{
				XmlElement Error = XmlDoc.CreateElement("Error");
				Error.InnerText = ex.Message;
				Response.AppendChild(Error);

				result.Exception = ex;
				result.IsSuccessful = false;
			}

			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

		public Result<XmlDocument> RequestCreditReport
		(
			int client_number,
			string client_type,
			string ref_code,
			string primary_birth_city,
			string secondary_birth_city,
			string primary_first_name,
			string primary_last_name,
			string primary_ssn,
			DateTime? primary_birth_date,
			string street_line1,
			string city,
			string state,
			string zip,
			string secondary_first_name,
			string secondary_last_name,
			string secondary_ssn,
			DateTime? secondary_birth_date
		)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			XmlDocument XmlDoc = new XmlDocument();
			XmlElement Response = XmlDoc.CreateElement("Response");

			try
			{

				if(!string.IsNullOrEmpty(primary_birth_city))
				{
					using(UniSession UvSess = OpenUniSession())
					{
						using(UniFile UvFile = UvSess.CreateUniFile("CREDRPT_QUE"))
						{
							using(UniDynArray UvRec = new UniDynArray(UvSess))
							{
								string final_id = FinalId(client_number, client_type, ref_code);

								UvRec.Insert(1, primary_first_name);
								UvRec.Insert(2, primary_last_name);
								UvRec.Insert(3, primary_ssn);
								UvRec.Insert(4, primary_birth_date.HasValue ? primary_birth_date.Value.ToShortDateString() : string.Empty);
								UvRec.Insert(5, street_line1);
								UvRec.Insert(8, city);
								UvRec.Insert(9, state);
								UvRec.Insert(10, zip);
								UvRec.Insert(11, primary_birth_city);
								UvFile.Write(final_id, UvRec);

								if(!string.IsNullOrEmpty(secondary_birth_city))
								{
									final_id += "*S";
									UvRec.Replace(1, secondary_first_name);
									UvRec.Replace(2, secondary_last_name);
									UvRec.Replace(3, secondary_ssn);
									UvRec.Replace(4, secondary_birth_date.HasValue ? secondary_birth_date.Value.ToShortDateString() : string.Empty);
									UvRec.Replace(11, secondary_birth_city);
									UvFile.Write(final_id, UvRec);
								}

								XmlElement DmpAvail = XmlDoc.CreateElement("DmpAvail");

								using(UniFile StFee = UvSess.CreateUniFile("WEB.STATEFEES"))
								{
									try
									{
										DmpAvail.InnerText = StFee.ReadField(state, 16).ToString();
									}
									catch(UniFileException)
									{
										DmpAvail.InnerText = "";
									}
								}
								Response.AppendChild(DmpAvail);

								result.IsSuccessful = true;
							}
						}

						UniObjects.CloseSession(UvSess);
					}
				}
			}
			catch(Exception ex)
			{
				XmlElement Error = XmlDoc.CreateElement("Error");
				Error.InnerText = ex.Message;
				Response.AppendChild(Error);

				result.Exception = ex;
				result.IsSuccessful = false;
			}

			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

		public Result GetCredRpt(int client_number)
		{
			Result result = new Result();

			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					bool clear_credit_report_pull = false;

					using(UniFile UvFile = UvSess.CreateUniFile("CREDACCT_QUE"))
					{
						string client_type = null;
						string ref_code = null;
						using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
						{
							var contact_detail =
							(
								from c in data_context.contactdetails
								where (c.client_number == client_number)
								select new
								{
									ClientType = c.ClientType,
									RefCode = c.refcode,
								}
							).FirstOrDefault();

							if(contact_detail != null)
							{
								client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
								ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
							}
						}

						UniDynArray UvRec = null;
						int i = 1;
						string final_id = FinalId(client_number, client_type, ref_code);
						bool done = false;

						try
						{
							UvRec = UvFile.Read(final_id + "*" + i.ToString());

							using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
							{
								var creditor_details = from c in data_context.creditordetails
													   where ((c.client_number == client_number) && (string.Compare(c.cr_flag, "y", true) == 0))
													   select c;
								data_context.creditordetails.DeleteAllOnSubmit(creditor_details);
								data_context.SubmitChanges();
							}
						}
						catch(UniFileException)
						{
							done = true;
						}


						while(!done)
						{
							clear_credit_report_pull = true;

							try
							{
								int creditor_bal = 0;
								try
								{
									creditor_bal = (int) float.Parse(CleanData(UvRec.Extract(2).ToString(), NoChr + "."));
								}
								catch(FormatException)
								{
									creditor_bal = 0;
								}

								int creditor_payments = 0;
								try
								{
									creditor_payments = (int) float.Parse(CleanData(UvRec.Extract(6).ToString(), NoChr + "."));
								}
								catch(FormatException)
								{
									creditor_payments = 0;
								}

								string creditor_name = CleanData(UvRec.Extract(1).ToString(), AlphaNoChr);
								string creditor_jointacct = CleanData(UvRec.Extract(5).ToString(), AlphaNoChr);
								string CrAcntNoEnc = CleanData(UvRec.Extract(3).ToString(), AlphaNoChr);
								string PreAcntNoEnc = CleanData(UvRec.Extract(3).ToString(), AlphaNoChr);

								using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
								{
									creditordetail creditor_detail = new creditordetail()
									{
										creditor_intrate = 0.0f,
										creditor_past_due = "0",
										cr_flag = "y",
										client_number = client_number,
										creditor_name = creditor_name,
										creditor_bal = creditor_bal,
										creditor_payments = creditor_payments,
										creditor_jointacct = creditor_jointacct,
										CrAcntNoEnc = data_context.EncryptText(CrAcntNoEnc),
										PreAcntNoEnc = data_context.EncryptText(PreAcntNoEnc),
									};

									data_context.creditordetails.InsertOnSubmit(creditor_detail);
									data_context.SubmitChanges();
								}
							}
							catch(Exception)
							{
							}

							try
							{
								UvFile.DeleteRecord(string.Format("{0}*{1}", final_id, i));
							}
							catch(UniFileException)
							{
							}

							i++;
							try
							{
								UvRec = UvFile.Read(string.Format("{0}*{1}", final_id, i));
							}
							catch(UniFileException)
							{
								done = true;
							}
						}
						if(UvRec != null)
						{
							UvRec.Dispose();
						}
					}

					if(clear_credit_report_pull)
					{
						using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
						{
							var contact_detail = from c in data_context.contactdetails
												 where (c.client_number == client_number)
												 select c;

							foreach(var c in contact_detail)
							{
								c.cred_rpt_pull = null;
							}

							data_context.SubmitChanges();
						}
					}

					UniObjects.CloseSession(UvSess);

					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public Result<CreditScoreDto> GetCredScore(int client_number)
		{
			var result = new Result<CreditScoreDto>();
			var dto = new CreditScoreDto();

			using(var UvSess = OpenUniSession())
			using(var UvCmd = UvSess.CreateUniCommand())
			{
				try
				{
					string RecID = String.Format("IN{0}", client_number.ToString());
					UvCmd.Command = String.Format("RUN WEB.BP GET.FICO {0}", RecID);
					UvCmd.Execute();
					string UvRec = UvCmd.Response;

					result.IsSuccessful = !String.IsNullOrEmpty(UvRec);
					if(!String.IsNullOrEmpty(UvRec))
					{
						string[] UvFields = UvRec.Split(FldDlim);
						dto.PrimaryScore = UvFields[1].Trim();
						dto.SecondaryScore = UvFields[2].Trim();
					}
				}
				catch
				{
					result.IsSuccessful = false;
				}
			}

			result.Value = dto;
			return result;
		}

		public Result<XmlDocument> HaveCredRpt(int client_number)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			XmlDocument XmlDoc = new XmlDocument();
			XmlElement Response = XmlDoc.CreateElement("Response");
			string OutTxt = string.Empty;

			try
			{
				bool cred_rpt_pull = false;
				string client_type = null;
				string ref_code = null;
				using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
				{
					var contact_detail =
					(
						from c in data_context.contactdetails
						where (c.client_number == client_number)
						select new
						{
							ClientType = c.ClientType,
							RefCode = c.refcode,
							CredRptPull = c.cred_rpt_pull,
						}
					).FirstOrDefault();

					if(contact_detail != null)
					{
						client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
						ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
						cred_rpt_pull = (contact_detail.CredRptPull == 'y');
					}
				}

				if(cred_rpt_pull)
				{
					string final_id = FinalId(client_number, client_type, ref_code);

					using(UniSession UvSess = OpenUniSession())
					{
						using(UniFile UvFile = UvSess.CreateUniFile("CREDACCT_QUE"))
						{
							try
							{
								using(UniDynArray UvRec = UvFile.Read(string.Format("{0}*1", final_id)))
								{
									OutTxt = "Ready";
								}
							}
							catch(UniFileException)
							{
								OutTxt = "NotYet";
							}
						}
					}
				}
				else
				{
					OutTxt = "NoRpt";
				}

				result.IsSuccessful = true;
			}
			catch(Exception ex)
			{
				OutTxt = "Error";

				result.Exception = ex;
				result.IsSuccessful = false;
			}

			XmlElement CredRpt = XmlDoc.CreateElement("CredRpt");
			CredRpt.InnerText = OutTxt;
			Response.AppendChild(CredRpt);
			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

		public Result<XmlDocument> SaveTrans(int client_number, int van_tran_ref, string invoice_no, float char_amt)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			UniSession UvSess = null;
			String SeqNo = "";
			XmlDocument XmlDoc = new XmlDocument();
			String RecID = "IN" + client_number.ToString();
			XmlElement Response = XmlDoc.CreateElement("Response");
			XmlElement ClNo = XmlDoc.CreateElement("InNo");
			ClNo.InnerText = RecID;
			Response.AppendChild(ClNo);

			try
			{
				UvSess = OpenUniSession();
				UniCommand UvCmd = UvSess.CreateUniCommand();
				try
				{
					UvCmd.Command = "RUN WEB.BP GET.UV.DATE";
					UvCmd.Execute();
					String UvDTS = UvCmd.Response.Trim();
					UvCmd.Command = "RUN WEB.BP GET.CLNO " + RecID;
					UvCmd.Execute();
					String UvClNo = UvCmd.Response.Trim();
					UniFile UvTrans = null;
					if(UvClNo.Trim() != "")
					{
						UniFile BrCam = UvSess.CreateUniFile("WEB.BRCAM");
						try
						{
							UniDynArray UvBrRec = BrCam.Read(UvClNo.Trim());
							SeqNo = invoice_no.Substring(invoice_no.LastIndexOf('-') + 1);
							UvBrRec.Replace(213, UvDTS);//
							UvBrRec.Replace(276, invoice_no);//
							UvBrRec.Replace(278, SeqNo);//
							BrCam.Write(UvClNo.Trim(), UvBrRec);
							UvBrRec.Dispose();
						}
						catch(UniFileException)
						{
						}
						BrCam.Close();
						BrCam.Dispose();
						UvTrans = UvSess.CreateUniFile("WEB.BRTRANS");
					}
					else
					{
						UvTrans = UvSess.CreateUniFile("WEB.TEMP.BRTRANS");
						UvClNo = RecID;
					}
					try
					{
						UniFile SeqCon = UvSess.CreateUniFile("WEB.CAM.CONTROL");
						UniDynArray UvSeqRec = SeqCon.Read("BR.TRANS.SEQ");
						SeqNo = UvSeqRec.Extract(1).ToString();
						int NextSeqNo = int.Parse(SeqNo) + 1;
						UvSeqRec.Replace(1, NextSeqNo.ToString());
						SeqCon.Write("BR.TRANS.SEQ", UvSeqRec);
						UvSeqRec.Dispose();
						SeqCon.Close();
						SeqCon.Dispose();
						UniDynArray TransRec = new UniDynArray(UvSess);
						TransRec.Insert(1, UvDTS.Trim());//BILL.DATE
						TransRec.Insert(2, "D");//PAYTYPE
						TransRec.Insert(3, char_amt.ToString());//BILL.AMOUNT
						TransRec.Insert(4, UvClNo);//CLIENT.NO
						TransRec.Insert(10, van_tran_ref.ToString());//PAYMENT.REF.NO
						TransRec.Insert(11, "PMT");//TRANS.TYPE
						TransRec.Insert(12, invoice_no);//INVOICE.NO
						UvTrans.Write("I" + SeqNo, TransRec);
						TransRec.Dispose();

						result.IsSuccessful = true;
					}
					catch(UniFileException ex)
					{
						XmlElement Error = XmlDoc.CreateElement("Error");
						Error.InnerText = ex.Message;
						Response.AppendChild(Error);
					}
					UvTrans.Close();
					UvTrans.Dispose();
				}
				catch(UniSessionException ex)
				{
					XmlElement Error = XmlDoc.CreateElement("Error");
					Error.InnerText = ex.Message;
					Response.AppendChild(Error);

					result.IsSuccessful = false;
				}
			}
			catch(Exception)
			{
				if(UvSess != null && UvSess.IsActive)
				{
					UniObjects.CloseSession(UvSess);
					UvSess = null;
				}

				result.IsSuccessful = false;
			}
			finally
			{
				if(UvSess != null && UvSess.IsActive)
				{
					UniObjects.CloseSession(UvSess);
					UvSess = null;
				}
			}

			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

		public Result<XmlDocument> GetNextSeqNo(int client_number)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			UniSession UvSess = null;
			XmlDocument XmlDoc = new XmlDocument();
			String RecID = "IN" + client_number.ToString();
			XmlElement Response = XmlDoc.CreateElement("Response");
			XmlElement ClNo = XmlDoc.CreateElement("InNo");
			ClNo.InnerText = RecID;
			Response.AppendChild(ClNo);
			try
			{
				UvSess = OpenUniSession();

				UniCommand UvCmd = UvSess.CreateUniCommand();
				try
				{
					UvCmd.Command = "RUN WEB.BP GET.CLNO " + RecID;
					UvCmd.Execute();
					String UvClNo = UvCmd.Response.Trim();
					if(UvClNo.Trim() != "")
					{
						UniFile UvFile = UvSess.CreateUniFile("WEB.BRCAM");
						try
						{
							UniDynArray UvRec = UvFile.Read(UvClNo.Trim());
							int NestSeqNo = 0;
							if(!int.TryParse(UvRec.Extract(278).ToString().Trim(), out NestSeqNo))
							{
								NestSeqNo = 0;
							}
							NestSeqNo++;
							XmlElement SeqNo = XmlDoc.CreateElement("SeqNo");
							SeqNo.InnerText = NestSeqNo.ToString();
							Response.AppendChild(SeqNo);
							UvRec.Dispose();
						}
						catch(UniFileException ex)
						{
							XmlElement Error = XmlDoc.CreateElement("Error");
							Error.InnerText = ex.Message;
							//Response.AppendChild(Error);
							XmlElement SeqNo = XmlDoc.CreateElement("SeqNo");
							SeqNo.InnerText = "1";
							Response.AppendChild(SeqNo);
						}
						UvFile.Close();
						UvFile.Dispose();
					}
					else
					{
						XmlElement SeqNo = XmlDoc.CreateElement("SeqNo");
						SeqNo.InnerText = "1";
						Response.AppendChild(SeqNo);
					}
				}
				catch(UniCommandException)
				{
					XmlElement SeqNo = XmlDoc.CreateElement("SeqNo");
					SeqNo.InnerText = "1";
					Response.AppendChild(SeqNo);
				}
				UvCmd.Dispose();
			}
			catch(Exception)
			{
				if(UvSess != null && UvSess.IsActive)
				{
					UniObjects.CloseSession(UvSess);
					UvSess = null;
				}
			}
			finally
			{
				if(UvSess != null && UvSess.IsActive)
				{
					UniObjects.CloseSession(UvSess);
					UvSess = null;
				}
			}
			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

		public Result<XmlDocument> UpdateOCS(int client_number, int stat)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			try
			{
				string RecID = string.Format("IN{0}", client_number);

				XmlDocument XmlDoc = new XmlDocument();
				XmlElement Response = XmlDoc.CreateElement("Response");

				string loan_number = null;
				string ref_code = null;
				using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
				{
					var contact_detail =
					(
						from c in data_context.contactdetails
						where (c.client_number == client_number)
						select new
						{
							LoanNumber = c.loan_number,
							RefCode = c.refcode,
						}
					).FirstOrDefault();

					if(contact_detail != null)
					{
						loan_number = (contact_detail.LoanNumber != null) ? contact_detail.LoanNumber.Trim() : null;
						ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
					}
				}


				if(!string.IsNullOrEmpty(loan_number))
				{
					using(UniSession UvSess = OpenUniSession())
					{
						using(UniCommand UvCmd = UvSess.CreateUniCommand())
						{
							try
							{
								UvCmd.Command = "RUN WEB.BP GET.UV.DATE";
								UvCmd.Execute();
								string UvDTS = UvCmd.Response.Trim();
								string[] OcsID = null;

								using(UniSubroutine GetIDs = UvSess.CreateUniSubroutine("SEARCH.DATA.SUB", 2))
								{
									GetIDs.SetArg(0, "SELECT WEB.OCS WITH LOAN.NO = \"" + loan_number + "\"");
									GetIDs.Call();
									OcsID = GetIDs.GetArg(1).Split(FldDlim);

									using(UniFile OcsFile = UvSess.CreateUniFile("WEB.OCS"))
									{
										using(UniDynArray OcsRec = OcsFile.Read(OcsID[0]))
										{
											OcsRec.Replace(21, UvDTS); //RCVD.DATE
											OcsRec.Replace(33, UvDTS); //MOD.DATE
											OcsRec.Replace(38, UvDTS); //WEB.CALL.DATES
											OcsRec.Replace(31, "I"); //QUE
											OcsRec.Replace(41, "Internet Contact (" + stat + ")"); //WEB.NOTES

											switch(stat)
											{
												case 1:
													int ContAtmps;
													if(int.TryParse(OcsRec.Extract(23).ToString().Trim(), out ContAtmps))
													{
														ContAtmps++;
													}
													else
													{
														ContAtmps = 1;
													}
													OcsRec.Replace(23, ContAtmps.ToString()); // CONTACT.ATMPS
													OcsRec.Replace(37, "CT"); // WEB.STATUS
													OcsRec.Replace(39, "NI"); // WEB.RESULT.CODES
													break;

												case 2:
													OcsRec.Replace(37, "MC"); // WEB.STATUS
													OcsRec.Replace(39, "MD"); // WEB.RESULT.CODES
													break;

												case 3:
													OcsRec.Replace(37, "CO"); // WEB.STATUS
													OcsRec.Replace(39, "CO"); // WEB.RESULT.CODES
													break;
											}
											OcsFile.Write(OcsID[0], OcsRec);
											result.IsSuccessful = true;
										}
									}
								}
							}
							catch(UniFileException)
							{
								if(ref_code == "Sav/161")
								{
									XmlElement Error = XmlDoc.CreateElement("Error");
									Error.InnerText = "Error finding OCS Record";
									Response.AppendChild(Error);
								}
							}
						}

						UniObjects.CloseSession(UvSess);
					}
				}

				XmlDoc.AppendChild(Response);

				result.Value = XmlDoc;
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = true;
			}

			return result;
		}

		public Result<XmlDocument> LoadDmp(int client_number)
		{
			Result<XmlDocument> result = new Result<XmlDocument>();

			XmlDocument XmlDoc = new XmlDocument();
			XmlElement Response = XmlDoc.CreateElement("Response");

			try
			{
				Address[] addresses = identity.AddressesGet(client_number);

				if((addresses != null) && (addresses.Length > 0))
				{
					string state = addresses[0].State;
					int? processed = null;
					string dmp_fco_pcp_flag = null;
					string dmp_fco_pcp_date = null;
					string client_type = null;
					string ref_code = null;

					using(CredabilityHostDataContext data_context = new CredabilityHostDataContext())
					{
						var contact_detail =
						(
							from c in data_context.contactdetails
							where (c.client_number == client_number)
							select new
							{
								Processed = c.processed,
								DmpFcoPcpFlag = c.dmp_fco_pcp_flag,
								DmpFcoPcpDate = c.dmp_fco_pcp_date,
								ClientType = c.ClientType,
								RefCode = c.refcode,
							}
						).FirstOrDefault();

						if(contact_detail != null)
						{
							processed = contact_detail.Processed;
							dmp_fco_pcp_flag = (contact_detail.DmpFcoPcpFlag != null) ? contact_detail.DmpFcoPcpFlag.Trim() : null;
							dmp_fco_pcp_date = (contact_detail.DmpFcoPcpDate != null) ? contact_detail.DmpFcoPcpDate.Trim() : null;
							client_type = (contact_detail.ClientType != null) ? contact_detail.ClientType.Trim() : null;
							ref_code = (contact_detail.RefCode != null) ? contact_detail.RefCode.Trim() : null;
						}
					}

					bool DmpFlag = true;

					using(UniSession UvSess = OpenUniSession())
					{
						using(UniFile StFee = UvSess.CreateUniFile("WEB.STATEFEES"))
						{
							try
							{
								if(StFee.ReadField(state, 16).ToString().Trim() == "N") // DMPAVAIL
								{
									DmpFlag = false;
								}
							}
							catch(UniFileException)
							{
								DmpFlag = false;
							}
						}

						if(DmpFlag && (processed > 0))
						{
							string final_id;

							if((client_type == "HUD") || ((client_type == "WEL") && (ref_code == "WFH/360")))
							{
								final_id = string.Format("INH{0}", client_number);
							}
							else
							{
								final_id = string.Format("IN{0}", client_number);
							}


							UniSubroutine GetIDs = UvSess.CreateUniSubroutine("SEARCH.DATA.SUB", 2);
							GetIDs.SetArg(0, "SELECT WEB.INWEB WITH INNO = \"" + final_id + "\"");
							GetIDs.Call();
							String ClNo = GetIDs.GetArg(1).Trim();
							GetIDs.Dispose();
							if(ClNo != "")
							{
								if(ClNo.Contains(FldDlim.ToString()))
								{
									String[] HoldClNo = ClNo.Split(FldDlim);
									ClNo = HoldClNo[0];
								}
								UniFile ClMaster = UvSess.CreateUniFile("WEB.CM");
								String Stat = ClMaster.ReadField(ClNo, 11).ToString().Trim();//STAT
								ClMaster.Close();
								ClMaster.Dispose();
								if(Stat == "A" || Stat == "AR")
								{
									DmpFlag = false;
								}
							}
						}
						XmlElement DmpSignUp = XmlDoc.CreateElement("DmpSignUp");
						DmpSignUp.InnerText = dmp_fco_pcp_flag;
						Response.AppendChild(DmpSignUp);
						XmlElement DmpDate = XmlDoc.CreateElement("DmpDate");
						DmpDate.InnerText = dmp_fco_pcp_date;
						Response.AppendChild(DmpDate);

						UniObjects.CloseSession(UvSess);

					}

					XmlElement DmpDisp = XmlDoc.CreateElement("DmpDisp");
					DmpDisp.InnerText = DmpFlag.ToString();
					Response.AppendChild(DmpDisp);

					result.IsSuccessful = true;
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			XmlDoc.AppendChild(Response);

			result.Value = XmlDoc;

			return result;
		}

        public Result<double> GetCharAmt(string AttorneyCode, string ZipCode)
        {
            Result<double> result = new Result<double>(50.0);

            string cou_int_amt = "";
            double char_amt = 0.0;

            using (var UvSess = OpenUniSession())
            using (var UvCmd = UvSess.CreateUniCommand())
            {
                try
                {
                    //SessionState.PrimaryZipCode, SessionState.FirmID
                    UvCmd.Command = String.Format("RUN WEB.BP GET.FEE {0} {1} {2} {3}",
                        (String.IsNullOrEmpty(ZipCode) ? "99999" : ZipCode),
                        (String.IsNullOrEmpty(AttorneyCode) ? "9999" : AttorneyCode),
                        "B",
                        "I");
                    UvCmd.Execute();
                    string UvResult = UvCmd.Response.Trim();

                    result.IsSuccessful = !String.IsNullOrEmpty(UvResult);
                    if (!String.IsNullOrEmpty(UvResult))
                    {
                        cou_int_amt = UvResult;
                    }
                    else
                    {
                        result.IsSuccessful = false;
                        cou_int_amt = "5000";
                    }
                }
                catch (Exception exception)
                {
                    result.Exception = exception;
                    result.IsSuccessful = false;
                }
            }

            if (double.TryParse(cou_int_amt, out char_amt))
            {
                result.Value = char_amt / 100.0;

                result.IsSuccessful = true;
            }

            return result;
        }

        public Result<double> GetCharAmt(string AttorneyCode, string ZipCode, string LineOfService)
		{
			Result<double> result = new Result<double>(50.0);

            string cou_int_amt = "";
            double char_amt = 0.0;
            
            using (var UvSess = OpenUniSession())
            using (var UvCmd = UvSess.CreateUniCommand())
            {
                try
                {
                    //SessionState.PrimaryZipCode, SessionState.FirmID
                    UvCmd.Command = String.Format("RUN WEB.BP GET.FEE {0} {1} {2} {3}",
                        (String.IsNullOrEmpty(ZipCode) ? "99999" : ZipCode) ,
                        (String.IsNullOrEmpty(AttorneyCode) ? "9999" : AttorneyCode),
                        LineOfService, 
                        "I");
                    UvCmd.Execute();
                    string UvResult = UvCmd.Response.Trim();

                    result.IsSuccessful = !String.IsNullOrEmpty(UvResult);
                    if (!String.IsNullOrEmpty(UvResult))
                    {
                        cou_int_amt = UvResult;
                    }
                    else
                    {
                        result.IsSuccessful = false;
                        cou_int_amt = "5000";
                    }
                }
                catch (Exception exception)
                {
                    result.Exception = exception;
                    result.IsSuccessful = false;
                }
            }

            if (double.TryParse(cou_int_amt, out char_amt))
            {
                result.Value = char_amt / 100.0;

                result.IsSuccessful = true;
            }

			return result;
		}

		public Result<string> GetUvDts()
		{
			Result<string> result = new Result<string>();

			try
			{
				using(UniSession uni_session = OpenUniSession())
				{
					using(UniCommand uni_command = uni_session.CreateUniCommand())
					{
						uni_command.Command = "RUN WEB.BP GET.UV.DATE";
						uni_command.Execute();
						result.Value = uni_command.Response.Trim();
						result.IsSuccessful = true;
					}
				}
			}
			catch(Exception exception)
			{
				result.Exception = exception;
				result.IsSuccessful = false;
			}

			return result;
		}

		public Result<string> StageUvData(UvPushData UvData)
		{
			Result<string> Back = new Result<string>();
			try
			{
				using(UniSession UvSess = OpenUniSession())
				{
					using(UniFile CredDet = UvSess.CreateUniFile("SQL_CREDITORDETAIL"))
					{
                        DeleteAllEntries(UvData.InNo, CredDet);

						int i = 0;
						UniDynArray CredRec;
						foreach(var CurCred in UvData.Creditors)
						{
							CredRec = UvSess.CreateUniDynArray();
							CredRec.Insert(1, CurCred.Name);//CREDNAME
							CredRec.Insert(2, CurCred.Balance.ToString());//CREDBAL
							CredRec.Insert(3, CurCred.IntRate.ToString());//IRATE
							CredRec.Insert(4, CurCred.Payment.ToString());//PAYMENTS
							CredRec.Insert(5, CurCred.PreAcctNo);//PREACCTNBR
							CredRec.Insert(6, CurCred.PriAcctHolder);//PRIACCTNBR
							CredRec.Insert(7, CurCred.PastDue);//PASTDUE
							CredRec.Insert(8, CurCred.CrAcctNo);//ACCTNO

							CredDet.Write(String.Format("{0}*{1}", UvData.InNo, (++i).ToString()), CredRec);
							CredRec.Dispose();
						}
					}
					using(UniFile UvConDet = UvSess.CreateUniFile("SQL_CONTACTDETAIL"))
					using(UniFile UvConDet2 = UvSess.CreateUniFile("SQL_CONTACTDETAIL2"))
					{
						UniDynArray UvRec, UvRec2;
						try
						{
							UvRec = UvConDet.Read(UvData.InNo.ToString());
						}
						catch(UniFileException)
						{
							UvRec = UvSess.CreateUniDynArray();
							UvRec.Insert(247, " ");
						}
						try
						{
							UvRec2 = UvConDet2.Read(UvData.InNo.ToString());
						}
						catch(UniFileException)
						{
							UvRec2 = UvSess.CreateUniDynArray();
							UvRec2.Insert(62, " ");
						}

						//contactdetail SqlConDet = App
						UvRec.Replace(1, UvData.PriFirstName);//FNAME
						UvRec.Replace(2, UvData.PriMidName);//MINITIAL
						UvRec.Replace(3, UvData.PriLastName);//LNAME
						UvRec.Replace(4, UvData.Addr1);//ADDRESS
						UvRec.Replace(5, UvData.City);//CITY
						UvRec.Replace(6, UvData.State);//ST
						UvRec.Replace(7, UvData.Zip);//ZIP
						UvRec.Replace(8, UvData.PriConfirmMaiden);//CONF_MAIDEN
						UvRec.Replace(10, UvData.PriSsn);//SSN
						UvRec.Replace(11, UvData.PriDob);//DOB
						UvRec.Replace(12, UvData.PriSex);//SEX
						UvRec.Replace(13, UvData.PriMarital);//MARITAL
						UvRec.Replace(14, UvData.Phone1);//PHONE
						UvRec.Replace(15, UvData.eMail);//EMAIL
						UvRec.Replace(16, UvData.SecFirstName);//CO_FNAME
						UvRec.Replace(17, UvData.SecMidName);//CO_MINITIAL
						UvRec.Replace(18, UvData.SecLastName);//CO_LNAME
						UvRec.Replace(19, UvData.SecSsn);//CO_SSN
						UvRec.Replace(20, UvData.SecDob);//CO_DOB
						UvRec.Replace(21, (UvData.MortCurrent != null) ? UvData.MortCurrent.ToString() : String.Empty);//MORT_CURRENT
						UvRec.Replace(22, UvData.ValHome.ToString());//VAL_HOME
						UvRec.Replace(23, UvData.ValCar.ToString());//VAL_CAR
						UvRec.Replace(24, UvData.OweCar.ToString());//OWE_CAR
						UvRec.Replace(25, UvData.OweOther.ToString());//OWE_OTHER
						UvRec.Replace(27, UvData.RentMort.ToString());//RENT_MORT
						UvRec.Replace(28, UvData.EquityLoanTaxIns.ToString());//EQUITY_LOAN_TAX_INS
						UvRec.Replace(29, UvData.HomeMaintenance.ToString());//HOME_MAINTENANCE
						UvRec.Replace(30, UvData.MedicalInsurance.ToString());//INSURANCE
						UvRec.Replace(31, UvData.CarPayments.ToString());//VEHICLE_PAYMENTS
						UvRec.Replace(32, UvData.CarInsurance.ToString());//CAR_INSURANCE
						UvRec.Replace(33, UvData.Utilities.ToString());//UTILITIES
						UvRec.Replace(34, UvData.Telephone.ToString());//TELEPHONE
						UvRec.Replace(35, UvData.Groceries.ToString());//GROCERIES
						UvRec.Replace(36, UvData.FoodAway.ToString());//FOOD_AWAY
						UvRec.Replace(37, UvData.CarMaintenance.ToString());//CAR_MAINTENANCE
						UvRec.Replace(38, UvData.PublicTransportation.ToString());//PUBLIC_TRANSPORTATION
						UvRec.Replace(39, UvData.MedicalPrescription.ToString());//MEDICAL_PRESCRIPTION
						UvRec.Replace(40, UvData.ChildSupportAlimony.ToString());//CHILD_SUPPORT
						UvRec.Replace(41, UvData.ChildElderCare.ToString());//CHILD_ELDER_CARE
						UvRec.Replace(42, UvData.Miscellaneous.ToString());//MISC_EXP
						UvRec.Replace(44, UvData.Education.ToString());//EDUCATION
						UvRec.Replace(45, UvData.Laundry.ToString());//LAUNDRY
						UvRec.Replace(46, UvData.Clothing.ToString());//CLOTHING
						UvRec.Replace(47, UvData.BeautyBarber.ToString());//BEAUTY_BARBER
						UvRec.Replace(48, UvData.PersonalExpenses.ToString());//PERSONAL_EXP
						UvRec.Replace(49, UvData.ClubDues.ToString());//CLUB_DUES
						UvRec.Replace(50, UvData.Contributions.ToString());//CONTRIBUTIONS
						UvRec.Replace(51, UvData.Recreation.ToString());//RECREATION
						UvRec.Replace(52, UvData.Gifts.ToString());//GIFTS
						UvRec.Replace(53, UvData.ValSavings.ToString());//SAVINGS
						var Income = UvData.GrossIncome + UvData.PartTimeGrossIncome + UvData.AlimonyIncome;
						Income += UvData.ChildSupportIncome + UvData.GovtAssistIncome + UvData.OtherIncome;
						UvRec.Replace(54, Income.ToString());//MONTHLY_GROSS_INCOME
						Income = UvData.NetIncome + UvData.PartTimeNetIncome;
						UvRec.Replace(55, Income.ToString());//MONTHLY_NET_INCOME
						UvRec.Replace(56, UvData.SizeOfHousehold.ToString());//SIZE_OF_HOUSEHOLD
						UvRec.Replace(57, UvData.ValOther.ToString());//VAL_OTHER
						UvRec.Replace(58, UvData.OweHome.ToString());//OWE_HOME
						UvRec.Replace(59, UvData.DmpFcoPcpFlag);//DMP_FCO_PCP_FLAG
						UvRec.Replace(60, UvData.DmpFcoPcpDate);//DMP_FCO_PCP_DATE
						UvRec.Replace(61, UvData.ContactReason);//CONTACT_REASON
						UvRec.Replace(62, UvData.PriHispanic);//CONTACT_HISPANIC
						UvRec.Replace(63, UvData.PriRace);//CONTACT_RACE
						UvRec.Replace(64, UvData.Bankrupt);//BANKRUPT
						UvRec.Replace(65, UvData.MortIntrestRate.ToString());//MORT_RATE
						var HousingType = UvData.HousingType;
						if(UvData.ClientType == "PrP")
							HousingType = "B";
						UvRec.Replace(68, HousingType);//HOUSING_TYPE
						if(UvData.ClientType == "BK")
							UvRec.Replace(69, "y");//
						UvRec.Replace(70, UvData.CredRptPull);//CRED_RPT_PULL
						UvRec.Replace(71, UvData.PriBirthCity);//CRED_RPT_BCITY
						UvRec.Replace(72, UvData.FirmCode);//FIRM_ID
						UvRec.Replace(73, UvData.FirmName);//FIRM_NAME
						UvRec.Replace(74, UvData.AttyName);//ATTY_NAME
						UvRec.Replace(75, UvData.BankAbaNo);//ABANUMBER
						UvRec.Replace(76, UvData.BankAcctNo);//ACCTNUMBER
						UvRec.Replace(77, UvData.eMail);//REAL_EMAIL
						UvRec.Replace(78, UvData.PartTimeGrossIncome.ToString());//MONTHLY_PARTTIME_INCOME
						UvRec.Replace(79, UvData.AlimonyIncome.ToString());//MONTHLY_ALIMONY_INCOME
						UvRec.Replace(80, UvData.ChildSupportIncome.ToString());//MONTHLY_CHILDSUPPORT_INCOME
						UvRec.Replace(81, UvData.GovtAssistIncome.ToString());//MONTHLY_GOVTASSIST_INCOME
						UvRec.Replace(82, UvData.ContactComments);//CONTACT_COMMENTS
						UvRec.Replace(83, UvData.CreditorComments);//CREDITOR_COMMENTS
						UvRec.Replace(84, UvData.PaymentType);//PAYTYPE
						UvRec.Replace(85, UvData.DebitCardFirstName);//DC_FNAMEONCARD
						UvRec.Replace(86, UvData.DebitCardMidName);//DC_MNAMEONCARD
						UvRec.Replace(87, UvData.DebitCardLastName);//DC_LNAMEONCARD
						UvRec.Replace(88, UvData.DebitCardBillAddr1);//DC_BILLADDR
						UvRec.Replace(89, UvData.DebitCardBillCity);//DC_BILLCITY
						UvRec.Replace(90, UvData.DebitCardBillState);//DC_BILLSTATE
						UvRec.Replace(91, UvData.DebitCardBillZip);//DC_BILLZIP
						UvRec.Replace(92, UvData.DebitCardAcctNo);//DC_ACCTNUM
						UvRec.Replace(93, UvData.DebitCardExpDate);//DC_EXPDATE
						UvRec.Replace(94, UvData.MTCN);//MTCN
						UvRec.Replace(95, UvData.MortHolder);//MORT_HOLDER
						UvRec.Replace(96, UvData.MortLoanNo);//LOAN_NUMBER
						UvRec.Replace(97, UvData.MortType);//MORT_TYPE
						UvRec.Replace(98, UvData.MortRateType);//RATE_TYPE
						UvRec.Replace(99, UvData.MortDate);//MORT_DATE
						UvRec.Replace(100, UvData.MortOrignalBal.ToString());//ORIG_BAL
						UvRec.Replace(102, UvData.MosDelinq.ToString());//MOS_DELINQ
						UvRec.Replace(104, UvData.MortAmtAvail.ToString());//AMT_AVAIL
						UvRec.Replace(105, UvData.RepayPlan);//REPAY_PLAN
						UvRec.Replace(106, UvData.ForeclosureDate);//FORECLOSURE_DATE
						UvRec.Replace(107, UvData.MortLastContactDate);//LAST_CONTACT_DATE
						UvRec.Replace(108, UvData.MortSecHolder);//SECONDARY_HOLDER
						UvRec.Replace(109, UvData.MortSecAmt.ToString());//SECONDARY_AMT
						UvRec.Replace(110, UvData.MortSecStatus);//SECONDARY_STATUS
						UvRec.Replace(111, "Y");//TR.FLAG
						UvRec.Replace(112, "intcouns");//COUNSELOR
						UvRec.Replace(113, UvData.ConfirmDateTime.ToString());//CONFIRM_DATE
						UvRec.Replace(114, UvData.MortLastContactDesc);//LAST_CONTACT_DESC
						UvRec.Replace(115, UvData.MortYears.ToString());//MORT_YEARS
						if(UvData.SpanishFlag.ToUpper() == "Y")
							UvRec.Replace(116, "S");//LANGUAGE
						UvRec.Replace(117, UvData.RefCode);//REFCODE
						UvRec.Replace(118, UvData.FirmEmail);//ATTY_EMAIL
						UvRec.Replace(119, UvData.Addr2);//CONTACT_ADDRESS2
						UvRec.Replace(120, UvData.UserName);//CLIENT_LOGIN
						UvRec.Replace(121, DateTime.Now.ToString());//PUSH_DATETIME
						if((UvData.AttySit == "NoAt" || UvData.AttySit == "Self") && UvData.BsrpStAlowed)
							UvRec.Replace(122, "Y");//SENDREF
						if(UvData.DebitCardTransDTS != null)
							UvRec.Replace(123, UvData.DebitCardTransDTS.ToString());//PAYDATE
						UvRec.Replace(124, UvData.UserName);//WEB_LOGIN
						UvRec.Replace(126, UvData.DebitCardVanTransRef);//CONFIRMATION_NO
						UvRec.Replace(127, UvData.CoCounsPres);//CO_COUNSEL
						UvRec.Replace(128, UvData.PriorCouns);//PRI_COUNSEL
						UvRec.Replace(129, UvData.PriorDMP);//PRIOR_DMP
						UvRec.Replace(130, UvData.RecentDMP);//RECENT_DMP
						UvRec.Replace(131, UvData.PriEducation);//EDUCATION_LEVEL
						UvRec.Replace(132, UvData.PriEmployed);//CL_EMPLOYED
						UvRec.Replace(133, UvData.SecEmployed);//SP_EMPLOYED
						UvRec.Replace(134, UvData.ContactReason2);//CONTACT_REASON2
						UvRec.Replace(135, UvData.MedExp);//MED_EXP_PROB
						UvRec.Replace(136, UvData.LostSO);//SO_LOSS_PROB
						UvRec.Replace(137, UvData.HealthWage);//HEALTH_PROB
						UvRec.Replace(138, UvData.Unemployed);//UNEMPLOY_PROB
						UvRec.Replace(139, UvData.TaxLien);//TAX_PROB
						UvRec.Replace(140, UvData.MortRate);//INTRATE_PROB
						UvRec.Replace(141, UvData.CcrcAprov);//CCRC_FLAG
						if(UvData.SignUp == 1)
							UvRec.Replace(142, "A");//OPT.STAT
						if(UvData.SignUpDTS != null)
							UvRec.Replace(143, UvData.SignUpDTS.ToString());//OPT.DATE
						if(UvData.JointFile == 1)
						{
							UvRec.Replace(144, "J");//FILE.STAT
							if(UvData.GotSec1040DTS != null)
								UvRec.Replace(155, UvData.GotSec1040DTS.ToString());//SP.FORM1
							if(UvData.GotSecPayStubDTS != null)
								UvRec.Replace(156, UvData.GotSecPayStubDTS.ToString());//SP.FORM2
							if(UvData.GotScheduleIDTS != null)
								UvRec.Replace(157, UvData.GotScheduleIDTS.ToString());//SP.FORM3
							if(UvData.GotSsdiDTS != null)
								UvRec.Replace(158, UvData.GotSsdiDTS.ToString());//SP.FORM4
							if(UvData.GotProBonoDTS != null)
								UvRec.Replace(159, UvData.GotProBonoDTS.ToString());//SP.FORM5
							if(UvData.FeePolicyDTS != null)
							{
								UvRec.Replace(160, UvData.FeePolicyDTS.ToString());//ACK1.DATE
								UvRec.Replace(161, UvData.FeePolicyDTS.ToString());//ACK1.DATE
							}
						}
						UvRec.Replace(145, UvData.WaiverType);//TYPE
						if(UvData.Aproved == 1)
							UvRec.Replace(146, "E");//APPR.STAT
						else
							UvRec.Replace(146, "N");//APPR.STAT
						if(UvData.DecisionDTS != null)
							UvRec.Replace(147, UvData.DecisionDTS.ToString());//APPR.DATE
						if(UvData.GotPri1040DTS != null)
							UvRec.Replace(148, UvData.GotPri1040DTS.ToString());//CL.FORM1
						if(UvData.GotPriPayStubDTS != null)
							UvRec.Replace(149, UvData.GotPriPayStubDTS.ToString());//CL.FORM2
						if(UvData.GotScheduleIDTS != null)
							UvRec.Replace(150, UvData.GotScheduleIDTS.ToString());//CL.FORM3
						if(UvData.GotSsdiDTS != null)
							UvRec.Replace(151, UvData.GotSsdiDTS.ToString());//CL.FORM4
						if(UvData.GotProBonoDTS != null)
							UvRec.Replace(152, UvData.GotProBonoDTS.ToString());//CL.FORM5
						if(UvData.FeePolicyDTS != null)
						{
							UvRec.Replace(153, UvData.FeePolicyDTS.ToString());//ACK1.DATE
							UvRec.Replace(154, UvData.FeePolicyDTS.ToString());//ACK1.DATE
						}
						UvRec.Replace(163, UvData.CardPayments.ToString());//CCMIN.PMT
						UvRec.Replace(164, UvData.OtherLoans.ToString());//INSTALL.AMT
						UvRec.Replace(165, UvData.Prop4Sale);//PROP4SALE
						UvRec.Replace(166, UvData.Note4Close);//4CLOSEIND
						UvRec.Replace(167, "08");//REFSOURCE
						string Contat30 = "N";
						DateTime ContactDate;
						if(DateTime.TryParse(UvData.MortLastContactDate, out ContactDate))
							if(DateTime.Compare(UvData.ConfirmDateTime, ContactDate) <= 30)
								Contat30 = "Y";
						UvRec.Replace(168, Contat30);//30DAYCONTACT
						UvRec.Replace(169, UvData.WhoInHouse);//PROPSTATUS
						UvRec.Replace(170, UvData.MortSecLoanNo);//LOAN_NUMBER2
						UvRec.Replace(171, UvData.ValRetirement.ToString());//RET_AMT
						UvRec.Replace(172, UvData.ValProperty.ToString());//PERSPROP_AMT
						if(UvData.BkForPrv == 1)
							UvRec.Replace(173, "Y");//BKFOR.PRV
						else
							UvRec.Replace(173, "N");//BKFOR.PRV
						if(UvData.HpfID != null)
							UvRec.Replace(174, UvData.HpfID.ToString());//FCID
						UvRec.Replace(175, UvData.PartTimeNetIncome.ToString());//MONTHLY_PARTTIME_NET
						UvRec.Replace(176, UvData.UtlTrash.ToString());//UTRASH
						UvRec.Replace(177, UvData.UtlTv.ToString());//UTV
						UvRec.Replace(178, UvData.UtlGas.ToString());//UGAS
						UvRec.Replace(179, UvData.Telephone.ToString());//UPHONE
						UvRec.Replace(180, UvData.UtlWater.ToString());//UWATER
						UvRec.Replace(181, UvData.UtlElectric.ToString());//UELECT
						UvRec.Replace(182, UvData.MoFee.ToString());//PROP.DUES
						UvRec.Replace(183, UvData.IncludeFha);//PMI.INC
						UvRec.Replace(184, UvData.AnPropIns.ToString());//PROP.INSURE
						UvRec.Replace(185, UvData.IncludeIns.ToString());//INS.INC
						UvRec.Replace(186, UvData.AnPropTax.ToString());//PROP.TAXES
						UvRec.Replace(187, UvData.IncludeTax);//TAX.INC
						UvRec.Replace(188, UvData.StartIntRate.ToString());//ORIG.MRATE
						UvRec.Replace(189, UvData.TopIntRate.ToString());//MAX.ADJ
						UvRec.Replace(190, UvData.AstPay.ToString());//DP.ASSIST
						UvRec.Replace(191, UvData.MoEquity.ToString());//EQLOAN_AMT
						UvRec.Replace(192, UvData.Mo2ndMort.ToString());//2ND_AMT
						UvRec.Replace(193, UvData.MakeHomeAff);//HAMP_HARP
                      

						if(UvData.ConThem.ToLower() == "true")
						{
							int Days;
							if(int.TryParse(UvData.ConDay, out Days))
							{
								//Please Contact in the " & Trim(results("ConTime")) & " on " & DateAdd("d",Trim(results("ConDay")),DateValue(results("confirm_datetime"))) & " at " & Trim(results("ConInfo")) & "
								string ConMe = "Please Contact in the {0} on {1} at {2}";
								UvRec.Replace(194, String.Format(ConMe, UvData.ConTime, UvData.ConfirmDateTime.AddDays(Days), UvData.ConInfo));//CONTACT.ME
							}
						}

						UvRec2.Replace(1, UvData.ClientType);//CLIENT.TYPE
						if(UvData.ClientType == "PrP")
						{
							UvRec2.Replace(2, UvData.NewUtlTrash.ToString());//NEWUTLTRASH
							UvRec2.Replace(3, UvData.NewUtlTv.ToString());//NEWUTLTV
							UvRec2.Replace(4, UvData.NewUtlGas.ToString());//NEWUTLGAS
							UvRec2.Replace(5, UvData.NewUtlWater.ToString());//NEWUTLWATER
							UvRec2.Replace(6, UvData.NewUtlElectric.ToString());//NEWUTLELECTRIC
							UvRec2.Replace(7, UvData.NewMoFee.ToString());//NEWMOFEE
							UvRec2.Replace(8, UvData.NewMoPropIns.ToString());//NEWMOPROPINS
							UvRec2.Replace(9, UvData.NewMoPropTax.ToString());//NEWMOPROPTAX
							UvRec2.Replace(10, UvData.NewMo2ndMort.ToString());//NEWMO2NDMORT
							UvRec2.Replace(11, UvData.NewMoEquity.ToString());//NEWMOEQUITY
							UvRec2.Replace(12, UvData.NewCardPayments.ToString());//NEWCARDPAYMENTS
							UvRec2.Replace(13, UvData.NewOtherLoans.ToString());//NEWOTHERLOANS
							UvRec2.Replace(14, UvData.NewMiscExp.ToString());//NEWMISCEXP
							UvRec2.Replace(15, UvData.NewGifts.ToString());//NEWGIFTS
							UvRec2.Replace(16, UvData.NewContributions.ToString());//NEWCONTRIBUTIONS
							UvRec2.Replace(17, UvData.NewPublicTrans.ToString());//NEWPUBLICTRANS
							UvRec2.Replace(18, UvData.NewClubDues.ToString());//NEWCLUBDUES
							UvRec2.Replace(19, UvData.NewCarMaint.ToString());//NEWCARMAINT
							UvRec2.Replace(20, UvData.NewCarIns.ToString());//NEWCARINS
							UvRec2.Replace(21, UvData.NewVehiclePay.ToString());//NEWVEHICLEPAY
							UvRec2.Replace(22, UvData.NewBeauty.ToString());//NEWBEAUTY
							UvRec2.Replace(23, UvData.PersonalExpenses.ToString());//NEWPERDONALEXP
							UvRec2.Replace(24, UvData.NewElderCare.ToString());//NEWELDERCARE
							UvRec2.Replace(25, UvData.NewClothing.ToString());//NEWCLOTHING
							UvRec2.Replace(26, UvData.NewLaundry.ToString());//NEWLAUNDRY
							UvRec2.Replace(28, UvData.NewChildSupport.ToString());//NEWCHILDSUPPORT
							UvRec2.Replace(29, UvData.NewEdu.ToString());//NEWEDU
							UvRec2.Replace(30, UvData.NewMed.ToString());//NEWMED
							UvRec2.Replace(31, UvData.NewRecreation.ToString());//NEWRECREATION
							UvRec2.Replace(32, UvData.NewInsurance.ToString());//NEWINSURANCE
							UvRec2.Replace(33, UvData.NewFoodAway.ToString());//NEWFOODAWAY
							UvRec2.Replace(34, UvData.NewGroceries.ToString());//NEWGROCERIES
							UvRec2.Replace(35, UvData.NewPhone.ToString());//NEWPHONE
							UvRec2.Replace(36, UvData.NewMaint.ToString());//NEWMAINT
							UvRec2.Replace(37, UvData.NewMort.ToString());//NEWMORT
							UvRec2.Replace(38, UvData.HouseAdr);//HOUSEADR
							UvRec2.Replace(39, UvData.HouseCity);//HOUSECITY
							UvRec2.Replace(40, UvData.HouseSt);//HOUSEST
							UvRec2.Replace(41, UvData.HouseZip);//HOUSEZIP
							UvRec2.Replace(42, UvData.HousePrice.ToString());//HOUSEPRICE
							UvRec2.Replace(43, UvData.FndHouse);//FNDHOUSE
							UvRec2.Replace(44, UvData.HadHouse);//HADHOUSE
                           

							if(UvData.TheCloDate != null && UvData.HaveCloDate)
							{
								UvRec2.Replace(45, UvData.TheCloDate.ToString());//THECLODATE
								UvRec2.Replace(46, "Y");//HAVECLODATE
							}
							else
							{
								UvRec2.Replace(46, "N");//HAVECLODATE
							}
							if(UvData.BenAprov)
								UvRec2.Replace(47, "Y");//BENAPROV
							else
								UvRec2.Replace(47, "N");//BENAPROV
							if(UvData.AstPay)
								UvRec2.Replace(48, "Y");//ASTPAY
							else
								UvRec2.Replace(48, "N");//ASTPAY
							if(UvData.BegPro)
								UvRec2.Replace(49, "Y");//BEGPRO
							else
								UvRec2.Replace(49, "N");//BEGPRO
							UvRec2.Replace(50, UvData.RefEmail);//REFEMAIL
							UvRec2.Replace(51, UvData.RefName);//REFNAME
							UvRec2.Replace(52, UvData.RefInst);//REFINST
							UvRec.Replace(246, UvData.MoPropIns.ToString());//MOPROPINS
							UvRec.Replace(247, UvData.MoPropTax.ToString());//MOPROPTAX
						}
						UvRec2.Replace(54, UvData.IdmAccntNo.ToString());//ACCTID
						UvRec2.Replace(55, UvData.PriCertNo);//CL_CERTNO
						UvRec2.Replace(56, UvData.SecCertNo);//SP_CERTNO
						UvRec2.Replace(57, UvData.NetIncome2.ToString());//MONTHLY_NET_INCOME2
						UvRec2.Replace(58, UvData.GrossIncome2.ToString());//MONTHLY_GROSS_INCOME2
						UvRec2.Replace(59, UvData.PartTimeGrossIncome2.ToString());//MONTHLY_PARTTIME_INCOME2
						UvRec2.Replace(60, UvData.AlimonyIncome2.ToString());//MONTHLY_ALIMONY_INCOME2
						UvRec2.Replace(61, UvData.ChildSupportIncome2.ToString());//MONTHLY_CHILDSUPPORT_INCOME2
						UvRec2.Replace(62, UvData.GovtAssistIncome2.ToString());//MONTHLY_GOVTASSIST_INCOME2
                       
                        UvRec2.Replace(66, UvData.AssistModify.ToString());//ASSISTANCE
                        UvRec2.Replace(67, UvData.LoanModify.ToString());//GUARANTEE

                        UvRec2.Replace(68, UvData.BKCounseling_BillAmount.ToString());//BKCOUNSELING_BILLAMOUNT

                        // Hamp /Harp questions

                        UvRec2.Replace(69, UvData.MortgageModifiedHamp.ToString());//1.	Has your mortgage been previously modified under HAMP or have you ever been on a trial modification?
                        UvRec2.Replace(70, UvData.MortgageInvestor.ToString());//Mortgage Investor
                        UvRec2.Replace(71, UvData.IsBusinessOrPersonalMortgage.ToString());//Is this a business or a personal mortgage?                       
                        UvRec2.Replace(72, UvData.HouseType.ToString());// House type e.g. family or multi residence.
                        UvRec2.Replace(73, UvData.AgreeEscrowAccount.ToString());//Have you agreed to set-up an escrow account, if one does not exist?
                        UvRec2.Replace(74, UvData.ConvictedFelony.ToString());//convicted felony
                        UvRec2.Replace(75, UvData.IsLoanFirstLienFMorFMae.ToString());//Is the loan a first lien, conventional mortgage backed by Freddie Mac or Fannie Mae?                   
                        UvRec2.Replace(76, UvData.LoanSoldFMorFmae.ToString());//Was the loan sold to Freddie Mac or Fannie Mae on or before May 31, 2009?
                        UvRec2.Replace(77, UvData.LoanRefinanceProgram.ToString());//Was the loan refinanced under Home Affordable Refinance Program previously?
                        UvRec2.Replace(78, UvData.AnyLiens.ToString());//Are there any liens on the property? 
                        UvRec2.Replace(81, UvData.ActiveBankruptcy.ToString());//Have foreclosure proceedings begun or are you active in bankruptcy

						if(UvData.CounselorId.IsNullOrWhiteSpace())
							UvData.CounselorId = "IA";
						UvRec2.Replace(63, UvData.CounselorId);//COUNSELOR_ID

						UvConDet2.Write(UvData.InNo.ToString(), UvRec2);
						UvConDet.Write(UvData.InNo.ToString(), UvRec);
						Back.IsSuccessful = true;
						UvRec.Dispose();
						UvRec2.Dispose();
					}
				}
			}
			catch(Exception exception)
			{
				Back.Exception = exception;
				Back.IsSuccessful = false;
			}
			return Back;
		}

        private static int DeleteAllEntries(int InternetNumber, UniFile CredDet)
        {
            int i = 0;
            bool Done = false;
            string RecId;
            while (!Done)
            {

                //yuck!  delete the record if it already exists...
                try
                {
                    RecId = String.Format("{0}*{1}", InternetNumber, (++i).ToString());
                    UniDynArray DeleteMe = CredDet.Read(RecId);
                    CredDet.DeleteRecord(RecId);
                    DeleteMe.Dispose();
                }
                catch
                {
                    Done = true;
                }
            }
            return i;
        }

        public Result<string> SavePayrollDeductions(int InternetNumber, List<PayrollDeduction> Deductions) {

            Result<string> Back = new Result<string>();
            
            try
            {
                using (UniSession UvSess = OpenUniSession())
                {
                    using (UniFile CredDet = UvSess.CreateUniFile("SQL_PAYROLLDEDUCT"))
                    {
                        DeleteAllEntries(InternetNumber, CredDet);

                        string RecId;

                        for (int i = 0, l = Deductions.Count; i < l; i++) {
                            RecId = String.Format("{0}*{1}", InternetNumber, (i + 1).ToString());
                            
                            UniDynArray CredRec = UvSess.CreateUniDynArray();
                            CredRec.Insert(1, Deductions[i].Description);//Description
                            CredRec.Insert(2, Deductions[i].Amount.ToString());//Amount

                            CredDet.Write(RecId, CredRec);
                            CredRec.Dispose();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Back.Exception = exception;
                Back.IsSuccessful = false;
            }
            
            return Back;
        }

        public Result<List<PayrollDeduction>> ReadPayrollDeductions(int InternetNumber)
        {
            Result<List<PayrollDeduction>> Back = new Result<List<PayrollDeduction>>();
            Back.Value = new List<PayrollDeduction>();

            try
            {
                using (UniSession UvSess = OpenUniSession())
                {
                    using (UniFile CredDet = UvSess.CreateUniFile("SQL_PAYROLLDEDUCT"))
                    {

                        int i = 0;
                        bool Done = false;
                        string RecId;
                        while (!Done)
                        {
                            try
                            {
                                RecId = String.Format("{0}*{1}", InternetNumber, (++i).ToString());
                                UniDynArray record = CredDet.Read(RecId);
                                var description = record.Extract(1).StringValue;
                                var amount = record.Extract(2).StringValue;
                                decimal dAmount;
                                Decimal.TryParse(amount, out dAmount);

                                Back.Value.Add(new PayrollDeduction { Description = description, Amount = dAmount });

                                record.Dispose();
                            }
                            catch
                            {
                                Done = true;
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                Back.Exception = exception;
                Back.IsSuccessful = false;
            }

            return Back;
        }
    }
}