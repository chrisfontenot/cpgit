﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cccs.Identity;

namespace Cccs.Host.Test
{
	class App
	{
		private static IIdentity m_identity = new Cccs.Identity.Service.Client.Identity();
		private static IHost m_host = new Cccs.Host.Impl.Host(m_identity, "CcCsDev_Key", "ie3(2SeF647&d", "P630", "uvodbc", "bunny1", "WEB2CORE"); 

		public static IHost Host
		{
			get { return m_host; }
		}
	}
}
