﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;

namespace Cccs.Host.Test
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class HostTests
	{
		public HostTests()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion


		const int FIRM_ID = 1111;
		const int IN_NO = 74001;
		const int VAN_TRAN_REF = 4941127;
		const string INVOICE_NO = "1";
		const float CHAR_AMT = 1.0f;
		const int STAT = 1;
		const string LOAN_NUMBER = "600774262";

		 
		[TestMethod]
		public void HostTestMim_CreateLogin_ValidateData()
		{
			MimCreateLoginValidateDataResult result1 = App.Host.Mim_CreateLogin_ValidateData("581686519");
			MimCreateLoginValidateDataResult result2 = App.Host.Mim_CreateLogin_ValidateData("240112817");
		}


		[TestMethod]
		public void HostTestMim_LoginClient()
		{
			MimLoginClientResult result1 = App.Host.Mim_LoginClient("169453");
			MimLoginClientResult result2 = App.Host.Mim_LoginClient("169445");
		}

		[TestMethod]
		public void HostTestBch_HomeSaver_SaveData()
		{
			Result result = App.Host.Bch_HomeSaver_SaveData("Good", "HelpPhone", "Evenings", LOAN_NUMBER, "Randall", "Miller", "123 Main", "Suite 16", "Atlanta", "GA", "30350", "123-123-1234", "randall@email.com");
		}

		[TestMethod]
		public void HostTestUpdateOCS()
		{
			Result<XmlDocument> result = App.Host.UpdateOCS(IN_NO, STAT);
		}

		[TestMethod]
		public void HostTestHaveCredRpt()
		{
			Result<XmlDocument> result = App.Host.HaveCredRpt(IN_NO);
		}

		[TestMethod]
		public void HostTestGetNextSeqNo()
		{
			Result<XmlDocument> result = App.Host.GetNextSeqNo(IN_NO);
		}

		[TestMethod]
		public void HostTestSaveTrans()
		{
			Result<XmlDocument> result = App.Host.SaveTrans(IN_NO, VAN_TRAN_REF, INVOICE_NO, CHAR_AMT);
		}

		[TestMethod]
		public void HostTestGetEscrow()
		{
			Result<XmlDocument> result = App.Host.GetEscrow(FIRM_ID);
		}

		[TestMethod]
		public void HostTestReqCredRpt()
		{
			Result<XmlDocument> result = App.Host.ReqCredRpt(IN_NO, IN_NO);
		}

		[TestMethod]
		public void HostTestGetCredScore()
		{
			Result<XmlDocument> result = App.Host.GetCredScore(IN_NO);
		}

		[TestMethod]
		public void HostTestGetCharAmt()
		{
			Result<double> result = App.Host.GetCharAmt();
		}

		[TestMethod]
		public void HostTestGetCredRpt()
		{
			Result result = App.Host.GetCredRpt(IN_NO);
		}

		[TestMethod]
		public void HostTestLoadDmp()
		{
			Result<XmlDocument> result = App.Host.LoadDmp(IN_NO);
		}

	}
}
