﻿using System;
using System.Collections.Generic;

namespace Cccs.Host.Dto
{
	public class UvPushData
	{
		public bool PushClient { get; set; }

		public int InNo { get; set; }
		public DateTime ConfirmDateTime { get; set; }
		public int Processed { get; set; }
		public string Addr1 { get; set; }
		public string Addr2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone1 { get; set; }
		public string Phone2 { get; set; }
		public string eMail { get; set; }

		public string PriFirstName { get; set; }
		public string PriMidName { get; set; }
		public string PriLastName { get; set; }
		public string PriSsn { get; set; }
		public string PriConfirmMaiden { get; set; }
		public string PriMarital { get; set; }
		public string PriDob { get; set; }
		public string PriSex { get; set; }
		public string PriRace { get; set; }
		public string PriHispanic { get; set; }
		public string PriBirthCity { get; set; }
		public string PriEmployed { get; set; }
		public string PriEducation { get; set; }
		public string PriCertNo { get; set; }

		public string SecFirstName { get; set; }
		public string SecMidName { get; set; }
		public string SecLastName { get; set; }
		public string SecSsn { get; set; }
		public string SecConfirmMaiden { get; set; }
		public string SecMarital { get; set; }
		public string SecDob { get; set; }
		public string SecSex { get; set; }
		public string SecRace { get; set; }
		public string SecHispanic { get; set; }
		public string SecBirthCity { get; set; }
		public string SecEmployed { get; set; }
		public string SecCertNo { get; set; }

		//Payment
		public string PaymentType { get; set; }
		public string DebitCardFirstName { get; set; }
		public string DebitCardMidName { get; set; }
		public string DebitCardLastName { get; set; }
		public string DebitCardBillAddr1 { get; set; }
		public string DebitCardBillAddr2 { get; set; }
		public string DebitCardBillCity { get; set; }
		public string DebitCardBillState { get; set; }
		public string DebitCardBillZip { get; set; }
		public string DebitCardAcctNo { get; set; }
		public string DebitCardExpDate { get; set; }
		public DateTime? DebitCardTransDTS { get; set; }
		public string DebitCardVanTransRef { get; set; }
		public string BankAbaNo { get; set; }
		public string BankAcctNo { get; set; }
		public string MTCN { get; set; }

		//Net Worth
		public float ValHome { get; set; }
		public float ValCar { get; set; }
		public float ValProperty { get; set; }
		public float ValRetirement { get; set; }
		public float ValSavings { get; set; }
		public float ValOther { get; set; }
		public float OweHome { get; set; }
		public float OweCar { get; set; }
		public float OweOther { get; set; }

		//Monthly Food & Shelter
		public decimal RentMort { get; set; }

		public decimal EquityLoanTaxIns { get; set; }
		public float MoEquity { get; set; }
		public float Mo2ndMort { get; set; }

		public float AnPropIns { get; set; }
		public float MoPropIns { get { return AnPropIns / 12; } }
		public float AnPropTax { get; set; }
		public float MoPropTax { get { return AnPropTax / 12; } }
		public float MoFee { get; set; }
		public decimal HomeMaintenance { get; set; }

		public decimal Utilities { get; set; }// kill this one???????

		public float UtlElectric { get; set; }
		public float UtlWater { get; set; }
		public float UtlGas { get; set; }
		public float UtlTv { get; set; }
		public float UtlTrash { get; set; }
		public decimal Telephone { get; set; }
		public decimal FoodAway { get; set; }
		public decimal Groceries { get; set; }

		//Monthly Transportation
		public decimal CarPayments { get; set; }
		public decimal CarInsurance { get; set; }
		public decimal CarMaintenance { get; set; }
		public decimal PublicTransportation { get; set; }

		//Monthly Insurance, Medical & Childcare
		public decimal MedicalInsurance { get; set; }
		public decimal MedicalPrescription { get; set; }
		public decimal ChildSupportAlimony { get; set; }
		public decimal ChildElderCare { get; set; }
		public decimal Education { get; set; }

		//Monthly Personal & Miscellaneous Expenses
		public int CardPayments { get; set; }
		public int OtherLoans { get; set; }
		public decimal Contributions { get; set; }
		public decimal Clothing { get; set; }
		public decimal Laundry { get; set; }
		public decimal PersonalExpenses { get; set; }
		public decimal BeautyBarber { get; set; }
		public decimal Recreation { get; set; }
		public decimal ClubDues { get; set; }
		public decimal Gifts { get; set; }
		public decimal Miscellaneous { get; set; }

		//Income
		public float GrossIncome { get; set; }
		public float GrossIncome2 { get; set; }
		public float NetIncome { get; set; }
		public float NetIncome2 { get; set; }
		public float PartTimeGrossIncome { get; set; }
		public float PartTimeGrossIncome2 { get; set; }
		public float PartTimeNetIncome { get; set; }
		public float PartTimeNetIncome2 { get; set; }
		public float AlimonyIncome { get; set; }
		public float AlimonyIncome2 { get; set; }
		public float ChildSupportIncome { get; set; }
		public float ChildSupportIncome2 { get; set; }
		public float GovtAssistIncome { get; set; }
		public float GovtAssistIncome2 { get; set; }
		public float OtherIncome { get; set; }
		public float OtherIncome2 { get; set; }

		//Houshing
		public int? HpfID { get; set; }
		public string HousingType { get; set; }
		public float MosDelinq { get; set; }
		public int? MortCurrent { get; set; }
		public string MortHolder { get; set; }
		public string MortLoanNo { get; set; }
		public float MortIntrestRate { get; set; }
		public string MortType { get; set; }
		public float MortOrignalBal { get; set; }
		public string MortRateType { get; set; }
		public string MortDate { get; set; }
		public float MortYears { get; set; }
		public float MortAmtAvail { get; set; }
		public string MortLastContactDate { get; set; }
		public string MortLastContactDesc { get; set; }
		public string MakeHomeAff { get; set; }
		public string WhoInHouse { get; set; }
		public string Note4Close { get; set; }
		public string Prop4Sale { get; set; }
		public string RepayPlan { get; set; }
		public string IncludeIns { get; set; }
		public string IncludeTax { get; set; }
		public string IncludeFha { get; set; }
		public float StartIntRate { get; set; }
		public float TopIntRate { get; set; }
		public string ForeclosureDate { get; set; }

		public string MortSecHolder { get; set; }
		public string MortSecLoanNo { get; set; }
		public float MortSecAmt { get; set; }
		public string MortSecStatus { get; set; }

		public string RefCode { get; set; }
		public int BkForPrv { get; set; }
		public string SpanishFlag { get; set; }
		public string ClientType { get; set; }
		public string CcrcAprov { get; set; }
		public int SizeOfHousehold { get; set; }
		public string DmpFcoPcpFlag { get; set; }
		public string DmpFcoPcpDate { get; set; }
		public string CredRptPull { get; set; }
		public string ContactReason { get; set; }
		public string ContactReason2 { get; set; }
		public string Bankrupt { get; set; }
		public long IdmAccntNo { get; set; }
		public string SendRef { get; set; }
		public string CoCounsPres { get; set; }
		public string PriorCouns { get; set; }
		public string PriorDMP { get; set; }
		public string RecentDMP { get; set; }
		public string MedExp { get; set; }
		public string LostSO { get; set; }
		public string HealthWage { get; set; }
		public string Unemployed { get; set; }
		public string TaxLien { get; set; }
		public string MortRate { get; set; }
		public string GovGrant { get; set; }
		public string ContactComments { get; set; }
		public string CreditorComments { get; set; }
		public bool BsrpStAlowed { get; set; }
        public string LoanModify { get; set; }
   

        public string BKCounseling_BillAmount { get; set; }
        public string AssistModify { get; set; }

		public string ConThem { get; set; }
		public string ConMeth { get; set; }
		public string ConInfo { get; set; }
		public string ConTime { get; set; }
		public string ConDay { get; set; }

		public string AttySit { get; set; }
		public string FirmCode { get; set; }
		public string FirmName { get; set; }
		public string AttyName { get; set; }
		public string FirmEmail { get; set; }


		//PreHud
		public float NewUtlTv { get; set; }
		public float NewUtlGas { get; set; }
		public float NewUtlWater { get; set; }
		public float NewUtlElectric { get; set; }
		public float NewUtlTrash { get; set; }
		public float NewMoFee { get; set; }
		public float NewMoPropIns { get; set; }
		public float NewMoPropTax { get; set; }
		public float NewMo2ndMort { get; set; }
		public float NewMoEquity { get; set; }
		public float NewCardPayments { get; set; }
		public float NewOtherLoans { get; set; }
		public float NewMiscExp { get; set; }
		public float NewGifts { get; set; }
		public float NewContributions { get; set; }
		public float NewPublicTrans { get; set; }
		public float NewClubDues { get; set; }
		public float NewCarMaint { get; set; }
		public float NewCarIns { get; set; }
		public float NewVehiclePay { get; set; }
		public float NewBeauty { get; set; }
		public float NewPerdonalExp { get; set; }
		public float NewElderCare { get; set; }
		public float NewClothing { get; set; }
		public float NewLaundry { get; set; }
		public float NewChildSupport { get; set; }
		public float NewEdu { get; set; }
		public float NewMed { get; set; }
		public float NewRecreation { get; set; }
		public float NewInsurance { get; set; }
		public float NewFoodAway { get; set; }
		public float NewGroceries { get; set; }
		public float NewPhone { get; set; }
		public float NewMaint { get; set; }
		public float NewMort { get; set; }
		public bool BegPro { get; set; }
		public bool AstPay { get; set; }
		public bool BenAprov { get; set; }
		public bool HaveCloDate { get; set; }
		public DateTime? TheCloDate { get; set; }
		public float HousePrice { get; set; }
		public string HouseAdr { get; set; }
		public string HouseCity { get; set; }
		public string HouseSt { get; set; }
		public string HouseZip { get; set; }
		public string FndHouse { get; set; }
		public string HadHouse { get; set; }
		public string RefEmail { get; set; }
		public string RefName { get; set; }
		public string RefInst { get; set; }

		public DateTime? FeePolicyDTS { get; set; }
		public DateTime? DecisionDTS { get; set; }
		public DateTime? SignUpDTS { get; set; }
		public DateTime? GotSsdiDTS { get; set; }
		public DateTime? GotProBonoDTS { get; set; }
		public DateTime? GotPri1040DTS { get; set; }
		public DateTime? GotPriPayStubDTS { get; set; }
		public DateTime? GotSec1040DTS { get; set; }
		public DateTime? GotSecPayStubDTS { get; set; }
		public DateTime? GotScheduleIDTS { get; set; }
		public int SignUp { get; set; }
		public string WaiverType { get; set; }
		public int Aproved { get; set; }
		public int JointFile { get; set; }

		public string CounselorId { get; set; }
		public string UserName { get; set; }


        /////Begin Housing/ Hamp questions
        // Hamp Questions
        public string MortgageModifiedHamp { get; set; }
        public string MortgageInvestor { get; set; }
        public string IsBusinessOrPersonalMortgage { get; set; }
        public string HouseType { get; set; }
        public string AgreeEscrowAccount { get; set; }

        // Hamp 2 tier Questions
        public string ConvictedFelony { get; set; }
        public string IsLoanFirstLienFMorFMae { get; set; }

        // Harp Questions
        public string LoanSoldFMorFmae { get; set; }
        public string LoanRefinanceProgram { get; set; }

        // Short Sale/Deed in Lieu      
        public string AnyLiens { get; set; }

        public string ActiveBankruptcy { get; set; }
        ///////end housing and hamap questions

		public List<UvPushCreditor> Creditors { get; set; }
	}
}