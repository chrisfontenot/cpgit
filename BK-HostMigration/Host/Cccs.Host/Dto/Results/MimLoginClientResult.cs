﻿
namespace Cccs.Host
{
	public class MimLoginClientResult : Result
	{
		public string UvStat { get; set; }
		public string ClId { get; set; }
		public string SpUvId { get; set; }
		public string ContactSsn { get; set; }
		public bool LoginMimOk { get; set; }
        public string ReDirTarget { get; set; }
		public string JointSsn { get; set; }
		public string SecondaryFlag { get; set; }
		public string FirmId { get; set; }
		public string Escrow { get; set; }
		public string Escrow6 { get; set; }
		public string ErrorMsg { get; set; }
        public string ZipCode { get; set; }
	}
}