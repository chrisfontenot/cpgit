﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
	public class MimWaverAppLoadDataResult : Result
	{
		public string ContactFirstName { get; set; }
		public string ContactLastName	{ get; set; }
		public string ContactAddress { get; set; }
		public string ContactAddress2 { get; set; }
		public string ContactCity { get; set; }
		public string ContactState { get; set; }
		public string ContactZip { get; set; }
		public string ContactTelephone { get; set; }
		public string RealEmail { get; set; }
	}
}
