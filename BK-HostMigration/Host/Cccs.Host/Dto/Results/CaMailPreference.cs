﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
    public class CaMailPreferenceResult : Result
    {
        public String Letter{ get; set; }
        public String Statement{ get; set; }
    }
}
