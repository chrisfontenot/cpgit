﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
    public class CaAccountTransactionsResult : Result
    {
        public String LastDepositDate{get;set;}
        public String LastDepositAmount{get;set;}
        public String LastDisbursementDate{get;set;}
        public String LastDisbursementAmount{get;set;}
        public CaActivity[] Transaction{get;set;}
        public String MaxMsg {get;set;} //

    }

    public class CaActivity
    {
        public String Type{get;set;}
        public String Date{get;set;}
        public double Deposit{get;set;}
        public double Payment{get;set;}
    }
}
