﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
	public class MimQuicCalcLoadDataResult : Result
	{
		public string ContactState { get; set; }
		public string SizeOfHousehold { get; set; }
		public string MonthlyGrossIncome { get; set; }
	}
}
