﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
	public class MimCreateLoginValidateDataResult : Result
	{
		public bool IsValid { get; set; }

		public bool IsSsnAlreadyOnFile { get; set; }

		public string JointSsn { get; set; }

		public string SecondaryFlag { get; set; }

		public string SpUvId { get; set; }
	}
}
