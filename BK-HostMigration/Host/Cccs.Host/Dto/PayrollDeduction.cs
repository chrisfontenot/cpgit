﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host.Dto
{
    public class PayrollDeduction
    {
        public String Description { get; set; }
        public decimal Amount { get; set; }
    }
}
