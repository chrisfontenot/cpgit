﻿using System;
using System.Xml;
using Cccs.Host.Dto;
using System.Collections.Generic;

namespace Cccs.Host
{
	public interface IHost
	{
		#region Common

		string GetCounselorName(string counselorId);
		Result<XmlDocument> GetEscrow(int firm_id);
		Result<XmlDocument> ReqCredRpt(long user_id, int client_number);
		Result<XmlDocument> RequestCreditReport(int client_number, string client_type, string ref_code, string primary_birth_city, string secondary_birth_city, string primary_first_name, string primary_last_name, string primary_ssn, DateTime? primary_birth_date, string street_line1, string city, string state, string zip, string secondary_first_name, string secondary_last_name, string secondary_ssn, DateTime? secondary_birth_date);
		Result GetCredRpt(int client_number);
		Result<CreditScoreDto> GetCredScore(int client_number);
		Result<XmlDocument> SaveTrans(int client_number, int van_tran_ref, string invoice_no, float char_amt);
		Result<XmlDocument> GetNextSeqNo(int client_number);
		Result<XmlDocument> HaveCredRpt(int client_number);
		Result<XmlDocument> UpdateOCS(int client_number, int stat);
		Result<XmlDocument> LoadDmp(int client_number);
        //Result<double> GetCharAmt(string AttorneyCode, string ZipCode);
        Result<double> GetCharAmt(string AttorneyCode, string ZipCode, string LineOfService);
		Result<string> GetUvDts();
		Result<string> StageUvData(UvPushData UvData);
        Result<string> SavePayrollDeductions(int InternetNumber, List<PayrollDeduction> Deductions);
        Result<List<PayrollDeduction>> ReadPayrollDeductions(int InternetNumber);

		#endregion

		#region	BCH

		Result Bch_HomeSaver_SaveData(string need_help, string help_type, string contact_time, string loan_number, string first_name, string last_name, string address_line1, string address_line2, string city, string state, string zip, string phone, string email);
		void SendHpfDisclosure(int InNo, string Lang, string eMail, string FirstName, string LastName, string Addr1, string Addr2, string City, string State, string Zip);

		#endregion

		#region BK

		JudicialDistrict GetJudicialDistrict(string zipCode);
		string GetEoustJudicialDistrictByName(string districtName);
		EoustLogin GetEoustLogin();
		string GetFirmEmail(string FirmID);
		bool SetCertNo(CertReIssue ReIssueData);

		#endregion

		#region MIM


		MimLoginClientResult Mim_LoginClient(string reg_num);

		MimCreateLoginValidateDataResult Mim_CreateLogin_ValidateData(string ssn);

		MimDisclosureLoadDataResult Mim_Disclosure_LoadData(string ClNo);

		Result Mim_Disclosure_SaveTransaction(string ClNo, string dc_fnameoncard, string dc_mnameoncard, string dc_lnameoncard, string dc_bill_addr1, string dc_bill_city, string dc_bill_state, string dc_bill_zip, string dc_acctnum, string dc_exp_date, string cur_inv_no, string cur_seq_no, string van_trans_ref, string char_amt);

		MimForm7LoadDataResult Mim_Form7_LoadData(string ClNo, string regnum);

		Result Mim_Form7_SaveData(string ClNo, string contact_lastname, string contact_firstname, string contact_initial, string contact_ssn, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string contact_marital, string contact_sex, string contact_race, string contact_hispanic, string credrpt_birthcity, string cemail, string dayphone, string nitephone, string attyname, string attyemail, string firm_id, string escrow, string secondary_flag, string char_type, string casenumber, string bankruptcy_type, string income_range, string number_in_house, string languageCode, string char_amt);

        MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum);
		Result Mim_Form7_SaveWriteOff(string ClNo, string regnum, string char_amt);

		MimForm7MainLineResult Mim_Form7_MainLine(string contact_state);

		MimSetTransSaveWriteResult Mim_SetTrans_SaveWriteOff(string ClNo, string regnum, string char_amt);

		Result MimCompleteCourse(string ClNo, string prescore, string postscore, string TestDTS);

		MimQuicCalcLoadDataResult Mim_QuicCalc_LoadData(string ClNo, string regnum);

		Result Mim_QuicCalc_SaveData(string ClNo, string contact_state, string monthly_gross_income, string size_of_household);

		Result Mim_WaiverWaiver_SaveUv(string ClNo);

		MimWaverAppLoadDataResult Mim_WaverApp_LoadData(string ClNo, string regnum);
		Result Mim_WaverApp_SaveData(string clid, string contact_firstname, string contact_lastname, string contact_initial, string contact_address, string contact_address2, string contact_city, string contact_state, string contact_zip, string real_email, string contact_telephone);
		string Mim_NewClsID_Get(string reg_num);
		string Mim_HostID_ReturningUser_Get(string reg_num);
		Result MimSaveRegisterAttorney(string ClNo, string CaseNo, string Ssn, string FirmId, string AttnyName, string AttnyEmail, string UserName);
		Result MimSavePreTest(string ClNo, int PreTest);
        Result MimDisclosureSaveMtcn(string ClNo, string mtcn);
		Result MimDisclosureSaveMtcn(string ClNo, string mtcn, string char_amt);
        Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number);
		Result MimDisclosureSaveAch(string ClNo, string bank_name, string name_on_check, string aba_number, string acct_number, string char_amt);
		int GetMimSequenceNumber(string ClNo);
		double MimDisclosureGetChargeAmount(string ClNo);
        double MimDisclosureGetChargeAmount(string ZipCode, string FirmCode);
		Result MimSaveXRef(string PriSsn, string SecSsn);
		#endregion

		#region CA

		CaAccountTransactionsResult CaTrans(String ClientNo, String TransOpt);

		CaCredResult CaCreds(String ClientNo);

		CaMailPreferenceResult CaPerfGet(String ClientNo);

		Result CaPerfSet(String ClientNo, String LetterPref, String StatmentPref);

		Result CaInfoSet(String ClientNo, String HomePhone, String WorkPhone, String Addr1, String Addr2, String City, String State, String Zip, String FirstName, String LastName, String Email, String EmailW);

		Result CaCredBalSet(String AccountID, float NewBal);

		#endregion

	}
}