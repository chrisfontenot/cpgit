﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host
{
    public class CreditScoreDto
    {
        public string PrimaryScore { get; set; }
        public string SecondaryScore { get; set; }
    }
}
