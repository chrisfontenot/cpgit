﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cccs.Host.Dal
{
	public partial class CredabilityHostDataContext : System.Data.Linq.DataContext
	{
		private static string m_db_encryption_key;
		private static string m_db_encryption_password;

		public static void SetEncryption(string db_encryption_key, string db_encryption_password)
		{
			m_db_encryption_key = db_encryption_key;
			m_db_encryption_password = db_encryption_password;
		}

		public void OpenEncryptionKey()
		{
			if (!string.IsNullOrEmpty(m_db_encryption_key) && !string.IsNullOrEmpty(m_db_encryption_password))
			{
				this.ExecuteQuery<string>(string.Format("OPEN SYMMETRIC KEY {0} DECRYPTION BY PASSWORD = '{1}' ", m_db_encryption_key, m_db_encryption_password), new object[] { });
			}
		}
	}
}
