﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentingYourIncome.aspx.cs" Inherits="Cccs.Sandbox.Website.DocumentingYourIncome" MasterPageFile ="~/Master.Master" Title ="CCCS DocumentingYourIncome Profile"%>

    <asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
    </asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

    <script type="text/javascript">
    function addLoadEvent(func) {
        var oldonload = window.onload;
        if (typeof window.onload != 'function') {
            window.onload = func;
        }
        else {
            window.onload = function() {
                if (oldonload) {
                    oldonload();
                }
                func();
            }
        }
    }
    var ReFormat = true, HoldField = "";
    function prepareField(theField) {
        var sValue = theField.value;
        HoldField = sValue;
        sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
        if (sValue <= 0)
            sValue = '';
        theField.value = sValue;
        theField.select();
    }
    function CheckMod(theField) {
        if (HoldField != theField.value && HoldField != "") {
            document.getElementById("ModForm").value = 1;
            HoldField = "";
        }
    }
    function reformatCCCSCurrency(theField) {
        if (ReFormat) {
            var sValue = theField.value, OutTxt = "", Des;
            var NumStr, Len, i;
            sValue = stripCharsNotInBag(sValue, "0123456789.");
            if (sValue == 0) sValue = '0';
            NumStr = sValue - (sValue % 1);
            NumStr = String(NumStr);
            Len = NumStr.length - 1;
            for (i = 0; i <= Len; i++) {
                OutTxt += NumStr.charAt(i);
                if (i < Len && (Len - i) % 3 == 0) //>
                {
                    OutTxt += ",";
                }
            }
            sValue = '$' + OutTxt;
            theField.value = sValue;
            CheckMod(theField);
        }
        ReFormat = true;
    }
    function SetUpCalc(Field) {
        var sValue = Field.value;
        sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
        if (sValue <= 0)
            sValue = '0.00';
        Field.value = sValue;
        ReFormat = false;
        Field.focus()
    }
    function syncFields(Field) {
        var sValue = Field.value;
        var sName = Field.name;
        var myRegExp = /Gross/;
        var matchPos1 = sName.search(myRegExp);
        //check whether it's a 'Gross' field or a "Net' field
        if (matchPos1 != -1) {
            var snewName = sName.replace("Gross", "Net");
        }
        else {
            var snewName = sName.replace("Net", "Gross");
        }
        document.getElementById(snewName).value = sValue;
        reformatCCCSCurrency(document.getElementById(snewName));
    }
    function validateForm(form) {
        var bReturn = true;
        var total_Gross = Number(stripCharsNotInBag(form.elements['PriIncTotalGross'].value, "0123456789."));
        var total_Gross_Sec = Number(stripCharsNotInBag(form.elements['SecIncTotalGross'].value, "0123456789."));
        var total_Net = Number(stripCharsNotInBag(form.elements['PriIncTotalNet'].value, "0123456789."));
        var total_Net_Sec = Number(stripCharsNotInBag(form.elements['SecIncTotalNet'].value, "0123456789."));
        if (total_Gross < 1.15 * total_Net) {
            bReturn = confirm("Your gross income should include the total of all monthly income, including your paycheck before taxes, child support, etc. Your monthly gross income will be greater than your monthly net income.  Do you wish to continue?");
        }
        if (total_Gross_Sec != 0) {
            if (total_Gross_Sec < 1.15 * total_Net_Sec) {
                bReturn = confirm("Your gross income should include the total of all monthly income, including your paycheck before taxes, child support, etc. Your monthly gross income will be greater than your monthly net income.  Do you wish to continue?");
            }
        }
        if ((total_Gross >= 10000) || (total_Net >= 8000) || (total_Gross_Sec >= 10000) || (total_Net_Sec >= 8000)) {
            bReturn = confirm("The income you entered is relatively high.  Do you wish to continue?");
        }
        if ((total_Gross == 0) && (total_Net == 0) && (total_Gross_Sec == 0) && (total_Net_Sec == 0)) {
            bReturn = confirm("Both Gross and Net income that you entered is 0.  Do you wish to continue?");
        }
        else {
            if (total_Gross == 0) {
                bReturn = confirm("The Gross income you entered is 0.  Do you wish to continue?");
            }
            if (total_Net == 0) {
                bReturn = confirm("The Net income you entered is 0.  Do you wish to continue?");
            }
        }
        return bReturn;
    }
    function CalcMonthlyTotals() {
        var monthly_nonwage_total = Number(stripCharsNotInBag(document.getElementById('PriIncAlimonyGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('PriIncGovAstGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('PriIncChildSupportGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('PriIncOtherGross').value, "0123456789."));
        var monthly_nonwage_total2 = Number(stripCharsNotInBag(document.getElementById('SecIncAlimonyGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('SecIncGovAstGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('SecIncChildSupportGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('SecIncOtherGross').value, "0123456789."));

        document.getElementById('PriIncTotalGross').value = Number(stripCharsNotInBag(document.getElementById('PriIncGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('PriIncPtGross').value, "0123456789.")) + parseInt(monthly_nonwage_total);
        reformatCCCSCurrency(document.getElementById('PriIncTotalGross'));

        document.getElementById('SecIncTotalGross').value = Number(stripCharsNotInBag(document.getElementById('SecIncGross').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('SecIncPtGross').value, "0123456789.")) + parseInt(monthly_nonwage_total2);
        reformatCCCSCurrency(document.getElementById('SecIncTotalGross'));

        document.getElementById('SecIncTotalNet').value = Number(stripCharsNotInBag(document.getElementById('SecIncNet').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('SecIncPtNet').value, "0123456789.")) + parseInt(monthly_nonwage_total2);
        reformatCCCSCurrency(document.getElementById('SecIncTotalNet'));

        document.getElementById('PriIncTotalNet').value = Number(stripCharsNotInBag(document.getElementById('PriIncNet').value, "0123456789.")) + Number(stripCharsNotInBag(document.getElementById('PriIncPtNet').value, "0123456789.")) + parseInt(monthly_nonwage_total);
        reformatCCCSCurrency(document.getElementById('PriIncTotalNet'));
    }
    </script>

    <p class="NormTxt">
        As a part of preparing for your reverse mortgage, we will review your overall financial
        situation, including your income, living expenses and other debts. This review is
        designed to help you to confirm that the reverse mortgage will allow you to meet
        your financial goals and also help you to set financial goals for the future.
    </p>
    <p class="NormTxt">
        Accurately identifying all your income is important to helping us identify what
        options you have for handling your living expenses and debt payments.
        <blockquote class="NormTxt">
            <strong>Gross income</strong> is a total of the income you earn before any taxes,
            insurance or retirement plan contributions are excluded. The total gross monthly
            income would also include other sources of income, such as child support, government
            assistance or a part-time job, for example. Gross income will always be greater
            than net income.
            <br>
            <strong>Net income</strong> is the dollar amount you bring home each month from
            your various sources of income. All income figures should reflect your current income,
            not past or potential future earnings.
            <br>
            <strong>No income?</strong> Please enter $1 for Gross and Net income.
            <br>
            <strong>Unemployment or Social Security income?</strong> Please enter under Government
            Assistance.
            <br>
            <strong>Pension income?</strong> Please enter under Other.
            <br>
            <strong>My income varies.</strong> Please enter the average monthly income over
            the last 6-12 months.
            <br>
            <strong>Do I enter my spouse's income?</strong> Please enter the entire household
            income. (We'll ask about the household's expenses later.)
            <br>
            If you receive income from self employment from your business, please only enter
            the income used for the household.
        </blockquote>
        Please enter your information below:
    </p>
    <!-- Begin gross income add form -->
    <table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <!-- sizing control row  -->
            <td width="319" height="0">
            </td>
            <td width="15">
            </td>
            <td width="190">
            </td>
            <td width="190">
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="4" height="25" align="center">
                <table width="100%" cellspacing="3">
                    <tr>
                        <td height="25" class="NormHead" align="center">
                            Gross Household Income
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="4" height="5">
            </td>
        </tr>
        <tr class="GreyBack" valign="bottom">
            <td>
            </td>
            <td>
            </td>
            <td class="NormTxt">
                Primary Person
            </td>
            <td class="NormTxt">
                Secondary Person
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Monthly Gross Income from Primary Employment<br>
                <span class="TnyTxt">(Money you make before taxes and other deductions<br>
                    are subtracted from your paycheck.)</span>
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="1" name="PriIncGross" value="$0" type="text" id="PriIncGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncGross']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['PriIncGross']);">--%>
                <asp:TextBox ID="txtPriIncGross" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncGross'])">
                    <%--<img width="15" height="13" border="0" alt="Click here to see the calculator" src="img/calc.gif">--%><asp:Image
                        ID="imgCal1" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="13" name="SecIncGross" value="$0" type="text" id="SecIncGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncGross']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['SecIncGross']);">--%>
                <asp:TextBox ID="txtSecIncGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Monthly Gross Income from Secondary Employment<br>
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="2" name="PriIncPtGross" value="$0" type="text" id="PriIncPtGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncPtGross']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['PriIncPtGross']);">--%>
                <asp:TextBox ID="txtPriIncPtGross" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncPtGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncPtGross'])">
                    <%--<img width="15" height="13" border="0" alt="Click here to see the calculator" src="img/calc.gif">--%><asp:Image
                        ID="imgCal2" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="14" name="SecIncPtGross" value="$0" type="text" id="SecIncPtGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncPtGross']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['SecIncPtGross']);">--%>
                <asp:TextBox ID="txtSecIncPtGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Alimony Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="3" name="PriIncAlimonyGross" value="$0" type="text" id="PriIncAlimonyGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncAlimonyGross']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['PriIncAlimonyGross'])" onChange="syncFields(form.elements[this.id])" >--%>
                <asp:TextBox ID="txtPriIncAlimonyGross" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncAlimonyGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncAlimonyGross'])">
                    <asp:Image ID="imgCal3" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="15" name="SecIncAlimonyGross" value="$0" type="text" id="SecIncAlimonyGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncAlimonyGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncAlimonyGross']);">--%>
                <asp:TextBox ID="txtSecIncAlimonyGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Child Support Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="4" name="PriIncChildSupportGross" value="$0" type="text" id="PriIncChildSupportGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncChildSupportGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncChildSupportGross']);">--%>
                <asp:TextBox ID="txtPriIncChildSupportGross" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncChildSupportGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncChildSupportGross'])">
                    <asp:Image ID="imgCal4" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="16" name="SecIncChildSupportGross" value="$0" type="text" id="SecIncChildSupportGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncChildSupportGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncChildSupportGross']);">--%>
                <asp:TextBox ID="txtSecIncChildSupportGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Government Assistance Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="5" name="PriIncGovAstGross" title="examples include SSDI, food stamps, unemployment" value="$0" type="text" id="PriIncGovAstGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncGovAstGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncGovAstGross']);">--%>
                <asp:TextBox ID="txtPriIncGovAstGrosstxt" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncGovAstGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncGovAstGross'])">
                    <asp:Image ID="imgCal5" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="17" name="SecIncGovAstGross" title="examples include SSDI, food stamps, unemployment" value="$0" type="text" id="SecIncGovAstGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncGovAstGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncGovAstGross']);">--%>
                <asp:TextBox ID="txtSecIncGovAstGrosstxt" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Other Income
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="6" name="PriIncOtherGross" value="$0" type="text" id="PriIncOtherGross" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncOtherGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncOtherGross']);">--%>
                <asp:TextBox ID="txtPriIncOtherGross" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncOtherGross'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncOtherGross'])">
                    <asp:Image ID="imgCal6" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="18" name="SecIncOtherGross" value="$0" type="text" id="SecIncOtherGross" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncOtherGross']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncOtherGross']);">--%>
                <asp:TextBox ID="txtSecIncOtherGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Total Monthly Gross Income
            </td>
            <td>
            </td>
            <td>
                <%--<input name="PriIncTotalGross" type="text" disabled="true" class="NumBox" id="PriIncTotalGross" style="border: none; background-color: #e0e0e0;" size="14">--%>
                <asp:TextBox ID="txtPriIncTotalGross" runat="server"></asp:TextBox>
            </td>
            <td>
                <%--<input name="SecIncTotalGross" type="text" disabled="true" class="NumBox" id="SecIncTotalGross" style="border: none; background-color: #e0e0e0;" size="14">--%>
                <asp:TextBox ID="txtSecIncTotalGross" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td height="19" colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <!-- Begin net income add form -->
    <table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <!-- sizing control row  -->
            <td width="319" height="0">
            </td>
            <td width="15">
            </td>
            <td width="190">
            </td>
            <td width="190">
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="4" height="25" align="center">
                <table width="100%" cellspacing="3">
                    <tr>
                        <td height="25" class="NormHead" align="center">
                            Net Household Income
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="4" height="5">
            </td>
        </tr>
        <tr class="GreyBack" valign="bottom">
            <td>
            </td>
            <td>
            </td>
            <td class="NormTxt">
                Primary Person
            </td>
            <td class="NormTxt">
                Secondary Person
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Monthly Net Income from Primary Employment<br>
                <span class="TnyTxt">(Money you make after taxes and other deductions<br>
                    are subtracted from your paycheck; take home pay)</span>
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="7" name="PriIncNet" value="$0" type="text" id="PriIncNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncNet']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['PriIncNet']);">--%>
                <asp:TextBox ID="txtPriIncNet" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncNet'])">
                    <asp:Image ID="imgCal7" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="19" name="SecIncNet" value="$0" type="text" id="SecIncNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncNet']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['SecIncNet']);">--%>
                <asp:TextBox ID="txtSecIncNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Monthly Net Income from Secondary Employment<br>
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="8" name="PriIncPtNet" value="$0" type="text" id="PriIncPtNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncPtNet']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['PriIncPtNet']);">--%>
                <asp:TextBox ID="txtPriIncPtNet" runat="server" 
                    ontextchanged="txtPriIncPtNet_TextChanged"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncPtNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncPtNet'])">
                    <asp:Image ID="imgCal8" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="20" name="SecIncPtNet" value="$0" type="text" id="SecIncPtNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncPtNet']); CalcMonthlyTotals();" onFocus="prepareField(form.elements['SecIncPtNet']);">--%>
                <asp:TextBox ID="txtSecIncPtNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Alimony Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="9" name="PriIncAlimonyNet" value="$0" type="text" id="PriIncAlimonyNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncAlimonyNet']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncAlimonyNet']);">--%>
                <asp:TextBox ID="txtPriIncAlimonyNet" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncAlimonyNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncAlimonyNet'])">
                    <asp:Image ID="imgCal9" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="21" name="SecIncAlimonyNet" value="$0" type="text" id="SecIncAlimonyNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncAlimonyNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncAlimonyNet']);">--%>
                <asp:TextBox ID="txtSecIncAlimonyNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Child Support Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="10" name="PriIncChildSupportNet" value="$0" type="text" id="PriIncChildSupportNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncChildSupportNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncChildSupportNet']);">--%>
                <asp:TextBox ID="txtPriIncChildSupportNet" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncChildSupportNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncChildSupportNet'])">
                    <asp:Image ID="imgCal10" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="22" name="SecIncChildSupportNet" value="$0" type="text" id="SecIncChildSupportNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncChildSupportNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncChildSupportNet']);">--%>
                <asp:TextBox ID="txtSecIncChildSupportNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Government Assistance Received
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="11" name="PriIncGovAstNet" title="examples include SSDI, food stamps, unemployment" value="$0" type="text" id="PriIncGovAstNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncGovAstNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncGovAstNet']);">--%>
                <asp:TextBox ID="txtPriIncGovAstNet" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncGovAstNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncGovAstNet'])">
                    <asp:Image ID="imgCal11" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="23" name="SecIncGovAstNet" title="examples include SSDI, food stamps, unemployment" value="$0" type="text" id="SecIncGovAstNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncGovAstNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncGovAstNet']);">--%>
                <asp:TextBox ID="txtSecIncGovAstNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Other Income
            </td>
            <td>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="12" name="PriIncOtherNet" value="$0" type="text" id="PriIncOtherNet" size="14" onBlur="reformatCCCSCurrency (form.elements['PriIncOtherNet']); CalcMonthlyTotals();"  onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['PriIncOtherNet']);">--%>
                <asp:TextBox ID="txtPriIncOtherNet" runat="server"></asp:TextBox>
                <a href="javascript:TCR.TCRPopup(document.forms['form1'].elements['PriIncOtherNet'])"
                    onclick="SetUpCalc(document.forms['form1'].elements['PriIncOtherNet'])">
                    <asp:Image ID="imgCal12" runat="server" ImageUrl="~/images/calc.gif" ToolTip="Click here to see the calculator" /></a>
            </td>
            <td>
                <%--<input class="NumBox" tabindex="24" name="SecIncOtherNet" value="$0" type="text" id="SecIncOtherNet" size="14" onBlur="reformatCCCSCurrency (form.elements['SecIncOtherNet']); CalcMonthlyTotals();" onChange="syncFields(form.elements[this.id])" onFocus="prepareField(form.elements['SecIncOtherNet']);">--%>
                <asp:TextBox ID="txtSecIncOtherNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" id="" class="NormTxt" align="right">
                Total Monthly Net Income
            </td>
            <td>
            </td>
            <td>
                <%--<input name="PriIncTotalNet" type="text" disabled="true" class="NumBox" id="PriIncTotalNet" style="border: none; background-color: #e0e0e0;" size="14">--%>
                <asp:TextBox ID="txtPriIncTotalNet" runat="server"></asp:TextBox>
            </td>
            <td>
                <%--<input name="SecIncTotalNet" type="text" disabled="true" class="NumBox" id="SecIncTotalNet" style="border: none; background-color: #e0e0e0;" size="14">--%>
                <asp:TextBox ID="txtSecIncTotalNet" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td height="19" colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:ImageButton ID="imgBtnDocumentingPrevious" runat="server" 
                    ImageUrl="~/images/btn_OC_previous.gif" 
                    onclick="imgBtnDocumentingPrevious_Click" />
                <asp:ImageButton ID="imgBtnDocumentingContinue" runat="server" 
                    ImageUrl="~/images/btn_OC_Continue.gif" 
                    onclick="imgBtnDocumentingContinue_Click" />
                <asp:ImageButton ID="imgBtnDocumentingReturnToPrevious" runat="server" 
                    ImageUrl="~/images/btn_Continue_Later.gif" 
                    onclick="imgBtnDocumentingReturnToPrevious_Click" />
            </td>
        </tr>
    </table>
</asp:Content> 