﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnderstandingYourNetWorth.aspx.cs" Inherits="Cccs.Sandbox.Website.UnderstandingYourNetWorth" MasterPageFile ="~/Master.Master" Title ="CCCS Understanding Your Net Worth Profile" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<script type="text/javascript" > 
var InviteSpot = false;
var OptionValue = "", CntDn;
var CurFont = GetFontCookie();
function FontChange(Dir)
{
	switch(Dir)
	{
		case '+':
			if(CurFont < 3) //>
			{
				CurFont++;
				SetFontCookie()
			}
			else
			{
				return false;
			}
			break;
			
		case '-':
			if(CurFont > 0)
			{
				CurFont--;
				SetFontCookie()
			}
			else
			{
				return false;
			}
			break;
		default:
			break;
	}
//alert(CurFont);
	if(CurFont == 3)
	{
 
		document.getElementById("FontUp").innerHTML = '<img src="../images/Font+1G.gif" border="0">';
	}
	else
	{
		document.getElementById("FontUp").innerHTML = '<a href="#" onclick="javascript: FontChange(\'+\'); return false;" class="BigLink"><img src="../images/Font+1.gif" border="0"></a>';
	}
	if(CurFont == 0)
	{
		document.getElementById("FontDown").innerHTML = '<img src="../images/Font-1G.gif" border="0">';
	}
	else
	{
		document.getElementById("FontDown").innerHTML = '<a href="#" onclick="javascript: FontChange(\'-\'); return false;" class="BigLink"><img src="../images/Font-1.gif" border="0"></a>';
	}
	for(i = 0; i < 4; i++)//>
	{
		document.getElementById("fontSize"+i).disabled = true;
	}
	document.getElementById("fontSize"+CurFont).disabled = false;
	return false;
}
function SetFontCookie()
{
	var EXdate=new Date();
	EXdate.setDate(EXdate.getDate() + 20)
	document.cookie = "CccsCurFontSize=" + CurFont + ";expires=" + EXdate.toGMTString();
}
function GetFontCookie()
{
	CookieMon = "CccsCurFontSize=";
	if (document.cookie.length > 0)
	{
		Start=document.cookie.indexOf(CookieMon)
		if (Start != -1)
		{ 
			Start = Start + CookieMon.length;
			End = document.cookie.indexOf(";",Start);
			if (End == -1) 
				End = document.cookie.length;
			return Number(document.cookie.substring(Start,End));
		} 
	}
	return 0
}
function LogInCheck()
{
	var Form = document.getElementById("form1");
	if(validateForm(Form))
	{
		document.getElementById("Option").value = OptionValue;
		OptionValue = "";
		Form.submit();
	}
	else
	{
		return false;
	}
	return false;
}
function SetMod()
{
	document.getElementById("ModForm").value = 1;
}
function SaveWarning(TabTitle, CurTab)
{
	if(CurTab != '0' && document.getElementById("ModForm").value != '0')
	{
		return confirm("By navigating through these tabs any changes you may have made will be lost.\nPlease use the navigation buttons at the bottom of the page.\nDo you wish to continue on to " + TabTitle +"?");
	}
	else
	{
		return true;
	}
}
</script>
<script type="text/javascript"  src="FormChek.js"></script>
<script type="text/javascript" >
    var HoldField = "";
    function CalculateNetWorth(form) {
        var CredBal = 0, Asset = 0, Liab = 0, OutTxt = "";
        var NumStr, Len, i, Point;
        Asset += Number(stripCharsNotInBag(form.elements["HomeVal"].value, "0123456789."));
        Asset += Number(stripCharsNotInBag(form.elements["PrptVal"].value, "0123456789."));
        Asset += Number(stripCharsNotInBag(form.elements["AutoVal"].value, "0123456789."));
        Asset += Number(stripCharsNotInBag(form.elements["RetrVal"].value, "0123456789."));
        Asset += Number(stripCharsNotInBag(form.elements["BnkAccVal"].value, "0123456789."));
        Asset += Number(stripCharsNotInBag(form.elements["OthrVal"].value, "0123456789."));
        Liab += Number(stripCharsNotInBag(form.elements["HomeOwe"].value, "0123456789."));
        Liab += Number(stripCharsNotInBag(form.elements["AutoOwe"].value, "0123456789."));
        Liab += Number(stripCharsNotInBag(form.elements["SecOwe"].value, "0123456789."));
        Liab += Number(stripCharsNotInBag(form.elements["OthrOwe"].value, "0123456789."));
        NumStr = Math.abs(Asset - (Liab + CredBal));
        Des = NumStr % 1;
        NumStr -= Des;
        NumStr = String(NumStr);
        Len = NumStr.length - 1;
        for (i = 0; i <= Len; i++) {
            OutTxt += NumStr.charAt(i);
            if (i < Len && (Len - i) % 3 == 0)//>
            {
                OutTxt += ",";
            }
        }
        if (Asset - (Liab + CredBal) < 0) //>
        {
            OutTxt = "&nbsp;&nbsp;<strong><span class='ReqTxt'>- ($" + OutTxt + ")</span></strong>";
        }
        else {
            OutTxt = "&nbsp;&nbsp;<strong><span class='NormTxt' style='color:#009900;'>+ $" + OutTxt + "</span></strong>";
        }
        document.getElementById("NetWorh").innerHTML = OutTxt;
    }
    function prepareField(theField) {
        var sValue = theField.value;
        HoldField = sValue;
        sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
        if (sValue <= 0) sValue = '';
        theField.value = sValue;
        theField.select();
    }
    function CheckMod(theField) {
        if (HoldField != theField.value && HoldField != "") {
            document.getElementById("ModForm").value = 1;
            HoldField = "";
        }
    }
    function reformatPercent(theField) {
        var sValue = theField.value;
        sValue = Number(stripCharsNotInBag(sValue, "0123456789."));
        if (sValue <= 0)
            sValue = 0;
        theField.value = String(sValue.toFixed(2));
        CheckMod(theField);
    }
    function reformatCCCSCurrency(theField) {
        var sValue = theField.value, OutTxt = "", Des;
        var NumStr, Len, i;
        sValue = stripCharsNotInBag(sValue, "0123456789.");
        if (sValue == 0) sValue = '0';
        NumStr = sValue - (sValue % 1);
        NumStr = String(NumStr);
        Len = NumStr.length - 1;
        for (i = 0; i <= Len; i++)//>
        {
            OutTxt += NumStr.charAt(i);
            if (i < Len && (Len - i) % 3 == 0) //>
            {
                OutTxt += ",";
            }
        }
        sValue = '$' + OutTxt;
        theField.value = sValue;
        CheckMod(theField);
    }
    function CheckHomeVal() {

        return true;
    }
</script>



    <p class="NormTxt" align="left">
        Many people do not know their Net Worth, but determining it is just as important
        as knowing how much you owe. This next section will help you identify the value
        of your assets (things that you own) and your liabilities (debts you owe). Your
        Net Worth is calculated as the difference between the total value of your Assets
        minus the total amount of your Liabilities. Generally, the higher your Net Worth,
        the more resources and options you have to deal with any financial challenges.
    </p>
    <p class="ReqTxt">
        You have previously indicated you are buying or own but have not entered an amount
        for the value of your home. Please do so.</p>
    <!-- start Net Worth form -->
    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <!-- sizing control row  -->
            <td width="" height="0">
            </td>
            <td width="115">
            </td>
            <td width="">
            </td>
        </tr>
        <!-- Assets -->
        <tr class="GreyBack">
            <td colspan="3" height="25" align="center">
                <table width="100%" cellspacing="3">
                    <tr>
                        <td height="25" class="NormHead" align="center">
                            Assets
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="3" class="NormTxt">
                &nbsp;<strong>Please list the estimated value of the following items you own:</strong>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Current Value of Home
            </td>
            <td align="right">
                <%--<input class="NumBox" name="HomeVal" id="HomeVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(HomeVal);" onfocus="prepareField(HomeVal);">--%>
                    <asp:TextBox ID="txtHomeVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
                <span class="ReqTxt">&nbsp;*&nbsp;&nbsp;</span> <a class="NormLink" href="http://www.zillow.com/"
                    target="_blank">Retrieve Your Home's Estimated Value</a>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Real Estate/Personal Property
            </td>
            <td align="right">
                <%--<input class="NumBox" name="PrptVal" id="PrptVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(PrptVal);" onfocus="prepareField(PrptVal);">--%>
                    <asp:TextBox ID="txtPrptVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td class="ReqTxt">
                &nbsp;*
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Automobiles<br>
                <span class="TnyTxt">Do not include the value of leased vehicles</span>
            </td>
            <td align="right">
                <%--<input class="NumBox" name="AutoVal" id="AutoVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(AutoVal);" onfocus="prepareField(AutoVal);">--%>
                    <asp:TextBox ID="txtAutoVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
                <span class="ReqTxt">&nbsp;*&nbsp;&nbsp;</span> <a class="NormLink" href="http://www.edmunds.com/tmv/used"
                    target="_blank">Retrieve your auto's Estimated Value</a>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Current Value of 401K / Retirement Accounts
            </td>
            <td align="right">
                <%--<input class="NumBox" name="RetrVal" id="RetrVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(RetrVal);" onfocus="prepareField(RetrVal);">--%>
                    <asp:TextBox ID="txtRetrVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Checking/ Savings Accounts
            </td>
            <td align="right">
                <%--<input class="NumBox" name="BnkAccVal" id="BnkAccVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(BnkAccVal);" onfocus="prepareField(BnkAccVal);">--%>
                    <asp:TextBox ID="txtBnkAccVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Other
            </td>
            <td align="right">
                <%--<input class="NumBox" name="OthrVal" id="OthrVal" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(OthrVal);" onfocus="prepareField(OthrVal);">--%>
                    <asp:TextBox ID="txtOthrVal" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td height="20" colspan="3">
            </td>
        </tr>
        <!-- Liabilities -->
        <tr class="GreyBack">
            <td colspan="3" height="25" align="center">
                <table width="100%" cellspacing="3">
                    <tr>
                        <td height="25" class="NormHead" align="center">
                            Liabilities
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="GreyBack">
            <td colspan="3" class="NormTxt">
                &nbsp;<strong>Please list the estimated debt you owe on the following items:</strong>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Mortgage Balance
            </td>
            <td align="right">
                <%--<input class="NumBox" name="HomeOwe" id="HomeOwe" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(HomeOwe);" onfocus="prepareField(HomeOwe);">--%>
                    <asp:TextBox ID="txtHomeOwe" value="$0" runat="server"></asp:TextBox>
            </td>
            <td class="NormTxt">
                <span class="ReqTxt">&nbsp;*</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Interest Rate
                <%--<input class="NumBox" name="InterRate" id="InterRate" value="0" type="text" size="5"
                    onblur="reformatPercent(InterRate)">%--%>
                    <asp:TextBox ID="txtInterRate" value="$0" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Auto Loans
            </td>
            <td align="right">
                <%--<input class="NumBox" name="AutoOwe" id="AutoOwe" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(AutoOwe);" onfocus="prepareField(AutoOwe);">--%>
                    <asp:TextBox ID="txtAutoOwe" value="$0" runat="server"></asp:TextBox>
            </td>
            <td class="ReqTxt">
                &nbsp;*
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Secured Debts<br>
                (2nd mortgage, boat, etc.)
            </td>
            <td align="right">
                <%--<input class="NumBox" name="SecOwe" id="SecOwe" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(SecOwe);" onfocus="prepareField(SecOwe);">--%>
                    <asp:TextBox ID="txtSecOwe" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Other Debts (student loans, etc.)
            </td>
            <td align="right">
                <%--<input class="NumBox" name="OthrOwe" id="OthrOwe" value="$0" type="text" size="14"
                    onblur="reformatCCCSCurrency(OthrOwe);" onfocus="prepareField(OthrOwe);">--%>
                    <asp:TextBox ID="txtOthrOwe" value="$0" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Unsecured Debt<br>
                (from creditors you entered earlier)
            </td>
            <td align="right">
                $0
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="debt_mnt.asp?Back=Form5" class="NormLink">I need to
                    make adjustments</a>
            </td>
        </tr>
        <tr>
            <td height="20" colspan="3">
            </td>
        </tr>
        <!-- Net Worth -->
        <tr>
            <td valign="middle" colspan="3" class="GreyBack">
                <table width="100%" cellpadding="2">
                    <tr>
                        <td align="center" height="25" class="NormHead">
                            Net Worth
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="GreyBack">
            <td height="35" class="NormTxt" align="right">
                Your Net Worth&nbsp;
            </td>
            <td height="35" id="NetWorh" align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td height="30" colspan="3">
            </td>
        </tr>
  
    <tr>
             <td align="center">
						<asp:ImageButton ID="imgBtnUnderstandingNetWorthPrevious" runat="server" ImageUrl ="~/images/btn_OC_previous.gif" />
						<asp:ImageButton ID="imgBtnUnderstandingNetWorthContinue" runat="server" ImageUrl = "~/images/btn_OC_Continue.gif"/>
						<asp:ImageButton ID="imgBtnUnderstandingNetWorthToPrevious" runat="server" ImageUrl = "~/images/btn_Continue_Later.gif" />
					</td>
        </tr>
    </table>


</asp:Content>
