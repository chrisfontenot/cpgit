﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Cccs.Sandbox.Website.Profile" MasterPageFile="~/Master.Master" Title="CCCS User Profile"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
<table class="style1">
    <tr><td colspan="6">It is essential that we verify your identity and confirm that you are the person seeking this service. We conduct our verification in a three-step process.</td></tr> 
<tr><td colspan="4">
<b>1 . Provide Your Personal Information</b>
<p>
First, we ask you to provide detailed personal information about yourself by completing the following fields: </p></td></tr>
        <tr>
             <td>
                &nbsp;</td>
            <td class="style4">
                Owner</td>
            <td>
                &nbsp;</td>
           <td class="style4">
                Co-Owner
  **(Required for joint session and certificate) </td>
        </tr>
        <tr>
            <td class="style4">
                First Name</td>
            <td class="style3">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtCoOwnerFirstName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Initial</td>
            <td class="style3">
                <asp:TextBox ID="txtInitial" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtCoOwnerInitial" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Last Name</td>
            <td class="style3">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtCoOwnerLastName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                SSN</td>
            <td class="style3">
                <asp:TextBox ID="txtSSN" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtCoOwnerSSN" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Date of Birth(MM/DD/YYYY)</td>
            <td class="style3">
                <asp:TextBox ID="txtDOB" runat="server"></asp:TextBox>
                </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="txtCoOwnerDOB" runat="server" ></asp:TextBox>
                (MM/DD/YYYY)</td>
        </tr>
        <tr>
            <td class="style4">
                Marital Status</td>
            <td class="style3">
                <asp:DropDownList ID="ddlMaritalStatus" runat="server">
                   <asp:ListItem value="--select--">--select--</asp:ListItem>
                    <asp:ListItem Value="M">Married</asp:ListItem>
                     <asp:ListItem Value="S">Single</asp:ListItem>
                     <asp:ListItem Value="D">Divorced</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:DropDownList ID="ddlCoOwnerMaritalStatus" runat="server"> 
                     <asp:ListItem  Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="M">Married</asp:ListItem>
                     <asp:ListItem Value="S">Single</asp:ListItem>
                     <asp:ListItem Value="D">Divorced</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Sex</td>
            <td class="style3">
               <asp:DropDownList ID="ddlSex" runat="server" CssClass="ddl">
                    <asp:ListItem >--select--</asp:ListItem>
                    <asp:ListItem Value="M">Male</asp:ListItem>
                     <asp:ListItem Value="F">Female</asp:ListItem>
                    </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                <asp:DropDownList ID="ddlCoOwnerSex" runat="server">
                <asp:ListItem >--select--</asp:ListItem>
                    <asp:ListItem Value="M">Male</asp:ListItem>
                     <asp:ListItem Value="F">Female</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Race</td>
            <td class="style3">
                <asp:DropDownList ID="ddlRace" runat="server">
                <asp:ListItem Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="A">African American</asp:ListItem>
                     <asp:ListItem Value="W">White/Caucasian</asp:ListItem>
                     <asp:ListItem Value="X">Asian</asp:ListItem>
                     <asp:ListItem Value="M">Other Multiple Race</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td style="margin-left: 40px">
                <asp:DropDownList ID="ddlCoOwnerRace" runat="server" >
                   <asp:ListItem  Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="A">African American</asp:ListItem>
                     <asp:ListItem Value="W">White/Caucasian</asp:ListItem>
                     <asp:ListItem Value="X">Asian</asp:ListItem>
                     <asp:ListItem Value="M">Other Multiple Race</asp:ListItem>  
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Hispanic?</td>
            <td class="style3">
                <asp:DropDownList ID="ddlHispanic" runat="server">
                <asp:ListItem  Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                     <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                Hispanic?<asp:DropDownList ID="ddlCoHispanic" runat="server" >
                  <asp:ListItem  Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                     <asp:ListItem Value="0">No</asp:ListItem>
                     </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style4">
                Street Address</td>
            <td class="style3">
                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                Address Line2</td>
            <td class="style3">
                <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                City
            </td>
            <td class="style3">
                <asp:TextBox ID="txtCity" runat="server" ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                State</td>
            <td class="style3">
                <asp:DropDownList ID="ddlState" runat="server" >
                     <asp:ListItem  Value="">--select--</asp:ListItem>
                    <asp:ListItem Value="AA">AA</asp:ListItem>
                     <asp:ListItem Value="AL">AK</asp:ListItem>
                     <asp:ListItem Value="AL">AL</asp:ListItem>
                     <asp:ListItem Value="AP">AP</asp:ListItem>
                     <asp:ListItem Value="AR">AR</asp:ListItem>
                     <asp:ListItem Value="AS">AS</asp:ListItem>
                     <asp:ListItem Value="AZ">AZ</asp:ListItem>
                     <asp:ListItem Value="CA">CA</asp:ListItem>
                     <asp:ListItem Value="CO">CO</asp:ListItem>
                     <asp:ListItem Value="CT">CT</asp:ListItem>
                     <asp:ListItem Value="DC">DC</asp:ListItem>
                     <asp:ListItem Value="DE">DE</asp:ListItem>
                     <asp:ListItem Value="FL">FL</asp:ListItem>
                     <asp:ListItem Value="FM">FM</asp:ListItem>
                     <asp:ListItem Value="GA">GA</asp:ListItem>
                     <asp:ListItem Value="GU">GU</asp:ListItem>
                     <asp:ListItem Value="HI">HI</asp:ListItem>
                     <asp:ListItem Value="IA">IA</asp:ListItem>
                     <asp:ListItem Value="ID">ID</asp:ListItem>
                     <asp:ListItem Value="IL">IL</asp:ListItem>
                     <asp:ListItem Value="IN">IN</asp:ListItem>   
                     <asp:ListItem Value="KS">KS</asp:ListItem>
                     <asp:ListItem Value="KY">KY</asp:ListItem>								
				     <asp:ListItem Value="LA">LA</asp:ListItem>		
				     <asp:ListItem Value="MA">MA</asp:ListItem>	
				     <asp:ListItem Value="MD">MD</asp:ListItem>	
				     <asp:ListItem Value="ME">ME</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
				     <asp:ListItem Value="AL">AK</asp:ListItem>	
					</asp:DropDownList>	    					
										
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                Zip Code</td>
            <td class="style3">
                <asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
             <tr>
            <td class="style4">
                &nbsp;</td>
            <td class="style3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style4">
                &nbsp;</td>
                     
        </tr></table>
        <br>
				<li class="NormTxt">
					<p class="NormTxt">
						<u><strong>Authorize Us To Obtain Credit Report Data</strong></u>
					</p>
					<p class="NormTxt">
						To use our online counseling process, we require you to authorize us to pull a copy of your credit report. We will match the data you have provided with that found in the credit bureau database. This soft pull of your credit report will not affect your credit score and will not be seen by any 3<sup>rd</sup> party reviewing your credit report.
					</p>
					<p class="NormTxt">
						Please enter your city of birth as authorization for us to obtain a copy of your credit report:
					</p>
					<table cellspacing=0 cellpadding=0 width=770 align=center border=0>
						<tr>
							<td width="100" class="NormTxt"></td>
							<td width="335" class="NormTxt">
								<strong>Owner</strong>
							</td>
							<td width="335" class="NormTxt">
								<strong>Co-Owner (if applicable)</strong>
							</td>
						</tr>
						<tr>
							<td class="NormTxt"></td>
							<td class="ReqTxt">
								<asp:textbox runat="server" maxlength="50" class="TxtBox" tabindex="51" name="PriBirthCity" id="Text1"  onBlur="CleanField(form.elements['PriBirthCity'])" onChange="SetMod()"/>*
							</td>
							<td>
								<table border="0" align="left" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<asp:textbox runat="server" maxlength="50" class="TxtBox" tabindex="51" name="PriBirthCity" id="Textbox14"  onBlur="CleanField(form.elements['PriBirthCity'])" onChange="SetMod()"/>
											<span id="Span1">&nbsp;*</span>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				
<br>
				<br>
				</li>
				<li class="NormTxt">
					<p class="NormTxt">
						<u><strong>Affirm Your  Identity and Commit to Participate</strong></u>
					</p>
					<p class="NormTxt">
						Finally, we ask you to affirm that you are the person(s) identified above and commit to personally complete this process.
					</p>
					<ul class="NormTxt">
						<li>
							I certify that all the information provided on this page and all future information I provide to CCCS throughout my counseling session is true, correct, complete and offered in good faith.
						</li><br>
						<li>
							I understand that it is my personal responsibility as a client of CCCS to participate in and complete the counseling session.  I understand that knowingly making a false or fraudulent statement, misrepresenting my identity or not participating in my counseling session is a violation of the requirements for obtaining a certificate and may result in its revocation.
						</li>
					</ul>
				</li>
			</ol>
			<table cellspacing=0 cellpadding=0 width=770 align=center border=0>
				<tr>
					<td width="100" class="NormTxt"></td>
					<td width="20" class="NormTxt" valign="top">
						<input tabindex="54" type="checkbox" name="icertify" id="icertify">
					</td>
					<td width="315" class="NormTxt">
						<label for="icertify">
						<strong>I affirm the statements above.<br>(Owner)</strong>
						</label>
					</td>
					<td width="20" class="NormTxt" valign="top">
						<input tabindex="55" type="checkbox" name="icertify2" id="icertify2">
					</td>
					<td width="315" class="NormTxt">
						<label for="icertify2">
						<strong>I affirm the statements above.<br>(Co-Owner)</strong>
						</label>
					</td>
				</tr>
			</table>
			<p align="center">
				 <asp:ImageButton ID="imgBtnWelcome" runat="server" 
                       ImageUrl="~/images/btn_OC_Continue.gif" 
                       onclick="imgBtnContinueWelcome_Click" /></p>
</asp:Content>