﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DescribeYourSituation.aspx.cs" Inherits="Cccs.Sandbox.Website.DescribeYourSituations"  MasterPageFile="~/Master.Master" Title="CCCS Describe Your Situation"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
  <p class="ReqTxt">
        Please indicate the main event</p>
    <p class="NormTxt">
        Every person's situation is unique. By sharing the details of your personal situation,
        you will help us give the best possible advice. We also know that your privacy is
        important. The information you provide us will remain strictly confidential and
        will not be shared unless you authorize us to do so.
    </p>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="NormTxt">
                People typically cite one main reason they are considering a Reverse mortgage, what
                was yours?
            </td>
            <td nowrap class="ReqTxt">
                
                <asp:DropDownList ID="ddlTheEvent" CssClass ="TxtBox" runat="server" >
                <asp:ListItem value="No" Text="-- Select One --" Selected="True"></asp:ListItem> 
                <asp:ListItem value="20" Text="Pay off Existing Mortgage"></asp:ListItem> 
                <asp:ListItem value="21" Text="Catch up on Mortgage Payments"></asp:ListItem> 
                <asp:ListItem value="22" Text="Make Home Repairs"></asp:ListItem> 
                <asp:ListItem value="23" Text="Pay off Other Debts" ></asp:ListItem>
                <asp:ListItem value="24" Text="Increase Monthly Income" ></asp:ListItem>
                <asp:ListItem value="25" Text="Provide for Emergency Expenses" ></asp:ListItem>
                <asp:ListItem value="02" Text="Reduced Income" ></asp:ListItem>
                <asp:ListItem value="03" Text="Medical/Accident" ></asp:ListItem>
                <asp:ListItem value="04" Text="Death of a Family Member" ></asp:ListItem>
                <asp:ListItem value="05" Text="Divorce/Separation" ></asp:ListItem>
                <asp:ListItem value="06" Text="Unemployment" ></asp:ListItem>
                <asp:ListItem value="08" Text="Disability" ></asp:ListItem>
                <asp:ListItem value="09" Text="Gambling" ></asp:ListItem>
                <asp:ListItem value="12" Text="Child Support or Alimony" ></asp:ListItem>
                <asp:ListItem value="14" Text="Student Loans" ></asp:ListItem>
                <asp:ListItem value="01" Text="Over Obligation" ></asp:ListItem>
                <asp:ListItem value="07" Text="Other" ></asp:ListItem>
                </asp:DropDownList> 
                *
               

            </td>
        </tr>
        <tr>
            <td height="15" colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2" class="NormTxt">
                Describe the circumstances that led you to consider a Reverse mortgage: <span class="ReqTxt">
                    *</span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
              
               <asp:TextBox ID="txtComment" runat="server" TextMode ="MultiLine" Columns ="100" Rows ="5" ></asp:TextBox>  
            </td>
        </tr>
        <tr>
            <td height="15" colspan="2">
            </td>
        </tr>
        <tr>
            <td class="NormTxt">
                <strong>Have you missed any mortgage payments in the past six months?</strong>
            </td>
            <td class="NormTxt">
                
                <asp:RadioButton ID="rbtRentCurN" runat="server" GroupName="rbtMortgagePayment" Text="Yes"/> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <asp:RadioButton ID="rbtRentCurY" runat="server" GroupName="rbtMortgagePayment" Text="No"/>
                &nbsp;<span class="ReqTxt">*</span>
            </td>
        </tr>
        <tr>
            <td class="NormTxt">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - If yes, how many months have you missed?
            </td>
            <td class="NormTxt">
              <asp:TextBox ID="txtRentLate" runat="server" CssClass="TxtBox"></asp:TextBox>
                (number of months)
            </td>
        </tr>
        <tr>
            <td height="15" colspan="2">
            </td>
        </tr>
        <tr>
            <td class="NormTxt">
                Number of People Living in Household&nbsp;
            </td>
            <td>
                
                   <asp:TextBox ID="txtHouseholdSize" runat="server" CssClass="TxtBox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td height="15" colspan="2">
            </td>
        </tr>
    <tr>
    <td align="center">
						<asp:ImageButton ID="imgBtnDescribingPrevious" runat="server" 
                            ImageUrl ="~/images/btn_OC_previous.gif" 
                            onclick="imgBtnDescribingPrevious_Click" />
						<asp:ImageButton ID="imgBtnDescribingContinue" runat="server" ImageUrl = "~/images/btn_OC_Continue.gif" OnClick ="btn_DescribeContinue" />
						<asp:ImageButton ID="imgBtnReturnToPrevious" runat="server" 
                            ImageUrl = "~/images/btn_Continue_Later.gif" 
                            onclick="imgBtnReturnToPrevious_Click" />
					</td>
					</tr>
				</table>
</asp:Content>
