﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynamicListSample.aspx.cs" Inherits="Cccs.Sandbox.Website.DynamicListSample" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
		<asp:ListView ID="ListViewDynamic" runat="server">
			<LayoutTemplate>
				<table>
					<thead>
						<th></th>
						<th>Creditor Name</th>
						<th>Balance</th>
						<th>Interest Rate</th>
						<th>Monthly Payment</th>
						<th>Account Number</th>
						<th>Joint Account?</th>
						<th>Debt Past Due?</th>
					</thead>
					<thead>
						<th>Select for delete</th>
						<th>EXAMPLE</th>
						<th>$1,234</th>
						<th>12.5%</th>
						<th>$45</th>
						<th>Please provide full account #.</th>
						<th>No. Why?</th>
						<th>Current</th>
					</thead>
					<asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
					<tfoot>
						<td colspan="*">		    
							<asp:Button ID="ButtonAdd" runat="server" Text="Add" onclick="ButtonAdd_Click" />
							<asp:Button ID="ButtonDel" runat="server" Text="Del" onclick="ButtonDel_Click" />
						</td>
					</tfoot>
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td><asp:CheckBox ID="CheckBoxDel" runat="server" /></td>
					<td><asp:TextBox ID="TextBoxCreditor" runat="server" /></td>
					<td><asp:TextBox ID="TextBox1" runat="server" /></td>
					<td><asp:TextBox ID="TextBox2" runat="server" /></td>
					<td><asp:TextBox ID="TextBox3" runat="server" /></td>
					<td><asp:TextBox ID="TextBox4" runat="server" /></td>
					<td><asp:DropDownList ID="DropDownList1" runat="server" /></td>
					<td><asp:DropDownList ID="DropDownList2" runat="server" /></td>
				</tr>
			</ItemTemplate>
		</asp:ListView>

    </div>
    </form>
</body>
</html>
