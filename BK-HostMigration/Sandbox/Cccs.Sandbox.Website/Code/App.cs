﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Translation;
using Cccs.Identity;
using Cccs.Configuration;
using Cccs.Geographics;
//using Cccs.Credability;
//using Cccs.Transaction;


namespace Cccs.Sandbox.Website
{
	public static class App
	{
		private static IConfiguration m_configuration;
		private static ITranslation m_translation;
		private static IGeographics m_geographics;
		private static IIdentity m_identity;

		//private static ICredability m_credability;
		//private static ITransaction m_transaction;

		static App()
		{
			m_configuration = new Cccs.Configuration.Impl.Cache.Configuration(new Cccs.Configuration.Impl.Configuration());

			m_translation = new Cccs.Translation.Impl.Cache.Translation(new Cccs.Translation.Impl.Translation());

			m_geographics = new Cccs.Geographics.Impl.Cache.Geographics(new Cccs.Geographics.Impl.Geographics());

			m_identity = new Cccs.Identity.Impl.Identity(m_configuration, m_translation, m_geographics);

			//m_credability = new Cccs.Credability.Impl.Credability();
     
			//m_transaction = new Cccs.Transaction.Impl.Transaction();
		}

		public static IConfiguration Configuration
		{
			get { return m_configuration; }
	  }

		public static ITranslation Translation
		{
			get { return m_translation; }
		}

		public static IIdentity Identity
		{
			get { return m_identity; }
		}

		//public static ICredability Credability
		//{
		//  get { return m_credability; }
		//}

		//public static ITransaction Transaction 
		//{
		//    get { return m_transaction; }
		//}

		public static string Translate(string language_text_code)
		{
			return m_translation.LanguageTextGet(SessionState.LanguageCode, language_text_code);
		}
	}
}
