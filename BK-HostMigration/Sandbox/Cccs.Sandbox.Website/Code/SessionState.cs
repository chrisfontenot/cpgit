﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cccs.Web;

namespace Cccs.Sandbox.Website
{
	public static class SessionState
	{
		#region Constants

			private const string LANGUAGE_CODE = "LanguageCode";

		#endregion

			public static string LanguageCode
			{
				get 
				{ 
					string language_code = HttpContext.Current.Session.ValueGet<string>(LANGUAGE_CODE);

					if (string.IsNullOrEmpty(language_code))
					{
						language_code = Cccs.Translation.Language.EN;
					}

					return language_code; 
				}
				
				set 
				{ 
					HttpContext.Current.Session.ValueSet(LANGUAGE_CODE, value); 
				}
			}
	}
}
