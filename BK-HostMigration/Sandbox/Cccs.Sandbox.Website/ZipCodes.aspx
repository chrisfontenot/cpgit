﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="ZipCodes.aspx.cs" Inherits="Cccs.Sandbox.Website.ZipCodes" Theme="Cccs.Theme"%>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
  <br />
  <br />
	ZIP: <asp:TextBox ID="TextBoxZipCode" runat="server" MaxLength="5"></asp:TextBox>
  <br />
  <br />
  <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" onclick="ButtonSubmit_Click" />
	<br />
	<br />
	<asp:ListView ID="ListViewZipCodes" runat="server">
	  <LayoutTemplate>
	    <table>
	      <thead>
	        <th>ZIP</th>
	        <th>City</th>
	        <th>St</th>
	      </thead>
				<asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
	    </table>
	  </LayoutTemplate>
	  <ItemTemplate>
	    <tr>
	      <td><%# Eval("Zip") %></td>
	      <td><%# Eval("City") %></td>
	      <td><%# Eval("State") %></td>
	    </tr>
	  </ItemTemplate>
	  <EmptyDataTemplate>
			No information for specified ZIP.
	  </EmptyDataTemplate>
	</asp:ListView>


</asp:Content>
