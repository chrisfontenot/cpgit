﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Cccs.Sandbox.Website
{
	public partial class DynamicListSample : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ListViewDynamic.DataSource = new int[] { 1, 2, 3 };
				ListViewDynamic.DataBind();
			}
		}

		protected void ButtonAdd_Click(object sender, EventArgs e)
		{
			// Extract current data and rebind

			List<int> items = new List<int>();

			foreach (ListViewDataItem lvi in ListViewDynamic.Items)
			{
				items.Add(items.Count + 1);
			}

			items.Add(items.Count + 1);
			ListViewDynamic.DataSource = items;
			ListViewDynamic.DataBind();
		}

		protected void ButtonDel_Click(object sender, EventArgs e)
		{
			List<int> items = new List<int>();

			foreach (ListViewDataItem lvi in ListViewDynamic.Items)
			{
				CheckBox checkbox_del = lvi.FindControl("CheckBoxDel") as CheckBox;

				if (checkbox_del.Checked == false)
				{
					items.Add(items.Count + 1);
				}	
			}

			ListViewDynamic.DataSource = items;
			ListViewDynamic.DataBind();
		}

	}
}
