﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSelect.ascx.cs" Inherits="Cccs.Sandbox.Website.Controls.LanguageSelect" %>
<asp:UpdatePanel ID="UpdatePanelLanguageSelect" runat="server">
  <ContentTemplate>
		<asp:LinkButton ID="LinkButtonLanguageSelect" runat="server" CssClass="languageSelect" onclick="LinkButtonLanguageSelect_Click" />
	</ContentTemplate>
</asp:UpdatePanel>
