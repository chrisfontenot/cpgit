﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cccs.Identity;

namespace Cccs.Sandbox.Website
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Initialize();
			}
		}

		private void Initialize()
		{
			ButtonLogin.Text = App.Translate("Sandbox|Default|Submit");
		}

		protected void ButtonLogin_Click(object sender, EventArgs e)
		{
		  UserAuthenticateResult result = App.Identity.UserAuthenticate(TextBoxUsername.Text, TextBoxPassword.Text, Request.UserHostAddress);
		}
	}
}
