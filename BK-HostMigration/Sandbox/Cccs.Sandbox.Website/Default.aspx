﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Cccs.Sandbox.Website.Default" Theme="Cccs.Theme" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="ContentPlaceHolderHead" runat="server">
</asp:Content>
<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">

	<%= Cccs.Sandbox.Website.App.Translate("Sandbox|Default|Username") %>
	<asp:TextBox ID="TextBoxUsername" runat="server"></asp:TextBox>
	<br />
	<br />
	<%= Cccs.Sandbox.Website.App.Translate("Sandbox|Default|Password") %>
	<asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password"></asp:TextBox>
  <br />
  <br />
  <asp:Button ID="ButtonLogin" runat="server" onclick="ButtonLogin_Click" />

</asp:Content>
