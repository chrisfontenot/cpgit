#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using DebtPlus.Interfaces;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DebtPlus.Svc.Common
{
    /// <summary>
    /// Validate the account number for the client against the legal masks for the creditor.
    /// This is a business layer routine.
    /// </summary>
    public class AccountNumberValidation : IAccountNumberValidate, IDisposable
    {
        private DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
        private string privateValidatedAccountNumber = string.Empty;

#region  IDisposable Support
        private bool disposedValue = false;        //To detect redundant calls

        /// <summary>
        /// Dispose of the storage
        /// </summary>
        /// <param name="disposing">TRUE if Dispose is called. FALSE for Finalize</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;
                if (disposing)
                {
                }
            }
        }

        /// <summary>
        /// Dispose of the storage allocated by the class
        /// </summary>
        public void Dispose() // Implements IDisposable.Dispose
        {
            // Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#endregion

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        public AccountNumberValidation() : base()
        {
            InitializeComponents();
            RegisterHandlers();
        }

        /// <summary>
        /// Do initialization if needed
        /// </summary>
        private void InitializeComponents()
        {
            LastError = string.Empty;
        }

        /// <summary>
        /// Add any event processing that may be needed by this routine
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove the events registered by the RegisterHandlers method.
        /// </summary>
        private void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// return the reason why the account number is invalid as a string.
        /// </summary>
        /// <remarks></remarks>
        public string LastError { get; protected set; } // Implements IAccountNumberValidate.LastError

        /// <summary>
        /// return the account number as it was validated against the database. This may be different from the
        /// original account number since spaces and dash characters may have been removed.
        /// </summary>
        /// <remarks></remarks>
        public string ValidatedAccountNumber { get; protected set; } // Implements IAccountNumberValidate.ValidatedAccountNumber

        /// <summary>
        /// Is this a valid account number for this creditor?
        /// </summary>
        /// <param name="Creditor">The ID of the creditor in the database tables</param>
        /// <param name="AccountNumber">The account number to be validated</param>
        /// <returns>true if the account is valid. false otherwise.</returns>
        /// <remarks>For valid accounts, see ValidatedAccountNumber. For invalid accounts, see LastError.</remarks>
        public bool IsValidAccount(string Creditor, string AccountNumber) // Implements IAccountNumberValidate.IsValidAccount
        {
            // If the creditor is missing then the account number is invalid
            if (Creditor == string.Empty)
            {
                LastError = "Missing creditor ID";
                return false;
            }

            return LocalIsValidAccount(string.Format("WHERE cr.creditor = '{0}'", Creditor.Replace("'", "''")), AccountNumber);
        }

        /// <summary>
        /// Is this a valid account number for this creditor?
        /// </summary>
        /// <param name="Creditor">The relative number of the creditor in the database tables</param>
        /// <param name="AccountNumber">The account number to be validated</param>
        /// <returns>true if the account is valid. false otherwise.</returns>
        /// <remarks>For valid accounts, see ValidatedAccountNumber. For invalid accounts, see LastError.</remarks>
        public bool IsValidAccount(Int32 Creditor, string AccountNumber) // Implements IAccountNumberValidate.IsValidAccount
        {
            // If the creditor is missing then the account number is invalid
            if (Creditor <= 0)
            {
                LastError = "Missing creditor ID";
                return false;
            }

            return LocalIsValidAccount(string.Format("WHERE cr.creditor_id = '{0:f0}'", Creditor), AccountNumber);
        }

        /// <summary>
        /// Determine if the account number is valid for the selection criteria
        /// </summary>
        /// <param name="Selection"></param>
        /// <param name="AccountNumber"></param>
        /// <returns></returns>
        private bool LocalIsValidAccount(string Selection, string AccountNumber)
        {
            // If there is no account number then the account number is valid
            if (AccountNumber == string.Empty)
            {
                ValidatedAccountNumber = string.Empty;
                return true;
            }

            bool Answer = false;
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

            try
            {
                cn.Open();
                using (var ds = new System.Data.DataSet("ds"))
                {
                    Answer = ValidCreditor(ds, cn, Selection) &&
                             ValidPrefixes(ds, cn, Selection, AccountNumber) &&
                             ValidMask(ds, cn, Selection, AccountNumber);
                }
            }

            catch (SqlException ex)
            {
                LastError = ex.Message;
            }

            finally
            {
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            return Answer;
        }

        /// <summary>
        /// Validate the creditor. It must exist.
        /// </summary>
        /// <param name="ds">Dataset pointer</param>
        /// <param name="cn">Connection object</param>
        /// <param name="Selection">Selection clause for the commandtext parameter</param>
        /// <returns></returns>
        private bool ValidCreditor(System.Data.DataSet ds, SqlConnection cn, string Selection)
        {
            object ObjAnswer = System.DBNull.Value;

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.CommandText = "SELECT creditor FROM creditors cr " + Selection;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout;
                ObjAnswer = cmd.ExecuteScalar();
            }

            if (ObjAnswer != null && ObjAnswer != System.DBNull.Value)
            {
                return true;
            }

            LastError = "Invalid creditor ID";
            return false;
        }

        /// <summary>
        /// Validate the prefix lines. They must match the account number.
        /// </summary>
        /// <param name="ds">Dataset object</param>
        /// <param name="cn">Connection object</param>
        /// <param name="Selection">Selection clause for the creditor</param>
        /// <param name="AccountNumber">Account number</param>
        /// <returns></returns>
        private bool ValidPrefixes(System.Data.DataSet ds, SqlConnection cn, string Selection, string AccountNumber)
        {
            bool Answer = true;

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.CommandText = "SELECT ca.type, ca.additional FROM creditor_addkeys ca INNER JOIN creditors cr ON ca.creditor = cr.creditor " + Selection + " AND ca.type = 'P'";
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds, "creditor_addkeys");
                }
            }
            ValidatedAccountNumber = AccountNumber;

            // Find items that are a match to the prefix code
            using (var vue = new DataView(ds.Tables["creditor_addkeys"], "type='P'", string.Empty, DataViewRowState.CurrentRows))
            {
                if (vue.Count > 0)
                {
                    Answer = false;
                    foreach (DataRowView drv in vue)
                    {
                        string Prefix = DebtPlus.Utils.Nulls.DStr(drv["additional"]);
                        string TestAccount = AccountNumber.Replace(" ", "").Replace("-", "");
                        ValidatedAccountNumber = TestAccount;
                        if (TestAccount.StartsWith(Prefix))
                        {
                            Answer = true;
                            break;
                        }
                    }
                }
            }

            if (!Answer)
            {
                LastError = "Invalid prefix";
            }
            return Answer;
        }

        /// <summary>
        /// Validate the account masks. They must match the account number.
        /// </summary>
        /// <param name="ds">Dataset object</param>
        /// <param name="cn">Connection object</param>
        /// <param name="Selection">Selection clause</param>
        /// <param name="AccountNumber">Account number</param>
        /// <returns></returns>
        private bool ValidMask(System.Data.DataSet ds, SqlConnection cn, string Selection, string AccountNumber)
        {
            bool Answer = true;

            // Read the list of masks from the RPPS biller ID system
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.CommandText = "SELECT m.mask, m.Checkdigit FROM rpps_masks m INNER JOIN creditor_methods crm ON m.rpps_biller_id = crm.rpps_biller_id AND 'EFT' = crm.type INNER JOIN banks b on crm.bank = b.bank AND 'R' = b.type INNER JOIN creditors cr ON crm.creditor = cr.creditor_id " + Selection;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds, "masks");
                }
            }

            // If there are no rows then the account number is valid for the time being.
            // It is assumed that the payment will be by check and so we don't need to check it.
            ValidatedAccountNumber = AccountNumber;
            if (ds.Tables["masks"].Rows.Count > 0)
            {
                // Determine if the account_number is a valid match to any of the fields in the masks list
                foreach (DataRow row in ds.Tables["masks"].Rows)
                {
                    string mask = DebtPlus.Utils.Nulls.DStr(row[0]);
                    Int32 checksum = DebtPlus.Utils.Nulls.DInt(row[1]);

                    // Remove the spaces and dashes from the account number if the mask does not have them.
                    // We can temporarily allow the values for a moment but if the mask as no, then just take them out.
                    string TestAccount = AccountNumber;
                    if (mask.IndexOf(' ') < 0)
                    {
                        TestAccount = TestAccount.Replace(" ", "");
                    }

                    if (mask.IndexOf('-') < 0)
                    {
                        TestAccount = TestAccount.Replace("-", "");
                    }
                    ValidatedAccountNumber = TestAccount;

                    // The mask length must match the resulting account number length to be valid
                    if (mask.Length == TestAccount.Length)
                    {
                        // Conver the mask to a regular expression string.
                        var sb = new System.Text.StringBuilder();
                        sb.Append("^");
                        foreach (Char MaskChar in mask.ToCharArray())
                        {
                            switch (MaskChar)
                            {
                                case '@':
                                    sb.Append(@"[a-z]");
                                    break;
                                
                                case '#':
                                    sb.Append(@"\d");
                                    break;
                                
                                case 'd':
                                case 's':
                                case 'w':
                                    sb.Append(MaskChar);
                                    break;
                                
                                case '0':
                                case '1':
                                case '2':
                                case '3':
                                case '4':
                                case '5':
                                case '6':
                                case '7':
                                case '8':
                                case '9':
                                    sb.Append(MaskChar);
                                    break;

                                default:
                                    sb.Append(@"\");
                                    sb.Append(MaskChar);
                                    break;
                            }
                        }
                        sb.Append("$");

                        // Take the resulting regular expression and do a pattern match on the input account number
                        if (System.Text.RegularExpressions.Regex.IsMatch(TestAccount, sb.ToString(), RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Singleline))
                        {

                            // Determine if the account number has a valid checksum.
                            if (checksum != 0)
                            {
                                Answer = ValidLUHN(TestAccount);
                                if (!Answer)
                                {
                                    LastError = "Invalid checksum";
                                }
                                return Answer;
                            }

                            return true;
                        }
                    }
                }

                LastError = "Account number not valid for mask";
                Answer = false;
            }

            return Answer;
        }

        /// <summary>
        /// Validate the LUHN checksum if possible.
        /// </summary>
        /// <param name="AccountNumber">Account number</param>
        /// <returns></returns>
        private bool ValidLUHN(string AccountNumber)
        {
            bool doubleScaling = true; // start false. toggled before testing.
            Int32 checksum = 0;
            Int32 column = AccountNumber.Length;

            // Process the digits in the columns
            while (--column >= 0)
            {
                char currentChar = AccountNumber[column];
                if (System.Char.IsDigit(currentChar))
                {
                    Int32 digit = Convert.ToByte(currentChar) - 0x30;   // 0x30 == '0'.

                    // If this is the even column, scale by a factor of 2.
                    doubleScaling = !doubleScaling;
                    if (doubleScaling)
                    {
                        digit *= 2;
                        if (digit >= 10)
                        {
                            digit -= 9;
                        }
                    }

                    // Accumulate the checksum value
                    checksum += digit;

                    // It must be modulo 10
                    if (checksum >= 10)
                    {
                        checksum -= 10;
                    }
                }
            }

            // The checksum value is correct if the sum is now zero.
            return checksum == 0;
        }

#if false

        /// <summary>
        /// Small testing mainline if needed
        /// </summary>
        [STAThread]
        public static void Main()
        {
            using (var cls = new AccountNumberValidation())
            {
                cls.IsValidAccount("X0001", "1234");
                cls.IsValidAccount("A0336", "413158002144630");
            }
        }
#endif
    }
}
