﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Svc.Common
{
    /// <summary>
    /// Handle the various error conditions that may occur in the Service or Model layers
    /// </summary>
    public static class ErrorHandling
    {
        /// <summary>
        /// Handle the error conditions for the service layer. The errors are loggede to the
        /// system logging area and nothing more is done about the error. It is simply logged.
        /// </summary>
        /// <param name="ErrorResult">Pointer to the error result from the Repository layer</param>
        /// <returns>The status of the error handling.</returns>
        public static Int32 HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult)
        {
            // Do the standard logging of the exception
            if (ErrorResult.ex != null)
            {
                HandleErrors(ErrorResult.ex);
            }

            // Return some status code that is to be defined later.
            return 0;
        }

        /// <summary>
        /// Handle the error conditions for the service layer. The errors are loggede to the
        /// system logging area and nothing more is done about the error. It is simply logged.
        /// </summary>
        /// <param name="ErrorResult">Pointer to the error result from the Repository layer</param>
        /// <returns>The status of the error handling.</returns>
        public static Int32 HandleErrors(Exception ex)
        {
            // TODO -- log the error condition at this point. Don't display any windows or
            //         forms for the event, just do the logging since this is not a UI routine.

            // Return some status code that is to be defined later.
            return 0;
        }
    }
}
