using DebtPlus;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DebtPlus.Svc.Common
{
    /// <summary>
    /// Calculate the monthly fee for the client given the various parameters for the client.
    /// This is a business layer routine.
    /// </summary>
    public class MonthlyFeeCalculation : DebtPlus.Interfaces.Client.IMonthlyFee, DebtPlus.Interfaces.Client.IClient, IDisposable
    {
        // Local storage
        protected DebtPlus.LINQ.config_fee config_fee_row;

        /// <summary>
        /// Client ID number
        /// </summary>
        private Int32? privateClientId;

        /// <summary>
        /// ID for the config_fees table from the client
        /// </summary>
        private Int32? ConfigFeeFromClient_Answer;

        /// <summary>
        /// Value for the config fee
        /// </summary>
        private Int32? DefaultConfigFee_Answer;

#region Disposable
        // private bool disposedValue;        // To detect redundant calls

        /// <summary>
        /// Dispose of the locally allocated storage
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Dispose of the locally allocated storage
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#endregion

        /// <summary>
        /// Return the truncated value to two decimal places.
        /// </summary>
        /// <remarks>
        /// It must be exact. It must do the floor operation on the item and can
        /// not "round", i.e. 3.082 and 3.086 must both be 3.08 and not 3.09.
        /// </remarks>
        /// <param name="value">Input value to be truncated to two decimal places</param>
        /// <returns>The input value, adjusted so that there are only two decimal places in the number</returns>
        public static Decimal TruncateDecimal(decimal value)
        {
            if (value < 0M)
            {
                throw new ArgumentException("Truncation can not occur on negative values.");
                // return 0M - MyTruncate(0M - value);
            }
            return System.Math.Floor(value * 100M) / 100M;
        }

        /// <summary>
        /// Request a value for the named parameter
        /// </summary>
        /// <remarks></remarks>
        public event DebtPlus.Events.ParameterValueEventHandler QueryValue;

        /// <summary>
        /// Raise the QueryValue event but allow it to be overridden
        /// </summary>
        /// <param name="e">Pointer to the ParameterValueEventArgs structure</param>
        /// <remarks></remarks>
        protected virtual void OnQueryValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            if( QueryValue != null )
            {
                QueryValue(this, e);
            }
        }

        /// <summary>
        /// Raise the QueryValue event
        /// </summary>
        /// <param name="Name">Name of the item desired</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected object RequestQueryValue(string Name)
        {
            DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(Name);
            OnQueryValue(e);
            return e.Value;
        }

        /// <summary>
        /// Raise the QueryValue event
        /// </summary>
        /// <param name="Name">Name of the item desired</param>
        /// <param name="Value">Current value to be used as a default if needed</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected object RequestQueryValue(string Name, object Value)
        {
            DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(Name, Value);
            OnQueryValue(e);
            return e.Value;
        }

        /// <summary>
        /// Ask the caller and the debt list for a value
        /// </summary>
        /// <param name="Name">Name of the item desired</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected object GetValue(string Name)
        {
            return RequestQueryValue(Name);
        }

        /// <summary>
        /// Ask the caller and the debt list for a value
        /// </summary>
        /// <param name="Name">Name of the item desired</param>
        /// <param name="Value">Current value to be used as a default if needed</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected object GetValue(string Name, object Value)
        {
            return RequestQueryValue(Name, Value);
        }

        /// <summary>
        /// Determine the disbursed dollar amount
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected decimal GetDisbursedDollars()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed);
            return TruncateDecimal(DebtPlus.Utils.Nulls.DDec(Answer));
        }

        /// <summary>
        /// Determine the number of creditors disbursed
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Int32 GetCreditorsDisbursed()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_DisbursedCreditors);
            return DebtPlus.Utils.Nulls.DInt(Answer);
        }

        /// <summary>
        /// Should we limit the amount to the monthly maximum?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected bool GetLimitMonthlyMax()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_LimitMonthlyMax, true);
            return DebtPlus.Utils.Nulls.DBool(Answer);
        }

        /// <summary>
        /// How much money do we have to disburse?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected decimal GetAvailableFunds()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_AvailableFunds, 0M);
            return DebtPlus.Utils.Nulls.DDec(Answer);
        }

        /// <summary>
        /// Should we limit the amount to the disbursement maximum?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected bool GetLimitDisbMax()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_LimitDisbMax, true);
            return DebtPlus.Utils.Nulls.DBool(Answer);
        }

        /// <summary>
        /// Calculate the fee amount as a simple percentage of disbursed amount?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected bool GetInProrate()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_InProrate, false);
            return DebtPlus.Utils.Nulls.DBool(Answer);
        }

        /// <summary>
        /// Balance on the current fee record
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected decimal GetBalance()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_Balance);
            return TruncateDecimal(DebtPlus.Utils.Nulls.DDec(Answer));
        }

        /// <summary>
        /// Balance on the scheduled payment amount
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected decimal GetSchedPayment()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_SchedPayment);
            return TruncateDecimal(DebtPlus.Utils.Nulls.DDec(Answer));
        }

        /// <summary>
        /// Determine the dollar amount disbursed this month
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected decimal GetThisMonthDisbursed()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_ThisMonthDisbursement);
            return TruncateDecimal(DebtPlus.Utils.Nulls.DDec(Answer));
        }

        /// <summary>
        /// Determine which config fee record should be used
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected object GetConfigFee()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_ConfigFee);
            return Answer;
        }

        /// <summary>
        /// Determine which config fee record should be used
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Int32 GetClient()
        {
            object Answer = GetValue(DebtPlus.Events.ParameterValueEventArgs.name_client);
            return DebtPlus.Utils.Nulls.DInt(Answer, -1);
        }

        /// <summary>
        /// Client number
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 ClientId
        {
            get
            {
                if( privateClientId == null )
                {
                    privateClientId = GetClient();
                }
                return privateClientId.Value;
            }
            set
            {
                privateClientId = value;
            }
        }

        /// <summary>
        /// Find the configuration fee number from the client record
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Int32 ConfigFeeFromClient()
        {
            // Read the config fee from the client information
            if( ConfigFeeFromClient_Answer == null )
            {
                // Retrieve the information from the client database
                using (var dc = new DebtPlus.LINQ.BusinessContext())
                {
                    ConfigFeeFromClient_Answer = (from c in dc.clients where c.Id == ClientId select c.config_fee).FirstOrDefault();
                }
            }
            return ConfigFeeFromClient_Answer.HasValue ? ConfigFeeFromClient_Answer.Value : -1;
        }

        /// <summary>
        /// Find the default configuration fee record
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Int32 DefaultConfigFee()
        {
            // Find the default item if one is marked. (if(  more than one, take the first one.)
            if (DefaultConfigFee_Answer == null)
            {
                DefaultConfigFee_Answer = (from f in DebtPlus.LINQ.Cache.config_fee.getList() where f.Default == true select f.Id).FirstOrDefault();
                if (DefaultConfigFee_Answer == null)
                {
                    DefaultConfigFee_Answer = (from f in DebtPlus.LINQ.Cache.config_fee.getList() select f.Id).Min();
                    if (DefaultConfigFee_Answer == null)
                    {
                        DefaultConfigFee_Answer = 1;
                    }
                }
            }
            return DefaultConfigFee_Answer.Value;
        }

        /// <summary>
        /// Find the current configuration fee record
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected DebtPlus.LINQ.config_fee GetConfigFeeRow()
        {
            // try to find the item in the client's table
            Int32 ConfigFee = DebtPlus.Utils.Nulls.DInt(GetConfigFee());
            if (ConfigFee <= 0)
            {
                ConfigFee = ConfigFeeFromClient();
                if (ConfigFee <= 0)
                {
                    ConfigFee = DefaultConfigFee();
                }
            }

            // Find the row corresponding to the fee
            DebtPlus.LINQ.config_fee fee = (from f in DebtPlus.LINQ.Cache.config_fee.getList() where f.Id == ConfigFee select f).FirstOrDefault();
            if (fee == null)
            {
                ConfigFee = DefaultConfigFee();
                if (ConfigFee >= 1)
                {
                    fee = (from f in DebtPlus.LINQ.Cache.config_fee.getList() where f.Id == ConfigFee select f).FirstOrDefault();
                }
            }
            return fee;
        }

        /// <summary>
        /// Permit fee calculation based upon a flat rate amount?
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual bool ShouldCalculateFixedFee
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Permit fee calculation based upon the dollars disbursed?
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual bool ShouldCalculatePercentageFee
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Permit fee calculation based upon the creditors disbursed?
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual bool ShouldCalculateCreditorFee
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Calculate the fee amount for the configuration fee for this client.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual decimal FeeAmount()
        {
            decimal Answer = 0M;

            // try to find the fee information for the client
            config_fee_row = GetConfigFeeRow();
            if (config_fee_row != null)
            {
                // From the row, determine the type of processing desired
                switch (config_fee_row.fee_type)
                {
                    case 1:      // The amount is fixed at the monthly max figure
                        Answer = GetSchedPayment();

                        if( ShouldCalculateFixedFee )
                        {
                            // Limit the figure to the balance of the debt if required
                            if( config_fee_row.fee_balance_max )
                            {
                                decimal Balance = GetBalance();
                                if( Balance < Answer )
                                {
                                    Answer = Balance;
                                }
                            }
                        }
                        break;

                    case 2:      // Percentage of the dollars disbursed
                        if( ! ShouldCalculatePercentageFee )
                        {
                            Answer = GetSchedPayment();
                        }
                        else
                        {
                            Double FeePercent = config_fee_row.fee_percent ?? 0D;
                            if( FeePercent > 0.0D )
                            {
                                if( FeePercent >= 1.0D )
                                {
                                    FeePercent /= 100.0D;
                                }

                                decimal DollarsDisbursed = GetDisbursedDollars();
                                if( DollarsDisbursed > 0M )
                                {
                                    if( GetInProrate() )
                                    {
                                        double PartialFee = Convert.ToDouble(GetAvailableFunds()) / (1.0F + FeePercent);

                                        // Yes, we want the CEIL function, not the FLOOR function because we subtract it in the next statement!
                                        decimal DecimalPartialFee = System.Math.Ceiling(Convert.ToDecimal(PartialFee) * 100M) / 100M;

                                        Answer = GetAvailableFunds() - DecimalPartialFee;
                                    }
                                    else
                                    {
                                        double PartialFee = Convert.ToDouble(DollarsDisbursed) * Convert.ToDouble(FeePercent);
                                        decimal DecimalPartialFee = Convert.ToDecimal(PartialFee);
                                        Answer = TruncateDecimal(DecimalPartialFee);
                                    }
                                }
                            }
                        }
                        break;

                    case 3:      // Figure for each disbursed creditor
                        if( ! ShouldCalculateCreditorFee )
                        {
                            Answer = GetSchedPayment();
                        }
                        else
                        {
                            decimal FeeBase = config_fee_row.fee_base;
                            decimal FeePerCreditor = config_fee_row.fee_per_creditor ?? 0M;

                            if( FeeBase < 0M )
                            {
                                FeeBase = 0M;
                            }
                            if( FeePerCreditor < 0M )
                            {
                                FeePerCreditor = 0M;
                            }
                            Int32 CreditorsDisbursed = GetCreditorsDisbursed();
                            if( CreditorsDisbursed > 0 )
                            {
                                Answer = TruncateDecimal((Convert.ToDecimal(CreditorsDisbursed) * FeePerCreditor) + FeeBase);
                            }
                        }
                        break;

                    default:       // 0: the amount is waived
                        return Answer;
                }

                // Limit the amount to the maximum for the disbursement
                if( GetLimitDisbMax() )
                {
                    decimal DisbursementMax = config_fee_row.fee_disb_max ?? 0M;
                    if( DisbursementMax > 0M )
                    {
                        if( Answer > DisbursementMax )
                        {
                            Answer = DisbursementMax;
                        }
                    }
                }

                // Limit the disbursement to the monthly maximum if required
                if( GetLimitMonthlyMax() )
                {
                    decimal FeeMonthlyMax = config_fee_row.fee_monthly_max ?? 0M;
                    if( FeeMonthlyMax > 0M )
                    {
                        decimal CurrentMonthDisbursement = GetThisMonthDisbursed();
                        if( Answer + CurrentMonthDisbursement > FeeMonthlyMax )
                        {
                            Answer = FeeMonthlyMax - CurrentMonthDisbursement;
                            if( Answer < 0M )
                            {
                                Answer = 0M;
                            }
                        }
                    }
                }

                // The final limit is the maximum for the fee. A fee never exceeds this limit
                decimal FeeMaximum = config_fee_row.fee_maximum ?? 0M;
                if( FeeMaximum > 0M )
                {
                    if( Answer > FeeMaximum )
                    {
                        Answer = FeeMaximum;
                    }
                }
            }

            // return the value to the caller once we have calculated it
            return Answer;
        }
    }
}
