using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Widgets.Search
{
    public partial class VendorSearchClass : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Vendor.IVendorSearch
    {
        // Category values
        private const string Category_Vendor = "vendors";
        private const string Category_Alias = "name_or_alias";
        private const string Category_Name = "name";
        private const string Category_Telephone = "telephone";
        private const string Category_Address = "vendor_address";
        private const string Category_AccountNumber = "vendor_account_number";

        // Values used for the relation (2nd input field)
        private const string relation_EQ = "=";
        private const string relation_GE = ">=";
        private const string relation_GT = ">";
        private const string relation_LT = "<";
        private const string relation_LE = "<=";
        private const string relation_NE = "not =";
        private const string relation_between = "between";
        private const string relation_not_between = "not between";
        private const string relation_contains = "contains";
        private const string relation_not_contains = "not contain";
        private const string relation_ends = "ends";
        private const string relation_starts = "starts";
        private const string relation_not_starts = "not starts";

        // MRU keys
        private const string MRU_Vendor = "vendors";
        private const string MRU_Alias = "Vendor Alias";
        private const string MRU_Name = "Vendor Name";
        private const string MRU_AccountNumber = "Vendor Account No";
        private const string MRU_Telephone = "Vendor Telephone";
        private const string MRU_Address = "Vendor Address";

        // Information about the Vendor search
        private Int32 rowLimit;
        private Int32? lastVendor = null;
        private Int32? privateVendor = null;
        private System.Collections.Generic.List<SearchData> colSearchData = null;

        /// <summary>
        /// Class of items returned in the search operation.
        /// </summary>
        internal class SearchData
        {
            public SearchData() { }
            public Int32 Id { get; set; }           // Primary key to the table
            public string vendor { get; set; }      // Label field
            public string Name { get; set; }        // Vendor name
            public bool ActiveFlag { get; set; }    // Is the record active or not?
            public string address { get; set; }     // General Address information
        }

        /// <summary>
        /// Dispose of the class when it is desired
        /// </summary>
        /// <param name="disposing"></param>
        private void VendorSearchClass_Dispose(bool disposing)
        {
            // Remove the event handlers
            if (disposing && DebtPlus.Configuration.DesignMode.IsInDesignMode())
            {
                UnRegisterHandlers();
                if (colSearchData != null)
                {
                    colSearchData.RemoveAll(x => true);
                }
            }

            // Remove the pointers to the larger structures
            colSearchData = null;
        }

        /// <summary>
        /// Event to validate the Vendor selection
        /// </summary>
        protected void RaiseValidating(System.ComponentModel.CancelEventArgs e)
        {
            OnValidating(e);
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        public VendorSearchClass()
            : base()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();

                // Ensure that our window is not hidden.
                Form frm = Form.ActiveForm;
                if (frm != null && !frm.InvokeRequired && frm.TopMost)
                {
                    TopMost = true;
                }
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            // Form Load event
            this.Load += VendorSearchClass_Load;

            // OK button click event
            Button_OK.Click += Button_OK_Click;

            // Handle the changes in the relations
            Combo_Category.SelectedIndexChanged += Combo_Category_SelectedIndexChanged;
            Combo_Relation.SelectedIndexChanged += Combo_Relation_SelectedIndexChanged;

            // Select and trim the text fields
            Combo_Value1.Enter += Combo_Value1_Enter;
            Combo_Value2.Enter += Combo_Value1_Enter;
            Combo_Value1.Leave += Combo_Value1_Leave;
            Combo_Value2.Leave += Combo_Value1_Leave;

            // Form changed events.
            Combo_Category.SelectedValueChanged += Form_Changed;
            Combo_Relation.SelectedValueChanged += Form_Changed;
            Combo_Value1.SelectedIndexChanged += Form_Changed;
            Combo_Value1.TextChanged += Form_Changed;
            Combo_Value2.TextChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            // Form Load event
            this.Load -= VendorSearchClass_Load;

            // OK button click event
            Button_OK.Click -= Button_OK_Click;

            // Handle the changes in the relations
            Combo_Category.SelectedIndexChanged -= Combo_Category_SelectedIndexChanged;
            Combo_Relation.SelectedIndexChanged -= Combo_Relation_SelectedIndexChanged;

            // Select and trim the text fields
            Combo_Value1.Enter -= Combo_Value1_Enter;
            Combo_Value2.Enter -= Combo_Value1_Enter;
            Combo_Value1.Leave -= Combo_Value1_Leave;
            Combo_Value2.Leave -= Combo_Value1_Leave;

            // Form changed events.
            Combo_Category.SelectedValueChanged -= Form_Changed;
            Combo_Relation.SelectedValueChanged -= Form_Changed;
            Combo_Value1.SelectedIndexChanged -= Form_Changed;
            Combo_Value1.TextChanged -= Form_Changed;
            Combo_Value2.TextChanged -= Form_Changed;
        }

        /// <summary>
        /// The resulting vendor ID from the search operation. Valid only when
        /// the search is successful.
        /// </summary>
        public int? Vendor
        {
            get
            {
                return privateVendor;
            }
            set
            {
                // If there is no value then there is no value
                if (!value.HasValue)
                {
                    privateVendor = null;
                    return;
                }

                // Compare the value to an acceptable condition.
                if (System.Nullable.Compare<System.Int32>(privateVendor, value) != 0)
                {
                    System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(false);
                    Int32? lastValue = privateVendor;
                    privateVendor = value;
                    RaiseValidating(e);

                    // If the user did not like the new value then don't accept it.
                    if (e.Cancel)
                    {
                        privateVendor = lastValue;
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Try to complete the form with the new vendor ID
        /// </summary>
        /// <param name="NewVendor"></param>
        /// <returns></returns>
        protected void SetVendor(Int32 NewVendor)
        {
            Vendor = NewVendor;
            if (Vendor == NewVendor)
            {
                DefaultCategory = CategoryLabel;
                DefaultRelation = RelationLabel;
                DialogResult    = System.Windows.Forms.DialogResult.OK;
            }
        }

        #region Combobox Values
        /// <summary>
        /// Which item is selected for the category
        /// </summary>
        private string CategoryLabel
        {
            get
            {
                string Answer = string.Empty;
                if (Combo_Category.SelectedIndex >= 0)
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)Combo_Category.SelectedItem).value);
                }
                return Answer;
            }
        }

        /// <summary>
        /// Which item is selected for the relation
        /// </summary>
        private string RelationLabel
        {
            get
            {
                string Answer = string.Empty;
                if (Combo_Relation.SelectedIndex >= 0)
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)Combo_Relation.SelectedItem).value);
                }
                return Answer;
            }
        }
        #endregion

        /// <summary>
        /// Translate the category selection to an MRU key
        /// </summary>
        private string GetMRUKey
        {
            get
            {
                switch (CategoryLabel)
                {
                    case Category_Address:
                        return MRU_Address;

                    case Category_Alias:
                        return MRU_Alias;

                    case Category_Vendor:
                        return MRU_Vendor;

                    case Category_Name:
                        return MRU_Name;

                    case Category_Telephone:
                        return MRU_Telephone;

                    case Category_AccountNumber:
                        return MRU_AccountNumber;

                    default:
                        throw new ArgumentOutOfRangeException("CategoryLabel");
                }
            }
        }

        #region Form Navigation
        /// <summary>
        /// Load the category combo box with the proper values
        /// </summary>
        private void Load_Category()
        {
            Combo_Category.Properties.Items.Clear();

            // Add the item to the list. This is the default value.
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Vendor ID", Category_Vendor));

            // Add the other values to the list
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client Reference Number", Category_AccountNumber));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Vendor Name or Alias", Category_Alias));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Vendor Name", Category_Name));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Vendor Telephone Number", Category_Telephone));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Vendor Address", Category_Address));

            // Set the default item to the last displayed category
            string DefaultItem = DefaultCategory;
            foreach (DebtPlus.Data.Controls.ComboboxItem item in Combo_Category.Properties.Items)
            {
                if (string.Compare(Convert.ToString(item.value), DefaultItem, true) == 0)
                {
                    Combo_Category.SelectedItem = item;
                    break;
                }
            }

            // Choose the first item in the list if there is no default item
            if (Combo_Category.SelectedItem == null)
            {
                Combo_Category.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Load the relation combo box with the proper values
        /// </summary>
        private void Load_Relation()
        {
            Combo_Relation.Properties.Items.Clear();
            switch (CategoryLabel)
            {
                case Category_Vendor:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is atleast", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_between, relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_not_between, relation_not_between));
                    break;

                case Category_Alias:
                case Category_Name:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_contains, relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                case Category_Telephone:
                case Category_Address:
                case Category_AccountNumber:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is atleast", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_between, relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_not_between, relation_not_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_contains, relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                default:
                    throw new ArgumentOutOfRangeException("CategoryLabel");
            }

            // Set the default item to the last displayed category
            string DefaultItem = DefaultRelation;
            foreach (DebtPlus.Data.Controls.ComboboxItem item in Combo_Relation.Properties.Items)
            {
                if (string.Compare(Convert.ToString(item.value), DefaultItem, true) == 0)
                {
                    Combo_Relation.SelectedItem = item;
                    break;
                }
            }

            // Choose the first item in the list if there is no default item
            if (Combo_Relation.SelectedItem == null)
            {
                Combo_Relation.SelectedIndex = 0;
            }

            // Load the combo lists
            Load_Value_1();
            Load_Value_2();

            // Hide the second value box until it is selected by the relation
            label_and.Visible = false;
            Combo_Value2.Visible = false;
        }

        /// <summary>
        /// The starting selection criteria is stored in the registry
        /// </summary>
        private string DefaultCategory
        {
            get
            {
                string Answer = string.Empty;
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    reg = reg.OpenSubKey(Path, false);
                    if (reg != null)
                    {
                        Answer = Convert.ToString(reg.GetValue("vendor search method"));
                    }
                }
                catch { }
                finally
                {
                    if (reg != null)
                    {
                        reg.Close();
                    }
                }

                // Default the answer
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = Category_Vendor;
                }
                return Answer;
            }

            set
            {
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Path);
                if (reg != null)
                {
                    reg.SetValue("Vendor search method", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    reg.Close();
                }
            }
        }

        /// <summary>
        /// The starting relation for the form is stored in the registry
        /// </summary>
        private string DefaultRelation
        {
            get
            {
                string Answer = string.Empty;
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, false);
                    if (reg != null)
                    {
                        Answer = Convert.ToString(reg.GetValue("Vendor search relation"));
                    }
                }
                catch { }
                finally
                {
                    if (reg != null)
                    {
                        reg.Close();
                    }
                }

                // Default the answer
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = relation_EQ;
                }
                return Answer;
            }

            set
            {
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, true);
                    if (reg != null)
                    {
                        reg.SetValue("Vendor search relation", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    }
                }
                catch { }
                finally
                {
                    if (reg != null)
                    {
                        reg.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Load the list of valid items into the first input field
        /// </summary>
        private void Load_Value_1()
        {
            // Load the list of previous Vendors. Select the first one.
            string SavedText = Combo_Value1.Text;
            Combo_Value1.SelectedIndex = -1;
            Combo_Value1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            Combo_Value1.Properties.ReadOnly = false;
            Combo_Value1.Properties.Items.Clear();

            if (CategoryLabel == Category_Vendor)
            {
                // Do special processing on the tags for the Vendor
                using (var mruList = new DebtPlus.Data.MRU(MRU_Vendor, "Recent vendors"))
                {
                    string[] lst = mruList.GetList;
                    foreach (string item in lst)
                    {
                        Combo_Value1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ComboBoxItem(item));
                    }
                }
            }
            else
            {
                // Otherwise, load the standard list of items from the MRU list
                using (var mruList = new DebtPlus.Data.MRU(GetMRUKey))
                {
                    string[] lst = mruList.GetList;
                    foreach (string item in lst)
                    {
                        Combo_Value1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ComboBoxItem(item));
                    }
                }
            }

            // Restore the text for the item
            Combo_Value1.SelectedIndex = -1;
            Combo_Value1.Text = SavedText;
        }

        /// <summary>
        /// Load the list of valid items into the second input field
        /// </summary>
        private void Load_Value_2()
        {
            // Don't do anything here. We want to preserve the text field.
        }

        /// <summary>
        /// Process the LOAD event for the FORM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VendorSearchClass_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            // Save the maximum number for display since it is a static value.
            rowLimit = SelectLimitCount;

            try
            {
                // Load the comboboxes
                Load_Category();                    // Load the category information
                Load_Relation();                    // Load the relation list
                EnableOK();                         // Enable/Disable the OK button
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void Combo_Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// When the relationship control changes, enable or disable the second input field.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Relation_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Enable the second input field if needed
            bool ShouldEnable = IsValue2Enabled();
            label_and.Visible = ShouldEnable;
            Combo_Value2.Visible = ShouldEnable;

            // Load the values based upon the new relation
            Load_Value_1();

            // Check for the OK button being valid now.
            EnableOK();
        }

        /// <summary>
        /// Should the second input field be displayed?
        /// </summary>
        /// <returns></returns>
        private bool IsValue2Enabled()
        {
            // If the item is relation_between or relation_not_between then enable the second value
            switch (RelationLabel)
            {
                case relation_between:
                case relation_not_between:
                    return true;

                default:
                    return false;
            }
        }
        #endregion

        #region Form Validation
        /// <summary>
        /// Enable or disable the OK button as needed
        /// </summary>
        private void EnableOK()
        {
            // Enable or disable the checkbox based upon the criteria
            if (CategoryLabel == Category_Vendor && CategoryLabel == relation_EQ)
            {
                Check_InactiveVendors.Enabled = false;
            }
            else
            {
                Check_InactiveVendors.Enabled = true;
            }

            // Enable the OK button based upon the error condition on the form
            Button_OK.Enabled = true;//!HasErrors();
        }

        /// <summary>
        /// Determine if there are errors in the input fields.
        /// </summary>
        /// <returns>TRUE if there is an error. FALSE if the form is valid.</returns>
        private bool HasErrors()
        {
            string RelationSelected = RelationLabel;
            switch (CategoryLabel)
            {
                case Category_Vendor:
                    if (!valid_Vendor(Combo_Value1.Text))
                    {
                        return true;
                    }
                    if (IsValue2Enabled() && !valid_Vendor(Combo_Value2.Text))
                    {
                        return true;
                    }
                    break;

                case Category_Telephone:
                    if (!valid_phone(Combo_Value1.Text))
                    {
                        return true;
                    }
                    if (IsValue2Enabled() && !valid_phone(Combo_Value2.Text))
                    {
                        return true;
                    }
                    break;

                default:
                    if (!valid_string(Combo_Value1.Text))
                    {
                        return true;
                    }
                    if (IsValue2Enabled() && !valid_string(Combo_Value2.Text))
                    {
                        return true;
                    }
                    break;
            }

            return false;
        }

        /// <summary>
        /// Look for a valid vendor ID or label
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_Vendor(string Inputstring)
        {
            // The Vendor ID may be a number. Accept valid non-negative numbers
            Int32 Result;
            if (Int32.TryParse(Inputstring, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out Result))
            {
                return (Result >= 0);
            }

            // Use the name if one is referenced
            return valid_Vendor_label(Inputstring);
        }

        /// <summary>
        /// Look for a valid Vendor label.
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_Vendor_label(string Inputstring)
        {
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("^[A-Za-z]{1,2}[0-9][0-9][0-9][0-9]+$");
            return rx.IsMatch(Inputstring);
        }

        /// <summary>
        /// Look for a valid telephone number
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_phone(string Inputstring)
        {
            switch (RelationLabel)
            {
                case relation_contains:
                case relation_not_contains:
                case relation_starts:
                case relation_not_starts:
                case relation_ends:
                    return valid_string(Inputstring);    // Portion only. Do not validate the whole string.

                default:
                    return valid_string(Inputstring);    // Validate the whole telephone number
            }
        }

        /// <summary>
        /// Look for a valid text string
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_string(string Inputstring)
        {
            return Inputstring.Trim().Length > 0;
        }

        #endregion

        #region Combo_Value
        /// <summary>
        /// Select the string when the control enters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Value1_Enter(object sender, EventArgs e)
        {
            ((DevExpress.XtraEditors.ComboBoxEdit)sender).SelectAll();
        }

        /// <summary>
        /// Trim the string when the control is left
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Value1_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit ctl = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            ctl.Text = ctl.Text.Trim();
        }

        /// <summary>
        /// Handle the change in the form data. Control the OK button enabling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
        {
            EnableOK();
        }
        #endregion

        #region BUILD selection statement
        /// <summary>
        /// Create the valid constant for the search operation
        /// </summary>
        /// <param name="ctl">Control to retrieve the string</param>
        /// <returns></returns>
        private string Item_Value(string ctl)
        {
            return Item_Value(ctl, string.Empty, string.Empty);
        }

        /// <summary>
        /// Create the valid constant for the search operation
        /// </summary>
        /// <param name="ctl">Control to retrieve the string</param>
        /// <param name="strPrefix">Leading characters for the character mask</param>
        /// <param name="strSuffix">Trailing characters for the character mask</param>
        /// <returns></returns>
        private string Item_Value(string ctl, string strPrefix, string strSuffix)
        {
            switch (CategoryLabel)
            {
                case Category_Telephone:
                    return string.Format("'{0}{1}{2}'", strPrefix, DebtPlus.Utils.Format.Strings.DigitsOnly(ctl), strSuffix);

                default:
                    return string.Format("'{0}{1}{2}'", strPrefix, ctl.Trim().Replace("'", "''"), strSuffix);
            }
        }

        /// <summary>
        /// Construct the relation for the key to the constant
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string Relation_Value(string key)
        {
            string Answer = string.Empty;
            switch (RelationLabel)
            {
                case relation_EQ:
                    Answer = string.Format("{0}={1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_LT:
                    Answer = string.Format("{0}<{1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_LE:
                    Answer = string.Format("{0}<={1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_GT:
                    Answer = string.Format("{0}>{1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_GE:
                    Answer = string.Format("{0}>={1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_NE:
                    Answer = string.Format("NOT {0}={1}", key, Item_Value(Combo_Value1.Text));
                    break;

                case relation_starts:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;

                case relation_ends:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", string.Empty));
                    break;

                case relation_not_starts:
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;

                case relation_contains:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;

                case "not contains":
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;

                case relation_between:
                    Answer = string.Format("{0} BETWEEN {1} AND {2}", key, Item_Value(Combo_Value1.Text), Item_Value(Combo_Value2.Text));
                    break;

                default:
                    break;
            }

            return Answer;
        }

        /// <summary>
        /// Selection criteria when the search is for a Vendor ID
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        private string Fetch_Vendor(string strName)
        {
            return Relation_Value(strName);
        }

        /// <summary>
        /// Selection criteria when the search is against the alias/prefix table
        /// </summary>
        /// <returns></returns>
        private string Fetch_Alias()
        {
            return string.Format("(({0}) OR (v.oID IN (SELECT vendor FROM vendor_addkeys WITH (NOLOCK) WHERE {1})))", Relation_Value("v.name"), Relation_Value("additional"));
        }

        /// <summary>
        /// Selection criteria when the search is against the telephone number table
        /// </summary>
        /// <returns></returns>
        private string Fetch_ContactPhone()
        {
            return string.Format("v.vendor in (select vendor FROM vendor_contacts vc WITH (NOLOCK) inner join TelephoneNumbers t1 WITH (NOLOCK) on t1.TelephoneNumber = vc.TelephoneID where {0})", Relation_Value("t1.SearchValue"));
        }

        /// <summary>
        /// Selection criteria when the search is against the vendors table for the vendor name.
        /// </summary>
        /// <returns></returns>
        private string Fetch_Name()
        {
            return Relation_Value("v.Name");
        }

        /// <summary>
        /// Selection criteria when the search is for the account number (or reference field)
        /// </summary>
        /// <returns></returns>
        private string Fetch_ClientProduct(string field)
        {
            return string.Format("v.oID IN (SELECT vendor FROM client_products prod WITH (NOLOCK) WHERE {0})", Relation_Value("prod." + field));
        }

        /// <summary>
        /// Is this a vendor number or label?
        /// </summary>
        /// <returns>TRUE if number. FALSE If label.</returns>
        private bool use_vendor_id()
        {
            Int32 TestNumber = Int32.MinValue;
            return Int32.TryParse(Combo_Value1.Text, out TestNumber) && TestNumber > 0;
        }

        /// <summary>
        /// Build the select statement for the list of vendors
        /// </summary>
        /// <returns></returns>
        private string SelectStatement(Int32? LastVendor)
        {
            string SelectClause = string.Empty;

            switch (CategoryLabel)
            {
                case Category_Name:
                    SelectClause = Fetch_Name();
                    break;

                case Category_Alias:
                    SelectClause = Fetch_Alias();
                    break;

                case Category_Vendor:
                    if (use_vendor_id())
                    {
                        SelectClause = Fetch_Vendor("v.oID");
                    }
                    else
                    {
                        SelectClause = Fetch_Vendor("v.Label");
                    }
                    break;

                case Category_AccountNumber:
                    SelectClause = Fetch_ClientProduct("reference");
                    break;

                case Category_Address:
                    SelectClause = Fetch_Vendor("dbo.format_address_block(va.addressID)");
                    break;

                case Category_Telephone:
                    SelectClause = Fetch_ContactPhone();
                    break;

                default:
                    System.Diagnostics.Debug.Assert(false);
                    return string.Empty;
            }

            if (Check_InactiveVendors.CheckState != CheckState.Unchecked)
            {
                SelectClause += " AND (v.ActiveFlag<>0)";
            }

            // Start with the basic verb
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("SELECT ");

            // If there is a limit then limit the row set
            if (rowLimit > 0)
            {
                sb.AppendFormat("TOP {0:f0} ", (rowLimit + 1));
            }

            // Build the standard component of the statement
            sb.Append("v.oID as Id, v.Label as vendor, v.Name, v.ActiveFlag, dbo.format_address_block(va.addressID) AS address ");
            sb.Append("FROM vendors v LEFT OUTER JOIN vendor_addresses va ON v.oID = va.vendor AND 'G' = va.address_type ");

            if (LastVendor.HasValue && !string.IsNullOrWhiteSpace(SelectClause))
            {
                sb.AppendFormat(" WHERE ({1}) AND (v.oID >= '{0:f0}')", LastVendor.Value, SelectClause);
            }
            else
            {
                if (LastVendor.HasValue)
                {
                    sb.AppendFormat(" WHERE v.oID >= {0:f0}", LastVendor.Value);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(SelectClause))
                    {
                        sb.Append(" WHERE ");
                        sb.Append(SelectClause);
                    }
                }
            }

            sb.Append(" ORDER BY 1");
            return sb.ToString();
        }

        /// <summary>
        /// Handle the OK button CLICK event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            // If the entry is an ID and equality, just look up the vendor in the table.
            if (CategoryLabel == Category_Vendor && RelationLabel == relation_EQ)
            {
                string vendorID = Combo_Value1.Text.Trim();
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    // Look for a numeric response. if so, translate the number to a label.
                    if (System.Text.RegularExpressions.Regex.IsMatch(vendorID, @"^\d+$"))
                    {
                        Int32 searchID = default(Int32);
                        if (Int32.TryParse(vendorID, out searchID))
                        {
                            var q = bc.vendors.Where(s => s.Id == searchID).FirstOrDefault();
                            if (q != null)
                            {
                                SetVendor(q.Id);
                                return;
                            }
                        }
                    }

                    else
                    {
                        // Lookup the vendor by the label field
                        var q = bc.vendors.Where(s => s.Label == vendorID).FirstOrDefault();
                        if (q != null)
                        {
                            SetVendor(q.Id);
                            return;
                        }
                    }
                }

                DebtPlus.Data.Forms.MessageBox.Show("The Vendor is not valid. Please choose an existing Vendor.", "Information ! Found", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                return;
            }

            // Start with the initial search criteria
            colSearchData = LoadSearchList(SelectStatement(null), new object[] {});
            if (colSearchData == null)
            {
                return;
            }

            if (colSearchData.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no vendors which match your search criteria.\r\nPlease change the selection and try again or press\r\nCancel to return to the previous screen.", "No vendors could be found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // If only one row matched then stop here
            if (colSearchData.Count == 1)
            {
                SetVendor(colSearchData[0].Id);
                return;
            }

            // Display the initial search form
            using (var FormVendorList = new VendorListForm())
            {
                FormVendorList.NextPage += FormVendorList_NextPage;

                // Load the first page of vendors into the form
                LoadNextPage(FormVendorList);

                // Ask the user to choose a vendor at this point.
                if (FormVendorList.ShowDialog() == System.Windows.Forms.DialogResult.OK && FormVendorList.Vendor > 0)
                {
                    SetVendor(FormVendorList.Vendor);
                }
                FormVendorList.NextPage -= FormVendorList_NextPage;
                return;
            }
        }
        #endregion

        #region formVendorList Events
        /// <summary>
        /// Process the form's MORE button when it is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormVendorList_NextPage(object sender, EventArgs e)
        {
            colSearchData = LoadSearchList(SelectStatement(lastVendor), new object[] {});
            if (colSearchData != null)
            {
                LoadNextPage((VendorListForm)sender);
            }
        }

        /// <summary>
        /// Load the next page of vendors into the search function
        /// </summary>
        /// <param name="formCreditorList"></param>
        private void LoadNextPage(VendorListForm FormVendorList)
        {
            FormVendorList.Button_More.Visible = false;

            // If there are more than the limit in the search then enable the more button
            if (rowLimit > 0 && colSearchData.Count > rowLimit)
            {
                var q = colSearchData[rowLimit];
                lastVendor = q.Id;
                colSearchData.Remove(q);

                FormVendorList.Button_More.Visible = true;
            }

            // Load the datasource for the view.
            FormVendorList.GridControl1.DataSource = colSearchData;
            FormVendorList.GridControl1.RefreshDataSource();
        }
        #endregion

        #region Load the Vendor List Form Items
        /// <summary>
        /// The search limit for finding matches against the vendor criteria
        /// </summary>
        private Int32 SelectLimitCount
        {
            get
            {
                Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;
                Int32 Result = 1000;

                // Look in the machine values for the defaults for all users
                try
                {
                    RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\options", false);
                    if (RegKey != null)
                    {
                        object obj = RegKey.GetValue("Vendor search limit", string.Empty);
                        Int32 Answer = -1;
                        if (obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0)
                        {
                            Result = Answer;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }

                // Allow the user to override the machine settings
                RegKey = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    if (RegKey != null)
                    {
                        object obj = RegKey.GetValue("vendor search limit", string.Empty);
                        Int32 Answer = -1;
                        if (obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0)
                        {
                            Result = Answer;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }
                return Result;
            }
        }

        /// <summary>
        /// Rebuild the list of Vendors from the database
        /// </summary>
        private System.Collections.Generic.List<SearchData> LoadSearchList(string Statement, object[] parameters)
        {
            using (new Common.CursorManager())
            {
                try
                {
                    using (var bc = new BusinessContext())
                    {
                        var Result = bc.ExecuteQuery<SearchData>(Statement, parameters);
                        if (Result == null)
                        {
                            return new System.Collections.Generic.List<SearchData>();
                        }

                        // Convert the item to the collection of answers
                        return Result.Select(s => new SearchData() { ActiveFlag = s.ActiveFlag, address = s.address, Id = s.Id, Name = s.Name, vendor = s.vendor }).ToList();
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving vendor list");
                }

                return null;
            }
        }
        #endregion

        /// <summary>
        /// Save the current history for the next invocation
        /// </summary>
        /// <param name=Category_Creditor>The vendor ID which was selected</param>
        /// <remarks></remarks>
        private void Save_Recent(string VendorID)
        {
            // First, save the vendor ID
            using (var mruList = new DebtPlus.Data.MRU("Vendors"))
            {
                mruList.InsertTopMost(VendorID);
                mruList.SaveKey();
            }

            // Now, save the category relative item into the list.
            if (CategoryLabel != Category_Vendor)
            {
                using (var mruList = new DebtPlus.Data.MRU(GetMRUKey))
                {
                    mruList.InsertTopMost(Combo_Value1.Text.Trim());
                    mruList.SaveKey();
                }
            }
        }

        /// <summary>
        /// Perform the vendor search operation
        /// </summary>
        /// <param name="owner">Owner for the windows</param>
        /// <returns>The dialog result status for the search operation</returns>
        public DialogResult PerformVendorSearch(IWin32Window owner)
        {
            return this.ShowDialog(owner);
        }

        /// <summary>
        /// Perform the vendor search operation
        /// </summary>
        /// <returns>The dialog result status for the search operation</returns>
        public DialogResult PerformCreditorSearch()
        {
            return this.ShowDialog();
        }
    }
}
