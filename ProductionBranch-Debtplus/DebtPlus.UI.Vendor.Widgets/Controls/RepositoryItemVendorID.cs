using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;

namespace DebtPlus.UI.Vendor.Widgets.Controls
{
    [UserRepositoryItem("Register")]
    public class RepositoryItemVendorId : RepositoryItemButtonEdit
    {
        /// <summary>
        /// Register the control information when the control is initially created the first time.
        /// </summary>
        /// <remarks></remarks>
        static RepositoryItemVendorId()
        {
            Register();
        }

        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(VendorID), typeof(RepositoryItemVendorId), typeof(DevExpress.XtraEditors.ViewInfo.ButtonEditViewInfo), new DevExpress.XtraEditors.Drawing.ButtonEditPainter(), true));
        }

        public RepositoryItemVendorId()
        {
        }

        internal const string EditorName = "VendorId";

        /// <summary>
        /// Name for the editor. The specific created items use this string as a base name.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string EditorTypeName
        {
            get
            {
                return EditorName;
            }
        }

		public override void CreateDefaultButton()
        {
            Buttons.Add(new DebtPlus.UI.Common.Controls.SearchButton(true));
		}
    }
}
