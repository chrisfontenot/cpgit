#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.ComponentModel;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Widgets.Controls
{
    [System.Xml.Serialization.XmlType(Namespace = "urn:Vendor")]
    [System.Xml.Serialization.XmlRoot(Namespace = "urn:Vendor")]
    public partial class VendorID : DevExpress.XtraEditors.ButtonEdit
    {
        static VendorID()
        {
            RepositoryItemVendorId.Register();
        }

        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemVendorId.EditorName;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemVendorId Properties
        {
            get
            {
                return base.Properties as RepositoryItemVendorId;
            }
        }

        /// <summary>
        /// Handle the creation of the control
        /// </summary>
        public VendorID() : base()
        {
            InitializeComponent();
            EditValue = 0;
            TabOnSearch = true;

            // Force the character casing to be upper case. It looks better.
            Properties.CharacterCasing = CharacterCasing.Upper;

            try  //This may cause an error for some cases. Don't trap on the error condition.
            {
                AllowDrop = true;
            }
            catch { }
            RegisterHandlers();
        }

        /// <summary>
        /// Add any event handler registration
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove all events added by RegisterHandlers
        /// </summary>
        private void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// Validation that we perform on the creditor ID
        /// </summary>
        [System.Xml.Serialization.XmlType(Namespace = "urn:Vendor")]
        [System.Xml.Serialization.XmlRoot(Namespace = "urn:Vendor")]
        public enum ValidationLevelEnum
        {
            None           = 0,
            Exists         = 1,
            NotProhibitUse = 2
        }

        /// <summary>
        /// Should we do a tab operation when search is complete
        /// </summary>
        [Description("Should next control be selected when search is completed"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(bool), "true")]
        public bool TabOnSearch { get; set; }

        /// <summary>
        /// Raised when we change the validation level
        /// </summary>
        public event EventHandler ValidationLevelChanged;
        protected void RaiseValidationLevelChanged(EventArgs e)
        {
            EventHandler evt = ValidationLevelChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnValidationLevelChanged(EventArgs e)
        {
            RaiseValidationLevelChanged(e);
        }

        private ValidationLevelEnum _ValidationLevel = ValidationLevelEnum.None;

        /// <summary>
        /// Validation that we perform on the creditor ID
        /// </summary>
        [Description("Types of validation that we perform"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(ValidationLevelEnum), "None")]
        public ValidationLevelEnum ValidationLevel
        {
            get
            {
                return _ValidationLevel;
            }
            set
            {
                if (value != _ValidationLevel)
                {
                    _ValidationLevel = value;
                    OnValidationLevelChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// return the creditor ID as a string value
        /// </summary>
        [Description("Decoded edit value for the vendor ID"), Category("Data"), Browsable(true), DefaultValue(typeof(System.Nullable<Int32>), "null")]
        public new Int32? EditValue
        {
            get
            {
                return (Int32?) base.EditValue;
            }

            set
            {
                base.EditValue = value;
            }
        }

        #region  drag/drop
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // Do nothing if the operation is not valid
            if ( (e.Button == 0) || (e.X >= 0 && e.X < Width && e.Y >= 0 && e.Y < Height) )
            {
                return;
            }

            string txt;

            // if this is a text box then look for the selected text component of the text field
            if (((DevExpress.XtraEditors.TextEdit)this).SelectionLength > 0)
            {
                txt = SelectedText;
            }
            else
            {
                txt = string.Empty;
            }

            // if the text is missing then try the entire text field
            if (txt == string.Empty)
            {
                txt = Text.Trim();
            }

            // if there is no text then do nothing
            if (txt == string.Empty)
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            DataObject dobj = new DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = DoDragDrop(dobj, effect);
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            // if there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            base.OnDragDrop(e);

            // if there is a text field then look to determine the accepable processing
            if (!e.Data.GetDataPresent(DataFormats.Text, true))
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            EditValue = Convert.ToInt32(e.Data.GetData(DataFormats.Text, true));
        }

        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            // if the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
                return;
            }

            base.OnQueryContinueDrag(e);
        }
        #endregion

        #region  Vendor search
        protected override void OnClickButton(DevExpress.XtraEditors.Drawing.EditorButtonObjectInfoArgs buttonInfo)
        {
            string strTag = buttonInfo.Button.Tag as string;
            if (strTag != null && string.Compare(strTag, "search", true, System.Globalization.CultureInfo.InvariantCulture) == 0)
            {
                PerformVendorSearch();
            }
            else
            {
                base.OnClickButton(buttonInfo);
            }
        }

        public virtual void PerformVendorSearch()
        {
            using (var SearchItem = new DebtPlus.UI.Vendor.Widgets.Search.VendorSearchClass())
            {
                if (SearchItem.ShowDialog() == DialogResult.OK)
                {
                    EditValue = SearchItem.Vendor;
                    if (TabOnSearch)
                    {
                        Parent.SelectNextControl(this, true, true, false, true);
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Handle the keypress event. Do not allow leading whitespace
        /// </summary>
        protected override void OnEditorKeyPress(KeyPressEventArgs e)
        {
            base.OnEditorKeyPress(e);

            // Leading spaces are not significant and are removed
            if (!e.Handled)
            {
                if (Char.IsControl(e.KeyChar))
                {
                    return;
                }

                if (Char.IsWhiteSpace(e.KeyChar) && Text == string.Empty)
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Process the control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            Properties.NullText = string.Empty;
            Properties.MaxLength = 12;
            Properties.CharacterCasing = CharacterCasing.Upper;
        }

        /// <summary>
        /// When the control gets focus, select all of the text
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            SelectAll();
        }

        /// <summary>
        /// When the control gets focus, select all of the text
        /// </summary>
        protected override void OnValidating(CancelEventArgs e)
        {
            try
            {
                string inputText = Text.Trim();
                Int32 parsedVendorId = default(Int32);

                // If there is an empty string then accept the entry
                if (string.IsNullOrEmpty(inputText))
                {
                    ErrorText = string.Empty;
                    EditValue = (Int32?)null;
                }

                else if (Int32.TryParse(inputText, out parsedVendorId))
                {
                    // Look for a numerical entry to specify the specific vendor
                    if (parsedVendorId <= 0)
                    {
                        ErrorText = "Invalid vendor ID";
                        EditValue = (Int32?)null;
                        e.Cancel = true;
                        return;
                    }

                    // If we don't care about existence then just accept the number
                    if (ValidationLevel == ValidationLevelEnum.None)
                    {
                        EditValue = parsedVendorId;
                        ErrorText = string.Empty;
                    }
                    else
                    {
                        // Look for the vendor in the tables
                        using (var bc = new BusinessContext())
                        {
                            var q = bc.vendors.Where(s => s.Id == parsedVendorId).FirstOrDefault();
                            if (q == null)
                            {
                                ErrorText = "Invalid vendor ID";
                                EditValue = (Int32?)null;
                                e.Cancel = true;
                                return;
                            }

                            // If the vendor is not active then reject if we want only active vendors
                            if (!q.ActiveFlag && ValidationLevel == ValidationLevelEnum.NotProhibitUse)
                            {
                                ErrorText = "Vendor is not active";
                                EditValue = (Int32?)null;
                                e.Cancel = true;
                                return;
                            }

                            // Set the vendor ID and return success
                            EditValue = parsedVendorId;
                        }
                    }
                }
                else
                {
                    // Look up the vendor by the label
                    using (var bc = new BusinessContext())
                    {
                        var q = bc.vendors.Where(s => s.Label == inputText).FirstOrDefault();
                        if (q == null)
                        {
                            ErrorText = "Invalid vendor ID";
                            EditValue = (Int32?)null;
                            e.Cancel = true;
                            return;
                        }

                        // If the vendor is not active then reject if we want only active vendors
                        if (!q.ActiveFlag && ValidationLevel == ValidationLevelEnum.NotProhibitUse)
                        {
                            ErrorText = "Vendor is not active";
                            EditValue = (Int32?)null;
                            e.Cancel = true;
                            return;
                        }

                        // Set the vendor ID and return success
                        EditValue = q.Id;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                ErrorText = ex.Message;
                EditValue = (Int32?)null;
                return;
            }

            // Do the base validation after we have set the EditValue and there is no error.
            base.OnValidating(e);
        }
    }
}
