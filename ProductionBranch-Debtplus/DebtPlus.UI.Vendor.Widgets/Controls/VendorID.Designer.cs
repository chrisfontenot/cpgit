﻿namespace DebtPlus.UI.Vendor.Widgets.Controls
{
    partial class VendorID
    {
        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            UnRegisterHandlers();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
        }
    }
}