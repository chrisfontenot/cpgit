#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Interfaces.Creditor;
using DevExpress.XtraEditors.Controls;
using System.Windows.Forms;
using DevExpress.Utils;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Creditor.Create
{
    public partial class CreateCreditorClass : DebtPlus.Data.Forms.DebtPlusForm, ICreditorCreate
    {
        public CreateCreditorClass() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                           += CreateCreditorClass_Load;
            Button_OK.Click                += Button_OK_Click;
            creditor_type.EditValueChanged += creditor_type_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= CreateCreditorClass_Load;
            Button_OK.Click -= Button_OK_Click;
            creditor_type.EditValueChanged -= creditor_type_EditValueChanged;
        }

        /// <summary>
        /// The id of the newly created creditor
        /// </summary>
        public string Creditor { get; set; }

        private void CreateCreditorClass_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                creditor_type.Properties.DataSource = DebtPlus.LINQ.Cache.creditor_type.getList();
                creditor_type.EditValue             = DebtPlus.LINQ.Cache.creditor_type.getDefault();
                Button_OK.Enabled                   = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void creditor_type_EditValueChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return string.IsNullOrEmpty(DebtPlus.Utils.Nulls.v_String(creditor_type.EditValue));
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            string creditorType = DebtPlus.Utils.Nulls.v_String(creditor_type.EditValue);
            if (string.IsNullOrEmpty(creditorType))
            {
                return;
            }

            try
            {
                using (BusinessContext bc = new BusinessContext())
                {
                    Creditor     = bc.xpr_create_creditor(creditorType);
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    return;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LookUpEdit creditor_type;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.creditor_type = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.creditor_type.Properties).BeginInit();
            this.SuspendLayout();

            //
            //labelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128));
            this.LabelControl1.Appearance.Options.UseBackColor = true;
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(72, 16);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(228, 74);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creating today creditor requires specific permissions. You may not have the required" + " permission so the insert operation may fail. If you chose the New function by today" + "ccident then cancel now.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //labelControl2
            //
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.LabelControl2.Appearance.Options.UseFont = true;
            this.LabelControl2.Location = new System.Drawing.Point(8, 16);
            this.LabelControl2.Name = "labelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(57, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "WARNING:";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl3.Appearance.Options.UseTextOptions = true;
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl3.Location = new System.Drawing.Point(8, 96);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(296, 50);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Choose the type of the creditor from the following list. A creditor ID will be au" + "tomatically created once the type has been choosen.";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //creditor_type
            //
            this.creditor_type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.creditor_type.Location = new System.Drawing.Point(8, 152);
            this.creditor_type.Name = "creditor_type";
            this.creditor_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.creditor_type.Size = new System.Drawing.Size(296, 20);
            this.creditor_type.TabIndex = 3;
            this.creditor_type.ToolTip = "Choose the type of the creditor from the list";
            this.creditor_type.ToolTipController = this.ToolTipController1;
            this.creditor_type.Properties.NullText = "Select today type from this list...";
            this.creditor_type.Properties.AllowNullInput = DefaultBoolean.False;
            this.creditor_type.Properties.ShowHeader = true;
            this.creditor_type.Properties.ShowFooter = false;
            this.creditor_type.Properties.ShowLines = true;
            this.creditor_type.Properties.ValueMember = "Id";
            this.creditor_type.Properties.DisplayMember = "description";
            this.creditor_type.Properties.Columns.AddRange(new LookUpColumnInfo[] { new LookUpColumnInfo("Id", "Type", 20, FormatType.None, string.Empty, true, HorzAlignment.Center), new LookUpColumnInfo("description", 90, "Description") });
            //
            //button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(71, 184);
            this.Button_OK.Name = "button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to create the new creditor";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            this.Button_OK.Enabled = false;
            //
            //button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(167, 184);
            this.Button_Cancel.Name = "button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel the creation and return to the previous screen";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Form_CreateCreditor
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(312, 222);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.creditor_type);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.LabelControl2);
            this.MaximizeBox = false;
            this.Name = "Form_CreateCreditor";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Create New Creditor";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();

            ((System.ComponentModel.ISupportInitialize)this.creditor_type.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion
    }
}