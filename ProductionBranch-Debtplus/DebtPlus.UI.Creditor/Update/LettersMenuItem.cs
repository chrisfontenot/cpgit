using System.Threading;
using System.Windows.Forms;
using DebtPlus.Events;
using DebtPlus.Letters.Compose;
using DebtPlus.LINQ;
using DevExpress.XtraBars;

namespace DebtPlus.UI.Creditor.Update
{
    internal partial class LettersMenuItem : BarButtonItem
    {
        // Current creditor being edited
        private creditor creditorRecord = null;

        // Current letter record pointer
        private letter_type letterRecord = null;

        public LettersMenuItem() : base()
        {
        }

        public LettersMenuItem(string text) : this()
        {
            this.Caption = text;
        }

        public LettersMenuItem(string text, ItemClickEventHandler OnClickHandler) : this(text)
        {
            this.ItemClick += OnClickHandler;
        }

        public LettersMenuItem(string text, ItemClickEventHandler OnClickHandler, Shortcut Shortcut) : this(text, OnClickHandler)
        {
            this.ItemShortcut = new BarShortcut(Shortcut);
        }

        public LettersMenuItem(creditor CreditorRecord, letter_type LetterRecord, string Name) : this(Name)
        {
            this.creditorRecord = CreditorRecord;
            this.letterRecord   = LetterRecord;
        }

        /// <summary>
        /// Handle the CLICK event
        /// </summary>
        protected override void OnClick(BarItemLink link)
        {
            // Do the database update on the click event
            base.OnClick(link);

            // If there is no data then we can not continue
            if (letterRecord == null || creditorRecord == null)
            {
                return;
            }

            // Start today thread to process the letter and the class. Just let it run.
            Thread thrd = new Thread(new ParameterizedThreadStart(Show))
                                {
                                    Name = "Letters",
                                    IsBackground = false
                                };

            thrd.SetApartmentState(ApartmentState.STA);
            thrd.Start(null);
        }

        /// <summary>
        /// Thread procedure to display the letter
        /// </summary>
        /// <param name="obj">Parameter passed to the thread</param>
        public void Show(object obj)
        {
            using (Composition ltr = new Composition(letterRecord.Id))
            {
                ltr.GetValue += LetterGetField;
                try
                {
                    ltr.PrintLetter();
                }
                finally
                {
                    ltr.GetValue -= LetterGetField;
                }
            }
        }

        private void LetterGetField(object sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            if (e.Name == ParameterValueEventArgs.name_creditor)
            {
                e.Value = creditorRecord.Id;
            }
        }
    }
}