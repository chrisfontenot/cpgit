
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_Alias
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_Additional = new DevExpress.XtraEditors.TextEdit();
            this.lookupEdit_Type = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Additional.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupEdit_Type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(56, 88);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 4;
            this.button_OK.Text = "&OK";
            this.button_OK.ToolTipController = this.ToolTipController1;
            // 
            // button_Cancel
            // 
            this.button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Cancel.Location = new System.Drawing.Point(160, 88);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 5;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(8, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "&Type:";
            this.labelControl1.ToolTipController = this.ToolTipController1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(8, 51);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(30, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "&Value:";
            this.labelControl2.ToolTipController = this.ToolTipController1;
            // 
            // textEdit_Additional
            // 
            this.textEdit_Additional.Location = new System.Drawing.Point(48, 48);
            this.textEdit_Additional.Name = "textEdit_Additional";
            this.textEdit_Additional.Properties.MaxLength = 50;
            this.textEdit_Additional.Size = new System.Drawing.Size(224, 20);
            this.textEdit_Additional.TabIndex = 3;
            this.textEdit_Additional.ToolTip = "Enter the value that you wish to assign for the alias or prefix in this field. Yo" +
    "u must make an entry or press the CANCEL button.";
            this.textEdit_Additional.ToolTipController = this.ToolTipController1;
            // 
            // lookupEdit_Type
            // 
            this.lookupEdit_Type.Location = new System.Drawing.Point(48, 24);
            this.lookupEdit_Type.Name = "lookupEdit_Type";
            this.lookupEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupEdit_Type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookupEdit_Type.Properties.DisplayMember = "description";
            this.lookupEdit_Type.Properties.NullText = "";
            this.lookupEdit_Type.Properties.PopupSizeable = false;
            this.lookupEdit_Type.Properties.ShowFooter = false;
            this.lookupEdit_Type.Properties.ShowHeader = false;
            this.lookupEdit_Type.Properties.ValueMember = "Id";
            this.lookupEdit_Type.Size = new System.Drawing.Size(224, 20);
            this.lookupEdit_Type.TabIndex = 1;
            this.lookupEdit_Type.ToolTip = "Choose the type of the item from the list. A prefix is used to lookup account num" +
    "bers while an alias is simply an additional name for the creditor.";
            this.lookupEdit_Type.ToolTipController = this.ToolTipController1;
            // 
            // Form_Alias
            // 
            this.AcceptButton = this.button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.button_Cancel;
            this.ClientSize = new System.Drawing.Size(292, 120);
            this.Controls.Add(this.textEdit_Additional);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.lookupEdit_Type);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_Alias";
            this.Text = "Creditor Alias or Prefix Name";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Additional.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupEdit_Type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private DevExpress.XtraEditors.SimpleButton button_OK;
        private DevExpress.XtraEditors.SimpleButton button_Cancel;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textEdit_Additional;
        private DevExpress.XtraEditors.LookUpEdit lookupEdit_Type;
    }
}