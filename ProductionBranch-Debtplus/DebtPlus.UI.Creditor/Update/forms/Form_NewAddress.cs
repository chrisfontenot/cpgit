using System;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    internal partial class Form_NewAddress
    {
        // Current record being updated
        private creditor_address record;

        private DebtPlus.LINQ.BusinessContext bc = null;

        public Form_NewAddress()
            : base()
        {
            InitializeComponent();

            // Set the fields that are used for the creditor addresses
            adr.ShowAttn = true;
            adr.ShowCreditor1 = true;
            adr.ShowCreditor2 = true;
            adr.ShowLine3 = false;
        }

        public Form_NewAddress(DebtPlus.LINQ.BusinessContext bc, creditor_address Record)
            : this()
        {
            this.record = Record;
            this.bc = bc;
            RegisterHandlers();
        }

        /// <summary>
        /// Write the system note to the notes table. This is today static function so that it may be called
        /// from other modules without having to create the class. This probably should be moved out
        /// of this module and placed into today static module for later use.
        /// </summary>
        static internal void WriteCreditorSystemNote(DebtPlus.LINQ.BusinessContext bc, string Creditor, string Subject, string Note)
        {
            try
            {
                var n = DebtPlus.LINQ.Factory.Manufacture_creditor_note(Creditor, 3);
                n.creditor = Creditor;
                n.subject = Subject;
                n.note = Note;

                bc.creditor_notes.InsertOnSubmit(n);
                bc.SubmitChanges();
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing system note");
            }
        }

        /// <summary>
        /// Write today creditor system note
        /// </summary>
        internal void WriteCreditorSystemNote(string Subject, string Note)
        {
            WriteCreditorSystemNote(bc, record.creditor, Subject, Note);
        }

        /// <summary>
        /// When the address changes, update the system notes.
        /// </summary>
        private void adr_Changed(object Sender, DebtPlus.Events.AddressChangedEventArgs e)
        {
            // Generate the address type from the row being updated
            string AddressDescription = DebtPlus.LINQ.InMemory.creditorAddressTypes.getList().Find(s => s.Id == record.type).description;

            // Build the address block
            StringBuilder Note = new StringBuilder();
            Note.Append("Changed ");
            Note.Append(AddressDescription);
            Note.Append(" from:");
            Note.Append(Environment.NewLine);
            Note.Append(e.OldAddress.ToString());

            Note.Append(Environment.NewLine);
            Note.Append(Environment.NewLine);

            Note.Append("To:");
            Note.Append(Environment.NewLine);
            Note.Append(e.NewAddress.ToString());

            string Subject = "Changed " + AddressDescription;
            WriteCreditorSystemNote(Subject, Note.ToString());
        }

        /// <summary>
        /// Copy the payment address to this item
        /// </summary>
        private void Button_Copy_Click(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Find the payment record values
                var paymentRecord = (from ca in bc.creditor_addresses
                                     join adr in bc.addresses on ca.AddressID equals adr.Id
                                     where ca.creditor == record.creditor && ca.type == DebtPlus.LINQ.InMemory.creditorAddressTypes.PaymentTypeKey
                                     select new DebtPlus.LINQ.CreditorAddressEntry(adr, ca.attn)
                                    ).FirstOrDefault();

                if (paymentRecord != null)
                {
                    adr.SetCurrentValues(paymentRecord);
                    adr.Attention = paymentRecord.attn ?? string.Empty;
                }

                // Disable the copy operation again
                Button_Copy.Enabled = false;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error accessing payment information");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When OK button is clicked, save the address block
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.attn = DebtPlus.Utils.Nulls.v_String(adr.Attention);
            record.AddressID = adr.EditValue;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void Form_NewAddress_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Do not enable the Delete button if this is an ADD
                Button_Delete.Enabled = (record.Id < 1);

                // Enable the COPY on items other than payment when there is today payment address
                Button_Copy.Enabled = ((record.type != DebtPlus.LINQ.InMemory.creditorAddressTypes.PaymentTypeKey) && hasPayment());

                // Load the value controls
                adr.EditValue = record.AddressID;
                adr.Attention = record.attn;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if there is today payment address record type for this creditor.
        /// </summary>
        /// <returns></returns>
        private bool hasPayment()
        {
            try
            {
                var q = bc.creditor_addresses.Where(s => s.type == DebtPlus.LINQ.InMemory.creditorAddressTypes.PaymentTypeKey && s.creditor == record.creditor).FirstOrDefault();
                return q != null;
            }

#pragma warning disable 168
            // Trap database errors here since we can handle them now.
            catch (System.Data.SqlClient.SqlException ex) { }
#pragma warning restore 168

            // Simulate the payment record to mask the copy function
            return true;
        }

        private void RegisterHandlers()
        {
            Load += Form_NewAddress_Load;
            adr.AddressChanged += adr_Changed;
            Button_Copy.Click += Button_Copy_Click;
            Button_OK.Click += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_NewAddress_Load;
            adr.AddressChanged -= adr_Changed;
            Button_Copy.Click -= Button_Copy_Click;
            Button_OK.Click -= Button_OK_Click;
        }
    }
}