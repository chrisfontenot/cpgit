#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    internal partial class Form_EFTLocationEdit
    {
        // Record being updated
        private DebtPlus.LINQ.creditor_method record;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_EFTLocationEdit() : base()
        {
            InitializeComponent();
        }

        public Form_EFTLocationEdit(DebtPlus.LINQ.creditor_method record) : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        public string CreditorName { get; set; }

        private string GetBankType
        {
            get
            {
                var bankRecord = lk_bank.GetSelectedDataRow() as bank;
                if (bankRecord != null && bankRecord.type != null)
                {
                    return bankRecord.type;
                }
                return string.Empty;
            }
        }

        private string GetBillerID
        {
            get
            {
                string bankType = GetBankType;
                if (bankType == null || bankType == "C")
                {
                    return null;
                }

                return DebtPlus.Utils.Nulls.v_String(biller_id.EditValue);
            }
        }

        private void biller_id_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            // This is true if we are looking for payment information.
            bool searchPayment = record.type == "EFT";

            // Perform today search on the biller id
            using (RPPS.Biller.Form_RPPS_Search frm = new RPPS.Biller.Form_RPPS_Search(CreditorName, searchPayment))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    biller_id.Text    = frm.BillerID.Trim();
                    biller_id.Enabled = GetBankType != string.Empty;
                    Button_OK.Enabled = !HasErrors();

                    // Tab to the next control
                    SelectNextControl(biller_id, true, true, false, true);
                }
            }
        }

        /// <summary>
        /// Process the OK button CLICK event
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            switch (GetBankType ?? string.Empty)
            {
                case "R":
                    record.rpps_biller_id = GetBillerID;
                    break;

                case "E":
                    record.rpps_biller_id = null;
                    break;

                default:
                    record.rpps_biller_id = null;
                    break;
            }

            record.bank   = DebtPlus.Utils.Nulls.v_Int32(lk_bank.EditValue).GetValueOrDefault();
            record.region = DebtPlus.Utils.Nulls.v_Int32(lk_region.EditValue).GetValueOrDefault();
        }

        private void Form_EFTItemEdit_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Look at the record type to determine which banks are legal for the list.
                if (record.type == "EFT")
                {
                    string[] ValidTypes           = new string[] { "C", "R" };
                    lk_bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => ValidTypes.Contains(s.type) && s.ActiveFlag).ToList();
                }
                else
                {
                    string[] InvalidTypes         = new string[] { "D", "A" };
                    lk_bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => !InvalidTypes.Contains(s.type) && s.ActiveFlag).ToList();
                }

                System.Collections.Generic.List<region> colRegions = new System.Collections.Generic.List<region>();
                colRegions.AddRange(DebtPlus.LINQ.Cache.region.getList().FindAll(s => s.Id > 0).ToList());

                // Add an "ALL" region to the list of regions
                var allRegion = new DebtPlus.LINQ.region()
                {
                    ActiveFlag  = true,
                    Default     = false,
                    description = "All Regions",
                    Id          = 0
                };

                colRegions.Add(allRegion);

                // Load the editing controls with the current values
                lk_bank.EditValue               = record.bank;
                lk_region.EditValue             = record.region;
                lk_region.Properties.DataSource = colRegions;

                // Load the biller ID into the editing control
                switch (GetBankType ?? string.Empty)
                {
                    case "R":
                        biller_id.EditValue = record.rpps_biller_id;
                        biller_id.Enabled   = true;
                        break;

                    default:
                        biller_id.EditValue = null;
                        biller_id.Enabled   = false;
                        break;
                }

                // Correct the mask for the biller id in the text editing control
                UpdateBillerIDMask();

                // Enable the controls
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void FormChanged(object Sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
            biller_id.Enabled = "RE".IndexOf(GetBankType ?? string.Empty) >= 0;
        }

        private bool HasErrors()
        {
            // A region selection is required.
            if (lk_region.EditValue == null)
            {
                return true;
            }

            // Find the selected bank record. If none, the OK is not allowed.
            var bankRecord = lk_bank.GetSelectedDataRow() as bank;
            if (GetBankType == null)
            {
                return true;
            }

            // Checks/paper banks do not need today biller ID
            if (GetBankType == "C")
            {
                return false;
            }

            // There needs to be today biller for EFT transactions
            if (GetBillerID == string.Empty)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Change in the bank will change the format for the biller id
        /// </summary>
        private void lk_bank_EditValueChanged(object sender, EventArgs e)
        {
            UpdateBillerIDMask();
        }

        private void RegisterHandlers()
        {
            Load                        += Form_EFTItemEdit_Load;
            lk_bank.EditValueChanging   += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lk_region.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lk_bank.EditValueChanged    += FormChanged;
            lk_region.EditValueChanged  += FormChanged;
            biller_id.EditValueChanged  += FormChanged;
            Button_OK.Click             += Button_OK_Click;
            biller_id.ButtonPressed     += biller_id_ButtonPressed;
        }

        private void UnRegisterHandlers()
        {
            Load                        -= Form_EFTItemEdit_Load;
            lk_bank.EditValueChanging   -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lk_region.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lk_bank.EditValueChanged    -= FormChanged;
            lk_region.EditValueChanged  -= FormChanged;
            biller_id.EditValueChanged  -= FormChanged;
            Button_OK.Click             -= Button_OK_Click;
            biller_id.ButtonPressed     -= biller_id_ButtonPressed;
        }

        /// <summary>
        /// Process today change in the bank to control the editing input for the biller id
        /// </summary>
        private void UpdateBillerIDMask()
        {
            string strValue = string.Empty;

            biller_id.Properties.Mask.MaskType    = MaskType.RegEx;
            biller_id.Properties.Mask.BeepOnError = true;
            biller_id.Properties.Mask.SaveLiteral = false;

            if (biller_id.EditValue != null && !object.ReferenceEquals(biller_id.EditValue, DBNull.Value))
            {
                strValue = Convert.ToString(biller_id.EditValue);
            }

            switch (GetBankType ?? string.Empty)
            {
                case "R":
                    // RPPS banks have 10 digits for today biller id
                    if (strValue.Length > 10)
                    {
                        biller_id.EditValue = strValue.Substring(0, 10);
                    }
                    biller_id.Properties.Mask.EditMask = @"\d{10}";
                    biller_id.Properties.MaxLength     = 10;
                    break;

                default:
                    // Choose the smaller of the sizes just in case there is today problem.
                    biller_id.Properties.Mask.EditMask = @"\d{10}";
                    biller_id.Properties.MaxLength     = 10;
                    break;
            }
        }
    }
}