namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_CreditorContributionPCT
    {
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.effective_date = new DevExpress.XtraEditors.DateEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.fairshare_pct_eft = new DebtPlus.Data.Controls.PercentEdit();
            this.fairshare_pct_check = new DebtPlus.Data.Controls.PercentEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.creditor_type_eft = new DevExpress.XtraEditors.LookUpEdit();
            this.creditor_type_check = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fairshare_pct_eft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fairshare_pct_check.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_type_eft.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_type_check.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(66, 20);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(69, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Effective &Date";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // effective_date
            // 
            this.effective_date.EditValue = null;
            this.effective_date.Location = new System.Drawing.Point(146, 16);
            this.effective_date.Name = "effective_date";
            this.effective_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.effective_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.effective_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.effective_date.Size = new System.Drawing.Size(100, 20);
            this.effective_date.TabIndex = 1;
            this.effective_date.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 76);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(68, 13);
            this.LabelControl2.TabIndex = 3;
            this.LabelControl2.Text = "&EFT Payments";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 100);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(79, 13);
            this.LabelControl3.TabIndex = 6;
            this.LabelControl3.Text = "Chec&k Payments";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl4
            // 
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(106, 48);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(86, 13);
            this.LabelControl4.TabIndex = 2;
            this.LabelControl4.Text = "Contribution &Type";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl5
            // 
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(216, 48);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(81, 13);
            this.LabelControl5.TabIndex = 9;
            this.LabelControl5.Text = "Percentage &Rate";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            // 
            // fairshare_pct_eft
            // 
            this.fairshare_pct_eft.Location = new System.Drawing.Point(208, 72);
            this.fairshare_pct_eft.Name = "fairshare_pct_eft";
            this.fairshare_pct_eft.Properties.Allow100Percent = false;
            this.fairshare_pct_eft.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fairshare_pct_eft.Size = new System.Drawing.Size(88, 20);
            this.fairshare_pct_eft.TabIndex = 5;
            this.fairshare_pct_eft.ToolTipController = this.ToolTipController1;
            // 
            // fairshare_pct_check
            // 
            this.fairshare_pct_check.Location = new System.Drawing.Point(208, 96);
            this.fairshare_pct_check.Name = "fairshare_pct_check";
            this.fairshare_pct_check.Properties.Allow100Percent = false;
            this.fairshare_pct_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.fairshare_pct_check.Size = new System.Drawing.Size(88, 20);
            this.fairshare_pct_check.TabIndex = 8;
            this.fairshare_pct_check.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(67, 136);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 10;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(171, 136);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // creditor_type_eft
            // 
            this.creditor_type_eft.Location = new System.Drawing.Point(104, 72);
            this.creditor_type_eft.Name = "creditor_type_eft";
            this.creditor_type_eft.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.creditor_type_eft.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("bdn", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.creditor_type_eft.Properties.DisplayMember = "description";
            this.creditor_type_eft.Properties.NullText = "";
            this.creditor_type_eft.Properties.PopupSizeable = false;
            this.creditor_type_eft.Properties.ShowFooter = false;
            this.creditor_type_eft.Properties.ShowHeader = false;
            this.creditor_type_eft.Properties.SortColumnIndex = 1;
            this.creditor_type_eft.Properties.ValueMember = "bdn";
            this.creditor_type_eft.Size = new System.Drawing.Size(88, 20);
            this.creditor_type_eft.TabIndex = 4;
            this.creditor_type_eft.ToolTipController = this.ToolTipController1;
            // 
            // creditor_type_check
            // 
            this.creditor_type_check.Location = new System.Drawing.Point(104, 96);
            this.creditor_type_check.Name = "creditor_type_check";
            this.creditor_type_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.creditor_type_check.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("bdn", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.creditor_type_check.Properties.DisplayMember = "description";
            this.creditor_type_check.Properties.NullText = "";
            this.creditor_type_check.Properties.PopupSizeable = false;
            this.creditor_type_check.Properties.ShowFooter = false;
            this.creditor_type_check.Properties.ShowHeader = false;
            this.creditor_type_check.Properties.SortColumnIndex = 1;
            this.creditor_type_check.Properties.ValueMember = "bdn";
            this.creditor_type_check.Size = new System.Drawing.Size(88, 20);
            this.creditor_type_check.TabIndex = 7;
            this.creditor_type_check.ToolTipController = this.ToolTipController1;
            // 
            // Form_CreditorContributionPCT
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(312, 174);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.fairshare_pct_check);
            this.Controls.Add(this.fairshare_pct_eft);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.effective_date);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.creditor_type_eft);
            this.Controls.Add(this.creditor_type_check);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_CreditorContributionPCT";
            this.ShowInTaskbar = false;
            this.Text = "Contribution Percentage";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fairshare_pct_eft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fairshare_pct_check.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_type_eft.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_type_check.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.DateEdit effective_date;
        private DebtPlus.Data.Controls.PercentEdit fairshare_pct_eft;
        private DebtPlus.Data.Controls.PercentEdit fairshare_pct_check;
        private DevExpress.XtraEditors.LookUpEdit creditor_type_eft;
        private DevExpress.XtraEditors.LookUpEdit creditor_type_check;
    }
}