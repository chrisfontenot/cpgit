﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DevExpress.XtraTab;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_CreditorUpdate
    {
        /// <summary>
        /// Manufacture today page for the creditor.
        /// </summary>
        private System.Windows.Forms.Control Manufacture_Page(System.Windows.Forms.Control parentControl, string pageType)
        {
            // Fully qualify the name of the class that we want to create. It requires the full name.
            string typeName = string.Format("DebtPlus.UI.Creditor.Update.Pages.{0}", pageType);

            try
            {
                // Create the page instance. Pass it the current record being edited.
                // Get a pointer to the translation class
                System.Reflection.Assembly asm  = System.Reflection.Assembly.GetExecutingAssembly();

                // From the type, create an instance of the class
                System.Type typ = asm.GetType(typeName);
                if (typ != null)
                {
                    System.Windows.Forms.Control objPage = System.Activator.CreateInstance(typ) as System.Windows.Forms.Control;

                    // Supply the other parameters for the control.
                    objPage.Name = pageType;
                    objPage.Parent = parentControl;
                    objPage.Dock = DockStyle.Fill;

                    // Add this control to the form's list of controls
                    this.Controls.Add(objPage);

                    // Do the one-time initialization of the page.
                    ((DebtPlus.UI.Creditor.Update.Controls.ControlBase)objPage).ReadForm(bc, creditorRecord);
                    return objPage;
                }
                throw new ApplicationException(string.Format("Can not find type '{0}'", typeName));
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error creating tab page", ex);
            }
        }

        /// <summary>
        /// Process the newly selected tab page
        /// </summary>
        private void selectCurrentPage()
        {
            // We need today selected tab page to continue. Stop if there are none.
            var tabPage = XtraTabControl1.SelectedTabPage;
            if (tabPage == null)
            {
                return;
            }

            // Find the name of the page from the tag for the page. Use the tag rather than the index because the tabs can move.
            string pageName = Convert.ToString(tabPage.Tag);

            // Create the new page if we don't have one.
            System.Windows.Forms.Control currentControl = (tabPage.Controls.Count <= 0) ? null : tabPage.Controls[0];
            if (currentControl == null)
            {
                // Create the new page control
                currentControl = Manufacture_Page(tabPage, pageName);

                // Add the control to the page's set
                XtraTabControl1.SelectedTabPage.Controls.Clear();
                XtraTabControl1.SelectedTabPage.Controls.Add(currentControl);
            }

            // Invoke the Refresh function on the page. This is normally a stub, but we call it for the "general" page.
            ((DebtPlus.UI.Creditor.Update.Controls.ControlBase) currentControl).RefreshForm(bc, creditorRecord);
        }

        /// <summary>
        /// Process the Page change event for the tab control
        /// </summary>
        private void XtraTabControl1_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            // Remove the menu items and select the current tab page
            BarSubItem_Items.Reset();
            selectCurrentPage();
        }

        /// <summary>
        /// Process the Page change event for the tab control
        /// </summary>
        private void XtraTabControl1_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            // Find the currently selected tab page
            var tabPage = XtraTabControl1.SelectedTabPage;
            if (tabPage != null && tabPage.Controls.Count > 0)
            {
                // From the selected tab page, take the first control as our page form.
                var CurrentPage = tabPage.Controls[0] as DebtPlus.UI.Creditor.Update.Controls.ControlBase;

                // If there is one then perform the "save" function on that page to flush the data to the database.
                if (CurrentPage != null)
                {
                    try
                    {
                        CurrentPage.SaveForm(bc);   // Save the page data first
                        SaveForm();                 // Follow it with the changes to the current creditor record
                    }

                    catch (ApplicationException ex)
                    {
                        e.Cancel = true;
                        DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Form validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
