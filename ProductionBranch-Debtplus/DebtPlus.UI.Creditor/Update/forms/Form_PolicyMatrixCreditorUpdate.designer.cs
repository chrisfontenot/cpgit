namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_PolicyMatrixCreditorUpdate
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.description = new DevExpress.XtraEditors.LabelControl();
            this.item_value = new DevExpress.XtraEditors.TextEdit();
            this.message = new DevExpress.XtraEditors.MemoEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.use_sic = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item_value.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.use_sic.Properties).BeginInit();
            this.SuspendLayout();
            //
            //labelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(16, 12);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(320, 62);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Update the policy matrix information for this creditor. Note: You may not have pe" + "rmission to perform the update. If you don't please press the CANCEL button belo" + "w.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //labelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(16, 80);
            this.LabelControl2.Name = "labelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(31, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Policy:";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(16, 104);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(30, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Value:";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(16, 131);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(46, 13);
            this.LabelControl4.TabIndex = 5;
            this.LabelControl4.Text = "Message:";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            //
            //description
            //
            this.description.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.description.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.description.Location = new System.Drawing.Point(96, 80);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(234, 13);
            this.description.TabIndex = 2;
            this.description.ToolTipController = this.ToolTipController1;
            this.description.UseMnemonic = false;
            //
            //item_value
            //
            this.item_value.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.item_value.Location = new System.Drawing.Point(96, 101);
            this.item_value.Name = "item_value";
            this.item_value.Properties.MaxLength = 1024;
            this.item_value.Size = new System.Drawing.Size(234, 20);
            this.item_value.TabIndex = 4;
            this.item_value.ToolTipController = this.ToolTipController1;
            //
            //message
            //
            this.message.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.message.Location = new System.Drawing.Point(96, 128);
            this.message.Name = "message";
            this.message.Properties.MaxLength = 1024;
            this.message.Size = new System.Drawing.Size(234, 97);
            this.message.TabIndex = 6;
            this.message.ToolTipController = this.ToolTipController1;
            //
            //button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(87, 241);
            this.Button_OK.Name = "button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 7;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(183, 241);
            this.Button_Cancel.Name = "button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //use_sic
            //
            this.use_sic.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.use_sic.Location = new System.Drawing.Point(8, 273);
            this.use_sic.Name = "use_sic";
            this.use_sic.Properties.Caption = "Copy this change to all creditors with the same SIC code";
            this.use_sic.Size = new System.Drawing.Size(328, 19);
            this.use_sic.TabIndex = 9;
            this.use_sic.ToolTipController = this.ToolTipController1;
            //
            //Form_PolicyMatrixCreditorUpdate
            //
            this.ClientSize = new System.Drawing.Size(342, 295);
            this.Controls.Add(this.use_sic);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.message);
            this.Controls.Add(this.item_value);
            this.Controls.Add(this.description);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.MaximizeBox = false;
            this.Name = "Form_PolicyMatrixCreditorUpdate";
            this.Text = "Creditor Policy Update";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item_value.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.use_sic.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.MemoEdit message;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.LabelControl description;
        private DevExpress.XtraEditors.TextEdit item_value;
        private DevExpress.XtraEditors.CheckEdit use_sic;
    }
}
