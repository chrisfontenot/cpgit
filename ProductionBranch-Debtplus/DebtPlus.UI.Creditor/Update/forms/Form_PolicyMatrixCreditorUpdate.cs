#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    internal partial class Form_PolicyMatrixCreditorUpdate : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current record being updated
        private policy_matrix_policy record;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Form_PolicyMatrixCreditorUpdate()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="Record">Pointer to the record to be edited</param>
        public Form_PolicyMatrixCreditorUpdate(policy_matrix_policy Record)
            : this()
        {
            this.record = Record;
            RegisterHandlers();
        }

        /// <summary>
        /// Copy all of these items to all creditors with the same S.I.C. value
        /// </summary>
        public bool CopyToSIC
        {
            get
            {
                return use_sic.Checked;
            }
        }

        /// <summary>
        /// Update the record when the OK button is pressed
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.message = DebtPlus.Utils.Nulls.v_String(message.EditValue);
            record.item_value = DebtPlus.Utils.Nulls.v_String(item_value.EditValue);
        }

        /// <summary>
        /// When the form is closed, save the location
        /// </summary>
        private void Form_PolicyMatrixCreditorUpdate_FormClosing(object sender, FormClosingEventArgs e)
        {
            SavePlacement("CreditorUpdate.PolicyMatrix");
        }

        /// <summary>
        /// Process the form LOAD sequence
        /// </summary>
        private void Form_PolicyMatrixCreditorUpdate_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadPlacement("CreditorUpdate.PolicyMatrix");

                // Define the current values
                message.Text = record.message;
                item_value.Text = record.item_value;

                // From the policy matrix type, find the description
                var qType = DebtPlus.LINQ.Cache.policy_matrix_policy_type.getList().Find(s => s.Id == record.policy_matrix_policy_type);
                if (qType != null)
                {
                    description.Text = qType.description;
                }

                // If the user does not have permission then disable the controls
                if (!DebtPlus.Configuration.Config.UpdateCreditorPolicy)
                {
                    message.Properties.ReadOnly = true;
                    item_value.Properties.ReadOnly = true;
                    use_sic.Properties.ReadOnly = true;
                }

                // Disable the ok button until the fields are changed
                Button_OK.Enabled = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process today change in the form
        /// </summary>
        private void FormTextChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = true;
        }

        /// <summary>
        /// Register event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Form_PolicyMatrixCreditorUpdate_Load;
            FormClosing += Form_PolicyMatrixCreditorUpdate_FormClosing;
            message.TextChanged += FormTextChanged;
            item_value.TextChanged += FormTextChanged;
            Button_OK.Click += Button_OK_Click;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_PolicyMatrixCreditorUpdate_Load;
            FormClosing -= Form_PolicyMatrixCreditorUpdate_FormClosing;
            message.TextChanged -= FormTextChanged;
            item_value.TextChanged -= FormTextChanged;
            Button_OK.Click -= Button_OK_Click;
        }
    }
}
