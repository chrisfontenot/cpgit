﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_CreditorUpdate
    {
        /// <summary>
        /// Routine called when a report or letter menu item is chosen. This will update the database
        /// at the current time to ensure that the data is consistent with the entries for the current page.
        /// </summary>
        /// <param name="sender">BarManager when the event occurs</param>
        /// <param name="e">Pointer to the menus that were chosen</param>
        private void LetterReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Make sure that we are in the proper thread context to save the data.
            if (InvokeRequired)
            {
                var ia = BeginInvoke(new DevExpress.XtraBars.ItemClickEventHandler(LetterReport_ItemClick), new object[] { sender, e });
                EndInvoke(ia);
            }
            else
            {
                SaveForm();
            }
        }

        /// <summary>
        /// Save the form data to the database. The current tab page data is saved before the creditor
        /// record information is updated.
        /// </summary>
        private void SaveForm()
        {
            // Force the current control to be validated if this is needed. We do this by forcing
            // a tab switch to the next control in the page.
            Control ctl = this.ActiveControl;
            if (ctl != null)
            {
                SelectNextControl(ctl, true, false, true, true);
            }

            // Do the save operation on the current tab page before we try to save the creditor.
            var tabPage = XtraTabControl1.SelectedTabPage;
            if (tabPage != null && tabPage.Controls.Count > 0)
            {
                var CurrentPage = tabPage.Controls[0] as DebtPlus.UI.Creditor.Update.Controls.ControlBase;
                if (CurrentPage != null)
                {
                    CurrentPage.SaveForm(bc);
                }
            }

            // Save the creditorRecord data next
            bc.SubmitChanges();
        }
    }
}
