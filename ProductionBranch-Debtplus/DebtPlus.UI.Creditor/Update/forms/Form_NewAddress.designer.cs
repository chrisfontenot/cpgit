namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_NewAddress : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Copy = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Delete = new DevExpress.XtraEditors.SimpleButton();
            this.adr = new DebtPlus.Data.Controls.AddressRecordControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(226, 185);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(146, 185);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 2;
            this.Button_OK.Text = "&OK";
            // 
            // Button_Copy
            // 
            this.Button_Copy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_Copy.Location = new System.Drawing.Point(8, 185);
            this.Button_Copy.Name = "Button_Copy";
            this.Button_Copy.Size = new System.Drawing.Size(80, 23);
            this.Button_Copy.TabIndex = 1;
            this.Button_Copy.Text = "C&opy Payment";
            // 
            // Button_Delete
            // 
            this.Button_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Delete.DialogResult = System.Windows.Forms.DialogResult.No;
            this.Button_Delete.Location = new System.Drawing.Point(372, 185);
            this.Button_Delete.Name = "Button_Delete";
            this.Button_Delete.Size = new System.Drawing.Size(75, 23);
            this.Button_Delete.TabIndex = 4;
            this.Button_Delete.Text = "&Delete";
            // 
            // adr
            // 
            this.adr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.adr.Location = new System.Drawing.Point(13, 13);
            this.adr.Name = "adr";
            this.adr.Size = new System.Drawing.Size(427, 166);
            this.adr.TabIndex = 0;
            // 
            // Form_NewAddress
            // 
            this.ClientSize = new System.Drawing.Size(452, 223);
            this.Controls.Add(this.adr);
            this.Controls.Add(this.Button_Delete);
            this.Controls.Add(this.Button_Copy);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_Cancel);
            this.MinimumSize = new System.Drawing.Size(460, 239);
            this.Name = "Form_NewAddress";
            this.Text = "Update Address Information";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr)).EndInit();
            this.ResumeLayout(false);

        }

        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Copy;
        private DevExpress.XtraEditors.SimpleButton Button_Delete;
        private DebtPlus.Data.Controls.AddressRecordControl adr;
    }
}
