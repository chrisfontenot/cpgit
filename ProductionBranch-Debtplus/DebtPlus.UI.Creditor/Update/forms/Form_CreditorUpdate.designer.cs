using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Update.Controls;
using DebtPlus.UI.Creditor.Update.Pages;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    partial class Form_CreditorUpdate : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.XtraTabPage_General = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Name = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Contacts = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Contribution = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Interest = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Payments = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Operating = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Notes = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Aliases = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Statistics = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_PolicyMatrix = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_EFT = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Proposals = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_BalVer = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Drops = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Msgs = new DevExpress.XtraTab.XtraTabPage();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.MainMenu = new DevExpress.XtraBars.Bar();
            this.BarSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem_Items = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_View = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_Letters = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_WWW = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_www_user = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_www_user_create = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_www_messages = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_www_password = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem_Help = new DevExpress.XtraBars.BarSubItem();
            this.StatusBar = new DevExpress.XtraBars.Bar();
            this.BarStaticItem_Creditor = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_CreditorName = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_EFT = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_Check = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.BarButtonItem_File = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_View = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            this.XtraTabPage_General.SuspendLayout();
            this.XtraTabPage_Name.SuspendLayout();
            this.XtraTabPage_Contacts.SuspendLayout();
            this.XtraTabPage_Contribution.SuspendLayout();
            this.XtraTabPage_Interest.SuspendLayout();
            this.XtraTabPage_Payments.SuspendLayout();
            this.XtraTabPage_Operating.SuspendLayout();
            this.XtraTabPage_Notes.SuspendLayout();
            this.XtraTabPage_Aliases.SuspendLayout();
            this.XtraTabPage_Statistics.SuspendLayout();
            this.XtraTabPage_PolicyMatrix.SuspendLayout();
            this.XtraTabPage_EFT.SuspendLayout();
            this.XtraTabPage_Proposals.SuspendLayout();
            this.XtraTabPage_BalVer.SuspendLayout();
            this.XtraTabPage_Drops.SuspendLayout();
            this.XtraTabPage_Msgs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            this.SuspendLayout();
            //
            //XtraTabControl1
            //
            this.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XtraTabControl1.Location = new System.Drawing.Point(0, 24);
            this.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.SelectedTabPage = this.XtraTabPage_General;
            this.XtraTabControl1.Size = new System.Drawing.Size(584, 335);
            this.XtraTabControl1.TabIndex = 2;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
				this.XtraTabPage_General,
				this.XtraTabPage_Name,
				this.XtraTabPage_Contacts,
				this.XtraTabPage_Contribution,
				this.XtraTabPage_Interest,
				this.XtraTabPage_Payments,
				this.XtraTabPage_Operating,
				this.XtraTabPage_Notes,
				this.XtraTabPage_Aliases,
				this.XtraTabPage_Statistics,
				this.XtraTabPage_PolicyMatrix,
				this.XtraTabPage_EFT,
				this.XtraTabPage_Proposals,
				this.XtraTabPage_BalVer,
				this.XtraTabPage_Drops,
				this.XtraTabPage_Msgs
			});
            //
            //XtraTabPage_General
            //
            this.XtraTabPage_General.Name = "XtraTabPage_General";
            this.XtraTabPage_General.Size = new System.Drawing.Size(577, 284);
            this.XtraTabPage_General.Tag = "Page_General";
            this.XtraTabPage_General.Text = "General";
            //
            //XtraTabPage_Name
            //
            this.XtraTabPage_Name.Name = "XtraTabPage_Name";
            this.XtraTabPage_Name.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Name.Tag = "Page_Name";
            this.XtraTabPage_Name.Text = "Name";
            //
            //XtraTabPage_Contacts
            //
            this.XtraTabPage_Contacts.Name = "XtraTabPage_Contacts";
            this.XtraTabPage_Contacts.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Contacts.Tag = "Page_Contacts";
            this.XtraTabPage_Contacts.Text = "Contacts";
            //
            //XtraTabPage_Contribution
            //
            this.XtraTabPage_Contribution.Name = "XtraTabPage_Contribution";
            this.XtraTabPage_Contribution.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Contribution.Tag = "Page_Contribution";
            this.XtraTabPage_Contribution.Text = "Contribution";
            //
            //XtraTabPage_Interest
            //
            this.XtraTabPage_Interest.Name = "XtraTabPage_Interest";
            this.XtraTabPage_Interest.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Interest.Tag = "Page_Interest";
            this.XtraTabPage_Interest.Text = "Interest";
            //
            //XtraTabPage_Payments
            //
            this.XtraTabPage_Payments.Name = "XtraTabPage_Payments";
            this.XtraTabPage_Payments.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Payments.Tag = "Page_Payments";
            this.XtraTabPage_Payments.Text = "Payments";
            //
            //XtraTabPage_Operating
            //
            this.XtraTabPage_Operating.Name = "XtraTabPage_Operating";
            this.XtraTabPage_Operating.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Operating.Tag = "Page_Invoices";
            this.XtraTabPage_Operating.Text = "Operating Acct";
            //
            //XtraTabPage_Notes
            //
            this.XtraTabPage_Notes.Name = "XtraTabPage_Notes";
            this.XtraTabPage_Notes.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Notes.Tag = "Page_Notes";
            this.XtraTabPage_Notes.Text = "Notes";
            //
            //XtraTabPage_Aliases
            //
            this.XtraTabPage_Aliases.Name = "XtraTabPage_Aliases";
            this.XtraTabPage_Aliases.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Aliases.Tag = "Page_Aliases";
            this.XtraTabPage_Aliases.Text = "Aliases";
            //
            //XtraTabPage_Statistics
            //
            this.XtraTabPage_Statistics.Name = "XtraTabPage_Statistics";
            this.XtraTabPage_Statistics.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Statistics.Tag = "Page_Statistics";
            this.XtraTabPage_Statistics.Text = "Statistics";
            //
            //XtraTabPage_PolicyMatrix
            //
            this.XtraTabPage_PolicyMatrix.Name = "XtraTabPage_PolicyMatrix";
            this.XtraTabPage_PolicyMatrix.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_PolicyMatrix.Tag = "Page_PolicyMatrix";
            this.XtraTabPage_PolicyMatrix.Text = "Policy Matrix";
            //
            //XtraTabPage_EFT
            //
            this.XtraTabPage_EFT.Name = "XtraTabPage_EFT";
            this.XtraTabPage_EFT.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_EFT.Tag = "Page_ChecksEFT";
            this.XtraTabPage_EFT.Text = "Checks/EFT";
            //
            //XtraTabPage_Proposals
            //
            this.XtraTabPage_Proposals.Name = "XtraTabPage_Proposals";
            this.XtraTabPage_Proposals.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Proposals.Tag = "Page_Proposals";
            this.XtraTabPage_Proposals.Text = "Proposals";
            //
            //XtraTabPage_BalVer
            //
            this.XtraTabPage_BalVer.Name = "XtraTabPage_BalVer";
            this.XtraTabPage_BalVer.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_BalVer.Tag = "Page_BalanceVerification";
            this.XtraTabPage_BalVer.Text = "Bal Ver";
            //
            //XtraTabPage_Drops
            //
            this.XtraTabPage_Drops.Name = "XtraTabPage_Drops";
            this.XtraTabPage_Drops.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Drops.Tag = "Page_Drops";
            this.XtraTabPage_Drops.Text = "Drops";
            //
            //XtraTabPage_Msgs
            //
            this.XtraTabPage_Msgs.Name = "XtraTabPage_Msgs";
            this.XtraTabPage_Msgs.Size = new System.Drawing.Size(577, 287);
            this.XtraTabPage_Msgs.Tag = "Page_Msgs";
            this.XtraTabPage_Msgs.Text = "Msgs";
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
				this.MainMenu,
				this.StatusBar
			});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarButtonItem_File,
				this.BarButtonItem_View,
				this.BarSubItem_File,
				this.BarButtonItem_File_Exit,
				this.BarSubItem_Items,
				this.BarSubItem_View,
				this.BarSubItem_Letters,
				this.BarSubItem_Reports,
				this.BarSubItem_WWW,
				this.BarSubItem_Help,
				this.BarButtonItem_www_user,
				this.BarButtonItem_www_user_create,
				this.BarButtonItem_www_messages,
				this.BarButtonItem_www_password,
				this.BarStaticItem_Creditor,
				this.BarStaticItem_CreditorName,
				this.BarStaticItem_EFT,
				this.BarStaticItem_Check
			});
            this.barManager1.MainMenu = this.MainMenu;
            this.barManager1.MaxItemId = 19;
            this.barManager1.StatusBar = this.StatusBar;
            //
            //MainMenu
            //
            this.MainMenu.BarName = "Main menu";
            this.MainMenu.DockCol = 0;
            this.MainMenu.DockRow = 0;
            this.MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_File),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Items),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Letters),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Reports),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_WWW),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Help)
			});
            this.MainMenu.OptionsBar.MultiLine = true;
            this.MainMenu.OptionsBar.UseWholeRow = true;
            this.MainMenu.Text = "Main menu";
            //
            //BarSubItem_File
            //
            this.BarSubItem_File.Caption = "&File";
            this.BarSubItem_File.Id = 2;
            this.BarSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit) });
            this.BarSubItem_File.Name = "BarSubItem_File";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 3;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //BarSubItem_Items
            //
            this.BarSubItem_Items.Caption = "&Items";
            this.BarSubItem_Items.Id = 4;
            this.BarSubItem_Items.Name = "BarSubItem_Items";
            //
            //BarSubItem_View
            //
            this.BarSubItem_View.Caption = "&View";
            this.BarSubItem_View.Id = 5;
            this.BarSubItem_View.Name = "BarSubItem_View";
            //
            //BarSubItem_Letters
            //
            this.BarSubItem_Letters.Caption = "&Letters";
            this.BarSubItem_Letters.Id = 6;
            this.BarSubItem_Letters.Name = "BarSubItem_Letters";
            //
            //BarSubItem_Reports
            //
            this.BarSubItem_Reports.Caption = "&Reports";
            this.BarSubItem_Reports.Id = 7;
            this.BarSubItem_Reports.Name = "BarSubItem_Reports";
            //
            //BarSubItem_WWW
            //
            this.BarSubItem_WWW.Caption = "&WWW";
            this.BarSubItem_WWW.Id = 8;
            this.BarSubItem_WWW.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_www_user),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_www_messages),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_www_password)
			});
            this.BarSubItem_WWW.Name = "BarSubItem_WWW";
            //
            //BarButtonItem_www_user
            //
            this.BarButtonItem_www_user.Caption = "&User";
            this.BarButtonItem_www_user.Id = 11;
            this.BarButtonItem_www_user.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_www_user_create) });
            this.BarButtonItem_www_user.Name = "BarButtonItem_www_user";
            //
            //BarButtonItem_www_user_create
            //
            this.BarButtonItem_www_user_create.Caption = "&Create....";
            this.BarButtonItem_www_user_create.Id = 12;
            this.BarButtonItem_www_user_create.Name = "BarButtonItem_www_user_create";
            //
            //BarButtonItem_www_messages
            //
            this.BarButtonItem_www_messages.Caption = "&Messages...";
            this.BarButtonItem_www_messages.Id = 13;
            this.BarButtonItem_www_messages.Name = "BarButtonItem_www_messages";
            //
            //BarButtonItem_www_password
            //
            this.BarButtonItem_www_password.Caption = "&Password...";
            this.BarButtonItem_www_password.Id = 14;
            this.BarButtonItem_www_password.Name = "BarButtonItem_www_password";
            //
            //BarSubItem_Help
            //
            this.BarSubItem_Help.Caption = "&Help";
            this.BarSubItem_Help.Id = 9;
            this.BarSubItem_Help.Name = "BarSubItem_Help";
            //
            //StatusBar
            //
            this.StatusBar.Manager = barManager1;
            this.StatusBar.BarName = "Status bar";
            this.StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.StatusBar.DockCol = 0;
            this.StatusBar.DockRow = 0;
            this.StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.StatusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Creditor),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_CreditorName),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_EFT),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Check)
			});
            this.StatusBar.OptionsBar.AllowQuickCustomization = false;
            this.StatusBar.OptionsBar.DrawDragBorder = false;
            this.StatusBar.OptionsBar.UseWholeRow = true;
            this.StatusBar.Text = "Status bar";
            //
            //BarStaticItem_Creditor
            //
            this.BarStaticItem_Creditor.Caption = "ID";
            this.BarStaticItem_Creditor.Id = 15;
            this.BarStaticItem_Creditor.Name = "BarStaticItem_Creditor";
            this.BarStaticItem_Creditor.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //BarStaticItem_CreditorName
            //
            this.BarStaticItem_CreditorName.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.BarStaticItem_CreditorName.Caption = "CREDITOR NAME";
            this.BarStaticItem_CreditorName.Id = 16;
            this.BarStaticItem_CreditorName.Name = "BarStaticItem_CreditorName";
            this.BarStaticItem_CreditorName.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_CreditorName.Width = 32;
            //
            //BarStaticItem_EFT
            //
            this.BarStaticItem_EFT.Caption = "EFT";
            this.BarStaticItem_EFT.Id = 17;
            this.BarStaticItem_EFT.Name = "BarStaticItem_EFT";
            this.BarStaticItem_EFT.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //BarStaticItem_Check
            //
            this.BarStaticItem_Check.Caption = "CHECK";
            this.BarStaticItem_Check.Id = 18;
            this.BarStaticItem_Check.Name = "BarStaticItem_Check";
            this.BarStaticItem_Check.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //barDockControlTop
            //
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(584, 24);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 359);
            this.barDockControlBottom.Size = new System.Drawing.Size(584, 26);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 335);
            //
            //barDockControlRight
            //
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(584, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 335);
            //
            //BarButtonItem_File
            //
            this.BarButtonItem_File.Caption = "&File";
            this.BarButtonItem_File.Id = 0;
            this.BarButtonItem_File.Name = "BarButtonItem_File";
            //
            //BarButtonItem_View
            //
            this.BarButtonItem_View.Caption = "&View";
            this.BarButtonItem_View.Id = 1;
            this.BarButtonItem_View.Name = "BarButtonItem_View";
            //
            //Form_CreditorUpdate
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(584, 385);
            this.Controls.Add(this.XtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_CreditorUpdate";
            this.Text = "Creditor Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            this.XtraTabPage_General.ResumeLayout(false);
            this.XtraTabPage_Name.ResumeLayout(false);
            this.XtraTabPage_Contacts.ResumeLayout(false);
            this.XtraTabPage_Contribution.ResumeLayout(false);
            this.XtraTabPage_Interest.ResumeLayout(false);
            this.XtraTabPage_Payments.ResumeLayout(false);
            this.XtraTabPage_Operating.ResumeLayout(false);
            this.XtraTabPage_Notes.ResumeLayout(false);
            this.XtraTabPage_Aliases.ResumeLayout(false);
            this.XtraTabPage_Statistics.ResumeLayout(false);
            this.XtraTabPage_PolicyMatrix.ResumeLayout(false);
            this.XtraTabPage_EFT.ResumeLayout(false);
            this.XtraTabPage_Proposals.ResumeLayout(false);
            this.XtraTabPage_BalVer.ResumeLayout(false);
            this.XtraTabPage_Drops.ResumeLayout(false);
            this.XtraTabPage_Msgs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraTab.XtraTabControl XtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_General;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Name;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Contacts;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Contribution;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Interest;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Payments;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Operating;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Notes;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Aliases;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Statistics;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_PolicyMatrix;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_EFT;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Proposals;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_BalVer;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Drops;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Msgs;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar MainMenu;
        private DevExpress.XtraBars.Bar StatusBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem BarSubItem_File;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        private DevExpress.XtraBars.BarSubItem BarSubItem_Items;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_File;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_View;
        private DevExpress.XtraBars.BarSubItem BarSubItem_View;
        private DevExpress.XtraBars.BarSubItem BarSubItem_Letters;
        private DevExpress.XtraBars.BarSubItem BarSubItem_Reports;
        private DevExpress.XtraBars.BarSubItem BarSubItem_WWW;
        private DevExpress.XtraBars.BarSubItem BarSubItem_Help;
        private DevExpress.XtraBars.BarSubItem BarButtonItem_www_user;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_www_messages;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_www_password;
        private DevExpress.XtraBars.BarStaticItem BarStaticItem_Creditor;
        private DevExpress.XtraBars.BarStaticItem BarStaticItem_CreditorName;
        private DevExpress.XtraBars.BarStaticItem BarStaticItem_EFT;
        private DevExpress.XtraBars.BarStaticItem BarStaticItem_Check;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_www_user_create;
    }
}