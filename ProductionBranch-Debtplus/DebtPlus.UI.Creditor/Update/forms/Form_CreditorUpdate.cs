#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.WWW;
using DevExpress.XtraBars;
using DevExpress.XtraTab;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    public partial class Form_CreditorUpdate
    {
        // The record that is being edited by this instance of the editing form
        private DebtPlus.LINQ.creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize today new instance of our class
        /// </summary>
        public Form_CreditorUpdate()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize today new instance of our class
        /// </summary>
        /// <param name="creditorRecord">The current creditor record being edited</param>
        public Form_CreditorUpdate(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor creditorRecord, bool isCreated)
            : this()
        {
            // Save the parameters for later use
            this.bc             = bc;
            this.creditorRecord = creditorRecord;
            this.isCreated      = isCreated;

            // Register the event handlers
            RegisterHandlers();
        }

        /// <summary>
        /// Is the record created or is it edited? Some functions are not performed on today created record.
        /// </summary>
        public bool isCreated { get; private set; }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Resize                                  += Form_CreditorUpdate_Resize;
            Load                                    += CreditorUpdateForm_Load;
            FormClosing                             += CreditorUpdateForm_Closing;
            XtraTabControl1.SelectedPageChanging    += XtraTabControl1_SelectedPageChanging;
            XtraTabControl1.SelectedPageChanged     += XtraTabControl1_SelectedPageChanged;
            BarButtonItem_File_Exit.ItemClick       += BarButtonItem_File_Exit_ItemClick;
            BarButtonItem_www_messages.ItemClick    += MenuItem_WWW_Messages_Click;
            BarButtonItem_www_password.ItemClick    += MenuItem_WWW_Password_Click;
            BarButtonItem_www_user_create.ItemClick += MenuItem_WWW_User_Create_Click;

            // If there is a creditor record then add the monitor for the contribution list.
            if (creditorRecord != null)
            {
                creditorRecord.creditor_contribution_pcts.ListChanged += creditor_contribution_pcts_ListChanged;
            }
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Resize                                  -= Form_CreditorUpdate_Resize;
            FormClosing                             -= CreditorUpdateForm_Closing;
            Load                                    -= CreditorUpdateForm_Load;
            XtraTabControl1.SelectedPageChanging    -= XtraTabControl1_SelectedPageChanging;
            XtraTabControl1.SelectedPageChanged     -= XtraTabControl1_SelectedPageChanged;
            BarButtonItem_File_Exit.ItemClick       -= BarButtonItem_File_Exit_ItemClick;
            BarButtonItem_www_messages.ItemClick    -= MenuItem_WWW_Messages_Click;
            BarButtonItem_www_password.ItemClick    -= MenuItem_WWW_Password_Click;
            BarButtonItem_www_user_create.ItemClick -= MenuItem_WWW_User_Create_Click;

            // If there is a creditor record then remove the monitor for the contribution list.
            if (creditorRecord != null)
            {
                creditorRecord.creditor_contribution_pcts.ListChanged -= creditor_contribution_pcts_ListChanged;
            }
        }

        #region Form

        /// <summary>
        /// Process the FILE -> CLOSE CLICK event
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Handle the change in the creditor name for the status bar
        /// </summary>
        private void creditor_name_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.TextEdit source = sender as DevExpress.XtraEditors.TextEdit;
            if (source != null)
            {
                BarStaticItem_CreditorName.Caption = source.Text.Trim();
            }
        }

        /// <summary>
        /// Process the FORM : CLOSING event
        /// </summary>
        private void CreditorUpdateForm_Closing(object sender, FormClosingEventArgs e)
        {
            // If we are closing because it is normal (and not a shutdown) then save the form data
            if (e.CloseReason == CloseReason.UserClosing)
            {
                SaveForm();
            }
        }

        /// <summary>
        /// Process the FORM : LOAD event
        /// </summary>
        private void CreditorUpdateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the placement from previously
                LoadPlacement("Creditor.Update");

                // Load the menu items
                Load_Reports_MenuItems();
                Load_Letters_MenuItems();
                Load_View_MenuItems();

                // Update the status bar information as needed for the first time
                BarStaticItem_Creditor.Caption = creditorRecord.Id;
                BarStaticItem_CreditorName.Caption = creditorRecord.creditor_name;
                MyCreditorContributionPcts1_ContributionChanged();

                // Set the title bar text correctly
                Text = "DebtPlus " + creditorRecord.Id;

                // Force the tab control to show the general tab page at the start, no matter what the
                // previous designer page was.
                XtraTabControl1.SelectedTabPage = XtraTabPage_General;
                selectCurrentPage();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the FORM : RESIZE event
        /// </summary>
        private void Form_CreditorUpdate_Resize(object sender, EventArgs e)
        {
            // If the window is minimized then use the creditor ID
            if (WindowState == FormWindowState.Minimized)
            {
                Text = creditorRecord.Id;
            }
            else
            {
                // When the forms is full size then use the whole header line
                Text = "DebtPlus " + creditorRecord.Id;
            }
        }

        /// <summary>
        /// Raised when the contribution information is changed by the user
        /// </summary>
        private void creditor_contribution_pcts_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        {
            // We don't really care what was done to the list. Reset the display for all changes
            MyCreditorContributionPcts1_ContributionChanged();
        }

        /// <summary>
        /// Set the display fields for the creditor's EFT and proposal status
        /// </summary>
        private void MyCreditorContributionPcts1_ContributionChanged()
        {
            // We need the first "invalid" date for the search.
            var tomorrow = DateTime.Now.Date.AddDays(1);

            // Find the current contribution information from the collection.
            var q = creditorRecord.creditor_contribution_pcts.Where(s => s.effective_date < tomorrow).OrderByDescending(s => s.effective_date).FirstOrDefault();
            if (q != null)
            {
                BarStaticItem_EFT.Caption   = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(q.fairshare_pct_eft, q.creditor_type_eft);
                BarStaticItem_Check.Caption = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(q.fairshare_pct_check, q.creditor_type_check);
                return;
            }

            BarStaticItem_EFT.Caption   = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(0.0, 'N');
            BarStaticItem_Check.Caption = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(0.0, 'N');
        }
        #endregion Form

        #region File Menu

        /// <summary>
        /// Process the FILE_MENU : EXIT : CLICK event
        /// </summary>
        private void MenuItem_File_Exit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        #endregion File Menu

        #region View Menu

        /// <summary>
        /// Load the view menu items from the tab pages
        /// </summary>
        private void Load_View_MenuItems()
        {
            // Load the view menu items with the tab pages
            BarSubItem_View.Reset();
            foreach (XtraTabPage TabPage in XtraTabControl1.TabPages)
            {
                BarCheckItem NewItem = new BarCheckItem()
                {
                    Caption = "&" + TabPage.Text,
                    Checked = false,
                    Tag = TabPage.Tag
                };
                NewItem.ItemClick += MenuItem_View_Click;
                BarSubItem_View.AddItem(NewItem);
            }
            BarSubItem_View.Popup += MenuItem_View_Popup;
        }

        /// <summary>
        /// Process the VIEW menu items CLICK event
        /// </summary>
        private void MenuItem_View_Click(object sender, EventArgs e)
        {
            BarItemLink Link = ((BarManager)sender).PressedLink;
            BarItem Item = Link.Item;
            string SelectedPage = Convert.ToString(Item.Tag);

            // Find the desired page and select it. It will trip the update events automatically.
            foreach (XtraTabPage TabPage in XtraTabControl1.TabPages)
            {
                if (Convert.ToString(TabPage.Tag) == SelectedPage)
                {
                    XtraTabControl1.SelectedTabPageIndex = TabPage.TabIndex;
                    break;
                }
            }
        }

        /// <summary>
        /// Process the VIEW menu items POPUP event
        /// </summary>
        private void MenuItem_View_Popup(object sender, EventArgs e)
        {
            BarSubItem SubItem = (BarSubItem)sender;
            string ShowingPage = Convert.ToString(XtraTabControl1.SelectedTabPage.Tag);
            foreach (BarCheckItemLink Item in SubItem.ItemLinks)
            {
                ((BarCheckItem)Item.Item).Checked = string.Compare(Convert.ToString(((BarCheckItem)Item.Item).Tag), ShowingPage, false) == 0;
            }
        }

        #endregion View Menu

        #region WWW Menu
        private www_class creditor_www_class = null;

        /// <summary>
        /// Process the WWW MENU : MESSAGES : CLICK event
        /// </summary>
        private void MenuItem_WWW_Messages_Click(object sender, EventArgs e)
        {
            if (creditor_www_class == null)
            {
                creditor_www_class = new www_class(creditorRecord.Id);
            }
            creditor_www_class.ShowMessagesForm();
        }

        /// <summary>
        /// Process the WWW MENU : PASSWORD : CLICK event
        /// </summary>
        private void MenuItem_WWW_Password_Click(object sender, EventArgs e)
        {
            if (creditor_www_class == null)
            {
                creditor_www_class = new www_class(creditorRecord.Id);
            }
            creditor_www_class.ChangePassword();
        }

        /// <summary>
        /// Process the WWW_MENU : POPUP event
        /// </summary>
        private void MenuItem_WWW_Popup(object sender, EventArgs e)
        {
            // Create the class if not defined
            if (creditor_www_class == null)
            {
                creditor_www_class = new www_class(creditorRecord.Id);
            }

            // If the creditor has access then disable the "create" function otherwise, disable all but the "create" function.
            if (creditor_www_class.IsCreated())
            {
                BarButtonItem_www_messages.Enabled = true;
                BarButtonItem_www_password.Enabled = true;
                BarButtonItem_www_user_create.Enabled = false;
                BarButtonItem_www_user.Enabled = false;
            }
            else
            {
                BarButtonItem_www_messages.Enabled = false;
                BarButtonItem_www_password.Enabled = false;
                BarButtonItem_www_user_create.Enabled = true;
                BarButtonItem_www_user.Enabled = true;
            }
        }

        /// <summary>
        /// Process the WWW MENU : USER : CREATE : CLICK event
        /// </summary>
        private void MenuItem_WWW_User_Create_Click(object sender, EventArgs e)
        {
            if (creditor_www_class == null)
            {
                creditor_www_class = new www_class(creditorRecord.Id);
            }
            creditor_www_class.CreateUser();
        }

        #endregion WWW Menu

        #region Reports Menu

        private void Load_Reports_MenuItems()
        {
            Int32 reportCounter = 0;

            // Clear the current reports menu items. We need to remove the "Temp" item that enables the popup
            BarSubItem_Reports.ClearLinks();

            try
            {
                foreach (report currentRecord in
                        DebtPlus.LINQ.Cache.report.getList()
                            .FindAll(s => string.Compare(s.Type, "CR", true) == 0 && !string.IsNullOrWhiteSpace(s.menu_name))
                            .OrderBy(s => s.menu_name))
                {
                    string MenuName = currentRecord.menu_name.Trim();
                    BarSubItem band = BarSubItem_Reports;
                    do
                    {
                        Int32 DirectoryOffset = MenuName.IndexOf('\\');
                        if (DirectoryOffset < 0)
                        {
                            break;
                        }

                        string GroupName = MenuName.Substring(0, DirectoryOffset).Trim();
                        MenuName = MenuName.Substring(DirectoryOffset + 1);
                        if (GroupName != string.Empty)
                        {
                            BarSubItem FoundItem = null;
                            foreach (BarSubItem SearchItem in band.ItemLinks)
                            {
                                if (string.Compare(SearchItem.Caption, GroupName, true) == 0)
                                {
                                    FoundItem = SearchItem;
                                    break;
                                }
                            }

                            if (FoundItem == null)
                            {
                                FoundItem = new BarSubItem()
                                {
                                    Manager = barManager1,
                                    Caption = GroupName,
                                    Name = string.Format("Reports_Band_{0:f0}", ++reportCounter)
                                };
                                band.AddItem(FoundItem);
                            }
                            band = FoundItem;
                        }
                    } while (true);

                    var NewItem = new DebtPlus.UI.Creditor.Update.ReportsMenuItem(creditorRecord, currentRecord, MenuName)
                                    {
                                        Manager = barManager1,
                                        Name = string.Format("Reports_Item_{0:f0}", ++reportCounter)
                                    };
                    band.AddItem(NewItem);

                    // Hook a procedure to save the data when the menu item is chosen. The database needs to be updated
                    // before the report is run.
                    NewItem.ItemClick += LetterReport_ItemClick;
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error building reports menu");
            }
        }

        #endregion Reports Menu

        #region Letters Menu

        /// <summary>
        /// Load the Letters menu items from the database tables
        /// </summary>
        private void Load_Letters_MenuItems()
        {
            Int32 letterCounter = 0;
            try
            {
                foreach (letter_type currentRecord in
                        DebtPlus.LINQ.Cache.letter_type.getList()
                            .FindAll(s => string.Compare(s.letter_group, "CR", true) == 0 && !string.IsNullOrWhiteSpace(s.menu_name))
                            .OrderBy(s => s.menu_name))
                {
                    string MenuName = currentRecord.menu_name.Trim();
                    BarSubItem band = BarSubItem_Letters;
                    do
                    {
                        Int32 DirectoryOffset = MenuName.IndexOf('\\');
                        if (DirectoryOffset < 0)
                        {
                            break;
                        }
                        string GroupName = MenuName.Substring(0, DirectoryOffset).Trim();
                        MenuName = MenuName.Substring(DirectoryOffset + 1);
                        if (GroupName != string.Empty)
                        {
                            BarSubItem FoundItem = null;
                            foreach (BarSubItem SearchItem in band.ItemLinks)
                            {
                                if (string.Compare(SearchItem.Caption, GroupName, true) == 0)
                                {
                                    FoundItem = SearchItem;
                                    break;
                                }
                            }

                            if (FoundItem == null)
                            {
                                FoundItem = new BarSubItem()
                                                {
                                                    Manager = barManager1,
                                                    Caption = GroupName,
                                                    Name = string.Format("Letters_Band_{0:f0}", ++letterCounter)
                                                };
                                band.AddItem(FoundItem);
                            }
                            band = FoundItem;
                        }
                    }
                    while (true);

                    // Create the menu item for the letter
                    var NewItem = new DebtPlus.UI.Creditor.Update.LettersMenuItem(creditorRecord, currentRecord, MenuName)
                                        {
                                            Manager = barManager1,
                                            Name = string.Format("Letters_Item_{0:f0}", ++letterCounter)
                                        };

                    band.AddItem(NewItem);

                    // Hook a procedure to save the data when the menu item is chosen. The database needs to be updated
                    // before the report is run.
                    NewItem.ItemClick += LetterReport_ItemClick;
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error building letters menu");
            }
        }

        #endregion Letters Menu
    }
}