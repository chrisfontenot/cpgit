
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Creditor.Update.Forms
{
	partial class Form_EFTLocationEdit : DebtPlus.Data.Forms.DebtPlusForm
	{
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
            {
				if (disposing && components != null)
                {
					components.Dispose();
				}
            }
            finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_EFTLocationEdit));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.biller_id = new DevExpress.XtraEditors.ButtonEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lk_region = new DevExpress.XtraEditors.LookUpEdit();
            this.lk_bank = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.biller_id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lk_region.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lk_bank.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // biller_id
            // 
            this.biller_id.Location = new System.Drawing.Point(56, 77);
            this.biller_id.Name = "biller_id";
            this.biller_id.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Search", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("biller_id.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Press this button to search for the biller ID", "search", null, true)});
            this.biller_id.Properties.Mask.BeepOnError = true;
            this.biller_id.Properties.Mask.EditMask = "\\d{10}|\\d{12}";
            this.biller_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.biller_id.Properties.Mask.SaveLiteral = false;
            this.biller_id.Properties.MaxLength = 12;
            this.biller_id.Size = new System.Drawing.Size(184, 20);
            this.biller_id.TabIndex = 5;
            this.biller_id.ToolTip = "Enter the appropriate RPPS biller ID";
            this.biller_id.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(8, 32);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(33, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "&Region";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(8, 56);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(23, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "&Bank";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(8, 80);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(36, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Biller &ID";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // lk_region
            // 
            this.lk_region.Location = new System.Drawing.Point(56, 29);
            this.lk_region.Name = "lk_region";
            this.lk_region.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lk_region.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lk_region.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.lk_region.Properties.DisplayMember = "description";
            this.lk_region.Properties.NullText = "All Regions";
            this.lk_region.Properties.ShowFooter = false;
            this.lk_region.Properties.ShowHeader = false;
            this.lk_region.Properties.SortColumnIndex = 1;
            this.lk_region.Properties.ValueMember = "Id";
            this.lk_region.Size = new System.Drawing.Size(184, 20);
            this.lk_region.TabIndex = 1;
            this.lk_region.ToolTipController = this.ToolTipController1;
            // 
            // lk_bank
            // 
            this.lk_bank.Location = new System.Drawing.Point(56, 53);
            this.lk_bank.Name = "lk_bank";
            this.lk_bank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lk_bank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.lk_bank.Properties.DisplayMember = "description";
            this.lk_bank.Properties.NullText = "";
            this.lk_bank.Properties.ShowFooter = false;
            this.lk_bank.Properties.ShowHeader = false;
            this.lk_bank.Properties.SortColumnIndex = 1;
            this.lk_bank.Properties.ValueMember = "Id";
            this.lk_bank.Size = new System.Drawing.Size(184, 20);
            this.lk_bank.TabIndex = 3;
            this.lk_bank.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(35, 112);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 6;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(139, 112);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Form_EFTLocationEdit
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(248, 150);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.lk_bank);
            this.Controls.Add(this.lk_region);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.biller_id);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_EFTLocationEdit";
            this.Text = "Creditor Method";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.biller_id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lk_region.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lk_bank.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.ButtonEdit biller_id;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LookUpEdit lk_region;
		private DevExpress.XtraEditors.LookUpEdit lk_bank;
		private DevExpress.XtraEditors.SimpleButton Button_OK;
		private DevExpress.XtraEditors.SimpleButton Button_Cancel;
	}
}
