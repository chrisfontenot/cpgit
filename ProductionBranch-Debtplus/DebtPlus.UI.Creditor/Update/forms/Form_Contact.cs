#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    internal partial class Form_Contact : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current record being updated
        private creditor_contact record = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_Contact()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_Contact(creditor_contact Record)
            : this()
        {
            this.record = Record;

            // Set the lookup entries as needed
            lookUpEdit_Type.Properties.DataSource = DebtPlus.LINQ.Cache.creditor_contact_type.getList().Where(s => s.ActiveFlag).ToList();

            RegisterHandlers();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.NameID                = nameRecordControl1.EditValue;
            record.AddressID             = addressRecordControl1.EditValue;
            record.TelephoneID           = telephoneNumberRecordControl_Telephone.EditValue;
            record.FAXID                 = telephoneNumberRecordControl_FAX.EditValue;
            record.EmailID               = emailRecordControl1.EditValue;
            record.creditor_contact_type = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_Type.EditValue).GetValueOrDefault();
            record.Title                 = DebtPlus.Utils.Nulls.v_String(textEdit_title.EditValue);
            record.Notes                 = DebtPlus.Utils.Nulls.v_String(memoEdit_notes.EditValue);
        }

        private void Form_Changed(object sender, System.EventArgs e)
        {
            button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Initialize the form with context information
        /// </summary>
        private void Form_Contact_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Hook the record into the controls
                nameRecordControl1.EditValue                     = record.NameID;
                addressRecordControl1.EditValue                  = record.AddressID;
                telephoneNumberRecordControl_Telephone.EditValue = record.TelephoneID;
                telephoneNumberRecordControl_FAX.EditValue       = record.FAXID;
                emailRecordControl1.EditValue                    = record.EmailID;
                lookUpEdit_Type.EditValue                        = record.creditor_contact_type;
                textEdit_title.EditValue                         = record.Title;
                memoEdit_notes.EditValue                         = record.Notes;

                // If the user does not have permission than make the fields read-only
                if (!DebtPlus.Configuration.Config.UpdateCreditorContacts)
                {
                    memoEdit_notes.Properties.ReadOnly              = true;
                    textEdit_title.Properties.ReadOnly              = true;
                    lookUpEdit_Type.Properties.ReadOnly             = true;
                    nameRecordControl1.ReadOnly                     = true;
                    addressRecordControl1.ReadOnly                  = true;
                    telephoneNumberRecordControl_Telephone.ReadOnly = true;
                    telephoneNumberRecordControl_FAX.ReadOnly       = true;
                    emailRecordControl1.ReadOnly                    = true;
                }

                // Enable or disable the OK button based upon the control settings
                button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private bool HasErrors()
        {
            if (! DebtPlus.Configuration.Config.UpdateCreditorContacts)
            {
                return true;
            }
            return lookUpEdit_Type.EditValue == null;
        }

        private void RegisterHandlers()
        {
            Load                              += Form_Contact_Load;
            lookUpEdit_Type.EditValueChanged  += Form_Changed;
            lookUpEdit_Type.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            button_OK.Click                   += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                              -= Form_Contact_Load;
            lookUpEdit_Type.EditValueChanged  -= Form_Changed;
            lookUpEdit_Type.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            button_OK.Click                   -= Button_OK_Click;
        }
    }
}