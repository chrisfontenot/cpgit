#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Forms
{
    internal partial class Form_CreditorContributionPCT : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current record being edited
        private creditor_contribution_pct record;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_CreditorContributionPCT()
            : base()
        {
            InitializeComponent();
        }

        public Form_CreditorContributionPCT(creditor_contribution_pct Record)
            : this()
        {
            this.record = Record;

            // Load the list of valid types for the contribution records. We want just the B, D, and N types here.
            var colTypes                              = DebtPlus.LINQ.InMemory.CreditorBillDeductTypes.getList().FindAll(s => (new char[] { 'N', 'D', 'M' }).Contains(s.period)).ToList();
            creditor_type_check.Properties.DataSource = colTypes;
            creditor_type_eft.Properties.DataSource   = colTypes;

            RegisterHandlers();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.creditor_type_check = Convert.ToChar(creditor_type_check.EditValue);
            record.creditor_type_eft   = Convert.ToChar(creditor_type_eft.EditValue);
            record.fairshare_pct_check = DebtPlus.Utils.Nulls.v_Double(fairshare_pct_check.EditValue).GetValueOrDefault();
            record.fairshare_pct_eft   = DebtPlus.Utils.Nulls.v_Double(fairshare_pct_eft.EditValue);
            record.effective_date      = effective_date.DateTime.Date;

            // Correct the items to make sure that they reflect the proper values
            if (record.creditor_type_check == 'N' || record.fairshare_pct_check <= 0.0D)
            {
                record.creditor_type_check = 'N';
                record.fairshare_pct_check = 0.0D;
            }

            // Correct the items to make sure that they reflect the proper values
            if (record.fairshare_pct_eft.HasValue)
            {
                if (record.creditor_type_eft == 'N' || record.fairshare_pct_eft.Value <= 0.0D)
                {
                    record.creditor_type_eft = 'N';
                    record.fairshare_pct_eft = 0.0D;
                }
            }
        }

        private void EditValueChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private void Form_CreditorContributionPCT_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // If the record was set into the creditor then don't allow the change to be made
                if (record.Id > 0 && record.date_updated.HasValue)
                {
                    fairshare_pct_check.Properties.ReadOnly = true;
                    fairshare_pct_eft.Properties.ReadOnly   = true;
                    creditor_type_check.Properties.ReadOnly = true;
                    creditor_type_eft.Properties.ReadOnly   = true;
                    effective_date.Properties.ReadOnly      = true;
                }

                // The earliest effective date is today
                effective_date.Properties.MinValue = DateTime.Now.Date;

                creditor_type_check.EditValue      = record.creditor_type_check;
                creditor_type_eft.EditValue        = record.creditor_type_eft;
                fairshare_pct_check.EditValue      = record.fairshare_pct_check;
                fairshare_pct_eft.EditValue        = record.fairshare_pct_eft;
                effective_date.DateTime            = record.effective_date;

                // Enable the OK button based upon the current parameters
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private bool HasErrors()
        {
            // Do not allow the OK button if the values have been submitted.
            if (record.date_updated.HasValue)
            {
                return true;
            }

            // Validate the other fields
            if (creditor_type_check.EditValue == null)
            {
                return true;
            }

            if (creditor_type_eft.EditValue == null)
            {
                return true;
            }

            if (fairshare_pct_check.EditValue == null)
            {
                return true;
            }

            return false;
        }

        private void RegisterHandlers()
        {
            Load                                 += Form_CreditorContributionPCT_Load;
            effective_date.EditValueChanged      += EditValueChanged;
            fairshare_pct_check.EditValueChanged += EditValueChanged;
            fairshare_pct_eft.EditValueChanged   += EditValueChanged;
            Button_OK.Click                      += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                                 -= Form_CreditorContributionPCT_Load;
            effective_date.EditValueChanged      -= EditValueChanged;
            fairshare_pct_check.EditValueChanged -= EditValueChanged;
            fairshare_pct_eft.EditValueChanged   -= EditValueChanged;
            Button_OK.Click                      -= Button_OK_Click;
        }
    }
}