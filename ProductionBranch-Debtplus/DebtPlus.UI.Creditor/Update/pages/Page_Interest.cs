#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Interest : ControlBase
    {
        // Current creditor being edited
        private creditor creditorRecord = null;

        public Page_Interest()
            : base()
        {
            InitializeComponent();

            // Load the lookup Control datasource items
            voucher_spacing.Properties.DataSource = DebtPlus.LINQ.InMemory.VoucherSpacings.getList();
            creditor_class.Properties.DataSource = DebtPlus.LINQ.Cache.creditor_class.getList();
        }

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            UnRegisterHandlers();
            try
            {
                voucher_spacing.EditValue = creditorRecord.voucher_spacing;
                creditor_class.EditValue = creditorRecord.creditor_class;
                prohibit_use.Checked = creditorRecord.prohibit_use;
                highest_apr_amt.EditValue = creditorRecord.highest_apr_amt;
                highest_apr_pct.EditValue = creditorRecord.highest_apr_pct;
                lowest_apr_pct.EditValue = creditorRecord.lowest_apr_pct;
                max_amt_per_check.EditValue = creditorRecord.max_amt_per_check;
                max_clients_per_check.EditValue = creditorRecord.max_clients_per_check;
                max_fairshare_per_debt.EditValue = creditorRecord.max_fairshare_per_debt;
                medium_apr_amt.EditValue = creditorRecord.medium_apr_amt;
                medium_apr_pct.EditValue = creditorRecord.medium_apr_pct;
                returned_mail.EditValue = creditorRecord.returned_mail;
                mail_priority.EditValue = creditorRecord.mail_priority;
                usual_priority.EditValue = creditorRecord.usual_priority;

                // If the user does not have permission for this page then make the controls READ-ONLY
                if (!DebtPlus.Configuration.Config.UpdateCreditorInterest)
                {
                    usual_priority.Properties.ReadOnly = true;
                    mail_priority.Properties.ReadOnly = true;
                    returned_mail.Properties.ReadOnly = true;
                    returned_mail.Properties.ReadOnly = true;
                    medium_apr_pct.Properties.ReadOnly = true;
                    medium_apr_amt.Properties.ReadOnly = true;
                    lowest_apr_pct.Properties.ReadOnly = true;
                    highest_apr_amt.Properties.ReadOnly = true;
                    highest_apr_pct.Properties.ReadOnly = true;
                    max_amt_per_check.Properties.ReadOnly = true;
                    max_clients_per_check.Properties.ReadOnly = true;
                    max_fairshare_per_debt.Properties.ReadOnly = true;
                    prohibit_use.Properties.ReadOnly = true;
                    voucher_spacing.Properties.ReadOnly = true;
                    creditor_class.Properties.ReadOnly = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            UnRegisterHandlers();
            try
            {
                creditorRecord.voucher_spacing = DebtPlus.Utils.Nulls.v_Int32(voucher_spacing.EditValue).GetValueOrDefault();
                creditorRecord.creditor_class = DebtPlus.Utils.Nulls.v_Int32(creditor_class.EditValue).GetValueOrDefault();
                creditorRecord.prohibit_use = prohibit_use.Checked;
                creditorRecord.highest_apr_amt = DebtPlus.Utils.Nulls.v_Decimal(highest_apr_amt.EditValue).GetValueOrDefault();
                creditorRecord.highest_apr_pct = DebtPlus.Utils.Nulls.v_Double(highest_apr_pct.EditValue).GetValueOrDefault();
                creditorRecord.lowest_apr_pct = DebtPlus.Utils.Nulls.v_Double(lowest_apr_pct.EditValue).GetValueOrDefault();
                creditorRecord.max_amt_per_check = DebtPlus.Utils.Nulls.v_Decimal(max_amt_per_check.EditValue);
                creditorRecord.max_clients_per_check = DebtPlus.Utils.Nulls.v_Int32(max_clients_per_check.EditValue);
                creditorRecord.max_fairshare_per_debt = DebtPlus.Utils.Nulls.v_Decimal(max_fairshare_per_debt.EditValue);
                creditorRecord.medium_apr_amt = DebtPlus.Utils.Nulls.v_Decimal(medium_apr_amt.EditValue).GetValueOrDefault();
                creditorRecord.medium_apr_pct = DebtPlus.Utils.Nulls.v_Double(medium_apr_pct.EditValue).GetValueOrDefault();
                creditorRecord.returned_mail = DebtPlus.Utils.Nulls.v_DateTime(returned_mail.EditValue);
                creditorRecord.mail_priority = DebtPlus.Utils.Nulls.v_Int32(mail_priority.EditValue).GetValueOrDefault();
                creditorRecord.usual_priority = DebtPlus.Utils.Nulls.v_Int32(usual_priority.EditValue).GetValueOrDefault();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.LookUpEdit creditor_class;

        private DevExpress.XtraEditors.GroupControl GroupControl1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.GroupControl GroupControl2;

        private DevExpress.XtraEditors.GroupControl GroupControl3;

        private DevExpress.XtraEditors.CalcEdit highest_apr_amt;

        private DebtPlus.Data.Controls.PercentEdit highest_apr_pct;

        private DevExpress.XtraEditors.LabelControl LabelControl1;

        private DevExpress.XtraEditors.LabelControl LabelControl10;

        private DevExpress.XtraEditors.LabelControl LabelControl11;

        private DevExpress.XtraEditors.LabelControl LabelControl12;

        private DevExpress.XtraEditors.LabelControl LabelControl13;

        private DevExpress.XtraEditors.LabelControl LabelControl2;

        private DevExpress.XtraEditors.LabelControl LabelControl3;

        private DevExpress.XtraEditors.LabelControl LabelControl4;

        private DevExpress.XtraEditors.LabelControl LabelControl5;

        private DevExpress.XtraEditors.LabelControl LabelControl6;

        private DevExpress.XtraEditors.LabelControl LabelControl7;

        private DevExpress.XtraEditors.LabelControl LabelControl8;

        private DevExpress.XtraEditors.LabelControl LabelControl9;

        private DebtPlus.Data.Controls.PercentEdit lowest_apr_pct;

        private DevExpress.XtraEditors.SpinEdit mail_priority;

        private DevExpress.XtraEditors.CalcEdit max_amt_per_check;

        private DevExpress.XtraEditors.SpinEdit max_clients_per_check;

        private DevExpress.XtraEditors.CalcEdit max_fairshare_per_debt;

        private DevExpress.XtraEditors.CalcEdit medium_apr_amt;

        private DebtPlus.Data.Controls.PercentEdit medium_apr_pct;

        private DevExpress.XtraEditors.CheckEdit prohibit_use;

        private DevExpress.XtraEditors.DateEdit returned_mail;

        private DevExpress.XtraEditors.SpinEdit usual_priority;

        private DevExpress.XtraEditors.LookUpEdit voucher_spacing;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_Interest));
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.usual_priority = new DevExpress.XtraEditors.SpinEdit();
            this.returned_mail = new DevExpress.XtraEditors.DateEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.prohibit_use = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.GroupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.mail_priority = new DevExpress.XtraEditors.SpinEdit();
            this.max_clients_per_check = new DevExpress.XtraEditors.SpinEdit();
            this.max_fairshare_per_debt = new DevExpress.XtraEditors.CalcEdit();
            this.max_amt_per_check = new DevExpress.XtraEditors.CalcEdit();
            this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.medium_apr_amt = new DevExpress.XtraEditors.CalcEdit();
            this.highest_apr_amt = new DevExpress.XtraEditors.CalcEdit();
            this.highest_apr_pct = new DebtPlus.Data.Controls.PercentEdit();
            this.medium_apr_pct = new DebtPlus.Data.Controls.PercentEdit();
            this.lowest_apr_pct = new DebtPlus.Data.Controls.PercentEdit();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.voucher_spacing = new DevExpress.XtraEditors.LookUpEdit();
            this.creditor_class = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usual_priority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.returned_mail.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.returned_mail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prohibit_use.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl3)).BeginInit();
            this.GroupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mail_priority.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_clients_per_check.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_fairshare_per_debt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_amt_per_check.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medium_apr_amt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.highest_apr_amt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.highest_apr_pct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.medium_apr_pct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowest_apr_pct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_spacing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_class.Properties)).BeginInit();
            this.SuspendLayout();
            //
            // GroupControl2
            //
            this.GroupControl2.Controls.Add(this.usual_priority);
            this.GroupControl2.Controls.Add(this.returned_mail);
            this.GroupControl2.Controls.Add(this.LabelControl5);
            this.GroupControl2.Controls.Add(this.LabelControl4);
            this.GroupControl2.Controls.Add(this.prohibit_use);
            this.GroupControl2.Controls.Add(this.LabelControl8);
            this.GroupControl2.Controls.Add(this.creditor_class);
            this.GroupControl2.Location = new System.Drawing.Point(328, 120);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(208, 144);
            this.GroupControl2.TabIndex = 2;
            this.GroupControl2.Text = "Miscellaneous Indicators";
            //
            // usual_priority
            //
            this.usual_priority.EditValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.usual_priority.Location = new System.Drawing.Point(168, 72);
            this.usual_priority.Name = "usual_priority";
            this.usual_priority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.usual_priority.Properties.IsFloatValue = false;
            this.usual_priority.Properties.Mask.EditMask = "0";
            this.usual_priority.Properties.MaxLength = 1;
            this.usual_priority.Properties.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.usual_priority.Size = new System.Drawing.Size(32, 20);
            this.usual_priority.TabIndex = 5;
            this.usual_priority.ToolTip = "Change the priorirty for paying these debts. \"0\" is the HIGHEST priority, \"9\" is " +
    "the lowest. This is used to create new debts and the actual priority is assigned" +
    " on today per-debt basis.";
            //
            // returned_mail
            //
            this.returned_mail.EditValue = null;
            this.returned_mail.Location = new System.Drawing.Point(104, 48);
            this.returned_mail.Name = "returned_mail";
            this.returned_mail.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.returned_mail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.returned_mail.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.returned_mail.Size = new System.Drawing.Size(96, 20);
            this.returned_mail.TabIndex = 3;
            this.returned_mail.ToolTip = "This is the date that you received your mail back from the creditor due to today " +
    "bad address. Future proposals are not sent to this creditor if this field has to" +
    "day value.";
            //
            // LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(8, 76);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(102, 13);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "Usual Prorate Priority";
            //
            // LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(8, 52);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(92, 13);
            this.LabelControl4.TabIndex = 2;
            this.LabelControl4.Text = "Date Mail Returned";
            //
            // prohibit_use
            //
            this.prohibit_use.Location = new System.Drawing.Point(8, 97);
            this.prohibit_use.Name = "prohibit_use";
            this.prohibit_use.Properties.Caption = "Prohibit Use";
            this.prohibit_use.Size = new System.Drawing.Size(88, 19);
            this.prohibit_use.TabIndex = 6;
            this.prohibit_use.ToolTip = "Check this box to prevent this creditor from being assigned to new debts";
            //
            // LabelControl8
            //
            this.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl8.Location = new System.Drawing.Point(8, 28);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(24, 13);
            this.LabelControl8.TabIndex = 0;
            this.LabelControl8.Text = "Type";
            //
            // GroupControl3
            //
            this.GroupControl3.Controls.Add(this.LabelControl13);
            this.GroupControl3.Controls.Add(this.mail_priority);
            this.GroupControl3.Controls.Add(this.max_clients_per_check);
            this.GroupControl3.Controls.Add(this.max_fairshare_per_debt);
            this.GroupControl3.Controls.Add(this.max_amt_per_check);
            this.GroupControl3.Controls.Add(this.LabelControl12);
            this.GroupControl3.Controls.Add(this.LabelControl11);
            this.GroupControl3.Controls.Add(this.LabelControl10);
            this.GroupControl3.Controls.Add(this.LabelControl9);
            this.GroupControl3.Controls.Add(this.voucher_spacing);
            this.GroupControl3.Location = new System.Drawing.Point(40, 120);
            this.GroupControl3.Name = "GroupControl3";
            this.GroupControl3.Size = new System.Drawing.Size(280, 144);
            this.GroupControl3.TabIndex = 1;
            this.GroupControl3.Text = " Check Information ";
            //
            // LabelControl13
            //
            this.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl13.Location = new System.Drawing.Point(8, 124);
            this.LabelControl13.Name = "LabelControl13";
            this.LabelControl13.Size = new System.Drawing.Size(79, 13);
            this.LabelControl13.TabIndex = 8;
            this.LabelControl13.Text = "Voucher Spacing";
            //
            // mail_priority
            //
            this.mail_priority.EditValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.mail_priority.Location = new System.Drawing.Point(240, 96);
            this.mail_priority.Name = "mail_priority";
            this.mail_priority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.mail_priority.Properties.IsFloatValue = false;
            this.mail_priority.Properties.Mask.EditMask = "0";
            this.mail_priority.Properties.MaxLength = 1;
            this.mail_priority.Properties.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.mail_priority.Size = new System.Drawing.Size(32, 20);
            this.mail_priority.TabIndex = 7;
            this.mail_priority.ToolTip = "Checks are printed according to the priority grouping. This is the grouping for t" +
    "his creditor. \"0\" is the highest (will be printed first), while \"9\" is the lowes" +
    "t (printed last).";
            //
            // max_clients_per_check
            //
            this.max_clients_per_check.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.max_clients_per_check.Location = new System.Drawing.Point(224, 72);
            this.max_clients_per_check.Name = "max_clients_per_check";
            this.max_clients_per_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.max_clients_per_check.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.max_clients_per_check.Properties.DisplayFormat.FormatString = "#0; ; ";
            this.max_clients_per_check.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.max_clients_per_check.Properties.IsFloatValue = false;
            this.max_clients_per_check.Properties.Mask.EditMask = "N0";
            this.max_clients_per_check.Properties.MaxLength = 2;
            this.max_clients_per_check.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.max_clients_per_check.Size = new System.Drawing.Size(48, 20);
            this.max_clients_per_check.TabIndex = 5;
            this.max_clients_per_check.ToolTip = "This is the maximum number of clients that we will put on today single check for " +
    "this creditor. \"0\" (meaning the \"standard number\") is the default.";
            //
            // max_fairshare_per_debt
            //
            this.max_fairshare_per_debt.Location = new System.Drawing.Point(192, 48);
            this.max_fairshare_per_debt.Name = "max_fairshare_per_debt";
            this.max_fairshare_per_debt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.max_fairshare_per_debt.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.max_fairshare_per_debt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.max_fairshare_per_debt.Properties.EditFormat.FormatString = "f2";
            this.max_fairshare_per_debt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.max_fairshare_per_debt.Properties.Precision = 2;
            this.max_fairshare_per_debt.Size = new System.Drawing.Size(80, 20);
            this.max_fairshare_per_debt.TabIndex = 3;
            this.max_fairshare_per_debt.ToolTip = "This is the maximum amount of fairshare that we will calculate PER DEBT for this " +
    "creditor.";
            //
            // max_amt_per_check
            //
            this.max_amt_per_check.Location = new System.Drawing.Point(192, 24);
            this.max_amt_per_check.Name = "max_amt_per_check";
            this.max_amt_per_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.max_amt_per_check.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.max_amt_per_check.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.max_amt_per_check.Properties.EditFormat.FormatString = "f2";
            this.max_amt_per_check.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.max_amt_per_check.Properties.Precision = 2;
            this.max_amt_per_check.Size = new System.Drawing.Size(80, 20);
            this.max_amt_per_check.TabIndex = 1;
            this.max_amt_per_check.ToolTip = resources.GetString("max_amt_per_check.ToolTip");
            //
            // LabelControl12
            //
            this.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl12.Location = new System.Drawing.Point(8, 100);
            this.LabelControl12.Name = "LabelControl12";
            this.LabelControl12.Size = new System.Drawing.Size(95, 13);
            this.LabelControl12.TabIndex = 6;
            this.LabelControl12.Text = "Check Print Priority:";
            //
            // LabelControl11
            //
            this.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl11.Location = new System.Drawing.Point(8, 76);
            this.LabelControl11.Name = "LabelControl11";
            this.LabelControl11.Size = new System.Drawing.Size(130, 13);
            this.LabelControl11.TabIndex = 4;
            this.LabelControl11.Text = "Maximum Number of clients";
            //
            // LabelControl10
            //
            this.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl10.Location = new System.Drawing.Point(8, 52);
            this.LabelControl10.Name = "LabelControl10";
            this.LabelControl10.Size = new System.Drawing.Size(112, 13);
            this.LabelControl10.TabIndex = 2;
            this.LabelControl10.Text = "Max Fairshare per debt";
            //
            // LabelControl9
            //
            this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl9.Location = new System.Drawing.Point(8, 28);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new System.Drawing.Size(117, 13);
            this.LabelControl9.TabIndex = 0;
            this.LabelControl9.Text = "Maximum Amt per Check";
            //
            // GroupControl1
            //
            this.GroupControl1.Controls.Add(this.medium_apr_amt);
            this.GroupControl1.Controls.Add(this.highest_apr_amt);
            this.GroupControl1.Controls.Add(this.highest_apr_pct);
            this.GroupControl1.Controls.Add(this.medium_apr_pct);
            this.GroupControl1.Controls.Add(this.lowest_apr_pct);
            this.GroupControl1.Controls.Add(this.LabelControl7);
            this.GroupControl1.Controls.Add(this.LabelControl6);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Location = new System.Drawing.Point(40, 8);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(496, 104);
            this.GroupControl1.TabIndex = 0;
            this.GroupControl1.Text = "Interest Information";
            //
            // medium_apr_amt
            //
            this.medium_apr_amt.Location = new System.Drawing.Point(388, 48);
            this.medium_apr_amt.Name = "medium_apr_amt";
            this.medium_apr_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.medium_apr_amt.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.medium_apr_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.medium_apr_amt.Properties.EditFormat.FormatString = "f2";
            this.medium_apr_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.medium_apr_amt.Properties.Precision = 2;
            this.medium_apr_amt.Size = new System.Drawing.Size(100, 20);
            this.medium_apr_amt.TabIndex = 5;
            this.medium_apr_amt.ToolTip = "To enable the middle tier rate, enter the cutoff balance here.";
            //
            // highest_apr_amt
            //
            this.highest_apr_amt.Location = new System.Drawing.Point(388, 72);
            this.highest_apr_amt.Name = "highest_apr_amt";
            this.highest_apr_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.highest_apr_amt.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.highest_apr_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.highest_apr_amt.Properties.EditFormat.FormatString = "f2";
            this.highest_apr_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.highest_apr_amt.Properties.Precision = 2;
            this.highest_apr_amt.Size = new System.Drawing.Size(100, 20);
            this.highest_apr_amt.TabIndex = 9;
            this.highest_apr_amt.ToolTip = "To enable the upper tier, enter the cutoff amount here.";
            //
            // highest_apr_pct
            //
            this.highest_apr_pct.Location = new System.Drawing.Point(112, 72);
            this.highest_apr_pct.Name = "highest_apr_pct";
            this.highest_apr_pct.Properties.Allow100Percent = false;
            this.highest_apr_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.highest_apr_pct.Size = new System.Drawing.Size(80, 20);
            this.highest_apr_pct.TabIndex = 7;
            this.highest_apr_pct.ToolTip = "This is the upper tier interest rate. It is used for balances to the right. It is" +
    " typically the lowest interest rate.";
            //
            // medium_apr_pct
            //
            this.medium_apr_pct.Location = new System.Drawing.Point(112, 48);
            this.medium_apr_pct.Name = "medium_apr_pct";
            this.medium_apr_pct.Properties.Allow100Percent = false;
            this.medium_apr_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.medium_apr_pct.Size = new System.Drawing.Size(80, 20);
            this.medium_apr_pct.TabIndex = 3;
            this.medium_apr_pct.ToolTip = "This is the middle tier interest rate. It is used for balances above the figure t" +
    "o the right.";
            //
            // lowest_apr_pct
            //
            this.lowest_apr_pct.Location = new System.Drawing.Point(112, 24);
            this.lowest_apr_pct.Name = "lowest_apr_pct";
            this.lowest_apr_pct.Properties.Allow100Percent = false;
            this.lowest_apr_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lowest_apr_pct.Size = new System.Drawing.Size(80, 20);
            this.lowest_apr_pct.TabIndex = 1;
            this.lowest_apr_pct.ToolTip = "This is the percentage assigned by the creditor for balances below the cutoff amo" +
    "unt for MEDIUM. It is typically the highest interest rate.";
            //
            // LabelControl7
            //
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(224, 72);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(92, 13);
            this.LabelControl7.TabIndex = 8;
            this.LabelControl7.Text = "for balances above";
            //
            // LabelControl6
            //
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(224, 48);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(92, 13);
            this.LabelControl6.TabIndex = 4;
            this.LabelControl6.Text = "for balances above";
            //
            // LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 72);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(96, 13);
            this.LabelControl3.TabIndex = 6;
            this.LabelControl3.Text = "Highest APR % rate";
            //
            // LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 48);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(90, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Middle APR % rate";
            //
            // LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(8, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(94, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Lowest APR % rate";
            //
            // voucher_spacing
            //
            this.voucher_spacing.Location = new System.Drawing.Point(112, 120);
            this.voucher_spacing.Name = "voucher_spacing";
            this.voucher_spacing.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.voucher_spacing.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.voucher_spacing.Properties.DisplayMember = "description";
            this.voucher_spacing.Properties.NullText = "";
            this.voucher_spacing.Properties.ShowFooter = false;
            this.voucher_spacing.Properties.ShowHeader = false;
            this.voucher_spacing.Properties.SortColumnIndex = 1;
            this.voucher_spacing.Properties.ValueMember = "Id";
            this.voucher_spacing.Size = new System.Drawing.Size(160, 20);
            this.voucher_spacing.TabIndex = 9;
            this.voucher_spacing.ToolTip = "How should the spacing be performed on the check voucher? Single or double spaced" +
    " printed vouchers. If you choose \"None\" then the voucher detail is not printed o" +
    "n the check.";
            //
            // creditor_class
            //
            this.creditor_class.Location = new System.Drawing.Point(56, 24);
            this.creditor_class.Name = "creditor_class";
            this.creditor_class.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.creditor_class.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.creditor_class.Properties.DisplayMember = "description";
            this.creditor_class.Properties.NullText = "";
            this.creditor_class.Properties.ShowFooter = false;
            this.creditor_class.Properties.ShowHeader = false;
            this.creditor_class.Properties.SortColumnIndex = 1;
            this.creditor_class.Properties.ValueMember = "Id";
            this.creditor_class.Size = new System.Drawing.Size(144, 20);
            this.creditor_class.TabIndex = 1;
            this.creditor_class.ToolTip = "This is the type of the creditor. Most creditors are of the type \"standard credit" +
    "or\".";
            //
            // Page_Interest
            //
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.GroupControl3);
            this.Controls.Add(this.GroupControl2);
            this.Name = "Page_Interest";
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            this.GroupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usual_priority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.returned_mail.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.returned_mail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prohibit_use.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl3)).EndInit();
            this.GroupControl3.ResumeLayout(false);
            this.GroupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mail_priority.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_clients_per_check.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_fairshare_per_debt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.max_amt_per_check.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medium_apr_amt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.highest_apr_amt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.highest_apr_pct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.medium_apr_pct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowest_apr_pct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voucher_spacing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_class.Properties)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code
    }
}
