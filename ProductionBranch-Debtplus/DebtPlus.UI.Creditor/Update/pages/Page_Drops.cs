#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Drops : ControlBase
    {
        // Current creditor record being edited
        private creditor creditorRecord = null;

        public Page_Drops()
            : base()
        {
            InitializeComponent();
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DebtPlus.UI.Creditor.Update.Controls.DropTypesControl DropTypesControl1;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DebtPlus.UI.Creditor.Update.Controls.MyEFTLocation MyEFTLocation1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.MyEFTLocation1 = new DebtPlus.UI.Creditor.Update.Controls.MyEFTLocation();
            this.DropTypesControl1 = new DebtPlus.UI.Creditor.Update.Controls.DropTypesControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            this.SuspendLayout();
            //
            //MyEFTLocation1
            //
            this.MyEFTLocation1.Location = new System.Drawing.Point(305, 12);
            this.MyEFTLocation1.Name = "MyEFTLocation1";
            this.MyEFTLocation1.Size = new System.Drawing.Size(259, 272);
            this.MyEFTLocation1.TabIndex = 0;
            this.MyEFTLocation1.Type = "CLO";
            //
            //DropTypesControl1
            //
            this.DropTypesControl1.Location = new System.Drawing.Point(15, 34);
            this.DropTypesControl1.Name = "DropTypesControl1";
            this.DropTypesControl1.Size = new System.Drawing.Size(265, 247);
            this.DropTypesControl1.TabIndex = 2;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.DropTypesControl1);
            this.LayoutControl1.Controls.Add(this.MyEFTLocation1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 296);
            this.LayoutControl1.TabIndex = 3;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem2,
				this.LayoutControlGroup2,
				this.EmptySpaceItem1
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 296);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.MyEFTLocation1;
            this.LayoutControlItem2.CustomizationFormText = "List of biller types";
            this.LayoutControlItem2.Location = new System.Drawing.Point(293, 0);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(263, 276);
            this.LayoutControlItem2.Text = "List of biller types";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Send Drop Notices of These Types";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] { this.LayoutControlItem1 });
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(275, 276);
            this.LayoutControlGroup2.Text = "Send Drop Notices of These Types";
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.DropTypesControl1;
            this.LayoutControlItem1.CustomizationFormText = "Acceptable Drop Types";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(269, 251);
            this.LayoutControlItem1.Text = "Acceptable Drop Types";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(275, 0);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(18, 0);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(18, 10);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(18, 276);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //Page_Drops
            //
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Drops";
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            MyEFTLocation1.ReadForm(bc, creditorRecord);
            DropTypesControl1.ReadForm(bc, creditorRecord);
        }

        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            DropTypesControl1.SaveForm(bc);       // Record the changes to the database now.
        }
    }
}
