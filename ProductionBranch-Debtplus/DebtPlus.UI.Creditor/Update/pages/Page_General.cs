#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_General : ControlBase
    {
        // Current creditor record being edited with this page
        private creditor creditorRecord = null;

        public Page_General()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Refresh the page information as the page is being displayed
        /// </summary>
        /// <param name="CreditorRecord">Pointer to the current creditor record</param>
        public override void RefreshForm(DebtPlus.LINQ.BusinessContext bc, creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;

            UnRegisterHandlers();
            try
            {
                Read_Creditor(bc);
                Read_Prefixes(bc);
                Read_Address(bc);
                Read_Contacts(bc);

                // Suppress the controls if the user does not have edit permission
                if (!DebtPlus.Configuration.Config.UpdateCreditorGeneral)
                {
                    comment.Properties.ReadOnly = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            // Obtain the fields and update the creditor record with them
            creditorRecord.comment = DebtPlus.Utils.Nulls.v_String(comment.EditValue);
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region " Windows Form Designer generated code "

        private DevExpress.XtraEditors.LabelControl check_contribution;

        private DevExpress.XtraEditors.TextEdit comment;

        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.LabelControl contrib_cycle;

        private DevExpress.XtraEditors.LabelControl contrib_mtd_billed;

        private DevExpress.XtraEditors.LabelControl contrib_mtd_received;

        private DevExpress.XtraEditors.LabelControl contrib_ytd_billed;

        private DevExpress.XtraEditors.LabelControl contrib_ytd_received;

        private DevExpress.XtraEditors.LabelControl date_created;

        private DevExpress.XtraEditors.LabelControl distrib_mtd;

        private DevExpress.XtraEditors.LabelControl distrib_ytd;

        private DevExpress.XtraEditors.LabelControl eft_contribution;

        private DevExpress.XtraEditors.LabelControl fax;

        private DevExpress.XtraEditors.LabelControl first_distribution_date;

        private DevExpress.XtraLayout.EmptySpaceItem item0;

        private DevExpress.XtraLayout.EmptySpaceItem item1;

        private DevExpress.XtraLayout.EmptySpaceItem item2;

        private DevExpress.XtraLayout.EmptySpaceItem item3;

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl label_address;

        private DevExpress.XtraEditors.LabelControl last_invoice;

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem16;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem17;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem18;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem19;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;

        private DevExpress.XtraEditors.LabelControl phone;

        private DevExpress.XtraEditors.LabelControl prefixes;

        private DevExpress.XtraEditors.LabelControl returned_mail;

        private DevExpress.XtraEditors.LabelControl sic;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.comment = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.distrib_mtd = new DevExpress.XtraEditors.LabelControl();
            this.prefixes = new DevExpress.XtraEditors.LabelControl();
            this.first_distribution_date = new DevExpress.XtraEditors.LabelControl();
            this.date_created = new DevExpress.XtraEditors.LabelControl();
            this.distrib_ytd = new DevExpress.XtraEditors.LabelControl();
            this.check_contribution = new DevExpress.XtraEditors.LabelControl();
            this.last_invoice = new DevExpress.XtraEditors.LabelControl();
            this.contrib_mtd_received = new DevExpress.XtraEditors.LabelControl();
            this.returned_mail = new DevExpress.XtraEditors.LabelControl();
            this.fax = new DevExpress.XtraEditors.LabelControl();
            this.eft_contribution = new DevExpress.XtraEditors.LabelControl();
            this.label_address = new DevExpress.XtraEditors.LabelControl();
            this.phone = new DevExpress.XtraEditors.LabelControl();
            this.contrib_ytd_received = new DevExpress.XtraEditors.LabelControl();
            this.sic = new DevExpress.XtraEditors.LabelControl();
            this.contrib_cycle = new DevExpress.XtraEditors.LabelControl();
            this.contrib_mtd_billed = new DevExpress.XtraEditors.LabelControl();
            this.contrib_ytd_billed = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.item3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.item1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item0 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.item2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.comment.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem16).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem17).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem19).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item0).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem18).BeginInit();
            this.SuspendLayout();
            //
            //comment
            //
            this.comment.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.comment.Location = new System.Drawing.Point(62, 233);
            this.comment.Name = "comment";
            this.comment.Properties.MaxLength = 50;
            this.comment.Size = new System.Drawing.Size(502, 20);
            this.comment.StyleController = this.LayoutControl1;
            this.comment.TabIndex = 18;
            this.comment.ToolTip = "Enter today comment field here that is displayed with the creditor.";
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.distrib_mtd);
            this.LayoutControl1.Controls.Add(this.comment);
            this.LayoutControl1.Controls.Add(this.prefixes);
            this.LayoutControl1.Controls.Add(this.first_distribution_date);
            this.LayoutControl1.Controls.Add(this.date_created);
            this.LayoutControl1.Controls.Add(this.distrib_ytd);
            this.LayoutControl1.Controls.Add(this.check_contribution);
            this.LayoutControl1.Controls.Add(this.last_invoice);
            this.LayoutControl1.Controls.Add(this.contrib_mtd_received);
            this.LayoutControl1.Controls.Add(this.returned_mail);
            this.LayoutControl1.Controls.Add(this.fax);
            this.LayoutControl1.Controls.Add(this.eft_contribution);
            this.LayoutControl1.Controls.Add(this.label_address);
            this.LayoutControl1.Controls.Add(this.phone);
            this.LayoutControl1.Controls.Add(this.contrib_ytd_received);
            this.LayoutControl1.Controls.Add(this.sic);
            this.LayoutControl1.Controls.Add(this.contrib_cycle);
            this.LayoutControl1.Controls.Add(this.contrib_mtd_billed);
            this.LayoutControl1.Controls.Add(this.contrib_ytd_billed);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(448, 63, 450, 350);
            this.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 265);
            this.LayoutControl1.TabIndex = 37;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //distrib_mtd
            //
            this.distrib_mtd.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.distrib_mtd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.distrib_mtd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.distrib_mtd.CausesValidation = false;
            this.distrib_mtd.Location = new System.Drawing.Point(431, 12);
            this.distrib_mtd.Name = "distrib_mtd";
            this.distrib_mtd.Size = new System.Drawing.Size(133, 13);
            this.distrib_mtd.StyleController = this.LayoutControl1;
            this.distrib_mtd.TabIndex = 20;
            this.distrib_mtd.Tag = "*";
            this.distrib_mtd.Text = "$0.00";
            this.distrib_mtd.ToolTip = "How much money we have sent the creditor this month";
            this.distrib_mtd.UseMnemonic = false;
            //
            //prefixes
            //
            this.prefixes.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.prefixes.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.prefixes.CausesValidation = false;
            this.prefixes.Location = new System.Drawing.Point(68, 192);
            this.prefixes.Name = "prefixes";
            this.prefixes.Size = new System.Drawing.Size(496, 13);
            this.prefixes.StyleController = this.LayoutControl1;
            this.prefixes.TabIndex = 32;
            this.prefixes.Tag = "*";
            this.prefixes.ToolTip = "Account number prefixes";
            //
            //first_distribution_date
            //
            this.first_distribution_date.CausesValidation = false;
            this.first_distribution_date.Location = new System.Drawing.Point(146, 63);
            this.first_distribution_date.Name = "first_distribution_date";
            this.first_distribution_date.Size = new System.Drawing.Size(43, 13);
            this.first_distribution_date.StyleController = this.LayoutControl1;
            this.first_distribution_date.TabIndex = 36;
            this.first_distribution_date.Tag = "*";
            this.first_distribution_date.ToolTip = "Date that we first sent today payment to this creditor";
            //
            //date_created
            //
            this.date_created.CausesValidation = false;
            this.date_created.Location = new System.Drawing.Point(187, 113);
            this.date_created.Name = "date_created";
            this.date_created.Size = new System.Drawing.Size(43, 13);
            this.date_created.StyleController = this.LayoutControl1;
            this.date_created.TabIndex = 30;
            this.date_created.Tag = "*";
            this.date_created.ToolTip = "Date when the creditor was created";
            //
            //distrib_ytd
            //
            this.distrib_ytd.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.distrib_ytd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.distrib_ytd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.distrib_ytd.CausesValidation = false;
            this.distrib_ytd.Location = new System.Drawing.Point(431, 29);
            this.distrib_ytd.Name = "distrib_ytd";
            this.distrib_ytd.Size = new System.Drawing.Size(133, 13);
            this.distrib_ytd.StyleController = this.LayoutControl1;
            this.distrib_ytd.TabIndex = 21;
            this.distrib_ytd.Tag = "*";
            this.distrib_ytd.Text = "$0.00";
            this.distrib_ytd.ToolTip = "How much money we have sent the creditor this year";
            this.distrib_ytd.UseMnemonic = false;
            //
            //check_contribution
            //
            this.check_contribution.CausesValidation = false;
            this.check_contribution.Location = new System.Drawing.Point(146, 46);
            this.check_contribution.Name = "check_contribution";
            this.check_contribution.Size = new System.Drawing.Size(43, 13);
            this.check_contribution.StyleController = this.LayoutControl1;
            this.check_contribution.TabIndex = 35;
            this.check_contribution.Tag = "*";
            this.check_contribution.ToolTip = "How the creditor contributes to the agency when we pay by check ";
            //
            //last_invoice
            //
            this.last_invoice.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.last_invoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.last_invoice.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.last_invoice.CausesValidation = false;
            this.last_invoice.Location = new System.Drawing.Point(431, 175);
            this.last_invoice.Name = "last_invoice";
            this.last_invoice.Size = new System.Drawing.Size(133, 13);
            this.last_invoice.StyleController = this.LayoutControl1;
            this.last_invoice.TabIndex = 29;
            this.last_invoice.Tag = "*";
            this.last_invoice.ToolTip = "Invoice number that we last billed the creditor";
            this.last_invoice.UseMnemonic = false;
            //
            //contrib_mtd_received
            //
            this.contrib_mtd_received.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.contrib_mtd_received.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.contrib_mtd_received.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.contrib_mtd_received.CausesValidation = false;
            this.contrib_mtd_received.Location = new System.Drawing.Point(431, 46);
            this.contrib_mtd_received.Name = "contrib_mtd_received";
            this.contrib_mtd_received.Size = new System.Drawing.Size(133, 13);
            this.contrib_mtd_received.StyleController = this.LayoutControl1;
            this.contrib_mtd_received.TabIndex = 22;
            this.contrib_mtd_received.Tag = "*";
            this.contrib_mtd_received.Text = "$0.00";
            this.contrib_mtd_received.ToolTip = "How much money the creditor has given the agency this month";
            this.contrib_mtd_received.UseMnemonic = false;
            //
            //returned_mail
            //
            this.returned_mail.CausesValidation = false;
            this.returned_mail.Location = new System.Drawing.Point(83, 113);
            this.returned_mail.Name = "returned_mail";
            this.returned_mail.Size = new System.Drawing.Size(67, 13);
            this.returned_mail.StyleController = this.LayoutControl1;
            this.returned_mail.TabIndex = 31;
            this.returned_mail.Tag = "*";
            this.returned_mail.ToolTip = "Date that mail was returned. Further mailings are not sent until the address is corrected and this date is removed.";
            this.returned_mail.UseMnemonic = false;
            //
            //fax
            //
            this.fax.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.fax.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.fax.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.fax.CausesValidation = false;
            this.fax.Location = new System.Drawing.Point(431, 158);
            this.fax.Name = "fax";
            this.fax.Size = new System.Drawing.Size(133, 13);
            this.fax.StyleController = this.LayoutControl1;
            this.fax.TabIndex = 28;
            this.fax.Tag = "*";
            this.fax.ToolTip = "Primary contact FAX number";
            this.fax.UseMnemonic = false;
            //
            //eft_contribution
            //
            this.eft_contribution.CausesValidation = false;
            this.eft_contribution.Location = new System.Drawing.Point(146, 29);
            this.eft_contribution.Name = "eft_contribution";
            this.eft_contribution.Size = new System.Drawing.Size(43, 13);
            this.eft_contribution.StyleController = this.LayoutControl1;
            this.eft_contribution.TabIndex = 34;
            this.eft_contribution.Tag = "*";
            this.eft_contribution.ToolTip = "How the creditor contributes to the agency when we send payments electronically";
            //
            //label_address
            //
            this.label_address.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_address.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.label_address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.label_address.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.label_address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.label_address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.label_address.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.label_address.CausesValidation = false;
            this.label_address.Location = new System.Drawing.Point(20, 88);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(265, 13);
            this.label_address.StyleController = this.LayoutControl1;
            this.label_address.TabIndex = 19;
            this.label_address.ToolTip = "Creditor's payment address.";
            this.label_address.UseMnemonic = false;
            //
            //phone
            //
            this.phone.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.phone.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.phone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.phone.CausesValidation = false;
            this.phone.Location = new System.Drawing.Point(431, 141);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(133, 13);
            this.phone.StyleController = this.LayoutControl1;
            this.phone.TabIndex = 27;
            this.phone.Tag = "*";
            this.phone.ToolTip = "Primary contact telephone number";
            this.phone.UseMnemonic = false;
            //
            //contrib_ytd_received
            //
            this.contrib_ytd_received.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.contrib_ytd_received.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.contrib_ytd_received.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.contrib_ytd_received.CausesValidation = false;
            this.contrib_ytd_received.Location = new System.Drawing.Point(431, 63);
            this.contrib_ytd_received.Name = "contrib_ytd_received";
            this.contrib_ytd_received.Size = new System.Drawing.Size(133, 13);
            this.contrib_ytd_received.StyleController = this.LayoutControl1;
            this.contrib_ytd_received.TabIndex = 23;
            this.contrib_ytd_received.Tag = "*";
            this.contrib_ytd_received.Text = "$0.00";
            this.contrib_ytd_received.ToolTip = "How much money the creditor has given the agency this year";
            this.contrib_ytd_received.UseMnemonic = false;
            //
            //sic
            //
            this.sic.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.sic.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.sic.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.sic.CausesValidation = false;
            this.sic.Location = new System.Drawing.Point(431, 124);
            this.sic.Name = "sic";
            this.sic.Size = new System.Drawing.Size(133, 13);
            this.sic.StyleController = this.LayoutControl1;
            this.sic.TabIndex = 26;
            this.sic.Tag = "*";
            this.sic.ToolTip = "Standard Industry Code for this creditor";
            this.sic.UseMnemonic = false;
            //
            //contrib_cycle
            //
            this.contrib_cycle.CausesValidation = false;
            this.contrib_cycle.Location = new System.Drawing.Point(146, 12);
            this.contrib_cycle.Name = "contrib_cycle";
            this.contrib_cycle.Size = new System.Drawing.Size(43, 13);
            this.contrib_cycle.StyleController = this.LayoutControl1;
            this.contrib_cycle.TabIndex = 33;
            this.contrib_cycle.Tag = "*";
            this.contrib_cycle.ToolTip = "If the creditor is billed, what is the billing frequency";
            //
            //contrib_mtd_billed
            //
            this.contrib_mtd_billed.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.contrib_mtd_billed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.contrib_mtd_billed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.contrib_mtd_billed.CausesValidation = false;
            this.contrib_mtd_billed.Location = new System.Drawing.Point(431, 80);
            this.contrib_mtd_billed.Name = "contrib_mtd_billed";
            this.contrib_mtd_billed.Size = new System.Drawing.Size(133, 13);
            this.contrib_mtd_billed.StyleController = this.LayoutControl1;
            this.contrib_mtd_billed.TabIndex = 24;
            this.contrib_mtd_billed.Tag = "*";
            this.contrib_mtd_billed.Text = "$0.00";
            this.contrib_mtd_billed.ToolTip = "How much money that we have asked the creditor to pay us this month for billed contributions";
            this.contrib_mtd_billed.UseMnemonic = false;
            //
            //contrib_ytd_billed
            //
            this.contrib_ytd_billed.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.contrib_ytd_billed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.contrib_ytd_billed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.contrib_ytd_billed.CausesValidation = false;
            this.contrib_ytd_billed.Location = new System.Drawing.Point(431, 97);
            this.contrib_ytd_billed.Name = "contrib_ytd_billed";
            this.contrib_ytd_billed.Size = new System.Drawing.Size(133, 13);
            this.contrib_ytd_billed.StyleController = this.LayoutControl1;
            this.contrib_ytd_billed.TabIndex = 25;
            this.contrib_ytd_billed.Tag = "*";
            this.contrib_ytd_billed.Text = "$0.00";
            this.contrib_ytd_billed.ToolTip = "How much money that we have asked the creditor to pay us this year for billed contributions";
            this.contrib_ytd_billed.UseMnemonic = false;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlGroup2,
				this.LayoutControlGroup4,
				this.LayoutControlItem16,
				this.LayoutControlItem17,
				this.LayoutControlItem19,
				this.LayoutControlItem15,
				this.LayoutControlGroup3,
				this.item0,
				this.item2,
				this.LayoutControlItem18
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 265);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Dollar Amounts";
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem6,
				this.LayoutControlItem5,
				this.LayoutControlItem4,
				this.LayoutControlItem3,
				this.LayoutControlItem2,
				this.LayoutControlItem1,
				this.item3
			});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(285, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(271, 112);
            this.LayoutControlGroup2.Text = "Dollar Amounts";
            this.LayoutControlGroup2.TextVisible = false;
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.contrib_ytd_billed;
            this.LayoutControlItem6.CustomizationFormText = "YTD Contribution Billed";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 85);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem6.Text = "YTD Contribution Billed";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.contrib_mtd_billed;
            this.LayoutControlItem5.CustomizationFormText = "MTD Contribution Billed";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 68);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem5.Text = "MTD Contribution Billed";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.contrib_ytd_received;
            this.LayoutControlItem4.CustomizationFormText = "YTD Contribution Received";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem4.Text = "YTD Contribution Received";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.contrib_mtd_received;
            this.LayoutControlItem3.CustomizationFormText = "MTD Contribution Received";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem3.Text = "MTD Contribution Received";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.distrib_ytd;
            this.LayoutControlItem2.CustomizationFormText = "YTD Distribution";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem2.Text = "YTD Distribution";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.distrib_mtd;
            this.LayoutControlItem1.CustomizationFormText = "MTD Distribution";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem1.Text = "MTD Distribution";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(130, 13);
            //
            //item3
            //
            this.item3.AllowHotTrack = false;
            this.item3.CustomizationFormText = "item3";
            this.item3.Location = new System.Drawing.Point(0, 102);
            this.item3.Name = "item3";
            this.item3.Size = new System.Drawing.Size(271, 10);
            this.item3.Text = "item3";
            this.item3.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlGroup4
            //
            this.LayoutControlGroup4.CustomizationFormText = "Misc";
            this.LayoutControlGroup4.GroupBordersVisible = false;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem14,
				this.LayoutControlItem13,
				this.LayoutControlItem12,
				this.LayoutControlItem11
			});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(285, 112);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(271, 68);
            this.LayoutControlGroup4.Text = "Misc";
            this.LayoutControlGroup4.TextVisible = false;
            //
            //LayoutControlItem14
            //
            this.LayoutControlItem14.Control = this.last_invoice;
            this.LayoutControlItem14.CustomizationFormText = "Last Invoice Billed";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem14.Text = "Last Invoice Billed";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem13
            //
            this.LayoutControlItem13.Control = this.fax;
            this.LayoutControlItem13.CustomizationFormText = "FAX";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem13.Text = "FAX";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem12
            //
            this.LayoutControlItem12.Control = this.phone;
            this.LayoutControlItem12.CustomizationFormText = "Phone";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem12.Text = "Phone";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.sic;
            this.LayoutControlItem11.CustomizationFormText = "NFCC S.I.C. Code";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(271, 17);
            this.LayoutControlItem11.Text = "NFCC S.I.C. Code";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem16
            //
            this.LayoutControlItem16.Control = this.returned_mail;
            this.LayoutControlItem16.CustomizationFormText = "Returned Mail";
            this.LayoutControlItem16.Location = new System.Drawing.Point(0, 101);
            this.LayoutControlItem16.Name = "LayoutControlItem16";
            this.LayoutControlItem16.Size = new System.Drawing.Size(142, 79);
            this.LayoutControlItem16.Text = "Returned Mail";
            this.LayoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem16.TextSize = new System.Drawing.Size(66, 13);
            this.LayoutControlItem16.TextToControlDistance = 5;
            //
            //LayoutControlItem17
            //
            this.LayoutControlItem17.Control = this.date_created;
            this.LayoutControlItem17.CustomizationFormText = "Setup";
            this.LayoutControlItem17.Location = new System.Drawing.Point(142, 101);
            this.LayoutControlItem17.Name = "LayoutControlItem17";
            this.LayoutControlItem17.Size = new System.Drawing.Size(80, 79);
            this.LayoutControlItem17.Text = "Setup";
            this.LayoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem17.TextSize = new System.Drawing.Size(28, 13);
            this.LayoutControlItem17.TextToControlDistance = 5;
            //
            //LayoutControlItem19
            //
            this.LayoutControlItem19.Control = this.comment;
            this.LayoutControlItem19.CustomizationFormText = "Comment";
            this.LayoutControlItem19.Location = new System.Drawing.Point(0, 221);
            this.LayoutControlItem19.Name = "LayoutControlItem19";
            this.LayoutControlItem19.Size = new System.Drawing.Size(556, 24);
            this.LayoutControlItem19.Text = "Comment";
            this.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem19.TextSize = new System.Drawing.Size(45, 13);
            this.LayoutControlItem19.TextToControlDistance = 5;
            //
            //LayoutControlItem15
            //
            this.LayoutControlItem15.Control = this.label_address;
            this.LayoutControlItem15.CustomizationFormText = "Address";
            this.LayoutControlItem15.Location = new System.Drawing.Point(0, 68);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10);
            this.LayoutControlItem15.Size = new System.Drawing.Size(285, 33);
            this.LayoutControlItem15.Text = "Address";
            this.LayoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem15.TextToControlDistance = 0;
            this.LayoutControlItem15.TextVisible = false;
            //
            //LayoutControlGroup3
            //
            this.LayoutControlGroup3.CustomizationFormText = "Cycles And Contributions";
            this.LayoutControlGroup3.GroupBordersVisible = false;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem10,
				this.LayoutControlItem9,
				this.LayoutControlItem8,
				this.LayoutControlItem7,
				this.item1
			});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup3.Size = new System.Drawing.Size(285, 68);
            this.LayoutControlGroup3.Text = "Cycles And Contributions";
            this.LayoutControlGroup3.TextVisible = false;
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.first_distribution_date;
            this.LayoutControlItem10.CustomizationFormText = "First Distribution Date";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(181, 17);
            this.LayoutControlItem10.Text = "First Distribution Date";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.check_contribution;
            this.LayoutControlItem9.CustomizationFormText = "Check Contribution";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(181, 17);
            this.LayoutControlItem9.Text = "Check Contribution";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.eft_contribution;
            this.LayoutControlItem8.CustomizationFormText = "EFT Contribution";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(181, 17);
            this.LayoutControlItem8.Text = "EFT Contribution";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(130, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.contrib_cycle;
            this.LayoutControlItem7.CustomizationFormText = "Billing Cycle";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(181, 17);
            this.LayoutControlItem7.Text = "Billing Cycle";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(130, 13);
            //
            //item1
            //
            this.item1.AllowHotTrack = false;
            this.item1.CustomizationFormText = "item1";
            this.item1.Location = new System.Drawing.Point(181, 0);
            this.item1.MinSize = new System.Drawing.Size(104, 24);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(104, 68);
            this.item1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item1.Text = "item1";
            this.item1.TextSize = new System.Drawing.Size(0, 0);
            //
            //item0
            //
            this.item0.AllowHotTrack = false;
            this.item0.CustomizationFormText = "item0";
            this.item0.Location = new System.Drawing.Point(0, 197);
            this.item0.MinSize = new System.Drawing.Size(104, 24);
            this.item0.Name = "item0";
            this.item0.Size = new System.Drawing.Size(556, 24);
            this.item0.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item0.Text = "item0";
            this.item0.TextSize = new System.Drawing.Size(0, 0);
            //
            //item2
            //
            this.item2.AllowHotTrack = false;
            this.item2.CustomizationFormText = "item2";
            this.item2.Location = new System.Drawing.Point(222, 101);
            this.item2.Name = "item2";
            this.item2.Size = new System.Drawing.Size(63, 79);
            this.item2.Text = "item2";
            this.item2.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem18
            //
            this.LayoutControlItem18.Control = this.prefixes;
            this.LayoutControlItem18.CustomizationFormText = "Prefix(es):";
            this.LayoutControlItem18.Location = new System.Drawing.Point(0, 180);
            this.LayoutControlItem18.Name = "LayoutControlItem18";
            this.LayoutControlItem18.Size = new System.Drawing.Size(556, 17);
            this.LayoutControlItem18.Text = "Prefix(es):";
            this.LayoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem18.TextSize = new System.Drawing.Size(51, 13);
            this.LayoutControlItem18.TextToControlDistance = 5;
            //
            //Page_General
            //
            this.CausesValidation = false;
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_General";
            this.Size = new System.Drawing.Size(576, 265);
            ((System.ComponentModel.ISupportInitialize)this.comment.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem16).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem17).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem19).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item0).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem18).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            // Save the creditor reference for later
            this.creditorRecord = CreditorRecord;

            // The only editable field is the comment record
            comment.Text = creditorRecord.comment;

            // Format the remainder as display-only values
            distrib_mtd.Text = string.Format("{0:c}", creditorRecord.distrib_mtd);
            distrib_ytd.Text = string.Format("{0:c}", creditorRecord.distrib_ytd + creditorRecord.distrib_mtd);
            contrib_mtd_billed.Text = string.Format("{0:c}", creditorRecord.contrib_mtd_billed);
            contrib_ytd_billed.Text = string.Format("{0:c}", creditorRecord.contrib_ytd_billed + creditorRecord.contrib_mtd_billed);
            contrib_mtd_received.Text = string.Format("{0:c}", creditorRecord.contrib_mtd_received);
            contrib_ytd_received.Text = string.Format("{0:c}", creditorRecord.contrib_ytd_received + creditorRecord.contrib_mtd_received);

            date_created.Text = creditorRecord.date_created.ToShortDateString();

            if (creditorRecord.first_payment.HasValue)
            {
                ReadFirstPayment(bc, creditorRecord.first_payment.Value);
            }
            Read_Invoice(bc);
        }

        #region Prefixes

        private void Read_Prefixes(BusinessContext bc)
        {
            // Get the list of prefix values from the creditor reference
            var sb = new System.Text.StringBuilder();
            System.Collections.Generic.List<creditor_addkey> colRecords = bc.creditor_addkeys.Where(s => s.type == "P" && s.creditor == creditorRecord.Id).ToList();
            colRecords.ForEach(s => sb.AppendFormat(", {0}", s.additional));

            // clean up the string and set the text into the form
            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            if (sb.Length == 0)
            {
                sb.Append("NONE");
            }
            prefixes.Text = sb.ToString();
        }

        #endregion Prefixes

        #region Creditor

        private void Read_Creditor(BusinessContext bc)
        {
            returned_mail.Text = creditorRecord.returned_mail.HasValue ? creditorRecord.returned_mail.Value.ToShortDateString() : string.Empty;
            sic.Text = creditorRecord.sic ?? string.Empty;

            var contribRecord = DebtPlus.LINQ.InMemory.contribCycleTypes.getList().Find(s => s.Id == creditorRecord.contrib_cycle);
            if (contribRecord != null)
            {
                contrib_cycle.Text = contribRecord.description;
            }
            else
            {
                contrib_cycle.Text = "None";
            }

            if (creditorRecord.creditor_contribution_pct.HasValue)
            {
                ReadCreditorContribPct(bc, creditorRecord.creditor_contribution_pct.Value);
            }
        }

        private void ReadCreditorContribPct(BusinessContext bc, Int32 keyId)
        {
            var q = bc.creditor_contribution_pcts.Where(s => s.Id == keyId).FirstOrDefault();
            if (q != null)
            {
                eft_contribution.Text   = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(q.fairshare_pct_eft, q.creditor_type_eft);
                check_contribution.Text = DebtPlus.LINQ.creditor_contribution_pct.FormatRateAndContribution(q.fairshare_pct_check, q.creditor_type_check);
            }
        }

        private void ReadFirstPayment(BusinessContext bc, Int32 keyId)
        {
            var q = bc.registers_creditors.Where(s => s.Id == keyId).FirstOrDefault();
            if (q != null)
            {
                first_distribution_date.Text = q.date_created.ToShortDateString();
            }
        }

        #endregion Creditor

        #region Address

        private void Read_Address(BusinessContext bc)
        {
            var q = (from ca in bc.creditor_addresses
                     join a in bc.addresses on ca.AddressID equals a.Id
                     where ca.type == "P" && ca.creditor == creditorRecord.Id
                     select new DebtPlus.LINQ.CreditorAddressEntry(a, ca.attn)
                    ).FirstOrDefault();

            label_address.Text = (q == null) ? string.Empty : q.ToString();
        }

        #endregion Address

        #region Last Invoice

        private void Read_Invoice(BusinessContext bc)
        {
            var q = (from inv in bc.registers_invoices
                     where inv.creditor == creditorRecord.Id
                     orderby inv.date_created descending
                     select new { inv.inv_date, inv.inv_amount }
                    ).FirstOrDefault();

            last_invoice.Text = (q != null) ? string.Format("{0:d}   {1:c}", q.inv_date, q.inv_amount) : string.Empty;
        }

        #endregion Last Invoice

        #region Contact

        private void Read_Contacts(BusinessContext bc)
        {
            // Empty the fields in case the pointers are missing.
            phone.Text = string.Empty;
            fax.Text   = string.Empty;

            var qType = DebtPlus.LINQ.Cache.creditor_contact_type.getList().Find(s => (s.contact_type ?? string.Empty).IndexOf('P') >= 0);
            if (qType == null)
            {
                return;
            }

            var qContact = bc.creditor_contacts.Where(s => s.creditor == creditorRecord.Id && s.creditor_contact_type == qType.Id).FirstOrDefault();
            if (qContact == null)
            {
                return;
            }

            if (qContact.TelephoneID.HasValue)
            {
                var qPhone = bc.TelephoneNumbers.Where(s => s.Id == qContact.TelephoneID.Value).FirstOrDefault();
                if (qPhone != null)
                {
                    phone.Text = qPhone.ToString();
                }
            }

            if (qContact.FAXID.HasValue)
            {
                var qPhone = bc.TelephoneNumbers.Where(s => s.Id == qContact.FAXID.Value).FirstOrDefault();
                if (qPhone != null)
                {
                    fax.Text = qPhone.ToString();
                }
            }
        }

        #endregion Contact
    }
}
