#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Name : ControlBase
    {
        // Current creditor record
        private creditor creditorRecord = null;

        public Page_Name()
            : base()
        {
            InitializeComponent();

            payment_balance.Properties.Items.Clear();
            payment_balance.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Balance", "B"));
            payment_balance.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Payment", "P"));
            RegisterHandlers();
        }

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            UnRegisterHandlers();
            try
            {
                // Determine the entry for the payment/balance references
                payment_balance.SelectedIndex = (creditorRecord.payment_balance == 'B' ? 0 : 1);

                // Define the other fields
                creditor_name.EditValue = creditorRecord.creditor_name;
                division.EditValue = creditorRecord.division;
                min_accept_amt.EditValue = creditorRecord.min_accept_amt;
                min_accept_pct.EditValue = creditorRecord.min_accept_pct;

                // Read the address entries
                AddressInformation1.ReadForm(bc, creditorRecord);
                AddressInformation2.ReadForm(bc, creditorRecord);
                AddressInformation3.ReadForm(bc, creditorRecord);

                if (!DebtPlus.Configuration.Config.UpdateCreditorName)
                {
                    min_accept_amt.Properties.ReadOnly = true;
                    min_accept_pct.Properties.ReadOnly = true;
                    division.Properties.ReadOnly = true;
                    creditor_name.Properties.ReadOnly = true;
                    payment_balance.Properties.ReadOnly = true;
                    AddressInformation1.ReadOnly = true;
                    AddressInformation2.ReadOnly = true;
                    AddressInformation3.ReadOnly = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the form data
        /// </summary>
        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            // Determine the entry for the payment/balance references
            creditorRecord.payment_balance = payment_balance.SelectedIndex == 1 ? 'P' : 'B';

            // Define the other fields
            creditorRecord.creditor_name = DebtPlus.Utils.Nulls.v_String(creditor_name.EditValue);
            creditorRecord.division = DebtPlus.Utils.Nulls.v_String(division.EditValue);
            creditorRecord.min_accept_amt = DebtPlus.Utils.Nulls.v_Decimal(min_accept_amt.EditValue);
            creditorRecord.min_accept_pct = DebtPlus.Utils.Nulls.v_Double(min_accept_pct.EditValue);
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region " Windows Form Designer generated code "

        private AddressInformation AddressInformation1;

        private AddressInformation AddressInformation2;

        private AddressInformation AddressInformation3;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.TextEdit creditor_name;

        private DevExpress.XtraEditors.TextEdit division;

        private DevExpress.XtraEditors.GroupControl GroupControl1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;

        private DevExpress.XtraEditors.LabelControl LabelControl2;

        private DevExpress.XtraEditors.LabelControl LabelControl3;

        private DevExpress.XtraEditors.LabelControl LabelControl4;

        private DevExpress.XtraEditors.LabelControl LabelControl5;

        private DevExpress.XtraEditors.LabelControl LabelControl6;

        private DevExpress.XtraEditors.CalcEdit min_accept_amt;

        private DebtPlus.Data.Controls.PercentEdit min_accept_pct;

        private DevExpress.XtraEditors.ComboBoxEdit payment_balance;

        private DevExpress.XtraEditors.XtraScrollableControl XtraScrollableControl1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.creditor_name = new DevExpress.XtraEditors.TextEdit();
            this.division = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.payment_balance = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.min_accept_amt = new DevExpress.XtraEditors.CalcEdit();
            this.min_accept_pct = new DebtPlus.Data.Controls.PercentEdit();
            this.XtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.AddressInformation3 = new AddressInformation();
            this.AddressInformation2 = new AddressInformation();
            this.AddressInformation1 = new AddressInformation();
            ((System.ComponentModel.ISupportInitialize)this.creditor_name.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.division.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.payment_balance.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_amt.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_pct.Properties).BeginInit();
            this.XtraScrollableControl1.SuspendLayout();
            this.SuspendLayout();
            //
            //labelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 3);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(31, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name:";
            //
            //creditor_name
            //
            this.creditor_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.creditor_name.Location = new System.Drawing.Point(96, 0);
            this.creditor_name.Name = "creditor_name";
            this.creditor_name.Properties.MaxLength = 50;
            this.creditor_name.Size = new System.Drawing.Size(464, 20);
            this.creditor_name.TabIndex = 1;
            //
            //division
            //
            this.division.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.division.Location = new System.Drawing.Point(96, 20);
            this.division.Name = "division";
            this.division.Properties.MaxLength = 50;
            this.division.Size = new System.Drawing.Size(464, 20);
            this.division.TabIndex = 3;
            //
            //labelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 24);
            this.LabelControl2.Name = "labelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(40, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Division:";
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.payment_balance);
            this.GroupControl1.Controls.Add(this.LabelControl6);
            this.GroupControl1.Controls.Add(this.LabelControl5);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.min_accept_amt);
            this.GroupControl1.Controls.Add(this.min_accept_pct);
            this.GroupControl1.Location = new System.Drawing.Point(392, 48);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(168, 160);
            this.GroupControl1.TabIndex = 5;
            this.GroupControl1.Text = "Minimum Payment";
            //
            //payment_balance
            //
            this.payment_balance.Location = new System.Drawing.Point(56, 128);
            this.payment_balance.Name = "payment_balance";
            this.payment_balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.payment_balance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.payment_balance.Size = new System.Drawing.Size(104, 20);
            this.payment_balance.TabIndex = 6;
            this.payment_balance.ToolTip = "Choose either Payment or the Balance. Payment is normally used for Finance compan" + "ies while Balance is used for all other types.";
            //
            //LabelControl6
            //
            this.LabelControl6.Location = new System.Drawing.Point(16, 131);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(29, 13);
            this.LabelControl6.TabIndex = 5;
            this.LabelControl6.Text = "of the";
            //
            //LabelControl5
            //
            this.LabelControl5.Location = new System.Drawing.Point(123, 99);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(37, 13);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "percent";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(24, 35);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(6, 13);
            this.LabelControl4.TabIndex = 0;
            this.LabelControl4.Text = "$";
            //
            //LabelControl3
            //
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.LabelControl3.Appearance.Options.UseFont = true;
            this.LabelControl3.Appearance.Options.UseTextOptions = true;
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl3.Location = new System.Drawing.Point(8, 72);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(112, 16);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Or the greater of";
            //
            //min_accept_amt
            //
            this.min_accept_amt.Location = new System.Drawing.Point(56, 32);
            this.min_accept_amt.Name = "min_accept_amt";
            this.min_accept_amt.Properties.Appearance.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.min_accept_amt.Properties.DisplayFormat.FormatString = "c2";
            this.min_accept_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_amt.Properties.EditFormat.FormatString = "f2";
            this.min_accept_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_amt.Properties.Mask.BeepOnError = true;
            this.min_accept_amt.Properties.Precision = 2;
            this.min_accept_amt.Size = new System.Drawing.Size(104, 20);
            this.min_accept_amt.TabIndex = 1;
            this.min_accept_amt.ToolTip = "Enter the minimum dollar amount that will be configured for debts of this credito" + "r. This is today monthly amount figure.";
            //
            //min_accept_pct
            //
            this.min_accept_pct.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.min_accept_pct.Location = new System.Drawing.Point(16, 96);
            this.min_accept_pct.Name = "min_accept_pct";
            this.min_accept_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.min_accept_pct.Size = new System.Drawing.Size(91, 20);
            this.min_accept_pct.TabIndex = 3;
            this.min_accept_pct.TabStop = true;
            this.min_accept_pct.ToolTip = "Choose the minimum percentage allowed for today monthly payment.";
            //
            //XtraScrollableControl1
            //
            this.XtraScrollableControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.XtraScrollableControl1.Controls.Add(this.AddressInformation3);
            this.XtraScrollableControl1.Controls.Add(this.AddressInformation2);
            this.XtraScrollableControl1.Controls.Add(this.AddressInformation1);
            this.XtraScrollableControl1.Location = new System.Drawing.Point(0, 40);
            this.XtraScrollableControl1.Name = "XtraScrollableControl1";
            this.XtraScrollableControl1.Size = new System.Drawing.Size(392, 256);
            this.XtraScrollableControl1.TabIndex = 4;
            //
            //AddressInformation3
            //
            this.AddressInformation3.AddressType = "I";
            this.AddressInformation3.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.AddressInformation3.AutoScroll = true;
            this.AddressInformation3.Caption = "Invoices...";
            this.AddressInformation3.Location = new System.Drawing.Point(8, 176);
            this.AddressInformation3.Name = "AddressInformation3";
            this.AddressInformation3.ReadOnly = false;
            this.AddressInformation3.Size = new System.Drawing.Size(360, 80);
            this.AddressInformation3.TabIndex = 2;
            //
            //AddressInformation2
            //
            this.AddressInformation2.AddressType = "L";
            this.AddressInformation2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.AddressInformation2.AutoScroll = true;
            this.AddressInformation2.Caption = "Proposals...";
            this.AddressInformation2.Location = new System.Drawing.Point(8, 88);
            this.AddressInformation2.Name = "AddressInformation2";
            this.AddressInformation2.ReadOnly = false;
            this.AddressInformation2.Size = new System.Drawing.Size(360, 80);
            this.AddressInformation2.TabIndex = 1;
            //
            //AddressInformation1
            //
            this.AddressInformation1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.AddressInformation1.AutoScroll = true;
            this.AddressInformation1.Caption = "Payments...";
            this.AddressInformation1.Location = new System.Drawing.Point(8, 0);
            this.AddressInformation1.Name = "AddressInformation1";
            this.AddressInformation1.ReadOnly = false;
            this.AddressInformation1.Size = new System.Drawing.Size(360, 80);
            this.AddressInformation1.TabIndex = 0;
            //
            //Page_Name
            //
            this.Controls.Add(this.XtraScrollableControl1);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.division);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.creditor_name);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Name";
            ((System.ComponentModel.ISupportInitialize)this.creditor_name.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.division.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.payment_balance.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_amt.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_pct.Properties).EndInit();
            this.XtraScrollableControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "
    }
}
