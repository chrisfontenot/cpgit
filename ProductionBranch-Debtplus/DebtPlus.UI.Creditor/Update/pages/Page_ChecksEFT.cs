#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_ChecksEFT : ControlBase
    {
        // Current record being edited
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Page_ChecksEFT()
            : base()
        {
            InitializeComponent();

            // Load the list of banks into the control
            check_bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "C").ToList();

            RegisterHandlers();
        }

        #region " Windows Form Designer generated code "

        private BillerMasksControl BillerMasksControl1;

        private DevExpress.XtraEditors.LookUpEdit check_bank;

        private DevExpress.XtraEditors.SpinEdit check_payments;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.LabelControl LabelControl1;

        private DevExpress.XtraEditors.LabelControl LabelControl2;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private MyEFTLocation MyEFTLocation1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.MyEFTLocation1 = new DebtPlus.UI.Creditor.Update.Controls.MyEFTLocation();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.check_bank = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.check_payments = new DevExpress.XtraEditors.SpinEdit();
            this.BillerMasksControl1 = new DebtPlus.UI.Creditor.Update.Controls.BillerMasksControl();
            ((System.ComponentModel.ISupportInitialize)(this.check_bank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_payments.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // MyEFTLocation1
            // 
            this.MyEFTLocation1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.MyEFTLocation1.Location = new System.Drawing.Point(0, 64);
            this.MyEFTLocation1.Name = "MyEFTLocation1";
            this.MyEFTLocation1.Size = new System.Drawing.Size(280, 232);
            this.MyEFTLocation1.TabIndex = 1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(16, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(34, 13);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.Text = "Checks";
            // 
            // check_bank
            // 
            this.check_bank.Location = new System.Drawing.Point(64, 8);
            this.check_bank.Name = "check_bank";
            this.check_bank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.check_bank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.check_bank.Properties.DisplayMember = "description";
            this.check_bank.Properties.NullText = "";
            this.check_bank.Properties.ShowFooter = false;
            this.check_bank.Properties.ShowHeader = false;
            this.check_bank.Properties.SortColumnIndex = 1;
            this.check_bank.Properties.ValueMember = "Id";
            this.check_bank.Size = new System.Drawing.Size(216, 20);
            this.check_bank.TabIndex = 3;
            // 
            // LabelControl2
            // 
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(16, 40);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(176, 13);
            this.LabelControl2.TabIndex = 4;
            this.LabelControl2.Text = "Number of Initial Payments By Check";
            // 
            // check_payments
            // 
            this.check_payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.check_payments.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.check_payments.Location = new System.Drawing.Point(216, 36);
            this.check_payments.Name = "check_payments";
            this.check_payments.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.check_payments.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.check_payments.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.check_payments.Properties.IsFloatValue = false;
            this.check_payments.Properties.Mask.BeepOnError = true;
            this.check_payments.Properties.Mask.EditMask = "n0";
            this.check_payments.Properties.MaxLength = 5;
            this.check_payments.Properties.MaxValue = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.check_payments.Size = new System.Drawing.Size(64, 20);
            this.check_payments.TabIndex = 8;
            // 
            // BillerMasksControl1
            // 
            this.BillerMasksControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BillerMasksControl1.Location = new System.Drawing.Point(286, 0);
            this.BillerMasksControl1.Name = "BillerMasksControl1";
            this.BillerMasksControl1.Size = new System.Drawing.Size(287, 296);
            this.BillerMasksControl1.TabIndex = 9;
            // 
            // Page_ChecksEFT
            // 
            this.Controls.Add(this.BillerMasksControl1);
            this.Controls.Add(this.check_payments);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.check_bank);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.MyEFTLocation1);
            this.Name = "Page_ChecksEFT";
            ((System.ComponentModel.ISupportInitialize)(this.check_bank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_payments.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Process the initial read for the form data
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;
            UnRegisterHandlers();
            try
            {
                // Read the payment information
                MyEFTLocation1.ReadForm(bc, creditorRecord);
                BillerMasksControl1.ReadForm(bc, creditorRecord);

                check_bank.EditValue     = creditorRecord.check_bank;
                check_payments.EditValue = creditorRecord.check_payments;

                // Disable the controls on this page if the user does not have permission to change them.
                if (!DebtPlus.Configuration.Config.UpdateCreditorEFT)
                {
                    check_bank.Properties.ReadOnly     = true;
                    check_payments.Properties.ReadOnly = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the form information into the creditor record
        /// </summary>
        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            creditorRecord.check_bank     = DebtPlus.Utils.Nulls.v_Int32(check_bank.EditValue).GetValueOrDefault();
            creditorRecord.check_payments = DebtPlus.Utils.Nulls.v_Int32(check_payments.EditValue).GetValueOrDefault();
        }

        /// <summary>
        /// Handle today change in the list of payment methods. This requires that we rebuild the Biller ID list
        /// from the masks table.
        /// </summary>
        private void MyEFTLocation1_ListChanged(object sender, EventArgs e)
        {
            BillerMasksControl1.ReadForm(bc, creditorRecord);
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            check_bank.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            MyEFTLocation1.ListChanged   += MyEFTLocation1_ListChanged;
            check_bank.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            check_bank.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            MyEFTLocation1.ListChanged   -= MyEFTLocation1_ListChanged;
            check_bank.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }
    }
}