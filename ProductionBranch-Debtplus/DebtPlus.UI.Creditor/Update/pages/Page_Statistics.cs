#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Statistics : ControlBase
    {
        // Current creditor record being updated
        private creditor creditorRecord = null;

        public Page_Statistics()
            : base()
        {
            InitializeComponent();
            SimpleButton1.Click += SimpleButton1_Click;
        }

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            RefreshForm(false);
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem5;

        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem6;

        private DevExpress.XtraEditors.LabelControl LabelControl_average_scheduled_payments;

        private DevExpress.XtraEditors.LabelControl LabelControl_balance_client_count;

        private DevExpress.XtraEditors.LabelControl LabelControl_client_count;

        private DevExpress.XtraEditors.LabelControl LabelControl_total_debt;

        private DevExpress.XtraEditors.LabelControl LabelControl_total_monthly_payments;

        private DevExpress.XtraEditors.LabelControl LabelControl_zero_blance_client_count;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl7;

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;

        private DevExpress.XtraEditors.SimpleButton SimpleButton1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl_client_count = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_balance_client_count = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_total_monthly_payments = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_zero_blance_client_count = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_average_scheduled_payments = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_total_debt = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl7
            //
            this.LabelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12f, System.Drawing.FontStyle.Bold);
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(86, 22);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(404, 19);
            this.LabelControl7.StyleController = this.LayoutControl1;
            this.LabelControl7.TabIndex = 6;
            this.LabelControl7.Text = "Creditor Statistical Information For Active Clients";
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.LabelControl7);
            this.LayoutControl1.Controls.Add(this.LabelControl_client_count);
            this.LayoutControl1.Controls.Add(this.LabelControl_balance_client_count);
            this.LayoutControl1.Controls.Add(this.LabelControl_total_monthly_payments);
            this.LayoutControl1.Controls.Add(this.LabelControl_zero_blance_client_count);
            this.LayoutControl1.Controls.Add(this.LabelControl_average_scheduled_payments);
            this.LayoutControl1.Controls.Add(this.LabelControl_total_debt);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(448, 25, 450, 350);
            this.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 232);
            this.LayoutControl1.TabIndex = 14;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //SimpleButton1
            //
            this.SimpleButton1.Location = new System.Drawing.Point(250, 183);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 13;
            this.SimpleButton1.Text = "Recalculate";
            //
            //LabelControl_client_count
            //
            this.LabelControl_client_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_client_count.Location = new System.Drawing.Point(356, 59);
            this.LabelControl_client_count.Name = "LabelControl_client_count";
            this.LabelControl_client_count.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_client_count.StyleController = this.LayoutControl1;
            this.LabelControl_client_count.TabIndex = 7;
            this.LabelControl_client_count.Text = "0";
            //
            //LabelControl_balance_client_count
            //
            this.LabelControl_balance_client_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_balance_client_count.Location = new System.Drawing.Point(356, 76);
            this.LabelControl_balance_client_count.Name = "LabelControl_balance_client_count";
            this.LabelControl_balance_client_count.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_balance_client_count.StyleController = this.LayoutControl1;
            this.LabelControl_balance_client_count.TabIndex = 8;
            this.LabelControl_balance_client_count.Text = "0";
            //
            //LabelControl_total_monthly_payments
            //
            this.LabelControl_total_monthly_payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_total_monthly_payments.Location = new System.Drawing.Point(356, 93);
            this.LabelControl_total_monthly_payments.Name = "LabelControl_total_monthly_payments";
            this.LabelControl_total_monthly_payments.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_total_monthly_payments.StyleController = this.LayoutControl1;
            this.LabelControl_total_monthly_payments.TabIndex = 9;
            this.LabelControl_total_monthly_payments.Text = "$0.00";
            //
            //LabelControl_zero_blance_client_count
            //
            this.LabelControl_zero_blance_client_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_zero_blance_client_count.Location = new System.Drawing.Point(356, 110);
            this.LabelControl_zero_blance_client_count.Name = "LabelControl_zero_blance_client_count";
            this.LabelControl_zero_blance_client_count.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_zero_blance_client_count.StyleController = this.LayoutControl1;
            this.LabelControl_zero_blance_client_count.TabIndex = 10;
            this.LabelControl_zero_blance_client_count.Text = "0";
            //
            //LabelControl_average_scheduled_payments
            //
            this.LabelControl_average_scheduled_payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_average_scheduled_payments.Location = new System.Drawing.Point(356, 127);
            this.LabelControl_average_scheduled_payments.Name = "LabelControl_average_scheduled_payments";
            this.LabelControl_average_scheduled_payments.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_average_scheduled_payments.StyleController = this.LayoutControl1;
            this.LabelControl_average_scheduled_payments.TabIndex = 11;
            this.LabelControl_average_scheduled_payments.Text = "$0.00";
            //
            //LabelControl_total_debt
            //
            this.LabelControl_total_debt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_total_debt.Location = new System.Drawing.Point(356, 144);
            this.LabelControl_total_debt.Name = "LabelControl_total_debt";
            this.LabelControl_total_debt.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_total_debt.StyleController = this.LayoutControl1;
            this.LabelControl_total_debt.TabIndex = 12;
            this.LabelControl_total_debt.Text = "$0.00";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem7,
				this.LayoutControlItem8,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem4,
				this.EmptySpaceItem6,
				this.EmptySpaceItem5,
				this.EmptySpaceItem3
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "Root";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 232);
            this.LayoutControlGroup1.Text = "Root";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.SimpleButton1;
            this.LayoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 171);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(556, 27);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.LabelControl_total_debt;
            this.LayoutControlItem2.CustomizationFormText = "TotalAmountOwed";
            this.LayoutControlItem2.Location = new System.Drawing.Point(166, 132);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem2.Text = "Total Amount Owed to this Creditor:";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.LabelControl_average_scheduled_payments;
            this.LayoutControlItem3.CustomizationFormText = "Average Disbursement Factor";
            this.LayoutControlItem3.Location = new System.Drawing.Point(166, 115);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem3.Text = "Average Scheduled Payments:";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.LabelControl_zero_blance_client_count;
            this.LayoutControlItem4.CustomizationFormText = "Zero Balance Debts";
            this.LayoutControlItem4.Location = new System.Drawing.Point(166, 98);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem4.Text = "Count of Zero Balance Debts:";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.LabelControl_total_monthly_payments;
            this.LayoutControlItem5.CustomizationFormText = "Total Scheduled Payments";
            this.LayoutControlItem5.Location = new System.Drawing.Point(166, 81);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem5.Text = "Total Monthly Scheduled Payments:";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.LabelControl_balance_client_count;
            this.LayoutControlItem6.CustomizationFormText = "Clients With A Balance";
            this.LayoutControlItem6.Location = new System.Drawing.Point(166, 64);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem6.Text = "Count of debts with today Balance:";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.LabelControl_client_count;
            this.LayoutControlItem7.CustomizationFormText = "Client Count";
            this.LayoutControlItem7.Location = new System.Drawing.Point(166, 47);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(210, 17);
            this.LayoutControlItem7.Text = "Count of Clients:";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(174, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LayoutControlItem8.Control = this.LabelControl7;
            this.LayoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 10);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(556, 23);
            this.LayoutControlItem8.Text = "LayoutControlItem8";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 149);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(556, 22);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem2
            //
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 198);
            this.EmptySpaceItem2.MaxSize = new System.Drawing.Size(0, 14);
            this.EmptySpaceItem2.MinSize = new System.Drawing.Size(10, 14);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(556, 14);
            this.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem4
            //
            this.EmptySpaceItem4.AllowHotTrack = false;
            this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
            this.EmptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.EmptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.EmptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.EmptySpaceItem4.Name = "EmptySpaceItem4";
            this.EmptySpaceItem4.Size = new System.Drawing.Size(556, 10);
            this.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem4.Text = "EmptySpaceItem4";
            this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem6
            //
            this.EmptySpaceItem6.AllowHotTrack = false;
            this.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6";
            this.EmptySpaceItem6.Location = new System.Drawing.Point(376, 47);
            this.EmptySpaceItem6.Name = "EmptySpaceItem6";
            this.EmptySpaceItem6.Size = new System.Drawing.Size(180, 102);
            this.EmptySpaceItem6.Text = "EmptySpaceItem6";
            this.EmptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem5
            //
            this.EmptySpaceItem5.AllowHotTrack = false;
            this.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5";
            this.EmptySpaceItem5.Location = new System.Drawing.Point(0, 47);
            this.EmptySpaceItem5.Name = "EmptySpaceItem5";
            this.EmptySpaceItem5.Size = new System.Drawing.Size(166, 102);
            this.EmptySpaceItem5.Text = "EmptySpaceItem5";
            this.EmptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem3
            //
            this.EmptySpaceItem3.AllowHotTrack = false;
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 33);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(556, 14);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            //Page_Statistics
            //
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Statistics";
            this.Size = new System.Drawing.Size(576, 232);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code

        private void RefreshForm(bool force_calculation)
        {
            // Start with the statistics cleared
            LabelControl_balance_client_count.Text       = "0";
            LabelControl_client_count.Text               = "0";
            LabelControl_zero_blance_client_count.Text   = "0";
            LabelControl_average_scheduled_payments.Text = "$0.00";
            LabelControl_total_monthly_payments.Text     = "$0.00";
            LabelControl_total_debt.Text                 = "$0.00";

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var bc = new BusinessContext())
                    {
                        bc.CommandTimeout = 600 * 1000;         // 10 minutes

                        var Result = bc.xpr_Creditor_Statistics(creditorRecord.Id, force_calculation);
                        if (Result == null)
                        {
                            return;
                        }

                        // Display the result page data
                        LabelControl_balance_client_count.Text       = Result.clients_balance.ToString();
                        LabelControl_client_count.Text               = Result.clients.ToString();
                        LabelControl_zero_blance_client_count.Text   = Result.zero_bal_clients.ToString();
                        LabelControl_average_scheduled_payments.Text = string.Format("{0:c}", Result.avg_disbursement);
                        LabelControl_total_monthly_payments.Text     = string.Format("{0:c}", Result.total_disbursement);
                        LabelControl_total_debt.Text                 = string.Format("{0:c}", Result.total_balance);
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading statistic information");
            }
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {
            RefreshForm(true);
        }
    }
}