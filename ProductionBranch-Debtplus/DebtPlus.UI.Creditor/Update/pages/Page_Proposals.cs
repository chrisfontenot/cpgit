#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Proposals : ControlBase
    {
        // Current creditor record being updated
        private creditor creditorRecord = null;

        /// <summary>
        /// Create today new instance of our class
        /// </summary>
        public Page_Proposals()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Display the form information for the current page
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;

            // Read the list of payment information items
            MyEFTLocation1.ReadForm(bc, creditorRecord);

            // Define the fields from the current creditor record
            proposal_income_info.EditValue = creditorRecord.proposal_income_info;
            proposal_budget_info.EditValue = creditorRecord.proposal_budget_info;
            full_disclosure.EditValue = creditorRecord.full_disclosure;
        }

        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            creditorRecord.proposal_income_info = proposal_income_info.Checked;
            creditorRecord.proposal_budget_info = proposal_budget_info.Checked;
            creditorRecord.full_disclosure = full_disclosure.Checked;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
        }

        #region Windows Form Designer generated code

        private System.ComponentModel.IContainer components = null;
        private DevExpress.XtraEditors.CheckEdit full_disclosure;
        private DevExpress.XtraEditors.GroupControl GroupControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private MyEFTLocation MyEFTLocation1;
        private DevExpress.XtraEditors.CheckEdit proposal_budget_info;
        private DevExpress.XtraEditors.CheckEdit proposal_income_info;

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.MyEFTLocation1 = new MyEFTLocation();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.full_disclosure = new DevExpress.XtraEditors.CheckEdit();
            this.proposal_budget_info = new DevExpress.XtraEditors.CheckEdit();
            this.proposal_income_info = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.full_disclosure.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_budget_info.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_income_info.Properties).BeginInit();
            this.SuspendLayout();
            //
            //MyEFTLocation1
            //
            this.MyEFTLocation1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.MyEFTLocation1.Location = new System.Drawing.Point(288, 0);
            this.MyEFTLocation1.Name = "MyEFTLocation1";
            this.MyEFTLocation1.Size = new System.Drawing.Size(288, 296);
            this.MyEFTLocation1.TabIndex = 0;
            this.MyEFTLocation1.Type = "EDI";
            //
            //GroupControl1
            //
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Controls.Add(this.proposal_income_info);
            this.GroupControl1.Controls.Add(this.proposal_budget_info);
            this.GroupControl1.Controls.Add(this.full_disclosure);
            this.GroupControl1.Location = new System.Drawing.Point(8, 8);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(256, 120);
            this.GroupControl1.TabIndex = 1;
            this.GroupControl1.Text = "Proposal Options";
            //
            //full_disclosure
            //
            this.full_disclosure.Location = new System.Drawing.Point(8, 32);
            this.full_disclosure.Name = "full_disclosure";
            //
            //full_disclosure.Properties
            //
            this.full_disclosure.Properties.Caption = "Send proposals with full disclosure";
            this.full_disclosure.Size = new System.Drawing.Size(248, 19);
            this.full_disclosure.TabIndex = 0;
            //
            //proposal_budget_info
            //
            this.proposal_budget_info.Location = new System.Drawing.Point(24, 78);
            this.proposal_budget_info.Name = "proposal_budget_info";
            //
            //proposal_budget_info.Properties
            //
            this.proposal_budget_info.Properties.Caption = "Current Budget Breakdown";
            this.proposal_budget_info.Size = new System.Drawing.Size(232, 19);
            this.proposal_budget_info.TabIndex = 1;
            //
            //proposal_income_info
            //
            this.proposal_income_info.Location = new System.Drawing.Point(24, 97);
            this.proposal_income_info.Name = "proposal_income_info";
            //
            //proposal_income_info.Properties
            //
            this.proposal_income_info.Properties.Caption = "Secured Asset Values and Liabilities";
            this.proposal_income_info.Size = new System.Drawing.Size(232, 19);
            this.proposal_income_info.TabIndex = 2;
            //
            //labelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(16, 64);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(240, 14);
            this.LabelControl1.TabIndex = 3;
            this.LabelControl1.Text = "If Proposals are printed then include";
            //
            //Page_Proposals
            //
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.MyEFTLocation1);
            this.Name = "Page_Proposals";
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.full_disclosure.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_budget_info.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.proposal_income_info.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code
    }
}
