#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_PolicyMatrix : ControlBase
    {
        // Current creditor being edited
        private creditor creditorRecord = null;

        public Page_PolicyMatrix()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            PolicyMatrixGrid1.ReadForm(bc, creditorRecord);
        }

        private void PolicyMatrixGrid1_ShowMessage(object Sender, string Message)
        {
            description.Text = Message;
        }

        private void RegisterHandlers()
        {
            PolicyMatrixGrid1.ShowMessage += PolicyMatrixGrid1_ShowMessage;
        }

        private void UnRegisterHandlers()
        {
            PolicyMatrixGrid1.ShowMessage -= PolicyMatrixGrid1_ShowMessage;
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.MemoEdit description;
        private PolicyMatrixGrid PolicyMatrixGrid1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.description = new DevExpress.XtraEditors.MemoEdit();
            this.PolicyMatrixGrid1 = new PolicyMatrixGrid();
            ((System.ComponentModel.ISupportInitialize)this.description.Properties).BeginInit();
            this.SuspendLayout();
            //
            //description
            //
            this.description.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.description.Location = new System.Drawing.Point(0, 209);
            this.description.Name = "description";
            this.description.Properties.ReadOnly = true;
            this.description.Size = new System.Drawing.Size(576, 87);
            this.description.TabIndex = 1;
            //
            //PolicyMatrixGrid1
            //
            this.PolicyMatrixGrid1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.PolicyMatrixGrid1.Location = new System.Drawing.Point(0, 0);
            this.PolicyMatrixGrid1.Name = "PolicyMatrixGrid1";
            this.PolicyMatrixGrid1.Size = new System.Drawing.Size(576, 194);
            this.PolicyMatrixGrid1.TabIndex = 2;
            //
            //Page_PolicyMatrix
            //
            this.Controls.Add(this.PolicyMatrixGrid1);
            this.Controls.Add(this.description);
            this.Name = "Page_PolicyMatrix";
            ((System.ComponentModel.ISupportInitialize)this.description.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code
    }
}
