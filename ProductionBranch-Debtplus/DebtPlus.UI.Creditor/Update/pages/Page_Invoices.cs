#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Invoices : MyGridControl
    {
        // Collection of records being displayed
        private System.Collections.Generic.List<invoiceData> colRecords = null;

        // Current creditor being updated
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Page_Invoices()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Read the list of invoice records and update the display information with them
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    var query = bc.view_invoices.Where(s => s.creditor == creditorRecord.Id).Select(s => new invoiceData()
                        {
                            adj_amount = s.adj_amount.GetValueOrDefault(),
                            pmt_amount = s.pmt_amount.GetValueOrDefault(),
                            inv_amount = s.inv_amount,
                            adj_date   = s.adj_date,
                            checknum   = s.checknum,
                            inv_date   = s.inv_date,
                            invoice    = s.Id,
                            months     = s.months.GetValueOrDefault(),
                            pmt_date   = s.pmt_date
                        });

                    // If we include completed invoices then do not reject the completed items
                    if (! include_all_invoices.Checked)
                    {
                        query = query.Where(s => s.inv_amount > (s.pmt_amount + s.adj_amount));
                    }

                    // Perform the database request and update the record source
                    colRecords              = query.ToList();
                    GridControl1.DataSource = colRecords;
                    GridControl1.RefreshDataSource();

                    // Calculate the statistics for the invoice information
                    CalculateTotals(colRecords.OrderBy(s => s.checknum).ToList());
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice information");
            }
        }

        private void CalculateTotals(System.Collections.Generic.List<invoiceData> recordCollection)
        {
            decimal totals_total_billed      = 0m;
            decimal totals_total_payments    = 0m;
            decimal totals_total_adjustments = 0m;
            decimal totals_current           = 0m;
            decimal totals_net_30            = 0m;
            decimal totals_net_60            = 0m;
            decimal totals_net_90            = 0m;
            decimal totals_net_120           = 0m;

            // Process the rows looking for today change in the invoice number
            Int32 last_invoice_number = -1;
            foreach (invoiceData inv in recordCollection)
            {
                Int32 current_invoice_number = inv.invoice;
                if (last_invoice_number != current_invoice_number)
                {
                    last_invoice_number = current_invoice_number;

                    decimal current_inv_amount = inv.inv_amount;
                    decimal current_pmt_amount = inv.pmt_amount;
                    decimal current_adj_amount = inv.adj_amount;
                    System.DateTime inv_date   = inv.inv_date;

                    // Adjust the totals
                    totals_total_billed       += current_inv_amount;
                    totals_total_payments     += current_pmt_amount;
                    totals_total_adjustments  += current_adj_amount;

                    // Find the amount outstanding
                    decimal net_amount = current_inv_amount - current_pmt_amount - current_adj_amount;

                    // Add that figure to the outstanding column based upon the date
                    long Months = inv.months;
                    if (Months <= 0)
                    {
                        totals_current += net_amount;
                    }
                    else if (Months == 1)
                    {
                        totals_net_30 += net_amount;
                    }
                    else if (Months == 2)
                    {
                        totals_net_60 += net_amount;
                    }
                    else if (Months == 3)
                    {
                        totals_net_90 += net_amount;
                    }
                    else
                    {
                        totals_net_120 += net_amount;
                    }
                }
            }

            // Insert the totals into the fields
            total_billed.Text      = string.Format("{0:c}", totals_total_billed);
            total_adjustments.Text = string.Format("{0:c}", totals_total_adjustments);
            total_payments.Text    = string.Format("{0:c}", totals_total_payments);
            total_outstanding.Text = string.Format("{0:c}", totals_total_billed - totals_total_adjustments - totals_total_payments);

            net_current.Text = string.Format("{0:c}", totals_current);
            net_30.Text      = string.Format("{0:c}", totals_net_30);
            net_60.Text      = string.Format("{0:c}", totals_net_60);
            net_90.Text      = string.Format("{0:c}", totals_net_90);
            net_120.Text     = string.Format("{0:c}", totals_net_120);
        }

        private void include_all_invoices_CheckStateChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void RegisterHandlers()
        {
            include_all_invoices.CheckStateChanged += include_all_invoices_CheckStateChanged;
        }

        private void UnRegisterHandlers()
        {
            include_all_invoices.CheckStateChanged -= include_all_invoices_CheckStateChanged;
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_adj_amount;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_adj_date;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_checknum;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_inv_amount;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_inv_date;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_invoice;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_net_amount;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_pmt_amount;

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_pmt_date;

        private DevExpress.XtraEditors.CheckEdit include_all_invoices;

        private DevExpress.XtraEditors.LabelControl LabelControl1;

        private DevExpress.XtraEditors.LabelControl LabelControl10;

        private DevExpress.XtraEditors.LabelControl LabelControl11;

        private DevExpress.XtraEditors.LabelControl LabelControl12;

        private DevExpress.XtraEditors.LabelControl LabelControl14;

        private DevExpress.XtraEditors.LabelControl LabelControl2;

        private DevExpress.XtraEditors.LabelControl LabelControl3;

        private DevExpress.XtraEditors.LabelControl LabelControl4;

        private DevExpress.XtraEditors.LabelControl LabelControl9;

        private DevExpress.XtraEditors.LabelControl net_120;

        private DevExpress.XtraEditors.LabelControl net_30;

        private DevExpress.XtraEditors.LabelControl net_60;

        private DevExpress.XtraEditors.LabelControl net_90;

        private DevExpress.XtraEditors.LabelControl net_current;

        private DevExpress.XtraEditors.LabelControl total_adjustments;

        private DevExpress.XtraEditors.LabelControl total_billed;

        private DevExpress.XtraEditors.LabelControl total_outstanding;

        private DevExpress.XtraEditors.LabelControl total_payments;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.include_all_invoices = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.total_outstanding = new DevExpress.XtraEditors.LabelControl();
            this.total_adjustments = new DevExpress.XtraEditors.LabelControl();
            this.total_payments = new DevExpress.XtraEditors.LabelControl();
            this.total_billed = new DevExpress.XtraEditors.LabelControl();
            this.net_90 = new DevExpress.XtraEditors.LabelControl();
            this.net_60 = new DevExpress.XtraEditors.LabelControl();
            this.net_30 = new DevExpress.XtraEditors.LabelControl();
            this.net_current = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.net_120 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.gridColumn_invoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_invoice.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_inv_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_inv_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_checknum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_checknum.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_inv_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_inv_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_pmt_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_pmt_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_pmt_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_pmt_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_adj_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_adj_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_adj_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_adj_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_net_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_net_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.include_all_invoices.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_invoice,
				this.gridColumn_inv_date,
				this.gridColumn_checknum,
				this.gridColumn_inv_amount,
				this.gridColumn_pmt_date,
				this.gridColumn_pmt_amount,
				this.gridColumn_adj_date,
				this.gridColumn_adj_amount,
				this.gridColumn_net_amount
			});
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 80);
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(576, 184);
            this.GridControl1.TabIndex = 18;
            //
            //include_all_invoices
            //
            this.include_all_invoices.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.include_all_invoices.Location = new System.Drawing.Point(0, 272);
            this.include_all_invoices.Name = "include_all_invoices";
            //
            //include_all_invoices.Properties
            //
            this.include_all_invoices.Properties.Caption = "Show all invoices (including zero amount invoices)";
            this.include_all_invoices.Size = new System.Drawing.Size(264, 19);
            this.include_all_invoices.TabIndex = 19;
            //
            //labelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(24, 8);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(55, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Total Billed:";
            this.LabelControl1.UseMnemonic = false;
            //
            //labelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(24, 21);
            this.LabelControl2.Name = "labelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(78, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Total Payments:";
            this.LabelControl2.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(24, 34);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(91, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Total Adjustments:";
            this.LabelControl3.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(24, 47);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(90, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Total Outstanding:";
            this.LabelControl4.UseMnemonic = false;
            //
            //total_outstanding
            //
            this.total_outstanding.Appearance.Options.UseTextOptions = true;
            this.total_outstanding.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_outstanding.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_outstanding.Location = new System.Drawing.Point(120, 47);
            this.total_outstanding.Name = "total_outstanding";
            this.total_outstanding.Size = new System.Drawing.Size(90, 13);
            this.total_outstanding.TabIndex = 7;
            this.total_outstanding.Text = "$0.00";
            this.total_outstanding.UseMnemonic = false;
            //
            //total_adjustments
            //
            this.total_adjustments.Appearance.Options.UseTextOptions = true;
            this.total_adjustments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_adjustments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_adjustments.Location = new System.Drawing.Point(120, 34);
            this.total_adjustments.Name = "total_adjustments";
            this.total_adjustments.Size = new System.Drawing.Size(90, 13);
            this.total_adjustments.TabIndex = 5;
            this.total_adjustments.Text = "$0.00";
            this.total_adjustments.UseMnemonic = false;
            //
            //total_payments
            //
            this.total_payments.Appearance.Options.UseTextOptions = true;
            this.total_payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_payments.Location = new System.Drawing.Point(120, 21);
            this.total_payments.Name = "total_payments";
            this.total_payments.Size = new System.Drawing.Size(90, 13);
            this.total_payments.TabIndex = 3;
            this.total_payments.Text = "$0.00";
            this.total_payments.UseMnemonic = false;
            //
            //total_billed
            //
            this.total_billed.Appearance.Options.UseTextOptions = true;
            this.total_billed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.total_billed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.total_billed.Location = new System.Drawing.Point(120, 8);
            this.total_billed.Name = "total_billed";
            this.total_billed.Size = new System.Drawing.Size(90, 13);
            this.total_billed.TabIndex = 1;
            this.total_billed.Text = "$0.00";
            this.total_billed.UseMnemonic = false;
            //
            //net_90
            //
            this.net_90.Appearance.Options.UseTextOptions = true;
            this.net_90.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.net_90.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.net_90.Location = new System.Drawing.Point(400, 47);
            this.net_90.Name = "net_90";
            this.net_90.Size = new System.Drawing.Size(90, 13);
            this.net_90.TabIndex = 15;
            this.net_90.Text = "$0.00";
            this.net_90.UseMnemonic = false;
            //
            //net_60
            //
            this.net_60.Appearance.Options.UseTextOptions = true;
            this.net_60.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.net_60.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.net_60.Location = new System.Drawing.Point(400, 34);
            this.net_60.Name = "net_60";
            this.net_60.Size = new System.Drawing.Size(90, 13);
            this.net_60.TabIndex = 13;
            this.net_60.Text = "$0.00";
            this.net_60.UseMnemonic = false;
            //
            //net_30
            //
            this.net_30.Appearance.Options.UseTextOptions = true;
            this.net_30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.net_30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.net_30.Location = new System.Drawing.Point(400, 21);
            this.net_30.Name = "net_30";
            this.net_30.Size = new System.Drawing.Size(90, 13);
            this.net_30.TabIndex = 11;
            this.net_30.Text = "$0.00";
            this.net_30.UseMnemonic = false;
            //
            //net_current
            //
            this.net_current.Appearance.Options.UseTextOptions = true;
            this.net_current.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.net_current.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.net_current.Location = new System.Drawing.Point(400, 8);
            this.net_current.Name = "net_current";
            this.net_current.Size = new System.Drawing.Size(90, 13);
            this.net_current.TabIndex = 9;
            this.net_current.Text = "$0.00";
            this.net_current.UseMnemonic = false;
            //
            //LabelControl9
            //
            this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl9.Location = new System.Drawing.Point(304, 47);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new System.Drawing.Size(43, 13);
            this.LabelControl9.TabIndex = 14;
            this.LabelControl9.Text = "90 Days:";
            this.LabelControl9.UseMnemonic = false;
            //
            //LabelControl10
            //
            this.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl10.Location = new System.Drawing.Point(304, 34);
            this.LabelControl10.Name = "LabelControl10";
            this.LabelControl10.Size = new System.Drawing.Size(43, 13);
            this.LabelControl10.TabIndex = 12;
            this.LabelControl10.Text = "60 Days:";
            this.LabelControl10.UseMnemonic = false;
            //
            //LabelControl11
            //
            this.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl11.Location = new System.Drawing.Point(304, 21);
            this.LabelControl11.Name = "LabelControl11";
            this.LabelControl11.Size = new System.Drawing.Size(43, 13);
            this.LabelControl11.TabIndex = 10;
            this.LabelControl11.Text = "30 Days:";
            this.LabelControl11.UseMnemonic = false;
            //
            //LabelControl12
            //
            this.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl12.Location = new System.Drawing.Point(304, 8);
            this.LabelControl12.Name = "LabelControl12";
            this.LabelControl12.Size = new System.Drawing.Size(41, 13);
            this.LabelControl12.TabIndex = 8;
            this.LabelControl12.Text = "Current:";
            this.LabelControl12.UseMnemonic = false;
            //
            //net_120
            //
            this.net_120.Appearance.Options.UseTextOptions = true;
            this.net_120.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.net_120.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.net_120.Location = new System.Drawing.Point(400, 60);
            this.net_120.Name = "net_120";
            this.net_120.Size = new System.Drawing.Size(90, 13);
            this.net_120.TabIndex = 17;
            this.net_120.Text = "$0.00";
            this.net_120.UseMnemonic = false;
            //
            //LabelControl14
            //
            this.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl14.Location = new System.Drawing.Point(304, 60);
            this.LabelControl14.Name = "LabelControl14";
            this.LabelControl14.Size = new System.Drawing.Size(94, 13);
            this.LabelControl14.TabIndex = 16;
            this.LabelControl14.Text = "120 days and over:";
            this.LabelControl14.UseMnemonic = false;
            //
            //gridColumn_invoice
            //
            this.gridColumn_invoice.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_invoice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_invoice.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_invoice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_invoice.Caption = "Invoice";
            this.gridColumn_invoice.DisplayFormat.FormatString = "000000000";
            this.gridColumn_invoice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_invoice.FieldName = "invoice";
            this.gridColumn_invoice.GroupFormat.FormatString = "000000000";
            this.gridColumn_invoice.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_invoice.Name = "gridColumn_invoice";
            this.gridColumn_invoice.Visible = true;
            this.gridColumn_invoice.VisibleIndex = 0;
            //
            //gridColumn_inv_date
            //
            this.gridColumn_inv_date.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_inv_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_inv_date.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_inv_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_inv_date.Caption = "Date";
            this.gridColumn_inv_date.DisplayFormat.FormatString = "d";
            this.gridColumn_inv_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_inv_date.FieldName = "inv_date";
            this.gridColumn_inv_date.GroupFormat.FormatString = "d";
            this.gridColumn_inv_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_inv_date.Name = "gridColumn_inv_date";
            this.gridColumn_inv_date.Visible = true;
            this.gridColumn_inv_date.VisibleIndex = 1;
            //
            //gridColumn_checknum
            //
            this.gridColumn_checknum.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_checknum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_checknum.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_checknum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_checknum.Caption = "Check #";
            this.gridColumn_checknum.FieldName = "checknum";
            this.gridColumn_checknum.Name = "gridColumn_checknum";
            this.gridColumn_checknum.Visible = true;
            this.gridColumn_checknum.VisibleIndex = 2;
            //
            //gridColumn_inv_amount
            //
            this.gridColumn_inv_amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_inv_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_inv_amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_inv_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_inv_amount.Caption = "Inv Amt";
            this.gridColumn_inv_amount.DisplayFormat.FormatString = "c2";
            this.gridColumn_inv_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_inv_amount.FieldName = "inv_amount";
            this.gridColumn_inv_amount.GroupFormat.FormatString = "c2";
            this.gridColumn_inv_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_inv_amount.Name = "gridColumn_inv_amount";
            this.gridColumn_inv_amount.Visible = true;
            this.gridColumn_inv_amount.VisibleIndex = 3;
            //
            //gridColumn_pmt_date
            //
            this.gridColumn_pmt_date.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_pmt_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_pmt_date.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_pmt_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_pmt_date.Caption = "Pmt Date";
            this.gridColumn_pmt_date.DisplayFormat.FormatString = "d";
            this.gridColumn_pmt_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_pmt_date.FieldName = "pmt_date";
            this.gridColumn_pmt_date.GroupFormat.FormatString = "d";
            this.gridColumn_pmt_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_pmt_date.Name = "gridColumn_pmt_date";
            this.gridColumn_pmt_date.Visible = true;
            this.gridColumn_pmt_date.VisibleIndex = 4;
            //
            //gridColumn_pmt_amount
            //
            this.gridColumn_pmt_amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_pmt_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_pmt_amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_pmt_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_pmt_amount.Caption = "Pmt Amt";
            this.gridColumn_pmt_amount.DisplayFormat.FormatString = "c2";
            this.gridColumn_pmt_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_pmt_amount.FieldName = "pmt_amount";
            this.gridColumn_pmt_amount.GroupFormat.FormatString = "c2";
            this.gridColumn_pmt_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_pmt_amount.Name = "gridColumn_pmt_amount";
            this.gridColumn_pmt_amount.Visible = true;
            this.gridColumn_pmt_amount.VisibleIndex = 5;
            //
            //gridColumn_adj_date
            //
            this.gridColumn_adj_date.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_adj_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_adj_date.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_adj_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_adj_date.Caption = "Adj Date";
            this.gridColumn_adj_date.DisplayFormat.FormatString = "d";
            this.gridColumn_adj_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_adj_date.FieldName = "adj_date";
            this.gridColumn_adj_date.GroupFormat.FormatString = "d";
            this.gridColumn_adj_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_adj_date.Name = "gridColumn_adj_date";
            this.gridColumn_adj_date.Visible = true;
            this.gridColumn_adj_date.VisibleIndex = 6;
            //
            //gridColumn_adj_amount
            //
            this.gridColumn_adj_amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_adj_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_adj_amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_adj_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_adj_amount.Caption = "Adj Amt";
            this.gridColumn_adj_amount.DisplayFormat.FormatString = "c2";
            this.gridColumn_adj_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_adj_amount.FieldName = "adj_amount";
            this.gridColumn_adj_amount.GroupFormat.FormatString = "c2";
            this.gridColumn_adj_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_adj_amount.Name = "gridColumn_adj_amount";
            this.gridColumn_adj_amount.Visible = true;
            this.gridColumn_adj_amount.VisibleIndex = 7;
            //
            //gridColumn_net_amount
            //
            this.gridColumn_net_amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_net_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_net_amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_net_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_net_amount.Caption = "Net";
            this.gridColumn_net_amount.DisplayFormat.FormatString = "c2";
            this.gridColumn_net_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_net_amount.FieldName = "net_amount";
            this.gridColumn_net_amount.GroupFormat.FormatString = "c2";
            this.gridColumn_net_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_net_amount.Name = "gridColumn_net_amount";
            this.gridColumn_net_amount.Visible = true;
            this.gridColumn_net_amount.VisibleIndex = 8;
            //
            //Page_Invoices
            //
            this.Controls.Add(this.net_120);
            this.Controls.Add(this.LabelControl14);
            this.Controls.Add(this.net_90);
            this.Controls.Add(this.net_60);
            this.Controls.Add(this.net_30);
            this.Controls.Add(this.net_current);
            this.Controls.Add(this.LabelControl9);
            this.Controls.Add(this.LabelControl10);
            this.Controls.Add(this.LabelControl11);
            this.Controls.Add(this.LabelControl12);
            this.Controls.Add(this.total_outstanding);
            this.Controls.Add(this.total_adjustments);
            this.Controls.Add(this.total_payments);
            this.Controls.Add(this.total_billed);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.include_all_invoices);
            this.Name = "Page_Invoices";
            this.Controls.SetChildIndex(this.include_all_invoices, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.total_billed, 0);
            this.Controls.SetChildIndex(this.total_payments, 0);
            this.Controls.SetChildIndex(this.total_adjustments, 0);
            this.Controls.SetChildIndex(this.total_outstanding, 0);
            this.Controls.SetChildIndex(this.LabelControl12, 0);
            this.Controls.SetChildIndex(this.LabelControl11, 0);
            this.Controls.SetChildIndex(this.LabelControl10, 0);
            this.Controls.SetChildIndex(this.LabelControl9, 0);
            this.Controls.SetChildIndex(this.net_current, 0);
            this.Controls.SetChildIndex(this.net_30, 0);
            this.Controls.SetChildIndex(this.net_60, 0);
            this.Controls.SetChildIndex(this.net_90, 0);
            this.Controls.SetChildIndex(this.LabelControl14, 0);
            this.Controls.SetChildIndex(this.net_120, 0);
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.include_all_invoices.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code

        /// <summary>
        /// Local class to extend the invoice record for the display grid information
        /// </summary>
        private class invoiceData
        {
            public invoiceData()
            {
                invoice    = default(Int32);
                inv_date   = default(DateTime);
                inv_amount = default(decimal);
                pmt_amount = default(decimal);
                adj_amount = default(decimal);
                checknum   = null;
                adj_date   = null;
                pmt_date   = null;
            }

            public decimal adj_amount { get; set; }
            public DateTime? adj_date { get; set; }
            public Int64? checknum    { get; set; }
            public decimal inv_amount { get; set; }
            public DateTime inv_date  { get; set; }
            public Int32 invoice      { get; set; }
            public int months         { get; set; }
            public decimal pmt_amount { get; set; }
            public DateTime? pmt_date { get; set; }

            /// <summary>
            /// Invoice net amount
            /// </summary>
            public decimal net_amount
            {
                get
                {
                    return inv_amount - pmt_amount - adj_amount;
                }
            }
        }
    }
}
