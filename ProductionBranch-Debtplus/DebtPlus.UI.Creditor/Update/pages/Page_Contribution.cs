#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Contribution : ControlBase
    {
        // Current creditor being edited
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        public Page_Contribution() : base()
        {
            InitializeComponent();

            // Set the contribution cycles
            contrib_cycle.Properties.DataSource = DebtPlus.LINQ.InMemory.contribCycleTypes.getList();
            pledge_cycle.Properties.DataSource  = DebtPlus.LINQ.InMemory.contribCycleTypes.getList();

            RegisterHandlers();
        }

        #region " Windows Form Designer generated code "

        private DevExpress.XtraEditors.SpinEdit acceptance_days;
        private DevExpress.XtraEditors.SpinEdit chks_per_invoice;
        private DevExpress.XtraEditors.SpinEdit contrib_bill_month;
        private DevExpress.XtraEditors.LookUpEdit contrib_cycle;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraEditors.CalcEdit min_accept_per_bill;
        internal MyCreditorContributionPcts MyCreditorContributionPcts1;
        private DebtPlus.Data.Controls.PercentEdit percent_balance;
        private DevExpress.XtraEditors.CalcEdit pledge_amt;
        private DevExpress.XtraEditors.SpinEdit pledge_bill_month;
        private DevExpress.XtraEditors.LookUpEdit pledge_cycle;
        private DevExpress.XtraEditors.TextEdit po_number;
        private DevExpress.XtraEditors.TextEdit sic;
        private DevExpress.XtraEditors.CheckEdit suppress_invoice;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.contrib_cycle = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.MyCreditorContributionPcts1 = new MyCreditorContributionPcts();
            this.percent_balance = new DebtPlus.Data.Controls.PercentEdit();
            this.acceptance_days = new DevExpress.XtraEditors.SpinEdit();
            this.chks_per_invoice = new DevExpress.XtraEditors.SpinEdit();
            this.po_number = new DevExpress.XtraEditors.TextEdit();
            this.suppress_invoice = new DevExpress.XtraEditors.CheckEdit();
            this.min_accept_per_bill = new DevExpress.XtraEditors.CalcEdit();
            this.contrib_bill_month = new DevExpress.XtraEditors.SpinEdit();
            this.pledge_amt = new DevExpress.XtraEditors.CalcEdit();
            this.pledge_cycle = new DevExpress.XtraEditors.LookUpEdit();
            this.sic = new DevExpress.XtraEditors.TextEdit();
            this.pledge_bill_month = new DevExpress.XtraEditors.SpinEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.contrib_cycle.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.percent_balance.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.acceptance_days.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.chks_per_invoice.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.po_number.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.suppress_invoice.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_per_bill.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.contrib_bill_month.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_amt.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_cycle.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.sic.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_bill_month.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            this.SuspendLayout();
            //
            //contrib_cycle
            //
            this.contrib_cycle.Location = new System.Drawing.Point(97, 44);
            this.contrib_cycle.Name = "contrib_cycle";
            this.contrib_cycle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.contrib_cycle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.contrib_cycle.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.contrib_cycle.Properties.DisplayMember = "description";
            this.contrib_cycle.Properties.NullText = "";
            this.contrib_cycle.Properties.ShowFooter = false;
            this.contrib_cycle.Properties.ShowHeader = false;
            this.contrib_cycle.Properties.ValueMember = "Id";
            this.contrib_cycle.Size = new System.Drawing.Size(194, 20);
            this.contrib_cycle.StyleController = this.LayoutControl1;
            this.contrib_cycle.TabIndex = 1;
            this.contrib_cycle.Properties.SortColumnIndex = 1;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.MyCreditorContributionPcts1);
            this.LayoutControl1.Controls.Add(this.percent_balance);
            this.LayoutControl1.Controls.Add(this.acceptance_days);
            this.LayoutControl1.Controls.Add(this.chks_per_invoice);
            this.LayoutControl1.Controls.Add(this.po_number);
            this.LayoutControl1.Controls.Add(this.contrib_cycle);
            this.LayoutControl1.Controls.Add(this.suppress_invoice);
            this.LayoutControl1.Controls.Add(this.min_accept_per_bill);
            this.LayoutControl1.Controls.Add(this.contrib_bill_month);
            this.LayoutControl1.Controls.Add(this.pledge_amt);
            this.LayoutControl1.Controls.Add(this.pledge_cycle);
            this.LayoutControl1.Controls.Add(this.sic);
            this.LayoutControl1.Controls.Add(this.pledge_bill_month);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(617, 311);
            this.LayoutControl1.TabIndex = 11;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //MyCreditorContributionPcts1
            //
            this.MyCreditorContributionPcts1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.MyCreditorContributionPcts1.Location = new System.Drawing.Point(24, 140);
            this.MyCreditorContributionPcts1.Name = "MyCreditorContributionPcts1";
            this.MyCreditorContributionPcts1.ReadOnly = false;
            this.MyCreditorContributionPcts1.Size = new System.Drawing.Size(267, 80);
            this.MyCreditorContributionPcts1.TabIndex = 8;
            //
            //percent_balance
            //
            this.percent_balance.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.percent_balance.Location = new System.Drawing.Point(494, 212);
            this.percent_balance.Name = "percent_balance";
            this.percent_balance.Properties.Allow100Percent = true;
            this.percent_balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.percent_balance.Properties.DisplayFormat.FormatString = "{0:p3}";
            this.percent_balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.percent_balance.Properties.EditFormat.FormatString = "{0:p3}";
            this.percent_balance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.percent_balance.Properties.Mask.EditMask = "p3";
            this.percent_balance.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.percent_balance.Properties.Precision = 3;
            this.percent_balance.Size = new System.Drawing.Size(99, 20);
            this.percent_balance.StyleController = this.LayoutControl1;
            this.percent_balance.TabIndex = 9;
            //
            //acceptance_days
            //
            this.acceptance_days.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.acceptance_days.Location = new System.Drawing.Point(536, 188);
            this.acceptance_days.Name = "acceptance_days";
            this.acceptance_days.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.acceptance_days.Properties.DisplayFormat.FormatString = "##0; ; ";
            this.acceptance_days.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.acceptance_days.Properties.EditFormat.FormatString = "f0";
            this.acceptance_days.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.acceptance_days.Properties.IsFloatValue = false;
            this.acceptance_days.Properties.Mask.BeepOnError = true;
            this.acceptance_days.Properties.Mask.EditMask = "n0";
            this.acceptance_days.Properties.MaxLength = 3;
            this.acceptance_days.Properties.MaxValue = new decimal(new Int32[] {
				365,
				0,
				0,
				0
			});
            this.acceptance_days.Size = new System.Drawing.Size(57, 20);
            this.acceptance_days.StyleController = this.LayoutControl1;
            this.acceptance_days.TabIndex = 7;
            //
            //chks_per_invoice
            //
            this.chks_per_invoice.Location = new System.Drawing.Point(130, 116);
            this.chks_per_invoice.Name = "chks_per_invoice";
            this.chks_per_invoice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.chks_per_invoice.Properties.DisplayFormat.FormatString = "######0; ; ";
            this.chks_per_invoice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.chks_per_invoice.Properties.IsFloatValue = false;
            this.chks_per_invoice.Properties.Mask.BeepOnError = true;
            this.chks_per_invoice.Properties.Mask.EditMask = "n0";
            this.chks_per_invoice.Properties.MaxLength = 5;
            this.chks_per_invoice.Properties.MaxValue = new decimal(new Int32[] {
				32767,
				0,
				0,
				0
			});
            this.chks_per_invoice.Size = new System.Drawing.Size(161, 20);
            this.chks_per_invoice.StyleController = this.LayoutControl1;
            this.chks_per_invoice.TabIndex = 7;
            //
            //po_number
            //
            this.po_number.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.po_number.Location = new System.Drawing.Point(428, 164);
            this.po_number.Name = "po_number";
            this.po_number.Properties.MaxLength = 50;
            this.po_number.Size = new System.Drawing.Size(165, 20);
            this.po_number.StyleController = this.LayoutControl1;
            this.po_number.TabIndex = 5;
            //
            //suppress_invoice
            //
            this.suppress_invoice.Location = new System.Drawing.Point(24, 268);
            this.suppress_invoice.Name = "suppress_invoice";
            this.suppress_invoice.Properties.Caption = "Suppress Printing Invoices";
            this.suppress_invoice.Size = new System.Drawing.Size(267, 19);
            this.suppress_invoice.StyleController = this.LayoutControl1;
            this.suppress_invoice.TabIndex = 0;
            //
            //min_accept_per_bill
            //
            this.min_accept_per_bill.Location = new System.Drawing.Point(130, 92);
            this.min_accept_per_bill.Name = "min_accept_per_bill";
            this.min_accept_per_bill.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.min_accept_per_bill.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.min_accept_per_bill.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.min_accept_per_bill.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_per_bill.Properties.EditFormat.FormatString = "f2";
            this.min_accept_per_bill.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_per_bill.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.min_accept_per_bill.Properties.Precision = 2;
            this.min_accept_per_bill.Size = new System.Drawing.Size(161, 20);
            this.min_accept_per_bill.StyleController = this.LayoutControl1;
            this.min_accept_per_bill.TabIndex = 5;
            //
            //contrib_bill_month
            //
            this.contrib_bill_month.EditValue = new decimal(new Int32[] {
				1,
				0,
				0,
				0
			});
            this.contrib_bill_month.Location = new System.Drawing.Point(130, 68);
            this.contrib_bill_month.Name = "contrib_bill_month";
            this.contrib_bill_month.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.contrib_bill_month.Properties.DisplayFormat.FormatString = "f0";
            this.contrib_bill_month.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.contrib_bill_month.Properties.EditFormat.FormatString = "f0";
            this.contrib_bill_month.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.contrib_bill_month.Properties.IsFloatValue = false;
            this.contrib_bill_month.Properties.Mask.BeepOnError = true;
            this.contrib_bill_month.Properties.Mask.EditMask = "N0";
            this.contrib_bill_month.Properties.MaxLength = 2;
            this.contrib_bill_month.Properties.MaxValue = new decimal(new Int32[] {
				31,
				0,
				0,
				0
			});
            this.contrib_bill_month.Properties.MinValue = new decimal(new Int32[] {
				1,
				0,
				0,
				0
			});
            this.contrib_bill_month.Size = new System.Drawing.Size(161, 20);
            this.contrib_bill_month.StyleController = this.LayoutControl1;
            this.contrib_bill_month.TabIndex = 3;
            //
            //pledge_amt
            //
            this.pledge_amt.Location = new System.Drawing.Point(425, 92);
            this.pledge_amt.Name = "pledge_amt";
            this.pledge_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.pledge_amt.Properties.DisplayFormat.FormatString = "$###,###,###,##0.00; ; ";
            this.pledge_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pledge_amt.Properties.EditFormat.FormatString = "f2";
            this.pledge_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pledge_amt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.pledge_amt.Properties.Precision = 2;
            this.pledge_amt.Size = new System.Drawing.Size(168, 20);
            this.pledge_amt.StyleController = this.LayoutControl1;
            this.pledge_amt.TabIndex = 5;
            //
            //pledge_cycle
            //
            this.pledge_cycle.Location = new System.Drawing.Point(425, 44);
            this.pledge_cycle.Name = "pledge_cycle";
            this.pledge_cycle.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.pledge_cycle.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.pledge_cycle.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.pledge_cycle.Properties.DisplayMember = "description";
            this.pledge_cycle.Properties.NullText = "";
            this.pledge_cycle.Properties.ShowFooter = false;
            this.pledge_cycle.Properties.ShowHeader = false;
            this.pledge_cycle.Properties.ValueMember = "Id";
            this.pledge_cycle.Size = new System.Drawing.Size(168, 20);
            this.pledge_cycle.StyleController = this.LayoutControl1;
            this.pledge_cycle.TabIndex = 1;
            this.pledge_cycle.Properties.SortColumnIndex = 1;
            //
            //sic
            //
            this.sic.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.sic.Location = new System.Drawing.Point(384, 140);
            this.sic.Name = "sic";
            this.sic.Properties.MaxLength = 20;
            this.sic.Size = new System.Drawing.Size(209, 20);
            this.sic.StyleController = this.LayoutControl1;
            this.sic.TabIndex = 3;
            //
            //pledge_bill_month
            //
            this.pledge_bill_month.EditValue = new decimal(new Int32[] {
				1,
				0,
				0,
				0
			});
            this.pledge_bill_month.Location = new System.Drawing.Point(425, 68);
            this.pledge_bill_month.Name = "pledge_bill_month";
            this.pledge_bill_month.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.pledge_bill_month.Properties.DisplayFormat.FormatString = "f0";
            this.pledge_bill_month.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pledge_bill_month.Properties.EditFormat.FormatString = "f0";
            this.pledge_bill_month.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.pledge_bill_month.Properties.IsFloatValue = false;
            this.pledge_bill_month.Properties.Mask.BeepOnError = true;
            this.pledge_bill_month.Properties.Mask.EditMask = "N0";
            this.pledge_bill_month.Properties.MaxLength = 2;
            this.pledge_bill_month.Properties.MaxValue = new decimal(new Int32[] {
				31,
				0,
				0,
				0
			});
            this.pledge_bill_month.Properties.MinValue = new decimal(new Int32[] {
				1,
				0,
				0,
				0
			});
            this.pledge_bill_month.Size = new System.Drawing.Size(168, 20);
            this.pledge_bill_month.StyleController = this.LayoutControl1;
            this.pledge_bill_month.TabIndex = 3;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlGroup4,
				this.LayoutControlGroup3,
				this.LayoutControlGroup5,
				this.LayoutControlGroup2
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(617, 311);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlGroup4
            //
            this.LayoutControlGroup4.CustomizationFormText = "Invoice And Billing Information";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem10,
				this.LayoutControlItem9,
				this.LayoutControlItem8,
				this.LayoutControlItem7,
				this.LayoutControlItem5
			});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(295, 224);
            this.LayoutControlGroup4.Text = "Invoice And Billing Information";
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.MyCreditorContributionPcts1;
            this.LayoutControlItem10.CustomizationFormText = "Contribution List";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(271, 84);
            this.LayoutControlItem10.Text = "Contribution List";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.chks_per_invoice;
            this.LayoutControlItem9.CustomizationFormText = "Checks Per Invoice:";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(271, 24);
            this.LayoutControlItem9.Text = "Checks Per Invoice:";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(102, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.min_accept_per_bill;
            this.LayoutControlItem8.CustomizationFormText = "Max Invoice Amount:";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(271, 24);
            this.LayoutControlItem8.Text = "Max Invoice Amount:";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(102, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.contrib_bill_month;
            this.LayoutControlItem7.CustomizationFormText = "Billing Period:";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(271, 24);
            this.LayoutControlItem7.Text = "Billing Period:";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(102, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.contrib_cycle;
            this.LayoutControlItem5.CustomizationFormText = "Invoice Cycle:";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(271, 24);
            this.LayoutControlItem5.Text = "Invoice Cycle:";
            this.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(68, 13);
            this.LayoutControlItem5.TextToControlDistance = 5;
            //
            //LayoutControlGroup3
            //
            this.LayoutControlGroup3.CustomizationFormText = "Invoicing Options";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] { this.LayoutControlItem4 });
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 224);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(295, 67);
            this.LayoutControlGroup3.Text = "Invoicing Options";
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.suppress_invoice;
            this.LayoutControlItem4.CustomizationFormText = "Suppress Invoice";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(271, 23);
            this.LayoutControlItem4.Text = "Suppress Invoice";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            //
            //LayoutControlGroup5
            //
            this.LayoutControlGroup5.CustomizationFormText = "LayoutControlGroup5";
            this.LayoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem13,
				this.LayoutControlItem12,
				this.LayoutControlItem11,
				this.LayoutControlItem6
			});
            this.LayoutControlGroup5.Location = new System.Drawing.Point(295, 116);
            this.LayoutControlGroup5.Name = "LayoutControlGroup5";
            this.LayoutControlGroup5.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AutoSize;
            this.LayoutControlGroup5.Size = new System.Drawing.Size(302, 175);
            this.LayoutControlGroup5.Text = "LayoutControlGroup5";
            this.LayoutControlGroup5.TextVisible = false;
            //
            //LayoutControlItem13
            //
            this.LayoutControlItem13.Control = this.percent_balance;
            this.LayoutControlItem13.CustomizationFormText = "Default Proposed Debt Percentage:";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(278, 79);
            this.LayoutControlItem13.Text = "Default Proposed Debt Percentage:";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(171, 13);
            //
            //LayoutControlItem12
            //
            this.LayoutControlItem12.Control = this.acceptance_days;
            this.LayoutControlItem12.CustomizationFormText = "Days before DEFAULT Proposal Acceptance:";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem12.Text = "Days before DEFAULT Proposal Acceptance:";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(213, 13);
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.po_number;
            this.LayoutControlItem11.CustomizationFormText = "PO/Approval Number:";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem11.Text = "PO/Approval Number:";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(105, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.sic;
            this.LayoutControlItem6.CustomizationFormText = "SIC Number:";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem6.Text = "SIC Number:";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(61, 13);
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Pledge Information";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem3,
				this.LayoutControlItem2,
				this.LayoutControlItem1
			});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(295, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(302, 116);
            this.LayoutControlGroup2.Text = "Pledge Information";
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.pledge_amt;
            this.LayoutControlItem3.CustomizationFormText = "Amount";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem3.Text = "Amount";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(102, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.pledge_bill_month;
            this.LayoutControlItem2.CustomizationFormText = "Period";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem2.Text = "Period";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(102, 13);
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.pledge_cycle;
            this.LayoutControlItem1.CustomizationFormText = "Request Cycle";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(278, 24);
            this.LayoutControlItem1.Text = "Request Cycle";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(102, 13);
            //
            //Page_Contribution
            //
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Contribution";
            this.Size = new System.Drawing.Size(617, 311);
            ((System.ComponentModel.ISupportInitialize)this.contrib_cycle.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.percent_balance.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.acceptance_days.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.chks_per_invoice.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.po_number.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.suppress_invoice.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.min_accept_per_bill.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.contrib_bill_month.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_amt.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_cycle.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.sic.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.pledge_bill_month.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.bc = bc;
            this.creditorRecord = CreditorRecord;
            UnRegisterHandlers();
            try
            {
                contrib_cycle.EditValue       = creditorRecord.contrib_cycle;
                contrib_bill_month.EditValue  = creditorRecord.contrib_bill_month;
                pledge_cycle.EditValue        = creditorRecord.pledge_cycle;
                pledge_amt.EditValue          = creditorRecord.pledge_amt;
                pledge_bill_month.EditValue   = creditorRecord.pledge_bill_month;
                percent_balance.EditValue     = creditorRecord.percent_balance;
                acceptance_days.EditValue     = creditorRecord.acceptance_days;
                chks_per_invoice.EditValue    = creditorRecord.chks_per_invoice;
                min_accept_per_bill.EditValue = creditorRecord.min_accept_per_bill.GetValueOrDefault(0M);
                po_number.EditValue           = creditorRecord.po_number;
                sic.EditValue                 = creditorRecord.sic;
                suppress_invoice.EditValue    = creditorRecord.suppress_invoice;

                // Load any changes to the contribution percentages as well
                MyCreditorContributionPcts1.ReadForm(bc, creditorRecord);

                // Disable the controls if the form is marked read-only
                if (!DebtPlus.Configuration.Config.UpdateCreditorContribution)
                {
                    MyCreditorContributionPcts1.ReadOnly    = true;
                    suppress_invoice.Properties.ReadOnly    = true;
                    min_accept_per_bill.Properties.ReadOnly = true;
                    sic.Properties.ReadOnly                 = true;
                    po_number.Properties.ReadOnly           = true;
                    chks_per_invoice.Properties.ReadOnly    = true;
                    acceptance_days.Properties.ReadOnly     = true;
                    percent_balance.Properties.ReadOnly     = true;
                    pledge_amt.Properties.ReadOnly          = true;
                    pledge_bill_month.Properties.ReadOnly   = true;
                    pledge_cycle.Properties.ReadOnly        = true;
                    contrib_cycle.Properties.ReadOnly       = true;
                    contrib_bill_month.Properties.ReadOnly  = true;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            creditorRecord.contrib_cycle       = Convert.ToChar(contrib_cycle.EditValue);
            creditorRecord.pledge_cycle        = Convert.ToChar(pledge_cycle.EditValue);
            creditorRecord.contrib_bill_month  = DebtPlus.Utils.Nulls.v_Int32(contrib_bill_month.EditValue).GetValueOrDefault();
            creditorRecord.pledge_amt          = DebtPlus.Utils.Nulls.v_Decimal(pledge_amt.EditValue);
            creditorRecord.pledge_bill_month   = DebtPlus.Utils.Nulls.v_Int32(pledge_bill_month.EditValue).GetValueOrDefault();
            creditorRecord.percent_balance     = DebtPlus.Utils.Nulls.v_Double(percent_balance.EditValue).GetValueOrDefault();
            creditorRecord.acceptance_days     = DebtPlus.Utils.Nulls.v_Int32(acceptance_days.EditValue);
            creditorRecord.chks_per_invoice    = DebtPlus.Utils.Nulls.v_Int32(chks_per_invoice.EditValue);
            creditorRecord.min_accept_per_bill = DebtPlus.Utils.Nulls.v_Decimal(min_accept_per_bill.EditValue);
            creditorRecord.po_number           = DebtPlus.Utils.Nulls.v_String(po_number.EditValue);
            creditorRecord.sic                 = DebtPlus.Utils.Nulls.v_String(sic.EditValue);
            creditorRecord.suppress_invoice    = Convert.ToBoolean(suppress_invoice.EditValue);
        }

        private void Percent_Validating(object sender, CancelEventArgs e)
        {
            // Find the value for the current field. If the value is out of range then complain.
            double? Value = DebtPlus.Utils.Nulls.v_Double(((TextEdit)sender).EditValue);
            if (Value.HasValue)
            {
                string ErrorMessage = string.Empty;
                if (Value < 0.0)
                {
                    ErrorMessage = "Minimum value is 0%";
                }
                else if (Value > 1.0)
                {
                    ErrorMessage = "Maximum value is 100%";
                }
                else
                {
                    return;
                }

                e.Cancel = true;
                ((TextEdit)sender).ErrorText = ErrorMessage;
            }
        }

        private void RegisterHandlers()
        {
            contrib_cycle.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            pledge_cycle.EditValueChanging  += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            percent_balance.Validating      += Percent_Validating;
        }

        private void UnRegisterHandlers()
        {
            contrib_cycle.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            pledge_cycle.EditValueChanging  -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            percent_balance.Validating      -= Percent_Validating;
        }
    }
}
