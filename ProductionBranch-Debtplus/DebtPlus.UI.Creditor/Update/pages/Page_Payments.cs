#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Payments : MyGridControl
    {
        // Current creditor record being updated
        private creditor creditorRecord = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Page_Payments()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Initialize the new form with the initial data
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;
            UnRegisterHandlers();
            try
            {
                RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Dummy routine to implement the interface requirements. Do not trap. Just ignore it.
        /// </summary>
        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
        }

        /// <summary>
        /// Handle today change in the date range
        /// </summary>
        private void DateRangeList1_SelectedValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Reload the grid control with the record data from the database
        /// </summary>
        private void RefreshData()
        {
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Find the date ranges desired
                    DateTime firstDate = DateRangeList1.StartingDate.Date;
                    DateTime lastDate = DateRangeList1.EndingDate.AddDays(1).Date;

                    // Retrieve the transactions from the database
                    var colRecords = (
                                        from rc in bc.registers_creditors
                                        join tr in bc.registers_trusts on rc.trust_register equals tr.Id into group_join
                                        from subtr in group_join.DefaultIfEmpty()
                                        where rc.creditor == creditorRecord.Id
                                        && rc.date_created >= firstDate
                                        && rc.date_created < lastDate
                                        select new
                                        {
                                            creditor_register = rc.Id,
                                            tran_type         = rc.tran_type,
                                            item_date         = rc.date_created,
                                            checknum          = subtr == null ? null : subtr.checknum,
                                            item_reconciled   = subtr == null ? null : subtr.reconciled_date,
                                            credit_amt        = rc.credit_amt,
                                            debit_amt         = rc.debit_amt,
                                            invoice           = rc.invoice_register,
                                            message           = rc.message
                                        }
                                    ).ToList();

                    GridControl1.DataSource = colRecords;
                    GridControl1.RefreshDataSource();
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor transactions");
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            DateRangeList1.SelectedValueChanged += DateRangeList1_SelectedValueChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            DateRangeList1.SelectedValueChanged -= DateRangeList1_SelectedValueChanged;
        }

        #region Windows Form Designer generated code

        private System.ComponentModel.IContainer components = null;

        private DateRangeList DateRangeList1;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_register;

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn1;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn2;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn3;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn4;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn5;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn6;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn7;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn8;

        private DevExpress.XtraEditors.LabelControl LabelControl1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.DateRangeList1 = new DateRangeList();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.GridColumn_creditor_register = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_register.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DateRangeList1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn1,
				this.GridColumn2,
				this.GridColumn3,
				this.GridColumn4,
				this.GridColumn5,
				this.GridColumn6,
				this.GridColumn7,
				this.GridColumn8,
				this.GridColumn_creditor_register
			});
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn1, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Size = new System.Drawing.Size(576, 264);
            //
            //gridColumn_invoice
            //
            this.GridColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn1.Caption = "Date";
            this.GridColumn1.DisplayFormat.FormatString = "d";
            this.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn1.FieldName = "item_date";
            this.GridColumn1.GroupFormat.FormatString = "d";
            this.GridColumn1.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn1.Name = "gridColumn_invoice";
            this.GridColumn1.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn1.Visible = true;
            this.GridColumn1.VisibleIndex = 0;
            this.GridColumn1.Width = 62;
            //
            //gridColumn_inv_date
            //
            this.GridColumn2.Caption = "Tran";
            this.GridColumn2.FieldName = "tran_type";
            this.GridColumn2.Name = "gridColumn_inv_date";
            this.GridColumn2.Visible = true;
            this.GridColumn2.VisibleIndex = 1;
            this.GridColumn2.Width = 38;
            //
            //gridColumn_checknum
            //
            this.GridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn3.Caption = "Check";
            this.GridColumn3.FieldName = "checknum";
            this.GridColumn3.Name = "gridColumn_checknum";
            this.GridColumn3.Visible = true;
            this.GridColumn3.VisibleIndex = 2;
            this.GridColumn3.Width = 57;
            //
            //gridColumn_inv_amount
            //
            this.GridColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn4.Caption = "Credit Amt";
            this.GridColumn4.DisplayFormat.FormatString = "c2";
            this.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn4.FieldName = "credit_amt";
            this.GridColumn4.GroupFormat.FormatString = "c2";
            this.GridColumn4.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn4.Name = "gridColumn_inv_amount";
            this.GridColumn4.Visible = true;
            this.GridColumn4.VisibleIndex = 3;
            this.GridColumn4.Width = 68;
            //
            //gridColumn_pmt_date
            //
            this.GridColumn5.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn5.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn5.Caption = "Debit Amt";
            this.GridColumn5.DisplayFormat.FormatString = "c2";
            this.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn5.FieldName = "debit_amt";
            this.GridColumn5.GroupFormat.FormatString = "c2";
            this.GridColumn5.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn5.Name = "gridColumn_pmt_date";
            this.GridColumn5.Visible = true;
            this.GridColumn5.VisibleIndex = 4;
            this.GridColumn5.Width = 64;
            //
            //gridColumn_pmt_amount
            //
            this.GridColumn6.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn6.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn6.Caption = "Reconciled";
            this.GridColumn6.DisplayFormat.FormatString = "d";
            this.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn6.FieldName = "item_reconciled";
            this.GridColumn6.GroupFormat.FormatString = "d";
            this.GridColumn6.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn6.Name = "gridColumn_pmt_amount";
            this.GridColumn6.Visible = true;
            this.GridColumn6.VisibleIndex = 5;
            this.GridColumn6.Width = 66;
            //
            //gridColumn_adj_date
            //
            this.GridColumn7.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn7.Caption = "Invoice";
            this.GridColumn7.FieldName = "invoice";
            this.GridColumn7.Name = "gridColumn_adj_date";
            this.GridColumn7.Visible = true;
            this.GridColumn7.VisibleIndex = 6;
            this.GridColumn7.Width = 61;
            //
            //gridColumn_adj_amount
            //
            this.GridColumn8.Caption = "Message";
            this.GridColumn8.FieldName = "message";
            this.GridColumn8.Name = "gridColumn_adj_amount";
            this.GridColumn8.Visible = true;
            this.GridColumn8.VisibleIndex = 7;
            this.GridColumn8.Width = 156;
            //
            //DateRangeList1
            //
            this.DateRangeList1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.DateRangeList1.Location = new System.Drawing.Point(408, 272);
            this.DateRangeList1.Name = "DateRangeList1";
            //
            //DateRangeList1.Properties
            //
            this.DateRangeList1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.DateRangeList1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.DateRangeList1.Size = new System.Drawing.Size(160, 20);
            this.DateRangeList1.TabIndex = 1;
            //
            //labelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(296, 275);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(104, 13);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.Text = "Select the date range";
            //
            //GridColumn_creditor_register
            //
            this.GridColumn_creditor_register.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_creditor_register.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_creditor_register.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_creditor_register.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_creditor_register.Caption = "Record #";
            this.GridColumn_creditor_register.DisplayFormat.FormatString = "f0";
            this.GridColumn_creditor_register.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_creditor_register.FieldName = "creditor_register";
            this.GridColumn_creditor_register.GroupFormat.FormatString = "f0";
            this.GridColumn_creditor_register.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_creditor_register.Name = "GridColumn_creditor_register";
            //
            //Page_Payments
            //
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.DateRangeList1);
            this.Name = "Page_Payments";
            this.Controls.SetChildIndex(this.DateRangeList1, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DateRangeList1.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code
    }
}