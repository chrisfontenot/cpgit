#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;
using DevExpress.XtraGrid.Views.Base;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Aliases : MyGridControl
    {
        // Collection of alias records to be displayed
        private System.Collections.Generic.List<creditor_addkey> colRecords = null;

        private creditor creditorRecord = null;

        public Page_Aliases()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;

            // Formatting classes for the type identifier
            GridColumn_type.DisplayFormat.Format = new addkey_type_formatter();
            GridColumn_type.GroupFormat.Format = new addkey_type_formatter();

            RegisterHandlers();
        }

        /// <summary>
        /// Load the record collection and initialize the page.
        /// This is the same as the "load" event but we don't load these pages so there is no event.
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            colRecords = bc.creditor_addkeys.Where(s => s.creditor == creditorRecord.Id).ToList();
            GridControl1.DataSource = colRecords;
        }

        /// <summary>
        /// Create today new record
        /// </summary>
        protected override void OnCreateRecord()
        {
            creditor_addkey record = DebtPlus.LINQ.Factory.Manufacture_creditor_addkey(creditorRecord.Id);

            // Edit the record
            using (var frm = new Forms.Form_Alias(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Insert the record
            using (var bc = new BusinessContext())
            {
                bc.creditor_addkeys.InsertOnSubmit(record);
                bc.SubmitChanges();
            }

            // Add the record to the list
            colRecords.Add(record);
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Delete the indicated record
        /// </summary>
        /// <param name="obj">Record to be deleted</param>
        protected override void OnDeleteRecord(object obj)
        {
            // There must be today record to delete
            var record = obj as creditor_addkey;
            if (record == null)
            {
                return;
            }

            // Confirm the deletion
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            // Delete the record
            using (var bc = new BusinessContext())
            {
                var q = bc.creditor_addkeys.Where(s => s.Id == record.Id).FirstOrDefault();
                if (q != null)
                {
                    bc.creditor_addkeys.DeleteOnSubmit(q);
                    bc.SubmitChanges();

                    // Remove the item from the list
                    colRecords.Remove(record);
                    GridControl1.RefreshDataSource();
                }
            }
        }

        /// <summary>
        /// Edit the indicated record
        /// </summary>
        /// <param name="obj">Record to be edited</param>
        protected override void OnEditRecord(object obj)
        {
            // There must be today record to edit
            var record = obj as creditor_addkey;
            if (record == null)
            {
                return;
            }

            // Edit the record
            using (var frm = new Forms.Form_Alias(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Update the record
            using (var bc = new BusinessContext())
            {
                var q = bc.creditor_addkeys.Where(s => s.Id == record.Id).FirstOrDefault();
                if (q != null)
                {
                    q.additional = record.additional;
                    q.type = record.type;
                    bc.SubmitChanges();
                }
            }

            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Sort the items first by the numerical value and then strings
        /// </summary>
        private static Int32 CompareStringsNumerically(object value1, object value2)
        {
            // If both are missing then the answer is equal
            if (value1 == null && value2 == null)
            {
                return 0;
            }

            // If the first is missing and the second is not then the answer is less
            if (value1 == null)
            {
                return -1;
            }

            // If the second is missing and the first is not then the answer is greater
            if (value2 == null)
            {
                return 1;
            }

            // Find the numerical part and the string part
            Regex rx = new Regex("0*(?<numeric>\\d*)(?<alpha>.*)");

            // Look for today match on the first string
            string Whole1 = string.Empty;
            string Fractional1 = null;
            Match match_expression = rx.Match(Convert.ToString(value1));
            if (match_expression.Success)
            {
                Whole1 = match_expression.Groups[1].Value;
                Fractional1 = match_expression.Groups[2].Value;
            }
            else
            {
                Fractional1 = Convert.ToString(value1);
            }

            // Do the same to the second value
            string Whole2 = string.Empty;
            string Fractional2 = null;
            match_expression = rx.Match(Convert.ToString(value2));
            if (match_expression.Success)
            {
                Whole2 = match_expression.Groups[1].Value;
                Fractional2 = match_expression.Groups[2].Value;
            }
            else
            {
                Fractional2 = Convert.ToString(value2);
            }

            // Now, compare the two string lengths to determine order. The longer one is "greater" than the shorter one "smaller"
            Int32 answer = Whole1.Length.CompareTo(Whole2.Length);
            if (answer != 0)
            {
                return answer;
            }

            // If the two lengths are the same then compare the two strings.
            answer = Whole1.CompareTo(Whole2);
            if (answer != 0)
            {
                return answer;
            }

            // If the numeric parts are equal then compare the alphabetic parts
            return string.Compare(Fractional1, Fractional2, true);
        }

        /// <summary>
        /// Handle the grid custom sort event
        /// </summary>
        private void Gridview1_CustomColumnSort(object Sender, CustomColumnSortEventArgs e)
        {
            // Sort the name field in today special sequence
            if (object.ReferenceEquals(e.Column, GridColumn_Name))
            {
                e.Handled = true;
                e.Result = CompareStringsNumerically(e.Value1, e.Value2);
            }
        }

        private void RegisterHandlers()
        {
            GridView1.CustomColumnSort += Gridview1_CustomColumnSort;
        }

        private void UnRegisterHandlers()
        {
            GridView1.CustomColumnSort -= Gridview1_CustomColumnSort;
        }

        #region " Windows Form Designer generated code "

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Alias;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Name;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_type;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.GridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Alias = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 296);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(576, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 296);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(576, 0);
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.GridControl1.Location = new System.Drawing.Point(0, 40);
            this.GridControl1.Size = new System.Drawing.Size(576, 256);
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_type,
            this.GridColumn_Name,
            this.GridColumn_Alias});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(552, 48);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = "This form lists the alias names by which the creditor may have been known.  You t" +
    "odayre free to use any additional names or sequences by which this creditor may " +
    "be found.";
            // 
            // GridColumn_type
            // 
            this.GridColumn_type.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_type.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_type.Caption = "Type";
            this.GridColumn_type.CustomizationCaption = "Item Type";
            this.GridColumn_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_type.FieldName = "type";
            this.GridColumn_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_type.Name = "GridColumn_type";
            this.GridColumn_type.OptionsColumn.AllowEdit = false;
            this.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_type.OptionsColumn.ReadOnly = true;
            this.GridColumn_type.Visible = true;
            this.GridColumn_type.VisibleIndex = 0;
            this.GridColumn_type.Width = 81;
            // 
            // GridColumn_Name
            // 
            this.GridColumn_Name.Caption = "Name";
            this.GridColumn_Name.CustomizationCaption = "Item Name";
            this.GridColumn_Name.FieldName = "additional";
            this.GridColumn_Name.Name = "GridColumn_Name";
            this.GridColumn_Name.OptionsColumn.AllowEdit = false;
            this.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Name.OptionsColumn.ReadOnly = true;
            this.GridColumn_Name.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.GridColumn_Name.Visible = true;
            this.GridColumn_Name.VisibleIndex = 1;
            this.GridColumn_Name.Width = 518;
            // 
            // GridColumn_Alias
            // 
            this.GridColumn_Alias.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Alias.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Alias.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Alias.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Alias.Caption = "ID";
            this.GridColumn_Alias.CustomizationCaption = "Record ID";
            this.GridColumn_Alias.FieldName = "Id";
            this.GridColumn_Alias.Name = "GridColumn_Alias";
            this.GridColumn_Alias.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Alias.OptionsColumn.ReadOnly = true;
            // 
            // Page_Aliases
            // 
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Aliases";
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Formatting class for the type of alias
        /// </summary>
        private class addkey_type_formatter : IFormatProvider, ICustomFormatter
        {
            public addkey_type_formatter()
                : base()
            {
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (arg != null && arg is string)
                {
                    string strArg = Convert.ToString(arg);
                    var q = DebtPlus.LINQ.InMemory.addkeyTypes.getList().Find(s => s.Id == strArg);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
    }
}