#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Contacts : MyGridControl
    {
        // Current list of creditor contacts
        private System.Collections.Generic.List<creditor_contact> colRecords = null;

        // Current creditor being edited
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        public Page_Contacts()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
            RegisterHandlers();
        }

        // Delegate to define the formatting function
        private delegate string RetrieveDelegate(creditor_contact record);

        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            UnRegisterHandlers();

            this.creditorRecord = CreditorRecord;
            this.bc = bc;

            try
            {
                // Load the list of records to be processed
                colRecords = bc.creditor_contacts.Where(s => s.creditor == creditorRecord.Id).ToList();

                // Suppress update events for now
                GridControl1.BeginInit();

                // Set the display formats for the grid controls
                LoadCellOptions(GridColumn_creditor_contact_type, new ClassFormatContactType(colRecords));

                // Name references
                LoadCellOptions(GridColumn_name, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID); }));
                LoadCellOptions(GridColumn_name_prefix, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID, "Prefix"); }));
                LoadCellOptions(GridColumn_name_first, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID, "First"); }));
                LoadCellOptions(GridColumn_name_middle, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID, "Middle"); }));
                LoadCellOptions(GridColumn_name_last, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID, "Last"); }));
                LoadCellOptions(GridColumn_name_suffix, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getNameString(new DebtPlus.LINQ.BusinessContext(), record.NameID, "Suffix"); }));

                // Telephone references
                LoadCellOptions(GridColumn_phone, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.TelephoneID); }));
                LoadCellOptions(GridColumn_fax, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.FAXID); }));
                LoadCellOptions(GridColumn_title, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.AltTelephoneID); }));
                LoadCellOptions(GridColumn_phone_raw, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.TelephoneID, "Number"); }));
                LoadCellOptions(GridColumn_phone_ext, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.TelephoneID, "Ext"); }));
                LoadCellOptions(GridColumn_fax_raw, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getTelephoneNumberString(new DebtPlus.LINQ.BusinessContext(), record.TelephoneID); }));

                // Address references
                LoadCellOptions(GridColumn_address_line1, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "AddressLine1"); }));
                LoadCellOptions(GridColumn_address_line2, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "address_line_2"); }));
                LoadCellOptions(GridColumn_address_city, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "city"); }));
                LoadCellOptions(GridColumn_address_state, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "StateProvince"); }));
                LoadCellOptions(GridColumn_address_postalcode, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "PostalCode"); }));
                LoadCellOptions(GridColumn_address_line3, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getAddressString(new DebtPlus.LINQ.BusinessContext(), record.AddressID, "address_line_3"); }));

                // Email references
                LoadCellOptions(GridColumn_email, new ClassFieldFormatter(colRecords, delegate(creditor_contact record) { return getEmailString(new DebtPlus.LINQ.BusinessContext(), record.EmailID); }));

                GridControl1.DataSource = colRecords;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor contact records");
            }

            finally
            {
                RegisterHandlers();
                GridControl1.EndInit();
            }

            // Refresh the grid data
            GridControl1.RefreshDataSource();
        }

        protected override void OnCreateRecord()
        {
            // If the user is not allowed to update the record then stop here
            if (!DebtPlus.Configuration.Config.UpdateCreditorContacts)
            {
                return;
            }

            try
            {
                var record = DebtPlus.LINQ.Factory.Manufacture_creditor_contact();
                record.creditor = creditorRecord.Id;

                using (var frm = new Forms.Form_Contact(record))
                {
                    if (frm.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                }

                bc.creditor_contacts.InsertOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Add(record);
                GridControl1.RefreshDataSource();

                // Update the display information for the current record
                OnSelectRecord(record);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating new creditor contact");
            }
        }

        protected override void OnDeleteRecord(object obj)
        {
            // Find the record to be deleted
            var record = obj as creditor_contact;
            if (obj == null)
            {
                return;
            }

            // If the user is not allowed to update the record then stop here
            if (!DebtPlus.Configuration.Config.UpdateCreditorContacts)
            {
                return;
            }

            // Ask if the item is to be removed
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            // Remove the item from the database
            try
            {
                bc.creditor_contacts.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                GridControl1.RefreshDataSource();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting creditor contact");
            }
        }

        /// <summary>
        /// Edit the current record
        /// </summary>
        /// <param name="obj">Record to be updated</param>
        protected override void OnEditRecord(object obj)
        {
            var record = obj as creditor_contact;
            if (obj == null)
            {
                return;
            }

            // Display the record to the user
            using (var frm = new Forms.Form_Contact(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // If the user is not allowed to update the record then stop here
            if (!DebtPlus.Configuration.Config.UpdateCreditorContacts)
            {
                return;
            }

            // Update the record in the database
            try
            {
                bc.SubmitChanges();
                GridControl1.RefreshDataSource();

                // Update the display information for the current record
                OnSelectRecord(record);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating creditor contacts");
            }
        }

        protected override void OnSelectRecord(object obj)
        {
            GroupControl1.Text = "Contact Detail";

            // Find the record to be deleted
            var record = obj as creditor_contact;
            if (obj == null)
            {
                // Blank the current information
                label_name.Text    = string.Empty;
                label_title.Text   = string.Empty;
                label_address.Text = string.Empty;
                label_notes.Text   = string.Empty;
                label_phone.Text   = string.Empty;
                label_fax.Text     = string.Empty;
                label_email.Text   = string.Empty;
                return;
            }

            // Update the information from the current row. We need a new context because the update
            // controls for the records will update the database and not our local cache. So, we can't
            // use the cache for storing thing such as names, addresses, etc.
            using (var bcContacts = new BusinessContext())
            {
                label_name.Text    = getNameString(bcContacts, record.NameID);
                label_phone.Text   = getTelephoneNumberString(bcContacts, record.TelephoneID);
                label_fax.Text     = getTelephoneNumberString(bcContacts, record.FAXID);
                label_email.Text   = getEmailString(bcContacts, record.EmailID);
                label_address.Text = getAddressString(bcContacts, record.AddressID);
                label_title.Text   = record.Title;
                label_notes.Text   = record.Notes;
            }
        }

        /// <summary>
        /// Get the field from the Address record
        /// </summary>
        /// <param name="AddressID">Pointer to the desired address</param>
        /// <returns>The corresponding field from the record</returns>
        private static string getAddressString(DebtPlus.LINQ.BusinessContext bc, Int32? AddressID)
        {
            return getAddressString(bc, AddressID, string.Empty);
        }

        /// <summary>
        /// Get the field from the Address record
        /// </summary>
        /// <param name="AddressID">Pointer to the desired address</param>
        /// <param name="Field">Field name in the record</param>
        /// <returns>The corresponding field from the record</returns>
        private static string getAddressString(DebtPlus.LINQ.BusinessContext bc, Int32? AddressID, string Field)
        {
            // It is easy if there is no record pointer
            if (!AddressID.HasValue)
            {
                return string.Empty;
            }

            var q = bc.addresses.Where(s => s.Id == AddressID.Value).FirstOrDefault();
            if (q == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(Field))
            {
                return q.ToString();
            }

            // Return the corresponding field from the record with reflection.
            var pi = typeof(address).GetProperty(Field);
            return (string)pi.GetValue(q, null);
        }

        /// <summary>
        /// Get the field from the Email address record
        /// </summary>
        /// <param name="EmailID">Pointer to the desired email address</param>
        /// <returns>The corresponding field from the record</returns>
        private static string getEmailString(BusinessContext bc, Int32? EmailID)
        {
            return getEmailString(bc, EmailID, string.Empty);
        }

        /// <summary>
        /// Get the field from the Email address record
        /// </summary>
        /// <param name="EmailID">Pointer to the desired email address</param>
        /// <param name="Field">Field name in the record</param>
        /// <returns>The corresponding field from the record</returns>
        private static string getEmailString(BusinessContext bc, Int32? EmailID, string Field)
        {
            // It is easy if there is no record pointer
            if (!EmailID.HasValue)
            {
                return string.Empty;
            }

            var q = bc.EmailAddresses.Where(s => s.Id == EmailID.Value).FirstOrDefault();
            if (q == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(Field))
            {
                return q.ToString();
            }

            // Return the corresponding field from the record with reflection.
            var pi = typeof(EmailAddress).GetProperty(Field);
            return (string)pi.GetValue(q, null);
        }

        /// <summary>
        /// Get the desired field from the name record
        /// </summary>
        /// <param name="NameID">Pointer to the name record in the database</param>
        /// <returns>The corresponding name record field</returns>
        private static string getNameString(BusinessContext bc, Int32? NameID)
        {
            return getNameString(bc, NameID, string.Empty);
        }

        /// <summary>
        /// Get the desired field from the name record
        /// </summary>
        /// <param name="NameID">Pointer to the name record in the database</param>
        /// <param name="Field">Desired field in the name record</param>
        /// <returns>The corresponding name record field</returns>
        private static string getNameString(BusinessContext bc, Int32? NameID, string Field)
        {
            // It is easy if there is no record pointer
            if (!NameID.HasValue)
            {
                return string.Empty;
            }

            var q = bc.Names.Where(s => s.Id == NameID.Value).FirstOrDefault();
            if (q == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(Field))
            {
                return q.ToString();
            }

            // Return the corresponding field from the record with reflection.
            var pi = typeof(Name).GetProperty(Field);
            return (string)pi.GetValue(q, null);
        }

        /// <summary>
        /// Get the field from the telephone number record.
        /// </summary>
        /// <param name="PhoneID">Pointer to the telephone number record</param>
        /// <returns>The string value from the field in the record</returns>
        private static string getTelephoneNumberString(BusinessContext bc, Int32? PhoneID)
        {
            return getTelephoneNumberString(bc, PhoneID, string.Empty);
        }

        /// <summary>
        /// Get the field from the telephone number record.
        /// </summary>
        /// <param name="PhoneID">Pointer to the telephone number record</param>
        /// <param name="Field">String for the field name. Names are case-specific.</param>
        /// <returns>The string value from the field in the record</returns>
        private static string getTelephoneNumberString(BusinessContext bc, Int32? PhoneID, string Field)
        {
            // It is easy if there is no record pointer
            if (!PhoneID.HasValue)
            {
                return string.Empty;
            }

            var q = bc.TelephoneNumbers.Where(s => s.Id == PhoneID.Value).FirstOrDefault();
            if (q == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(Field))
            {
                return q.ToString();
            }

            // Return the corresponding field from the record with reflection.
            var pi = typeof(TelephoneNumber).GetProperty(Field);
            return (string) pi.GetValue(q, null);
        }

        private void LoadCellOptions(GridColumn Col, IFormatProvider fmt)
        {
            LoadCellOptions(Col, fmt, string.Empty);
        }

        private void LoadCellOptions(GridColumn Col, IFormatProvider fmt, string FormatString)
        {
            Col.DisplayFormat.FormatType = FormatType.Custom;
            Col.DisplayFormat.Format = fmt;
            Col.DisplayFormat.FormatString = FormatString;

            Col.GroupFormat.FormatType = FormatType.Custom;
            Col.GroupFormat.Format = fmt;
            Col.GroupFormat.FormatString = FormatString;

            Col.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
            Col.AppearanceCell.Options.UseTextOptions = true;

            Col.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near;
            Col.AppearanceHeader.Options.UseTextOptions = true;
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridColumn_creditor_contact_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_contact_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_phone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_phone.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_fax = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_fax.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_contact_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_contact_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name_prefix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name_prefix.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name_first = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name_first.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name_middle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name_middle.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name_last = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name_last.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name_suffix = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name_suffix.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_title = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_title.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_phone_raw = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_phone_raw.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_phone_ext = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_phone_ext.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_fax_raw = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_fax_raw.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_line1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_line1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_line2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_line2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_city = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_city.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_state = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_state.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_postalcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_postalcode.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_address_line3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_address_line3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_email = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_email.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_notes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_notes.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label_notes = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.label_address = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.label_email = new DevExpress.XtraEditors.LabelControl();
            this.label_fax = new DevExpress.XtraEditors.LabelControl();
            this.label_phone = new DevExpress.XtraEditors.LabelControl();
            this.label_title = new DevExpress.XtraEditors.LabelControl();
            this.label_name = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            this.SuspendLayout();
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_creditor_contact_type,
				this.GridColumn_name,
				this.GridColumn_phone,
				this.GridColumn_fax,
				this.GridColumn_id,
				this.GridColumn_contact_type,
				this.GridColumn_name_prefix,
				this.GridColumn_name_first,
				this.GridColumn_name_middle,
				this.GridColumn_name_last,
				this.GridColumn_name_suffix,
				this.GridColumn_title,
				this.GridColumn_phone_raw,
				this.GridColumn_phone_ext,
				this.GridColumn_fax_raw,
				this.GridColumn_address_line1,
				this.GridColumn_address_line2,
				this.GridColumn_address_city,
				this.GridColumn_address_state,
				this.GridColumn_address_postalcode,
				this.GridColumn_address_line3,
				this.GridColumn_email,
				this.GridColumn_notes
			});
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            //
            //GridControl1
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Size = new System.Drawing.Size(576, 152);
            //
            //GridColumn_creditor_contact_type
            //
            this.GridColumn_creditor_contact_type.Caption = "Type";
            this.GridColumn_creditor_contact_type.CustomizationCaption = "Formatted Type";
            this.GridColumn_creditor_contact_type.FieldName = "creditor_contact_type";
            this.GridColumn_creditor_contact_type.Name = "GridColumn_creditor_contact_type";
            this.GridColumn_creditor_contact_type.Visible = true;
            this.GridColumn_creditor_contact_type.VisibleIndex = 0;
            //
            //GridColumn_name
            //
            this.GridColumn_name.Caption = "Name";
            this.GridColumn_name.CustomizationCaption = "Formatted Name";
            this.GridColumn_name.FieldName = "Id";
            this.GridColumn_name.Name = "GridColumn_name";
            this.GridColumn_name.Visible = true;
            this.GridColumn_name.VisibleIndex = 1;
            //
            //GridColumn_phone
            //
            this.GridColumn_phone.Caption = "Phone";
            this.GridColumn_phone.CustomizationCaption = "Formatted Phone";
            this.GridColumn_phone.FieldName = "Id";
            this.GridColumn_phone.Name = "GridColumn_phone";
            this.GridColumn_phone.Visible = true;
            this.GridColumn_phone.VisibleIndex = 2;
            //
            //GridColumn_fax
            //
            this.GridColumn_fax.Caption = "FAX";
            this.GridColumn_fax.CustomizationCaption = "Formatted FAX";
            this.GridColumn_fax.FieldName = "Id";
            this.GridColumn_fax.Name = "GridColumn_fax";
            this.GridColumn_fax.Visible = true;
            this.GridColumn_fax.VisibleIndex = 3;
            //
            //GridColumn_id
            //
            this.GridColumn_id.Caption = "ID";
            this.GridColumn_id.CustomizationCaption = "Record Number";
            this.GridColumn_id.FieldName = "Id";
            this.GridColumn_id.Name = "GridColumn_id";
            //
            //GridColumn_contact_type
            //
            this.GridColumn_contact_type.Caption = "Type";
            this.GridColumn_contact_type.CustomizationCaption = "Raw Contact Type";
            this.GridColumn_contact_type.FieldName = "Id";
            this.GridColumn_contact_type.Name = "GridColumn_contact_type";
            //
            //GridColumn_name_prefix
            //
            this.GridColumn_name_prefix.Caption = "Prefix";
            this.GridColumn_name_prefix.CustomizationCaption = "Name : Prefix";
            this.GridColumn_name_prefix.FieldName = "Id";
            this.GridColumn_name_prefix.Name = "GridColumn_name_prefix";
            this.GridColumn_name_prefix.Tag = "prefix";
            //
            //GridColumn_name_first
            //
            this.GridColumn_name_first.Caption = "First";
            this.GridColumn_name_first.CustomizationCaption = "Name : First";
            this.GridColumn_name_first.FieldName = "Id";
            this.GridColumn_name_first.Name = "GridColumn_name_first";
            this.GridColumn_name_first.Tag = "first";
            //
            //GridColumn_name_middle
            //
            this.GridColumn_name_middle.Caption = "Middle";
            this.GridColumn_name_middle.CustomizationCaption = "Name : Middle";
            this.GridColumn_name_middle.FieldName = "Id";
            this.GridColumn_name_middle.Name = "GridColumn_name_middle";
            this.GridColumn_name_middle.Tag = "middle";
            //
            //GridColumn_name_last
            //
            this.GridColumn_name_last.Caption = "Last";
            this.GridColumn_name_last.CustomizationCaption = "Name : Last";
            this.GridColumn_name_last.FieldName = "Id";
            this.GridColumn_name_last.Name = "GridColumn_name_last";
            this.GridColumn_name_last.Tag = "last";
            //
            //GridColumn_name_suffix
            //
            this.GridColumn_name_suffix.Caption = "Suffix";
            this.GridColumn_name_suffix.CustomizationCaption = "Name : Suffix";
            this.GridColumn_name_suffix.FieldName = "Id";
            this.GridColumn_name_suffix.Name = "GridColumn_name_suffix";
            this.GridColumn_name_suffix.Tag = "suffix";
            //
            //GridColumn_title
            //
            this.GridColumn_title.Caption = "Title";
            this.GridColumn_title.CustomizationCaption = "Title";
            this.GridColumn_title.FieldName = "Id";
            this.GridColumn_title.Name = "GridColumn_title";
            //
            //GridColumn_phone_raw
            //
            this.GridColumn_phone_raw.Caption = "Phone";
            this.GridColumn_phone_raw.CustomizationCaption = "Raw Phone Number";
            this.GridColumn_phone_raw.FieldName = "Id";
            this.GridColumn_phone_raw.Name = "GridColumn_phone_raw";
            this.GridColumn_phone_raw.Tag = "number";
            //
            //GridColumn_phone_ext
            //
            this.GridColumn_phone_ext.Caption = "Ext";
            this.GridColumn_phone_ext.CustomizationCaption = "Raw Phone Extension";
            this.GridColumn_phone_ext.FieldName = "Id";
            this.GridColumn_phone_ext.Name = "GridColumn_phone_ext";
            this.GridColumn_phone_ext.Tag = "ext";
            //
            //GridColumn_fax_raw
            //
            this.GridColumn_fax_raw.Caption = "FAX";
            this.GridColumn_fax_raw.CustomizationCaption = "Raw FAX number";
            this.GridColumn_fax_raw.FieldName = "Id";
            this.GridColumn_fax_raw.Name = "GridColumn_fax_raw";
            //
            //GridColumn_address_line1
            //
            this.GridColumn_address_line1.Caption = "Address1";
            this.GridColumn_address_line1.CustomizationCaption = "Address : Line 1";
            this.GridColumn_address_line1.FieldName = "Id";
            this.GridColumn_address_line1.Name = "GridColumn_address_line1";
            this.GridColumn_address_line1.Tag = "address_line_1";
            //
            //GridColumn_address_line2
            //
            this.GridColumn_address_line2.Caption = "Address2";
            this.GridColumn_address_line2.CustomizationCaption = "Address : Line 2";
            this.GridColumn_address_line2.FieldName = "Id";
            this.GridColumn_address_line2.Name = "GridColumn_address_line2";
            this.GridColumn_address_line2.Tag = "address_line_2";
            //
            //GridColumn_address_city
            //
            this.GridColumn_address_city.Caption = "City";
            this.GridColumn_address_city.CustomizationCaption = "Address : City";
            this.GridColumn_address_city.FieldName = "Id";
            this.GridColumn_address_city.Name = "GridColumn_address_city";
            this.GridColumn_address_city.Tag = "city";
            //
            //GridColumn_address_state
            //
            this.GridColumn_address_state.Caption = "State";
            this.GridColumn_address_state.CustomizationCaption = "Address : State";
            this.GridColumn_address_state.FieldName = "Id";
            this.GridColumn_address_state.Name = "GridColumn_address_state";
            this.GridColumn_address_postalcode.Tag = "state";
            //
            //GridColumn_address_postalcode
            //
            this.GridColumn_address_postalcode.Caption = "Postal Code";
            this.GridColumn_address_postalcode.CustomizationCaption = "Address : ZIPCode";
            this.GridColumn_address_postalcode.FieldName = "Id";
            this.GridColumn_address_postalcode.Name = "GridColumn_address_postalcode";
            this.GridColumn_address_line3.Tag = "postalcode";
            //
            //GridColumn_address_line3
            //
            this.GridColumn_address_line3.Caption = "Address3";
            this.GridColumn_address_line3.CustomizationCaption = "Address : Line 3";
            this.GridColumn_address_line3.FieldName = "Id";
            this.GridColumn_address_line3.Name = "GridColumn_address_line3";
            this.GridColumn_address_line3.Tag = "address_line_3";
            //
            //GridColumn_email
            //
            this.GridColumn_email.Caption = "Email";
            this.GridColumn_email.CustomizationCaption = "Email Address";
            this.GridColumn_email.FieldName = "Id";
            this.GridColumn_email.Name = "GridColumn_email";
            //
            //GridColumn_notes
            //
            this.GridColumn_notes.Caption = "Notes";
            this.GridColumn_notes.CustomizationCaption = "Notes";
            this.GridColumn_notes.FieldName = "notes";
            this.GridColumn_notes.Name = "GridColumn_notes";
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.label_notes);
            this.GroupControl1.Controls.Add(this.LabelControl13);
            this.GroupControl1.Controls.Add(this.label_address);
            this.GroupControl1.Controls.Add(this.LabelControl11);
            this.GroupControl1.Controls.Add(this.label_email);
            this.GroupControl1.Controls.Add(this.label_fax);
            this.GroupControl1.Controls.Add(this.label_phone);
            this.GroupControl1.Controls.Add(this.label_title);
            this.GroupControl1.Controls.Add(this.label_name);
            this.GroupControl1.Controls.Add(this.LabelControl5);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Location = new System.Drawing.Point(0, 152);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(576, 144);
            this.GroupControl1.TabIndex = 1;
            //
            //label_notes
            //
            this.label_notes.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_notes.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_notes.Appearance.Options.UseBackColor = true;
            this.label_notes.Location = new System.Drawing.Point(56, 100);
            this.label_notes.Name = "label_notes";
            this.label_notes.Size = new System.Drawing.Size(520, 44);
            this.label_notes.TabIndex = 13;
            //
            //LabelControl13
            //
            this.LabelControl13.Location = new System.Drawing.Point(8, 100);
            this.LabelControl13.Name = "LabelControl13";
            this.LabelControl13.Size = new System.Drawing.Size(32, 14);
            this.LabelControl13.TabIndex = 12;
            this.LabelControl13.Text = "Notes:";
            //
            //label_address
            //
            this.label_address.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_address.Appearance.Options.UseBackColor = true;
            this.label_address.Location = new System.Drawing.Point(56, 52);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(232, 48);
            this.label_address.TabIndex = 11;
            //
            //LabelControl11
            //
            this.LabelControl11.Location = new System.Drawing.Point(8, 52);
            this.LabelControl11.Name = "LabelControl11";
            this.LabelControl11.Size = new System.Drawing.Size(40, 14);
            this.LabelControl11.TabIndex = 10;
            this.LabelControl11.Text = "Address";
            //
            //label_email
            //
            this.label_email.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_email.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_email.Appearance.Options.UseBackColor = true;
            this.label_email.Location = new System.Drawing.Point(344, 52);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(232, 14);
            this.label_email.TabIndex = 9;
            //
            //label_fax
            //
            this.label_fax.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_fax.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_fax.Appearance.Options.UseBackColor = true;
            this.label_fax.Location = new System.Drawing.Point(344, 38);
            this.label_fax.Name = "label_fax";
            this.label_fax.Size = new System.Drawing.Size(232, 14);
            this.label_fax.TabIndex = 8;
            //
            //label_phone
            //
            this.label_phone.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_phone.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_phone.Appearance.Options.UseBackColor = true;
            this.label_phone.Location = new System.Drawing.Point(344, 24);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(232, 14);
            this.label_phone.TabIndex = 7;
            //
            //label_title
            //
            this.label_title.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_title.Appearance.Options.UseBackColor = true;
            this.label_title.Location = new System.Drawing.Point(56, 38);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(232, 14);
            this.label_title.TabIndex = 6;
            //
            //label_name
            //
            this.label_name.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(255), Convert.ToByte(192));
            this.label_name.Appearance.Options.UseBackColor = true;
            this.label_name.Location = new System.Drawing.Point(56, 24);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(232, 14);
            this.label_name.TabIndex = 5;
            //
            //LabelControl5
            //
            this.LabelControl5.Location = new System.Drawing.Point(304, 52);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(40, 14);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "E-Mail";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(304, 38);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(40, 14);
            this.LabelControl4.TabIndex = 3;
            this.LabelControl4.Text = "FAX";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(304, 24);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(40, 14);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Phone";
            //
            //labelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 38);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(40, 14);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Title";
            //
            //labelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(40, 14);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name";
            //
            //Page_Contacts
            //
            this.Controls.Add(this.GroupControl1);
            this.Name = "Page_Contacts";
            this.Controls.SetChildIndex(this.GroupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private DevExpress.XtraEditors.GroupControl GroupControl1;
        private DevExpress.XtraEditors.LabelControl label_address;
        private DevExpress.XtraEditors.LabelControl label_email;
        private DevExpress.XtraEditors.LabelControl label_fax;
        private DevExpress.XtraEditors.LabelControl label_name;
        private DevExpress.XtraEditors.LabelControl label_notes;
        private DevExpress.XtraEditors.LabelControl label_phone;
        private DevExpress.XtraEditors.LabelControl label_title;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl11;
        private DevExpress.XtraEditors.LabelControl LabelControl13;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_contact_type;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name_last;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name_suffix;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_title;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_phone_raw;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_phone_ext;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_fax_raw;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_line1;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_line2;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_city;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_state;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_postalcode;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_address_line3;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_email;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_notes;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_phone;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_fax;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_id;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_contact_type;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name_prefix;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name_first;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_name_middle;

        #endregion Windows Form Designer generated code

        /// <summary>
        /// Format a database field
        /// </summary>
        private class ClassFieldFormatter : IFormatProvider, ICustomFormatter
        {
            private System.Collections.Generic.List<creditor_contact> colRecords = null;
            private RetrieveDelegate retriever = null;

            public ClassFieldFormatter(System.Collections.Generic.List<creditor_contact> ColRecords, RetrieveDelegate retriever)
            {
                this.colRecords = ColRecords;
                this.retriever = retriever;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (arg != null)
                {
                    Int32 keyID = Convert.ToInt32(arg);
                    if (keyID >= 1 && colRecords != null)
                    {
                        var q = colRecords.Find(s => s.Id == keyID);
                        if (q != null)
                        {
                            return retriever(q);
                        }
                    }
                }

                return string.Empty;
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }

        /// <summary>
        /// Format a Contact entry record
        /// </summary>
        private class ClassFormatContactType : IFormatProvider, ICustomFormatter
        {
            private System.Collections.Generic.List<creditor_contact> colRecords = null;

            public ClassFormatContactType(System.Collections.Generic.List<creditor_contact> ColRecords)
            {
                this.colRecords = ColRecords;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                if (arg == null)
                {
                    return string.Empty;
                }

                int keyID = Convert.ToInt32(arg);
                if (keyID < 1)
                {
                    return string.Empty;
                }

                var q = DebtPlus.LINQ.Cache.creditor_contact_type.getList().Find(s => s.Id == keyID);
                if (q != null)
                {
                    if (! string.IsNullOrEmpty(q.description))
                    {
                        return q.description;
                    }
                    return q.name;
                }

                return string.Empty;
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
    }
}