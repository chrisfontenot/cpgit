#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Linq;
using System.Threading;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Controls;

namespace DebtPlus.UI.Creditor.Update.Pages
{
    internal partial class Page_Notes : MyGridControl
    {
        // List of the note records being displayed
        private System.Collections.Generic.List<MyNote> colRecords = null;

        // Current creditor record being edited
        private creditor creditorRecord = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize today new instance of the class
        /// </summary>
        public Page_Notes()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
            RegisterHandlers();
        }

        // Delegates for the ending thread procedures to update the grid list
        private delegate void RefreshAllNotesDelegate();

        private delegate void RefreshOneNoteDelegate(MyNote editNote);

        /// <summary>
        /// Do any custom formatting of fields for the current grid. We format the type to a string.
        /// </summary>
        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the record being displayed
            var record = GridView1.GetRow(e.RowHandle) as MyNote;
            if (record == null)
            {
                return;
            }

            // If the column is the note type then format it properly
            if (e.Column == GridColumn_type)
            {
                try
                {
                    var noteType = (DebtPlus.LINQ.InMemory.Notes.NoteTypes) record.type;
                    e.DisplayText = noteType.ToString();
                }
                catch
                {
                    e.DisplayText = "Unknown";
                }
            }
        }

        /// <summary>
        /// Load the list of notes when the control is first loaded
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;
            UnRegisterHandlers();
            try
            {
                RefreshList(getCheckedSelection());
                GridControl1.RefreshDataSource();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the create operation on today new note
        /// </summary>
        protected override void OnCreateRecord()
        {
            // Do the edit operation for the note
            Thread ithrd = new Thread(new System.Threading.ParameterizedThreadStart(CreateThreadProcedure))
                                            {
                                                Name = "CreateCreditorNote",
                                                IsBackground = false
                                            };

            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(null);
        }

        /// <summary>
        /// Process the delete operation on the note
        /// </summary>
        /// <param name="obj"></param>
        protected override void OnDeleteRecord(object obj)
        {
            MyNote record = obj as MyNote;
            if (record == null)
            {
                return;
            }

            // Do the edit operation for the note
            Thread ithrd = new Thread(new System.Threading.ParameterizedThreadStart(DeleteThreadProcedure))
                                            {
                                                Name = "DeleteCreditorNote",
                                                IsBackground = false
                                            };

            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(record);
        }

        /// <summary>
        /// Process the edit operation on today note
        /// </summary>
        /// <param name="obj"></param>
        protected override void OnEditRecord(object obj)
        {
            MyNote record = obj as MyNote;
            if (record == null)
            {
                return;
            }

            // Do the edit operation for the note
            Thread ithrd = new Thread(new System.Threading.ParameterizedThreadStart(EditThreadProcedure))
                                {
                                    Name = "EditCreditorNote",
                                    IsBackground = false
                                };

            ithrd.SetApartmentState(ApartmentState.STA);
            ithrd.Start(record);
        }

        /// <summary>
        /// Process today POPUP menu
        /// </summary>
        protected override void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            // Handle the standard logic for the popup event
            base.PopupMenu_Items_Popup(sender, e);

            // Change the name of the EDIT operation to SHOW if indicated
            BarButtonItem_Change.Caption = "&Change";
            BarButtonItem_Change.Description = "Change the item in the list";
        }

        /// <summary>
        /// Once the checkbox has been changed then refresh the list with the new status
        /// </summary>
        private void CheckEdit_CheckStateChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                RefreshList(getCheckedSelection());
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the changing status for the checkbox. Do not allow all boxes to be cleared.
        /// </summary>
        private void CheckEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // Always allow the item to be turned on.
            if (Convert.ToBoolean(e.NewValue))
            {
                return;
            }

            // Determine if there are more than two items checked. If so, allow it. (We need two since we are clearing one.)
            int ItemCount = 0;
            foreach (var ctl in new DevExpress.XtraEditors.CheckEdit[] { CheckEdit_Alerts, CheckEdit_Perm, CheckEdit_Research, CheckEdit_System, CheckEdit_System, CheckEdit_Temporary })
            {
                if (ctl.Checked)
                {
                    ItemCount += 1;
                }
            }

            // If there is only one item then do not allow it to be turned off.
            if (ItemCount < 2)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Do the create function as today separate thread
        /// </summary>
        private void CreateThreadProcedure(object obj)
        {
            using (var noteClass = new DebtPlus.Notes.NoteClass.CreditorNote())
            {
                if (noteClass.Create(creditorRecord.Id))
                {
                    RefreshAllNotes();
                }
            }
        }

        /// <summary>
        /// Edit the note buffer as today separate thread
        /// </summary>
        private void DeleteThreadProcedure(object obj)
        {
            var record = obj as MyNote;
            if (record == null)
            {
                return;
            }

            // Retrieve the current note from the system for the edit operation to start
            using (var noteClass = new DebtPlus.Notes.NoteClass.CreditorNote())
            {
                if (noteClass.Delete(record.Id))
                {
                    RefreshAllNotes();
                }
            }
        }

        /// <summary>
        /// Edit the note buffer as today separate thread
        /// </summary>
        private void EditThreadProcedure(object obj)
        {
            var record = obj as MyNote;
            if (record == null)
            {
                return;
            }

            // Retrieve the current note from the system for the edit operation to start
            using (var noteClass = new DebtPlus.Notes.NoteClass.CreditorNote())
            {
                if (noteClass.Edit(record.Id))
                {
                    RefreshOneNote(record);
                }
            }
        }

        /// <summary>
        /// Return the list of record types that we want to select.
        /// </summary>
        /// <returns></returns>
        private int[] getCheckedSelection()
        {
            // Collection of record types desired
            var colItems = new System.Collections.Generic.List<int>();

            // Find the checked items
            if (CheckEdit_Perm.Checked)
            {
                colItems.Add((int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);
            }

            if (CheckEdit_Temporary.Checked)
            {
                colItems.Add((int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary);
            }

            if (CheckEdit_System.Checked)
            {
                colItems.Add((int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System);
            }

            if (CheckEdit_Alerts.Checked)
            {
                colItems.Add((int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert);
            }

            if (CheckEdit_Research.Checked)
            {
                colItems.Add((int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Research);
            }

            // If there are no checked items or all of the items are checked then we
            // do not need to test for the type. Return NULL to the caller in this instance.
            if (colItems.Count == 0 || colItems.Count == 5)
            {
                return null;
            }

            // Return the partial list of checked items since we need to select only certain types.
            return colItems.ToArray();
        }

        /// <summary>
        /// Routine called at the end of the create and delete functions to reload the note list
        /// with the current values.
        /// </summary>
        private void RefreshAllNotes()
        {
            // Switch to the editing thread if needed
            if (GridControl1.InvokeRequired)
            {
                IAsyncResult ia = GridControl1.BeginInvoke(new RefreshAllNotesDelegate(RefreshAllNotes));
                GridControl1.EndInvoke(ia);
                return;
            }

            // Refresh the grid list with the note data
            UnRegisterHandlers();
            try
            {
                RefreshList(getCheckedSelection());
                GridControl1.RefreshDataSource();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Rebuild the list of items for the control
        /// </summary>
        /// <param name="SelectionClause"></param>
        private void RefreshList(int[] selectionClause)
        {
            try
            {
                var query = bc.creditor_notes.Where(n => n.creditor == creditorRecord.Id);
                if (selectionClause != null)
                {
                    query = query.Where(n => selectionClause.Contains(n.type));
                }

                colRecords              = query.Select(noteRecord => new MyNote(noteRecord)).ToList();
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();
            }

#pragma warning disable 168
            catch (DBConcurrencyException ex) { }
#pragma warning restore 168

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor notes table");
            }
        }

        /// <summary>
        /// Routine called at the end of the create and delete functions to reload the note list
        /// with the current values.
        /// </summary>
        private void RefreshOneNote(MyNote editNote)
        {
            // Switch to the editing thread if needed
            if (GridControl1.InvokeRequired)
            {
                IAsyncResult ia = GridControl1.BeginInvoke(new RefreshOneNoteDelegate(RefreshOneNote), new object[] { editNote });
                GridControl1.EndInvoke(ia);
                return;
            }

            // Refresh the grid list with the note data
            UnRegisterHandlers();
            try
            {
                // Locate the specific note that was changed
                var qNoteRecord = colRecords.Find(s => s.Id == editNote.Id);
                if (qNoteRecord == null)
                {
                    return;
                }

                var qNewItem = bc.creditor_notes.Where(n => n.Id == qNoteRecord.Id).Select(n => new MyNote(n)).FirstOrDefault();

                // If it is not found then we can't do anything about it now.
                if (qNewItem == null)
                {
                    return;
                }

                // Update the shadow copy of the information
                qNoteRecord.type         = qNewItem.type;
                qNoteRecord.subject      = qNewItem.subject;
                qNoteRecord.created_by   = qNewItem.created_by;
                qNoteRecord.date_created = qNewItem.date_created;

                // Find the relative note in the collection of notes
                int colIndex = colRecords.IndexOf(qNoteRecord);
                if (colIndex < 0)
                {
                    return;
                }

                // Find the relative rowhandle for this note in the grid display.
                int rowHandle = GridView1.GetRowHandle(colIndex);
                if (rowHandle < 0)
                {
                    return;
                }

                // Refresh the row associated with that rowhandle
                GridView1.RefreshRow(rowHandle);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            CheckEdit_Alerts.EditValueChanging    += CheckEdit_EditValueChanging;
            CheckEdit_Perm.EditValueChanging      += CheckEdit_EditValueChanging;
            CheckEdit_Research.EditValueChanging  += CheckEdit_EditValueChanging;
            CheckEdit_System.EditValueChanging    += CheckEdit_EditValueChanging;
            CheckEdit_Temporary.EditValueChanging += CheckEdit_EditValueChanging;
            CheckEdit_Alerts.CheckStateChanged    += CheckEdit_CheckStateChanged;
            CheckEdit_Perm.CheckStateChanged      += CheckEdit_CheckStateChanged;
            CheckEdit_Research.CheckStateChanged  += CheckEdit_CheckStateChanged;
            CheckEdit_System.CheckStateChanged    += CheckEdit_CheckStateChanged;
            CheckEdit_Temporary.CheckStateChanged += CheckEdit_CheckStateChanged;
            GridView1.CustomColumnDisplayText     += GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            CheckEdit_Alerts.EditValueChanging    -= CheckEdit_EditValueChanging;
            CheckEdit_Perm.EditValueChanging      -= CheckEdit_EditValueChanging;
            CheckEdit_Research.EditValueChanging  -= CheckEdit_EditValueChanging;
            CheckEdit_System.EditValueChanging    -= CheckEdit_EditValueChanging;
            CheckEdit_Temporary.EditValueChanging -= CheckEdit_EditValueChanging;
            CheckEdit_Alerts.CheckStateChanged    -= CheckEdit_CheckStateChanged;
            CheckEdit_Perm.CheckStateChanged      -= CheckEdit_CheckStateChanged;
            CheckEdit_Research.CheckStateChanged  -= CheckEdit_CheckStateChanged;
            CheckEdit_System.CheckStateChanged    -= CheckEdit_CheckStateChanged;
            CheckEdit_Temporary.CheckStateChanged -= CheckEdit_CheckStateChanged;
            GridView1.CustomColumnDisplayText     -= GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Local class to hold the notes for the display grid
        /// </summary>
        private class MyNote
        {
            public MyNote()
            {
            }

            public MyNote(creditor_note noteRecord) : this()
            {
                created_by   = noteRecord.created_by;
                date_created = noteRecord.date_created;
                Id           = noteRecord.Id;
                subject      = noteRecord.subject;
                type         = noteRecord.type;
                dont_delete  = noteRecord.dont_delete;
                dont_edit    = noteRecord.dont_edit;
                dont_print   = noteRecord.dont_print;
            }

            public string created_by { get; set; }
            public DateTime date_created { get; set; }
            public Int32 Id { get; set; }
            public string subject { get; set; }
            public Int32 type { get; set; }
            public bool dont_edit { get; set; }
            public bool dont_delete { get; set; }
            public bool dont_print { get; set; }

            public string counselor
            {
                get
                {
                    return DebtPlus.Utils.Format.Counselor.FormatCounselor(created_by);
                }
            }
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.CheckEdit CheckEdit_Alerts;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_Perm;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_Research;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_System;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_Temporary;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_edit;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_type;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_counselor;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_subject;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Id;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_delete;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_dont_print;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.GroupControl GroupControl1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.CheckEdit_Alerts = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Temporary = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Research = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_System = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Perm = new DevExpress.XtraEditors.CheckEdit();
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_subject = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_subject.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_dont_edit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_edit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_dont_delete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_delete.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_dont_print = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_dont_print.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Alerts.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Temporary.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Research.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_System.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Perm.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 48);
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(576, 248);
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_date_created,
				this.GridColumn_type,
				this.GridColumn_counselor,
				this.GridColumn_subject,
				this.GridColumn_Id,
				this.GridColumn_dont_edit,
				this.GridColumn_dont_delete,
				this.GridColumn_dont_print
			});
            this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(695, 473, 208, 170);
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.CheckEdit_Alerts);
            this.GroupControl1.Controls.Add(this.CheckEdit_Temporary);
            this.GroupControl1.Controls.Add(this.CheckEdit_Research);
            this.GroupControl1.Controls.Add(this.CheckEdit_System);
            this.GroupControl1.Controls.Add(this.CheckEdit_Perm);
            this.GroupControl1.Location = new System.Drawing.Point(0, 0);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(576, 48);
            this.GroupControl1.TabIndex = 1;
            this.GroupControl1.Text = "Show Notes of the following type(s)";
            //
            //CheckEdit_Alerts
            //
            this.CheckEdit_Alerts.EditValue = true;
            this.CheckEdit_Alerts.Location = new System.Drawing.Point(456, 24);
            this.CheckEdit_Alerts.Name = "CheckEdit_Alerts";
            //
            //CheckEdit_Alerts.Properties
            //
            this.CheckEdit_Alerts.Properties.Caption = "Alerts";
            this.CheckEdit_Alerts.Size = new System.Drawing.Size(80, 19);
            this.CheckEdit_Alerts.TabIndex = 4;
            this.CheckEdit_Alerts.Tag = "4";
            //
            //CheckEdit_Temporary
            //
            this.CheckEdit_Temporary.EditValue = true;
            this.CheckEdit_Temporary.Location = new System.Drawing.Point(352, 24);
            this.CheckEdit_Temporary.Name = "CheckEdit_Temporary";
            //
            //CheckEdit_Temporary.Properties
            //
            this.CheckEdit_Temporary.Properties.Caption = "Temporary";
            this.CheckEdit_Temporary.Size = new System.Drawing.Size(80, 19);
            this.CheckEdit_Temporary.TabIndex = 3;
            this.CheckEdit_Temporary.Tag = "2";
            //
            //CheckEdit_Research
            //
            this.CheckEdit_Research.EditValue = true;
            this.CheckEdit_Research.Location = new System.Drawing.Point(248, 24);
            this.CheckEdit_Research.Name = "CheckEdit_Research";
            //
            //CheckEdit_Research.Properties
            //
            this.CheckEdit_Research.Properties.Caption = "Research";
            this.CheckEdit_Research.Size = new System.Drawing.Size(80, 19);
            this.CheckEdit_Research.TabIndex = 2;
            this.CheckEdit_Research.Tag = "6";
            //
            //CheckEdit_System
            //
            this.CheckEdit_System.EditValue = true;
            this.CheckEdit_System.Location = new System.Drawing.Point(144, 24);
            this.CheckEdit_System.Name = "CheckEdit_System";
            //
            //CheckEdit_System.Properties
            //
            this.CheckEdit_System.Properties.Caption = "System";
            this.CheckEdit_System.Size = new System.Drawing.Size(80, 19);
            this.CheckEdit_System.TabIndex = 1;
            this.CheckEdit_System.Tag = "3";
            //
            //CheckEdit_Perm
            //
            this.CheckEdit_Perm.EditValue = true;
            this.CheckEdit_Perm.Location = new System.Drawing.Point(40, 24);
            this.CheckEdit_Perm.Name = "CheckEdit_Perm";
            //
            //CheckEdit_Perm.Properties
            //
            this.CheckEdit_Perm.Properties.Caption = "Permanent";
            this.CheckEdit_Perm.Size = new System.Drawing.Size(80, 19);
            this.CheckEdit_Perm.TabIndex = 0;
            this.CheckEdit_Perm.Tag = "1";
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Date";
            this.GridColumn_date_created.CustomizationCaption = "Date and Time";
            this.GridColumn_date_created.DisplayFormat.FormatString = "M/d/yyyy h:mm tt";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 0;
            this.GridColumn_date_created.Width = 135;
            //
            //GridColumn_type
            //
            this.GridColumn_type.Caption = "Type";
            this.GridColumn_type.CustomizationCaption = "Note Type";
            this.GridColumn_type.FieldName = "type";
            this.GridColumn_type.Name = "GridColumn_type";
            this.GridColumn_type.Visible = true;
            this.GridColumn_type.VisibleIndex = 1;
            this.GridColumn_type.Width = 93;
            //
            //GridColumn_counselor
            //
            this.GridColumn_counselor.Caption = "Counselor";
            this.GridColumn_counselor.CustomizationCaption = "Counselor";
            this.GridColumn_counselor.FieldName = "counselor";
            this.GridColumn_counselor.Name = "GridColumn_counselor";
            this.GridColumn_counselor.Visible = true;
            this.GridColumn_counselor.VisibleIndex = 2;
            this.GridColumn_counselor.Width = 108;
            //
            //GridColumn_subject
            //
            this.GridColumn_subject.Caption = "Subject";
            this.GridColumn_subject.CustomizationCaption = "Subject";
            this.GridColumn_subject.FieldName = "subject";
            this.GridColumn_subject.Name = "GridColumn_subject";
            this.GridColumn_subject.Visible = true;
            this.GridColumn_subject.VisibleIndex = 3;
            this.GridColumn_subject.Width = 263;
            //
            //GridColumn_Id
            //
            this.GridColumn_Id.Caption = "ID";
            this.GridColumn_Id.CustomizationCaption = "Record ID";
            this.GridColumn_Id.DisplayFormat.FormatString = "f0";
            this.GridColumn_Id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Id.FieldName = "Id";
            this.GridColumn_Id.Name = "GridColumn_Id";
            //
            //GridColumn_dont_edit
            //
            this.GridColumn_dont_edit.Caption = "Edit?";
            this.GridColumn_dont_edit.CustomizationCaption = "Allow Edit?";
            this.GridColumn_dont_edit.FieldName = "dont_edit";
            this.GridColumn_dont_edit.Name = "GridColumn_dont_edit";
            //
            //GridColumn_dont_delete
            //
            this.GridColumn_dont_delete.Caption = "Delete?";
            this.GridColumn_dont_delete.CustomizationCaption = "Allow Delete?";
            this.GridColumn_dont_delete.FieldName = "dont_delete";
            this.GridColumn_dont_delete.Name = "GridColumn_dont_delete";
            //
            //GridColumn_dont_print
            //
            this.GridColumn_dont_print.Caption = "Print?";
            this.GridColumn_dont_print.CustomizationCaption = "Allow Print?";
            this.GridColumn_dont_print.FieldName = "dont_print";
            this.GridColumn_dont_print.Name = "GridColumn_dont_print";
            //
            //Page_Notes
            //
            this.Controls.Add(this.GroupControl1);
            this.Name = "Page_Notes";
            this.Controls.SetChildIndex(this.GroupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Alerts.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Temporary.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Research.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_System.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Perm.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code
    }
}