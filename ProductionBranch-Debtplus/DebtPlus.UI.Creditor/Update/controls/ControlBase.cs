namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal class ControlBase : DevExpress.XtraEditors.XtraUserControl
    {
        public ControlBase() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Read the initial record at the time of creation
        /// </summary>
        /// <param name="bc">Pointer to the database context</param>
        /// <param name="CreditorRecord">Pointer to the current creditor record being edited</param>
        public virtual void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
        }

        /// <summary>
        /// Refresh the data when the page is selected
        /// </summary>
        /// <param name="CreditorRecord">Pointer to the current creditor record being edited</param>
        public virtual void RefreshForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
        }

        /// <summary>
        /// Save the form contents when the page is no longer active
        /// </summary>
        public virtual void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
        }

        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }

            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            //
            //ControlBase
            //
            this.Name = "ControlBase";
            this.Size = new System.Drawing.Size(576, 296);
        }
    }
}