#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class DateRangeList : ComboBoxEdit
    {
        public DateRangeList()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Description("Get the ending date for the date range"), Browsable(false)]
        public System.DateTime EndingDate
        {
            get
            {
                System.DateTime result = new System.DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                switch (Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)Properties.Items[SelectedIndex]).value))
                {
                    case 0:
                        // all to date
                        break;

                    case 1:
                        // current month
                        result = new System.DateTime(result.Year, result.Month, 1).AddMonths(1).AddDays(-1);
                        break;

                    case 3:
                        // month to date
                        break;

                    case 2:
                        // current year
                        result = new System.DateTime(result.Year, 12, 31);
                        break;

                    case 4:
                        // year to date
                        break;

                    case 5:
                        // last 30 days
                        break;

                    case 6:
                        // last 3 months
                        break;

                    case 7:
                        // last 6 months
                        break;

                    case 8:
                        // last 12 months
                        break;

                    case 9:
                        // Previous month
                        result = new System.DateTime(result.Year, result.Month, 1).AddDays(-1);
                        break;

                    case 10:
                        // previous year
                        result = new System.DateTime(result.Year - 1, 12, 31);
                        break;
                }

                return result.AddSeconds((24.0 * 60.0 * 60.0) - 1.0);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Description("Get the starting date for the date range"), Browsable(false)]
        public System.DateTime StartingDate
        {
            get
            {
                System.DateTime result = DateTime.Now;

                switch (Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)Properties.Items[SelectedIndex]).value))
                {
                    case 0:
                        // all to date
                        result = new System.DateTime(1950, 1, 1);
                        break;

                    case 1:
                    case 3:
                        // current month, month to date
                        result = new System.DateTime(result.Year, result.Month, 1);
                        break;

                    case 2:
                    case 4:
                        // current year, year to date
                        result = new System.DateTime(result.Year, 1, 1);
                        break;

                    case 5:
                        // last 30 days
                        result = result.AddDays(-30);
                        break;

                    case 6:
                        // last 3 months
                        result = result.AddMonths(-3);
                        break;

                    case 7:
                        // last 6 months
                        result = result.AddMonths(-6);
                        break;

                    case 8:
                        // last 12 months
                        result = result.AddMonths(-12);
                        break;

                    case 9:
                        // Previous month
                        result = new System.DateTime(result.Year, result.Month, 1).AddMonths(-1);
                        break;

                    case 10:
                        // previous year
                        result = new System.DateTime(result.Year - 1, 1, 1);
                        break;
                }

                return new System.DateTime(result.Year, result.Month, result.Day);
            }
        }

        protected override void OnLoaded()
        {
            UnRegisterHandlers();
            try
            {
                base.OnLoaded();
                if (!IsLoading && !IsDesignMode)
                {
                    // Add the list of items to the collection
                    Properties.TextEditStyle = TextEditStyles.DisableTextEditor;
                    Properties.AllowNullInput = DefaultBoolean.False;

                    // Ensure that there are items when the control is loaded.
                    var itemList = Properties.Items;
                    if (itemList.Count == 0)
                    {
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("All To Date", 0, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Current Month", 1, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Current Year", 2, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Month to date", 3, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Year to date", 4, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 30 days", 5, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 3 months", 6, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 6 months", 7, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 12 months", 8, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Previous month", 9, true));
                        itemList.Add(new DebtPlus.Data.Controls.ComboboxItem("Previous year", 10, true));
                    }

                    try
                    {
                        SelectedIndex = DebtPlus.Configuration.Config.DateSelection;
                    }
#pragma warning disable 168
                    catch (System.Exception ex) { }
#pragma warning restore 168

                    // If there is no selection because it was invalid then change it to "current month"
                    if (SelectedIndex < 0)
                    {
                        SelectedIndex = 3;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void OnSelectedValueChanged(object sender, EventArgs e)
        {
            DebtPlus.Configuration.Config.DateSelection = SelectedIndex;
        }

        private void RegisterHandlers()
        {
            base.SelectedValueChanged += OnSelectedValueChanged;
        }

        private void UnRegisterHandlers()
        {
            base.SelectedValueChanged -= OnSelectedValueChanged;
        }

        #region " Windows Form Designer generated code "

        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion " Windows Form Designer generated code "
    }
}