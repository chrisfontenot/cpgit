#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class BillerMasksControl : MyGridControl
    {
        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public BillerMasksControl()
            : base()
        {
            InitializeComponent();
            EnableMenus = false;
        }

        /// <summary>
        /// Read the data from the system once we have today load event on the main form
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            // List of the RPPS banks
            System.Collections.Generic.List<bank> colBanks = DebtPlus.LINQ.Cache.bank.getList().Where(s => s.type == "R").ToList();

            // Find a list of the corresponding RPPS biller IDs that we want from the database
            string[] aryBillerIDs = CreditorRecord.creditor_methods
                                                .Where(s => s.type == "EFT")
                                                .Join(colBanks, cm => cm.bank, b => b.Id, (cm, b) => cm.rpps_biller_id)
                                                .ToArray();

            // Load the list of masks if there are any masks to be defined.
            if (aryBillerIDs.GetUpperBound(0) >= 0)
            {
                GridControl1.DataSource = bc.rpps_masks.Where(mk => aryBillerIDs.Contains(mk.rpps_biller_id)).ToList();
                return;
            }
            GridControl1.DataSource = null;
        }
    }
}