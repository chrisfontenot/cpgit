#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class MyCreditorContributionPcts : MyGridControl
    {
        // Current creditor record being edited
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        public MyCreditorContributionPcts()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
            RegisterHandlers();
        }

        /// <summary>
        /// Is the control for display purposes only?
        /// </summary>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Read the form information
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord     = CreditorRecord;
            this.bc                 = bc;

            GridControl1.DataSource = creditorRecord.creditor_contribution_pcts.GetNewBindingList();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Determine if we are allowed to create today new record
        /// </summary>
        protected override bool OkToCreateRecord()
        {
            return (!ReadOnly) && base.OkToCreateRecord();
        }

        /// <summary>
        /// Determine if we are allowed to delete today record
        /// </summary>
        protected override bool OkToDeleteRecord(object obj)
        {
            return (!ReadOnly) && base.OkToDeleteRecord(obj);
        }

        /// <summary>
        /// Determine if we are allowed to edit today record
        /// </summary>
        protected override bool OkToEditRecord(object obj)
        {
            return (!ReadOnly) && base.OkToEditRecord(obj);
        }

        /// <summary>
        /// Process an item creation
        /// </summary>
        protected override void OnCreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_creditor_contribution_pct(creditorRecord.Id);
            using (var frm = new Forms.Form_CreditorContributionPCT(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the database
            creditorRecord.creditor_contribution_pcts.Add(record);
            bc.SubmitChanges();

            // Update the list with the new record
            GridControl1.DataSource = creditorRecord.creditor_contribution_pcts.GetNewBindingList();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Process an item deletion
        /// </summary>
        protected override void OnDeleteRecord(object obj)
        {
            // Find the record to be deleted
            var record = obj as creditor_contribution_pct;
            if (record == null)
            {
                return;
            }

            // Ask if the deletion is to be performed
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            // Delete the current record
            creditorRecord.creditor_contribution_pcts.Remove(record);
            bc.SubmitChanges();

            // Update the list with the new record
            GridControl1.DataSource = creditorRecord.creditor_contribution_pcts.GetNewBindingList();
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Process an item edit
        /// </summary>
        protected override void OnEditRecord(object obj)
        {
            // Double-click will still call us so prevent the edit operation now
            if (!OkToEditRecord(obj))
            {
                return;
            }

            // Find the record to be deleted
            var record = obj as creditor_contribution_pct;
            if (record == null)
            {
                return;
            }

            // Do the edit operation
            using (var frm = new Forms.Form_CreditorContributionPCT(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            bc.SubmitChanges();

            GridControl1.DataSource = creditorRecord.creditor_contribution_pcts.GetNewBindingList();
            GridControl1.RefreshDataSource();
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region " Windows Form Designer generated code "

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Check_PCT;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_effective;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_EFT_pct;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_status;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Updated;
        private DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridColumn_Updated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Updated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_effective = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_effective.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_EFT_pct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_EFT_pct.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Check_PCT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Check_PCT.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Size = new System.Drawing.Size(248, 96);
            this.GridControl1.TabIndex = 8;
            //
            //GridView1
            //
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_effective,
				this.GridColumn_status,
				this.GridColumn_EFT_pct,
				this.GridColumn_Check_PCT,
				this.GridColumn_created_by,
				this.GridColumn_date_created,
				this.GridColumn_Updated
			});
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_effective, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GridColumn_Updated
            //
            this.GridColumn_Updated.Caption = "Date Changed";
            this.GridColumn_Updated.CustomizationCaption = "Date when item was put into effect";
            this.GridColumn_Updated.DisplayFormat.FormatString = "d";
            this.GridColumn_Updated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Updated.FieldName = "date_updated";
            this.GridColumn_Updated.GroupFormat.FormatString = "d";
            this.GridColumn_Updated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Updated.Name = "GridColumn_Updated";
            //
            //GridColumn_effective
            //
            this.GridColumn_effective.Caption = "Effective";
            this.GridColumn_effective.CustomizationCaption = "Effective Date";
            this.GridColumn_effective.DisplayFormat.FormatString = "d";
            this.GridColumn_effective.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective.FieldName = "effective_date";
            this.GridColumn_effective.GroupFormat.FormatString = "d";
            this.GridColumn_effective.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective.Name = "GridColumn_effective";
            this.GridColumn_effective.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_effective.Visible = true;
            this.GridColumn_effective.VisibleIndex = 0;
            this.GridColumn_effective.Width = 77;
            //
            //GridColumn_status
            //
            this.GridColumn_status.Caption = "Status";
            this.GridColumn_status.CustomizationCaption = "Contribution Type (Bill/Deduct/None)";
            this.GridColumn_status.FieldName = "status";
            this.GridColumn_status.Name = "GridColumn_status";
            this.GridColumn_status.Visible = true;
            this.GridColumn_status.VisibleIndex = 1;
            this.GridColumn_status.Width = 51;
            //
            //GridColumn_EFT_pct
            //
            this.GridColumn_EFT_pct.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_EFT_pct.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_EFT_pct.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_EFT_pct.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_EFT_pct.Caption = "% EFT";
            this.GridColumn_EFT_pct.CustomizationCaption = "EFT Percentage";
            this.GridColumn_EFT_pct.DisplayFormat.FormatString = "p";
            this.GridColumn_EFT_pct.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_EFT_pct.FieldName = "fairshare_pct_eft";
            this.GridColumn_EFT_pct.GroupFormat.FormatString = "p";
            this.GridColumn_EFT_pct.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_EFT_pct.Name = "GridColumn_EFT_pct";
            this.GridColumn_EFT_pct.Visible = true;
            this.GridColumn_EFT_pct.VisibleIndex = 2;
            this.GridColumn_EFT_pct.Width = 52;
            //
            //GridColumn_Check_PCT
            //
            this.GridColumn_Check_PCT.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Check_PCT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Check_PCT.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Check_PCT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Check_PCT.Caption = "% Check";
            this.GridColumn_Check_PCT.CustomizationCaption = "Check Percentage";
            this.GridColumn_Check_PCT.DisplayFormat.FormatString = "p";
            this.GridColumn_Check_PCT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Check_PCT.FieldName = "fairshare_pct_check";
            this.GridColumn_Check_PCT.GroupFormat.FormatString = "p";
            this.GridColumn_Check_PCT.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Check_PCT.Name = "GridColumn_Check_PCT";
            this.GridColumn_Check_PCT.Visible = true;
            this.GridColumn_Check_PCT.VisibleIndex = 3;
            this.GridColumn_Check_PCT.Width = 64;
            //
            //GridColumn_created_by
            //
            this.GridColumn_created_by.Caption = "Creator";
            this.GridColumn_created_by.CustomizationCaption = "Person who created the item";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Date Added";
            this.GridColumn_date_created.CustomizationCaption = "Date added to system";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            //
            //MyCreditorContributionPcts
            //
            this.Name = "MyCreditorContributionPcts";
            this.Size = new System.Drawing.Size(248, 96);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "
    }
}
