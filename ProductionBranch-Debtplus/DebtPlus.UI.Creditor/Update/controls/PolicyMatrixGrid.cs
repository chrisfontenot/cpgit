#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class PolicyMatrixGrid : MyGridControl
    {
        // List of the records to be displayed in this control
        private System.Collections.Generic.List<policy_matrix_policy> colRecords;

        // Current creditor being updated
        private creditor creditorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public PolicyMatrixGrid()
            : base()
        {
            InitializeComponent();
            gridColumn_description.DisplayFormat.Format = new description_formtter();
            gridColumn_description.GroupFormat.Format = new description_formtter();
            RegisterHandlers();
        }

        /// <summary>
        /// Handle the ShowMessage event
        /// </summary>
        /// <param name="Sender">Sender of the message</param>
        /// <param name="Message">Message to be displayed</param>
        public delegate void ShowMessageEventHandler(object Sender, string Message);

        /// <summary>
        /// Generated when the user selected today policy
        /// </summary>
        public event ShowMessageEventHandler ShowMessage;

        /// <summary>
        /// Read the creditor information when the control is created
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;

            UnRegisterHandlers();
            try
            {
                colRecords = bc.policy_matrix_policies.Where(s => s.creditor == CreditorRecord.Id).ToList();
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading policy matrix");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Is is legal to create today row for this table at this time?
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected override bool OkToCreateRecord()
        {
            return base.OkToCreateRecord() && DebtPlus.Configuration.Config.UpdateCreditorPolicy;
        }

        /// <summary>
        /// Is it legal to delete this row on the table?
        /// </summary>
        /// <param name="obj">Record to be deleted</param>
        /// <returns></returns>
        protected override bool OkToDeleteRecord(object obj)
        {
            return base.OkToDeleteRecord(obj) && DebtPlus.Configuration.Config.UpdateCreditorPolicy;
        }

        /// <summary>
        /// Is it legal to update the record for this table?
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected override bool OkToEditRecord(object obj)
        {
            return base.OkToEditRecord(obj) && DebtPlus.Configuration.Config.UpdateCreditorPolicy;
        }

        /// <summary>
        /// Handle the update sequence when the record is changed
        /// </summary>
        /// <param name="obj">Record to be edited</param>
        protected override void OnEditRecord(object obj)
        {
            // We need to have today valid record.
            var record = obj as policy_matrix_policy;
            if (record == null)
            {
                return;
            }

            // Do the form update to change the policy data
            using (var frm = new Forms.Form_PolicyMatrixCreditorUpdate(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                // If the user does not permission to update the policies then ignore the event
                if (!DebtPlus.Configuration.Config.UpdateCreditorPolicy)
                {
                    return;
                }

                try
                {
                    // Do the database update for the current row
                    bc.xpr_insert_policy_matrix_policies(record, frm.CopyToSIC);

                    // Correct the record with the changed information
                    bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record);
                    GridView1.RefreshData();

                    // Display the message information
                    OnShowMessage(record.message);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating creditor policy matrix");
                }
            }
        }

        protected override void OnSelectRecord(object obj)
        {
            var record = obj as policy_matrix_policy;
            if (record == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(record.message))
            {
                OnShowMessage(record.message);
            }
        }

        /// <summary>
        /// Raise the ShowMessage event
        /// </summary>
        /// <param name="Message"></param>
        protected virtual void OnShowMessage(string Message)
        {
            RaiseShowMessage(Message);
        }

        /// <summary>
        /// Raise the ShowMesage event
        /// </summary>
        /// <param name="Message"></param>
        protected void RaiseShowMessage(string Message)
        {
            var evt = ShowMessage;
            if (evt != null)
            {
                evt(this, Message);
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// Class to format the description column
        /// </summary>
        private class description_formtter : System.ICustomFormatter, System.IFormatProvider
        {
            public string Format(string format, object arg, System.IFormatProvider formatProvider)
            {
                if (arg != null)
                {
                    string argKey = System.Convert.ToString(arg);
                    var q = DebtPlus.LINQ.Cache.policy_matrix_policy_type.getList().Find(s => string.Compare(s.Id, argKey, true) == 0);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }

            public object GetFormat(System.Type formatType)
            {
                return this;
            }
        }
    }
}
