using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class AddressInformation : ControlBase
    {
        // Current creditor being updated
        private creditor creditorRecord = null;

        private DebtPlus.LINQ.BusinessContext bc = null;

        // Read-only status
        private bool privateReadOnly = false;

        // Current record for the address associated with the control.
        private creditor_address record = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public AddressInformation()
            : base()
        {
            InitializeComponent();
            AddressType = DebtPlus.LINQ.InMemory.creditorAddressTypes.PaymentTypeKey;
            ReadOnly = false;
            label_Address.Text = string.Empty;

            RegisterHandlers();
        }

        /// <summary>
        /// Type of the address entry
        /// </summary>
        [Description("Set or Get the type of the address block for the creditor"), Category("DebtPlus"), DefaultValue("P")]
        public string AddressType { get; set; }

        /// <summary>
        /// Caption for the button
        /// </summary>
        [Description("Get or Set the text for the update button"), Category("DebtPlus"), DefaultValue("")]
        public string Caption
        {
            get
            {
                return Button_Update.Text;
            }
            set
            {
                Button_Update.Text = value;
            }
        }

        /// <summary>
        /// Read-only status for the control
        /// </summary>
        public bool ReadOnly
        {
            get
            {
                return privateReadOnly;
            }
            set
            {
                privateReadOnly = value;
                Button_Update.Enabled = !ReadOnly;
            }
        }

        /// <summary>
        /// Read the address information associated with this type
        /// </summary>
        public override void ReadForm(BusinessContext bc, creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc = bc;
            Button_Update.Enabled = !ReadOnly;

            UnRegisterHandlers();
            try
            {
                var addressRecord = bc.creditor_addresses.Where(s => s.creditor == creditorRecord.Id && s.type == AddressType).FirstOrDefault();
                RefreshData(addressRecord);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address entry");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the update event on the button click
        /// </summary>
        private void Button_Update_Click(object sender, EventArgs e)
        {
            // Ignore the button event if it was read-only
            if (ReadOnly)
            {
                return;
            }

            // Retrieve the current record to be edited
            if (record == null || record.Id < 1)
            {
                createNewRecord();
            }
            else
            {
                editRecord(record);
            }
        }

        /// <summary>
        /// Create today new address record
        /// </summary>
        private void createNewRecord()
        {
            var addressRecord = DebtPlus.LINQ.Factory.Manufacture_creditor_address(creditorRecord.Id, AddressType);
            using (var frm = new Forms.Form_NewAddress(bc, addressRecord))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Insert the new record
            try
            {
                bc.creditor_addresses.InsertOnSubmit(addressRecord);
                bc.SubmitChanges();
                RefreshData(addressRecord);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating creditor address data");
            }
        }

        /// <summary>
        /// Delete the current record
        /// </summary>
        /// <param name="AddressRecord">Creditor Address record to be used</param>
        private void deleteRecord(DebtPlus.LINQ.creditor_address addressRecord)
        {
            try
            {
                var q = bc.creditor_addresses.Where(s => s.Id == addressRecord.Id).FirstOrDefault();
                if (q != null)
                {
                    bc.creditor_addresses.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    RefreshData(null);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting creditor address");
            }
        }

        /// <summary>
        /// Edit the current address record
        /// </summary>
        /// <param name="AddressRecord">Creditor Address record to be used</param>
        private void editRecord(DebtPlus.LINQ.creditor_address AddressRecord)
        {
            // Edit the address record
            using (var frm = new Forms.Form_NewAddress(bc, AddressRecord))
            {
                // Look at the response. It determines what we do with the record.
                switch (frm.ShowDialog())
                {
                    case DialogResult.No:   // This is the "Delete" button event.
                        deleteRecord(AddressRecord);
                        return;

                    case DialogResult.OK:   // This is the "OK" button event.
                        break;

                    default:                // The only other one should be CANCEL.
                        return;
                }
            }

            try
            {
                // Update the record in the tables
                var q = bc.creditor_addresses.Where(s => s.Id == AddressRecord.Id).FirstOrDefault();
                if (q != null)
                {
                    q.attn = AddressRecord.attn;
                    q.AddressID = AddressRecord.AddressID;
                    bc.SubmitChanges();
                }

                RefreshData(AddressRecord);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating creditor address table");
            }
        }

        /// <summary>
        /// Refresh the address record display
        /// </summary>
        private void RefreshData(creditor_address AddressRecord)
        {
            // Save the record for later references
            record = AddressRecord;

            // If there is no record then the address text is blank
            if (AddressRecord == null)
            {
                label_Address.Text = string.Empty;
                return;
            }

            // There must be an associated address to continue
            if (AddressRecord.AddressID.HasValue)
            {
                // Do not use "bc" here as that caches the previous address and since the control changes the address
                // behinds the "scenes" so to speak, we need to force the system to fetch a new copy of the address
                // from the database. So, we need a new database context to do that.
                using (var adrBc = new BusinessContext())
                {
                    // Retrieve the address entry from the database
                    var adr = adrBc.addresses.Where(s => s.Id == AddressRecord.AddressID.Value).Select(s => new DebtPlus.LINQ.CreditorAddressEntry(s, record.attn)).FirstOrDefault();
                    if (adr != null)
                    {
                        label_Address.Text = adr.ToString();
                        return;
                    }
                }
            }

            // Default the address text to just the attention line
            label_Address.Text = AddressRecord.attn;
        }

        /// <summary>
        /// Add the event handler registrations
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Update.Click += Button_Update_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Update.Click -= Button_Update_Click;
        }

        private DevExpress.XtraEditors.SimpleButton Button_Update;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.MemoEdit label_Address;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Button_Update = new DevExpress.XtraEditors.SimpleButton();
            this.label_Address = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)this.label_Address.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Button_Update
            //
            this.Button_Update.AllowFocus = false;
            this.Button_Update.CausesValidation = false;
            this.Button_Update.Enabled = false;
            this.Button_Update.Location = new System.Drawing.Point(0, 0);
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.Size = new System.Drawing.Size(75, 23);
            this.Button_Update.TabIndex = 1;
            this.Button_Update.TabStop = false;
            this.Button_Update.Text = "Update...";
            this.Button_Update.ToolTip = "Click here to update the address information.";
            //
            //label_Address
            //
            this.label_Address.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_Address.Location = new System.Drawing.Point(88, 0);
            this.label_Address.Name = "label_Address";
            this.label_Address.Properties.AcceptsReturn = false;
            this.label_Address.Properties.AllowFocused = false;
            this.label_Address.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.label_Address.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.label_Address.Properties.Appearance.Options.UseBackColor = true;
            this.label_Address.Properties.ReadOnly = true;
            this.label_Address.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.label_Address.Properties.WordWrap = false;
            this.label_Address.Size = new System.Drawing.Size(269, 56);
            this.label_Address.TabIndex = 0;
            this.label_Address.TabStop = false;
            //
            //AddressInformation
            //
            this.Controls.Add(this.Button_Update);
            this.Controls.Add(this.label_Address);
            this.Name = "AddressInformation";
            this.Size = new System.Drawing.Size(360, 56);
            ((System.ComponentModel.ISupportInitialize)this.label_Address.Properties).EndInit();
            this.ResumeLayout(false);
        }
    }
}