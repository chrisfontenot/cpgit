#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Creditor.Update.Forms;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class MyEFTLocation
    {
        // Current creditor record being edited
        private creditor creditorRecord          = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public MyEFTLocation()
            : base()
        {
            InitializeComponent();

            Type = "EFT";
            EnableMenus = true;

            RegisterHandlers();
        }

        /// <summary>
        /// Format a custom cell text. We don't want to use the format provider because there is no
        /// context with the format provider. We need the row and the row is not defined until the
        /// database is updated, sometime later.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the row. If no row then there is no text.
            var methodRow = GridView1.GetRow(e.RowHandle) as creditor_method;
            if (methodRow == null)
            {
                e.DisplayText = e.Value.ToString();
                return;
            }

            // If the item is the biller ID then we want to change to the proper biller ID.
            if (e.Column == GridColumn_formatted_biller_id)
            {
                if (methodRow.bank > 0)
                {
                    var bankRecord = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == methodRow.bank);
                    if (bankRecord != null)
                    {
                        if (bankRecord.type == "R")
                        {
                            e.DisplayText = methodRow.rpps_biller_id;
                            return;
                        }
                    }
                }
                e.DisplayText = string.Empty;
                return;
            }

            // Do the region ID name
            if (e.Column == GridColumn_formatted_region)
            {
                if (methodRow.region == 0)
                {
                    e.DisplayText = "ALL";
                    return;
                }

                if (methodRow.region > 0)
                {
                    var regionRecord = DebtPlus.LINQ.Cache.region.getList().Find(s => s.Id == methodRow.region);
                    if (regionRecord != null)
                    {
                        e.DisplayText = regionRecord.description;
                        return;
                    }
                }
                e.DisplayText = string.Empty;
                return;
            }

            // Do the bank name
            if (e.Column == GridColumn_formatted_bank)
            {
                if (methodRow.bank > 0)
                {
                    var bankRecord = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == methodRow.bank);
                    if (bankRecord != null)
                    {
                        if (bankRecord.type == "C" && Type != "EFT")
                        {
                            e.DisplayText = "Printed";
                            return;
                        }

                        e.DisplayText = bankRecord.description;
                        return;
                    }
                }
                e.DisplayText = string.Empty;
                return;
            }

            // Finally, just use the current value for the item
            e.DisplayText = e.Value.ToString();
        }

        /// <summary>
        /// Type of the item to be processed.
        /// </summary>
        [Description("Get or Set the type of the record."), DefaultValue("EFT"), Browsable(true), Category("DebtPlus")]
        public string Type { get; set; }

        /// <summary>
        /// Process the initial form "load" event. We don't load these controls so there is no
        /// "load" event but we simulate it here.
        /// </summary>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;
            this.bc             = bc;
            UnRegisterHandlers();
            RegisterHandlers();

            GridControl1.DataSource = creditorRecord.creditor_methods.Where(s => s.type == Type).ToList();
        }

        /// <summary>
        /// Create today new record
        /// </summary>
        protected override void OnCreateRecord()
        {
            // Create today new record of the desired type
            creditor_method record = DebtPlus.LINQ.Factory.Manufacture_creditor_method(creditorRecord.creditor_id, Type);
            using (var frm = new Form_EFTLocationEdit(record) { CreditorName = creditorRecord.creditor_name })
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Update the record with the new information
            creditorRecord.creditor_methods.Add(record);
            bc.SubmitChanges();

            // Rebind the list of creditor methods to the grid control
            GridControl1.DataSource = creditorRecord.creditor_methods.Where(s => s.type == Type).ToList();
            GridControl1.RefreshDataSource();

            // Notify the user of the change to the list
            OnListChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Delete the indicated record
        /// </summary>
        /// <param name="obj">Record to be deleted</param>
        protected override void OnDeleteRecord(object obj)
        {
            // The record must exist
            creditor_method record = obj as creditor_method;
            if (record == null)
            {
                return;
            }

            // Verify the delete
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            // Delete the creditor method from the list
            creditorRecord.creditor_methods.Remove(record);
            bc.SubmitChanges();

            // Rebind the list of creditor methods to the grid control
            GridControl1.DataSource = creditorRecord.creditor_methods.Where(s => s.type == Type).ToList();
            GridControl1.RefreshDataSource();

            // Notify the user of the change to the list
            OnListChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Edit the indicated record
        /// </summary>
        /// <param name="obj">Record to be changed</param>
        protected override void OnEditRecord(object obj)
        {
            // The record must exist
            creditor_method record = obj as creditor_method;
            if (record == null)
            {
                return;
            }

            // Edit the record
            using (var frm = new Form_EFTLocationEdit(record) { CreditorName = creditorRecord.creditor_name })
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Update the database with the changes
            bc.SubmitChanges();

            // Rebind the list of creditor methods to the grid control
            GridControl1.DataSource = creditorRecord.creditor_methods.Where(s => s.type == Type).ToList();
            GridControl1.RefreshDataSource();

            // Notify the user of the change to the list
            OnListChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Register event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            GridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Remove event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            GridView1.CustomColumnDisplayText -= GridView1_CustomColumnDisplayText;
        }

        #region ListChanged

        /// <summary>
        /// Indicate that we changed the list of methods
        /// </summary>
        public event EventHandler ListChanged;

        /// <summary>
        /// Raise the ListChanged event
        /// </summary>
        /// <param name="e">Arguments for the event</param>
        protected virtual void OnListChanged(EventArgs e)
        {
            RaiseListChanged(e);
        }

        /// <summary>
        /// Raise the ListChanged event
        /// </summary>
        /// <param name="e">Arguments for the event</param>
        protected void RaiseListChanged(EventArgs e)
        {
            var evt = ListChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        #endregion ListChanged
    }
}
