namespace DebtPlus.UI.Creditor.Update.Controls
{
    partial class MyEFTLocation : MyGridControl
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridColumn_Creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_region = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_bank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_rpps_biller_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_bank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_biller_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_region = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_method = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 296);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(576, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 296);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(576, 0);
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(190)))), ((int)(((byte)(243)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(109)))), ((int)(((byte)(185)))));
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.GridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseFont = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.GridView1.Appearance.Preview.Options.UseBackColor = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_creditor_method,
            this.GridColumn_Creditor,
            this.GridColumn_region,
            this.GridColumn_bank,
            this.GridColumn_rpps_biller_id,
            this.GridColumn_created_by,
            this.GridColumn_date_created,
            this.GridColumn_formatted_bank,
            this.GridColumn_formatted_biller_id,
            this.GridColumn_formatted_region});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_formatted_biller_id, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // GridColumn_Creditor
            // 
            this.GridColumn_Creditor.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Creditor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Creditor.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Creditor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Creditor.Caption = "CreditorID";
            this.GridColumn_Creditor.CustomizationCaption = "Creditor Number";
            this.GridColumn_Creditor.FieldName = "creditor";
            this.GridColumn_Creditor.Name = "GridColumn_Creditor";
            this.GridColumn_Creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            // 
            // GridColumn_region
            // 
            this.GridColumn_region.Caption = "Region ID";
            this.GridColumn_region.CustomizationCaption = "Region ID associated with record";
            this.GridColumn_region.FieldName = "region";
            this.GridColumn_region.Name = "GridColumn_region";
            this.GridColumn_region.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_region.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            // 
            // GridColumn_bank
            // 
            this.GridColumn_bank.Caption = "Bank";
            this.GridColumn_bank.CustomizationCaption = "Bank #";
            this.GridColumn_bank.FieldName = "bank";
            this.GridColumn_bank.Name = "GridColumn_bank";
            this.GridColumn_bank.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_bank.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            // 
            // GridColumn_rpps_biller_id
            // 
            this.GridColumn_rpps_biller_id.Caption = "RPPS Biller";
            this.GridColumn_rpps_biller_id.CustomizationCaption = "RPPS Biller";
            this.GridColumn_rpps_biller_id.FieldName = "rpps_biller_id";
            this.GridColumn_rpps_biller_id.Name = "GridColumn_rpps_biller_id";
            this.GridColumn_rpps_biller_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_rpps_biller_id.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            // 
            // GridColumn_created_by
            // 
            this.GridColumn_created_by.Caption = "Creator";
            this.GridColumn_created_by.CustomizationCaption = "Person who created the record";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_created_by.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            // 
            // GridColumn_date_created
            // 
            this.GridColumn_date_created.Caption = "Date Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Item Created";
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            // 
            // GridColumn_formatted_bank
            // 
            this.GridColumn_formatted_bank.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_formatted_bank.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_bank.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_formatted_bank.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_bank.Caption = "Bank";
            this.GridColumn_formatted_bank.CustomizationCaption = "Bank Label";
            this.GridColumn_formatted_bank.FieldName = "bank";
            this.GridColumn_formatted_bank.Name = "GridColumn_formatted_bank";
            this.GridColumn_formatted_bank.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_bank.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_bank.UnboundExpression = "True";
            this.GridColumn_formatted_bank.Visible = true;
            this.GridColumn_formatted_bank.VisibleIndex = 1;
            // 
            // GridColumn_formatted_biller_id
            // 
            this.GridColumn_formatted_biller_id.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_formatted_biller_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_biller_id.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_formatted_biller_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_biller_id.Caption = "Biller ID";
            this.GridColumn_formatted_biller_id.CustomizationCaption = "Appropriate biller ID";
            this.GridColumn_formatted_biller_id.FieldName = "rpps_biller_id";
            this.GridColumn_formatted_biller_id.Name = "GridColumn_formatted_biller_id";
            this.GridColumn_formatted_biller_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_biller_id.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_biller_id.UnboundExpression = "True";
            this.GridColumn_formatted_biller_id.Visible = true;
            this.GridColumn_formatted_biller_id.VisibleIndex = 2;
            // 
            // GridColumn_formatted_region
            // 
            this.GridColumn_formatted_region.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_formatted_region.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_region.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_formatted_region.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_formatted_region.Caption = "Region";
            this.GridColumn_formatted_region.CustomizationCaption = "Region Name";
            this.GridColumn_formatted_region.FieldName = "region";
            this.GridColumn_formatted_region.Name = "GridColumn_formatted_region";
            this.GridColumn_formatted_region.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_region.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_formatted_region.UnboundExpression = "True";
            // 
            // GridColumn_creditor_method
            // 
            this.GridColumn_creditor_method.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_creditor_method.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_creditor_method.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_creditor_method.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_creditor_method.Caption = "ID";
            this.GridColumn_creditor_method.CustomizationCaption = "Record ID";
            this.GridColumn_creditor_method.FieldName = "Id";
            this.GridColumn_creditor_method.Name = "GridColumn_creditor_method";
            this.GridColumn_creditor_method.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor_method.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_creditor_method.Visible = true;
            this.GridColumn_creditor_method.VisibleIndex = 0;
            // 
            // MyEFTLocation
            // 
            this.Name = "MyEFTLocation";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Creditor;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_region;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_bank;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_rpps_biller_id;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_bank;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_biller_id;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_region;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_method;
    }
}
