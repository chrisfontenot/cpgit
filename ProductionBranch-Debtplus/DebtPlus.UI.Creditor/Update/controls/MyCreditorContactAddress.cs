#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Windows.Forms;
using DebtPlus.Data.Controls;
using DebtPlus.Svc.Creditor;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class MyCreditorContactAddress : DebtPlus.Data.Controls.AddressRecordControl
    {
        public MyCreditorContactAddress()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Context information for request local storage.
        /// </summary>
        protected CreditorUpdateClass Context
        {
            get
            {
                if (!DesignMode)
                {
                    Form frm = FindForm();
                    if (frm != null)
                    {
                        return ((ICreditorController)frm).Context;
                    }
                }

                return null;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)this.Line2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.City.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PostalCode.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.States.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.House.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Direction.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Street.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Suffix.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier_Value.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Attn.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.line3.Properties).BeginInit();
            this.SuspendLayout();
            //
            //MyCreditorContactAddress
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.Name = "MyCreditorContactAddress";
            this.Size = new System.Drawing.Size(476, 149);
            ((System.ComponentModel.ISupportInitialize)this.Line2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.City.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PostalCode.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.States.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.House.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Direction.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Street.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Suffix.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier_Value.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Attn.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.line3.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private DataRow row;
        /// <summary>
        ///     Read the record from the database and load the controls
        /// </summary>
        public override DebtPlus.LINQ.address ReadRecord(System.Nullable<Int32> addressId)
        {

            // Allocate the value structure
            DebtPlus.LINQ.address NewValues = new DebtPlus.LINQ.address(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3);

            // Find the row for the edit operation
            Int32 oID = addressId.GetValueOrDefault();
            if (oID > 0)
            {
                row = Context.CreditorDS.GetAddressRowByID(oID);

                if (row != null)
                {
                    NewValues.Id = oID;
                    NewValues.creditor_prefix_1 = DebtPlus.Utils.Nulls.DStr(row["creditor_prefix_1"]);
                    NewValues.creditor_prefix_2 = DebtPlus.Utils.Nulls.DStr(row["creditor_prefix_2"]);
                    NewValues.house = DebtPlus.Utils.Nulls.DStr(row["house"]);
                    NewValues.direction = DebtPlus.Utils.Nulls.DStr(row["direction"]);
                    NewValues.street = DebtPlus.Utils.Nulls.DStr(row["street"]);
                    NewValues.suffix = DebtPlus.Utils.Nulls.DStr(row["suffix"]);
                    NewValues.city = DebtPlus.Utils.Nulls.DStr(row["city"]);
                    NewValues.state = DebtPlus.Utils.Nulls.DInt(row["state"]);
                    NewValues.PostalCode = DebtPlus.Utils.Nulls.DStr(row["PostalCode"]);
                    NewValues.address_line_2 = DebtPlus.Utils.Nulls.DStr(row["Address_Line_2"]);
                    NewValues.address_line_3 = DebtPlus.Utils.Nulls.DStr(row["Address_Line_3"]);
                }
            }

            if (string.IsNullOrEmpty(NewValues.modifier))
            {
                NewValues.modifier = "STE";
            }

            // Ensure that the state has today value.
            if (NewValues.state < 1)
            {
                NewValues.state = DebtPlus.LINQ.Cache.state.getDefault().GetValueOrDefault(0);
            }

            return NewValues;
        }

        protected override int? SaveRecord()
        {
            DataRow row = null;

            // If the address is read-only then there can't be today change
            if (ReadOnly)
            {
                return OriginalData.Id;
            }

            // Find the current values for the address
            DebtPlus.LINQ.address NewValues = GetCurrentValues();

            // If there is an id then we need to look to see if the address changed.

            if (NewValues.Id > 0)
            {
                // If the values have not changed then just abort and return the current pointer.
                if (NewValues.CompareTo(OriginalData) == 0)
                {
                    return OriginalData.Id;
                }

                // The address was changed. Find the address in the tables and update it
                row = Context.CreditorDS.AddressesTable().Rows.Find(NewValues.Id);
                if (row != null)
                {
                    OnAddressChanged(new DebtPlus.Events.AddressChangedEventArgs(NewValues, OriginalData));
                    UpdateRowValues(ref row, ref NewValues);
                    OriginalData = NewValues;
                    return OriginalData.Id;
                }
            }

            // Add the record to the database
            OnAddressChanged(new DebtPlus.Events.AddressChangedEventArgs(NewValues, OriginalData));

            row = Context.CreditorDS.AddressesTable().NewRow();
            Context.CreditorDS.AddressesTable().Rows.Add(row);
            UpdateRowValues(ref row, ref NewValues);
            NewValues.Id = Convert.ToInt32(row["address"]);

            // Update the original values for the next save operation
            OriginalData = NewValues;
            return OriginalData.Id;
        }


        protected void UpdateRowValues(ref DataRow row, ref DebtPlus.LINQ.address NewValues)
        {
            row.BeginEdit();
            row["creditor_prefix_1"] = NewValues.creditor_prefix_1;
            row["creditor_prefix_2"] = NewValues.creditor_prefix_2;
            row["house"] = NewValues.house;
            row["direction"] = NewValues.direction;
            row["street"] = NewValues.street;
            row["suffix"] = NewValues.suffix;
            row["city"] = NewValues.city;
            row["state"] = NewValues.state;
            row["PostalCode"] = NewValues.PostalCode;
            row["Address_Line_2"] = NewValues.address_line_2;
            row["Address_Line_3"] = NewValues.address_line_3;
            row.EndEdit();

            // Commit the changes to the database
            var ccr = DebtPlus.Repository.Addresses.CommitChanges(row.Table);
            if (!ccr.Success)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ccr, "Error saving data to the database");
            }
        }
    }
}