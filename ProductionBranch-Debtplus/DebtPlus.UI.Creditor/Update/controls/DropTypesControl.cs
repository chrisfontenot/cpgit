#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Update.Controls
{
    internal partial class DropTypesControl : ControlBase
    {
        // Current creditor record being edited
        private creditor creditorRecord = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <remarks></remarks>
        public DropTypesControl()
            : base()
        {
            InitializeComponent();
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.CheckedListBoxControl DropTypeList1;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.DropTypeList1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            ((System.ComponentModel.ISupportInitialize)this.DropTypeList1).BeginInit();
            this.SuspendLayout();
            //
            //IndicatorList
            //
            this.DropTypeList1.CheckOnClick = true;
            this.DropTypeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DropTypeList1.Location = new System.Drawing.Point(0, 0);
            this.DropTypeList1.Name = "IndicatorList";
            this.DropTypeList1.Size = new System.Drawing.Size(576, 296);
            this.DropTypeList1.TabIndex = 0;
            this.DropTypeList1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            //
            //IndicatorsControl
            //
            this.Controls.Add(this.DropTypeList1);
            this.Name = "IndicatorsControl";
            ((System.ComponentModel.ISupportInitialize)this.DropTypeList1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion Windows Form Designer generated code

        /// <summary>
        /// Read and display the information on the form
        /// </summary>
        /// <remarks></remarks>
        public override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.creditor CreditorRecord)
        {
            this.creditorRecord = CreditorRecord;

            // Collection of drop reason codes
            System.Collections.Generic.List<RPPSDropReason> colRecords = new System.Collections.Generic.List<RPPSDropReason>();

            // Generate a collection of the drop reason codes. We want distinct string values from the list.
            System.Collections.Generic.List<string> dropReasonCodes = (from dr in bc.drop_reasons where dr.rpps_code != null select dr.rpps_code).Distinct().ToList();

            // Translate the strings into a collection of RPPSDropReason classes
            dropReasonCodes.ForEach(s => colRecords.Add(new RPPSDropReason(s)));

            // Check all of the items that match the code values for the current creditor.
            foreach (var record in bc.creditor_drop_reasons.Where(s => s.creditor == creditorRecord.Id))
            {
                var q = colRecords.Find(s => s.Id == record.rpps_code);
                if (q != null)
                {
                    q.Checked = CheckState.Checked;
                }
            }

            // Update the control with the proper record list. We discard the list at this point
            // and everything is carried in the control from this point onward.
            foreach (var item in colRecords)
            {
                DropTypeList1.Items.Add(new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(item.Id, item.Id, item.Checked));
            }
        }

        /// <summary>
        /// Save the information on the form when needed
        /// </summary>
        /// <remarks></remarks>
        public override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            try
            {
                // System Note buffer
                var sb = new System.Text.StringBuilder();

                // Get a collection of the records to be processed
                var colRecords = bc.creditor_drop_reasons.Where(s => s.creditor == creditorRecord.Id).ToList();

                foreach (DebtPlus.Data.Controls.SortedCheckedListboxControlItem item in DropTypeList1.Items)
                {
                    // The value is the RPPS code
                    string rpps_code = System.Convert.ToString(item.Value);

                    // Find the record in the creditor collection
                    creditor_drop_reason q = colRecords.Find(s => s.rpps_code == rpps_code);

                    if (item.CheckState == CheckState.Checked)
                    {
                        // If the item is checked then add the record if it is missing
                        if (q == null)
                        {
                            q = DebtPlus.LINQ.Factory.Manufacture_creditor_drop_reason(creditorRecord.Id, rpps_code);
                            bc.creditor_drop_reasons.InsertOnSubmit(q);

                            sb.AppendFormat("\r\nAdded drop reason {0}", rpps_code);
                        }
                    }
                    else
                    {
                        // The item is not checked. Delete the record if it is defined.
                        if (q != null)
                        {
                            bc.creditor_drop_reasons.DeleteOnSubmit(q);
                            sb.AppendFormat("\r\nRemoved drop reason {0}", rpps_code);
                        }
                    }
                }

                // Generate a system note if there is anything to say
                if (sb.Length > 0)
                {
                    sb.Insert(0, "Changed the EFT drop reason list for this creditor\r\n");

                    var n = DebtPlus.LINQ.Factory.Manufacture_creditor_note(creditorRecord.Id, 3);
                    n.subject = "Changed creditor EFT drop reason list";
                    n.note = sb.ToString();

                    bc.creditor_notes.InsertOnSubmit(n);
                }

                // Submit all of the changes now.
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the creditor drop reasons table");
            }
        }

        /// <summary>
        /// Class to hold the RPPS code values
        /// </summary>
        private class RPPSDropReason
        {
            public RPPSDropReason()
            {
                Id = null; Checked = CheckState.Unchecked;
            }

            public RPPSDropReason(string Id) : this()
            {
                this.Id = Id;
            }

            public System.Windows.Forms.CheckState Checked { get; set; }

            public string Id { get; set; }
        }
    }
}