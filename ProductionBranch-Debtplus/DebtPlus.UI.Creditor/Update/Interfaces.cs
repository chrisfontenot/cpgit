﻿namespace DebtPlus.UI.Creditor.CS.Update
{
    public interface ControlBase
    {
        void ReadForm(DebtPlus.LINQ.creditor CreditorRecord);
        void RefreshForm(DebtPlus.LINQ.creditor CreditorRecord);
        void SaveForm();
    }
}
