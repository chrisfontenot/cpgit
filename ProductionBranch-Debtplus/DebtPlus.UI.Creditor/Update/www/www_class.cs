#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Creditor.Update.WWW
{
    internal partial class www_class
    {
        // Information about the current creditor
        string creditorLabel = null;
        creditor_www record = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="CreditorLabel"></param>
        public www_class(string CreditorLabel) : base()
        {
            this.creditorLabel = CreditorLabel;
        }

        /// <summary>
        /// Display the creditor messages list
        /// </summary>
        internal void ShowMessagesForm()
        {
            // The creditor record must exist.
            if (!IsCreated())
            {
                return;
            }

            // Process the notes
            using (var frm = new Form_Messages(creditorLabel))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Returns TRUE if the record has been created and should not be created again.
        /// </summary>
        /// <returns></returns>
        public bool IsCreated()
        {
            if (record != null)
            {
                return true;
            }

            using (var bc = new BusinessContext())
            {
                record = bc.creditor_wwws.Where(s => s.Id == creditorLabel).FirstOrDefault();
                return record != null;
            }
        }

        /// <summary>
        /// Create today new web reference entry to allow the user signon permissions
        /// </summary>
        internal void CreateUser()
        {
            if (IsCreated())
            {
                return;
            }

            // Create today new record
            record = DebtPlus.LINQ.Factory.Manufacture_creditor_www(creditorLabel);
            using (var frm = new Form_NewUser(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                using (var bc = new BusinessContext())
                {
                    bc.creditor_wwws.InsertOnSubmit(record);
                    bc.SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Change the password on the existing entry
        /// </summary>
        internal void ChangePassword()
        {
            if (!IsCreated())
            {
                return;
            }

            // Display the request for today new password
            string NewValue = string.Empty;
            if (DebtPlus.Data.Forms.InputBox.Show("What is the new password?", ref NewValue, "Change Password", DebtPlus.LINQ.Factory.CreatePassword()) != DialogResult.OK || string.IsNullOrEmpty(NewValue))
            {
                return;
            }
            record.password = DebtPlus.LINQ.Factory.MD5(NewValue);

            using (var bc = new BusinessContext())
            {
                bc.SubmitChanges();
            }
        }
    }
}