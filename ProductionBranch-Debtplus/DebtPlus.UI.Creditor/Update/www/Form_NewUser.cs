#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.Creditor.Update.WWW
{
    internal partial class Form_NewUser : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DebtPlus.LINQ.creditor_www record = null;
        public Form_NewUser(DebtPlus.LINQ.creditor_www Record)
            : this()
        {
            this.record = Record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += Form_NewUser_Load;
            password.EditValueChanged += FormChanged;
            Button_OK.Click += new EventHandler(Button_OK_Click);
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_NewUser_Load;
            password.EditValueChanged -= FormChanged;
            Button_OK.Click -= new EventHandler(Button_OK_Click);
        }

        #region " Windows Form Designer generated code "

        public Form_NewUser()
            : base()
        {
            Load += Form_NewUser_Load;

            //This call is required by the Windows Form Designer.
            InitializeComponent();

            //Add any initialization after the InitializeComponent() call
            question.Enabled = false;
            answer.Enabled = false;
        }

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.TextEdit password;
        private DevExpress.XtraEditors.TextEdit answer;

        private DevExpress.XtraEditors.ComboBoxEdit question;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.password = new DevExpress.XtraEditors.TextEdit();
            this.answer = new DevExpress.XtraEditors.TextEdit();
            this.question = new DevExpress.XtraEditors.ComboBoxEdit();

            ((System.ComponentModel.ISupportInitialize)this.password.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.answer.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.question.Properties).BeginInit();
            this.SuspendLayout();

            //
            //labelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "labelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(280, 48);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "This form will create an access account for the user on the web interface. Please" + " enter the information for the user data";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //labelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 67);
            this.LabelControl2.Name = "labelControl2";
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Password";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 91);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Hint";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(8, 115);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.TabIndex = 5;
            this.LabelControl4.Text = "answer";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            //
            //button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(57, 152);
            this.Button_OK.Name = "button_OK";
            this.Button_OK.TabIndex = 7;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to create the user's access to the system";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(161, 152);
            this.Button_Cancel.Name = "button_Cancel";
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel the data entry and return to the previous display";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //password
            //
            this.password.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.password.Location = new System.Drawing.Point(88, 64);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(192, 20);
            this.password.TabIndex = 2;
            this.password.ToolTip = "Enter the account password here.";
            this.password.ToolTipController = this.ToolTipController1;
            //
            //answer
            //
            this.answer.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.answer.Location = new System.Drawing.Point(88, 112);
            this.answer.Name = "answer";
            //
            //answer.Properties
            //
            this.answer.Properties.MaxLength = 50;
            this.answer.Size = new System.Drawing.Size(192, 20);
            this.answer.TabIndex = 6;
            this.answer.ToolTip = "Enter the answer to the question. This will enable the user to change the passwor" + "d once the proper answer is given.";
            this.answer.ToolTipController = this.ToolTipController1;
            //
            //question
            //
            this.question.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.question.Location = new System.Drawing.Point(88, 88);
            this.question.Name = "question";
            //
            //question.Properties
            //
            this.question.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.question.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.question.Size = new System.Drawing.Size(192, 20);
            this.question.TabIndex = 4;
            this.question.ToolTip = "Choose today question to be asked of the user when validating the user's identity";
            this.question.ToolTipController = this.ToolTipController1;
            //
            //Form_NewUser
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(292, 190);
            this.Controls.Add(this.question);
            this.Controls.Add(this.answer);
            this.Controls.Add(this.password);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.MaximizeBox = false;
            this.Name = "Form_NewUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "New User";

            ((System.ComponentModel.ISupportInitialize)this.password.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.answer.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.question.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private void Form_NewUser_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                password.Text = DebtPlus.LINQ.Factory.CreatePassword();
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void FormChanged(object Sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return string.IsNullOrWhiteSpace(password.Text);
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.password = DebtPlus.LINQ.Factory.MD5(password.Text.Trim());
        }
    }
}