﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.Conversion.WWWPasswords
{
    internal static class Program
    {
        /// <summary>
        /// Lifted from other projects. It is basically the same code that is used in several
        /// places, including the web.
        /// </summary>
        /// <param name="inputString">Input string to be encrypted</param>
        /// <returns>The encrypted string value</returns>
        public static string Md5(string inputString)
        {
            // Handle the missing password condition as a blank string.
            if (string.IsNullOrWhiteSpace(inputString))
            {
                return string.Empty;
            }

            // First, convert the string to an array of bytes
            Byte[] tempSource = Encoding.ASCII.GetBytes(inputString.ToLower());

            // Next, encode the password stream
            var md5Class = new System.Security.Cryptography.MD5CryptoServiceProvider();
            Byte[] tempHash = md5Class.ComputeHash(tempSource);

            // Finally, merge the byte stream to a string
            var sbPasswd = new StringBuilder();
            for(var index = 0; index <= tempHash.GetUpperBound(0); ++index)
            {
                sbPasswd.Append(tempHash[index].ToString("X2"));
            }

            // The encoded password is the combined string
            return sbPasswd.ToString().ToUpper();
        }

        static void Main()
        {
            using (var bc = new BusinessContext())
            {
                var colRecords = bc.client_wwws.Where(s => s.ApplicationName == "/AttorneyPortal" && s.Password.StartsWith("*"));
                foreach (var item in colRecords)
                {
                    item.Password = Md5(item.Comment);
//                  item.Comment = string.Empty;
                }

                // Retrieve the addresses used by the conversion.
//              var colAddr = (from adr in bc.addresses join v in bc.vendor_contacts on adr.Id equals v.addressID select adr).ToList();
//              ProcessAddressList(colAddr);

//              colAddr = (from adr in bc.addresses join v in bc.vendor_addresses on adr.Id equals v.addressID select adr).ToList();
//              ProcessAddressList(colAddr);

                // Rewrite the items with the encrypted passwords
                bc.SubmitChanges();
            }
        }

        private static void ProcessAddressList(IEnumerable<address> colAddr)
        {
            // Convert the address references as well
            var sb = new StringBuilder();
            var parser = new AddressParser.AddressParser();

            foreach (var adr in colAddr)
            {
                // Ignore Puerto Rico addresses. They don't do well in the US parser.
                if (adr.state == 52)
                {
                    continue;
                }

                sb.Clear();
                sb.Append(adr.street ?? string.Empty);
                if (!string.IsNullOrWhiteSpace(adr.address_line_2))
                {
                    sb.AppendFormat(" {0}", adr.address_line_2);
                }

                sb.AppendFormat(" {0}, CA 10001", adr.city ?? string.Empty);
                sb.Replace("  ", " ");

                var result         = parser.ParseAddress(sb.ToString());

                // Extract the results from the parser
                if (result != null && !string.IsNullOrWhiteSpace(result.Street))
                {
                    adr.address_line_2 = string.Empty;
                    adr.address_line_3 = string.Empty;
                    adr.house          = result.Number ?? string.Empty;
                    adr.direction      = result.Predirectional ?? string.Empty;
                    adr.street         = result.Street ?? string.Empty;
                    adr.suffix         = result.Suffix ?? string.Empty;
                    adr.modifier       = result.SecondaryUnit ?? string.Empty;
                    adr.modifier_value = result.SecondaryNumber ?? string.Empty;
                    adr.city           = result.City ?? string.Empty;

                    // The post directional is not part of our address block. (It should be!)
                    if (!string.IsNullOrWhiteSpace(result.Postdirectional))
                    {
                        adr.street += " " + result.Postdirectional;
                    }
                }
            }
        }
    }
}
