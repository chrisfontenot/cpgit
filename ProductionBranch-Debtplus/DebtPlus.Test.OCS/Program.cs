﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.Test.OCS
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Test.TestStateMachineExecutor();
            Test.TestEIFamilyTriggerEffects();
            Test.TestPhoneNumberValidation();
            Test.TestStateConfigLoader1();
            Test.TestStateConfigLoader2();
            Test.TestGeneralScenarioClass();
            
            Test.TestSpreadsheetShouldHavePrimaryKey();
            Test.TestSpreadsheetShouldHaveOneDateError();
            Test.TestSpreadsheetShouldHaveOnePhoneError();
            Test.TestSpreadsheetShouldGiveInactiveClient();            

            Console.WriteLine("Finished");
            Console.ReadLine();
        }
    }
}
