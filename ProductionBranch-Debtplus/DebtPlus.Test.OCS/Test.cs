﻿using System;
using System.Collections.Generic;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS.Rules;
using DebtPlus.UI.Housing.OCS;

namespace DebtPlus.Test.OCS
{
    public class Test
    {
        private static Action<bool> assert = (check) =>
        {
            if (check == true)
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            }
            else {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;            
            }

            Console.WriteLine((check == true ? "Pass" : "Fail"));

            Console.ResetColor();
        };

        public static void TestStateMachineExecutor()
        {
            Console.WriteLine("TestStateMachineExecutor");
            Console.WriteLine("SetActive tests");
            var result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.DoNothing }, new OCSClient { ActiveFlag = true });
            assert(result.ActiveFlag == true);
            result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.DoNothing }, new OCSClient { ActiveFlag = false });
            assert(result.ActiveFlag == false);
            result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.SetActive }, new OCSClient { ActiveFlag = false });
            assert(result.ActiveFlag == true);
            result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.SetActive }, new OCSClient { ActiveFlag = true });
            assert(result.ActiveFlag == true);
            result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.SetInactive }, new OCSClient { ActiveFlag = false });
            assert(result.ActiveFlag == false);
            result = StateExecutor.Execute(new TriggerEffect { SetActive = ACTIVE_RULE.SetInactive }, new OCSClient { ActiveFlag = true });
            assert(result.ActiveFlag == false);

            Console.WriteLine("IsCOQueue tests");
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetHighTouch }, new OCSClient { Queue = null });
            assert(result.Queue == OCS_QUEUE.HighTouch);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetHighTouch }, new OCSClient { Queue = OCS_QUEUE.HighTouch });
            assert(result.Queue == OCS_QUEUE.HighTouch);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetHighTouch }, new OCSClient { Queue = OCS_QUEUE.Counselor });
            assert(result.Queue == OCS_QUEUE.HighTouch);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetCounselor }, new OCSClient { Queue = null });
            assert(result.Queue == OCS_QUEUE.Counselor);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetCounselor }, new OCSClient { Queue = OCS_QUEUE.HighTouch });
            assert(result.Queue == OCS_QUEUE.Counselor);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.SetCounselor }, new OCSClient { Queue = OCS_QUEUE.Counselor });
            assert(result.Queue == OCS_QUEUE.Counselor);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.DoNothing }, new OCSClient { Queue = OCS_QUEUE.Counselor });
            assert(result.Queue == OCS_QUEUE.Counselor);
            result = StateExecutor.Execute(new TriggerEffect { SetQueue = QUEUE_RULE.DoNothing }, new OCSClient { Queue = OCS_QUEUE.HighTouch });
            assert(result.Queue == OCS_QUEUE.HighTouch);

            Console.WriteLine("Increment tests");
            result = StateExecutor.Execute(new TriggerEffect { IncrementAttempts = INCREMENT_RULE.DoNothing }, new OCSClient { ContactAttemptCount = 0 });
            assert(result.ContactAttemptCount == 0);
            result = StateExecutor.Execute(new TriggerEffect { IncrementAttempts = INCREMENT_RULE.IncrementBy1 }, new OCSClient { ContactAttemptCount = 0 });
            assert(result.ContactAttemptCount == 1);
            /*Test executor for IsCOQueue = null & IncrementAttempts = null */
            result = StateExecutor.Execute(new TriggerEffect { IncrementAttempts = INCREMENT_RULE.ResetTo1 }, new OCSClient { ContactAttemptCount = 0 });
            assert(result.ContactAttemptCount == 1);
            result = StateExecutor.Execute(new TriggerEffect { IncrementAttempts = INCREMENT_RULE.ResetTo1 }, new OCSClient { ContactAttemptCount = 4 });
            assert(result.ContactAttemptCount == 1);

            Console.WriteLine("Last Chance Rules tests");
            //should do nothing tests
            result = StateExecutor.Execute(new TriggerEffect { LastChanceRules = false }, new OCSClient { });
            assert(!result.PostModLastChanceFlag.HasValue);
            assert(!result.PostModLastChanceDate.HasValue);
            result = StateExecutor.Execute(new TriggerEffect { LastChanceRules = false }, new OCSClient { PostModLastChanceDate = new DateTime(2015, 1, 1), PostModLastChanceFlag = 2 });
            assert(result.PostModLastChanceFlag.HasValue && result.PostModLastChanceDate.Value.Date == new DateTime(2015, 1, 1).Date);
            assert(result.PostModLastChanceDate.HasValue && result.PostModLastChanceFlag.Value == 2);

            //calculates
            result = StateExecutor.Execute(new TriggerEffect { LastChanceRules = true }, new OCSClient { });
            assert(result.PostModLastChanceFlag.HasValue);
            Console.WriteLine(result.PostModLastChanceFlag.Value);
            assert(result.PostModLastChanceDate.HasValue && result.PostModLastChanceDate.Value.Date == DateTime.Now.Date);

            //does not calculate...note this is really testing the rule at this point and not the executor
            result = StateExecutor.Execute(new TriggerEffect { LastChanceRules = true }, new OCSClient { PostModLastChanceDate = new DateTime(2015, 1, 1), PostModLastChanceFlag = 2 });
            assert(result.PostModLastChanceFlag.HasValue && result.PostModLastChanceDate.Value.Date == new DateTime(2015, 1, 1).Date);
            assert(result.PostModLastChanceDate.HasValue && result.PostModLastChanceFlag.Value == 2);

            Console.WriteLine("SetStatus tests");
            result = StateExecutor.Execute(new TriggerEffect { SetStatus = null }, new OCSClient { StatusCode = STATUS_CODE.GeneralQuestions });
            assert(result.StatusCode == STATUS_CODE.GeneralQuestions);
            result = StateExecutor.Execute(new TriggerEffect { SetStatus = STATUS_CODE.Counseled }, new OCSClient { StatusCode = STATUS_CODE.GeneralQuestions });
            assert(result.StatusCode == STATUS_CODE.Counseled);

            //Really testing calculation CalculatePnOfT at this point
            result = StateExecutor.Execute(new TriggerEffect { CalcStatus = STATUS_FUNC.PnOfT }, new OCSClient { StatusCode = STATUS_CODE.GeneralQuestions });
            assert(result.StatusCode == STATUS_CODE.PaymentReminder1);
            result = StateExecutor.Execute(new TriggerEffect { CalcStatus = STATUS_FUNC.PnOfT }, new OCSClient { LastRightPartyContact = DateTime.Now });
            assert(result.StatusCode == STATUS_CODE.PaymentReminder1);

            var now = DateTime.Now;
            result = StateExecutor.Execute(new TriggerEffect { CalcStatus = STATUS_FUNC.PnOfT }, new OCSClient { LastRightPartyContact = new DateTime(now.Year, now.Month - 1, now.Day) });
            assert(result.StatusCode == STATUS_CODE.PaymentReminder2);

            result = StateExecutor.Execute(new TriggerEffect { CalcStatus = STATUS_FUNC.PnOfT }, new OCSClient { LastRightPartyContact = new DateTime(now.Year - 2, now.Month, now.Day) });
            assert(result.StatusCode == STATUS_CODE.PaymentReminder12);
        }

        public static OCSClient test(STATUS_CODE priorCode, RESULT_CODE result, TRIGGER trigger = TRIGGER.NoTrigger, bool active = true, int attemptCount = 1, OCS_QUEUE queue = OCS_QUEUE.HighTouch, Person.LANGUAGE lang = Person.LANGUAGE.English)
        {
            var client = new OCSClient { 
                ActiveFlag = active, 
                ContactAttemptCount = attemptCount,
                Queue = queue,
                PreferredLanguage = lang
            };

            client.PhoneNumbers = new List<LINQ.TelephoneNumber> { 
                new LINQ.TelephoneNumber {
                    SetNumber = "(111) 111-1111",
                    AutoAssignedType = PHONE_TYPE.ClientHome
                }
            };

            client.StartContactAttempt();
            client.CurrentAttempt.Outcomes[0].Result = result;

            return MasterStateMachine.UpdateOnTrigger(PROGRAM.FreddieMacEI, trigger, priorCode, client);
        }

        public static void TestEIFamilyTriggerEffects() {
            Console.WriteLine("EI Family State Machine tests.");

            var result = test(STATUS_CODE.NewRecord, RESULT_CODE.CSRWillCallBack);
            assert(result.ActiveFlag == true);
            assert(result.StatusCode == STATUS_CODE.CSRWillCallBack);
            assert(result.ContactAttemptCount == 2);

            result = test(STATUS_CODE.NewRecord, RESULT_CODE.Skip, TRIGGER.LanguageChanged, lang: Person.LANGUAGE.Spanish);
            assert(result.ActiveFlag == true);
            assert(result.StatusCode == STATUS_CODE.NewRecord);
            assert(result.ContactAttemptCount == 1);
        }

        public static void TestPhoneNumberValidation(){
            Console.WriteLine("Phone number validation tests");

            Action<String> shouldFail = str => assert(CommonValidation.IsPhone(str) == false);
            Action<String> shouldPass = str => assert(CommonValidation.IsPhone(str) == true);

            shouldFail("");
            shouldFail("raspberry");
            shouldFail("1111111111");
            shouldFail("1111111");
            shouldFail("1112221111");
            shouldFail("1111112222");
            shouldFail("2221111111");            
            shouldFail("1002223333");
            shouldFail("(222) 111-1111");//just to prove nothing but digits matter

            shouldPass("5552223333");
            shouldPass("555-222-3333");
            shouldPass("555.222.3333");
            shouldPass("(555) 222-3333");
        }

        public static void TestStateConfigLoader1()
        {
            Console.WriteLine("TestStateConfigLoader1");

            var cfg = new ScenarioConfig
            {
                UniversalTriggers = new List<TriggerEffect> { 
                    new TriggerEffect { Trigger = TRIGGER.Result_SK },
                    new TriggerEffect { Trigger = TRIGGER.Result_BN }
                },
                Scenarios = new List<GeneralScenario> { 
                    new GeneralScenario {
                        StatusCode = STATUS_CODE.NewRecord,
                        Triggers = new List<TriggerEffect> {
                            new TriggerEffect { Trigger = TRIGGER.Result_CO },
                            new TriggerEffect { Trigger = TRIGGER.Result_CC }
                        }
                    },
                    new GeneralScenario {
                        StatusCode = STATUS_CODE.Counseled,
                        Triggers = new List<TriggerEffect> {
                            new TriggerEffect { Trigger = TRIGGER.Result_DS }
                        }
                    }
                }
            };
            var list = ScenarioConfigLoader.load(cfg);
            
            assert(list.Count == 2);

            var first = (GeneralScenario)list.First(s => s.StatusCode == STATUS_CODE.NewRecord);
            assert(first.Triggers.Count == 4);
            assert(first.Triggers.Exists(t => t.Trigger == TRIGGER.Result_SK));
            assert(first.Triggers.Exists(t => t.Trigger == TRIGGER.Result_BN));
            assert(first.Triggers.Exists(t => t.Trigger == TRIGGER.Result_CO));
            assert(first.Triggers.Exists(t => t.Trigger == TRIGGER.Result_CC));

            var second = (GeneralScenario)list.First(s => s.StatusCode == STATUS_CODE.Counseled);
            assert(second.Triggers.Count == 3);
            assert(second.Triggers.Exists(t => t.Trigger == TRIGGER.Result_SK));
            assert(second.Triggers.Exists(t => t.Trigger == TRIGGER.Result_BN));
            assert(second.Triggers.Exists(t => t.Trigger == TRIGGER.Result_DS));
        }

        public static void TestStateConfigLoader2()
        {
            Console.WriteLine("TestStateConfigLoader2");

            var str =
@"{
    ""UniversalTriggers"": [
        {
            ""Trigger"": ""Result_SK"",
            ""SetStatus"": null,
            ""IsCOQueue"": null,
            ""LastChanceRules"": false,
            ""IncrementAttempts"": ""ResetTo1""
        },
        {
            ""Trigger"": ""Result_BN"",
            ""SetStatus"": null,
            ""IsCOQueue"": null,
            ""LastChanceRules"": false,
            ""IncrementAttempts"": ""ResetTo1""
        }
    ],
    ""Scenarios"": [
        {
            ""StatusCode"": ""NewRecord"",
            ""Triggers"": [
                {
                    ""Trigger"": ""Result_CO"",
                    ""SetActive"": ""SetActive"",
                    ""SetStatus"": null,
                    ""IsCOQueue"": null,
                    ""LastChanceRules"": false,
                    ""IncrementAttempts"": ""ResetTo1""
                },
                {
                    ""Trigger"": ""Result_CC"",
                    ""SetStatus"": null,
                    ""IsCOQueue"": null,
                    ""LastChanceRules"": false,
                    ""IncrementAttempts"": ""ResetTo1""
                }
            ]
        },
        {
            ""StatusCode"": ""Counseled"",
            ""Triggers"": [
                {
                    ""Trigger"": ""Result_DS"",
                    ""SetStatus"": null,
                    ""IsCOQueue"": null,
                    ""LastChanceRules"": false,
                    ""IncrementAttempts"": ""ResetTo1""
                }
            ]
        }
    ]
}";
            ScenarioConfig cfg = Newtonsoft.Json.JsonConvert.DeserializeObject<ScenarioConfig>(str);

//            var foo = Newtonsoft.Json.JsonConvert.SerializeObject(DebtPlus.Svc.OCS.Rules.FMACPostMod.CounselorQueueStates.getSerializable());

            assert(cfg.UniversalTriggers.Exists(t => t.Trigger == TRIGGER.Result_SK));
            assert(cfg.UniversalTriggers.Exists(t => t.Trigger == TRIGGER.Result_BN));

            assert(cfg.Scenarios.Exists(s => s.StatusCode == STATUS_CODE.NewRecord));
            var triggers = cfg.Scenarios.First(s => s.StatusCode == STATUS_CODE.NewRecord).Triggers;
            assert(triggers.Exists(t => t.Trigger == TRIGGER.Result_CO));
            assert(triggers.Exists(t => t.Trigger == TRIGGER.Result_CC));

            assert(cfg.Scenarios.Exists(s => s.StatusCode == STATUS_CODE.Counseled));
            triggers = cfg.Scenarios.First(s => s.StatusCode == STATUS_CODE.Counseled).Triggers;
            assert(triggers.Exists(t => t.Trigger == TRIGGER.Result_DS));
        }

        public static void TestGeneralScenarioClass() {
            var scenario = new GeneralScenario
            {
                Triggers = new List<TriggerEffect> { 
                    new TriggerEffect { Trigger = TRIGGER.Result_DS },
                    new TriggerEffect { Trigger = TRIGGER.Result_EX }
                }
            };

            assert(scenario.GetTriggerEffect(TRIGGER.Result_DS) != null);

            bool exceptionThrown = false;
            try{
                scenario.GetTriggerEffect(TRIGGER.Result_IM);
            }catch(InvalidTrigger ex){
                exceptionThrown = true;
                Console.WriteLine("Exception caught: " + ex.Message);
            }

            assert(exceptionThrown);
        }

        public static void RunSpreadsheetTest(String filename, Action<System.IO.FileStream> test) {

            var path = System.IO.Path.Combine(
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "TestSpreadsheets",
                filename);

            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                test(stream);
            }            

        }

        public static void TestSpreadsheetShouldHavePrimaryKey(){
            Console.WriteLine("TestSpreadsheetShouldHavePrimaryKey");

            var thrown = false;
            
            RunSpreadsheetTest("ErrorPrimaryKey.xlsx", (stream) =>
            {
                Tuple<List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>, List<String>> allTheThings = null;

                try
                {
                    var fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
                    var fieldConvertActions = FMACExcelMap.fieldConvertActions;

                    //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                    allTheThings = ExcelToRelationalMapper.mapFile(
                        stream,
                        fieldValidationFunctions,
                        fieldConvertActions,
                        ProgramConfigService.getKeyColumn(PROGRAM.FreddieMacEI));
                }
                catch (InvalidSpreadsheetException ex)
                {
                    //for now do nothing with the actual exception
                    thrown = true;
                    //Console.WriteLine(String.Join(Environment.NewLine + Environment.NewLine, ex.errors));
                }
            });

            assert(thrown);
        }

        public static void TestSpreadsheetShouldHaveOneDateError() {
            Console.WriteLine("TestSpreadsheetShouldHaveOneDateError");

            RunSpreadsheetTest("OneDateError.xlsx", (stream) =>
            {
                var fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
                var fieldConvertActions = FMACExcelMap.fieldConvertActions;

                //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                var allTheThings = ExcelToRelationalMapper.mapFile(
                    stream,
                    fieldValidationFunctions,
                    fieldConvertActions,
                    ProgramConfigService.getKeyColumn(PROGRAM.FreddieMacEI));

                var errors = allTheThings.Item2;

                assert(errors.Count == 1);
                assert(errors.Where(e => e.Contains("\"Actual Sale Date\" has errors")).Count() == 1);
            });
        }

        public static void TestSpreadsheetShouldHaveOnePhoneError() {
            Console.WriteLine("TestSpreadsheetShouldHaveOnePhoneError");

            RunSpreadsheetTest("OnePhoneError.xlsx", (stream) =>
            {
                var fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
                var fieldConvertActions = FMACExcelMap.fieldConvertActions;

                //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                var allTheThings = ExcelToRelationalMapper.mapFile(
                    stream,
                    fieldValidationFunctions,
                    fieldConvertActions,
                    ProgramConfigService.getKeyColumn(PROGRAM.FreddieMacEI));

                var errors = allTheThings.Item2;

                assert(errors.Count == 1);
                assert(errors.Where(e => e.Contains("\"Phone2\" has errors")).Count() == 1);

                Console.WriteLine(String.Join(Environment.NewLine + Environment.NewLine, allTheThings.Item2));
            });
        }

        public static void TestSpreadsheetShouldGiveInactiveClient() {
            Console.WriteLine("TestSpreadsheetShouldGiveInactiveClient");

            RunSpreadsheetTest("NoValidPhonesEqualsInactiveClient.xlsx", (stream) =>
            {
                var fieldValidationFunctions = FMACExcelMap.fieldValidationFuncs;
                var fieldConvertActions = FMACExcelMap.fieldConvertActions;

                //if the format of the excel spreadsheet changes (i.e. different program), then we will pass different maps of functions
                var allTheThings = ExcelToRelationalMapper.mapFile(
                    stream,
                    fieldValidationFunctions,
                    fieldConvertActions,
                    ProgramConfigService.getKeyColumn(PROGRAM.FreddieMacEI));

                var client = allTheThings.Item1.First().Item1;

                assert(client.ActiveFlag == false);
            });        
        }
    }
}
