﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public sealed class TelephoneNumber : IComparable<TelephoneNumber>
    {
        public TelephoneNumber()
        {
            Id = -1;
            Acode = null;
            Number = null;
            Ext = null;
            NumberDisplay = string.Empty;
        }

        public TelephoneNumber(Int32 Country, string Acode, string Number, string Ext) : this()
        {
            this.Country = Country;
            this.Acode = Acode;
            this.Number = Number;
            this.Ext = Ext;
        }

        /// <summary>
        /// The record number in the database table "TelephoneNumbers"
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 Id { get; set; }

        /// <summary>
        /// Country dialing code
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 Country { get; set; }

        /// <summary>
        /// Area/City dialing code
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Acode { get; set; }

        /// <summary>
        /// Telephone number
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Number { get; set; }

        /// <summary>
        /// Telephone number extension
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Ext { get; set; }

        /// <summary>
        /// Display text for the telephone number
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string NumberDisplay { get; set; }

        /// <summary>
        /// Is this record NEW?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is this record EMPTY?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(NumberDisplay);
        }

        /// <summary>
        /// Compare the contents between two copies of the telephone number
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 CompareTo(TelephoneNumber other) // Implements System.IComparable<TelephoneNumberData>.CompareTo
        {
            return string.Compare(NumberDisplay, other.NumberDisplay, true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Convert the item to a suitable display string
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return NumberDisplay;
        }
    }
}
