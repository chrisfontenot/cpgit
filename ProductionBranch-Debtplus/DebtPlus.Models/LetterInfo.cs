﻿using System;

namespace DebtPlus.Models
{
    // results from xpr_letter_fetch
    [Obsolete("Use LINQ Tables")]
    public class LetterInfo
    {
        public double TopMargin { get; set; }
        public double BottomMargin { get; set; }
        public double LeftMargin { get; set; }
        public double RightMargin { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public Int32 Language { get; set; }
        public string LetterCode { get; set; }
        public Int32 LetterType { get; set; }
        public Int32 DisplayMode { get; set; }
        public string QueueName { get; set; }
    }
}
