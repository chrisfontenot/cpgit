﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class ClientAppointment
    {
        /// <summary>
        /// Create the instance of the data class
        /// </summary>
        public ClientAppointment()
        {
            Id = -1;                            // There is no record yet.
            status = "P";                       // "P" = pending
            result = string.Empty;              // result = not specified
            HousingFeeAmount = 0M;              // No fee changed yet
            confirmation_status = 0;            // 0 = not confirmed
            date_updated = DateTime.Now;        // date the record was changed
            date_created = DateTime.Now;        // date the record was created
            created_by = string.Empty;          // person who created the record
            ClientID = -1;
            priority = false;
            housing = false;
            post_purchase = false;
            credit = false;
            callback_ph = string.Empty;
        }

        public Int32 Id { get; set; }                   // field is called 'client_appointment' in the database
        public Int32 ClientID { get; set; }             // field is called 'client' in the database
        public Int32? appt_time { get; set; }
        public Int32? counselor { get; set; }
        public DateTime start_time { get; set; }
        public DateTime? end_time { get; set; }
        public Int32? office { get; set; }
        public Int32? workshop { get; set; }
        public Int32? workshop_people { get; set; }
        public Int32? appt_type { get; set; }
        public string status { get; set; }
        public string result { get; set; }
        public Int32? previous_appointment { get; set; }
        public Int32? referred_to { get; set; }
        public Int32? referred_by { get; set; }
        public Int32? bankruptcy_class { get; set; }
        public Boolean priority { get; set; }
        public Boolean housing { get; set; }
        public Boolean post_purchase { get; set; }
        public Boolean credit { get; set; }
        public string callback_ph { get; set; }
        public decimal HousingFeeAmount { get; set; }
        public Int32 confirmation_status { get; set; }
        public DateTime? date_confirmed { get; set; }
        public DateTime date_updated { get; set; }
        public DateTime date_created { get; set; }
        public string created_by { get; set; }

        /// <summary>
        /// Is the record "new", that is does not exist in the database?
        /// </summary>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmty()
        {
            return false;
        }

        /// <summary>
        /// Return the string format for the Banks record.
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return Id.ToString();
        }
    }
}
