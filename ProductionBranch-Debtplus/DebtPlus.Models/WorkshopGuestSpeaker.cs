﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class WorkshopGuestSpeaker
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }

        public WorkshopGuestSpeaker()
        {
            Id = -1;
            Name = string.Empty;
            Email = string.Empty;
            Telephone = string.Empty;
        }
    }
}
