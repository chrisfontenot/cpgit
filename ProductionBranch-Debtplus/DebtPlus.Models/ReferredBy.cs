﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class ReferredBy
    {
        public Int32 Id { get; set; }
        public string Description { get; set; }
        public Int32 ReferredByType { get; set; }
        public bool Default { get; set; }
        public bool ActiveFlag { get; set; }
        public string Note { get; set; }
        public string Hud9902Section { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
