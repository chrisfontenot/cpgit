﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class Banks : System.IComparable<Banks>
    {

    /// <summary>
    /// Default ACH priority value.
    /// </summary>
    /// <remarks></remarks>
    public readonly string default_ach_priority = "01";

    /// <summary>
    /// ACH bank
    /// </summary>
    /// <remarks></remarks>
    public readonly string Type_ACH = "A";           // ACH bank

    /// <summary>
    /// Checking bank
    /// </summary>
    /// <remarks></remarks>
    public readonly string Type_CHECK = "C";         // Checking bank

    /// <summary>
    /// Deposit (no checking) bank
    /// </summary>
    /// <remarks></remarks>
    public readonly string Type_DEPOSIT = "D";       // Deposit (no checks) bank

    /// <summary>
    /// RPPS bank
    /// </summary>
    /// <remarks></remarks>
    public readonly string Type_RPPS = "R";          // RPPS bank

    /// <summary>
    /// (obsolete) VANCO bank
    /// </summary>
    /// <remarks></remarks>
    public readonly string Type_VANCO = "V";         // obsolete -- VANCO bank

    /// <summary>
    /// Bank ID number
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 Id { get; set; }                   // field is called 'Banks' in the db

    /// <summary>
    /// Descriptive name for the bank
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string description { get; set; }

    /// <summary>
    /// Is the item the DEFAULT for a combobox/lookupedit control?
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public bool Default { get; set; }

    /// <summary>
    /// Is the item active and can be selected for new references?
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public bool ActiveFlag { get; set; }

    /// <summary>
    /// Type of the bank
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string _Type { get; set; }

    /// <summary>
    /// Name of the contact person at the bank
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32? ContactNameID { get; set; }

    /// <summary>
    /// Name of the bank (not description)
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string bank_name { get; set; }

    /// <summary>
    /// Pointer to the bank's address record
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32? BankAddressID { get; set; }

    /// <summary>
    /// ABA (routing number) for the bank
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string aba { get; set; }

    /// <summary>
    /// Account number for the bank
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string account_number { get; set; }

    /// <summary>
    /// Immediate origin for RPPS banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string immediate_origin { get; set; }

    /// <summary>
    /// Immediate origin name for RPPS banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string immediate_origin_name { get; set; }

    /// <summary>
    /// Priority for ACH banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string ach_priority { get; set; }

    /// <summary>
    /// Company ID for ACH banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string ach_company_id { get; set; }

    /// <summary>
    /// Origin DFI for ACH banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string ach_origin_dfi { get; set; }

    /// <summary>
    /// Should a withdrawl operation be generated for the deposit items on ACH?
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public bool ach_enable_offset { get; set; }

    /// <summary>
    /// Immediate destination for RPPS / ACH banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string immediate_destination { get; set; }

    /// <summary>
    /// Immediate desitination name for RPPS / ACH banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string immediate_destination_name { get; set; }

    /// <summary>
    /// Check number for checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 checknum { get; set; }

    /// <summary>
    /// Maximum number of items allowed on a check for checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32? max_clients_per_check { get; set; }

    /// <summary>
    /// Maximum dollar amount allowed on a check for checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public decimal? max_amt_per_check { get; set; }

    /// <summary>
    /// Batch number for ACH / RPPS banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 batch_number { get; set; }

    /// <summary>
    /// Transaction number for ACH / RPPS banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public Int32 transaction_number { get; set; }

    /// <summary>
    /// Image of the check signature for checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public System.Drawing.Image SignatureImage { get; set; }

    /// <summary>
    /// Prefix lines written before the text file on ACH / RPPS / Checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string prefix_line { get; set; }

    /// <summary>
    /// Suffix lines written after the text file on ACH / RPPS / Checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string suffix_line { get; set; }

    /// <summary>
    /// Output directory for text files on ACH / RPPS / Checking banks
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string output_directory { get; set; }

    /// <summary>
    /// Person who created the row
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public string created_by { get; set; }

    /// <summary>
    /// Date/Time when the row was created
    /// </summary>
    /// <value></value>
    /// <returns></returns>
    /// <remarks></remarks>
    public DateTime date_created { get; set; }

    /// <summary>
    /// Create the instance of the data class
    /// </summary>
    public Banks()
    {
        Id = 0;
        description = null;
        Default = false;
        ActiveFlag = true;
        _Type = string.Empty;
        ContactNameID = null;
        bank_name = null;
        BankAddressID = null;
        aba = null;
        account_number = null;
        immediate_origin = null;
        immediate_origin_name = null;
        ach_priority = default_ach_priority;
        ach_company_id = null;
        ach_origin_dfi = null;
        ach_enable_offset = false;
        immediate_destination = null;
        immediate_destination_name = null;
        checknum = 0;
        max_amt_per_check = null;
        max_clients_per_check = null;
        batch_number = 0;
        transaction_number = 0;
        SignatureImage = null;
        prefix_line = null;
        suffix_line = null;
        output_directory = null;
        created_by = null;
        date_created = DateTime.Now;
    }

    /// <summary>
    /// Is the record "new", that is does not exist in the database?
    /// </summary>
    public bool IsNew()
    {
        return Id < 1;
    }

    /// <summary>
    /// Is the current record empty? Does it have any data to be saved?
    /// </summary>
    public bool IsEmty()
    {
        // Look for the simple items that are never null
        if( Default || ! ActiveFlag || checknum > 0 || transaction_number > 0 || batch_number > 0 || ach_enable_offset )
        {
            return false;
        }

        if( ach_priority != default_ach_priority )
        {
            return false;
        }

        // Find the other items that are possibly null
        foreach( object item in new object[] {description, _Type, ContactNameID, bank_name, BankAddressID, aba, account_number, immediate_destination, immediate_destination_name, immediate_origin, immediate_origin_name, ach_company_id, ach_origin_dfi, max_amt_per_check, max_clients_per_check, SignatureImage, prefix_line, suffix_line, output_directory} )
        {
            if( item != null )
            {
                return false;
            }
        }

        // The record has no changes.
        return true;
    }

    /// <summary>
    /// Compare the current Banks structure to the one that is passed. Return the relationship.
    /// </summary>
    /// <param name="other">Pointer to the second Banks structure for the comparison.</param>
    /// <returns></returns>
    public Int32 CompareTo(Banks other)
    {
        // The description is the most important field for a sort order. Do it first.
        Int32 Relation = Compare(description, other.description);

        // The other fields follow in descending priority
        if( Relation == 0 ) Relation = Default.CompareTo(other.Default);
        if( Relation == 0 ) Relation = ActiveFlag.CompareTo(other.ActiveFlag);
        if( Relation == 0 ) Relation = _Type.CompareTo(other._Type);
        if( Relation == 0 ) Relation = Compare(bank_name, other.bank_name);
        if( Relation == 0 ) Relation = Compare(aba, other.aba);
        if( Relation == 0 ) Relation = Compare(account_number, other.account_number);
        if( Relation == 0 ) Relation = Compare(immediate_origin, other.immediate_origin);
        if( Relation == 0 ) Relation = Compare(immediate_origin_name, other.immediate_origin_name);
        if( Relation == 0 ) Relation = Compare(immediate_destination, other.immediate_destination);
        if( Relation == 0 ) Relation = Compare(immediate_destination_name, other.immediate_destination_name);
        if( Relation == 0 ) Relation = Compare(ach_priority, other.ach_priority);
        if( Relation == 0 ) Relation = Compare(ach_company_id, other.ach_company_id);
        if( Relation == 0 ) Relation = Compare(ach_origin_dfi, other.ach_origin_dfi);
        if( Relation == 0 ) Relation = Compare(prefix_line, other.prefix_line);
        if( Relation == 0 ) Relation = Compare(suffix_line, other.suffix_line);
        if( Relation == 0 ) Relation = Compare(output_directory, other.output_directory);
        if( Relation == 0 ) Relation = checknum.CompareTo(other.checknum);
        if( Relation == 0 ) Relation = batch_number.CompareTo(other.batch_number);
        if( Relation == 0 ) Relation = transaction_number.CompareTo(other.transaction_number);
        if( Relation == 0 ) Relation = ach_enable_offset.CompareTo(other.ach_enable_offset);

        // These fields are a bit more tricky. We allow nulls for simple items because the database has nullable fields
        // for them. Therefore, we need to compare the null vaules before we can test the relationship of the non-null items.
        if( Relation == 0 )
        {
            if( ContactNameID == null && other.ContactNameID == null )
            {
                Relation = 0;
            }
            else if ( ContactNameID == null )
            {
                Relation = -1;
            }
            else if ( other.ContactNameID == null )
            {
                Relation = 1;
            }
            else
            {
                Relation = ContactNameID.Value.CompareTo(other.ContactNameID.Value);
            }
        }

        if( Relation == 0 )
        {
            if( BankAddressID == null && other.BankAddressID == null )
            {
                Relation = 0;
            }
            else if ( BankAddressID == null )
            {
                Relation = -1;
            }
            else if ( other.BankAddressID == null )
            {
                Relation = 1;
            }
            else
            {
                Relation = BankAddressID.Value.CompareTo(other.BankAddressID.Value);
            }
        }

        if( Relation == 0 )
        {
            if( max_amt_per_check == null && other.max_amt_per_check == null )
            {
                Relation = 0;
            }
            else if ( max_amt_per_check == null )
            {
                Relation = -1;
            }
            else if ( other.max_amt_per_check == null )
            {
                Relation = 1;
            }
            else
            {
                Relation = max_amt_per_check.Value.CompareTo(other.max_amt_per_check.Value);
            }
        }

        if( Relation == 0 )
        {
            if( max_clients_per_check == null && other.max_clients_per_check == null )
            {
                Relation = 0;
            }
            else if ( max_clients_per_check == null )
            {
                Relation = -1;
            }
            else if ( other.max_clients_per_check == null )
            {
                Relation = 1;
            }
            else
            {
                Relation = max_clients_per_check.Value.CompareTo(other.max_clients_per_check.Value);
            }
        }

        if( Relation == 0 )
        {
            if( SignatureImage == null && other.SignatureImage == null )
            {
                Relation = 0;
            }
            else
                if (SignatureImage == null)
                {
                    Relation = -1;
                }
                else
                {
                    if (other.SignatureImage == null)
                    {
                        Relation = 1;
                    }
                    else
                    {
                        if (SignatureImage.Equals(other.SignatureImage))
                        {
                            Relation = 1;
                        }
                    }
                }
            }
            return Relation;
        }

        private Int32 Compare(string a, string b)
        {
            // They are equal if they are both empty
            if( string.IsNullOrEmpty(a) && string.IsNullOrEmpty(b))
            {
                return 0;
            }

            if( string.IsNullOrEmpty(a) )
            {
                return 1;
            }

            if( string.IsNullOrEmpty(b) )
            {
                return -1;
            }

        // Neither is empty. Do the comparisons now.
        return String.Compare(a.Trim(), b.Trim(), true, System.Globalization.CultureInfo.InvariantCulture);
        }

    /// <summary>
    /// Return the string format for the Banks record.
    /// </summary>
    /// <returns></returns>
    public new string ToString()
    {
        if( string.IsNullOrEmpty( description ))
        {
            return string.Empty;
        }
        return description.Trim();
    }
    }
}
