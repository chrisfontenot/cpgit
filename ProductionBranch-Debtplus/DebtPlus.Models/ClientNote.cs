﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class ClientNote
    {
        public Int32 Id { get; set; }                   // field is called 'client_note' in the db
        public Int32 ClientId { get; set; }             // field is called 'client' in the db
        public Int32? CreditorId { get; set; }          // field is called 'client_creditor' in the db
        public Int32? PriorNote { get; set; }
        public Int32 _Type { get; set; }
        public DateTime? Expires { get; set; }
        public bool DontEdit { get; set; }
        public bool DontDelete { get; set; }
        public bool DontPrint { get; set; }
        public string Subject { get; set; }             // nullable
        public string Note { get; set; }                // nullable
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }           // not null

        public ClientNote()
        {
            _Type = 0;
            DateCreated = DateTime.Now;
            CreatedBy = string.Empty;
            Subject = string.Empty;
            Note = string.Empty;
            DontDelete = false;
            DontEdit = false;
            DontPrint = false;
            Id = -1;
        }
    }
}
