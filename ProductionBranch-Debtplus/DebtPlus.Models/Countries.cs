﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class Countries
    {
        public Int32 Id { get; set; }
        public string Description { get; set; }
        public Int32 country_code { get; set; }
        public bool Default { get; set; }
        public bool ActiveFlag { get; set; }
        public string created_by { get; set; }
        public DateTime date_created { get; set; }
    }
}
