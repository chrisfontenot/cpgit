﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class HousingDownloadFormStuff
    {
        public string HcsId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
