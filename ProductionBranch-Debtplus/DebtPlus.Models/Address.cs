﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class Address : IComparable<Address>
    {
        private readonly bool showAttn;      // Is the attention line displayed?
        private readonly bool showCreditor1; // Is the creditor line 1 valid?
        private readonly bool showCreditor2; // Is the creditor line 2 valid?
        private readonly bool showLine3;     // Is the third line of the address valid?

        /// <summary>
        /// Default value for the address record modifier field.
        /// </summary>
        public const string DefaultModifier = "APT";

        private Address()
        {
            Id = -1;
            Attn = string.Empty;
            Creditor_Prefix_1 = string.Empty;
            Creditor_Prefix_2 = string.Empty;
            House = string.Empty;
            Direction = string.Empty;
            Street = string.Empty;
            Suffix = string.Empty;
            Modifier = DefaultModifier;
            Modifier_Value = string.Empty;
            Address_Line_2 = string.Empty;
            Address_Line_3 = string.Empty;
            City = string.Empty;
            State = 0;
            StateText = string.Empty;
            PostalCode = string.Empty;
        }

        /// <summary>
        /// Create the instance of the data class
        /// </summary>
        /// <param name="ShowAttn">Should the creditor Attn line be shown?</param>
        /// <param name="ShowCreditor1">Should the creditor line 1 be shown?</param>
        /// <param name="ShowCreditor2">Should the creditor line 2 be shown?</param>
        /// <param name="ShowLine3">Should the address line 3 be shown?</param>
        public Address(bool ShowAttn, bool ShowCreditor1, bool ShowCreditor2, bool ShowLine3)
            : this()
        {
            showAttn = ShowAttn;
            showCreditor1 = ShowCreditor1;
            showCreditor2 = ShowCreditor2;
            showLine3 = ShowLine3;
        }

        /// <summary>
        /// The record ID of the current dataset.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 Id { get; set; }

        /// <summary>
        /// Creditor's attention line
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Attn { get; set; }

        /// <summary>
        /// Prefix line 1 for the creditor
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Creditor_Prefix_1 { get; set; }

        /// <summary>
        /// Prefix line 2 for the creditor
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Creditor_Prefix_2 { get; set; }

        /// <summary>
        /// House number
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string House { get; set; }

        /// <summary>
        /// Leading direction
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Direction { get; set; }

        /// <summary>
        /// Street name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Street { get; set; }

        /// <summary>
        /// Suffix for the string (i.e. BLVD, RD, LN, etc.)
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Suffix { get; set; }

        /// <summary>
        /// Modifier for the address (i.e. APPT, FLOOR, BLDG, etc.)
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Modifier { get; set; }

        /// <summary>
        /// Value for the modifier. THe apartment number, etc.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Modifier_Value { get; set; }

        /// <summary>
        /// Second line of the address
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Address_Line_2 { get; set; }

        /// <summary>
        /// Third line of the address
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Address_Line_3 { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string City { get; set; }

        /// <summary>
        /// State ID
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 State { get; set; }

        /// <summary>
        /// The name of the State ID as text. Needed for CityStateZip.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string StateText { get; set; }

        /// <summary>
        /// Postal code (ZipCode)
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string PostalCode { get; set; }

        /// <summary>
        /// Is the record "new", that is does not exist in the database?
        /// </summary>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmpty()
        {
            // Look at the standardly empty strings
            foreach (string str in new string[] { Creditor_Prefix_1, Creditor_Prefix_2, House, Direction, Street, Suffix, Modifier_Value, Address_Line_2, Address_Line_3, City, PostalCode })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
            }

            // Look for the modifier
            if (string.IsNullOrWhiteSpace(Modifier) || string.Compare(Modifier, DefaultModifier, true, System.Globalization.CultureInfo.InvariantCulture) != 0)
            {
                return false;
            }

            // The record has no changes.
            return true;
        }

        /// <summary>
        /// Compare the current address structure to the one that is passed. Return the relationship.
        /// </summary>
        /// <param name="other">Pointer to the second address structure for the comparison.</param>
        /// <returns></returns>
        public Int32 CompareTo(Address other) // Implements IComparable<Address>.CompareTo
        {
            Int32 Relation = Compare(Attn, other.Attn);

            if (Relation == 0)
            {
                Relation = Compare(Creditor_Prefix_1, other.Creditor_Prefix_1);
            }

            if (Relation == 0)
            {
                Relation = Compare(Creditor_Prefix_2, other.Creditor_Prefix_2);
            }

            if (Relation == 0)
            {
                Relation = Compare(House, other.House);
            }

            if (Relation == 0)
            {
                Relation = Compare(Direction, other.Direction);
            }

            if (Relation == 0)
            {
                Relation = Compare(Street, other.Street);
            }

            if (Relation == 0)
            {
                Relation = Compare(Suffix, other.Suffix);
            }

            if (Relation == 0)
            {
                Relation = Compare(Modifier, other.Modifier);
            }

            if (Relation == 0)
            {
                Relation = Compare(Modifier_Value, other.Modifier_Value);
            }

            if (Relation == 0)
            {
                Relation = Compare(Address_Line_2, other.Address_Line_2);
            }

            if (Relation == 0)
            {
                Relation = Compare(Address_Line_3, other.Address_Line_3);
            }

            if (Relation == 0)
            {
                Relation = Compare(City, other.City);
            }

            if (Relation == 0)
            {
                Relation = Compare(PostalCode, other.PostalCode);
            }

            if (Relation == 0)
            {
                Relation = State.CompareTo(other.State);
            }
            return Relation;
        }

        private static Int32 Compare(string a, string b)
        {
            // They are equal if they are both empty
            if (string.IsNullOrWhiteSpace(a) && string.IsNullOrWhiteSpace(b))
            {
                return 0;
            }

            // If only one is empty then return the proper relationship
            if (string.IsNullOrWhiteSpace(a))
            {
                return 1;
            }

            if (string.IsNullOrWhiteSpace(b))
            {
                return -1;
            }

            // Neither is empty. Do the comparisons now.
            return string.Compare(a.Trim(), b.Trim(), true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Convert the first line address to a string
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string AddressLine1
        {
            get
            {
                var sb = new System.Text.StringBuilder();

                // Generate the initial line with a single space between the fields
                foreach (string str in new string[] { House, Direction, Street, Suffix })
                {
                    if (!string.IsNullOrWhiteSpace(str))
                    {
                        sb.AppendFormat(" {0}", str.Trim());
                    }
                }

                // Determine if the modifier and value is defined. Ignore "APT" without a modifier.
                if (string.Compare(Modifier, DefaultModifier, true) != 0 || !string.IsNullOrWhiteSpace(Modifier_Value))
                {
                    // The modifier and modifier value go together. You can't have a value without the modifier.
                    if (string.IsNullOrWhiteSpace(Modifier) && !string.IsNullOrWhiteSpace(Modifier_Value))
                    {
                        sb.AppendFormat(", {0} {1}", DefaultModifier, Modifier_Value.Trim());
                    }
                    else if (!string.IsNullOrWhiteSpace(Modifier))
                    {
                        sb.AppendFormat(", {0}", Modifier.Trim());

                        if (!string.IsNullOrWhiteSpace(Modifier_Value))
                        {
                            sb.AppendFormat(" {0}", Modifier_Value.Trim());
                        }
                    }
                }

                // The answer is the combined result. Remove the leading and trailing blanks.
                return sb.ToString().Trim(new char[] { ' ', ',' });
            }
        }

        /// <summary>
        /// Convert the city, state, and zipcode to an address line
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string CityStateZip
        {
            get
            {
                var sb = new System.Text.StringBuilder();

                if (!string.IsNullOrWhiteSpace(City))
                {
                    sb.AppendFormat(" {0}", City.Trim());
                }

                if (!string.IsNullOrWhiteSpace(StateText))
                {
                    sb.AppendFormat("  {0}", StateText.Trim());  // Yes, there are two spaces here. See USPO formatting regs for the reason.
                }

                if (!string.IsNullOrWhiteSpace(PostalCode))
                {
                    sb.AppendFormat(" {0}", PostalCode.Trim());
                }

                // The answer is the combined result.
                return sb.ToString().Trim();
            }
        }

        /// <summary>
        /// Return the string format for the address record.
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            var sb = new System.Text.StringBuilder();

            // Include the attention line
            if (showAttn && !string.IsNullOrWhiteSpace(Attn))
            {
                sb.Append(Environment.NewLine);
                sb.AppendFormat("ATTN: {0}", Attn.Trim());
            }

            // Include the creditor prefix line 1
            if (showCreditor1 && !string.IsNullOrWhiteSpace(Creditor_Prefix_1))
            {
                sb.Append(Environment.NewLine);
                sb.Append(Creditor_Prefix_1.Trim());
            }

            // Include the creditor prefix line 2
            if (showCreditor2 && !string.IsNullOrWhiteSpace(Creditor_Prefix_2))
            {
                sb.Append(Environment.NewLine);
                sb.Append(Creditor_Prefix_2.Trim());
            }

            // Add the standard line1 and 2 of the address block
            var str = AddressLine1;
            if (!string.IsNullOrWhiteSpace(str))
            {
                sb.Append(Environment.NewLine);
                sb.Append(str);
            }

            if (!string.IsNullOrWhiteSpace(Address_Line_2))
            {
                sb.Append(Environment.NewLine);
                sb.Append(Address_Line_2.Trim());
            }

            // Include the address line 3 if needed
            if (showLine3 && !string.IsNullOrWhiteSpace(Address_Line_3))
            {
                sb.Append(Environment.NewLine);
                sb.Append(Address_Line_3.Trim());
            }

            // Follow it with the city / state / zipcode
            str = CityStateZip;
            if (!string.IsNullOrWhiteSpace(str))
            {
                sb.Append(Environment.NewLine);
                sb.Append(str);
            }

            return sb.ToString().Trim();
        }
    }
}
