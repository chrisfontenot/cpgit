﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public sealed class EmailAddress : IComparable<EmailAddress>
    {
        // Strings for the various special modes
        public const string cREFUSED = "DECLINED";
        public const string cNONE = "NONE";
        public const string cMISSING = "NOT SPECIFIED";

        /// <summary>
        /// List of items that are stored in the database "validation" field for the various settings.
        /// Internally, "valid" and "invalid" are the same thing. It is only when the email is validated
        /// and proven to be invalid that we change the type to invalid.
        /// </summary>
        /// <remarks></remarks>
        public enum EmailValidationEnum
        {
            Missing = 0,
            Valid = 1,
            Invalid = 2,
            Refused = 3,
            None = 4
        }

        /// <summary>
        /// Primary key to the EmailAddress table
        /// </summary>
        public Object Id { get; set; }

        /// <summary>
        /// String for the Email Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// ValidationCode level
        /// </summary>
        public EmailValidationEnum ValidationCode { get; set; }

        /// <summary>
        /// Create the class
        /// </summary>
        public EmailAddress()
        {
            Id = System.DBNull.Value;
            Address = cMISSING;
            ValidationCode = EmailValidationEnum.Missing;
        }

        public EmailAddress(string Address, EmailValidationEnum Validation) : this()
        {
            this.Address = Address;
            this.ValidationCode = Validation;
        }

        /// <summary>
        /// Is this a "new" record that has not been saved?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id == null || System.Object.Equals(Id, System.DBNull.Value);
        }

        /// <summary>
        /// Is the address empty?
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            switch (ValidationCode)
            {
                case EmailValidationEnum.Missing:
                    return true;

                case EmailValidationEnum.Valid:
                case EmailValidationEnum.Invalid:
                    return string.IsNullOrWhiteSpace(Address);

                default:
                    return false;
            }
        }

        public int CompareTo(EmailAddress other)
        {
            // Both "invalid" and "valid" are the same for our comparison at this time.
            Int32 a_Validation = Convert.ToInt32(ValidationCode == EmailValidationEnum.Invalid || ValidationCode == EmailValidationEnum.Valid ? EmailValidationEnum.Valid : ValidationCode);
            Int32 b_Validation = Convert.ToInt32(other.ValidationCode == EmailValidationEnum.Invalid || other.ValidationCode == EmailValidationEnum.Valid ? EmailValidationEnum.Valid : other.ValidationCode);

            Int32 Relation = a_Validation.CompareTo(b_Validation);
            if (Relation == 0)
            {
                if (a_Validation == (Int32)EmailValidationEnum.Valid)
                {
                    Relation = string.Compare((Address ?? string.Empty), (other.Address ?? string.Empty), true);
                }
            }
            return Relation;
        }

        public override string ToString()
        {
            switch (ValidationCode)
            {
                case EmailValidationEnum.Missing:
                    return cMISSING;

                case EmailValidationEnum.Refused:
                    return cREFUSED;

                case EmailValidationEnum.None:
                    return cNONE;

                default:
                    return Address;
            }
        }
    }
}
