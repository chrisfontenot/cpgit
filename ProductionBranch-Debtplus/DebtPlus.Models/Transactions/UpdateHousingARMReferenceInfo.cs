﻿using System;
using System.Collections.Generic;

namespace DebtPlus.Models.Transactions
{
    [System.Obsolete("Use LINQ Tables")]
    public class UpdateHousingARMReferenceInfo
    {
        public Int32 GroupId { get; set; }
        public List<HousingARMReferenceItem> Items { get; set; }

        public class HousingARMReferenceItem
        {
            public Int32 Id { get; set; }
            public string LongDesc { get; set; }
            public string Name { get; set; }
            public string ShortDesc { get; set; }
        }

        public UpdateHousingARMReferenceInfo()
        {
            Items = new List<HousingARMReferenceItem>();
        }
    }
}
