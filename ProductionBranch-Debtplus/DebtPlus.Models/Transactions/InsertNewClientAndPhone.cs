﻿namespace DebtPlus.Models.Transactions
{
    [System.Obsolete("Use LINQ Tables")]
    public class InsertNewClientAndPhone
    {
        public NewClient NewClient { get; set; }
        public DebtPlus.Interfaces.TelephoneNumbers.ITelephoneNumber Telephone { get; set; }
    }
}
