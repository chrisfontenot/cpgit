using System;
using System.Collections.Generic;

namespace DebtPlus.Models.Transactions
{
    [System.Obsolete("Use LINQ Tables")]
    public class DeleteInsertWorkshopContent
    {
        public Int32 Type { get; set; }
        public List<Int32> DeleteIds { get; set; }
        public List<Int32> InsertIds { get; set; }

        public DeleteInsertWorkshopContent()
        {
            DeleteIds = new List<Int32>();
            InsertIds = new List<Int32>();
        }
    }
}
