﻿namespace DebtPlus.Models.Transactions
{
    [System.Obsolete("Use LINQ Tables")]
    public class InsertWorkshopTypesAndContent
    {
        public WorkshopType _Type { get; set; }
        public DeleteInsertWorkshopContent Diwc { get; set; }
    }
}
