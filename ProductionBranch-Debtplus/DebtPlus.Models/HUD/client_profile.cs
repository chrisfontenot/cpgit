#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class client_profile : ExtractBase
    {

        #region storage
        private Int32 privateCounseling_Rental_Workshop;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Complete_Budget_Counseling;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Consumer_Loan;
        private Int32 privateCount_HomeMaintenance_Fin_Management_HECM_Counseled;
        private Int32 privateCount_HomeMaintenance_Fin_Management_HECM_Obtained;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Homeequity_Loan;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Legal_Assistance;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Mortgage_Refinanced;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Other;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Other_Social_Agency;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Receiving_Counseling;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Sold_House;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Utilities_Current;
        private Int32 privateCount_HomeMaintenance_Fin_Management_Withdrew_Counseling;
        private Int32 privateCount_Occupied_Emergency_Shelter;
        private Int32 privateCount_Occupied_PermntHouse_wo_RentAssist;
        private Int32 privateCount_Occupied_Permnthouse_RentAssist;
        private Int32 privateCount_Occupied_Transitional_Housing;
        private Int32 privateCount_Remained_Homeless;
        private Int32 privateCount_Seek_Shelter_Other;
        private Int32 privateCount_Withdrew_Counseling;
        private Int32 privateCount_Counseled_Ref_Other_SocAgency;
        private Int32 privateCount_Currently_Receiving_Counsel;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Longterm_Counseling;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Other;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_PurchHousing;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Readyafter_90;
        private Int32 privateCount_Prepurchase_Homebuyer_Counsel_Within_90;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Bankcruptcy;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Debt_Manage_Plan;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Mortgage_Current;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Mortgage_Modified;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Mortgage_Refinanced;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Other;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Referred_to_Legal;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Second_Mortgage_Received;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Sold_Property_Alternative;
        private Int32 privateCount_Prevent_Mortgage_Deliquency_Withdrew;
        private Int32 privateCount_Seeking_Help_Housing_Current_Counseled;
        private Int32 privateCount_Seeking_Help_Housing_Debt_Mngmt_Entered;
        private Int32 privateCount_Seeking_Help_Housing_Found_Alternative_Housing;
        private Int32 privateCount_Seeking_Help_Housing_Other;
        private Int32 privateCount_Seeking_Help_Housing_Recertification_Subsidy_Program;
        private Int32 privateCount_Seeking_Help_Housing_Referred_Legal_Agency_Eviction;
        private Int32 privateCount_Seeking_Help_Housing_Referred_Legal_Aid_Agency;
        private Int32 privateCount_Seeking_Help_Housing_Referred_Other_Social_Agency;
        private Int32 privateCount_Seeking_Help_Housing_Referred_to_Rental_Assistance;
        private Int32 privateCount_Seeking_Help_Housing_Remain_CurrentHousing;
        private Int32 privateCount_Seeking_Help_Housing_Resolved_Issue;
        private Int32 privateCount_Seeking_Help_Housing_Search_Assistance;
        private Int32 privateCount_Seeking_Help_Housing_Security_Dep_Dispute;
        private Int32 privateCount_Seeking_Help_Housing_Temp_Rental_Relief;
        private Int32 privateCount_Seeking_Help_Housing_Utilities_Current;
        private Int32 privateCount_Seeking_Help_Housing_Withdrew_Counseling;
        private Int32 privateEthnicity_Clients_Counseling_Hispanic;
        private Int32 privateEthnicity_Clients_Counseling_No_Response;
        private Int32 privateEthnicity_Clients_Counseling_Non_Hispanic;
        private Int32 privateAMI_No_Response;
        private Int32 privateGreater100_AMI_Level;
        private Int32 privateLesser50_AMI_Level;
        private Int32 privateA50_79_AMI_Level;
        private Int32 privateA80_100_AMI_Level;
        private Int32 privateCompl_Workshop_Predatory_Lend;
        private Int32 privateMultiRace_Clients_Counseling_AMINDWHT;
        private Int32 privateMultiRace_Clients_Counseling_AMRCINDBLK;
        private Int32 privateMultiRace_Clients_Counseling_ASIANWHT;
        private Int32 privateMultiRace_Clients_Counseling_BLKWHT;
        private Int32 privateMultiRace_Clients_Counseling_NoResponse;
        private Int32 privateMultiRace_Clients_Counseling_OtherMLTRC;
        private Int32 privateRace_Clients_Counseling_American_Indian_Alaskan_Native;
        private Int32 privateRace_Clients_Counseling_Asian;
        private Int32 privateRace_Clients_Counseling_Black_AfricanAmerican;
        private Int32 privateRace_Clients_Counseling_Pacific_Islanders;
        private Int32 privateRace_Clients_Counseling_White;
        private Int32 privateClient_ID_Num;
        private Int32 privateClient_Purpose_Of_Visit;
        private Int32 privateClient_Outcome_Of_Visit;
        private Int32 privateClient_Case_Num;
        private Int32 privateClient_Counselor_ID;
        private Int32 privateClient_Head_Of_Household_Type;
        private Int32 privateClient_Credit_Score;
        private Int32 privateClient_Credit_Score_Source;
        private Int32 privateClient_No_Credit_Score_Reason;
        private string privateClient_First_Name;
        private string privateClient_Last_Name;
        private string privateClient_Middle_Name;
        private string privateClient_Street_Address_1;
        private string privateClient_Street_Address_2;
        private string privateClient_City;
        private Int32 privateClient_State;
        private string privateClient_ZipCode;
        private string privateClient_New_Street_Address_1;
        private string privateClient_New_Street_Address_2;
        private string privateClient_New_City;
        private Int32 privateClient_New_State;
        private string privateClient_New_ZipCode;
        private string privateClient_Spouse_First_Name;
        private string privateClient_Spouse_Last_Name;
        private string privateClient_Spouse_Middle_Name;
        private bool privateClient_Farm_Worker;
        private bool privateClient_Colonias_Resident;
        private bool privateClient_Disabled;
        private bool privateClient_HECM_Certificate;
        private bool privateClient_Predatory_Lending;
        private bool privateClient_FirstTime_Home_Buyer;
        private bool privateClient_Discrimination_Victim;
        private bool privateClient_Mortgage_Deliquency;
        private bool privateClient_Second_Loan_Exists;
        private bool privateClient_Intake_Loan_Type_Is_Hybrid_ARM;
        private bool privateClient_Intake_Loan_Type_Is_Option_ARM;
        private bool privateClient_Intake_Loan_Type_Is_Interest_Only;
        private bool privateClient_Intake_Loan_Type_Is_FHA_Or_VA_Insured;
        private bool privateClient_Intake_Loan_Type_Is_Privately_Held;
        private bool privateClient_Intake_Loan_Type_Has_Interest_Rate_Reset;
        private Nullable<DateTime> privateClient_Counsel_Session_DT_Start;
        private DateTime privateClient_Counsel_Session_DT_End = DateTime.MinValue;
        private Nullable<DateTime> privateClient_Intake_DT;
        private DateTime privateClient_Birth_DT = DateTime.MinValue;
        private DateTime privateClient_HECM_Certificate_Issue_Date = DateTime.MinValue;
        private DateTime privateClient_HECM_Certificate_Expiration_Date = DateTime.MinValue;
        private string privateClient_HECM_Certificate_ID;
        private DateTime privateClient_Sales_Contract_Signed = DateTime.MinValue;
        private decimal privateClient_Grant_Amount_Used;
        private decimal privateClient_Mortgage_Closing_Cost;
        private double privateClient_Mortgage_Interest_Rate;
        private Int32 privateClient_Counseling_Termination;
        private Int32 privateClient_Income_Level;
        private Int32 privateClient_Highest_Educ_Grade;
        private Int32 privateClient_HUD_Assistance;
        private Int32 privateClient_Dependents_Num;
        private Int32 privateClient_Language_Spoken;
        private Int32 privateClient_Session_Duration;
        private Int32 privateClient_Counseling_Type;
        private decimal privateClient_Counseling_Fee;
        private Int32 privateClient_Attribute_HUD_Grant;
        private Int32 privateClient_Finance_Type_Before;
        private Int32 privateClient_Finance_Type_After;
        private Int32 privateClient_Mortgage_Type;
        private Int32 privateClient_Mortgage_Type_After;
        private Int32 privateClient_referred_by;
        private Int32 privateClient_Job_Duration;
        private Int32 privateClient_Household_Debt;
        private string privateClient_Loan_Being_Reported;
        private Int32 privateClient_Intake_Loan_Type;
        private Int32 privateClient_Family_Size;
        private Int32 privateClient_Marital_Status;
        private Int32 privateClient_Race_ID;
        private Int32 privateClient_Ethnicity_ID;
        private decimal privateClient_Household_Gross_Monthly_Income;
        private string privateClient_Gender;
        private string privateClient_Spouse_SSN;
        private string privateClient_SSN1;
        private string privateClient_SSN2;
        private string privateClient_Mobile_Phone_Num;
        private string privateClient_Phone_Num;
        private string privateClient_Fax;
        private string privateClient_Email;
        private Int32 privateGroup_session_id;
        private Int32 privatePerson_1;
        private Int32 privatePerson_2;
        private Int32 privateHud_interview;
        private Int32 privateInterview_type;
        private Int32 privateHud_result;
        private Nullable<DateTime> privateInterview_date;
        private Nullable<DateTime> privateResult_date;
        private Nullable<DateTime> privateTermination_date;
        private string privateInterview_result;
        private decimal privateAmi;
        private Int32 privateClient_appointment;
        private Int32 privateHousing_property;
        private Int32 privatePrimary_loan;
        private Int32 privateSecondary_loan;
        private Int32 privateCompl_Workshop_HomeFin_Credit_Repair;
        private Int32 privateCompl_Resolv_Prevent_Mortg_Deliq;
        private Int32 privateCompl_Other_Workshop;
        private Int32 privateCompl_HomeMaint_FinMngt;
        private Int32 privateCompl_HomeBuyer_Educ_Workshop;
        private Int32 privateCompl_Help_FairHousing_Workshop;

        public Int32 Compl_Help_FairHousing_Workshop
        {
            get
            {
                return privateCompl_Help_FairHousing_Workshop;
            }
            set
            {
                DoSetProperty("compl_Help_FairHousing_Workshop", ref privateCompl_Help_FairHousing_Workshop, value);
            }
        }

        public Int32 Compl_HomeBuyer_Educ_Workshop
        {
            get
            {
                return privateCompl_HomeBuyer_Educ_Workshop;
            }
            set
            {
                DoSetProperty("compl_HomeBuyer_Educ_Workshop", ref privateCompl_HomeBuyer_Educ_Workshop, value);
            }
        }

        public Int32 Compl_HomeMaint_FinMngt
        {
            get
            {
                return privateCompl_HomeMaint_FinMngt;
            }
            set
            {
                DoSetProperty("compl_HomeMaint_FinMngt", ref privateCompl_HomeMaint_FinMngt, value);
            }
        }

        public Int32 Compl_Other_Workshop
        {
            get
            {
                return privateCompl_Other_Workshop;
            }
            set
            {
                DoSetProperty("compl_Other_Workshop", ref privateCompl_Other_Workshop, value);
            }
        }

        public Int32 Compl_Resolv_Prevent_Mortg_Deliq
        {
            get
            {
                return privateCompl_Resolv_Prevent_Mortg_Deliq;
            }
            set
            {
                DoSetProperty("compl_Resolv_Prevent_Mortg_Deliq", ref privateCompl_Resolv_Prevent_Mortg_Deliq, value);
            }
        }

        public Int32 Compl_Workshop_HomeFin_Credit_Repair
        {
            get
            {
                return privateCompl_Workshop_HomeFin_Credit_Repair;
            }
            set
            {
                DoSetProperty("compl_Workshop_HomeFin_Credit_Repair", ref privateCompl_Workshop_HomeFin_Credit_Repair, value);
            }
        }

        public Int32 Compl_Workshop_Predatory_Lend
        {
            get
            {
                return privateCompl_Workshop_Predatory_Lend;
            }
            set
            {
                DoSetProperty("compl_Workshop_Predatory_Lend", ref privateCompl_Workshop_Predatory_Lend, value);
            }
        }

        public Int32 Counseling_Rental_Workshop
        {
            get
            {
                return privateCounseling_Rental_Workshop;
            }
            set
            {
                DoSetProperty("counseling_Rental_Workshop", ref privateCounseling_Rental_Workshop, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Complete_Budget_Counseling;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling", ref privateCount_HomeMaintenance_Fin_Management_Complete_Budget_Counseling, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling", ref privateCount_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Consumer_Loan
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Consumer_Loan;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Consumer_Loan", ref privateCount_HomeMaintenance_Fin_Management_Consumer_Loan, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_HECM_Counseled
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_HECM_Counseled;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_HECM_Counseled", ref privateCount_HomeMaintenance_Fin_Management_HECM_Counseled, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_HECM_Obtained
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_HECM_Obtained;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_HECM_Obtained", ref privateCount_HomeMaintenance_Fin_Management_HECM_Obtained, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Homeequity_Loan
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Homeequity_Loan;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Homeequity_Loan", ref privateCount_HomeMaintenance_Fin_Management_Homeequity_Loan, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Legal_Assistance
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Legal_Assistance;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Legal_Assistance", ref privateCount_HomeMaintenance_Fin_Management_Legal_Assistance, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Mortgage_Refinanced;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Mortgage_Refinanced", ref privateCount_HomeMaintenance_Fin_Management_Mortgage_Refinanced, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage", ref privateCount_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Other
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Other;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Other", ref privateCount_HomeMaintenance_Fin_Management_Other, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Other_Social_Agency
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Other_Social_Agency;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Other_Social_Agency", ref privateCount_HomeMaintenance_Fin_Management_Other_Social_Agency, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Receiving_Counseling
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Receiving_Counseling;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Receiving_Counseling", ref privateCount_HomeMaintenance_Fin_Management_Receiving_Counseling, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Sold_House
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Sold_House;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Sold_House", ref privateCount_HomeMaintenance_Fin_Management_Sold_House, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Utilities_Current
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Utilities_Current;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Utilities_Current", ref privateCount_HomeMaintenance_Fin_Management_Utilities_Current, value);
            }
        }

        public Int32 Count_HomeMaintenance_Fin_Management_Withdrew_Counseling
        {
            get
            {
                return privateCount_HomeMaintenance_Fin_Management_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("count_HomeMaintenance_Fin_Management_Withdrew_Counseling", ref privateCount_HomeMaintenance_Fin_Management_Withdrew_Counseling, value);
            }
        }

        public Int32 Count_Occupied_Emergency_Shelter
        {
            get
            {
                return privateCount_Occupied_Emergency_Shelter;
            }
            set
            {
                DoSetProperty("count_Occupied_Emergency_Shelter", ref privateCount_Occupied_Emergency_Shelter, value);
            }
        }

        public Int32 Count_Occupied_PermntHouse_wo_RentAssist
        {
            get
            {
                return privateCount_Occupied_PermntHouse_wo_RentAssist;
            }
            set
            {
                DoSetProperty("count_Occupied_PermntHouse_wo_RentAssist", ref privateCount_Occupied_PermntHouse_wo_RentAssist, value);
            }
        }

        public Int32 Count_Occupied_Permnthouse_RentAssist
        {
            get
            {
                return privateCount_Occupied_Permnthouse_RentAssist;
            }
            set
            {
                DoSetProperty("count_Occupied_Permnthouse_RentAssist", ref privateCount_Occupied_Permnthouse_RentAssist, value);
            }
        }

        public Int32 Count_Occupied_Transitional_Housing
        {
            get
            {
                return privateCount_Occupied_Transitional_Housing;
            }
            set
            {
                DoSetProperty("count_Occupied_Transitional_Housing", ref privateCount_Occupied_Transitional_Housing, value);
            }
        }

        public Int32 Count_Remained_Homeless
        {
            get
            {
                return privateCount_Remained_Homeless;
            }
            set
            {
                DoSetProperty("count_Remained_Homeless", ref privateCount_Remained_Homeless, value);
            }
        }

        public Int32 Count_Seek_Shelter_Other
        {
            get
            {
                return privateCount_Seek_Shelter_Other;
            }
            set
            {
                DoSetProperty("count_Seek_Shelter_Other", ref privateCount_Seek_Shelter_Other, value);
            }
        }

        public Int32 Count_Withdrew_Counseling
        {
            get
            {
                return privateCount_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("count_Withdrew_Counseling", ref privateCount_Withdrew_Counseling, value);
            }
        }

        public Int32 Count_Counseled_Ref_Other_SocAgency
        {
            get
            {
                return privateCount_Counseled_Ref_Other_SocAgency;
            }
            set
            {
                DoSetProperty("count_Counseled_Ref_Other_SocAgency", ref privateCount_Counseled_Ref_Other_SocAgency, value);
            }
        }

        public Int32 Count_Currently_Receiving_Counsel
        {
            get
            {
                return privateCount_Currently_Receiving_Counsel;
            }
            set
            {
                DoSetProperty("count_Currently_Receiving_Counsel", ref privateCount_Currently_Receiving_Counsel, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog", ref privateCount_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Longterm_Counseling;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling", ref privateCount_Prepurchase_Homebuyer_Counsel_Longterm_Counseling, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Other
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Other;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Other", ref privateCount_Prepurchase_Homebuyer_Counsel_Other, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase", ref privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew", ref privateCount_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_PurchHousing
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_PurchHousing;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_PurchHousing", ref privateCount_Prepurchase_Homebuyer_Counsel_PurchHousing, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Readyafter_90
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Readyafter_90;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Readyafter_90", ref privateCount_Prepurchase_Homebuyer_Counsel_Readyafter_90, value);
            }
        }

        public Int32 Count_Prepurchase_Homebuyer_Counsel_Within_90
        {
            get
            {
                return privateCount_Prepurchase_Homebuyer_Counsel_Within_90;
            }
            set
            {
                DoSetProperty("count_Prepurchase_Homebuyer_Counsel_Within_90", ref privateCount_Prepurchase_Homebuyer_Counsel_Within_90, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Bankcruptcy
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Bankcruptcy;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Bankcruptcy", ref privateCount_Prevent_Mortgage_Deliquency_Bankcruptcy, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention", ref privateCount_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Debt_Manage_Plan;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan", ref privateCount_Prevent_Mortgage_Deliquency_Debt_Manage_Plan, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu", ref privateCount_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Current
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Mortgage_Current;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Mortgage_Current", ref privateCount_Prevent_Mortgage_Deliquency_Mortgage_Current, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed", ref privateCount_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Modified
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Mortgage_Modified;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Mortgage_Modified", ref privateCount_Prevent_Mortgage_Deliquency_Mortgage_Modified, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Mortgage_Refinanced;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced", ref privateCount_Prevent_Mortgage_Deliquency_Mortgage_Refinanced, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Other
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Other;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Other", ref privateCount_Prevent_Mortgage_Deliquency_Other, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender", ref privateCount_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale", ref privateCount_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Referred_to_Legal
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Referred_to_Legal;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Referred_to_Legal", ref privateCount_Prevent_Mortgage_Deliquency_Referred_to_Legal, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv", ref privateCount_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated", ref privateCount_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Second_Mortgage_Received;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received", ref privateCount_Prevent_Mortgage_Deliquency_Second_Mortgage_Received, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Sold_Property_Alternative;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative", ref privateCount_Prevent_Mortgage_Deliquency_Sold_Property_Alternative, value);
            }
        }

        public Int32 Count_Prevent_Mortgage_Deliquency_Withdrew
        {
            get
            {
                return privateCount_Prevent_Mortgage_Deliquency_Withdrew;
            }
            set
            {
                DoSetProperty("count_Prevent_Mortgage_Deliquency_Withdrew", ref privateCount_Prevent_Mortgage_Deliquency_Withdrew, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Current_Counseled
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Current_Counseled;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Current_Counseled", ref privateCount_Seeking_Help_Housing_Current_Counseled, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Debt_Mngmt_Entered
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Debt_Mngmt_Entered;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Debt_Mngmt_Entered", ref privateCount_Seeking_Help_Housing_Debt_Mngmt_Entered, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Found_Alternative_Housing
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Found_Alternative_Housing;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Found_Alternative_Housing", ref privateCount_Seeking_Help_Housing_Found_Alternative_Housing, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Other
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Other;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Other", ref privateCount_Seeking_Help_Housing_Other, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Recertification_Subsidy_Program
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Recertification_Subsidy_Program;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Recertification_Subsidy_Program", ref privateCount_Seeking_Help_Housing_Recertification_Subsidy_Program, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Referred_Legal_Agency_Eviction
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Referred_Legal_Agency_Eviction;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Referred_Legal_Agency_Eviction", ref privateCount_Seeking_Help_Housing_Referred_Legal_Agency_Eviction, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Referred_Legal_Aid_Agency
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Referred_Legal_Aid_Agency;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Referred_Legal_Aid_Agency", ref privateCount_Seeking_Help_Housing_Referred_Legal_Aid_Agency, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Referred_Other_Social_Agency
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Referred_Other_Social_Agency;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Referred_Other_Social_Agency", ref privateCount_Seeking_Help_Housing_Referred_Other_Social_Agency, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Referred_to_Rental_Assistance
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Referred_to_Rental_Assistance;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Referred_to_Rental_Assistance", ref privateCount_Seeking_Help_Housing_Referred_to_Rental_Assistance, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Remain_CurrentHousing
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Remain_CurrentHousing;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Remain_CurrentHousing", ref privateCount_Seeking_Help_Housing_Remain_CurrentHousing, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Resolved_Issue
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Resolved_Issue;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Resolved_Issue", ref privateCount_Seeking_Help_Housing_Resolved_Issue, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Search_Assistance
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Search_Assistance;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Search_Assistance", ref privateCount_Seeking_Help_Housing_Search_Assistance, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Security_Dep_Dispute
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Security_Dep_Dispute;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Security_Dep_Dispute", ref privateCount_Seeking_Help_Housing_Security_Dep_Dispute, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Temp_Rental_Relief
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Temp_Rental_Relief;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Temp_Rental_Relief", ref privateCount_Seeking_Help_Housing_Temp_Rental_Relief, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Utilities_Current
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Utilities_Current;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Utilities_Current", ref privateCount_Seeking_Help_Housing_Utilities_Current, value);
            }
        }

        public Int32 Count_Seeking_Help_Housing_Withdrew_Counseling
        {
            get
            {
                return privateCount_Seeking_Help_Housing_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("count_Seeking_Help_Housing_Withdrew_Counseling", ref privateCount_Seeking_Help_Housing_Withdrew_Counseling, value);
            }
        }

        public Int32 Ethnicity_Clients_Counseling_Hispanic
        {
            get
            {
                return privateEthnicity_Clients_Counseling_Hispanic;
            }
            set
            {
                DoSetProperty("ethnicity_Clients_Counseling_Hispanic", ref privateEthnicity_Clients_Counseling_Hispanic, value);
            }
        }

        public Int32 Ethnicity_Clients_Counseling_No_Response
        {
            get
            {
                return privateEthnicity_Clients_Counseling_No_Response;
            }
            set
            {
                DoSetProperty("ethnicity_Clients_Counseling_No_Response", ref privateEthnicity_Clients_Counseling_No_Response, value);
            }
        }

        public Int32 Ethnicity_Clients_Counseling_Non_Hispanic
        {
            get
            {
                return privateEthnicity_Clients_Counseling_Non_Hispanic;
            }
            set
            {
                DoSetProperty("ethnicity_Clients_Counseling_Non_Hispanic", ref privateEthnicity_Clients_Counseling_Non_Hispanic, value);
            }
        }

        public Int32 AMI_No_Response
        {
            get
            {
                return privateAMI_No_Response;
            }
            set
            {
                DoSetProperty("aMI_No_Response", ref privateAMI_No_Response, value);
            }
        }

        public Int32 Greater100_AMI_Level
        {
            get
            {
                return privateGreater100_AMI_Level;
            }
            set
            {
                DoSetProperty("greater100_AMI_Level", ref privateGreater100_AMI_Level, value);
            }
        }

        public Int32 Lesser50_AMI_Level
        {
            get
            {
                return privateLesser50_AMI_Level;
            }
            set
            {
                DoSetProperty("lesser50_AMI_Level", ref privateLesser50_AMI_Level, value);
            }
        }

        public Int32 a50_79_AMI_Level
        {
            get
            {
                return privateA50_79_AMI_Level;
            }
            set
            {
                DoSetProperty("a50_79_AMI_Level", ref privateA50_79_AMI_Level, value);
            }
        }

        public Int32 a80_100_AMI_Level
        {
            get
            {
                return privateA80_100_AMI_Level;
            }
            set
            {
                DoSetProperty("a80_100_AMI_Level", ref privateA80_100_AMI_Level, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_AMINDWHT
        {
            get
            {
                return privateMultiRace_Clients_Counseling_AMINDWHT;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_AMINDWHT", ref privateMultiRace_Clients_Counseling_AMINDWHT, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_AMRCINDBLK
        {
            get
            {
                return privateMultiRace_Clients_Counseling_AMRCINDBLK;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_AMRCINDBLK", ref privateMultiRace_Clients_Counseling_AMRCINDBLK, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_ASIANWHT
        {
            get
            {
                return privateMultiRace_Clients_Counseling_ASIANWHT;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_ASIANWHT", ref privateMultiRace_Clients_Counseling_ASIANWHT, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_BLKWHT
        {
            get
            {
                return privateMultiRace_Clients_Counseling_BLKWHT;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_BLKWHT", ref privateMultiRace_Clients_Counseling_BLKWHT, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_NoResponse
        {
            get
            {
                return privateMultiRace_Clients_Counseling_NoResponse;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_NoResponse", ref privateMultiRace_Clients_Counseling_NoResponse, value);
            }
        }

        public Int32 MultiRace_Clients_Counseling_OtherMLTRC
        {
            get
            {
                return privateMultiRace_Clients_Counseling_OtherMLTRC;
            }
            set
            {
                DoSetProperty("multiRace_Clients_Counseling_OtherMLTRC", ref privateMultiRace_Clients_Counseling_OtherMLTRC, value);
            }
        }

        public Int32 Race_Clients_Counseling_American_Indian_Alaskan_Native
        {
            get
            {
                return privateRace_Clients_Counseling_American_Indian_Alaskan_Native;
            }
            set
            {
                DoSetProperty("race_Clients_Counseling_American_Indian_Alaskan_Native", ref privateRace_Clients_Counseling_American_Indian_Alaskan_Native, value);
            }
        }

        public Int32 Race_Clients_Counseling_Asian
        {
            get
            {
                return privateRace_Clients_Counseling_Asian;
            }
            set
            {
                DoSetProperty("race_Clients_Counseling_Asian", ref privateRace_Clients_Counseling_Asian, value);
            }
        }

        public Int32 Race_Clients_Counseling_Black_AfricanAmerican
        {
            get
            {
                return privateRace_Clients_Counseling_Black_AfricanAmerican;
            }
            set
            {
                DoSetProperty("race_Clients_Counseling_Black_AfricanAmerican", ref privateRace_Clients_Counseling_Black_AfricanAmerican, value);
            }
        }

        public Int32 Race_Clients_Counseling_Pacific_Islanders
        {
            get
            {
                return privateRace_Clients_Counseling_Pacific_Islanders;
            }
            set
            {
                DoSetProperty("race_Clients_Counseling_Pacific_Islanders", ref privateRace_Clients_Counseling_Pacific_Islanders, value);
            }
        }

        public Int32 Race_Clients_Counseling_White
        {
            get
            {
                return privateRace_Clients_Counseling_White;
            }
            set
            {
                DoSetProperty("race_Clients_Counseling_White", ref privateRace_Clients_Counseling_White, value);
            }
        }

        public Int32 Client_ID_Num
        {
            get
            {
                return privateClient_ID_Num;
            }
            set
            {
                DoSetProperty("client_ID_Num", ref privateClient_ID_Num, value);
            }
        }

        public Int32 Client_Purpose_Of_Visit
        {
            get
            {
                if (privateClient_Purpose_Of_Visit < 2)
                {
                    return 2;
                }
                return privateClient_Purpose_Of_Visit;
            }
            set
            {
                DoSetProperty("client_Purpose_Of_Visit", ref privateClient_Purpose_Of_Visit, value);
            }
        }

        public Int32 Client_Outcome_Of_Visit
        {
            get
            {
                if (privateClient_Outcome_Of_Visit < 2)
                {
                    return 2;
                }
                return privateClient_Outcome_Of_Visit;
            }
            set
            {
                DoSetProperty("Client_Outcome_Of_Visit", ref privateClient_Outcome_Of_Visit, value);
            }
        }

        public Int32 Client_Case_Num
        {
            get
            {
                if (privateClient_Case_Num <= 0)
                {
                    return privateClient_ID_Num;
                }
                return privateClient_Case_Num;
            }
            set
            {
                DoSetProperty("Client_Case_Num", ref privateClient_Case_Num, value);
            }
        }

        public Int32 Client_Counselor_ID
        {
            get
            {
                return privateClient_Counselor_ID;
            }
            set
            {
                DoSetProperty("Client_Counselor_ID", ref privateClient_Counselor_ID, value);
            }
        }

        public Int32 Client_Head_Of_Household_Type
        {
            get
            {
                if (privateClient_Head_Of_Household_Type < 2)
                {
                    return 2;
                }
                return privateClient_Head_Of_Household_Type;
            }
            set
            {
                DoSetProperty("client_Head_Of_Household_Type", ref privateClient_Head_Of_Household_Type, value);
            }
        }

        public Int32 Client_Credit_Score
        {
            get
            {
                return privateClient_Credit_Score;
            }
            set
            {
                DoSetProperty("client_Credit_Score", ref privateClient_Credit_Score, value);
            }
        }

        public Int32 Client_Credit_Score_Source
        {
            get
            {
                return privateClient_Credit_Score_Source;
            }
            set
            {
                DoSetProperty("client_Credit_Score_Source", ref privateClient_Credit_Score_Source, value);
            }
        }

        public Int32 Client_No_Credit_Score_Reason
        {
            get
            {
                return privateClient_No_Credit_Score_Reason;
            }
            set
            {
                DoSetProperty("client_No_Credit_Score_Reason", ref privateClient_No_Credit_Score_Reason, value);
            }
        }

        public string Client_First_Name
        {
            get
            {
                return privateClient_First_Name;
            }
            set
            {
                DoSetProperty("client_First_Name", ref privateClient_First_Name, value);
            }
        }

        public string Client_Last_Name
        {
            get
            {
                return privateClient_Last_Name;
            }
            set
            {
                DoSetProperty("client_Last_Name", ref privateClient_Last_Name, value);
            }
        }

        public string Client_Middle_Name
        {
            get
            {
                return privateClient_Middle_Name;
            }
            set
            {
                DoSetProperty("client_Middle_Name", ref privateClient_Middle_Name, value);
            }
        }

        public string Client_Street_Address_1
        {
            get
            {
                return privateClient_Street_Address_1;
            }
            set
            {
                DoSetProperty("client_Street_Address_1", ref privateClient_Street_Address_1, value);
            }
        }

        public string Client_Street_Address_2
        {
            get
            {
                return privateClient_Street_Address_2;
            }
            set
            {
                DoSetProperty("client_Street_Address_2", ref privateClient_Street_Address_2, value);
            }
        }

        public string Client_City
        {
            get
            {
                if (privateClient_City == string.Empty)
                {
                    return "-";
                }
                return privateClient_City;
            }
            set
            {
                DoSetProperty("client_City", ref privateClient_City, value);
            }
        }

        public Int32 Client_State
        {
            get
{
            if( privateClient_State < 2 )
{
    return 61; //NOT SPECIFIED
}
            return privateClient_State;
        }
            set
            {
                DoSetProperty("client_State", ref privateClient_State, value);
            }
        }

        public string Client_ZipCode
        {
            get
            {
                if (privateClient_ZipCode == string.Empty)
                {
                    return "00000";
                }
                return privateClient_ZipCode;
            }
            set
            {
                DoSetProperty("client_ZipCode", ref privateClient_ZipCode, value);
            }
        }

        public string Client_New_Street_Address_1
        {
            get
            {
                return privateClient_New_Street_Address_1;
            }
            set
            {
                DoSetProperty("client_New_Street_Address_1", ref privateClient_New_Street_Address_1, value);
            }
        }

        public string Client_New_Street_Address_2
        {
            get
            {
                return privateClient_New_Street_Address_2;
            }
            set
            {
                DoSetProperty("client_New_Street_Address_2", ref privateClient_New_Street_Address_2, value);
            }
        }

        public string Client_New_City
        {
            get
            {
                if (privateClient_New_City == string.Empty)
                {
                    return "-";
                }
                return privateClient_New_City;
            }
            set
            {
                DoSetProperty("client_New_City", ref privateClient_New_City, value);
            }
        }

        public Int32 Client_New_State
        {
            get
            {
                if (privateClient_New_State <= 1)
                {
                    return 61;
                }
                return privateClient_New_State;
            }
            set
            {
                DoSetProperty("client_New_State", ref privateClient_New_State, value);
            }
        }

        public string Client_New_ZipCode
        {
            get
            {
                if (privateClient_New_ZipCode == string.Empty)
                {
                    return "00000";
                }
                return privateClient_New_ZipCode;
            }
            set
            {
                DoSetProperty("client_New_ZipCode", ref privateClient_New_ZipCode, value);
            }
        }

        public string Client_Spouse_First_Name
        {
            get
            {
                return privateClient_Spouse_First_Name;
            }
            set
            {
                DoSetProperty("client_Spouse_First_Name", ref privateClient_Spouse_First_Name, value);
            }
        }

        public string Client_Spouse_Last_Name
        {
            get
            {
                return privateClient_Spouse_Last_Name;
            }
            set
            {
                DoSetProperty("client_Spouse_Last_Name", ref privateClient_Spouse_Last_Name, value);
            }
        }

        public string Client_Spouse_Middle_Name
        {
            get
            {
                return privateClient_Spouse_Middle_Name;
            }
            set
            {
                DoSetProperty("client_Spouse_Middle_Name", ref privateClient_Spouse_Middle_Name, value);
            }
        }

        public bool Client_Farm_Worker
        {
            get
            {
                return privateClient_Farm_Worker;
            }
            set
            {
                DoSetProperty("client_Farm_Worker", ref privateClient_Farm_Worker, value);
            }
        }

        public bool Client_Colonias_Resident
        {
            get
            {
                return privateClient_Colonias_Resident;
            }
            set
            {
                DoSetProperty("client_Colonias_Resident", ref privateClient_Colonias_Resident, value);
            }
        }

        public bool Client_Disabled
        {
            get
            {
                return privateClient_Disabled;
            }
            set
            {
                DoSetProperty("client_Disabled", ref privateClient_Disabled, value);
            }
        }

        public bool Client_HECM_Certificate
        {
            get
            {
                return privateClient_HECM_Certificate;
            }
            set
            {
                DoSetProperty("client_HECM_Certificate", ref privateClient_HECM_Certificate, value);
            }
        }

        public bool Client_Predatory_Lending
        {
            get
            {
                return privateClient_Predatory_Lending;
            }
            set
            {
                DoSetProperty("client_Predatory_Lending", ref privateClient_Predatory_Lending, value);
            }
        }

        public bool Client_FirstTime_Home_Buyer
        {
            get
            {
                return privateClient_FirstTime_Home_Buyer;
            }
            set
            {
                DoSetProperty("client_FirstTime_Home_Buyer", ref privateClient_FirstTime_Home_Buyer, value);
            }
        }

        public bool Client_Discrimination_Victim
        {
            get
            {
                return privateClient_Discrimination_Victim;
            }
            set
            {
                DoSetProperty("client_Discrimination_Victim", ref privateClient_Discrimination_Victim, value);
            }
        }

        public bool Client_Mortgage_Deliquency
        {
            get
            {
                return privateClient_Mortgage_Deliquency;
            }
            set
            {
                DoSetProperty("client_Mortgage_Deliquency", ref privateClient_Mortgage_Deliquency, value);
            }
        }

        public bool Client_Second_Loan_Exists
        {
            get
            {
                return privateClient_Second_Loan_Exists;
            }
            set
            {
                DoSetProperty("client_Second_Loan_Exists", ref privateClient_Second_Loan_Exists, value);
            }
        }

        public bool Client_Intake_Loan_Type_Is_Hybrid_ARM
        {
            get
            {
                return privateClient_Intake_Loan_Type_Is_Hybrid_ARM;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Is_Hybrid_ARM", ref privateClient_Intake_Loan_Type_Is_Hybrid_ARM, value);
            }
        }

        public bool Client_Intake_Loan_Type_Is_Option_ARM
        {
            get
            {
                return privateClient_Intake_Loan_Type_Is_Option_ARM;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Is_Option_ARM", ref privateClient_Intake_Loan_Type_Is_Option_ARM, value);
            }
        }

        public bool Client_Intake_Loan_Type_Is_Interest_Only
        {
            get
            {
                return privateClient_Intake_Loan_Type_Is_Interest_Only;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Is_Interest_Only", ref privateClient_Intake_Loan_Type_Is_Interest_Only, value);
            }
        }

        public bool Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured
        {
            get
            {
                return privateClient_Intake_Loan_Type_Is_FHA_Or_VA_Insured;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Is_FHA_Or_VA_Insured", ref privateClient_Intake_Loan_Type_Is_FHA_Or_VA_Insured, value);
            }
        }

        public bool Client_Intake_Loan_Type_Is_Privately_Held
        {
            get
            {
                return privateClient_Intake_Loan_Type_Is_Privately_Held;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Is_Privately_Held", ref privateClient_Intake_Loan_Type_Is_Privately_Held, value);
            }
        }

        public bool Client_Intake_Loan_Type_Has_Interest_Rate_Reset
        {
            get
            {
                return privateClient_Intake_Loan_Type_Has_Interest_Rate_Reset;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type_Has_Interest_Rate_Reset", ref privateClient_Intake_Loan_Type_Has_Interest_Rate_Reset, value);
            }
        }

        public Nullable<DateTime> Client_Counsel_Session_DT_Start
        {
            get
            {
                return privateClient_Counsel_Session_DT_Start;
            }
            set
            {
                DoSetProperty("client_Counsel_Session_DT_Start", ref privateClient_Counsel_Session_DT_Start, value);
            }
        }

        public DateTime Client_Counsel_Session_DT_End
        {
            get
            {
                return privateClient_Counsel_Session_DT_End;
            }
            set
            {
                DoSetProperty("client_Counsel_Session_DT_End", ref privateClient_Counsel_Session_DT_End, value);
            }
        }

        public Nullable<DateTime> Client_Intake_DT
        {
            get
            {
                return privateClient_Intake_DT;
            }
            set
            {
                DoSetProperty("client_Intake_DT", ref privateClient_Intake_DT, value);
            }
        }

        public DateTime Client_Birth_DT
        {
            get
            {
                return privateClient_Birth_DT;
            }
            set
            {
                DoSetProperty("client_Birth_DT", ref privateClient_Birth_DT, value);
            }
        }

        public DateTime Client_HECM_Certificate_Issue_Date
        {
            get
            {
                return privateClient_HECM_Certificate_Issue_Date;
            }
            set
            {
                DoSetProperty("client_HECM_Certificate_Issue_Date", ref privateClient_HECM_Certificate_Issue_Date, value);
            }
        }

        public DateTime Client_HECM_Certificate_Expiration_Date
        {
            get
            {
                return privateClient_HECM_Certificate_Expiration_Date;
            }
            set
            {
                DoSetProperty("client_HECM_Certificate_Expiration_Date", ref privateClient_HECM_Certificate_Expiration_Date, value);
            }
        }

        public string Client_HECM_Certificate_ID
        {
            get
            {
                return privateClient_HECM_Certificate_ID;
            }
            set
            {
                DoSetProperty("client_HECM_Certificate_ID", ref privateClient_HECM_Certificate_ID, value);
            }
        }

        public DateTime Client_Sales_Contract_Signed
        {
            get
            {
                return privateClient_Sales_Contract_Signed;
            }
            set
            {
                DoSetProperty("client_Sales_Contract_Signed", ref privateClient_Sales_Contract_Signed, value);
            }
        }

        public decimal Client_Grant_Amount_Used
        {
            get
            {
                return privateClient_Grant_Amount_Used;
            }
            set
            {
                DoSetProperty("client_Grant_Amount_Used", ref privateClient_Grant_Amount_Used, value);
            }
        }

        public decimal Client_Mortgage_Closing_Cost
        {
            get
            {
                return privateClient_Mortgage_Closing_Cost;
            }
            set
            {
                DoSetProperty("client_Mortgage_Closing_Cost", ref privateClient_Mortgage_Closing_Cost, value);
            }
        }

        public double Client_Mortgage_Interest_Rate
        {
            get
            {
                if (privateClient_Mortgage_Interest_Rate <= 0.0D)
                {
                    return 0.0D;
                }
                if (privateClient_Mortgage_Interest_Rate < 1.0D)
                { //Allow for my values of 0 to 1.0 rather than 0 - 100.0
                    return privateClient_Mortgage_Interest_Rate * 100.0D;
                }
                return privateClient_Mortgage_Interest_Rate;
            }
            set
            {
                DoSetProperty("client_Mortgage_Interest_Rate", ref privateClient_Mortgage_Interest_Rate, value);
            }
        }

        public Int32 Client_Counseling_Termination
        {
            get
            {
                if (privateClient_Counseling_Termination < 1)
                {
                    return 1;
                }
                return privateClient_Counseling_Termination;
            }
            set
            {
                DoSetProperty("client_Counseling_Termination", ref privateClient_Counseling_Termination, value);
            }
        }

        public Int32 Client_Income_Level
        {
            get
            {
                if (privateClient_Income_Level < 2)
                {
                    return 2;
                }
                return privateClient_Income_Level;
            }
            set
            {
                DoSetProperty("client_Income_Level", ref privateClient_Income_Level, value);
            }
        }

        public Int32 Client_Highest_Educ_Grade
        {
            get
            {
                if (privateClient_Highest_Educ_Grade < 1)
                {
                    return 1;
                }
                return privateClient_Highest_Educ_Grade;
            }
            set
            {
                DoSetProperty("client_Highest_Educ_Grade", ref privateClient_Highest_Educ_Grade, value);
            }
        }

        public Int32 Client_HUD_Assistance
        {
            get
            {
                if (privateClient_HUD_Assistance < 2)
                {
                    return 2;
                }
                return privateClient_HUD_Assistance;
            }
            set
            {
                DoSetProperty("client_HUD_Assistance", ref privateClient_HUD_Assistance, value);
            }
        }

        public Int32 Client_Dependents_Num
        {
            get
            {
                return privateClient_Dependents_Num;
            }
            set
            {
                DoSetProperty("client_Dependents_Num", ref privateClient_Dependents_Num, value);
            }
        }

        public Int32 Client_Language_Spoken
        {
            get
            {
                if (privateClient_Language_Spoken < 2)
                {
                    return 2;
                }
                return privateClient_Language_Spoken;
            }
            set
            {
                DoSetProperty("client_Language_Spoken", ref privateClient_Language_Spoken, value);
            }
        }

        public Int32 Client_Session_Duration
        {
            get
            {
                if (privateClient_Session_Duration < 2)
                {
                    return 1;
                }
                return privateClient_Session_Duration;
            }
            set
            {
                DoSetProperty("client_Session_Duration", ref privateClient_Session_Duration, value);
            }
        }

        public Int32 Client_Counseling_Type
        {
            get
            {
                if (privateClient_Counseling_Type < 2)
                {
                    return 2;
                }
                return privateClient_Counseling_Type;
            }
            set
            {
                DoSetProperty("client_Counseling_Type", ref privateClient_Counseling_Type, value);
            }
        }

        public decimal Client_Counseling_Fee
        {
            get
            {
                if (privateClient_Counseling_Fee < 1)
                {
                    return 1;
                }
                return privateClient_Counseling_Fee;
            }
            set
            {
                DoSetProperty("client_Counseling_Fee", ref privateClient_Counseling_Fee, value);
            }
        }

        public Int32 Client_Attribute_HUD_Grant
        {
            get
            {
                if (privateClient_Attribute_HUD_Grant < 2)
                {
                    return 2;
                }
                return privateClient_Attribute_HUD_Grant;
            }
            set
            {
                DoSetProperty("client_Attribute_HUD_Grant", ref privateClient_Attribute_HUD_Grant, value);
            }
        }

        public Int32 Client_Finance_Type_Before
        {
            get
            {
                if (privateClient_Finance_Type_Before < 6)
                {
                    return 6;
                }
                return privateClient_Finance_Type_Before;
            }
            set
            {
                DoSetProperty("client_Finance_Type_Before", ref privateClient_Finance_Type_Before, value);
            }
        }

        public Int32 Client_Finance_Type_After
        {
            get
            {
                if (privateClient_Finance_Type_After < 6)
                {
                    return 6;
                }
                return privateClient_Finance_Type_After;
            }
            set
            {
                DoSetProperty("client_Finance_Type_After", ref privateClient_Finance_Type_After, value);
            }
        }

        public Int32 Client_Mortgage_Type
        {
            get
            {
                if (privateClient_Mortgage_Type < 1)
                {
                    return 13;
                }
                return privateClient_Mortgage_Type;
            }
            set
            {
                DoSetProperty("client_Mortgage_Type", ref privateClient_Mortgage_Type, value);
            }
        }

        public Int32 Client_Mortgage_Type_After
        {
            get
            {
                if (privateClient_Mortgage_Type_After < 1)
                {
                    return 13;
                }
                return privateClient_Mortgage_Type_After;
            }
            set
            {
                DoSetProperty("client_Mortgage_Type_After", ref privateClient_Mortgage_Type_After, value);
            }
        }

        public Int32 Client_referred_by
        {
            get
            {
                if (privateClient_referred_by < 2)
                {
                    return 2;
                }
                return privateClient_referred_by;
            }
            set
            {
                DoSetProperty("client_referred_by", ref privateClient_referred_by, value);
            }
        }

        public Int32 Client_Job_Duration
        {
            get
            {
                if (privateClient_Job_Duration <= 0)
                {
                    return 0;
                }
                return privateClient_Job_Duration;
            }
            set
            {
                DoSetProperty("client_Job_Duration", ref privateClient_Job_Duration, value);
            }
        }

        public Int32 Client_Household_Debt
        {
            get
            {
                if (privateClient_Household_Debt < 2)
                {
                    return 1;
                }
                return privateClient_Household_Debt;
            }
            set
            {
                DoSetProperty("client_Household_Debt", ref privateClient_Household_Debt, value);
            }
        }

        public string Client_Loan_Being_Reported
        {
            get
            {
                if (string.IsNullOrEmpty(privateClient_Loan_Being_Reported))
                {
                    return "First";
                }
                return privateClient_Loan_Being_Reported;
            }
            set
            {
                DoSetProperty("client_Loan_Being_Reported", ref privateClient_Loan_Being_Reported, value);
            }
        }

        public Int32 Client_Intake_Loan_Type
        {
            get
            {
                if (privateClient_Intake_Loan_Type < 6)
                {
                    return 6;
                }
                return privateClient_Intake_Loan_Type;
            }
            set
            {
                DoSetProperty("client_Intake_Loan_Type", ref privateClient_Intake_Loan_Type, value);
            }
        }

        public Int32 Client_Family_Size
        {
            get
            {
                if (privateClient_Family_Size < 1)
                {
                    return 1;
                }
                return privateClient_Family_Size;
            }
            set
            {
                DoSetProperty("client_Family_Size", ref privateClient_Family_Size, value);
            }
        }

        public Int32 Client_Marital_Status
        {
            get
            {
                if (privateClient_Family_Size < 1)
                {
                    return 1;
                }
                return privateClient_Marital_Status;
            }
            set
            {
                DoSetProperty("client_Marital_Status", ref privateClient_Marital_Status, value);
            }
        }

        public Int32 Client_Race_ID
        {
            get
            {
                if (privateClient_Race_ID < 2)
                {
                    return 2;
                }
                return privateClient_Race_ID;
            }
            set
            {
                DoSetProperty("client_Race_ID", ref privateClient_Race_ID, value);
            }
        }

        public Int32 Client_Ethnicity_ID
        {
            get
            {
                if (privateClient_Ethnicity_ID < 1)
                {
                    return 4;
                }
                return privateClient_Ethnicity_ID;
            }
            set
            {
                DoSetProperty("client_Ethnicity_ID", ref privateClient_Ethnicity_ID, value);
            }
        }

        public decimal Client_Household_Gross_Monthly_Income
        {
            get
            {
                if (privateClient_Household_Gross_Monthly_Income < 1)
                {
                    return 1M;
                }
                return privateClient_Household_Gross_Monthly_Income;
            }
            set
            {
                DoSetProperty("client_Household_Gross_Monthly_Income", ref privateClient_Household_Gross_Monthly_Income, value);
            }
        }

        public string Client_Gender
        {
            get
            {
                switch ((privateClient_Gender + " ").Substring(0, 1).ToUpper())
                {
                    case "M":
                    case "F":
                        return privateClient_Gender;
                    default:
                        return "M";
                }
            }
            set
            {
                DoSetProperty("client_Gender", ref privateClient_Gender, value);
            }
        }

        public string Client_Spouse_SSN
        {
            get
            {
                return privateClient_Spouse_SSN;
            }
            set
            {
                DoSetProperty("client_Spouse_SSN", ref privateClient_Spouse_SSN, value);
            }
        }

        public string Client_SSN1
        {
            get
            {
                return privateClient_SSN1;
            }
            set
            {
                DoSetProperty("client_SSN1", ref privateClient_SSN1, value);
            }
        }

        public string Client_SSN2
        {
            get
            {
                return privateClient_SSN2;
            }
            set
            {
                DoSetProperty("client_SSN2", ref privateClient_SSN2, value);
            }
        }

        public string Client_Mobile_Phone_Num
        {
            get
            {
                return privateClient_Mobile_Phone_Num;
            }
            set
            {
                DoSetProperty("client_Mobile_Phone_Num", ref privateClient_Mobile_Phone_Num, value);
            }
        }

        public string Client_Phone_Num
        {
            get
            {
                return privateClient_Phone_Num;
            }
            set
            {
                DoSetProperty("client_Phone_Num", ref privateClient_Phone_Num, value);
            }
        }

        public string Client_Fax
        {
            get
            {
                return privateClient_Fax;
            }
            set
            {
                DoSetProperty("client_Fax", ref privateClient_Fax, value);
            }
        }

        public string Client_Email
        {
            get
            {
                return privateClient_Email;
            }
            set
            {
                DoSetProperty("client_Email", ref privateClient_Email, value);
            }
        }

        public Int32 group_session_id
        {
            get
            {
                return privateGroup_session_id;
            }
            set
            {
                DoSetProperty("group_session_id", ref privateGroup_session_id, value);
            }
        }

        public Int32 person_1
        {
            get
            {
                return privatePerson_1;
            }
            set
            {
                DoSetProperty("person_1", ref privatePerson_1, value);
            }
        }

        public Int32 person_2
        {
            get
            {
                return privatePerson_2;
            }
            set
            {
                DoSetProperty("person_2", ref privatePerson_2, value);
            }
        }

        public Int32 hud_interview
        {
            get
            {
                return privateHud_interview;
            }
            set
            {
                DoSetProperty("hud_interview", ref privateHud_interview, value);
            }
        }

        public Int32 interview_type
        {
            get
            {
                return privateInterview_type;
            }
            set
            {
                DoSetProperty("interview_type", ref privateInterview_type, value);
            }
        }

        public Int32 hud_result
        {
            get
            {
                return privateHud_result;
            }
            set
            {
                DoSetProperty("hud_result", ref privateHud_result, value);
            }
        }

        public Nullable<DateTime> interview_date
        {
            get
            {
                return privateInterview_date;
            }
            set
            {
                DoSetProperty("interview_date", ref privateInterview_date, value);
            }
        }

        public Nullable<DateTime> result_date
        {
            get
            {
                return privateResult_date;
            }
            set
            {
                DoSetProperty("result_date", ref privateResult_date, value);
            }
        }

        public Nullable<DateTime> termination_date
        {
            get
            {
                return privateTermination_date;
            }
            set
            {
                DoSetProperty("termination_date", ref privateTermination_date, value);
            }
        }

        public string interview_result
        {
            get
            {
                return privateInterview_result;
            }
            set
            {
                DoSetProperty("interview_result", ref privateInterview_result, value);
            }
        }

        public decimal ami
        {
            get
            {
                return privateAmi;
            }
            set
            {
                DoSetProperty("ami", ref privateAmi, value);
            }
        }

        public Int32 client_appointment
        {
            get
            {
                return privateClient_appointment;
            }
            set
            {
                DoSetProperty("Client_appointment", ref privateClient_appointment, value);
            }
        }

        public Int32 housing_property
        {
            get
            {
                return privateHousing_property;
            }
            set
            {
                DoSetProperty("Housing_property", ref privateHousing_property, value);
            }
        }

        public Int32 primary_loan
        {
            get
            {
                return privatePrimary_loan;
            }
            set
            {
                DoSetProperty("primary_loan", ref privatePrimary_loan, value);
            }
        }

        public Int32 secondary_loan
        {
            get
            {
                return privateSecondary_loan;
            }
            set
            {
                DoSetProperty("Secondary_loan", ref privateSecondary_loan, value);
            }
        }

        #endregion

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {

            xml.WriteStartElement("tns:Client_Profile");
            xml.WriteStartElement("tns:Client_ID_Num");
            xml.WriteValue(Client_ID_Num);
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Case_Num");
            xml.WriteValue(Client_Case_Num);
            xml.WriteEndElement();

            if (!string.IsNullOrEmpty(Client_SSN1))
            {
                xml.WriteStartElement("tns:Client_SSN1");
                xml.WriteValue(FmtSSN(Client_SSN1));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_SSN2))
            {
                xml.WriteStartElement("tns:Client_SSN2");
                xml.WriteValue(FmtSSNLast(Client_SSN2));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_First_Name))
            {
                xml.WriteStartElement("tns:Client_First_Name");
                xml.WriteValue(FmtString(Client_First_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Last_Name))
            {
                xml.WriteStartElement("tns:Client_Last_Name");
                xml.WriteValue(FmtString(Client_Last_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Middle_Name))
            {
                xml.WriteStartElement("tns:Client_Middle_Name");
                xml.WriteValue(FmtString(Client_Middle_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Street_Address_1))
            {
                xml.WriteStartElement("tns:Client_Street_Address_1");
                xml.WriteValue(FmtString(Client_Street_Address_1));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Street_Address_2))
            {
                xml.WriteStartElement("tns:Client_Street_Address_2");
                xml.WriteValue(FmtString(Client_Street_Address_2));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_City))
            {
                xml.WriteStartElement("tns:Client_City");
                xml.WriteValue(FmtString(Client_City));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_State > 0)
            {
                xml.WriteStartElement("tns:Client_State");
                xml.WriteValue(FmtInt32(Client_State));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_ZipCode))
            {
                xml.WriteStartElement("tns:Client_Zip");
                xml.WriteValue(FmtString(Client_ZipCode));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_New_Street_Address_1))
            {
                xml.WriteStartElement("tns:Client_New_Street_Address_1");
                xml.WriteValue(FmtString(Client_New_Street_Address_1));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_New_Street_Address_2))
            {
                xml.WriteStartElement("tns:Client_New_Street_Address_2");
                xml.WriteValue(FmtString(Client_New_Street_Address_2));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_New_City))
            {
                xml.WriteStartElement("tns:Client_New_City");
                xml.WriteValue(FmtString(Client_New_City));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_New_State > 0)
            {
                xml.WriteStartElement("tns:Client_New_State");
                xml.WriteValue(FmtInt32(Client_New_State));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_New_ZipCode))
            {
                xml.WriteStartElement("tns:Client_New_Zip");
                xml.WriteValue(FmtString(Client_New_ZipCode));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_Phone_Num))
            {
                xml.WriteStartElement("tns:Client_Phone_Num");
                xml.WriteValue(FmtTelephoneNumber(Client_Phone_Num));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Mobile_Phone_Num))
            {
                xml.WriteStartElement("tns:Client_Mobile_Phone_Num");
                xml.WriteValue(FmtTelephoneNumber(Client_Mobile_Phone_Num));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Fax))
            {
                xml.WriteStartElement("tns:Client_Fax");
                xml.WriteValue(FmtTelephoneNumber(Client_Fax));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Email))
            {
                xml.WriteStartElement("tns:Client_Email");
                xml.WriteValue(FmtString(Client_Email));
                xml.WriteEndElement();
            }

            if (Client_Family_Size > 0)
            {
                xml.WriteStartElement("tns:Client_Family_Size");
                xml.WriteValue(FmtInt32(Client_Family_Size));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_Gender))
            {
                xml.WriteStartElement("tns:Client_Gender");
                xml.WriteValue(FmtString(Client_Gender));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Marital_Status > 0)
            {
                xml.WriteStartElement("tns:Client_Marital_Status");
                xml.WriteValue(FmtInt32(Client_Marital_Status));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Race_ID > 0)
            {
                xml.WriteStartElement("tns:Client_Race_ID");
                xml.WriteValue(FmtInt32(Client_Race_ID));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Ethnicity_ID > 0)
            {
                xml.WriteStartElement("tns:Client_Ethnicity_ID");
                xml.WriteValue(FmtInt32(Client_Ethnicity_ID));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Household_Gross_Monthly_Income > 0M)
            {
                xml.WriteStartElement("tns:Client_Household_Gross_Monthly_Income");
                xml.WriteValue(FmtInt32(Convert.ToInt32(Client_Household_Gross_Monthly_Income)));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Head_Of_Household_Type > 0)
            {
                xml.WriteStartElement("tns:Client_Head_Of_Household_Type");
                xml.WriteValue(FmtInt32(Client_Head_Of_Household_Type));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Birth_DT != DateTime.MinValue)
            {
                xml.WriteStartElement("tns:Client_Birth_DT");
                xml.WriteValue(FmtDate(Client_Birth_DT));
                xml.WriteEndElement();
            }

            xml.WriteStartElement("tns:Client_Counselor_ID");
            xml.WriteValue(Client_Counselor_ID);
            xml.WriteEndElement();

            if (Client_Highest_Educ_Grade > 0)
            {
                xml.WriteStartElement("tns:Client_Highest_Educ_Grade");
                xml.WriteValue(FmtInt32(Client_Highest_Educ_Grade));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_Farm_Worker");
            xml.WriteValue(FmtYN(Client_Farm_Worker));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Colonias_Resident");
            xml.WriteValue(FmtYN(Client_Colonias_Resident));
            xml.WriteEndElement();

            if (Client_HUD_Assistance > 0)
            {
                xml.WriteStartElement("tns:Client_HUD_Assistance");
                xml.WriteValue(FmtInt32(Client_HUD_Assistance));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_Disabled");
            xml.WriteValue(FmtYN(Client_Disabled));
            xml.WriteEndElement();

            if (Client_Dependents_Num >= 0)
            {
                xml.WriteStartElement("tns:Client_Dependents_Num");
                xml.WriteValue(FmtInt32(Client_Dependents_Num));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_Intake_DT");
            xml.WriteValue(FmtDate(Client_Intake_DT));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Counsel_Session_DT_Start");
            xml.WriteValue(FmtDate(Client_Counsel_Session_DT_Start, true));
            xml.WriteEndElement();

            if (Client_Counsel_Session_DT_End != DateTime.MinValue)
            {
                xml.WriteStartElement("tns:Client_Counsel_Session_DT_End");
                xml.WriteValue(FmtDate(Client_Counsel_Session_DT_End, true));
                xml.WriteEndElement();
            }

            if (Client_Language_Spoken > 0)
            {
                xml.WriteStartElement("tns:Client_Language_Spoken");
                xml.WriteValue(FmtInt32(Client_Language_Spoken));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Session_Duration > 0)
            {
                xml.WriteStartElement("tns:Client_Session_Duration");
                xml.WriteValue(FmtInt32(Client_Session_Duration));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Counseling_Type > 0)
            {
                xml.WriteStartElement("tns:Client_Counseling_Type");
                xml.WriteValue(FmtInt32(Client_Counseling_Type));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Counseling_Termination > 0)
            {
                xml.WriteStartElement("tns:Client_Counseling_Termination");
                xml.WriteValue(FmtInt32(Client_Counseling_Termination));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Counseling_Fee > 0M)
            {
                xml.WriteStartElement("tns:Client_Counseling_Fee");
                xml.WriteValue(FmtInt32(Convert.ToInt32(Client_Counseling_Fee)));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Attribute_HUD_Grant > 0)
            {
                xml.WriteStartElement("tns:Client_Attribute_HUD_Grant");
                xml.WriteValue(FmtInt32(Client_Attribute_HUD_Grant));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Grant_Amount_Used > 0M)
            {
                xml.WriteStartElement("tns:Client_Grant_Amount_Used");
                xml.WriteValue(FmtInt32(Convert.ToInt32(Client_Grant_Amount_Used)));
                xml.WriteEndElement();
            }

            xml.WriteStartElement("tns:Client_HECM_Certificate");
            xml.WriteValue(FmtYN(Client_HECM_Certificate));
            xml.WriteEndElement();

            if (Client_HECM_Certificate_Issue_Date != DateTime.MinValue)
            {
                xml.WriteStartElement("tns:Client_HECM_Certificate_Issue_Date");
                xml.WriteValue(FmtDate(Client_HECM_Certificate_Issue_Date));
                xml.WriteEndElement();
            }

            if (Client_HECM_Certificate_Expiration_Date != DateTime.MinValue)
            {
                xml.WriteStartElement("tns:Client_HECM_Certificate_Expiration_Date");
                xml.WriteValue(FmtDate(Client_HECM_Certificate_Expiration_Date));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_HECM_Certificate_ID))
            {
                xml.WriteStartElement("tns:Client_HECM_Certificate_ID");
                xml.WriteValue(FmtString(Client_HECM_Certificate_ID));
                xml.WriteEndElement();
            }

            xml.WriteStartElement("tns:Client_Predatory_Lending");
            xml.WriteValue(FmtYN(Client_Predatory_Lending));
            xml.WriteEndElement();

            if (Client_Mortgage_Type > 0)
            {
                xml.WriteStartElement("tns:Client_Mortgage_Type");
                xml.WriteValue(FmtInt32(Client_Mortgage_Type));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Mortgage_Type_After > 0)
            {
                xml.WriteStartElement("tns:Client_Mortgage_Type_After");
                xml.WriteValue(FmtInt32(Client_Mortgage_Type_After));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Finance_Type_Before > 0)
            {
                xml.WriteStartElement("tns:Client_Finance_Type_Before");
                xml.WriteValue(FmtInt32(Client_Finance_Type_Before));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Finance_Type_After > 0)
            {
                xml.WriteStartElement("tns:Client_Finance_Type_After");
                xml.WriteValue(FmtInt32(Client_Finance_Type_After));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_FirstTime_Home_Buyer");
            xml.WriteValue(FmtYN(Client_FirstTime_Home_Buyer));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Discrimination_Victim");
            xml.WriteValue(FmtYN(Client_Discrimination_Victim));
            xml.WriteEndElement();

            if (Client_Mortgage_Closing_Cost > 0M)
            {
                xml.WriteStartElement("tns:Client_Mortgage_Closing_Cost");
                xml.WriteValue(FmtInt32(Convert.ToInt32(Client_Mortgage_Closing_Cost)));
                xml.WriteEndElement();
            }

            if (Client_Mortgage_Interest_Rate > 0.0D)
            {
                xml.WriteStartElement("tns:Client_Mortgage_Interest_Rate");
                xml.WriteValue(string.Format("{0:00.000}", Client_Mortgage_Interest_Rate));
                xml.WriteEndElement();
            }

            if (Client_referred_by > 0)
            {
                xml.WriteStartElement("tns:Client_referred_by");
                xml.WriteValue(FmtInt32(Client_referred_by));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Sales_Contract_Signed != DateTime.MinValue)
            {
                xml.WriteStartElement("tns:Client_Sales_Contract_Signed");
                xml.WriteValue(FmtDate(Client_Sales_Contract_Signed));
                xml.WriteEndElement();
            }

            xml.WriteStartElement("tns:Client_Job_Duration");
            xml.WriteValue(FmtInt32(Client_Job_Duration));
            xml.WriteEndElement();

            if (Client_Credit_Score > 0)
            {
                xml.WriteStartElement("tns:Client_Credit_Score");
                xml.WriteValue(FmtInt32(Client_Credit_Score));
                xml.WriteEndElement();
            }

            if (Client_Credit_Score_Source > 0)
            {
                xml.WriteStartElement("tns:Client_Credit_Score_Source");
                xml.WriteValue(FmtInt32(Client_Credit_Score_Source));
                xml.WriteEndElement();
            }

            if (Client_No_Credit_Score_Reason > 0)
            {
                xml.WriteStartElement("tns:Client_No_Credit_Score_Reason");
                xml.WriteValue(FmtInt32(Client_No_Credit_Score_Reason));
                xml.WriteEndElement();
            }

            if (Client_Household_Debt > 0)
            {
                xml.WriteStartElement("tns:Client_Household_Debt");
                xml.WriteValue(FmtInt32(Client_Household_Debt));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (!string.IsNullOrEmpty(Client_Loan_Being_Reported))
            {
                xml.WriteStartElement("tns:Client_Loan_Being_Reported");
                xml.WriteValue(FmtString(Client_Loan_Being_Reported));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_Second_Loan_Exists");
            xml.WriteValue(FmtYN(Client_Second_Loan_Exists));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Mortgage_Deliquency");
            xml.WriteValue(FmtYN(Client_Mortgage_Deliquency));
            xml.WriteEndElement();

            if (Client_Intake_Loan_Type > 0)
            {
                xml.WriteStartElement("tns:Client_Intake_Loan_Type");
                xml.WriteValue(FmtInt32(Client_Intake_Loan_Type));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Is_Hybrid_ARM");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Is_Hybrid_ARM));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Is_Option_ARM");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Is_Option_ARM));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Is_Interest_Only");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Is_Interest_Only));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Is_Privately_Held");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Is_Privately_Held));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Client_Intake_Loan_Type_Has_Interest_Rate_Reset");
            xml.WriteValue(FmtYN(Client_Intake_Loan_Type_Has_Interest_Rate_Reset));
            xml.WriteEndElement();

            if (!string.IsNullOrEmpty(Client_Spouse_First_Name))
            {
                xml.WriteStartElement("tns:Client_Spouse_First_Name");
                xml.WriteValue(FmtString(Client_Spouse_First_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Spouse_Last_Name))
            {
                xml.WriteStartElement("tns:Client_Spouse_Last_Name");
                xml.WriteValue(FmtString(Client_Spouse_Last_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Spouse_Middle_Name))
            {
                xml.WriteStartElement("tns:Client_Spouse_Middle_Name");
                xml.WriteValue(FmtString(Client_Spouse_Middle_Name));
                xml.WriteEndElement();
            }

            if (!string.IsNullOrEmpty(Client_Spouse_SSN))
            {
                xml.WriteStartElement("tns:Client_Spouse_SSN");
                xml.WriteValue(FmtSSN(Client_Spouse_SSN));
                xml.WriteEndElement();
            }

            if (Client_Income_Level > 0)
            {
                xml.WriteStartElement("tns:Client_Income_Level");
                xml.WriteValue(FmtInt32(Client_Income_Level));
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Purpose_Of_Visit > 0)
            {
                xml.WriteStartElement("tns:Client_Purpose_Of_Visit");
                xml.WriteValue(Client_Purpose_Of_Visit);
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            if (Client_Outcome_Of_Visit > 0)
            {
                xml.WriteStartElement("tns:Client_Outcome_Of_Visit");
                xml.WriteValue(Client_Outcome_Of_Visit);
                xml.WriteEndElement();
            }
            else
            {
                System.Diagnostics.Debug.Assert(false);
            }

            xml.WriteEndElement();
        }
    }
}