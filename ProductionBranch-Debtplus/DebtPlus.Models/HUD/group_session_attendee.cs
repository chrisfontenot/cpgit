#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class group_session_attendee : ExtractBase
    {

        #region storage
        private Int32 private_attendee_id;
        public Int32 attendee_id
        {
            get
            {
                return private_attendee_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("attendee_id", ref private_attendee_id, value);
            }
        }

        private decimal private_Attendee_Fee_Amount;
        public decimal Attendee_Fee_Amount
        {
            get
            {
                return private_Attendee_Fee_Amount;
            }
            set
            {
                DoSetProperty("Attendee_Fee_Amount", ref private_Attendee_Fee_Amount, value);
            }
        }

        private Int32 private_Attendee_referred_by;
        public Int32 Attendee_referred_by
        {
            get
            {
                return private_Attendee_referred_by;
            }
            set
            {
                DoSetProperty("Attendee_referred_by", ref private_Attendee_referred_by, value);
            }
        }

        private bool private_Attendee_FirstTime_Home_Buyer;
        public bool Attendee_FirstTime_Home_Buyer
        {
            get
            {
                return private_Attendee_FirstTime_Home_Buyer;
            }
            set
            {
                DoSetProperty("Attendee_FirstTime_Home_Buyer", ref private_Attendee_FirstTime_Home_Buyer, value);
            }
        }
        #endregion

        private Int32 private_group_session_id = 0;
        public Int32 group_session_id
        {
            get
            {
                return private_group_session_id;
            }
            set
            {
                DoSetProperty("group_session_id", ref private_group_session_id, value);
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:group_session_attendee");
            xml.WriteElementString("tns:attendee_id", FmtInt32(attendee_id));
            xml.WriteElementString("tns:Attendee_Fee_Amount", FmtInt32(Convert.ToInt32(Attendee_Fee_Amount)));
            xml.WriteElementString("tns:Attendee_referred_by", FmtInt32(Attendee_referred_by));
            xml.WriteElementString("tns:Attendee_FirstTime_Home_Buyer", FmtYN(Attendee_FirstTime_Home_Buyer));
            xml.WriteEndElement();
        }
    }
}
