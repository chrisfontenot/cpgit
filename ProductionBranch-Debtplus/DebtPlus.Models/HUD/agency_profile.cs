#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public partial class AgencyProfileData
    {
        public AgencyProfileData()
        {
        }

        public AgencyProfileData(string UserName, string Password)
        {
            privateUserName = UserName;
            privatePassword = Password;
        }

        #region Version 3.0 compatability
        public string agc_address1
        {
            get
            {
                return agc_physical_address1;
            }
            set
            {
                agc_physical_address1 = value;
                agc_mailing_address1 = value;
            }
        }

        public string agc_address2
        {
            get
            {
                return agc_physical_address2;
            }
            set
            {
                agc_physical_address2 = value;
                agc_mailing_address2 = value;
            }
        }

        public string agc_address3
        {
            get
            {
                return agc_physical_address3;
            }
            set
            {
                agc_physical_address3 = value;
                agc_mailing_address3 = value;
            }
        }

        public string agc_address4
        {
            get
            {
                return agc_physical_address4;
            }
            set
            {
                agc_physical_address4 = value;
                agc_mailing_address4 = value;
            }
        }

        public string agc_city
        {
            get
            {
                return agc_physical_city;
            }
            set
            {
                agc_physical_city = value;
                agc_mailing_city = value;
            }
        }

        public Int32 agc_state
        {
            get
            {
                return agc_physical_state;
            }
            set
            {
                agc_physical_state = value;
                agc_mailing_state = value;
            }
        }

        public string agc_zip
        {
            get
            {
                return agc_physical_zip;
            }
            set
            {
                agc_physical_zip = value;
                agc_mailing_zip = value;
            }
        }

        public Int32 agc_counselin_budget_amt
        {
            get
            {
                return agc_counseling_budget_amount;
            }
            set
            {
                agc_counseling_budget_amount = value;
            }
        }

        public string alternate_phone_nbr
        {
            get
            {
                return agc_tollfree_phone_nbr;
            }
            set
            {
                agc_tollfree_phone_nbr = value;
            }
        }
        #endregion

        #region storage
        private Int32 privateagc_hcs_id;
        public Int32 agc_hcs_id
        {
            get
            {
                return privateagc_hcs_id;
            }
            set
            {
                DoSetProperty("agc_hcs_id", ref privateagc_hcs_id, value);
            }
        }

        private Int32 privatefiscal_year = 12;
        public Int32 fiscal_year
        {
            get
            {
                return privatefiscal_year;
            }
            set
            {
                DoSetProperty("fiscal_year", ref privatefiscal_year, value);
            }
        }

        private Int32 privatefiscal_quarter = 1;
        public Int32 fiscal_quarter
        {
            get
            {
                return privatefiscal_quarter;
            }
            set
            {
                if (value <= 0)
                {
                    value = 1;
                }
                DoSetProperty("fiscal_quarter", ref privatefiscal_quarter, value);
            }
        }

        private Int32 privatereported_month = 1;
        public Int32 reported_month
        {
            get
            {
                return privatereported_month;
            }
            set
            {
                if (value <= 0)
                {
                    value = 1;
                }
                DoSetProperty("reported_month", ref privatereported_month, value);
            }
        }

        private string privateagc_name = "-";
        public string agc_name
        {
            get
            {
                return privateagc_name;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_name", ref privateagc_name, value);
            }
        }

        private string privateagc_ein = "00-0000000";
        public string agc_ein
        {
            get
            {
                return privateagc_ein;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "00-0000000";
                }
                if (value.Length > 10)
                {
                    value = value.Substring(0, 10);
                }
                DoSetProperty("agc_ein", ref privateagc_ein, value);
            }
        }

        private string privateagc_dun_nbr = "000000000";
        public string agc_dun_nbr
        {
            get
            {
                return privateagc_dun_nbr;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "000000000";
                }
                if (value.Length > 9)
                {
                    value = value.Substring(0, 9);
                }
                DoSetProperty("agc_dun_nbr", ref privateagc_dun_nbr, value);
            }
        }

        private string privateagc_physical_address1 = "-";
        public string agc_physical_address1
        {
            get
            {
                return privateagc_physical_address1;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_physical_address1", ref privateagc_physical_address1, value);
            }
        }

        private string privateagc_physical_address2 = "-";
        public string agc_physical_address2
        {
            get
            {
                return privateagc_physical_address2;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_physical_address2", ref privateagc_physical_address2, value);
            }
        }

        private string privateagc_physical_address3 = "-";
        public string agc_physical_address3
        {
            get
            {
                return privateagc_physical_address3;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_physical_address3", ref privateagc_physical_address3, value);
            }
        }

        private string privateagc_physical_address4 = "-";
        public string agc_physical_address4
        {
            get
            {
                return privateagc_physical_address4;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_physical_address4", ref privateagc_physical_address4, value);
            }
        }

        private string privateagc_physical_city = "-";
        public string agc_physical_city
        {
            get
            {
                return privateagc_physical_city;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 40)
                {
                    value = value.Substring(0, 40);
                }
                DoSetProperty("agc_physical_city", ref privateagc_physical_city, value);
            }
        }

        private Int32 privateagc_physical_state = DEFAULT_STATE;
        public Int32 agc_physical_state
        {
            get
            {
                return privateagc_physical_state;
            }
            set
            {
                DoSetProperty("agc_physical_state", ref privateagc_physical_state, value);
            }
        }

        private string privateagc_physical_zip = "00000";
        public string agc_physical_zip
        {
            get
            {
                return privateagc_physical_zip;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "00000";
                }
                if (value.Length > 5)
                {
                    value = value.Substring(0, 5);
                }
                DoSetProperty("agc_physical_zip", ref privateagc_physical_zip, value);
            }
        }

        private string privateagc_mailing_address1 = "-";
        public string agc_mailing_address1
        {
            get
            {
                return privateagc_mailing_address1;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_mailing_address1", ref privateagc_mailing_address1, value);
            }
        }

        private string privateagc_mailing_address2 = "-";
        public string agc_mailing_address2
        {
            get
            {
                return privateagc_mailing_address2;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_mailing_address2", ref privateagc_mailing_address2, value);
            }
        }

        private string privateagc_mailing_address3 = "-";
        public string agc_mailing_address3
        {
            get
            {
                return privateagc_mailing_address3;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_mailing_address3", ref privateagc_mailing_address3, value);
            }
        }

        private string privateagc_mailing_address4 = "-";
        public string agc_mailing_address4
        {
            get
            {
                return privateagc_mailing_address4;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_mailing_address4", ref privateagc_mailing_address4, value);
            }
        }

        private string privateagc_mailing_city = "-";
        public string agc_mailing_city
        {
            get
            {
                return privateagc_mailing_city;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "-";
                }
                if (value.Length > 40)
                {
                    value = value.Substring(0, 40);
                }
                DoSetProperty("agc_mailing_city", ref privateagc_mailing_city, value);
            }
        }

        private Int32 privateagc_mailing_state = DEFAULT_STATE;
        public Int32 agc_mailing_state
        {
            get
            {
                return privateagc_mailing_state;
            }
            set
            {
                DoSetProperty("agc_mailing_state", ref privateagc_mailing_state, value);
            }
        }

        private string privateagc_mailing_zip = "00000";
        public string agc_mailing_zip
        {
            get
            {
                return privateagc_mailing_zip;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "00000";
                }
                if (value.Length > 5)
                {
                    value = value.Substring(0, 5);
                }
                DoSetProperty("agc_mailing_zip", ref privateagc_mailing_zip, value);
            }
        }

        private string privateDescription = string.Empty;
        public string Description
        {
            get
            {
                return privateDescription;
            }
            set
            {
                DoSetProperty("Description", ref privateDescription, value);
            }
        }

        private string privateagc_web_site = "http://";
        public string agc_web_site
        {
            get
            {
                return privateagc_web_site;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "http://";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_web_site", ref privateagc_web_site, value);
            }
        }

        private string privateagc_phone_nbr = "000-000-0000";
        public string agc_phone_nbr
        {
            get
            {
                return privateagc_phone_nbr;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "000-000-0000";
                }
                if (value.Length > 12)
                {
                    value = value.Substring(0, 12);
                }
                DoSetProperty("agc_phone_nbr", ref privateagc_phone_nbr, value);
            }
        }

        private string privateagc_tollfree_phone_nbr = "000-000-0000";
        public string agc_tollfree_phone_nbr
        {
            get
            {
                return privateagc_tollfree_phone_nbr;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "000-000-0000";
                }
                if (value.Length > 12)
                {
                    value = value.Substring(0, 12);
                }
                DoSetProperty("agc_tollfree_phone_nbr", ref privateagc_tollfree_phone_nbr, value);
            }
        }

        private string privateagc_fax_nbr = "000-000-0000";
        public string agc_fax_nbr
        {
            get
            {
                return privateagc_fax_nbr;
            }
            set
            {
                if (value == string.Empty)
                {
                    value = "000-000-0000";
                }
                if (value.Length > 12)
                {
                    value = value.Substring(0, 12);
                }
                DoSetProperty("agc_fax_nbr", ref privateagc_fax_nbr, value);
            }
        }

        private string privateagc_email = string.Empty;
        public string agc_email
        {
            get
            {
                return privateagc_email;
            }
            set
            {
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("agc_email", ref privateagc_email, value);
            }
        }

        private bool privateagc_faith_based_ind;
        public bool agc_faith_based_ind
        {
            get
            {
                return privateagc_faith_based_ind;
            }
            set
            {
                DoSetProperty("agc_faith_based_ind", ref privateagc_faith_based_ind, value);
            }
        }

        private bool privateagc_colonias_ind;
        public bool agc_colonias_ind
        {
            get
            {
                return privateagc_colonias_ind;
            }
            set
            {
                DoSetProperty("agc_colonias_ind", ref privateagc_colonias_ind, value);
            }
        }

        private bool privateagc_migrfarm_worker_ind;
        public bool agc_migrfarm_worker_ind
        {
            get
            {
                return privateagc_migrfarm_worker_ind;
            }
            set
            {
                DoSetProperty("agc_migrfarm_worker_ind", ref privateagc_migrfarm_worker_ind, value);
            }
        }

        private Int32 privateagc_counseling_budget_amount;
        public Int32 agc_counseling_budget_amount
        {
            get
            {
                return privateagc_counseling_budget_amount;
            }
            set
            {
                if (value <= 0)
                {
                    value = 0;
                }
                DoSetProperty("agc_counseling_budget_amount", ref privateagc_counseling_budget_amount, value);
            }
        }

        // Information that is not sent but is used to keep track of the items
        private Int32 privateOffice;
        public Int32 office
        {
            get
            {
                return privateOffice;
            }
            set
            {
                DoSetProperty("office", ref privateOffice, value);
            }
        }

        private bool privateSend9902 = true;
        public bool Send9902
        {
            get
            {
                return privateSend9902;
            }
            set
            {
                DoSetProperty("Send9902", ref privateSend9902, value);
            }
        }

        private bool privateSendCounselors = true;
        public bool SendCounselors
        {
            get
            {
                return privateSendCounselors;
            }
            set
            {
                DoSetProperty("SendCounselors", ref privateSendCounselors, value);
            }
        }

        private bool privateSendClients = true;
        public bool SendClients
        {
            get
            {
                return privateSendClients;
            }
            set
            {
                DoSetProperty("SendClients", ref privateSendClients, value);
            }
        }

        private bool privateSendAttendees = true;
        public bool SendAttendees
        {
            get
            {
                return privateSendAttendees;
            }
            set
            {
                DoSetProperty("SendAttendees", ref privateSendAttendees, value);
            }
        }

        private bool privateSendClientSSN;
        public bool SendClientSSN
        {
            get
            {
                return privateSendClientSSN;
            }
            set
            {
                DoSetProperty("SendClientSSN", ref privateSendClientSSN, value);
            }
        }

        private Int32 privateSendClientCount;
        public Int32 SendClientCount
        {
            get
            {
                return privateSendClientCount;
            }
            set
            {
                DoSetProperty("SendClientCount", ref privateSendClientCount, value);
            }
        }

        private string privateUserName = "";
        public string UserName
        {
            get
            {
                return privateUserName;
            }
            set
            {
                if (value != string.Empty)
                {
                    DoSetProperty("UserName", ref privateUserName, value);
                }
            }
        }

        private string privatePassword = "";
        public string Password
        {
            get
            {
                return privatePassword;
            }
            set
            {
                if (value != string.Empty)
                {
                    DoSetProperty("Password", ref privatePassword, value);
                }
            }
        }

        private bool privateDefault;
        public bool Default
        {
            get
            {
                return privateDefault;
            }
            set
            {
                DoSetProperty("Default", ref privateDefault, value);
            }
        }

        private bool privateActiveFlag;
        public bool ActiveFlag
        {
            get
            {
                return privateActiveFlag;
            }
            set
            {
                DoSetProperty("ActiveFlag", ref privateActiveFlag, value);
            }
        }

        private string privateSubmissionID = string.Empty;
        public string SubmissionID
        {
            get
            {
                return privateSubmissionID;
            }
            set
            {
                DoSetProperty("SubmissionID", ref privateSubmissionID, value);
            }
        }

        private string privateSubmissionDate = string.Empty;
        public string SubmissionDate
        {
            get
            {
                return privateSubmissionDate;
            }
            set
            {
                DoSetProperty("SubmissionDate", ref privateSubmissionDate, value);
            }
        }

        private string privateSubmissionStatus = string.Empty;
        public string SubmissionStatus
        {
            get
            {
                return privateSubmissionStatus;
            }
            set
            {
                DoSetProperty("SubmissionStatus", ref privateSubmissionStatus, value);
            }
        }
        #endregion
    }
}
