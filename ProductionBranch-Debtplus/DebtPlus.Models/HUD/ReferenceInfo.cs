#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class ReferenceInfo : System.ComponentModel.INotifyPropertyChanged
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged; // Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        private void RaisePropertyChanged(string PropertyID)
        {
            var evt = PropertyChanged;
            if (evt != null)
            {
                evt(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyID));
            }
        }

        private Int32 privateGroupID;
        public Int32 GroupID
        {
            get
            {
                return privateGroupID;
            }
            set
            {
                if (privateGroupID != value)
                {
                    privateGroupID = value;
                    RaisePropertyChanged("GroupID");
                }
            }
        }

        private Int32 privateID;
        public Int32 ID
        {
            get
            {
                return privateID;
            }
            set
            {
                if (value != ID)
                {
                    privateID = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        private string privatelongDesc;
        public string longDesc
        {
            get
            {
                return privatelongDesc;
            }
            set
            {
                if (string.Compare(value, privatelongDesc, true) != 0)
                {
                    privatelongDesc = value;
                    RaisePropertyChanged("longDesc");
                }
            }
        }

        private string privateName;
        public string name
        {
            get
            {
                return privateName;
            }
            set
            {
                if (string.Compare(value, privateName, true) != 0)
                {
                    privateName = value;
                    RaisePropertyChanged("name");
                }
            }
        }

        private string privateShortDesc;
        public string shortDesc
        {
            get
            {
                return privateShortDesc;
            }
            set
            {
                if (string.Compare(value, privateShortDesc, true) != 0)
                {
                    privateShortDesc = value;
                    RaisePropertyChanged("shortDesc");
                }
            }
        }
    }
}
