﻿using System;

namespace DebtPlus.Models.HUD
{
    public class EventMessageArgs : EventArgs
    {
        public string Msg { get; private set; }
        public DateTime ItemDate { get; private set; }

        public EventMessageArgs()
        {
            Msg = string.Empty;
        }

        public EventMessageArgs(string msg)
        {
            Msg = msg;
        }

        public EventMessageArgs(DateTime itemDate)
        {
            Msg = string.Empty;
            ItemDate = itemDate;
        }

    }

    public delegate void MessageUpdate(object Sender, EventMessageArgs e);
}
