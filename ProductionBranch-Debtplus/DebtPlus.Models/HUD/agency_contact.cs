#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class agency_contact : ExtractBase
    {
        public agency_contact()
        {
        }

        public agency_contact(Int32 Type)
        {
            contact_type = Type.ToString();
        }

        #region storage
        private string privatecontact_type = "11";
        public string contact_type
        {
            get
            {
                return privatecontact_type;
            }
            set
            {
                DoSetProperty("contact_type", ref privatecontact_type, value);
            }
        }

        private string privatecontact_fname = "a";
        public string contact_fname
        {
            get
            {
                return privatecontact_fname;
            }
            set
            {
                DoSetProperty("contact_fname", ref privatecontact_fname, value);
            }
        }

        private string privatecontact_lname = "a";
        public string contact_lname
        {
            get
            {
                return privatecontact_lname;
            }
            set
            {
                DoSetProperty("contact_lname", ref privatecontact_lname, value);
            }
        }

        private string privatecontact_mname = "a";
        public string contact_mname
        {
            get
            {
                return privatecontact_mname;
            }
            set
            {
                DoSetProperty("contact_mname", ref privatecontact_mname, value);
            }
        }

        private string privatecontact_title = "2";
        public string contact_title
        {
            get
            {
                return privatecontact_title;
            }
            set
            {
                DoSetProperty("contact_title", ref privatecontact_title, value);
            }
        }

        private string privatecontact_address1 = "a";
        public string contact_address1
        {
            get
            {
                return privatecontact_address1;
            }
            set
            {
                DoSetProperty("contact_address1", ref privatecontact_address1, value);
            }
        }

        private string privatecontact_address2 = "-";
        public string contact_address2
        {
            get
            {
                return privatecontact_address2;
            }
            set
            {
                DoSetProperty("contact_address2", ref privatecontact_address2, value);
            }
        }

        private string privatecontact_city = "-";
        public string contact_city
        {
            get
            {
                return privatecontact_city;
            }
            set
            {
                DoSetProperty("contact_city", ref privatecontact_city, value);
            }
        }

        private string privatecontact_state = DEFAULT_STATE.ToString();
        public string contact_state
        {
            get
            {
                return privatecontact_state;
            }
            set
            {
                DoSetProperty("contact_state", ref privatecontact_state, value);
            }
        }

        private string privatecontact_zip_code = "00000";
        public string contact_zip_code
        {
            get
            {
                return privatecontact_zip_code;
            }
            set
            {
                DoSetProperty("contact_zip_code", ref privatecontact_zip_code, value);
            }
        }

        private string privatecontact_phone_nbr = "000-000-0000";
        public string contact_phone_nbr
        {
            get
            {
                return privatecontact_phone_nbr;
            }
            set
            {
                DoSetProperty("contact_phone_nbr", ref privatecontact_phone_nbr, value);
            }
        }

        private string privatecontact_ext_nbr = "00000";
        public string contact_ext_nbr
        {
            get
            {
                return privatecontact_ext_nbr;
            }
            set
            {
                DoSetProperty("contact_ext_nbr", ref privatecontact_ext_nbr, value);
            }
        }

        private string privatecontact_mobile_nbr = "000-000-0000";
        public string contact_mobile_nbr
        {
            get
            {
                return privatecontact_mobile_nbr;
            }
            set
            {
                DoSetProperty("contact_mobile_nbr", ref privatecontact_mobile_nbr, value);
            }
        }

        private string privatecontact_fax_nbr = "000-000-0000";
        public string contact_fax_nbr
        {
            get
            {
                return privatecontact_fax_nbr;
            }
            set
            {
                DoSetProperty("contact_fax_nbr", ref privatecontact_fax_nbr, value);
            }
        }

        private string privatecontact_email = "myemail@someplace.org";
        public string contact_email
        {
            get
            {
                return privatecontact_email;
            }
            set
            {
                DoSetProperty("contact_email", ref privatecontact_email, value);
            }
        }
        #endregion

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Agency_Contact");
            xml.WriteElementString("tns:contact_type", FmtInt32(contact_type));
            xml.WriteElementString("tns:contact_fname", FmtString(contact_fname));
            xml.WriteElementString("tns:contact_lname", FmtString(contact_lname));
            xml.WriteElementString("tns:contact_mname", FmtString(contact_mname));
            xml.WriteElementString("tns:contact_title", FmtInt32(contact_title));
            xml.WriteElementString("tns:contact_address1", FmtString(contact_address1));
            xml.WriteElementString("tns:contact_address2", FmtString(contact_address2));
            xml.WriteElementString("tns:contact_city", FmtString(contact_city));
            xml.WriteElementString("tns:contact_state", FmtInt32(contact_state));
            xml.WriteElementString("tns:contact_zip_code", FmtString(contact_zip_code));
            xml.WriteElementString("tns:contact_phone_nbr", FmtString(contact_phone_nbr));
            xml.WriteElementString("tns:contact_ext_nbr", FmtString(contact_ext_nbr));
            xml.WriteElementString("tns:contact_mobile_nbr", FmtString(contact_mobile_nbr));
            xml.WriteElementString("tns:contact_fax_nbr", FmtString(contact_fax_nbr));
            xml.WriteElementString("tns:contact_email", FmtString(contact_email));

            xml.WriteEndElement();
        }
    }
}

