using System;

namespace DebtPlus.Models.HUD
{
    public class CounselorTraining : ExtractBase
    {

        private Int32 private_counselor;
        public Int32 counselor
        {
            get
            {
                return private_counselor;
            }
            set
            {
                DoSetProperty("counselor", ref private_counselor, value);
            }
        }

        private Int32 private_TrainingCourse;
        public Int32 TrainingCourse
        {
            get
            {
                return private_TrainingCourse;
            }
            set
            {
                DoSetProperty("TrainingCourse", ref private_TrainingCourse, value);
            }
        }

        private bool private_TrainingCertificate;
        public bool TrainingCertificate
        {
            get
            {
                return private_TrainingCertificate;
            }
            set
            {
                DoSetProperty("TrainingCertificate", ref private_TrainingCertificate, value);
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Counselor_Training");
            xml.WriteElementString("tns:cms_counselor_id", counselor.ToString());
            xml.WriteElementString("tns:cnslor_training_cert", FmtYN(TrainingCertificate));
            xml.WriteElementString("tns:cnslor_training_course_id", TrainingCourse.ToString());
            xml.WriteEndElement();
        }
    }
}
