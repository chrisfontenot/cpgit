#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Xml;

namespace DebtPlus.Models.HUD
{
    public class AgencySubmissionInfo : ExtractBase
    {

        private AgencyProfileData privateAgencyProfileData = new AgencyProfileData();
        public AgencyProfileData AgencyProfileData
        {
            get
            {
                return privateAgencyProfileData;
            }
            set
            {
                privateAgencyProfileData = value;
            }
        }

        private System.Collections.Generic.List<agency_contact> privateagency_contacts = new System.Collections.Generic.List<agency_contact>();
        public System.Collections.Generic.List<agency_contact> Agency_Contacts
        {
            get
            {
                return privateagency_contacts;
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, XmlWriter xml)
        {
            throw new NotImplementedException();
        }

        public void SerializeData(AgencyProfileData CurrentAgency, XmlWriter xml, string Profile_Header_Line)
        {

            // Include the submission data information
            xml.WriteRaw(string.Format("<tns:SubmissionData {0}>", Profile_Header_Line));

            // Include the agency profile data
            AgencyProfileData.SerializeData(CurrentAgency, xml);

            // List the contact information
            if (Agency_Contacts.Count > 0)
            {
                xml.WriteStartElement("tns:Agency_Contacts");
                foreach (agency_contact item in Agency_Contacts)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            // End the submission data block
            xml.WriteRaw("</tns:SubmissionData>");
        }
    }
}
