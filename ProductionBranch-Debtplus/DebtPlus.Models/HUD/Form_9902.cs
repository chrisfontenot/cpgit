#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class form_9902 : ExtractBase
    {

        #region storage
        private Int32 private_report_period_id = 0;
        public Int32 report_period_id
        {
            get
            {
                return private_report_period_id;
            }
            set
            {
                DoSetProperty("report_period_id", ref private_report_period_id, value);
            }
        }

        private Int32 private_Ethnicity_Clients_Counseling_Hispanic = 0;
        public Int32 Ethnicity_Clients_Counseling_Hispanic
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_Hispanic;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_Hispanic", ref private_Ethnicity_Clients_Counseling_Hispanic, value);
            }
        }

        private Int32 private_Ethnicity_Clients_Counseling_Non_Hispanic = 0;
        public Int32 Ethnicity_Clients_Counseling_Non_Hispanic
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_Non_Hispanic;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_Non_Hispanic", ref private_Ethnicity_Clients_Counseling_Non_Hispanic, value);
            }
        }

        private Int32 private_Ethnicity_Clients_Counseling_No_Response = 0;
        public Int32 Ethnicity_Clients_Counseling_No_Response
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_No_Response;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_No_Response", ref private_Ethnicity_Clients_Counseling_No_Response, value);
            }
        }

        public Int32 Section_3_Total
        {
            get
            {
                return Ethnicity_Clients_Counseling_Hispanic + Ethnicity_Clients_Counseling_No_Response + Ethnicity_Clients_Counseling_Non_Hispanic;
            }
            set
            {
            }
        }

        private Int32 private_Race_Clients_Counseling_American_Indian_Alaskan_Native = 0;
        public Int32 Race_Clients_Counseling_American_Indian_Alaskan_Native
        {
            get
            {
                return private_Race_Clients_Counseling_American_Indian_Alaskan_Native;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_American_Indian_Alaskan_Native", ref private_Race_Clients_Counseling_American_Indian_Alaskan_Native, value);
            }
        }

        private Int32 private_Race_Clients_Counseling_Asian = 0;
        public Int32 Race_Clients_Counseling_Asian
        {
            get
            {
                return private_Race_Clients_Counseling_Asian;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Asian", ref private_Race_Clients_Counseling_Asian, value);
            }
        }

        private Int32 private_Race_Clients_Counseling_Black_AfricanAmerican = 0;
        public Int32 Race_Clients_Counseling_Black_AfricanAmerican
        {
            get
            {
                return private_Race_Clients_Counseling_Black_AfricanAmerican;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Black_AfricanAmerican", ref private_Race_Clients_Counseling_Black_AfricanAmerican, value);
            }
        }

        private Int32 private_Race_Clients_Counseling_Pacific_Islanders = 0;
        public Int32 Race_Clients_Counseling_Pacific_Islanders
        {
            get
            {
                return private_Race_Clients_Counseling_Pacific_Islanders;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Pacific_Islanders", ref private_Race_Clients_Counseling_Pacific_Islanders, value);
            }
        }

        private Int32 private_Race_Clients_Counseling_White = 0;
        public Int32 Race_Clients_Counseling_White
        {
            get
            {
                return private_Race_Clients_Counseling_White;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_White", ref private_Race_Clients_Counseling_White, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_AMINDWHT = 0;
        public Int32 MultiRace_Clients_Counseling_AMINDWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_AMINDWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_AMINDWHT", ref private_MultiRace_Clients_Counseling_AMINDWHT, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_ASIANWHT = 0;
        public Int32 MultiRace_Clients_Counseling_ASIANWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_ASIANWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_ASIANWHT", ref private_MultiRace_Clients_Counseling_ASIANWHT, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_BLKWHT = 0;
        public Int32 MultiRace_Clients_Counseling_BLKWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_BLKWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_BLKWHT", ref private_MultiRace_Clients_Counseling_BLKWHT, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_AMRCINDBLK = 0;
        public Int32 MultiRace_Clients_Counseling_AMRCINDBLK
        {
            get
            {
                return private_MultiRace_Clients_Counseling_AMRCINDBLK;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_AMRCINDBLK", ref private_MultiRace_Clients_Counseling_AMRCINDBLK, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_OtherMLTRC = 0;
        public Int32 MultiRace_Clients_Counseling_OtherMLTRC
        {
            get
            {
                return private_MultiRace_Clients_Counseling_OtherMLTRC;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_OtherMLTRC", ref private_MultiRace_Clients_Counseling_OtherMLTRC, value);
            }
        }

        private Int32 private_MultiRace_Clients_Counseling_NoResponse = 0;
        public Int32 MultiRace_Clients_Counseling_NoResponse
        {
            get
            {
                return private_MultiRace_Clients_Counseling_NoResponse;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_NoResponse", ref private_MultiRace_Clients_Counseling_NoResponse, value);
            }
        }

        public Int32 Section_4_Total
        {
            get
            {
                return Race_Clients_Counseling_American_Indian_Alaskan_Native + Race_Clients_Counseling_Asian + Race_Clients_Counseling_Black_AfricanAmerican + Race_Clients_Counseling_Pacific_Islanders + Race_Clients_Counseling_White + MultiRace_Clients_Counseling_AMINDWHT + MultiRace_Clients_Counseling_AMRCINDBLK + MultiRace_Clients_Counseling_ASIANWHT + MultiRace_Clients_Counseling_BLKWHT + MultiRace_Clients_Counseling_NoResponse + MultiRace_Clients_Counseling_OtherMLTRC;
            }
            set
            {
            }
        }

        private Int32 private_Lesser50_AMI_Level = 0;
        public Int32 Lesser50_AMI_Level
        {
            get
            {
                return private_Lesser50_AMI_Level;
            }
            set
            {
                DoSetProperty("Lesser50_AMI_Level", ref private_Lesser50_AMI_Level, value);
            }
        }

        private Int32 private_a50_79_AMI_Level = 0;
        public Int32 a50_79_AMI_Level
        {
            get
            {
                return private_a50_79_AMI_Level;
            }
            set
            {
                DoSetProperty("a50_79_AMI_Level", ref private_a50_79_AMI_Level, value);
            }
        }

        private Int32 private_a80_100_AMI_Level = 0;
        public Int32 a80_100_AMI_Level
        {
            get
            {
                return private_a80_100_AMI_Level;
            }
            set
            {
                DoSetProperty("a80_100_AMI_Level", ref private_a80_100_AMI_Level, value);
            }
        }

        private Int32 private_Greater100_AMI_Level = 0;
        public Int32 Greater100_AMI_Level
        {
            get
            {
                return private_Greater100_AMI_Level;
            }
            set
            {
                DoSetProperty("Greater100_AMI_Level", ref private_Greater100_AMI_Level, value);
            }
        }

        private Int32 private_AMI_No_Response = 0;
        public Int32 AMI_No_Response
        {
            get
            {
                return private_AMI_No_Response;
            }
            set
            {
                DoSetProperty("AMI_No_Response", ref private_AMI_No_Response, value);
            }
        }

        public Int32 Section_5_Total
        {
            get
            {
                return AMI_No_Response + a50_79_AMI_Level + a80_100_AMI_Level + Greater100_AMI_Level + Lesser50_AMI_Level;
            }
            set
            {
            }
        }

        private Int32 private_Compl_HomeBuyer_Educ_Workshop = 0;
        public Int32 Compl_HomeBuyer_Educ_Workshop
        {
            get
            {
                return private_Compl_HomeBuyer_Educ_Workshop;
            }
            set
            {
                DoSetProperty("Compl_HomeBuyer_Educ_Workshop", ref private_Compl_HomeBuyer_Educ_Workshop, value);
            }
        }

        private Int32 private_Compl_Workshop_HomeFin_Credit_Repair = 0;
        public Int32 Compl_Workshop_HomeFin_Credit_Repair
        {
            get
            {
                return private_Compl_Workshop_HomeFin_Credit_Repair;
            }
            set
            {
                DoSetProperty("Compl_Workshop_HomeFin_Credit_Repair", ref private_Compl_Workshop_HomeFin_Credit_Repair, value);
            }
        }

        private Int32 private_Compl_Resolv_Prevent_Mortg_Deliq = 0;
        public Int32 Compl_Resolv_Prevent_Mortg_Deliq
        {
            get
            {
                return private_Compl_Resolv_Prevent_Mortg_Deliq;
            }
            set
            {
                DoSetProperty("Compl_Resolv_Prevent_Mortg_Deliq", ref private_Compl_Resolv_Prevent_Mortg_Deliq, value);
            }
        }

        private Int32 private_Compl_HomeMaint_FinMngt = 0;
        public Int32 Compl_HomeMaint_FinMngt
        {
            get
            {
                return private_Compl_HomeMaint_FinMngt;
            }
            set
            {
                DoSetProperty("Compl_HomeMaint_FinMngt", ref private_Compl_HomeMaint_FinMngt, value);
            }
        }

        private Int32 private_Compl_Help_FairHousing_Workshop = 0;
        public Int32 Compl_Help_FairHousing_Workshop
        {
            get
            {
                return private_Compl_Help_FairHousing_Workshop;
            }
            set
            {
                DoSetProperty("Compl_Help_FairHousing_Workshop", ref private_Compl_Help_FairHousing_Workshop, value);
            }
        }

        private Int32 private_Compl_Workshop_Predatory_Lend = 0;
        public Int32 Compl_Workshop_Predatory_Lend
        {
            get
            {
                return private_Compl_Workshop_Predatory_Lend;
            }
            set
            {
                DoSetProperty("Compl_Workshop_Predatory_Lend", ref private_Compl_Workshop_Predatory_Lend, value);
            }
        }

        private Int32 private_Counseling_Rental_Workshop = 0;
        public Int32 Counseling_Rental_Workshop
        {
            get
            {
                return private_Counseling_Rental_Workshop;
            }
            set
            {
                DoSetProperty("Counseling_Rental_Workshop", ref private_Counseling_Rental_Workshop, value);
            }
        }

        private Int32 private_Compl_Other_Workshop = 0;
        public Int32 Compl_Other_Workshop
        {
            get
            {
                return private_Compl_Other_Workshop;
            }
            set
            {
                DoSetProperty("Compl_Other_Workshop", ref private_Compl_Other_Workshop, value);
            }
        }

        public Int32 Group_6_Total
        {
            get
            {
                return Compl_HomeBuyer_Educ_Workshop + Compl_Workshop_HomeFin_Credit_Repair + Compl_Resolv_Prevent_Mortg_Deliq + Compl_HomeMaint_FinMngt + Compl_Help_FairHousing_Workshop + Compl_Workshop_Predatory_Lend + Counseling_Rental_Workshop + Compl_Other_Workshop;
            }
            set
            {
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_PurchHousing = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_PurchHousing
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_PurchHousing;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_PurchHousing", ref private_Count_Prepurchase_Homebuyer_Counsel_PurchHousing, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Within_90 = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Within_90
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Within_90;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Within_90", ref private_Count_Prepurchase_Homebuyer_Counsel_Within_90, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Readyafter_90 = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Readyafter_90
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Readyafter_90;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Readyafter_90", ref private_Count_Prepurchase_Homebuyer_Counsel_Readyafter_90, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling", ref private_Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog", ref private_Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase", ref private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew", ref private_Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew, value);
            }
        }

        private Int32 private_Count_Prepurchase_Homebuyer_Counsel_Other = 0;
        public Int32 Count_Prepurchase_Homebuyer_Counsel_Other
        {
            get
            {
                return private_Count_Prepurchase_Homebuyer_Counsel_Other;
            }
            set
            {
                DoSetProperty("Count_Prepurchase_Homebuyer_Counsel_Other", ref private_Count_Prepurchase_Homebuyer_Counsel_Other, value);
            }
        }

        public Int32 Section_7a_Total
        {
            get
            {
                return Count_Prepurchase_Homebuyer_Counsel_PurchHousing + Count_Prepurchase_Homebuyer_Counsel_Within_90 + Count_Prepurchase_Homebuyer_Counsel_Readyafter_90 + Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling + Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog + Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase + Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew + Count_Prepurchase_Homebuyer_Counsel_Other;
            }
            set
            {
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Mortgage_Current = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Current
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Mortgage_Current;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Mortgage_Current", ref private_Count_Prevent_Mortgage_Deliquency_Mortgage_Current, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced", ref private_Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Mortgage_Modified = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Modified
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Mortgage_Modified;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Mortgage_Modified", ref private_Count_Prevent_Mortgage_Deliquency_Mortgage_Modified, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received", ref private_Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated", ref private_Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu", ref private_Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative", ref private_Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale", ref private_Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed", ref private_Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv", ref private_Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender", ref private_Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Bankcruptcy = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Bankcruptcy
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Bankcruptcy;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Bankcruptcy", ref private_Count_Prevent_Mortgage_Deliquency_Bankcruptcy, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan", ref private_Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Referred_to_Legal = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Referred_to_Legal
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Referred_to_Legal;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Referred_to_Legal", ref private_Count_Prevent_Mortgage_Deliquency_Referred_to_Legal, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention", ref private_Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Withdrew = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Withdrew
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Withdrew;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Withdrew", ref private_Count_Prevent_Mortgage_Deliquency_Withdrew, value);
            }
        }

        private Int32 private_Count_Prevent_Mortgage_Deliquency_Other = 0;
        public Int32 Count_Prevent_Mortgage_Deliquency_Other
        {
            get
            {
                return private_Count_Prevent_Mortgage_Deliquency_Other;
            }
            set
            {
                DoSetProperty("Count_Prevent_Mortgage_Deliquency_Other", ref private_Count_Prevent_Mortgage_Deliquency_Other, value);
            }
        }

        public Int32 Section_7b_Total
        {
            get
            {
                return 0
                + Count_Prevent_Mortgage_Deliquency_Mortgage_Current
                + Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced
                + Count_Prevent_Mortgage_Deliquency_Mortgage_Modified
                + Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received
                + Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated
                + Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu
                + Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative
                + Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale
                + Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed
                + Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv
                + Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender
                + Count_Prevent_Mortgage_Deliquency_Bankcruptcy
                + Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan
                + Count_Prevent_Mortgage_Deliquency_Referred_to_Legal
                + Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention
                + Count_Prevent_Mortgage_Deliquency_Withdrew
                + Count_Prevent_Mortgage_Deliquency_Other;
            }
            set
            {
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_HECM_Obtained = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_HECM_Obtained
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_HECM_Obtained;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_HECM_Obtained", ref private_Count_HomeMaintenance_Fin_Management_HECM_Obtained, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_HECM_Counseled = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_HECM_Counseled
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_HECM_Counseled;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_HECM_Counseled", ref private_Count_HomeMaintenance_Fin_Management_HECM_Counseled, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage", ref private_Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Homeequity_Loan = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Homeequity_Loan
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Homeequity_Loan;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Homeequity_Loan", ref private_Count_HomeMaintenance_Fin_Management_Homeequity_Loan, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Consumer_Loan = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Consumer_Loan
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Consumer_Loan;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Consumer_Loan", ref private_Count_HomeMaintenance_Fin_Management_Consumer_Loan, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced", ref private_Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Other_Social_Agency = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Other_Social_Agency
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Other_Social_Agency;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Other_Social_Agency", ref private_Count_HomeMaintenance_Fin_Management_Other_Social_Agency, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Sold_House = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Sold_House
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Sold_House;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Sold_House", ref private_Count_HomeMaintenance_Fin_Management_Sold_House, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling", ref private_Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling", ref private_Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Utilities_Current = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Utilities_Current
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Utilities_Current;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Utilities_Current", ref private_Count_HomeMaintenance_Fin_Management_Utilities_Current, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Legal_Assistance = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Legal_Assistance
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Legal_Assistance;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Legal_Assistance", ref private_Count_HomeMaintenance_Fin_Management_Legal_Assistance, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Receiving_Counseling = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Receiving_Counseling
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Receiving_Counseling;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Receiving_Counseling", ref private_Count_HomeMaintenance_Fin_Management_Receiving_Counseling, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Withdrew_Counseling = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Withdrew_Counseling
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Withdrew_Counseling", ref private_Count_HomeMaintenance_Fin_Management_Withdrew_Counseling, value);
            }
        }

        private Int32 private_Count_HomeMaintenance_Fin_Management_Other = 0;
        public Int32 Count_HomeMaintenance_Fin_Management_Other
        {
            get
            {
                return private_Count_HomeMaintenance_Fin_Management_Other;
            }
            set
            {
                DoSetProperty("Count_HomeMaintenance_Fin_Management_Other", ref private_Count_HomeMaintenance_Fin_Management_Other, value);
            }
        }

        public Int32 Section_7c_Total
        {
            get
            {
                return 0
                + Count_HomeMaintenance_Fin_Management_HECM_Obtained
                + Count_HomeMaintenance_Fin_Management_HECM_Counseled
                + Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage
                + Count_HomeMaintenance_Fin_Management_Homeequity_Loan
                + Count_HomeMaintenance_Fin_Management_Consumer_Loan
                + Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced
                + Count_HomeMaintenance_Fin_Management_Other_Social_Agency
                + Count_HomeMaintenance_Fin_Management_Sold_House
                + Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling
                + Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling
                + Count_HomeMaintenance_Fin_Management_Utilities_Current
                + Count_HomeMaintenance_Fin_Management_Legal_Assistance
                + Count_HomeMaintenance_Fin_Management_Receiving_Counseling
                + Count_HomeMaintenance_Fin_Management_Withdrew_Counseling
                + Count_HomeMaintenance_Fin_Management_Other;
            }
            set
            {
            }
        }

        private Int32 private_Seeking_Help_Housing_Search_Assistance = 0;
        public Int32 Seeking_Help_Housing_Search_Assistance
        {
            get
            {
                return private_Seeking_Help_Housing_Search_Assistance;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Search_Assistance", ref private_Seeking_Help_Housing_Search_Assistance, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Temp_Rental_Relief = 0;
        public Int32 Seeking_Help_Housing_Temp_Rental_Relief
        {
            get
            {
                return private_Seeking_Help_Housing_Temp_Rental_Relief;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Temp_Rental_Relief", ref private_Seeking_Help_Housing_Temp_Rental_Relief, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Referred_to_Rental_Assistance = 0;
        public Int32 Seeking_Help_Housing_Referred_to_Rental_Assistance
        {
            get
            {
                return private_Seeking_Help_Housing_Referred_to_Rental_Assistance;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Referred_to_Rental_Assistance", ref private_Seeking_Help_Housing_Referred_to_Rental_Assistance, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Recertification_Subsidy_Program = 0;
        public Int32 Seeking_Help_Housing_Recertification_Subsidy_Program
        {
            get
            {
                return private_Seeking_Help_Housing_Recertification_Subsidy_Program;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Recertification_Subsidy_Program", ref private_Seeking_Help_Housing_Recertification_Subsidy_Program, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Referred_Other_Social_Agency = 0;
        public Int32 Seeking_Help_Housing_Referred_Other_Social_Agency
        {
            get
            {
                return private_Seeking_Help_Housing_Referred_Other_Social_Agency;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Referred_Other_Social_Agency", ref private_Seeking_Help_Housing_Referred_Other_Social_Agency, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Referred_Legal_Aid_Agency = 0;
        public Int32 Seeking_Help_Housing_Referred_Legal_Aid_Agency
        {
            get
            {
                return private_Seeking_Help_Housing_Referred_Legal_Aid_Agency;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Referred_Legal_Aid_Agency", ref private_Seeking_Help_Housing_Referred_Legal_Aid_Agency, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Referred_Legal_Agency_Eviction = 0;
        public Int32 Seeking_Help_Housing_Referred_Legal_Agency_Eviction
        {
            get
            {
                return private_Seeking_Help_Housing_Referred_Legal_Agency_Eviction;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Referred_Legal_Agency_Eviction", ref private_Seeking_Help_Housing_Referred_Legal_Agency_Eviction, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Found_Alternative_Housing = 0;
        public Int32 Seeking_Help_Housing_Found_Alternative_Housing
        {
            get
            {
                return private_Seeking_Help_Housing_Found_Alternative_Housing;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Found_Alternative_Housing", ref private_Seeking_Help_Housing_Found_Alternative_Housing, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Remain_CurrentHousing = 0;
        public Int32 Seeking_Help_Housing_Remain_CurrentHousing
        {
            get
            {
                return private_Seeking_Help_Housing_Remain_CurrentHousing;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Remain_CurrentHousing", ref private_Seeking_Help_Housing_Remain_CurrentHousing, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Resolved_Issue = 0;
        public Int32 Seeking_Help_Housing_Resolved_Issue
        {
            get
            {
                return private_Seeking_Help_Housing_Resolved_Issue;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Resolved_Issue", ref private_Seeking_Help_Housing_Resolved_Issue, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Debt_Mngmt_Entered = 0;
        public Int32 Seeking_Help_Housing_Debt_Mngmt_Entered
        {
            get
            {
                return private_Seeking_Help_Housing_Debt_Mngmt_Entered;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Debt_Mngmt_Entered", ref private_Seeking_Help_Housing_Debt_Mngmt_Entered, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Utilities_Current = 0;
        public Int32 Seeking_Help_Housing_Utilities_Current
        {
            get
            {
                return private_Seeking_Help_Housing_Utilities_Current;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Utilities_Current", ref private_Seeking_Help_Housing_Utilities_Current, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Security_Dep_Dispute = 0;
        public Int32 Seeking_Help_Housing_Security_Dep_Dispute
        {
            get
            {
                return private_Seeking_Help_Housing_Security_Dep_Dispute;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Security_Dep_Dispute", ref private_Seeking_Help_Housing_Security_Dep_Dispute, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Current_Counseled = 0;
        public Int32 Seeking_Help_Housing_Current_Counseled
        {
            get
            {
                return private_Seeking_Help_Housing_Current_Counseled;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Current_Counseled", ref private_Seeking_Help_Housing_Current_Counseled, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Withdrew_Counseling = 0;
        public Int32 Seeking_Help_Housing_Withdrew_Counseling
        {
            get
            {
                return private_Seeking_Help_Housing_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Withdrew_Counseling", ref private_Seeking_Help_Housing_Withdrew_Counseling, value);
            }
        }

        private Int32 private_Seeking_Help_Housing_Other = 0;
        public Int32 Seeking_Help_Housing_Other
        {
            get
            {
                return private_Seeking_Help_Housing_Other;
            }
            set
            {
                DoSetProperty("Seeking_Help_Housing_Other", ref private_Seeking_Help_Housing_Other, value);
            }
        }

        public Int32 Section_7d_Total
        {
            get
            {
                return 0
                + Seeking_Help_Housing_Search_Assistance
                + Seeking_Help_Housing_Temp_Rental_Relief
                + Seeking_Help_Housing_Referred_to_Rental_Assistance
                + Seeking_Help_Housing_Recertification_Subsidy_Program
                + Seeking_Help_Housing_Referred_Other_Social_Agency
                + Seeking_Help_Housing_Referred_Legal_Aid_Agency
                + Seeking_Help_Housing_Referred_Legal_Agency_Eviction
                + Seeking_Help_Housing_Found_Alternative_Housing
                + Seeking_Help_Housing_Remain_CurrentHousing
                + Seeking_Help_Housing_Resolved_Issue
                + Seeking_Help_Housing_Debt_Mngmt_Entered
                + Seeking_Help_Housing_Utilities_Current
                + Seeking_Help_Housing_Security_Dep_Dispute
                + Seeking_Help_Housing_Current_Counseled
                + Seeking_Help_Housing_Withdrew_Counseling
                + Seeking_Help_Housing_Other;
            }
            set
            {
            }
        }

        private Int32 private_Count_Occupied_Emergency_Shelter = 0;
        public Int32 Count_Occupied_Emergency_Shelter
        {
            get
            {
                return private_Count_Occupied_Emergency_Shelter;
            }
            set
            {
                DoSetProperty("Count_Occupied_Emergency_Shelter", ref private_Count_Occupied_Emergency_Shelter, value);
            }
        }

        private Int32 private_Count_Occupied_Transitional_Housing = 0;
        public Int32 Count_Occupied_Transitional_Housing
        {
            get
            {
                return private_Count_Occupied_Transitional_Housing;
            }
            set
            {
                DoSetProperty("Count_Occupied_Transitional_Housing", ref private_Count_Occupied_Transitional_Housing, value);
            }
        }

        private Int32 private_Count_Occupied_Permnthouse_RentAssist = 0;
        public Int32 Count_Occupied_Permnthouse_RentAssist
        {
            get
            {
                return private_Count_Occupied_Permnthouse_RentAssist;
            }
            set
            {
                DoSetProperty("Count_Occupied_Permnthouse_RentAssist", ref private_Count_Occupied_Permnthouse_RentAssist, value);
            }
        }

        private Int32 private_Count_Occupied_PermntHouse_wo_RentAssist = 0;
        public Int32 Count_Occupied_PermntHouse_wo_RentAssist
        {
            get
            {
                return private_Count_Occupied_PermntHouse_wo_RentAssist;
            }
            set
            {
                DoSetProperty("Count_Occupied_PermntHouse_wo_RentAssist", ref private_Count_Occupied_PermntHouse_wo_RentAssist, value);
            }
        }

        private Int32 private_Count_Counseled_Ref_Other_SocAgency = 0;
        public Int32 Count_Counseled_Ref_Other_SocAgency
        {
            get
            {
                return private_Count_Counseled_Ref_Other_SocAgency;
            }
            set
            {
                DoSetProperty("Count_Counseled_Ref_Other_SocAgency", ref private_Count_Counseled_Ref_Other_SocAgency, value);
            }
        }

        private Int32 private_Count_Remained_Homeless = 0;
        public Int32 Count_Remained_Homeless
        {
            get
            {
                return private_Count_Remained_Homeless;
            }
            set
            {
                DoSetProperty("Count_Remained_Homeless", ref private_Count_Remained_Homeless, value);
            }
        }

        private Int32 private_Count_Currently_Receiving_Counsel = 0;
        public Int32 Count_Currently_Receiving_Counsel
        {
            get
            {
                return private_Count_Currently_Receiving_Counsel;
            }
            set
            {
                DoSetProperty("Count_Currently_Receiving_Counsel", ref private_Count_Currently_Receiving_Counsel, value);
            }
        }

        private Int32 private_Count_Withdrew_Counseling = 0;
        public Int32 Count_Withdrew_Counseling
        {
            get
            {
                return private_Count_Withdrew_Counseling;
            }
            set
            {
                DoSetProperty("Count_Withdrew_Counseling", ref private_Count_Withdrew_Counseling, value);
            }
        }

        private Int32 private_Count_Seek_Shelter_Other = 0;
        public Int32 Count_Seek_Shelter_Other
        {
            get
            {
                return private_Count_Seek_Shelter_Other;
            }
            set
            {
                DoSetProperty("Count_Seek_Shelter_Other", ref private_Count_Seek_Shelter_Other, value);
            }
        }

        public Int32 Section_7e_Total
        {
            get
            {
                return 0
                + Count_Occupied_Emergency_Shelter
                + Count_Occupied_Transitional_Housing
                + Count_Occupied_Permnthouse_RentAssist
                + Count_Occupied_PermntHouse_wo_RentAssist
                + Count_Counseled_Ref_Other_SocAgency
                + Count_Remained_Homeless
                + Count_Currently_Receiving_Counsel
                + Count_Withdrew_Counseling
                + Count_Seek_Shelter_Other;
            }
            set
            {
            }
        }

        public Int32 Section_7a_e_Total
        {
            get
            {
                return Section_7a_Total + Section_7b_Total + Section_7c_Total + Section_7d_Total + Section_7e_Total;
            }
            set
            {
            }
        }

        public Int32 Section_6_7_Total
        {
            get
            {
                return Group_6_Total + Section_7a_e_Total;
            }
            set
            {
            }
        }

        #endregion

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Form_9902");

            xml.WriteStartElement("tns:report_period_id");
            xml.WriteValue(FmtInt32(report_period_id));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:AMI_No_Response");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(AMI_No_Response));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_Help_FairHousing_Workshop");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_Help_FairHousing_Workshop));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_HomeBuyer_Educ_Workshop");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_HomeBuyer_Educ_Workshop));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_HomeMaint_FinMngt");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_HomeMaint_FinMngt));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_Other_Workshop");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_Other_Workshop));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_Resolv_Prevent_Mortg_Deliq");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_Resolv_Prevent_Mortg_Deliq));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_Workshop_HomeFin_Credit_Repair");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_Workshop_HomeFin_Credit_Repair));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Compl_Workshop_Predatory_Lend");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Compl_Workshop_Predatory_Lend));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Counseling_Rental_Workshop");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Counseling_Rental_Workshop));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Counseled_Ref_Other_SocAgency");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Counseled_Ref_Other_SocAgency));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Currently_Receiving_Counsel");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Currently_Receiving_Counsel));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Consumer_Loan");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Consumer_Loan));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_HECM_Counseled");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_HECM_Counseled));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_HECM_Obtained");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_HECM_Obtained));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Homeequity_Loan");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Homeequity_Loan));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Legal_Assistance");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Legal_Assistance));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Other");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Other));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Other_Social_Agency");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Other_Social_Agency));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Receiving_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Receiving_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Sold_House");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Sold_House));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Utilities_Current");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Utilities_Current));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_HomeMaintenance_Fin_Management_Withdrew_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_HomeMaintenance_Fin_Management_Withdrew_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Occupied_Emergency_Shelter");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Occupied_Emergency_Shelter));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Occupied_PermntHouse_wo_RentAssist");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Occupied_PermntHouse_wo_RentAssist));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Occupied_Permnthouse_RentAssist");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Occupied_Permnthouse_RentAssist));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Occupied_Transitional_Housing");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Occupied_Transitional_Housing));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Other");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Other));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_PurchHousing");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_PurchHousing));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Readyafter_90");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Readyafter_90));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prepurchase_Homebuyer_Counsel_Within_90");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prepurchase_Homebuyer_Counsel_Within_90));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Bankcruptcy");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Bankcruptcy));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Mortgage_Current");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Mortgage_Current));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Mortgage_Modified");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Mortgage_Modified));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Other");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Other));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Referred_to_Legal");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Referred_to_Legal));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Prevent_Mortgage_Deliquency_Withdrew");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Prevent_Mortgage_Deliquency_Withdrew));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Remained_Homeless");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Remained_Homeless));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Seek_Shelter_Other");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Seek_Shelter_Other));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Count_Withdrew_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Count_Withdrew_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Ethnicity_Clients_Counseling_Hispanic");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Ethnicity_Clients_Counseling_Hispanic));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Ethnicity_Clients_Counseling_No_Response");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Ethnicity_Clients_Counseling_No_Response));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Ethnicity_Clients_Counseling_Non_Hispanic");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Ethnicity_Clients_Counseling_Non_Hispanic));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Greater100_AMI_Level");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Greater100_AMI_Level));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Lesser50_AMI_Level");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Lesser50_AMI_Level));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_AMINDWHT");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_AMINDWHT));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_AMRCINDBLK");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_AMRCINDBLK));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_ASIANWHT");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_ASIANWHT));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_BLKWHT");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_BLKWHT));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_NoResponse");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_NoResponse));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:MultiRace_Clients_Counseling_OtherMLTRC");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(MultiRace_Clients_Counseling_OtherMLTRC));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Race_Clients_Counseling_American_Indian_Alaskan_Native");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Race_Clients_Counseling_American_Indian_Alaskan_Native));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Race_Clients_Counseling_Asian");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Race_Clients_Counseling_Asian));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Race_Clients_Counseling_Black_AfricanAmerican");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Race_Clients_Counseling_Black_AfricanAmerican));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Race_Clients_Counseling_Pacific_Islanders");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Race_Clients_Counseling_Pacific_Islanders));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Race_Clients_Counseling_White");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Race_Clients_Counseling_White));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Current_Counseled");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Current_Counseled));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Debt_Mngmt_Entered");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Debt_Mngmt_Entered));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Found_Alternative_Housing");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Found_Alternative_Housing));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Other");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Other));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Recertification_Subsidy_Program");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Recertification_Subsidy_Program));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Referred_Legal_Agency_Eviction");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Referred_Legal_Agency_Eviction));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Referred_Legal_Aid_Agency");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Referred_Legal_Aid_Agency));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Referred_Other_Social_Agency");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Referred_Other_Social_Agency));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Referred_to_Rental_Assistance");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Referred_to_Rental_Assistance));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Remain_CurrentHousing");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Remain_CurrentHousing));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Resolved_Issue");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Resolved_Issue));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Search_Assistance");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Search_Assistance));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Security_Dep_Dispute");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Security_Dep_Dispute));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Temp_Rental_Relief");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Temp_Rental_Relief));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Utilities_Current");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Utilities_Current));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Seeking_Help_Housing_Withdrew_Counseling");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Seeking_Help_Housing_Withdrew_Counseling));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:a50_79_AMI_Level");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(a50_79_AMI_Level));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:a80_100_AMI_Level");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(a80_100_AMI_Level));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_3_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_3_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_4_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_4_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_5_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_5_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Group_6_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Group_6_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7a_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7a_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7b_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7b_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7c_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7c_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7d_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7d_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7e_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7e_Total));
            xml.WriteEndElement();

            xml.WriteStartElement("tns:Section_7a_e_Total");
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(FmtInt32(Section_7a_e_Total));
            xml.WriteEndElement();

            xml.WriteEndElement();
        }
    }
}
