using System;

namespace DebtPlus.Models.HUD
{
    public class CounselorTrainingCourse : ExtractBase
    {

        private Int32 private_TrainingCourse;
        private string private_cnslor_training_title = string.Empty;
        private DateTime private_cnslor_training_date;
        private Int32 private_cnslor_training_org;
        private string private_cnslor_training_org_other = string.Empty;
        private Int32 private_cnslor_training_sponsor;
        private string private_cnslor_training_sponsor_other = string.Empty;
        private Int32 private_cnslor_training_duration = 0;

        public Int32 TrainingCourse
        {
            get
            {
                return private_TrainingCourse;
            }
            set
            {
                DoSetProperty("TrainingCourse", ref private_TrainingCourse, value);
            }
        }

        public string cnslor_training_title
        {
            get
            {
                return private_cnslor_training_title;
            }
            set
            {
                DoSetProperty("cnslor_training_title", ref private_cnslor_training_title, value);
            }
        }

        public DateTime cnslor_training_date
        {
            get
            {
                return private_cnslor_training_date;
            }

            set
            {
                DoSetProperty("cnslor_training_date", ref private_cnslor_training_date, value);
            }
        }

        public Int32 cnslor_training_org
        {
            get
            {
                return private_cnslor_training_org;
            }
            set
            {
                DoSetProperty("cnslor_training_org", ref private_cnslor_training_org, value);
            }
        }

        public string cnslor_training_org_other
        {
            get
            {
                return private_cnslor_training_org_other;
            }
            set
            {
                DoSetProperty("cnslor_training_org_other", ref private_cnslor_training_org_other, value);
            }
        }

        public Int32 cnslor_training_sponsor
        {
            get
            {
                return private_cnslor_training_sponsor;
            }
            set
            {
                DoSetProperty("cnslor_training_sponsor", ref private_cnslor_training_sponsor, value);
            }
        }

        public string cnslor_training_sponsor_other
        {
            get
            {
                return private_cnslor_training_sponsor_other;
            }
            set
            {
                DoSetProperty("cnslor_training_sponsor_other", ref private_cnslor_training_sponsor_other, value);
            }
        }

        public Int32 cnslor_training_duration
        {
            get
            {
                return private_cnslor_training_duration;
            }
            set
            {
                DoSetProperty("cnslor_training_duration", ref private_cnslor_training_duration, value);
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Counselor_Training_Course");
            xml.WriteElementString("tns:cnslor_training_course_id", TrainingCourse.ToString());
            xml.WriteElementString("tns:cnslor_training_title", FmtString(cnslor_training_title));
            xml.WriteElementString("tns:cnslor_training_date", FmtDate(private_cnslor_training_date));
            xml.WriteElementString("tns:cnslor_training_org", cnslor_training_org.ToString());
            xml.WriteElementString("tns:cnslor_training_org_other", FmtString(cnslor_training_org_other));
            xml.WriteElementString("tns:cnslor_training_sponsor", cnslor_training_sponsor.ToString());
            xml.WriteElementString("tns:cnslor_training_sponsor_other", FmtString(cnslor_training_sponsor_other));
            if (cnslor_training_duration > 0)
            {
                xml.WriteElementString("tns:cnslor_training_duration", cnslor_training_duration.ToString());
            }
            xml.WriteEndElement();
        }
    }
}
