#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    partial class AgencyProfileData : ExtractBase
    {
        // These fields are serialized in their own submission. They are agency specific locations.
        public form_9902 Form9902 = new form_9902();
        public System.Collections.Generic.List<counselor_profile> CounselorProfiles = new System.Collections.Generic.List<counselor_profile>();
        // public System.Collections.Generic.List<object> ClientList = new System.Collections.Generic.List<object>();
        public System.Collections.Generic.List<client_profile> ClientList = new System.Collections.Generic.List<client_profile>();
        public System.Collections.Generic.List<CounselorTraining> CounselorTrainings = new System.Collections.Generic.List<CounselorTraining>();
        public System.Collections.Generic.List<CounselorTrainingCourse> CounselorTrainingCourses = new System.Collections.Generic.List<CounselorTrainingCourse>();
        public System.Collections.Generic.List<group_session> GroupSessions = new System.Collections.Generic.List<group_session>();
        public System.Collections.Generic.List<attendee> Attendees = new System.Collections.Generic.List<attendee>();

        public event MessageUpdate UpdateSubmittedDate;
        protected void Raise_UpdateSubmittedDate(EventMessageArgs e)
        {
            var evt = UpdateSubmittedDate;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionDateChanged(DateTime ItemDate)
        {
            Raise_UpdateSubmittedDate(new EventMessageArgs(ItemDate));
        }

        public event MessageUpdate UpdateSubmissionID;
        protected void Raise_UpdateSubmissionID(EventMessageArgs e)
        {
            var evt = UpdateSubmissionID;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionIDChanged(Int64 ID)
        {
            Raise_UpdateSubmissionID(new EventMessageArgs(ID.ToString()));
        }

        public event MessageUpdate UpdateSubmissionStatus;
        protected void Raise_UpdateSubmissionStatus(EventMessageArgs e)
        {
            var evt = UpdateSubmissionStatus;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionStatusChanged(string Status)
        {
            Raise_UpdateSubmissionStatus(new EventMessageArgs(Status));
        }

        private readonly System.Collections.Generic.List<Language> privateagency_languages = new System.Collections.Generic.List<Language>();
        public System.Collections.Generic.List<Language> Agency_Languages
        {
            get
            {
                return privateagency_languages;
            }
        }

        private readonly System.Collections.Generic.List<Counseling_Method> privateagency_counseling_methods = new System.Collections.Generic.List<Counseling_Method>();
        public System.Collections.Generic.List<Counseling_Method> Agency_Counseling_Methods
        {
            get
            {
                return privateagency_counseling_methods;
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:AgencyProfileData");
            xml.WriteElementString("tns:agc_ein", FmtEIN(agc_ein));
            xml.WriteElementString("tns:reported_month", FmtInt32(reported_month));
            xml.WriteElementString("tns:agc_dun_nbr", FmtDBN(agc_dun_nbr));
            xml.WriteElementString("tns:agc_physical_address1", FmtString(agc_physical_address1));
            xml.WriteElementString("tns:agc_physical_address2", FmtString(agc_physical_address2));
            xml.WriteElementString("tns:agc_physical_address3", FmtString(agc_physical_address3));
            xml.WriteElementString("tns:agc_physical_address4", FmtString(agc_physical_address4));
            xml.WriteElementString("tns:agc_physical_city", FmtString(agc_physical_city));
            xml.WriteElementString("tns:agc_physical_state", FmtInt32(agc_physical_state));
            xml.WriteElementString("tns:agc_physical_zip", FmtZip(agc_physical_zip));
            xml.WriteElementString("tns:agc_mailing_address1", FmtString(agc_mailing_address1));
            xml.WriteElementString("tns:agc_mailing_address2", FmtString(agc_mailing_address2));
            xml.WriteElementString("tns:agc_mailing_address3", FmtString(agc_mailing_address3));
            xml.WriteElementString("tns:agc_mailing_address4", FmtString(agc_mailing_address4));
            xml.WriteElementString("tns:agc_mailing_city", FmtString(agc_mailing_city));
            xml.WriteElementString("tns:agc_mailing_state", FmtInt32(agc_mailing_state));
            xml.WriteElementString("tns:agc_mailing_zip", FmtZip(agc_mailing_zip));
            xml.WriteElementString("tns:agc_web_site", FmtString(agc_web_site));
            xml.WriteElementString("tns:agc_phone_nbr", FmtTelephoneNumber(agc_phone_nbr));
            xml.WriteElementString("tns:agc_tollfree_phone_nbr", FmtTelephoneNumber(agc_tollfree_phone_nbr));
            xml.WriteElementString("tns:agc_fax_nbr", FmtTelephoneNumber(agc_fax_nbr));
            xml.WriteElementString("tns:agc_email", FmtString(agc_email));
            xml.WriteElementString("tns:agc_faith_based_ind", FmtYN(agc_faith_based_ind));
            xml.WriteElementString("tns:agc_colonias_ind", FmtYN(agc_colonias_ind));
            xml.WriteElementString("tns:agc_migrfarm_worker_ind", FmtYN(agc_migrfarm_worker_ind));
            xml.WriteElementString("tns:agc_counseling_budget_amount", FmtDecimal(agc_counseling_budget_amount));

            if (Agency_Languages.Count > 0)
            {
                xml.WriteStartElement("tns:agency_languages");
                foreach (Language item in Agency_Languages)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            // Agency counseling methods
            if (Agency_Counseling_Methods.Count > 0)
            {
                xml.WriteStartElement("tns:agency_counseling_methods");
                foreach (Counseling_Method item in Agency_Counseling_Methods)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }
    }
}
