#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Models.HUD
{
    public class attendee : ExtractBase
    {

        #region storage
        private Int32 private_attendee_id;
        public Int32 attendee_id
        {
            get
            {
                return private_attendee_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("attendee_id", ref private_attendee_id, value);
            }
        }

        private string private_Attendee_fname;
        public string Attendee_fname
        {
            get
            {
                if (private_Attendee_fname == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_fname;
            }
            set
            {
                DoSetProperty("Attendee_fname", ref private_Attendee_fname, value);
            }
        }

        private string private_Attendee_lname;
        public string Attendee_lname
        {
            get
            {
                if (private_Attendee_lname == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_lname;
            }
            set
            {
                DoSetProperty("Attendee_lname", ref private_Attendee_lname, value);
            }
        }

        private string private_Attendee_mname;
        public string Attendee_mname
        {
            get
            {
                return private_Attendee_mname;
            }
            set
            {
                DoSetProperty("Attendee_mname", ref private_Attendee_mname, value);
            }
        }

        private string private_Attendee_Address_1;
        public string Attendee_Address_1
        {
            get
            {
                if (private_Attendee_Address_1 == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_Address_1;
            }
            set
            {
                DoSetProperty("Attendee_Address_1", ref private_Attendee_Address_1, value);
            }
        }

        private string private_Attendee_Address_2;
        public string Attendee_Address_2
        {
            get
            {
                return private_Attendee_Address_2;
            }
            set
            {
                DoSetProperty("Attendee_Address_2", ref private_Attendee_Address_2, value);
            }
        }

        private string private_Attendee_City;
        public string Attendee_City
        {
            get
            {
                if (private_Attendee_City == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_City;
            }
            set
            {
                DoSetProperty("Attendee_City", ref private_Attendee_City, value);
            }
        }

        private Int32 private_Attendee_State;
        public Int32 Attendee_State
        {
            get
            {
                if (private_Attendee_State <= 1)
                {
                    return 61;
                }
                return private_Attendee_State;
            }
            set
            {
                DoSetProperty("Attendee_State", ref private_Attendee_State, value);
            }
        }

        private string private_Attendee_Zip_Code;
        public string Attendee_Zip_Code
        {
            get
            {
                if (private_Attendee_Zip_Code == string.Empty)
                {
                    return "00000";
                }
                return private_Attendee_Zip_Code;
            }
            set
            {
                DoSetProperty("Attendee_Zip_Code", ref private_Attendee_Zip_Code, value);
            }
        }

        private Int32 private_Attendee_Race_ID;
        public Int32 Attendee_Race_ID
        {
            get
            {
                if (private_Attendee_Race_ID < 1)
                {
                    return 1;
                }
                return private_Attendee_Race_ID;
            }
            set
            {
                DoSetProperty("Attendee_Race_ID", ref private_Attendee_Race_ID, value);
            }
        }

        private Int32 private_Attendee_Ethnicity_ID;
        public Int32 Attendee_Ethnicity_ID
        {
            get
            {
                if (private_Attendee_Ethnicity_ID < 1)
                {
                    return 1;
                }
                return private_Attendee_Ethnicity_ID;
            }
            set
            {
                DoSetProperty("Attendee_Ethnicity_ID", ref private_Attendee_Ethnicity_ID, value);
            }
        }

        private Int32 private_Attendee_Income_Level;
        public Int32 Attendee_Income_Level
        {
            get
            {
                if (private_Attendee_Income_Level < 1)
                {
                    return 1;
                }
                return private_Attendee_Income_Level;
            }
            set
            {
                DoSetProperty("Attendee_Income_Level", ref private_Attendee_Income_Level, value);
            }
        }
        #endregion

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Attendee");

            xml.WriteElementString("tns:attendee_id", FmtInt32(attendee_id));
            if (!string.IsNullOrEmpty(Attendee_fname))
            {
                xml.WriteElementString("tns:Attendee_fname", FmtString(Attendee_fname));
            }
            if (!string.IsNullOrEmpty(Attendee_lname))
            {
                xml.WriteElementString("tns:Attendee_lname", FmtString(Attendee_lname));
            }
            if (!string.IsNullOrEmpty(Attendee_mname))
            {
                xml.WriteElementString("tns:Attendee_mname", FmtString(Attendee_mname));
            }
            if (!string.IsNullOrEmpty(Attendee_Address_1))
            {
                xml.WriteElementString("tns:Attendee_Address_1", FmtString(Attendee_Address_1));
            }
            if (!string.IsNullOrEmpty(Attendee_Address_2))
            {
                xml.WriteElementString("tns:Attendee_Address_2", FmtString(Attendee_Address_2));
            }
            if (!string.IsNullOrEmpty(Attendee_City))
            {
                xml.WriteElementString("tns:Attendee_City", FmtString(Attendee_City));
            }
            if (Attendee_State > 0)
            {
                xml.WriteElementString("tns:Attendee_State", FmtInt32(Attendee_State));
            }
            if (!string.IsNullOrEmpty(Attendee_Zip_Code))
            {
                xml.WriteElementString("tns:Attendee_Zip_Code", FmtString(Attendee_Zip_Code));
            }
            if (Attendee_Race_ID > 0)
            {
                xml.WriteElementString("tns:Attendee_Race_ID", FmtInt32(Attendee_Race_ID));
            }
            if (Attendee_Ethnicity_ID > 0)
            {
                xml.WriteElementString("tns:Attendee_Ethnicity_ID", FmtInt32(Attendee_Ethnicity_ID));
            }
            if (Attendee_Income_Level > 0)
            {
                xml.WriteElementString("tns:Attendee_Income_Level", FmtInt32(Attendee_Income_Level));
            }

            xml.WriteEndElement();
        }
    }
}
