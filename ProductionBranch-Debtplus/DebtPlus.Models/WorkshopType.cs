﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class WorkshopType
    {
        public Int32 Id { get; set; }
        public string Description { get; set; }
        public string Duration { get; set; }
        public bool ActiveFlag { get; set; }
        public Decimal HousingFeeAmount { get; set; }
        public Int32 HudGrant { get; set; }
    }
}
