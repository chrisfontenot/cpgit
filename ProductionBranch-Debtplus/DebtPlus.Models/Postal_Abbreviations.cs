﻿using System;

namespace DebtPlus.Models
{
    /// <summary>
    /// List of abbreviations used in the address record. These came from the USPO.
    /// </summary>
    [Obsolete("Use LINQ Tables")]
    public class PostalAbbreviation : IComparable<PostalAbbreviation>, System.Collections.IComparer
    {
        /// <summary>
        /// List of valid items for the "Type" field in this record
        /// </summary>
        public enum TypeEnum
        {
            Empty = 0,                                  // Not valid. The record is empty.
            Direction = 1,                              // The direction, i.e. N, S, E, W, etc.
            Suffix = 2,                                 // The street suffix, i.e. "BLVD", "RD", etc.
            Modifier = 3                                // The modifier value, i.e. "APT", "STE", etc.
        }

        public Int32 Id { get; set; }
        public TypeEnum _Type { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public bool RequireModifier { get; set; }
        public bool Default { get; set; }

        public string NeedsNumber
        {
            get
            {
                return RequireModifier ? "Yes" : "No";
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public PostalAbbreviation()
        {
            Id = -1;
            _Type = TypeEnum.Empty;
            Description = string.Empty;
            Abbreviation = string.Empty;
            RequireModifier = false;
            Default = false;
        }

        /// <summary>
        /// Is the record "new", that is does not exist in the database?
        /// </summary>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmpty()
        {
            // Determine if the fields have changed since it was created
            if (!String.IsNullOrWhiteSpace(Description) || !String.IsNullOrEmpty(Abbreviation))
            {
                return false;
            }
            if (_Type != TypeEnum.Empty)
            {
                return false;
            }
            if (RequireModifier || Default)
            {
                return false;
            }

            // The record has not been modified since it was created
            return true;
        }

        /// <summary>
        /// Compare the current address structure to the one that is passed. Return the relationship.
        /// </summary>
        /// <param name="other">Pointer to the second address structure for the comparison.</param>
        /// <returns>The relative comparison value, -1, 0, +1</returns>
        public Int32 CompareTo(PostalAbbreviation other)
        {
            Int32 Relation = String.Compare(Description, other.Description, true, System.Globalization.CultureInfo.InvariantCulture);

            if (Relation == 0) Relation = String.Compare(Abbreviation, other.Abbreviation, true, System.Globalization.CultureInfo.InvariantCulture);
            if (Relation == 0) Relation = _Type.CompareTo(other._Type);
            if (Relation == 0) Relation = Default.CompareTo(other.Default);
            if (Relation == 0) Relation = RequireModifier.CompareTo(other.RequireModifier);

            return Relation;
        }

        /// <summary>
        /// Return the string format for the address record.
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return Description;
        }

        /// <summary>
        /// Comparison function for the sort operation. We sort a list by description only.
        /// </summary>
        /// <param name="x">First item for the comparison</param>
        /// <param name="y">Second item for hte comparison</param>
        /// <returns>The relative comparison value, -1, 0, +1</returns>
        public Int32 Compare(object x, object y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null)
            {
                return -1;
            }
            if (y == null)
            {
                return 1;
            }
            return string.Compare((x as PostalAbbreviation).Description, (y as PostalAbbreviation).Description, true);
        }
    }
}
