﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class Name : IComparable<Name>
    {
        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        public Name()
        {
            Id = -1;
            Prefix = null;
            First = null;
            Middle = null;
            Last = null;
            Suffix = null;
        }

        /// <summary>
        /// The record number in the database table "Names"
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 Id { get; set; }

        /// <summary>
        /// Prefix portion of the name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Prefix { get; set; }

        /// <summary>
        /// First name portion of the name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string First { get; set; }

        /// <summary>
        /// Middle name portion of the name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Middle { get; set; }

        /// <summary>
        /// Last name portion of the name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Last { get; set; }

        /// <summary>
        /// Suffix portion of the name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string Suffix { get; set; }

        /// <summary>
        /// Compare the two name records to determine the relative order.
        /// </summary>
        /// <param name="other">Pointer to the other name record for comparison</param>
        /// <returns></returns>
        public Int32 CompareTo(Name other) // Implements System.IComparable<AddressData>.CompareTo
        {
            Int32 Relation;

            // If the other item is missing then the current is always greater.
            if (other == null)
            {
                Relation = 1;
            }
            else
            {
                // Compare the prefix string
                Relation = Compare(Prefix, other.Prefix);
            }

            // Compare the other parts of the name until we find the first non-match
            if (Relation == 0)
            {
                Relation = Compare(First, other.First);
            }
            if (Relation == 0)
            {
                Relation = Compare(Middle, other.Middle);
            }
            if (Relation == 0)
            {
                Relation = Compare(Last, other.Last);
            }
            if (Relation == 0)
            {
                Relation = Compare(Suffix, other.Suffix);
            }
            return Relation;
        }

        /// <summary>
        /// Compare two strings and return the relation
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static Int32 Compare(string a, string b)
        {
            // If both are missing or empty then the strings are equal
            if (string.IsNullOrWhiteSpace(a) && string.IsNullOrWhiteSpace(b))
            {
                return 0;
            }

            // If the left is missing (and right is present) then the right is less
            if (string.IsNullOrWhiteSpace(a))
            {
                return -1;
            }

            // If the right is missing then it is greater
            if (string.IsNullOrWhiteSpace(b))
            {
                return 1;
            }

            // Otherwise, both strings are given. Do a normal, case-insitive compare.
            return string.Compare(a, b, true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Determine if the name has any data to be recorded
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            foreach (string item in new string[] { Prefix, First, Middle, Last, Suffix })
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Is the item a new record or does it exist in the database?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Return the string value for the current name
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return DebtPlus.LINQ.Name.FormatNormalName(Prefix, First, Middle, Last, Suffix);
        }
    }
}
