﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class ResourceType
    {
        public Int32 Id { get; set; }
        public string Description { get; set; }
        public string Label { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
