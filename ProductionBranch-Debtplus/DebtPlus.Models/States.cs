﻿using System;

namespace DebtPlus.Models
{
    /// <summary>
    /// List of abbreviations used in the address record. These came from the USPO.
    /// </summary>
    [Obsolete("Use LINQ Tables")]
    public class States : IComparable<States>, System.Collections.IComparer
    {
        public Int32 Id { get; set; }
        public string MailingCode { get; set; }
        public string Name { get; set; }
        public Int32 Country { get; set; }
        public Int32 TimeZoneID { get; set; }
        public bool USAFormat { get; set; }
        public bool Default { get; set; }
        public bool ActiveFlag { get; set; }
        public string AddressFormat { get; set; }
        public string hud_9902 { get; set; }
        public bool DoingBusiness { get; set; }
        public string created_by { get; set; }
        public DateTime date_created { get; set; }

        public string FormattedMailingCode
        {
            get
            {
                if (string.IsNullOrEmpty(MailingCode))
                {
                    return string.Empty;
                }
                return (MailingCode + "  ").Substring(0, 2);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public States()
        {
            Id = -1;
            Name = string.Empty;
            MailingCode = string.Empty;
            Country = 0;
            TimeZoneID = 0;
            USAFormat = false;
            Default = false;
            ActiveFlag = true;
            AddressFormat = "{0} {1}  {2}";
            hud_9902 = String.Empty;
            DoingBusiness = false;
        }

        /// <summary>
        /// Is the record "new", that is does not exist in the database?
        /// </summary>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Name) || Country == 0 || TimeZoneID == 0;
        }

        /// <summary>
        /// Compare the current address structure to the one that is passed. Return the relationship.
        /// </summary>
        /// <param name="other">Pointer to the second address structure for the comparison.</param>
        /// <returns>The relative comparison value, -1, 0, +1</returns>
        public Int32 CompareTo(States other)
        {
            Int32 Relation = Name.CompareTo(other.Name);
            if( Relation == 0 ) Relation = MailingCode.CompareTo(other.MailingCode);
            if( Relation == 0 ) Relation = Country.CompareTo(other.Country);
            if( Relation == 0 ) Relation = TimeZoneID.CompareTo(other.TimeZoneID);
            if( Relation == 0 ) Relation = AddressFormat.CompareTo(other.AddressFormat);
            if( Relation == 0 ) Relation = Default.CompareTo(other.Default);
            if( Relation == 0 ) Relation = ActiveFlag.CompareTo(other.ActiveFlag);
            if( Relation == 0 ) Relation = DoingBusiness.CompareTo(other.DoingBusiness);
            if( Relation == 0 ) Relation = hud_9902.CompareTo(other.hud_9902);
            return Relation;
        }

        /// <summary>
        /// Return the string format for the address record.
        /// </summary>
        /// <returns></returns>
        public new string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Comparison function for the sort operation. We sort the table by description only.
        /// </summary>
        /// <param name="x">First item for the comparison</param>
        /// <param name="y">Second item for hte comparison</param>
        /// <returns>The relative comparison value, -1, 0, +1</returns>
        public Int32 Compare(object x, object y)
        {
            if (x == null && y == null) return 0;

            var xState = x as States;
            var yState = y as States;
            if (x == null || xState == null) return -1;
            if (y == null || yState == null) return 1;

            return System.String.Compare((x as States).Name, (y as States).Name, System.StringComparison.OrdinalIgnoreCase);
        }
    }
}
