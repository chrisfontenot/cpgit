﻿using System;

namespace DebtPlus.Models
{
    [Obsolete("Use LINQ Tables")]
    public class NewClient
    {
        public Int32 Id { get; set; }
        public Int32 TelephoneId { get; set; }
        public Int32 MortgageProblem { get; set; }
        public Int32 ReferredBy { get; set; }
        public Int32 MethodFirstContact { get; set; }
    }
}
