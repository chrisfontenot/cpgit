﻿This library contains the models in the D+ system.  It must not depend on any other D+ project, and 
lives at the same level as the Repository.  Instances of classes defined in the Model library are 
passed back and forth from the data layer to the business logic layer or UI layer.