﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Server.Email
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var cls = new Mailer();
                cls.RunProgram();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Environment.Exit(2);
            }
        }
    }
}
