﻿// Define to disable production tests
// #define DEBUGGINGMODE

namespace DebtPlus.Server.Email
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Substitution information for the email are a list of this type
    /// </summary>
    public class Substitution
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Just a non-static class for the program. I am not fond of static classes
    /// just because they are a console application.
    /// </summary>
    public class Mailer
    {
        /// <summary>
        /// Configuration information
        /// </summary>
        private class Config
        {
            public Int32 Port              { get; set; }
            public string Server           { get; set; }
            public string ErrorsTo         { get; set; }
            public string UserName         { get; set; }
            public string Password         { get; set; }
            public string MailDirectory    { get; set; }
            public bool Ssl                { get; set; }
            public string InterfaceMode    { get; set; }
            public Int32 InterMessageDelay { get; set; }
            public Int32 Timeout           { get; set; }
            public string DebuggingTo      { get; set; }
            public Int32 MaxRetryCount     { get; set; }
            public Int32 RetentionDays     { get; set; }

            /// <summary>
            /// Initialize the interface
            /// </summary>
            public Config()
            {
                Port              = Properties.Settings.Default.SMTP_Port;
                Server            = Properties.Settings.Default.SMTP_Server;
                ErrorsTo          = Properties.Settings.Default.SMTP_ErrorsTo;
                UserName          = Properties.Settings.Default.SMTP_UserName;
                Password          = Properties.Settings.Default.SMTP_Password;
                MailDirectory     = Properties.Settings.Default.SMTP_MailOutputDirectory;
                InterfaceMode     = Properties.Settings.Default.SMTP_InterfaceMode;
                DebuggingTo       = Properties.Settings.Default.SMTP_DebuggingTo;
                InterMessageDelay = Properties.Settings.Default.SMTP_DelayBetweenMessages;
                Timeout           = Properties.Settings.Default.SMTP_Timeout;
                MaxRetryCount     = Properties.Settings.Default.MaxRetry;
                Ssl               = Properties.Settings.Default.SMTP_UseSSL;
                RetentionDays     = Properties.Settings.Default.RetentionDays;
            }
        }

        // Collection of records to be processed
        List<email_queue> colRecords = null;
        DebtPlusDataClassDataContext dc = null;

        // Dictionary of filename extensions to corresponding mime types
        System.Collections.Specialized.StringDictionary mimeTypes = new System.Collections.Specialized.StringDictionary();

        /// <summary>
        /// Process the mail queue
        /// </summary>
        public void RunProgram()
        {
            // Configuration information
            Config configSettings = new Config();

            // Start with the connection string from the settings region
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlus.Server.Email.Properties.Settings.DebtPlus"].ConnectionString;

            // Time for the current and next attempts
            DateTime currentAttempt = DateTime.UtcNow;
            DateTime nextAttempt    = currentAttempt.AddMinutes(10);

            // Allocate a data context for the data
            dc = new DebtPlusDataClassDataContext(connectionString);

            // Retrieve the list of email messages that we want to send. Terminate if there is nothing to do
            colRecords = dc.email_queues
#if DEBUGGINGMODE
                .Where(s => s.template == 7)
#else
                .Where(s => (s.status == "PENDING" || s.status == "FAIL")
                && (s.attempts < configSettings.MaxRetryCount)
                && (! s.next_attempt.HasValue || s.next_attempt.GetValueOrDefault() <= currentAttempt))
#endif
                .ToList();

            if (colRecords.Count > 0)
            {
                // Template information
                Int32 currentTemplateId = 0;
                email_template currentTemplate = null;

                // Send the message to the remote server
                using (var ieRecords = colRecords.OrderBy(s => s.template).ThenBy(s => s.Id).GetEnumerator())
                {
                    string htmlBody = null;
                    string textBody = null;
                    List<Attachment> colAttachments = null;

                    while (ieRecords.MoveNext())
                    {
                        var item = ieRecords.Current;

                        // Count the attempt to send the message and set the time for the last attempt.
                        item.attempts += 1;
                        item.last_attempt = currentAttempt;
                        item.next_attempt = nextAttempt;

                        try
                        {
                            // If the template has changed then retrieve the current template. The messages are in order by the template.
                            if (item.template != currentTemplateId)
                            {
                                currentTemplate = dc.email_templates.Where(s => s.Id == item.template).First();
                                colAttachments = Attachment.DecodeCollection(currentTemplate.attachments);
                                htmlBody = Encoding.UTF8.GetString(GetUri(currentTemplate.html_URI));
                                textBody = Encoding.UTF8.GetString(GetUri(currentTemplate.text_URI));
                                currentTemplateId = item.template;
                            }

                            // We need a target email address or we can't sent the message.
                            if (string.IsNullOrWhiteSpace(item.email_address))
                            {
                                item.status = "FAIL";
                                item.last_error = "Improperly formed email address";
                                continue;
                            }

                            // Try to send the message at this time.
                            SendNextEmailMessage(configSettings, currentTemplate, ref item,
                                (htmlBody ?? string.Empty) + string.Empty, (textBody ?? string.Empty) + string.Empty,
                                configSettings.RetentionDays, colAttachments);
                        }

                        catch (Exception ex)
                        {
                            // We really want the first exception, not just generic "failure to send email".
                            while (ex.InnerException != null)
                            {
                                ex = ex.InnerException;
                            }
                            item.last_error = ex.Message;
                            item.status = "FAIL";
                        }
                    }
                }
            }

            // Expire the items that have a death date less than today's value
            DateTime deathDate = DateTime.Now.Date;
            foreach (var item in dc.email_queues.Where(s => s.death_date != null && s.death_date <= deathDate))
            {
                dc.email_queues.DeleteOnSubmit(item);
            }

            // Submit the changes to the database.
            dc.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
        }

        // Delegate to tell how to convert mapped strings
        private delegate string HtmlConversionDelegate(string input);

        /// <summary>
        /// Send the single message to the remote server
        /// </summary>
        /// <param name="configSettings">Settings for the configuration properties</param>
        /// <param name="currentTemplate">Current template being processed</param>
        /// <param name="emailMessage">Pointer to the current message to be processed</param>
        /// <param name="htmlBody">HTML body for the message</param>
        /// <param name="textBody">Text body for the message</param>
        /// <param name="retentionDays">Number of days to keep the entry in the queue</param>
        /// <param name="colAttachments">List of attachments for this mail template</param>
        private void SendNextEmailMessage(Config configSettings, email_template currentTemplate, ref email_queue emailMessage, string htmlBody, string textBody, Int32 retentionDays, List<Attachment> colAttachments)
        {
            // Ensure that the stream pointers are gone. We will open the streams as needed later.
            foreach (var att in colAttachments)
            {
                att.msImage = null;
            }

            try
            {
                // Allocate the network link to send the next message
                using (var mailClient = new System.Net.Mail.SmtpClient(configSettings.Server, configSettings.Port))
                {
                    // Set the flag for SSL and other common items
                    mailClient.EnableSsl = configSettings.Ssl;
                    mailClient.Timeout   = configSettings.Timeout;

                    // If there is a credential then use it.
                    if (!string.IsNullOrEmpty(configSettings.UserName))
                    {
                        mailClient.Credentials = new System.Net.NetworkCredential(configSettings.UserName, configSettings.Password);
                    }

                    // Add the delivery information
                    if (String.Compare(configSettings.InterfaceMode, "PickupDirectoryFromIis", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        mailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.PickupDirectoryFromIis;
                    }

                    else if (String.Compare(configSettings.InterfaceMode, "Directory", StringComparison.OrdinalIgnoreCase) == 0 && !string.IsNullOrEmpty(Properties.Settings.Default.SMTP_MailOutputDirectory))
                    {
                        mailClient.DeliveryMethod          = System.Net.Mail.SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        mailClient.PickupDirectoryLocation = configSettings.MailDirectory;
                    }
                    else mailClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                    // Generate the email message
                    using (var msg = new System.Net.Mail.MailMessage())
                    {
                        if (!string.IsNullOrEmpty(configSettings.ErrorsTo))
                        {
                            msg.ReplyToList.Clear();
                            msg.ReplyToList.Add(
                                new System.Net.Mail.MailAddress(Properties.Settings.Default.SMTP_ErrorsTo));

                            // Add an old header for the error message delivery. It is not used as much but older mailers will still do it.
                            msg.Headers.Add("Errors-To", configSettings.ErrorsTo);
                        }

                        // Split the destination email address into sub-addresses so that we can send the message to multiple targets
                        string targetEmail = !string.IsNullOrEmpty(configSettings.DebuggingTo)
                            ? configSettings.DebuggingTo
                            : emailMessage.email_address;
                        string[] colTargets = targetEmail.Split(',');
                        if (colTargets.GetUpperBound(0) < 1)
                        {
                            colTargets = targetEmail.Split(';');
                        }

                        // Add all of the targets to the message list.
                        foreach (string strTarget in colTargets)
                        {
                            var trimTarget = strTarget.Trim();
                            if (trimTarget == string.Empty)
                            {
                                continue;
                            }

                            // Put the first item into the "to" field. The others go into the "bcc" field
                            if (msg.To.Count == 0)
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(trimTarget,
                                    emailMessage.email_name ?? trimTarget));
                            }
                            else
                            {
                                msg.Bcc.Add(new System.Net.Mail.MailAddress(trimTarget,
                                    emailMessage.email_name ?? trimTarget));
                            }
                        }

                        // Map the substitutions on the body text. Do not touch the original template text strings
                        // but copy them to the "new" versions. Use the "new" versions after substitution.
                        var subs = GetSubstitutions(emailMessage.substitutions);
                        string newSubject;
                        string newHtmlBody;
                        string newTextBody;

                        if (subs != null && subs.Count > 0)
                        {
                            newHtmlBody = performSubstitutions(htmlBody, subs, ConvertToHtml);
                            newTextBody = performSubstitutions(textBody, subs, input => input);
                            newSubject = performSubstitutions(currentTemplate.subject, subs, input => input);
                        }
                        else // There are no substitutions. Use the original text as-is.
                        {
                            newHtmlBody = htmlBody;
                            newTextBody = textBody;
                            newSubject = currentTemplate.subject;
                        }

                        msg.DeliveryNotificationOptions = System.Net.Mail.DeliveryNotificationOptions.OnFailure;
                        msg.Sender = new System.Net.Mail.MailAddress(currentTemplate.sender_email,
                            currentTemplate.sender_name ?? currentTemplate.sender_email);
                        msg.From = new System.Net.Mail.MailAddress(currentTemplate.sender_email,
                            currentTemplate.sender_name ?? currentTemplate.sender_email);
                        msg.Subject = newSubject;
                        msg.IsBodyHtml = false;

                        // Make one last check to ensure that there is something to be used as body text.
                        if (string.IsNullOrWhiteSpace(newTextBody) && string.IsNullOrWhiteSpace(newHtmlBody))
                        {
                            throw new ArgumentException("the message does not have any text to be sent.");
                        }

                        // If there is no html then we have a text body only
                        if (string.IsNullOrWhiteSpace(newHtmlBody))
                        {
                            msg.Body = newTextBody;
                        }

                        // There is HTML. Add it.
                        else
                        {
                            msg.Body = string.IsNullOrWhiteSpace(newTextBody) ? ConvertToHtml(newHtmlBody) : newTextBody;

                            var colResources = new List<System.Net.Mail.LinkedResource>();
                            newHtmlBody = ReplaceImgTags(currentTemplate, newHtmlBody, ref colResources);

                            // Create an alternate view for the HTML text block.
                            var htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(newHtmlBody, null,
                                "text/html");
                            msg.AlternateViews.Add(htmlView);

                            // Add the resources to the HTML view
                            foreach (var lnk in colResources)
                            {
                                htmlView.LinkedResources.Add(lnk);
                            }
                        }

                        // Add the list of attachment items to the message buffer
                        using (IEnumerator<Attachment> ieAttachment = colAttachments.GetEnumerator())
                        {
                            while (ieAttachment.MoveNext())
                            {
                                var att = ieAttachment.Current;

                                // Read the attachment into the image buffer if there is no image (but we have a filename)
                                if (!string.IsNullOrWhiteSpace(att.FileName) && att.Image == null)
                                {
                                    att.Image = readLocalFile(att.FileName);
                                }

                                // Ignore empty images
                                if (att.Image == null || att.Image.GetLength(0) < 1)
                                {
                                    continue;
                                }

                                // We need to keep the stream open until the message is sent.
                                att.msImage = new System.IO.MemoryStream(att.Image);

                                var mailAttachment = new System.Net.Mail.Attachment(att.msImage, att.Name)
                                {
                                    ContentType = {MediaType = att.Type},
                                    ContentId   = att.ContentID
                                };

                                // Fill in the ContentDisposition information for the attachment. It is generally incomplete so supply the missing pieces.
                                mailAttachment.ContentDisposition.CreationDate = DateTime.Now.Date;
                                mailAttachment.ContentDisposition.ModificationDate = DateTime.Now.Date;
                                mailAttachment.ContentDisposition.ReadDate = DateTime.Now.Date;
                                mailAttachment.ContentDisposition.FileName = att.Name;
                                mailAttachment.ContentDisposition.Inline = att.Inline;
                                mailAttachment.ContentDisposition.DispositionType = att.DispositionType;
                                mailAttachment.ContentDisposition.Size = att.msImage.Length;

                                msg.Attachments.Add(mailAttachment);
                            }

                            // Send the message
                            mailClient.Send(msg);

                            // Indicate the successful transmission of the message.
                            emailMessage.status = "SUCCESS";
                            emailMessage.date_sent = DateTime.UtcNow;
                            emailMessage.death_date = DateTime.Now.AddDays(retentionDays).Date;
                        }
                    }
                }
            }

            finally
            {
                // Ensure that the memory streams for the attachments are closed. We don't want them left dangling.
                foreach (var att in colAttachments)
                {
                    if (att.msImage != null)
                    {
                        att.msImage.Dispose();
                        att.msImage = null;
                    }
                }
            }
        }

        /// <summary>
        /// Replace the "img" tags in the html body with the corresponding attachment reference.
        /// </summary>
        private string ReplaceImgTags(email_template currentTemplate, string inputBodyText, ref List<System.Net.Mail.LinkedResource> colAttachments)
        {
            var sb = new StringBuilder(inputBodyText);

            var expr = new System.Text.RegularExpressions.Regex(@"\<img[^<]*\>", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
            var matchList = expr.Matches(sb.ToString(), 0);
            for (int index = matchList.Count - 1; index >= 0; --index)
            {
                var matchFound = matchList[index];
                if (matchFound.Length > 0)
                {
                    var strImg = matchFound.Groups[0].Captures[0].Value;
                    var newString = replaceImg(currentTemplate, strImg, ref colAttachments);
                    sb.Remove(matchFound.Index, matchFound.Length);
                    sb.Insert(matchFound.Index, newString);
                }
            }

            // The body text is the resulting buffer.
            return sb.ToString();
        }

        /// <summary>
        /// Look for the IMG src attribute and replace it with the appropriate "Content ID" field reference.
        /// </summary>
        /// <returns></returns>
        private string replaceImg(email_template currentTemplate, string inputString, ref List<System.Net.Mail.LinkedResource> colAttachments)
        {
            var rx = new System.Text.RegularExpressions.Regex("src=['\"]([^'\"]*)['\"]");
            var match = rx.Match(inputString, 0);
            if (match.Success)
            {
                var filename = match.Groups[1].Captures[0].Value;
#if DEBUGGINGMODE
                filename = System.IO.Path.Combine(@"D:\HPFBluePrint", System.IO.Path.GetFileName(filename));
#endif
                if (!System.IO.Path.IsPathRooted(filename))
                {
                    var htmlUri  = new Uri(currentTemplate.html_URI);
                    var pathName = System.IO.Path.GetDirectoryName(htmlUri.LocalPath);
                    filename     = System.IO.Path.Combine(pathName, filename);
                }

                var att = new System.Net.Mail.LinkedResource(filename);
                att.ContentId = Guid.NewGuid().ToString();
                att.ContentType = new System.Net.Mime.ContentType()
                {
                    MediaType = MimeTypes.MimeTypeMap.GetMimeType(System.IO.Path.GetExtension(filename)),
                    Name = System.IO.Path.GetFileName(filename)
                };

                // Add the attachment to the list for this message
                colAttachments.Add(att);

                // Replace the image tag with the "CID"
                return string.Format("{0} src=\"cid:{1}\" {2}", inputString.Substring(0, match.Index), att.ContentId, inputString.Substring(match.Index + match.Length));
            }
            return inputString;
        }

        /// <summary>
        /// Retrieve the URI file contents as a byte buffer
        /// </summary>
        /// <param name="fileName">The URI file name</param>
        /// <returns></returns>
        private byte[] GetUri(string fileName)
        {
            // If there is no entry then there is no result
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return null;
            }

            // Attempt to access the file contents. Look for a file reference and read it directly if so.
            Uri fileUri = new Uri(fileName);
            if (fileUri.IsFile)
            {
                return readLocalFile(fileUri.LocalPath);
            }

            return readNetPath(fileUri);
        }

        /// <summary>
        /// Retrieve the HTML text from a local disk system location
        /// </summary>
        /// <param name="fileName">The local filename to be used</param>
        /// <returns></returns>
        private byte[] readLocalFile(string fileName)
        {
#if DEBUGGINGMODE
            fileName = System.IO.Path.Combine(@"D:\HPFBluePrint", System.IO.Path.GetFileName(fileName));
#endif
            using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    var buffer = new byte[4096];
                    var count = fs.Read(buffer, 0, 4096);

                    // Write the full buffer to the local memory stream.
                    while (count == 4096)
                    {
                        ms.Write(buffer, 0, count);
                        count = fs.Read(buffer, 0, 4096);
                    }

                    // If there is a partial buffer, write it
                    if (count > 0)
                    {
                        ms.Write(buffer, 0, count);
                    }

                    ms.Position = 0L;
                    return ms.ToArray();
                }
            }
        }

        /// <summary>
        /// Retrieve the message buffer from a network request
        /// </summary>
        /// <param name="fileUri">The file URI to be used</param>
        /// <returns></returns>
        private byte[] readNetPath(Uri fileUri)
        {
            // Allocate a request structure to retrieve the data from the URI
            System.Net.WebRequest request = System.Net.WebRequest.Create(fileUri);
            request.Timeout               = 30*60*1000;
            request.UseDefaultCredentials = true;
            request.Proxy.Credentials     = request.Credentials;

            // Send the request to get the data
            System.Net.WebResponse response = request.GetResponse();

            // Read the response that comes back. This should be the text block
            using (System.IO.Stream fs = response.GetResponseStream())
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    var buffer = new byte[4096];
                    var count = fs.Read(buffer, 0, 4096);

                    // Write the full buffer to the local memory stream.
                    while (count == 4096)
                    {
                        ms.Write(buffer, 0, count);
                        count = fs.Read(buffer, 0, 4096);
                    }

                    // If there is a partial buffer, write it
                    if (count > 0)
                    {
                        ms.Write(buffer, 0, count);
                    }

                    ms.Position = 0L;
                    return ms.ToArray();
                }
            }
        }

        /// <summary>
        /// Perform the field substitutions
        /// </summary>
        /// <param name="messageTemplate">Message template text with the field references</param>
        /// <param name="subs">List of substitutions to be performed</param>
        /// <param name="conversionFunction">Function to perform the conversion of the field to its output value</param>
        /// <returns>The resulting message text</returns>
        private string performSubstitutions(string messageTemplate, List<Substitution> subs, HtmlConversionDelegate conversionFunction)
        {
            // Find the field references in the message template buffer.
            string regexp = @"\<field\s*id=""(?<id>[^""]+)"".*[/\\]>";
            return System.Text.RegularExpressions.Regex.Replace(
                messageTemplate,
                regexp,

                delegate(System.Text.RegularExpressions.Match m)
                {
                    // Locate the field for this item. If not found, remove the string.
                    var fieldName = m.Groups[1].Captures[0].Value;
                    var q = subs.Find(s => String.Compare(s.Field, fieldName, StringComparison.OrdinalIgnoreCase) == 0);
                    if (q == null)
                    {
                        return messageTemplate.Substring(m.Index, m.Length);
                    }

                    // Return the converted replacement string.
                    return conversionFunction(q.Value ?? string.Empty);
                },

                System.Text.RegularExpressions.RegexOptions.Multiline | System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.CultureInvariant
            );
        }

        /// <summary>
        /// Convert the input string from ASCII to formats suitable as HTML
        /// </summary>
        /// <returns>The input string, converted to proper HTML text</returns>
        private string ConvertToHtml(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";        // matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";              // match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";  // matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />

            var lineBreakRegex = new System.Text.RegularExpressions.Regex(lineBreak, System.Text.RegularExpressions.RegexOptions.Multiline);
            var stripFormattingRegex = new System.Text.RegularExpressions.Regex(stripFormatting, System.Text.RegularExpressions.RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new System.Text.RegularExpressions.Regex(tagWhiteSpace, System.Text.RegularExpressions.RegexOptions.Multiline);

            var text = System.Net.WebUtility.HtmlDecode(html);                      // Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");                      // Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);    // Strip formatting

            return stripFormattingRegex.Replace(text, string.Empty);
        }

        /// <summary>
        /// Collect the list of substitutions from the XML document. The document is parsed for a list of substitution fields.
        /// </summary>
        /// <param name="xHead">Pointer to the XML document head</param>
        /// <returns>A collection of substitution records</returns>
        private List<Substitution> GetSubstitutions(System.Xml.Linq.XElement xHead)
        {
            // Allocate a list. If there is no substitution field then return the empty list.
            var answer = new List<Substitution>();
            if (xHead == null)
            {
                return answer;
            }

            // Allocate a reader. If we can't then just quit here.
            var rdr = xHead.CreateReader(System.Xml.Linq.ReaderOptions.OmitDuplicateNamespaces);

            // Ignore any headers in the data
            rdr.MoveToContent();

            // Look for the <substitution> fields.
            while (rdr.Read())
            {
                if (rdr.NodeType == System.Xml.XmlNodeType.Element && String.Compare(rdr.Name, "substitution", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Substitution sub = getSubstitutionElement(ref rdr);
                    if (sub != null && ! string.IsNullOrWhiteSpace(sub.Field))
                    {
                        answer.Add(sub);
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Read the Substitution item
        /// </summary>
        /// <param name="rdr">Current XML reader</param>
        /// <returns>The next substitution record at the reader's position</returns>
        private Substitution getSubstitutionElement(ref System.Xml.XmlReader rdr)
        {
            string fieldName = null;
            string value     = null;
            while (! rdr.EOF)
            {
                // Look for the ending tag
                if (rdr.NodeType == System.Xml.XmlNodeType.EndElement && String.Compare(rdr.Name, "substitution", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    return new Substitution() { Field = fieldName, Value = value };
                }

                // Ignore non-element items
                if (rdr.NodeType != System.Xml.XmlNodeType.Element)
                {
                    rdr.Read();
                    continue;
                }

                // Process the field and value tags
                if (String.Compare(rdr.Name, "field", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    fieldName = rdr.ReadElementContentAsString();
                }
                else if (String.Compare(rdr.Name, "value", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    value = rdr.ReadElementContentAsString();
                }
                else
                {
                    rdr.Read();
                }
            }

            // The table did not contain the proper format
            return null;
        }
    }
}
