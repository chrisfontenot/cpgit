﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BottomLine
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BottomLine))
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_person_1_net = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_person_1_net = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_person_2_net = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_person_2_net = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_asset = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_asset = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_expense = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_expense = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_expense_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_expense_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow13 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_debt_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_debt_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow12 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_self_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lbl_v2_plan_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8})
        Me.PageHeader.HeightF = 62.5!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.HeightF = 253.1057!
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrTable1
        '
        Me.XrTable1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(32.29167!, 0.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow5, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9, Me.XrTableRow13, Me.XrTableRow11, Me.XrTableRow12, Me.XrTableRow10})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(690.2917!, 224.3605!)
        Me.XrTable1.StylePriority.UseFont = False
        '
        'XrTableRow6
        '
        Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell16, Me.XrTableCell15})
        Me.XrTableRow6.Name = "XrTableRow6"
        Me.XrTableRow6.Weight = 0.063829787234042548R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell8.Weight = 0.41606218586526644R
        '
        'XrTableCell16
        '
        Me.XrTableCell16.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell16.Name = "XrTableCell16"
        Me.XrTableCell16.StylePriority.UseFont = False
        Me.XrTableCell16.StylePriority.UseTextAlignment = False
        Me.XrTableCell16.Text = "CORRIENTE"
        Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell16.Weight = 0.21183687997209924R
        '
        'XrTableCell15
        '
        Me.XrTableCell15.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell15.Name = "XrTableCell15"
        Me.XrTableCell15.StylePriority.UseFont = False
        Me.XrTableCell15.StylePriority.UseTextAlignment = False
        Me.XrTableCell15.Text = "SUGIRIÓ"
        Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell15.Weight = 0.19446339939008142R
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.lbl_v2_self_person_1_net, Me.lbl_v2_plan_person_1_net})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.063829787234042548R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.CanGrow = False
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell1.StylePriority.UsePadding = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "Solicitante de Empleo Ingresos Mensuales"
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 0.41606218586526644R
        Me.XrTableCell1.WordWrap = False
        '
        'lbl_v2_self_person_1_net
        '
        Me.lbl_v2_self_person_1_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_person_1_net", "{0:c}")})
        Me.lbl_v2_self_person_1_net.Name = "lbl_v2_self_person_1_net"
        Me.lbl_v2_self_person_1_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_person_1_net.StylePriority.UsePadding = False
        Me.lbl_v2_self_person_1_net.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_person_1_net.Text = "v2_self_person_1_net"
        Me.lbl_v2_self_person_1_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_person_1_net.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_person_1_net
        '
        Me.lbl_v2_plan_person_1_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_person_1_net", "{0:c}")})
        Me.lbl_v2_plan_person_1_net.Name = "lbl_v2_plan_person_1_net"
        Me.lbl_v2_plan_person_1_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_person_1_net.StylePriority.UsePadding = False
        Me.lbl_v2_plan_person_1_net.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_person_1_net.Text = "v2_plan_person_1_net"
        Me.lbl_v2_plan_person_1_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_person_1_net.Weight = 0.19446339939008142R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.lbl_v2_self_person_2_net, Me.lbl_v2_plan_person_2_net})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 0.063829787234042548R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.CanGrow = False
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        Me.XrTableCell4.Text = "Ingreso mensual Solicitante Segundo de empleo"
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell4.Weight = 0.41606218586526644R
        Me.XrTableCell4.WordWrap = False
        '
        'lbl_v2_self_person_2_net
        '
        Me.lbl_v2_self_person_2_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_person_2_net", "{0:c}")})
        Me.lbl_v2_self_person_2_net.Name = "lbl_v2_self_person_2_net"
        Me.lbl_v2_self_person_2_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_person_2_net.StylePriority.UsePadding = False
        Me.lbl_v2_self_person_2_net.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_person_2_net.Text = "v2_self_person_2_net"
        Me.lbl_v2_self_person_2_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_person_2_net.Weight = 0.21183687997209927R
        '
        'lbl_v2_plan_person_2_net
        '
        Me.lbl_v2_plan_person_2_net.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_person_2_net", "{0:c}")})
        Me.lbl_v2_plan_person_2_net.Name = "lbl_v2_plan_person_2_net"
        Me.lbl_v2_plan_person_2_net.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_person_2_net.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_person_2_net.Text = "v2_plan_person_2_net"
        Me.lbl_v2_plan_person_2_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_person_2_net.Weight = 0.19446339939008142R
        '
        'XrTableRow5
        '
        Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.lbl_v2_self_asset, Me.lbl_v2_plan_asset})
        Me.XrTableRow5.Name = "XrTableRow5"
        Me.XrTableRow5.Weight = 0.063829787234042548R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.CanGrow = False
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell18.StylePriority.UseTextAlignment = False
        Me.XrTableCell18.Text = "Otros ingresos mensuales"
        Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell18.Weight = 0.41606218586526644R
        Me.XrTableCell18.WordWrap = False
        '
        'lbl_v2_self_asset
        '
        Me.lbl_v2_self_asset.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_asset", "{0:c}")})
        Me.lbl_v2_self_asset.Name = "lbl_v2_self_asset"
        Me.lbl_v2_self_asset.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_asset.StylePriority.UsePadding = False
        Me.lbl_v2_self_asset.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_asset.Text = "v2_self_asset"
        Me.lbl_v2_self_asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_asset.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_asset
        '
        Me.lbl_v2_plan_asset.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_asset", "{0:c}")})
        Me.lbl_v2_plan_asset.Name = "lbl_v2_plan_asset"
        Me.lbl_v2_plan_asset.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_asset.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_asset.Text = "v2_plan_asset"
        Me.lbl_v2_plan_asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_asset.Weight = 0.19446339939008142R
        '
        'XrTableRow4
        '
        Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14, Me.lbl_v2_self_total_assets, Me.lbl_v2_plan_total_assets})
        Me.XrTableRow4.Name = "XrTableRow4"
        Me.XrTableRow4.Weight = 0.063829787234042548R
        '
        'XrTableCell14
        '
        Me.XrTableCell14.CanGrow = False
        Me.XrTableCell14.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell14.Name = "XrTableCell14"
        Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell14.StylePriority.UseFont = False
        Me.XrTableCell14.StylePriority.UseTextAlignment = False
        Me.XrTableCell14.Text = "Ingreso Mensual Total parcial"
        Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell14.Weight = 0.41606218586526644R
        Me.XrTableCell14.WordWrap = False
        '
        'lbl_v2_self_total_assets
        '
        Me.lbl_v2_self_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_total_assets", "{0:c}")})
        Me.lbl_v2_self_total_assets.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_v2_self_total_assets.Name = "lbl_v2_self_total_assets"
        Me.lbl_v2_self_total_assets.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_total_assets.StylePriority.UseFont = False
        Me.lbl_v2_self_total_assets.StylePriority.UsePadding = False
        Me.lbl_v2_self_total_assets.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_total_assets.Text = "v2_self_total_assets"
        Me.lbl_v2_self_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_total_assets.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_total_assets
        '
        Me.lbl_v2_plan_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_total_assets", "{0:c}")})
        Me.lbl_v2_plan_total_assets.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_v2_plan_total_assets.Name = "lbl_v2_plan_total_assets"
        Me.lbl_v2_plan_total_assets.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_total_assets.StylePriority.UseFont = False
        Me.lbl_v2_plan_total_assets.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_total_assets.Text = "v2_plan_total_assets"
        Me.lbl_v2_plan_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_total_assets.Weight = 0.19446339939008142R
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 0.063829787234042548R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.CanGrow = False
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell7.StylePriority.UseTextAlignment = False
        Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell7.Weight = 0.822362465227447R
        Me.XrTableCell7.WordWrap = False
        '
        'XrTableRow7
        '
        Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell26, Me.lbl_v2_self_expense, Me.lbl_v2_plan_expense})
        Me.XrTableRow7.Name = "XrTableRow7"
        Me.XrTableRow7.Weight = 0.063829787234042548R
        '
        'XrTableCell26
        '
        Me.XrTableCell26.CanGrow = False
        Me.XrTableCell26.Name = "XrTableCell26"
        Me.XrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell26.StylePriority.UseTextAlignment = False
        Me.XrTableCell26.Text = "Total de gastos presupuestados"
        Me.XrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell26.Weight = 0.41606218586526644R
        Me.XrTableCell26.WordWrap = False
        '
        'lbl_v2_self_expense
        '
        Me.lbl_v2_self_expense.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_expense", "{0:c}")})
        Me.lbl_v2_self_expense.Name = "lbl_v2_self_expense"
        Me.lbl_v2_self_expense.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_expense.StylePriority.UsePadding = False
        Me.lbl_v2_self_expense.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_expense.Text = "v2_self_expense"
        Me.lbl_v2_self_expense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_expense.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_expense
        '
        Me.lbl_v2_plan_expense.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_expense", "{0:c}")})
        Me.lbl_v2_plan_expense.Name = "lbl_v2_plan_expense"
        Me.lbl_v2_plan_expense.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_expense.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_expense.Text = "v2_plan_expense"
        Me.lbl_v2_plan_expense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_expense.Weight = 0.19446339939008142R
        '
        'XrTableRow8
        '
        Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell30, Me.lbl_v2_self_other_debt, Me.lbl_v2_plan_other_debt})
        Me.XrTableRow8.Name = "XrTableRow8"
        Me.XrTableRow8.Weight = 0.063840115592043833R
        '
        'XrTableCell30
        '
        Me.XrTableCell30.CanGrow = False
        Me.XrTableCell30.Name = "XrTableCell30"
        Me.XrTableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell30.StylePriority.UseTextAlignment = False
        Me.XrTableCell30.Text = "Otras Deudas"
        Me.XrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell30.Weight = 0.41606218586526644R
        Me.XrTableCell30.WordWrap = False
        '
        'lbl_v2_self_other_debt
        '
        Me.lbl_v2_self_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_other_debt", "{0:c}")})
        Me.lbl_v2_self_other_debt.Name = "lbl_v2_self_other_debt"
        Me.lbl_v2_self_other_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_other_debt.StylePriority.UsePadding = False
        Me.lbl_v2_self_other_debt.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_other_debt.Text = "v2_self_other_debt"
        Me.lbl_v2_self_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_other_debt.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_other_debt
        '
        Me.lbl_v2_plan_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_other_debt", "{0:c}")})
        Me.lbl_v2_plan_other_debt.Name = "lbl_v2_plan_other_debt"
        Me.lbl_v2_plan_other_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_other_debt.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_other_debt.Text = "v2_plan_other_debt"
        Me.lbl_v2_plan_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_other_debt.Weight = 0.19446339939008142R
        '
        'XrTableRow9
        '
        Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell34, Me.lbl_v2_self_expense_total, Me.lbl_v2_plan_expense_total})
        Me.XrTableRow9.Name = "XrTableRow9"
        Me.XrTableRow9.Weight = 0.06384012264681567R
        '
        'XrTableCell34
        '
        Me.XrTableCell34.CanGrow = False
        Me.XrTableCell34.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell34.Name = "XrTableCell34"
        Me.XrTableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell34.StylePriority.UseFont = False
        Me.XrTableCell34.StylePriority.UseTextAlignment = False
        Me.XrTableCell34.Text = "Suma Parcial de Exceso o Déficit"
        Me.XrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell34.Weight = 0.41606218586526644R
        Me.XrTableCell34.WordWrap = False
        '
        'lbl_v2_self_expense_total
        '
        Me.lbl_v2_self_expense_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_expense_total", "{0:c}")})
        Me.lbl_v2_self_expense_total.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_v2_self_expense_total.Name = "lbl_v2_self_expense_total"
        Me.lbl_v2_self_expense_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_expense_total.StylePriority.UseFont = False
        Me.lbl_v2_self_expense_total.StylePriority.UsePadding = False
        Me.lbl_v2_self_expense_total.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_expense_total.Text = "v2_self_expense_total"
        Me.lbl_v2_self_expense_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_expense_total.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_expense_total
        '
        Me.lbl_v2_plan_expense_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_expense_total", "{0:c}")})
        Me.lbl_v2_plan_expense_total.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_v2_plan_expense_total.Name = "lbl_v2_plan_expense_total"
        Me.lbl_v2_plan_expense_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_expense_total.StylePriority.UseFont = False
        Me.lbl_v2_plan_expense_total.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_expense_total.Text = "v2_plan_expense_total"
        Me.lbl_v2_plan_expense_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_expense_total.Weight = 0.19446339939008142R
        '
        'XrTableRow13
        '
        Me.XrTableRow13.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6})
        Me.XrTableRow13.Name = "XrTableRow13"
        Me.XrTableRow13.Weight = 0.063840115592043833R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.StylePriority.UseTextAlignment = False
        Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell6.Weight = 0.822362465227447R
        '
        'XrTableRow11
        '
        Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell42, Me.lbl_v2_self_debt_payment, Me.lbl_v2_plan_debt_payment})
        Me.XrTableRow11.Name = "XrTableRow11"
        Me.XrTableRow11.Weight = 0.063840115592043833R
        '
        'XrTableCell42
        '
        Me.XrTableCell42.CanGrow = False
        Me.XrTableCell42.Name = "XrTableCell42"
        Me.XrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell42.StylePriority.UseTextAlignment = False
        Me.XrTableCell42.Text = "Pagos de Deuda"
        Me.XrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell42.Weight = 0.41606218586526644R
        Me.XrTableCell42.WordWrap = False
        '
        'lbl_v2_self_debt_payment
        '
        Me.lbl_v2_self_debt_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_debt_payment", "{0:c}")})
        Me.lbl_v2_self_debt_payment.Name = "lbl_v2_self_debt_payment"
        Me.lbl_v2_self_debt_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_debt_payment.StylePriority.UsePadding = False
        Me.lbl_v2_self_debt_payment.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_debt_payment.Text = "v2_self_debt_payment"
        Me.lbl_v2_self_debt_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_debt_payment.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_debt_payment
        '
        Me.lbl_v2_plan_debt_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_debt_payment", "{0:c}")})
        Me.lbl_v2_plan_debt_payment.Name = "lbl_v2_plan_debt_payment"
        Me.lbl_v2_plan_debt_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_debt_payment.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_debt_payment.Text = "v2_plan_debt_payment"
        Me.lbl_v2_plan_debt_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_debt_payment.Weight = 0.19446339939008142R
        '
        'XrTableRow12
        '
        Me.XrTableRow12.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2})
        Me.XrTableRow12.Name = "XrTableRow12"
        Me.XrTableRow12.Weight = 0.063840164975446562R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.StylePriority.UseTextAlignment = False
        Me.XrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell2.Weight = 0.822362465227447R
        '
        'XrTableRow10
        '
        Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell38, Me.lbl_v2_self_total, Me.lbl_v2_plan_total})
        Me.XrTableRow10.Name = "XrTableRow10"
        Me.XrTableRow10.Weight = 0.063840164975446562R
        '
        'XrTableCell38
        '
        Me.XrTableCell38.CanGrow = False
        Me.XrTableCell38.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell38.Name = "XrTableCell38"
        Me.XrTableCell38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell38.StylePriority.UseFont = False
        Me.XrTableCell38.StylePriority.UseTextAlignment = False
        Me.XrTableCell38.Text = "Suma de Exceso o Déficit"
        Me.XrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell38.Weight = 0.41606218586526644R
        Me.XrTableCell38.WordWrap = False
        '
        'lbl_v2_self_total
        '
        Me.lbl_v2_self_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_self_total", "{0:c}")})
        Me.lbl_v2_self_total.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_v2_self_total.Name = "lbl_v2_self_total"
        Me.lbl_v2_self_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_self_total.StylePriority.UseFont = False
        Me.lbl_v2_self_total.StylePriority.UsePadding = False
        Me.lbl_v2_self_total.StylePriority.UseTextAlignment = False
        Me.lbl_v2_self_total.Text = "v2_self_total"
        Me.lbl_v2_self_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_self_total.Weight = 0.21183687997209924R
        '
        'lbl_v2_plan_total
        '
        Me.lbl_v2_plan_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "v2_plan_total", "{0:c}")})
        Me.lbl_v2_plan_total.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.lbl_v2_plan_total.Name = "lbl_v2_plan_total"
        Me.lbl_v2_plan_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lbl_v2_plan_total.StylePriority.UseFont = False
        Me.lbl_v2_plan_total.StylePriority.UseTextAlignment = False
        Me.lbl_v2_plan_total.Text = "v2_plan_total"
        Me.lbl_v2_plan_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lbl_v2_plan_total.Weight = 0.19446339939008142R
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(752.1166!, 36.41666!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "RESUMEN"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'BottomLine
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "BottomLine_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_person_1_net As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_person_2_net As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_asset As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_total_assets As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_expense As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_other_debt As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_expense_total As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_debt_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_plan_total As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_person_1_net As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_person_2_net As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_asset As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_total_assets As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_expense As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_other_debt As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_expense_total As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_debt_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lbl_v2_self_total As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTableRow13 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow12 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
End Class
