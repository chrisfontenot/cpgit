#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class Budget

    Public Sub New()
        MyBase.New()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf BudgetSubReport_BeforePrint
        'AddHandler XrLabel_client_amount.BeforePrint, AddressOf XrLabel_client_amount_BeforePrint
        'AddHandler XrLabel_suggested_amount.BeforePrint, AddressOf XrLabel_suggested_amount_BeforePrint
        'AddHandler XrLabel_detail.BeforePrint, AddressOf XrLabel_detail_BeforePrint
        'AddHandler XrLabel_difference.BeforePrint, AddressOf XrLabel_difference_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Private Sub BudgetSubReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_BudgetDetail_ByBudget"
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                cn.Open()
                Dim Budget As Integer = CType(Rpt.Parameters("ParameterBudget").Value, Integer)
                If Budget <= 0 Then
                    Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT dbo.map_client_to_budget(@client)"
                            .CommandType = System.Data.CommandType.Text
                            .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                            Dim obj As Object = .ExecuteScalar
                            Budget = DebtPlus.Utils.Nulls.DInt(obj)
                        End With
                    End Using
                End If

                If Budget > 0 Then
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_PrintClient_BudgetDetail_ByBudget"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .CommandTimeout = 0
                            .Parameters.Add("@budget", System.Data.SqlDbType.Int).Value = Budget
                            .Parameters.Add("@Language", System.Data.SqlDbType.Int).Value = 2
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                    If Not tbl.Columns.Contains("difference") Then
                        tbl.Columns.Add("difference", GetType(System.Decimal), "[client_amount]-[suggested_amount]")
                    End If
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If

        If tbl IsNot Nothing Then
            Rpt.DataSource = New System.Data.DataView(tbl, System.String.Empty, "budget_category", System.Data.DataViewRowState.CurrentRows)
        End If
    End Sub

    Private Sub XrLabel_client_amount_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
            e.Cancel = Not Detail
        End With
    End Sub

    Private Sub XrLabel_suggested_amount_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
            e.Cancel = Not Detail
        End With
    End Sub

    Private Sub XrLabel_difference_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
            e.Cancel = Not Detail
        End With
    End Sub

    Private Sub XrLabel_detail_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim Heading As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("heading")) <> 0
            If Heading Then
                .BackColor = System.Drawing.Color.Red
                .ForeColor = System.Drawing.Color.White
                .Font = New System.Drawing.Font(.Font, System.Drawing.FontStyle.Bold)
            Else
                .BackColor = System.Drawing.Color.Transparent
                .ForeColor = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.ControlText)
                .Font = New System.Drawing.Font(.Font, System.Drawing.FontStyle.Regular)
            End If
        End With
    End Sub

End Class
