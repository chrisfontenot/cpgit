﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.Reports.Client.Packets.Testing
{
    public partial class Form1 : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form1()
        {
            InitializeComponent();

            this.Load += new EventHandler(Form1_Load);
            lookUpEdit1.EditValueChanged += FormChanged;
            textEdit1.EditValueChanged += FormChanged;
            simpleButton1.Enabled = false;
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            var PacketList = (from r in System.IO.Directory.GetFiles(System.IO.Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + @"\..\..\.."), "*.repx", System.IO.SearchOption.AllDirectories) select new Packets(r)).ToList();
            lookUpEdit1.Properties.DataSource = PacketList;

            // Load the previous values for the form
            int ClientId = Properties.Settings.Default.ClientID;
            string Packet = System.IO.Path.GetFileName(Properties.Settings.Default.PacketName);

            if (ClientId > 0)
            {
                textEdit1.Text = ClientId.ToString();
            }

            var p = (from l in PacketList where string.Compare(l.PacketName,Packet,true) == 0 select l).FirstOrDefault();
            lookUpEdit1.EditValue = p;
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Boolean enabled = (lookUpEdit1.EditValue != null && textEdit1.Text.Length > 0);
            simpleButton1.Enabled = enabled;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string PacketName = (lookUpEdit1.EditValue as Packets).FileName;
            Int32 ClientID = Convert.ToInt32(textEdit1.Text.Trim());

            // Start the packet processing
            try
            {
                DebtPlus.Interfaces.Reports.IReports rpt = new DebtPlus.Reports.Client.Packet.PacketReport(PacketName);
                if (rpt != null)
                {
                    ((DebtPlus.Interfaces.Client.IClient)rpt).ClientId = ClientID;
                    rpt.DisplayPreviewDialog();

                    // Save the settings for the next time
                    Properties.Settings.Default.ClientID = ClientID;
                    Properties.Settings.Default.PacketName = PacketName;
                    Properties.Settings.Default.Save();

                    if (Properties.Settings.Default.CloseWhenComplete)
                    {
                        Close();
                    }
                }
                else
                {
                    DebtPlus.Data.Forms.MessageBox.Show("Report creation failed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception error occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
