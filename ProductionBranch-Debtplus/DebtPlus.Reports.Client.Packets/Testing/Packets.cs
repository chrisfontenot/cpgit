﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Reports.Client.Packets.Testing
{
    public class Packets
    {
        public string PacketName
        {
            get
            {
                return System.IO.Path.GetFileName(FileName);
            }
            set
            {
            }
        }

        public string FileName { get; set; }

        public Packets(string FileName)
        {
            this.FileName = FileName;
        }
    }
}
