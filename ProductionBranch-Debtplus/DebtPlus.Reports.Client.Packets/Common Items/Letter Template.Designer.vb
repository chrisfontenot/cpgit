<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Letter_Template
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Letter_Template))
        Me.XrPictureBox_4_Template = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrRichText_Address_Template2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.ReportFooter_2_Template = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel_ReportFooter_Label_Template = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox_3_Template = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox_2_Template = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox_1_Template = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel_1_Template = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader_1_Template = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupFooter_1_Template = New DevExpress.XtraReports.UI.GroupFooterBand()
        CType(Me.XrRichText_Address_Template2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPictureBox_4_Template
        '
        Me.XrPictureBox_4_Template.Image = CType(resources.GetObject("XrPictureBox_4_Template.Image"), System.Drawing.Image)
        Me.XrPictureBox_4_Template.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPictureBox_4_Template.Name = "XrPictureBox_4_Template"
        Me.XrPictureBox_4_Template.SizeF = New System.Drawing.SizeF(198.0!, 100.0!)
        '
        'XrRichText_Address_Template2
        '
        Me.XrRichText_Address_Template2.LocationFloat = New DevExpress.Utils.PointFloat(220.7083!, 0.0!)
        Me.XrRichText_Address_Template2.Name = "XrRichText_Address_Template2"
        Me.XrRichText_Address_Template2.SerializableRtfString = resources.GetString("XrRichText_Address_Template2.SerializableRtfString")
        Me.XrRichText_Address_Template2.SizeF = New System.Drawing.SizeF(519.2917!, 100.0!)
        '
        'ReportFooter_2_Template
        '
        Me.ReportFooter_2_Template.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_ReportFooter_Label_Template})
        Me.ReportFooter_2_Template.HeightF = 23.0!
        Me.ReportFooter_2_Template.Name = "ReportFooter_2_Template"
        Me.ReportFooter_2_Template.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrLabel_ReportFooter_Label_Template
        '
        Me.XrLabel_ReportFooter_Label_Template.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_ReportFooter_Label_Template.Name = "XrLabel_ReportFooter_Label_Template"
        Me.XrLabel_ReportFooter_Label_Template.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ReportFooter_Label_Template.SizeF = New System.Drawing.SizeF(750.0!, 23.0!)
        Me.XrLabel_ReportFooter_Label_Template.StylePriority.UseTextAlignment = False
        Me.XrLabel_ReportFooter_Label_Template.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrPictureBox_3_Template
        '
        Me.XrPictureBox_3_Template.Image = CType(resources.GetObject("XrPictureBox_3_Template.Image"), System.Drawing.Image)
        Me.XrPictureBox_3_Template.LocationFloat = New DevExpress.Utils.PointFloat(510.0!, 0.0!)
        Me.XrPictureBox_3_Template.Name = "XrPictureBox_3_Template"
        Me.XrPictureBox_3_Template.SizeF = New System.Drawing.SizeF(79.0!, 74.0!)
        Me.XrPictureBox_3_Template.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'XrPictureBox_2_Template
        '
        Me.XrPictureBox_2_Template.Image = CType(resources.GetObject("XrPictureBox_2_Template.Image"), System.Drawing.Image)
        Me.XrPictureBox_2_Template.LocationFloat = New DevExpress.Utils.PointFloat(175.0!, 0.0!)
        Me.XrPictureBox_2_Template.Name = "XrPictureBox_2_Template"
        Me.XrPictureBox_2_Template.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
        Me.XrPictureBox_2_Template.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'XrPictureBox_1_Template
        '
        Me.XrPictureBox_1_Template.Image = CType(resources.GetObject("XrPictureBox_1_Template.Image"), System.Drawing.Image)
        Me.XrPictureBox_1_Template.LocationFloat = New DevExpress.Utils.PointFloat(77.0!, 0.0!)
        Me.XrPictureBox_1_Template.Name = "XrPictureBox_1_Template"
        Me.XrPictureBox_1_Template.SizeF = New System.Drawing.SizeF(46.0!, 74.0!)
        Me.XrPictureBox_1_Template.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'XrLabel_1_Template
        '
        Me.XrLabel_1_Template.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_1_Template.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 12.5!)
        Me.XrLabel_1_Template.Multiline = True
        Me.XrLabel_1_Template.Name = "XrLabel_1_Template"
        Me.XrLabel_1_Template.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_1_Template.SizeF = New System.Drawing.SizeF(204.1667!, 36.54167!)
        Me.XrLabel_1_Template.StylePriority.UseFont = False
        Me.XrLabel_1_Template.StylePriority.UseTextAlignment = False
        Me.XrLabel_1_Template.Text = "www.ClearPointCCS.org" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1-877-877-1995"
        Me.XrLabel_1_Template.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'GroupHeader_1_Template
        '
        Me.GroupHeader_1_Template.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox_4_Template, Me.XrRichText_Address_Template2})
        Me.GroupHeader_1_Template.Name = "GroupHeader_1_Template"
        '
        'GroupFooter_1_Template
        '
        Me.GroupFooter_1_Template.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox_1_Template, Me.XrPictureBox_2_Template, Me.XrPictureBox_3_Template, Me.XrLabel_1_Template})
        Me.GroupFooter_1_Template.HeightF = 74.0!
        Me.GroupFooter_1_Template.Name = "GroupFooter_1_Template"
        Me.GroupFooter_1_Template.PrintAtBottom = True
        '
        'Letter_Template
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportFooter_2_Template, Me.GroupHeader_1_Template, Me.GroupFooter_1_Template})
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 25, 25)
        Me.ReportPrintOptions.DetailCount = 1
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupFooter_1_Template, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_1_Template, 0)
        Me.Controls.SetChildIndex(Me.ReportFooter_2_Template, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.XrRichText_Address_Template2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Protected Friend WithEvents ReportFooter_2_Template As DevExpress.XtraReports.UI.ReportFooterBand
    Protected Friend WithEvents XrLabel_1_Template As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrPictureBox_3_Template As DevExpress.XtraReports.UI.XRPictureBox
    Protected Friend WithEvents XrPictureBox_2_Template As DevExpress.XtraReports.UI.XRPictureBox
    Protected Friend WithEvents XrPictureBox_1_Template As DevExpress.XtraReports.UI.XRPictureBox
    Protected Friend WithEvents XrPictureBox_4_Template As DevExpress.XtraReports.UI.XRPictureBox
    Protected Friend WithEvents XrRichText_Address_Template2 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrLabel_ReportFooter_Label_Template As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader_1_Template As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupFooter_1_Template As DevExpress.XtraReports.UI.GroupFooterBand
End Class
