<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DMPDebts
    Inherits DMPDebts_Simple

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPDebts))
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel_Client2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrTable9
        '
        Me.XrTable9.StylePriority.UsePadding = False
        '
        'XrTableRow37
        '
        Me.XrTableRow37.StylePriority.UseBorders = False
        Me.XrTableRow37.StylePriority.UseFont = False
        Me.XrTableRow37.StylePriority.UseTextAlignment = False
        '
        'XrTableCell98
        '
        Me.XrTableCell98.StylePriority.UseBorders = False
        '
        'XrTableCell101
        '
        Me.XrTableCell101.StylePriority.UseBackColor = False
        Me.XrTableCell101.StylePriority.UseBorderColor = False
        Me.XrTableCell101.StylePriority.UseBorders = False
        Me.XrTableCell101.StylePriority.UseBorderWidth = False
        Me.XrTableCell101.StylePriority.UseForeColor = False
        Me.XrTableCell101.StylePriority.UsePadding = False
        '
        'XrTableCell102
        '
        Me.XrTableCell102.StylePriority.UseBackColor = False
        Me.XrTableCell102.StylePriority.UseBorderColor = False
        Me.XrTableCell102.StylePriority.UseBorders = False
        Me.XrTableCell102.StylePriority.UseBorderWidth = False
        Me.XrTableCell102.StylePriority.UseForeColor = False
        '
        'XrTableCell103
        '
        Me.XrTableCell103.StylePriority.UseBorders = False
        '
        'XrTableRow34
        '
        Me.XrTableRow34.StylePriority.UseBorders = False
        Me.XrTableRow34.StylePriority.UseFont = False
        Me.XrTableRow34.StylePriority.UseTextAlignment = False
        '
        'XrTableCell8
        '
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        '
        'XrTableCell78
        '
        Me.XrTableCell78.StylePriority.UseBorders = False
        Me.XrTableCell78.StylePriority.UseTextAlignment = False
        '
        'XrTableCell70
        '
        Me.XrTableCell70.StylePriority.UseBorders = False
        '
        'XrTableCell90
        '
        Me.XrTableCell90.StylePriority.UseBorders = False
        '
        'XrTableCell93
        '
        Me.XrTableCell93.StylePriority.UseBorders = False
        Me.XrTableCell93.StylePriority.UseTextAlignment = False
        '
        'XrTable10
        '
        Me.XrTable10.StylePriority.UseBorders = False
        Me.XrTable10.StylePriority.UsePadding = False
        '
        'XrTableCell_creditor_name
        '
        Me.XrTableCell_creditor_name.StylePriority.UsePadding = False
        Me.XrTableCell_creditor_name.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_account_number
        '
        Me.XrTableCell_account_number.StylePriority.UseTextAlignment = False
        '
        'XrTableCel_balance
        '
        Me.XrTableCel_balance.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_dmp_rate
        '
        Me.XrTableCell_dmp_rate.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_disbursement_factor
        '
        Me.XrTableCell_disbursement_factor.StylePriority.UseTextAlignment = False
        '
        'XrTableCellnon_dmp_rate
        '
        Me.XrTableCellnon_dmp_rate.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_non_dmp_payment
        '
        Me.XrTableCell_non_dmp_payment.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_payout_date
        '
        Me.XrTableCell_payout_date.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
        Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_creditor_address
        '
        Me.XrTableCell_creditor_address.Scripts.OnBeforePrint = "XrTableCell_creditor_address_BeforePrint"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1, Me.XrLabel3, Me.XrPanel1, Me.XrPanel2, Me.XrLabel_Client2})
        Me.ReportFooter.HeightF = 578.1251!
        Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel_Client2, 0)
        Me.ReportFooter.Controls.SetChildIndex(Me.XrPanel2, 0)
        Me.ReportFooter.Controls.SetChildIndex(Me.XrPanel1, 0)
        Me.ReportFooter.Controls.SetChildIndex(Me.XrLabel3, 0)
        Me.ReportFooter.Controls.SetChildIndex(Me.XrRichText1, 0)
        Me.ReportFooter.Controls.SetChildIndex(Me.XrTable11, 0)
        '
        'XrTable11
        '
        Me.XrTable11.StylePriority.UseBorderColor = False
        Me.XrTable11.StylePriority.UseBorders = False
        Me.XrTable11.StylePriority.UseFont = False
        Me.XrTable11.StylePriority.UseForeColor = False
        Me.XrTable11.StylePriority.UsePadding = False
        '
        'XrTableRow36
        '
        Me.XrTableRow36.StylePriority.UseFont = False
        Me.XrTableRow36.StylePriority.UseTextAlignment = False
        '
        'XrTableCell80
        '
        Me.XrTableCell80.StylePriority.UseBorders = False
        Me.XrTableCell80.StylePriority.UseTextAlignment = False
        '
        'XrTableCell_total_balance
        '
        Me.XrTableCell_total_balance.StylePriority.UseBorders = False
        Me.XrTableCell_total_balance.StylePriority.UseBorderWidth = False
        '
        'XrTableCell_total_disbursement_factor
        '
        Me.XrTableCell_total_disbursement_factor.StylePriority.UseBorders = False
        Me.XrTableCell_total_disbursement_factor.StylePriority.UseBorderWidth = False
        '
        'XrTableCell_total_non_dmp_payment
        '
        Me.XrTableCell_total_non_dmp_payment.StylePriority.UseBorders = False
        Me.XrTableCell_total_non_dmp_payment.StylePriority.UseBorderWidth = False
        '
        'XrTableCell95
        '
        Me.XrTableCell95.StylePriority.UseBorders = False
        '
        'XrLabel12
        '
        Me.XrLabel12.StylePriority.UseBorders = False
        '
        'XrLabel9
        '
        Me.XrLabel9.StylePriority.UseBorders = False
        '
        'XrLabel1
        '
        Me.XrLabel1.StylePriority.UseBorders = False
        '
        'XrLabel2
        '
        Me.XrLabel2.StylePriority.UseBorders = False
        '
        'XrLabel5
        '
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Client
        '
        Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client.StylePriority.UseFont = False
        Me.XrLabel_Client.StylePriority.UseTextAlignment = False
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.StylePriority.UsePadding = False
        '
        'XrLabel_PageHeader
        '
        Me.XrLabel_PageHeader.StylePriority.UseFont = False
        Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 40.58335!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(750.4167!, 391.7083!)
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0002384186!, 442.0417!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrLabel3.Scripts.OnBeforePrint = "XrLabel_ClientNumber_BeforePrint"
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(549.9996!, 18.00003!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UsePadding = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "I UNDERSTAND THAT THIS IS ONLY AN ESTIMATE AND IS IN NO WAY BINDING"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrPanel2
        '
        Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel2})
        Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 523.9584!)
        Me.XrPanel2.Name = "XrPanel2"
        Me.XrPanel2.SizeF = New System.Drawing.SizeF(753.1249!, 54.16672!)
        '
        'XrLabel_Client2
        '
        Me.XrLabel_Client2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Client2.LocationFloat = New DevExpress.Utils.PointFloat(592.7083!, 442.0417!)
        Me.XrLabel_Client2.Name = "XrLabel_Client2"
        Me.XrLabel_Client2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrLabel_Client2.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client2.SizeF = New System.Drawing.SizeF(160.4166!, 18.00003!)
        Me.XrLabel_Client2.StylePriority.UseFont = False
        Me.XrLabel_Client2.StylePriority.UsePadding = False
        Me.XrLabel_Client2.StylePriority.UseTextAlignment = False
        Me.XrLabel_Client2.Text = "Client: 0000000"
        Me.XrLabel_Client2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel9})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0002384186!, 469.7917!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(753.1249!, 54.16672!)
        '
        'DMPDebts
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportFooter, Me.PageFooter})
        Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
        Me.ReportPrintOptions.PrintOnEmptyDataSource = False
        Me.Scripts.OnBeforePrint = "DMPDebts_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Protected Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Protected Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Protected Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_Client2 As DevExpress.XtraReports.UI.XRLabel
End Class
