<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DMPDebts
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DMPDebts))
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.XrTable9 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow37 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell98 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell101 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell102 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell103 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow34 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell78 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell70 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell81 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell71 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell97 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell87 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell90 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell93 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable10 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow35 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_name = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_account_number = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCel_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCellnon_dmp_rate = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_payout_date = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_creditor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow38 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell_creditor_address = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell107 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Client2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrTable11 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow36 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell80 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_balance = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_disbursement_factor = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_non_dmp_payment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell95 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable9})
        Me.PageHeader.HeightF = 140.25!
        'Me.PageHeader.Controls.SetChildIndex(Me.XrTable9, 0)
        'Me.PageHeader.Controls.SetChildIndex(Me.XrPictureBox1, 0)
        'Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_PageHeader, 0)
        ''
        ''XrPictureBox1
        ''
        'Me.XrPictureBox1.StylePriority.UsePadding = False
        ''
        ''XrLabel_PageHeader
        ''
        'Me.XrLabel_PageHeader.StylePriority.UseFont = False
        'Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable10})
        Me.Detail.HeightF = 34.0!
        '
        'XrTable9
        '
        Me.XrTable9.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.XrTable9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 106.25!)
        Me.XrTable9.Name = "XrTable9"
        Me.XrTable9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable9.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow37, Me.XrTableRow34})
        Me.XrTable9.SizeF = New System.Drawing.SizeF(743.7501!, 34.0!)
        Me.XrTable9.StylePriority.UsePadding = False
        '
        'XrTableRow37
        '
        Me.XrTableRow37.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow37.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell98, Me.XrTableCell101, Me.XrTableCell102, Me.XrTableCell103})
        Me.XrTableRow37.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow37.Name = "XrTableRow37"
        Me.XrTableRow37.StylePriority.UseBorders = False
        Me.XrTableRow37.StylePriority.UseFont = False
        Me.XrTableRow37.StylePriority.UseTextAlignment = False
        Me.XrTableRow37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.XrTableRow37.Weight = 1.0R
        '
        'XrTableCell98
        '
        Me.XrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell98.CanGrow = False
        Me.XrTableCell98.Name = "XrTableCell98"
        Me.XrTableCell98.StylePriority.UseBorders = False
        Me.XrTableCell98.Weight = 1.5R
        '
        'XrTableCell101
        '
        Me.XrTableCell101.BackColor = System.Drawing.Color.Black
        Me.XrTableCell101.BorderColor = System.Drawing.Color.White
        Me.XrTableCell101.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell101.BorderWidth = 5
        Me.XrTableCell101.CanGrow = False
        Me.XrTableCell101.ForeColor = System.Drawing.Color.White
        Me.XrTableCell101.Name = "XrTableCell101"
        Me.XrTableCell101.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTableCell101.StylePriority.UseBackColor = False
        Me.XrTableCell101.StylePriority.UseBorderColor = False
        Me.XrTableCell101.StylePriority.UseBorders = False
        Me.XrTableCell101.StylePriority.UseBorderWidth = False
        Me.XrTableCell101.StylePriority.UseForeColor = False
        Me.XrTableCell101.StylePriority.UsePadding = False
        Me.XrTableCell101.Text = "PLAN"
        Me.XrTableCell101.Weight = 0.61175878630211611R
        '
        'XrTableCell102
        '
        Me.XrTableCell102.BackColor = System.Drawing.Color.Black
        Me.XrTableCell102.BorderColor = System.Drawing.Color.White
        Me.XrTableCell102.Borders = CType((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell102.BorderWidth = 5
        Me.XrTableCell102.CanGrow = False
        Me.XrTableCell102.ForeColor = System.Drawing.Color.White
        Me.XrTableCell102.Name = "XrTableCell102"
        Me.XrTableCell102.StylePriority.UseBackColor = False
        Me.XrTableCell102.StylePriority.UseBorderColor = False
        Me.XrTableCell102.StylePriority.UseBorders = False
        Me.XrTableCell102.StylePriority.UseBorderWidth = False
        Me.XrTableCell102.StylePriority.UseForeColor = False
        Me.XrTableCell102.Text = "SELF"
        Me.XrTableCell102.Weight = 0.61748744332011629R
        '
        'XrTableCell103
        '
        Me.XrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
        Me.XrTableCell103.CanGrow = False
        Me.XrTableCell103.Name = "XrTableCell103"
        Me.XrTableCell103.StylePriority.UseBorders = False
        Me.XrTableCell103.Weight = 0.85869376331118485R
        '
        'XrTableRow34
        '
        Me.XrTableRow34.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableRow34.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell78, Me.XrTableCell70, Me.XrTableCell81, Me.XrTableCell71, Me.XrTableCell97, Me.XrTableCell87, Me.XrTableCell90, Me.XrTableCell93})
        Me.XrTableRow34.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow34.Name = "XrTableRow34"
        Me.XrTableRow34.StylePriority.UseBorders = False
        Me.XrTableRow34.StylePriority.UseFont = False
        Me.XrTableRow34.StylePriority.UseTextAlignment = False
        Me.XrTableRow34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow34.Weight = 1.0R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell8.CanGrow = False
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.StylePriority.UseBorders = False
        Me.XrTableCell8.StylePriority.UseTextAlignment = False
        Me.XrTableCell8.Text = "Name"
        Me.XrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell8.Weight = 0.8944722827347481R
        '
        'XrTableCell78
        '
        Me.XrTableCell78.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell78.CanGrow = False
        Me.XrTableCell78.Name = "XrTableCell78"
        Me.XrTableCell78.StylePriority.UseBorders = False
        Me.XrTableCell78.StylePriority.UseTextAlignment = False
        Me.XrTableCell78.Text = "ACCT"
        Me.XrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell78.Weight = 0.21708540107311997R
        '
        'XrTableCell70
        '
        Me.XrTableCell70.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell70.CanGrow = False
        Me.XrTableCell70.Name = "XrTableCell70"
        Me.XrTableCell70.StylePriority.UseBorders = False
        Me.XrTableCell70.Text = "Balance"
        Me.XrTableCell70.Weight = 0.38844231619213188R
        '
        'XrTableCell81
        '
        Me.XrTableCell81.CanGrow = False
        Me.XrTableCell81.Name = "XrTableCell81"
        Me.XrTableCell81.Text = "Rate"
        Me.XrTableCell81.Weight = 0.28371872791692843R
        '
        'XrTableCell71
        '
        Me.XrTableCell71.CanGrow = False
        Me.XrTableCell71.Name = "XrTableCell71"
        Me.XrTableCell71.Text = "Payment"
        Me.XrTableCell71.Weight = 0.32804005991873442R
        '
        'XrTableCell97
        '
        Me.XrTableCell97.CanGrow = False
        Me.XrTableCell97.Name = "XrTableCell97"
        Me.XrTableCell97.Text = "Rate"
        Me.XrTableCell97.Weight = 0.28944709202752039R
        '
        'XrTableCell87
        '
        Me.XrTableCell87.CanGrow = False
        Me.XrTableCell87.Name = "XrTableCell87"
        Me.XrTableCell87.Text = "Payment"
        Me.XrTableCell87.Weight = 0.32804034975904922R
        '
        'XrTableCell90
        '
        Me.XrTableCell90.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell90.CanGrow = False
        Me.XrTableCell90.Name = "XrTableCell90"
        Me.XrTableCell90.StylePriority.UseBorders = False
        Me.XrTableCell90.Text = "Payout"
        Me.XrTableCell90.Weight = 0.31839196056576835R
        '
        'XrTableCell93
        '
        Me.XrTableCell93.Borders = CType(((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTableCell93.CanGrow = False
        Me.XrTableCell93.Name = "XrTableCell93"
        Me.XrTableCell93.StylePriority.UseBorders = False
        Me.XrTableCell93.StylePriority.UseTextAlignment = False
        Me.XrTableCell93.Text = "Creditor ID"
        Me.XrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell93.Weight = 0.5403018027454165R
        '
        'XrTable10
        '
        Me.XrTable10.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable10.LocationFloat = New DevExpress.Utils.PointFloat(0.0002441406!, 0.0!)
        Me.XrTable10.Name = "XrTable10"
        Me.XrTable10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable10.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow35, Me.XrTableRow38})
        Me.XrTable10.SizeF = New System.Drawing.SizeF(743.7499!, 34.0!)
        Me.XrTable10.StylePriority.UseBorders = False
        Me.XrTable10.StylePriority.UsePadding = False
        '
        'XrTableRow35
        '
        Me.XrTableRow35.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_name, Me.XrTableCell_account_number, Me.XrTableCel_balance, Me.XrTableCell_dmp_rate, Me.XrTableCell_disbursement_factor, Me.XrTableCellnon_dmp_rate, Me.XrTableCell_non_dmp_payment, Me.XrTableCell_payout_date, Me.XrTableCell_creditor})
        Me.XrTableRow35.Name = "XrTableRow35"
        Me.XrTableRow35.Weight = 1.0R
        '
        'XrTableCell_creditor_name
        '
        Me.XrTableCell_creditor_name.CanShrink = True
        Me.XrTableCell_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
        Me.XrTableCell_creditor_name.Name = "XrTableCell_creditor_name"
        Me.XrTableCell_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrTableCell_creditor_name.StylePriority.UsePadding = False
        Me.XrTableCell_creditor_name.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor_name.Weight = 0.8944723918098616R
        Me.XrTableCell_creditor_name.WordWrap = False
        '
        'XrTableCell_account_number
        '
        Me.XrTableCell_account_number.CanGrow = False
        Me.XrTableCell_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrTableCell_account_number.Name = "XrTableCell_account_number"
        Me.XrTableCell_account_number.StylePriority.UseTextAlignment = False
        Me.XrTableCell_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_account_number.Weight = 0.21708544706405339R
        Me.XrTableCell_account_number.WordWrap = False
        '
        'XrTableCel_balance
        '
        Me.XrTableCel_balance.CanGrow = False
        Me.XrTableCel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrTableCel_balance.Name = "XrTableCel_balance"
        Me.XrTableCel_balance.StylePriority.UseTextAlignment = False
        Me.XrTableCel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCel_balance.Weight = 0.388442161126085R
        Me.XrTableCel_balance.WordWrap = False
        '
        'XrTableCell_dmp_rate
        '
        Me.XrTableCell_dmp_rate.CanGrow = False
        Me.XrTableCell_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_interest", "{0:p}")})
        Me.XrTableCell_dmp_rate.Name = "XrTableCell_dmp_rate"
        Me.XrTableCell_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCell_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_dmp_rate.Weight = 0.28371872791692843R
        Me.XrTableCell_dmp_rate.WordWrap = False
        '
        'XrTableCell_disbursement_factor
        '
        Me.XrTableCell_disbursement_factor.CanGrow = False
        Me.XrTableCell_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment", "{0:c}")})
        Me.XrTableCell_disbursement_factor.Name = "XrTableCell_disbursement_factor"
        Me.XrTableCell_disbursement_factor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_disbursement_factor.Weight = 0.32804005991873431R
        Me.XrTableCell_disbursement_factor.WordWrap = False
        '
        'XrTableCellnon_dmp_rate
        '
        Me.XrTableCellnon_dmp_rate.CanGrow = False
        Me.XrTableCellnon_dmp_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_interest", "{0:p}")})
        Me.XrTableCellnon_dmp_rate.Name = "XrTableCellnon_dmp_rate"
        Me.XrTableCellnon_dmp_rate.StylePriority.UseTextAlignment = False
        Me.XrTableCellnon_dmp_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCellnon_dmp_rate.Weight = 0.28944738646847523R
        Me.XrTableCellnon_dmp_rate.WordWrap = False
        '
        'XrTableCell_non_dmp_payment
        '
        Me.XrTableCell_non_dmp_payment.CanGrow = False
        Me.XrTableCell_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment", "{0:c}")})
        Me.XrTableCell_non_dmp_payment.Name = "XrTableCell_non_dmp_payment"
        Me.XrTableCell_non_dmp_payment.StylePriority.UseTextAlignment = False
        Me.XrTableCell_non_dmp_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_non_dmp_payment.Weight = 0.3232161551624087R
        Me.XrTableCell_non_dmp_payment.WordWrap = False
        '
        'XrTableCell_payout_date
        '
        Me.XrTableCell_payout_date.CanGrow = False
        Me.XrTableCell_payout_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payout", "{0:M/yy}")})
        Me.XrTableCell_payout_date.Name = "XrTableCell_payout_date"
        Me.XrTableCell_payout_date.StylePriority.UseTextAlignment = False
        Me.XrTableCell_payout_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_payout_date.Weight = 0.3232161551624087R
        Me.XrTableCell_payout_date.WordWrap = False
        '
        'XrTableCell_creditor
        '
        Me.XrTableCell_creditor.CanGrow = False
        Me.XrTableCell_creditor.Name = "XrTableCell_creditor"
        Me.XrTableCell_creditor.Scripts.OnBeforePrint = "XrTableCell_creditor_BeforePrint"
        Me.XrTableCell_creditor.StylePriority.UseTextAlignment = False
        Me.XrTableCell_creditor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell_creditor.Weight = 0.5403009194225521R
        Me.XrTableCell_creditor.WordWrap = False
        '
        'XrTableRow38
        '
        Me.XrTableRow38.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell_creditor_address, Me.XrTableCell107})
        Me.XrTableRow38.Name = "XrTableRow38"
        Me.XrTableRow38.Visible = False
        Me.XrTableRow38.Weight = 1.0R
        '
        'XrTableCell_creditor_address
        '
        Me.XrTableCell_creditor_address.Multiline = True
        Me.XrTableCell_creditor_address.Name = "XrTableCell_creditor_address"
        Me.XrTableCell_creditor_address.Scripts.OnBeforePrint = "XrTableCell_creditor_address_BeforePrint"
        Me.XrTableCell_creditor_address.Weight = 1.1115567040493066R
        Me.XrTableCell_creditor_address.WordWrap = False
        '
        'XrTableCell107
        '
        Me.XrTableCell107.Name = "XrTableCell107"
        Me.XrTableCell107.Weight = 2.4763827000022007R
        Me.XrTableCell107.WordWrap = False
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel3, Me.XrPanel2, Me.XrLabel_Client2, Me.XrPanel1, Me.XrRichText1, Me.XrTable11})
        Me.ReportFooter.HeightF = 578.1251!
        Me.ReportFooter.KeepTogether = True
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.0002384186!, 442.0417!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrLabel3.Scripts.OnBeforePrint = "XrLabel_ClientNumber_BeforePrint"
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(549.9996!, 18.00003!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UsePadding = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "I UNDERSTAND THAT THIS IS ONLY AN ESTIMATE AND IS IN NO WAY BINDING"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
        '
        'XrPanel2
        '
        Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel2})
        Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 523.9584!)
        Me.XrPanel2.Name = "XrPanel2"
        Me.XrPanel2.SizeF = New System.Drawing.SizeF(753.1249!, 54.16672!)
        '
        'XrLabel01
        '
        Me.XrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.4167!)
        Me.XrLabel1.Name = "XrLabel01"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(549.9999!, 16.75002!)
        Me.XrLabel1.StylePriority.UseBorders = False
        Me.XrLabel1.Text = "Client Signature"
        '
        'XrLabel2
        '
        Me.XrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 37.41673!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(178.1249!, 16.75!)
        Me.XrLabel2.StylePriority.UseBorders = False
        Me.XrLabel2.Text = "Date"
        '
        'XrLabel_Client2
        '
        Me.XrLabel_Client2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Client2.LocationFloat = New DevExpress.Utils.PointFloat(592.7083!, 442.0417!)
        Me.XrLabel_Client2.Name = "XrLabel_Client2"
        Me.XrLabel_Client2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrLabel_Client2.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client2.SizeF = New System.Drawing.SizeF(160.4166!, 18.00003!)
        Me.XrLabel_Client2.StylePriority.UseFont = False
        Me.XrLabel_Client2.StylePriority.UsePadding = False
        Me.XrLabel_Client2.StylePriority.UseTextAlignment = False
        Me.XrLabel_Client2.Text = "Client: 0000000"
        Me.XrLabel_Client2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12, Me.XrLabel9})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0002384186!, 469.7917!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(753.1249!, 54.16672!)
        '
        'XrLabel12
        '
        Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.4167!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(549.9999!, 16.75002!)
        Me.XrLabel12.StylePriority.UseBorders = False
        Me.XrLabel12.Text = "Client Signature"
        '
        'XrLabel09
        '
        Me.XrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 37.41673!)
        Me.XrLabel9.Name = "XrLabel09"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(178.1249!, 16.75!)
        Me.XrLabel9.StylePriority.UseBorders = False
        Me.XrLabel9.Text = "Date"
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 40.58335!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(750.4167!, 391.7083!)
        '
        'XrTable11
        '
        Me.XrTable11.BorderColor = System.Drawing.Color.Teal
        Me.XrTable11.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
            Or DevExpress.XtraPrinting.BorderSide.Right) _
            Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
        Me.XrTable11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTable11.ForeColor = System.Drawing.Color.Teal
        Me.XrTable11.LocationFloat = New DevExpress.Utils.PointFloat(0.0002441406!, 10.0!)
        Me.XrTable11.Name = "XrTable11"
        Me.XrTable11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrTable11.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow36})
        Me.XrTable11.SizeF = New System.Drawing.SizeF(743.75!, 17.0!)
        Me.XrTable11.StylePriority.UseBorderColor = False
        Me.XrTable11.StylePriority.UseBorders = False
        Me.XrTable11.StylePriority.UseFont = False
        Me.XrTable11.StylePriority.UseForeColor = False
        Me.XrTable11.StylePriority.UsePadding = False
        '
        'XrTableRow36
        '
        Me.XrTableRow36.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell80, Me.XrTableCell_total_balance, Me.XrTableCell_total_disbursement_factor, Me.XrTableCell_total_non_dmp_payment, Me.XrTableCell95})
        Me.XrTableRow36.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableRow36.Name = "XrTableRow36"
        Me.XrTableRow36.StylePriority.UseFont = False
        Me.XrTableRow36.StylePriority.UseTextAlignment = False
        Me.XrTableRow36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableRow36.Weight = 0.47999998276654476R
        '
        'XrTableCell80
        '
        Me.XrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrTableCell80.Name = "XrTableCell80"
        Me.XrTableCell80.StylePriority.UseBorders = False
        Me.XrTableCell80.StylePriority.UseTextAlignment = False
        Me.XrTableCell80.Text = "TOTALS"
        Me.XrTableCell80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell80.Weight = 0.8944723986141645R
        '
        'XrTableCell_total_balance
        '
        Me.XrTableCell_total_balance.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrTableCell_total_balance.BorderWidth = 2
        Me.XrTableCell_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance")})
        Me.XrTableCell_total_balance.Name = "XrTableCell_total_balance"
        Me.XrTableCell_total_balance.StylePriority.UseBorders = False
        Me.XrTableCell_total_balance.StylePriority.UseBorderWidth = False
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrTableCell_total_balance.Summary = XrSummary1
        Me.XrTableCell_total_balance.Weight = 0.60552760138583528R
        Me.XrTableCell_total_balance.WordWrap = False
        '
        'XrTableCell_total_disbursement_factor
        '
        Me.XrTableCell_total_disbursement_factor.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrTableCell_total_disbursement_factor.BorderWidth = 2
        Me.XrTableCell_total_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "dmp_payment")})
        Me.XrTableCell_total_disbursement_factor.Name = "XrTableCell_total_disbursement_factor"
        Me.XrTableCell_total_disbursement_factor.StylePriority.UseBorders = False
        Me.XrTableCell_total_disbursement_factor.StylePriority.UseBorderWidth = False
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrTableCell_total_disbursement_factor.Summary = XrSummary2
        Me.XrTableCell_total_disbursement_factor.Weight = 0.61175864061518537R
        Me.XrTableCell_total_disbursement_factor.WordWrap = False
        '
        'XrTableCell_total_non_dmp_payment
        '
        Me.XrTableCell_total_non_dmp_payment.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrTableCell_total_non_dmp_payment.BorderWidth = 2
        Me.XrTableCell_total_non_dmp_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "non_dmp_payment")})
        Me.XrTableCell_total_non_dmp_payment.Name = "XrTableCell_total_non_dmp_payment"
        Me.XrTableCell_total_non_dmp_payment.StylePriority.UseBorders = False
        Me.XrTableCell_total_non_dmp_payment.StylePriority.UseBorderWidth = False
        XrSummary3.FormatString = "{0:c}"
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrTableCell_total_non_dmp_payment.Summary = XrSummary3
        Me.XrTableCell_total_non_dmp_payment.Weight = 0.617487589007047R
        Me.XrTableCell_total_non_dmp_payment.WordWrap = False
        '
        'XrTableCell95
        '
        Me.XrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.XrTableCell95.Name = "XrTableCell95"
        Me.XrTableCell95.StylePriority.UseBorders = False
        Me.XrTableCell95.Weight = 0.8586934688702299R
        Me.XrTableCell95.WordWrap = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel_Client})
        Me.PageFooter.HeightF = 35.41667!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Visible = False
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.0002441406!, 21.79167!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 13.625!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Rev. 02/2011"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Client
        '
        Me.XrLabel_Client.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(590.0001!, 9.083298!)
        Me.XrLabel_Client.Name = "XrLabel_Client"
        Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(160.4166!, 26.33337!)
        Me.XrLabel_Client.StylePriority.UseFont = False
        Me.XrLabel_Client.StylePriority.UseTextAlignment = False
        Me.XrLabel_Client.Text = "Client: 0000000"
        Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8})
        Me.ReportHeader.HeightF = 36.41666!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrLabel08
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(1.008545!, 0.0!)
        Me.XrLabel8.Name = "XrLabel08"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(752.1166!, 36.41666!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "CREDITOR LIST"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'DMPDebts
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportFooter, Me.PageFooter, Me.ReportHeader})
        Me.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
        Me.ReportPrintOptions.PrintOnEmptyDataSource = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMPDebts_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportFooter, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMargin, 0)
        Me.Controls.SetChildIndex(Me.TopMargin, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me.XrTable9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrTable9 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow37 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell98 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell101 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell102 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell103 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow34 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell78 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell70 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell81 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell71 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell97 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell87 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell90 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell93 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTable10 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow35 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_name As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_account_number As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCel_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCellnon_dmp_rate As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_payout_date As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_creditor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow38 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell_creditor_address As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell107 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrTable11 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow36 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell80 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_balance As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_disbursement_factor As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_non_dmp_payment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell95 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Protected Friend WithEvents XrLabel_Client2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
End Class
