<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrivacyPolicy
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrivacyPolicy))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.CalculatedField1 = New DevExpress.XtraReports.UI.CalculatedField()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader_Page3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText_Page3 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader_Page1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText_Page1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader_Page2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText_Page2 = New DevExpress.XtraReports.UI.XRRichText()
        CType(Me.XrRichText_Page3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText_Page1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText_Page2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 25.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 25.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'CalculatedField1
        '
        Me.CalculatedField1.DisplayName = "format_day"
        Me.CalculatedField1.Expression = "'Expression String'"
        Me.CalculatedField1.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
        Me.CalculatedField1.Name = "CalculatedField1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
        Me.ReportFooter.HeightF = 13.625!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PrintAtBottom = True
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(652.0833!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 13.625!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "Rev. 02/2011"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader_Page3
        '
        Me.GroupHeader_Page3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Page3})
        Me.GroupHeader_Page3.HeightF = 884.3749!
        Me.GroupHeader_Page3.Name = "GroupHeader_Page3"
        Me.GroupHeader_Page3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText_Page3
        '
        Me.XrRichText_Page3.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
        Me.XrRichText_Page3.Name = "XrRichText_Page3"
        Me.XrRichText_Page3.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100.0!)
        Me.XrRichText_Page3.SerializableRtfString = resources.GetString("XrRichText_Page3.SerializableRtfString")
        Me.XrRichText_Page3.SizeF = New System.Drawing.SizeF(752.0833!, 884.3749!)
        Me.XrRichText_Page3.StylePriority.UsePadding = False
        '
        'GroupHeader_Page1
        '
        Me.GroupHeader_Page1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Page1})
        Me.GroupHeader_Page1.HeightF = 960.4167!
        Me.GroupHeader_Page1.Level = 2
        Me.GroupHeader_Page1.Name = "GroupHeader_Page1"
        '
        'XrRichText_Page1
        '
        Me.XrRichText_Page1.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
        Me.XrRichText_Page1.Name = "XrRichText_Page1"
        Me.XrRichText_Page1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 10, 10, 100.0!)
        Me.XrRichText_Page1.SerializableRtfString = resources.GetString("XrRichText_Page1.SerializableRtfString")
        Me.XrRichText_Page1.SizeF = New System.Drawing.SizeF(752.0833!, 960.4167!)
        Me.XrRichText_Page1.StylePriority.UsePadding = False
        '
        'GroupHeader_Page2
        '
        Me.GroupHeader_Page2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText_Page2})
        Me.GroupHeader_Page2.HeightF = 343.7917!
        Me.GroupHeader_Page2.Level = 1
        Me.GroupHeader_Page2.Name = "GroupHeader_Page2"
        Me.GroupHeader_Page2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText_Page2
        '
        Me.XrRichText_Page2.LocationFloat = New DevExpress.Utils.PointFloat(25.0!, 0.0!)
        Me.XrRichText_Page2.Name = "XrRichText_Page2"
        Me.XrRichText_Page2.SerializableRtfString = resources.GetString("XrRichText_Page2.SerializableRtfString")
        Me.XrRichText_Page2.SizeF = New System.Drawing.SizeF(752.0833!, 343.7917!)
        '
        'PrivacyPolicy
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportFooter, Me.GroupHeader_Page3, Me.GroupHeader_Page1, Me.GroupHeader_Page2})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField1})
        Me.Margins = New System.Drawing.Printing.Margins(24, 25, 25, 25)
        Me.ReportPrintOptions.DetailCount = 1
        Me.ReportPrintOptions.DetailCountAtDesignTime = 1
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "PrivacyPolicy_BeforePrint"
        Me.ShowPrintMarginsWarning = False
        Me.ShowPrintStatusDialog = False
        Me.Version = "11.2"
        CType(Me.XrRichText_Page3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText_Page1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText_Page2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Protected Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Protected Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Protected Friend WithEvents CalculatedField1 As DevExpress.XtraReports.UI.CalculatedField
    Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents GroupHeader_Page3 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents GroupHeader_Page1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader_Page2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrRichText_Page3 As DevExpress.XtraReports.UI.XRRichText
    Protected Friend WithEvents XrRichText_Page1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrRichText_Page2 As DevExpress.XtraReports.UI.XRRichText
End Class
