﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Invoice
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Invoice))
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrPanel_Detail = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel_LineTotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_UnitPrice = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Description = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrTable2 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_client_name = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_client_address = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_appointment_date = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_Date = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_Client = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Header_Invoice = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_AmountDue = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_AmountPaid = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Subtotal = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrTable3 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell6 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_Footer_Client = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_Footer_Name = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell10 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_Footer_Address = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupFooter2 = New DevExpress.XtraReports.UI.GroupFooterBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_Detail})
        Me.Detail.HeightF = 22.92!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPanel_Detail
        '
        Me.XrPanel_Detail.BorderColor = System.Drawing.Color.LightBlue
        Me.XrPanel_Detail.Borders = DevExpress.XtraPrinting.BorderSide.Left
        Me.XrPanel_Detail.BorderWidth = 2
        Me.XrPanel_Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_LineTotal, Me.XrLabel_UnitPrice, Me.XrLabel_Description})
        Me.XrPanel_Detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPanel_Detail.Name = "XrPanel_Detail"
        Me.XrPanel_Detail.SizeF = New System.Drawing.SizeF(799.0!, 22.92!)
        Me.XrPanel_Detail.StylePriority.UseBorderColor = False
        Me.XrPanel_Detail.StylePriority.UseBorders = False
        Me.XrPanel_Detail.StylePriority.UseBorderWidth = False
        '
        'XrLabel_LineTotal
        '
        Me.XrLabel_LineTotal.BackColor = System.Drawing.Color.LightBlue
        Me.XrLabel_LineTotal.BorderWidth = 0
        Me.XrLabel_LineTotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LineTotal", "{0:f2}")})
        Me.XrLabel_LineTotal.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
        Me.XrLabel_LineTotal.Name = "XrLabel_LineTotal"
        Me.XrLabel_LineTotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.XrLabel_LineTotal.SizeF = New System.Drawing.SizeF(98.99829!, 22.92!)
        Me.XrLabel_LineTotal.StylePriority.UseBackColor = False
        Me.XrLabel_LineTotal.StylePriority.UseBorderWidth = False
        Me.XrLabel_LineTotal.StylePriority.UsePadding = False
        Me.XrLabel_LineTotal.StylePriority.UseTextAlignment = False
        Me.XrLabel_LineTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_UnitPrice
        '
        Me.XrLabel_UnitPrice.BorderWidth = 0
        Me.XrLabel_UnitPrice.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "UnitPrice", "{0:f2}")})
        Me.XrLabel_UnitPrice.LocationFloat = New DevExpress.Utils.PointFloat(480.8749!, 0.0!)
        Me.XrLabel_UnitPrice.Name = "XrLabel_UnitPrice"
        Me.XrLabel_UnitPrice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_UnitPrice.SizeF = New System.Drawing.SizeF(103.0417!, 22.91667!)
        Me.XrLabel_UnitPrice.StylePriority.UseBorderWidth = False
        Me.XrLabel_UnitPrice.StylePriority.UseTextAlignment = False
        Me.XrLabel_UnitPrice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_Description
        '
        Me.XrLabel_Description.BorderWidth = 0
        Me.XrLabel_Description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Description")})
        Me.XrLabel_Description.LocationFloat = New DevExpress.Utils.PointFloat(9.99999!, 0.0!)
        Me.XrLabel_Description.Name = "XrLabel_Description"
        Me.XrLabel_Description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Description.SizeF = New System.Drawing.SizeF(470.875!, 22.91667!)
        Me.XrLabel_Description.StylePriority.UseBorderWidth = False
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 25.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.StylePriority.UseFont = False
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 25.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrTable2
        '
        Me.XrTable2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 184.375!)
        Me.XrTable2.Name = "XrTable2"
        Me.XrTable2.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow3, Me.XrTableRow4, Me.XrTableRow5})
        Me.XrTable2.SizeF = New System.Drawing.SizeF(769.7918!, 75.0!)
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3, Me.XrTableCell_client_name})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 1.0R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.Text = "RE:"
        Me.XrTableCell3.Weight = 0.13898078028655228R
        '
        'XrTableCell_client_name
        '
        Me.XrTableCell_client_name.Name = "XrTableCell_client_name"
        Me.XrTableCell_client_name.Weight = 2.8610192197134476R
        '
        'XrTableRow4
        '
        Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7, Me.XrTableCell_client_address})
        Me.XrTableRow4.Name = "XrTableRow4"
        Me.XrTableRow4.Weight = 1.0R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Weight = 0.13898081001950505R
        '
        'XrTableCell_client_address
        '
        Me.XrTableCell_client_address.Multiline = True
        Me.XrTableCell_client_address.Name = "XrTableCell_client_address"
        Me.XrTableCell_client_address.Weight = 2.8610191899804946R
        '
        'XrTableRow5
        '
        Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell_appointment_date})
        Me.XrTableRow5.Name = "XrTableRow5"
        Me.XrTableRow5.Weight = 1.0R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Text = "Date of Service:"
        Me.XrTableCell8.Weight = 0.46811589274697529R
        '
        'XrTableCell_appointment_date
        '
        Me.XrTableCell_appointment_date.Name = "XrTableCell_appointment_date"
        Me.XrTableCell_appointment_date.Weight = 2.5318841072530249R
        '
        'XrTable1
        '
        Me.XrTable1.BorderWidth = 0
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(618.7502!, 91.66666!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(151.0416!, 49.99999!)
        Me.XrTable1.StylePriority.UseBorderWidth = False
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2, Me.XrTableCell_Date})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "Date"
        Me.XrTableCell2.Weight = 0.96758628235083055R
        '
        'XrTableCell_Date
        '
        Me.XrTableCell_Date.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell_Date.Name = "XrTableCell_Date"
        Me.XrTableCell_Date.StylePriority.UseFont = False
        Me.XrTableCell_Date.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrTableCell_Date.Weight = 2.0324137176491694R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.XrTableCell_Client})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Text = "Inv #"
        Me.XrTableCell1.Weight = 0.96758628235083055R
        '
        'XrTableCell_Client
        '
        Me.XrTableCell_Client.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell_Client.Name = "XrTableCell_Client"
        Me.XrTableCell_Client.StylePriority.UseFont = False
        Me.XrTableCell_Client.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrTableCell_Client.Weight = 2.0324137176491694R
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 103.125!)
        Me.XrLabel1.Multiline = True
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(314.5833!, 68.83333!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "ClearPoint Credit Counseling Solutions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "270 Peachtree Street NW, Suite 1800" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Atla" & _
    "nta, GA 30303"
        '
        'XrLabel_Header_Invoice
        '
        Me.XrLabel_Header_Invoice.Font = New System.Drawing.Font("Palatino Linotype", 38.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Header_Invoice.ForeColor = System.Drawing.Color.SteelBlue
        Me.XrLabel_Header_Invoice.LocationFloat = New DevExpress.Utils.PointFloat(579.1667!, 10.00001!)
        Me.XrLabel_Header_Invoice.Name = "XrLabel_Header_Invoice"
        Me.XrLabel_Header_Invoice.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Header_Invoice.SizeF = New System.Drawing.SizeF(190.6251!, 48.95832!)
        Me.XrLabel_Header_Invoice.StylePriority.UseFont = False
        Me.XrLabel_Header_Invoice.StylePriority.UseForeColor = False
        Me.XrLabel_Header_Invoice.StylePriority.UseTextAlignment = False
        Me.XrLabel_Header_Invoice.Text = "Invoice"
        Me.XrLabel_Header_Invoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(186.4583!, 89.99999!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage
        '
        'GroupHeader1
        '
        Me.GroupHeader1.BackColor = System.Drawing.Color.MediumTurquoise
        Me.GroupHeader1.BorderColor = System.Drawing.Color.Transparent
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
        Me.GroupHeader1.ForeColor = System.Drawing.Color.White
        Me.GroupHeader1.HeightF = 22.92!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.StylePriority.UseBackColor = False
        Me.GroupHeader1.StylePriority.UseBorderColor = False
        Me.GroupHeader1.StylePriority.UseForeColor = False
        '
        'XrPanel1
        '
        Me.XrPanel1.BackColor = System.Drawing.Color.SteelBlue
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel5, Me.XrLabel4})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(799.0!, 22.92!)
        Me.XrPanel1.StylePriority.UseBackColor = False
        '
        'XrLabel7
        '
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 0.0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(99.0!, 22.92!)
        Me.XrLabel7.StylePriority.UsePadding = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Line Total"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel5
        '
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(480.8749!, 0.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(103.0417!, 22.91667!)
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "Unit Price"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel4
        '
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(9.99999!, 0.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(470.875!, 22.91667!)
        Me.XrLabel4.Text = "Description"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel15, Me.XrLabel14, Me.XrLabel_AmountDue, Me.XrLabel12, Me.XrLabel_AmountPaid, Me.XrLabel10, Me.XrLabel9, Me.XrLabel_Subtotal})
        Me.GroupFooter1.HeightF = 78.99998!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'XrLabel15
        '
        Me.XrLabel15.BackColor = System.Drawing.Color.SkyBlue
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(685.5832!, 55.99998!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(14.41678!, 23.0!)
        Me.XrLabel15.StylePriority.UseBackColor = False
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "$"
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel14
        '
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(685.5832!, 10.00001!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(14.41678!, 23.0!)
        Me.XrLabel14.StylePriority.UseTextAlignment = False
        Me.XrLabel14.Text = "$"
        Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_AmountDue
        '
        Me.XrLabel_AmountDue.BackColor = System.Drawing.Color.SkyBlue
        Me.XrLabel_AmountDue.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "AmountDue")})
        Me.XrLabel_AmountDue.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 55.99997!)
        Me.XrLabel_AmountDue.Name = "XrLabel_AmountDue"
        Me.XrLabel_AmountDue.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.XrLabel_AmountDue.SizeF = New System.Drawing.SizeF(98.99829!, 23.0!)
        Me.XrLabel_AmountDue.StylePriority.UseBackColor = False
        Me.XrLabel_AmountDue.StylePriority.UsePadding = False
        Me.XrLabel_AmountDue.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:f2}"
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_AmountDue.Summary = XrSummary1
        Me.XrLabel_AmountDue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel12
        '
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(469.7918!, 55.99997!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(206.0834!, 23.0!)
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "Amount Due"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_AmountPaid
        '
        Me.XrLabel_AmountPaid.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Payment")})
        Me.XrLabel_AmountPaid.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 32.99999!)
        Me.XrLabel_AmountPaid.Name = "XrLabel_AmountPaid"
        Me.XrLabel_AmountPaid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.XrLabel_AmountPaid.SizeF = New System.Drawing.SizeF(98.99829!, 23.0!)
        Me.XrLabel_AmountPaid.StylePriority.UseBackColor = False
        Me.XrLabel_AmountPaid.StylePriority.UsePadding = False
        Me.XrLabel_AmountPaid.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:f2}"
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_AmountPaid.Summary = XrSummary2
        Me.XrLabel_AmountPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel10
        '
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(469.7918!, 32.99999!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(206.0834!, 23.0!)
        Me.XrLabel10.StylePriority.UseTextAlignment = False
        Me.XrLabel10.Text = "Amount Paid"
        Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel9
        '
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(469.7918!, 10.0!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(206.0834!, 23.0!)
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Subtotal"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_Subtotal
        '
        Me.XrLabel_Subtotal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "LineTotal")})
        Me.XrLabel_Subtotal.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 10.0!)
        Me.XrLabel_Subtotal.Name = "XrLabel_Subtotal"
        Me.XrLabel_Subtotal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 10, 0, 0, 100.0!)
        Me.XrLabel_Subtotal.SizeF = New System.Drawing.SizeF(98.99829!, 23.0!)
        Me.XrLabel_Subtotal.StylePriority.UsePadding = False
        Me.XrLabel_Subtotal.StylePriority.UseTextAlignment = False
        XrSummary3.FormatString = "{0:f2}"
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group
        Me.XrLabel_Subtotal.Summary = XrSummary3
        Me.XrLabel_Subtotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel8
        '
        Me.XrLabel8.BorderColor = System.Drawing.Color.LightSeaGreen
        Me.XrLabel8.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
        Me.XrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrLabel8.BorderWidth = 3
        Me.XrLabel8.Font = New System.Drawing.Font("Palatino Linotype", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.ForeColor = System.Drawing.Color.DarkSlateGray
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(798.9983!, 23.00001!)
        Me.XrLabel8.StylePriority.UseBorderColor = False
        Me.XrLabel8.StylePriority.UseBorderDashStyle = False
        Me.XrLabel8.StylePriority.UseBorders = False
        Me.XrLabel8.StylePriority.UseBorderWidth = False
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseForeColor = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "Please return this coupon with the payment if you are going to send the payment b" & _
    "y mail"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrTable3
        '
        Me.XrTable3.BorderWidth = 0
        Me.XrTable3.LocationFloat = New DevExpress.Utils.PointFloat(497.9167!, 49.58331!)
        Me.XrTable3.Name = "XrTable3"
        Me.XrTable3.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow7, Me.XrTableRow6, Me.XrTableRow8})
        Me.XrTable3.SizeF = New System.Drawing.SizeF(292.0833!, 69.0!)
        Me.XrTable3.StylePriority.UseBorderWidth = False
        '
        'XrTableRow7
        '
        Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell6, Me.XrTableCell_Footer_Client})
        Me.XrTableRow7.Name = "XrTableRow7"
        Me.XrTableRow7.Weight = 0.53670425627445928R
        '
        'XrTableCell6
        '
        Me.XrTableCell6.Name = "XrTableCell6"
        Me.XrTableCell6.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
        Me.XrTableCell6.StylePriority.UsePadding = False
        Me.XrTableCell6.StylePriority.UseTextAlignment = False
        Me.XrTableCell6.Text = "Inv #"
        Me.XrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrTableCell6.Weight = 0.67022957515180115R
        '
        'XrTableCell_Footer_Client
        '
        Me.XrTableCell_Footer_Client.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell_Footer_Client.Name = "XrTableCell_Footer_Client"
        Me.XrTableCell_Footer_Client.StylePriority.UseFont = False
        Me.XrTableCell_Footer_Client.StylePriority.UseTextAlignment = False
        Me.XrTableCell_Footer_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        Me.XrTableCell_Footer_Client.Weight = 2.4178762189397807R
        '
        'XrTableRow6
        '
        Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.XrTableCell_Footer_Name})
        Me.XrTableRow6.Name = "XrTableRow6"
        Me.XrTableRow6.Weight = 0.53670425627445928R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
        Me.XrTableCell4.StylePriority.UsePadding = False
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        Me.XrTableCell4.Text = "Name"
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        Me.XrTableCell4.Weight = 0.67022957515180115R
        '
        'XrTableCell_Footer_Name
        '
        Me.XrTableCell_Footer_Name.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell_Footer_Name.Name = "XrTableCell_Footer_Name"
        Me.XrTableCell_Footer_Name.StylePriority.UseFont = False
        Me.XrTableCell_Footer_Name.Weight = 2.4178762189397807R
        '
        'XrTableRow8
        '
        Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell10, Me.XrTableCell_Footer_Address})
        Me.XrTableRow8.Name = "XrTableRow8"
        Me.XrTableRow8.Weight = 0.53670425627445928R
        '
        'XrTableCell10
        '
        Me.XrTableCell10.Name = "XrTableCell10"
        Me.XrTableCell10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100.0!)
        Me.XrTableCell10.StylePriority.UsePadding = False
        Me.XrTableCell10.Weight = 0.67022957515180115R
        '
        'XrTableCell_Footer_Address
        '
        Me.XrTableCell_Footer_Address.Multiline = True
        Me.XrTableCell_Footer_Address.Name = "XrTableCell_Footer_Address"
        Me.XrTableCell_Footer_Address.Weight = 2.4178762189397807R
        '
        'XrLabel18
        '
        Me.XrLabel18.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel18.ForeColor = System.Drawing.Color.SteelBlue
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 147.625!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        Me.XrLabel18.StylePriority.UseFont = False
        Me.XrLabel18.StylePriority.UseForeColor = False
        Me.XrLabel18.StylePriority.UseTextAlignment = False
        Me.XrLabel18.Text = "Thank you for your business!"
        Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel17
        '
        Me.XrLabel17.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(139.5833!, 49.58331!)
        Me.XrLabel17.Multiline = True
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(341.2917!, 98.04169!)
        Me.XrLabel17.Text = "ClearPoint Credit Counseling Solutions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Attn: Payment Processing-FHLB" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "270 Peacht" & _
    "ree Street, NW, Suite 1800" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Atlanta, GA 30303"
        '
        'XrLabel16
        '
        Me.XrLabel16.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 49.58331!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(139.5833!, 23.0!)
        Me.XrLabel16.Text = "Mail Payment To:"
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable2, Me.XrTable1, Me.XrLabel1, Me.XrLabel_Header_Invoice, Me.XrPictureBox1})
        Me.GroupHeader2.HeightF = 271.875!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrTable3, Me.XrLabel18, Me.XrLabel17, Me.XrLabel16})
        Me.GroupFooter2.HeightF = 170.625!
        Me.GroupFooter2.Level = 1
        Me.GroupFooter2.Name = "GroupFooter2"
        Me.GroupFooter2.PrintAtBottom = True
        '
        'XrLabel2
        '
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        '
        'Invoice
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.GroupHeader1, Me.GroupFooter1, Me.GroupHeader2, Me.GroupFooter2})
        Me.Font = New System.Drawing.Font("Palatino Linotype", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Scripts.OnBeforePrint = "Invoice_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrTable3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Public Detail As DevExpress.XtraReports.UI.DetailBand
    Public TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Public BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Public ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Public XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_Header_Invoice As DevExpress.XtraReports.UI.XRLabel
    Public XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Public XrPanel_Detail As DevExpress.XtraReports.UI.XRPanel
    Public XrLabel_LineTotal As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_UnitPrice As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_Description As DevExpress.XtraReports.UI.XRLabel
    Public XrTable2 As DevExpress.XtraReports.UI.XRTable
    Public XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Public XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableCell_client_name As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Public XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableCell_client_address As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Public XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableCell_appointment_date As DevExpress.XtraReports.UI.XRTableCell
    Public XrTable1 As DevExpress.XtraReports.UI.XRTable
    Public XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Public XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableCell_Date As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Public XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Public XrTableCell_Client As DevExpress.XtraReports.UI.XRTableCell
    Public GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Public XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Public XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Public GroupFooter1 As DevExpress.XtraReports.UI.GroupFooterBand
    Public XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_AmountDue As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_AmountPaid As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel_Subtotal As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Public XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Public WithEvents XrTable3 As DevExpress.XtraReports.UI.XRTable
    Public WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Public WithEvents XrTableCell6 As DevExpress.XtraReports.UI.XRTableCell
    Public WithEvents XrTableCell_Footer_Client As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Footer_Name As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell10 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_Footer_Address As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupFooter2 As DevExpress.XtraReports.UI.GroupFooterBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
End Class
