<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OtherDebts
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(OtherDebts))
        Me.XrControlStyle_Header = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrControlStyle_Totals = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrLabel_creditor_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel_total_payment = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_total_balance = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_payment = New DevExpress.XtraReports.UI.XRLabel()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrControlStyle_GroupHeader = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrControlStyle_HeaderPannel = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.XrLabel_interest_rate = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrLabel_Client = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrControlStyle_Header
        '
        Me.XrControlStyle_Header.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal
        Me.XrControlStyle_Header.Name = "XrControlStyle_Header"
        Me.XrControlStyle_Header.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrControlStyle_Totals
        '
        Me.XrControlStyle_Totals.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_Totals.Name = "XrControlStyle_Totals"
        Me.XrControlStyle_Totals.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1, Me.XrLabel1})
        Me.PageHeader.HeightF = 80.83334!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrPanel1
        '
        Me.XrPanel1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom
        Me.XrPanel1.BackColor = System.Drawing.Color.Teal
        Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
        Me.XrPanel1.CanGrow = False
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 55.83337!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100.0!)
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(789.9999!, 18.0!)
        Me.XrPanel1.StylePriority.UseBackColor = False
        Me.XrPanel1.StylePriority.UseBorderColor = False
        Me.XrPanel1.StylePriority.UsePadding = False
        '
        'XrLabel6
        '
        Me.XrLabel6.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel6.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel6.CanGrow = False
        Me.XrLabel6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel6.ForeColor = System.Drawing.Color.White
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(536.0!, 1.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(63.0!, 16.0!)
        Me.XrLabel6.StylePriority.UseBackColor = False
        Me.XrLabel6.StylePriority.UseBorderColor = False
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.StylePriority.UseForeColor = False
        Me.XrLabel6.StylePriority.UseTextAlignment = False
        Me.XrLabel6.Text = "APR"
        Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel5
        '
        Me.XrLabel5.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel5.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel5.CanGrow = False
        Me.XrLabel5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel5.ForeColor = System.Drawing.Color.White
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(441.0!, 1.0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel5.StylePriority.UseBackColor = False
        Me.XrLabel5.StylePriority.UseBorderColor = False
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.StylePriority.UseForeColor = False
        Me.XrLabel5.StylePriority.UseTextAlignment = False
        Me.XrLabel5.Text = "PAYMENT"
        Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel4
        '
        Me.XrLabel4.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel4.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel4.CanGrow = False
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel4.ForeColor = System.Drawing.Color.White
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 1.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel4.StylePriority.UseBackColor = False
        Me.XrLabel4.StylePriority.UseBorderColor = False
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseForeColor = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "BALANCE"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel3
        '
        Me.XrLabel3.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel3.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel3.CanGrow = False
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel3.ForeColor = System.Drawing.Color.White
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 1.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
        Me.XrLabel3.StylePriority.UseBackColor = False
        Me.XrLabel3.StylePriority.UseBorderColor = False
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseForeColor = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "ACCOUNT"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel2.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel2.ForeColor = System.Drawing.Color.White
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(183.0!, 16.0!)
        Me.XrLabel2.StylePriority.UseBackColor = False
        Me.XrLabel2.StylePriority.UseBorderColor = False
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseForeColor = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "CREDITOR"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Top
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(790.0!, 35.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "NON-DMP DEBTS"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel11
        '
        Me.XrLabel11.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel11.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel11.CanGrow = False
        Me.XrLabel11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel11.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(183.0!, 16.0!)
        Me.XrLabel11.StylePriority.UseBackColor = False
        Me.XrLabel11.StylePriority.UseBorderColor = False
        Me.XrLabel11.StylePriority.UseFont = False
        Me.XrLabel11.StylePriority.UseForeColor = False
        Me.XrLabel11.StylePriority.UseTextAlignment = False
        Me.XrLabel11.Text = "TOTALS"
        Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 0.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_creditor_name
        '
        Me.XrLabel_creditor_name.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_creditor_name.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_creditor_name.CanGrow = False
        Me.XrLabel_creditor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "creditor_name")})
        Me.XrLabel_creditor_name.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_creditor_name.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_creditor_name.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
        Me.XrLabel_creditor_name.Name = "XrLabel_creditor_name"
        Me.XrLabel_creditor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_creditor_name.SizeF = New System.Drawing.SizeF(183.0!, 16.0!)
        Me.XrLabel_creditor_name.StylePriority.UseBackColor = False
        Me.XrLabel_creditor_name.StylePriority.UseBorderColor = False
        Me.XrLabel_creditor_name.StylePriority.UseFont = False
        Me.XrLabel_creditor_name.StylePriority.UseForeColor = False
        Me.XrLabel_creditor_name.StylePriority.UseTextAlignment = False
        Me.XrLabel_creditor_name.Text = "CREDITOR"
        Me.XrLabel_creditor_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_total_payment, Me.XrLabel_total_balance, Me.XrLabel11})
        Me.ReportFooter.HeightF = 43.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLine1
        '
        Me.XrLine1.BorderWidth = 1
        Me.XrLine1.ForeColor = System.Drawing.Color.Teal
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(508.0!, 9.0!)
        Me.XrLine1.StylePriority.UseBorderWidth = False
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrLabel_total_payment
        '
        Me.XrLabel_total_payment.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_total_payment.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_total_payment.CanGrow = False
        Me.XrLabel_total_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment", "{0:c}")})
        Me.XrLabel_total_payment.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_payment.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_total_payment.LocationFloat = New DevExpress.Utils.PointFloat(441.0!, 17.0!)
        Me.XrLabel_total_payment.Name = "XrLabel_total_payment"
        Me.XrLabel_total_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_payment.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel_total_payment.StylePriority.UseBackColor = False
        Me.XrLabel_total_payment.StylePriority.UseBorderColor = False
        Me.XrLabel_total_payment.StylePriority.UseFont = False
        Me.XrLabel_total_payment.StylePriority.UseForeColor = False
        Me.XrLabel_total_payment.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.IgnoreNullValues = True
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrLabel_total_payment.Summary = XrSummary1
        Me.XrLabel_total_payment.Text = "$0.00"
        Me.XrLabel_total_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_total_balance
        '
        Me.XrLabel_total_balance.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_total_balance.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_total_balance.CanGrow = False
        Me.XrLabel_total_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrLabel_total_balance.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_balance.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_total_balance.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 17.0!)
        Me.XrLabel_total_balance.Name = "XrLabel_total_balance"
        Me.XrLabel_total_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_balance.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel_total_balance.StylePriority.UseBackColor = False
        Me.XrLabel_total_balance.StylePriority.UseBorderColor = False
        Me.XrLabel_total_balance.StylePriority.UseFont = False
        Me.XrLabel_total_balance.StylePriority.UseForeColor = False
        Me.XrLabel_total_balance.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.IgnoreNullValues = True
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrLabel_total_balance.Summary = XrSummary2
        Me.XrLabel_total_balance.Text = "$0.00"
        Me.XrLabel_total_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_payment
        '
        Me.XrLabel_payment.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_payment.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_payment.CanGrow = False
        Me.XrLabel_payment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payment", "{0:c}")})
        Me.XrLabel_payment.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_payment.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_payment.LocationFloat = New DevExpress.Utils.PointFloat(441.0!, 0.0!)
        Me.XrLabel_payment.Name = "XrLabel_payment"
        Me.XrLabel_payment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_payment.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel_payment.StylePriority.UseBackColor = False
        Me.XrLabel_payment.StylePriority.UseBorderColor = False
        Me.XrLabel_payment.StylePriority.UseFont = False
        Me.XrLabel_payment.StylePriority.UseForeColor = False
        Me.XrLabel_payment.StylePriority.UseTextAlignment = False
        Me.XrLabel_payment.Text = "$0.00"
        Me.XrLabel_payment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 0.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrControlStyle_GroupHeader
        '
        Me.XrControlStyle_GroupHeader.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon
        Me.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader"
        Me.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrControlStyle_HeaderPannel
        '
        Me.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal
        Me.XrControlStyle_HeaderPannel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White
        Me.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel"
        Me.XrControlStyle_HeaderPannel.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_interest_rate
        '
        Me.XrLabel_interest_rate.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_interest_rate.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_interest_rate.CanGrow = False
        Me.XrLabel_interest_rate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "interest_rate", "{0:p}")})
        Me.XrLabel_interest_rate.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_interest_rate.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_interest_rate.LocationFloat = New DevExpress.Utils.PointFloat(536.0!, 0.0!)
        Me.XrLabel_interest_rate.Name = "XrLabel_interest_rate"
        Me.XrLabel_interest_rate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_interest_rate.SizeF = New System.Drawing.SizeF(63.0!, 16.0!)
        Me.XrLabel_interest_rate.StylePriority.UseBackColor = False
        Me.XrLabel_interest_rate.StylePriority.UseBorderColor = False
        Me.XrLabel_interest_rate.StylePriority.UseFont = False
        Me.XrLabel_interest_rate.StylePriority.UseForeColor = False
        Me.XrLabel_interest_rate.StylePriority.UseTextAlignment = False
        Me.XrLabel_interest_rate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_account_number
        '
        Me.XrLabel_account_number.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_account_number.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_account_number.CanGrow = False
        Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrLabel_account_number.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_account_number.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(200.0!, 0.0!)
        Me.XrLabel_account_number.Name = "XrLabel_account_number"
        Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(150.0!, 16.0!)
        Me.XrLabel_account_number.StylePriority.UseBackColor = False
        Me.XrLabel_account_number.StylePriority.UseBorderColor = False
        Me.XrLabel_account_number.StylePriority.UseFont = False
        Me.XrLabel_account_number.StylePriority.UseForeColor = False
        Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
        Me.XrLabel_account_number.Text = "XXXXX"
        Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_payment, Me.XrLabel_balance, Me.XrLabel_creditor_name, Me.XrLabel_interest_rate, Me.XrLabel_account_number})
        Me.Detail.HeightF = 16.0!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_balance
        '
        Me.XrLabel_balance.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_balance.BorderColor = System.Drawing.Color.Transparent
        Me.XrLabel_balance.CanGrow = False
        Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrLabel_balance.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_balance.ForeColor = System.Drawing.SystemColors.WindowText
        Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(358.0!, 0.0!)
        Me.XrLabel_balance.Name = "XrLabel_balance"
        Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(75.0!, 16.0!)
        Me.XrLabel_balance.StylePriority.UseBackColor = False
        Me.XrLabel_balance.StylePriority.UseBorderColor = False
        Me.XrLabel_balance.StylePriority.UseFont = False
        Me.XrLabel_balance.StylePriority.UseForeColor = False
        Me.XrLabel_balance.StylePriority.UseTextAlignment = False
        Me.XrLabel_balance.Text = "$0.00"
        Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Client, Me.XrLabel7})
        Me.PageFooter.HeightF = 26.33337!
        Me.PageFooter.Name = "PageFooter"
        '
        'XrLabel_Client
        '
        Me.XrLabel_Client.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Client.LocationFloat = New DevExpress.Utils.PointFloat(536.0!, 0.0!)
        Me.XrLabel_Client.Name = "XrLabel_Client"
        Me.XrLabel_Client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_Client.SizeF = New System.Drawing.SizeF(160.4166!, 26.33337!)
        Me.XrLabel_Client.StylePriority.UseFont = False
        Me.XrLabel_Client.StylePriority.UseTextAlignment = False
        Me.XrLabel_Client.Text = "Client: 0000000"
        Me.XrLabel_Client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 12.5!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(100.0!, 13.625!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.StylePriority.UseTextAlignment = False
        Me.XrLabel7.Text = "Rev. 02/2011"
        Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine2})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrLine2
        '
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(790.0!, 23.0!)
        '
        'OtherDebts
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportFooter, Me.PageFooter, Me.ReportHeader})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 0, 0)
        Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
        Me.ReportPrintOptions.PrintOnEmptyDataSource = False
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "OtherDebts_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
        Me.Version = "11.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrControlStyle_Header As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle_Totals As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents XrLabel_creditor_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel_total_payment As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_total_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_payment As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrControlStyle_GroupHeader As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrControlStyle_HeaderPannel As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrLabel_interest_rate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrLabel_Client As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Protected Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
End Class
