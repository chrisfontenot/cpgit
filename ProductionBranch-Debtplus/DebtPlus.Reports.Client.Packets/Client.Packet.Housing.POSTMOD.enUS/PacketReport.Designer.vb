<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader_action_plan = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader14 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader16 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader17 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader18 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.CoverPage()
        Me.XrSubreport_ClientRightsPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ClientRightsPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ClientRightsPolicy()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.BottomLine()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.NetWorth()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.Budget()
        Me.XrSubreport_ActionPlan_Items = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ActionPlanItems()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ActionPlanGoals()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.PrivacyPolicy2 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.PrivacyPolicy()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AgreementForServices1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.AgreementForServices()
        Me.XrSubreport_WelcomePage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.WelcomePage1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.WelcomePage()
        Me.XrSubreport_GeneralService = New DevExpress.XtraReports.UI.XRSubreport()
        Me.GeneralServices2 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.GeneralServices()
        Me.XrSubreport_ForeclosurePrevention = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ForeclosurePreventionDisclosure1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ForeclosurePreventionDisclosure()
        Me.XrSubreport_YourFinancialFuture = New DevExpress.XtraReports.UI.XRSubreport()
        Me.YourFinancialFuture1 = New DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.YourFinancialFuture()
        Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WelcomePage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneralServices2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ForeclosurePreventionDisclosure1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.YourFinancialFuture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 1.041667!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak1, Me.XrSubreport_Cover, Me.XrSubreport_WelcomePage})
        Me.ReportHeader.HeightF = 48.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader_action_plan
        '
        Me.GroupHeader_action_plan.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientRightsPolicy})
        Me.GroupHeader_action_plan.HeightF = 23.0!
        Me.GroupHeader_action_plan.Level = 1
        Me.GroupHeader_action_plan.Name = "GroupHeader_action_plan"
        Me.GroupHeader_action_plan.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader14
        '
        Me.GroupHeader14.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader14.HeightF = 23.79166!
        Me.GroupHeader14.Level = 8
        Me.GroupHeader14.Name = "GroupHeader14"
        Me.GroupHeader14.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader15.HeightF = 23.0!
        Me.GroupHeader15.Level = 6
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader1.HeightF = 23.0!
        Me.GroupHeader1.Level = 7
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Items})
        Me.GroupHeader5.HeightF = 23.0!
        Me.GroupHeader5.Level = 9
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader7.HeightF = 23.0!
        Me.GroupHeader7.Level = 10
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader13.HeightF = 23.0!
        Me.GroupHeader13.Level = 2
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader6.HeightF = 23.0!
        Me.GroupHeader6.Level = 4
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader16
        '
        Me.GroupHeader16.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralService})
        Me.GroupHeader16.HeightF = 23.0!
        Me.GroupHeader16.Name = "GroupHeader16"
        Me.GroupHeader16.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader17
        '
        Me.GroupHeader17.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ForeclosurePrevention})
        Me.GroupHeader17.HeightF = 26.04167!
        Me.GroupHeader17.Level = 3
        Me.GroupHeader17.Name = "GroupHeader17"
        Me.GroupHeader17.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader18
        '
        Me.GroupHeader18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_YourFinancialFuture})
        Me.GroupHeader18.HeightF = 27.08333!
        Me.GroupHeader18.Level = 5
        Me.GroupHeader18.Name = "GroupHeader18"
        Me.GroupHeader18.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_ClientRightsPolicy
        '
        Me.XrSubreport_ClientRightsPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ClientRightsPolicy.Name = "XrSubreport_ClientRightsPolicy"
        Me.XrSubreport_ClientRightsPolicy.ReportSource = Me.ClientRightsPolicy1
        Me.XrSubreport_ClientRightsPolicy.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.7916451!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(800.0!, 22.99994!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_ActionPlan_Items
        '
        Me.XrSubreport_ActionPlan_Items.CanShrink = True
        Me.XrSubreport_ActionPlan_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_ActionPlan_Items.Name = "XrSubreport_ActionPlan_Items"
        Me.XrSubreport_ActionPlan_Items.ReportSource = Me.ActionPlanItems1
        Me.XrSubreport_ActionPlan_Items.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.PrivacyPolicy2
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_WelcomePage
        '
        Me.XrSubreport_WelcomePage.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 25.0!)
        Me.XrSubreport_WelcomePage.Name = "XrSubreport_WelcomePage"
        Me.XrSubreport_WelcomePage.ReportSource = Me.WelcomePage1
        Me.XrSubreport_WelcomePage.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_GeneralService
        '
        Me.XrSubreport_GeneralService.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralService.Name = "XrSubreport_GeneralService"
        Me.XrSubreport_GeneralService.ReportSource = Me.GeneralServices2
        Me.XrSubreport_GeneralService.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_ForeclosurePrevention
        '
        Me.XrSubreport_ForeclosurePrevention.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ForeclosurePrevention.Name = "XrSubreport_ForeclosurePrevention"
        Me.XrSubreport_ForeclosurePrevention.ReportSource = Me.ForeclosurePreventionDisclosure1
        Me.XrSubreport_ForeclosurePrevention.SizeF = New System.Drawing.SizeF(795.8749!, 23.0!)
        '
        'XrSubreport_YourFinancialFuture
        '
        Me.XrSubreport_YourFinancialFuture.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_YourFinancialFuture.Name = "XrSubreport_YourFinancialFuture"
        Me.XrSubreport_YourFinancialFuture.ReportSource = Me.YourFinancialFuture1
        Me.XrSubreport_YourFinancialFuture.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrPageBreak1
        '
        Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
        Me.XrPageBreak1.Name = "XrPageBreak1"
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader_action_plan, Me.GroupHeader14, Me.GroupHeader15, Me.GroupHeader1, Me.GroupHeader5, Me.GroupHeader7, Me.GroupHeader13, Me.GroupHeader6, Me.GroupHeader16, Me.GroupHeader17, Me.GroupHeader18})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader18, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader17, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader16, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader14, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_action_plan, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WelcomePage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneralServices2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ForeclosurePreventionDisclosure1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.YourFinancialFuture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ActionPlanGoals
    Private ActionPlanItems1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ActionPlanItems
    Private AgreementForServices1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.AgreementForServices
    Private BottomLine1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.BottomLine
    Private Budget1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.Budget
    Private ClientRightsPolicy1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ClientRightsPolicy
    Private CoverPage1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.CoverPage
    Private ForeclosurePreventionDisclosure1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.ForeclosurePreventionDisclosure
    Private GeneralServices2 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.GeneralServices
    Private GroupHeader_action_plan As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader14 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader16 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader17 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader18 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private NetWorth1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.NetWorth
    Private PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Private PrivacyPolicy2 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.PrivacyPolicy
    Private ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private WelcomePage1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.WelcomePage
    Private XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Private XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Private XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Private XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_ActionPlan_Items As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_ClientRightsPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_ForeclosurePrevention As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_GeneralService As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_WelcomePage As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_YourFinancialFuture As DevExpress.XtraReports.UI.XRSubreport
    Private YourFinancialFuture1 As DebtPlus.Reports.Client.Packet.Housing.POSTMOD.enUS.YourFinancialFuture
    Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
End Class
