<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class WelcomePage
    Inherits Letter_Template

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(WelcomePage))
        Me.XrLabel_NameAndAddress = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        CType(Me.XrRichText_Address_Template2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrLabel_1_Template
        '
        Me.XrLabel_1_Template.StylePriority.UseFont = False
        Me.XrLabel_1_Template.StylePriority.UseTextAlignment = False
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_NameAndAddress, Me.XrPageInfo1, Me.XrRichText1})
        Me.Detail.HeightF = 682.9153!
        '
        'XrLabel_NameAndAddress
        '
        Me.XrLabel_NameAndAddress.BackColor = System.Drawing.Color.Transparent
        Me.XrLabel_NameAndAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_NameAndAddress.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 91.25!)
        Me.XrLabel_NameAndAddress.Multiline = True
        Me.XrLabel_NameAndAddress.Name = "XrLabel_NameAndAddress"
        Me.XrLabel_NameAndAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_NameAndAddress.Scripts.OnBeforePrint = "XrLabel_NameAndAddress_BeforePrint"
        Me.XrLabel_NameAndAddress.SizeF = New System.Drawing.SizeF(403.125!, 99.99999!)
        Me.XrLabel_NameAndAddress.StylePriority.UseBackColor = False
        Me.XrLabel_NameAndAddress.StylePriority.UseFont = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo1.Format = "{0:MMMM d, yyyy}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(300.0!, 23.0!)
        Me.XrPageInfo1.StylePriority.UseFont = False
        Me.XrPageInfo1.StylePriority.UsePadding = False
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 200.5!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(748.0!, 482.4153!)
        Me.XrRichText1.StylePriority.UsePadding = False
        '
        'WelcomePage
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportFooter_2_Template})
        Me.ReportPrintOptions.DetailCount = 1
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "Cover_Letter_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrRichText_Address_Template2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrLabel_NameAndAddress As DevExpress.XtraReports.UI.XRLabel
End Class
