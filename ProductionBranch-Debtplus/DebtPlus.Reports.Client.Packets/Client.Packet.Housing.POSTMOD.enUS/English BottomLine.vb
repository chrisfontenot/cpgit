#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class BottomLine

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        'AddHandler BeforePrint, AddressOf BottomLine_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Public Class ReportData
        Public Sub New()
        End Sub

        Public Sub New(ByVal row As System.Data.DataRow)
            MyClass.New()

            ' Save the values for later use
            _payroll_1 = DebtPlus.Utils.Nulls.DDec(row("payroll_1"))
            _payroll_2 = DebtPlus.Utils.Nulls.DDec(row("payroll_2"))
            _assets = DebtPlus.Utils.Nulls.DDec(row("assets"))
            _living_expenses = DebtPlus.Utils.Nulls.DDec(row("living_expenses"))
            _housing_expenses = DebtPlus.Utils.Nulls.DDec(row("housing_expenses"))
            _other_expenses = DebtPlus.Utils.Nulls.DDec(row("other_expenses"))
            _other_debt = DebtPlus.Utils.Nulls.DDec(row("other_debt"))
            _program_fees = DebtPlus.Utils.Nulls.DDec(row("program_fees"))
        End Sub

        Private _payroll_1 As Decimal = 0D
        Public ReadOnly Property payroll_1 As Decimal
            Get
                Return _payroll_1
            End Get
        End Property

        Private _payroll_2 As Decimal = 0D
        Public ReadOnly Property payroll_2 As Decimal
            Get
                Return _payroll_2
            End Get
        End Property

        Private _assets As Decimal = 0D
        Public ReadOnly Property assets As Decimal
            Get
                Return _assets
            End Get
        End Property

        Public ReadOnly Property total_assets As Decimal
            Get
                Return assets + payroll_1 + payroll_2
            End Get
        End Property

        Private _living_expenses As Decimal = 0D
        Public ReadOnly Property living_expenses As Decimal
            Get
                Return _living_expenses
            End Get
        End Property

        Private _housing_expenses As Decimal = 0D
        Public ReadOnly Property housing_expenses As Decimal
            Get
                Return _housing_expenses
            End Get
        End Property

        Private _other_expenses As Decimal = 0D
        Public ReadOnly Property other_expenses As Decimal
            Get
                Return _other_expenses
            End Get
        End Property

        Public ReadOnly Property total_budget_expenses As Decimal
            Get
                Return living_expenses + housing_expenses + other_expenses
            End Get
        End Property

        Private _other_debt As Decimal = 0D
        Public ReadOnly Property other_debt As Decimal
            Get
                Return _other_debt
            End Get
        End Property

        Public ReadOnly Property available As Decimal
            Get
                Return total_assets - total_budget_expenses - other_debt
            End Get
        End Property

        Private _program_fees As Decimal = 0D
        Public ReadOnly Property program_fees As Decimal
            Get
                Return 0D  ' We suppress this item for this report copy
                ' Return _program_fees
            End Get
        End Property

        Public ReadOnly Property grand_total As Decimal
            Get
                Return available - program_fees
            End Get
        End Property

    End Class

    Private Sub BottomLine_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_BottomLine"
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                cn.Open()

                Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)
                Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "rpt_PrintClient_BottomLine"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .CommandTimeout = 0
                        .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                    End With

                    Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                        tbl = ds.Tables(TableName)
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If

        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            Dim lst As New System.Collections.Generic.List(Of ReportData)
            lst.Add(New ReportData(tbl.Rows(0)))
            Rpt.DataSource = lst
        End If
    End Sub
End Class
