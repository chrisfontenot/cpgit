#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class ActionPlanAddendum
    Public Sub New()
        MyBase.New()
        InitializeComponent()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        'AddHandler BeforePrint, AddressOf ActionPlanAddendum_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Public Class RecordClass
        Public Property applicant_fico_score As Int32 = 0
        Public Property coapplicant_fico_score As Int32 = 0
    End Class

    Private Sub ActionPlanAddendum_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_ActionPlan_Addendum"
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt) ' CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)

        Try
            Using ds As New System.Data.DataSet()
                Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT relation, fico_score FROM people p WHERE client = @client ORDER BY relation"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using
                End Using

                Dim tbl As System.Data.DataTable = ds.Tables(TableName)
                Dim rec As New RecordClass()
                For Each row As DataRow In tbl.Rows
                    If DebtPlus.Utils.Nulls.DInt(row(0)) = 1 Then
                        rec.applicant_fico_score = DebtPlus.Utils.Nulls.DInt(row(1))
                    Else
                        rec.coapplicant_fico_score = DebtPlus.Utils.Nulls.DInt(row(1))
                    End If
                Next

                Dim lst As New System.Collections.Generic.List(Of RecordClass)
                lst.Add(rec)
                Rpt.DataSource = lst
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")
        End Try
    End Sub
End Class
