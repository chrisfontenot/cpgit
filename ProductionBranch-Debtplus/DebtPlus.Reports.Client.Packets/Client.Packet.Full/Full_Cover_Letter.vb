#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class Full_Cover_Letter
    Inherits Letter_Template

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        AddHandler BeforePrint, AddressOf Full_Cover_Letter_BeforePrint
    End Sub

    Private Sub Full_Cover_Letter_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "view_client_address"
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim masterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim clientId As System.Int32 = Convert.ToInt32(masterRpt.Parameters("ParameterClient").Value, System.Globalization.CultureInfo.InvariantCulture)
        Dim ds As System.Data.DataSet = CType(masterRpt.DataSource, System.Data.DataView).Table.DataSet

        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()
            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT * FROM view_client_address WHERE [client]=@Client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = clientId

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)

                    rpt.DataSource = ds.Tables(TableName).DefaultView
                    For Each calcField As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calcField.Assign(rpt.DataSource, rpt.DataMember)
                    Next
                End Using
            End Using
        End Using
    End Sub

End Class
