﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BottomLine
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BottomLine))
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow6 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell8 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell15 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell16 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_payroll_1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpApplicantMonthlyIncome = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell4 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_payroll_2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.preDmpCoAppMonthlyIncome = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow5 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell18 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_assets = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpOtherMonthlyIncome = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow4 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell14 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_assets = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpTotalMontlyIncome = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell7 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow7 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell26 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_total_budget_expenses = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpTotalBudgetExpense = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow8 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell30 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_other_debt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpOtherNonManagedDebt = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow9 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell34 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_available = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpSubtotalSurplus = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow11 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell42 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_program_fees = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpPlanPayment = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow10 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell38 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableCell_grand_total = New DevExpress.XtraReports.UI.XRTableCell()
        Me.lblPreDmpTotalSurplus = New DevExpress.XtraReports.UI.XRTableCell()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.StylePriority.UsePadding = False
        '
        'XrLabel_PageHeader
        '
        Me.XrLabel_PageHeader.Multiline = True
        Me.XrLabel_PageHeader.StylePriority.UseFont = False
        Me.XrLabel_PageHeader.StylePriority.UseTextAlignment = False
        Me.XrLabel_PageHeader.Text = "MONTHLY" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "SURPLUS/DEFICIT"
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1})
        Me.Detail.HeightF = 253.1057!
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrTable1
        '
        Me.XrTable1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(50.0!, 0.0!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow6, Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow5, Me.XrTableRow4, Me.XrTableRow3, Me.XrTableRow7, Me.XrTableRow8, Me.XrTableRow9, Me.XrTableRow11, Me.XrTableRow10})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(690.2917!, 235.8485!)
        Me.XrTable1.StylePriority.UseFont = False
        '
        'XrTableRow6
        '
        Me.XrTableRow6.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell8, Me.XrTableCell16, Me.XrTableCell15})
        Me.XrTableRow6.Name = "XrTableRow6"
        Me.XrTableRow6.Weight = 0.063829787234042548R
        '
        'XrTableCell8
        '
        Me.XrTableCell8.Name = "XrTableCell8"
        Me.XrTableCell8.Weight = 0.43343566644728426R
        '
        'XrTableCell15
        '
        Me.XrTableCell15.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell15.Name = "XrTableCell15"
        Me.XrTableCell15.StylePriority.UseFont = False
        Me.XrTableCell15.StylePriority.UseTextAlignment = False
        Me.XrTableCell15.Text = "SUGGESTED"
        Me.XrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell15.Weight = 0.19446339939008142R
        '
        'XrTableCell16
        '
        Me.XrTableCell16.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTableCell16.Name = "XrTableCell16"
        Me.XrTableCell16.StylePriority.UseFont = False
        Me.XrTableCell16.StylePriority.UseTextAlignment = False
        Me.XrTableCell16.Text = "SELF"
        Me.XrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell16.Weight = 0.19446339939008142R
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1, Me.lblPreDmpApplicantMonthlyIncome, Me.XrTableCell_payroll_1})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 0.063829787234042548R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.CanGrow = False
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 5, 5, 100.0!)
        Me.XrTableCell1.StylePriority.UsePadding = False
        Me.XrTableCell1.StylePriority.UseTextAlignment = False
        Me.XrTableCell1.Text = "Applicant Employment Monthly Income" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell1.Weight = 0.43343566644728426R
        Me.XrTableCell1.WordWrap = False
        '
        'XrTableCell_payroll_1
        '
        Me.XrTableCell_payroll_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_1", "{0:c}")})
        Me.XrTableCell_payroll_1.Name = "XrTableCell_payroll_1"
        Me.XrTableCell_payroll_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 5, 5, 100.0!)
        Me.XrTableCell_payroll_1.StylePriority.UsePadding = False
        Me.XrTableCell_payroll_1.StylePriority.UseTextAlignment = False
        Me.XrTableCell_payroll_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_payroll_1.Weight = 0.19446339939008142R
        '
        'lblPreDmpApplicantMonthlyIncome
        '
        Me.lblPreDmpApplicantMonthlyIncome.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_1", "{0:c}")})
        Me.lblPreDmpApplicantMonthlyIncome.Name = "lblPreDmpApplicantMonthlyIncome"
        Me.lblPreDmpApplicantMonthlyIncome.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 5, 5, 100.0!)
        Me.lblPreDmpApplicantMonthlyIncome.StylePriority.UsePadding = False
        Me.lblPreDmpApplicantMonthlyIncome.StylePriority.UseTextAlignment = False
        Me.lblPreDmpApplicantMonthlyIncome.Text = "lblPreDmpApplicantMonthlyIncome"
        Me.lblPreDmpApplicantMonthlyIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpApplicantMonthlyIncome.Weight = 0.19446339939008142R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell4, Me.preDmpCoAppMonthlyIncome, Me.XrTableCell_payroll_2})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 0.063829787234042548R
        '
        'XrTableCell4
        '
        Me.XrTableCell4.CanGrow = False
        Me.XrTableCell4.Name = "XrTableCell4"
        Me.XrTableCell4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell4.StylePriority.UseTextAlignment = False
        Me.XrTableCell4.Text = "Co-Applicant Employment Monthly Income" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell4.Weight = 0.43343566644728426R
        Me.XrTableCell4.WordWrap = False
        '
        'XrTableCell_payroll_2
        '
        Me.XrTableCell_payroll_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_2", "{0:c}")})
        Me.XrTableCell_payroll_2.Name = "XrTableCell_payroll_2"
        Me.XrTableCell_payroll_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_payroll_2.StylePriority.UseTextAlignment = False
        Me.XrTableCell_payroll_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_payroll_2.Weight = 0.19446339939008142R
        '
        'preDmpCoAppMonthlyIncome
        '
        Me.preDmpCoAppMonthlyIncome.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "payroll_2", "{0:c}")})
        Me.preDmpCoAppMonthlyIncome.Name = "preDmpCoAppMonthlyIncome"
        Me.preDmpCoAppMonthlyIncome.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.preDmpCoAppMonthlyIncome.StylePriority.UsePadding = False
        Me.preDmpCoAppMonthlyIncome.StylePriority.UseTextAlignment = False
        Me.preDmpCoAppMonthlyIncome.Text = "preDmpCoAppMonthlyIncome"
        Me.preDmpCoAppMonthlyIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.preDmpCoAppMonthlyIncome.Weight = 0.19446339939008142R
        '
        'XrTableRow5
        '
        Me.XrTableRow5.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell18, Me.lblPreDmpOtherMonthlyIncome, Me.XrTableCell_assets})
        Me.XrTableRow5.Name = "XrTableRow5"
        Me.XrTableRow5.Weight = 0.063829787234042548R
        '
        'XrTableCell18
        '
        Me.XrTableCell18.CanGrow = False
        Me.XrTableCell18.Name = "XrTableCell18"
        Me.XrTableCell18.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell18.StylePriority.UseTextAlignment = False
        Me.XrTableCell18.Text = "Other Monthly Income" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell18.Weight = 0.43343566644728426R
        Me.XrTableCell18.WordWrap = False
        '
        'XrTableCell_assets
        '
        Me.XrTableCell_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "assets", "{0:c}")})
        Me.XrTableCell_assets.Name = "XrTableCell_assets"
        Me.XrTableCell_assets.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_assets.StylePriority.UseTextAlignment = False
        Me.XrTableCell_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_assets.Weight = 0.19446339939008142R
        '
        'lblPreDmpOtherMonthlyIncome
        '
        Me.lblPreDmpOtherMonthlyIncome.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "assets", "{0:c}")})
        Me.lblPreDmpOtherMonthlyIncome.Name = "lblPreDmpOtherMonthlyIncome"
        Me.lblPreDmpOtherMonthlyIncome.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpOtherMonthlyIncome.StylePriority.UsePadding = False
        Me.lblPreDmpOtherMonthlyIncome.StylePriority.UseTextAlignment = False
        Me.lblPreDmpOtherMonthlyIncome.Text = "lblPreDmpOtherMonthlyIncome"
        Me.lblPreDmpOtherMonthlyIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpOtherMonthlyIncome.Weight = 0.19446339939008142R
        '
        'XrTableRow4
        '
        Me.XrTableRow4.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell14, Me.lblPreDmpTotalMontlyIncome, Me.XrTableCell_total_assets})
        Me.XrTableRow4.Name = "XrTableRow4"
        Me.XrTableRow4.Weight = 0.063829787234042548R
        '
        'XrTableCell14
        '
        Me.XrTableCell14.CanGrow = False
        Me.XrTableCell14.Name = "XrTableCell14"
        Me.XrTableCell14.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell14.StylePriority.UseTextAlignment = False
        Me.XrTableCell14.Text = "Total Monthly Income" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell14.Weight = 0.43343566644728426R
        Me.XrTableCell14.WordWrap = False
        '
        'XrTableCell_total_assets
        '
        Me.XrTableCell_total_assets.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_assets", "{0:c}")})
        Me.XrTableCell_total_assets.Name = "XrTableCell_total_assets"
        Me.XrTableCell_total_assets.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_total_assets.StylePriority.UseTextAlignment = False
        Me.XrTableCell_total_assets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_total_assets.Weight = 0.19446339939008142R
        '
        'lblPreDmpTotalMontlyIncome
        '
        Me.lblPreDmpTotalMontlyIncome.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_assets", "{0:c}")})
        Me.lblPreDmpTotalMontlyIncome.Name = "lblPreDmpTotalMontlyIncome"
        Me.lblPreDmpTotalMontlyIncome.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpTotalMontlyIncome.StylePriority.UsePadding = False
        Me.lblPreDmpTotalMontlyIncome.StylePriority.UseTextAlignment = False
        Me.lblPreDmpTotalMontlyIncome.Text = "lblPreDmpTotalMontlyIncome"
        Me.lblPreDmpTotalMontlyIncome.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpTotalMontlyIncome.Weight = 0.19446339939008142R
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell7})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 0.063829787234042548R
        '
        'XrTableCell7
        '
        Me.XrTableCell7.CanGrow = False
        Me.XrTableCell7.Name = "XrTableCell7"
        Me.XrTableCell7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell7.StylePriority.UseTextAlignment = False
        Me.XrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell7.Weight = 0.822362465227447R
        Me.XrTableCell7.WordWrap = False
        '
        'XrTableRow7
        '
        Me.XrTableRow7.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell26, Me.lblPreDmpTotalBudgetExpense, Me.XrTableCell_total_budget_expenses})
        Me.XrTableRow7.Name = "XrTableRow7"
        Me.XrTableRow7.Weight = 0.063829787234042548R
        '
        'XrTableCell26
        '
        Me.XrTableCell26.CanGrow = False
        Me.XrTableCell26.Name = "XrTableCell26"
        Me.XrTableCell26.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell26.StylePriority.UseTextAlignment = False
        Me.XrTableCell26.Text = "Total Budgeted Expenses" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell26.Weight = 0.43343566644728426R
        Me.XrTableCell26.WordWrap = False
        '
        'XrTableCell_total_budget_expenses
        '
        Me.XrTableCell_total_budget_expenses.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "total_budget_expenses", "{0:c}")})
        Me.XrTableCell_total_budget_expenses.Name = "XrTableCell_total_budget_expenses"
        Me.XrTableCell_total_budget_expenses.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_total_budget_expenses.StylePriority.UseTextAlignment = False
        Me.XrTableCell_total_budget_expenses.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_total_budget_expenses.Weight = 0.19446339939008142R
        '
        'lblPreDmpTotalBudgetExpense
        '
        Me.lblPreDmpTotalBudgetExpense.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pre_plan_expense", "{0:c}")})
        Me.lblPreDmpTotalBudgetExpense.Name = "lblPreDmpTotalBudgetExpense"
        Me.lblPreDmpTotalBudgetExpense.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpTotalBudgetExpense.StylePriority.UsePadding = False
        Me.lblPreDmpTotalBudgetExpense.StylePriority.UseTextAlignment = False
        Me.lblPreDmpTotalBudgetExpense.Text = "lblPreDmpTotalBudgetExpense"
        Me.lblPreDmpTotalBudgetExpense.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpTotalBudgetExpense.Weight = 0.19446339939008142R
        '
        'XrTableRow8
        '
        Me.XrTableRow8.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell30, Me.lblPreDmpOtherNonManagedDebt, Me.XrTableCell_other_debt})
        Me.XrTableRow8.Name = "XrTableRow8"
        Me.XrTableRow8.Weight = 0.10638297872340426R
        '
        'XrTableCell30
        '
        Me.XrTableCell30.CanGrow = False
        Me.XrTableCell30.Name = "XrTableCell30"
        Me.XrTableCell30.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell30.StylePriority.UseTextAlignment = False
        Me.XrTableCell30.Text = "Other Debt" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell30.Weight = 0.43343566644728426R
        Me.XrTableCell30.WordWrap = False
        '
        'XrTableCell_other_debt
        '
        Me.XrTableCell_other_debt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "other_debt", "{0:c}")})
        Me.XrTableCell_other_debt.Name = "XrTableCell_other_debt"
        Me.XrTableCell_other_debt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_other_debt.StylePriority.UseTextAlignment = False
        Me.XrTableCell_other_debt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_other_debt.Weight = 0.19446339939008142R
        '
        'lblPreDmpOtherNonManagedDebt
        '
        Me.lblPreDmpOtherNonManagedDebt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "other_debt", "{0:c}")})
        Me.lblPreDmpOtherNonManagedDebt.Name = "lblPreDmpOtherNonManagedDebt"
        Me.lblPreDmpOtherNonManagedDebt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpOtherNonManagedDebt.StylePriority.UsePadding = False
        Me.lblPreDmpOtherNonManagedDebt.StylePriority.UseTextAlignment = False
        Me.lblPreDmpOtherNonManagedDebt.Text = "lblPreDmpOtherNonManagedDebt"
        Me.lblPreDmpOtherNonManagedDebt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpOtherNonManagedDebt.Weight = 0.19446339939008142R
        '
        'XrTableRow9
        '
        Me.XrTableRow9.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell34, Me.lblPreDmpSubtotalSurplus, Me.XrTableCell_available})
        Me.XrTableRow9.Name = "XrTableRow9"
        Me.XrTableRow9.Weight = 0.10638297872340426R
        '
        'XrTableCell34
        '
        Me.XrTableCell34.CanGrow = False
        Me.XrTableCell34.Name = "XrTableCell34"
        Me.XrTableCell34.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell34.StylePriority.UseTextAlignment = False
        Me.XrTableCell34.Text = "Subtotal Surplus/Deficit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell34.Weight = 0.43343566644728426R
        Me.XrTableCell34.WordWrap = False
        '
        'XrTableCell_available
        '
        Me.XrTableCell_available.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "available", "{0:c}")})
        Me.XrTableCell_available.Name = "XrTableCell_available"
        Me.XrTableCell_available.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_available.StylePriority.UseTextAlignment = False
        Me.XrTableCell_available.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_available.Weight = 0.19446339939008142R
        '
        'lblPreDmpSubtotalSurplus
        '
        Me.lblPreDmpSubtotalSurplus.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pre_dmp_subtotal", "{0:c}")})
        Me.lblPreDmpSubtotalSurplus.Name = "lblPreDmpSubtotalSurplus"
        Me.lblPreDmpSubtotalSurplus.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpSubtotalSurplus.StylePriority.UsePadding = False
        Me.lblPreDmpSubtotalSurplus.StylePriority.UseTextAlignment = False
        Me.lblPreDmpSubtotalSurplus.Text = "lblPreDmpSubtotalSurplus"
        Me.lblPreDmpSubtotalSurplus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpSubtotalSurplus.Weight = 0.19446339939008142R
        '
        'XrTableRow11
        '
        Me.XrTableRow11.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell42, Me.lblPreDmpPlanPayment, Me.XrTableCell_program_fees})
        Me.XrTableRow11.Name = "XrTableRow11"
        Me.XrTableRow11.Weight = 0.10638297872340426R
        '
        'XrTableCell42
        '
        Me.XrTableCell42.CanGrow = False
        Me.XrTableCell42.Name = "XrTableCell42"
        Me.XrTableCell42.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell42.StylePriority.UseTextAlignment = False
        Me.XrTableCell42.Text = "Debt Payments" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell42.Weight = 0.43343566644728426R
        Me.XrTableCell42.WordWrap = False
        '
        'XrTableCell_program_fees
        '
        Me.XrTableCell_program_fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "program_fees", "{0:c}")})
        Me.XrTableCell_program_fees.Name = "XrTableCell_program_fees"
        Me.XrTableCell_program_fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_program_fees.StylePriority.UseTextAlignment = False
        Me.XrTableCell_program_fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_program_fees.Weight = 0.19446339939008142R
        '
        'lblPreDmpPlanPayment
        '
        Me.lblPreDmpPlanPayment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pre_plan_payment", "{0:c}")})
        Me.lblPreDmpPlanPayment.Name = "lblPreDmpPlanPayment"
        Me.lblPreDmpPlanPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpPlanPayment.StylePriority.UsePadding = False
        Me.lblPreDmpPlanPayment.StylePriority.UseTextAlignment = False
        Me.lblPreDmpPlanPayment.Text = "lblPreDmpDebtPayment"
        Me.lblPreDmpPlanPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpPlanPayment.Weight = 0.19446339939008142R
        '
        'XrTableRow10
        '
        Me.XrTableRow10.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell38, Me.lblPreDmpTotalSurplus, Me.XrTableCell_grand_total})
        Me.XrTableRow10.Name = "XrTableRow10"
        Me.XrTableRow10.Weight = 0.10638297872340426R
        '
        'XrTableCell38
        '
        Me.XrTableCell38.CanGrow = False
        Me.XrTableCell38.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell38.Name = "XrTableCell38"
        Me.XrTableCell38.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell38.StylePriority.UseFont = False
        Me.XrTableCell38.StylePriority.UseTextAlignment = False
        Me.XrTableCell38.Text = "TOTAL SURPLUS/DEFICIT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.XrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        Me.XrTableCell38.Weight = 0.43343566644728426R
        Me.XrTableCell38.WordWrap = False
        '
        'XrTableCell_grand_total
        '
        Me.XrTableCell_grand_total.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "grand_total", "{0:c}")})
        Me.XrTableCell_grand_total.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrTableCell_grand_total.Name = "XrTableCell_grand_total"
        Me.XrTableCell_grand_total.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrTableCell_grand_total.StylePriority.UseFont = False
        Me.XrTableCell_grand_total.StylePriority.UseTextAlignment = False
        Me.XrTableCell_grand_total.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.XrTableCell_grand_total.Weight = 0.19446339939008142R
        '
        'lblPreDmpTotalSurplus
        '
        Me.lblPreDmpTotalSurplus.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "pre_dmp_total_surplus", "{0:c}")})
        Me.lblPreDmpTotalSurplus.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPreDmpTotalSurplus.Name = "lblPreDmpTotalSurplus"
        Me.lblPreDmpTotalSurplus.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.lblPreDmpTotalSurplus.StylePriority.UseFont = False
        Me.lblPreDmpTotalSurplus.StylePriority.UsePadding = False
        Me.lblPreDmpTotalSurplus.StylePriority.UseTextAlignment = False
        Me.lblPreDmpTotalSurplus.Text = "lblPreDmpTotalSurplus"
        Me.lblPreDmpTotalSurplus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        Me.lblPreDmpTotalSurplus.Weight = 0.19446339939008142R
        '
        'BottomLine
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "BottomLine_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_payroll_1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell4 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_payroll_2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow5 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell18 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_assets As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow4 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell14 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_assets As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell7 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow7 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell26 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_total_budget_expenses As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow8 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell30 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_other_debt As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow9 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell34 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_available As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow11 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell42 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_program_fees As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow10 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell38 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell_grand_total As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow6 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell8 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell15 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableCell16 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpApplicantMonthlyIncome As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents preDmpCoAppMonthlyIncome As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpOtherMonthlyIncome As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpTotalMontlyIncome As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpTotalBudgetExpense As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpOtherNonManagedDebt As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpSubtotalSurplus As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpPlanPayment As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents lblPreDmpTotalSurplus As DevExpress.XtraReports.UI.XRTableCell
End Class
