#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass
    Implements DebtPlus.Interfaces.Client.IClient

    ''' <summary>
    '''     Create an instance of our report
    ''' </summary>
    Public Sub New()
        MyBase.New()
        InitializeComponent()

        ' Ensure that the parameters are defaulted to their "missing" status
        Parameter_Client = -1

        ' Ensure that the DevExpress parameter dialog is not used on the report.
        RequestParameters = False
        For Each parm As DevExpress.XtraReports.Parameters.Parameter In Parameters
            parm.Visible = False
        Next

        ' Set the script references to the current (running) values.
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies

        ' Copy the script references to the sub-reports as well
        For Each Band As DevExpress.XtraReports.UI.Band In Bands
            For Each ctl As DevExpress.XtraReports.UI.XRControl In Band.Controls
                Dim subReport As DevExpress.XtraReports.UI.XRSubreport = TryCast(ctl, DevExpress.XtraReports.UI.XRSubreport)
                If subReport IsNot Nothing Then
                    Dim rpt As DevExpress.XtraReports.UI.XtraReport = subReport.ReportSource
                    If rpt IsNot Nothing Then
                        rpt.ScriptReferences = ScriptReferences
                    End If
                End If
            Next
        Next

        AddHandler BeforePrint, AddressOf Report_BeforePrint
        AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
        AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
        AddHandler XrLabel_OrgName.BeforePrint, AddressOf XrLabel_OrgName_BeforePrint
    End Sub

    ''' <summary>
    ''' Client ID. This is the primary key to the clients table.
    ''' </summary>
    Public Property ClientID As Int32 Implements DebtPlus.Interfaces.Client.IClient.ClientId
        Get
            Return Parameter_Client
        End Get
        Set(value As Int32)
            Parameter_Client = value
        End Set
    End Property

    ''' <summary>
    ''' Client ID. This is the primary key to the clients table.
    ''' </summary>
    Public Property Parameter_Client() As System.Int32
        Get
            Dim parm As DevExpress.XtraReports.Parameters.Parameter = FindParameter("ParameterClient")
            If parm Is Nothing Then Return -1
            Return Convert.ToInt32(parm.Value)
        End Get
        Set(ByVal value As System.Int32)
            SetParameter("ParameterClient", GetType(Int32), value, "Client ID", False)
        End Set
    End Property

    ' ********************************************* COPIED TO SCRIPTS ***********************************************************
    Dim ds As New System.Data.DataSet("ds")

    Private Sub Report_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim ClientId As Int32 = Convert.ToInt32(rpt.Parameters("ParameterClient").Value)

        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "rpt_PrintClient"
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = ClientId

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "rpt_PrintClient")
                End Using
            End Using
        End Using

        rpt.DataSource = ds.Tables("rpt_PrintClient").DefaultView
    End Sub

    Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
        Dim Client As Object = rpt.Parameters("ParameterClient").Value
        lbl.Text = DebtPlus.Utils.Format.Client.FormatClientID(Client)
    End Sub

    Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)

        Dim vue As System.Data.DataView = CType(rpt.DataSource, System.Data.DataView)
        If vue.Count > 0 Then
            Dim row As System.Data.DataRow = vue(0).Row
            If row("client_name") IsNot Nothing AndAlso row("client_name") IsNot System.DBNull.Value Then
                lbl.Text = Convert.ToString(row("client_name")).Trim
            End If
        End If
    End Sub

    Private Sub XrLabel_OrgName_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)

        Dim vue As System.Data.DataView = CType(rpt.DataSource, System.Data.DataView)
        If vue.Count > 0 Then
            Dim row As System.Data.DataRow = vue(0).Row
            If row("org_name") IsNot Nothing AndAlso row("org_name") IsNot System.DBNull.Value Then
                lbl.Text = Convert.ToString(row("org_name")).Trim
            End If
        End If
    End Sub
End Class
