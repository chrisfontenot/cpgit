<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PacketReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.XrPageBreak13 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader8 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader9 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader10 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_DMPDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.DmpDebts1 = New DebtPlus.Reports.Client.Packet.Full.DMPDebts()
        Me.XrSubreport_CoverSheet = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.Full.CoverPage()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.Full.NetWorth()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Full.BottomLine()
        Me.XrSubreport_OtherDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.OtherDebts1 = New DebtPlus.Reports.Client.Packet.Full.OtherDebts()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Full.ActionPlanGoals()
        Me.XrSubreport_ActionPlan_Items = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems1 = New DebtPlus.Reports.Client.Packet.Full.ActionPlanItems()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Full.Budget()
        Me.XrSubreport_GrievanceProcedures = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ClientGrievanceProcedures1 = New DebtPlus.Reports.Client.Packet.Full.ClientGrievanceProcedures()
        Me.XrSubreport_ACH_Signup = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AcH_Erollment1 = New DebtPlus.Reports.Client.Packet.Full.ACH_Erollment()
        Me.XrSubreport_GettingStartedWithACH = New DevExpress.XtraReports.UI.XRSubreport()
        Me.GettingStartedWithACH1 = New DebtPlus.Reports.Client.Packet.Full.GettingStartedWithACH()
        Me.XrSubreport_MonthlyStatements = New DevExpress.XtraReports.UI.XRSubreport()
        Me.MonthlyStatements1 = New DebtPlus.Reports.Client.Packet.Full.MonthlyStatements()
        Me.XrSubreport_AppropriateChoices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AppropriateChoices1 = New DebtPlus.Reports.Client.Packet.Full.AppropriateChoices()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.PrivacyPolicy1 = New DebtPlus.Reports.Client.Packet.Full.PrivacyPolicy()
        Me.XrSubreport_Enrollment_Checklist = New DevExpress.XtraReports.UI.XRSubreport()
        Me.DmP_Checklist1 = New DebtPlus.Reports.Client.Packet.Full.DMP_Checklist()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AgreementForServices1 = New DebtPlus.Reports.Client.Packet.Full.AgreementForServices()
        Me.XrSubreport_CoverLetter = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Full_Cover_Letter1 = New DebtPlus.Reports.Client.Packet.Full.Full_Cover_Letter()
        Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientGrievanceProcedures1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AcH_Erollment1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GettingStartedWithACH1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MonthlyStatements1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AppropriateChoices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DmP_Checklist1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DMPDebts})
        Me.Detail.HeightF = 23.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPageBreak13
        '
        Me.XrPageBreak13.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 352.9996!)
        Me.XrPageBreak13.Name = "XrPageBreak13"
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak1, Me.XrSubreport_CoverSheet, Me.XrSubreport_CoverLetter})
        Me.ReportHeader.HeightF = 48.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_OrgName, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLine1})
        Me.PageFooter.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 40.58337!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        Me.PageFooter.Visible = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 4.999987!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(118.75!, 22.79167!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 5.0!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.7917!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLine1
        '
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(796.8748!, 5.0!)
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader5.HeightF = 23.0!
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine, Me.XrSubreport_OtherDebts})
        Me.GroupHeader6.HeightF = 58.83333!
        Me.GroupHeader6.Level = 1
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals, Me.XrSubreport_ActionPlan_Items})
        Me.GroupHeader7.HeightF = 63.00003!
        Me.GroupHeader7.Level = 2
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader8
        '
        Me.GroupHeader8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader8.HeightF = 23.0!
        Me.GroupHeader8.Level = 3
        Me.GroupHeader8.Name = "GroupHeader8"
        Me.GroupHeader8.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader9
        '
        Me.GroupHeader9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GrievanceProcedures})
        Me.GroupHeader9.HeightF = 23.0!
        Me.GroupHeader9.Level = 4
        Me.GroupHeader9.Name = "GroupHeader9"
        Me.GroupHeader9.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader10
        '
        Me.GroupHeader10.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ACH_Signup})
        Me.GroupHeader10.HeightF = 23.0!
        Me.GroupHeader10.Level = 5
        Me.GroupHeader10.Name = "GroupHeader10"
        Me.GroupHeader10.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GettingStartedWithACH})
        Me.GroupHeader11.HeightF = 23.0!
        Me.GroupHeader11.Level = 6
        Me.GroupHeader11.Name = "GroupHeader11"
        Me.GroupHeader11.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_MonthlyStatements})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Level = 7
        Me.GroupHeader12.Name = "GroupHeader12"
        Me.GroupHeader12.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AppropriateChoices})
        Me.GroupHeader13.HeightF = 23.0!
        Me.GroupHeader13.Level = 8
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader15.HeightF = 23.0!
        Me.GroupHeader15.Level = 11
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Enrollment_Checklist})
        Me.GroupHeader1.HeightF = 23.0!
        Me.GroupHeader1.Level = 9
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader2.HeightF = 29.25002!
        Me.GroupHeader2.Level = 10
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_DMPDebts
        '
        Me.XrSubreport_DMPDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_DMPDebts.Name = "XrSubreport_DMPDebts"
        Me.XrSubreport_DMPDebts.ReportSource = Me.DmpDebts1
        Me.XrSubreport_DMPDebts.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_CoverSheet
        '
        Me.XrSubreport_CoverSheet.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_CoverSheet.Name = "XrSubreport_CoverSheet"
        Me.XrSubreport_CoverSheet.ReportSource = Me.CoverPage1
        Me.XrSubreport_CoverSheet.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.00006357829!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(798.0!, 22.99994!)
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(798.0!, 22.99997!)
        '
        'XrSubreport_OtherDebts
        '
        Me.XrSubreport_OtherDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 35.83333!)
        Me.XrSubreport_OtherDebts.Name = "XrSubreport_OtherDebts"
        Me.XrSubreport_OtherDebts.ReportSource = Me.OtherDebts1
        Me.XrSubreport_OtherDebts.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_ActionPlan_Items
        '
        Me.XrSubreport_ActionPlan_Items.CanShrink = True
        Me.XrSubreport_ActionPlan_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 40.00003!)
        Me.XrSubreport_ActionPlan_Items.Name = "XrSubreport_ActionPlan_Items"
        Me.XrSubreport_ActionPlan_Items.ReportSource = Me.ActionPlanItems1
        Me.XrSubreport_ActionPlan_Items.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_GrievanceProcedures
        '
        Me.XrSubreport_GrievanceProcedures.CanShrink = True
        Me.XrSubreport_GrievanceProcedures.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GrievanceProcedures.Name = "XrSubreport_GrievanceProcedures"
        Me.XrSubreport_GrievanceProcedures.ReportSource = Me.ClientGrievanceProcedures1
        Me.XrSubreport_GrievanceProcedures.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_ACH_Signup
        '
        Me.XrSubreport_ACH_Signup.CanShrink = True
        Me.XrSubreport_ACH_Signup.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ACH_Signup.Name = "XrSubreport_ACH_Signup"
        Me.XrSubreport_ACH_Signup.ReportSource = Me.AcH_Erollment1
        Me.XrSubreport_ACH_Signup.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_GettingStartedWithACH
        '
        Me.XrSubreport_GettingStartedWithACH.CanShrink = True
        Me.XrSubreport_GettingStartedWithACH.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GettingStartedWithACH.Name = "XrSubreport_GettingStartedWithACH"
        Me.XrSubreport_GettingStartedWithACH.ReportSource = Me.GettingStartedWithACH1
        Me.XrSubreport_GettingStartedWithACH.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_MonthlyStatements
        '
        Me.XrSubreport_MonthlyStatements.CanShrink = True
        Me.XrSubreport_MonthlyStatements.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_MonthlyStatements.Name = "XrSubreport_MonthlyStatements"
        Me.XrSubreport_MonthlyStatements.ReportSource = Me.MonthlyStatements1
        Me.XrSubreport_MonthlyStatements.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_AppropriateChoices
        '
        Me.XrSubreport_AppropriateChoices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AppropriateChoices.Name = "XrSubreport_AppropriateChoices"
        Me.XrSubreport_AppropriateChoices.ReportSource = Me.AppropriateChoices1
        Me.XrSubreport_AppropriateChoices.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(2.0!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.PrivacyPolicy1
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_Enrollment_Checklist
        '
        Me.XrSubreport_Enrollment_Checklist.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Enrollment_Checklist.Name = "XrSubreport_Enrollment_Checklist"
        Me.XrSubreport_Enrollment_Checklist.ReportSource = Me.DmP_Checklist1
        Me.XrSubreport_Enrollment_Checklist.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(798.0!, 29.25002!)
        '
        'XrSubreport_CoverLetter
        '
        Me.XrSubreport_CoverLetter.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
        Me.XrSubreport_CoverLetter.Name = "XrSubreport_CoverLetter"
        Me.XrSubreport_CoverLetter.ReportSource = Me.Full_Cover_Letter1
        Me.XrSubreport_CoverLetter.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrPageBreak1
        '
        Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
        Me.XrPageBreak1.Name = "XrPageBreak1"
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader5, Me.GroupHeader6, Me.GroupHeader7, Me.GroupHeader8, Me.GroupHeader9, Me.GroupHeader10, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader13, Me.GroupHeader15, Me.GroupHeader1, Me.GroupHeader2})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader10, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader9, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader8, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientGrievanceProcedures1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AcH_Erollment1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GettingStartedWithACH1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MonthlyStatements1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AppropriateChoices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DmP_Checklist1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Protected Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Protected Friend WithEvents XrSubreport_CoverLetter As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_OtherDebts As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_DMPDebts As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrSubreport_Enrollment_Checklist As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_AppropriateChoices As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_MonthlyStatements As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_GettingStartedWithACH As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_GrievanceProcedures As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_ACH_Signup As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_ActionPlan_Items As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrPageBreak13 As DevExpress.XtraReports.UI.XRPageBreak
    Private WithEvents DmP_Checklist1 As DebtPlus.Reports.Client.Packet.Full.DMP_Checklist
    Private WithEvents PrivacyPolicy1 As DebtPlus.Reports.Client.Packet.Full.PrivacyPolicy
    Private WithEvents AppropriateChoices1 As DebtPlus.Reports.Client.Packet.Full.AppropriateChoices
    Private WithEvents MonthlyStatements1 As DebtPlus.Reports.Client.Packet.Full.MonthlyStatements
    Private WithEvents GettingStartedWithACH1 As DebtPlus.Reports.Client.Packet.Full.GettingStartedWithACH
    Private WithEvents AcH_Erollment1 As DebtPlus.Reports.Client.Packet.Full.ACH_Erollment
    Private WithEvents ClientGrievanceProcedures1 As DebtPlus.Reports.Client.Packet.Full.ClientGrievanceProcedures
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.Full.Budget
    Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Full.ActionPlanGoals
    Private WithEvents ActionPlanItems1 As DebtPlus.Reports.Client.Packet.Full.ActionPlanItems
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.Full.BottomLine
    Private WithEvents OtherDebts1 As DebtPlus.Reports.Client.Packet.Full.OtherDebts
    Private WithEvents NetWorth1 As DebtPlus.Reports.Client.Packet.Full.NetWorth
    Private WithEvents DmpDebts1 As DebtPlus.Reports.Client.Packet.Full.DMPDebts
    Friend WithEvents GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader8 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader9 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader10 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents Full_Cover_Letter1 As DebtPlus.Reports.Client.Packet.Full.Full_Cover_Letter
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents AgreementForServices1 As DebtPlus.Reports.Client.Packet.Full.AgreementForServices
    Private WithEvents CoverPage1 As DebtPlus.Reports.Client.Packet.Full.CoverPage
    Protected Friend WithEvents XrSubreport_CoverSheet As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
End Class
