#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class ActionPlanGoals

    Public Sub New()
        MyBase.New()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf ActionPlanGoals_BeforePrint
        'AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_Client_BeforePrint
        'AddHandler XrLabel_Client2.BeforePrint, AddressOf XrLabel_Client_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Private Sub XrLabel_Client_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)

        Dim ClientId As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)
        lbl.Text = "Client: " + DebtPlus.Utils.Format.Client.FormatClientID(ClientId)
    End Sub

    Private Sub XrLabel_Client2_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        XrLabel_Client_BeforePrint(sender, e)
    End Sub

    Private Sub ActionPlanGoals_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "action_items_goals"
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()
                    Dim ActionPlan As Integer = CType(Rpt.Parameters("ParameterActionPlan").Value, Integer)
                    If ActionPlan <= 0 Then
                        Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT dbo.map_client_to_action_plan(@client)"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                            Dim obj As Object = cmd.ExecuteScalar
                            ActionPlan = DebtPlus.Utils.Nulls.DInt(obj)
                        End Using
                    End If

                    If ActionPlan > 0 Then
                        Using cmd As New System.Data.SqlClient.SqlCommand()
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [text] FROM [action_items_goals] WITH (NOLOCK) WHERE [action_plan] = @ActionPlan"
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.CommandTimeout = 0
                            cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = ActionPlan

                            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, TableName)
                            End Using
                        End Using

                        tbl = ds.Tables(TableName)
                    End If
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")            End Try
        End If

        '-- Find the text string for the goals information
        Dim RTFText As String
        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            RTFText = DebtPlus.Utils.Nulls.DStr(tbl.Rows(0)(0)).Trim
        Else
            RTFText = String.Empty
        End If

        '-- Ensure that the text is somewhat valid for RTF
        If RTFText = String.Empty Then RTFText = "{\rtf1 }"

        '-- Set the RTF text into the display control.
        Dim Item As DevExpress.XtraReports.UI.XRRichText = TryCast(Rpt.FindControl("XrRichText1", True), DevExpress.XtraReports.UI.XRRichText)
        If Item IsNot Nothing Then
            Try
                If RTFText.StartsWith("{\") Then
                    Item.Rtf = RTFText
                Else
                    Item.Text = RTFText
                End If

            Catch ex As System.Exception
                Item.Text = RTFText
            End Try
        End If
    End Sub
End Class
