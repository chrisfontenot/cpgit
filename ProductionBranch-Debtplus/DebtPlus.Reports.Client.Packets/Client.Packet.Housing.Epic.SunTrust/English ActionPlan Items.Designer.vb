﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActionPlanItems
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ActionPlanItems))
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrLabel_group_name = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_description = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_checked = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ParameterActionPlan = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.PrintOn = DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_checked, Me.XrLabel_description})
        Me.Detail.HeightF = 18.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel_client})
        Me.PageFooter.HeightF = 26.33338!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Visible = False
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 12.70838!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 13.625!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Rev. 04/2015"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_client
        '
        Me.XrLabel_client.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(587.5!, 0.0!)
        Me.XrLabel_client.Name = "XrLabel_client"
        Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_client.SizeF = New System.Drawing.SizeF(160.4166!, 26.33337!)
        Me.XrLabel_client.StylePriority.UseFont = False
        Me.XrLabel_client.StylePriority.UseTextAlignment = False
        Me.XrLabel_client.Text = "Client: 0000000"
        Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_group_name})
        Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("item_group", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
        Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail
        Me.GroupHeader1.HeightF = 32.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.RepeatEveryPage = True
        '
        'XrLabel_group_name
        '
        Me.XrLabel_group_name.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_group_name.ForeColor = System.Drawing.Color.Teal
        Me.XrLabel_group_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 15.0!)
        Me.XrLabel_group_name.Name = "XrLabel_group_name"
        Me.XrLabel_group_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_group_name.Scripts.OnBeforePrint = "XrLabel_group_name_BeforePrint"
        Me.XrLabel_group_name.SizeF = New System.Drawing.SizeF(500.0!, 17.0!)
        Me.XrLabel_group_name.StylePriority.UseFont = False
        Me.XrLabel_group_name.StylePriority.UseForeColor = False
        '
        'XrLabel_description
        '
        Me.XrLabel_description.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
        Me.XrLabel_description.Font = New System.Drawing.Font("Arial", 9.75!)
        Me.XrLabel_description.LocationFloat = New DevExpress.Utils.PointFloat(17.0!, 1.0!)
        Me.XrLabel_description.Name = "XrLabel_description"
        Me.XrLabel_description.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_description.SizeF = New System.Drawing.SizeF(483.0!, 17.0!)
        Me.XrLabel_description.StylePriority.UseFont = False
        '
        'XrLabel_checked
        '
        Me.XrLabel_checked.Font = New System.Drawing.Font("Wingdings", 9.75!)
        Me.XrLabel_checked.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_checked.Name = "XrLabel_checked"
        Me.XrLabel_checked.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_checked.Scripts.OnBeforePrint = "XrLabel_checked_BeforePrint"
        Me.XrLabel_checked.SizeF = New System.Drawing.SizeF(16.0!, 18.0!)
        Me.XrLabel_checked.StylePriority.UseFont = False
        Me.XrLabel_checked.WordWrap = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8})
        Me.ReportHeader.HeightF = 36.41666!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(3.125!, 0.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(752.1166!, 36.41666!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "ACTION PLAN"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'ParameterActionPlan
        '
        Me.ParameterActionPlan.Description = "Plan ID"
        Me.ParameterActionPlan.Name = "ParameterActionPlan"
        Me.ParameterActionPlan.Type = GetType(Integer)
        Me.ParameterActionPlan.Visible = False
        '
        'ActionPlanItems
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.PageFooter, Me.GroupHeader1, Me.ReportHeader})
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterActionPlan})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "ActionPlanItemsSubReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMargin, 0)
        Me.Controls.SetChildIndex(Me.TopMargin, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrLabel_group_name As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_checked As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_description As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents ParameterActionPlan As DevExpress.XtraReports.Parameters.Parameter
    Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
End Class
