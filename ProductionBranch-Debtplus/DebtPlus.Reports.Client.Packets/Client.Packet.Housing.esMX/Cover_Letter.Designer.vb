<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cover_Letter
    Inherits Letter_Template

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Cover_Letter))
        Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrLabel_NameAndAddress = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_NameAndAddress, Me.XrLabel_Date, Me.XrRichText1})
        Me.Detail.HeightF = 511.8735!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Date
        '
        Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(2.000014!, 10.00001!)
        Me.XrLabel_Date.Name = "XrLabel_Date"
        Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Date.Scripts.OnBeforePrint = "XrLabel_Date_BeforePrint"
        Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(330.1249!, 23.0!)
        Me.XrLabel_Date.StylePriority.UseTextAlignment = False
        Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(2.000014!, 167.7916!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 5, 5, 100.0!)
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(748.0!, 320.5402!)
        Me.XrRichText1.StylePriority.UsePadding = False
        '
        'XrLabel_NameAndAddress
        '
        Me.XrLabel_NameAndAddress.LocationFloat = New DevExpress.Utils.PointFloat(2.000014!, 45.79166!)
        Me.XrLabel_NameAndAddress.Multiline = True
        Me.XrLabel_NameAndAddress.Name = "XrLabel_NameAndAddress"
        Me.XrLabel_NameAndAddress.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_NameAndAddress.Scripts.OnBeforePrint = "XrLabel_NameAndAddress_BeforePrint"
        Me.XrLabel_NameAndAddress.SizeF = New System.Drawing.SizeF(300.0!, 100.0!)
        '
        'Cover_Letter
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1})
        Me.Margins = New System.Drawing.Printing.Margins(50, 50, 25, 25)
        Me.ReportPrintOptions.DetailCount = 1
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "CoverPage_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_NameAndAddress As DevExpress.XtraReports.UI.XRLabel
End Class
