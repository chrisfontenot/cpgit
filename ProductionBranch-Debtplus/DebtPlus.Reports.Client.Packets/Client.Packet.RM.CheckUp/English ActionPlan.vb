#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class ActionPlan

    Public Sub New()
        MyBase.New()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf ActionPlanItemsSubReport_BeforePrint
        'AddHandler XrLabel_checked.BeforePrint, AddressOf XrLabel_checked_BeforePrint
        'AddHandler XrLabel_group_name.BeforePrint, AddressOf XrLabel_group_name_BeforePrint
        'AddHandler XrSubreport_Goals.BeforePrint, AddressOf XrSubreport_Goals_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Private Function getActionPlanID(ByVal Rpt As DevExpress.XtraReports.UI.XtraReport) As System.Int32
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim ActionPlan As Integer = CType(Rpt.Parameters("ParameterActionPlan").Value, Integer)
        If ActionPlan > 0 Then
            Return ActionPlan
        End If

        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()
            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT dbo.map_client_to_action_plan(@client)"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                Dim obj As Object = cmd.ExecuteScalar
                Return DebtPlus.Utils.Nulls.DInt(obj)
            End Using
        End Using
    End Function

    Private Function getActionPlanItems(ByVal Rpt As DevExpress.XtraReports.UI.XtraReport) As System.Data.DataTable
        Const TableName As String = "rpt_PrintClient_ActionPlan_ByPlan"
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet

        ' Find the current table. If found then stop here
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)
        If tbl IsNot Nothing Then
            Return tbl
        End If

        ' Find the plan for this report
        Dim ActionPlan As Integer = getActionPlanID(Rpt)
        If ActionPlan <= 0 Then
            Return tbl
        End If

        ' From the plan, obtain the items
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()

            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "rpt_PrintClient_ActionPlan_ByPlan"
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = ActionPlan

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                End Using
            End Using

            Return ds.Tables(TableName)
        End Using
    End Function

    Private Function getActionPlanGoals(ByVal Rpt As DevExpress.XtraReports.UI.XtraReport) As System.Data.DataTable
        Const TableName As String = "action_items_goals"
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet

        ' Find the current table. If found then stop here
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)
        If tbl IsNot Nothing Then
            Return tbl
        End If

        ' Find the plan for this report
        Dim ActionPlan As Integer = getActionPlanID(Rpt)
        If ActionPlan <= 0 Then
            Return tbl
        End If

        ' From the plan, obtain the items
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()

            Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT [text] FROM [action_items_goals] WITH (NOLOCK) WHERE [action_plan] = @ActionPlan"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = ActionPlan

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, TableName)
                End Using
            End Using

            Return ds.Tables(TableName)
        End Using
    End Function

    Private Sub ActionPlanItemsSubReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)

        Try
            Dim tbl As System.Data.DataTable = getActionPlanItems(Rpt)
            If tbl IsNot Nothing Then
                Rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "item_group, description", System.Data.DataViewRowState.CurrentRows)
            End If

        Catch ex As System.Data.SqlClient.SqlException
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")
        End Try
    End Sub

    Private Sub XrLabel_group_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Select Case DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("item_group"))
                Case 1 : .Text = "Action Items"
                Case 2 : .Text = "Items to Increase Income"
                Case 3 : .Text = "Items to Reduce Expenses"
                Case 4 : .Text = "Items Required for DMP"
            End Select
        End With
    End Sub

    Private Sub XrLabel_checked_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim Checked As Boolean = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("checked")) <> 0
            If Checked Then
                .Text = New String(Convert.ToChar(252), 1)
            Else
                .Text = New String(Convert.ToChar(111), 1)
            End If
        End With
    End Sub

    Private Sub XrSubreport_Goals_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

        ' Find the XRSubReport control
        Dim ctl As DevExpress.XtraReports.UI.XRSubreport = TryCast(sender, DevExpress.XtraReports.UI.XRSubreport)
        If ctl Is Nothing Then
            e.Cancel = True
            Return
        End If

        ' Find the current report source from the control
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(ctl.Report, DevExpress.XtraReports.UI.XtraReport)

        ' Find the report from the report source for the current control
        Dim subRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(ctl.ReportSource, DevExpress.XtraReports.UI.XtraReport)
        If subRpt Is Nothing Then
            e.Cancel = True
            Return
        End If

        ' From the report source, find the label that we want to update
        Dim lbl As DevExpress.XtraReports.UI.XRRichText = TryCast(subRpt.Bands(DevExpress.XtraReports.UI.BandKind.Detail).FindControl("XrRichText1", True), DevExpress.XtraReports.UI.XRRichText)
        If lbl Is Nothing Then
            e.Cancel = True
            Return
        End If

        Try
            ' Find the table with the single row for the goal text
            Dim tbl As System.Data.DataTable = getActionPlanGoals(Rpt)
            If tbl Is Nothing Then
                e.Cancel = True
                Return
            End If

            ' There needs to be a column and a row or we don't have data. No data = no print
            If tbl.Columns.Count < 1 Then
                e.Cancel = True
                Return
            End If

            If tbl.Rows.Count < 1 Then
                e.Cancel = True
                Return
            End If

            ' The text must not be empty
            Dim row As System.Data.DataRow = tbl.Rows(0)
            Dim goalText As String = DebtPlus.Utils.Nulls.DStr(row(0))
            If String.IsNullOrEmpty(goalText) Then
                e.Cancel = True
                Return
            End If

            Try
                ' Load the control with the HTML document
                If DebtPlus.Utils.Format.Strings.IsHTML(goalText) Then
                    lbl.Html = goalText
                    Return
                End If
            Catch ex As Exception
            End Try

            Try
                ' Load the RTF text as the document
                If DebtPlus.Utils.Format.Strings.IsRTF(goalText) Then
                    lbl.Rtf = goalText
                    Return
                End If
            Catch ex As Exception
            End Try

            Try
                ' Finally, load the item as text
                lbl.Text = goalText
                Return

            Catch ex As Exception
            End Try

        Catch ex As System.Data.SqlClient.SqlException
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")
        End Try

        ' Can not load the item. Cancel the printing
        e.Cancel = True
    End Sub
End Class
