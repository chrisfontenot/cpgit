#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class Cover_Letter

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        'AddHandler BeforePrint, AddressOf CoverPage_BeforePrint
    End Sub

    Private Sub CoverPage_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim LocalRpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(LocalRpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim Client As System.Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value, System.Globalization.CultureInfo.InvariantCulture)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet

        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT * FROM view_client_address WHERE [client]=@Client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = Client

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "view_client_address")
                End Using
            End Using
        End Using

        '-- Set the report source
        LocalRpt.DataSource = ds.Tables("view_client_address").DefaultView

        '-- Set the data source for any calculated fields
        For Each calcField As DevExpress.XtraReports.UI.CalculatedField In LocalRpt.CalculatedFields
            calcField.Assign(LocalRpt.DataSource, LocalRpt.DataMember)
        Next
    End Sub

    Private Sub XrLabel_NameAndAddress_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
        Dim sb As New System.Text.StringBuilder
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(lbl.Report, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)

        Dim vue As System.Data.DataView = CType(rpt.DataSource, System.Data.DataView)
        If vue.Count > 0 Then
            Dim drv As System.Data.DataRowView = vue(0)

            '-- Obtain the parameters for the name from the view
            For Each FieldName As String In New String() {"name", "addr1", "addr2", "addr3"}
                Dim obj As Object = drv(FieldName)
                If obj IsNot Nothing AndAlso obj IsNot System.DBNull.Value Then
                    Dim strObj As String = Convert.ToString(obj, System.Globalization.CultureInfo.InvariantCulture).Trim
                    If strObj <> String.Empty Then
                        sb.Append(System.Environment.NewLine)
                        sb.Append(strObj)
                    End If
                End If
            Next

            sb.Insert(0, DebtPlus.Utils.Format.Client.FormatClientID(MasterRpt.Parameters("ParameterClient").Value))
        End If

        lbl.Text = sb.ToString()
    End Sub
End Class
