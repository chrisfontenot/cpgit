<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.ActionPlanGoals()
        Me.XrSubreport_ActionPlan_Items = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems1 = New DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.ActionPlanItems()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.Budget()
        Me.XrSubreport_Summary = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.BottomLine()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Items})
        Me.GroupHeader11.HeightF = 23.0!
        Me.GroupHeader11.Level = 2
        Me.GroupHeader11.Name = "GroupHeader11"
        Me.GroupHeader11.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Name = "GroupHeader12"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Summary})
        Me.GroupHeader1.HeightF = 27.08333!
        Me.GroupHeader1.Level = 1
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_ActionPlan_Items
        '
        Me.XrSubreport_ActionPlan_Items.CanShrink = True
        Me.XrSubreport_ActionPlan_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.9998798!, 0.0!)
        Me.XrSubreport_ActionPlan_Items.Name = "XrSubreport_ActionPlan_Items"
        Me.XrSubreport_ActionPlan_Items.ReportSource = Me.ActionPlanItems1
        Me.XrSubreport_ActionPlan_Items.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(4.125122!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(795.8749!, 23.0!)
        '
        'XrSubreport_Summary
        '
        Me.XrSubreport_Summary.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Summary.Name = "XrSubreport_Summary"
        Me.XrSubreport_Summary.ReportSource = Me.BottomLine1
        Me.XrSubreport_Summary.SizeF = New System.Drawing.SizeF(799.0002!, 23.0!)
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader1})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.Budget
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.ActionPlanGoals
    Protected Friend WithEvents XrSubreport_ActionPlan_Items As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanItems1 As DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.ActionPlanItems
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_Summary As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.Housing.HECM.DefaultServicerExport.enUS.BottomLine
End Class
