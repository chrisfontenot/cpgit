<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader17 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader16 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader18 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader8 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader9 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader10 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader14 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader19 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader20 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader21 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.ABT.CoverPage()
        Me.XrSubreport_Counseling_Summary = New DevExpress.XtraReports.UI.XRSubreport()
        Me.English_Counseling_Summary1 = New DebtPlus.Reports.Client.Packet.ABT.EnglishCounselingSummary()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AgreementForServices1 = New DebtPlus.Reports.Client.Packet.ABT.AgreementForServices()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.PrivacyPolicy2 = New DebtPlus.Reports.Client.Packet.ABT.PrivacyPolicy()
        Me.XrSubreport_ClientRightsPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ClientRightsPolicy1 = New DebtPlus.Reports.Client.Packet.ABT.ClientRightsPolicy()
        Me.XrSubreport_DMPDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.DmpDebts1 = New DebtPlus.Reports.Client.Packet.ABT.DMPDebts()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.ABT.NetWorth()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.ABT.Budget()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.ABT.BottomLine()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals2 = New DebtPlus.Reports.Client.Packet.ABT.ActionPlanGoals()
        Me.XrSubreport_OtherDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_ActionPlanItems = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems2 = New DebtPlus.Reports.Client.Packet.ABT.ActionPlanItems()
        Me.XrSubreport_Financial_Future = New DevExpress.XtraReports.UI.XRSubreport()
        Me.YourfinancialFuture1 = New DebtPlus.Reports.Client.Packet.ABT.YourfinancialFuture()
        Me.XrSubreport_ImportHomebuyingInfo = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Important_Home_Buying_Info1 = New DebtPlus.Reports.Client.Packet.ABT.Important_Home_Buying_Info()
        Me.XrSubreport_HUDForYourProtection = New DevExpress.XtraReports.UI.XRSubreport()
        Me.HudForYourProtection1 = New DebtPlus.Reports.Client.Packet.ABT.HUDForYourProtection()
        Me.XrSubreport_QuestionsToAskHomeInspector = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Questions_to_Ask_Home_Inspector1 = New DebtPlus.Reports.Client.Packet.ABT.QuestionsToAskInspector()
        Me.XrSubreport_MaintainingYourHome = New DevExpress.XtraReports.UI.XRSubreport()
        Me.MaintainingYourHome1 = New DebtPlus.Reports.Client.Packet.ABT.MaintainingYourHome()
        Me.XrSubreport_HowToAvoidForeclosure = New DevExpress.XtraReports.UI.XRSubreport()
        Me.HowToAvoidForeclosure1 = New DebtPlus.Reports.Client.Packet.ABT.HowToAvoidForeclosure()
        Me.XrSubreport_ABTCertificate = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AbtCertificate1 = New DebtPlus.Reports.Client.Packet.ABT.ABTCertificate()
        Me.XrSubreport_GeneralServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.GeneralServices1 = New DebtPlus.Reports.Client.Packet.ABT.GeneralServices()
        Me.XrSubreport_CoverPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Full_Cover_Letter1 = New DebtPlus.Reports.Client.Packet.ABT.Cover_Letter()
        Me.XrSubreport_BlankPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BlankPage1 = New DebtPlus.Reports.Client.Packet.ABT.BlankPage()
        Me.OtherDebts1 = New DebtPlus.Reports.Client.Packet.ABT.OtherDebts()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.English_Counseling_Summary1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.YourfinancialFuture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Important_Home_Buying_Info1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HudForYourProtection1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Questions_to_Ask_Home_Inspector1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MaintainingYourHome1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HowToAvoidForeclosure1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AbtCertificate1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Cover})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader17
        '
        Me.GroupHeader17.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Counseling_Summary})
        Me.GroupHeader17.HeightF = 23.95833!
        Me.GroupHeader17.Level = 18
        Me.GroupHeader17.Name = "GroupHeader17"
        Me.GroupHeader17.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader16
        '
        Me.GroupHeader16.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader16.HeightF = 23.95833!
        Me.GroupHeader16.Level = 17
        Me.GroupHeader16.Name = "GroupHeader16"
        Me.GroupHeader16.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader18
        '
        Me.GroupHeader18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader18.HeightF = 25.08335!
        Me.GroupHeader18.Level = 16
        Me.GroupHeader18.Name = "GroupHeader18"
        Me.GroupHeader18.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientRightsPolicy})
        Me.GroupHeader15.HeightF = 23.0!
        Me.GroupHeader15.Level = 15
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DMPDebts})
        Me.GroupHeader6.HeightF = 28.125!
        Me.GroupHeader6.Level = 11
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader7.HeightF = 26.04167!
        Me.GroupHeader7.Level = 9
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader8
        '
        Me.GroupHeader8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader8.HeightF = 26.04167!
        Me.GroupHeader8.Level = 13
        Me.GroupHeader8.Name = "GroupHeader8"
        Me.GroupHeader8.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader9
        '
        Me.GroupHeader9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader9.HeightF = 22.99997!
        Me.GroupHeader9.Level = 12
        Me.GroupHeader9.Name = "GroupHeader9"
        Me.GroupHeader9.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader1.HeightF = 23.0!
        Me.GroupHeader1.Level = 8
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_OtherDebts})
        Me.GroupHeader2.HeightF = 24.66666!
        Me.GroupHeader2.Level = 10
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader3
        '
        Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlanItems})
        Me.GroupHeader3.HeightF = 25.00001!
        Me.GroupHeader3.Level = 7
        Me.GroupHeader3.Name = "GroupHeader3"
        Me.GroupHeader3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader4
        '
        Me.GroupHeader4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Financial_Future})
        Me.GroupHeader4.HeightF = 25.0!
        Me.GroupHeader4.Level = 6
        Me.GroupHeader4.Name = "GroupHeader4"
        Me.GroupHeader4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ImportHomebuyingInfo})
        Me.GroupHeader5.HeightF = 25.0!
        Me.GroupHeader5.Level = 5
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader10
        '
        Me.GroupHeader10.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_HUDForYourProtection})
        Me.GroupHeader10.HeightF = 25.0!
        Me.GroupHeader10.Level = 4
        Me.GroupHeader10.Name = "GroupHeader10"
        Me.GroupHeader10.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_QuestionsToAskHomeInspector})
        Me.GroupHeader11.HeightF = 26.12502!
        Me.GroupHeader11.Level = 3
        Me.GroupHeader11.Name = "GroupHeader11"
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_MaintainingYourHome})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Level = 2
        Me.GroupHeader12.Name = "GroupHeader12"
        Me.GroupHeader12.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_HowToAvoidForeclosure})
        Me.GroupHeader13.HeightF = 23.95833!
        Me.GroupHeader13.Level = 1
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader14
        '
        Me.GroupHeader14.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ABTCertificate})
        Me.GroupHeader14.HeightF = 23.0!
        Me.GroupHeader14.Name = "GroupHeader14"
        Me.GroupHeader14.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader19
        '
        Me.GroupHeader19.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralServices})
        Me.GroupHeader19.HeightF = 23.0!
        Me.GroupHeader19.Level = 14
        Me.GroupHeader19.Name = "GroupHeader19"
        '
        'GroupHeader20
        '
        Me.GroupHeader20.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_CoverPage})
        Me.GroupHeader20.HeightF = 23.0!
        Me.GroupHeader20.Level = 19
        Me.GroupHeader20.Name = "GroupHeader20"
        Me.GroupHeader20.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader21
        '
        Me.GroupHeader21.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BlankPage})
        Me.GroupHeader21.HeightF = 23.0!
        Me.GroupHeader21.Level = 20
        Me.GroupHeader21.Name = "GroupHeader21"
        Me.GroupHeader21.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_Counseling_Summary
        '
        Me.XrSubreport_Counseling_Summary.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Counseling_Summary.Name = "XrSubreport_Counseling_Summary"
        Me.XrSubreport_Counseling_Summary.ReportSource = Me.English_Counseling_Summary1
        Me.XrSubreport_Counseling_Summary.SizeF = New System.Drawing.SizeF(797.8746!, 23.0!)
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.PrivacyPolicy2
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_ClientRightsPolicy
        '
        Me.XrSubreport_ClientRightsPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ClientRightsPolicy.Name = "XrSubreport_ClientRightsPolicy"
        Me.XrSubreport_ClientRightsPolicy.ReportSource = Me.ClientRightsPolicy1
        Me.XrSubreport_ClientRightsPolicy.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_DMPDebts
        '
        Me.XrSubreport_DMPDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_DMPDebts.Name = "XrSubreport_DMPDebts"
        Me.XrSubreport_DMPDebts.ReportSource = Me.DmpDebts1
        Me.XrSubreport_DMPDebts.SizeF = New System.Drawing.SizeF(793.7495!, 28.125!)
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(796.8745!, 22.99994!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(799.0001!, 22.99997!)
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals2
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_OtherDebts
        '
        Me.XrSubreport_OtherDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_OtherDebts.Name = "XrSubreport_OtherDebts"
        Me.XrSubreport_OtherDebts.ReportSource = Me.OtherDebts1
        Me.XrSubreport_OtherDebts.SizeF = New System.Drawing.SizeF(793.7496!, 23.0!)
        '
        'XrSubreport_ActionPlanItems
        '
        Me.XrSubreport_ActionPlanItems.CanShrink = True
        Me.XrSubreport_ActionPlanItems.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlanItems.Name = "XrSubreport_ActionPlanItems"
        Me.XrSubreport_ActionPlanItems.ReportSource = Me.ActionPlanItems2
        Me.XrSubreport_ActionPlanItems.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_Financial_Future
        '
        Me.XrSubreport_Financial_Future.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Financial_Future.Name = "XrSubreport_Financial_Future"
        Me.XrSubreport_Financial_Future.ReportSource = Me.YourfinancialFuture1
        Me.XrSubreport_Financial_Future.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_ImportHomebuyingInfo
        '
        Me.XrSubreport_ImportHomebuyingInfo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ImportHomebuyingInfo.Name = "XrSubreport_ImportHomebuyingInfo"
        Me.XrSubreport_ImportHomebuyingInfo.ReportSource = Me.Important_Home_Buying_Info1
        Me.XrSubreport_ImportHomebuyingInfo.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_HUDForYourProtection
        '
        Me.XrSubreport_HUDForYourProtection.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_HUDForYourProtection.Name = "XrSubreport_HUDForYourProtection"
        Me.XrSubreport_HUDForYourProtection.ReportSource = Me.HudForYourProtection1
        Me.XrSubreport_HUDForYourProtection.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_QuestionsToAskHomeInspector
        '
        Me.XrSubreport_QuestionsToAskHomeInspector.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_QuestionsToAskHomeInspector.Name = "XrSubreport_QuestionsToAskHomeInspector"
        Me.XrSubreport_QuestionsToAskHomeInspector.ReportSource = Me.Questions_to_Ask_Home_Inspector1
        Me.XrSubreport_QuestionsToAskHomeInspector.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_MaintainingYourHome
        '
        Me.XrSubreport_MaintainingYourHome.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_MaintainingYourHome.Name = "XrSubreport_MaintainingYourHome"
        Me.XrSubreport_MaintainingYourHome.ReportSource = Me.MaintainingYourHome1
        Me.XrSubreport_MaintainingYourHome.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_HowToAvoidForeclosure
        '
        Me.XrSubreport_HowToAvoidForeclosure.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_HowToAvoidForeclosure.Name = "XrSubreport_HowToAvoidForeclosure"
        Me.XrSubreport_HowToAvoidForeclosure.ReportSource = Me.HowToAvoidForeclosure1
        Me.XrSubreport_HowToAvoidForeclosure.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_ABTCertificate
        '
        Me.XrSubreport_ABTCertificate.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ABTCertificate.Name = "XrSubreport_ABTCertificate"
        Me.XrSubreport_ABTCertificate.ReportSource = Me.AbtCertificate1
        Me.XrSubreport_ABTCertificate.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_GeneralServices
        '
        Me.XrSubreport_GeneralServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralServices.Name = "XrSubreport_GeneralServices"
        Me.XrSubreport_GeneralServices.ReportSource = Me.GeneralServices1
        Me.XrSubreport_GeneralServices.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_CoverPage
        '
        Me.XrSubreport_CoverPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_CoverPage.Name = "XrSubreport_CoverPage"
        Me.XrSubreport_CoverPage.ReportSource = Me.Full_Cover_Letter1
        Me.XrSubreport_CoverPage.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_BlankPage
        '
        Me.XrSubreport_BlankPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BlankPage.Name = "XrSubreport_BlankPage"
        Me.XrSubreport_BlankPage.ReportSource = Me.BlankPage1
        Me.XrSubreport_BlankPage.SizeF = New System.Drawing.SizeF(796.8746!, 23.0!)
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader17, Me.GroupHeader16, Me.GroupHeader18, Me.GroupHeader15, Me.GroupHeader6, Me.GroupHeader7, Me.GroupHeader8, Me.GroupHeader9, Me.GroupHeader1, Me.GroupHeader2, Me.GroupHeader3, Me.GroupHeader4, Me.GroupHeader5, Me.GroupHeader10, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader13, Me.GroupHeader14, Me.GroupHeader19, Me.GroupHeader20, Me.GroupHeader21})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader21, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader20, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader19, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader14, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader10, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader4, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader9, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader8, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader18, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader16, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader17, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.English_Counseling_Summary1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.YourfinancialFuture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Important_Home_Buying_Info1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HudForYourProtection1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Questions_to_Ask_Home_Inspector1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MaintainingYourHome1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HowToAvoidForeclosure1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AbtCertificate1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Protected Friend WithEvents XrSubreport_CoverPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Full_Cover_Letter1 As Cover_Letter
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport_ClientRightsPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ClientRightsPolicy1 As DebtPlus.Reports.Client.Packet.ABT.ClientRightsPolicy
    Protected Friend WithEvents XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.ABT.BottomLine
    Protected Friend WithEvents XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents NetWorth1 As DebtPlus.Reports.Client.Packet.ABT.NetWorth
    Protected Friend WithEvents XrSubreport_DMPDebts As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents DmpDebts1 As DebtPlus.Reports.Client.Packet.ABT.DMPDebts
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.ABT.Budget
    Protected Friend WithEvents XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents PrivacyPolicy2 As DebtPlus.Reports.Client.Packet.ABT.PrivacyPolicy
    Friend WithEvents XrSubreport_Counseling_Summary As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents GroupHeader17 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader16 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents AgreementForServices1 As DebtPlus.Reports.Client.Packet.ABT.AgreementForServices
    Protected Friend WithEvents XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents GroupHeader18 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader8 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader9 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_OtherDebts As DevExpress.XtraReports.UI.XRSubreport
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents English_Counseling_Summary1 As DebtPlus.Reports.Client.Packet.ABT.EnglishCounselingSummary
    Private WithEvents ActionPlanGoals2 As DebtPlus.Reports.Client.Packet.ABT.ActionPlanGoals
    Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_ActionPlanItems As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanItems2 As DebtPlus.Reports.Client.Packet.ABT.ActionPlanItems
    Friend WithEvents GroupHeader4 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_Financial_Future As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents YourfinancialFuture1 As DebtPlus.Reports.Client.Packet.ABT.YourfinancialFuture
    Friend WithEvents GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_ImportHomebuyingInfo As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Important_Home_Buying_Info1 As DebtPlus.Reports.Client.Packet.ABT.Important_Home_Buying_Info
    Friend WithEvents GroupHeader10 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_HUDForYourProtection As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents HudForYourProtection1 As DebtPlus.Reports.Client.Packet.ABT.HUDForYourProtection
    Friend WithEvents GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_QuestionsToAskHomeInspector As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Questions_to_Ask_Home_Inspector1 As DebtPlus.Reports.Client.Packet.ABT.QuestionsToAskInspector
    Friend WithEvents GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_MaintainingYourHome As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents MaintainingYourHome1 As DebtPlus.Reports.Client.Packet.ABT.MaintainingYourHome
    Friend WithEvents GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_HowToAvoidForeclosure As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents HowToAvoidForeclosure1 As DebtPlus.Reports.Client.Packet.ABT.HowToAvoidForeclosure
    Friend WithEvents GroupHeader14 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_ABTCertificate As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents AbtCertificate1 As DebtPlus.Reports.Client.Packet.ABT.ABTCertificate
    Friend WithEvents GroupHeader19 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_GeneralServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents GeneralServices1 As DebtPlus.Reports.Client.Packet.ABT.GeneralServices
    Protected Friend WithEvents XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents CoverPage1 As DebtPlus.Reports.Client.Packet.ABT.CoverPage
    Friend WithEvents GroupHeader20 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader21 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_BlankPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BlankPage1 As DebtPlus.Reports.Client.Packet.ABT.BlankPage
    Private WithEvents OtherDebts1 As DebtPlus.Reports.Client.Packet.ABT.OtherDebts


End Class
