#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class ABTCertificate

    Public Sub New()
        MyBase.New()
        MyClass.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()
        'AddHandler XrLabel3.BeforePrint, AddressOf XrLabel_Client_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Private Sub XrLabelClientName_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs)
        Dim LocalRpt As DevExpress.XtraReports.UI.XtraReport = CType(CType(sender, DevExpress.XtraReports.UI.XRLabel).Report, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(LocalRpt)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim Client As System.Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value, System.Globalization.CultureInfo.InvariantCulture)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet

        Using cn As New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
            cn.Open()
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT * FROM view_client_address WHERE [client]=@Client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = Client

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "view_client_address")
                End Using
            End Using

            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "select v.name as clientname, ca.partner as ABTID,v.counselor_name as counselorname, ca.start_time as apptstarttime from client_appointments ca inner join view_client_address v on v.client = ca.client WHERE ca.client = @Client and ca.partner is not null "

                cmd.CommandType = System.Data.CommandType.Text
                cmd.Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = Client

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "ABT_Record")
                End Using
            End Using

        End Using

        Dim tbl As System.Data.DataTable = ds.Tables("ABT_Record")
        If tbl.Rows.Count > 0 Then
            XrLabel_ABTIdValue.Text = tbl.Rows(0)("ABTID").ToString()
            XrLabel_CounselorValue.Text = tbl.Rows(0)("counselorname").ToString()
            XrLabel_ProgramstartDateValue.Text = tbl.Rows(0)("apptstarttime").ToString()
            XrLabelClientName.Text = tbl.Rows(0)("clientname").ToString()

        End If
    End Sub

    
End Class
