<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrTable1 = New DevExpress.XtraReports.UI.XRTable()
        Me.XrTableRow1 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell3 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow2 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell1 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrTableRow3 = New DevExpress.XtraReports.UI.XRTableRow()
        Me.XrTableCell2 = New DevExpress.XtraReports.UI.XRTableCell()
        Me.XrRichText7 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText9 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader8 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader9 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader10 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader14 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader16 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.CoverPage()
        Me.XrSubreport_ClientRightsPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ClientRightsPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.ClientRightsPolicy()
        Me.XrSubreport_GeneralServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.GeneralServices1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.GeneralServices()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.BottomLine()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.NetWorth()
        Me.XrSubreport_DMPDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.DmpDebts1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.DMPDebts()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.Budget()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.ActionPlanGoals()
        Me.XrSubreport_ActionPlan_Items = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.ActionPlanItems()
        Me.XrSubreport_HousingPrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.PrivacyPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.HousingPrivacyPolicy()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AgreementForServices1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.AgreementForServices()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.PrivacyPolicy2 = New DebtPlus.Reports.Client.Packet.Housing.enUS.PrivacyPolicy()
        Me.XrSubreport_CoverPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Full_Cover_Letter1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.Cover_Letter()
        Me.GroupHeader17 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_BlankPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BlankPage1 = New DebtPlus.Reports.Client.Packet.Housing.enUS.BlankPage()
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Cover})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrTable1, Me.XrRichText7})
        Me.GroupHeader1.Expanded = False
        Me.GroupHeader1.HeightF = 596.9585!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrTable1
        '
        Me.XrTable1.Borders = DevExpress.XtraPrinting.BorderSide.Top
        Me.XrTable1.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrTable1.LocationFloat = New DevExpress.Utils.PointFloat(250.0!, 521.9585!)
        Me.XrTable1.Name = "XrTable1"
        Me.XrTable1.Rows.AddRange(New DevExpress.XtraReports.UI.XRTableRow() {Me.XrTableRow1, Me.XrTableRow2, Me.XrTableRow3})
        Me.XrTable1.SizeF = New System.Drawing.SizeF(300.0!, 75.0!)
        Me.XrTable1.StylePriority.UseBorders = False
        Me.XrTable1.StylePriority.UseFont = False
        '
        'XrTableRow1
        '
        Me.XrTableRow1.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell3})
        Me.XrTableRow1.Name = "XrTableRow1"
        Me.XrTableRow1.Weight = 1.0R
        '
        'XrTableCell3
        '
        Me.XrTableCell3.Font = New System.Drawing.Font("Times New Roman", 9.75!)
        Me.XrTableCell3.Name = "XrTableCell3"
        Me.XrTableCell3.StylePriority.UseFont = False
        Me.XrTableCell3.Weight = 3.0R
        '
        'XrTableRow2
        '
        Me.XrTableRow2.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell1})
        Me.XrTableRow2.Name = "XrTableRow2"
        Me.XrTableRow2.Weight = 1.0R
        '
        'XrTableCell1
        '
        Me.XrTableCell1.Name = "XrTableCell1"
        Me.XrTableCell1.Text = "(print full name)"
        Me.XrTableCell1.Weight = 3.0R
        '
        'XrTableRow3
        '
        Me.XrTableRow3.Cells.AddRange(New DevExpress.XtraReports.UI.XRTableCell() {Me.XrTableCell2})
        Me.XrTableRow3.Name = "XrTableRow3"
        Me.XrTableRow3.Weight = 1.0R
        '
        'XrTableCell2
        '
        Me.XrTableCell2.Name = "XrTableCell2"
        Me.XrTableCell2.Text = "Date"
        Me.XrTableCell2.Weight = 3.0R
        '
        'XrRichText7
        '
        Me.XrRichText7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText7.Name = "XrRichText7"
        Me.XrRichText7.SerializableRtfString = resources.GetString("XrRichText7.SerializableRtfString")
        Me.XrRichText7.SizeF = New System.Drawing.SizeF(795.8748!, 521.9585!)
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientRightsPolicy})
        Me.GroupHeader2.HeightF = 23.0!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader3
        '
        Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralServices})
        Me.GroupHeader3.HeightF = 23.0!
        Me.GroupHeader3.Level = 2
        Me.GroupHeader3.Name = "GroupHeader3"
        Me.GroupHeader3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader4
        '
        Me.GroupHeader4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText9})
        Me.GroupHeader4.HeightF = 186.875!
        Me.GroupHeader4.Level = 3
        Me.GroupHeader4.Name = "GroupHeader4"
        Me.GroupHeader4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText9
        '
        Me.XrRichText9.LocationFloat = New DevExpress.Utils.PointFloat(0.9998322!, 0.0!)
        Me.XrRichText9.Name = "XrRichText9"
        Me.XrRichText9.SerializableRtfString = resources.GetString("XrRichText9.SerializableRtfString")
        Me.XrRichText9.SizeF = New System.Drawing.SizeF(794.8749!, 186.875!)
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2})
        Me.GroupHeader5.HeightF = 1486.541!
        Me.GroupHeader5.Level = 4
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText2
        '
        Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText2.Name = "XrRichText2"
        Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
        Me.XrRichText2.SizeF = New System.Drawing.SizeF(799.0001!, 1486.541!)
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_HousingPrivacyPolicy})
        Me.GroupHeader6.HeightF = 23.0!
        Me.GroupHeader6.Level = 5
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader7.HeightF = 22.99997!
        Me.GroupHeader7.Level = 6
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader8
        '
        Me.GroupHeader8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader8.HeightF = 22.99994!
        Me.GroupHeader8.Level = 7
        Me.GroupHeader8.Name = "GroupHeader8"
        Me.GroupHeader8.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader9
        '
        Me.GroupHeader9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DMPDebts})
        Me.GroupHeader9.HeightF = 22.99994!
        Me.GroupHeader9.Level = 8
        Me.GroupHeader9.Name = "GroupHeader9"
        Me.GroupHeader9.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader10
        '
        Me.GroupHeader10.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader10.HeightF = 23.0!
        Me.GroupHeader10.Level = 9
        Me.GroupHeader10.Name = "GroupHeader10"
        Me.GroupHeader10.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader11.HeightF = 23.0!
        Me.GroupHeader11.Level = 11
        Me.GroupHeader11.Name = "GroupHeader11"
        Me.GroupHeader11.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Items})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Level = 10
        Me.GroupHeader12.Name = "GroupHeader12"
        Me.GroupHeader12.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
        Me.GroupHeader13.HeightF = 363.625!
        Me.GroupHeader13.Level = 12
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(795.8749!, 363.625!)
        Me.XrRichText1.StylePriority.UsePadding = False
        '
        'GroupHeader14
        '
        Me.GroupHeader14.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader14.HeightF = 23.79166!
        Me.GroupHeader14.Level = 13
        Me.GroupHeader14.Name = "GroupHeader14"
        Me.GroupHeader14.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader15.HeightF = 23.0!
        Me.GroupHeader15.Level = 14
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader16
        '
        Me.GroupHeader16.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_CoverPage})
        Me.GroupHeader16.HeightF = 23.0!
        Me.GroupHeader16.Level = 15
        Me.GroupHeader16.Name = "GroupHeader16"
        Me.GroupHeader16.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_ClientRightsPolicy
        '
        Me.XrSubreport_ClientRightsPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ClientRightsPolicy.Name = "XrSubreport_ClientRightsPolicy"
        Me.XrSubreport_ClientRightsPolicy.ReportSource = Me.ClientRightsPolicy1
        Me.XrSubreport_ClientRightsPolicy.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_GeneralServices
        '
        Me.XrSubreport_GeneralServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralServices.Name = "XrSubreport_GeneralServices"
        Me.XrSubreport_GeneralServices.ReportSource = Me.GeneralServices1
        Me.XrSubreport_GeneralServices.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(799.0001!, 22.99997!)
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(799.0001!, 22.99994!)
        '
        'XrSubreport_DMPDebts
        '
        Me.XrSubreport_DMPDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_DMPDebts.Name = "XrSubreport_DMPDebts"
        Me.XrSubreport_DMPDebts.ReportSource = Me.DmpDebts1
        Me.XrSubreport_DMPDebts.SizeF = New System.Drawing.SizeF(793.7495!, 22.99994!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(795.8749!, 23.0!)
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_ActionPlan_Items
        '
        Me.XrSubreport_ActionPlan_Items.CanShrink = True
        Me.XrSubreport_ActionPlan_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Items.Name = "XrSubreport_ActionPlan_Items"
        Me.XrSubreport_ActionPlan_Items.ReportSource = Me.ActionPlanItems1
        Me.XrSubreport_ActionPlan_Items.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_HousingPrivacyPolicy
        '
        Me.XrSubreport_HousingPrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_HousingPrivacyPolicy.Name = "XrSubreport_HousingPrivacyPolicy"
        Me.XrSubreport_HousingPrivacyPolicy.ReportSource = Me.PrivacyPolicy1
        Me.XrSubreport_HousingPrivacyPolicy.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.PrivacyPolicy2
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'XrSubreport_CoverPage
        '
        Me.XrSubreport_CoverPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_CoverPage.Name = "XrSubreport_CoverPage"
        Me.XrSubreport_CoverPage.ReportSource = Me.Full_Cover_Letter1
        Me.XrSubreport_CoverPage.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'GroupHeader17
        '
        Me.GroupHeader17.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BlankPage})
        Me.GroupHeader17.HeightF = 23.0!
        Me.GroupHeader17.Level = 16
        Me.GroupHeader17.Name = "GroupHeader17"
        Me.GroupHeader17.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_BlankPage
        '
        Me.XrSubreport_BlankPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BlankPage.Name = "XrSubreport_BlankPage"
        Me.XrSubreport_BlankPage.ReportSource = Me.BlankPage1
        Me.XrSubreport_BlankPage.SizeF = New System.Drawing.SizeF(797.9999!, 23.0!)
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader1, Me.GroupHeader2, Me.GroupHeader3, Me.GroupHeader4, Me.GroupHeader5, Me.GroupHeader7, Me.GroupHeader8, Me.GroupHeader9, Me.GroupHeader10, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader13, Me.GroupHeader6, Me.GroupHeader14, Me.GroupHeader15, Me.GroupHeader16, Me.GroupHeader17})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader17, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader16, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader14, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader10, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader9, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader8, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader4, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.XrTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader4 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader8 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader9 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader10 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Protected Friend WithEvents XrSubreport_CoverPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Full_Cover_Letter1 As Cover_Letter
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrTable1 As DevExpress.XtraReports.UI.XRTable
    Friend WithEvents XrTableRow1 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell3 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow2 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell1 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrTableRow3 As DevExpress.XtraReports.UI.XRTableRow
    Friend WithEvents XrTableCell2 As DevExpress.XtraReports.UI.XRTableCell
    Friend WithEvents XrRichText7 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrSubreport_ClientRightsPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ClientRightsPolicy1 As DebtPlus.Reports.Client.Packet.Housing.enUS.ClientRightsPolicy
    Friend WithEvents XrRichText9 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Protected Friend WithEvents XrSubreport_HousingPrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents PrivacyPolicy1 As DebtPlus.Reports.Client.Packet.Housing.enUS.HousingPrivacyPolicy
    Protected Friend WithEvents XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.Housing.enUS.BottomLine
    Protected Friend WithEvents XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents NetWorth1 As DebtPlus.Reports.Client.Packet.Housing.enUS.NetWorth
    Protected Friend WithEvents XrSubreport_DMPDebts As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents DmpDebts1 As DebtPlus.Reports.Client.Packet.Housing.enUS.DMPDebts
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.Housing.enUS.Budget
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Housing.enUS.ActionPlanGoals
    Protected Friend WithEvents XrSubreport_ActionPlan_Items As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanItems1 As DebtPlus.Reports.Client.Packet.Housing.enUS.ActionPlanItems
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader14 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents AgreementForServices1 As DebtPlus.Reports.Client.Packet.Housing.enUS.AgreementForServices
    Protected Friend WithEvents XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents PrivacyPolicy2 As DebtPlus.Reports.Client.Packet.Housing.enUS.PrivacyPolicy
    Friend WithEvents XrSubreport_GeneralServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents GeneralServices1 As DebtPlus.Reports.Client.Packet.Housing.enUS.GeneralServices
    Protected Friend WithEvents XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents GroupHeader16 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents CoverPage1 As DebtPlus.Reports.Client.Packet.Housing.enUS.CoverPage
    Friend WithEvents GroupHeader17 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_BlankPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BlankPage1 As DebtPlus.Reports.Client.Packet.Housing.enUS.BlankPage
End Class
