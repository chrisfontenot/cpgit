﻿Public Class EnglishFICO

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        'AddHandler BeforePrint, AddressOf FICO_1_BeforePrint
    End Sub

    Private Sub FICO_1_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_FicoScore"

        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = _
"SELECT p.client, p.relation, p.person, p.FICO_Score, p.FICO_pull_date, p.CreditAgency, sr.oID as ScoreReasonID, " +
"case when sr.oID is null then 'No factors were given' else sr.description end as description, " +
"case when sr.oID is null then 'The Credit Reporting Agency did not supply a reason.' else sr.long_description end as long_description, " +
"case when sr.oID is null then 'There are no items at this time.' else sr.tip end as tip, " +
"dbo.format_normal_name(default,pn.first,pn.middle,pn.last,pn.suffix) as client_name, dbo.format_normal_name(default,con.first,default,con.last,con.suffix) as counselor_name " +
"from people p with (nolock) " +
"left outer join names pn with (nolock) on p.NameID = pn.Name " +
"left outer join PeopleFICOScoreReasons pr with (nolock) on p.person = pr.PersonID " +
"left outer join FICOScoreReasonCodes sr with (nolock) on pr.ScoreReasonID = sr.oID " +
"left outer join counselors co with (nolock) on co.person = suser_sname() " +
"left outer join names con with (nolock) on co.nameid = con.name " +
"where p.client = @client And isnull(p.fico_score, 0) > 0 " +
"order by p.relation, p.person;"

                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, "FicoScore")
                            tbl = ds.Tables("FicoScore")
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading net worth")            End Try
        End If

        If tbl IsNot Nothing Then
            Rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "[relation],[person]", System.Data.DataViewRowState.CurrentRows)
        End If
    End Sub
End Class
