#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class Signatures

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        ' AddHandler BeforePrint, AddressOf Signatures_BeforePrint
    End Sub

    Dim ds As New System.Data.DataSet("ds")

    Private Sub Signatures_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "signatures"
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)
        Dim client As Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value)

        If tbl Is Nothing Then
            Try
                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    cn.Open()

                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT client, relation, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name from people p inner join names pn on p.nameid = pn.name where p.client = @client"
                        cmd.CommandType = System.Data.CommandType.Text
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = client

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using
                End Using

                tbl = ds.Tables(TableName)
                If tbl.Rows.Count < 1 Then
                    Dim row As System.Data.DataRow = tbl.NewRow()
                    row("client") = client
                    row("relation") = 1
                    row("name") = "Signature"
                    tbl.Rows.Add(row)
                End If

                rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "relation", System.Data.DataViewRowState.CurrentRows)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client names")            End Try
        End If
    End Sub
End Class
