#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class NetWorth

    Public Sub New()
        MyBase.New()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf NetWorthSubReport_BeforePrint
    End Sub

    Private Sub NetWorthSubReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_NetWorth"

        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                Using cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = cn
                        cmd.CommandText = "rpt_Financial_Net_Worth"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure
                        cmd.CommandTimeout = 0
                        cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)

                            tbl = ds.Tables(TableName)
                            If Not tbl.Columns.Contains("difference") Then
                                tbl.Columns.Add("difference", GetType(Decimal), "isnull([l_value],0)-isnull([r_value],0)")
                            End If
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading net worth")            End Try
        End If

        If tbl IsNot Nothing Then
            Rpt.DataSource = tbl.DefaultView
        End If
    End Sub
End Class
