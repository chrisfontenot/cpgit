<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.CoverPage()
        Me.XrSubreport_CoverLetter = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Cover_Letter1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Cover_Letter()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.GroupHeader_ClientRightsPolicy = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_ClientRightsPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ClientRightsPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ClientRightsPolicy()
        Me.GroupHeader_BottomLine = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.BottomLine()
        Me.GroupHeader_NetWorth = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.NetWorth()
        Me.GroupHeader_Addendum = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_Addendum = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Addendum1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Addendum()
        Me.GroupHeader_StateInformation = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_StateInformation = New DevExpress.XtraReports.UI.XRSubreport()
        Me.English_Housing_Privacy_Principles1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.English_Housing_Privacy_Principles()
        Me.GroupHeader_Budget = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Budget()
        Me.GroupHeader_ActionPlan_Addendum = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_ActionPlan_Addendum = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanAddendum1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ActionPlanAddendum()
        Me.GroupHeader_ActionPlan_Goals = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ActionPlanGoals()
        Me.GroupHeader_GeneralServices = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_GeneralServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.GeneralServices1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.GeneralServices()
        Me.XrSubreport_FICO = New DevExpress.XtraReports.UI.XRSubreport()
        Me.EnglishFICO1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.EnglishFICO()
        Me.GroupHeader_AgreementForServices = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.AgreementForServices1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.AgreementForServices()
        Me.XrSubreport_HPF_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.HpF_PrivacyPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.HPF_PrivacyPolicy()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrPageBreak2 = New DevExpress.XtraReports.UI.XRPageBreak()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Addendum1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.English_Housing_Privacy_Principles1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanAddendum1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EnglishFICO1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HpF_PrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 1.041667!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak1, Me.XrSubreport_Cover, Me.XrSubreport_CoverLetter})
        Me.ReportHeader.HeightF = 47.99998!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrPageBreak1
        '
        Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
        Me.XrPageBreak1.Name = "XrPageBreak1"
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_CoverLetter
        '
        Me.XrSubreport_CoverLetter.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 24.99999!)
        Me.XrSubreport_CoverLetter.Name = "XrSubreport_CoverLetter"
        Me.XrSubreport_CoverLetter.ReportSource = Me.Cover_Letter1
        Me.XrSubreport_CoverLetter.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader_ClientRightsPolicy
        '
        Me.GroupHeader_ClientRightsPolicy.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientRightsPolicy})
        Me.GroupHeader_ClientRightsPolicy.HeightF = 23.0!
        Me.GroupHeader_ClientRightsPolicy.Level = 1
        Me.GroupHeader_ClientRightsPolicy.Name = "GroupHeader_ClientRightsPolicy"
        Me.GroupHeader_ClientRightsPolicy.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ClientRightsPolicy
        '
        Me.XrSubreport_ClientRightsPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ClientRightsPolicy.Name = "XrSubreport_ClientRightsPolicy"
        Me.XrSubreport_ClientRightsPolicy.ReportSource = Me.ClientRightsPolicy1
        Me.XrSubreport_ClientRightsPolicy.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_BottomLine
        '
        Me.GroupHeader_BottomLine.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader_BottomLine.HeightF = 23.79166!
        Me.GroupHeader_BottomLine.Level = 7
        Me.GroupHeader_BottomLine.Name = "GroupHeader_BottomLine"
        Me.GroupHeader_BottomLine.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.7916451!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_NetWorth
        '
        Me.GroupHeader_NetWorth.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader_NetWorth.HeightF = 23.0!
        Me.GroupHeader_NetWorth.Level = 6
        Me.GroupHeader_NetWorth.Name = "GroupHeader_NetWorth"
        Me.GroupHeader_NetWorth.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(800.0!, 22.99994!)
        '
        'GroupHeader_Addendum
        '
        Me.GroupHeader_Addendum.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Addendum})
        Me.GroupHeader_Addendum.HeightF = 23.00002!
        Me.GroupHeader_Addendum.Name = "GroupHeader_Addendum"
        Me.GroupHeader_Addendum.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Addendum
        '
        Me.XrSubreport_Addendum.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Addendum.Name = "XrSubreport_Addendum"
        Me.XrSubreport_Addendum.ReportSource = Me.Addendum1
        Me.XrSubreport_Addendum.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_StateInformation
        '
        Me.GroupHeader_StateInformation.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_StateInformation})
        Me.GroupHeader_StateInformation.HeightF = 23.0!
        Me.GroupHeader_StateInformation.Level = 4
        Me.GroupHeader_StateInformation.Name = "GroupHeader_StateInformation"
        Me.GroupHeader_StateInformation.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_StateInformation
        '
        Me.XrSubreport_StateInformation.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_StateInformation.Name = "XrSubreport_StateInformation"
        Me.XrSubreport_StateInformation.ReportSource = Me.English_Housing_Privacy_Principles1
        Me.XrSubreport_StateInformation.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_Budget
        '
        Me.GroupHeader_Budget.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader_Budget.HeightF = 23.0!
        Me.GroupHeader_Budget.Level = 8
        Me.GroupHeader_Budget.Name = "GroupHeader_Budget"
        Me.GroupHeader_Budget.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_ActionPlan_Addendum
        '
        Me.GroupHeader_ActionPlan_Addendum.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Addendum})
        Me.GroupHeader_ActionPlan_Addendum.HeightF = 23.0!
        Me.GroupHeader_ActionPlan_Addendum.Level = 5
        Me.GroupHeader_ActionPlan_Addendum.Name = "GroupHeader_ActionPlan_Addendum"
        Me.GroupHeader_ActionPlan_Addendum.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ActionPlan_Addendum
        '
        Me.XrSubreport_ActionPlan_Addendum.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Addendum.Name = "XrSubreport_ActionPlan_Addendum"
        Me.XrSubreport_ActionPlan_Addendum.ReportSource = Me.ActionPlanAddendum1
        Me.XrSubreport_ActionPlan_Addendum.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_ActionPlan_Goals
        '
        Me.GroupHeader_ActionPlan_Goals.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader_ActionPlan_Goals.HeightF = 23.0!
        Me.GroupHeader_ActionPlan_Goals.Level = 9
        Me.GroupHeader_ActionPlan_Goals.Name = "GroupHeader_ActionPlan_Goals"
        Me.GroupHeader_ActionPlan_Goals.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'GroupHeader_GeneralServices
        '
        Me.GroupHeader_GeneralServices.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralServices})
        Me.GroupHeader_GeneralServices.HeightF = 23.0!
        Me.GroupHeader_GeneralServices.Level = 3
        Me.GroupHeader_GeneralServices.Name = "GroupHeader_GeneralServices"
        Me.GroupHeader_GeneralServices.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_GeneralServices
        '
        Me.XrSubreport_GeneralServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralServices.Name = "XrSubreport_GeneralServices"
        Me.XrSubreport_GeneralServices.ReportSource = Me.GeneralServices1
        Me.XrSubreport_GeneralServices.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_FICO
        '
        Me.XrSubreport_FICO.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 24.99999!)
        Me.XrSubreport_FICO.Name = "XrSubreport_FICO"
        Me.XrSubreport_FICO.ReportSource = Me.EnglishFICO1
        Me.XrSubreport_FICO.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader_AgreementForServices
        '
        Me.GroupHeader_AgreementForServices.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader_AgreementForServices.HeightF = 23.0!
        Me.GroupHeader_AgreementForServices.Level = 2
        Me.GroupHeader_AgreementForServices.Name = "GroupHeader_AgreementForServices"
        Me.GroupHeader_AgreementForServices.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_HPF_PrivacyPolicy
        '
        Me.XrSubreport_HPF_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.00001589457!, 0.0!)
        Me.XrSubreport_HPF_PrivacyPolicy.Name = "XrSubreport_HPF_PrivacyPolicy"
        Me.XrSubreport_HPF_PrivacyPolicy.ReportSource = Me.HpF_PrivacyPolicy1
        Me.XrSubreport_HPF_PrivacyPolicy.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak2, Me.XrSubreport_FICO, Me.XrSubreport_HPF_PrivacyPolicy})
        Me.ReportFooter.HeightF = 47.99998!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrPageBreak2
        '
        Me.XrPageBreak2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
        Me.XrPageBreak2.Name = "XrPageBreak2"
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader_ClientRightsPolicy, Me.GroupHeader_BottomLine, Me.GroupHeader_NetWorth, Me.GroupHeader_Addendum, Me.GroupHeader_StateInformation, Me.GroupHeader_Budget, Me.GroupHeader_ActionPlan_Addendum, Me.GroupHeader_ActionPlan_Goals, Me.GroupHeader_GeneralServices, Me.GroupHeader_AgreementForServices, Me.ReportFooter})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportFooter, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_AgreementForServices, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_GeneralServices, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_ActionPlan_Goals, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_ActionPlan_Addendum, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_Budget, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_StateInformation, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_Addendum, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_NetWorth, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_BottomLine, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_ClientRightsPolicy, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Addendum1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.English_Housing_Privacy_Principles1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanAddendum1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EnglishFICO1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HpF_PrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents GroupHeader_ClientRightsPolicy As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Protected Friend WithEvents XrSubreport_CoverLetter As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrSubreport_ClientRightsPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ClientRightsPolicy1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ClientRightsPolicy
    Protected Friend WithEvents XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents NetWorth1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.NetWorth
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Budget
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ActionPlanGoals
    Friend WithEvents GroupHeader_BottomLine As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader_NetWorth As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents Cover_Letter1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Cover_Letter
    Friend WithEvents GroupHeader_Addendum As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader_StateInformation As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_ActionPlan_Addendum As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanAddendum1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.ActionPlanAddendum
    Friend WithEvents XrSubreport_Addendum As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Addendum1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.Addendum
    Protected Friend WithEvents XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.BottomLine
    Friend WithEvents GroupHeader_Budget As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_StateInformation As DevExpress.XtraReports.UI.XRSubreport
    Friend WithEvents GroupHeader_ActionPlan_Addendum As DevExpress.XtraReports.UI.GroupHeaderBand
    Private WithEvents English_Housing_Privacy_Principles1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.English_Housing_Privacy_Principles
    Friend WithEvents GroupHeader_ActionPlan_Goals As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader_GeneralServices As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_GeneralServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents GeneralServices1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.GeneralServices
    Protected Friend WithEvents XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents CoverPage1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.CoverPage
    Friend WithEvents XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
    Friend WithEvents XrSubreport_HPF_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents HpF_PrivacyPolicy1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.HPF_PrivacyPolicy
    Friend WithEvents GroupHeader_AgreementForServices As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents AgreementForServices1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.AgreementForServices
    Friend WithEvents XrSubreport_FICO As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents EnglishFICO1 As DebtPlus.Reports.Client.Packet.Housing.HPF.enUS.EnglishFICO
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrPageBreak2 As DevExpress.XtraReports.UI.XRPageBreak
End Class
