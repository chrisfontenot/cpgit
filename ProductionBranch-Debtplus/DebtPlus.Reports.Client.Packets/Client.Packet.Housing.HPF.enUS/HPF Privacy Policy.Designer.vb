﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HPF_PrivacyPolicy
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HPF_PrivacyPolicy))
        Me.GroupHeader_Page3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText3 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader_Page2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader_Page1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader_Page4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText4 = New DevExpress.XtraReports.UI.XRRichText()
        CType(Me.XrRichText3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 22.91667!
        '
        'GroupHeader_Page3
        '
        Me.GroupHeader_Page3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText3})
        Me.GroupHeader_Page3.HeightF = 23.0!
        Me.GroupHeader_Page3.Level = 1
        Me.GroupHeader_Page3.Name = "GroupHeader_Page3"
        Me.GroupHeader_Page3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText3
        '
        Me.XrRichText3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText3.Name = "XrRichText3"
        Me.XrRichText3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrRichText3.SerializableRtfString = resources.GetString("XrRichText3.SerializableRtfString")
        Me.XrRichText3.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        Me.XrRichText3.StylePriority.UsePadding = False
        '
        'GroupHeader_Page2
        '
        Me.GroupHeader_Page2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2})
        Me.GroupHeader_Page2.HeightF = 23.0!
        Me.GroupHeader_Page2.Level = 2
        Me.GroupHeader_Page2.Name = "GroupHeader_Page2"
        Me.GroupHeader_Page2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText2
        '
        Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText2.Name = "XrRichText2"
        Me.XrRichText2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
        Me.XrRichText2.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        Me.XrRichText2.StylePriority.UsePadding = False
        '
        'GroupHeader_Page1
        '
        Me.GroupHeader_Page1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
        Me.GroupHeader_Page1.HeightF = 23.0!
        Me.GroupHeader_Page1.Level = 3
        Me.GroupHeader_Page1.Name = "GroupHeader_Page1"
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        Me.XrRichText1.StylePriority.UsePadding = False
        '
        'GroupHeader_Page4
        '
        Me.GroupHeader_Page4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText4})
        Me.GroupHeader_Page4.HeightF = 23.0!
        Me.GroupHeader_Page4.Name = "GroupHeader_Page4"
        Me.GroupHeader_Page4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText4
        '
        Me.XrRichText4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText4.Name = "XrRichText4"
        Me.XrRichText4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrRichText4.SerializableRtfString = resources.GetString("XrRichText4.SerializableRtfString")
        Me.XrRichText4.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        Me.XrRichText4.StylePriority.UsePadding = False
        '
        'HPF_PrivacyPolicy
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.GroupHeader_Page3, Me.GroupHeader_Page2, Me.GroupHeader_Page1, Me.GroupHeader_Page4})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 23, 25)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader_Page4, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_Page1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_Page2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader_Page3, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMargin, 0)
        Me.Controls.SetChildIndex(Me.TopMargin, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me.XrRichText3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents GroupHeader_Page3 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText3 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader_Page2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader_Page1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader_Page4 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText4 As DevExpress.XtraReports.UI.XRRichText
End Class
