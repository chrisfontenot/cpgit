﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Budget
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Budget))
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel_total_difference = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_total_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_total_client_amount = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_client_amount = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_detail = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_difference = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
        Me.ParameterBudget = New DevExpress.XtraReports.Parameters.Parameter()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel2, Me.XrLabel3, Me.XrLabel4})
        Me.PageHeader.HeightF = 32.25009!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_difference, Me.XrLabel_detail, Me.XrLabel_client_amount, Me.XrLabel_suggested_amount})
        Me.Detail.HeightF = 17.0!
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_difference, Me.XrLabel_total_suggested_amount, Me.XrLine1, Me.XrLabel7, Me.XrLabel_total_client_amount})
        Me.ReportFooter.HeightF = 27.00001!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLabel_total_difference
        '
        Me.XrLabel_total_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference")})
        Me.XrLabel_total_difference.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_difference.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 10.00001!)
        Me.XrLabel_total_difference.Name = "XrLabel_total_difference"
        Me.XrLabel_total_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_difference.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_total_difference.StylePriority.UseFont = False
        Me.XrLabel_total_difference.StylePriority.UseTextAlignment = False
        XrSummary1.FormatString = "{0:c}"
        XrSummary1.IgnoreNullValues = True
        XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrLabel_total_difference.Summary = XrSummary1
        Me.XrLabel_total_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_total_suggested_amount
        '
        Me.XrLabel_total_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount")})
        Me.XrLabel_total_suggested_amount.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 10.00001!)
        Me.XrLabel_total_suggested_amount.Name = "XrLabel_total_suggested_amount"
        Me.XrLabel_total_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_total_suggested_amount.StylePriority.UseFont = False
        Me.XrLabel_total_suggested_amount.StylePriority.UseTextAlignment = False
        XrSummary2.FormatString = "{0:c}"
        XrSummary2.IgnoreNullValues = True
        XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrLabel_total_suggested_amount.Summary = XrSummary2
        Me.XrLabel_total_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLine1
        '
        Me.XrLine1.ForeColor = System.Drawing.Color.Teal
        Me.XrLine1.LineWidth = 2
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(659.75!, 9.000015!)
        Me.XrLine1.StylePriority.UseForeColor = False
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "TOTAL MENSUAL"
        '
        'XrLabel_total_client_amount
        '
        Me.XrLabel_total_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount")})
        Me.XrLabel_total_client_amount.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
        Me.XrLabel_total_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 10.00001!)
        Me.XrLabel_total_client_amount.Name = "XrLabel_total_client_amount"
        Me.XrLabel_total_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_total_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel_total_client_amount.StylePriority.UseFont = False
        Me.XrLabel_total_client_amount.StylePriority.UseTextAlignment = False
        XrSummary3.FormatString = "{0:c}"
        XrSummary3.IgnoreNullValues = True
        XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
        Me.XrLabel_total_client_amount.Summary = XrSummary3
        Me.XrLabel_total_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 10.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.StylePriority.UseTextAlignment = False
        Me.XrLabel4.Text = "SUGIERE"
        Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 10.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.StylePriority.UseTextAlignment = False
        Me.XrLabel3.Text = "CLIENTE"
        Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel2
        '
        Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "CATEGORÍA"
        '
        'XrLabel_suggested_amount
        '
        Me.XrLabel_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
        Me.XrLabel_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 0.0!)
        Me.XrLabel_suggested_amount.Name = "XrLabel_suggested_amount"
        Me.XrLabel_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_suggested_amount.Scripts.OnBeforePrint = "XrLabel_suggested_amount_BeforePrint"
        Me.XrLabel_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_suggested_amount.StylePriority.UseTextAlignment = False
        Me.XrLabel_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_client_amount
        '
        Me.XrLabel_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
        Me.XrLabel_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 0.0!)
        Me.XrLabel_client_amount.Name = "XrLabel_client_amount"
        Me.XrLabel_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client_amount.Scripts.OnBeforePrint = "XrLabel_client_amount_BeforePrint"
        Me.XrLabel_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
        Me.XrLabel_client_amount.StylePriority.UseTextAlignment = False
        Me.XrLabel_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_detail
        '
        Me.XrLabel_detail.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
        Me.XrLabel_detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_detail.Name = "XrLabel_detail"
        Me.XrLabel_detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_detail.Scripts.OnBeforePrint = "XrLabel_detail_BeforePrint"
        Me.XrLabel_detail.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
        '
        'XrLabel1
        '
        Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 10.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "DIFERENCIA"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_difference
        '
        Me.XrLabel_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
        Me.XrLabel_difference.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 0.0!)
        Me.XrLabel_difference.Name = "XrLabel_difference"
        Me.XrLabel_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_difference.Scripts.OnBeforePrint = "XrLabel_difference_BeforePrint"
        Me.XrLabel_difference.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
        Me.XrLabel_difference.StylePriority.UseTextAlignment = False
        Me.XrLabel_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel_client})
        Me.PageFooter.HeightF = 26.33338!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.Visible = False
        '
        'XrLabel9
        '
        Me.XrLabel9.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 12.70838!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 13.625!)
        Me.XrLabel9.StylePriority.UseFont = False
        Me.XrLabel9.StylePriority.UseTextAlignment = False
        Me.XrLabel9.Text = "Rev. 02/2011"
        Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_client
        '
        Me.XrLabel_client.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(587.5!, 0.0!)
        Me.XrLabel_client.Name = "XrLabel_client"
        Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
        Me.XrLabel_client.SizeF = New System.Drawing.SizeF(160.4166!, 26.33337!)
        Me.XrLabel_client.StylePriority.UseFont = False
        Me.XrLabel_client.StylePriority.UseTextAlignment = False
        Me.XrLabel_client.Text = "Client: 0000000"
        Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'ParameterBudget
        '
        Me.ParameterBudget.Description = "Budget ID"
        Me.ParameterBudget.Name = "ParameterBudget"
        Me.ParameterBudget.Type = GetType(Integer)
        Me.ParameterBudget.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8})
        Me.ReportHeader.HeightF = 46.875!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold)
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(752.1166!, 36.41666!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "INFORMACION DEL PRESUPUESTO"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'Budget
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportFooter, Me.PageFooter, Me.ReportHeader})
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterBudget})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "BudgetSubReport_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportFooter, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMargin, 0)
        Me.Controls.SetChildIndex(Me.TopMargin, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Protected Friend WithEvents XrLabel_total_suggested_amount As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_total_client_amount As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_detail As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_client_amount As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_suggested_amount As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_difference As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_total_difference As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ParameterBudget As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Protected Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
End Class
