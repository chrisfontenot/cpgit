<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText8 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText7 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader3 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText5 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText2 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText4 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText3 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader8 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader9 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader10 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader14 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader16 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader17 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader18 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.XrSubreport_GeneralServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.CoverPage1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.CoverPage()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BottomLine1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.BottomLine()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.NetWorth1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.NetWorth()
        Me.XrSubreport_DMPDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.DmpDebts1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.DMPDebts()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Budget1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.Budget()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanGoals1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.ActionPlanGoals()
        Me.XrSubreport_ActionPlan_Items = New DevExpress.XtraReports.UI.XRSubreport()
        Me.ActionPlanItems1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.ActionPlanItems()
        Me.XrSubreport_AgreementForServicesMX_WA = New DevExpress.XtraReports.UI.XRSubreport()
        Me.SpanishAgreementForServices1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishAgreementForServices()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.SpanishPrivacyPolicy1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishPrivacyPolicy()
        Me.XrSubreport_CoverPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.Cover_Letter1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.Cover_Letter()
        Me.XrSubreport_BlankPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.BlankPage1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.BlankPage()
        Me.SpanishGeneralServices1 = New DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishGeneralServices()
        CType(Me.XrRichText8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpanishAgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpanishPrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpanishGeneralServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Cover})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText8})
        Me.GroupHeader1.HeightF = 115.7083!
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText8
        '
        Me.XrRichText8.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText8.Name = "XrRichText8"
        Me.XrRichText8.SerializableRtfString = resources.GetString("XrRichText8.SerializableRtfString")
        Me.XrRichText8.SizeF = New System.Drawing.SizeF(798.0!, 115.7083!)
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText7})
        Me.GroupHeader2.HeightF = 103.3784!
        Me.GroupHeader2.Level = 1
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText7
        '
        Me.XrRichText7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText7.Name = "XrRichText7"
        Me.XrRichText7.SerializableRtfString = resources.GetString("XrRichText7.SerializableRtfString")
        Me.XrRichText7.SizeF = New System.Drawing.SizeF(798.0!, 103.3784!)
        '
        'GroupHeader3
        '
        Me.GroupHeader3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralServices})
        Me.GroupHeader3.HeightF = 23.0!
        Me.GroupHeader3.Level = 2
        Me.GroupHeader3.Name = "GroupHeader3"
        Me.GroupHeader3.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader4
        '
        Me.GroupHeader4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText5})
        Me.GroupHeader4.HeightF = 220.9166!
        Me.GroupHeader4.Level = 3
        Me.GroupHeader4.Name = "GroupHeader4"
        Me.GroupHeader4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText5
        '
        Me.XrRichText5.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText5.Name = "XrRichText5"
        Me.XrRichText5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrRichText5.SerializableRtfString = resources.GetString("XrRichText5.SerializableRtfString")
        Me.XrRichText5.SizeF = New System.Drawing.SizeF(796.8748!, 220.9166!)
        Me.XrRichText5.StylePriority.UsePadding = False
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText2})
        Me.GroupHeader5.HeightF = 139.6666!
        Me.GroupHeader5.Level = 4
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText2
        '
        Me.XrRichText2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText2.Name = "XrRichText2"
        Me.XrRichText2.SerializableRtfString = resources.GetString("XrRichText2.SerializableRtfString")
        Me.XrRichText2.SizeF = New System.Drawing.SizeF(798.0!, 139.6666!)
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText4})
        Me.GroupHeader6.HeightF = 220.9166!
        Me.GroupHeader6.Level = 5
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText4
        '
        Me.XrRichText4.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText4.Name = "XrRichText4"
        Me.XrRichText4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrRichText4.SerializableRtfString = resources.GetString("XrRichText4.SerializableRtfString")
        Me.XrRichText4.SizeF = New System.Drawing.SizeF(796.8748!, 220.9166!)
        Me.XrRichText4.StylePriority.UsePadding = False
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText3})
        Me.GroupHeader7.HeightF = 876.2083!
        Me.GroupHeader7.Level = 6
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText3
        '
        Me.XrRichText3.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText3.Name = "XrRichText3"
        Me.XrRichText3.SerializableRtfString = resources.GetString("XrRichText3.SerializableRtfString")
        Me.XrRichText3.SizeF = New System.Drawing.SizeF(795.8749!, 876.2083!)
        '
        'GroupHeader8
        '
        Me.GroupHeader8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader8.HeightF = 23.0!
        Me.GroupHeader8.Level = 7
        Me.GroupHeader8.Name = "GroupHeader8"
        Me.GroupHeader8.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader9
        '
        Me.GroupHeader9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader9.HeightF = 23.0!
        Me.GroupHeader9.Level = 8
        Me.GroupHeader9.Name = "GroupHeader9"
        Me.GroupHeader9.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader10
        '
        Me.GroupHeader10.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DMPDebts})
        Me.GroupHeader10.HeightF = 23.0!
        Me.GroupHeader10.Level = 9
        Me.GroupHeader10.Name = "GroupHeader10"
        Me.GroupHeader10.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader11.HeightF = 23.0!
        Me.GroupHeader11.Level = 10
        Me.GroupHeader11.Name = "GroupHeader11"
        Me.GroupHeader11.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Level = 11
        Me.GroupHeader12.Name = "GroupHeader12"
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Items})
        Me.GroupHeader13.HeightF = 23.0!
        Me.GroupHeader13.Level = 12
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader14
        '
        Me.GroupHeader14.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrRichText1})
        Me.GroupHeader14.HeightF = 363.625!
        Me.GroupHeader14.Level = 13
        Me.GroupHeader14.Name = "GroupHeader14"
        Me.GroupHeader14.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100.0!)
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(795.8749!, 363.625!)
        Me.XrRichText1.StylePriority.UsePadding = False
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServicesMX_WA})
        Me.GroupHeader15.HeightF = 27.08333!
        Me.GroupHeader15.Level = 14
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader16
        '
        Me.GroupHeader16.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader16.HeightF = 25.0!
        Me.GroupHeader16.Level = 15
        Me.GroupHeader16.Name = "GroupHeader16"
        Me.GroupHeader16.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader17
        '
        Me.GroupHeader17.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_CoverPage})
        Me.GroupHeader17.HeightF = 23.0!
        Me.GroupHeader17.Level = 16
        Me.GroupHeader17.Name = "GroupHeader17"
        Me.GroupHeader17.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'GroupHeader18
        '
        Me.GroupHeader18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BlankPage})
        Me.GroupHeader18.HeightF = 23.0!
        Me.GroupHeader18.Level = 17
        Me.GroupHeader18.Name = "GroupHeader18"
        Me.GroupHeader18.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_GeneralServices
        '
        Me.XrSubreport_GeneralServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralServices.Name = "XrSubreport_GeneralServices"
        Me.XrSubreport_GeneralServices.ReportSource = Me.SpanishGeneralServices1
        Me.XrSubreport_GeneralServices.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(795.8749!, 23.0!)
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_DMPDebts
        '
        Me.XrSubreport_DMPDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_DMPDebts.Name = "XrSubreport_DMPDebts"
        Me.XrSubreport_DMPDebts.ReportSource = Me.DmpDebts1
        Me.XrSubreport_DMPDebts.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_ActionPlan_Items
        '
        Me.XrSubreport_ActionPlan_Items.CanShrink = True
        Me.XrSubreport_ActionPlan_Items.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Items.Name = "XrSubreport_ActionPlan_Items"
        Me.XrSubreport_ActionPlan_Items.ReportSource = Me.ActionPlanItems1
        Me.XrSubreport_ActionPlan_Items.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_AgreementForServicesMX_WA
        '
        Me.XrSubreport_AgreementForServicesMX_WA.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AgreementForServicesMX_WA.Name = "XrSubreport_AgreementForServicesMX_WA"
        Me.XrSubreport_AgreementForServicesMX_WA.ReportSource = Me.SpanishAgreementForServices1
        Me.XrSubreport_AgreementForServicesMX_WA.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.SpanishPrivacyPolicy1
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_CoverPage
        '
        Me.XrSubreport_CoverPage.LocationFloat = New DevExpress.Utils.PointFloat(3.125191!, 0.0!)
        Me.XrSubreport_CoverPage.Name = "XrSubreport_CoverPage"
        Me.XrSubreport_CoverPage.ReportSource = Me.Cover_Letter1
        Me.XrSubreport_CoverPage.SizeF = New System.Drawing.SizeF(796.8748!, 23.0!)
        '
        'XrSubreport_BlankPage
        '
        Me.XrSubreport_BlankPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BlankPage.Name = "XrSubreport_BlankPage"
        Me.XrSubreport_BlankPage.ReportSource = Me.BlankPage1
        Me.XrSubreport_BlankPage.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter, Me.ReportHeader, Me.GroupHeader1, Me.GroupHeader2, Me.GroupHeader3, Me.GroupHeader4, Me.GroupHeader5, Me.GroupHeader6, Me.GroupHeader7, Me.GroupHeader8, Me.GroupHeader9, Me.GroupHeader10, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader13, Me.GroupHeader14, Me.GroupHeader15, Me.GroupHeader16, Me.GroupHeader17, Me.GroupHeader18})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.GroupHeader18, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader17, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader16, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader14, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader10, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader9, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader8, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader4, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader3, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.XrRichText8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionPlanItems1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpanishAgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpanishPrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BlankPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpanishGeneralServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Protected Friend WithEvents ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Protected Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Protected Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Protected Friend WithEvents XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrSubreport_CoverPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Cover_Letter1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.Cover_Letter
    Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText8 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText7 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader3 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader4 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText5 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText2 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText4 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText3 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader8 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BottomLine1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.BottomLine
    Friend WithEvents GroupHeader9 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents NetWorth1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.NetWorth
    Friend WithEvents GroupHeader10 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_DMPDebts As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents DmpDebts1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.DMPDebts
    Friend WithEvents GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents Budget1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.Budget
    Friend WithEvents GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanGoals1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.ActionPlanGoals
    Friend WithEvents GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Protected Friend WithEvents XrSubreport_ActionPlan_Items As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents ActionPlanItems1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.ActionPlanItems
    Friend WithEvents GroupHeader14 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_AgreementForServicesMX_WA As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents SpanishAgreementForServices1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishAgreementForServices
    Friend WithEvents GroupHeader16 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents SpanishPrivacyPolicy1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishPrivacyPolicy
    Protected Friend WithEvents XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents CoverPage1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.CoverPage
    Friend WithEvents GroupHeader17 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents GroupHeader18 As DevExpress.XtraReports.UI.GroupHeaderBand
    Friend WithEvents XrSubreport_BlankPage As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents BlankPage1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.BlankPage
    Friend WithEvents XrSubreport_GeneralServices As DevExpress.XtraReports.UI.XRSubreport
    Private WithEvents SpanishGeneralServices1 As DebtPlus.Reports.Client.Packet.Housing.WA.esMX.SpanishGeneralServices
End Class
