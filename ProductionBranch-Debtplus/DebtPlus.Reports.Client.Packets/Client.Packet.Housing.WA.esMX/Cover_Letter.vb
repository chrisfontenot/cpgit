#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Public Class Cover_Letter

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        'AddHandler BeforePrint, AddressOf CoverPage_BeforePrint
    End Sub

    Private Sub CoverPage_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Dim LocalRpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = CType(LocalRpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim Client As System.Int32 = Convert.ToInt32(MasterRpt.Parameters("ParameterClient").Value, System.Globalization.CultureInfo.InvariantCulture)
        Dim ds As System.Data.DataSet = CType(MasterReport.DataSource, System.Data.DataView).Table.DataSet

        Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
            With cmd
                .Connection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
                .CommandText = "SELECT * FROM view_client_address WHERE [client]=@Client"
                .CommandType = System.Data.CommandType.Text
                .Parameters.Add("@Client", System.Data.SqlDbType.Int).Value = Client
            End With

            Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "view_client_address")
            End Using
        End Using

        '-- Set the report source
        With LocalRpt
            .DataSource = ds.Tables("view_client_address").DefaultView

            '-- Set the data source for any calculated fields
            For Each calcField As DevExpress.XtraReports.UI.CalculatedField In LocalRpt.CalculatedFields
                With calcField
                    .Assign(LocalRpt.DataSource, LocalRpt.DataMember)
                End With
            Next
        End With
    End Sub
End Class
