#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class ActionPlanItems

    Public Sub New()
        MyBase.New()
        ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()

        'AddHandler BeforePrint, AddressOf ActionPlanItemsSubReport_BeforePrint
        'AddHandler XrLabel_checked.BeforePrint, AddressOf XrLabel_checked_BeforePrint
        'AddHandler XrLabel_group_name.BeforePrint, AddressOf XrLabel_group_name_BeforePrint
    End Sub

    Private Function TopLevelReport(ByVal rpt As DevExpress.XtraReports.UI.XtraReport) As DevExpress.XtraReports.UI.XtraReport
        Dim Parent As DevExpress.XtraReports.UI.XtraReport = rpt
        Do While Parent IsNot Nothing
            Parent = Parent.MasterReport
            If Parent.MasterReport Is Nothing OrElse Object.Equals(Parent.MasterReport, Parent) Then Exit Do
        Loop
        Return Parent
    End Function

    Private Sub ActionPlanItemsSubReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        Const TableName As String = "rpt_PrintClient_ActionPlan_ByPlan"
        Dim Rpt As DevExpress.XtraReports.UI.XtraReport = CType(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim MasterRpt As DevExpress.XtraReports.UI.XtraReport = TopLevelReport(Rpt) ' CType(Rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim sqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString)
        Dim ds As System.Data.DataSet = CType(MasterRpt.DataSource, System.Data.DataView).Table.DataSet
        Dim tbl As System.Data.DataTable = ds.Tables(TableName)

        If tbl Is Nothing Then
            Try
                cn.Open()
                Dim ActionPlan As Integer = CType(Rpt.Parameters("ParameterActionPlan").Value, Integer)
                If ActionPlan <= 0 Then
                    Dim Client As Integer = CType(MasterRpt.Parameters("ParameterClient").Value, Integer)

                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT dbo.map_client_to_action_plan(@client)"
                            .CommandType = System.Data.CommandType.Text
                            .Parameters.Add("@client", System.Data.SqlDbType.Int).Value = Client
                            Dim obj As Object = .ExecuteScalar
                            ActionPlan = DebtPlus.Utils.Nulls.DInt(obj)
                        End With
                    End Using
                End If

                If ActionPlan > 0 Then
                    Using cmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "rpt_PrintClient_ActionPlan_ByPlan"
                            .CommandType = System.Data.CommandType.StoredProcedure
                            .CommandTimeout = 0
                            .Parameters.Add("@ActionPlan", System.Data.SqlDbType.Int).Value = ActionPlan
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    tbl = ds.Tables(TableName)
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading report information")

            Finally
                If cn IsNot Nothing Then
                    If cn.State <> System.Data.ConnectionState.Closed Then cn.Close()
                    cn.Dispose()
                End If
            End Try
        End If

        If tbl IsNot Nothing Then
            Rpt.DataSource = New System.Data.DataView(tbl, String.Empty, "item_group, description", System.Data.DataViewRowState.CurrentRows)
        End If
    End Sub

    Private Sub XrLabel_group_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Select Case DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("item_group"))
                Case 1 : .Text = "Action Items"
                Case 2 : .Text = "Items to Increase Income"
                Case 3 : .Text = "Items to Reduce Expenses"
                Case 4 : .Text = "Items Required for DMP"
            End Select
        End With
    End Sub

    Private Sub XrLabel_checked_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
        With CType(sender, DevExpress.XtraReports.UI.XRLabel)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.Report, DevExpress.XtraReports.UI.XtraReport)
            Dim Checked As Boolean = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("checked")) <> 0
            If Checked Then
                .Text = New String(Convert.ToChar(252), 1)
            Else
                .Text = New String(Convert.ToChar(111), 1)
            End If
        End With
    End Sub
End Class
