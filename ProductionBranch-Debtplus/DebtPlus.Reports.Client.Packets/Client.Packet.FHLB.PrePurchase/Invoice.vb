﻿Public Class Invoice
    Public Sub New()
        MyBase.New()
        InitializeComponent()
        ' RegisterHandlers()
    End Sub

    Private Sub RegisterHandlers()
        AddHandler BeforePrint, AddressOf Invoice_BeforePrint
        AddHandler XrPanel_Detail.BeforePrint, AddressOf XrPanel_Detail_BeforePrint
    End Sub

    ''' <summary>
    ''' Prepare the report to be printed
    ''' </summary>
    ''' <param name="sender">Pointer to the current report</param>
    ''' <param name="e">Printing event arguments</param>
    ''' <remarks></remarks>
    Private Sub Invoice_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)

        ' Find the information for the report
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim masterRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim clientID As Int32 = Convert.ToInt32(masterRpt.Parameters("ParameterClient").Value)

        Try
            Using ds As New System.Data.DataSet()
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    ' Read the information that goes into the header area
                    ReadClientData(rpt, cn, ds, clientID)
                    ReadAppointmentData(rpt, cn, ds, clientID)

                    ' Set the report data-source to the transaction information
                    rpt.DataSource = getCreditorData(cn, ds, clientID)
                End Using
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice report data")
        End Try
    End Sub

    ''' <summary>
    ''' Read the client information for the current report
    ''' </summary>
    ''' <param name="cn">Current database connection object</param>
    ''' <param name="ds">Current dataset object</param>
    ''' <param name="clientID">Current client ID number</param>
    ''' <remarks></remarks>
    Private Sub ReadClientData(rpt As DevExpress.XtraReports.UI.XtraReport, cn As System.Data.SqlClient.SqlConnection, ds As System.Data.DataSet, clientID As Int32)

        ' Read the client information from the view if needed
        Dim tbl As System.Data.DataTable = ds.Tables("view_client_address")
        If tbl Is Nothing Then
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT [name],[addr1],[addr2],[addr3] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "view_client_address")
                    tbl = ds.Tables("view_client_address")
                End Using
            End Using
        End If

        ' There should be a view table at this point. If not then we can't retrieve the information.
        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            Dim row As System.Data.DataRow = tbl.Rows(0)

            ' Generate the client address block as a merge of the address lines
            Dim sb As New System.Text.StringBuilder()
            For Each obj As Object In New Object() {row("addr1"), row("addr2"), row("addr3")}
                Dim str As String = DebtPlus.Utils.Nulls.v_String(obj)
                If Not String.IsNullOrWhiteSpace(str) Then
                    sb.Append(System.Environment.NewLine)
                    sb.Append(str)
                End If
            Next

            ' Toss the leading cr/lf sequence
            If sb.Length > 0 Then
                sb.Remove(0, 2)
            End If

            ' The resulting address record is put into the proper cell
            rpt.FindControl("XrTableCell_client_address", True).Text = sb.ToString()
            rpt.FindControl("XrTableCell_Footer_Address", True).Text = sb.ToString()

            rpt.FindControl("XrTableCell_client_name", True).Text = If(DebtPlus.Utils.Nulls.v_String(row("name")), String.Empty)
            rpt.FindControl("XrTableCell_Footer_Name", True).Text = If(DebtPlus.Utils.Nulls.v_String(row("name")), String.Empty)
        End If

        ' Insert the client ID and name into the proper locations
        rpt.FindControl("XrTableCell_Client", True).Text = DebtPlus.Utils.Format.Client.FormatClientID(clientID)
        rpt.FindControl("XrTableCell_Footer_Client", True).Text = DebtPlus.Utils.Format.Client.FormatClientID(clientID)
    End Sub

    ''' <summary>
    ''' Read the header information for the current appointment data
    ''' </summary>
    ''' <param name="cn">Current database connection object</param>
    ''' <param name="ds">Current dataset object</param>
    ''' <param name="clientID">Current client ID number</param>
    ''' <remarks></remarks>
    Private Sub ReadAppointmentData(rpt As DevExpress.XtraReports.UI.XtraReport, cn As System.Data.SqlClient.SqlConnection, ds As System.Data.DataSet, clientID As Int32)
        Dim tbl As System.Data.DataTable = ds.Tables("client_appointments")
        If tbl Is Nothing Then
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 ca.start_time, ca.partner FROM client_appointments ca WITH (NOLOCK) WHERE ca.client = @client AND ltrim(rtrim(isnull(ca.partner,''))) <> '' AND ca.status in ('K','W') AND ca.office IS NOT NULL ORDER BY ca.start_time DESC"
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
                cmd.CommandTimeout = 0
                cmd.CommandType = System.Data.CommandType.Text

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "client_appointments")
                    tbl = ds.Tables("client_appointments")
                End Using
            End Using
        End If

        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            Dim row As System.Data.DataRow = tbl.Rows(0)

            ' Set the client appointment information
            rpt.FindControl("XrTableCell_Date", True).Text = System.String.Format("{0:M/d/yy}", Convert.ToDateTime(row("start_time")))
            rpt.FindControl("XrTableCell_appointment_date", True).Text = System.String.Format("{0:MMMM d, yyyy}", Convert.ToDateTime(row("start_time")))
        End If
    End Sub

    Private Class dataItems
        ''' <summary>
        ''' Initialize the new class
        ''' </summary>
        Public Sub New()
        End Sub

        Private privateBackgroundColor As System.Drawing.Color

        ''' <summary>
        ''' Background color for the current row
        ''' </summary>
        ''' <value></value>
        ''' <returns>The color object to paint the detail row</returns>
        ''' <remarks></remarks>
        Public Property BackgroundColor As System.Drawing.Color
            Get
                Return privateBackgroundColor
            End Get
            Private Set(value As System.Drawing.Color)
                privateBackgroundColor = value
            End Set
        End Property

        ''' <summary>
        ''' Description for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Description As String = Nothing

        ''' <summary>
        ''' Item number for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ItemNo As String = Nothing

        ''' <summary>
        ''' Quantity for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Quantity As System.Nullable(Of Int32) = Nothing

        ''' <summary>
        ''' Discount amount for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Discount As System.Nullable(Of Decimal) = Nothing

        ''' <summary>
        ''' Unit price for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property UnitPrice As System.Nullable(Of Decimal) = Nothing

        ''' <summary>
        ''' Line total for the detail line
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property LineTotal As System.Nullable(Of Decimal) = Nothing

        ''' <summary>
        ''' Initialize the new class
        ''' </summary>
        ''' <param name="itemNumber">The current row number</param>
        ''' <remarks></remarks>
        Public Sub New(itemNumber As Int32)
            BackgroundColor = If((itemNumber Mod 2) = 0, Color.LightBlue, Color.Transparent)
        End Sub

        ''' <summary>
        ''' Initialize the new class
        ''' </summary>
        ''' <param name="itemNumber">The current row number</param>
        ''' <param name="row">A pointer to the data row with the detail information</param>
        ''' <remarks></remarks>
        Public Sub New(itemNumber As Int32, row As System.Data.DataRow)
            BackgroundColor = If((itemNumber Mod 2) = 0, Color.LightBlue, Color.Transparent)
            Description = DebtPlus.Utils.Nulls.v_String(row("creditor_name"))
            UnitPrice = DebtPlus.Utils.Nulls.v_Decimal(row("orig_balance"))
            ItemNo = DebtPlus.Utils.Nulls.v_String(row("creditor"))
            Quantity = 1
            Discount = DebtPlus.Utils.Nulls.v_String(row("payments"))
            LineTotal = UnitPrice
        End Sub
    End Class

    ''' <summary>
    ''' Retrieve the rows for the detail information
    ''' </summary>
    ''' <param name="cn">Current database connection object</param>
    ''' <param name="ds">Current dataset object</param>
    ''' <param name="clientID">Current client ID number</param>
    ''' <returns>A collection of records to be used for the detail information</returns>
    ''' <remarks></remarks>
    Private Function getCreditorData(cn As System.Data.SqlClient.SqlConnection, ds As System.Data.DataSet, clientID As Int32)
        Dim tbl As System.Data.DataTable = ds.Tables("invoice_details")

        ' If there is no table then read the information from the database.
        If tbl Is Nothing Then
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 bal.orig_balance as orig_balance, bal.total_payments as payments, cc.creditor as creditor, cr.creditor_name as creditor_name FROM client_creditor cc WITH (NOLOCK) INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance INNER JOIN creditors cr on cr.creditor = cc.creditor WHERE cc.client = @clientID AND cc.creditor = @creditor AND cc.reassigned_debt = 0"
                cmd.CommandType = System.Data.CommandType.Text
                cmd.CommandTimeout = 0
                cmd.Parameters.Add("@clientID", System.Data.SqlDbType.Int).Value = clientID
                cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = "X0001"

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "invoice_details")
                    tbl = ds.Tables("invoice_details")
                End Using
            End Using
        End If

        ' If there is a table then generate the 10 lines of detail information.
        ' Only the first line has any details. The other nine are blank.
        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            Dim colItems As New System.Collections.ArrayList()

            Dim row As System.Data.DataRow = tbl.Rows(0)
            Dim rowNumber As Int32 = 0
            colItems.Add(New dataItems(rowNumber, row))
            rowNumber += 1

            While rowNumber < 11
                colItems.Add(New dataItems(rowNumber))
                rowNumber += 1
            End While

            Return colItems
        End If

        Return Nothing
    End Function

    Private Sub XrPanel_Detail_BeforePrint(sender As Object, e As System.Drawing.Printing.PrintEventArgs)
        Dim panel As DevExpress.XtraReports.UI.XRPanel = TryCast(sender, DevExpress.XtraReports.UI.XRPanel)
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(panel.Report, DevExpress.XtraReports.UI.XtraReport)
        Dim dataItem As dataItems = TryCast(rpt.GetCurrentRow, dataItems)
        panel.BackColor = dataItem.BackgroundColor
    End Sub
End Class