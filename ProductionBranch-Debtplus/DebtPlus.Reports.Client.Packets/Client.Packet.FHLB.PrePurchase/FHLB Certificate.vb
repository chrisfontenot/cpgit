#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class FHLBCertificate

    Public Sub New()
        MyBase.New()
        MyClass.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
        InitializeComponent()
    End Sub

    Private Sub FHLBCertificate_BeforePrint(sender As System.Object, e As System.Drawing.Printing.PrintEventArgs)

        ' Find the information for the report
        Dim rpt As DevExpress.XtraReports.UI.XtraReport = TryCast(sender, DevExpress.XtraReports.UI.XtraReport)
        Dim masterRpt As DevExpress.XtraReports.UI.XtraReport = TryCast(rpt.MasterReport, DevExpress.XtraReports.UI.XtraReport)
        Dim clientID As Int32 = Convert.ToInt32(masterRpt.Parameters("ParameterClient").Value)

        Try
            Using ds As New System.Data.DataSet()
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    ' Read the information that goes into the header area
                    ReadClientData(rpt, cn, ds, clientID)
                    ReadAppointmentData(rpt, cn, ds, clientID)
                End Using
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice report data")
        End Try
    End Sub

    ''' <summary>
    ''' Read the client information for the current report
    ''' </summary>
    ''' <param name="cn">Current database connection object</param>
    ''' <param name="ds">Current dataset object</param>
    ''' <param name="clientID">Current client ID number</param>
    ''' <remarks></remarks>
    Private Sub ReadClientData(rpt As DevExpress.XtraReports.UI.XtraReport, cn As System.Data.SqlClient.SqlConnection, ds As System.Data.DataSet, clientID As Int32)

        ' Read the client information from the view if needed
        Using cmd As New System.Data.SqlClient.SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = "SELECT [salutation] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client"
            cmd.CommandType = System.Data.CommandType.Text
            cmd.CommandTimeout = 0
            cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
            Dim obj = cmd.ExecuteScalar()
            Dim strName As String = DebtPlus.Utils.Nulls.v_String(obj)

            If Not String.IsNullOrEmpty(strName) Then
                rpt.FindControl("XrLabel_ClientName", True).Text = strName
            End If
        End Using
    End Sub

    ''' <summary>
    ''' Read the header information for the current appointment data
    ''' </summary>
    ''' <param name="cn">Current database connection object</param>
    ''' <param name="ds">Current dataset object</param>
    ''' <param name="clientID">Current client ID number</param>
    ''' <remarks></remarks>
    Private Sub ReadAppointmentData(rpt As DevExpress.XtraReports.UI.XtraReport, cn As System.Data.SqlClient.SqlConnection, ds As System.Data.DataSet, clientID As Int32)
        Dim tbl As System.Data.DataTable = ds.Tables("client_appointments")
        If tbl Is Nothing Then
            Using cmd As New System.Data.SqlClient.SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT TOP 1 ca.client_appointment, ca.counselor, ca.start_time, ca.partner FROM client_appointments ca WITH (NOLOCK) WHERE ca.client = @client AND ltrim(rtrim(isnull(ca.partner,''))) <> '' AND ca.status in ('K','W') AND ca.office IS NOT NULL ORDER BY ca.start_time DESC"
                cmd.Parameters.Add("@client", System.Data.SqlDbType.Int).Value = clientID
                cmd.CommandTimeout = 0
                cmd.CommandType = System.Data.CommandType.Text

                Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "client_appointments")
                    tbl = ds.Tables("client_appointments")
                End Using
            End Using
        End If

        If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
            Dim row As System.Data.DataRow = tbl.Rows(0)
            Dim counselorID As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(row("counselor"))

            If counselorID.HasValue Then
                ' Find the name of the counselor
                Using cmd As New System.Data.SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandTimeout = 0
                    cmd.CommandType = System.Data.CommandType.Text
                    cmd.CommandText = "SELECT dbo.format_normal_name(default, cox.first, default, cox.last, default) as counselor_name FROM counselors co inner join names cox on co.nameid = cox.name WHERE co.counselor = @counselor"
                    cmd.Parameters.Add("@counselor", System.Data.SqlDbType.Int).Value = counselorID.Value
                    Dim objName As Object = cmd.ExecuteScalar()
                    Dim counselor_name As String = DebtPlus.Utils.Nulls.v_String(objName)

                    If Not String.IsNullOrEmpty(counselor_name) Then
                        rpt.FindControl("XrLabel_CounselorName", True).Text = counselor_name
                    End If
                End Using
            End If

            rpt.FindControl("XrLabel_Id", True).Text = If(DebtPlus.Utils.Nulls.v_String(row("partner")), String.Empty)
            rpt.FindControl("XrLabel_Date", True).Text = System.String.Format("{0:MMM d, yyyy}", Convert.ToDateTime(row("start_time")))
        End If
    End Sub
End Class
