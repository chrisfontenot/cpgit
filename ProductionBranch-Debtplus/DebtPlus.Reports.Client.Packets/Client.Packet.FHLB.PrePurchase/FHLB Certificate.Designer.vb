﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FHLBCertificate
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FHLBCertificate))
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Id = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_CounselorName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Id_String = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Counselor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ProgramstartDate = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrCrossBandBox1 = New DevExpress.XtraReports.UI.XRCrossBandBox()
        Me.XrPictureBox5 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrPictureBox5, Me.XrPictureBox3, Me.XrPictureBox4, Me.XrPictureBox2, Me.XrLabel_ProgramstartDate, Me.XrLabel_Counselor, Me.XrLabel_Id_String, Me.XrLabel_Date, Me.XrLabel_CounselorName, Me.XrLabel_Id, Me.XrLabel7, Me.XrPictureBox1, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel_ClientName, Me.XrLabel3, Me.XrLabel2})
        Me.Detail.HeightF = 918.5416!
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 22.91667!
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 16.0!, System.Drawing.FontStyle.Italic)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(319.79!, 184.33!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(161.4583!, 29.25!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "This certifies that"
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(178.13!, 226.04!)
        Me.XrLabel_ClientName.Multiline = True
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabelClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(450.0!, 37.58337!)
        Me.XrLabel_ClientName.StylePriority.UseFont = False
        Me.XrLabel_ClientName.StylePriority.UseTextAlignment = False
        Me.XrLabel_ClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 14.0!, System.Drawing.FontStyle.Italic)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(307.29!, 276.04!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(228.1249!, 39.66666!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = "Has successfully completed"
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Calibri", 24.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(178.13!, 328.12!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(495.8267!, 55.29166!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "Pre-Purchase Housing counseling"
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(337.5!, 414.58!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(117.7082!, 22.99997!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "Conducted by"
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(238.54!, 451.04!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(336.4585!, 185.4999!)
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(111.46!, 648.96!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(573.9584!, 50.91663!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "This certificate is to be used for Homebuyer Education only, not FHA " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "          " & _
    "                                      mortgage insurance reduction"
        '
        'XrLabel_Id
        '
        Me.XrLabel_Id.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Id.LocationFloat = New DevExpress.Utils.PointFloat(111.46!, 723.83!)
        Me.XrLabel_Id.Name = "XrLabel_Id"
        Me.XrLabel_Id.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Id.SizeF = New System.Drawing.SizeF(162.5!, 22.99994!)
        Me.XrLabel_Id.StylePriority.UseFont = False
        Me.XrLabel_Id.StylePriority.UseTextAlignment = False
        Me.XrLabel_Id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_CounselorName
        '
        Me.XrLabel_CounselorName.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_CounselorName.LocationFloat = New DevExpress.Utils.PointFloat(347.92!, 723.83!)
        Me.XrLabel_CounselorName.Name = "XrLabel_CounselorName"
        Me.XrLabel_CounselorName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_CounselorName.SizeF = New System.Drawing.SizeF(227.0784!, 22.99994!)
        Me.XrLabel_CounselorName.StylePriority.UseFont = False
        Me.XrLabel_CounselorName.StylePriority.UseTextAlignment = False
        Me.XrLabel_CounselorName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Date
        '
        Me.XrLabel_Date.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(587.4983!, 723.83!)
        Me.XrLabel_Date.Name = "XrLabel_Date"
        Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(167.7083!, 22.99994!)
        Me.XrLabel_Date.StylePriority.UseFont = False
        Me.XrLabel_Date.StylePriority.UseTextAlignment = False
        Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Id_String
        '
        Me.XrLabel_Id_String.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Id_String.LocationFloat = New DevExpress.Utils.PointFloat(111.46!, 746.83!)
        Me.XrLabel_Id_String.Name = "XrLabel_Id_String"
        Me.XrLabel_Id_String.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Id_String.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel_Id_String.StylePriority.UseFont = False
        Me.XrLabel_Id_String.Text = "ID#"
        '
        'XrLabel_Counselor
        '
        Me.XrLabel_Counselor.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Counselor.LocationFloat = New DevExpress.Utils.PointFloat(347.92!, 746.8301!)
        Me.XrLabel_Counselor.Name = "XrLabel_Counselor"
        Me.XrLabel_Counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Counselor.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel_Counselor.StylePriority.UseFont = False
        Me.XrLabel_Counselor.Text = "Certified by"
        '
        'XrLabel_ProgramstartDate
        '
        Me.XrLabel_ProgramstartDate.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_ProgramstartDate.LocationFloat = New DevExpress.Utils.PointFloat(587.4982!, 746.8301!)
        Me.XrLabel_ProgramstartDate.Name = "XrLabel_ProgramstartDate"
        Me.XrLabel_ProgramstartDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ProgramstartDate.SizeF = New System.Drawing.SizeF(145.8334!, 23.0!)
        Me.XrLabel_ProgramstartDate.StylePriority.UseFont = False
        Me.XrLabel_ProgramstartDate.Text = "Date Of Completion"
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(124.9967!, 799.96!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(76.04167!, 69.87494!)
        '
        'XrPictureBox4
        '
        Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
        Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(427.7789!, 799.96!)
        Me.XrPictureBox4.Name = "XrPictureBox4"
        Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(107.2917!, 89.66663!)
        '
        'XrPictureBox3
        '
        Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
        Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(578.1283!, 799.96!)
        Me.XrPictureBox3.Name = "XrPictureBox3"
        Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(96.875!, 89.66663!)
        '
        'XrCrossBandBox1
        '
        Me.XrCrossBandBox1.EndBand = Me.Detail
        Me.XrCrossBandBox1.EndPointFloat = New DevExpress.Utils.PointFloat(1.041667!, 895.8331!)
        Me.XrCrossBandBox1.LocationFloat = New DevExpress.Utils.PointFloat(1.041667!, 7.291667!)
        Me.XrCrossBandBox1.Name = "XrCrossBandBox1"
        Me.XrCrossBandBox1.StartBand = Me.Detail
        Me.XrCrossBandBox1.StartPointFloat = New DevExpress.Utils.PointFloat(1.041667!, 7.291667!)
        Me.XrCrossBandBox1.WidthF = 785.1665!
        '
        'XrPictureBox5
        '
        Me.XrPictureBox5.Image = CType(resources.GetObject("XrPictureBox5.Image"), System.Drawing.Image)
        Me.XrPictureBox5.LocationFloat = New DevExpress.Utils.PointFloat(244.0961!, 799.9601!)
        Me.XrPictureBox5.Name = "XrPictureBox5"
        Me.XrPictureBox5.SizeF = New System.Drawing.SizeF(140.625!, 69.87494!)
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
        Me.ReportHeader.HeightF = 23.0!
        Me.ReportHeader.Name = "ReportHeader"
        Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(1.041667!, 0.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(143.75!, 23.0!)
        '
        'XrLabel2
        '
        Me.XrLabel2.CanGrow = False
        Me.XrLabel2.Font = New System.Drawing.Font("Calibri", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(18.02176!, 79.12172!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 2, 2, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(763.9565!, 70.91666!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UsePadding = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "ACHIEVEMENT"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLabel8
        '
        Me.XrLabel8.CanGrow = False
        Me.XrLabel8.Font = New System.Drawing.Font("Calibri", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(18.02176!, 23.83003!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 2, 2, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(763.9565!, 70.91666!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.StylePriority.UsePadding = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "CERTIFICATE OF"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'FHLBCertificate
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader, Me.ReportHeader})
        Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandBox1})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 23, 25)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = Nothing
        Me.Scripts.OnBeforePrint = "FHLBCertificate_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.PageHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMargin, 0)
        Me.Controls.SetChildIndex(Me.TopMargin, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Id As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_CounselorName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Id_String As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Counselor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_ProgramstartDate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrCrossBandBox1 As DevExpress.XtraReports.UI.XRCrossBandBox
    Friend WithEvents XrPictureBox5 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
End Class
