<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class PacketReport
    Inherits DebtPlus.Reports.Template.BaseXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PacketReport))
        Me.ActionPlanGoals1 = New ActionPlanGoals()
        Me.AgreementForServices1 = New AgreementForServices()
        Me.BottomLine1 = New BottomLine()
        Me.Budget1 = New Budget()
        Me.ClientRightsPolicy1 = New ClientRightsPolicy()
        Me.CoverPage1 = New CoverPage()
        Me.DmpDebts1 = New DMPDebts()
        Me.English_Counseling_Summary1 = New EnglishCounselingSummary()
        Me.FHLBCertificate1 = New FHLBCertificate()
        Me.Full_Cover_Letter1 = New Cover_Letter()
        Me.GeneralServices1 = New GeneralServices()
        Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader2 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader4 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader5 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader6 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader7 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader8 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader9 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader10 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader11 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader12 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader13 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader15 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader16 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader17 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader18 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.GroupHeader19 = New DevExpress.XtraReports.UI.GroupHeaderBand()
        Me.HowToAvoidForeclosure1 = New HowToAvoidForeclosure()
        Me.HudForYourProtection1 = New HUDForYourProtection()
        Me.Important_Home_Buying_Info1 = New Important_Home_Buying_Info()
        Me.Invoice1 = New Invoice()
        Me.MaintainingYourHome1 = New MaintainingYourHome()
        Me.NetWorth1 = New NetWorth()
        Me.OtherDebts1 = New OtherDebts()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.ParameterClient = New DevExpress.XtraReports.Parameters.Parameter()
        Me.PrivacyPolicy1 = New PrivacyPolicy()
        Me.Questions_to_Ask_Home_Inspector1 = New QuestionsToAskInspector()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_OrgName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPageBreak1 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.XrPageBreak2 = New DevExpress.XtraReports.UI.XRPageBreak()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrSubreport_ActionPlan_Goals = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_AgreementForServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_BottomLine = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Budget = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_ClientRightsPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Counseling_Summary = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Cover = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_CoverPage = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_DMPDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_FHLBCertificate = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Financial_Future = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_GeneralServices = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_HowToAvoidForeclosure = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_HUDForYourProtection = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_ImportHomebuyingInfo = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_Invoice = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_MaintainingYourHome = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_NetWorth = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_OtherDebts = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_PrivacyPolicy = New DevExpress.XtraReports.UI.XRSubreport()
        Me.XrSubreport_QuestionsToAskHomeInspector = New DevExpress.XtraReports.UI.XRSubreport()
        Me.YourfinancialFuture1 = New YourfinancialFuture()
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.English_Counseling_Summary1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FHLBCertificate1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HowToAvoidForeclosure1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HudForYourProtection1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Important_Home_Buying_Info1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Invoice1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MaintainingYourHome1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Questions_to_Ask_Home_Inspector1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.YourfinancialFuture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.HeightF = 0.0!
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.StylePriority.UseTextAlignment = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'ParameterClient
        '
        Me.ParameterClient.Description = "Client ID"
        Me.ParameterClient.Name = "ParameterClient"
        Me.ParameterClient.Type = GetType(Integer)
        Me.ParameterClient.Value = -1
        Me.ParameterClient.Visible = False
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak1, Me.XrSubreport_Cover, Me.XrSubreport_CoverPage})
        Me.ReportHeader.HeightF = 48.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrPageBreak1
        '
        Me.XrPageBreak1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 22.99999!)
        Me.XrPageBreak1.Name = "XrPageBreak1"
        '
        'XrSubreport_Cover
        '
        Me.XrSubreport_Cover.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Cover.Name = "XrSubreport_Cover"
        Me.XrSubreport_Cover.ReportSource = Me.CoverPage1
        Me.XrSubreport_Cover.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'XrSubreport_CoverPage
        '
        Me.XrSubreport_CoverPage.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
        Me.XrSubreport_CoverPage.Name = "XrSubreport_CoverPage"
        Me.XrSubreport_CoverPage.ReportSource = Me.Full_Cover_Letter1
        Me.XrSubreport_CoverPage.SizeF = New System.Drawing.SizeF(800.0!, 23.0!)
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_OrgName})
        Me.PageFooter.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
        Me.PageFooter.ForeColor = System.Drawing.Color.Teal
        Me.PageFooter.HeightF = 55.37506!
        Me.PageFooter.Name = "PageFooter"
        Me.PageFooter.PrintOn = CType((DevExpress.XtraReports.UI.PrintOnPages.NotWithReportHeader Or DevExpress.XtraReports.UI.PrintOnPages.NotWithReportFooter), DevExpress.XtraReports.UI.PrintOnPages)
        Me.PageFooter.StylePriority.UseFont = False
        Me.PageFooter.StylePriority.UseForeColor = False
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Format = "Page {0:f0}"
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(700.0!, 19.79169!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.Number
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.79168!)
        Me.XrPageInfo1.StylePriority.UseTextAlignment = False
        Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrLabel_ClientName
        '
        Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 19.79169!)
        Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
        Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientName.Scripts.OnBeforePrint = "XrLabel_ClientName_BeforePrint"
        Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(599.9999!, 17.79168!)
        '
        'XrLabel_ClientID
        '
        Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 37.58337!)
        Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
        Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ClientID.Scripts.OnBeforePrint = "XrLabel_ClientID_BeforePrint"
        Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(118.75!, 17.79167!)
        '
        'XrLabel_OrgName
        '
        Me.XrLabel_OrgName.LocationFloat = New DevExpress.Utils.PointFloat(121.8752!, 37.58337!)
        Me.XrLabel_OrgName.Name = "XrLabel_OrgName"
        Me.XrLabel_OrgName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_OrgName.Scripts.OnBeforePrint = "XrLabel_OrgName_BeforePrint"
        Me.XrLabel_OrgName.SizeF = New System.Drawing.SizeF(678.1248!, 17.79169!)
        Me.XrLabel_OrgName.StylePriority.UseTextAlignment = False
        Me.XrLabel_OrgName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'GroupHeader17
        '
        Me.GroupHeader17.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Counseling_Summary})
        Me.GroupHeader17.HeightF = 23.95833!
        Me.GroupHeader17.Level = 16
        Me.GroupHeader17.Name = "GroupHeader17"
        Me.GroupHeader17.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Counseling_Summary
        '
        Me.XrSubreport_Counseling_Summary.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Counseling_Summary.Name = "XrSubreport_Counseling_Summary"
        Me.XrSubreport_Counseling_Summary.ReportSource = Me.English_Counseling_Summary1
        Me.XrSubreport_Counseling_Summary.SizeF = New System.Drawing.SizeF(797.8746!, 23.0!)
        '
        'GroupHeader16
        '
        Me.GroupHeader16.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_AgreementForServices})
        Me.GroupHeader16.HeightF = 23.95833!
        Me.GroupHeader16.Level = 15
        Me.GroupHeader16.Name = "GroupHeader16"
        Me.GroupHeader16.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_AgreementForServices
        '
        Me.XrSubreport_AgreementForServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_AgreementForServices.Name = "XrSubreport_AgreementForServices"
        Me.XrSubreport_AgreementForServices.ReportSource = Me.AgreementForServices1
        Me.XrSubreport_AgreementForServices.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader18
        '
        Me.GroupHeader18.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_PrivacyPolicy})
        Me.GroupHeader18.HeightF = 25.08335!
        Me.GroupHeader18.Level = 14
        Me.GroupHeader18.Name = "GroupHeader18"
        Me.GroupHeader18.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_PrivacyPolicy
        '
        Me.XrSubreport_PrivacyPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_PrivacyPolicy.Name = "XrSubreport_PrivacyPolicy"
        Me.XrSubreport_PrivacyPolicy.ReportSource = Me.PrivacyPolicy1
        Me.XrSubreport_PrivacyPolicy.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader15
        '
        Me.GroupHeader15.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ClientRightsPolicy})
        Me.GroupHeader15.HeightF = 23.0!
        Me.GroupHeader15.Level = 13
        Me.GroupHeader15.Name = "GroupHeader15"
        Me.GroupHeader15.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ClientRightsPolicy
        '
        Me.XrSubreport_ClientRightsPolicy.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ClientRightsPolicy.Name = "XrSubreport_ClientRightsPolicy"
        Me.XrSubreport_ClientRightsPolicy.ReportSource = Me.ClientRightsPolicy1
        Me.XrSubreport_ClientRightsPolicy.SizeF = New System.Drawing.SizeF(799.0001!, 23.0!)
        '
        'GroupHeader6
        '
        Me.GroupHeader6.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_DMPDebts})
        Me.GroupHeader6.HeightF = 28.125!
        Me.GroupHeader6.Level = 9
        Me.GroupHeader6.Name = "GroupHeader6"
        Me.GroupHeader6.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_DMPDebts
        '
        Me.XrSubreport_DMPDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_DMPDebts.Name = "XrSubreport_DMPDebts"
        Me.XrSubreport_DMPDebts.ReportSource = Me.DmpDebts1
        Me.XrSubreport_DMPDebts.SizeF = New System.Drawing.SizeF(793.7495!, 28.125!)
        '
        'GroupHeader7
        '
        Me.GroupHeader7.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_NetWorth})
        Me.GroupHeader7.HeightF = 26.04167!
        Me.GroupHeader7.Level = 7
        Me.GroupHeader7.Name = "GroupHeader7"
        Me.GroupHeader7.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_NetWorth
        '
        Me.XrSubreport_NetWorth.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_NetWorth.Name = "XrSubreport_NetWorth"
        Me.XrSubreport_NetWorth.ReportSource = Me.NetWorth1
        Me.XrSubreport_NetWorth.SizeF = New System.Drawing.SizeF(796.8745!, 22.99994!)
        '
        'GroupHeader8
        '
        Me.GroupHeader8.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Budget})
        Me.GroupHeader8.HeightF = 26.04167!
        Me.GroupHeader8.Level = 11
        Me.GroupHeader8.Name = "GroupHeader8"
        Me.GroupHeader8.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Budget
        '
        Me.XrSubreport_Budget.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Budget.Name = "XrSubreport_Budget"
        Me.XrSubreport_Budget.ReportSource = Me.Budget1
        Me.XrSubreport_Budget.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader9
        '
        Me.GroupHeader9.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_BottomLine})
        Me.GroupHeader9.HeightF = 22.99997!
        Me.GroupHeader9.Level = 10
        Me.GroupHeader9.Name = "GroupHeader9"
        Me.GroupHeader9.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_BottomLine
        '
        Me.XrSubreport_BottomLine.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_BottomLine.Name = "XrSubreport_BottomLine"
        Me.XrSubreport_BottomLine.ReportSource = Me.BottomLine1
        Me.XrSubreport_BottomLine.SizeF = New System.Drawing.SizeF(799.0001!, 22.99997!)
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ActionPlan_Goals})
        Me.GroupHeader1.HeightF = 23.0!
        Me.GroupHeader1.Level = 6
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ActionPlan_Goals
        '
        Me.XrSubreport_ActionPlan_Goals.CanShrink = True
        Me.XrSubreport_ActionPlan_Goals.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ActionPlan_Goals.Name = "XrSubreport_ActionPlan_Goals"
        Me.XrSubreport_ActionPlan_Goals.ReportSource = Me.ActionPlanGoals1
        Me.XrSubreport_ActionPlan_Goals.SizeF = New System.Drawing.SizeF(798.0!, 23.0!)
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_OtherDebts})
        Me.GroupHeader2.HeightF = 24.66666!
        Me.GroupHeader2.Level = 8
        Me.GroupHeader2.Name = "GroupHeader2"
        Me.GroupHeader2.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_OtherDebts
        '
        Me.XrSubreport_OtherDebts.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_OtherDebts.Name = "XrSubreport_OtherDebts"
        Me.XrSubreport_OtherDebts.ReportSource = Me.OtherDebts1
        Me.XrSubreport_OtherDebts.SizeF = New System.Drawing.SizeF(793.7496!, 23.0!)
        '
        'GroupHeader4
        '
        Me.GroupHeader4.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Financial_Future})
        Me.GroupHeader4.HeightF = 25.0!
        Me.GroupHeader4.Level = 5
        Me.GroupHeader4.Name = "GroupHeader4"
        Me.GroupHeader4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_Financial_Future
        '
        Me.XrSubreport_Financial_Future.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_Financial_Future.Name = "XrSubreport_Financial_Future"
        Me.XrSubreport_Financial_Future.ReportSource = Me.YourfinancialFuture1
        Me.XrSubreport_Financial_Future.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader5
        '
        Me.GroupHeader5.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_ImportHomebuyingInfo})
        Me.GroupHeader5.HeightF = 25.0!
        Me.GroupHeader5.Level = 4
        Me.GroupHeader5.Name = "GroupHeader5"
        Me.GroupHeader5.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_ImportHomebuyingInfo
        '
        Me.XrSubreport_ImportHomebuyingInfo.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_ImportHomebuyingInfo.Name = "XrSubreport_ImportHomebuyingInfo"
        Me.XrSubreport_ImportHomebuyingInfo.ReportSource = Me.Important_Home_Buying_Info1
        Me.XrSubreport_ImportHomebuyingInfo.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader10
        '
        Me.GroupHeader10.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_HUDForYourProtection})
        Me.GroupHeader10.HeightF = 25.0!
        Me.GroupHeader10.Level = 3
        Me.GroupHeader10.Name = "GroupHeader10"
        Me.GroupHeader10.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_HUDForYourProtection
        '
        Me.XrSubreport_HUDForYourProtection.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_HUDForYourProtection.Name = "XrSubreport_HUDForYourProtection"
        Me.XrSubreport_HUDForYourProtection.ReportSource = Me.HudForYourProtection1
        Me.XrSubreport_HUDForYourProtection.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader11
        '
        Me.GroupHeader11.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_QuestionsToAskHomeInspector})
        Me.GroupHeader11.HeightF = 23.0!
        Me.GroupHeader11.Level = 2
        Me.GroupHeader11.Name = "GroupHeader11"
        '
        'XrSubreport_QuestionsToAskHomeInspector
        '
        Me.XrSubreport_QuestionsToAskHomeInspector.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_QuestionsToAskHomeInspector.Name = "XrSubreport_QuestionsToAskHomeInspector"
        Me.XrSubreport_QuestionsToAskHomeInspector.ReportSource = Me.Questions_to_Ask_Home_Inspector1
        Me.XrSubreport_QuestionsToAskHomeInspector.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader12
        '
        Me.GroupHeader12.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_MaintainingYourHome})
        Me.GroupHeader12.HeightF = 23.0!
        Me.GroupHeader12.Level = 1
        Me.GroupHeader12.Name = "GroupHeader12"
        Me.GroupHeader12.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_MaintainingYourHome
        '
        Me.XrSubreport_MaintainingYourHome.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_MaintainingYourHome.Name = "XrSubreport_MaintainingYourHome"
        Me.XrSubreport_MaintainingYourHome.ReportSource = Me.MaintainingYourHome1
        Me.XrSubreport_MaintainingYourHome.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader13
        '
        Me.GroupHeader13.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_HowToAvoidForeclosure})
        Me.GroupHeader13.HeightF = 23.95833!
        Me.GroupHeader13.Name = "GroupHeader13"
        Me.GroupHeader13.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrSubreport_HowToAvoidForeclosure
        '
        Me.XrSubreport_HowToAvoidForeclosure.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_HowToAvoidForeclosure.Name = "XrSubreport_HowToAvoidForeclosure"
        Me.XrSubreport_HowToAvoidForeclosure.ReportSource = Me.HowToAvoidForeclosure1
        Me.XrSubreport_HowToAvoidForeclosure.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'GroupHeader19
        '
        Me.GroupHeader19.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_GeneralServices})
        Me.GroupHeader19.HeightF = 23.0!
        Me.GroupHeader19.Level = 12
        Me.GroupHeader19.Name = "GroupHeader19"
        '
        'XrSubreport_GeneralServices
        '
        Me.XrSubreport_GeneralServices.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_GeneralServices.Name = "XrSubreport_GeneralServices"
        Me.XrSubreport_GeneralServices.ReportSource = Me.GeneralServices1
        Me.XrSubreport_GeneralServices.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageBreak2, Me.XrSubreport_Invoice, Me.XrSubreport_FHLBCertificate})
        Me.ReportFooter.HeightF = 46.0!
        Me.ReportFooter.Name = "ReportFooter"
        Me.ReportFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
        '
        'XrPageBreak2
        '
        Me.XrPageBreak2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 21.0!)
        Me.XrPageBreak2.Name = "XrPageBreak2"
        '
        'XrSubreport_Invoice
        '
        Me.XrSubreport_Invoice.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 23.0!)
        Me.XrSubreport_Invoice.Name = "XrSubreport_Invoice"
        Me.XrSubreport_Invoice.ReportSource = Me.Invoice1
        Me.XrSubreport_Invoice.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'XrSubreport_FHLBCertificate
        '
        Me.XrSubreport_FHLBCertificate.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrSubreport_FHLBCertificate.Name = "XrSubreport_FHLBCertificate"
        Me.XrSubreport_FHLBCertificate.ReportSource = Me.FHLBCertificate1
        Me.XrSubreport_FHLBCertificate.SizeF = New System.Drawing.SizeF(799.0!, 23.0!)
        '
        'PacketReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.ReportHeader, Me.PageFooter, Me.GroupHeader17, Me.GroupHeader16, Me.GroupHeader18, Me.GroupHeader15, Me.GroupHeader6, Me.GroupHeader7, Me.GroupHeader8, Me.GroupHeader9, Me.GroupHeader1, Me.GroupHeader2, Me.GroupHeader4, Me.GroupHeader5, Me.GroupHeader10, Me.GroupHeader11, Me.GroupHeader12, Me.GroupHeader13, Me.GroupHeader19, Me.ReportFooter})
        Me.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Italic)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterClient})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
        Me.Scripts.OnBeforePrint = "DMP_Report_BeforePrint"
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        Me.Controls.SetChildIndex(Me.ReportFooter, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader19, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader13, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader12, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader11, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader10, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader5, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader4, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader2, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader9, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader8, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader7, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader6, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader15, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader18, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader16, 0)
        Me.Controls.SetChildIndex(Me.GroupHeader17, 0)
        Me.Controls.SetChildIndex(Me.PageFooter, 0)
        Me.Controls.SetChildIndex(Me.ReportHeader, 0)
        Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
        Me.Controls.SetChildIndex(Me.Detail, 0)
        Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
        CType(Me.ActionPlanGoals1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgreementForServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BottomLine1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Budget1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientRightsPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CoverPage1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DmpDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.English_Counseling_Summary1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FHLBCertificate1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Full_Cover_Letter1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneralServices1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HowToAvoidForeclosure1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HudForYourProtection1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Important_Home_Buying_Info1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Invoice1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MaintainingYourHome1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NetWorth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OtherDebts1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PrivacyPolicy1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Questions_to_Ask_Home_Inspector1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.YourfinancialFuture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
    End Sub

    Private ActionPlanGoals1 As ActionPlanGoals
    Private AgreementForServices1 As AgreementForServices
    Private BottomLine1 As BottomLine
    Private Budget1 As Budget
    Private ClientRightsPolicy1 As ClientRightsPolicy
    Private CoverPage1 As CoverPage
    Private DmpDebts1 As DMPDebts
    Private English_Counseling_Summary1 As EnglishCounselingSummary
    Private FHLBCertificate1 As FHLBCertificate
    Private Full_Cover_Letter1 As Cover_Letter
    Private GeneralServices1 As GeneralServices
    Private GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader2 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader4 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader5 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader6 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader7 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader8 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader9 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader10 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader11 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader12 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader13 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader15 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader16 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader17 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader18 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private GroupHeader19 As DevExpress.XtraReports.UI.GroupHeaderBand
    Private HowToAvoidForeclosure1 As HowToAvoidForeclosure
    Private HudForYourProtection1 As HUDForYourProtection
    Private Important_Home_Buying_Info1 As Important_Home_Buying_Info
    Private Invoice1 As Invoice
    Private MaintainingYourHome1 As MaintainingYourHome
    Private NetWorth1 As NetWorth
    Private OtherDebts1 As OtherDebts
    Private PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Private ParameterClient As DevExpress.XtraReports.Parameters.Parameter
    Private PrivacyPolicy1 As PrivacyPolicy
    Private Questions_to_Ask_Home_Inspector1 As QuestionsToAskInspector
    Private ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Private ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Private XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    Private XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
    Private XrLabel_OrgName As DevExpress.XtraReports.UI.XRLabel
    Private XrPageBreak1 As DevExpress.XtraReports.UI.XRPageBreak
    Private XrPageBreak2 As DevExpress.XtraReports.UI.XRPageBreak
    Private XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Private XrSubreport_ActionPlan_Goals As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_AgreementForServices As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_BottomLine As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Budget As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_ClientRightsPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Counseling_Summary As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Cover As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_CoverPage As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_DMPDebts As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_FHLBCertificate As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Financial_Future As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_GeneralServices As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_HowToAvoidForeclosure As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_HUDForYourProtection As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_ImportHomebuyingInfo As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_Invoice As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_MaintainingYourHome As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_NetWorth As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_OtherDebts As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_PrivacyPolicy As DevExpress.XtraReports.UI.XRSubreport
    Private XrSubreport_QuestionsToAskHomeInspector As DevExpress.XtraReports.UI.XRSubreport
    Private YourfinancialFuture1 As YourfinancialFuture
End Class
