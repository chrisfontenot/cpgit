﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ABTCertificate
    Inherits BaseSubReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ABTCertificate))
        Me.XrRichText1 = New DevExpress.XtraReports.UI.XRRichText()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabelClientName = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_CounselorValue = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ProgramstartDateValue = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_Counselor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_ProgramstartDate = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox2 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox4 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrPictureBox3 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.XrCrossBandBox1 = New DevExpress.XtraReports.UI.XRCrossBandBox()
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox3, Me.XrPictureBox4, Me.XrPictureBox2, Me.XrLabel_ProgramstartDate, Me.XrLabel_Counselor, Me.XrLabel_ProgramstartDateValue, Me.XrLabel_CounselorValue, Me.XrLabel7, Me.XrPictureBox1, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabelClientName, Me.XrLabel3, Me.XrRichText1})
        Me.Detail.HeightF = 918.5416!
        '
        'TopMargin
        '
        Me.TopMargin.HeightF = 22.91667!
        '
        'XrRichText1
        '
        Me.XrRichText1.LocationFloat = New DevExpress.Utils.PointFloat(178.13!, 10.0!)
        Me.XrRichText1.Name = "XrRichText1"
        Me.XrRichText1.SerializableRtfString = resources.GetString("XrRichText1.SerializableRtfString")
        Me.XrRichText1.SizeF = New System.Drawing.SizeF(450.0!, 160.5!)
        Me.XrRichText1.StylePriority.UseFont = False
        '
        'XrLabel3
        '
        Me.XrLabel3.Font = New System.Drawing.Font("Calibri", 16.0!, System.Drawing.FontStyle.Italic)
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(319.79!, 184.33!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(161.4583!, 29.25!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "This certifies that"
        '
        'XrLabelClientName
        '
        Me.XrLabelClientName.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabelClientName.LocationFloat = New DevExpress.Utils.PointFloat(178.13!, 226.04!)
        Me.XrLabelClientName.Multiline = True
        Me.XrLabelClientName.Name = "XrLabelClientName"
        Me.XrLabelClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabelClientName.Scripts.OnBeforePrint = "XrLabelClientName_BeforePrint"
        Me.XrLabelClientName.SizeF = New System.Drawing.SizeF(450.0!, 37.58337!)
        Me.XrLabelClientName.StylePriority.UseFont = False
        Me.XrLabelClientName.StylePriority.UseTextAlignment = False
        Me.XrLabelClientName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Calibri", 14.0!, System.Drawing.FontStyle.Italic)
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(307.29!, 276.04!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(228.1249!, 39.66666!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = "Has successfully completed"
        '
        'XrLabel5
        '
        Me.XrLabel5.Font = New System.Drawing.Font("Calibri", 24.0!)
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(178.13!, 328.12!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(495.8267!, 55.29166!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "Pre-Purchase Housing counseling"
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(337.5!, 414.58!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(117.7082!, 22.99997!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "Conducted by"
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(238.54!, 451.04!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(336.4585!, 185.4999!)
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(111.46!, 648.96!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(573.9584!, 50.91663!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "This certificate is to be used for Homebuyer Education only, not FHA " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "          " & _
    "                                      mortgage insurance reduction"
        '
        'XrLabel_CounselorValue
        '
        Me.XrLabel_CounselorValue.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_CounselorValue.LocationFloat = New DevExpress.Utils.PointFloat(347.92!, 723.83!)
        Me.XrLabel_CounselorValue.Name = "XrLabel_CounselorValue"
        Me.XrLabel_CounselorValue.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_CounselorValue.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel_CounselorValue.StylePriority.UseFont = False
        Me.XrLabel_CounselorValue.StylePriority.UseTextAlignment = False
        Me.XrLabel_CounselorValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_ProgramstartDateValue
        '
        Me.XrLabel_ProgramstartDateValue.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_ProgramstartDateValue.LocationFloat = New DevExpress.Utils.PointFloat(587.4983!, 723.83!)
        Me.XrLabel_ProgramstartDateValue.Name = "XrLabel_ProgramstartDateValue"
        Me.XrLabel_ProgramstartDateValue.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ProgramstartDateValue.SizeF = New System.Drawing.SizeF(167.7083!, 22.99994!)
        Me.XrLabel_ProgramstartDateValue.StylePriority.UseFont = False
        Me.XrLabel_ProgramstartDateValue.StylePriority.UseTextAlignment = False
        Me.XrLabel_ProgramstartDateValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel_Counselor
        '
        Me.XrLabel_Counselor.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_Counselor.LocationFloat = New DevExpress.Utils.PointFloat(347.92!, 746.8301!)
        Me.XrLabel_Counselor.Name = "XrLabel_Counselor"
        Me.XrLabel_Counselor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_Counselor.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel_Counselor.StylePriority.UseFont = False
        Me.XrLabel_Counselor.Text = "Certified by"
        '
        'XrLabel_ProgramstartDate
        '
        Me.XrLabel_ProgramstartDate.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel_ProgramstartDate.LocationFloat = New DevExpress.Utils.PointFloat(587.4982!, 746.8301!)
        Me.XrLabel_ProgramstartDate.Name = "XrLabel_ProgramstartDate"
        Me.XrLabel_ProgramstartDate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_ProgramstartDate.SizeF = New System.Drawing.SizeF(145.8334!, 23.0!)
        Me.XrLabel_ProgramstartDate.StylePriority.UseFont = False
        Me.XrLabel_ProgramstartDate.Text = "Date Of Completion"
        '
        'XrPictureBox2
        '
        Me.XrPictureBox2.Image = CType(resources.GetObject("XrPictureBox2.Image"), System.Drawing.Image)
        Me.XrPictureBox2.LocationFloat = New DevExpress.Utils.PointFloat(238.54!, 799.96!)
        Me.XrPictureBox2.Name = "XrPictureBox2"
        Me.XrPictureBox2.SizeF = New System.Drawing.SizeF(76.04167!, 69.87494!)
        '
        'XrPictureBox4
        '
        Me.XrPictureBox4.Image = CType(resources.GetObject("XrPictureBox4.Image"), System.Drawing.Image)
        Me.XrPictureBox4.LocationFloat = New DevExpress.Utils.PointFloat(347.92!, 799.96!)
        Me.XrPictureBox4.Name = "XrPictureBox4"
        Me.XrPictureBox4.SizeF = New System.Drawing.SizeF(107.2917!, 89.66663!)
        '
        'XrPictureBox3
        '
        Me.XrPictureBox3.Image = CType(resources.GetObject("XrPictureBox3.Image"), System.Drawing.Image)
        Me.XrPictureBox3.LocationFloat = New DevExpress.Utils.PointFloat(503.13!, 799.96!)
        Me.XrPictureBox3.Name = "XrPictureBox3"
        Me.XrPictureBox3.SizeF = New System.Drawing.SizeF(96.875!, 89.66663!)
        '
        'XrCrossBandBox1
        '
        Me.XrCrossBandBox1.EndBand = Me.Detail
        Me.XrCrossBandBox1.EndPointFloat = New DevExpress.Utils.PointFloat(1.041667!, 895.8331!)
        Me.XrCrossBandBox1.LocationFloat = New DevExpress.Utils.PointFloat(1.041667!, 7.291667!)
        Me.XrCrossBandBox1.Name = "XrCrossBandBox1"
        Me.XrCrossBandBox1.StartBand = Me.Detail
        Me.XrCrossBandBox1.StartPointFloat = New DevExpress.Utils.PointFloat(1.041667!, 7.291667!)
        Me.XrCrossBandBox1.WidthF = 785.1665!
        '
        'ABTCertificate
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.CrossBandControls.AddRange(New DevExpress.XtraReports.UI.XRCrossBandControl() {Me.XrCrossBandBox1})
        Me.Margins = New System.Drawing.Printing.Margins(25, 25, 23, 25)
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.ScriptReferencesString = Nothing
        Me.ScriptsSource = resources.GetString("$this.ScriptsSource")
        Me.Version = "11.2"
        CType(Me.XrRichText1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrRichText1 As DevExpress.XtraReports.UI.XRRichText
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabelClientName As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_CounselorValue As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_ProgramstartDateValue As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_Counselor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_ProgramstartDate As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPictureBox2 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox4 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPictureBox3 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrCrossBandBox1 As DevExpress.XtraReports.UI.XRCrossBandBox
End Class
