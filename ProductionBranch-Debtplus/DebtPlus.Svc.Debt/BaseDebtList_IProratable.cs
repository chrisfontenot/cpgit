using System.Linq;

namespace DebtPlus.Svc.Debt
{
    partial class BaseDebtList : DebtPlus.Interfaces.Debt.IProratableList
    {
        /// <summary>
        /// Find the amount of money that is not subject to prorate
        /// </summary>
        public virtual decimal TotalFees()
        {
            return this.Cast<DebtPlus.Interfaces.Debt.IProratable>().Where(s => s.IsFee || s.IsPayoff).Sum(s => s.debit_amt);
        }

        /// <summary>
        /// Locate the payment cushion if there is one.
        /// </summary>
        public virtual DebtPlus.Interfaces.Debt.IProratable PaymentCushionDebt
        {
            get
            {
                return this.Cast<DebtPlus.Interfaces.Debt.IProratable>().Where(s => s.DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.PaymentCushion).FirstOrDefault();
            }
        }

        /// <summary>
        /// Locate the debt for the monthly fee account
        /// </summary>
        public virtual DebtPlus.Interfaces.Debt.IProratable MonthlyFeeDebt
        {
            get
            {
                return this.Cast<DebtPlus.Interfaces.Debt.IProratable>().Where(s => s.DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee).FirstOrDefault();
            }
        }

        /// <summary>
        /// Calculate the total of the payments for active creditors
        /// </summary>
        public virtual decimal TotalPayments()
        {
            return this.Cast<DebtPlus.Interfaces.Debt.IProratable>().Where(s => s.IsProratable).Sum(s => s.debit_amt);
        }
    }
}