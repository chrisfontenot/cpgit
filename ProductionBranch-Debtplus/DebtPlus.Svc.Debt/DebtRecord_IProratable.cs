#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using DebtPlus.Interfaces.Debt;
using DebtPlus.Utils;
using DebtPlus.Interfaces.Disbursement;

namespace DebtPlus.Svc.Debt
{
    partial class DebtRecord : IProratable
    {
        /// <summary>
        /// Current Balance
        /// </summary>
        public override decimal current_balance
        {
            get
            {
                return base.current_balance;
            }
        }

        /// <summary>
        /// Current Balance
        /// </summary>
        decimal IProratable.current_balance
        {
            get
            {
                return current_balance;
            }
        }

        /// <summary>
        /// Type of the debt for the prorate function
        /// </summary>
        public override DebtPlus.Interfaces.Debt.DebtTypeEnum DebtType
        {
            get { return base.DebtType; }
            set { base.DebtType = value; }
        }

        /// <summary>
        /// Should debt be prorated?
        /// </summary>
        public virtual bool IsProratable
        {
            get
            {
                if (!DebtCurrentStorage.isPayoff && !IsHeld && proratable_IsActive)
                {
                    switch (DebtType)
                    {
                        case DebtTypeEnum.PaymentCushion:
                            return true;

                        case DebtTypeEnum.Normal:
                            return true;

                        case DebtTypeEnum.AgencyFee:
                            return true;

                        default:
                            return false;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Return the disbursement factor for the prorate function
        /// </summary>
        public virtual decimal proratable_disbursement_factor
        {
            get { return disbursement_factor; }
            set { disbursement_factor = value; }
        }

        /// <summary>
        /// Return the disbursement factor for the prorate function
        /// </summary>
        decimal IProratable.disbursement_factor
        {
            get { return proratable_disbursement_factor; }
            set { proratable_disbursement_factor = value; }
        }

        /// <summary>
        /// Return the Active Status for the prorate function
        /// </summary>
        public virtual bool proratable_IsActive
        {
            get { return IsActive; }
            set { IsActive = value; }
        }

        /// <summary>
        /// Return the Active Status for the prorate function
        /// </summary>
        bool IProratable.IsActive
        {
            get { return proratable_IsActive; }
            set { proratable_IsActive = value; }
        }

        /// <summary>
        /// Return the Active Status for the prorate function
        /// </summary>
        public virtual bool IsPayoff
        {
            get { return DebtCurrentStorage.isPayoff; }
            set { DebtCurrentStorage.isPayoff = value; }
        }

        /// <summary>
        /// Return the Current Month's payments for the prorate function
        /// </summary>
        public decimal proratable_payments_month_0
        {
            get { return payments_month_0; }
            set { payments_month_0 = value; }
        }

        /// <summary>
        /// Return the Current Month's payments for the prorate function
        /// </summary>
        decimal IProratable.payments_month_0
        {
            get { return proratable_payments_month_0; }
            set { proratable_payments_month_0 = value; }
        }

        /// <summary>
        /// Return the priority for the prorate function
        /// </summary>
        public Int32 proratable_priority
        {
            get
            {
                Int32 answer = default(Int32);
                if (priority >= 0 && priority < 10)
                {
                    answer = priority;
                }
                else
                {
                    answer = 9;
                }
                return answer;
            }
            set { priority = value; }
        }

        /// <summary>
        /// Return the priority for the prorate function
        /// </summary>
        Int32 IProratable.priority
        {
            get { return proratable_priority; }
            set { proratable_priority = value; }
        }

        /// <summary>
        /// Return the Scheduled Payment for the prorate function
        /// </summary>
        public decimal proratable_sched_payment
        {
            get { return sched_payment; }
            set { sched_payment = value; }
        }

        /// <summary>
        /// Return the Scheduled Payment for the prorate function
        /// </summary>
        decimal IProratable.sched_payment
        {
            get { return proratable_sched_payment; }
            set { proratable_sched_payment = value; }
        }

        /// <summary>
        /// Return the Total Interest for the prorate function
        /// </summary>
        public decimal proratable_total_interest
        {
            get { return total_interest; }
            set { total_interest = value; }
        }
        decimal IProratable.total_interest
        {
            get { return proratable_total_interest; }
            set { proratable_total_interest = value; }
        }

        /// <summary>
        /// Should debt be reserved and not prorated but paid the full amount?
        /// </summary>
        public virtual bool IsFee
        {
            get
            {
                // If the item is a payoff for the debt then it is a fee amount and not proratable.
                // we want the money to pay off the debt taken off the top first since the disbursement
                // factor is reduced to the payoff amount.
                if (DebtCurrentStorage.isPayoff)
                {
                    return true;
                }

                // Otherwise, look that the other flags and return false if the value is not a valid debt.
                if (!IsHeld && IsActive)
                {
                    switch (DebtType)
                    {
                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.FixedAgencyFee:
                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee:
                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee:
                            return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Return the adjusted original balance for the debt
        /// </summary>
        decimal IProratable.adjusted_original_balance
        {
            get
            {
                return orig_balance + orig_balance_adjustment;
            }
        }
    }
}