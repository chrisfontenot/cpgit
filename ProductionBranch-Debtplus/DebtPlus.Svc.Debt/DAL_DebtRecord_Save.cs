#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using DebtPlus.Repository;

namespace DebtPlus.Svc.Debt
{
    partial class DAL_DebtRecord
    {
        /// <summary>
        /// Save the changed data into the database
        /// </summary>
        public virtual void SaveData()
        {
            SaveData(null);
        }

        public virtual void SaveData(SqlTransaction txn)
        {
            var sbSubject = new System.Text.StringBuilder();
            var sbNote = new System.Text.StringBuilder();
            var sbCommand = new System.Text.StringBuilder();
            bool debtDropped = false;

            // Create the command to save the record
            using (SqlCommand cmd = new SqlCommand())
            {
                // Percentage fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.fairshare_pct_eft, currentStorage.fairshare_pct_eft) != 0)
                {
                    cmd.Parameters.Add("@fairshare_pct_eft", SqlDbType.Float).Value = fairshare_pct_eft;
                    sbCommand.Append(",[fairshare_pct_eft]=@fairshare_pct_eft");
                    sbSubject.Append(",EFT %");
                    sbNote.AppendFormat("{0}Changed Fairshare EFT percent from {1:p} to {2:p}", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.fairshare_pct_eft), fairshare_pct_eft);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.fairshare_pct_check, currentStorage.fairshare_pct_check) != 0)
                {
                    cmd.Parameters.Add("@fairshare_pct_check", SqlDbType.Float).Value = fairshare_pct_check;
                    sbCommand.Append(",[fairshare_pct_check]=@fairshare_pct_check");
                    sbSubject.Append(",CHECK %");
                    sbNote.AppendFormat("{0}Changed Fairshare CHECK percent from {1:p} to {2:p}", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.fairshare_pct_check), fairshare_pct_check);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.dmp_payout_interest, currentStorage.dmp_payout_interest) != 0)
                {
                    cmd.Parameters.Add("@dmp_payout_interest", SqlDbType.Float).Value = dmp_payout_interest;
                    sbCommand.Append(",[dmp_payout_interest]=@dmp_payout_interest");
                    sbSubject.Append(",PAYOUT %");
                    sbNote.AppendFormat("{0}Changed DmpPayoutInterest percent from {1:p} to {2:p}", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.dmp_payout_interest), dmp_payout_interest);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.dmp_interest, currentStorage.dmp_interest) != 0)
                {
                    cmd.Parameters.Add("@dmp_interest", SqlDbType.Float).Value = dmp_interest;
                    sbCommand.Append(",[dmp_interest]=@dmp_interest");
                    sbSubject.Append(",INTEREST %");
                    sbNote.AppendFormat("{0}Changed DmpInterest from {1:p} to {2:p}", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.dmp_interest), dmp_interest);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.non_dmp_interest, currentStorage.non_dmp_interest) != 0)
                {
                    cmd.Parameters.Add("@non_dmp_interest", SqlDbType.Float).Value = non_dmp_interest;
                    sbCommand.Append(",[non_dmp_interest]=@non_dmp_interest");
                    sbSubject.Append(",NONDMP %");
                    sbNote.AppendFormat("{0}Changed NonDmpInterest from {1:p} to {2:p}", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.non_dmp_interest), non_dmp_interest);
                }

                // Date fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.expected_payout_date, currentStorage.expected_payout_date) != 0)
                {
                    cmd.Parameters.Add("@expected_payout_date", SqlDbType.DateTime).Value = expected_payout_date;
                    sbCommand.Append(",[expected_payout_date]=@expected_payout_date");
                    sbSubject.Append(",PAYOUT");
                    sbNote.AppendFormat("{0}Changed ExpectedPayoutDate from {1:d} to {2:d}", Environment.NewLine, ddate(originalStorage.expected_payout_date), expected_payout_date);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.last_payment_date_b4_dmp, currentStorage.last_payment_date_b4_dmp) != 0)
                {
                    cmd.Parameters.Add("@last_payment_date_b4_dmp", SqlDbType.DateTime).Value = last_payment_date_b4_dmp;
                    sbCommand.Append(",[last_payment_date_b4_dmp]=@last_payment_date_b4_dmp");
                    sbSubject.Append(",LAST PAYMENT DT");
                    sbNote.AppendFormat("{0}Changed LastPaymentDateB4Dmp from {1:d} to {2:d}", Environment.NewLine, ddate(originalStorage.last_payment_date_b4_dmp), last_payment_date_b4_dmp);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.last_stmt_date, currentStorage.last_stmt_date) != 0)
                {
                    cmd.Parameters.Add("@last_stmt_date", SqlDbType.DateTime).Value = last_stmt_date;
                    sbCommand.Append(",[last_stmt_date]=@last_stmt_date");
                    sbSubject.Append(",STMT date");
                    sbNote.AppendFormat("{0}Changed Statement Date from {1:d} to {2:d}", Environment.NewLine, ddate(originalStorage.last_stmt_date), last_stmt_date);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.start_date, currentStorage.start_date) != 0)
                {
                    cmd.Parameters.Add("@start_date", SqlDbType.DateTime).Value = start_date;
                    sbCommand.Append(",[start_date]=@start_date");
                    sbSubject.Append(",StartDate");
                    sbNote.AppendFormat("{0}Changed StartDate from {1:d} to {2:d}", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.start_date), start_date);
                }

                // Bit fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.send_drop_notice, currentStorage.send_drop_notice) != 0)
                {
                    cmd.Parameters.Add("@send_drop_notice", SqlDbType.Bit).Value = send_drop_notice;
                    sbCommand.Append(",[send_drop_notice]=@send_drop_notice");
                    sbSubject.Append(",Send Drop");
                    sbNote.AppendFormat("{0}Changed drop notice from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DBool(originalStorage.send_drop_notice).ToString(), send_drop_notice.ToString());
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.student_loan_release, currentStorage.student_loan_release) != 0)
                {
                    cmd.Parameters.Add("@student_loan_release", SqlDbType.Bit).Value = student_loan_release;
                    sbCommand.Append(",[student_loan_release]=@student_loan_release");
                    sbSubject.Append(",StudentLoan");
                    sbNote.AppendFormat("{0}Changed StudentLoanRelease from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DBool(originalStorage.student_loan_release).ToString(), student_loan_release.ToString());
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.irs_form_on_file, currentStorage.irs_form_on_file) != 0)
                {
                    cmd.Parameters.Add("@irs_form_on_file", SqlDbType.Bit).Value = irs_form_on_file;
                    sbCommand.Append(",[irs_form_on_file]=@irs_form_on_file");
                    sbSubject.Append(",IRS");
                    sbNote.AppendFormat("{0}Changed IrsFormOnFile from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DBool(originalStorage.irs_form_on_file).ToString(), irs_form_on_file.ToString());
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.balance_verification_release, currentStorage.balance_verification_release) != 0)
                {
                    cmd.Parameters.Add("@balance_verification_release", SqlDbType.Bit).Value = balance_verification_release;
                    sbCommand.Append(",[balance_verification_release]=@balance_verification_release");
                    sbSubject.Append(",CDV Rel");
                    sbNote.AppendFormat("{0}Changed BalanceVerificationRelease from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DBool(originalStorage.balance_verification_release).ToString(), balance_verification_release.ToString());
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.hold_disbursements, currentStorage.hold_disbursements) != 0)
                {
                    cmd.Parameters.Add("@hold_disbursements", SqlDbType.Bit).Value = hold_disbursements;
                    sbCommand.Append(",[hold_disbursements]=@hold_disbursements");
                    sbSubject.Append(",HOLD");
                    sbNote.AppendFormat("{0}Changed HoldDisbursements from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DBool(originalStorage.hold_disbursements).ToString(), hold_disbursements.ToString());
                }

                // DO NOT include "DateDispChanged". == is set later below....

                // string fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.message, currentStorage.message) != 0)
                {
                    cmd.Parameters.Add("@message", SqlDbType.VarChar, 256).Value = Message == null ? System.DBNull.Value : Message;
                    sbCommand.Append(",[message]=@message");
                    sbSubject.Append(",MSG");
                    sbNote.AppendFormat("{0}Changed message from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.message), Message);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.account_number, currentStorage.account_number) != 0)
                {
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 256).Value = account_number == null ? string.Empty : account_number;
                    sbCommand.Append(",[account_number]=@account_number");
                    sbSubject.Append(",ACCT NUM");
                    sbNote.AppendFormat("{0}Changed AccountNumber from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.account_number), account_number);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.balance_verify_by, currentStorage.balance_verify_by) != 0)
                {
                    cmd.Parameters.Add("@balance_verify_by", SqlDbType.VarChar, 256).Value = balance_verify_by;
                    sbCommand.Append(",[balance_verify_by]=@balance_verify_by");
                    sbSubject.Append(",VERIFIED");
                    sbNote.AppendFormat("{0}Changed BalanceVerifyBy from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.balance_verify_by), balance_verify_by);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.balance_verify_date, currentStorage.balance_verify_date) != 0)
                {
                    cmd.Parameters.Add("@balance_verify_date", SqlDbType.VarChar, 256).Value = balance_verify_date;
                    sbCommand.Append(",[balance_verify_date]=@balance_verify_date");
                    sbSubject.Append(",VERIFIED");
                    sbNote.AppendFormat("{0}Changed BalanceVerifyDate from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.balance_verify_date), balance_verify_date);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.cr_creditor_name, currentStorage.cr_creditor_name) != 0)
                {
                    cmd.Parameters.Add("@cr_creditor_name", SqlDbType.VarChar, 256).Value = cr_creditor_name;
                    sbCommand.Append(",[cr_creditor_name]=@cr_creditor_name");
                    sbSubject.Append(",CrCreditorName");
                    sbNote.AppendFormat("{0}Changed CrCreditorName from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.cr_creditor_name), cr_creditor_name);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.client_name, currentStorage.client_name) != 0)
                {
                    cmd.Parameters.Add("@client_name", SqlDbType.VarChar, 256).Value = client_name;
                    sbCommand.Append(",[client_name]=@client_name");
                    sbSubject.Append(",ClientName");
                    sbNote.AppendFormat("{0}Changed ClientName from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.client_name), client_name);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.creditor_name, currentStorage.creditor_name) != 0)
                {
                    cmd.Parameters.Add("@creditor_name", SqlDbType.VarChar, 256).Value = creditor_name;
                    sbCommand.Append(",[creditor_name]=@creditor_name");
                    sbSubject.Append(",CreditorName");
                    sbNote.AppendFormat("{0}Changed CreditorName from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.creditor_name), creditor_name);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.rpps_client_type_indicator, currentStorage.rpps_client_type_indicator) != 0)
                {
                    cmd.Parameters.Add("@rpps_client_type_indicator", SqlDbType.VarChar, 256).Value = rpps_client_type_indicator;
                    sbCommand.Append(",[rpps_client_type_indicator]=@rpps_client_type_indicator");
                    sbSubject.Append(",RppsClientTypeIndicator");
                    sbNote.AppendFormat("{0}Changed RppsClientTypeIndicator from '{1}' to '{2}'", Environment.NewLine, DebtPlus.Utils.Nulls.DStr(originalStorage.rpps_client_type_indicator), rpps_client_type_indicator);
                }

                // Types
                if (DebtPlus.Repository.Common.Compare(originalStorage.drop_reason, currentStorage.drop_reason) != 0)
                {
                    cmd.Parameters.Add("@drop_reason", SqlDbType.Int).Value = currentStorage.drop_reason;
                    sbCommand.Append(",[drop_reason]=@drop_reason,[drop_date]=getdate(),[drop_reason_sent]=NULL");
                    sbSubject.Append(",drop reason");
                    sbNote.AppendFormat("{0}Changed drop reason from '{1}' to '{2}'", Environment.NewLine, GetDrop_ReasonDescription(originalStorage.drop_reason), GetDrop_ReasonDescription(currentStorage.drop_reason));

                    // Indicate the debt has been dropped and we need to send a drop notice
                    if (currentStorage.drop_reason != null && !object.ReferenceEquals(currentStorage.drop_reason, DBNull.Value))
                    {
                        debtDropped = true;
                    }
                }

                // Int32 fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.payment_rpps_mask, currentStorage.payment_rpps_mask) != 0)
                {
                    cmd.Parameters.Add("@payment_rpps_mask", SqlDbType.Int, 0).Value = payment_rpps_mask;
                    sbCommand.Append(",[payment_rpps_mask]=@payment_rpps_mask");
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.rpps_mask, currentStorage.rpps_mask) != 0)
                {
                    cmd.Parameters.Add("@rpps_mask", SqlDbType.Int, 0).Value = rpps_mask;
                    sbCommand.Append(",[rpps_mask]=@rpps_mask");
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.months_delinquent, currentStorage.months_delinquent) != 0)
                {
                    cmd.Parameters.Add("@months_delinquent", SqlDbType.Int, 0).Value = DebtPlus.Utils.Nulls.DInt(months_delinquent);
                    sbCommand.Append(",[months_delinquent]=@months_delinquent");
                    sbSubject.Append(",DELINQUENT");
                    sbNote.AppendFormat("{0}Changed MonthsDelinquent from '{1:f0}' to '{2:f0}'", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.months_delinquent), months_delinquent);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.Person, currentStorage.Person) != 0)
                {
                    cmd.Parameters.Add("@Person", SqlDbType.Int, 0).Value = person;
                    sbCommand.Append(",[Person]=@Person");
                    sbSubject.Append(",Person");
                    sbNote.AppendFormat("{0}Changed Person from '{1:f0}' to '{2:f0}'", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.Person), person);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.priority, currentStorage.priority) != 0)
                {
                    cmd.Parameters.Add("@priority", SqlDbType.Int, 0).Value = priority;
                    sbCommand.Append(",[priority]=@priority");
                    sbSubject.Append(",priority");
                    sbNote.AppendFormat("{0}Changed priority from '{1:f0}' to '{2:f0}'", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.priority), priority);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.line_number, currentStorage.line_number) != 0)
                {
                    cmd.Parameters.Add("@line_number", SqlDbType.Int, 0).Value = line_number;
                    sbCommand.Append(",[line_number]=@line_number");
                    sbSubject.Append(",LineNumber");
                    sbNote.AppendFormat("{0}Changed LineNumber from '{1:f0}' to '{2:f0}'", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.line_number), line_number);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.percent_balance, currentStorage.percent_balance) != 0)
                {
                    cmd.Parameters.Add("@percent_balance", SqlDbType.Float, 0).Value = percent_balance;
                    sbCommand.Append(",[percent_balance]=@percent_balance");
                    sbSubject.Append(",PercentBalance");
                    sbNote.AppendFormat("{0}Changed PercentBalance from '{1:p3}%' to '{2:p3}%'", Environment.NewLine, DebtPlus.Utils.Nulls.DDbl(originalStorage.percent_balance), percent_balance);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.terms, currentStorage.terms) != 0)
                {
                    cmd.Parameters.Add("@terms", SqlDbType.Int, 0).Value = terms;
                    sbCommand.Append(",[terms]=@terms");
                    sbSubject.Append(",terms");
                    sbNote.AppendFormat("{0}Changed terms from '{1:f0}' to '{2:f0}'", Environment.NewLine, DebtPlus.Utils.Nulls.DInt(originalStorage.terms), terms);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.send_bal_verify, currentStorage.send_bal_verify) != 0)
                {
                    cmd.Parameters.Add("@send_bal_verify", SqlDbType.Int, 0).Value = send_bal_verify;
                    sbCommand.Append(",[send_bal_verify]=@send_bal_verify");
                    sbSubject.Append(",Send BalVer");
                    sbNote.AppendFormat("{0}Changed SendBalVerify from '{1:f0}' to '{2:f0}'", Environment.NewLine, GetSend_Bal_Verify_Description(originalStorage.send_bal_verify), GetSend_Bal_Verify_Description(send_bal_verify));
                }

                // decimal fields
                if (DebtPlus.Repository.Common.Compare(originalStorage.sched_payment, currentStorage.sched_payment) != 0)
                {
                    cmd.Parameters.Add("@sched_payment", SqlDbType.Decimal, 0).Value = sched_payment;
                    sbCommand.Append(",[sched_payment]=@sched_payment");
                    sbSubject.Append(",SCHED PAY");
                    sbNote.AppendFormat("{0}Changed SchedPayment from {1:c} to {2:c}", Environment.NewLine, DebtPlus.Utils.Nulls.DDec(originalStorage.sched_payment), sched_payment);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.disbursement_factor, currentStorage.disbursement_factor) != 0)
                {
                    cmd.Parameters.Add("@disbursement_factor", SqlDbType.Decimal, 0).Value = disbursement_factor;
                    sbCommand.Append(",[disbursement_factor]=@disbursement_factor,[date_disp_changed]=getdate()");
                    sbSubject.Append(",DISB");
                    sbNote.AppendFormat("{0}Changed DisbursementFactor from {1:c} to {2:c}", Environment.NewLine, DebtPlus.Utils.Nulls.DDec(originalStorage.disbursement_factor), disbursement_factor);
                }

                if (DebtPlus.Repository.Common.Compare(originalStorage.non_dmp_payment, currentStorage.non_dmp_payment) != 0)
                {
                    cmd.Parameters.Add("@non_dmp_payment", SqlDbType.Decimal, 0).Value = non_dmp_payment;
                    sbCommand.Append(",[non_dmp_payment]=@non_dmp_payment");
                    sbSubject.Append(",NONDMP $");
                    sbNote.AppendFormat("{0}Changed NonDmpPayment from {1:c} to {2:c}", Environment.NewLine, DebtPlus.Utils.Nulls.DDec(originalStorage.non_dmp_payment), non_dmp_payment);
                }

                // finally, complete the update command if possible
                if (sbCommand.Length != 0)
                {
                    sbCommand.Remove(0, 1);
                    sbCommand.Insert(0, "UPDATE [client_creditor] SET ");
                    sbCommand.Append(" WHERE [client_creditor]=@ClientCreditor");
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sbCommand.ToString();

                    cmd.Parameters.Add("@ClientCreditor", SqlDbType.Int).Value = client_creditor;

                    // Execute the command
                    DebtPlus.Repository.Common.ExecuteNonQuery(cmd, txn);

                    // if we dropped the debt then we need to do some additional work
                    if (debtDropped)
                    {
                        DebtPlus.Repository.Debts.DropDebt(client_creditor, txn);
                    }

                    // Replace the original data with the current version for the next time
                    originalStorage = (RecordStorage)currentStorage.Clone();
                }
            }

            // Generate the system note for the changes if needed
            if (sbSubject.Length != 0)
            {
                sbSubject.Remove(0, 1);
                sbSubject.Insert(0, "Changed ");

                sbNote.Insert(0, "Changed the following fields:" + Environment.NewLine);

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "INSERT INTO [client_notes] ([client],[client_creditor],[type],[subject],[note],[dont_print],[dont_delete],[dont_edit]) VALUES (@Client, @ClientCreditor, 3, @subject, @note, 0, 1, 1)";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@Client", SqlDbType.Int).Value = ClientId;
                    cmd.Parameters.Add("@ClientCreditor", SqlDbType.Int).Value = client_creditor;
                    cmd.Parameters.Add("@subject", SqlDbType.VarChar, 80).Value = sbSubject.ToString();
                    cmd.Parameters.Add("@note", SqlDbType.Text, Int32.MaxValue).Value = sbNote.ToString();
                    DebtPlus.Repository.Common.ExecuteNonQuery(cmd, txn);
                }
            }

            // The current storage is no longer modified since the last save operation
            currentStorage.IsModified = false;
            privateIsNew = false;
        }

        /// <summary>
        /// Convert a date to a string
        /// </summary>
        private string ddate(object inputDate)
        {
            string answer = "NONE";
            if (inputDate != null && inputDate is DateTime)
            {
                answer = Convert.ToDateTime(inputDate, System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
            }
            return answer;
        }

        /// <summary>
        /// Drop Reasons table
        /// </summary>
        private DataSet ds = null;
        public virtual DataTable DropReasonsTable()
        {
            const string TableName = "drop_reasons";

            if (ds == null)
            {
                ds = new DataSet("ds");
            }

            DataTable tbl = ds.Tables[TableName];

            // if the table is not found then read the information from the database
            if (tbl == null)
            {
                DebtPlus.Repository.LookupTables.DropReasons.GetAll(ds, TableName);
                tbl = ds.Tables[TableName];
            }

            return tbl;
        }

        /// <summary>
        /// Obtain a description for the item
        /// </summary>
        public virtual string GetDrop_ReasonDescription(object key)
        {
            string answer = string.Empty;

            if (key != null && !object.ReferenceEquals(key, DBNull.Value))
            {
                DataTable tbl = DropReasonsTable();
                if (tbl != null)
                {
                    DataRow row = tbl.Rows.Find(key);
                    if (row != null)
                    {
                        if (!object.ReferenceEquals(row["description"], DBNull.Value))
                        {
                            answer = Convert.ToString(row["description"]).Trim();
                        }
                    }
                }
            }

            return answer;
        }
    }
}