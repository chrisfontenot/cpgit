#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Debt
{
    public class DebtUpdateDebtRecord : DebtRecord
    {

        public DebtUpdateDebtRecord()
            : base()
        {
        }
    }

    public class DebtUpdateRecordList : BaseDebtList
    {
        public DebtUpdateRecordList()
            : base(typeof(DebtUpdateDebtRecord))
        {
        }

        /// <summary>
        /// Read the "list" of debts, but this only consists of a single debt
        /// </summary>

        private string SelectClause;

        public void ReadDebtsTable(string selectClause)
        {
            // Save the select clause for the override later.
            this.SelectClause = selectClause;

            // Read the client as if we were reading ClientId 1. This is not really the
            // case since the select clause is overridden by this class.
            base.ReadDebtsTable(1);

            // Correct the client from the "1" that we simply hard-coded here.
            if (Count > 0)
            {
                DebtListCurrentStorage.ClientId = ((DebtPlus.Interfaces.Debt.IDebtRecord)this[0]).ClientId;
            }
        }

        /// <summary>
        /// Replacement routine to read a single debt into this table
        /// </summary>
        protected override SqlCommand view_debt_info_Select(Int32 client)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = string.Format("SELECT [account_number],[balance_verify_by],[balance_verify_date],[check_payments],[client],[client_creditor],[client_name],[creditor_name],[contact_name],[created_by],[date_created],[date_disp_changed],[disbursement_factor],[dmp_interest],[dmp_payout_interest],[drop_date],[drop_reason],[drop_reason_sent],[expected_payout_date],[fairshare_pct_check],[fairshare_pct_eft],[hold_disbursements],[interest_this_creditor],[irs_form_on_file],[last_communication],[last_payment_date_b4_dmp],[last_stmt_date],[line_number],[message],[months_delinquent],[non_dmp_interest],[non_dmp_payment],[orig_dmp_payment],[payments_this_creditor],[percent_balance],[person],[prenote_date],[priority],[IsActive],[returns_this_creditor],[sched_payment],[send_bal_verify],[send_drop_notice],[start_date],[student_loan_release],[rpps_client_type_indicator],[terms],[verify_request_date],[balance_verification_release],[client_creditor_proposal],[proposal_balance],[proposal_status],[client_creditor_balance],[balance_client_creditor],[current_sched_payment],[orig_balance],[orig_balance_adjustment],[payments_month_0],[payments_month_1],[total_interest],[total_payments],[total_sched_payment],[creditor_interest],[creditor],[cr_creditor_name],[cr_division_name],[cr_payment_balance],[cr_min_accept_amt],[cr_min_accept_pct],[ccl_zero_balance],[ccl_prorate],[ccl_always_disburse],[ccl_agency_account],[first_payment],[first_payment_date],[first_payment_amt],[first_payment_type],[last_payment],[last_payment_date],[last_payment_amt],[last_payment_type],[payment_rpps_mask],[rpps_mask] FROM view_debt_info WHERE {0}", SelectClause);
            cmd.CommandType = CommandType.Text;
            return cmd;
        }
    }
}