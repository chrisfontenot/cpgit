#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Debt
{
    public abstract partial class DAL_DebtRecord : DebtPlus.Interfaces.Debt.IDebtRecord, System.ComponentModel.INotifyPropertyChanged, System.ComponentModel.ISupportInitialize, DebtPlus.Interfaces.Debt.INotifyDataChanged, System.IDisposable
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        public event DebtPlus.Events.DataChangedEventHandler DataChanged;

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <remarks></remarks>
        public DAL_DebtRecord()
            : base()
        {
            currentStorage = new RecordStorage();
            originalStorage = new RecordStorage();
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="Source">Pointer to the original storage definition for a clone operation</param>
        /// <remarks></remarks>
        public DAL_DebtRecord(DAL_DebtRecord source)
            : base()
        {
            currentStorage = (RecordStorage)source.currentStorage.Clone();
            originalStorage = (RecordStorage)source.currentStorage.Clone();
        }

        #region "Parent"

        /// <summary>
        /// Pointer to the list for the items in the list
        /// </summary>
        private DebtPlus.Interfaces.Debt.IDebtRecordList privateParent = null;
        public virtual DebtPlus.Interfaces.Debt.IDebtRecordList Parent
        {
            get { return privateParent; }
            set
            {
                privateParent = value;
                currentStorage.IsDetached = value != null;
            }
        }
        #endregion

        #region "PropertyChanged"
        /// <summary>
        /// Interface called only from the grid update once we are in the proper thread. This will trip the grid to
        /// update the current row with the current values.
        /// </summary>
        /// <remarks></remarks>
        public virtual void RaisePropertyChanged(string PropertyName)
        {
            if (inInit == 0)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
                }
            }
        }
        #endregion

        #region "IDataChanged"
        protected void RaiseDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            if (inInit == 0)
            {
                try
                {
                    if (DataChanged != null)
                    {
                        DataChanged(this, e);
                    }

                    // Raise the PropertyChanged event if the item was not handled by the event.
                    if (!e.Handled)
                    {
                        RaisePropertyChanged(e.PropertyName);
                    }
                }
                catch
                {
                }
            }
        }

        protected virtual void OnDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            RaiseDataChanged(e);
        }

        protected void OnDataChanged(string propertyName)
        {
            OnDataChanged(new DebtPlus.Events.DataChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref object oldValue, object newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;
            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref bool oldValue, bool newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;
            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref string oldValue, string newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;

            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref double oldValue, double newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;

            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref Int32 oldValue, Int32 newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;
            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref decimal oldValue, decimal newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;
            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }

        /// <summary>
        /// Change the property value for a dataum
        /// </summary>
        /// <param name="propertyName">Name of the property being modified</param>
        /// <param name="oldValue">Previous (current) value of the datum</param>
        /// <param name="newValue">new value for the datum</param>
        /// <remarks></remarks>
        protected virtual bool SetProperty(string propertyName, ref DateTime oldValue, DateTime newValue)
        {
            bool answer = DebtPlus.Repository.Common.Compare(oldValue, newValue, true) != 0;
            if (answer)
            {
                oldValue = newValue;
                currentStorage.IsModified = true;
                OnDataChanged(propertyName);
            }

            return answer;
        }
        #endregion

        /// <summary>
        /// Type of the debt
        /// </summary>
        public virtual DebtPlus.Interfaces.Debt.DebtTypeEnum DebtType
        {
            get { return currentStorage.DebtType; }
            set
            {
                currentStorage.DebtType = value;
                originalStorage.DebtType = value;
            }
        }

        /// <summary>
        /// Determine the type of the debt
        /// </summary>

        public virtual void SetDebtType(DebtPlus.Interfaces.Debt.SpecialCreditors specialCreditorList)
        {
            // Look for the cushion creditor. We recognize only the entered name.
            // These creditors don't have an ID since they are not coded.
            if (creditor_name != null && string.Compare(creditor_name, "payment cushion", true) == 0)
            {
                DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.PaymentCushion;
                return;
            }

            // Look for the setup creditor account name
            if (string.Compare(Creditor, specialCreditorList.SetupCreditor, true) == 0)
            {
                DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee;
                return;
            }

            // If this is not an agency account then it is a normal creditor. Set it and leave.
            if (!ccl_agency_account)
            {
                DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal;
                return;
            }

            // Look at active items only from this point to find the other types

            if (IsActive)
            {
                // Look for the monthly fee creditor account name
                if (string.Compare(Creditor, specialCreditorList.MonthlyFeeCreditor, true) == 0)
                {
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee;
                    return;
                }

                // Look for the deduction creditor account name
                if (string.Compare(Creditor, specialCreditorList.DeductCreditor, true) == 0)
                {
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Fairshare;
                    return;
                }
            }

            // Assume that this is a general fee account
            DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.AgencyFee;
        }

        /// <summary>
        /// Minimum payment amount for this debt
        /// </summary>
        public virtual decimal MinimumPayment()
        {
            return MinimumPayment(current_balance);
        }

        public virtual decimal MinimumPayment(decimal currentBalance)
        {
            return Parent.MinimumPayment;
        }

        /// <summary>
        /// Maximum payment amount for this debt
        /// </summary>
        public virtual decimal MaximumPayment()
        {
            return MaximumPayment(current_balance);
        }

        public virtual decimal MaximumPayment(decimal currentBalance)
        {
            return Parent.MaximumPayment;
        }

        [DatabaseField("IsActive", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool IsActive
        {
            get
            {
                if (!currentStorage.IsActive)
                {
                    return false;
                }

                if (balance_client_creditor != client_creditor)
                {
                    return false;
                }

                return true;
            }
            set { SetProperty("IsActive", ref currentStorage.IsActive, value); }
        }

        [DatabaseField("client", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 ClientId
        {
            get { return currentStorage.ClientId; }
            set { SetProperty("ClientId", ref currentStorage.ClientId, value); }
        }

        [DatabaseField("creditor", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string Creditor
        {
            get { return currentStorage.Creditor; }
            set { SetProperty("Creditor", ref currentStorage.Creditor, value); }
        }

        [DatabaseField("client_creditor", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 client_creditor
        {
            get { return currentStorage.client_creditor; }
            set { SetProperty("client_creditor", ref currentStorage.client_creditor, value); }
        }

        /// <summary>
        /// Debt interface for the future use.
        /// </summary>
        public virtual Int32 Debt
        {
            get { return client_creditor; }
            set { client_creditor = value; }
        }

        public Int32 DebtId
        {
            get
            {
                return Debt;
            }
            set
            {
                Debt = value;
            }
        }

        // FIXED POINT FIELDS;
        [DatabaseField("balance_client_creditor", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 balance_client_creditor
        {
            get { return currentStorage.balance_client_creditor; }
            set { SetProperty("balance_client_creditor", ref currentStorage.balance_client_creditor, value); }
        }

        [DatabaseField("check_payments", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 check_payments
        {
            get { return currentStorage.check_payments; }
            set { SetProperty("check_payments", ref currentStorage.check_payments, value); }
        }

        [DatabaseField("client_creditor_balance", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 client_creditor_balance
        {
            get { return currentStorage.client_creditor_balance; }
            set { SetProperty("client_creditor_balance", ref currentStorage.client_creditor_balance, value); }
        }

        [DatabaseField("client_creditor_proposal", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 client_creditor_proposal
        {
            get { return currentStorage.client_creditor_proposal; }
            set { SetProperty("client_creditor_proposal", ref currentStorage.client_creditor_proposal, value); }
        }

        [DatabaseField("first_payment", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 first_payment
        {
            get { return currentStorage.first_payment; }
            set { SetProperty("first_payment", ref currentStorage.first_payment, value); }
        }

        [DatabaseField("last_payment", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 last_payment
        {
            get { return currentStorage.last_payment; }
            set { SetProperty("last_payment", ref currentStorage.last_payment, value); }
        }

        [DatabaseField("line_number", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 line_number
        {
            get { return currentStorage.line_number; }
            set { SetProperty("line_number", ref currentStorage.line_number, value); }
        }

        [DatabaseField("months_delinquent", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 months_delinquent
        {
            get { return currentStorage.months_delinquent; }
            set { SetProperty("months_delinquent", ref currentStorage.months_delinquent, value); }
        }

        [DatabaseField("person", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 person
        {
            get { return currentStorage.Person; }
            set { SetProperty("Person", ref currentStorage.Person, value); }
        }

        [DatabaseField("priority", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 priority
        {
            get { return currentStorage.priority; }
            set { SetProperty("priority", ref currentStorage.priority, value); }
        }

        [DatabaseField("proposal_status", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 proposal_status
        {
            get { return currentStorage.proposal_status; }
            set { SetProperty("proposal_status", ref currentStorage.proposal_status, value); }
        }

        [DatabaseField("send_bal_verify", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 send_bal_verify
        {
            get { return currentStorage.send_bal_verify; }
            set { SetProperty("send_bal_verify", ref currentStorage.send_bal_verify, value); }
        }

        [DatabaseField("terms", DatabaseFieldAttribute.FieldType.TypeInt32)]
        public virtual Int32 terms
        {
            get { return currentStorage.terms; }
            set { SetProperty("terms", ref currentStorage.terms, value); }
        }

        // MONITARY FIELDS;
        [DatabaseField("cr_min_accept_amt", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal cr_min_accept_amt
        {
            get { return currentStorage.cr_min_accept_amt; }
            set { SetProperty("cr_min_accept_amt", ref currentStorage.cr_min_accept_amt, value); }
        }

        [DatabaseField("current_sched_payment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal current_sched_payment
        {
            get { return currentStorage.current_sched_payment; }
            set { SetProperty("current_sched_payment", ref currentStorage.current_sched_payment, value); }
        }

        [DatabaseField("disbursement_factor", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal disbursement_factor
        {
            get { return currentStorage.disbursement_factor; }
            set { SetProperty("disbursement_factor", ref currentStorage.disbursement_factor, value); }
        }

        public virtual decimal display_disbursement_factor
        {
            get { return disbursement_factor; }
            set { disbursement_factor = value; }
        }

        [DatabaseField("first_payment_amt", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal first_payment_amt
        {
            get { return currentStorage.first_payment_amt; }
            set { SetProperty("first_payment_amt", ref currentStorage.first_payment_amt, value); }
        }

        [DatabaseField("interest_this_creditor", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal interest_this_creditor
        {
            get { return currentStorage.interest_this_creditor; }
            set { SetProperty("interest_this_creditor", ref currentStorage.interest_this_creditor, value); }
        }

        [DatabaseField("last_payment_amt", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal last_payment_amt
        {
            get { return currentStorage.last_payment_amt; }
            set { SetProperty("last_payment_amt", ref currentStorage.last_payment_amt, value); }
        }

        [DatabaseField("last_stmt_balance", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal last_stmt_balance
        {
            get { return currentStorage.last_stmt_balance; }
            set { SetProperty("last_stmt_balance", ref currentStorage.last_stmt_balance, value); }
        }

        [DatabaseField("non_dmp_payment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal non_dmp_payment
        {
            get { return currentStorage.non_dmp_payment; }
            set { SetProperty("non_dmp_payment", ref currentStorage.non_dmp_payment, value); }
        }

        [DatabaseField("orig_balance", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal orig_balance
        {
            get { return currentStorage.orig_balance; }
            set
            {
                if (SetProperty("orig_balance", ref currentStorage.orig_balance, value))
                {
                    OnDataChanged("display_orig_balance");
                    OnDataChanged("display_disbursement_factor");
                    OnDataChanged("display_current_balance");
                }
            }
        }

        [DatabaseField("orig_balance_adjustment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal orig_balance_adjustment
        {
            get { return currentStorage.orig_balance_adjustment; }
            set { SetProperty("orig_balance_adjustment", ref currentStorage.orig_balance_adjustment, value); }
        }

        [DatabaseField("orig_dmp_payment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal orig_dmp_payment
        {
            get { return currentStorage.orig_dmp_payment; }
            set { SetProperty("orig_dmp_payment", ref currentStorage.orig_dmp_payment, value); }
        }

        [DatabaseField("payments_month_0", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal payments_month_0
        {
            get { return currentStorage.payments_month_0; }
            set { SetProperty("payments_month_0", ref currentStorage.payments_month_0, value); }
        }

        [DatabaseField("payments_month_1", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal payments_month_1
        {
            get { return currentStorage.payments_month_1; }
            set { SetProperty("payments_month_1", ref currentStorage.payments_month_1, value); }
        }

        [DatabaseField("payments_this_creditor", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal payments_this_creditor
        {
            get { return currentStorage.payments_this_creditor; }
            set { SetProperty("payments_this_creditor", ref currentStorage.payments_this_creditor, value); }
        }

        [DatabaseField("proposal_balance", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal proposal_balance
        {
            get { return currentStorage.proposal_balance; }
            set { SetProperty("proposal_balance", ref currentStorage.proposal_balance, value); }
        }

        [DatabaseField("returns_this_creditor", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal returns_this_creditor
        {
            get { return currentStorage.returns_this_creditor; }
            set { SetProperty("returns_this_creditor", ref currentStorage.returns_this_creditor, value); }
        }

        [DatabaseField("sched_payment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal sched_payment
        {
            get { return currentStorage.sched_payment; }
            set { SetProperty("sched_payment", ref currentStorage.sched_payment, value); }
        }

        [DatabaseField("total_interest", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal total_interest
        {
            get { return currentStorage.total_interest; }
            set { SetProperty("total_interest", ref currentStorage.total_interest, value); }
        }

        [DatabaseField("total_payments", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal total_payments
        {
            get { return currentStorage.total_payments; }
            set { SetProperty("total_payments", ref currentStorage.total_payments, value); }
        }

        [DatabaseField("total_sched_payment", DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public virtual decimal total_sched_payment
        {
            get { return currentStorage.total_sched_payment; }
            set { SetProperty("total_sched_payment", ref currentStorage.total_sched_payment, value); }
        }

        // PERCENTAGE FIELDS;
        [DatabaseField("cr_min_accept_pct", DatabaseFieldAttribute.FieldType.TypeDouble)]
        public virtual double cr_min_accept_pct
        {
            get { return currentStorage.cr_min_accept_pct; }
            set { SetProperty("cr_min_accept_pct", ref currentStorage.cr_min_accept_pct, value); }
        }

        [DatabaseField("fairshare_pct_check", DatabaseFieldAttribute.FieldType.TypeDouble)]
        public virtual object fairshare_pct_check
        {
            get { return currentStorage.fairshare_pct_check; }
            set { SetProperty("fairshare_pct_check", ref currentStorage.fairshare_pct_check, value); }
        }

        [DatabaseField("fairshare_pct_eft", DatabaseFieldAttribute.FieldType.TypeDouble)]
        public virtual object fairshare_pct_eft
        {
            get { return currentStorage.fairshare_pct_eft; }
            set { SetProperty("fairshare_pct_eft", ref currentStorage.fairshare_pct_eft, value); }
        }

        [DatabaseField("non_dmp_interest", DatabaseFieldAttribute.FieldType.TypeDouble)]
        public virtual double non_dmp_interest
        {
            get { return currentStorage.non_dmp_interest; }
            set { SetProperty("non_dmp_interest", ref currentStorage.non_dmp_interest, value); }
        }

        [DatabaseField("percent_balance", DatabaseFieldAttribute.FieldType.TypeDouble)]
        public virtual double percent_balance
        {
            get { return currentStorage.percent_balance; }
            set { SetProperty("percent_balance", ref currentStorage.percent_balance, value); }
        }

        // string FIELDS
        [DatabaseField("account_number", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string account_number
        {
            get { return currentStorage.account_number; }
            set { SetProperty("account_number", ref currentStorage.account_number, value); }
        }

        [DatabaseField("balance_verify_by", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string balance_verify_by
        {
            get { return currentStorage.balance_verify_by; }
            set { SetProperty("balance_verify_by", ref currentStorage.balance_verify_by, value); }
        }

        [DatabaseField("client_name", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string client_name
        {
            get { return currentStorage.client_name; }
            set { SetProperty("client_name", ref currentStorage.client_name, value); }
        }

        [DatabaseField("contact_name", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string contact_name
        {
            get { return currentStorage.contact_name; }
            set { SetProperty("contact_name", ref currentStorage.contact_name, value); }
        }

        [DatabaseField("cr_creditor_name", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string cr_creditor_name
        {
            get { return currentStorage.cr_creditor_name; }
            set { SetProperty("cr_creditor_name", ref currentStorage.cr_creditor_name, value); }
        }

        [DatabaseField("cr_division_name", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string cr_division_name
        {
            get { return currentStorage.cr_division_name; }
            set { SetProperty("cr_division_name", ref currentStorage.cr_division_name, value); }
        }

        [DatabaseField("cr_payment_balance", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string cr_payment_balance
        {
            get { return currentStorage.cr_payment_balance; }
            set { SetProperty("cr_payment_balance", ref currentStorage.cr_payment_balance, value); }
        }

        [DatabaseField("created_by", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string created_by
        {
            get { return currentStorage.created_by; }
            set { SetProperty("created_by", ref currentStorage.created_by, value); }
        }

        [DatabaseField("creditor_name", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string creditor_name
        {
            get { return currentStorage.creditor_name; }
            set { SetProperty("creditor_name", ref currentStorage.creditor_name, value); }
        }

        [DatabaseField("first_payment_type", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string first_payment_type
        {
            get { return currentStorage.first_payment_type; }
            set { SetProperty("first_payment_type", ref currentStorage.first_payment_type, value); }
        }

        [DatabaseField("last_communication", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string last_communication
        {
            get { return currentStorage.last_communication; }
            set { SetProperty("last_communication", ref currentStorage.last_communication, value); }
        }

        [DatabaseField("last_payment_type", DatabaseFieldAttribute.FieldType.TypeString)]
        public virtual string last_payment_type
        {
            get { return currentStorage.last_payment_type; }
            set { SetProperty("last_payment_type", ref currentStorage.last_payment_type, value); }
        }

        // FLAG FIELDS
        [DatabaseField("ccl_agency_account", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool ccl_agency_account
        {
            get { return currentStorage.ccl_agency_account; }
            set { SetProperty("ccl_agency_account", ref currentStorage.ccl_agency_account, value); }
        }

        [DatabaseField("ccl_always_disburse", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool ccl_always_disburse
        {
            get { return currentStorage.ccl_always_disburse; }
            set { SetProperty("ccl_always_disburse", ref currentStorage.ccl_always_disburse, value); }
        }

        [DatabaseField("ccl_prorate", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool ccl_prorate
        {
            get { return currentStorage.ccl_prorate; }
            set { SetProperty("ccl_prorate", ref currentStorage.ccl_prorate, value); }
        }

        [DatabaseField("ccl_zero_balance", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool ccl_zero_balance
        {
            get { return currentStorage.ccl_zero_balance; }
            set { SetProperty("ccl_zero_balance", ref currentStorage.ccl_zero_balance, value); }
        }

        [DatabaseField("hold_disbursements", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool hold_disbursements
        {
            get { return currentStorage.hold_disbursements; }
            set { SetProperty("hold_disbursements", ref currentStorage.hold_disbursements, value); }
        }

        [DatabaseField("irs_form_on_file", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool irs_form_on_file
        {
            get { return currentStorage.irs_form_on_file; }
            set { SetProperty("irs_form_on_file", ref currentStorage.irs_form_on_file, value); }
        }

        [DatabaseField("send_drop_notice", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool send_drop_notice
        {
            get { return currentStorage.send_drop_notice; }
            set { SetProperty("send_drop_notice", ref currentStorage.send_drop_notice, value); }
        }

        [DatabaseField("student_loan_release", DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public virtual bool student_loan_release
        {
            get { return currentStorage.student_loan_release; }
            set { SetProperty("student_loan_release", ref currentStorage.student_loan_release, value); }
        }

        // DATE/TIME FIELDS
        [DatabaseField("date_created", DatabaseFieldAttribute.FieldType.TypeDate)]
        public virtual DateTime date_created
        {
            get { return currentStorage.date_created; }
            set { SetProperty("date_created", ref currentStorage.date_created, value); }
        }

        // OTHERS, INCLUDING DATE/TIME WHICH ARE NULLABLE (DBNULL)
        [DatabaseField("balance_verification_release")]
        public virtual object balance_verification_release
        {
            get { return currentStorage.balance_verification_release; }
            set { SetProperty("balance_verification_release", ref currentStorage.balance_verification_release, value); }
        }

        [DatabaseField("balance_verify_date")]
        public virtual object balance_verify_date
        {
            get { return currentStorage.balance_verify_date; }
            set { SetProperty("balance_verify_date", ref currentStorage.balance_verify_date, value); }
        }

        /// <summary>
        /// Interest figure associated with the creditor
        /// </summary>
        [DatabaseField("creditor_interest")]
        public virtual object creditor_interest
        {
            get { return currentStorage.creditor_interest; }
            set { SetProperty("creditor_interest", ref currentStorage.creditor_interest, value); }
        }

        /// <summary>
        /// Date when the disbursement factor was last changed.
        /// </summary>
        [DatabaseField("date_disp_changed")]
        public virtual object date_disp_changed
        {
            get { return currentStorage.date_disp_changed; }
            set { SetProperty("date_disp_changed", ref currentStorage.date_disp_changed, value); }
        }

        /// <summary>
        /// Date when the proposal was prenoted.
        /// </summary>
        [DatabaseField("date_proposal_prenoted")]
        public virtual object date_proposal_prenoted
        {
            get { return currentStorage.date_proposal_prenoted; }
            set { SetProperty("date_proposal_prenoted", ref currentStorage.date_proposal_prenoted, value); }
        }

        /// <summary>
        /// Interest rate associated with this debt. It overrides the creditor's normal rate.
        /// </summary>
        [DatabaseField("dmp_interest")]
        public virtual object dmp_interest
        {
            get { return currentStorage.dmp_interest; }
            set { SetProperty("dmp_interest", ref currentStorage.dmp_interest, value); }
        }

        /// <summary>
        /// Interest figure used on the payout report. This is normally set before the debt is fully
        /// coded and accepted and is used by the payout to calculate proposed interest figures.
        /// </summary>
        [DatabaseField("dmp_payout_interest")]
        public virtual object dmp_payout_interest
        {
            get { return currentStorage.dmp_payout_interest; }
            set { SetProperty("dmp_payout_interest", ref currentStorage.dmp_payout_interest, value); }
        }

        /// <summary>
        /// Date when the debt was officially dropped
        /// </summary>
        [DatabaseField("drop_date")]
        public virtual object drop_date
        {
            get { return currentStorage.drop_date; }
            set { SetProperty("drop_date", ref currentStorage.drop_date, value); }
        }

        /// <summary>
        /// Reason for dropping this debt
        /// </summary>
        [DatabaseField("drop_reason")]
        public virtual object drop_reason
        {
            get { return currentStorage.drop_reason; }
            set { SetProperty("drop_reason", ref currentStorage.drop_reason, value); }
        }

        /// <summary>
        /// Date when the debt was dropped and the drop reason set
        /// </summary>
        [DatabaseField("drop_reason_date")]
        public virtual object drop_reason_date
        {
            get { return currentStorage.drop_reason_date; }
            set { SetProperty("drop_reason_date", ref currentStorage.drop_reason_date, value); }
        }

        /// <summary>
        /// Date when the drop reason was sent to the creditor
        /// </summary>
        [DatabaseField("drop_reason_sent")]
        public virtual object drop_reason_sent
        {
            get { return currentStorage.drop_reason_sent; }
            set { SetProperty("drop_reason_sent", ref currentStorage.drop_reason_sent, value); }
        }

        /// <summary>
        /// Expected payout date for paying off this debt according to the payment plan
        /// </summary>
        [DatabaseField("expected_payout_date")]
        public virtual object expected_payout_date
        {
            get { return currentStorage.expected_payout_date; }
            set { SetProperty("expected_payout_date", ref currentStorage.expected_payout_date, value); }
        }

        /// <summary>
        /// Date of the first payment for this debt
        /// </summary>
        [DatabaseField("first_payment_date")]
        public virtual object first_payment_date
        {
            get { return currentStorage.first_payment_date; }
            set { SetProperty("first_payment_date", ref currentStorage.first_payment_date, value); }
        }

        /// <summary>
        /// Date of the last payment for this debt
        /// </summary>
        [DatabaseField("last_payment_date")]
        public virtual object last_payment_date
        {
            get { return currentStorage.last_payment_date; }
            set { SetProperty("last_payment_date", ref currentStorage.last_payment_date, value); }
        }

        /// <summary>
        /// Date of the last payment before starting the DMP program
        /// </summary>
        [DatabaseField("last_payment_date_b4_dmp")]
        public virtual object last_payment_date_b4_dmp
        {
            get { return currentStorage.last_payment_date_b4_dmp; }
            set { SetProperty("last_payment_date_b4_dmp", ref currentStorage.last_payment_date_b4_dmp, value); }
        }

        /// <summary>
        /// Date when the client's statement was last examined
        /// </summary>
        [DatabaseField("last_stmt_date")]
        public virtual object last_stmt_date
        {
            get { return currentStorage.last_stmt_date; }
            set { SetProperty("last_stmt_date", ref currentStorage.last_stmt_date, value); }
        }

        /// <summary>
        /// General message field to replace the account number on the display
        /// </summary>
        [DatabaseField("message")]
        public virtual object Message
        {
            get { return currentStorage.message; }
            set { SetProperty("message", ref currentStorage.message, value); }
        }

        /// <summary>
        /// Date when the debt was prenoted on EFT
        /// </summary>
        [DatabaseField("prenote_date")]
        public virtual object prenote_date
        {
            get { return currentStorage.prenote_date; }
            set { SetProperty("prenote_date", ref currentStorage.prenote_date, value); }
        }

        /// <summary>
        /// Debt EFT client type indicator
        /// </summary>
        [DatabaseField("rpps_client_type_indicator")]
        public virtual object rpps_client_type_indicator
        {
            get { return currentStorage.rpps_client_type_indicator; }
            set { SetProperty("rpps_client_type_indicator", ref currentStorage.rpps_client_type_indicator, value); }
        }

        /// <summary>
        /// Pointer to the EFT payment information
        /// </summary>
        [DatabaseField("rpps_mask")]
        public virtual object rpps_mask
        {
            get { return currentStorage.rpps_mask; }
            set { SetProperty("rpps_mask", ref currentStorage.rpps_mask, value); }
        }

        /// <summary>
        /// Pointer to the EFT payment mask record associated with this debt
        /// </summary>
        [DatabaseField("payment_rpps_mask")]
        public virtual object payment_rpps_mask
        {
            get { return currentStorage.payment_rpps_mask; }
            set { SetProperty("payment_rpps_mask", ref currentStorage.payment_rpps_mask, value); }
        }

        /// <summary>
        /// Date when payments are to start
        /// </summary>
        [DatabaseField("start_date")]
        public virtual object start_date
        {
            get { return currentStorage.start_date; }
            set { SetProperty("start_date", ref currentStorage.start_date, value); }
        }

        /// <summary>
        /// Date when the balance verification was received and updated
        /// </summary>
        [DatabaseField("verify_request_date")]
        public virtual object verify_request_date
        {
            get { return currentStorage.verify_request_date; }
            set { SetProperty("verify_request_date", ref currentStorage.verify_request_date, value); }
        }

        /// <summary>
        /// Determine the current status for the debt
        /// </summary>
        public virtual DataRowState DebtStatus
        {
            get
            {
                // There must be a parent to have a status
                if (privateParent == null)
                {
                    return DataRowState.Detached;
                }

                // If the record is new and not saved then it is added
                if (IsNew)
                {
                    return DataRowState.Added;
                }

                // If we changed anything then it is modified
                if (currentStorage.IsModified)
                {
                    return DataRowState.Modified;
                }

                // The default is not changed
                return DataRowState.Unchanged;
            }
        }

        protected bool privateIsEdit = false;
        /// <summary>
        /// Determine if the row is being edited
        /// </summary>
        public virtual bool IsEdit
        {
            get { return privateIsEdit; }
        }

        protected bool privateIsNew = true;
        /// <summary>
        /// Determine if the row is added or existed in table previously
        /// </summary>
        public virtual bool IsNew
        {
            get { return privateIsNew; }
        }

        /// <summary>
        /// Current balance information
        /// </summary>
        public virtual decimal current_balance
        {
            get { return orig_balance + orig_balance_adjustment + total_interest - total_payments; }
        }

        #region "GetSend_Bal_Verify"

        /// <summary>
        /// Balance verification status types
        /// </summary>
        public virtual DataTable BalanceVerificationStatusTypesTable()
        {
            const string TableName = "BalanceVerificationStatusTypes";

            if (ds == null)
            {
                ds = new DataSet("ds");
            }

            DataTable tbl = ds.Tables[TableName];

            if (tbl == null)
            {
                lock (ds)
                {
                    tbl = new DataTable(TableName);
                    DataColumn newCol = new DataColumn("oID", typeof(Int32));
                    newCol.Caption = "ID";
                    tbl.Columns.Add(newCol);
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns[0] };

                    newCol = new DataColumn("description", typeof(string));
                    newCol.Caption = "Description";
                    newCol.MaxLength = 50;
                    tbl.Columns.Add(newCol);

                    newCol = new DataColumn("SuspendProposals", typeof(bool));
                    newCol.Caption = "Suspend Proposals";
                    tbl.Columns.Add(newCol);

                    // Add the rows to the table
                    tbl.Rows.Add(new object[] { 0, "Normal", false });
                    tbl.Rows.Add(new object[] { 1, "Request Balance Update", false });
                    tbl.Rows.Add(new object[] { 2, "Must Send Prenote BAL", true });
                    tbl.Rows.Add(new object[] { 3, "Prenote BAL Sent", true });
                    tbl.Rows.Add(new object[] { 4, "Prenote BAL Failed", true });
                    tbl.Rows.Add(new object[] { 5, "Prenote BAL Success", false });

                    // Add the table to the dataset
                    ds.Tables.Add(tbl);
                }
            }

            return tbl;
        }

        /// <summary>
        /// Obtain a description for the item
        /// </summary>
        public virtual string GetSend_Bal_Verify_Description(object key)
        {
            string answer = string.Empty;

            if (key != null && !object.ReferenceEquals(key, DBNull.Value))
            {
                DataTable tbl = BalanceVerificationStatusTypesTable();
                if (tbl != null)
                {
                    DataRow row = tbl.Rows.Find(key);
                    if (row != null)
                    {
                        if (!object.ReferenceEquals(row["description"], DBNull.Value))
                        {
                            answer = Convert.ToString(row["description"]).Trim();
                        }
                    }
                }
            }

            return answer;
        }
        #endregion

        #region "IDisposable Support"

        /// <summary>
        /// Dispose of the current object
        /// </summary>
        // To detect redundant calls
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    disposedValue = true;
                    currentStorage.Dispose();
                    if (previousStorage != null)
                    {
                        previousStorage.Dispose();
                    }

                    if (originalStorage != null)
                    {
                        originalStorage.Dispose();
                    }

                    if (ds != null)
                    {
                        ds.Dispose();
                        ds = null;
                    }
                }

                previousStorage = null;
                currentStorage = null;
                originalStorage = null;
            }
        }

        /// <summary>
        /// Dispose of the storage
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finailize the disposal
        /// </summary>
        ~DAL_DebtRecord()
        {
            Dispose(false);
        }

        #endregion

        #region "ISupportInitialize"

        /// <summary>
        /// Do any post-processing after the initialization sequence
        /// </summary>
        protected virtual void EndOfInit()
        {
            originalStorage = new RecordStorage(currentStorage);
        }

        /// <summary>
        /// Support the BeginInit and EndInit sequences
        /// </summary>
        public virtual void BeginInit()
        {
            inInit += 1;
        }

        public virtual void EndInit()
        {
            if (inInit <= 1)
            {
                EndOfInit();
                inInit = 1;
            }
            inInit -= 1;
        }
        #endregion
    }
}