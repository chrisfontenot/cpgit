#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Debt
{
    partial class DAL_DebtRecord : DebtPlus.Interfaces.Debt.IDebtRecordLoad
    {

        /// <summary>
        /// Load the record
        /// </summary>
        /// <param name="rdr"></param>
        public void LoadRecord(System.Type RecordType, ref IDataReader rdr)
        {
            for (Int32 indx = rdr.FieldCount - 1; indx >= 0; indx += -1)
            {
                if (!rdr.IsDBNull(indx))
                {
                    ProcessField(RecordType, rdr.GetName(indx), rdr.GetValue(indx));
                }
            }
        }

        /// <summary>
        /// Load the record
        /// </summary>
        /// <param name="row"></param>
        public void LoadRecord(System.Type RecordType, ref DataRow row)
        {
            DataTable tbl = row.Table;
            foreach (DataColumn col in tbl.Columns)
            {
                if (!row.IsNull(col))
                {
                    ProcessField(RecordType, col.ColumnName, row[col]);
                }
            }
        }

        /// <summary>
        /// Set the field value in the debt record to the passed parameter.
        /// </summary>
        /// <param name="RecordType"></param>
        /// <param name="FieldName"></param>
        /// <param name="objValue"></param>

        private void ProcessField(System.Type RecordType, string FieldName, object objValue)
        {
            // Process the properties in the current row
            System.Reflection.PropertyInfo[] properties = RecordType.GetProperties();
            foreach (System.Reflection.PropertyInfo propertyField in properties)
            {
                object[] propertyAttributeList = propertyField.GetCustomAttributes(typeof(DatabaseFieldAttribute), true);
                if (propertyAttributeList != null && propertyAttributeList.Length == 1)
                {
                    DatabaseFieldAttribute customAttr = propertyAttributeList[0] as DatabaseFieldAttribute;
                    if (customAttr != null)
                    {
                        string propertyFieldName = customAttr.ColumnName;
                        DatabaseFieldAttribute.FieldType TypeOfField = customAttr.TypeOfField;

                        // Per SQL, ignore the case of the column name. If the database is case sensitive, we need to change this.
                        if (string.Compare(FieldName, propertyFieldName, true, System.Globalization.CultureInfo.InvariantCulture) == 0)
                        {
                            // If the item is not an object then convert the data to the appropriate type
                            switch (TypeOfField)
                            {
                                case DatabaseFieldAttribute.FieldType.TypeObject:
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeInt32:
                                    objValue = Convert.ToInt32(objValue);
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeBoolean:
                                    objValue = Convert.ToBoolean(objValue);
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeDate:
                                    objValue = Convert.ToDateTime(objValue);
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeDecimal:
                                    objValue = Convert.ToDecimal(objValue);
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeDouble:
                                    objValue = Convert.ToDouble(objValue);
                                    break;
                                case DatabaseFieldAttribute.FieldType.TypeString:
                                    objValue = Convert.ToString(objValue);
                                    break;
                                default:
                                    break;
                            }

                            // Set the propertyField to the current column value
                            propertyField.SetValue(this, objValue, null);
                            break;
                        }
                    }
                }
            }
        }
    }
}