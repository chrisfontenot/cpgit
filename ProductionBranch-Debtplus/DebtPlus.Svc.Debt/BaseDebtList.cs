#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DebtPlus.Svc.Debt
{
    public partial class BaseDebtList : System.Collections.ArrayList, DebtPlus.Interfaces.Debt.IDebtRecordList, System.ComponentModel.ISupportInitialize, IDisposable
    {
        /// <summary>
        /// Arguments to the RecaulculateMonthlyFee event. The list holder should recalculate the monthly fee amount.
        /// </summary>
        /// <remarks></remarks>
        public sealed class RecalculateMonthlyFeeEventArgs : EventArgs
        {
            private readonly DebtPlus.Interfaces.Debt.IDebtRecord privateDebt;

            static RecalculateMonthlyFeeEventArgs emptyClass = null;
            /// <summary>
            /// Create a normal instance of our class
            /// </summary>
            /// <param name="Debt"></param>
            /// <remarks></remarks>
            public RecalculateMonthlyFeeEventArgs(DebtPlus.Interfaces.Debt.IDebtRecord debt)
                : base()
            {
                privateDebt = debt;
            }

            public RecalculateMonthlyFeeEventArgs()
            {
            }

            /// <summary>
            /// Pointer to the debt that was changed. NULL if it is not known.
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public DebtPlus.Interfaces.Debt.IDebtRecord Debt
            {
                get { return privateDebt; }
            }

            /// <summary>
            /// return the empty RecaulculateEventArgs structure
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public static new RecalculateMonthlyFeeEventArgs Empty
            {
                get
                {
                    if (emptyClass == null)
                    {
                        emptyClass = new RecalculateMonthlyFeeEventArgs();
                    }
                    return emptyClass;
                }
            }
        }

        // event for the DataChanged items in the list
        public event DebtPlus.Events.DataChangedEventHandler DataChanged;

        /// <summary>
        /// Signal the DataChanged event but allow it to be overridden
        /// </summary>
        /// <param name="e">DataChanged event arguments</param>
        /// <remarks></remarks>
        protected virtual void OnDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            if (DataChanged != null)
            {
                DataChanged(this, e);
            }
        }

        /// <summary>
        /// Raise the DataChanged event
        /// </summary>
        /// <param name="e">DataChanged event arguments</param>
        /// <remarks></remarks>
        public void RaiseDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            OnDataChanged(e);
        }

        public delegate void RecalculateMonthlyFeeEventHandler(object sender, RecalculateMonthlyFeeEventArgs e);
        public event RecalculateMonthlyFeeEventHandler RecalculateMonthlyFee;

        /// <summary>
        /// Signal the RecalculateMonthlyFee event but allow it to be overriden
        /// </summary>
        /// <param name="e">RecalculateMonthlyFeeEventArgs event arguments</param>
        /// <remarks></remarks>
        protected virtual void OnRecalculateMonthlyFee(RecalculateMonthlyFeeEventArgs e)
        {
            if (RecalculateMonthlyFee != null)
            {
                RecalculateMonthlyFee(this, e);
            }
        }

        /// <summary>
        /// Raise the RecalculateMonthlyFee event
        /// </summary>
        /// <param name="debt">Pointer to the debt that has been changed. Used in building the arguments.</param>
        /// <remarks></remarks>
        protected void RaiseRecalculateMonthlyFee(DebtPlus.Interfaces.Debt.IDebtRecord debt)
        {
            if (!InInit)
            {
                OnRecalculateMonthlyFee(new RecalculateMonthlyFeeEventArgs(debt));
            }
        }

        /// <summary>
        /// Raise the RecalculateMonthlyFee event
        /// </summary>
        /// <remarks></remarks>
        protected void RaiseRecalculateMonthlyFee()
        {
            if (!InInit)
            {
                OnRecalculateMonthlyFee(RecalculateMonthlyFeeEventArgs.Empty);
            }
        }

        /// <summary>
        /// Handle the record's DataChanged event to indicate that the field in the record has been modified.
        /// </summary>
        /// <param name="Sender">Pointer to the record that was changed</param>
        /// <param name="e">Arguments defining the field that was changed.</param>
        /// <remarks></remarks>
        private void RecordDataChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Pass along the changed event to the list owner
            RaiseDataChanged(new DebtPlus.Events.DataChangedEventArgs(sender, e.PropertyName));

            // We need an additional event if we should recalculate the monthly fee.
            switch (e.PropertyName)
            {
                case "disbursement_factor":
                case "orig_balance_adjustment":
                case "orig_balance":
                case "total_payments":
                case "total_interest":
                case "hold_disbursements":
                    if (((DebtPlus.Interfaces.Debt.IDebtRecord)sender).DebtType != DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee)
                    {
                        RaiseRecalculateMonthlyFee((DebtPlus.Interfaces.Debt.IDebtRecord)sender);
                    }
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// General storage for this class
        /// </summary>
        /// <remarks></remarks>
        public class DebtListStorage : ICloneable, IDisposable
        {
            /// <summary>
            /// Create a normal blank instance of our class
            /// </summary>
            /// <remarks></remarks>
            public DebtListStorage()
                : base()
            {
                SpecialCreditorList.DeductCreditor     = "Z9999";
                SpecialCreditorList.MonthlyFeeCreditor = "X0001";
                SpecialCreditorList.SetupCreditor      = string.Empty;
            }

            /// <summary>
            /// Create a normal instance of our class
            /// </summary>
            /// <param name="Source">Pointer to the current storage to clone</param>
            /// <remarks></remarks>
            public DebtListStorage(DebtListStorage source)
                : base()
            {
                PrivateID             = source.PrivateID;
                ItemTypes             = source.ItemTypes;
                ClientId              = source.ClientId;
                PrivateThreshold      = source.PrivateThreshold;
                PrivateMinimumPayment = source.PrivateMinimumPayment;

                // Copy the Creditor list
                SpecialCreditorList   = source.SpecialCreditorList;
            }

            /// <summary>
            /// Generate a new structure with the identical data as the source
            /// </summary>
            /// <returns></returns>
            /// <remarks></remarks>
            public object Clone()
            {
                return new DebtListStorage(this);
            }

            /// <summary>
            /// Generate a new ID sequence for the debt list
            /// </summary>
            /// <returns></returns>
            public static Int64 GenerateID()
            {
                return DateTime.Now.Ticks;
            }

            public Int64 PrivateID;
            public Type ItemTypes;
            public DebtPlus.Interfaces.Debt.SpecialCreditors SpecialCreditorList;
            public Int32 ClientId;
            public double PrivateThreshold = 0.021;
            public decimal PrivateMinimumPayment = 0m;

            #region "IDisposable Support"

            // To detect redundant calls;
            private bool disposedValue = false;

            // IDisposable
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    disposedValue = true;
                    if (disposing)
                    {
                    }
                }
            }

            public void Dispose()
            {
                // Implements IDisposable.Dispose
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            #endregion
        }

        public DebtListStorage DebtListCurrentStorage;
        #region "QueryConfigFeeValue"

        /// <summary>
        /// Ask our creator to help if needed before giving up
        /// </summary>
        public event DebtPlus.Events.ParameterValueEventHandler QueryConfigFeeValue;

        protected virtual void OnQueryConfigFeeValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            if (QueryConfigFeeValue != null)
            {
                QueryConfigFeeValue(this, e);
            }
        }

        protected object RaiseQueryConfigFeeValue(string name)
        {
            var e = new DebtPlus.Events.ParameterValueEventArgs(name);
            OnQueryConfigFeeValue(e);
            return e.Value;
        }
        #endregion

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public BaseDebtList(Type itemTypes)
            : base()
        {
            DebtListCurrentStorage           = new DebtListStorage();
            DebtListCurrentStorage.ItemTypes = itemTypes;
            DebtListCurrentStorage.PrivateID = DebtListStorage.GenerateID();
        }

        public BaseDebtList()
            : base()
        {
            DebtListCurrentStorage.PrivateID = DebtListStorage.GenerateID();
        }

        public BaseDebtList(Int32 capacity)
            : base(capacity)
        {
            DebtListCurrentStorage.PrivateID = DebtListStorage.GenerateID();
        }

        public BaseDebtList(ICollection c)
            : base(c)
        {
            DebtListCurrentStorage.PrivateID = DebtListStorage.GenerateID();
        }

        #region "ISupportInitialize"

        protected Int32 initCount = 0;
        public void BeginInit()
        {
            // Implements System.ComponentModel.ISupportInitialize.BeginInit
            initCount += 1;
        }

        public void EndInit()
        {
            // Implements System.ComponentModel.ISupportInitialize.EndInit
            if (initCount == 1)
            {
                initCount = 0;
            }
            else
            {
                initCount -= 1;
            }
        }

        protected bool InInit
        {
            get { return initCount > 0; }
        }
        #endregion

        /// <summary>
        /// ID of the debt list
        /// </summary>
        public virtual Int64 ID
        {
            get { return DebtListCurrentStorage.PrivateID; }
        }

        #region "CollectionChanged"

        /// <summary>
        /// Indicate when the collection was changed in item count
        /// </summary>
        public event EventHandler CollectionChanged;

        protected virtual void OnCollectionChanged(EventArgs e)
        {
            if (CollectionChanged != null)
            {
                CollectionChanged(this, e);
            }
        }

        protected void RaiseCollectionChanged()
        {
            if (!InInit)
            {
                OnCollectionChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Remove the debt record from the list
        /// </summary>
        public override void Remove(object obj)
        {
            base.Remove(obj);
            RaiseCollectionChanged();

            // After adding the item, recompute the monthly fee
            RaiseRecalculateMonthlyFee();
        }

        public override void RemoveAt(Int32 index)
        {
            base.RemoveAt(index);
            RaiseCollectionChanged();

            // After adding the item, recompute the monthly fee
            RaiseRecalculateMonthlyFee();
        }

        /// <summary>
        /// Remove A debt from the system
        /// </summary>
        public virtual void RemoveDebt(object obj)
        {
            Remove(obj);

            // After adding the item, recompute the monthly fee
            RaiseCollectionChanged();
            RaiseRecalculateMonthlyFee();
        }

        public virtual void RemoveDebtAt(Int32 index)
        {
            RemoveAt(index);

            // After adding the item, recompute the monthly fee
            RaiseCollectionChanged();
            RaiseRecalculateMonthlyFee();
        }

        /// <summary>
        /// Remove all items from the list
        /// </summary>
        public override void Clear()
        {
            if (base.Count > 0)
            {
                base.Clear();
                RaiseCollectionChanged();
            }
        }

        /// <summary>
        /// Add a collection of items to the list
        /// </summary>
        public override void AddRange(ICollection c)
        {
            foreach (object item in c)
            {
                Add(item);
            }
        }

        /// <summary>
        /// Overload the normal add function for the new debt record
        /// </summary>
        public override Int32 Add(object record)
        {
            Int32 ordinal = base.Add(record);

            // After adding, set the parent to the list.
            ((DebtPlus.Interfaces.Debt.IDebtRecord)record).Parent = this;

            // Indicate the list was changed
            RaiseCollectionChanged();

            // Hook into the property changed event handler to find notifications that we want to see
            ((DebtPlus.Interfaces.Debt.INotifyDataChanged)record).DataChanged += RecordDataChanged;

            // After adding the item, recompute the monthly fee
            RaiseRecalculateMonthlyFee();

            return ordinal;
        }
        #endregion

        /// <summary>
        /// Read the configuration information for our group of debts
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <remarks></remarks>
        public virtual void ReadConfigDebtInfo(SqlConnection cn, SqlTransaction txn)
        {
            var cnf = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
            if (cnf != null)
            {
                DebtListCurrentStorage.SpecialCreditorList.MonthlyFeeCreditor = cnf.paf_creditor;
                DebtListCurrentStorage.SpecialCreditorList.DeductCreditor     = cnf.deduct_creditor;
                DebtListCurrentStorage.SpecialCreditorList.SetupCreditor      = cnf.setup_creditor;
                Threshold                                                     = cnf.threshold;
                MinimumPayment                                                = cnf.payment_minimum.GetValueOrDefault(0M);
            }
        }

        /// <summary>
        /// Select command to read the debt information
        /// </summary>
        /// <param name="ClientId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual SqlCommand view_debt_info_Select(Int32 ClientId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT [account_number],[balance_verify_by],[balance_verify_date],[check_payments],[client],[client_creditor],[client_name],[creditor_name],[contact_name],[created_by],[date_created],[date_disp_changed],[disbursement_factor],[dmp_interest],[dmp_payout_interest],[drop_date],[drop_reason],[drop_reason_sent],[expected_payout_date],[fairshare_pct_check],[fairshare_pct_eft],[hold_disbursements],[interest_this_creditor],[irs_form_on_file],[last_communication],[last_payment_date_b4_dmp],[last_stmt_date],[line_number],[message],[months_delinquent],[non_dmp_interest],[non_dmp_payment],[orig_dmp_payment],[payments_this_creditor],[percent_balance],[person],[prenote_date],[priority],[IsActive],[returns_this_creditor],[sched_payment],[send_bal_verify],[send_drop_notice],[start_date],[student_loan_release],[rpps_client_type_indicator],[terms],[verify_request_date],[balance_verification_release],[client_creditor_proposal],[proposal_balance],[proposal_status],[client_creditor_balance],[balance_client_creditor],[current_sched_payment],[orig_balance],[orig_balance_adjustment],[payments_month_0],[payments_month_1],[total_interest],[total_payments],[total_sched_payment],[creditor_interest],[creditor],[cr_creditor_name],[cr_division_name],[cr_payment_balance],[cr_min_accept_amt],[cr_min_accept_pct],[ccl_zero_balance],[ccl_prorate],[ccl_always_disburse],[ccl_agency_account],[first_payment],[first_payment_date],[first_payment_amt],[first_payment_type],[last_payment],[last_payment_date],[last_payment_amt],[last_payment_type],[payment_rpps_mask],[rpps_mask] FROM [view_debt_info] WITH (NOLOCK) WHERE [Client]=@Client";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@Client", SqlDbType.Int).Value = ClientId;
            return cmd;
        }

        /// <summary>
        /// Read the list of debts for our collection
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <remarks></remarks>
        public virtual void ReadDebtInfo(SqlConnection cn, SqlTransaction txn, Int32 ClientId)
        {
            // Toss all of our rows so that we don't attempt to duplicate debts
            Clear();
            DebtListCurrentStorage.ClientId = ClientId;
            bool foundMonthlyFee = false;

            using (DataSet ds = new DataSet("ds"))
            {
                // Build the command to retrieve the data from the database
                using (var cmd = view_debt_info_Select(ClientId))
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "view_debt_info");
                    }
                }

                // Cache the pointer to the database table
                DataTable tbl = ds.Tables["view_debt_info"];

                // Read each row and create a new debt record
                IEnumerator ie = tbl.Rows.GetEnumerator();
                while (ie.MoveNext())
                {
                    DataRow row = (DataRow)ie.Current;
                    object obj = Activator.CreateInstance(DebtListCurrentStorage.ItemTypes);
                    DebtPlus.Interfaces.Debt.IDebtRecord record = (DebtPlus.Interfaces.Debt.IDebtRecord)obj;

                    // Start the initial loading of the data
                    if (record is System.ComponentModel.ISupportInitialize)
                    {
                        ((System.ComponentModel.ISupportInitialize)record).BeginInit();
                    }

                    // Read the definitions from the row and store the values into the class
                    ((DebtPlus.Interfaces.Debt.IDebtRecordLoad)record).LoadRecord(DebtListCurrentStorage.ItemTypes, ref row);

                    // Set the Creditor type before we test it
                    record.SetDebtType(DebtListCurrentStorage.SpecialCreditorList);

                    // Special logic here to prevent having more than one monthly fee.
                    // Change any monthly fee to "normal" and at the end, reset the last item to "monthly fee" again.
                    if (record.DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee)
                    {
                        if (!foundMonthlyFee)
                        {
                            foundMonthlyFee = true;
                        }
                        else
                        {
                            record.DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal;
                        }
                    }

                    // Add the record to the list
                    Add(record);

                    // Complete the initial loading of the data
                    if (record is System.ComponentModel.ISupportInitialize)
                    {
                        ((System.ComponentModel.ISupportInitialize)record).EndInit();
                    }
                }
            }
        }

        /// <summary>
        /// Read the debt table list
        /// </summary>
        /// <remarks></remarks>
        public virtual void ReadDebtsTable(Int32 ClientId)
        {
            // Find the special Creditor IDs from the config table for now.
            using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                cn.Open();
                ReadDebtsTable(cn, null, ClientId);
            }
        }

        /// <summary>
        /// Read the debt table list
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <remarks></remarks>
        public virtual void ReadDebtsTable(SqlConnection cn, SqlTransaction txn, Int32 ClientId)
        {
            // Read the confirmation information
            ReadConfigDebtInfo(cn, txn);

            // Read the list of debts
            ReadDebtInfo(cn, txn, ClientId);

            // Sort them according to the sort order defined in the debt structure
            Sort();
        }

        /// <summary>
        /// Determine the number of active creditors
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public virtual Int32 TotalCreditors()
        {
            return this.Cast<DebtPlus.Interfaces.Debt.IProratable>().Where(s => s.IsProratable).Count();
        }

        /// <summary>
        /// Locate a record based upon the primary key to the record
        /// </summary>
        /// <param name="Key">Key value for the debt record</param>
        /// <returns>A pointer to the record or NOTHING if it is not found</returns>
        /// <remarks></remarks>
        public virtual object Find(Int32 key)
        {
            return this.Cast<DebtRecord>().Where(s => key.Equals(s.client_creditor)).FirstOrDefault();
        }

        /// <summary>
        /// Record all debt records to the database when they are modified
        /// </summary>
        /// <remarks></remarks>
        public virtual void SaveData()
        {
            SaveData(false);
        }

        /// <summary>
        /// Record all debt records to the database when they are modified.
        /// </summary>
        /// <param name="ForceWrites">if TRUE, the debt records are written if they are modified or not.</param>
        /// <remarks></remarks>
        public virtual void SaveData(bool forceWrites)
        {
            SaveData(null, forceWrites);
        }

        /// <summary>
        /// Record all debt records to the database when they are modified.
        /// </summary>
        /// <param name="txn">Pointer to the transaction object</param>
        /// <remarks></remarks>
        public virtual void SaveData(SqlTransaction txn)
        {
            SaveData(txn, false);
        }

        /// <summary>
        /// Record all debt records to the database when they are modified.
        /// </summary>
        /// <param name="txn">Pointer to the transaction object</param>
        /// <param name="ForceWrites">if TRUE, the debt records are written if they are modified or not.</param>
        /// <remarks></remarks>
        public virtual void SaveData(SqlTransaction txn, bool ForceWrites)
        {
            // Process each record in the list to find the ones that have been modified since we were last here.
            foreach (DAL_DebtRecord record in this)
            {
                bool modified = ForceWrites;

                switch (record.DebtStatus)
                {
                    case DataRowState.Added:
                    case DataRowState.Modified:
                        modified = true;
                        break;
                    default:
                        break;
                }

                // if the connection is closed, open it
                if (modified)
                {
                    record.SaveData(txn);
                }
            }
        }

        /// <summary>
        /// Refresh the information from the current database debts
        /// </summary>
        /// <remarks></remarks>
        public virtual void RefreshData()
        {
            throw new NotImplementedException("RefreshData must be overridden to be used");
        }

        #region "IDisposable Support"
        private bool disposedValue = false;
        // To detect redundant calls;

        /// <summary>
        /// Dispose of the current list storage
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;

                if (disposing)
                {
                    // Toss the debt records from the list
                    Clear();

                    // Toss the list storage too
                    DebtListCurrentStorage.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~BaseDebtList()
        {
            Dispose(false);
        }
        #endregion
    }
}
