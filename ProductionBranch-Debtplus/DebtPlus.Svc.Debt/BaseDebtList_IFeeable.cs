using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Svc.Debt
{
    partial class BaseDebtList : DebtPlus.Interfaces.Debt.IFeeable
    {

        /// <summary>
        /// Process requests from the fee calculation for values from us
        /// </summary>
        public virtual object QueryFeeValue(string Item)
        {
            switch (Item)
            {
                case DebtPlus.Events.ParameterValueEventArgs.name_paf_creditor:
                    return DebtListCurrentStorage.SpecialCreditorList.MonthlyFeeCreditor;
                case DebtPlus.Events.ParameterValueEventArgs.name_deduct_creditor:
                    return DebtListCurrentStorage.SpecialCreditorList.DeductCreditor;
                case DebtPlus.Events.ParameterValueEventArgs.name_setup_creditor:
                    return DebtListCurrentStorage.SpecialCreditorList.SetupCreditor;
                case DebtPlus.Events.ParameterValueEventArgs.name_total_fees:
                    return TotalFees();
                case DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed:
                    return TotalPayments();
                case DebtPlus.Events.ParameterValueEventArgs.name_client:
                    return DebtListCurrentStorage.ClientId;
                default:
                    return RaiseQueryConfigFeeValue(Item);
            }
        }
    }
}
