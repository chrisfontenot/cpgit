#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;

namespace DebtPlus.Svc.Debt
{
    public abstract partial class DebtRecord : DAL_DebtRecord, IComparable
    {

        /// <summary>
        /// Local storage for the class
        /// </summary>
        protected class DebtRecordStorage : RecordStorage
        {

            public object debit_amt = 0m;

            public bool isPayoff = false;
            public DebtRecordStorage()
                : base()
            {
            }

            public DebtRecordStorage(DebtRecordStorage PriorCopy)
                : base(PriorCopy)
            {
                debit_amt = PriorCopy.debit_amt;
                isPayoff  = PriorCopy.isPayoff;
            }

            public DebtRecordStorage(RecordStorage Prior)
                : base(Prior)
            {
            }

            public new object Clone()
            {
                return new DebtRecordStorage(this);
            }
        }

        protected DebtRecordStorage DebtCurrentStorage;
        protected DebtRecordStorage DebtPreviousStorage;

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <remarks></remarks>
        public DebtRecord()
            : base()
        {
            DebtCurrentStorage = new DebtRecordStorage();
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="Source">Pointer to the previous record to be copied should this be a "clone" operation.</param>
        /// <remarks></remarks>
        public DebtRecord(DebtRecord Source)
            : base(Source)
        {
            DebtCurrentStorage = (DebtRecordStorage)Source.DebtCurrentStorage.Clone();
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="Source">Pointer to the original storage definition for a clone operation</param>
        /// <remarks></remarks>
        public DebtRecord(DAL_DebtRecord Source)
            : base(Source)
        {
        }

        /// <summary>
        /// Override the field changed and generate additional items based upon values that are used in other fields.
        /// We need to send to the grid ANY field that may be display that has been modified. It includes the display-only
        /// fields or they will not be updated when the underlying data structures are changed.
        /// </summary>
        /// <param name="e">The parameter to be used in the DataChanged event. Basically, it is the property that was changed.</param>
        /// <remarks></remarks>

        protected override void OnDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            // Do the standard logic to trip the event.
            base.OnDataChanged(e);

            // Look at the property and trigger other items based upon the low level changes
            switch (e.PropertyName)
            {
                case "account_number":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_account_number"));
                    break;

                case "balance_verify_by":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_verified_status"));
                    break;

                case "balance_verify_date":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_verified_status"));
                    break;

                case "ccl_zero_balance":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("MaximumPayment"));
                    break;

                case "cr_creditor_name":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_name"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_and_division"));
                    break;

                case "cr_division":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_and_division"));
                    break;

                case "cr_division_name":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_and_division"));
                    break;

                case "cr_name":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_and_division"));
                    break;

                case "cr_min_accept_amt":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    break;

                case "cr_min_accept_pct":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    break;

                case "cr_payment_balance":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    break;

                case "creditor":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_debt_id"));
                    break;

                case "creditor_name":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_name"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_creditor_and_division"));
                    break;

                case "creditor_interest":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_intererst_rate"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_dmp_interest"));
                    break;

                case "current_balance":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_current_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("MaximumPayment"));
                    break;

                case "disbursement_factor":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_disbursement_factor"));
                    break;

                case "display_proposal_status":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_debt_id"));
                    break;

                case "dmp_interest":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_intererst_rate"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_dmp_interest"));
                    break;

                case "dmp_payout_interest":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_dmp_interest"));
                    break;

                case "hold_disbursements":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("IsHeld"));
                    break;

                case "IsActive":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("IsHeld"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_current_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("MaximumPayment"));
                    break;

                case "MaximumPayment":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_disbursement_factor"));
                    break;

                case "message":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_account_number"));
                    break;

                case "non_dmp_payment":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    break;

                case "OriginalPayment":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("CreditorMinimumPayment"));
                    break;

                case "orig_balance":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("adjusted_orig_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("current_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_orig_balance"));
                    break;

                case "orig_balance_adjustment":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("adjusted_orig_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("current_balance"));
                    break;

                case "payments_this_creditor":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_payments_this_creditor"));
                    break;

                case "proposal_status":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_proposal_status"));
                    break;

                case "returns_this_creditor":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_payments_this_creditor"));
                    break;

                case "total_interest":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("adjusted_orig_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_current_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_total_interest"));
                    break;

                case "total_payments":
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("adjusted_orig_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_current_balance"));
                    OnDataChanged(new DebtPlus.Events.DataChangedEventArgs("display_total_payments"));
                    break;
                default:
                    break;
            }
        }

        #region " IComparable "

        /// <summary>
        /// Compare the items for the sort function
        /// </summary>
        public Int32 CompareTo(object obj)
        {
            // Compare the relative order for a stacking sequence
            if (!(obj is DebtRecord))
            {
                throw new ArgumentException("Comparison type of DebtRecord is expected", "obj");
            }
            DebtRecord CmpObj = (DebtRecord)obj;

            // Start by the user's assigned ordering sequence for stacking debts
            Int32 Order = DebtPlus.Repository.Common.Compare(line_number, CmpObj.line_number);

            // Compare the dmp interest override
            if (Order == 0)
            {
                Order = DebtPlus.Repository.Common.Compare(dmp_interest, CmpObj.dmp_interest);
            }

            // Compare the creditor interest rate
            if (Order == 0)
            {
                Order = DebtPlus.Repository.Common.Compare(creditor_interest, CmpObj.creditor_interest);
            }

            // Compare the current balance
            if (Order == 0)
            {
                Order = DebtPlus.Repository.Common.Compare(display_current_balance, CmpObj.display_current_balance);
            }

            // Compare the total interest charged
            if (Order == 0)
            {
                Order = DebtPlus.Repository.Common.Compare(total_interest, CmpObj.total_interest);
            }

            // Return the stacking order for the debts
            return Order;
        }

        #endregion

        /// <summary>
        /// Adjusted Original Balance
        /// </summary>
        public decimal adjusted_orig_balance
        {
            get { return orig_balance + orig_balance_adjustment; }
        }

        /// <summary>
        /// Interest rate for the display purposes
        /// </summary>
        public object display_intererst_rate
        {
            get
            {
                System.Nullable<double> d = DebtPlus.Utils.Nulls.v_Double(dmp_interest);
                if (!d.HasValue)
                {
                    d = DebtPlus.Utils.Nulls.v_Double(creditor_interest);
                }

                if (d.HasValue)
                {
                    return d.Value;
                }

                return null;
            }
        }

        /// <summary>
        /// display_account_number
        /// </summary>
        public string display_account_number
        {
            get
            {
                string answer = (DebtPlus.Utils.Nulls.v_String(Message) ?? string.Empty).Trim();
                if (answer == string.Empty)
                {
                    answer = account_number;
                }
                return answer;
            }
        }

        /// <summary>
        /// display_creditor
        /// </summary>
        public string display_creditor
        {
            get { return Creditor; }
        }

        /// <summary>
        /// display_creditor_name
        /// </summary>
        public string display_creditor_name
        {
            get
            {
                string answer = cr_creditor_name;
                if (string.IsNullOrWhiteSpace(answer))
                {
                    answer = creditor_name;
                }
                return answer;
            }
        }

        /// <summary>
        /// display_creditor_and_division
        /// </summary>
        public string display_creditor_and_division
        {
            get
            {
                string cr_name     = display_creditor_name;
                string cr_division = cr_division_name;
                if (string.IsNullOrWhiteSpace(cr_name) || string.IsNullOrWhiteSpace(cr_division))
                {
                    return cr_name + cr_division;
                }
                return string.Format("{0} {1}", cr_name, cr_division);
            }
        }

        /// <summary>
        /// display_current_balance
        /// </summary>
        public virtual decimal display_current_balance
        {
            get
            {
                if (!IsActive || current_balance < 0M)
                {
                    return 0M;
                }
                return current_balance;
            }
        }

        /// <summary>
        /// Return the minimum payment amount
        /// </summary>
        public virtual decimal AgencyMinimumPayment()
        {
            return AgencyMinimumPayment(current_balance);
        }

        public virtual decimal AgencyMinimumPayment(decimal CurrentBalance)
        {
            // If there is a balance then use the threshold figure to find the minimum amount
            if (Parent.Threshold > 0.0D)
            {
                return System.Math.Truncate(Convert.ToDecimal(Convert.ToDouble(CurrentBalance) * Parent.Threshold) * 100M) / 100M;
            }

            return 0M;
        }

        /// <summary>
        /// Return the minimum payment amount
        /// </summary>
        public virtual decimal CreditorMinimumPayment()
        {
            return CreditorMinimumPayment(current_balance);
        }

        public virtual decimal CreditorMinimumPayment(decimal CurrentBalance)
        {
            decimal answer = 0m;

            // Reduce the rate to a valid item
            double Rate = cr_min_accept_pct;
            while (Rate >= 1.0D)
            {
                Rate /= 100.0D;
            }


            if (Rate > 0.0D)
            {
                // If the creditor wants a percentage of the original payment, then use the non-dmp payment amount
                if (cr_payment_balance == "P")
                {
                    answer = Convert.ToDecimal(Convert.ToDouble(non_dmp_payment) * Rate);
                }
                else
                {
                    // Otherwise, use a percentage of the current balance for the figure
                    answer = Convert.ToDecimal(Convert.ToDouble(CurrentBalance) * Rate);
                }
            }

            // If the creditor has a dollar amount then use it
            if (cr_min_accept_amt > 0M)
            {
                decimal DollarAmount = cr_min_accept_amt;
                if (DollarAmount > 0m && answer < DollarAmount)
                {
                    answer = DollarAmount;
                }
            }

            return System.Math.Truncate(answer * 100M) / 100M;
        }

        /// <summary>
        /// Return the minimum payment amount
        /// </summary>
        public override decimal MinimumPayment()
        {
            return MinimumPayment(current_balance);
        }

        public override decimal MinimumPayment(decimal CurrentBalance)
        {
            // Use the figure from the creditor alone
            decimal answer = CreditorMinimumPayment(CurrentBalance);

            // If the creditor has no figure then use the agency figures
            if (answer <= 0M)
            {
                decimal AgencyAmount = AgencyMinimumPayment(CurrentBalance);
                decimal BaseAmount   = base.MinimumPayment(CurrentBalance);

                // Choose the larger of the two figures. Use either our base figure
                // or the agency percentage amount.
                if (BaseAmount < AgencyAmount)
                {
                    answer = AgencyAmount;
                }
                else
                {
                    answer = BaseAmount;
                }
            }

            return answer;
        }

        /// <summary>
        /// Return the maximum payment amount
        /// </summary>
        public override decimal MaximumPayment()
        {
            decimal Value = base.MaximumPayment();
            if (!IsActive)
            {
                Value = 0M;
            }
            else if (!ccl_zero_balance)
            {
                if (Value > current_balance)
                {
                    Value = System.Math.Truncate(current_balance * 100M) / 100M;
                }
            }
            return Value;
        }

        /// <summary>
        /// display_disbursement_factor
        /// </summary>
        public override decimal display_disbursement_factor
        {
            get
            {
                decimal answer = disbursement_factor;
                if (answer > MaximumPayment())
                {
                    answer = MaximumPayment();
                }
                return answer;
            }
            set
            {
                if (value > MaximumPayment())
                {
                    value = MaximumPayment();
                }
                disbursement_factor = value;
            }
        }

        /// <summary>
        /// display_debt_id
        /// </summary>
        public string display_debt_id
        {
            get
            {
                string answer = string.Empty;
                if (Creditor != null && !object.ReferenceEquals(Creditor, DBNull.Value))
                {
                    answer = string.Format("{0}{1}", Creditor, display_proposal_status);
                }
                return answer;
            }
        }

        /// <summary>
        /// display_payments_this_creditor
        /// </summary>
        public decimal display_payments_this_creditor
        {
            get { return payments_this_creditor - returns_this_creditor; }
        }

        /// <summary>
        /// display_total_payments
        /// </summary>
        public decimal display_total_payments
        {
            get { return total_payments; }
        }

        /// <summary>
        /// display_total_interest
        /// </summary>
        public decimal display_total_interest
        {
            get { return total_interest; }
        }

        /// <summary>
        /// display_verified_status
        /// </summary>
        public string display_verified_status
        {
            get
            {
                string VerifiedStatus = string.Empty;

                // Look for both the verified by and verified date to set the verification flag.
                // the "un-verify" logic will clear the date but leaves the verified by on purpose.
                if (balance_verify_by.Trim() != string.Empty)
                {
                    if (balance_verify_date != null && !object.ReferenceEquals(balance_verify_date, DBNull.Value))
                    {
                        VerifiedStatus = "V";
                    }
                }

                return VerifiedStatus;
            }
        }

        /// <summary>
        /// display_orig_balance
        /// </summary>
        public virtual decimal display_orig_balance
        {
            get { return orig_balance; }
        }

        /// <summary>
        /// display_dmp_interest
        /// </summary>
        public object display_dmp_interest
        {
            get
            {
                // Use the dmp override rate first
                object answer = dmp_interest;

                // Accept the payout interest rate if there is on. Special Case : Rate of 0.0 is "nothing"
                if (object.ReferenceEquals(answer, DBNull.Value))
                {
                    answer = dmp_payout_interest;
                    if (!object.ReferenceEquals(answer, DBNull.Value) && Convert.ToDouble(answer) <= 0.0)
                    {
                        answer = DBNull.Value;
                    }
                }

                // Use the creditor interest rate as a last resort
                if (object.ReferenceEquals(answer, DBNull.Value))
                {
                    answer = creditor_interest;
                }

                return answer;
            }
        }

        /// <summary>
        /// display_proposal_status
        /// </summary>
        public string display_proposal_status
        {
            get
            {
                switch (proposal_status)
                {
                    case 0:
                    case 1:
                        return "-";
                    case 2:
                        return "D";
                    case 3:
                        return "Y";
                    case 4:
                        return "R";
                    default:
                        return "?";
                }
            }
        }

        /// <summary>
        /// Delete the current debt information on the database
        /// </summary>
        public virtual bool Delete()
        {
            return false;
        }

        public virtual void RefreshData()
        {
        }
    }
}