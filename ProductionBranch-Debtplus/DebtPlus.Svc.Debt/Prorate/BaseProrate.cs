#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.Interfaces.Debt;

namespace DebtPlus.Svc.Debt
{
    /// <summary>
    /// Base abstract class to do the prorate operation.
    /// </summary>
    public abstract class BaseProrate : IDisposable
    {
        /// <summary>
        /// Do any local disposal of storage.
        /// </summary>
        /// <param name="disposing">TRUE if doing the Dispose. FALSE for Finalize.</param>
        protected virtual void Dispose(bool disposing)
        {
            DebtList = null;
        }

        protected IProratableList DebtList = null;
        protected bool useDisbursementFactor;

        protected decimal availableFunds;

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        /// <param name="DebtList"></param>
        public BaseProrate(IProratableList DebtList)
            : this()
        {
            this.DebtList = DebtList;
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public BaseProrate()
            : base()
        {
        }

        /// <summary>
        /// Perform the prorate function on the data
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        public virtual void Prorate(decimal trustBalance)
        {
            Prorate(trustBalance, false);
        }

        /// <summary>
        /// Perform the prorate function on the data
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        /// <param name="useDisbursementFactor">Should the disbursement factor be used in calculations or the scheduled pay. TRUE = disbursement_factor, FALSE=scheduled pay</param>
        public virtual void Prorate(decimal trustBalance, bool useDisbursementFactor)
        {
            this.useDisbursementFactor = useDisbursementFactor;

            // Save the parameter for the callback function
            availableFunds = trustBalance;

            // Recalculate the monthly fee with the payment information
            using (DebtPlus.Interfaces.Client.IMonthlyFee feeClass = new DebtPlus.Svc.Common.MonthlyFeeCalculation())
            {
                // Adjust the PAF creditor's scheduled payment with the correct amount
                IProratable record = DebtList.MonthlyFeeDebt;
                if (record != null)
                {
                    feeClass.QueryValue += fees_QueryValue;
                    record.debit_amt     = feeClass.FeeAmount();
                    feeClass.QueryValue -= fees_QueryValue;
                    record.sched_payment = record.debit_amt;
                }

                // Find the amount of money that needs to be "taken off the top" of the payments and not prorated.
                decimal reserved = DebtList.TotalFees();

                // Do an initial stab at the proration procedure.
                TryProrate(trustBalance - reserved);

                // Limit the disbursements to the debt balances where they are needed.
                decimal lastPayments = default(decimal);
                decimal currentPayments = DebtList.TotalPayments();
                do
                {
                    lastPayments = currentPayments;

                    // if the payments match the expected or there are no payments then we have nothing left to do
                    if (lastPayments + reserved == trustBalance)
                    {
                        break;
                    }

                    if (lastPayments == 0m)
                    {
                        break;
                    }

                    // Adjust the PAF creditor's scheduled payment with the correct amount
                    record = DebtList.MonthlyFeeDebt;
                    if (record != null)
                    {
                        feeClass.QueryValue += fees_QueryValue;
                        record.debit_amt     = feeClass.FeeAmount();
                        feeClass.QueryValue -= fees_QueryValue;
                        record.sched_payment = record.debit_amt;
                    }

                    // Do the prorate function again once the values have changed
                    TryProrate(trustBalance - reserved);

                    // if the value did not change then we have reached equilibrium and stop
                    currentPayments = DebtList.TotalPayments();

                    // Attempt to perform a final adjustment to make things the proper figures.
                    // We do this if the amount to be adjusted is small, but non-zero
                    decimal excess = trustBalance - reserved - currentPayments;
                    if (excess != 0m)
                    {
                        AdjustSmallAmounts(excess);
                        currentPayments = DebtList.TotalPayments();
                    }
                } while (currentPayments != lastPayments);
            }
        }

        /// <summary>
        /// Callback function to find information in calculating the monthly fee
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected virtual void fees_QueryValue(object sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            switch (e.Name)
            {
                case DebtPlus.Events.ParameterValueEventArgs.name_SchedPayment:
                    if (useDisbursementFactor)
                    {
                        e.Value = ((IFeeable)DebtList).QueryFeeValue(DebtPlus.Events.ParameterValueEventArgs.name_DisbursementFactor);
                    }
                    else
                    {
                        e.Value = ((IFeeable)DebtList).QueryFeeValue(e.Name);
                    }
                    break;

                case DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed:
                    e.Value = DebtList.TotalPayments();
                    break;

                case DebtPlus.Events.ParameterValueEventArgs.name_InProrate:
                    e.Value = true;
                    break;

                case DebtPlus.Events.ParameterValueEventArgs.name_AvailableFunds:
                    e.Value = availableFunds;
                    break;

                // Pass the request on to the debt list for an answer
                default:
                    e.Value = ((IFeeable)DebtList).QueryFeeValue(e.Name);
                    break;
            }
        }

        // Perform the basic prorate operation on the list of debts
        protected void TryProrate(decimal trustBalance)
        {
            decimal paymentTotal = DebtList.TotalPayments();

            // Do nothing if the amounts match
            if (paymentTotal == trustBalance)
            {
                return;
            }

            // if the amount is negative then simply set the amounts to zero and let it adjust upwards
            if (trustBalance <= 0m)
            {
                foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                {
                    record.debit_amt = 0m;
                }
                return;
            }

            //--------------------------------------------------------------------------------------
            //        Do the prorate function based upon the direction needed                     --
            //--------------------------------------------------------------------------------------

            // Determine the relative order for the prorate function
            if (trustBalance < paymentTotal)
            {
                ProrateInsufficientFunds(trustBalance);
            }
            else
            {
                ProrateExcessFunds(trustBalance);
            }
        }

        /// <summary>
        /// Prorate insufficient funds when there is not enough money to pay this debt priority group.
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        protected virtual void ProrateInsufficientFunds(decimal trustBalance)
        {
            bool fChanged = false;

            // Process the other amounts for the various priority levels, in priority level.

            for (Int32 priority = 0; priority <= 9; priority++)
            {
                // There should be a positive (or zero) amount of money. It should NEVER be negative!
                System.Diagnostics.Debug.Assert(trustBalance >= 0m);

                // if there is no money left then pay nothing to the creditors
                if (trustBalance <= 0m)
                {
                    // Clear the debit amounts from the debts for this priority
                    foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                    {
                        if (record.priority == priority && record.IsProratable)
                        {
                            record.debit_amt = 0m;
                        }
                    }
                }
                else
                {
                    // Find the total for the payments at this priority level
                    decimal currentTotal = priorityTotal(priority);

                    if (currentTotal > 0m)
                    {
                        // if there is enough money then pay all at 100% of their total payment
                        if (currentTotal <= trustBalance)
                        {
                            trustBalance -= currentTotal;
                        }
                        else
                        {
                            // Pay the creditors a prorated amount based upon their disbursment to the total amount.
                            decimal paid = 0m;
                            foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                            {
                                // Pay the creditor a prorated amount based on their payment and the total for the level
                                if (record.priority == priority && record.IsProratable)
                                {
                                    record.debit_amt = (record.debit_amt / currentTotal) * trustBalance;
                                    if (record.debit_amt > record.current_balance)
                                    {
                                        record.debit_amt = record.current_balance;
                                    }

                                    // Update the total paid for this priority
                                    paid += record.debit_amt;
                                }
                            }
                            trustBalance -= paid;

                            // Smooth the debts out to adjust for penny differences as needed
                            fChanged = true;
                            while (fChanged && trustBalance != 0m)
                            {
                                fChanged = false;

                                // Examine the creditors in this priority group again . . . .
                                foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                                {
                                    if (trustBalance == 0m)
                                    {
                                        break;
                                    }

                                    // if this is the proper group and not a fee creditor then . . . .
                                    if (record.priority == priority && record.IsProratable)
                                    {
                                        // if we over-disbursed then reduce the disbursement amount by a penny per creditor
                                        if (trustBalance < 0m)
                                        {
                                            if (record.debit_amt > 0m)
                                            {
                                                record.debit_amt -= 0.01m;
                                                trustBalance += 0.01m;
                                                fChanged = true;
                                            }
                                        }
                                        else
                                        {
                                            // Otherwise, increase the disbursement amount by a penny per creditor
                                            if (record.debit_amt < record.current_balance)
                                            {
                                                record.debit_amt += 0.01m;
                                                trustBalance -= 0.01m;
                                                fChanged = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Find the total amount of money required for this priority group
        /// </summary>
        /// <param name="priority">The priority for the group of debts.</param>
        /// <returns></returns>
        protected decimal priorityTotal(Int32 priority)
        {
            decimal totalAmount = 0m;

            // Clear the debit amounts from the debts for this priority
            foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
            {
                if (record.priority == priority && !record.IsHeld && record.IsProratable)
                {
                    if (record.DebtType == DebtTypeEnum.MonthlyFee)
                    {
                        totalAmount += record.debit_amt;
                    }
                    else
                    {
                        if (record.debit_amt >= record.current_balance)
                        {
                            totalAmount += record.current_balance;
                        }
                        else
                        {
                            totalAmount += record.debit_amt;
                        }
                    }
                }
            }

            return totalAmount;
        }

        /// <summary>
        /// Prorate money when there is more money that what is required.
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        protected virtual void ProrateExcessFunds(decimal trustBalance)
        {
            bool fAdjusted = true;
            Int32 attemptCount = 0;
            decimal excess = default(decimal);

            // Go through the creditors and prorate the funds as long as is money
            while (trustBalance > 0m && fAdjusted && attemptCount < 10)
            {
                fAdjusted = false;
                attemptCount += 1;

                // This prevents the non-convergence from happening. Give up if we have tried too many times.
                // Compute the total of the disbursement factors
                decimal totalAmount = DebtList.TotalPayments();
                if (totalAmount == 0m)
                {
                    return;
                }

                // Calculate the excess amount that may be prorated amongst the creditors
                excess = trustBalance - totalAmount;
                if (excess <= 0m)
                {
                    break;
                }

                // Adjust the disbursements so that the figures are computed properly

                foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                {
                    // Do not pay held debts
                    if (record.IsHeld)
                    {
                        record.debit_amt = 0m;
                    }
                    else
                    {
                        // Fee accounts do not partake in this exercise. They are simply paid as the amount given.
                        if ((record.IsProratable) && (record.debit_amt < record.current_balance))
                        {
                            // Give a little bit to this creditor for the amount which is the excess
                            record.debit_amt = System.Math.Truncate(Convert.ToDecimal(Convert.ToDouble(trustBalance) * (Convert.ToDouble(record.debit_amt) / Convert.ToDouble(totalAmount))) * 100m) / 100m;

                            // if( the debt is now paid off then limit the disbursement to the balance and recalculate the amounts again.
                            if (record.debit_amt > record.current_balance)
                            {
                                record.debit_amt = record.current_balance;
                                fAdjusted = true;
                            }
                        }
                    }
                }
            }

            // Smooth the debts out to adjust for penny differences as needed
            fAdjusted = true;
            excess = trustBalance - DebtList.TotalPayments();

            // A brute force method works for small amounts... Just give it out one penny at a time to correct the amount.
            // if the amount is large (more than 5 cents) then do the algorithm with closer numbers again since will will converge.
            if (System.Math.Abs(excess) < 0.05m && excess != 0m)
            {
                AdjustSmallAmounts(excess);
            }
        }

        /// <summary>
        /// Adjust the amounts in the payments by small (i.e. penny) differences either up or down
        /// </summary>
        /// <param name="Excess">The amount of the adjustment to be made.</param>
        /// <remarks></remarks>
        public virtual void AdjustSmallAmounts(decimal excess)
        {
            bool fAdjusted = true;

            while (fAdjusted && excess != 0m)
            {
                fAdjusted = false;

                // Examine the creditors in this priority group again . . . .
                foreach (IProratable record in (System.Collections.IEnumerable)DebtList)
                {
                    if (excess == 0m)
                    {
                        break;
                    }

                    // if this is not a fee creditor then . . . .

                    if ((record.IsProratable) && record.debit_amt != 0m)
                    {
                        // if we over-disbursed then reduce the disbursement amount by a penny per creditor
                        if (excess < 0m)
                        {
                            if (record.debit_amt > 0m)
                            {
                                record.debit_amt -= 0.01m;
                                excess           += 0.01m;
                                fAdjusted         = true;
                            }
                        }
                        else
                        {
                            // Otherwise, increase the disbursement amount by a penny per creditor
                            if (record.debit_amt < record.current_balance)
                            {
                                record.debit_amt += 0.01m;
                                excess           -= 0.01m;
                                fAdjusted         = true;
                            }
                        }
                    }
                }
            }
        }

        #region "IDisposable"
        /// <summary>
        /// Used to prevent redundant Dispose operations
        /// </summary>

        private bool disposed = true;

        /// <summary>
        /// Handle the Dispose sequence here. This is not overridable. See Dispose(bool).
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                Dispose(true);
            }
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Do the finalization sequence now
        /// </summary>
        ~BaseProrate()
        {
            Dispose(false);
        }

        #endregion "IDisposable"
    }
}