namespace DebtPlus.Svc.Debt
{
    /// <summary>
    /// General class for doing the prorate operation. It will accept the Stacking flag from the client.
    /// </summary>
    public class ProrateDebts : BaseProrate
    {
        /// <summary>
        /// Initialize the class
        /// </summary>
        public ProrateDebts()
            : base()
        {
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="DebtList"></param>
        public ProrateDebts(DebtPlus.Interfaces.Debt.IProratableList DebtList)
            : base(DebtList)
        {
        }

        /// <summary>
        /// Prorate excessive funds when there is more money than what is required
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        protected override void ProrateExcessFunds(decimal trustBalance)
        {
            bool stackCreditor = false;

            if (stackCreditor)
            {
                ProrateStackedCreditors(trustBalance);
            }
            else
            {
                base.ProrateExcessFunds(trustBalance);
            }
        }

        /// <summary>
        /// Process the stacking condition for the creditors. Pay all of the excess in the highest priority
        /// rather than trying to distribute it to all of the creditors, including lower priority items.
        /// </summary>
        /// <param name="TrustBalance">The amount to be distributed to the debts.</param>
        protected virtual void ProrateStackedCreditors(decimal trustBalance)
        {
            // Compute the total of the disbursement factors
            decimal totalAmount = DebtList.TotalPayments();

            // if the proration is proper then terminate the processing
            if (totalAmount == 0m)
            {
                return;
            }

            // Calculate the excess amount that may be prorated amongst the creditors
            decimal excess = System.Math.Truncate((trustBalance - totalAmount) * 100m) / 100m;

            // Pay the creditors the amount based upon the excess

            foreach (DebtPlus.Interfaces.Debt.IProratable record in (System.Collections.IEnumerable)DebtList)
            {
                // if the debt is held, disburse $0.00
                if (record.IsHeld)
                {
                    record.debit_amt = 0m;
                }
                else
                {
                    // Do not process fee accounts. Those area always paid to the limit specified and never varies.

                    if (record.IsProratable)
                    {
                        // if the amount in excess is greater than the balance, pay off the debt totally.
                        decimal paid = excess;
                        if (paid > record.current_balance)
                        {
                            paid = record.current_balance;
                            record.debit_amt += paid;
                            excess -= paid;
                        }
                    }
                }
            }
        }
    }
}