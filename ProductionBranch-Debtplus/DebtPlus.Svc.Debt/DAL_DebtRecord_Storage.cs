
#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Debt
{
    partial class DAL_DebtRecord
    {
        protected RecordStorage originalStorage;
        protected RecordStorage currentStorage;
        protected RecordStorage previousStorage;

        protected Int32 inInit = 0;
        /// <summary>
        /// Storage for this class
        /// </summary>
        protected class RecordStorage : ICloneable, IDisposable
        {

            // ID fields;
            public Int32 ClientId;
            public String Creditor;
            public Int32 client_creditor;

            public DebtPlus.Interfaces.Debt.DebtTypeEnum DebtType;
            // FIXED POINT FIELDS;
            public Int32 balance_client_creditor;
            public Int32 check_payments;
            public Int32 client_creditor_balance;
            public Int32 client_creditor_proposal;
            public Int32 first_payment;
            public Int32 last_payment;
            public Int32 line_number;
            public Int32 months_delinquent;
            public Int32 Person;
            public Int32 priority;
            public Int32 proposal_status;
            public Int32 send_bal_verify;

            public Int32 terms;
            // MONITARY FIELDS;
            public Decimal cr_min_accept_amt;
            public Decimal current_sched_payment;
            public Decimal disbursement_factor;
            public Decimal first_payment_amt;
            public Decimal interest_this_creditor;
            public Decimal last_payment_amt;
            public Decimal last_stmt_balance;
            public Decimal non_dmp_payment;
            public Decimal orig_balance;
            public Decimal orig_balance_adjustment;
            public Decimal orig_dmp_payment;
            public Decimal payments_month_0;
            public Decimal payments_month_1;
            public Decimal payments_this_creditor;
            public Decimal proposal_balance;
            public Decimal returns_this_creditor;
            public Decimal sched_payment;
            public Decimal total_interest;
            public Decimal total_payments;

            public Decimal total_sched_payment;
            // PERCENTAGE FIELDS;
            public Double cr_min_accept_pct;
            public Double non_dmp_interest;

            public Double percent_balance;
            // STRING FIELDS
            public String account_number;
            public String balance_verify_by;
            public String client_name;
            public String contact_name;
            public String cr_creditor_name;
            public String cr_division_name;
            public String cr_payment_balance;
            public String created_by;
            public String creditor_name;
            public String first_payment_type;
            public String last_communication;

            public String last_payment_type;
            // FLAG FIELDS
            public Boolean ccl_agency_account;
            public Boolean ccl_always_disburse;
            public Boolean ccl_prorate;
            public Boolean ccl_zero_balance;
            public Boolean hold_disbursements;
            public Boolean irs_form_on_file;
            public Boolean IsActive;
            public Boolean send_drop_notice;

            public Boolean student_loan_release;
            // DATE/TIME FIELDS

            public DateTime date_created;
            // OTHERS, INCLUDING DATE/TIME WHICH ARE NULLABLE (DBNULL)
            public Object balance_verification_release;
            public Object balance_verify_date;
            public Object creditor_interest;
            public Object date_disp_changed;
            public Object date_proposal_prenoted;
            public Object dmp_interest;
            public Object dmp_payout_interest;
            public Object drop_date;
            public Object drop_reason;
            public Object drop_reason_date;
            public Object drop_reason_sent;
            public Object expected_payout_date;
            public Object first_payment_date;
            public Object last_payment_date;
            public Object last_payment_date_b4_dmp;
            public Object last_stmt_date;
            public Object message;
            public Object payment_rpps_mask;
            public Object prenote_date;
            public Object rpps_client_type_indicator;
            public Object rpps_mask;
            public Object start_date;
            public Object verify_request_date;
            public Object fairshare_pct_check;

            public Object fairshare_pct_eft;
            // Define the flags for processing the update events
            public bool IsDetached;

            public bool IsModified;
            public RecordStorage()
                : base()
            {

                // ID fields
                ClientId = -1;
                Creditor = string.Empty;
                client_creditor = 0;
                DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal;

                // FIXED POINT FIELDS
                balance_client_creditor = 0;
                check_payments = 0;
                client_creditor_balance = 0;
                client_creditor_proposal = 0;
                first_payment = 0;
                last_payment = 0;
                line_number = 0;
                months_delinquent = 0;
                Person = 0;
                priority = 0;
                proposal_status = 0;
                send_bal_verify = 0;
                terms = 0;

                // MONITARY FIELDS
                cr_min_accept_amt = 0m;
                current_sched_payment = 0m;
                disbursement_factor = 0m;
                first_payment_amt = 0m;
                interest_this_creditor = 0m;
                last_payment_amt = 0m;
                last_stmt_balance = 0m;
                non_dmp_payment = 0m;
                orig_balance = 0m;
                orig_balance_adjustment = 0m;
                orig_dmp_payment = 0m;
                payments_month_0 = 0m;
                payments_month_1 = 0m;
                payments_this_creditor = 0m;
                proposal_balance = 0m;
                returns_this_creditor = 0m;
                sched_payment = 0m;
                total_interest = 0m;
                total_payments = 0m;
                total_sched_payment = 0m;

                // PERCENTAGE FIELDS
                cr_min_accept_pct = 0.0;
                non_dmp_interest = 0.0;
                percent_balance = 0.0;

                // STRING FIELDS
                account_number = string.Empty;
                balance_verify_by = string.Empty;
                client_name = string.Empty;
                contact_name = string.Empty;
                cr_creditor_name = string.Empty;
                cr_division_name = string.Empty;
                cr_payment_balance = string.Empty;
                created_by = string.Empty;
                creditor_name = string.Empty;
                first_payment_type = string.Empty;
                last_communication = string.Empty;
                last_payment_type = string.Empty;

                // FLAG FIELDS
                ccl_agency_account = false;
                ccl_always_disburse = false;
                ccl_prorate = false;
                ccl_zero_balance = false;
                hold_disbursements = false;
                irs_form_on_file = false;
                IsActive = true;
                send_drop_notice = false;
                student_loan_release = false;

                // DATE/TIME FIELDS
                date_created = DateTime.MinValue;

                // OTHERS, INCLUDING DATE/TIME WHICH ARE NULLABLE (DBNULL)
                balance_verification_release = DBNull.Value;
                balance_verify_date = DBNull.Value;
                creditor_interest = DBNull.Value;
                date_disp_changed = DBNull.Value;
                date_proposal_prenoted = DBNull.Value;
                dmp_interest = DBNull.Value;
                dmp_payout_interest = DBNull.Value;
                drop_date = DBNull.Value;
                drop_reason = DBNull.Value;
                drop_reason_date = DBNull.Value;
                drop_reason_sent = DBNull.Value;
                expected_payout_date = DBNull.Value;
                first_payment_date = DBNull.Value;
                last_payment_date = DBNull.Value;
                last_payment_date_b4_dmp = DBNull.Value;
                last_stmt_date = DBNull.Value;
                message = DBNull.Value;
                payment_rpps_mask = DBNull.Value;
                prenote_date = DBNull.Value;
                rpps_client_type_indicator = DBNull.Value;
                rpps_mask = DBNull.Value;
                start_date = DBNull.Value;
                verify_request_date = DBNull.Value;
                fairshare_pct_check = System.DBNull.Value;
                fairshare_pct_eft = System.DBNull.Value;

                // Define the flags for processing the update events
                IsDetached = false;
                IsModified = false;
            }

            public RecordStorage(RecordStorage prior)
                : base()
            {

                // ID fields
                ClientId = prior.ClientId;
                Creditor = prior.Creditor;
                client_creditor = prior.client_creditor;
                DebtType = prior.DebtType;

                // FIXED POINT FIELDS
                balance_client_creditor = prior.balance_client_creditor;
                check_payments = prior.check_payments;
                client_creditor_balance = prior.client_creditor_balance;
                client_creditor_proposal = prior.client_creditor_proposal;
                first_payment = prior.first_payment;
                last_payment = prior.last_payment;
                line_number = prior.line_number;
                months_delinquent = prior.months_delinquent;
                Person = prior.Person;
                priority = prior.priority;
                proposal_status = prior.proposal_status;
                send_bal_verify = prior.send_bal_verify;
                terms = prior.terms;

                // MONITARY FIELDS
                cr_min_accept_amt = prior.cr_min_accept_amt;
                current_sched_payment = prior.current_sched_payment;
                disbursement_factor = prior.disbursement_factor;
                first_payment_amt = prior.first_payment_amt;
                interest_this_creditor = prior.interest_this_creditor;
                last_payment_amt = prior.last_payment_amt;
                last_stmt_balance = prior.last_stmt_balance;
                non_dmp_payment = prior.non_dmp_payment;
                orig_balance = prior.orig_balance;
                orig_balance_adjustment = prior.orig_balance_adjustment;
                orig_dmp_payment = prior.orig_dmp_payment;
                payments_month_0 = prior.payments_month_0;
                payments_month_1 = prior.payments_month_1;
                payments_this_creditor = prior.payments_this_creditor;
                proposal_balance = prior.proposal_balance;
                returns_this_creditor = prior.returns_this_creditor;
                sched_payment = prior.sched_payment;
                total_interest = prior.total_interest;
                total_payments = prior.total_payments;
                total_sched_payment = prior.total_sched_payment;

                // PERCENTAGE FIELDS
                cr_min_accept_pct = prior.cr_min_accept_pct;
                fairshare_pct_check = prior.fairshare_pct_check;
                fairshare_pct_eft = prior.fairshare_pct_eft;
                non_dmp_interest = prior.non_dmp_interest;
                percent_balance = prior.percent_balance;

                // STRING FIELDS
                account_number = prior.account_number;
                balance_verify_by = prior.balance_verify_by;
                client_name = prior.client_name;
                contact_name = prior.contact_name;
                cr_creditor_name = prior.cr_creditor_name;
                cr_division_name = prior.cr_division_name;
                cr_payment_balance = prior.cr_payment_balance;
                created_by = prior.created_by;
                creditor_name = prior.creditor_name;
                first_payment_type = prior.first_payment_type;
                last_communication = prior.last_communication;
                last_payment_type = prior.last_payment_type;

                // FLAG FIELDS
                ccl_agency_account = prior.ccl_agency_account;
                ccl_always_disburse = prior.ccl_always_disburse;
                ccl_prorate = prior.ccl_prorate;
                ccl_zero_balance = prior.ccl_zero_balance;
                hold_disbursements = prior.hold_disbursements;
                irs_form_on_file = prior.irs_form_on_file;
                IsActive = prior.IsActive;
                send_drop_notice = prior.send_drop_notice;
                student_loan_release = prior.student_loan_release;

                // DATE/TIME FIELDS
                date_created = prior.date_created;

                // OTHERS, INCLUDING DATE/TIME WHICH ARE NULLABLE (DBNULL)
                balance_verification_release = prior.balance_verification_release;
                balance_verify_date = prior.balance_verify_date;
                creditor_interest = prior.creditor_interest;
                date_disp_changed = prior.date_disp_changed;
                date_proposal_prenoted = prior.date_proposal_prenoted;
                dmp_interest = prior.dmp_interest;
                dmp_payout_interest = prior.dmp_payout_interest;
                drop_date = prior.drop_date;
                drop_reason = prior.drop_reason;
                drop_reason_date = prior.drop_reason_date;
                drop_reason_sent = prior.drop_reason_sent;
                expected_payout_date = prior.expected_payout_date;
                first_payment_date = prior.first_payment_date;
                last_payment_date = prior.last_payment_date;
                last_payment_date_b4_dmp = prior.last_payment_date_b4_dmp;
                last_stmt_date = prior.last_stmt_date;
                message = prior.message;
                payment_rpps_mask = prior.payment_rpps_mask;
                prenote_date = prior.prenote_date;
                rpps_client_type_indicator = prior.rpps_client_type_indicator;
                rpps_mask = prior.rpps_mask;
                start_date = prior.start_date;
                verify_request_date = prior.verify_request_date;

                // Define the flags for processing the update events
                IsDetached = prior.IsDetached;
                IsModified = prior.IsModified;
            }

            public object Clone()
            {
                return new RecordStorage(this);
            }

            // To detect redundant calls
            private bool disposedValue = false;
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    disposedValue = true;
                    if (disposing)
                    {
                    }
                }
            }

            #region "IDisposable Support"
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            ~RecordStorage()
            {
                Dispose(false);
            }
            #endregion
        }
    }
}