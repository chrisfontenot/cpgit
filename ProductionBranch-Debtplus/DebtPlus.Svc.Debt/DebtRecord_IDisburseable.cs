#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using DebtPlus.Interfaces.Debt;
using DebtPlus.Utils;
using DebtPlus.Interfaces.Disbursement;

namespace DebtPlus.Svc.Debt
{
    partial class DebtRecord : IDisburseable
    {
        /// <summary>
        /// Disbursements are to be held
        /// </summary>
        public bool IsHeld
        {
            get
            {
                return Convert.ToBoolean(hold_disbursements) || !Convert.ToBoolean(IsActive);
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Working payment amount
        /// </summary>
        public virtual decimal debit_amt
        {
            get
            {
                return DebtPlus.Utils.Nulls.DDec(DebtCurrentStorage.debit_amt);
            }
            set
            {
                value = System.Decimal.Truncate(value * 100M) / 100M;
                Debug.Assert(value >= 0m);
                SetProperty("debit_amt", ref DebtCurrentStorage.debit_amt, value);
            }
        }

        decimal IDisburseable.debit_amt
        {
            get
            {
                return debit_amt;
            }
            set
            {
                debit_amt = value;
            }
        }
    }
}