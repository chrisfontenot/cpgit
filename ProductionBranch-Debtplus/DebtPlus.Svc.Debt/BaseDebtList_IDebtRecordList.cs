#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Svc.Debt
{
    partial class BaseDebtList : DebtPlus.Interfaces.Debt.IDebtRecordList
    {

        /// <summary>
        /// Default percentage to pay debts for this agency
        /// </summary>
        public virtual double Threshold
        {
            get { return DebtListCurrentStorage.PrivateThreshold; }
            set { DebtListCurrentStorage.PrivateThreshold = value; }
        }

        /// <summary>
        /// Minimum amount to pay on all Creditor debts
        /// </summary>
        public virtual decimal MinimumPayment
        {
            get { return DebtListCurrentStorage.PrivateMinimumPayment; }
            set
            {
                if (value < 0m)
                {
                    throw new ArgumentException("Value must be at least $0.00", "MinimumPayment");
                }
                DebtListCurrentStorage.PrivateMinimumPayment = value;
            }
        }

        /// <summary>
        /// Maximum amount to pay on all Creditor debts
        /// </summary>
        public virtual decimal MaximumPayment
        {
            // Allow some room for scaling and rounding. It is not important. It is just a very large number.;
            get { return decimal.MaxValue / 1000m; }
            set { }
        }
    }
}
