#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Svc.Common;

namespace DebtPlus.Svc.Debt
{
    public class CreateDebtClass
    {
        protected Int32 ClientId = Int32.MinValue;
        public CreateDebtClass(Int32 ClientId)
        {
            this.ClientId = ClientId;
        }
        
        [Obsolete("Use LINQ tables")]
        public void GetCreditorInfoById(string myCreditorID, ref string readCreditor, ref bool prohibitUse)
        {
            DebtPlus.Repository.Creditors.GetCreditorInfoById(myCreditorID, ref readCreditor, ref prohibitUse);
        }

        public DataTable GetCreditorsByAccountNumber(string accountNumber)
        {
            const string tableName = "creditor_search";
            DataSet ds = new DataSet("ds");
            DebtPlus.Repository.Creditors.GetCreditorsByAccountNumber(ds, tableName, accountNumber);
            return ds.Tables[tableName];
        }

        [Obsolete("Use LINQ tables")]
        public DataTable GetCreditorsByName(string myCreditorName)
        {
            const string tableName = "creditor_search";
            DataSet ds = new DataSet("ds");
            DebtPlus.Repository.Creditors.GetCreditorsByName(ds, tableName, myCreditorName);
            return ds.Tables[tableName];
        }

        [Obsolete("Use version with string arguments")]
        public Int32 CreateDebt(object myCreditor, string myAccountNumber, object myUnnamedCreditor, out string emsg)
        {
            return CreateDebt(DebtPlus.Utils.Nulls.v_String(myCreditor), myAccountNumber, DebtPlus.Utils.Nulls.v_String(myUnnamedCreditor), out emsg);
        }

        public Int32 CreateDebt(string myCreditor, string myAccountNumber, string myUnnamedCreditor, out string emsg)
        {
            // Validate the account number against the creditor if there is a creditor
            if (!string.IsNullOrEmpty(myCreditor) && !string.IsNullOrEmpty(myAccountNumber))
            {
                using (AccountNumberValidation cls = new AccountNumberValidation())
                {
                    if (!cls.IsValidAccount(myCreditor, myAccountNumber))
                    {
                        emsg = "Error validating account number";
                        return 0;
                    }
                }
            }

            try
            {
                using (DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext())
                {
                    Int32 DebtID = bc.xpr_create_debt(ClientId, myCreditor, myUnnamedCreditor, myAccountNumber);
                    if (DebtID <= 0)
                    {
                        emsg = "Unable to create the debt";
                        return 0;
                    }
                    emsg = null;
                    return DebtID;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                emsg = ex.Message;
                return 0;
            }
        }
    }
}
