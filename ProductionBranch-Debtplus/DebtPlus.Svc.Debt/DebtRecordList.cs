#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Linq;
using DebtPlus.Interfaces.Debt;

namespace DebtPlus.Svc.Debt
{
    public abstract class DebtRecordList : BaseDebtList
    {

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="ItemTypes">Type of the debt records to be used in the list</param>
        public DebtRecordList(Type ItemTypes)
            : base(ItemTypes)
        {
        }

        /// <summary>
        /// Handle the fee calculation questions for the information to be used in calculating the fee amount
        /// </summary>
        /// <param name="Item">Pointer to the name of the item desired</param>
        /// <returns>The value associated with the item or NOTHING if the value can not be determined now.</returns>
        public override object QueryFeeValue(string Item)
        {
            object answer = null;
            IProratable fee_creditor = null;

            switch (Item)
            {
                case "sched_payment":
                    fee_creditor = MonthlyFeeDebt;
                    if (fee_creditor != null)
                    {
                        answer = fee_creditor.sched_payment;
                    }
                    break;

                case "disbursement_factor":
                    fee_creditor = MonthlyFeeDebt;
                    if (fee_creditor != null)
                    {
                        answer = fee_creditor.disbursement_factor;
                    }
                    break;

                case "balance":
                    fee_creditor = MonthlyFeeDebt;
                    if (fee_creditor != null)
                    {
                        answer = fee_creditor.current_balance;
                    }
                    break;

                case "payments_month_0":
                    fee_creditor = MonthlyFeeDebt;
                    if (fee_creditor != null)
                    {
                        answer = fee_creditor.payments_month_0;
                    }
                    break;

                case "dollars_disbursed":
                    answer = TotalPayments();
                    break;

                case "disbursed_creditors":
                    answer = TotalCreditors();
                    break;

                case "limit_debt_balance":
                    answer = false;
                    break;

                case "limit_monthly_max":
                    answer = false;             // = true for sched_payment, false for disbursement_factor
                    break;

                default:
                    answer = base.QueryFeeValue(Item);
                    break;
            }

            return answer;
        }

        /// <summary>
        /// Calculate the total payments for the debts in the list
        /// </summary>
        /// <returns>The dollar amount sum of the "total_payments" field for all proratable debts</returns>
        public override decimal TotalPayments()
        {
            return this.Cast<IProratable>().Where(s => s.IsProratable && !s.IsHeld && !s.IsFee).Sum(s => s.debit_amt);
        }

        /// <summary>
        /// Calculate the total payments for all fee accounts
        /// </summary>
        /// <returns>The dollar amount sum of the "total_payments" field for all non-held and FEE accounts</returns>
        public virtual decimal FeePayments()
        {
            return this.Cast<IProratable>().Where(s => s.IsFee && !s.IsHeld).Sum(s => s.debit_amt);
        }

        /// <summary>
        /// Calculate the total amount of all fixed fee accounts
        /// </summary>
        /// <returns>The dollar amount of the "total_payments" field for all fee accounts</returns>
        public virtual decimal TotalFixedFeePayments()
        {
            return this.Cast<IProratable>().Where(s => s.DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.FixedAgencyFee).Sum(s => s.debit_amt);
        }

        /// <summary>
        /// Calculate the number of creditors for the disbursement
        /// </summary>
        /// <returns>The number of creditors that are not held, are not a fee account, and have a disbursement amount and balance</returns>
        public override Int32 TotalCreditors()
        {
            return this.Cast<IProratable>().Where(s => !s.IsHeld && !s.IsFee && s.disbursement_factor > 0M && s.current_balance > 0M).Count();
        }
    }
}