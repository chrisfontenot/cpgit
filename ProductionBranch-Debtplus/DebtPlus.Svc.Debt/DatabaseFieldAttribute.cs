using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Svc.Debt
{

    /// <summary>
    /// Class used for the reflection in loading the debt records.
    /// </summary>
    public class DatabaseFieldAttribute : System.Attribute
    {

        /// <summary>
        /// Types of the data fields in the record.
        /// </summary>
        public enum FieldType
        {
            TypeObject,
            // Unspecified
            TypeString,
            // String field
            TypeInt32,
            // Fixed point number
            TypeBoolean,
            // Boolean
            TypeDecimal,
            // Monitary field
            TypeDouble,
            // Double (percentage)
            TypeDate
            // DateTime 
        }

        public FieldType TypeOfField
        {
            get { return m_TypeOfField; }
            private set { m_TypeOfField = value; }
        }

        private FieldType m_TypeOfField;
        public string ColumnName
        {
            get { return m_ColumnName; }
            private set { m_ColumnName = value; }
        }

        private string m_ColumnName;
        /// <summary>
        /// Create the class
        /// </summary>
        private DatabaseFieldAttribute()
        {
        }

        /// <summary>
        /// Create the class
        /// </summary>
        /// <param name="ColumnName">Name of the column in the result set</param>
        public DatabaseFieldAttribute(string ColumnName)
            : base()
        {
            this.ColumnName = ColumnName;
            this.TypeOfField = FieldType.TypeObject;
        }

        /// <summary>
        /// Create the class
        /// </summary>
        /// <param name="ColumnName">Name of the column in the result set</param>
        public DatabaseFieldAttribute(string ColumnName, FieldType TypeOfField)
            : base()
        {
            this.ColumnName = ColumnName;
            this.TypeOfField = TypeOfField;
        }
    }
}
