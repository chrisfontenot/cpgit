namespace DebtPlus.RPPS.Biller
{
    partial class Form_RPPS_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Form_RPPS_Search_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Search = new DevExpress.XtraEditors.SimpleButton();
            this.creditor_name = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_name.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(10, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(388, 106);
            this.LabelControl1.TabIndex = 5;
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            this.LabelControl1.Text = "This procedure will attempt to find the creditor's ID from the list of names associated with them. Normally, the creditor name is sufficient to find the possible biller IDs. However, if you wish, you may change the biller name in the search by modifying the search term below.\r\n\r\nAs a general rule, the shorter the name the more likely that you will find a match in the tables.";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(217, 164);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to close the form and return to the previous dialog";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Button_Search
            // 
            this.Button_Search.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Search.Enabled = false;
            this.Button_Search.Location = new System.Drawing.Point(121, 164);
            this.Button_Search.Name = "Button_Search";
            this.Button_Search.Size = new System.Drawing.Size(75, 23);
            this.Button_Search.TabIndex = 7;
            this.Button_Search.Text = "Search";
            this.Button_Search.ToolTip = "Click here to start the search operation after you have entered the creditor name" +
    " above.";
            this.Button_Search.ToolTipController = this.ToolTipController1;
            // 
            // creditor_name
            // 
            this.creditor_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.creditor_name.Location = new System.Drawing.Point(50, 124);
            this.creditor_name.Name = "creditor_name";
            this.creditor_name.Properties.MaxLength = 80;
            this.creditor_name.Size = new System.Drawing.Size(348, 20);
            this.creditor_name.TabIndex = 6;
            this.creditor_name.ToolTip = "Enter the name that you wish to find the list of biller IDs.";
            this.creditor_name.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(13, 127);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(31, 13);
            this.LabelControl3.TabIndex = 9;
            this.LabelControl3.Text = "Name:";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // Search
            // 
            this.AcceptButton = this.Button_Search;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(408, 198);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Search);
            this.Controls.Add(this.creditor_name);
            this.Name = "Search";
            this.Text = "RPPS Biller ID Search";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditor_name.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Search;
        public DevExpress.XtraEditors.TextEdit creditor_name;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
    }
}