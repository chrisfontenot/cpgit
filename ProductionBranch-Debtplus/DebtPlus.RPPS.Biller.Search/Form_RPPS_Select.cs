﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus;


namespace DebtPlus.RPPS.Biller
{
    internal partial class Form_RPPS_Select : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Selected biller ID
        /// </summary>
        public string BillerID { get; private set; }

        /// <summary>
        /// Dispose of local allocated storage
        /// </summary>
        /// <param name="disposing"></param>
        private void Form_RPPS_Select_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Initialize the instance of the class
        /// </summary>
        /// <param name="vue"></param>
        public Form_RPPS_Select(DataView vue) : base()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                GridControl1.DataSource = vue;
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += Form_RPPS_Select_Load;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= Form_RPPS_Select_Load;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.DoubleClick -= GridView1_DoubleClick;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_RPPS_Select_Load(object sender, EventArgs e)
        {
            if (GridView1.FocusedRowHandle >= 0)
            {
                selectBiller(GridView1.FocusedRowHandle);
            }
        }

        /// <summary>
        /// Handle the change in the focused row on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) // Handles GridView1.FocusedRowChanged
        {
            selectBiller(e.FocusedRowHandle);
        }

        /// <summary>
        /// Select the biller from the indicated row
        /// </summary>
        /// <param name="ControlRow"></param>
        private void selectBiller(Int32 ControlRow)
        {
            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if( ControlRow >= 0 )
            {
                drv = (DataRowView) GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    BillerID = Convert.ToString(drv["rpps_biller_id"]);
                    Button_OK.Enabled = true;
                    return;
                }
            }

            Button_OK.Enabled = false;
        }

        /// <summary>
        /// Process the double-click event on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e) // Handles GridView1.DoubleClick
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            Int32 ControlRow = ( hi.IsValid && hi.InRow ) ? hi.RowHandle : -1;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                drv = (DataRowView)GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    BillerID = Convert.ToString(drv["rpps_biller_id"]);
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }
    }
}
