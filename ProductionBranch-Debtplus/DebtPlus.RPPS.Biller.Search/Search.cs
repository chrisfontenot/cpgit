using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus;
using DebtPlus.LINQ;

namespace DebtPlus.RPPS.Biller
{
    public partial class Form_RPPS_Search : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Kludge for now
        DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

        internal bool SearchPayment = true;
        internal string CreditorName = string.Empty;

        /// <summary>
        /// The resulting BILLER ID from the search
        /// </summary>
        public string BillerID { get; private set; }

        private void Form_RPPS_Search_Dispose(bool disposing)
        {
            UnRegisterHandlers();
        }

        public Form_RPPS_Search() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        public Form_RPPS_Search(bool SearchPayment)
            : this()
        {
            this.SearchPayment = SearchPayment;
        }

        public Form_RPPS_Search(string CreditorName)
            : this()
        {
            this.CreditorName = CreditorName;
        }

        public Form_RPPS_Search(string CreditorName, bool SearchPayment)
            : this()
        {
            this.CreditorName = CreditorName;
            this.SearchPayment = SearchPayment;
        }

        private void RegisterHandlers()
        {
            this.Load += new EventHandler(Form_RPPS_Search_Load);
            creditor_name.TextChanged += new EventHandler(FormChanged);
            Button_Search.Click += new EventHandler(Button_Search_Click);
        }

        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(Form_RPPS_Search_Load);
            creditor_name.TextChanged -= new EventHandler(FormChanged);
            Button_Search.Click -= new EventHandler(Button_Search_Click);
        }

        private void Form_RPPS_Search_Load(object sender, EventArgs e) // Handles this.Load
        {
            UnRegisterHandlers();
            try
            {
                // Set the name for the search operation into the dialog
                CreditorName = CreditorName.Trim();
                if( CreditorName.Length > 50 )
                {
                    CreditorName = CreditorName.Substring(0, 50);
                }
                creditor_name.Text = CreditorName;
            }

            finally
            {
                RegisterHandlers();
            }

            // Enable/disable the search button and start the dialog.
            Button_Search.Enabled = ! HasErrors();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Button_Search.Enabled = ! HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(creditor_name.Text))
            {
                return true;
            }

            return false;
        }

        private void Button_Search_Click(object sender, EventArgs e) // Handles Button_Search.Click
        {
            using( DataSet ds = new DataSet("ds") )
            {
                DataTable tbl;
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                try
                {
                    cn.Open();

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = SearchPayment ? "xpr_rpps_biller_search" : "xpr_rpps_proposal_search";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@biller_name", SqlDbType.VarChar, 256).Value = creditor_name.Text.Trim();

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "rpps_billers");
                            tbl = ds.Tables["rpps_billers"];
                        }
                    }

                    DataView vue = tbl.DefaultView;

                    // If there are no values then complain.
                    if (vue.Count == 0)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("There are no creditors which match the name that you entered.\r\n\r\nYou might try to shorten the name and then choose from the resulting list.", "Sorry, no matches were found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    // IF there is exactly one item then just select it and don't bother asking
                    else if (vue.Count == 1)
                    {
                        BillerID = Convert.ToString(vue[0][0]);
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }

                    else
                    {
                        // There are more than just the one. Ask which one is desired
                        System.Windows.Forms.DialogResult answer;
                        using (var frm = new Form_RPPS_Select(vue))
                        {
                            answer = frm.ShowDialog();
                            BillerID = frm.BillerID;
                        }

                        // IF successful in the list then complete this dialog with the result
                        if (answer == System.Windows.Forms.DialogResult.OK)
                        {
                            DialogResult = System.Windows.Forms.DialogResult.OK;
                        }
                    }
                }

                catch (SqlException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error searching for RPPS biller id", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                finally
                {
                    cn.Dispose();
                }
            }
        }
    }
}
