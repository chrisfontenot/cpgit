﻿using System;

namespace DebtPlus.Desktop
{
    public partial class Form_ApplicationTerminating : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_ApplicationTerminating()
        {
            InitializeComponent();
            Load += new EventHandler(Form_ApplicationTerminating_Load);
        }

        private void Form_ApplicationTerminating_Load(object sender, EventArgs e)
        {
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 30 * 1000;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}