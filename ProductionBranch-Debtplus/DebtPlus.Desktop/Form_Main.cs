#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Common;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraTreeList;

namespace DebtPlus.Desktop
{
    internal partial class Form_Main : DebtPlus.Data.Forms.DebtPlusForm
    {
        private readonly MainMenuArgParser ap;
        protected static ResourceManager RM = new ResourceManager("DebtPlus.Desktop.Form_Main_Strings", Assembly.GetExecutingAssembly());
        private DateTime deathTime;
        private string _currentSystemName = string.Empty;
        private string _currentCompanyName = string.Empty;

        // Information to ensure that the process is restarted on a regular basis
        private System.Timers.Timer elapsedTimer = null;

        private const string ApplicationName = "DebtPlus.Main";

        /// <summary>
        /// This is a list of the names that have been changed since the tables were last updated.
        /// </summary>
        /// <remarks></remarks>
        private System.Collections.Generic.List<LookAside> LookAsideList = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        /// <param name="ap"></param>
        /// <remarks></remarks>
        public Form_Main(MainMenuArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        /// <summary>
        /// Initialize the form
        /// </summary>
        /// <remarks></remarks>
        public Form_Main()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();

            // Calculate a death time for the application. You get at least 5 hours of use and then
            // we roll it "forward" to 02:00 of the following morning.
            deathTime = DateTime.Now.AddHours(29);
            deathTime = new DateTime(deathTime.Year, deathTime.Month, deathTime.Day, 2, 0, 0);

            // Try to load the LookAsideList to represent the items that are replaced
            try
            {
                var asm   = System.Reflection.Assembly.GetExecutingAssembly();
                var path  = System.IO.Path.GetDirectoryName(asm.Location);
                var fname = System.IO.Path.Combine(path, @"config\LookAside.xml");
                if (System.IO.File.Exists(fname))
                {
                    using (var stm = new System.IO.FileStream(fname, System.IO.FileMode.Open))
                    {
                        var xml       = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<LookAside>));
                        LookAsideList = (System.Collections.Generic.List<LookAside>)xml.Deserialize(stm);
                    }
                }
            }
            catch { }  // Ignore any error that is found in the de-serialization.

            // If there is no list then we need to create a new item.
            if (LookAsideList == null)
            {
                LookAsideList = new System.Collections.Generic.List<LookAside>();
            }

            // If Open Office is installed then use it for Excel extracts
            if (OpenOfficeCalcInstalled())
            {
                LookAsideList.Add(new LookAside("DebtPlus.UI.Desktop.CS.Extract.OpenOffice.Calc.Mainline"));
                LookAsideList.Add(new LookAside("DebtPlus.UI.Desktop.Extract.Excel.Mainline", "DebtPlus.UI.Desktop.CS.Extract.OpenOffice.Calc.Mainline"));
            }
            else
            {
                // If Open Office is not installed then use Excel
                if (MicrosoftExcelInstalled())
                {
                    LookAsideList.Add(new LookAside("DebtPlus.UI.Desktop.CS.Extract.Excel.Mainline"));
                }
            }
        }

        /// <summary>
        /// Is Microsoft Excel installed on the computer?
        /// </summary>
        /// <returns></returns>
        private bool MicrosoftExcelInstalled()
        {
            return GetCurVer("Excel.Application") != null;
        }

        /// <summary>
        /// Is OpenOffice installed on the computer?
        /// </summary>
        /// <returns></returns>
        private bool OpenOfficeCalcInstalled()
        {
            return GetCurVer("com.sun.star.ServiceManager") != null;
        }

        /// <summary>
        /// Determine the current class version key from the application key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetCurVer(string key)
        {
            Microsoft.Win32.RegistryKey reg = null;
            try
            {
                // The registry tree that we want is HKCR\key\CurVer
                reg = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(key);
                {
                    if (reg != null)
                    {
                        reg = reg.OpenSubKey("CurVer");
                        if (reg != null)
                        {
                            // Look for the "(Default)" entry to find the answer.
                            return reg.GetValue("").ToString();
                        }
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Register the events to be handled by this code
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            Load                                  += Form_Main_Load;
            SizeChanged                           += Form_Main_SizeChanged;
            barSubItem_SystemNames.Popup          += mnuFile_Systems_Popup;
            barButtonItem1.ItemClick              += BarButtonItem1_ItemClick;
            barButtonItem2.ItemClick              += BarButtonItem2_ItemClick;
            barButtonItem3.ItemClick              += BarButtonItem3_ItemClick;
            barButtonItem_Options_Reset.ItemClick += BarButtonItem_Options_Reset_ItemClick;
            barButtonItem_WhoAmI.ItemClick        += barButtonItem_WhoAmI_ItemClick;
            treeList1.MouseClick                  += TreeList1_MouseClick;
            barManager1.EndCustomization          += BarManager1_EndCustomization;
            bar2.DockChanged                      += BarManager1_EndCustomization;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        /// <remarks></remarks>
        private void UnregisterHandlers()
        {
            Load                                  -= Form_Main_Load;
            SizeChanged                           -= Form_Main_SizeChanged;
            barSubItem_SystemNames.Popup          -= mnuFile_Systems_Popup;
            barButtonItem1.ItemClick              -= BarButtonItem1_ItemClick;
            barButtonItem2.ItemClick              -= BarButtonItem2_ItemClick;
            barButtonItem3.ItemClick              -= BarButtonItem3_ItemClick;
            barButtonItem_Options_Reset.ItemClick -= BarButtonItem_Options_Reset_ItemClick;
            barButtonItem_WhoAmI.ItemClick        -= barButtonItem_WhoAmI_ItemClick;
            treeList1.MouseClick                  -= TreeList1_MouseClick;
            barManager1.EndCustomization          -= BarManager1_EndCustomization;
            bar2.DockChanged                      -= BarManager1_EndCustomization;
        }

        /// <summary>
        /// Terminate the application when the timer expires sometime between 2 and 5 am.
        /// We need to flush the cache of stored items. There is no reason for someone to keep the
        /// process running for more than 24 hours.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void elapsedTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            elapsedTimer.Stop();

            // If we are to terminate the application at this time then ensure that the user has gone "home".
            if (DateTime.Now >= deathTime)
            {
                var thrd = new System.Threading.Thread(TerminationThread);
                thrd.SetApartmentState(ApartmentState.STA);
                thrd.Start();
                return;
            }

            // Check the timer every 30 minutes of use
            elapsedTimer.Interval = 30 * 60 * 1000;
            elapsedTimer.Start();
        }

        /// <summary>
        /// Thread to manage the termination of the process.
        /// </summary>
        private void TerminationThread()
        {
            using (var frm = new Form_ApplicationTerminating())
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                {
                    Application.Exit();
                    return;
                }

                Termination_Delay();
            }
        }

        // Delegate to the delay procedure
        private delegate void Termination_Delay_Delegate();

        /// <summary>
        /// The user wanted to delay the termination. Wait an additional 10 minutes.
        /// </summary>
        private void Termination_Delay()
        {
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new Termination_Delay_Delegate(Termination_Delay)));
                return;
            }

            // The user wanted to hold off for a while longer. Give him a bit more time.
            elapsedTimer.Stop();
            deathTime = DateTime.Now.AddMinutes(10);    // new termination time
            elapsedTimer.Interval = 10 * 60 * 1000;     // Wait the 10 minutes that the user wanted.
            elapsedTimer.Start();
        }

        /// <summary>
        /// Process the form LOAD sequence
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Main_Load(object sender, EventArgs e)
        {
            UnregisterHandlers();
            try
            {
                // Find the period for the running process. If one is given set the ending date accordingly.
                if (Properties.Settings.Default.AutoExit > 0L)
                {
                    // Compute the termination time period. We really end the application between 2 and 5 am after this period.
                    deathTime = DateTime.Now.AddDays(1).AddMinutes((double)Properties.Settings.Default.AutoExit);
                    deathTime = new DateTime(deathTime.Year, deathTime.Month, deathTime.Day, 2, 0, 0);

                    // Start a timer to run for the polling period. We want to end the application sometime when it is not busy.
                    elapsedTimer          = new System.Timers.Timer() { AutoReset = false };
                    elapsedTimer.Interval = 30 * 60 * 1000;
                    elapsedTimer.Elapsed += new System.Timers.ElapsedEventHandler(elapsedTimer_Elapsed);
                    elapsedTimer.Start();
                }

                string configFile = BarManager1_XML_File;
                if (File.Exists(configFile))
                {
                    barManager1.RestoreLayoutFromXml(configFile);
                }

                LoadPlacement(ApplicationName, "1");

                Show_Forms_At_Startup();
                show_ticklers_at_startup();
                PopulateSystemMenus();

                // Set the hooks for the changes in the skin names
                SkinHelper.InitSkinPopupMenu(barSubItem_Skin_Names);
                HookSkinChanges(barSubItem_Skin_Names);
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers on the menu items for the skin names. The menu items are loaded
        /// by calling the routine in DevExpress' library so we can't hook into the click events. We have to
        /// do it the hard way and walk the menu tree.
        /// </summary>
        /// <param name="barItem"></param>
        /// <remarks></remarks>
        private void HookSkinChanges(DevExpress.XtraBars.BarItem barItem)
        {
            if (barItem is DevExpress.XtraBars.BarSubItem)
            {
                DevExpress.XtraBars.LinksInfo col = ((DevExpress.XtraBars.BarSubItem)barItem).LinksPersistInfo;
                if (col != null && col.Count > 0)
                {
                    foreach (DevExpress.XtraBars.LinkPersistInfo NewBarLinkItem in col)
                    {
                        HookSkinChanges(NewBarLinkItem.Item);
                    }
                }
            }

            if (barItem is DevExpress.XtraBars.BarButtonItem)
            {
                barItem.ItemClick += SkinChanged;
            }
        }

        /// <summary>
        /// Process a click event on the skins menu list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SkinChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DebtPlus.Configuration.Config.SkinName = e.Item.Caption;
        }

        /// <summary>
        /// Reset the form caption when the window size changes. It is typically minimized or enlarged
        /// so we set the name appropriately
        /// </summary>
        /// <remarks></remarks>
        private void ResetCaption()
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Text = RM.GetString("reset_caption.minimized");
            }
            else
            {
                if (_currentCompanyName != string.Empty)
                {
                    Text = string.Format(RM.GetString("reset_caption.named"), _currentCompanyName);
                }
                else
                {
                    Text = RM.GetString("reset_caption.unnamed");
                }
            }
        }

        /// <summary>
        /// Populate the menu lists with the appropriate values
        /// This will load values from the registry - which may/may not be there.  App will try to load from DB as well in TryToSwitchSystems.
        /// </summary>
        private void PopulateSystemMenus()
        {
            bool systemLoaded = false;

            // Remove items from the systems menu
            barSubItem_SystemNames.ClearLinks();

            const string systemsKey = "\\Database\\Systems";
            string[] systemNames = DebtPlus.Configuration.DebtPlusRegistry.GetInstallationSubKeyNames(systemsKey);

            if (systemNames != null && systemNames.GetUpperBound(0) >= systemNames.GetLowerBound(0))
            {
                Array.Sort(systemNames);

                // List the systems in the registry key as targets to the "systems" menu
                foreach (string sysName in systemNames)
                {
                    string descriptionName = DebtPlus.Configuration.DebtPlusRegistry.GetInstallationValue(systemsKey + sysName, "description");
                    if (string.IsNullOrWhiteSpace(descriptionName))
                    {
                        descriptionName = sysName;
                    }

                    // Add the new submenu to the list of systems
                    BarCheckItem newItem = new BarCheckItem()
                    {
                        Tag = sysName,
                        Caption = descriptionName
                    };
                    newItem.ItemClick += SystemMenuItemClick;
                    barSubItem_SystemNames.AddItem(newItem);
                }

                // Define the default system groupRecord
                string defaultSystem = DebtPlus.Configuration.DebtPlusRegistry.GetInstallationValue(systemsKey, "Default");
                if (defaultSystem != null)
                {
                    if (TryToSwitchSystems(defaultSystem.Trim()))
                    {
                        systemLoaded = true;
                    }
                }
            }

            // If there are items for the submenu then make it visible
            if (barSubItem_SystemNames.ItemLinks.Count > 0)
            {
                barSubItem_SystemNames.Visibility = BarItemVisibility.Always;
            }

            if (!systemLoaded)
            {
                TryToSwitchSystems(string.Empty);
            }
        }

        /// <summary>
        /// Handle the condition when the system is changed from the menu
        /// </summary>
        private void SystemMenuItemClick(object sender, ItemClickEventArgs e)
        {
            string newSystemName = Convert.ToString(e.Item.Tag);
            if (!TryToSwitchSystems(newSystemName))
            {
                try
                {
                    LoadItems(_currentCompanyName);
                    Refresh();
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        private bool TryToSwitchSystems(string newSystemName)
        {
            bool answer = false;
            try
            {
                using (var cm = new CursorManager())
                {
                    string newCompanyName = string.Empty;
                    var configRecord = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
                    if (configRecord != null)
                    {
                        newCompanyName = configRecord.name;
                    }

                    // All is well. Update the values for this program.
                    _currentCompanyName = newCompanyName;
                    _currentSystemName = newSystemName;
                    ResetCaption();
                    LoadItems(newCompanyName);
                    Refresh();
                    answer = true;
                }

                // Ensure that the environment variable is properly set
                Environment.SetEnvironmentVariable("DEBTPLUS_KEY", newSystemName);
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error trying to switch systems");
            }

            return answer;
        }

        /// <summary>
        /// Load the items from the database and fill out the list of programs
        /// </summary>
        /// <param name="newCompanyName">Name of the company for the head banner</param>
        /// <remarks></remarks>

        private void LoadItems(string newCompanyName)
        {
            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                treeList1.DataSource = (from m in bc.lst_menus(null) orderby m.seq select m).ToList();
            }

            // Refresh the data source
            treeList1.RefreshDataSource();
            treeListColumn_text.Caption = newCompanyName;
        }

        /// <summary>
        /// Process the single click event on the list of programs
        /// </summary>
        private void TreeList1_MouseClick(object sender, MouseEventArgs e)
        {
            // The tree must exist....
            TreeList treeList = sender as TreeList;
            if (treeList == null)
            {
                return;
            }

            // we need to have a fully formed tree to start....
            if (string.IsNullOrWhiteSpace(treeList.KeyFieldName))
            {
                return;
            }

            System.Collections.Generic.IList<DebtPlus.LINQ.lst_menusResult> menuList = treeList1.DataSource as System.Collections.Generic.IList<DebtPlus.LINQ.lst_menusResult>;

            // locate where the mouse click occurred
            TreeListHitInfo hi = treeList1.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (menuList != null && hi != null)
            {

                // Find the node that was selected
                var node = hi.Node;
                if (node != null)
                {
                    Int32 seq = Convert.ToInt32(node.GetValue(treeList.KeyFieldName));
                    DebtPlus.LINQ.lst_menusResult menuItem = (from l in menuList where l.seq == seq select l).Single();

                    // launch the window that corresponds to the key
                    if (menuItem.attributes == 0)
                    {
                        LaunchWindow(menuItem);
                    }
                }
            }
        }

        /// <summary>
        /// Start the indicated window based upon the menu groupRecord passed
        /// </summary>
        /// <param name="menuItem">Pointer to the menusResult structure</param>
        /// <remarks></remarks>

        public void LaunchWindow(DebtPlus.LINQ.lst_menusResult menuItem)
        {
            UnregisterHandlers();
            try
            {
                using (CursorManager cm = new CursorManager(Cursors.AppStarting))
                {
                    StartApplication(menuItem.type, menuItem.argument);
                }
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error starting application");
            }

            finally
            {
                RegisterHandlers();
            }
        }

        private void barButtonItem_WhoAmI_ItemClick(object sender, System.EventArgs e)
    	{
            var thrd = new System.Threading.Thread(WhoAmIThread);
            thrd.IsBackground = false;
            thrd.Name = "WHoAmI";
            thrd.SetApartmentState(ApartmentState.STA);
            thrd.Start();
        }

        private void WhoAmIThread(object obj)
        {
            DebtPlus.Data.Forms.MessageBox.Show(string.Format("Your account on the database is associated with the user: {0}", DebtPlus.LINQ.BusinessContext.suser_sname()), "Who Am I?");
        }

        private void StartApplication(string typeName, string Arguments)
        {
            // Look for the name change in the look-aside table. We want the new class name if the old one is being used.
		    string RevisedName = (from l in LookAsideList where string.Compare(l.OldName, typeName, true, System.Globalization.CultureInfo.InvariantCulture) == 0 select l.NewName).FirstOrDefault();
		    if (!string.IsNullOrEmpty(RevisedName))
            {
			    typeName = RevisedName;
		    }

		    System.Type ProgramType = DebtPlus.Utils.Modules.FindTypeFromName(typeName);
		    if (ProgramType == null)
            {
			    DebtPlus.Data.Forms.MessageBox.Show(string.Format("Unable to run the class '{0}'", typeName), "Error running menu groupRecord", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
		    }

			ConstructorInfo constructorInfoObj = ProgramType.GetConstructor(new Type[] {});
			object mmiInstance = constructorInfoObj.Invoke(null);
			if (mmiInstance == null)
            {
				DebtPlus.Data.Forms.MessageBox.Show(string.Format("Unable to run the class '{0}'", typeName), "Error running menu groupRecord", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Look for an instance of the IDesktopMainline. If the groupRecord is a mainline then run it with the arguments.
			IDesktopMainline programInstance = mmiInstance as IDesktopMainline;
			if (programInstance != null)
            {
				// Execute the instance
				if (string.IsNullOrWhiteSpace(Arguments))
                {
					programInstance.OldAppMain(new string[] {});
                    return;
                }
				programInstance.OldAppMain(new string[] { Arguments });
                return;
			}

			// Table Administration feature
			var formInstance = mmiInstance as DebtPlus.Data.Forms.DebtPlusForm;
			if (formInstance != null)
            {
				formInstance.Show();
			}
	    }

        /// <summary>
        /// Process the click event on the systems menu. Set the checked groupRecord for the current system
        /// </summary>
        private void mnuFile_Systems_Popup(object sender, EventArgs e)
        {
            foreach (BarCheckItemLink item in ((BarSubItem)sender).ItemLinks)
            {
                var itm = (BarCheckItem)item.Item;
                itm.Checked = Convert.ToString(itm.Tag).CompareTo(_currentSystemName) == 0;
            }
        }

        /// <summary>
        /// Show the forms at the startup event.
        /// </summary>
        /// <remarks>This is performed in a background thread.</remarks>
        public void Show_Forms_At_Startup()
        {
            ThreadStart thrdProc = new ThreadStart(FormsThread);
            Thread thrd = new Thread(thrdProc)
            {
                Name = "DebtPlus_Startup",
                IsBackground = true
            };
            thrd.Start();
        }

        /// <summary>
        /// Display the ticklers when we start the client. We show the events for the current counselor.
        /// </summary>
        private static void show_ticklers_at_startup()
        {
        }

        /// <summary>
        /// Display the splash and tips forms
        /// </summary>
        private void FormsThread()
        {
            // Show the splash screen first on this thread
            if (DebtPlus.Configuration.Config.ShowSplash)
            {
                using (Form_Splash frm = new Form_Splash())
                {
                    frm.ShowDialog();
                }
            }

            // Show the tips next
            if (DebtPlus.Configuration.Config.ShowStartupTips)
            {
                using (Form_Tips frm = new Form_Tips())
                {
                    frm.ShowDialog();
                }
            }
        }

        /// <summary>
        /// Process the Close menu event
        /// </summary>
        private void BarButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Display the Tips information
        /// </summary>
        private void BarButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (Form_Tips frm = new Form_Tips())
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Display the Help information
        /// </summary>
        private void BarButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var frm = new DebtPlus.Data.Forms.AboutBoxForm())
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// When the form size is changed, update the caption information.
        /// </summary>
        private void Form_Main_SizeChanged(object sender, EventArgs e)
        {
            ResetCaption();
        }

        /// <summary>
        /// Load the BarManager information
        /// </summary>
        private static string BarManager1_XML_File
        {
            get
            {
                // Path to our data files
                string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "DebtPlus");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                // Return the relative file in that directory
                const string fname = "DebtPlus.MainMenu.Menu.xml";
                return System.IO.Path.Combine(path, fname);
            }
        }

        /// <summary>
        /// Save the BarManager information
        /// </summary>
        private void BarManager1_EndCustomization(object sender, EventArgs e)
        {
            barManager1.SaveLayoutToXml(BarManager1_XML_File);
        }

        /// <summary>
        /// Process the click on the remove options menu groupRecord
        /// </summary>

        private void BarButtonItem_Options_Reset_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("This option will reset all settings to their default values.{0}Are you sure that you wish to do this?", Environment.NewLine), "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus");

                string errors = "";
                if (Directory.Exists(path))
                {
                    errors = DebtPlus.Utils.Filesystem.RemoveDirectory(path);
                }

                if (errors.Length > 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(errors, "Errors occurred in removing the options", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The operation completed without error", "Operation Complete", MessageBoxButtons.OK);
                }
            }
        }

        private void BarButtonItem_Expand_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            treeList1.ExpandAll();
        }

        private void BarButtonItem_Collapse_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            treeList1.CollapseAll();
        }
    }

    /// <summary>
    /// Class to represent the entries in the look-aside list table
    /// </summary>
    /// <remarks></remarks>
    public class LookAside
    {
        public string OldName { get; set; }
        public string NewName { get; set; }

        public LookAside() { }

        public LookAside(string newName)
            : this(newName.Replace(".CS.", "."), newName)
        {
        }

        public LookAside(string oldName, string newName)
        {
            this.OldName = oldName;
            this.NewName = newName;
        }
    }
}
