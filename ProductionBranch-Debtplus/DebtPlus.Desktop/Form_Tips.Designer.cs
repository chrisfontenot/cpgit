﻿namespace DebtPlus.Desktop
{
    partial class Form_Tips
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Tips));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new DevExpress.XtraEditors.LabelControl();
            this.lblTipText = new DevExpress.XtraEditors.LabelControl();
            this.chkLoadTipsAtStartup = new DevExpress.XtraEditors.CheckEdit();
            this.button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.button_Next = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.pictureBox1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.chkLoadTipsAtStartup.Properties).BeginInit();
            this.SuspendLayout();
            //
            //panel1
            //
            this.panel1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblTipText);
            this.panel1.Location = new System.Drawing.Point(10, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(254, 163);
            this.panel1.TabIndex = 3;
            //
            //pictureBox1
            //
            this.pictureBox1.Image = (System.Drawing.Image)resources.GetObject("pictureBox1.Image");
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(8, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            //
            //label1
            //
            this.label1.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.label1.Appearance.Font = new System.Drawing.Font("Arial", 8f);
            this.label1.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label1.Appearance.Options.UseBackColor = true;
            this.label1.Appearance.Options.UseFont = true;
            this.label1.Appearance.Options.UseForeColor = true;
            this.label1.Appearance.Options.UseTextOptions = true;
            this.label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(40, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Did you know...";
            //
            //lblTipText
            //
            this.lblTipText.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lblTipText.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.lblTipText.Appearance.Font = new System.Drawing.Font("Arial", 8f);
            this.lblTipText.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTipText.Appearance.Options.UseBackColor = true;
            this.lblTipText.Appearance.Options.UseFont = true;
            this.lblTipText.Appearance.Options.UseForeColor = true;
            this.lblTipText.Appearance.Options.UseTextOptions = true;
            this.lblTipText.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lblTipText.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblTipText.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblTipText.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipText.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblTipText.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblTipText.Location = new System.Drawing.Point(41, 42);
            this.lblTipText.Name = "lblTipText";
            this.lblTipText.Size = new System.Drawing.Size(210, 118);
            this.lblTipText.TabIndex = 1;
            this.lblTipText.Text = "XXXXXXXXXXX";
            this.lblTipText.UseMnemonic = false;
            //
            //chkLoadTipsAtStartup
            //
            this.chkLoadTipsAtStartup.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.chkLoadTipsAtStartup.Location = new System.Drawing.Point(8, 182);
            this.chkLoadTipsAtStartup.Name = "chkLoadTipsAtStartup";
            this.chkLoadTipsAtStartup.Properties.Caption = "Show Tips at Startup";
            this.chkLoadTipsAtStartup.Size = new System.Drawing.Size(128, 19);
            this.chkLoadTipsAtStartup.TabIndex = 2;
            this.chkLoadTipsAtStartup.ToolTip = "If you do not wish to see this form the next time that you start the application," + " check this box.";
            this.chkLoadTipsAtStartup.ToolTipController = this.ToolTipController1;
            //
            //button_OK
            //
            this.button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_OK.Location = new System.Drawing.Point(272, 17);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 25);
            this.button_OK.TabIndex = 0;
            this.button_OK.Text = "&OK";
            this.button_OK.ToolTip = "Click here to dismiss this form.";
            this.button_OK.ToolTipController = this.ToolTipController1;
            //
            //button_Next
            //
            this.button_Next.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.button_Next.Location = new System.Drawing.Point(272, 52);
            this.button_Next.Name = "button_Next";
            this.button_Next.Size = new System.Drawing.Size(75, 24);
            this.button_Next.TabIndex = 1;
            this.button_Next.Text = "&Next Tip";
            this.button_Next.ToolTip = "If you wish to see the next tip, press this button.";
            this.button_Next.ToolTipController = this.ToolTipController1;
            //
            //Form_Tips
            //
            this.AcceptButton = this.button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(360, 213);
            this.Controls.Add(this.button_Next);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkLoadTipsAtStartup);
            this.Name = "Form_Tips";
            this.Text = "Tip of the Day";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.pictureBox1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.chkLoadTipsAtStartup.Properties).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl label1;
        private DevExpress.XtraEditors.LabelControl lblTipText;
        private DevExpress.XtraEditors.SimpleButton button_OK;
        private DevExpress.XtraEditors.SimpleButton button_Next;
        private DevExpress.XtraEditors.CheckEdit chkLoadTipsAtStartup;
        private System.Windows.Forms.Panel panel1;
    }
}
