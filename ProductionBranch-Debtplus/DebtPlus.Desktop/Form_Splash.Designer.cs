﻿namespace DebtPlus.Desktop
{
    partial class Form_Splash
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Splash));
            this.lblLicenseTo = new System.Windows.Forms.TextBox();
            this.lblProductName = new DevExpress.XtraEditors.LabelControl();
            this.lblPlatform = new DevExpress.XtraEditors.LabelControl();
            this.lblVersion = new DevExpress.XtraEditors.LabelControl();
            this.lblWarning = new DevExpress.XtraEditors.LabelControl();
            this.lblCompany = new DevExpress.XtraEditors.LabelControl();
            this.lblCopyright = new DevExpress.XtraEditors.LabelControl();
            this.imgLogo = new System.Windows.Forms.PictureBox();
            this.label1 = new DevExpress.XtraEditors.LabelControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.imgLogo).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.checkEdit1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ToolTipController1
            //
            this.ToolTipController1.Active = false;
            //
            //lblLicenseTo
            //
            this.lblLicenseTo.AcceptsReturn = true;
            this.lblLicenseTo.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.lblLicenseTo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblLicenseTo.CausesValidation = false;
            this.lblLicenseTo.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblLicenseTo.Enabled = false;
            this.lblLicenseTo.Font = new System.Drawing.Font("Arial", 8f);
            this.lblLicenseTo.ForeColor = System.Drawing.Color.Black;
            this.lblLicenseTo.Location = new System.Drawing.Point(8, 9);
            this.lblLicenseTo.MaxLength = 0;
            this.lblLicenseTo.Multiline = true;
            this.lblLicenseTo.Name = "lblLicenseTo";
            this.lblLicenseTo.ReadOnly = true;
            this.lblLicenseTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblLicenseTo.Size = new System.Drawing.Size(457, 52);
            this.lblLicenseTo.TabIndex = 16;
            this.lblLicenseTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            //
            //lblProductName
            //
            this.lblProductName.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblProductName.Appearance.Font = new System.Drawing.Font("Arial", 24f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            this.lblProductName.Appearance.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
            this.lblProductName.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblProductName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblProductName.Location = new System.Drawing.Point(160, 69);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(138, 37);
            this.lblProductName.TabIndex = 15;
            this.lblProductName.Text = "DebtPlus";
            //
            //lblPlatform
            //
            this.lblPlatform.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPlatform.Appearance.Font = new System.Drawing.Font("Arial", 15.75f, System.Drawing.FontStyle.Bold);
            this.lblPlatform.Appearance.ForeColor = System.Drawing.Color.FromArgb(0, 128, 0);
            this.lblPlatform.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblPlatform.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblPlatform.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lblPlatform.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblPlatform.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblPlatform.Location = new System.Drawing.Point(320, 129);
            this.lblPlatform.Name = "lblPlatform";
            this.lblPlatform.Size = new System.Drawing.Size(148, 24);
            this.lblPlatform.TabIndex = 14;
            this.lblPlatform.Text = "iNTEL Pentium";
            //
            //lblVersion
            //
            this.lblVersion.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Appearance.Font = new System.Drawing.Font("Arial", 12f, System.Drawing.FontStyle.Bold);
            this.lblVersion.Appearance.ForeColor = System.Drawing.Color.FromArgb(0, 128, 0);
            this.lblVersion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblVersion.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lblVersion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.lblVersion.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblVersion.Location = new System.Drawing.Point(152, 155);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(63, 19);
            this.lblVersion.TabIndex = 13;
            this.lblVersion.Text = "Version ";
            //
            //lblWarning
            //
            this.lblWarning.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblWarning.Appearance.Font = new System.Drawing.Font("Arial", 9.75f);
            this.lblWarning.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblWarning.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblWarning.Location = new System.Drawing.Point(8, 258);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(383, 16);
            this.lblWarning.TabIndex = 12;
            this.lblWarning.Text = "Use is governed by the license agreement with your organization.";
            //
            //lblCompany
            //
            this.lblCompany.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblCompany.Appearance.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Bold);
            this.lblCompany.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCompany.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCompany.Location = new System.Drawing.Point(224, 215);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(101, 16);
            this.lblCompany.TabIndex = 11;
            this.lblCompany.Text = "DebtPlus, L.L.C.";
            //
            //lblCopyright
            //
            this.lblCopyright.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblCopyright.Appearance.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Bold);
            this.lblCopyright.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCopyright.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCopyright.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCopyright.Location = new System.Drawing.Point(224, 198);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(125, 16);
            this.lblCopyright.TabIndex = 10;
            this.lblCopyright.Text = "Copyright 1999-2003";
            //
            //imgLogo
            //
            this.imgLogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.imgLogo.Image = (System.Drawing.Image)resources.GetObject("imgLogo.Image");
            this.imgLogo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.imgLogo.Location = new System.Drawing.Point(24, 69);
            this.imgLogo.Name = "imgLogo";
            this.imgLogo.Size = new System.Drawing.Size(121, 137);
            this.imgLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgLogo.TabIndex = 17;
            this.imgLogo.TabStop = false;
            //
            //label1
            //
            this.label1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.label1.Appearance.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Bold);
            this.label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(224, 233);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "El Dorado Hills, California";
            //
            //timer1
            //
            //
            //checkEdit1
            //
            this.checkEdit1.Location = new System.Drawing.Point(6, 280);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(0, 0, 192);
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "Do not display this again";
            this.checkEdit1.Size = new System.Drawing.Size(148, 19);
            this.checkEdit1.TabIndex = 18;
            //
            //Form_Splash
            //
            this.Appearance.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.Appearance.Options.UseBackColor = true;
            this.ClientSize = new System.Drawing.Size(480, 304);
            this.Controls.Add(this.checkEdit1);
            this.Controls.Add(this.lblLicenseTo);
            this.Controls.Add(this.lblProductName);
            this.Controls.Add(this.lblPlatform);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.imgLogo);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form_Splash";
            this.ShowInTaskbar = false;
            this.Text = "SplashForm";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.imgLogo).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.checkEdit1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.TextBox lblLicenseTo;
        private DevExpress.XtraEditors.LabelControl lblProductName;
        private DevExpress.XtraEditors.LabelControl lblPlatform;
        private DevExpress.XtraEditors.LabelControl lblVersion;
        private DevExpress.XtraEditors.LabelControl lblWarning;
        private DevExpress.XtraEditors.LabelControl lblCompany;
        private DevExpress.XtraEditors.LabelControl lblCopyright;
        private System.Windows.Forms.PictureBox imgLogo;
        private DevExpress.XtraEditors.LabelControl label1;
        private System.Windows.Forms.Timer timer1;
    }
}
