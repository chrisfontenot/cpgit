#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraTreeList;

namespace DebtPlus.Desktop
{
    internal class MainMenuArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Initial system name from the registry to be used
        /// </summary>
        private string _systemName = string.Empty;
        public string systemName
        {
            get { return _systemName; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public MainMenuArgParser()
            : base(new string[] { "s" })
        {
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "s":
                    _systemName = switchValue;
                    break;
                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;
                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " [-s Name]")
            //AppendErrorLine("       -s : Initial database system name to be used")
        }
    }
}
