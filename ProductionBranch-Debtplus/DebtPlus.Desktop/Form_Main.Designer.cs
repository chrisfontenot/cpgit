﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Desktop
{
    partial class Form_Main
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.treeListColumn_attributes = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_SystemNames = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_Skin_Names = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem_Options_Reset = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Expand = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Collapse = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn_text = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn_Arguments = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn_Type = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn_menu_level = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.barButtonItem_WhoAmI = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // treeListColumn_attributes
            // 
            this.treeListColumn_attributes.Caption = "Attributes";
            this.treeListColumn_attributes.CustomizationCaption = "Attributes";
            this.treeListColumn_attributes.FieldName = "attributes";
            this.treeListColumn_attributes.Name = "treeListColumn_attributes";
            this.treeListColumn_attributes.OptionsColumn.AllowEdit = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("category", new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c"))});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barStaticItem1,
            this.barSubItem_SystemNames,
            this.barButtonItem4,
            this.barSubItem5,
            this.barButtonItem_Options_Reset,
            this.barSubItem_Skin_Names,
            this.barButtonItem5,
            this.barButtonItem_Expand,
            this.barButtonItem_Collapse,
            this.barButtonItem_WhoAmI});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 17;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "&File";
            this.barSubItem1.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_SystemNames),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barSubItem_SystemNames
            // 
            this.barSubItem_SystemNames.Caption = "Systems";
            this.barSubItem_SystemNames.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barSubItem_SystemNames.Description = "Configured systems";
            this.barSubItem_SystemNames.Id = 7;
            this.barSubItem_SystemNames.MenuCaption = "List of configured systems";
            this.barSubItem_SystemNames.Name = "barSubItem_SystemNames";
            this.barSubItem_SystemNames.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing;
            // 
            // barButton_File_Exit
            // 
            this.barButtonItem1.Caption = "&Exit";
            this.barButtonItem1.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem1.Id = 3;
            this.barButtonItem1.Name = "barButton_File_Exit";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "&View";
            this.barSubItem2.Id = 1;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Skin_Names),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Expand, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Collapse)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem_Skin_Names
            // 
            this.barSubItem_Skin_Names.Caption = "Skins";
            this.barSubItem_Skin_Names.Id = 11;
            this.barSubItem_Skin_Names.Name = "barSubItem_Skin_Names";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Options";
            this.barSubItem5.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barSubItem5.Id = 9;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Options_Reset)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem_Options_Reset
            // 
            this.barButtonItem_Options_Reset.Caption = "Reset...";
            this.barButtonItem_Options_Reset.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem_Options_Reset.Id = 10;
            this.barButtonItem_Options_Reset.Name = "barButtonItem_Options_Reset";
            // 
            // barButtonItem_View_Tree
            // 
            this.barButtonItem2.Caption = "Tool Tips";
            this.barButtonItem2.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem2.Id = 4;
            this.barButtonItem2.Name = "barButtonItem_View_Tree";
            // 
            // barButtonItem_Expand
            // 
            this.barButtonItem_Expand.Caption = "Expand All";
            this.barButtonItem_Expand.Id = 14;
            this.barButtonItem_Expand.Name = "barButtonItem_Expand";
            // 
            // barButtonItem_Collapse
            // 
            this.barButtonItem_Collapse.Caption = "Collapse All";
            this.barButtonItem_Collapse.Id = 15;
            this.barButtonItem_Collapse.Name = "barButtonItem_Collapse";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "&Help";
            this.barSubItem3.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barSubItem3.Id = 2;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_WhoAmI)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "About DebtPlus...";
            this.barButtonItem3.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem3.Id = 5;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(506, 25);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 273);
            this.barDockControlBottom.Size = new System.Drawing.Size(506, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 248);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(506, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 248);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticItem1.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barStaticItem1.Id = 6;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem1.Width = 32;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Default";
            this.barButtonItem4.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem4.Id = 8;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.CategoryGuid = new System.Guid("40e286ac-055c-4dd8-ae08-bae9f0da9e7c");
            this.barButtonItem5.Id = 12;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.treeList1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 25);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(506, 248);
            this.panelControl1.TabIndex = 4;
            // 
            // treeList1
            // 
            this.treeList1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.treeList1.Appearance.Empty.Options.UseBackColor = true;
            this.treeList1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(242)))), ((int)(((byte)(254)))));
            this.treeList1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.EvenRow.Options.UseBackColor = true;
            this.treeList1.Appearance.EvenRow.Options.UseForeColor = true;
            this.treeList1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.treeList1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeList1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.treeList1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(106)))), ((int)(((byte)(197)))));
            this.treeList1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.treeList1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.treeList1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.treeList1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeList1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.treeList1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeList1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeList1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.treeList1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.treeList1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.treeList1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeList1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeList1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.GroupButton.Options.UseBackColor = true;
            this.treeList1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.treeList1.Appearance.GroupButton.Options.UseForeColor = true;
            this.treeList1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeList1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(216)))), ((int)(((byte)(247)))));
            this.treeList1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.treeList1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.treeList1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.treeList1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeList1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(171)))), ((int)(((byte)(228)))));
            this.treeList1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(236)))), ((int)(((byte)(254)))));
            this.treeList1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.treeList1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.treeList1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.treeList1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.treeList1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(153)))), ((int)(((byte)(228)))));
            this.treeList1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(224)))), ((int)(((byte)(251)))));
            this.treeList1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.treeList1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.treeList1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.treeList1.Appearance.HorzLine.Options.UseBackColor = true;
            this.treeList1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.treeList1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.OddRow.Options.UseBackColor = true;
            this.treeList1.Appearance.OddRow.Options.UseForeColor = true;
            this.treeList1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(252)))), ((int)(((byte)(255)))));
            this.treeList1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(129)))), ((int)(((byte)(185)))));
            this.treeList1.Appearance.Preview.Options.UseBackColor = true;
            this.treeList1.Appearance.Preview.Options.UseForeColor = true;
            this.treeList1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.treeList1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.treeList1.Appearance.Row.Options.UseBackColor = true;
            this.treeList1.Appearance.Row.Options.UseForeColor = true;
            this.treeList1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(126)))), ((int)(((byte)(217)))));
            this.treeList1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.treeList1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.treeList1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.treeList1.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.treeList1.Appearance.TreeLine.Options.UseBackColor = true;
            this.treeList1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(127)))), ((int)(((byte)(196)))));
            this.treeList1.Appearance.VertLine.Options.UseBackColor = true;
            this.treeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn_text,
            this.treeListColumn_attributes,
            this.treeListColumn_Arguments,
            this.treeListColumn_Type,
            this.treeListColumn_menu_level});
            this.treeList1.Dock = System.Windows.Forms.DockStyle.Fill;
            styleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            styleFormatCondition1.Appearance.Options.UseFont = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.treeListColumn_attributes;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            styleFormatCondition1.Value1 = 0;
            this.treeList1.FormatConditions.AddRange(new DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition[] {
            styleFormatCondition1});
            this.treeList1.KeyFieldName = "seq";
            this.treeList1.Location = new System.Drawing.Point(2, 2);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.AutoPopulateColumns = false;
            this.treeList1.OptionsBehavior.Editable = false;
            this.treeList1.OptionsView.EnableAppearanceEvenRow = true;
            this.treeList1.OptionsView.EnableAppearanceOddRow = true;
            this.treeList1.OptionsView.ShowIndicator = false;
            this.treeList1.ParentFieldName = "parentseq";
            this.treeList1.Size = new System.Drawing.Size(502, 244);
            this.treeList1.TabIndex = 1;
            // 
            // treeListColumn_text
            // 
            this.treeListColumn_text.Caption = "Description";
            this.treeListColumn_text.CustomizationCaption = "Description";
            this.treeListColumn_text.FieldName = "text";
            this.treeListColumn_text.Name = "treeListColumn_text";
            this.treeListColumn_text.OptionsColumn.AllowEdit = false;
            this.treeListColumn_text.OptionsColumn.AllowSort = false;
            this.treeListColumn_text.Visible = true;
            this.treeListColumn_text.VisibleIndex = 0;
            this.treeListColumn_text.Width = 288;
            // 
            // treeListColumn_Arguments
            // 
            this.treeListColumn_Arguments.Caption = "ap";
            this.treeListColumn_Arguments.CustomizationCaption = "ap";
            this.treeListColumn_Arguments.FieldName = "argument";
            this.treeListColumn_Arguments.Name = "treeListColumn_Arguments";
            // 
            // treeListColumn_Type
            // 
            this.treeListColumn_Type.Caption = "ClassName";
            this.treeListColumn_Type.CustomizationCaption = "Class Name";
            this.treeListColumn_Type.FieldName = "type";
            this.treeListColumn_Type.Name = "treeListColumn_Type";
            // 
            // treeListColumn_menu_level
            // 
            this.treeListColumn_menu_level.Caption = "Level";
            this.treeListColumn_menu_level.CustomizationCaption = "Menu Level";
            this.treeListColumn_menu_level.FieldName = "menu_level";
            this.treeListColumn_menu_level.Format.FormatString = "f0";
            this.treeListColumn_menu_level.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.treeListColumn_menu_level.Name = "treeListColumn_menu_level";
            this.treeListColumn_menu_level.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Integer;
            // 
            // barButtonItem_WhoAmI
            // 
            this.barButtonItem_WhoAmI.Caption = "Who Am I...";
            this.barButtonItem_WhoAmI.Id = 16;
            this.barButtonItem_WhoAmI.Name = "barButtonItem_WhoAmI";
            // 
            // Form_Main
            // 
            this.ClientSize = new System.Drawing.Size(506, 273);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "DebtPlus Debt Management System";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.ResumeLayout(false);

        }

        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar bar2;
        internal DevExpress.XtraBars.BarSubItem barSubItem1;
        internal DevExpress.XtraBars.BarSubItem barSubItem2;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem1;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem2;
        internal DevExpress.XtraBars.BarSubItem barSubItem3;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem3;
        internal DevExpress.XtraBars.BarStaticItem barStaticItem1;
        internal DevExpress.XtraBars.BarSubItem barSubItem_SystemNames;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem4;
        internal DevExpress.XtraEditors.PanelControl panelControl1;
        internal DevExpress.XtraTreeList.TreeList treeList1;
        internal DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn_text;
        internal DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn_attributes;
        internal DevExpress.XtraBars.BarSubItem barSubItem5;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem_Options_Reset;
        internal DevExpress.XtraBars.BarSubItem barSubItem_Skin_Names;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem5;
        internal DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn_Arguments;
        internal DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn_Type;
        internal DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn_menu_level;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem_Expand;
        internal DevExpress.XtraBars.BarButtonItem barButtonItem_Collapse;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_WhoAmI;
    }
}
