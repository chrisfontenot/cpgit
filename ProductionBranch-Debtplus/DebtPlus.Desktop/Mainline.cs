#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.UI.Common;

// Required for the logger information
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace DebtPlus.Desktop
{
    /// <summary>
    /// System required class to handle the program instantiation.
    /// </summary>
    public static class MainlineThreadClass
    {
        /// <summary>
        /// Main Entry to the program
        /// </summary>
        /// <param name="args">List of arguments to the program file</param>
        [STAThread()]
        public static void Main(string[] args)
        {
            using (var processingClass = new Mainline())
            {
                processingClass.Main(args);
            }
        }
    }

    /// <summary>
    /// Class to process the mainline to DebtPlus.
    /// </summary>
    public partial class Mainline : System.IDisposable
    {
        // Then, in each class, the logging information is required.
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        partial void OnStart();
        partial void OnCreate();

        /// <summary>
        /// Create a new instance of our processing class
        /// </summary>
        public Mainline()
        {
            OnCreate();
        }

        public void Main(string[] args)
        {
            // Do any one-time startup initialization that may be needed
            OnStart();

            // Save the information in the repository
            DebtPlus.Repository.Setup.ConnectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            DebtPlus.Repository.Setup.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout;

            // Log the start of the executable
            log.Info("Program starting");

            Startup.Register(args);

            // Load the assemblies that are referenced in this file
            ForceAllToLoad();

            // Register the handlers
            Application.ThreadException += ThreadExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;

            // Parse the arguments
            MainMenuArgParser ap = new MainMenuArgParser();
            if (ap.Parse(args))
            {
                Application.Run(new Form_Main(ap));
            }

            log.Info("Program terminating");
        }

        /// <summary>
        /// Force all referenced assemblies to be loaded into the current process. This is used by the
        /// Desktop process to ensure that the referenced types are able to be located. Of course, it slows
        /// down the loading of the desktop to force all possible references to be resolved, but it was slow
        /// in the first place.
        /// </summary>
        public static void ForceAllToLoad()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            var loadedPaths = loadedAssemblies.Select(a => a.Location).ToArray();
            var referencedPaths = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
            var toLoad = referencedPaths.Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase)).ToList();

            try
            {
                toLoad.ForEach(path => loadedAssemblies.Add(AppDomain.CurrentDomain.Load(System.Reflection.AssemblyName.GetAssemblyName(path))));
            }
            catch { }
        }

        private static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            // log.Fatal("UnhandledExceptionHandler Exception occurred", ex)
            DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Un-handled exception Event Handler", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private static void ThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            Exception ex = e.Exception;
            // log.Fatal("ThreadExceptionHandler Exception occurred", ex)
            DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Thread Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        /// <summary>
        /// Handle the dispose event processing
        /// </summary>
        /// <param name="Disposing">TRUE if being called from Dispose(). FALSE otherwise.</param>
        protected virtual void Dispose(bool Disposing)
        {
        }

        /// <summary>
        /// Dispose of the local storage for this class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Termination routine for the class.
        /// </summary>
        ~Mainline()
        {
            Dispose(false);
        }
    }
}
