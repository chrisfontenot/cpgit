#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Resources;

namespace DebtPlus.Desktop
{
    public partial class Form_Tips : DebtPlus.Data.Forms.DebtPlusForm
    {

        // The in-memory database of tips.
        StringCollection Tips = new StringCollection();
        Int32 CurrentTip;

        // Name of tips file
        const string TIP_FILE = "TIPOFDAY.TXT";

        public Form_Tips()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            button_Next.Click                      += Button_Next_Click;
            chkLoadTipsAtStartup.CheckStateChanged += chkLoadTipsAtStartup_CheckStateChanged;
            Load                                   += frmTip_Load;
            GotFocus                               += Form_Tips_GotFocus;
        }

        private void UnRegisterHandlers()
        {
            button_Next.Click                      -= Button_Next_Click;
            chkLoadTipsAtStartup.CheckStateChanged -= chkLoadTipsAtStartup_CheckStateChanged;
            Load                                   -= frmTip_Load;
            GotFocus                               -= Form_Tips_GotFocus;
        }

        // Resource manager information
        protected static ResourceManager RM = new ResourceManager("DebtPlus.Desktop.Form_Tips_Strings", Assembly.GetExecutingAssembly());
        protected string ApplicationName()
        {
            return "DebtPlus.Tips";
        }

        public bool LoadTips(string sFile)
        {
            try
            {
                using (StreamReader rb = new StreamReader(sFile))
                {
                    string LineText = null;
                    LineText = rb.ReadLine();
                    while (LineText != null)
                    {
                        Tips.Add(LineText);
                        LineText = rb.ReadLine();
                    }

                    return true;
                }
            }
            catch { }

            return false;
        }

        private void chkLoadTipsAtStartup_CheckStateChanged(object eventSender, EventArgs eventArgs)
        {
            DebtPlus.Configuration.Config.ShowStartupTips = chkLoadTipsAtStartup.Checked;
        }

        private void Form_Tips_GotFocus(object sender, EventArgs e)
        {
            this.TopMost = false;
        }

        private void frmTip_Load(object eventSender, EventArgs eventArgs)
        {
            // Set the checked state accordingly
            UnRegisterHandlers();
            try
            {
                chkLoadTipsAtStartup.Checked = DebtPlus.Configuration.Config.ShowStartupTips;

                // Find the path to the executing program. Perhaps the tips file is there
                Assembly asm = Assembly.GetExecutingAssembly();
                string fname = asm.Location;
                string path = System.IO.Path.GetDirectoryName(fname);

                fname = System.IO.Path.Combine(path, TIP_FILE);
                if (File.Exists(fname))
                {
                    LoadTips(fname);
                }

                if (Tips.Count <= 0)
                {
                    Tips.Add(string.Format("That the {0} file was not found? ", TIP_FILE) + "\r\n\r\nCreate a text file named " + TIP_FILE + " using NotePad with 1 tip per line. " + "Then place it in the directory " + fname + ". ");
                }

                // Select a tip at random.
                DateTime dt = DateTime.Now;
                var rnd = new System.Random(Convert.ToInt32(dt.Ticks % 0x7fffffffL));
                CurrentTip = Convert.ToInt32(System.Math.Floor(Convert.ToDouble(Tips.Count) * rnd.NextDouble()));

                if (CurrentTip <= 0)
                {
                    CurrentTip = 1;
                }

                // If there is only one tip then there is no "next tip"
                if (Tips.Count < 2)
                {
                    button_Next.Enabled = false;
                }

                // Display a tip at random.
                DisplayCurrentTip();
            }

            finally
            {
                RegisterHandlers();
            }
        }

        private void DisplayCurrentTip()
        {
            if (Tips.Count > 0)
            {
                lblTipText.Text = Tips[CurrentTip - 1];
                lblTipText.Refresh();
            }
        }

        private void Button_Next_Click(object sender, EventArgs e)
        {
            // Cycle to the next sequential tip
            CurrentTip = CurrentTip + 1;
            if (Tips.Count < CurrentTip)
            {
                CurrentTip = 1;
            }

            // Display a tip at random.
            DisplayCurrentTip();
        }
    }
}
