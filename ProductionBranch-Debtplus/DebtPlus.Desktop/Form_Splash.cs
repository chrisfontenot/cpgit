#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Drawing;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;

namespace DebtPlus.Desktop
{
    public partial class Form_Splash : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Resource manager information
        protected ResourceManager RM = new ResourceManager("DebtPlus.Desktop.Form_Splash_Strings", Assembly.GetExecutingAssembly());

        public Form_Splash()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                      += SplashForm_Load;
            Closed                    += SplashForm_Closed;
            checkEdit1.CheckedChanged += checkEdit1_CheckedChanged;
            timer1.Tick               += Timer1_Tick;
        }

        private void UnRegisterHandlers()
        {
            Load                      -= SplashForm_Load;
            Closed                    -= SplashForm_Closed;
            checkEdit1.CheckedChanged -= checkEdit1_CheckedChanged;
            timer1.Tick               -= Timer1_Tick;
        }

        // Tripped when the form is closed
        public event EventHandler Completed;

        protected void RaiseCompleted(EventArgs e)
        {
            var evt = Completed;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnCompleted(EventArgs e)
        {
            RaiseCompleted(e);
        }

        private void SplashForm_Closed(object sender, EventArgs e)
        {
            OnCompleted(EventArgs.Empty);
        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            string strName = DebtPlus.Configuration.Config.InstallationName;
            string strCompany = DebtPlus.Configuration.Config.InstallationCompany;

            UnRegisterHandlers();
            try
            {
                if (strName == string.Empty)
                {
                    lblLicenseTo.Font = new Font(lblLicenseTo.Font.FontFamily, 12, FontStyle.Bold, GraphicsUnit.Pixel);
                    lblLicenseTo.ForeColor = Color.Red;
                    strName = RM.GetString("load.unregistered_version");
                }
                else
                {
                    strName = RM.GetString("load.licensed_to") + Environment.NewLine + strName;
                    if (strCompany != string.Empty)
                    {
                        strName = strName + Environment.NewLine + strCompany;
                    }
                    lblLicenseTo.Font = new Font(lblLicenseTo.Font, FontStyle.Bold);
                }

                lblLicenseTo.Text = strName;

                // Set the version from the program version string
                lblVersion.Text = string.Format("{0} Built: {1:d}", DebtPlus.Configuration.AssemblyInfo.Version, BuildDate());

                lblProductName.Text = "DebtPlus Desktop";

                // Force the form over the parent
                BringToFront();

                // Start a timer to bring up our dialog
                Opacity = 0.0;
                timer1.Interval = 140;
                timer1.Enabled = true;
                timer1.Start();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private DateTime BuildDate()
        {
            DateTime answer = DateTime.MinValue;
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            string l = a.Location;
            if (System.IO.File.Exists(l))
            {
                answer = System.IO.File.GetCreationTime(l);
            }
            return answer;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            // If our window is fully shown then it is time to terminate the dialog.
            if (Opacity == 1.0)
            {
                timer1.Stop();
                DialogResult = DialogResult.Cancel;
                return;
            }

            // Bring up our window a bit at a time. When we reach full then set the time delay to 3 seconds.
            Opacity += 0.1;
            if (Opacity == 1.0)
            {
                timer1.Stop();
                timer1.Interval = 3000;
                timer1.Start();
            }
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            DebtPlus.Configuration.Config.ShowSplash = ! checkEdit1.Checked;
        }
    }
}
