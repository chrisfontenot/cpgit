using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes
[assembly: AssemblyTitle("DebtPlus.Desktop")] 
[assembly: AssemblyDescription("")] 
[assembly: AssemblyCompany("")] 
[assembly: AssemblyProduct("DebtPlus.Desktop")] 
[assembly: AssemblyCopyright("Copyright ©  2013")] 
[assembly: AssemblyTrademark("")] 
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("610a2009-271c-4254-b814-7fe407e3e101")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the //*// as shown below:
// [assembly: AssemblyVersion("1.0.*")] 

[assembly: AssemblyVersion("17.4.20.0227")] 
[assembly: AssemblyInformationalVersion("17.4.20.0227")] 
[assembly: AssemblyFileVersion("17.4.20.0227")] 
