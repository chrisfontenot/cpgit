﻿// To enable logging for debugging purposes, remove the comment sequence from the following line
// #define LOGTRANSFER

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Server.FHLB.Upload
{
    public abstract class Processing : System.IDisposable
    {
        /// <summary>
        /// Initialize the new class.
        /// </summary>
        /// <remarks>
		/// It is an empty procedure for the time being since we don't need initialization.
        /// </remarks>
        public Processing()
        {
        }

        /// <summary>
        /// Process the submission of the client appointment data to the remote site
        /// </summary>
        public void ProcessUpload()
        {
			// Establish the database connection at this point. We need to retrieve the items to be processed.
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlus"].ConnectionString;
            using (var dc = new DebtPlusDataClassDataContext(connectionString))
            {
                // Retrieve the list of appointments that we need to send to the remote system.
                System.Linq.IQueryable<client_appointment> q = dc.client_appointments;
                q = q.Where(s => s.date_uploaded == null);

                // The appointment type needs to be valid. We look only for appointment type #155. This value is canned and fixed.
                q = q.Where(s => s.appt_type == 155);

                // The referral sources needs to be valid
                int[] colResults = dc.referred_bies.Where(s => s.description.StartsWith("FHLB ")).Select(s => s.Id).ToArray<int>();
                if (colResults.GetUpperBound(0) >= 0)
                {
                    q = q.Where(s => colResults.Contains(s.referred_by.GetValueOrDefault()));
                }

                // The status must be completed
                q = q.Where(s => new char[] { 'K', 'W' }.Contains(s.status));

                // Retrieve the list of appointments to be processed
                List<client_appointment> colAppointments = q.ToList();

                // Build the output buffer for the file. If there is nothing to send then just exit.
                string OutputText = EncodeSubmissionText(dc, colAppointments);
                if (string.IsNullOrEmpty(OutputText))
                {
                    return;
                }

                // Submit the file to the remote system
                if (!SendUploadFile(OutputText))
                {
                    return;
                }

                // Mark the transactions as having been uploaded
                DateTime now = System.DateTime.Now;
                colAppointments.ForEach(s => s.date_uploaded = now);

                // Submit the changes to the database
                dc.SubmitChanges();
            }
        }

        /// <summary>
        /// Generate the text string for the output file to be submitted
        /// </summary>
        /// <param name="colAppointments"></param>
        /// <returns></returns>
        private string EncodeSubmissionText(DebtPlusDataClassDataContext dc, System.Collections.Generic.List<client_appointment> colAppointments)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var item in colAppointments)
            {
				// Format the client name information
                Name n = getNameRecord(dc, item.client);
                sb.AppendFormat("\r\n{2},{1},{0}", new object[] {
                                                       /* 0 => */   (n == null ? string.Empty : n.First ?? string.Empty),
                                                       /* 1 => */   (n == null ? string.Empty : n.Last ?? string.Empty),
                                                       /* 2 => */   item.partner
                                                                });
            }

			// Add the leading header line if there is data to be sent. Leave the leading cr/lf as it will be after the header line.
            if (sb.Length > 0)
            {
                sb.Insert(0, "Reservation Number,Last Name,First Name");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Retrieve the "name" record from the database associated with the client.
        /// </summary>
        private Name getNameRecord(DebtPlusDataClassDataContext dc, Int32 clientID)
        {
            // Find the people table entry for the applicant record
            var p = dc.peoples.Where(s => s.Client == clientID && s.Relation == 1).FirstOrDefault();

            // If there is one then look for the applicant's name record.
            if (p != null && p.NameID != null)
            {
                // If one is found, return that record to the caller.
                var n = dc.Names.Where(s => s.Id == p.NameID).FirstOrDefault();
                if (n != null)
                {
                    return n;
                }
            }

            // Return a null value to indicate that there is no name.
            return null;
        }

        /// <summary>
        /// Send the data to the remote site
        /// </summary>
        /// <param name="outputText">Text buffer to be written to the remote host.</param>
        /// <returns></returns>
        protected abstract bool SendUploadFile(string outputText);

        /// <summary>
        /// Handle the dispose/finalize references
        /// </summary>
        /// <param name="Disposing">Type of call. TRUE = dispose(), FALSE = finalize()</param>
        protected virtual void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Do the Dispose function. It is basically simple. We don't expand upon it. We just dispose of our storage.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Finalize the class storage. We need to discard any large objects at this time.
        /// </summary>
        ~Processing()
        {
            Dispose(false);
        }
    }
}