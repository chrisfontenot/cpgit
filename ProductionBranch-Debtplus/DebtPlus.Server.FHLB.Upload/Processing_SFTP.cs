﻿// To enable logging for debugging purposes, remove the comment sequence from the following line
// #define LOGTRANSFER

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Server.FHLB.Upload
{
    public class Processing_SFTP : Processing
    {
        /// <summary>
        /// Initialize the new class.
        /// </summary>
        /// <remarks>
		/// It is an empty procedure for the time being since we don't need initialization.
        /// </remarks>
        public Processing_SFTP() : base()
        {
        }

        /// <summary>
        /// Send the data to the remote site
        /// </summary>
        /// <param name="outputText">Text buffer to be written to the remote host.</param>
        /// <returns></returns>
        protected override bool SendUploadFile(string outputText)
        {
            // Pointer to the current client
            Tamir.SharpSsh.Sftp ftpClient = null;

            // Determine the current property settings
            string pathName     = Properties.Settings.Default.FTP_Directory;
            string hostName     = Properties.Settings.Default.FTP_URL;
            string userName     = Properties.Settings.Default.FTP_UserName;
            string password     = Properties.Settings.Default.FTP_Password;
            string fileTemplate = Properties.Settings.Default.FTP_FileName;
            int portNumber      = Properties.Settings.Default.FTP_Port;

            // Filename for the upload. This varies depending upon the date/time and the editing template from the config file.
            string fileName = System.IO.Path.GetFileName(string.Format(fileTemplate, DateTime.Now));

            // Delete the current file by this name from the directory if it is still lying around.
            string temporaryPath = System.IO.Path.GetTempPath();
            string temporaryFileName = System.IO.Path.Combine(temporaryPath, fileName);
            if (System.IO.File.Exists(temporaryFileName))
            {
                System.IO.File.Delete(temporaryFileName);
            }

            try
            {
                // Open the file with a creation operation. This will fail if the file is currently present.
                using (var stream = new System.IO.FileStream(temporaryFileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.ReadWrite))
                {
                    // Convert the string to an array of bytes
                    byte[] byteArray = Encoding.ASCII.GetBytes(outputText);

                    int offset = 0;
                    long bytes = byteArray.Length;

                    // Write the first part of the buffer to the file, 4096 bytes per write.
                    while (bytes > 4096L)
                    {
                        stream.Write(byteArray, offset, 4096);
                        offset += 4096;
                        bytes  -= 4096L;
                    }

                    // Write the last block and close the stream.
                    if (bytes > 0L)
                    {
                        stream.Write(byteArray, offset, (int)bytes);
                    }

                    stream.Flush();
                    stream.Close();
                }

                // Create the connection to the remote site
                ftpClient                     = new Tamir.SharpSsh.Sftp(hostName, userName, password);
				
#if LOGTRANSFER
                ftpClient.OnTransferStart    += new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferStart);
                ftpClient.OnTransferProgress += new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferProgress);
                ftpClient.OnTransferEnd      += new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferEnd);
#endif

                // Send the file to the remote host
                ftpClient.Connect(portNumber);
                string targetName = System.IO.Path.Combine(pathName, fileName);
                ftpClient.Put(temporaryFileName, targetName);
#if LOGTRANSFER
                ftpClient.OnTransferStart    -= new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferStart);
                ftpClient.OnTransferProgress -= new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferProgress);
                ftpClient.OnTransferEnd      -= new Tamir.SharpSsh.FileTransferEvent(ftpClient_OnTransferEnd);
#endif

                return true;
            }

            finally
            {
                try
                {
                    // Close the file. This may fail. However, since it is the close and
                    // we are on the way out then we don't care about the error.
                    if (ftpClient != null)
                    {
                        try { ftpClient.Close(); }
                        catch { }

                        ftpClient = null;
                    }

                    // Delete the temporary file if we get here. Ignore errors since it is the temporary directory
                    // and will be deleted as soon as the system is restarted. However, it is nice to clean up our
                    // own junk so that it is not left lying around.
                    System.IO.File.Delete(temporaryFileName);
                }
                catch { }
            }
        }

        /// <summary>
        /// Starting a file transfer. Log the starting sequence.
        /// </summary>
        private static void ftpClient_OnTransferStart(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            Console.WriteLine(string.Format("Starting SFTP transfer from '{0}' to '{1}'", src, dst));
            if (!string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine(message);
            }
        }

        /// <summary>
        /// Currently transferring the file contents. Log the processing sequence.
        /// </summary>
        private static void ftpClient_OnTransferProgress(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine(message);
            }
            Console.WriteLine(string.Format("Written {0:n0} bytes out of {1:n0}", transferredBytes, totalBytes));
        }

        /// <summary>
        /// End of the file transfer. Log the closing sequence.
        /// </summary>
        private static void ftpClient_OnTransferEnd(string src, string dst, int transferredBytes, int totalBytes, string message)
        {
            if (!string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine(message);
            }
            Console.WriteLine("Closing transfer mode");
        }
    }
}