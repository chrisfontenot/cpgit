﻿// To enable pause event before exit on an error condition, un-comment the following line
// #define PAUSE

using System;

namespace DebtPlus.Server.FHLB.Upload
{
    public class Program
    {
        private static void Main(string[] args)
        {
			// Allocate a class to perform the upload. All processing is in this class.
            using (Processing cls = new Processing_SFTP())
            {
                try
                {
					// Do the upload operation
                    cls.ProcessUpload();
                    System.Environment.Exit(0);
                }

                catch (System.Exception ex)
                {
					// Write the error message to STDERR rather than Console.
                    using (var outStream = Console.OpenStandardError())
                    {
                        using (var txt = new System.IO.StreamWriter(outStream))
                        {
                            txt.WriteLine(ex.ToString());
                            txt.Flush();
                        }
                    }
					
#if PAUSE
                    // Request the the user acknowledge the message status before we quit.
                    Console.WriteLine();
                    Console.Write("Press ENTER to continue: ");
                    Console.ReadLine();
#endif

                    System.Environment.Exit(1);
                }
            }
        }
    }
}