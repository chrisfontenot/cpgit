#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Module Testing
    <STAThread()> _
    Public Sub Main(ByVal Arguments() As String)
        DebtPlus.Startup.Register(Arguments)

        'Dim cls_item As New DebtPlus.Notes.NoteClass.ClientNote(New DebtPlus.Storage.SQLInfoClass)
        'cls_item.Edit(22061525)
        'cls_item.Create(11010, Nothing)
        'Dim cls_item As New DebtPlus.Notes.NoteClass.DisbursementNote(New DebtPlus.Storage.SQLInfoClass)
        'cls_item.Create(11010, 2736)
        Dim cls_item As New DebtPlus.Notes.NoteClass.DisbursementNote(New DebtPlus.Storage.SQLInfoClass)
        cls_item.Create(11010, 23)
        'Dim cls_item As New DebtPlus.Notes.NoteClass.DebtNote(New DebtPlus.Storage.SQLInfoClass)
        'cls_item.Edit(43)
    End Sub
End Module
