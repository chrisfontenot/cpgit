﻿namespace DebtPlus.UI.Housing.HPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DebtPlus.LINQ;
    using DebtPlus.UI.Housing.HPF.Data;

    partial class SubmissionCase
    {
        public DebtPlus.UI.Housing.HPF.Data.Event GenerateEvent(Housing_property propertyRecord)
        {
            var answer = new DebtPlus.UI.Housing.HPF.Data.Event();

            using (var bc = new BusinessContext())
            {
                // Find the client housing information
                var clientHousingRecord = bc.client_housings.Where(s => s.Id == propertyRecord.HousingID).FirstOrDefault();
                if (clientHousingRecord == null)
                {
                    throw new ApplicationException("clent_housing record can not be located");
                }

                // Find the client information
                var clientRecord = bc.clients.Where(s => s.Id == clientHousingRecord.Id).FirstOrDefault();
                if (clientRecord == null)
                {
                    throw new ApplicationException("client record can not be located");
                }

                // Locate the counselor for this record
                var co = DebtPlus.LINQ.Cache.counselor.getList().Find(s => string.Compare(s.Person, propertyRecord.created_by, true) == 0);
                if (co != null)
                {
                    answer.ChgLstUserId = Mapping.mapCounselor(co);
                }

                answer.Comments = string.Empty;
                answer.CompletedInd = "N";
                answer.CounselorIdRef = answer.ChgLstUserId;
                answer.EventDt = DateTime.Now;
                answer.EventId = null;
                answer.OptOutReason = null;
                answer.OptOutReasonCd = null;
                answer.ProgramRefusalDt = null;
                answer.RpcInd = "N";
                answer.ProgramStageId = DebtPlus.LINQ.Cache.HPFProgramStage.getDefault();
                answer.EventOutcomeCd = DebtPlus.LINQ.Cache.HPFEventOutcomeType.getDefault().ToString();
                answer.EventTypeCd = DebtPlus.LINQ.Cache.HPFEventType.getDefault().ToString();

                // The final answer is the session object
                return answer;
            }
        }
    }
}

