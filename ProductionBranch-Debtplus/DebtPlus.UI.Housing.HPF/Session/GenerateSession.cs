﻿namespace DebtPlus.UI.Housing.HPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DebtPlus.LINQ;
    using DebtPlus.UI.Housing.HPF.Data;

    partial class SubmissionCase
    {
        /// <summary>
        /// Generate a default session record but don't attempt to read the action plan notes. It is left as an empty string.
        /// It is normally replaced by the caller with the value from the foreclosure case record that was previously obtained.
        /// </summary>
        /// <param name="propertyRecord"></param>
        /// <returns></returns>
        public DebtPlus.UI.Housing.HPF.Data.Session GenerateSession(Housing_property propertyRecord)
        {
            using (var bc = new BusinessContext())
            {
                // Find the client housing information
                var clientHousingRecord = bc.client_housings.Where(s => s.Id == propertyRecord.HousingID).FirstOrDefault();
                if (clientHousingRecord == null)
                {
                    throw new ApplicationException("clent_housing record can not be located");
                }

                // Find the client information
                var clientRecord = bc.clients.Where(s => s.Id == clientHousingRecord.Id).FirstOrDefault();
                if (clientRecord == null)
                {
                    throw new ApplicationException("client record can not be located");
                }

                return GenerateSession(bc, clientHousingRecord, clientRecord, propertyRecord);
            }
        }

        /// <summary>
        /// Generate a default session record. Read and convert the action plan notes since they have not been read.
        /// </summary>
        /// <param name="propertyRecord"></param>
        /// <returns></returns>
        public DebtPlus.UI.Housing.HPF.Data.Session GenerateSessionWithNotes(Housing_property propertyRecord)
        {
            using (var bc = new BusinessContext())
            {
                // Find the client housing information
                var clientHousingRecord = bc.client_housings.Where(s => s.Id == propertyRecord.HousingID).FirstOrDefault();
                if (clientHousingRecord == null)
                {
                    throw new ApplicationException("clent_housing record can not be located");
                }

                // Find the client information
                var clientRecord = bc.clients.Where(s => s.Id == clientHousingRecord.Id).FirstOrDefault();
                if (clientRecord == null)
                {
                    throw new ApplicationException("client record can not be located");
                }

                var answer = GenerateSession(bc, clientHousingRecord, clientRecord, propertyRecord);

                // Read the current action plan information
                var actionPlanRecord = bc.action_plans.Where(s => s.client == clientRecord.Id).OrderByDescending(s => s.date_created).FirstOrDefault();
                if (actionPlanRecord != null)
                {
                    string action_plan_goals = bc.action_items_goals.Where(s => s.Id == actionPlanRecord.Id).Select(s => s.text).FirstOrDefault();
                    answer.ActionItemsNotes = ConvertToText(action_plan_goals);
                }

                return answer;
            }
        }

        /// <summary>
        /// Generate the default session record. It is typically used with the Fannie Mae DQ event submission.
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="clientHousingRecord"></param>
        /// <param name="clientRecord"></param>
        /// <param name="propertyRecord"></param>
        /// <returns></returns>
        private DebtPlus.UI.Housing.HPF.Data.Session GenerateSession(BusinessContext bc, client_housing clientHousingRecord, client clientRecord, Housing_property propertyRecord)
        {
            var answer = new DebtPlus.UI.Housing.HPF.Data.Session();

            answer.ProgramStageId             = DebtPlus.LINQ.Cache.HPFProgramStage.getDefault();
            answer.ActionItemsNotes           = null;
            answer.FollowupNotes              = string.Empty;
            answer.LoanDfltReasonNotes        = string.Empty;
            answer.HouseholdGrossAnnualIncome = DebtPlus.UI.Housing.HPF.Data.Convert.ToDouble((totalGross(bc, clientRecord.Id) + totalOtherIncome(bc, clientRecord.Id)) * 12M);
            answer.HouseholdNetAnnualIncome   = DebtPlus.UI.Housing.HPF.Data.Convert.ToDouble((totalNet(bc, clientRecord.Id) + totalOtherIncome(bc, clientRecord.Id)) * 12M);

            // Find the budget set
            answer.BudgetSetId                = bc.budgets.Where(s => s.client == clientRecord.Id).OrderByDescending(s => s.date_created).Select(s => s.Id).FirstOrDefault();
                
            // Locate the counselor for this record
            var co = DebtPlus.LINQ.Cache.counselor.getList().Find(s => string.Compare(s.Person, propertyRecord.created_by, true) == 0);
            if (co != null)
            {
                answer.ChgLstUserId = Mapping.mapCounselor(co);
            }

            // Find the Debt-To-Income figure
            answer.DtiRatio = clientHousingRecord.BackEnd_DTI;

            // Find the debt load calculation
            answer.DebtLoadCalculation = DebtPlus.UI.Housing.HPF.Data.Convert.ToDouble
                (
                    totalDebtBalance(bc, clientRecord.Id)
                    + totalSecured(bc, clientRecord.Id)
                    + totalOtherDebts(bc, clientRecord.Id)
                );

            // The final answer is the session object
            return answer;
        }

        /// <summary>
        /// Total gross pay amount
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalGross(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.peoples.Where(s => s.Client == ClientID).Select(s => new { net_income = s.net_income, gross_income = s.gross_income, frequency = s.Frequency }).ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }
            return colItems.Sum(x => DebtPlus.LINQ.BusinessContext.PayAmount(x.gross_income > 0M ? x.gross_income : x.net_income, x.frequency));
        }

        /// <summary>
        /// Total net pay amount
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalNet(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.peoples.Where(s => s.Client == ClientID).Select(s => new { net_income = s.net_income, gross_income = s.gross_income, frequency = s.Frequency }).ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }
            return colItems.Sum(x => DebtPlus.LINQ.BusinessContext.PayAmount(x.net_income > 0M ? x.net_income : x.gross_income, x.frequency));
        }

        /// <summary>
        /// Total other sources of income
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalOtherIncome(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.assets.Where(s => s.client == ClientID).Select(x => new { amount = x.amount, frequency = x.frequency }) .ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }

            return colItems.Sum(x => DebtPlus.LINQ.BusinessContext.PayAmount(x.amount, x.frequency));
        }

        /// <summary>
        /// Total secured (non-managed) debt amount
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalSecured(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.secured_properties.Join(bc.secured_loans, p => p.Id, l => l.secured_property, (p, l) => new { client = p.client, balance = l.balance }).Where(s => s.client == ClientID).ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }
            return colItems.Sum(s => s.balance);
        }

        /// <summary>
        /// Total other (non-managed) debt amount
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalOtherDebts(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.client_other_debts.Where(s => s.client == ClientID).ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }
            return colItems.Sum(s => s.balance);
        }

        /// <summary>
        /// Total managed debt balance
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        private decimal totalDebtBalance(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var colItems = bc.client_creditor_balances
                        .Join(bc.client_creditors, bal => bal.Id, cc => cc.client_creditor_balance, (bal, cc) => new { bal = bal, cc = cc })
                        .Where(x => x.cc.client == ClientID && !x.cc.reassigned_debt)
                        .Select(s => new { balance = s.bal.orig_balance + s.bal.orig_balance_adjustment + s.bal.total_interest - s.bal.total_payments })
                        .ToList();
            if (colItems == null || colItems.Count < 1)
            {
                return 0M;
            }

            return colItems.Sum(t => t.balance);
        }
    }
}
