﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.HPF
{
    public partial class SubmitSession : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
#if false
        public DebtPlus.Svc.Housing.HPF.Case.Session mapSession(Int32 ClientID, HPFSession sessionRecord)
        {
            using (var bc = new BusinessContext())
            {
                var answer = new DebtPlus.Svc.Housing.HPF.Case.Session()
                {
                    ActionItemsNotes           = DebtPlus.UI.Housing.HPF.SubmitCase.ReadActionPlanGoals(bc, ClientID),
                    BudgetSetId                = sessionRecord.budgetSetID,
                    DebtLoadCalculation        = sessionRecord.debtLoadCalculation,
                    DtiRatio                   = sessionRecord.dtiRatio,
                    FollowupNotes              = sessionRecord.followupNote,
                    HouseholdGrossAnnualIncome = sessionRecord.householdGrossAnnualIncome,
                    HouseholdNetAnnualIncome   = sessionRecord.householdNetAnnualIncome,
                    LoanDfltReasonNotes        = sessionRecord.loanDfltReasonNote,
                    SessionId                  = sessionRecord.sessionID
                };
                return answer;
            }
        }

        public DebtPlus.Svc.Housing.HPF.Case.Event mapEvent(HPFEvent eventRecord)
        {
            var answer = new DebtPlus.Svc.Housing.HPF.Case.Event()
            {
                ChgLstUserId     = eventRecord.chgLastUserID,
                Comments         = eventRecord.comments,
                CompletedInd     = eventRecord.completedIND,
                CounselorIdRef   = eventRecord.counselorIdREF,
                EventDt          = eventRecord.eventDT,
                EventId          = eventRecord.eventID,
                EventOutcomeCd   = eventRecord.eventOutcomeCD,
                EventTypeCd      = eventRecord.eventTypeCD,
                OptOutReasonCd   = eventRecord.optOutReasonCD,
                ProgramRefusalDt = eventRecord.programRefusalDT,
                ProgramStageId   = eventRecord.programStageID,
                RpcInd           = eventRecord.rpcIND
            };

            return answer;
        }

        public Int32? SendSession(HPFSession sessionRecord, HPFEvent eventRecord)
        {
            var req = new DebtPlus.Svc.Housing.HPF.Data.Session();
            
            // Generate the session and event information
            // var session = mapSession(ClientID, sessionRecord);
            // session.CompletionEvent = mapEvent(eventRecord);

            // Generate the request
            using (var cls = new DebtPlus.Svc.Housing.HPF.Communication())
            {
                var resp = cls.Submit(eventRecord.property, req);
                if (resp.status == Svc.Housing.HPF.Agency.ResponseStatus.Success)
                {
                    return sessionRecord.sessionID;
                }

                return null;
            }
        }
#endif
    }
}
