﻿using System;
using System.Collections.Generic;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.HPF
{
    public partial class SubmissionCase : IDisposable
    {
        /// <summary>
        /// Generate the foreclosure case set from the database.
        /// </summary>
        /// <param name="propertyRecord"></param>
        /// <returns></returns>
        public DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet GenerateForeclosureCaseSet(Housing_property propertyRecord)
        {
            // We need a property record or we would not be here.
            if (propertyRecord == null)
            {
                throw new ArgumentNullException("Missing property record");
            }

            // Allocate a submission case
            var reqOp = new DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet();
            reqOp.db.propertyRecord = propertyRecord;

            // Retrieve the client, people, KeyField, and other fields from the case.
            using (var bc = new BusinessContext())
            {
                try
                {
                    // From the property record, find the housing record
                    reqOp.db.housingRecord = bc.client_housings.Where(s => s.Id == propertyRecord.HousingID).FirstOrDefault();
                    if (reqOp.db.housingRecord == null)
                    {
                        throw new ApplicationException("Housing record could not be located.");
                    }

                    // From the housing record, find the client record
                    reqOp.db.clientRecord = bc.clients.Where(s => s.Id == reqOp.db.housingRecord.Id).FirstOrDefault();
                    if (reqOp.db.clientRecord == null)
                    {
                        throw new ApplicationException("Client record could not be located.");
                    }

                    // These fields are used in the logging operation
                    reqOp.propertyID = reqOp.db.propertyRecord.Id;
                    reqOp.ClientID = reqOp.db.clientRecord.Id;

                    // Find the people collection for this client
                    reqOp.db.colPeople = bc.peoples.Where(s => s.Client == reqOp.db.clientRecord.Id).ToList();

                    // Earners are simple the number of people defined
                    reqOp.ForeclosureCase.IncomeEarnersCd = reqOp.db.colPeople.Count().ToString();

                    // Find the correct action plan record
                    reqOp.db.actionPlanRecord = bc.action_plans.Where(s => s.client == reqOp.db.clientRecord.Id).OrderByDescending(s => s.date_created).FirstOrDefault();

                    // Add the asset income figures
                    reqOp.db.colOtherIncomes = bc.assets.Where(s => s.client == reqOp.db.clientRecord.Id).ToList();
                    reqOp.db.colDebts = bc.client_creditors.Where(s => s.client == reqOp.db.clientRecord.Id && !s.reassigned_debt).ToList();

                    // Set the client's home address as the mailing fields. Initialize the property fields to that as well.
                    reqOp.db.homeAddress = null;
                    if (reqOp.db.clientRecord.AddressID.HasValue)
                    {
                        reqOp.db.homeAddress = bc.addresses.Where(s => s.Id == reqOp.db.clientRecord.AddressID.Value).FirstOrDefault();
                    }

                    reqOp.db.propertyAddr = null;
                    if (reqOp.db.propertyRecord.PropertyAddress.HasValue && ! reqOp.db.propertyRecord.UseHomeAddress)
                    {
                        reqOp.db.propertyAddr = bc.addresses.Where(s => s.Id == reqOp.db.propertyRecord.PropertyAddress.Value).FirstOrDefault();
                    }
                    if (reqOp.db.propertyAddr == null && reqOp.db.clientRecord.AddressID.HasValue)
                    {
                        reqOp.db.propertyAddr = reqOp.db.homeAddress;
                    }

                    // Find the interview record
                    reqOp.db.hudInterviewRecord = Procedures.MapClientToInterview(
                        bc,
                        reqOp.db.propertyRecord.HousingID,
                        Procedures.getMortgageInterviewType());   // Only look for foreclosure case types!

                    if (reqOp.db.hudInterviewRecord != null)
                    {
                        // Try to find the matching appointment
                        reqOp.db.clientAppointmentRecord = (from ht in bc.hud_transactions
                                                            join ca in bc.client_appointments on ht.client_appointment equals ca.Id
                                                            where ht.client_appointment != null && ht.hud_interview == reqOp.db.hudInterviewRecord.Id
                                                            select ca).FirstOrDefault();
                    }

                    if (reqOp.db.clientAppointmentRecord == null)
                    {
                        reqOp.db.clientAppointmentRecord = (from ca in bc.client_appointments
                                                            where ca.client == reqOp.db.clientRecord.Id && new char[] { 'K', 'W' }.Contains(ca.status)
                                                            orderby ca.start_time descending
                                                            select ca).FirstOrDefault();
                    }

                    // Look for the disclosure information
                    reqOp.db.privacyDisclosure    = GetDisclosure(bc, reqOp, reqOp.db.propertyRecord.Id, client_DisclosureType.description_HPF_PRIVACY);
                    reqOp.db.mhaDisclosure        = GetMHADisclosure(bc, reqOp, reqOp.db.propertyRecord.Id, client_DisclosureType.description_MHA);
                    reqOp.db.creditAuthDisclosure = GetDisclosure(bc, reqOp, reqOp.db.propertyRecord.Id, client_DisclosureType.description_HPF_CBR);
                    reqOp.db.shareDisclosure      = GetDisclosure(bc, reqOp, reqOp.db.propertyRecord.Id, client_DisclosureType.description_HPF_SHARE);

                    if (reqOp.db.privacyDisclosure == null)
                    {
                        reqOp.db.privacyDisclosure = GetDisclosure(bc, reqOp, null, client_DisclosureType.description_HPF_PRIVACY);
                    }

                    if (reqOp.db.mhaDisclosure == null)
                    {
                        reqOp.db.mhaDisclosure = GetMHADisclosure(bc, reqOp, null, client_DisclosureType.description_MHA);
                    }

                    // Find the counselor from the property record. We look only at the property record. It is either there or it is not.
                    // If there is no counselor information then there is no counselor information.
                    reqOp.db.counselorRecord = null;
                    if (reqOp.db.propertyRecord.HPF_counselor.HasValue)
                    {
                        reqOp.db.counselorRecord = DebtPlus.LINQ.Cache.counselor.getList().Find(s => s.Id == reqOp.db.propertyRecord.HPF_counselor.Value);
                    }

                    // Get the list of loans for the property
                    reqOp.db.colLoans        = bc.Housing_loans.Where(s => s.PropertyID == reqOp.db.propertyRecord.Id).ToList();
                    reqOp.db.firstLoanRecord = reqOp.db.colLoans.Find(s => s.Loan1st2nd == 1);
                    reqOp.db.applicantRecord = reqOp.db.colPeople.Find(s => s.Relation == 1);

                    // From the KeyField record, get the borrower and co-borrower information fields
                    reqOp.db.borrowerRecord = null;
                    if (reqOp.db.propertyRecord.OwnerID != null)
                    {
                        reqOp.db.borrowerRecord = bc.Housing_borrowers.Where(s => s.Id == reqOp.db.propertyRecord.OwnerID.Value).FirstOrDefault();
                    }

                    if (reqOp.db.borrowerRecord == null)
                    {
                        reqOp.db.borrowerRecord = DebtPlus.LINQ.Factory.Manufacture_housing_borrower();
                        reqOp.db.borrowerRecord.PersonID = 1;
                    }

                    // Set the co-borrower record
                    reqOp.db.coBorrowerRecord = null;
                    if (reqOp.db.propertyRecord.CoOwnerID.HasValue)
                    {
                        reqOp.db.coBorrowerRecord = bc.Housing_borrowers.Where(s => s.Id == reqOp.db.propertyRecord.CoOwnerID.Value).FirstOrDefault();
                    }

                    if (reqOp.db.coBorrowerRecord == null)
                    {
                        reqOp.db.coBorrowerRecord = DebtPlus.LINQ.Factory.Manufacture_housing_borrower();
                        reqOp.db.coBorrowerRecord.PersonID = 2;
                    }

                    // Find the intake detail information
                    if (reqOp.db.firstLoanRecord != null)
                    {
                        // Get the current lender information.
                        if (reqOp.db.firstLoanRecord.CurrentLenderID.HasValue)
                        {
                            reqOp.db.firstLoanCurrentLender = bc.Housing_lenders.Where(s => s.Id == reqOp.db.firstLoanRecord.CurrentLenderID.Value).FirstOrDefault();
                        }

                        // If there is an original value then obtain it too
                        if (reqOp.db.firstLoanRecord.OrigLenderID.HasValue && !reqOp.db.firstLoanRecord.UseOrigLenderID)
                        {
                            reqOp.db.firstLoanOrigLender = bc.Housing_lenders.Where(s => s.Id == reqOp.db.firstLoanRecord.OrigLenderID.Value).FirstOrDefault();
                        }

                        // If there is no original lender then use the current settings. We can just assign the same structure since the data is not updated, only read.
                        if (reqOp.db.firstLoanOrigLender == null)
                        {
                            reqOp.db.firstLoanOrigLender = reqOp.db.firstLoanCurrentLender;
                        }
                    }

                    // ******* AT THIS POINT THE DATA HAS BEEN READ. NOW, CONVERT THE DATA VALUES TO A SUITABLE CASE **********
                    var otherIncomeAmount = reqOp.db.colOtherIncomes.Sum(s => DebtPlus.LINQ.BusinessContext.PayAmount(s.amount, s.frequency));

                    // Set the gross annual income figures
                    reqOp.ForeclosureCase.HouseholdGrossAnnualIncomeAmt = 12M * (reqOp.db.colPeople.Sum(s => DebtPlus.LINQ.BusinessContext.PayAmount(s.gross_income > 0M ? s.gross_income : s.net_income, s.Frequency)) + otherIncomeAmount);

                    // Finally, we can set the flags and values based upon the record sets
                    SetHousingRecordFields(bc, reqOp);
                    SetClientFields(bc, reqOp);
                    SetHousingPropertyFields(bc, reqOp);
                    SetBudgetFields(bc, reqOp);
                    SetTelephoneNumbers(bc, reqOp);
                    SetEmailAddresses(bc, reqOp);
                    SetActionPlanFields(bc, reqOp);
                    setHudInformation(bc, reqOp);
                    setDisclosures(bc, reqOp);
                    setCounselorData(bc, reqOp);
                    setIndicatorData(bc, reqOp);
                    setSufficientIncomeInd(bc, reqOp);
                    setAgencyData(bc, reqOp);

                    // Retrieve the fields from the current loan servicer and move them to the foreclosure case.
                    // These really should be on the property, not the loan if this is the case.
                    if (reqOp.db.firstLoanCurrentLender != null)
                    {
                        reqOp.ForeclosureCase.CounselorAttemptedSrvcrContactOutcomeCd = Mapping.mapCnslrContactLenderOutcome(reqOp.db.firstLoanCurrentLender.CnslrContactLenderOutcome);
                        reqOp.ForeclosureCase.CounselorAttemptedSrvcrContactReasonCd  = Mapping.mapCnslrContactLenderReason(reqOp.db.firstLoanCurrentLender.CnslrContactLenderReason);
                        reqOp.ForeclosureCase.ContactedSrvcrRecentlyOutcomeCd         = Mapping.mapClientConcatLenderOutcome(reqOp.db.firstLoanCurrentLender.ClientConcatLenderOutcome);
                    }

                    if (reqOp.db.privacyDisclosure != null)
                    {
                        reqOp.ForeclosureCase.MotherMaidenLname = reqOp.db.privacyDisclosure.Disclosure.applicant_value;
                    }

                    // If there is a disclosure than retrieve the pull indicator and date from the disclosure record.
                    if (reqOp.db.creditAuthDisclosure != null)
                    {
                        reqOp.ForeclosureCase.CreditPullIndDt      = reqOp.db.creditAuthDisclosure.Disclosure.date_created;
                        reqOp.ForeclosureCase.IntakeCreditScore    = reqOp.db.applicantRecord.FICO_Score.GetValueOrDefault(0).ToString();
                        reqOp.ForeclosureCase.CreditPullInd        = Mapping.mapCreditPullInd(reqOp.db.creditAuthDisclosure.Disclosure.applicant);
                        reqOp.ForeclosureCase.IntakeCreditBureauCd = Mapping.mapCreditBureau(reqOp.db.applicantRecord.CreditAgency);
                    }

                    // ******* AT THIS POINT THE DATA HAS BEEN CONVERTED. DO PEEP-HOLE VALIDATION OF THE FIELDS. **********

                    // All of these pieces are required. If any is missing, reset them all.
                    if (reqOp.ForeclosureCase.CreditPullInd        == "N"  ||
                        reqOp.ForeclosureCase.CreditPullInd        == "?"  ||
                        reqOp.ForeclosureCase.CreditPullIndDt      == null ||
                        reqOp.ForeclosureCase.IntakeCreditBureauCd == null ||
                        reqOp.ForeclosureCase.IntakeCreditScore    == null)
                    {
                        reqOp.ForeclosureCase.CreditPullIndDt      = null;
                        reqOp.ForeclosureCase.CreditPullInd        = "N";
                        reqOp.ForeclosureCase.IntakeCreditBureauCd = null;
                        reqOp.ForeclosureCase.IntakeCreditScore    = null;
                    }

                    // Look for bankruptcy status
                    if (reqOp.db.applicantRecord.bkfileddate.HasValue)
                    {
                        reqOp.ForeclosureCase.BankruptcyInd           = "Y";
                        reqOp.ForeclosureCase.BankruptcyLoaDt         = reqOp.db.applicantRecord.bkAuthLetterDate;
                        reqOp.ForeclosureCase.BankruptcyPmtCurrentInd = Mapping.mapYN(reqOp.db.applicantRecord.bkPaymentCurrent.GetValueOrDefault(1) != 0);
                        reqOp.ForeclosureCase.BankruptcyAttorney      = "UNKNOWN";
                    }

                    // Finally, at the very end define some of the fields that vary based upon the entire record
                    reqOp.ForeclosureCase.LEPStatusCd = string.Compare(reqOp.ForeclosureCase.BorrowerPreferredLangCd, "ENG", true) == 0 ? "B" : "A";

                    // Find the loan default reason notes
                    reqOp.ForeclosureCase.LoanDfltReasonNotes = bc.Housing_PropertyNotes.Where(s => s.PropertyID == reqOp.db.propertyRecord.Id).OrderByDescending(s => s.date_created).Select(s => s.NoteBuffer).FirstOrDefault();

                    // Set the outcome information
                    reqOp.Outcome = bc.HPFOutcomes.Where(s => s.propertyID == reqOp.db.propertyRecord.Id).Select(s => GenerateOutcome(bc, s)).ToList();

                    // If the outcome code is missing, make it "114".
                    if (string.IsNullOrEmpty(reqOp.ForeclosureCase.HudOutcomeCd))
                    {
                        reqOp.ForeclosureCase.HudOutcomeCd = "114";
                    }

                    return reqOp;
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error accessing database for HPF submission data");
                    return null;
                }
            }
        }

        /// <summary>
        /// Determine if the client has sufficient income for the loans
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        private void setSufficientIncomeInd(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            decimal totalNetIncome     = reqOp.db.colPeople.Sum(s => s.net_income > 0M ? DebtPlus.LINQ.BusinessContext.PayAmount(s.net_income, s.Frequency) : DebtPlus.LINQ.BusinessContext.PayAmount(s.gross_income, s.Frequency));
            decimal totalOtherIncome   = reqOp.db.colOtherIncomes.Sum(s => DebtPlus.LINQ.BusinessContext.PayAmount(s.amount, s.frequency));
            decimal totalBudgetExpense = reqOp.db.colBudgetDetails.Sum(s => s.suggested_amount);
            decimal totalDmpExpense    = reqOp.db.colDebts.Sum(s => s.disbursement_factor);

            // Mark all of the loans with this status
            string SufficientField     = Mapping.mapYN((totalNetIncome + totalOtherIncome) > (totalBudgetExpense + totalDmpExpense));
            reqOp.CaseLoans.ForEach(s => s.SufficientIncomeInd = SufficientField);
        }

        /// <summary>
        /// Set the fields from the housing record
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="housingRecord"></param>
        private void SetHousingRecordFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            reqOp.ForeclosureCase.CertificateId = reqOp.db.housingRecord.HECM_Certificate_ID;
            reqOp.ForeclosureCase.MortgagePmtRatio = Mapping.mapPercent(reqOp.db.housingRecord.FrontEnd_DTI);
            if (reqOp.ForeclosureCase.MortgagePmtRatio < 1.0)
            {
                reqOp.ForeclosureCase.MortgagePmtRatio = null;
            }
        }

        /// <summary>
        /// Set the fields associated with the client record
        /// </summary>
        /// <param name="reqOp">Pointer to the current request record</param>
        /// <param name="clientRecord">Pointer to the client record</param>
        private void SetClientFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Set the language and marital status information from the client record
            reqOp.ForeclosureCase.BorrowerPreferredLangCd = Mapping.mapLanguage(reqOp.db.clientRecord.language);
            reqOp.ForeclosureCase.BorrowerMaritalStatusCd = Mapping.mapMaritalStatus(reqOp.db.clientRecord.marital_status);
            reqOp.ForeclosureCase.CounseledLanguageCd     = Mapping.mapLanguage(reqOp.db.clientRecord.language);
            reqOp.ForeclosureCase.AgencyClientNum         = reqOp.db.clientRecord.Id.ToString("00000000");
            reqOp.ForeclosureCase.CaseSourceCd            = Mapping.mapReferredBy(reqOp.db.clientRecord.referred_by);
            reqOp.ForeclosureCase.DependentNum            = reqOp.db.clientRecord.dependents;
            reqOp.ForeclosureCase.DfltReason1stCd         = Mapping.mapFinancialProblem(reqOp.db.clientRecord.cause_fin_problem1);
            reqOp.ForeclosureCase.DfltReason2ndCd         = Mapping.mapFinancialProblem(reqOp.db.clientRecord.cause_fin_problem2);
            reqOp.ForeclosureCase.HouseholdCd             = Mapping.mapHousehold(reqOp.db.clientRecord.household);
            reqOp.ForeclosureCase.DmpDt                   = reqOp.db.clientRecord.dmp_status_date;

            // If there is a home address then set the values into the record as the contact information
            if (reqOp.db.homeAddress != null)
            {
                reqOp.ForeclosureCase.ContactAddr1    = reqOp.db.homeAddress.AddressLine1;
                reqOp.ForeclosureCase.ContactAddr2    = reqOp.db.homeAddress.address_line_2;
                reqOp.ForeclosureCase.ContactCity     = reqOp.db.homeAddress.city;
                reqOp.ForeclosureCase.ContactStateCd  = Mapping.mapState(reqOp.db.homeAddress.state);
                reqOp.ForeclosureCase.ContactZip      = Mapping.mapZip(reqOp.db.homeAddress.PostalCode);
                reqOp.ForeclosureCase.ContactZipPlus4 = Mapping.mapZipPlus4(reqOp.db.homeAddress.PostalCode);
            }
        }

        /// <summary>
        /// Set the values from the housing_property record
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="propertyRecord"></param>
        private void SetHousingPropertyFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Set the various fields from the property record
            reqOp.ForeclosureCase.FcNoticeReceiveInd         = Mapping.mapYN(reqOp.db.propertyRecord.FcNoticeReceivedIND);
            reqOp.ForeclosureCase.ForSaleInd                 = Mapping.mapYN(reqOp.db.propertyRecord.ForSaleIND);
            reqOp.ForeclosureCase.WorkedWithAnotherAgencyInd = Mapping.mapYN(reqOp.db.propertyRecord.HPF_worked_with_other_agency);
            reqOp.ForeclosureCase.ReasonForCallCd            = Mapping.mapReasonForCall(reqOp.db.propertyRecord.HPF_ReasonForCallCD);
            reqOp.ForeclosureCase.FcSaleDate                 = reqOp.db.propertyRecord.FcSaleDT;
            reqOp.ForeclosureCase.HomePurchasePrice          = reqOp.db.propertyRecord.PurchasePrice;
            reqOp.ForeclosureCase.HomePurchaseYear           = reqOp.db.propertyRecord.PurchaseYear;
            reqOp.ForeclosureCase.IntakeDt                   = reqOp.db.propertyRecord.date_created;
            reqOp.ForeclosureCase.FcId                       = reqOp.db.propertyRecord.HPF_foreclosureCaseID;

            // By definition, the home is "single family". We don't support multiple families sharing an address.
            reqOp.ForeclosureCase.SingleFamilyHomeInd        = Mapping.mapYN(true);

            // The estimated market value is land plus improvements (house)
            reqOp.ForeclosureCase.PropertyCd                 = Mapping.mapProperty(reqOp.db.propertyRecord.PropertyType);
            reqOp.ForeclosureCase.ForSaleInd                 = Mapping.mapYN(reqOp.db.propertyRecord.ForSaleIND);
            reqOp.ForeclosureCase.PrimResEstMktValue         = reqOp.db.propertyRecord.LandValue + reqOp.db.propertyRecord.ImprovementsValue;
            reqOp.ForeclosureCase.HomeCurrentMarketValue     = reqOp.db.propertyRecord.LandValue + reqOp.db.propertyRecord.ImprovementsValue;
            reqOp.ForeclosureCase.HomeSalePrice              = reqOp.db.propertyRecord.SalePrice;
            reqOp.ForeclosureCase.OccupantNum                = reqOp.db.propertyRecord.Occupancy;

            SetBorrowerFields(bc, reqOp.db.colPeople, reqOp, reqOp.db.borrowerRecord);
            SetCoBorrowerFields(bc, reqOp.db.colPeople, reqOp, reqOp.db.coBorrowerRecord);

            if (reqOp.db.propertyAddr != null)
            {
                reqOp.ForeclosureCase.PropStateCd            = Mapping.mapState(reqOp.db.propertyAddr.state);
                reqOp.ForeclosureCase.PropZip                = Mapping.mapZip(reqOp.db.propertyAddr.PostalCode);
                reqOp.ForeclosureCase.PropZipPlus4           = Mapping.mapZipPlus4(reqOp.db.propertyAddr.PostalCode);
                reqOp.ForeclosureCase.RuralAreaStatusCd      = Mapping.mapUSDAStatus(reqOp.db.propertyAddr.USDA_Status);
                reqOp.ForeclosureCase.PropAddr1              = reqOp.db.propertyAddr.AddressLine1;
                reqOp.ForeclosureCase.PropAddr2              = reqOp.db.propertyAddr.address_line_2;
                reqOp.ForeclosureCase.PropCity               = reqOp.db.propertyAddr.city;
            }

            // Set the housing loan information
            SetHousingLoanFields(bc, reqOp);

            // Set the residency information for the current property
            if (reqOp.db.propertyRecord.Residency > 0)
            {
                reqOp.db.residency = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList().Find(s => s.Id == reqOp.db.propertyRecord.Residency);
                if (reqOp.db.residency != null)
                {
                    reqOp.ForeclosureCase.PrimaryResidenceInd = Mapping.mapYN(reqOp.db.residency.primaryResidence);
                    reqOp.ForeclosureCase.VacantOrCondemedInd = Mapping.mapYN(reqOp.db.residency.vacantOrCondemned);
                    reqOp.ForeclosureCase.OwnerOccupiedInd    = Mapping.mapYN(reqOp.db.residency.ownerOccupied);
                }
            }

            // Set the Program, Sponsor, and Campaign fields
            if (reqOp.db.propertyRecord.HPF_Program != null)
            {
                reqOp.db.hpfProgram = DebtPlus.LINQ.Cache.HPFProgram.getList().Find(s => s.Id == reqOp.db.propertyRecord.HPF_Program);
                if (reqOp.db.hpfProgram != null)
                {
                    reqOp.ForeclosureCase.ProgramId  = reqOp.db.hpfProgram.programID;
                    reqOp.ForeclosureCase.SponsorId  = reqOp.db.hpfProgram.sponsorID;
                    reqOp.ForeclosureCase.CampaignId = reqOp.db.hpfProgram.campaignID;
                }
            }

            // A program is required. Use the default value of 101 if there is no program.
            if (reqOp.ForeclosureCase.ProgramId == null)
            {
                reqOp.ForeclosureCase.ProgramId = 101;
            }

            // Do the subprogram field for the entry.
            reqOp.ForeclosureCase.SubprogramCd = null;
            if (reqOp.db.propertyRecord.HPF_SubProgram != null)
            {
                reqOp.db.hpfSubProgram = bc.HPFSubPrograms.Where(s => s.Id == reqOp.db.propertyRecord.HPF_SubProgram.Value).FirstOrDefault();
                if (reqOp.db.hpfSubProgram != null)
                {
                    // Set the code string from the table if there is one
                    if (!string.IsNullOrWhiteSpace(reqOp.db.hpfSubProgram.SubProgramCode))
                    {
                        reqOp.ForeclosureCase.SubprogramCd = reqOp.db.hpfSubProgram.SubProgramCode;
                    }

                    // Set the value from the table if there is one
                    if (reqOp.db.hpfSubProgram.SubProgramId.HasValue)
                    {
                        reqOp.ForeclosureCase.SubprogramId = reqOp.db.hpfSubProgram.SubProgramId.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Set the budget information fields
        /// </summary>
        /// <param name="reqOp"></param>
        /// <param name="colPeople"></param>
        private void SetBudgetFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Generate the base list for the client net income figures
            var colBudgetCategory = reqOp.db.colPeople.Select(s => new my_budgetCategory(s.Relation == 1 ? 1 : 2, s.net_income, s.gross_income, s.Frequency)).ToList();

            // Include the other sources of income
            colBudgetCategory.AddRange(reqOp.db.colOtherIncomes.Select(s => new my_budgetCategory(Mapping.mapAssetIds(s.asset_id).GetValueOrDefault(), s.amount, s.amount, s.frequency)));

            // Add the debt disbursement figures
            colBudgetCategory.AddRange(reqOp.db.colDebts.Select(s => new my_budgetCategory(51, s.non_dmp_payment, s.disbursement_factor, 4)));

            // Find the latest budget category. Use NULL if there is no budget
            var q = bc.budgets.Where(s => s.client == reqOp.db.clientRecord.Id).OrderByDescending(s => s.date_created).Select(s => new { s.Id }).FirstOrDefault();
            reqOp.db.budgetID = q == null ? new Int32?() : new Int32?(q.Id);

            // Read the budget information if there is a budget
            if (reqOp.db.budgetID != null)
            {
                reqOp.db.colBudgetDetails = bc.budget_details.Where(s => s.budget == reqOp.db.budgetID.Value).ToList();
                colBudgetCategory.AddRange(reqOp.db.colBudgetDetails.Select(s => new my_budgetCategory(Mapping.mapBudgetCategory(s.budget_category).GetValueOrDefault(), s.client_amount, s.suggested_amount, 4)));
            }

            // Summarize the client items down to the corresponding values for the transfer
            reqOp.BudgetItems.AddRange(colBudgetCategory
                                       .Where(s => s.category > 0)
                                       .OrderBy(s => s.category)
                                       .GroupBy(grp => new { grp.category })
                                       .Select(s => new DebtPlus.UI.Housing.HPF.Data.BudgetItem() { BudgetSubcategoryId = s.Key.category, BudgetItemAmt = s.Sum(i => i.client_amount), BudgetNote = string.Empty }));

            // Summarize the suggested items down to the corresponding values for the transfer
            reqOp.ProposedBudgetItems.AddRange(colBudgetCategory
                                       .Where(s => s.category > 0)
                                       .OrderBy(s => s.category)
                                       .GroupBy(grp => new { grp.category })
                                       .Select(s => new DebtPlus.UI.Housing.HPF.Data.BudgetItem() { BudgetSubcategoryId = s.Key.category, BudgetItemAmt = s.Sum(i => i.suggested_amount), BudgetNote = string.Empty }));
        }

        /// <summary>
        /// Set the telephone numbers into the request case
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="clientRecord"></param>
        /// <param name="colPeople"></param>
        private void SetTelephoneNumbers(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // List of possible telephone numbers for the client. We take the first two items.
            var colContactNumbers = new List<my_contactNumber>();
            colContactNumbers.Add(new my_contactNumber(bc, "HOME", 1, reqOp.db.clientRecord.HomeTelephoneID));
            colContactNumbers.AddRange(reqOp.db.colPeople.Select(s => new my_contactNumber(bc, "CELL", 2, s.CellTelephoneID)).ToList());
            colContactNumbers.Add(new my_contactNumber(bc, "OTHER", 4, reqOp.db.clientRecord.MsgTelephoneID));

            // From the list of possible values, find the primary entry
            var colOrderedList = colContactNumbers.OrderBy(s => s.Level).ToList();
            var contactPhone = colOrderedList.FirstOrDefault();
            if (contactPhone != null)
            {
                reqOp.ForeclosureCase.PrimaryContactNo       = contactPhone.TelephoneNumber;
                reqOp.ForeclosureCase.PrimaryContactNoTypeCd = contactPhone.TelephoneType;
                colOrderedList.Remove(contactPhone);
            }

            // From the list of possible values, find the primary entry
            contactPhone = colOrderedList.FirstOrDefault();
            if (contactPhone != null)
            {
                reqOp.ForeclosureCase.SecondContactNo       = contactPhone.TelephoneNumber;
                reqOp.ForeclosureCase.SecondContactNoTypeCd = contactPhone.TelephoneType;
            }
        }

        /// <summary>
        /// Set the Email address fields
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="clientRecord"></param>
        private void SetEmailAddresses(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Find the valid email addresses for the people in the system. We ignore the ones that are refused.
            var Addresses = bc.peoples
                            .Join(bc.EmailAddresses, p => p.EmailID.Value, e => e.Id, (p, e) => new { clientID = p.Client, e = e })
                            .Where(x => x.clientID == reqOp.db.clientRecord.Id && new Int32[] { 1, 2 }.Contains(x.e.Validation))
                            .Select(s => s.e.Address).ToList();

            if (Addresses.Count > 0)
            {
                reqOp.ForeclosureCase.Email1 = Addresses[0];
                if (Addresses.Count > 1)
                {
                    reqOp.ForeclosureCase.Email2 = Addresses[1];
                }
            }
        }

        /// <summary>
        /// Set the action plan information
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="actionPlanRecord"></param>
        private void SetActionPlanFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            if (reqOp.db.actionPlanRecord != null)
            {
                string action_plan_goals               = bc.action_items_goals.Where(s => s.Id == reqOp.db.actionPlanRecord.Id).Select(s => s.text).FirstOrDefault();
                reqOp.ForeclosureCase.ActionItemsNotes = ConvertToText(action_plan_goals);
            }
        }

        /// <summary>
        /// Set the information for HUD's appointment status
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="clientRecord"></param>
        /// <param name="propertyRecord"></param>
        private void setHudInformation(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            if (reqOp.db.hudInterviewRecord != null)
            {
                reqOp.ForeclosureCase.HudOutcomeCd           = Mapping.mapHudOutcome(reqOp.db.hudInterviewRecord.hud_result);
                reqOp.ForeclosureCase.HudTerminationDt       = reqOp.db.hudInterviewRecord.termination_date;
                reqOp.ForeclosureCase.HudTerminationReasonCd = Mapping.mapHudTerminationReason(reqOp.db.hudInterviewRecord.termination_reason);
            }

            // From the appointment determine the duration
            if (reqOp.db.clientAppointmentRecord != null && reqOp.db.clientAppointmentRecord.end_time != null)
            {
                reqOp.ForeclosureCase.CounselingDurationCd = Mapping.mapCounselingDuration((int)((reqOp.db.clientAppointmentRecord.end_time.Value - reqOp.db.clientAppointmentRecord.start_time).TotalMinutes));
            }
        }

        /// <summary>
        /// Update the loan information with the disclosure data
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="clientRecord"></param>
        /// <param name="propertyRecord"></param>
        private void setDisclosures(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Get the mother's maiden name from the applicant field of the disclosure record. We don't care about the co-applicant information.
            if (reqOp.db.privacyDisclosure != null)
            {
                reqOp.ForeclosureCase.MotherMaidenLname = reqOp.db.privacyDisclosure.Disclosure.applicant_value;
            }

            if (reqOp.db.mhaDisclosure != null)
            {
                reqOp.CaseLoans.ForEach(s =>
                {
                    s.ThirtyDaysLatePastYrInd                = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.answer_03_04);
                    s.LongTermAffordInd                      = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.answer_03_05);
                    s.PriorHampInd                           = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.answer_02_03);
                    s.PrinBalWithinLimitInd                  = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.answer_01_07);
                    s.OrigPriorTo2009Ind                     = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.answer_01_02);
                    s.HarpEligibleInd                        = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.eligible_HARP);
                    s.HampEligibleInd                        = Mapping.mapYN(reqOp.db.mhaDisclosure.MHADisclosure.eligible_FHA_HAMP | reqOp.db.mhaDisclosure.MHADisclosure.eligible_HAMP_tier_1 | reqOp.db.mhaDisclosure.MHADisclosure.eligible_HAMP_tier_2);
                });
            }

            // Determine the status of the consent flags
            switch (GetServicerConsent(bc, reqOp))
            {
                case 2:     // YES to all (also credit pull = multiple)
                    reqOp.ForeclosureCase.ServicerConsentInd = "Y";
                    reqOp.ForeclosureCase.FundingConsentInd  = "Y";
                    break;

                case 6:     // YES to funding only
                    reqOp.ForeclosureCase.ServicerConsentInd = "N";
                    reqOp.ForeclosureCase.FundingConsentInd  = "Y";
                    break;

                case -1:     // No servicer consent disclosure
                    reqOp.ForeclosureCase.ServicerConsentInd = "?";
                    reqOp.ForeclosureCase.FundingConsentInd  = "?";
                    break;

                case 0:     // must choose (not really stored)
                case 1:     // NO to both
                case 3:     // not applicable
                case 5:     // single credit pull only
                case 4:     // not available
                default:    // Some other strange value
                    reqOp.ForeclosureCase.ServicerConsentInd = "N";
                    reqOp.ForeclosureCase.FundingConsentInd  = "N";
                    break;
            }

            // Look at the credit report authorization to determine its status
            if (reqOp.db.creditAuthDisclosure == null)
            {
                reqOp.db.creditAuthDisclosure                = GetDisclosure(bc, reqOp, reqOp.db.propertyRecord.Id, client_DisclosureType.description_HPF_CBR);
            }
        }

        /// <summary>
        /// Set the information about the counselor
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="propertyRecord"></param>
        /// <param name="clientRecord"></param>
        private void setCounselorData(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            if (reqOp.db.counselorRecord == null)
            {
                return;
            }

            // Insert the assigned and last changed counselor information. For now, they are the same information.
            reqOp.ForeclosureCase.AssignedCounselorIdRef = Mapping.mapCounselor(reqOp.db.counselorRecord);
            reqOp.ForeclosureCase.ChgLstUserId = reqOp.ForeclosureCase.AssignedCounselorIdRef;

            if (reqOp.db.counselorRecord.Name != null)
            {
                reqOp.ForeclosureCase.CounselorFname = reqOp.db.counselorRecord.Name.First;
                reqOp.ForeclosureCase.CounselorLname = reqOp.db.counselorRecord.Name.Last;
            }

            if (reqOp.db.counselorRecord.EmailID.HasValue)
            {
                var email = bc.EmailAddresses.Where(s => s.Id == reqOp.db.counselorRecord.EmailID.Value).FirstOrDefault();
                if (email != null)
                {
                    reqOp.ForeclosureCase.CounselorEmail = email.Address;
                }
            }

            // This is just a canned email address in case the counselor does not have one.
            if (string.IsNullOrWhiteSpace(reqOp.ForeclosureCase.CounselorEmail))
            {
                reqOp.ForeclosureCase.CounselorEmail = "info@clearpointccs.org";
            }
        }

        /// <summary>
        /// Determine the settings from some indicator flags.
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="clientRecord"></param>
        private void setIndicatorData(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Read the list of indicators for this client
            var colIndicators = bc.client_indicators.Where(s => s.client == reqOp.db.clientRecord.Id && (new Int32[] { 147, 239 }).Contains(s.indicator)).ToList();

            // Look for indicator #147
            reqOp.ForeclosureCase.AgencyMediaInterestInd = Mapping.mapYN(colIndicators.Exists(s => s.indicator == 147));

            // Look for indicator #239
            reqOp.ForeclosureCase.AgencySuccessStoryInd = Mapping.mapYN(colIndicators.Exists(s => s.indicator == 239));
        }

        /// <summary>
        /// Set the agency information for the request
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="propertyRecord"></param>
        private void setAgencyData(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Set the agency case number
            reqOp.ForeclosureCase.AgencyCaseNum = reqOp.db.propertyRecord.Id.ToString();
            reqOp.ForeclosureCase.AgencyId      = 2821;          // Agency ID assigned by HPF. Unlike to change.

            // The counselor information is static
            reqOp.ForeclosureCase.CounselorPhone = "8777568831";
            reqOp.ForeclosureCase.CounselorExt   = null;

            // If there is a bankruptcy status then we don't know the attorney
            if (reqOp.ForeclosureCase.BankruptcyInd == "Y")
            {
                reqOp.ForeclosureCase.BankruptcyAttorney = "UNKNOWN";
            }
        }

        /// <summary>
        /// Translate a debtplus housing_loan record to a CaseLoan class.
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="loanRecord"></param>
        /// <returns></returns>
        private DebtPlus.UI.Housing.HPF.Data.CaseLoan GenerateCaseLoan(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, Housing_loan loanRecord)
        {
            Housing_lender currentLender      = null;
            Housing_lender origLender         = null;
            Housing_loan_detail currentDetail = null;
            Housing_loan_detail intakeDetail  = null;

            // Generate the loan case information
            var answer = new DebtPlus.UI.Housing.HPF.Data.CaseLoan();

            // Take the fields from the loan record itself.
            answer.CurrentLoanBalanceAmt = loanRecord.CurrentLoanBalanceAmt;
            answer.Loan1st2nd            = (loanRecord.Loan1st2nd.Value < 2) ? "1ST" : "2ND";
            answer.LoanDelinqStatusCd    = Mapping.mapLoanDelinquencyMonths(loanRecord.LoanDelinquencyMonths);
            answer.OrigLoanAmt           = loanRecord.OrigLoanAmt;

            // Lender information
            if (loanRecord.CurrentLenderID.HasValue)
            {
                currentLender = bc.Housing_lenders.Where(s => s.Id == loanRecord.CurrentLenderID.Value).FirstOrDefault();
            }

            // If there is an original value then obtain it too
            if (loanRecord.OrigLenderID.HasValue && !loanRecord.UseOrigLenderID)
            {
                origLender = bc.Housing_lenders.Where(s => s.Id == loanRecord.OrigLenderID.Value).FirstOrDefault();
            }

            // If there is no original lender then use the current settings. We can just assign the same structure since the data is not updated, only read.
            if (origLender == null)
            {
                origLender = currentLender;
            }

            // Take the information form the current lender record
            if (currentLender != null)
            {
                DateTime? dt                             = currentLender.ContactLenderDate;
                answer.ContactedSrvcrRecentlyInd         = (dt.HasValue && dt.Value.AddDays(30).Date > DateTime.Now.Date);

                // The servicer workout plan comes from the lender. We assume that it is current for the time being.
                answer.SrvcrWorkoutPlanCurrentInd        = answer.HasWorkoutPlanInd;
                answer.CounselorContactedSrvcrInd        = currentLender.ContactLenderSuccess.GetValueOrDefault();
                answer.CounselorAttemptedSrvcrContactInd = currentLender.AttemptContactDate.HasValue;
                answer.DiscussedSolutionWithSrvcrInd     = currentLender.AttemptContactDate.HasValue;
                answer.CurrentServicerFdicNcuaNum        = currentLender.FdicNcusNum;
                answer.ServicerId                        = Mapping.mapServicerId(currentLender);
                answer.OtherServicerName                 = Mapping.mapServicerName(currentLender);
                answer.AcctNum                           = currentLender.AcctNum;
                answer.LoanLookupCd                      = Mapping.mapLoanLookupCd(currentLender.InvestorID);

                // If there is no servicer then we need to use the one for "other".
                if (!currentLender.ServicerID.HasValue)
                {
                    var q = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList().Find(s => s.IsOther);
                    if (q != null)
                    {
                        answer.ServicerId = q.ServiceID;
                    }
                }
            }

            // Take the information form the current lender record
            if (origLender != null)
            {
                answer.OriginatingLenderName = Mapping.mapServicerName(origLender);
            }

            // Find the detail fields
            if (loanRecord.IntakeDetailID.HasValue)
            {
                intakeDetail = bc.Housing_loan_details.Where(s => s.Id == loanRecord.IntakeDetailID.Value).FirstOrDefault();
            }

            if (intakeDetail == null)
            {
                intakeDetail = DebtPlus.LINQ.Factory.Manufacture_housing_loan_detail();
            }

            if (!loanRecord.IntakeSameAsCurrent && loanRecord.CurrentDetailID.HasValue)
            {
                currentDetail = bc.Housing_loan_details.Where(s => s.Id == loanRecord.CurrentDetailID).FirstOrDefault();
            }

            if (currentDetail == null)
            {
                currentDetail = intakeDetail;
            }

            // Take the detail information from the current detail record
            if (currentDetail != null)
            {
                answer.ArmResetInd       = Mapping.mapYN(currentDetail.ARM_Reset);
                answer.MortgageTypeCd    = Mapping.mapMortgageType(currentDetail);
                answer.MortgageProgramCd = Mapping.mapMorgageProgram(currentDetail);
                answer.TermLengthCd      = Mapping.mapTermLengthCd(currentDetail);
            }

            // Take the interest rate from the intake record
            if (intakeDetail != null)
            {
                answer.InterestRate = Mapping.mapPercent(intakeDetail.InterestRate);
            }

            // The resulting CaseLoan is returned.
            return answer;
        }

        /// <summary>
        /// Map the current DebtPlus outcome record to the values needed by HPF
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="outcomeRecord"></param>
        /// <returns></returns>
        private DebtPlus.UI.Housing.HPF.Data.OutcomeItem GenerateOutcome(BusinessContext bc, HPFOutcome outcomeRecord)
        {
            var answer                     = new DebtPlus.UI.Housing.HPF.Data.OutcomeItem();
            answer.ExtRefOtherName         = outcomeRecord.extRefOtherName;
            answer.NonprofitreferralKeyNum = outcomeRecord.NonProfitReferralName;

            // Translate the Outcome Type field
            var q = DebtPlus.LINQ.Cache.HPFOutcomeType.getList().Find(s => s.Id == outcomeRecord.outcomeTypeID);
            if (q != null && !string.IsNullOrEmpty(q.hpf))
            {
                answer.OutcomeTypeId = int.Parse(q.hpf);
            }
            return answer;
        }

        /// <summary>
        /// Get the Servicer/Funding Consent indicator
        /// </summary>
        /// <param name="clientRecord">client Record to be transmitted</param>
        /// <param name="KeyField">Housing KeyField to be transmitted</param>
        private int GetServicerConsent(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            if (reqOp.db.shareDisclosure != null)
            {
                return reqOp.db.shareDisclosure.Disclosure.applicant;
            }
            return -1;
        }

        /// <summary>
        /// Get the status for the MHA disclosure for a client
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="propertyID"></param>
        /// <param name="DisclosureType"></param>
        /// <returns></returns>
        private DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure GetMHADisclosure(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, Int32? propertyID, string DisclosureType)
        {
            if (propertyID.HasValue)
            {
                return (from d in bc.client_disclosures
                        join l in bc.client_DisclosureLanguageTypes on d.disclosureID equals l.Id
                        join t in bc.client_DisclosureTypes on l.clientDisclosureTypeID equals t.Id
                        join m in bc.client_disclosures_MHAs on d.Id equals m.client_disclosure_ID
                        orderby d.date_created descending
                        where d.client == reqOp.db.clientRecord.Id
                        && d.propertyID == propertyID.Value
                        && string.Compare(t.description, DisclosureType, true) == 0
                        select new DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure() { Disclosure = d, DisclosureType = t, MHADisclosure = m }).FirstOrDefault();
            }

            return (from d in bc.client_disclosures
                    join l in bc.client_DisclosureLanguageTypes on d.disclosureID equals l.Id
                    join t in bc.client_DisclosureTypes on l.clientDisclosureTypeID equals t.Id
                    join m in bc.client_disclosures_MHAs on d.Id equals m.client_disclosure_ID
                    orderby d.date_created descending
                    where d.client == reqOp.db.clientRecord.Id
                    && string.Compare(t.description, DisclosureType, true) == 0
                    select new DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure() { Disclosure = d, DisclosureType = t, MHADisclosure = m }).FirstOrDefault();
        }

        /// <summary>
        /// Get the status of a disclosure for a client
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="propertyID"></param>
        /// <param name="DisclosureType"></param>
        /// <returns></returns>
        private DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure GetDisclosure(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, Int32? propertyID, string DisclosureType)
        {
            if (propertyID.HasValue)
            {
                return (from d in bc.client_disclosures
                        join l in bc.client_DisclosureLanguageTypes on d.disclosureID equals l.Id
                        join t in bc.client_DisclosureTypes on l.clientDisclosureTypeID equals t.Id
                        orderby d.date_created descending
                        where d.client == reqOp.db.clientRecord.Id
                        && d.propertyID == propertyID.Value
                        && string.Compare(t.description, DisclosureType, true) == 0
                        select new DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure() { Disclosure = d, DisclosureType = t, MHADisclosure = null }).FirstOrDefault();
            }

            return (from d in bc.client_disclosures
                    join l in bc.client_DisclosureLanguageTypes on d.disclosureID equals l.Id
                    join t in bc.client_DisclosureTypes on l.clientDisclosureTypeID equals t.Id
                    orderby d.date_created descending
                    where d.client == reqOp.db.clientRecord.Id
                    && string.Compare(t.description, DisclosureType, true) == 0
                    select new DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet.my_disclosure() { Disclosure = d, DisclosureType = t, MHADisclosure = null }).FirstOrDefault();
        }

        /// <summary>
        /// Set the fields associated with the applicant record
        /// </summary>
        /// <param name="reqOp">Pointer to the current request record</param>
        /// <param name="peopleRecord">Pointer to the people record</param>
        private void SetApplicantFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, DebtPlus.Interfaces.Client.IPeople peopleRecord)
        {
            // If the record is missing then the fields are also missing
            if (peopleRecord == null)
            {
                return;
            }

            // If there is a name then include the person's name
            if (peopleRecord.NameID.HasValue)
            {
                var n = bc.Names.Where(s => s.Id == peopleRecord.NameID.Value).FirstOrDefault();
                if (n != null)
                {
                    reqOp.ForeclosureCase.BorrowerFname = n.First;
                    reqOp.ForeclosureCase.BorrowerLname = n.Last;
                    reqOp.ForeclosureCase.BorrowerMname = n.Middle;
                }
            }

            // Set the various fields for the person as a borrower
            reqOp.ForeclosureCase.BorrowerDob                  = peopleRecord.Birthdate;
            reqOp.ForeclosureCase.BorrowerOccupation           = Mapping.mapOccupation(peopleRecord.job, peopleRecord.other_job);
            reqOp.ForeclosureCase.BorrowerSsn                  = peopleRecord.SSN;
            reqOp.ForeclosureCase.BorrowerDisabledInd          = Mapping.mapDisabled(peopleRecord.Disabled);

            // Fields that should be per-person but are not. Take only the applicant values.
            reqOp.ForeclosureCase.HispanicInd                  = Mapping.mapEthnicity(peopleRecord.Ethnicity);
            reqOp.ForeclosureCase.GenderCd                     = Mapping.mapGender(peopleRecord.Gender);
            reqOp.ForeclosureCase.RaceCd                       = Mapping.mapRace(peopleRecord.Race);
            reqOp.ForeclosureCase.BorrowerEducLevelCompletedCd = Mapping.mapEducation(peopleRecord.Education);

            // Military service fields

            // Never in military and not dependant of military person
            if (peopleRecord.MilitaryStatusID == 0 && peopleRecord.MilitaryDependentID == 0)
            {
                reqOp.ForeclosureCase.MilServiceMemInd    = "N";
                reqOp.ForeclosureCase.MilServiceMemDepInd = "N";
                reqOp.ForeclosureCase.MilServiceRankCd    = null;
                reqOp.ForeclosureCase.MilitaryServiceCd   = null;
                reqOp.ForeclosureCase.MilServiceBranchCd  = null;
                reqOp.ForeclosureCase.MilServiceDepCd     = null;
                reqOp.ForeclosureCase.MilPCSInd           = null;
            }

            else
            {
                if (peopleRecord.MilitaryStatusID == 6 || peopleRecord.MilitaryDependentID > 0)
                {
                    // Dependant of military person
                    reqOp.ForeclosureCase.MilServiceMemInd    = "N";
                    reqOp.ForeclosureCase.MilServiceMemDepInd = "Y";
                    reqOp.ForeclosureCase.MilServiceDepCd     = Mapping.mapMilitaryDependent(peopleRecord.MilitaryDependentID);
                    reqOp.ForeclosureCase.MilPCSInd           = "N";
                }
                else
                {
                    // In military or vet
                    reqOp.ForeclosureCase.MilServiceMemInd    = "Y";
                    reqOp.ForeclosureCase.MilServiceMemDepInd = "N";
                    reqOp.ForeclosureCase.MilServiceDepCd     = null;
                    reqOp.ForeclosureCase.MilPCSInd           = Mapping.mapYN(peopleRecord.MilitaryStatusID == 3);
                }

                reqOp.ForeclosureCase.MilitaryServiceCd  = Mapping.mapMilitaryStatus(peopleRecord.MilitaryStatusID);
                reqOp.ForeclosureCase.MilServiceBranchCd = Mapping.mapMilitaryService(peopleRecord.MilitaryServiceID);
                reqOp.ForeclosureCase.MilServiceRankCd   = Mapping.mapMilitaryGrade(peopleRecord.MilitaryGradeID);
            }
        }

        /// <summary>
        /// Set the fields associated with the co-applicant record
        /// </summary>
        /// <param name="reqOp">Pointer to the current request record</param>
        /// <param name="peopleRecord">Pointer to the people record</param>
        private void SetCoApplicantFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, DebtPlus.Interfaces.Client.IPeople peopleRecord)
        {
            // If the record is missing then the fields are also missing
            if (peopleRecord == null)
            {
                return;
            }

            // If there is a name then include the person's name
            if (peopleRecord.NameID.HasValue)
            {
                var n = bc.Names.Where(s => s.Id == peopleRecord.NameID.Value).FirstOrDefault();
                if (n != null)
                {
                    reqOp.ForeclosureCase.CoBorrowerFname = n.First;
                    reqOp.ForeclosureCase.CoBorrowerLname = n.Last;
                    reqOp.ForeclosureCase.CoBorrowerMname = n.Middle;
                }
            }

            // Set the various fields for the person as a borrower
            reqOp.ForeclosureCase.CoBorrowerDob         = peopleRecord.Birthdate;
            reqOp.ForeclosureCase.CoBorrowerOccupation  = Mapping.mapOccupation(peopleRecord.job, peopleRecord.other_job);
            reqOp.ForeclosureCase.CoBorrowerSsn         = peopleRecord.SSN;
            reqOp.ForeclosureCase.CoBorrowerDisabledInd = Mapping.mapDisabled(peopleRecord.Disabled);
        }

        /// <summary>
        /// Set the borrower information into the request case
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="borrowerReocrd"></param>
        private void SetBorrowerFields(BusinessContext bc, List<people> colPeople, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, Housing_borrower borrowerReocrd)
        {
            if (borrowerReocrd == null)
            {
                return;
            }

            // If the fields are specified in the record then use them now.
            if (borrowerReocrd.PersonID == 0)
            {
                SetApplicantFields(bc, reqOp, borrowerReocrd);

                // Replace the client fields with the information from the borrower data
                reqOp.ForeclosureCase.CounseledLanguageCd     = Mapping.mapLanguage(borrowerReocrd.Language);
                reqOp.ForeclosureCase.BorrowerPreferredLangCd = Mapping.mapLanguage(borrowerReocrd.Language);
                reqOp.ForeclosureCase.BorrowerMaritalStatusCd = Mapping.mapMaritalStatus(borrowerReocrd.marital_status);
                return;
            }

            // If the fields say "applicant" then use the people record to find the applicant.
            if (borrowerReocrd.PersonID == 2)
            {
                var qCoApplicant = colPeople.Find(s => s.Relation != 1);
                if (qCoApplicant != null)
                {
                    SetApplicantFields(bc, reqOp, qCoApplicant);
                    return;
                }
            }

            // If the fields say "applicant" then use the people record to find the applicant.
            var qApplicant = colPeople.Find(s => s.Relation == 1);
            if (qApplicant != null)
            {
                SetApplicantFields(bc, reqOp, qApplicant);
                return;
            }
        }

        /// <summary>
        /// Set the borrower information into the request case
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="borrowerReocrd"></param>
        private void SetCoBorrowerFields(BusinessContext bc, List<people> colPeople, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, Housing_borrower borrowerReocrd)
        {
            if (borrowerReocrd == null)
            {
                return;
            }

            // If the fields are specified in the record then use them now.
            if (borrowerReocrd.PersonID == 0)
            {
                SetCoApplicantFields(bc, reqOp, borrowerReocrd);
                return;
            }

            // If the fields say "applicant" then use the people record to find the applicant.
            if (borrowerReocrd.PersonID == 2)
            {
                var qCoApplicant = colPeople.Find(s => s.Relation != 1);
                if (qCoApplicant != null)
                {
                    SetCoApplicantFields(bc, reqOp, qCoApplicant);
                    return;
                }
            }

            // If the fields say "applicant" then use the people record to find the applicant.
            var qApplicant = colPeople.Find(s => s.Relation == 1);
            if (qApplicant != null)
            {
                SetCoApplicantFields(bc, reqOp, qApplicant);
                return;
            }
        }

        /// <summary>
        /// Update the foreclose case with the information from the case loan data
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="caseLoanRecord"></param>
        private void SetFirstMortgageLoanFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp, DebtPlus.UI.Housing.HPF.Data.CaseLoan caseLoanRecord)
        {
            // These fields are added to each case loan so that we can process all of the items together. We take only the one
            // from the first loan position and update the foreclosure case accordingly. THe others are simply ignored.
            reqOp.ForeclosureCase.HasWorkoutPlanInd                 = Mapping.mapYN(caseLoanRecord.HasWorkoutPlanInd);
            reqOp.ForeclosureCase.SrvcrWorkoutPlanCurrentInd        = Mapping.mapYN(caseLoanRecord.SrvcrWorkoutPlanCurrentInd);
            reqOp.ForeclosureCase.ContactedSrvcrRecentlyInd         = Mapping.mapYN(caseLoanRecord.ContactedSrvcrRecentlyInd);
            reqOp.ForeclosureCase.CounselorContactedSrvcrInd        = Mapping.mapYN(caseLoanRecord.CounselorContactedSrvcrInd);
            reqOp.ForeclosureCase.CounselorAttemptedSrvcrContactInd = Mapping.mapYN(caseLoanRecord.CounselorAttemptedSrvcrContactInd);
            reqOp.ForeclosureCase.DiscussedSolutionWithSrvcrInd     = Mapping.mapYN(caseLoanRecord.DiscussedSolutionWithSrvcrInd);
        }

        /// <summary>
        /// Set the housing loan information fields
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="reqOp"></param>
        /// <param name="propertyRecord"></param>
        private void SetHousingLoanFields(BusinessContext bc, DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            // Generate the list of case loans
            reqOp.CaseLoans = reqOp.db.colLoans.Select(s => GenerateCaseLoan(bc, reqOp, s)).ToList();
            var firstLoan = reqOp.CaseLoans.Find(s => s.Loan1st2nd == "1ST");
            if (firstLoan != null)
            {
                SetFirstMortgageLoanFields(bc, reqOp, firstLoan);
            }

            // Set the DSA detail information
            if (reqOp.db.firstLoanRecord != null && reqOp.db.firstLoanRecord.DsaDetailID.HasValue)
            {
                reqOp.db.firstLoanDSADetail = bc.Housing_Loan_DSADetails.Where(s => s.Id == reqOp.db.firstLoanRecord.DsaDetailID.Value).FirstOrDefault();
                if (reqOp.db.firstLoanDSADetail != null)
                {
                    reqOp.ForeclosureCase.DocExclusionCd = Mapping.mapDocExclusionCd(reqOp.db.firstLoanDSADetail.NoDsaReason);
                    reqOp.ForeclosureCase.DocIndDt       = reqOp.db.propertyRecord.last_change_date.HasValue ? new System.Nullable<DateTime>(reqOp.db.propertyRecord.last_change_date.Value.Date) : null;
                    reqOp.ForeclosureCase.DocInd         = Mapping.mapYN(reqOp.db.firstLoanDSADetail.ProgramBenefit.GetValueOrDefault(-1) == 0 /* Yes */);
                }
            }

            // Now, if the value is correct for "YES" then clear the reason for the exclusion. It is not sent.
            if (reqOp.ForeclosureCase.DocInd == "Y" && reqOp.ForeclosureCase.DocIndDt.HasValue)
            {
                reqOp.ForeclosureCase.DocExclusionCd = null;
            }
            else
            {
                // We don't have a DocInd. Make the value "N" and clear the date entry.
                reqOp.ForeclosureCase.DocInd   = "N";
                reqOp.ForeclosureCase.DocIndDt = null;

                // Default the reason why we don't have a DocExclusion code
                if (string.IsNullOrWhiteSpace(reqOp.ForeclosureCase.DocExclusionCd))
                {
                    reqOp.ForeclosureCase.DocExclusionCd = "NOTINTERESTED";
                }
            }
        }

        /// <summary>
        /// Set the fields associated with the client record
        /// </summary>
        /// <param name="reqOp">Pointer to the current request record</param>
        /// <param name="clientRecord">Pointer to the client record</param>
        private string ConvertToText(string InputString)
        {
            if (string.IsNullOrEmpty(InputString))
            {
                return null;
            }

            // If the text note is RTF then convert it to normal text
            if (DebtPlus.Utils.Format.Strings.IsRTF(InputString))
            {
                try
                {
                    using (var ctl = new DevExpress.XtraRichEdit.RichEditControl())
                    {
                        ctl.RtfText = InputString;
                        return ctl.Text;
                    }
                }
                catch { }
            }

            // If the text note is HTML then convert it to normal text
            if (DebtPlus.Utils.Format.Strings.IsHTML(InputString))
            {
                try
                {
                    using (var ctl = new DevExpress.XtraRichEdit.RichEditControl())
                    {
                        ctl.HtmlText = InputString;
                        return ctl.Text;
                    }
                }
                catch { }
            }

            // The string is just text. Return it as-is
            return InputString;
        }

        /// <summary>
        /// Dispose of local storage
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Local class to help with summarizing the budget information
        /// </summary>
        private class my_budgetCategory
        {
            public my_budgetCategory()
            {
                category = 0; client_amount = 0M; suggested_amount = 0M;
            }

            public my_budgetCategory(Int32 category, decimal net_amount, decimal gross_amount, Int32 frequency)
                : this()
            {
                this.category         = category;
                this.suggested_amount = this.client_amount;

                if (category == 1 || category == 2)
                {
                    this.client_amount    = DebtPlus.LINQ.BusinessContext.PayAmount(net_amount > 0M ? net_amount : gross_amount, frequency);
                    this.suggested_amount = this.client_amount;
                }
                else
                {
                    this.client_amount    = DebtPlus.LINQ.BusinessContext.PayAmount(net_amount, frequency);
                    this.suggested_amount = DebtPlus.LINQ.BusinessContext.PayAmount(gross_amount, frequency);
                }
            }

            public Int32 category { get; set; }
            public decimal client_amount { get; set; }
            public decimal suggested_amount { get; set; }
        }

        /// <summary>
        /// Information about the telephone number contact
        /// </summary>
        private class my_contactNumber
        {
            public my_contactNumber(BusinessContext bc, string TelephoneType, Int32 Level, Int32? TelephoneID)
            {
                this.TelephoneType = TelephoneType;
                if (TelephoneID.HasValue)
                {
                    var q = bc.TelephoneNumbers.Where(s => s.Id == TelephoneID.Value).FirstOrDefault();
                    if (q != null)
                    {
                        TelephoneNumber = DebtPlus.Utils.Format.Strings.DigitsOnly(string.Format("{0}{1}", q.Acode ?? string.Empty, q.Number ?? string.Empty));
                        this.Level      = (q.Acode ?? string.Empty) != "000" && TelephoneNumber.Length == 10 ? Level : 10;
                        return;
                    }
                }

                TelephoneNumber = null;
                this.Level = Level;
            }

            public Int32 Level { get; set; }
            public string TelephoneNumber { get; set; }
            public string TelephoneType { get; set; }
        }
    }
}
