﻿namespace DebtPlus.UI.Housing.HPF
{
    using DebtPlus.SOAP.HPF.Agency;
    using System;

    /// <summary>
    /// Validation rule class
    /// </summary>
    public class ValidationRule
    {
        /// <summary>
        /// Relationships are listed here
        /// </summary>
        public enum RelationTypes
        {
            @equals,
            @notequals
        };

        /// <summary>
        /// Name of the field in the KeyField object
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Error code number in the errors table
        /// </summary>
        public string ErrorCodePrefix { get; set; }

        /// <summary>
        /// Error code number in the errors table
        /// </summary>
        public string ErrorCodeId { get; set; }

        /// <summary>
        /// KeyField for the comparison
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Type of relation that is needed for a valid rule.
        /// </summary>
        public RelationTypes Relation { get; set; }

        /// <summary>
        /// Initialize the new class object
        /// </summary>
        public ValidationRule()
        {
        }

        /// <summary>
        /// Initialize the new class object
        /// </summary>
        public ValidationRule(string FieldName, string ErrorCodePrefix, string ErrorCodeId, string Value, RelationTypes Relation)
            : this()
        {
            this.FieldName = FieldName;
            this.ErrorCodePrefix = ErrorCodePrefix;
            this.ErrorCodeId = ErrorCodeId;
            this.Value = Value;
            this.Relation = Relation;
        }

        /// <summary>
        /// Get the value from the KeyField class for the indicated field name.
        /// </summary>
        public bool IsValid(object InputClass)
        {
            return IsValid(InputClass, FieldName);
        }

        /// <summary>
        /// Look for a valid match to the indicated field in the structure.
        /// </summary>
        /// <param name="InputClass"></param>
        /// <param name="SearchName"></param>
        /// <returns></returns>
        public bool IsValid(object InputClass, string SearchName)
        {
            // If the KeyField object is null then the value is simply null
            if (InputClass == null || string.IsNullOrEmpty(SearchName))
            {
                return compareRelation(null);
            }

            // Determine if the field name is like "groupName(indexName=indexValue).remainder"
            var m = System.Text.RegularExpressions.Regex.Match(SearchName, @"^(?<groupName>[_a-z][_a-z0-9]*)\((?<indexName>[_a-z][_a-z0-9]*)\=(?<indexValue>[^)]+)\)\.(?<remainder>.*)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (m.Success)
            {
                return compareIndexedField(InputClass, m);
            }

            // Determine if the field name is like "groupName.remainder"
            m = System.Text.RegularExpressions.Regex.Match(SearchName, @"^(?<groupName>[_a-z][_a-z0-9]*)\.(?<remainder>.*)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (m.Success)
            {
                return compareNonIndexedField(InputClass, m);
            }

            // Otherwise, the entry is just "field-name". Process the structure reference.
            try
            {
                Type t = InputClass.GetType();
                object itemValue = t.InvokeMember(SearchName, System.Reflection.BindingFlags.GetProperty, null, InputClass, null);
                return compareRelation(itemValue);
            }
            catch (System.Exception ex)
            {
                // If the system wants to show the traps then do so now.
                var swConfig = new System.Diagnostics.BooleanSwitch("HousingHPFTrap", "Include message boxes in traps");
                Boolean trConfig = swConfig.Enabled;
                if (trConfig) { DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception in validation", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error); }

                return false;
            }
        }

        private bool compareIndexedField(object InputClass, System.Text.RegularExpressions.Match m)
        {
            string groupName  = m.Groups["groupName"].Value.ToString().Trim();
            string indexName  = m.Groups["indexName"].Value.ToString().Trim();
            string indexValue = m.Groups["indexValue"].Value.ToString().Trim();
            string remainder  = m.Groups["remainder"].Value.ToString().Trim();

            // Find the collection to be scanned.
            Type t = InputClass.GetType();
            InputClass = t.InvokeMember(groupName, System.Reflection.BindingFlags.GetProperty, null, InputClass, null);

            // If the input class is not a collection then we can't process it here. Fail.
            if (! (InputClass is System.Collections.IEnumerable))
            {
                return false;
            }

            System.Collections.IEnumerator ieList = (InputClass as System.Collections.IEnumerable).GetEnumerator();
            if (!ieList.MoveNext())
            {
                return compareRelation(null);
            }

            // Find the record with the appropriate field name having the fieldvalue
            do
            {
                object objField = ieList.Current;
                System.Type fieldType = objField.GetType();

                // Retrieve the value of the index field.
                object currentValue = fieldType.InvokeMember(indexName, System.Reflection.BindingFlags.GetProperty, null, objField, null);

                // If the value of the index field matches then we have found the one item. Compare it normally.
                if (isEqual(currentValue, indexValue))
                {
                    return IsValid(objField, remainder);
                }
            } while (ieList.MoveNext());

            // We have gone through all entries in the list and there is no match. This is a failure.
            return false;
        }

        private bool compareNonIndexedField(object InputClass, System.Text.RegularExpressions.Match m)
        {
            string groupName = m.Groups["groupName"].Value.ToString().Trim();
            string remainder = m.Groups["remainder"].Value.ToString().Trim();

            // Find the collection to be scanned.
            Type t = InputClass.GetType();
            InputClass = t.InvokeMember(groupName, System.Reflection.BindingFlags.GetProperty, null, InputClass, null);

            // If the input class is not a collection then we can't process it here. Fail.
            if (!(InputClass is System.Collections.IEnumerable))
            {
                return IsValid(InputClass, remainder);
            }

            System.Collections.IEnumerator ieList = (InputClass as System.Collections.IEnumerable).GetEnumerator();
            if (! ieList.MoveNext())
            {
                return compareRelation(null);
            }

            // Find the record with the appropriate field name having the fieldvalue
            do
            {
                object objField = ieList.Current;
                if (!IsValid(objField, remainder))
                {
                    return false;
                }

            } while (ieList.MoveNext());

            // All entries match successfully.
            return true;
        }

        /// <summary>
        /// Compare the itemValue to the desired value with the desired relation and return the test status
        /// </summary>
        /// <param name="itemValue">The value for the comparison</param>
        /// <returns>TRUE if the rule is valid. FALSE otherwise.</returns>
        private bool compareRelation(object itemValue)
        {
            return compareRelation(itemValue, Relation);
        }

        /// <summary>
        /// Compare the itemValue to the desired value with the desired relation and return the test status
        /// </summary>
        /// <param name="itemValue">The value for the comparison</param>
        /// <param name="relationship">Relationship to be used</param>
        /// <returns>TRUE if the rule is valid. FALSE otherwise.</returns>
        private bool compareRelation(object itemValue, RelationTypes relationship)
        {
            // If both fields are null then they are the same
            if (itemValue == null && Value == null)
            {
                return relationship == RelationTypes.equals;
            }

            // If either field is null then they are different
            if (itemValue == null || Value == null)
            {
                return relationship != RelationTypes.equals;
            }

            // Process the relation values to determine the validity of the object.
            switch (relationship)
            {
                case RelationTypes.equals:
                    return isEqual(itemValue, Value);
                case RelationTypes.notequals:
                    return !compareRelation(itemValue, RelationTypes.equals);
                default:
                    return false;
            }
        }

        /// <summary>
        /// Compare the object value with the entry to determine equality only.
        /// </summary>
        /// <returns>TRUE if the values are equal. FALSE otherwise.</returns>
        private bool isEqual(object a, string b)
        {
            // Both are not null. Do a string comparison.
            return string.Compare(getStr(a), b, true) == 0;
        }

        /// <summary>
        /// Translate the KeyField object to a string. We do special formatting on specific types.
        /// </summary>
        private string getStr(object a)
        {
            // A null value is an empty string
            if (a == null)
            {
                return string.Empty;
            }

            // If the KeyField is a string then just return it
            if (a is string)
            {
                return a as string;
            }

            // Dates need to be formatted properly. We don't use the time component.
            if (a is DateTime)
            {
                return string.Format("{0:yyyyMMdd}", a);
            }

            // Numbers are simply formatted as a fixed point value
            if (a is Int32 || a is Int64 || a is byte || a is Int16)
            {
                return string.Format("{0:f0}", (Int64)(Int32)a);
            }

            // Money fields are formatted with two decimal places only.
            if (a is decimal)
            {
                return string.Format("{0:f2}", a);
            }

            // Everything else is simply mapped to a string.
            return a.ToString();
        }
    }

    /// <summary>
    /// Process the HPF Case submission logic
    /// </summary>
    public partial class SubmissionCase
    {
        /// <summary>
        /// Validate the request structure for the foreclosure case to ensure that all of the items are proper
        /// </summary>
        /// <param name="reqOp">The foreclosure case request</param>
        /// <returns>A submission reply object as if the request was submitted.</returns>
        public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveResponse Validate(DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet reqOp)
        {
            var reply = new DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveResponse() { Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success };
            var colMessages = new System.Collections.Generic.List<DebtPlus.SOAP.HPF.Agency.ExceptionMessage>();

            // File name to the set of rules as an XML document. If there is no name then there are no validations.
            string fname = Properties.Settings.Default.DebtPlus_UI_Housing_HPF_ValidationFile;
            if (string.IsNullOrEmpty(fname))
            {
                return reply;
            }

            // If the path is local then make it local to the executing assembly
            if (! System.IO.Path.IsPathRooted(fname))
            {
                string pathName = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                fname = System.IO.Path.GetFullPath(System.IO.Path.Combine(pathName, fname));
            }

            // Ensure that the file exists. Complain if it does not.
            if (!System.IO.File.Exists(fname))
            {
                colMessages.Add(new DebtPlus.SOAP.HPF.Agency.ExceptionMessage() { ErrorCode = "SYS0001", Message = string.Format("Validation file at '{0}' was not found.", fname) });
                reply.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail;
                reply.Messages = colMessages.ToArray();
                return reply;
            }

            System.Collections.Generic.List<ValidationRule> colTests;
            try
            {
                using (System.IO.FileStream fs = System.IO.File.OpenRead(fname))
                {
                    // De-serialize the XML document into our list.
                    System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<ValidationRule>), string.Empty);
                    colTests = (System.Collections.Generic.List<ValidationRule>)ser.Deserialize(fs);
                }
            }
            catch (System.Exception ex)
            {
                // If the system wants to show the traps then do so now.
                var swConfig = new System.Diagnostics.BooleanSwitch("HousingHPFTrap", "Include message boxes in traps");
                Boolean trConfig = swConfig.Enabled;
                if (trConfig) { DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception in validation", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error); }

                // Something happened in reading the list of rules. This is always a fatal "rule violation".
                colMessages.Add (new DebtPlus.SOAP.HPF.Agency.ExceptionMessage() { ErrorCode = "SYS0000", Message = "Validation File Failure:\r\n\r\n" + ex.Message } );
                reply.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail;
                reply.Messages = colMessages.ToArray();
                return reply;
            }

            // Allocate a new reply
            foreach (ValidationRule rule in colTests)
            {
                try
                {
                    // If the rule is not valid then use the error to find the description in the database table.
                    if (!rule.IsValid(reqOp))
                    {
                        colMessages.Add(new DebtPlus.SOAP.HPF.Agency.ExceptionMessage()
                                    {
                                        ErrorCode = rule.ErrorCodePrefix + rule.ErrorCodeId,
                                        Message   = getMessageText(rule.ErrorCodePrefix, rule.ErrorCodeId)
                                    });
                    }
                }

                catch (Exception ex)
                {
                    var swConfig = new System.Diagnostics.BooleanSwitch("HousingHPFTrap", "Include message boxes in traps");
                    Boolean trConfig = swConfig.Enabled;
                    if (trConfig) { DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception in validation", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error); }

                    // Catch any exception that happens as a rule violation. There should not be any if the program and rule list are correct.
                    colMessages.Add(new DebtPlus.SOAP.HPF.Agency.ExceptionMessage()
                            {
                                ErrorCode = "SYS0000",
                                Message = ex.Message
                            });
                }
            }

            // If there are pending messages then generate the array of items.
            reply.Messages = colMessages.ToArray();
            if (colMessages.Count == 0)
            {
                // The response was valid. There are no errors.
                reply.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success;
                return reply;
            }

            // Look for failed conditions
            foreach (var msg in reply.Messages)
            {
                if (! msg.ErrorCode.StartsWith("WARN"))
                {
                    reply.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail;
                    return reply;
                }
            }

            reply.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Warning;
            return reply;
        }

        /// <summary>
        /// Retrieve the error message text from the database tables.
        /// </summary>
        /// <param name="errorCodePrefix">Prefix value. Normal value is ERR</param>
        /// <param name="errorCodeId">Error code id</param>
        /// <returns></returns>
        private string getMessageText(string errorCodePrefix, string errorCodeId)
        {
            // Look in the database table for the error code.
            var q = DebtPlus.LINQ.Cache.HPFErrorCode.getList().Find(s => s.Prefix == errorCodePrefix && s.ErrorID == errorCodeId);
            if (q != null)
            {
                return string.Format("{0}{1}--{2}", errorCodePrefix, errorCodeId, q.Description);
            }

            // Return the standard error message
            return string.Format("{0}{1}--Text is not defined in the error codes table", errorCodePrefix, errorCodeId);
        }
    }
}
