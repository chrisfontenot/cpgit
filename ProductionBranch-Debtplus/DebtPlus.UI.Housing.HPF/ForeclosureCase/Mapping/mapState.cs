﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the State field
        /// </summary>
        public static string mapState(Int32? stateCode)
        {
            if (stateCode == null)
            {
                return null;
            }

            var q = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == stateCode.Value);

            // If the entry is found in the US then return the corresponding state mailing code. This is the two letter abbreviation of the state.
            if (q != null && q.Country == 1 && !string.IsNullOrEmpty(q.MailingCode) && q.MailingCode.Length == 2)
            {
                return q.MailingCode;
            }

            // Return null on invalid entries
            return null;
        }
    }
}
