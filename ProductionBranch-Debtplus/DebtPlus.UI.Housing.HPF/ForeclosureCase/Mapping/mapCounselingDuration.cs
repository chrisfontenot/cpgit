﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the appointment duration field
        /// </summary>
        public static string mapCounselingDuration(Int32? keyField)
        {
            if (keyField == null)
            {
                return null;
            }

            // Handle the conditions based upon the number of minutes
            if (keyField.Value < 30)
            {
                return "<30";
            }

            if (keyField.Value < 60)
            {
                return "30-59";
            }

            if (keyField.Value < 90)
            {
                return "60-89";
            }

            if (keyField.Value < 121)
            {
                return "90-120";
            }

            return ">121";
        }
    }
}
