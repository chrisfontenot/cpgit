﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Housing_CounselorContactReason field
        /// </summary>
        public static string mapCnslrContactLenderReason(Int32? KeyField)
        {
            // If there is no key then return null
            if (KeyField == null)
            {
                return null;
            }

            // Find the row. If it exists then return the code value for it.
            var q = DebtPlus.LINQ.Cache.Housing_CounselorContactReasonType.getList().Find(s => s.Id == KeyField.Value);
            if (q != null && q.hpf != null)
            {
                return q.hpf;
            }

            // If the reason can't be found, use null
            return null;
        }
    }
}
