﻿namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapUSDAStatus(char KeyField)
        {
            // Rural area.
            if (KeyField == 'Y')
            {
                return "A";
            }

            // Not in rural area
            if (KeyField == 'N')
            {
                return "B";
            }

            // These can be "?" or "U". Both are unknown at this time.
            return "C";
        }
    }
}
