﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the HudOutcome field
        /// </summary>
        public static string mapHudOutcome(Int32? KeyField)
        {
            if (KeyField != null)
            {
                var q = DebtPlus.LINQ.Cache.Housing_ResultType.getList().Find(s => s.Id == KeyField.Value);
                if (q != null && !string.IsNullOrWhiteSpace(q.hpf))
                {
                    return q.hpf;
                }
            }

            // The default for a missing item is "114". This is "continuing counseling".
            return "114";
        }
    }
}
