﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapOptOutReasonCD(Int32? KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }
            return KeyField.Value.ToString();
        }

        /// <summary>
        /// Map the Event Type field
        /// </summary>
        public static string mapOptOutReason(Int32? KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            return null;
        }
    }
}
