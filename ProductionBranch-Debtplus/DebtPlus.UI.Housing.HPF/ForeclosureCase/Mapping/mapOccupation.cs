﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapOccupation(Int32? jobID, string KeyField)
        {
            if (jobID.HasValue && jobID.Value > 0)
            {
                var q = DebtPlus.LINQ.Cache.job_description.getList().Find(s => s.Id == jobID);
                if (q != null)
                {
                    return mapOccupation(q.description);
                }
            }

            return mapOccupation(KeyField);
        }

        public static string mapOccupation(string KeyField)
        {
            // For now, there is no mapping used. We use whatever DebtPlus said was the occupation.
            return KeyField;
        }
    }
}
