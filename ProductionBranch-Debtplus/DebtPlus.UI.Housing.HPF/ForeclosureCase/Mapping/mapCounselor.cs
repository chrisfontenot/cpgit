﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the counselor ID reference
        /// </summary>
        public static string mapCounselor(Int32? counselorID)
        {
            if (counselorID.HasValue)
            {
                return mapCounselor(DebtPlus.LINQ.Cache.counselor.getList().Find(s => s.Id == counselorID.Value));
            }
            return null;
        }

        /// <summary>
        /// Map the counselor ID reference
        /// </summary>
        public static string mapCounselor(DebtPlus.LINQ.counselor counselorRecord)
        {
            // If there is no record then there is no name.
            if (counselorRecord == null)
            {
                return null;
            }

            // Use just the counselor ID. We change the names all the time and the value can't change from one instance to another.
            return counselorRecord.Id.ToString();
        }
    }
}
