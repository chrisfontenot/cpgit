﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Language field
        /// </summary>
        public static string mapLanguage(System.Int32? KeyField)
        {
            if (KeyField != null)
            {
                var q = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(s => s.Id == KeyField.Value);
                if (q != null && !string.IsNullOrEmpty(q.hpf))
                {
                    return q.hpf;
                }
            }
            return null;
        }
    }
}
