﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the income disabled status
        /// </summary>
        public static string mapCreditPullInd(Int32? KeyField)
        {
            if (KeyField.HasValue)
            {
                switch (KeyField.Value)
                {
                    case 5:             // Single pull only
                        return "S";

                    case 2:             // yes (multiple pull)
                        return "M";

                    case 1:             // no
                    case 3:             // not applicable
                    case 4:             // unavailable
                    case 6:             // yes funding only
                    default:
                        break;
                }
                return "N";
            }

            return null;
        }
    }
}
