﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the HudTerminationReason field
        /// </summary>
        public static string mapHudTerminationReason(Int32? KeyField)
        {
            if (KeyField != null)
            {
                var q = DebtPlus.LINQ.Cache.Housing_TerminationReasonType.getList().Find(s => s.Id == KeyField.Value);
                if (q != null && !string.IsNullOrWhiteSpace(q.hpf))
                {
                    return q.hpf;
                }
            }

            return null;
        }
    }
}
