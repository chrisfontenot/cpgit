﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapServicerName(DebtPlus.LINQ.Housing_lender KeyField)
        {
            if (! string.IsNullOrEmpty(KeyField.ServicerName))
            {
                return KeyField.ServicerName;
            }

            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                var qLender = bc.Housing_lender_servicers.Where(s => s.Id == KeyField.ServicerID).FirstOrDefault();
                if (qLender == null)
                {
                    return null;
                }
                return qLender.description;
            }
        }

        public static Int32? mapServicerId(DebtPlus.LINQ.Housing_lender housing_lenderRecord)
        {
            if (housing_lenderRecord == null || housing_lenderRecord.ServicerID == null)
            {
                return null;
            }

            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                var qLender = bc.Housing_lender_servicers.Where(s => s.Id == housing_lenderRecord.ServicerID.Value).FirstOrDefault();
                if (qLender != null)
                {
                    return qLender.ServiceID;
                }
            }
            return null;
        }
    }
}
