﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the budget category item identifiers
        /// </summary>
        public static Int32? mapBudgetCategory(Int32? keyField)
        {
            // Take the simple route if there is no value to map
            if (!keyField.HasValue || keyField.Value == 0)
            {
                return null;
            }

            // Find the entry in the budget categories table. If found, return the result if there is one.
            var q = DebtPlus.LINQ.Cache.budget_category.getList().Find(s => s.Id == keyField.Value);
            if (q != null && ! String.IsNullOrEmpty(q.hpf))
            {
                return int.Parse(q.hpf);
            }

            // The budget category is an optional additional item. Return the code for it.
            return 49;        // Monthly expenses : Other
        }
    }
}
