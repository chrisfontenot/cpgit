﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapMorgageProgram(DebtPlus.LINQ.Housing_loan_detail currentDetail)
        {
            if (currentDetail.FinanceTypeCD == null)
            {
                return null;
            }

            if (currentDetail.Privately_Held_Loan)
            {
                return "PRIV";
            }

            switch (currentDetail.FinanceTypeCD.Value)
            {
                case 1:
                    return "CONV";  // CONVENTIONAL
                case 2:
                    return "FHA";   // FHA
                case 3:
                    return "UNK";   // N/A
                case 4:
                    return "USDA";  // USDA
                case 5:
                    return "VA";    // VA
                default:
                    return "UNK";
            }
        }
    }
}
