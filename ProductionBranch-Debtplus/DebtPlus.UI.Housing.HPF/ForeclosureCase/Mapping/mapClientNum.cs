﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the client ID identifier
        /// </summary>
        public static string mapClientNum(Int32? keyField)
        {
            if (keyField == null)
            {
                return null;
            }

            return keyField.Value.ToString();
        }
    }
}
