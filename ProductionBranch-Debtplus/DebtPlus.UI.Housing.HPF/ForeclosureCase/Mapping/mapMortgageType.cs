﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapMortgageType(DebtPlus.LINQ.Housing_loan_detail currentDetail)
        {
            if (currentDetail == null)
            {
                return null;
            }

            if (currentDetail.Interest_Only_Loan)
            {
                return "INTONLY";
            }

            if (currentDetail.Hybrid_ARM_Loan)
            {
                return "HYBARM";
            }

            // If there is no type specified then return "UNKNOWN"
            if (!currentDetail.MortgageTypeCD.HasValue)
            {
                return "UNK";
            }

            // Find the type in the list and return the appropriate code value
            var q = DebtPlus.LINQ.Cache.Housing_MortgageType.getList().Find(s => s.Id == currentDetail.MortgageTypeCD);
            if (q != null)
            {
                string description = " " + q.description.ToLower() + " ";

                // Look at the description of the type to determine the appropriate value
                if (description.Contains(" hybrid arm ") || description.Contains(" hybrid-arm ") || description.Contains(" hybridarm "))
                {
                    return "HYBARM";
                }

                if (description.Contains(" interest only ") || description.Contains(" interest-only "))
                {
                    return "INTONLY";
                }

                if (description.Contains(" payment option ") || description.Contains(" payment-option "))
                {
                    return "POA";
                }

                if (description.Contains(" arm "))
                {
                    return "ARM";
                }

                if (description.Contains(" fixed "))
                {
                    return "FIXED";
                }

                return "UNK";
            }

            return null;
        }
    }
}
