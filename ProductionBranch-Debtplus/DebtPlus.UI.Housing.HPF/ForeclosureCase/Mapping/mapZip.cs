﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the zipcode field
        /// </summary>
        public static string mapZip(string KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            // Remove all but the numbers from the zipcodes. We assume that the addresses are US format only at this point.
            KeyField = DebtPlus.Utils.Format.Strings.DigitsOnly(KeyField);
            if (string.IsNullOrEmpty(KeyField))
            {
                return null;
            }

            return KeyField.PadLeft(5, '0').Substring(0, 5);
        }

        /// <summary>
        /// Map the zipcode field
        /// </summary>
        public static string mapZipPlus4(string KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            // Remove all but the numbers from the zipcodes. We assume that the addresses are US format only at this point.
            KeyField = DebtPlus.Utils.Format.Strings.DigitsOnly(KeyField);
            if (string.IsNullOrEmpty(KeyField))
            {
                return null;
            }

            return KeyField.PadRight(9, '0').Substring(5, 4);
        }
    }
}
