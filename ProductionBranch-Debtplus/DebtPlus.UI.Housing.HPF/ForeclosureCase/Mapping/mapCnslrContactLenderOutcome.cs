﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Housing_CounselorContactOutcome field
        /// </summary>
        public static string mapCnslrContactLenderOutcome(Int32? KeyField)
        {
            // If there is no key then return null
            if (KeyField == null)
            {
                return null;
            }

            // Find the row. If it exists then return the code value for it.
            var q = DebtPlus.LINQ.Cache.Housing_CounselorContactOutcomeType.getList().Find(s => s.Id == KeyField.Value);
            if (q != null && q.hpf != null)
            {
                return q.hpf;
            }

            // If the reason can't be found, use null
            return null;
        }
    }
}
