﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the ReasonForCall status
        /// </summary>
        public static string mapReasonForCall(Int32? KeyField)
        {
            if (KeyField.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.Housing_ReasonForCallType.getList().Find(s => s.Id == KeyField.Value);
                if (q != null)
                {
                    return q.hpf;
                }
            }
            return null;
        }
    }
}
