﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapDocExclusionCd(Int32? KeyField)
        {
            if (KeyField.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.NoDsaProgramReason.getList().Find(s => s.Id == KeyField.Value);
                if (q != null && !string.IsNullOrEmpty(q.hpf))
                {
                    return q.hpf;
                }
            }

            return null;
        }
    }
}
