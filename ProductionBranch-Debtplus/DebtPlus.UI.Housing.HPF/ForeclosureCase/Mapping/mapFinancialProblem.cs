﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Default Reason field
        /// </summary>
        public static string mapFinancialProblem(Int32? KeyField)
        {
            // If there is no financial problem then return null
            if (KeyField == null)
            {
                return null;
            }

            // Find the financial problem. If it exists then return the code value for it.
            var q = DebtPlus.LINQ.Cache.financial_problemType.getList().Find(s => s.Id == KeyField.Value);
            if (q != null && q.hpf != null)
            {
                return q.hpf;
            }

            // If the reason can't be found, use "other"
            return "24";
        }
    }
}
