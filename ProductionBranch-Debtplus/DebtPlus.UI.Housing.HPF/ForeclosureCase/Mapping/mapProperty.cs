﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Property field
        /// </summary>
        public static string mapProperty(Int32? KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            var q = DebtPlus.LINQ.Cache.HousingType.getList().Find(s => s.Id == KeyField.Value);
            if (q != null && ! string.IsNullOrWhiteSpace(q.hpf))
            {
                return q.hpf;
            }

            return null;
        }
    }
}
