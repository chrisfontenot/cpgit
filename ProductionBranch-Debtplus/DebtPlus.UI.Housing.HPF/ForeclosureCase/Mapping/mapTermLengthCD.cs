﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapTermLengthCd(DebtPlus.LINQ.Housing_loan_detail currentDetail)
        {
            if (currentDetail != null && currentDetail.MortgageTypeCD.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.Housing_MortgageType.getList().Find(s => s.Id == currentDetail.MortgageTypeCD.Value);
                if (q != null)
                {
                    return q.termLengthCD;
                }
            }
            return null;
        }
    }
}
