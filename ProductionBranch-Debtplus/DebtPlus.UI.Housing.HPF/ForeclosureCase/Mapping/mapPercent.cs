﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Convert from a percentage used by DebtPlus to the percentage wanted by HPF.
        /// </summary>
        /// <param name="KeyField"></param>
        /// <returns></returns>
        public static double? mapPercent(double? KeyField)
        {
            if (!KeyField.HasValue)
            {
                return null;
            }

            // Convert the percentage from what DebtPlus keeps to what HPF wants
            double fullValue = KeyField.Value;
            if (fullValue < 0.0D)
            {
                throw new ArgumentOutOfRangeException("percent is out of range");
            }

            // Values less than 1.0 are normal. Scale them to be the desired status.
            if (fullValue < 1.0D)
            {
                fullValue *= 100.0D;
            }

            // Reduce the percentage figures to only three decimal places if possible
            // We can not guarantee the results, but we will have tried here.
            return System.Math.Truncate(fullValue * 1000.0D) / 1000.0D;
        }
    }
}
