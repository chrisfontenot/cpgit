﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapDisabled(bool KeyField)
        {
            return mapYN(KeyField);
        }

        /// <summary>
        /// Map the income disabled status
        /// </summary>
        public static string mapDisabled(Int32? KeyField)
        {
            if (KeyField.HasValue)
            {
                return mapDisabled(KeyField.Value != 0);
            }
            return null;
        }
    }
}
