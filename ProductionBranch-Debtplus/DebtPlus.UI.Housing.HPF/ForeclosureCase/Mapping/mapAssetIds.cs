﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the income category item identifiers
        /// </summary>
        public static Int32? mapAssetIds(Int32? KeyField)
        {
            if (!KeyField.HasValue)
            {
                return null;
            }

            var q = DebtPlus.LINQ.Cache.asset_id.getList().Find(s => s.Id == KeyField.Value);
            if (q != null && !string.IsNullOrEmpty(q.hpf))
            {
                return int.Parse(q.hpf);
            }

            // The income category is an optional additional item. Return the code for it.
            return 6;           // Just some income category ID
        }
    }
}
