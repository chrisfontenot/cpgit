﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map a Yes or No answer to the proper string. We keep it as a boolean. They want a string.
        /// </summary>
        /// <param name="KeyField"></param>
        /// <returns></returns>
        public static string mapYN(bool? Value)
        {
            if (Value.HasValue && Value.Value)
            {
                return "Y";
            }
            return "N";
        }
    }
}
