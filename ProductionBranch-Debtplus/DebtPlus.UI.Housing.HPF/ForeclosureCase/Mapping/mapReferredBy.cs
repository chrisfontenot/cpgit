﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the case source item identifiers
        /// </summary>
        public static string mapReferredBy(Int32? keyField)
        {
            if (keyField != null)
            {
                var q = DebtPlus.LINQ.Cache.referred_by.getList().Find(s => s.Id == keyField.Value);
                if (q != null && ! string.IsNullOrWhiteSpace(q.hpf))
                {
                    return q.hpf;
                }
                return "OTHER";
            }

            return null;
        }
    }
}
