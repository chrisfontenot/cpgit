﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapLoanLookupCd(Int32? KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            switch (KeyField.Value)
            {
                case 2:
                    return "FANNIE";
                case 3:
                    return "FREDDIE";
                default:
                    return "NOTFOUND";
            }
        }
    }
}
