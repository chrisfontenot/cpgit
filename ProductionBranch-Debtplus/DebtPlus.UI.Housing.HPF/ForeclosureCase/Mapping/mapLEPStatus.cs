﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the LEPStatus field
        /// </summary>
        public static string mapLEPStatus(Int32? KeyField)
        {
            if (KeyField.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(s => s.Id == KeyField.Value);
                if (q != null)
                {
                    return (string.Compare(q.hpf, "ENG", true) == 0) ? "A" : "B";
                }
            }

            return "C";
        }
    }
}
