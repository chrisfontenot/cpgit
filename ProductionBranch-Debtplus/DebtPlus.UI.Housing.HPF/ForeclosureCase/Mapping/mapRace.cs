﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Race field
        /// </summary>
        public static string mapRace(Int32? KeyField)
        {
            if (KeyField != null)
            {
                var q = DebtPlus.LINQ.Cache.RaceType.getList().Find(s => s.Id == KeyField.Value);
                if (q != null)
                {
                    return q.hpf;
                }
            }

            return null;
        }
    }
}
