﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the CreditBureau field
        /// </summary>
        public static string mapCreditBureau(string KeyField)
        {
            if (string.IsNullOrWhiteSpace(KeyField))
            {
                return null;
            }

            // Find the item in the tables and return the corresponding map entry
            if (string.Compare(KeyField, "EQUIFAX", true) == 0)
            {
                return "EQUIFAX";
            }

            if (string.Compare(KeyField, "EXPERIAN", true) == 0)
            {
                return "EXPERIAN";
            }

            if (string.Compare(KeyField, "TRANSUNION", true) == 0)
            {
                return "TRANSUNION";
            }

            if (string.Compare(KeyField, "TRI-MERGE", true) == 0)
            {
                return "TRI-MERGE";
            }

            return null;
        }
    }
}
