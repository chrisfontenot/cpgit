﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Program Stage field
        /// </summary>
        /// <remarks>Since our values are the same as HPF's, this is a dummy routine. It is provided
        /// should se decide later to use different values.
        /// </remarks>
        public static Int32? mapProgramStage(Int32? KeyField)
        {
            return KeyField;
        }

        /// <summary>
        /// Map the Program Stage Description field
        /// </summary>
        /// <remarks>Since our values are the same as HPF's, this is a dummy routine. It is provided
        /// should se decide later to use different values.
        /// </remarks>
        public static string mapProgramStageDesc(Int32? KeyField)
        {
            if (KeyField == null)
            {
                return null;
            }

            return null;
        }
    }
}
