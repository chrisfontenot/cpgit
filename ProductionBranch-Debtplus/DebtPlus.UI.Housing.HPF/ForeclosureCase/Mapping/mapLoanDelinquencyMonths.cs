﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        public static string mapLoanDelinquencyMonths(Int32? KeyField)
        {
            // Map the number of KeyField to the values that HPF wants
            switch (KeyField.GetValueOrDefault(-2))
            {
                case -2:
                    return "UNK";
                case -1:
                    return "STRUG";
                case 0:
                    return "CUR";
                case 1:
                    return "<30";
                case 2:
                    return "30-59";
                case 3:
                    return "60-89";
                case 4:
                    return "90-119";
                default:
                    return "120+";
            }
        }
    }
}
