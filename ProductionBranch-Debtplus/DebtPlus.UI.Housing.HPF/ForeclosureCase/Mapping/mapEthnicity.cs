﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF
{
    public static partial class Mapping
    {
        /// <summary>
        /// Map the Ethnicity field
        /// </summary>
        public static string mapEthnicity(Int32? KeyField)
        {
            if (KeyField != null)
            {
                var q = DebtPlus.LINQ.Cache.EthnicityType.getList().Find(s => s.Id == KeyField.Value);
                if (q != null)
                {
                    return q.hpf;
                }
            }

            return null;
        }
    }
}
