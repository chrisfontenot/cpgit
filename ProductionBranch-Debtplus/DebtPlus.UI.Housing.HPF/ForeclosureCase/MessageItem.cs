﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF
{
    /// <summary>
    /// Base class of messages
    /// </summary>
    public abstract class MessageItem
    {
        public string text { get; private set; }
        public MessageItem() { }
        public MessageItem(string text) : this() { this.text = text; }
        public override string ToString() { return text; }
    }

    /// <summary>
    /// Error messages
    /// </summary>
    public sealed class ErrorMessage : MessageItem
    {
        public ErrorMessage() : base() { }
        public ErrorMessage(string text) : base(text) { }
        public override string ToString() { return "ERROR: " + text; }
    }

    /// <summary>
    /// Warning messages
    /// </summary>
    public sealed class WarningMessage : MessageItem
    {
        public WarningMessage() : base() { }
        public WarningMessage(string text) : base(text) { }
        public override string ToString() { return "WARNING: " + text; }
    }
}
