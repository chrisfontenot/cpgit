﻿// -----------------------------------------------------------------------
// <copyright file="UIParams.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.SOAP.HPF.Case
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class UIParams
    {
        public class OutcomeClass
        {
            public Int32? outcomeType { get; set; }
            public string extRefOtherName { get; set; }
            public string NonprofitrReferralKeyNum { get; set; }

            public OutcomeClass()
            {
            }

            public OutcomeClass(Int32? outcomeType, string extRefOtherName, string NonprofitrReferralKeyNum) : this()
            {
                this.outcomeType = outcomeType;
                this.extRefOtherName = extRefOtherName;
                this.NonprofitrReferralKeyNum = NonprofitrReferralKeyNum;
            }
        }

        public class EventClass
        {
            public string comments { get; set; }
            public bool completed { get; set; }
            public Int32 eventOutcome { get; set; }
            public Int32 eventType { get; set; }
            public Int32 optOutReason { get; set; }
            public DateTime programRefusal { get; set; }
            public Int32 programStage { get; set; }
            public bool rpc { get; set; }
            public Int32 counselor { get; set; }
        }

        public class SessionCass
        {
            public Int32?   ActionItemsNote;
            public Int32?   FollowupNote;
            public Int32?   LoanDfltReasonNote;
            public decimal  DebtLoadCalculation;
            public double?  DTI_Ratio;
            public decimal? HouseholdNetIncome;
            public decimal? HouseholdGrossIncome;
        }

        private System.Collections.Generic.List<OutcomeClass> _Outcomes = new System.Collections.Generic.List<OutcomeClass>();
        public System.Collections.Generic.List<OutcomeClass> Outcomes
        {
            get
            {
                return _Outcomes;
            }
        }

        private EventClass _Event = new EventClass();
        public EventClass Event
        {
            get
            {
                return _Event;
            }
        }

        private SessionCass _Session = new SessionCass();
        public SessionCass Session
        {
            get
            {
                return _Session;
            }
        }
    }
}
