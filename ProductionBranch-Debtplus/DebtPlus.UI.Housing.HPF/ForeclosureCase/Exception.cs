﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.SOAP.HPF
{
    public class HPFException : System.Exception
    {
        public HPFException() : base() { }
        public HPFException(string message) : base(message) { }
        public HPFException(string message, System.Exception innerException) : base(message, innerException) { }
        public HPFException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    public sealed class HPFNotInMapException : HPFException
    {
        public HPFNotInMapException() : base() { }
        public HPFNotInMapException(string message) : base(message) { }
        public HPFNotInMapException(string message, System.Exception innerException) : base(message, innerException) { }
        public HPFNotInMapException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
