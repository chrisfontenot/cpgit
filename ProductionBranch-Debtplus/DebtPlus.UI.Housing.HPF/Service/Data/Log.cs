﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    public class Log
    {
        public Log()
        {
        }

        public string AuthorizedInd { get; set; }
        public string CallSourceCd { get; set; }
        public string City { get; set; }
        public string CustomerPhone { get; set; }
        public string DelinqInd { get; set; }
        public string DocFulfillmentCd { get; set; }
        public string DTIInd { get; set; }
        public string Email { get; set; }
        public System.Nullable<System.DateTime> EndDate { get; set; }
        public System.Nullable<int> FcId { get; set; }
        public System.Nullable<System.DateTime> FCSaleDate { get; set; }
        public string FinalDispoCd { get; set; }
        public string FinalDispoDesc { get; set; }
        public string FirstName { get; set; }
        public string FnmaLookupCd { get; set; }
        public string FnmApptSetInd { get; set; }
        public string FnmLookupInd { get; set; }
        public string FnmRegionalOfficeCd { get; set; }
        public System.Nullable<decimal> GrossIncomeAmount { get; set; }
        public string HopeNetCallId { get; set; }
        public string HomeownerInd { get; set; }
        public string ICTCallId { get; set; }
        public string InvestorRentalInd { get; set; }
        public string LastName { get; set; }
        public string LoanAccountNumber { get; set; }
        public string LoanDelinqStatusCd { get; set; }
        public string LoanLookupCd { get; set; }
        public System.Nullable<double> LoanModInterestRate { get; set; }
        public System.Nullable<double> LoanModTerm { get; set; }
        public string LoanTrialModCd { get; set; }
        public string MailingAddr1 { get; set; }
        public string MailingAddr2 { get; set; }
        public string MailingCity { get; set; }
        public string MailingStateCd { get; set; }
        public string MailingZip { get; set; }
        public string MaxLoanAmountInd { get; set; }
        public string MHAEligibilityCd { get; set; }
        public string MHAEligibilityDesc { get; set; }
        public string MHAIneligibilityReasonCd { get; set; }
        public string MHAIneligibilityReasonDesc { get; set; }
        public string MHAInfoShareInd { get; set; }
        public string MilActiveInd { get; set; }
        public string MotherMaidenLastName { get; set; }
        public string NonprofitReferralKeyNum1 { get; set; }
        public string NonprofitReferralDesc1 { get; set; }
        public string NonprofitReferralKeyNum2 { get; set; }
        public string NonprofitReferralDesc2 { get; set; }
        public string NonprofitReferralKeyNum3 { get; set; }
        public string NonprofitReferralDesc3 { get; set; }
        public string OptOutReasonCd { get; set; }
        public string OriginatedPrior2009Ind { get; set; }
        public string OtherServicerName { get; set; }
        public System.Nullable<decimal> PaymentAmount { get; set; }
        public string PowerOfAttorneyInd { get; set; }
        public string PreviousUpInd { get; set; }
        public string PrimaryResidenceInd { get; set; }
        public System.Nullable<int> ProgramId { get; set; }
        public string PropStreetAddress { get; set; }
        public string PropZipFull9 { get; set; }
        public string ReasonForCall { get; set; }
        public string RepeatDiscInd { get; set; }
        public System.Nullable<int> ServicerCAId { get; set; }
        public System.Nullable<System.DateTime> ServicerCALastContactDate { get; set; }
        public string ServicerCAOtherName { get; set; }
        public string ServicerCAName { get; set; }
        public System.Nullable<int> ServicerCANumber { get; set; }
        public System.Nullable<int> ServicerId { get; set; }
        public string ServicerInProcessCd { get; set; }
        public string ServicerName { get; set; }
        public string SpocInd { get; set; }
        public System.Nullable<int> SponsorId { get; set; }
        public string SponsorLoanNum { get; set; }
        public string SrvcTrnsfrInd { get; set; }
        public System.Nullable<System.DateTime> StartDate { get; set; }
        public string State { get; set; }
        public string ThirdPartyContactCd { get; set; }
        public string TNCountyCd { get; set; }
        public string UnemployedInd { get; set; }
        public string UpBenefitsInd { get; set; }
        public string StepUpReasonCd { get; set; }

        public Log(DebtPlus.SOAP.HPF.Agency.CallLogWSReturnDTO returnDTO)
        {
            this.AuthorizedInd = returnDTO.AuthorizedInd;
            this.CallSourceCd = returnDTO.CallSourceCd;
            this.City = returnDTO.City;
            this.CustomerPhone = returnDTO.CustomerPhone;
            this.DelinqInd = returnDTO.DelinqInd;
            this.DocFulfillmentCd = returnDTO.DocFulfillmentCd;
            this.DTIInd = returnDTO.DTIInd;
            this.Email = returnDTO.Email;
            this.EndDate = returnDTO.EndDate;
            this.FcId = returnDTO.FcId;
            this.FCSaleDate = returnDTO.FCSaleDate;
            this.FinalDispoCd = returnDTO.FinalDispoCd;
            this.FinalDispoDesc = returnDTO.FinalDispoDesc;
            this.FirstName = returnDTO.FirstName;
            this.FnmaLookupCd = returnDTO.FnmaLookupCd;
            this.FnmApptSetInd = returnDTO.FnmApptSetInd;
            this.FnmLookupInd = returnDTO.FnmLookupInd;
            this.FnmRegionalOfficeCd = returnDTO.FnmRegionalOfficeCd;
            this.GrossIncomeAmount = Convert.ToDecimal(returnDTO.GrossIncomeAmount);
            this.HopeNetCallId = returnDTO.HopeNetCallId;
            this.HomeownerInd = returnDTO.HomeownerInd;
            this.ICTCallId = returnDTO.ICTCallId;
            this.InvestorRentalInd = returnDTO.InvestorRentalInd;
            this.LastName = returnDTO.LastName;
            this.LoanAccountNumber = returnDTO.LoanAccountNumber;
            this.LoanDelinqStatusCd = returnDTO.LoanDelinqStatusCd;
            this.LoanLookupCd = returnDTO.LoanLookupCd;
            this.LoanModInterestRate = returnDTO.LoanModInterestRate;
            this.LoanModTerm = returnDTO.LoanModTerm;
            this.LoanTrialModCd = returnDTO.LoanTrialModCd;
            this.MailingAddr1 = returnDTO.MailingAddr1;
            this.MailingAddr2 = returnDTO.MailingAddr2;
            this.MailingCity = returnDTO.MailingCity;
            this.MailingStateCd = returnDTO.MailingStateCd;
            this.MailingZip = returnDTO.MailingZip;
            this.MaxLoanAmountInd = returnDTO.MaxLoanAmountInd;
            this.MHAEligibilityCd = returnDTO.MHAEligibilityCd;
            this.MHAEligibilityDesc = returnDTO.MHAEligibilityDesc;
            this.MHAIneligibilityReasonCd = returnDTO.MHAIneligibilityReasonCd;
            this.MHAIneligibilityReasonDesc = returnDTO.MHAIneligibilityReasonDesc;
            this.MHAInfoShareInd = returnDTO.MHAInfoShareInd;
            this.MilActiveInd = returnDTO.MilActiveInd;
            this.MotherMaidenLastName = returnDTO.MotherMaidenLastName;
            this.NonprofitReferralKeyNum1 = returnDTO.NonprofitReferralKeyNum1;
            this.NonprofitReferralDesc1 = returnDTO.NonprofitReferralDesc1;
            this.NonprofitReferralKeyNum2 = returnDTO.NonprofitReferralKeyNum2;
            this.NonprofitReferralDesc2 = returnDTO.NonprofitReferralDesc2;
            this.NonprofitReferralKeyNum3 = returnDTO.NonprofitReferralKeyNum3;
            this.NonprofitReferralDesc3 = returnDTO.NonprofitReferralDesc3;
            this.OptOutReasonCd = returnDTO.OptOutReasonCd;
            this.OriginatedPrior2009Ind = returnDTO.OriginatedPrior2009Ind;
            this.OtherServicerName = returnDTO.OtherServicerName;
            this.PaymentAmount = Convert.ToDecimal(returnDTO.PaymentAmount);
            this.PowerOfAttorneyInd = returnDTO.PowerOfAttorneyInd;
            this.PreviousUpInd = returnDTO.PreviousUpInd;
            this.PrimaryResidenceInd = returnDTO.PrimaryResidenceInd;
            this.ProgramId = returnDTO.ProgramId;
            this.PropStreetAddress = returnDTO.PropStreetAddress;
            this.PropZipFull9 = returnDTO.PropZipFull9;
            this.ReasonForCall = returnDTO.ReasonForCall;
            this.RepeatDiscInd = returnDTO.RepeatDiscInd;
            this.ServicerCAId = returnDTO.ServicerCAId;
            this.ServicerCALastContactDate = returnDTO.ServicerCALastContactDate;
            this.ServicerCAOtherName = returnDTO.ServicerCAOtherName;
            this.ServicerCAName = returnDTO.ServicerCAName;
            this.ServicerCANumber = returnDTO.ServicerCANumber;
            this.ServicerId = returnDTO.ServicerId;
            this.ServicerInProcessCd = returnDTO.ServicerInProcessCd;
            this.ServicerName = returnDTO.ServicerName;
            this.SpocInd = returnDTO.SpocInd;
            this.SponsorId = returnDTO.SponsorId;
            this.SponsorLoanNum = returnDTO.SponsorLoanNum;
            this.SrvcTrnsfrInd = returnDTO.SrvcTrnsfrInd;
            this.StartDate = returnDTO.StartDate;
            this.State = returnDTO.State;
            this.ThirdPartyContactCd = returnDTO.ThirdPartyContactCd;
            this.TNCountyCd = returnDTO.TNCountyCd;
            this.UnemployedInd = returnDTO.UnemployedInd;
            this.UpBenefitsInd = returnDTO.UpBenefitsInd;
            this.StepUpReasonCd = returnDTO.StepUpReasonCd;
        }
    }
}
