﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// Budget Item Information
    /// </summary>
    public partial class BudgetItem
    {
        public BudgetItem()
        {
        }

        public decimal? BudgetItemAmt { get; set; }
        public string BudgetNote { get; set; }
        public Int32? BudgetSubcategoryId { get; set; }

        /// <summary>
        /// Extract the BudgetItemDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.BudgetItemDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.BudgetItemDTO()
            {
                BudgetItemAmt = Convert.ToDouble(this.BudgetItemAmt),
                BudgetNote = this.BudgetNote,
                BudgetSubcategoryId = this.BudgetSubcategoryId
            };
            return answer;
        }
    }
}
