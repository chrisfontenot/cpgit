﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// Budget Item Information
    /// </summary>
    public partial class CreditReport
    {
        public CreditReport()
        {
        }

        public System.Nullable<System.DateTime> CreditPullDt { get; set; }
        public System.Nullable<Int32> CreditScore { get; set; }
        public string CreditBureauCd { get; set; }
        public System.Nullable<decimal> RevolvingMonthlyPayment { get; set; }
        public System.Nullable<decimal> RevolvingBal { get; set; }
        public System.Nullable<decimal> RevolvingLimitAmt { get; set; }
        public System.Nullable<decimal> InstallmentMonthlyPayment { get; set; }
        public System.Nullable<decimal> InstallmentBal { get; set; }
        public System.Nullable<decimal> InstallmentLimitAmt { get; set; }
        public string FirstMortgageHistory { get; set; }
        public string SecondMortgageHistory { get; set; }
        public string ThirdMortgageHistory { get; set; }
        public System.Nullable<System.DateTime> FnmaFnlmcSoldDt { get; set; }

        /// <summary>
        /// Extract the CreditReportDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.CreditReportDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.CreditReportDTO()
            {
                CreditPullDt = this.CreditPullDt,
                CreditScore = Convert.ToString(this.CreditScore),
                CreditBureauCd = this.CreditBureauCd,
                RevolvingMonthlyPayment = Convert.ToDouble(this.RevolvingMonthlyPayment),
                RevolvingBal = Convert.ToDouble(this.RevolvingBal),
                RevolvingLimitAmt = Convert.ToDouble(this.RevolvingLimitAmt),
                InstallmentMonthlyPayment = Convert.ToDouble(this.InstallmentMonthlyPayment),
                InstallmentBal = Convert.ToDouble(this.InstallmentBal),
                InstallmentLimitAmt = Convert.ToDouble(this.InstallmentLimitAmt),
                FirstMortgageHistory = this.FirstMortgageHistory,
                SecondMortgageHistory = this.SecondMortgageHistory,
                ThirdMortgageHistory = this.ThirdMortgageHistory,
                FnmaFnlmcSoldDt = this.FnmaFnlmcSoldDt
            };
            return answer;
        }
    }
}
