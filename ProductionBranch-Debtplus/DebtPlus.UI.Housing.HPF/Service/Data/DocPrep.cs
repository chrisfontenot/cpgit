﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// DocPrep Information
    /// </summary>
    public partial class DocPrep
    {
        public DocPrep()
        {
        }

        public System.Nullable<int> FCID { get; set; }
        public System.Nullable<int> DocPrepId { get; set; }
        public string StatusCd { get; set; }
        public string Group { get; set; }
        public string OrderId { get; set; }
        public string StatusDesc { get; set; }
        public System.Nullable<System.DateTime> StatusDt { get; set; }
        public string ChgLstUserId { get; set; }
        public string HomeownerFileName { get; set; }
        public string HomeownerQCResultsFileName { get; set; }
        public string HomeownerRequiredDocsChecklistFileName { get; set; }

        /// <summary>
        /// Extract the DocPrepDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.DocPrepDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.DocPrepDTO()
            {
                FCID = this.FCID,
                DocPrepId = this.DocPrepId,
                StatusCd = this.StatusCd,
                Group = this.Group,
                OrderId = this.OrderId,
                StatusDesc = this.StatusDesc,
                StatusDt = this.StatusDt,
                ChgLstUserId = this.ChgLstUserId,
                HomeownerFileName = this.HomeownerFileName,
                HomeownerQCResultsFileName = this.HomeownerQCResultsFileName,
                HomeownerRequiredDocsChecklistFileName = this.HomeownerRequiredDocsChecklistFileName,
            };
            return answer;
        }
    }
}
