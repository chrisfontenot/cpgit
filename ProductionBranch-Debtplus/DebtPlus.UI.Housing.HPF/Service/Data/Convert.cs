﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    public static class Convert
    {
        /// <summary>
        /// Convert the input value to a System.Nullable double.
        /// </summary>
        /// <param name="Value">The input value to be converted</param>
        /// <returns></returns>
        public static System.Nullable<double> ToDouble(System.Nullable<decimal> Value)
        {
            if (!Value.HasValue)
            {
                return null;
            }
            return new System.Nullable<double>(System.Convert.ToDouble(System.Decimal.Truncate(Value.Value * 100M) / 100M));
        }

        /// <summary>
        /// Convert the input value to a System.Nullable decimal.
        /// </summary>
        /// <param name="Value">The input value to be converted</param>
        /// <returns></returns>
        public static System.Nullable<decimal> ToDecimal(System.Nullable<double> Value)
        {
            if (!Value.HasValue)
            {
                return null;
            }
            return new System.Nullable<decimal>(System.Convert.ToDecimal(Value));
        }

        /// <summary>
        /// Convert the input value to a System.Nullable byte
        /// </summary>
        /// <param name="Value">The input value to be converted</param>
        /// <returns></returns>
        public static System.Nullable<byte> ToByte(System.Nullable<Int32> Value)
        {
            if (!Value.HasValue)
            {
                return null;
            }
            if (Value.Value < -255 || Value.Value > 255)
            {
                return null;
            }
            return new System.Nullable<Byte>(System.Convert.ToByte(Value.Value));
        }

        /// <summary>
        /// Convert the input value to a string object
        /// </summary>
        /// <param name="Value">The input value to be converted</param>
        /// <returns></returns>
        public static System.String ToString(System.Nullable<Int32> Value)
        {
            if (!Value.HasValue)
            {
                return null;
            }
            return Value.Value.ToString();
        }
    }
}
