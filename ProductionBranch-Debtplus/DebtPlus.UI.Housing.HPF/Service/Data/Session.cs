﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.UI.Housing.HPF;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// Session Information
    /// </summary>
    public partial class Session
    {
        public Session()
        {
            CompletionEvent = new Event();
            LoanDfltReasonNotes = "...";
            ActionItemsNotes = "...";
            FollowupNotes = "...";
            HouseholdGrossAnnualIncome = 0.0;
            HouseholdNetAnnualIncome = 0.0;
        }

        public System.Nullable<int> SessionId { get; set; }
        public System.Nullable<int> FcId { get; set; }
        public System.Nullable<int> EventId { get; set; }
        public Event CompletionEvent { get; set; }
        public System.Nullable<int> BudgetSetId { get; set; }
        public string LoanDfltReasonNotes { get; set; }
        public string ActionItemsNotes { get; set; }
        public string FollowupNotes { get; set; }
        public System.Nullable<double> HouseholdGrossAnnualIncome { get; set; }
        public System.Nullable<double> HouseholdNetAnnualIncome { get; set; }
        public System.Nullable<double> DebtLoadCalculation { get; set; }
        public System.Nullable<double> DtiRatio { get; set; }
        public string ChgLstUserId { get; set; }
        public System.Nullable<int> ProgramStageId { get; set; }
        public string ProgramStageDesc { get; set; }

        /// <summary>
        /// Extract the SessionDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.SessionDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.SessionDTO()
            {
                SessionId = this.SessionId,
                FcId = this.FcId,
                EventId = this.EventId,
                CompletionEvent = this.CompletionEvent.Extract(),
                BudgetSetId = this.BudgetSetId,
                LoanDfltReasonNotes = this.LoanDfltReasonNotes,
                ActionItemsNotes = this.ActionItemsNotes,
                FollowupNotes = this.FollowupNotes,
                HouseholdGrossAnnualIncome = this.HouseholdGrossAnnualIncome,
                HouseholdNetAnnualIncome = this.HouseholdNetAnnualIncome,
                DebtLoadCalculation = this.DebtLoadCalculation,
                DtiRatio = this.DtiRatio,
                ChgLstUserId = this.ChgLstUserId,
                ProgramStageId = this.ProgramStageId,
                ProgramStageDesc = this.ProgramStageDesc
            };
            return answer;
        }
        /// <summary>
        /// Serialize the current structure to an XML frame so that it may be recorded in the database
        /// </summary>
        /// <returns>The resulting string equivalent of the structure</returns>
        public string Serialize()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(Session), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }
    }
}
