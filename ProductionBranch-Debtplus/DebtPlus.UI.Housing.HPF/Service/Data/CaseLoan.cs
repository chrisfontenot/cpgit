﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// Budget Item Information
    /// </summary>
    public partial class CaseLoan
    {
        public CaseLoan()
        {
        }

        public System.Nullable<int> ServicerId { get; set; }
        public string OtherServicerName { get; set; }
        public string AcctNum { get; set; }
        public string Loan1st2nd { get; set; }
        public string MortgageTypeCd { get; set; }
        public string ArmResetInd { get; set; }
        public string TermLengthCd { get; set; }
        public string LoanDelinqStatusCd { get; set; }
        public System.Nullable<decimal> CurrentLoanBalanceAmt { get; set; }
        public System.Nullable<decimal> OrigLoanAmt { get; set; }
        public System.Nullable<double> InterestRate { get; set; }
        public string OriginatingLenderName { get; set; }
        public string OrigMortgageCoFdicNcusNum { get; set; }
        public string OrigMortgageCoName { get; set; }
        public string OrginalLoanNum { get; set; }
        public string CurrentServicerFdicNcuaNum { get; set; }
        public string MortgageProgramCd { get; set; }
        public System.Nullable<System.DateTime> ArmRateAdjustDt { get; set; }
        public System.Nullable<int> ArmLockDuration { get; set; }
        public string LoanLookupCd { get; set; }
        public string ThirtyDaysLatePastYrInd { get; set; }
        public string PmtMissLessOneYrLoanInd { get; set; }
        public string SufficientIncomeInd { get; set; }
        public string LongTermAffordInd { get; set; }
        public string HarpEligibleInd { get; set; }
        public string OrigPriorTo2009Ind { get; set; }
        public string PriorHampInd { get; set; }
        public string PrinBalWithinLimitInd { get; set; }
        public string HampEligibleInd { get; set; }
        public string LossMitStatusCd { get; set; }
        public string LoanTrialModCd { get; set; }
        public System.Nullable<double> LoanModInterestRate { get; set; }
        public System.Nullable<double> LoanModTerm { get; set; }

        // Fields generated for DebtPlus' use
        public bool HasWorkoutPlanInd { get; set; }
        public bool SrvcrWorkoutPlanCurrentInd { get; set; }
        public bool ContactedSrvcrRecentlyInd { get; set; }
        public bool CounselorContactedSrvcrInd { get; set; }
        public bool CounselorAttemptedSrvcrContactInd { get; set; }
        public bool DiscussedSolutionWithSrvcrInd { get; set; }

        /// <summary>
        /// Extract the CaseLoanDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.CaseLoanDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.CaseLoanDTO()
            {
                ServicerId = this.ServicerId,
                OtherServicerName = this.OtherServicerName,
                AcctNum = this.AcctNum,
                Loan1st2nd = this.Loan1st2nd,
                MortgageTypeCd = this.MortgageTypeCd,
                ArmResetInd = this.ArmResetInd,
                TermLengthCd = this.TermLengthCd,
                LoanDelinqStatusCd = this.LoanDelinqStatusCd,
                CurrentLoanBalanceAmt = Convert.ToDouble(this.CurrentLoanBalanceAmt),
                OrigLoanAmt = Convert.ToDouble(this.OrigLoanAmt),
                InterestRate = this.InterestRate,
                OriginatingLenderName = this.OriginatingLenderName,
                OrigMortgageCoFdicNcusNum = this.OrigMortgageCoFdicNcusNum,
                OrigMortgageCoName = this.OrigMortgageCoName,
                OrginalLoanNum = this.OrginalLoanNum,
                CurrentServicerFdicNcuaNum = this.CurrentServicerFdicNcuaNum,
                MortgageProgramCd = this.MortgageProgramCd,
                ArmRateAdjustDt = this.ArmRateAdjustDt,
                ArmLockDuration = this.ArmLockDuration,
                LoanLookupCd = this.LoanLookupCd,
                ThirtyDaysLatePastYrInd = this.ThirtyDaysLatePastYrInd,
                PmtMissLessOneYrLoanInd = this.PmtMissLessOneYrLoanInd,
                SufficientIncomeInd = this.SufficientIncomeInd,
                LongTermAffordInd = this.LongTermAffordInd,
                HarpEligibleInd = this.HarpEligibleInd,
                OrigPriorTo2009Ind = this.OrigPriorTo2009Ind,
                PriorHampInd = this.PriorHampInd,
                PrinBalWithinLimitInd = this.PrinBalWithinLimitInd,
                HampEligibleInd = this.HampEligibleInd,
                LossMitStatusCd = this.LossMitStatusCd,
                LoanTrialModCd = this.LoanTrialModCd,
                LoanModInterestRate = this.LoanModInterestRate,
                LoanModTerm = this.LoanModTerm
            };
            return answer;
        }
    }
}
