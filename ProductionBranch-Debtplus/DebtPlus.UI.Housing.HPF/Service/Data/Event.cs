﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    using DebtPlus.UI.Housing.HPF;

    /// <summary>
    /// Event Information
    /// </summary>
    public partial class Event
    {
        public Event()
        {
        }

        public System.Nullable<int> EventId { get; set; }
        public System.Nullable<int> FcId { get; set; }
        public System.Nullable<int> ProgramStageId { get; set; }
        public string EventTypeCd { get; set; }
        public System.Nullable<System.DateTime> EventDt { get; set; }
        public string RpcInd { get; set; }
        public string EventOutcomeCd { get; set; }
        public string CompletedInd { get; set; }
        public string CounselorIdRef { get; set; }
        public System.Nullable<System.DateTime> ProgramRefusalDt { get; set; }
        public string AgencyName { get; set; }
        public string ChgLstUserId { get; set; }
        public string OptOutReasonCd { get; set; }
        public string OptOutReason { get; set; }
        public string Comments { get; set; }

        /// <summary>
        /// Extract the EventDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.EventDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.EventDTO()
            {
                EventId = this.EventId,
                FcId = this.FcId,
                ProgramStageId = this.ProgramStageId,
                EventTypeCd = this.EventTypeCd,
                EventDt = this.EventDt,
                RpcInd = this.RpcInd,
                EventOutcomeCd = this.EventOutcomeCd,
                CompletedInd = this.CompletedInd,
                CounselorIdRef = this.CounselorIdRef,
                ProgramRefusalDt = this.ProgramRefusalDt,
                AgencyName = this.AgencyName,
                ChgLstUserId = this.ChgLstUserId,
                OptOutReasonCd = this.OptOutReasonCd,
                OptOutReason = this.OptOutReason,
                Comments = this.Comments
            };
            return answer;
        }
    }
}
