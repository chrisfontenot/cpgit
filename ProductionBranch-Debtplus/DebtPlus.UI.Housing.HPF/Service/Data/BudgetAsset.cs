﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// Budget Asset Information
    /// </summary>
    public partial class BudgetAsset
    {
        public BudgetAsset()
        {
            AssetName = string.Empty;
            AssetValue = null;
        }

        public string AssetName { get; set; }
        public decimal? AssetValue { get; set; }

        /// <summary>
        /// Extract the BudgetAssetDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.BudgetAssetDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.BudgetAssetDTO()
            {
                AssetName = this.AssetName,
                AssetValue = Convert.ToDouble(this.AssetValue)
            };
            return answer;
        }
    }
}
