﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    public class ForeclosureCase
    {
        public ForeclosureCase()
        {
            BankruptcyInd      = "N";
            CreditPullInd      = "?";
            FundingConsentInd  = "?";
            ServicerConsentInd = "?";
        }

        public string ActionItemsNotes { get; set; }
        public string AgencyCaseNum { get; set; }
        public string AgencyClientNum { get; set; }
        public string AgencyMediaInterestInd { get; set; }
        public string AgencySuccessStoryInd { get; set; }
        public string AssignedCounselorIdRef { get; set; }
        public string BankruptcyAttorney { get; set; }
        public string BankruptcyInd { get; set; }
        public string BankruptcyPmtCurrentInd { get; set; }
        public string BorrowerDisabledInd { get; set; }
        public string BorrowerEducLevelCompletedCd { get; set; }
        public string BorrowerFname { get; set; }
        public string BorrowerLname { get; set; }
        public string BorrowerMaritalStatusCd { get; set; }
        public string BorrowerMname { get; set; }
        public string BorrowerOccupation { get; set; }
        public string BorrowerPreferredLangCd { get; set; }
        public string BorrowerSsn { get; set; }
        public string CallId { get; set; }
        public string CaseSourceCd { get; set; }
        public string CertificateId { get; set; }
        public string ChgLstUserId { get; set; }
        public string CoBorrowerDisabledInd { get; set; }
        public string CoBorrowerFname { get; set; }
        public string CoBorrowerLname { get; set; }
        public string CoBorrowerMname { get; set; }
        public string CoBorrowerOccupation { get; set; }
        public string CoBorrowerSsn { get; set; }
        public string ContactAddr1 { get; set; }
        public string ContactAddr2 { get; set; }
        public string ContactCity { get; set; }
        public string ContactedSrvcrRecentlyInd { get; set; }
        public string ContactedSrvcrRecentlyOutcomeCd { get; set; }
        public string ContactStateCd { get; set; }
        public string ContactZip { get; set; }
        public string ContactZipPlus4 { get; set; }
        public string ConversionCountInd { get; set; }
        public string CounseledLanguageCd { get; set; }
        public string CounselingDurationCd { get; set; }
        public string CounselorAttemptedSrvcrContactInd { get; set; }
        public string CounselorAttemptedSrvcrContactOutcomeCd { get; set; }
        public string CounselorAttemptedSrvcrContactReasonCd { get; set; }
        public string CounselorContactedSrvcrInd { get; set; }
        public string CounselorEmail { get; set; }
        public string CounselorExt { get; set; }
        public string CounselorFname { get; set; }
        public string CounselorLname { get; set; }
        public string CounselorPhone { get; set; }
        public string CreditPullInd { get; set; }
        public string DfltReason1stCd { get; set; }
        public string DfltReason2ndCd { get; set; }
        public string DiscussedSolutionWithSrvcrInd { get; set; }
        public string DiscussedSolutionWithSrvcrOutcomeCd { get; set; }
        public string DocExclusionCd { get; set; }
        public string DocInd { get; set; }
        public string DocOptOutCd { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string ErcpOutcomeCd { get; set; }
        public string FcNoticeReceiveInd { get; set; }
        public string FollowupNotes { get; set; }
        public string ForSaleInd { get; set; }
        public string FundingConsentInd { get; set; }
        public string GenderCd { get; set; }
        public string HasWorkoutPlanInd { get; set; }
        public string HispanicInd { get; set; }
        public string HouseholdCd { get; set; }
        public string HudOutcomeCd { get; set; }
        public string HudTerminationReasonCd { get; set; }
        public string IncomeEarnersCd { get; set; }
        public string IntakeCreditBureauCd { get; set; }
        public string IntakeCreditScore { get; set; }
        public string LEPStatusCd { get; set; }
        public string LoanDfltReasonNotes { get; set; }
        public string MilitaryServiceCd { get; set; }
        public string MilPCSInd { get; set; }
        public string MilServiceBranchCd { get; set; }
        public string MilServiceDepCd { get; set; }
        public string MilServiceMemDepInd { get; set; }
        public string MilServiceMemInd { get; set; }
        public string MilServiceRankCd { get; set; }
        public string MotherMaidenLname { get; set; }
        public string OptOutSurveyInd { get; set; }
        public string OwnerOccupiedInd { get; set; }
        public string PreferredContactTime { get; set; }
        public string PrimaryContactNo { get; set; }
        public string PrimaryContactNoTypeCd { get; set; }
        public string PrimaryResidenceInd { get; set; }
        public string ProgramName { get; set; }
        public string PropAddr1 { get; set; }
        public string PropAddr2 { get; set; }
        public string PropCity { get; set; }
        public string PropertyCd { get; set; }
        public string PropStateCd { get; set; }
        public string PropZip { get; set; }
        public string PropZipPlus4 { get; set; }
        public string RaceCd { get; set; }
        public string RealtyCompany { get; set; }
        public string ReasonForCallCd { get; set; }
        public string ReasonForHotlineCd { get; set; }
        public string ReferralClientNum { get; set; }
        public string RuralAreaStatusCd { get; set; }
        public string SecondContactNo { get; set; }
        public string SecondContactNoTypeCd { get; set; }
        public string ServicerComplaintOrEscalationCd { get; set; }
        public string ServicerComplaintOrEscalationInd { get; set; }
        public string ServicerConsentInd { get; set; }
        public string SingleFamilyHomeInd { get; set; }
        public string SpocInd { get; set; }
        public string SrvcrWorkoutPlanCurrentInd { get; set; }
        public string SubprogramCd { get; set; }
        public string SummarySentOtherCd { get; set; }
        public string SvcrContactedHomeownerCd { get; set; }
        public string VacantOrCondemedInd { get; set; }
        public string VipInd { get; set; }
        public string VipReason { get; set; }
        public string WorkedWithAnotherAgencyInd { get; set; }

        public System.Nullable<decimal> HomeCurrentMarketValue { get; set; }
        public System.Nullable<decimal> HomePurchasePrice { get; set; }
        public System.Nullable<decimal> HomeSalePrice { get; set; }
        public System.Nullable<decimal> HouseholdGrossAnnualIncomeAmt { get; set; }
        public System.Nullable<decimal> PrimResEstMktValue { get; set; }

        public System.Nullable<double> MortgagePmtRatio { get; set; }

        public System.Nullable<int> AgencyId { get; set; }
        public System.Nullable<int> CampaignId { get; set; }
        public System.Nullable<int> DependentNum { get; set; }
        public System.Nullable<int> FcId { get; set; }
        public System.Nullable<int> FnmaHcoId { get; set; }
        public System.Nullable<int> HomePurchaseYear { get; set; }
        public System.Nullable<int> NumberOfUnits { get; set; }
        public System.Nullable<int> OccupantNum { get; set; }
        public System.Nullable<int> ProgramId { get; set; }
        public System.Nullable<int> SponsorId { get; set; }
        public System.Nullable<int> SubprogramId { get; set; }

        public System.Nullable<System.DateTime> BankruptcyLoaDt { get; set; }
        public System.Nullable<System.DateTime> BorrowerDob { get; set; }
        public System.Nullable<System.DateTime> CoBorrowerDob { get; set; }
        public System.Nullable<System.DateTime> CreditPullIndDt { get; set; }
        public System.Nullable<System.DateTime> DmpDt { get; set; }
        public System.Nullable<System.DateTime> DocIndDt { get; set; }
        public System.Nullable<System.DateTime> DocOptOutDt { get; set; }
        public System.Nullable<System.DateTime> FcSaleDate { get; set; }
        public System.Nullable<System.DateTime> HudTerminationDt { get; set; }
        public System.Nullable<System.DateTime> IntakeDt { get; set; }
        public System.Nullable<System.DateTime> SummarySentOtherDt { get; set; }

        /// <summary>
        /// Extract the current case to a ForeclosureCaseDTO object for transmission
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.ForeclosureCaseDTO()
            {
                ActionItemsNotes                        = this.ActionItemsNotes,
                AgencyCaseNum                           = this.AgencyCaseNum,
                AgencyClientNum                         = this.AgencyClientNum,
                AgencyId                                = this.AgencyId,
                AgencyMediaInterestInd                  = this.AgencyMediaInterestInd,
                AgencySuccessStoryInd                   = this.AgencySuccessStoryInd,
                AssignedCounselorIdRef                  = this.AssignedCounselorIdRef,
                BankruptcyAttorney                      = this.BankruptcyAttorney,
                BankruptcyInd                           = this.BankruptcyInd,
                BankruptcyLoaDt                         = this.BankruptcyLoaDt,
                BankruptcyPmtCurrentInd                 = this.BankruptcyPmtCurrentInd,
                BorrowerDisabledInd                     = this.BorrowerDisabledInd,
                BorrowerDob                             = this.BorrowerDob,
                BorrowerEducLevelCompletedCd            = this.BorrowerEducLevelCompletedCd,
                BorrowerFname                           = this.BorrowerFname,
                BorrowerLname                           = this.BorrowerLname,
                BorrowerMaritalStatusCd                 = this.BorrowerMaritalStatusCd,
                BorrowerMname                           = this.BorrowerMname,
                BorrowerOccupation                      = this.BorrowerOccupation,
                BorrowerPreferredLangCd                 = this.BorrowerPreferredLangCd,
                BorrowerSsn                             = this.BorrowerSsn,
                CallId                                  = this.CallId,
                CampaignId                              = this.CampaignId,
                CaseSourceCd                            = this.CaseSourceCd,
                CertificateId                           = this.CertificateId,
                ChgLstUserId                            = this.ChgLstUserId,
                CoBorrowerDisabledInd                   = this.CoBorrowerDisabledInd,
                CoBorrowerDob                           = this.CoBorrowerDob,
                CoBorrowerFname                         = this.CoBorrowerFname,
                CoBorrowerLname                         = this.CoBorrowerLname,
                CoBorrowerMname                         = this.CoBorrowerMname,
                CoBorrowerOccupation                    = this.CoBorrowerOccupation,
                CoBorrowerSsn                           = this.CoBorrowerSsn,
                ContactAddr1                            = this.ContactAddr1,
                ContactAddr2                            = this.ContactAddr2,
                ContactCity                             = this.ContactCity,
                ContactedSrvcrRecentlyInd               = this.ContactedSrvcrRecentlyInd,
                ContactedSrvcrRecentlyOutcomeCd         = this.ContactedSrvcrRecentlyOutcomeCd,
                ContactStateCd                          = this.ContactStateCd,
                ContactZip                              = this.ContactZip,
                ContactZipPlus4                         = this.ContactZipPlus4,
                ConversionCountInd                      = this.ConversionCountInd,
                CounseledLanguageCd                     = this.CounseledLanguageCd,
                CounselingDurationCd                    = this.CounselingDurationCd,
                CounselorAttemptedSrvcrContactInd       = this.CounselorAttemptedSrvcrContactInd,
                CounselorAttemptedSrvcrContactOutcomeCd = this.CounselorAttemptedSrvcrContactOutcomeCd,
                CounselorAttemptedSrvcrContactReasonCd  = this.CounselorAttemptedSrvcrContactReasonCd,
                CounselorContactedSrvcrInd              = this.CounselorContactedSrvcrInd,
                CounselorEmail                          = this.CounselorEmail,
                CounselorExt                            = this.CounselorExt,
                CounselorFname                          = this.CounselorFname,
                CounselorLname                          = this.CounselorLname,
                CounselorPhone                          = this.CounselorPhone,
                CreditPullInd                           = this.CreditPullInd,
                CreditPullIndDt                         = this.CreditPullIndDt,
                DependentNum                            = Convert.ToByte(this.DependentNum),
                DfltReason1stCd                         = this.DfltReason1stCd,
                DfltReason2ndCd                         = this.DfltReason2ndCd,
                DiscussedSolutionWithSrvcrInd           = this.DiscussedSolutionWithSrvcrInd,
                DiscussedSolutionWithSrvcrOutcomeCd     = this.DiscussedSolutionWithSrvcrOutcomeCd,
                DmpDt                                   = this.DmpDt,
                DocExclusionCd                          = this.DocExclusionCd,
                DocInd                                  = this.DocInd,
                DocIndDt                                = this.DocIndDt,
                DocOptOutCd                             = this.DocOptOutCd,
                DocOptOutDt                             = this.DocOptOutDt,
                Email1                                  = this.Email1,
                Email2                                  = this.Email2,
                ErcpOutcomeCd                           = this.ErcpOutcomeCd,
                FcId                                    = this.FcId,
                FcNoticeReceiveInd                      = this.FcNoticeReceiveInd,
                FcSaleDate                              = this.FcSaleDate,
                FnmaHcoId                               = this.FnmaHcoId,
                FollowupNotes                           = this.FollowupNotes,
                ForSaleInd                              = this.ForSaleInd,
                FundingConsentInd                       = this.FundingConsentInd,
                GenderCd                                = this.GenderCd,
                HasWorkoutPlanInd                       = this.HasWorkoutPlanInd,
                HispanicInd                             = this.HispanicInd,
                HomeCurrentMarketValue                  = Convert.ToDouble(this.HomeCurrentMarketValue),
                HomePurchasePrice                       = Convert.ToDouble(this.HomePurchasePrice),
                HomePurchaseYear                        = this.HomePurchaseYear,
                HomeSalePrice                           = Convert.ToDouble(this.HomeSalePrice),
                HouseholdCd                             = this.HouseholdCd,
                HouseholdGrossAnnualIncomeAmt           = Convert.ToDouble(this.HouseholdGrossAnnualIncomeAmt),
                HudOutcomeCd                            = this.HudOutcomeCd,
                HudTerminationDt                        = this.HudTerminationDt,
                HudTerminationReasonCd                  = this.HudTerminationReasonCd,
                IncomeEarnersCd                         = this.IncomeEarnersCd,
                IntakeCreditBureauCd                    = this.IntakeCreditBureauCd,
                IntakeCreditScore                       = this.IntakeCreditScore,
                IntakeDt                                = this.IntakeDt,
                LEPStatusCd                             = this.LEPStatusCd,
                LoanDfltReasonNotes                     = this.LoanDfltReasonNotes,
                MilitaryServiceCd                       = this.MilitaryServiceCd,
                MilPCSInd                               = this.MilPCSInd,
                MilServiceBranchCd                      = this.MilServiceBranchCd,
                MilServiceDepCd                         = this.MilServiceDepCd,
                MilServiceMemDepInd                     = this.MilServiceMemDepInd,
                MilServiceMemInd                        = this.MilServiceMemInd,
                MilServiceRankCd                        = this.MilServiceRankCd,
                MortgagePmtRatio                        = this.MortgagePmtRatio,
                MotherMaidenLname                       = this.MotherMaidenLname,
                NumberOfUnits                           = this.NumberOfUnits,
                OccupantNum                             = Convert.ToByte(this.OccupantNum),
                OptOutSurveyInd                         = this.OptOutSurveyInd,
                OwnerOccupiedInd                        = this.OwnerOccupiedInd,
                PreferredContactTime                    = this.PreferredContactTime,
                PrimaryContactNo                        = this.PrimaryContactNo,
                PrimaryContactNoTypeCd                  = this.PrimaryContactNoTypeCd,
                PrimaryResidenceInd                     = this.PrimaryResidenceInd,
                PrimResEstMktValue                      = Convert.ToDouble(this.PrimResEstMktValue),
                ProgramId                               = this.ProgramId,
                ProgramName                             = this.ProgramName,
                PropAddr1                               = this.PropAddr1,
                PropAddr2                               = this.PropAddr2,
                PropCity                                = this.PropCity,
                PropertyCd                              = this.PropertyCd,
                PropStateCd                             = this.PropStateCd,
                PropZip                                 = this.PropZip,
                PropZipPlus4                            = this.PropZipPlus4,
                RaceCd                                  = this.RaceCd,
                RealtyCompany                           = this.RealtyCompany,
                ReasonForCallCd                         = this.ReasonForCallCd,
                ReasonForHotlineCd                      = this.ReasonForHotlineCd,
                ReferralClientNum                       = this.ReferralClientNum,
                RuralAreaStatusCd                       = this.RuralAreaStatusCd,
                SecondContactNo                         = this.SecondContactNo,
                SecondContactNoTypeCd                   = this.SecondContactNoTypeCd,
                ServicerComplaintOrEscalationCd         = this.ServicerComplaintOrEscalationCd,
                ServicerComplaintOrEscalationInd        = this.ServicerComplaintOrEscalationInd,
                ServicerConsentInd                      = this.ServicerConsentInd,
                SingleFamilyHomeInd                     = this.SingleFamilyHomeInd,
                SpocInd                                 = this.SpocInd,
                SponsorId                               = this.SponsorId,
                SrvcrWorkoutPlanCurrentInd              = this.SrvcrWorkoutPlanCurrentInd,
                SubprogramCd                            = this.SubprogramCd,
                SubprogramId                            = this.SubprogramId,
                SummarySentOtherCd                      = this.SummarySentOtherCd,
                SummarySentOtherDt                      = this.SummarySentOtherDt,
                SvcrContactedHomeownerCd                = this.SvcrContactedHomeownerCd,
                VacantOrCondemedInd                     = this.VacantOrCondemedInd,
                VipInd                                  = this.VipInd,
                VipReason                               = this.VipReason,
                WorkedWithAnotherAgencyInd              = this.WorkedWithAnotherAgencyInd
            };

            return answer;
        }
    }
}
