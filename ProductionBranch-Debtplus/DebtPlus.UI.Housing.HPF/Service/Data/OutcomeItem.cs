﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    /// <summary>
    /// OutcomeItem Information
    /// </summary>
    public partial class OutcomeItem
    {
        public OutcomeItem()
        {
        }

        public System.Nullable<int> OutcomeTypeId { get; set; }
        public string NonprofitreferralKeyNum { get; set; }
        public string ExtRefOtherName { get; set; }

        /// <summary>
        /// Extract the OutcomeItemDTO object from the current structure
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.OutcomeItemDTO Extract()
        {
            var answer = new DebtPlus.SOAP.HPF.Agency.OutcomeItemDTO()
            {
                ExtRefOtherName = this.ExtRefOtherName,
                NonprofitreferralKeyNum = this.NonprofitreferralKeyNum,
                OutcomeTypeId = this.OutcomeTypeId
            };
            return answer;
        }
    }
}
