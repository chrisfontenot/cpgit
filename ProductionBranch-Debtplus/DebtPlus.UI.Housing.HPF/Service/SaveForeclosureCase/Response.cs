﻿using System;
using System.Linq;

namespace DebtPlus.UI.Housing.HPF.Case
{
    public class Response
    {
        public int AgencyId { get; set; }
        public DateTime? CompletedDt { get; set; }
        public int? FcId { get; set; }
        public DebtPlus.SOAP.HPF.Agency.ResponseStatus Status { get; set; }
        public System.Collections.Generic.List<DebtPlus.SOAP.HPF.Agency.ExceptionMessage> Messages;

        /// <summary>
        /// Initialize the new response class
        /// </summary>
        public Response()
        {
            // Default the status to success and no messages. This is basically useless.
            Messages = new System.Collections.Generic.List<DebtPlus.SOAP.HPF.Agency.ExceptionMessage>();
            Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success;
        }

        /// <summary>
        /// Initialize the new response class
        /// </summary>
        /// <param name="resp"></param>
        public Response(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveResponse resp) : this()
        {
            AgencyId = resp.AgencyId;
            CompletedDt = resp.CompletedDt;
            FcId = resp.FcId;
            Status = resp.Status;
            Messages = resp.Messages.ToList<DebtPlus.SOAP.HPF.Agency.ExceptionMessage>();
        }

        /// <summary>
        /// Serialize the current structure to an XML frame so that it may be recorded in the database
        /// </summary>
        /// <returns>The resulting string equivalent of the structure</returns>
        public string Serialize()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(Response), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }

        /// <summary>
        /// Convert the response class to a string for later use
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            sb.Append("\r\nSTATUS: ");

            switch (Status)
            {
                case DebtPlus.SOAP.HPF.Agency.ResponseStatus.AuthenticationFail:
                    sb.Append(" Authentication failure (invalid user or password)");
                    break;

                case DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success:
                    sb.Append(" Successful transfer");
                    break;

                case DebtPlus.SOAP.HPF.Agency.ResponseStatus.Warning:
                    sb.Append(" WARNING");
                    break;

                case DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail:
                    sb.Append(" FAILURE");
                    break;

                default:
                    break;
            }

            // Include the agency ID information
            if (AgencyId > 0)
            {
                sb.AppendFormat("\r\nAgency ID = {0:f0}", AgencyId);
            }

            // Include the foreclosure case ID
            if (FcId.HasValue)
            {
                sb.AppendFormat("\r\nFCID = {0:f0}", FcId.Value);
            }

            // Include the completion date
            if (CompletedDt.HasValue)
            {
                sb.AppendFormat("\r\nCompleted Date = {0:d}", CompletedDt.Value);
            }

            // Add any messages
            if (Messages.Count > 0)
            {
                sb.Append("\r\n\r\nRESPONSE MESSAGES:");
                foreach(DebtPlus.SOAP.HPF.Agency.ExceptionMessage msg in Messages)
                {
                    sb.AppendFormat("\r\n* {0}", msg.Message);
                }
            }

            // Remove the leading cr/lf
            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            // The resulting string is the value.
            return sb.ToString();
        }
    }
}
