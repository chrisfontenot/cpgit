﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Housing.HPF.Data
{
    public partial class ForeclosureCaseSet : IDisposable
    {
        public ForeclosureCaseSet()
        {
            db = new Database();
            ForeclosureCase = new ForeclosureCase();
            BudgetAssets = new System.Collections.Generic.List<BudgetAsset>();
            BudgetItems = new System.Collections.Generic.List<BudgetItem>();
            ProposedBudgetItems = new System.Collections.Generic.List<BudgetItem>();
            CaseLoans = new System.Collections.Generic.List<CaseLoan>();
            CreditReport = new System.Collections.Generic.List<CreditReport>();
            DocPrep = new System.Collections.Generic.List<DocPrep>();
            Outcome = new System.Collections.Generic.List<OutcomeItem>();
            Sessions = new System.Collections.Generic.List<Session>();
        }

        public ForeclosureCase ForeclosureCase { get; set; }
        public System.Collections.Generic.List<BudgetAsset> BudgetAssets { get; set; }
        public System.Collections.Generic.List<BudgetItem> BudgetItems { get; set; }
        public System.Collections.Generic.List<BudgetItem> ProposedBudgetItems { get; set; }
        public System.Collections.Generic.List<CaseLoan> CaseLoans { get; set; }
        public System.Collections.Generic.List<CreditReport> CreditReport { get; set; }
        public System.Collections.Generic.List<DocPrep> DocPrep { get; set; }
        public System.Collections.Generic.List<OutcomeItem> Outcome { get; set; }
        public System.Collections.Generic.List<Session> Sessions { get; set; }

        /// <summary>
        /// Extract the submission set information from the items. Turn the lists to arrays as needed.
        /// </summary>
        /// <returns></returns>
        public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSetDTO Extract()
        {
            // Generate the basic response block. The array of items are easy to generate.
            var answer = new DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSetDTO()
            {
                ForeclosureCase = this.ForeclosureCase.Extract(),
                BudgetAssets = BudgetAssets.Select(s => s.Extract()).ToArray(),
                BudgetItems = BudgetItems.Select(s => s.Extract()).ToArray(),
                ProposedBudgetItems = ProposedBudgetItems.Select(s => s.Extract()).ToArray(),
                CaseLoans = CaseLoans.Select(s => s.Extract()).ToArray(),
                CreditReport = CreditReport.Select(s => s.Extract()).ToArray(),
                DocPrep = DocPrep.Select(s => s.Extract()).ToArray(),
                Outcome = Outcome.Select(s => s.Extract()).ToArray(),
                Sessions = Sessions.Select(s => s.Extract()).ToArray()
            };
            return answer;
        }

        /// <summary>
        /// Property record ID being transmitted in this request
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32 propertyID { get; set; }

        /// <summary>
        /// Current Client ID
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public Int32? ClientID { get; set; }

        /// <summary>
        /// Serialize the current structure to an XML frame so that it may be recorded in the database
        /// </summary>
        /// <returns>The resulting string equivalent of the structure</returns>
        public string Serialize()
        {
            // Allocate a file to hold the scripting item
            var sb = new System.Text.StringBuilder();
            using (var fs = new System.IO.StringWriter(sb))
            {
                var ser = new System.Xml.Serialization.XmlSerializer(typeof(ForeclosureCaseSet), string.Empty);
                ser.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }

            // On the serialized buffer we need to do a bit of "cleanup" to make things work properly
            // with the XML column in the database.
            System.Collections.Generic.List<string> lines = sb.ToString().Replace("\r", string.Empty).Split('\n').ToList<string>();

            // Discard the <?xml ...> line since we can't change the encoding in the database write
            if (lines.Count > 0 && lines[0].StartsWith("<?xml", StringComparison.CurrentCultureIgnoreCase))
            {
                lines.RemoveAt(0);
            }

            return string.Join("\r\n", lines);              // Return the resulting string buffer
        }

        /// <summary>
        /// Information about the disclosure class response.
        /// </summary>
        public class my_disclosure
        {
            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client_disclosure Disclosure { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client_DisclosureType DisclosureType { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client_disclosures_MHA MHADisclosure { get; set; }
        }

        public class Database
        {
            public Database() { }

            [System.Xml.Serialization.XmlIgnore]
            public System.Collections.Generic.List<DebtPlus.LINQ.people> colPeople { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.Collections.Generic.List<DebtPlus.LINQ.asset> colOtherIncomes { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.Collections.Generic.List<DebtPlus.LINQ.client_creditor> colDebts { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.Collections.Generic.List<DebtPlus.LINQ.budget_detail> colBudgetDetails { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public System.Collections.Generic.List<DebtPlus.LINQ.Housing_loan> colLoans { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_property propertyRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_loan firstLoanRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_lender firstLoanOrigLender { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_lender firstLoanCurrentLender { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_Loan_DSADetail firstLoanDSADetail { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client_housing housingRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client clientRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.people applicantRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.people coapplicantRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.action_plan actionPlanRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.address homeAddress { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.address propertyAddr { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_ResidencyType residency { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_borrower borrowerRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.Housing_borrower coBorrowerRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.HPFProgram hpfProgram { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.HPFSubProgram hpfSubProgram { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.counselor counselorRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.hud_interview hudInterviewRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public DebtPlus.LINQ.client_appointment clientAppointmentRecord { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public Int32? budgetID { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public my_disclosure privacyDisclosure { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public my_disclosure mhaDisclosure { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public my_disclosure creditAuthDisclosure { get; set; }

            [System.Xml.Serialization.XmlIgnore]
            public my_disclosure shareDisclosure { get; set; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public Database db { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
