﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DebtPlus.Web.HPF.PrivacyPolicy
{
    /// <summary>
    /// Summary description for BaseClass
    /// </summary>
    public class BaseClass : System.Web.UI.Page
    {
        public BaseClass()
            : base()
        {
        }

        protected void DeliverPage()
        {
            // Find the tag from the request string
            var uriRequest = Request.Url;
            var requestString = uriRequest.PathAndQuery;
            var regex = new System.Text.RegularExpressions.Regex(@"/(en|es)/[a-z]+\.asp[x]?/([-A-Za-z0-9]+)\s*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);

            var matches = regex.Match(requestString);
            if (matches.Success)
            {
                markDelivered(matches.Groups[1].Captures[0].Value, matches.Groups[2].Captures[0].Value);
            }

            // First, we must identify the type of file that we are sending as a PDF document so that it is not left
            // to the browser to show it as a HTML document.
            HttpContext.Current.Response.ContentType = "application/pdf";

            // Deliver the policy to the user
            string filename = Server.MapPath("Document.pdf");
            HttpContext.Current.Response.TransmitFile(filename);
            HttpContext.Current.Response.Flush();
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Look for the ID for the indicated record. Mark the record as having been displayed to the user.
        /// </summary>
        private void markDelivered(string languageString, string keyString)
        {
            if (string.IsNullOrEmpty(keyString))
            {
                return;
            }

            try
            {
                // Take the connection string and open the database to update the table.
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlusConnectionString"].ConnectionString;
                using (var dc = new DataClassesDataContext(connectionString))
                {

                    // Find the record in the table
                    var q = (from f in dc.HPFPrivacyPolicies where f.email_ident == keyString select f).FirstOrDefault();
                    if (q != null)
                    {
                        // Mark the record as having been shown on this date
                        q.shown_status = 'V';
                        q.shown_language = languageString;
                        q.shown_date = DateTime.Now;

                        // Submit the changes to the database
                        dc.SubmitChanges();
                    }
                }
            }

            // Do not die should we have an error. There is probably little that we can do
            // about it so just ignore it for the time being.
            catch { }
        }
    }
}
