Imports System.ComponentModel
Imports DebtPlus.Utils

Namespace Comparison
    Public Class ComparisonBaseClass
        Implements INotifyPropertyChanged

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

        Protected Sub RaisePropertyChanged(ByVal e As PropertyChangedEventArgs)
            RaiseEvent PropertyChanged(Me, e)
        End Sub

        Protected Overridable Sub OnPropertyChanged(ByVal e As PropertyChangedEventArgs)
            RaisePropertyChanged(e)
        End Sub

        Protected Sub RaisePropertyChanged(ByVal PropertyName As String)
            OnPropertyChanged(New PropertyChangedEventArgs(PropertyName))
        End Sub

        Protected Sub SetProperty(ByVal PropertyName As String, ByRef OldValue As Int32, ByVal NewValue As Object)
            OldValue = Convert.ToInt32(NewValue)
            RaisePropertyChanged(PropertyName)
        End Sub

        Protected Sub SetProperty(ByVal PropertyName As String, ByRef OldValue As String, ByVal NewValue As Object)
            OldValue = Convert.ToString(NewValue)
            RaisePropertyChanged(PropertyName)
        End Sub

        Protected Sub SetProperty(ByVal PropertyName As String, ByRef OldValue As Double, ByVal NewValue As Object)
            OldValue = Convert.ToDouble(NewValue)
            RaisePropertyChanged(PropertyName)
        End Sub

        Protected Sub SetProperty(ByVal PropertyName As String, ByRef OldValue As Decimal, ByVal NewValue As Object)
            OldValue = Convert.ToDecimal(NewValue)
            RaisePropertyChanged(PropertyName)
        End Sub
    End Class
End NameSpace
