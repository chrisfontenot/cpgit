#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Windows.Forms
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Drawing
Imports DevExpress.XtraGrid
Imports System.IO

Namespace Appointments.Controls

    Friend Class MyGridControl
        Inherits ControlBaseAppointment
        Protected ControlRow As Int32
        Protected EnableMenus As Boolean

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Add the "Handles" clauses here
            AddHandler GridView1.MouseUp, AddressOf GridView1_MouseUp
            AddHandler PopupMenu_Items.Popup, AddressOf PopupMenu_Items_Popup
            AddHandler BarButtonItem_Add.ItemClick, AddressOf MenuItemCreate
            AddHandler BarButtonItem_Change.ItemClick, AddressOf MenuItemEdit
            AddHandler BarButtonItem_Delete.ItemClick, AddressOf MenuItemDelete
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler Load, AddressOf MyGridControl_Load
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            RemoveHandler GridControl1.Layout, AddressOf LayoutChanged
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Protected Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Protected Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Protected WithEvents PopupMenu_Items As DevExpress.XtraBars.PopupMenu
        Protected WithEvents BarButtonItem_Add As DevExpress.XtraBars.BarButtonItem
        Protected WithEvents BarButtonItem_Change As DevExpress.XtraBars.BarButtonItem
        Protected WithEvents BarButtonItem_Delete As DevExpress.XtraBars.BarButtonItem

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.BarButtonItem_Add = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Change = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Delete = New DevExpress.XtraBars.BarButtonItem
            Me.PopupMenu_Items = New DevExpress.XtraBars.PopupMenu(Me.components)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(576, 296)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(
                CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(
                CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(
                CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(
                CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                    CType(CType(246, Byte), Int32),
                                                                                    CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32),
                                                                                      CType(CType(251, Byte), Int32),
                                                                                      CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32),
                                                                                        CType(CType(251, Byte), Int32),
                                                                                        CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(
                CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(
                CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                          CType(CType(246, Byte), Int32),
                                                                                          CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32),
                                                                                        CType(CType(133, Byte), Int32),
                                                                                        CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32),
                                                                                         CType(CType(109, Byte), Int32),
                                                                                         CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32),
                                                                                           CType(CType(139, Byte), Int32),
                                                                                           CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                          CType(CType(184, Byte), Int32),
                                                                                          CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                            CType(CType(184, Byte), Int32),
                                                                                            CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                          CType(CType(184, Byte), Int32),
                                                                                          CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                            CType(CType(184, Byte), Int32),
                                                                                            CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32),
                                                                                          CType(CType(216, Byte), Int32),
                                                                                          CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32),
                                                                                            CType(CType(216, Byte), Int32),
                                                                                            CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                         CType(CType(246, Byte), Int32),
                                                                                         CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32),
                                                                                       CType(CType(216, Byte), Int32),
                                                                                       CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32),
                                                                                         CType(CType(216, Byte), Int32),
                                                                                         CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32),
                                                                                          CType(CType(201, Byte), Int32),
                                                                                          CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32),
                                                                                            CType(CType(201, Byte), Int32),
                                                                                            CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(
                CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(
                CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(
                CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                       CType(CType(184, Byte), Int32),
                                                                                       CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                     CType(CType(246, Byte), Int32),
                                                                                     CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                       CType(CType(246, Byte), Int32),
                                                                                       CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32),
                                                                                      CType(CType(155, Byte), Int32),
                                                                                      CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32),
                                                                                  CType(CType(251, Byte), Int32),
                                                                                  CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32),
                                                                                           CType(CType(246, Byte), Int32),
                                                                                           CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32),
                                                                                          CType(CType(155, Byte), Int32),
                                                                                          CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32),
                                                                                       CType(CType(184, Byte), Int32),
                                                                                       CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'barManager1
            '
            Me.BarManager1.DockControls.Add(Me.barDockControlTop)
            Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
            Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
            Me.BarManager1.DockControls.Add(Me.barDockControlRight)
            Me.BarManager1.Form = Me
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() _
                                             {Me.BarButtonItem_Add, Me.BarButtonItem_Change, Me.BarButtonItem_Delete})
            Me.BarManager1.MaxItemId = 11
            '
            'BarButtonItem_Add
            '
            Me.BarButtonItem_Add.Caption = "Add..."
            Me.BarButtonItem_Add.Description = "Add an item to the list"
            Me.BarButtonItem_Add.Id = 0
            Me.BarButtonItem_Add.Name = "BarButtonItem_Add"
            '
            'BarButtonItem_Change
            '
            Me.BarButtonItem_Change.Caption = "Change..."
            Me.BarButtonItem_Change.Description = "Change the list item"
            Me.BarButtonItem_Change.Id = 1
            Me.BarButtonItem_Change.Name = "BarButtonItem_Change"
            '
            'BarButtonItem_Delete
            '
            Me.BarButtonItem_Delete.Caption = "Delete..."
            Me.BarButtonItem_Delete.Description = "Remove the item from the list"
            Me.BarButtonItem_Delete.Id = 2
            Me.BarButtonItem_Delete.Name = "BarButtonItem_Delete"
            '
            'PopupMenu_Items
            '
            Me.PopupMenu_Items.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() _
                                                            {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Add),
                                                             New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Change),
                                                             New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Delete)})
            Me.PopupMenu_Items.Manager = Me.BarManager1
            Me.PopupMenu_Items.MenuCaption = "Right Click Menus"
            Me.PopupMenu_Items.Name = "PopupMenu_Items"
            '
            'MyGridControl
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "MyGridControl"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        ''' <summary>
        ''' Handle the condition where the popup menu was activated
        ''' </summary>
        Private Sub GridView1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            If Not DesignMode AndAlso EnableMenus AndAlso e.Button = MouseButtons.Right Then
                PopupMenu_Items.ShowPopup(MousePosition)
            End If
        End Sub

        ''' <summary>
        ''' Handle the condition where the popup menu was activated
        ''' </summary>
        Protected Overridable Sub PopupMenu_Items_Popup(ByVal sender As Object, ByVal e As EventArgs)
            BarButtonItem_Add.Enabled = OkToCreate()
            BarButtonItem_Delete.Enabled = (ControlRow >= 0) AndAlso OkToDelete(GridView1.GetRow(ControlRow))
            BarButtonItem_Change.Enabled = (ControlRow >= 0) AndAlso OkToEdit(GridView1.GetRow(ControlRow))
        End Sub

        ''' <summary>
        ''' Process a CREATE menu item choice
        ''' </summary>
        Protected Sub MenuItemCreate(ByVal sender As Object, ByVal e As EventArgs)
            If Not DesignMode Then
                OnCreateItem()
            End If
        End Sub

        ''' <summary>
        ''' Process an EDIT menu item choice
        ''' </summary>
        Protected Sub MenuItemEdit(ByVal sender As Object, ByVal e As EventArgs)
            If Not DesignMode Then
                If ControlRow >= 0 Then
                    Dim obj As Object = GridView1.GetRow(ControlRow)
                    If obj IsNot Nothing Then
                        OnEditItem(obj)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process a DELETE menu item
        ''' </summary>
        Protected Sub MenuItemDelete(ByVal sender As Object, ByVal e As EventArgs)
            If Not DesignMode Then
                If ControlRow >= 0 Then
                    Dim obj As Object = GridView1.GetRow(ControlRow)
                    If obj IsNot Nothing Then
                        OnDeleteItem(obj)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Look for the mouse down on the grid control
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            If Not DesignMode Then
                Dim gv As GridView = GridView1
                Dim hi As GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

                ' Remember the position for the popup menu handler.
                If hi.InRow Then
                    ControlRow = hi.RowHandle
                Else
                    ControlRow = -1
                End If
            End If
        End Sub

        ''' <summary>
        ''' Is the item valid to be deleted?
        ''' </summary>
        Protected Overridable Function OkToDelete(ByVal obj As Object) As Boolean
            Return obj IsNot Nothing
        End Function

        ''' <summary>
        ''' Is the item valid to be edited?
        ''' </summary>
        Protected Overridable Function OkToEdit(ByVal obj As Object) As Boolean
            Return obj IsNot Nothing
        End Function

        ''' <summary>
        ''' Is the item valid to be created?
        ''' </summary>
        Protected Overridable Function OkToCreate() As Boolean
            Return True
        End Function

        ''' <summary>
        ''' The create operation was desired
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub OnCreateItem()
            Dim vue As DataView = TryCast(GridControl1.DataSource, DataView)
            If vue IsNot Nothing Then
                Dim drv As DataRowView = vue.AddNew()
                drv.BeginEdit()
                If Not CreateItem(drv) Then
                    drv.CancelEdit()
                    Return
                End If

                drv.EndEdit()
                Return
            End If

            Dim tbl As DataTable = TryCast(GridControl1.DataSource, DataTable)
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.NewRow()
                row.BeginEdit()
                If Not CreateItem(row) Then
                    row.CancelEdit()
                    Return
                End If
                row.EndEdit()
                tbl.Rows.Add(row)
            End If
        End Sub

        ''' <summary>
        ''' Overridable function to create an item in the list
        ''' </summary>
        Protected Overridable Function CreateItem(ByVal drv As DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Function CreateItem(ByVal row As DataRow) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Delete the indicated item from the list
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overridable Sub OnDeleteItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                If DeleteItem(drv) Then
                    drv.Delete()
                End If
                Return
            End If

            Dim row As DataRow = TryCast(obj, DataRow)
            If row IsNot Nothing Then
                If DeleteItem(row) Then
                    row.Delete()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Overridable function to delete the item in the list
        ''' </summary>
        Protected Overridable Function DeleteItem(ByVal dvr As DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Function DeleteItem(ByVal row As DataRow) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Edit the indicated object in the list
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overridable Sub OnEditItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                drv.BeginEdit()
                If Not EditItem(drv) Then
                    drv.CancelEdit()
                    Return
                End If

                drv.EndEdit()
                Return
            End If

            Dim row As DataRow = TryCast(obj, DataRow)
            If row IsNot Nothing Then
                row.BeginEdit()
                If Not EditItem(row) Then
                    row.CancelEdit()
                    Return
                End If
                row.EndEdit()
            End If
        End Sub

        ''' <summary>
        ''' Over-ridable function to edit the item in the list
        ''' </summary>
        Protected Overridable Function EditItem(ByVal dvr As DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Function EditItem(ByVal dvr As DataRow) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            If Not DesignMode Then
                Dim ControlRow As Int32 = e.FocusedRowHandle
                OnSelectItem(GridView1.GetRow(ControlRow))
            End If
        End Sub

        ''' <summary>
        ''' Edit the indicated object in the list
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overridable Sub OnSelectItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(GridControl1.DataSource, DataRowView)
            If drv IsNot Nothing Then
                SelectItem(drv)
                Return
            End If

            Dim row As DataRow = TryCast(GridControl1.DataSource, DataRow)
            If row IsNot Nothing Then
                SelectItem(row)
            End If
        End Sub

        ''' <summary>
        ''' Over-ridable function to indicate that the row was selected
        ''' </summary>
        Protected Overridable Sub SelectItem(ByVal dvr As DataRowView)
        End Sub

        Protected Overridable Sub SelectItem(ByVal row As DataRow)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            If Not DesignMode Then

                ' Find the row targeted as the double-click item
                Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
                Dim ControlRow As Int32 = hi.RowHandle

                ' Find the datarowview from the input tables.
                If ControlRow >= 0 Then
                    Dim obj As Object = GridView1.GetRow(ControlRow)
                    OnSelectItem(obj)
                    If obj IsNot Nothing Then
                        OnEditItem(obj)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode OrElse ParentForm Is Nothing Then Return String.Empty
            Dim basePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar)
            Return Path.Combine(basePath, String.Format("Client.Update{0}{1}", Path.DirectorySeparatorChar, ParentForm.Name))
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            ReloadGridControlLayout()
        End Sub

        ''' <summary>
        ''' Reload the layout of the grid control if needed
        ''' </summary>
        Protected Sub ReloadGridControlLayout()

            ' Find the base path to the saved file location
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then
                BeginInit()
                Try
                    Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    If File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If
                Catch ex As DirectoryNotFoundException
                Catch ex As FileNotFoundException

                Finally
                    EndInit()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Protected Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            If Not InInt() Then
                Dim PathName As String = XMLBasePath()
                If Not String.IsNullOrEmpty(PathName) Then
                    If Not Directory.Exists(PathName) Then
                        Directory.CreateDirectory(PathName)
                    End If

                    Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    GridView1.SaveLayoutToXml(FileName)
                End If
            End If
        End Sub
    End Class
End Namespace
