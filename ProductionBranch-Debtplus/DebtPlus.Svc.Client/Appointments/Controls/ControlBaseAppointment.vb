#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Controls
Imports DevExpress.XtraEditors

Namespace Appointments.Controls

    Friend Class ControlBaseAppointment
        Inherits XtraUserControl
        Implements IAppointmentControls
        Implements ISupportInitialize

        ' Delegate to the UpdateFields function to reject field names from being updated
        Friend Delegate Function TableFilterHandler(ByVal FieldName As String) As Boolean

#Region "ISupportInitialize"

        Private InitCount As Int32

        Protected Function InInt() As Boolean
            Return InitCount > 0
        End Function

        Public Sub BeginInit() Implements ISupportInitialize.BeginInit
            InitCount += 1
        End Sub

        Public Sub EndInit() Implements ISupportInitialize.EndInit
            InitCount -= 1
            If InitCount < 0 Then
                InitCount = 0
            End If
        End Sub

#End Region

        ''' <summary>
        ''' Read the form data
        ''' </summary>
        <Description("Called when the data is to be reloaded on this page.")>
        Friend Overridable Sub ReadForm() Implements IAppointmentControls.ReadForm
        End Sub

        ''' <summary>
        ''' Save the form changes
        ''' </summary>
        <Description("Called when the data is to be saved on this page.")>
        Friend Overridable Sub SaveForm() Implements IAppointmentControls.SaveForm
        End Sub

        ''' <summary>
        ''' Process Menu Item choices
        ''' </summary>
        <Description("Called when a menu item is clicked.")>
        Friend Overridable Sub HandleMenuClick(ByVal ItemName As String) Implements IAppointmentControls.HandleMenuClick
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            '
            'ControlBase
            '
            Me.Name = "ControlBaseAppointment"
            Me.Size = New System.Drawing.Size(576, 296)
        End Sub

#End Region

    End Class
End NameSpace
