#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Events
Imports DebtPlus.Utils.Format

Namespace Appointments
    Public Class ClientLetterThread

        Private ReadOnly ClientAppointment As DebtPlus.LINQ.client_appointment

        Public Sub New(ByVal ClientAppointment As DebtPlus.LINQ.client_appointment)
            Me.ClientAppointment = ClientAppointment
        End Sub

        ''' <summary>
        ''' Supply values for the parameters on the letter
        ''' </summary>
        Public Sub LetterThread_GetValue(ByVal Sender As Object, ByVal value As DebtPlus.Events.ParameterValueEventArgs)

            ' Return the client ID to the letter thread if needed
            If String.Compare(value.Name, ParameterValueEventArgs.name_client, True) = 0 Then
                value.Value = ClientAppointment.client

                ' Return the appointment ID to the letter thread if needed
            ElseIf String.Compare(value.Name, ParameterValueEventArgs.name_appointment, True) = 0 Then
                value.Value = ClientAppointment.Id
            End If

        End Sub
    End Class
End NameSpace
