#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.DataSets

Namespace Appointments

    Public Class ClientAppointmentClass
        Inherits Object

        Public Sub New(ByVal clientDs As ClientDataSet)
            MyClass.New()
            MyClass.ClientDs = clientDs
        End Sub

        Private _privateClientDs As ClientDataSet
        Public Property ClientDs() As ClientDataSet
            Get
                If _privateClientDs Is Nothing Then _privateClientDs = New ClientDataSet
                Return _privateClientDs
            End Get
            Set(ByVal value As ClientDataSet)
                _privateClientDs = value
            End Set
        End Property

        Public Property ClientId() As Int32
            Get
                Return ClientDs.ClientId
            End Get
            Set(ByVal value As Int32)
                ClientDs.ClientId = value
            End Set
        End Property

        Private AppointmentDs As New DataSet("AppointmentDS")

        Public Sub New()
            MyBase.New()
        End Sub

        <Obsolete("Use LINQ tables")> _
        Public Function AppointmentConfirmationTypesView() As DataView
            Return New DataView(ClientDs.MessagesTable, "[item_type]='APPT CONFIRMATION'", "description", DataViewRowState.CurrentRows) ' obsolete
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function AppointmentReferredByTable() As DataTable
            Const myTableName As String = "appt_referred_by"
            If AppointmentDs.Tables(myTableName) IsNot Nothing Then Return AppointmentDs.Tables(myTableName)

            Dim gdr As Repository.GetDataResult = Repository.Appointments.GetAllReferredByAppointments(AppointmentDs, myTableName)
            If Not gdr.Success Then
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
                Return Nothing
            End If

            With AppointmentDs.Tables(myTableName)
                .PrimaryKey = New DataColumn() {.Columns(0)}
            End With

            Return AppointmentDs.Tables(myTableName)
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function AppointmentTypesTable() As DataTable
            Const myTableName As String = "appt_types"
            If AppointmentDs.Tables(myTableName) IsNot Nothing Then Return AppointmentDs.Tables(myTableName)

            Dim gdr As Repository.GetDataResult = Repository.LookupTables.ApptTypes.GetAll2(AppointmentDs, myTableName)
            If Not gdr.Success Then
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
                Return Nothing
            End If

            With AppointmentDs.Tables(myTableName)
                .PrimaryKey = New DataColumn() {.Columns("appt_type")}
            End With

            Return AppointmentDs.Tables(myTableName)
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function FindAppointmentTypeByClientAppointment(ByVal oID As Int32) As Int32
            Dim apptType As Int32 = -1

            Dim tbl As DataTable = ClientAppointmentsTable(oID)
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(oID)
                If row IsNot Nothing Then
                    apptType = DebtPlus.Utils.Nulls.DInt(row("appt_type"))
                End If
            End If

            Return apptType
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function CancelLetterByClientAppointment(ByVal oID As Int32) As String
            Const letterID As String = "cancel_letter"
            Dim apptType As Int32 = FindAppointmentTypeByClientAppointment(oID)

            Dim tbl As DataTable = AppointmentTypesTable()
            Dim row As DataRow = tbl.Rows.Find(apptType)

            Dim answer As String
            If row IsNot Nothing AndAlso row(letterID) IsNot Nothing AndAlso row(letterID) IsNot DBNull.Value Then
                answer = Convert.ToString(row(letterID)).Trim()
            Else
                answer = String.Empty
            End If

            Return answer
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function BookLetterByClientAppointment(ByVal oID As Int32) As String
            Const letterID As String = "create_letter"
            Dim apptType As Int32 = FindAppointmentTypeByClientAppointment(oID)
            Dim tbl As DataTable = AppointmentTypesTable()
            Dim row As DataRow = tbl.Rows.Find(apptType)

            Dim answer As String
            If row IsNot Nothing AndAlso row(letterID) IsNot Nothing AndAlso row(letterID) IsNot DBNull.Value Then
                answer = Convert.ToString(row(letterID)).Trim()
            Else
                answer = String.Empty
            End If

            Return answer
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function RescheduleLetterByClientAppointment(ByVal oID As Int32) As String
            Const letterID As String = "reschedule_letter"
            Dim apptType As Int32 = FindAppointmentTypeByClientAppointment(oID)
            Dim tbl As DataTable = AppointmentTypesTable()
            Dim row As DataRow = tbl.Rows.Find(apptType)

            Dim answer As String
            If row IsNot Nothing AndAlso row(letterID) IsNot Nothing AndAlso row(letterID) IsNot DBNull.Value Then
                answer = Convert.ToString(row(letterID)).Trim()
            Else
                answer = String.Empty
            End If

            Return answer
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function ClientAppointmentType(ByVal clientAppointmentId As Int32) As Int32
            Dim tbl As DataTable = ClientAppointmentsTable()
            Dim row As DataRow = tbl.Rows.Find(clientAppointmentId)
            Dim answer As Int32
            If row IsNot Nothing Then
                answer = DebtPlus.Utils.Nulls.DInt(row("appt_type"), -1)
            Else
                answer = -1
            End If
            Return answer
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Sub FlushClientAppointmentsTable()
            Const myTableName As String = "ClientAppointments"
            Dim tbl As DataTable = AppointmentDs.Tables(myTableName)
            If (tbl IsNot Nothing) Then
                AppointmentDs.Tables.Remove(tbl)
                tbl.Dispose()
            End If
        End Sub

        <Obsolete("Use LINQ tables")> _
        Public Sub UpdateClientAppointments()
            Repository.ClientAppointments.CommitChanges2(ClientAppointmentsTable)
        End Sub

        <Obsolete("Use LINQ tables")> _
        Public Function ClientAppointmentsTable(ByVal clientAppointmentId As Int32) As DataTable
            Const myTableName As String = "ClientAppointments"
            If AppointmentDs.Tables(myTableName) IsNot Nothing Then Return AppointmentDs.Tables(myTableName)

            Dim gdr As Repository.GetDataResult = Repository.ClientAppointments.GetById(AppointmentDs, myTableName, clientAppointmentId)
            If Not gdr.Success Then
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
                Return Nothing
            End If

            SetClientAppointmentsTableMetaData()

            Return AppointmentDs.Tables(myTableName)
        End Function

        <Obsolete("Use LINQ tables")> _
        Public Function ClientAppointmentsTable() As DataTable
            Const myTableName As String = "ClientAppointments"
            If AppointmentDs.Tables(myTableName) IsNot Nothing Then Return AppointmentDs.Tables(myTableName)

            Dim gdr As Repository.GetDataResult = Repository.ClientAppointments.GetByClientId(AppointmentDs, myTableName, ClientId)
            If Not gdr.Success Then
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
                Return Nothing
            End If

            SetClientAppointmentsTableMetaData()

            Return AppointmentDs.Tables(myTableName)
        End Function

        <Obsolete("Use LINQ tables")> _
        Private Sub SetClientAppointmentsTableMetaData()
            Const myTableName As String = "ClientAppointments"
            If AppointmentDs.Tables(myTableName) Is Nothing Then Return

            With AppointmentDs.Tables(myTableName)
                If .PrimaryKey.GetUpperBound(0) < 0 Then
                    .PrimaryKey = New DataColumn() {.Columns("client_appointment")}
                    With .Columns("client_appointment")
                        .AutoIncrement = True
                        .AutoIncrementSeed = -1
                        .AutoIncrementStep = -1
                    End With
                End If
            End With
        End Sub

    End Class
End Namespace
