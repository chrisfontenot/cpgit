using System;
using System.Data.SqlClient;

namespace Guidelight.Data
{
    public static class SqlParameterExtensions
    {
        /// <summary>
        /// If string is Null or Empty, set the SqlParameter.Value to DBNull
        /// </summary>
        public static void SafeSetString(this SqlParameter param, string val)
        {
            if (param == null) return;
            param.Value = String.IsNullOrEmpty(val) ? Convert.DBNull : val;
        }

        public static void SafeSetNonZeroInt(this SqlParameter param, int val)
        {
            if (param == null) return;
            param.Value = val == 0 ? Convert.DBNull : val;
        }

        public static void SafeSetNullableInt(this SqlParameter param, int? val)
        {
            if (param == null) return;
            param.Value = val ?? Convert.DBNull;
        }

        public static void SafeSetNullableDecimal(this SqlParameter param, decimal? val)
        {
            if (param == null) return;
            param.Value = val ?? Convert.DBNull;
        }

        public static void SafeSetNullableDateTime(this SqlParameter param, DateTime? val)
        {
            if (param == null) return;
            param.Value = val ?? Convert.DBNull;
        }
    }
}