using System;
using System.Data;

namespace Guidelight.Data
{
    public static class SqlDataReaderExtensions
    {
        public static string SafeGetString(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return string.Empty;
            if (Convert.IsDBNull(reader[colName])) return string.Empty;

            return reader[colName].ToString();
        }

        public static string SafeGetNullableString(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return null;
            if (Convert.IsDBNull(reader[colName])) return null;

            return reader[colName].ToString();
        }

        public static int SafeGetIntOrMin(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return int.MinValue;
            if (Convert.IsDBNull(reader[colName])) return int.MinValue;

            int val;
            return Int32.TryParse(reader[colName].ToString(), out val) ? val : int.MinValue;
        }

        public static int SafeGetIntOrZero(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return 0;
            if (Convert.IsDBNull(reader[colName])) return 0;

            int val;
            return Int32.TryParse(reader[colName].ToString(), out val) ? val : 0;
        }

        public static int? SafeGetNullableInt(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return null;
            if (Convert.IsDBNull(reader[colName])) return null;

            int val;
            if (Int32.TryParse(reader[colName].ToString(), out val))
                return val;
            return null;
        }

        public static decimal SafeGetDecimal(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return decimal.MinValue;
            if (Convert.IsDBNull(reader[colName])) return decimal.MinValue;

            decimal val;
            return Decimal.TryParse(reader[colName].ToString(), out val) ? val : decimal.MinValue;
        }

        public static decimal? SafeGetNullableDecimal(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return decimal.MinValue;
            if (Convert.IsDBNull(reader[colName])) return null;

            decimal val;
            return Decimal.TryParse(reader[colName].ToString(), out val) ? (decimal?) val : null;
        }

        public static DateTime SafeGetDateTime(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return DateTime.MinValue;
            if (Convert.IsDBNull(reader[colName])) return DateTime.MinValue;

            DateTime val;
            return DateTime.TryParse(reader[colName].ToString(), out val) ? val : DateTime.MinValue;
        }

        public static DateTime? SafeGetNullableDateTime(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return null;
            if (Convert.IsDBNull(reader[colName])) return null;

            DateTime val;
            if (DateTime.TryParse(reader[colName].ToString(), out val))
                return val;
            return null;
        }

        public static bool SafeGetBoolOrFalse(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return false;
            if (Convert.IsDBNull(reader[colName]) || reader[colName] == null) return false;

            string val = reader[colName].ToString();

            // bool is most likely coded as a '0' or a '1'
            int n;
            if (Int32.TryParse(reader[colName].ToString(), out n)) return n != 0;

            // but it could be a 'true' or 'false'
            bool b;
            return Boolean.TryParse(val, out b) ? b : false;
        }

        public static bool? SafeGetNullableBool(this IDataReader reader, string colName)
        {
            if (string.IsNullOrEmpty(colName)) return null;
            if (Convert.IsDBNull(reader[colName]) || reader[colName] == null) return null;

            string val = reader[colName].ToString();

            // bool is most likely coded as a '0' or a '1'
            int n;
            if (Int32.TryParse(reader[colName].ToString(), out n)) return n != 0;

            // but it could be a 'true' or 'false'
            bool b;
            if (Boolean.TryParse(val, out b))
                return b;
            return null;
        }

        public static bool FieldExists(this IDataReader reader, string fieldName)
        {
            reader.GetSchemaTable().DefaultView.RowFilter = string.Format("ColumnName= '{0}'", fieldName);
            return (reader.GetSchemaTable().DefaultView.Count > 0);
        }
    }
}
