﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class Modules
    {
        /// <summary>
        /// Find the type associated with the string name
        /// </summary>
        /// <param name="typeName">The name of the type desired</param>
        /// <returns>A type reference that corresponds to the name passed</returns>
        /// <remarks>It is not able to load a new type. It only looks in the loaded types.</remarks>
        public static System.Type FindTypeFromName(string typeName)
        {
            var asmList = (from a in AppDomain.CurrentDomain.GetAssemblies() select a).ToList();
            return (from a in asmList where a.GetType(typeName, false, true) != null select a.GetType(typeName, false, true)).FirstOrDefault();
        }
    }
}
