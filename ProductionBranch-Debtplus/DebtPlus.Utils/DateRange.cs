﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public enum DateRange
    {
        @Today = 0,
        @Yesterday,
        @This_Week,
        @Last_Week,
        @This_Month,
        @Last_Month,
        @Days_30,
        @Days_60,
        @Days_90,
        @Quarter_To_Date,
        @Last_Quarter,
        @Year_To_Date,
        @Last_Year,
        @All_Transactions,
        @Specific_Date_Range,
        @Tomorrow
    }
}
