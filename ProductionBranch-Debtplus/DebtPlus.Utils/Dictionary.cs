﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public class Dictionary
    {
        public Dictionary() : base()
        {
            aff = String.Empty;
            dic = String.Empty;
            Format = "OpenOffice";
            Enabled = false;
            Culture = System.Globalization.CultureInfo.CurrentCulture.Name;
        }

        public string aff { get; set; }
        public string dic { get; set; }
        public string Format { get; set; }
        public Boolean Enabled { get; set; }
        public string Culture { get; set; }

        public System.IO.Stream GetDICStream(string pathName)
        {
            String fname = System.IO.Path.IsPathRooted(dic) ? dic : System.IO.Path.Combine(pathName, dic);
            return new System.IO.FileStream(fname, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        }

        public System.IO.Stream GetAFFStream(string pathName)
        {
            String fname = System.IO.Path.IsPathRooted(aff) ? aff : System.IO.Path.Combine(pathName, aff);
            return new System.IO.FileStream(fname, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        }

        public static string defaultCulture (string fname)
        {
            return System.IO.Path.GetFileNameWithoutExtension(fname).Replace("_", "-");
        }
    }
}
