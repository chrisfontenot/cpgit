﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class PhoneNumber
    {
        /// <summary>
        /// Extract the telephone number from the telephone number string
        /// </summary>
        public static String ExtractPhoneNumber(Object Input)
        {
            String answer = String.Empty;

            if (Input != null && !object.Equals(Input, System.DBNull.Value))
            {

                // Find the result from the input string
                answer = Convert.ToString(Input).Trim();

                // Split the phone number at the extension boundary
                Regex rx = new Regex(@"(.*){Extension|Ext\.|Ext|x\.|x}(.*)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
                String[] Items = rx.Split(answer);
                if (Items.GetUpperBound(0) >= 0)
                {
                    answer = Items[0].Trim();
                }

                // Remove all but the digits and ensure that the input is the proper size
                answer = Strings.DigitsOnly(answer).PadLeft(10, '0');
            }

            return answer;
        }

        /// <summary>
        /// Extract the extension from the telephone number string
        /// </summary>
        public static String ExtractExtension(Object Input)
        {
             String answer = String.Empty;

            if (Input != null && !object.Equals(Input, System.DBNull.Value))
{
                answer = Convert.ToString(Input).Trim();

                // Split the phone number at the extension boundary
                Regex rx = new Regex(@"(.*){Extension|Ext\.|Ext|x\.|x}(.*)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
                 String[] Items = rx.Split(answer);
                if (Items.GetUpperBound(0) > 0)
{
                    answer = Items[Items.GetUpperBound(0)].Trim();
                }

                // Remove all but the digits and ensure that the input is the proper size
                answer = Strings.DigitsOnly(answer);
            }

            return answer;
        }

        /// <summary>
        /// Format the US Telephone numbers
        /// </summary>
        public static String FormatPhoneNumber(Object Input)
        {
            String answer = String.Empty;

            if (Input != null && !object.Equals(Input, System.DBNull.Value))
            {
                String strValue = Convert.ToString(Input);
                strValue = Strings.DigitsOnly(strValue);

                // If less than 7 digits, pad it with trailing zeros
                if (strValue.Length < 7)
                {
                    strValue = strValue.PadRight(7, '0');

                    // If less than 10 digits, pad it left with leading zeros
                }
                else if (strValue.Length < 10)
                {
                    strValue = strValue.PadLeft(10, '0');
                }

                // Format the phone number based upon the length
                switch (strValue.Length)
                {
                    case 7:
                        if (strValue != "0000000")
                        {
                            answer = strValue.Substring(0, 3) + "-" + strValue.Substring(3, 4);
                        }
                        break;

                    case 10:
                        if (strValue != "0000000000")
                        {
                            answer = "(" + strValue.Substring(0, 3) + ") " + strValue.Substring(3, 3) + "-" + strValue.Substring(6, 4);
                        }
                        break;

                    default:
                        answer = strValue;
                        break;
                }
            }
            return answer;
        }

        /// <summary>
        /// Format the extension field
        /// </summary>
        public static String FormatExtension(Object Input)
        {
            String answer = String.Empty;
            if (Input != null && !object.Equals(Input, System.DBNull.Value))
            {
                answer = Convert.ToString(Input).Trim();
            }
            if (answer != String.Empty)
            {
                answer = "Ext. " + answer;
            }
            return answer;
        }

        /// <summary>
        /// Format the telephone number and extension fields
        /// </summary>
        public static String FormatPhoneAndExtension(Object Input)
        {
            // Find the pieces and format each part separately
            String part1 = FormatPhoneNumber(ExtractPhoneNumber(Input));
            String part2 = FormatExtension(ExtractExtension(Input));

            // Combine the two parts together with a space separator as needed
            if (part1 != String.Empty && part2 != String.Empty)
            {
                return String.Format("{0} {1}", part1, part2);
            }
            else
            {
                return part1 + part2;
            }
        }

        /// <summary>
        /// Interface for grid events to format telephone numbers
        /// </summary>
        // [XmlType(namespace="urn:PhoneNumberCustomFormatter")]
        // [XmlRoot(namespace="urn:PhoneNumberCustomFormatter")]
        public class CustomFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) //  Implements ICustomFormatter.Format
            {
                return FormatPhoneNumber(arg);
            }
        }
    }
}

