﻿namespace DebtPlus.Utils.Format
{
    public static class Names
    {
        /// <summary>
        /// Format the person's name from the component pieces
        /// </summary>
        [System.Obsolete("Use DebtPlus.LINQ.Name.FormatNormalName", true)]
        public static System.String FormatNormalName(System.Object prefix, System.Object first, System.Object middle, System.Object last, System.Object suffix)
        {
            return DebtPlus.LINQ.Name.FormatNormalName(prefix, first, middle, last, suffix);
        }

        /// <summary>
        /// Format the name in last name first order
        /// </summary>
        [System.Obsolete("Use DebtPlus.LINQ.Name.FormatReverseName", true)]
        public static System.String FormatReverseName(System.Object prefix, System.Object first, System.Object middle, System.Object last, System.Object suffix)
        {
            return DebtPlus.LINQ.Name.FormatReverseName(prefix, first, middle, last, suffix);
        }
    }
}
