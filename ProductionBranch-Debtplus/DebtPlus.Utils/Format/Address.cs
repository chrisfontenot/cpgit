﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using DebtPlus.Utils;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;
using DebtPlus.LINQ;

namespace DebtPlus.Utils.Format
{
    public static class Address
    {
        /// <summary>
        /// Generic routine to convert a list of items to a string address block
        /// </summary>
        /// <param name="Components">List of the input strings</param>
        /// <returns></returns>
        public static string AddressBlock(IEnumerable Components)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.Collections.IEnumerator ie = Components.GetEnumerator();

            while (ie.MoveNext())
            {
                // Find the next item. Ignore NULL strings.
                object item = ie.Current;
                if (item != null && !object.Equals(item, System.DBNull.Value))
                {
                    // Ignore empty strings. They would just be a blank line.
                    string strItem = Convert.ToString(item).Trim();
                    if (!string.IsNullOrEmpty(strItem))
                    {
                        sb.AppendFormat("{0}{1}", Environment.NewLine, strItem);
                    }
                }
            }

            // Remove the leading CR/LF that was inserted as the items were added.
            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            // The resulting string is the functions' value.
            return sb.ToString();
        }

        /// <summary>
        /// Convert the input string array to an address block text
        /// </summary>
        /// <param name="Components"></param>
        /// <returns></returns>
        public static string AddressBlock(string[] Components)
        {
            return AddressBlock(Components.ToList<string>());
        }

        /// <summary>
        /// Convert the input strings to an address block
        /// </summary>
        /// <param name="Line1"></param>
        /// <param name="Line2"></param>
        /// <param name="Line3"></param>
        /// <returns></returns>
        public static string AddressBlock(string Line1, string Line2, string Line3)
        {
            return AddressBlock(new string[] { Line1, Line2, Line3 });
        }

        /// <summary>
        /// Convert the input strings to an address block
        /// </summary>
        /// <param name="Line1"></param>
        /// <param name="Line2"></param>
        /// <param name="Line3"></param>
        /// <param name="Line4"></param>
        /// <returns></returns>
        public static string AddressBlock(string Line1, string Line2, string Line3, string Line4)
        {
            return AddressBlock(new string[] { Line1, Line2, Line3, Line4 });
        }

        /// <summary>
        /// Convert the input strings to an address block
        /// </summary>
        /// <param name="Line1"></param>
        /// <param name="Line2"></param>
        /// <param name="Line3"></param>
        /// <param name="Line4"></param>
        /// <param name="Line5"></param>
        /// <returns></returns>
        public static string AddressBlock(string Line1, string Line2, string Line3, string Line4, string Line5)
        {
            return AddressBlock(new string[] { Line1, Line2, Line3, Line4, Line5 });
        }

        /// <summary>
        /// Convert the input strings to an address block
        /// </summary>
        /// <param name="Line1"></param>
        /// <param name="Line2"></param>
        /// <param name="Line3"></param>
        /// <param name="Line4"></param>
        /// <param name="Line5"></param>
        /// <param name="Line6"></param>
        /// <returns></returns>
        public static string AddressBlock(string Line1, string Line2, string Line3, string Line4, string Line5, string Line6)
        {
            return AddressBlock(new string[] { Line1, Line2, Line3, Line4, Line5, Line6 });
        }

        /// <summary>
        /// Generate the first line of an address from the component pieces
        /// </summary>
        /// <param name="house"></param>
        /// <param name="leadingDirection"></param>
        /// <param name="street"></param>
        /// <param name="suffix"></param>
        /// <param name="modifier"></param>
        /// <param name="modifier_value"></param>
        /// <returns></returns>
        public static string AddressLine1(string house, string leadingDirection, string street, string suffix, string modifier, string modifier_value)
        {
            return AddressLine1(house, leadingDirection, street, suffix, modifier, modifier_value, string.Empty);
        }

        /// <summary>
        /// Generate the first line of an address from the component pieces
        /// </summary>
        /// <param name="house"></param>
        /// <param name="leadingDirection"></param>
        /// <param name="street"></param>
        /// <param name="suffix"></param>
        /// <param name="modifier"></param>
        /// <param name="modifier_value"></param>
        /// <param name="trailingDirection"></param>
        /// <returns></returns>
        public static string AddressLine1(string house, string leadingDirection, string street, string suffix, string modifier, string modifier_value, string trailingDirection)
        {
            StringBuilder sb = new StringBuilder();

            // Do not allow the modifier to exist without a value if a value is required
            if ((modifier == "APT" || modifier == "STE") && string.IsNullOrEmpty(modifier_value))
            {
                modifier = null;
            }

            // Just merge the component pieces together in the proper sequence to generate the address information
            foreach (string strItem in new string[] { house, leadingDirection, street, suffix, modifier, modifier_value, trailingDirection })
            {
                if (!string.IsNullOrEmpty(strItem))
                {
                    sb.AppendFormat(" {0}", strItem);
                }
            }

            return sb.ToString().Trim();
        }

        /// <summary>
        /// Format the last portion of the address line to a string.
        /// </summary>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <returns></returns>
        public static string FormatCityStateZip(string city, Int32 state, string postalCode)
        {
            if (state <= 0)
            {
                return FormatCityStateZip(city, (DebtPlus.LINQ.state) null, postalCode);
            }

            return FormatCityStateZip(city, DebtPlus.LINQ.Cache.state.StateRecordById(state), postalCode);
        }

        /// <summary>
        /// Format the last portion of the address line to a string.
        /// </summary>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <returns></returns>
        public static string FormatCityStateZip(string city, Int32? state, string postalCode)
        {
            DebtPlus.LINQ.state currentState = state.HasValue ? DebtPlus.LINQ.Cache.state.StateRecordById(state.Value) : null;
            return FormatCityStateZip(city, currentState, postalCode);
        }

        // Convert the address information to the text block.
        // This routine is passed the pointer to the state. It will not look it up. That is left to the caller.
        private static string FormatCityStateZip(string city, DebtPlus.LINQ.state currentState, string postalCode)
        {
            if (currentState != null)
            {
                string stateName = currentState.MailingCode.PadRight(2).Substring(0, 2);
                Int32 country = currentState.Country;
                string AddressFormat = currentState.AddressFormat;

                // If the entry is for an US address then format the postal-code as a ZIP+4 code
                if (currentState.USAFormat > 0)
                {
                    postalCode = PostalCode.FormatPostalCode(postalCode);
                }

                // Retrieve the country name if there is one.
                string CountryName = string.Empty;
                if (country >= 0)
                {
                    DebtPlus.LINQ.country currentCountry = DebtPlus.LINQ.Cache.country.countryRecordById(country);
                    if (currentCountry != null && !string.IsNullOrEmpty(currentCountry.description))
                    {
                        CountryName = currentCountry.description.Trim();
                    }
                }

                // The address format string may not be empty. If so, default it.
                if (string.IsNullOrEmpty(AddressFormat))
                {
                    AddressFormat = "{0} {1}  {2}";
                }

                return string.Format(AddressFormat, city, stateName, postalCode, CountryName).Replace(@"\r", "\r").Replace(@"\n", "\n");
            }

            return string.Format("{0} {1}", city, postalCode);
        }

        /// <summary>
        /// Format the address information in the indicated record on the database
        /// </summary>
        /// <param name="AddressId">The ID of the address record</param>
        /// <returns></returns>
        public static string FormatAddress(Int32 AddressId)
        {
            using (var dc = new BusinessContext())
            {
                DebtPlus.LINQ.address record = (from a in dc.addresses where a.Id == AddressId select a).FirstOrDefault();
                if (record != null)
                {
                    return AddressBlock(
                                record.creditor_prefix_1,
                                record.creditor_prefix_2,
                                AddressLine1(record.house, record.direction, record.street, record.suffix, record.modifier, record.modifier_value),
                                record.address_line_2,
                                record.address_line_3,
                                FormatCityStateZip(record.city, record.state, record.PostalCode));
                }
            }
            return string.Empty;
        }
    }
}
