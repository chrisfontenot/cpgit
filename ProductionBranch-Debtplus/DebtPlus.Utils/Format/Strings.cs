﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class Strings
    {
        private static Regex IsRTF_rx = new Regex(@"^\s*\{\\rtf.*\}[^\u0021-\u007E]*$", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
        /// <summary>
        /// Determine if the input string is valid RTF text
        /// </summary>
        /// <param name="InputString">Input string to be scanned.</param>
        /// <returns>TRUE if the item looks like RTF, FALSE otherwise</returns>
        /// <remarks>This function uses regular expressions to trim the non-digit characters from the input.</remarks>
        public static Boolean IsRTF(String InputString)
        {
            if (!string.IsNullOrEmpty(InputString))
            {
                return IsRTF_rx.IsMatch(InputString);
            }
            return false;
        }

        private static Regex IsHTML_rx = new Regex(@"^\s*(\<!DOCTYPE.*\>\s*)?\<html\>.*\</html\>[^\u0021-\u007E]*$", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
        /// <summary>
        /// Determine if the input string is valid HTML text
        /// </summary>
        /// <param name="InputString">Input string to be scanned.</param>
        /// <returns>TRUE if the item looks like HTML, FALSE otherwise</returns>
        /// <remarks>This function uses regular expressions to trim the non-digit characters from the input.</remarks>
        public static Boolean IsHTML(String InputString)
        {
            if (!string.IsNullOrEmpty(InputString))
            {
                return IsHTML_rx.IsMatch(InputString);
            }
            return false;
        }

        /// <summary>
        /// Remove all of the non-digit characters from the input string
        /// </summary>
        /// <param name="InputString">Input string to be processed.</param>
        /// <returns>A string equal to InputString but with only digits</returns>
        /// <remarks>This function uses regular expressions to trim the non-digit characters from the input.</remarks>
        public static String DigitsOnly(String InputString)
        {
            if (!string.IsNullOrEmpty(InputString))
            {
                return Regex.Replace(InputString, @"\D", String.Empty);
            }
            return string.Empty;
        }

        /// <summary>
        /// Convert the input value to whole dollars. The cents are removed.
        /// </summary>
        /// <param name="curValue">Decimal value to be converted. Fractional dollars are removed from this field.</param>
        /// <returns>A 64 bit int value representing the dollar figure</returns>
        public static Int64 FormattedWholeDollars(Decimal curValue)
        {
            Int64 answer = Convert.ToInt64(DebtPlus.Utils.Math.Truncate(curValue));
            return answer;
        }

        /// <summary>
        /// Convert the input value to an implied decimal point of two places. The result is a Int64 value that is the number of cents in the original input decimal.
        /// </summary>
        /// <param name="curValue">Value to be converted.</param>
        /// <returns>A 64 bit int value representing the cents figure</returns>
        public static Int64 FormattedMoney(Decimal curValue)
        {
            return Convert.ToInt64(DebtPlus.Utils.Math.Truncate(curValue * 100M));
        }

        /// <summary>
        /// Convert the input string to a suitable string for HTML.
        /// </summary>
        /// <param name="input">The ASCII text string.</param>
        /// <returns>The HTML text equivalent</returns>
        public static string toHTML(string input)
        {
            return toHTML(input, false);
        }

        /// <summary>
        /// Convert the input string to a suitable string for HTML.
        /// </summary>
        /// <param name="input">The ASCII text string.</param>
        /// <param name="UseParagraphBreaks">TRUE if we use paragraphs as indicated for lines. FALSE to simply break the line.</param>
        /// <returns>The HTML text equivalent</returns>
        public static string toHTML(string input, bool UseParagraphBreaks)
        {
            return toHTML(input, UseParagraphBreaks ? "</p><p>" : "<br/>");
        }

        /// <summary>
        /// Convert the input string to a suitable string for HTML.
        /// </summary>
        /// <param name="input">The ASCII text string.</param>
        /// <param name="ParagraphSequence">String to end and start a new paragraph break on cr/lf</param>
        /// <returns>The HTML text equivalent</returns>
        public static string toHTML(string input, string ParagraphSequence)
        {
            // Load the text into the working buffer
            var sb = new System.Text.StringBuilder(input);

            // Replace the characters that cause markup problems. Must do the "&" first!!!
            sb.Replace("&",  "&amp;");
            sb.Replace("<",  "&lt;");
            sb.Replace(">",  "&gt;");
            sb.Replace("\"", "&quot");

            // Line breaks are paragraphs breaks in the line
            sb.Replace("\r\n", ParagraphSequence);
            sb.Replace("\r",   "<br/>");
            sb.Replace("\n",   "<br/>");

            // Wrap a paragraph marker around the text lines
            sb.Insert(0, "<p>");
            sb.Append("</p>");

            // The resulting string is the HTML text to be used.
            return sb.ToString();
        }
    }
}
