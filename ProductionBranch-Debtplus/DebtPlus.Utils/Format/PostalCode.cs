﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Utils;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class PostalCode
    {
        /// <summary>
        /// Format US Postal code numbers
        /// </summary>
        public static String FormatPostalCode(Object Input)
        {
            String answer = String.Empty;

            if (Input != null && !object.Equals(Input, System.DBNull.Value))
            {
                answer = Convert.ToString(Input).Trim();

                // Look for a USA ZipCode
                if ((new Regex(@"\d{5,9}")).IsMatch(answer))
                {
                    String Postalcode = answer.PadRight(9, '0');
                    if (Postalcode == "000000000")
                    {
                        return String.Empty;
                    }
                    if (Postalcode.EndsWith("0000"))
                    {
                        answer = Postalcode.Substring(0, 5);
                    }
                    else
                    {
                        answer = Postalcode.Substring(0, 5) + "-" + Postalcode.Substring(5, 4);
                    }
                }
            }
            return answer;
        }

        /// <summary>
        /// Format US Postal code numbers
        /// </summary>
        // [XmlType(namespace="urn:PostalCodeCustomFormatter")]
        // [XmlRoot(namespace="urn:PostalCodeCustomFormatter")]
        public class CustomFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) //  Implements ICustomFormatter.Format
            {
                return FormatPostalCode(arg);
            }
        }
    }
}
