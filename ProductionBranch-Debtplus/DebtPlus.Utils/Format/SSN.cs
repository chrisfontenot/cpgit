﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Utils;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class SSN
    {
        /// <summary>
        /// Format the input social security number to a display string
        /// </summary>
        /// <param name="SSN">The input string value for the social security number</param>
        /// <returns>A Number formatted for displaying the social security number</returns>
        public static String FormatSSN(Object SSN)
        {
             String answer = String.Empty;

            if (SSN != null && !object.Equals(SSN, System.DBNull.Value))
            {
                String NewString = Strings.DigitsOnly(Convert.ToString(SSN)).PadLeft(9, '0');
                if (NewString != "000000000")
                {
                    answer = NewString.Substring(0, 3) + "-" + NewString.Substring(3, 2) + "-" + NewString.Substring(5, 4);
                }
            }
            return answer;
        }

        /// <summary>
        /// This method is used by the formatter to mask the first 5 characters of a SSN number
        /// </summary>
        /// <param name="SSN"></param>
        /// <returns></returns>
        public static String FormatMaskedSSN(Object SSN)
        {
            String answer = String.Empty;

            if (SSN != null && !object.Equals(SSN, System.DBNull.Value))
            {
                String NewString = Strings.DigitsOnly(Convert.ToString(SSN)).PadLeft(9, '0');
                if (NewString != "000000000")
                {
                    answer = "xxx-xx-" + NewString.Substring(5, 4);
                }
            }
            return answer;
        }

        // [XmlType(namespace="urn:SSNCustomFormatter")]
        // [XmlRoot(namespace="urn:SSNCustomFormatter")]
        public class CustomFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) // Implements ICustomFormatter.Format
            {
                return FormatSSN(arg);
            }
        }

        public class MaskFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) // Implements ICustomFormatter.Format
            {
                return FormatMaskedSSN(arg);
            }
        }
    }
}
