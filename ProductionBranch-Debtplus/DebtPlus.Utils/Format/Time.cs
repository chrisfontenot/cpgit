﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class Time
    {
        /// <summary>
        /// Format the number of minutes into a suitable display form
        /// </summary>
        /// <returns>A string with hours and minutes</returns>
        public static String HoursAndMinutes(Object Value)
        {
            String txt = String.Empty;

            // Convert the input to an Int32
            Int32 ItemValue = 0;
            if (Value != null && !System.Object.Equals(Value, System.DBNull.Value))
            {
                try
                {
                    ItemValue = Convert.ToInt32(Value, CultureInfo.InvariantCulture);
                }
                catch { }
            }

            // Find the hours and minutes
            if (ItemValue > 0)
            {
                Int32 Minutes;
                Int32 Hours = System.Math.DivRem(ItemValue, 60, out Minutes);

                String HoursString = String.Empty;
                if (Hours > 0)
                {
                    HoursString = String.Format("{0:f0} hour", Hours);
                    if (Hours != 1)
                    {
                        HoursString += "s";
                    }
                }

                String MinutesString = String.Empty;
                if (Minutes > 0)
                {
                    MinutesString = String.Format("{0:f0} minute", Minutes);
                    if (Minutes != 1)
                    {
                        MinutesString += "s";
                    }
                }

                if (HoursString != String.Empty && MinutesString != String.Empty)
                {
                    txt = String.Format("{0} and {1}", HoursString, MinutesString);
                }
                else
                {
                    txt = HoursString + MinutesString;
                }
            }

            return txt;
        }
    }

    /// <summary>
    /// Allow for formatting of a percent between 1.0 and 100.0
    /// </summary>
    // [XmlType(namespace="urn:PercentCustomFormatter")]
    // [XmlRoot(namespace="urn:PercentCustomFormatter")]
    public class CustomFormatter : IFormatProvider, ICustomFormatter
    {
        public String Format(String format1, Object arg, IFormatProvider formatProvider) //  Implements ICustomFormatter.Format
        {
            String answer = String.Empty;
            if (arg != null && !System.Object.Equals(arg, System.DBNull.Value))
            {
                Double V = Convert.ToDouble(arg);

                // Make the formatting specification legal for use by string.format
                String FmtString = format1;
                if (!FmtString.StartsWith("{"))
                {
                    FmtString = "{0:" + FmtString + "}";
                }

                // Reduce the value by two places to the right if the format calls for it
                if (FmtString.StartsWith("{0:P"))
                {
                    if (V < 0.0D)
                    {
                        V = (0.0D - ((0.0D - V) / 100.0D));
                    }
                    else
                    {
                        V /= 100.0D;
                    }
                }

                // return the value as a string
                answer = String.Format(FmtString, V);
            }

            return answer;
        }

        public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
        {
            return this;
        }
    }
}
