﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils.Format
{
    static partial class toWords
    {
        private const System.Int32 MAX_LEN = 80;
        private const System.Int32 CENTS_LEN = 19;
        private const System.Int32 MAX_DOLLARS = MAX_LEN - CENTS_LEN;

        /// <summary>
        /// Routine to split the input figure into the corresponding dollar and cents integer amounts.
        /// </summary>
        /// <param name="Amount">Dollar amount to be converted</param>
        /// <param name="Dollars">Whole dollars figure</param>
        /// <param name="Cents">Whole cents figure</param>
        private static void SplitAmount(decimal Amount, out System.Int64 Dollars, out System.Int32 Cents)
        {
            // Split the amount into whole dollars and cents
            string TempString = System.String.Format("{0:00000000000000000000000.00}", System.Math.Floor(Amount * 100m) / 100m);

            // This is a bit of a hack. I would like to use "floor" but it does not work here. So, do it the hard way.
            string[] Parts = TempString.Split('.');
            Dollars = System.Int64.Parse(Parts[0]);
            Cents = System.Int32.Parse(Parts[1]);
        }

        public static string ToWords(decimal Amount)
        {
            return ToWords(Amount, MAX_LEN);
        }

        /// <summary>
        /// Convert a decimal number to a suitable string for printing on a check
        /// </summary>
        /// <param name="Amount">Dollar amount to be converted</param>
        /// <param name="MaxLength">Maximum length allowed for the resulting string</param>
        /// <returns>If the string is too long, a simpler format of just dollars and cents is used.</returns>
        public static string ToWords(decimal Amount, System.Int32 MaxLength)
        {
            string answer = null;
            string CentsString = null;

            // Split the number into dollars and cents.
            System.Int64 Dollars;
            System.Int32 Cents;
            SplitAmount(Amount, out Dollars, out Cents);

            // Assume standard formatting for the cents string
            CentsString = System.String.Format(" AND {0:00}/100 DOLLARS", Cents);

            // If there are no dollars then use the term "zero" otherwise, convert it to a string
            if (Dollars == 0L)
            {
                answer = "ZERO" + CentsString;
            }
            else
            {
                answer = Format_Text_Numbers(Dollars) + CentsString;
            }

            // If the resulting string is too long then shorten it.
            if (MaxLength > 0 && answer.Length > MaxLength)
            {
                answer = string.Format("***{0:f0}*** DOLLARS AND ***{1:00}*** CENTS", Dollars, Cents);
            }

            return answer;
        }

        /// <summary>
        /// This class is used to translate specific values to their corresponding text string
        /// </summary>
        private class numberList
        {
            public System.Int64 Value { get; set; }
            public string text { get; set; }
            public numberList(System.Int64 Value, string text)
            {
                this.Value = Value;
                this.text = text;
            }
        }

        // This is a list of the specific values to be translated
        private static System.Collections.Generic.List<numberList> englishNumberCollection = new System.Collections.Generic.List<numberList>(new numberList[]
        {
            new numberList(0L,  ""),
            new numberList(1L,  "ONE"),
            new numberList(2L,  "TWO"),
            new numberList(3L,  "THREE"),
            new numberList(4L,  "FOUR"),
            new numberList(5L,  "FIVE"),
            new numberList(6L,  "SIX"),
            new numberList(7L,  "SEVEN"),
            new numberList(8L,  "EIGHT"),
            new numberList(9L,  "NINE"),
            new numberList(10L, "TEN"),
            new numberList(11L, "ELEVEN"),
            new numberList(12L, "TWELVE"),
            new numberList(13L, "THIRTEEN"),
            new numberList(14L, "FOURTEEN"),
            new numberList(15L, "FIFTEEN"),
            new numberList(16L, "SIXTEEN"),
            new numberList(17L, "SEVENTEEN"),
            new numberList(18L, "EIGHTEEN"),
            new numberList(19L, "NINETEEN"),
            new numberList(20L, "TWENTY"),
            new numberList(30L, "THIRTY"),
            new numberList(40L, "FORTY"),
            new numberList(50L, "FIFTY"),
            new numberList(60L, "SIXTY"),
            new numberList(70L, "SEVENTY"),
            new numberList(80L, "EIGHTY"),
            new numberList(90L, "NINETY")
        });

        /// <summary>
        /// Return the text for the corresponding input number.
        /// </summary>
        /// <param name="Amount">The whole amount. Fractions are handled separately.</param>
        /// <returns></returns>
        private static string Format_Text_Numbers(System.Int64 Amount)
        {
            // Look for a specific amount and use the corresponding text string
            var q = englishNumberCollection.Find(s => s.Value == Amount);
            if (q != null)
            {
                return q.text;
            }

            string Result = null;
            System.Int64 Quotient = 0L;
            System.Int64 Remainder = 0;

            // Handle amounts greater than 1 million dollars
            if (Amount >= 1000000L)
            {
                Quotient = System.Math.DivRem(Amount, 1000000L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " MILLION";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} MILLION", Quotient);
                }

                return Result.Trim();
            }

            // Handle amounts greater than 1 thousand dollars
            if (Amount >= 1000L)
            {
                Quotient = System.Math.DivRem(Amount, 1000L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " THOUSAND";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} THOUSAND", Quotient);
                }
                return Result.Trim();
            }

            // Handle amounts greater than 100 dollars
            if (Amount >= 100L)
            {
                Quotient = System.Math.DivRem(Amount, 100L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " HUNDRED";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} HUNDRED", Quotient);
                }
                return Result.Trim();
            }

            // Handle amounts less than 100 dollars (but greater than 20 dollars)
            Quotient = System.Math.DivRem(Amount, 10L, out Remainder);
            return string.Format("{0} {1}", Format_Text_Numbers(Quotient * 10L), Format_Text_Numbers(Remainder)).Trim();
        }
    }
}
