﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils.Format
{
    static partial class toWords
    {
        /// <summary>
        /// Local translation table for numbers to Spanish words
        /// </summary>
        private static System.Collections.Generic.List<numberList> spanishNumberCollection = new System.Collections.Generic.List<numberList>(new numberList[]
        {
            new numberList(1L,  "primaria"),
            new numberList(2L,  "segundo"),
            new numberList(3L,  "tercero"),
            new numberList(4L,  "cuarto"),
            new numberList(5L,  "quinto"),
            new numberList(6L,  "sexto"),
            new numberList(7L,  "séptimo"),
            new numberList(8L,  "octavo"),
            new numberList(9L,  "noveno"),
            new numberList(10L, "décimo"),
            new numberList(11L, "undecimo"),
            new numberList(12L, "duodecimo"),
            new numberList(13L, "decimotercero"),
            new numberList(14L, "decimocuarto"),
            new numberList(15L, "decimoquinto"),
            new numberList(16L, "decimosexto"),
            new numberList(17L, "decimoséptimo"),
            new numberList(18L, "decimoctavo"),
            new numberList(19L, "decimonoveno"),
            new numberList(20L, "vigésimo"),
            new numberList(30L, "trigésimo")
        });

        /// <summary>
        /// Local routine to translate the input number to a string
        /// </summary>
        /// <param name="Count">The value to be translated</param>
        /// <returns></returns>
        public static string SpanishOtherWord(System.Int64 Count)
        {
            // If the value is greater than 39 then quit
            if (Count > 39)
            {
                return string.Empty;
            }

            // Look in the table for a match to the desired number.
            var q = englishNumberCollection.Find(s => s.Value == Count);
            if (q != null)
            {
                return q.text;
            }

            // If not found then reduce it to tens and units to try again.
            System.Int64 Quotient = 0L;
            System.Int64 Remainder = 0;

            // Handle amounts greater than 1 million dollars
            Quotient = System.Math.DivRem(Count, 10L, out Remainder);
            return string.Format("{0} {1}", SpanishOther((int)Quotient), SpanishOther((int)Remainder));
        }

        /// <summary>
        /// Convert the input value to a string suitable for referring to other budget categories.
        /// </summary>
        /// <param name="Count"></param>
        /// <returns></returns>
        public static string SpanishOther(System.Int64 Count)
        {
            // Find the corresponding value for the counter
            string answer = SpanishOtherWord(Count);

            // If the value is zero then just return "other"
            if (answer != string.Empty)
            {
                // Only "5" is plural. Make it so.
                if (Count == 5)
                {
                    return string.Format("{0} otras", answer);
                }
            }

            // The others are singular.
            return string.Format("{0} otra", answer).Trim();
        }
    }
}
