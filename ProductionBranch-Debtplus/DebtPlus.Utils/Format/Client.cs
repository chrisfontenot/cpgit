﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.Utils;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class Client
    {
        /// <summary>
        /// Format the client ID
        /// </summary>
        public static String FormatClientID(Object ClientObject)
        {
            String answer = String.Empty;
            Int64 IntValue;

            if (ClientObject != null)
            {
                // For some reasons, the client is passed in as a string. Format the value into a number and return the resulting string value.
                if (ClientObject is String)
                {
                    if (Int64.TryParse(Convert.ToString(ClientObject), NumberStyles.Integer, CultureInfo.InvariantCulture, out IntValue))
                    {
                        answer = String.Format("{0:0000000}", IntValue);
                    }

                }
                else
                {

                    // Up-shift int32 values to int64
                    if (ClientObject is Int32)
                    {
                        IntValue = Convert.ToInt64(ClientObject);

                        // This should not happen, but if it does, allow for a 16 bit Int32 to be a client number.
                    }
                    else if (ClientObject is short)
                    {
                        IntValue = Convert.ToInt64(Convert.ToUInt32(ClientObject));

                        // Convert int64 items to their proper format
                    }
                    else if (ClientObject is Int64)
                    {
                        IntValue = Convert.ToInt64(ClientObject);
                    }
                    else
                    {

                        // All others return an empty string since they are not portable here.
                        // (Which client is "True" or which one is "False"?)
                        return answer;
                    }

                    // The value is converted to a 64 bit Int32. Format it as a string now.
                    if (IntValue >= 0)
                    {
                        if (IntValue < 1000000)
                        {
                            answer = IntValue.ToString("0000000");
                        }
                        else
                        {
                            answer = IntValue.ToString();
                        }
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Format the client ID in bound controls
        /// </summary>
        // [XmlType(namespace="urn:ClientCustomFormatter")];
        // [XmlRoot(namespace="urn:ClientCustomFormatter")];
        public class CustomFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) //  Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) //  Implements ICustomFormatter.Format
            {
                return String.Format("{0:0000000}", arg);
            }
        }
    }
}
