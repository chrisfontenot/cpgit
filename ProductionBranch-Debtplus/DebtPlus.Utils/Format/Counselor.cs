﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DebtPlus.Utils.Format
{
    public static class Counselor
    {
        /// <summary>
        /// Format the counselor name
        /// </summary>
        public static String FormatCounselor(Object CounselorName)
        {
            String answer = String.Empty;

            if (CounselorName != null && !object.Equals(CounselorName, System.DBNull.Value))
            {
                answer = Convert.ToString(CounselorName).Trim();
                Int32 SlashMaker = answer.LastIndexOfAny(new Char[] { '/', '\\' });
                if (SlashMaker > 0)
                {
                    answer = answer.Substring(SlashMaker + 1);
                }
            }

            return answer;
        }

        /// <summary>
        /// Interface for the counselor name to the grid formatter
        /// </summary>
        // [XmlType(namespace="urn:CounselorCustomFormatter")]
        // [XmlRoot(namespace="urn:CounselorCustomFormatter")]
        public class CustomFormatter : IFormatProvider, ICustomFormatter
        {
            public Object GetFormat(Type formatType) // Implements IFormatProvider.GetFormat
            {
                return this;
            }

            public String Format(String FormatString, Object arg, IFormatProvider formatProvider) //  Implements ICustomFormatter.Format
            {
                return FormatCounselor(arg);
            }
        }
    }
}
