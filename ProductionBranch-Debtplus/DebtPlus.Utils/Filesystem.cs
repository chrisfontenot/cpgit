﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class Filesystem
    {
        /// <summary>
        /// Remove a directory from the system
        /// </summary>
        /// <param name="Path"></param>
        /// <remarks></remarks>
        public static String RemoveDirectory(String Path)
        {
            try
            {
                System.IO.Directory.Delete(Path, true);
                return string.Empty;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
