﻿public static class ExtensionMethods
{
    /// <summary>
    /// Fold the first letter to upper case
    /// </summary>
    /// <param name="value">The string to be converted</param>
    /// <returns>The converted string</returns>
    public static string UppercaseFirstLetter(this string value)
    {
	    if (value.Length > 0)
	    {
	        char[] array = value.ToCharArray();
	        array[0] = char.ToUpper(array[0]);
	        return new string(array);
	    }
	    return value;
    }

    /// <summary>
    /// Return the leftmost characters from the string
    /// </summary>
    /// <param name="value">String to be extracted</param>
    /// <param name="count">Number of characters desired</param>
    /// <returns>The resulting string</returns>
    public static string Left(this string value, int count)
    {
        if (value == null)
        {
            return null;
        }
        if (count < 1)
        {
            throw new System.ArgumentOutOfRangeException("count must be positive");
        }
        if (count > value.Length)
        {
            return value;
        }
        return value.Substring(0, count);
    }

    /// <summary>
    /// Return the right most characters from the string
    /// </summary>
    /// <param name="value">String to be extracted</param>
    /// <param name="count">Number of characters desired</param>
    /// <returns>The resulting string</returns>
    public static string Right(this string value, int count)
    {
        if (value == null)
        {
            return null;
        }
        if (count < 1)
        {
            throw new System.ArgumentOutOfRangeException("count must be positive");
        }
        if (count > value.Length)
        {
            return value;
        }
        return value.Substring(value.Length - count);
    }
}
