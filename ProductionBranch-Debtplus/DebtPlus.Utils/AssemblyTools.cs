﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class AssemblyTools
    {
        public static String[] GetScriptReferences()
        {
            return GetAllSiblingAssemblies();
        }

        public static System.String[] GetAllSiblingAssemblies()
        {
            System.Collections.Generic.List<string> items = new System.Collections.Generic.List<string>();

            // Load the calling assembly as a reference
            String executingAssemblyName = System.Reflection.Assembly.GetExecutingAssembly().Location.ToString();
            items.Add(executingAssemblyName);

            String executingAssemblyFolder = System.IO.Path.GetDirectoryName(executingAssemblyName);
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(executingAssemblyFolder);
            System.IO.FileInfo[] assemblies = di.GetFiles("*.dll", System.IO.SearchOption.TopDirectoryOnly);

            items.AddRange(from asm in assemblies select asm.FullName);

            return items.ToArray();
        }
    }
}
