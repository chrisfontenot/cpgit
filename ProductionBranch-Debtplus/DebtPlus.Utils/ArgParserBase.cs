﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public abstract class ArgParserBase : DebtPlus.Interfaces.IArgParser
    {
        /// <summary>
        ///     //  File:      ArgParser.vb
        ///     //
        ///     //  Summary:   Reusable class for parsing command-line arguments.
        ///     //
        ///     //---------------------------------------------------------------------
        ///     //  This file is part of the Microsoft .NET Framework SDK Code Samples.
        ///     //
        ///     //  Copyright (C) Microsoft Corporation.  All rights reserved.
        ///     //
        ///     //This source code is intended only as a supplement to Microsoft
        ///     //Development Tools and/or on-line documentation.  See these other
        ///     //materials for detailed information regarding Microsoft code samples.
        ///     //
        ///     //THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
        ///     //KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
        ///     //IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
        ///     //PARTICULAR PURPOSE.
        /// </summary>
        private readonly string[] switchChars;             // For example: "/", "-"
        private readonly string[] switchSymbols;           // Switch character(s)
        private readonly Boolean caseSensitiveSwitches;    // Are switches case-sensitive?

        // Error text buffer
        private readonly System.Text.StringBuilder _sb = new System.Text.StringBuilder();
        public String ErrorMessage
        {
            get
            {
                return _sb.ToString();
            }
        }

        // Domain of results when processing a command-line argument switch
        public enum SwitchStatus
        {
            @NoError,
            @YesError,
            @Quit,        // Just quit with an abort but do not generate an error. Typically from OnDoneParse
            @ShowUsage
        }

        public ArgParserBase(String[] switchSymbols)
            : this(switchSymbols, false, new String[] { "/", "-" })
        {
        }

        public ArgParserBase(String[] switchSymbols, Boolean caseSensitiveSwitches)
            : this(switchSymbols, caseSensitiveSwitches, new String[] { "/", "-" })
        {
        }

        public ArgParserBase(String[] switchSymbols, Boolean caseSensitiveSwitches, String[] switchChars)
        {
            this.switchSymbols = switchSymbols;
            this.caseSensitiveSwitches = caseSensitiveSwitches;
            this.switchChars = switchChars;
        }

        /// <summary>
        /// Display the error dialog about the command being invalid.
        /// </summary>
        protected virtual void OnUsage(String errorInfo) { }

        protected virtual void AppendErrorLine(String errorLine)
        {
            _sb.Append(errorLine);
            _sb.Append(Environment.NewLine);
        }

        // Every derived class must implement an OnSwitch method or a switch is considered an error
        protected virtual SwitchStatus OnSwitch(String switchSymbol, String switchValue)
        {
            return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
        }

        // Every derived class must implement an OnNonSwitch method or a non-switch is considerred an error
        protected virtual SwitchStatus OnNonSwitch(String value)
        {
            return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
        }

        // The derived class is notified after all command-line switches have been parsed.
        // The derived class can perform any sanity checking required at this time.
        protected virtual SwitchStatus OnDoneParse()
        {
            // By default, we//ll assume that all parsing was successful
            return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }

        // This Parse method parses an arbitrary set of arguments
        public bool Parse(string[] args)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError; //Assume parsing is sucessful.
            Int32 ArgNum;
            for (ArgNum = 0; ArgNum < args.Length; ++ArgNum)
            {
                if (!(ss == DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError))
                {
                    break;
                }

                // Determine if this argument starts with a valid switch character
                bool fIsSwitch = false;
                Int32 n;
                for (n = 0; n < switchChars.Length; ++n)
                {
                    if ((fIsSwitch == true))
                    {
                        break;
                    }
                    fIsSwitch = (0 == string.CompareOrdinal(args[ArgNum], 0, switchChars[n], 0, 1));
                }

                if (fIsSwitch)
                {
                    // Does the switch begin with a legal switch symbol?
                    bool fLegalSwitchSymbol = false;
                    for (n = 0; n < switchSymbols.Length; ++n) //re-using local var n to traverse an entirely seperate loop
                    {
                        if (caseSensitiveSwitches)
                        {
                            fLegalSwitchSymbol = (0 == string.CompareOrdinal(args[ArgNum], 1, switchSymbols[n], 0, switchSymbols[n].Length));
                        }
                        else
                        {
                            fLegalSwitchSymbol = (0 == string.CompareOrdinal(args[ArgNum].ToUpper(System.Globalization.CultureInfo.InvariantCulture), 1, switchSymbols[n].ToUpper(System.Globalization.CultureInfo.InvariantCulture), 0, switchSymbols[n].Length));
                        }
                        if (fLegalSwitchSymbol)
                        {
                            break;
                        }
                    }
                    if (!fLegalSwitchSymbol)
                    {
                        // User specified an unrecognized switch, exit
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                        break;
                    }
                    else
                    {
                        // This is a legal switch, notified the derived class of this switch and its value
                        if ((caseSensitiveSwitches))
                        {
                            ss = OnSwitch(switchSymbols[n], args[ArgNum].Substring((1 + switchSymbols[n].Length)));
                        }
                        else
                        {
                            ss = OnSwitch(switchSymbols[n].ToLower(System.Globalization.CultureInfo.InvariantCulture), args[ArgNum].Substring((1 + switchSymbols[n].Length)));
                        }
                    }
                }
                else
                {
                    // This is not a switch, notified the derived class of this "non-switch value"
                    ss = OnNonSwitch(args[ArgNum]);
                }
            }

            if (ss == DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError)
            {
                // Status indicates that an error occurred, show it and the proper usage
                if (ArgNum == args.Length)
                {
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                }
                else
                {
                    OnUsage(args[ArgNum]);
                }
            }

            // Finished parsing arguments
            if (ss == DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError)
            {
                // No error occurred while parsing, let derived class perform a 
                // sanity check and return an appropriate status
                ss = OnDoneParse();
            }

            if (ss == DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage)
            {
                // Status indicates that usage should be shown, show it
                OnUsage(null);
            }

            // return whether all parsing was successful.
            return ss == DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }
    }

    /// <summary>
    /// An empty arg parser for the routines that don't expect arguments of any kind
    /// </summary>
    public sealed class EmptyArgParser : ArgParserBase
    {
        public EmptyArgParser() : base(new string[] { }) { }
        protected override void OnUsage(String errorInfo) { }
    }
}
