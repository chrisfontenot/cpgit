﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class Calendar
    {
        /// <summary>
        /// Find the next working day for the bank
        /// </summary>
        /// <returns>The next working day for the bank</returns>
        /// <remarks></remarks>
        public static DateTime NextWorkingDay(DateTime InputDateTime)
        {
            // Ensure that it does not fall on a weekend
            switch (InputDateTime.DayOfWeek)
            {
                case DayOfWeek.Friday:
                    return InputDateTime.AddDays(3); // FRI -> MON
                case DayOfWeek.Saturday:
                    return InputDateTime.AddDays(2); // SAT -> MON
                default:
                    return InputDateTime.AddDays(1); // SUN -> MON, MON -> TUE, TUE -> WED, WED -> THU, THU -> FRI
            }
        }
    }
}
