﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class Nulls
    {
        /// <summary>
        /// Convert an object to decimal
        /// </summary>
        public static decimal DDec(object obj)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is decimal)
                {
                    return (decimal)obj;
                }

                try
                {
                    decimal tempResult;
                    if (obj is string && decimal.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToDecimal(obj);
                }
                catch { }
            }
            return 0M;
        }

        /// <summary>
        /// Convert an object to decimal
        /// </summary>
        public static decimal DDec(object obj, decimal DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is decimal)
                {
                    return (decimal)obj;
                }

                try
                {
                    decimal tempResult;
                    if (obj is string && decimal.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToDecimal(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Convert an object to fixed point 32 bit Int32
        /// </summary>
        public static Int32 DInt(object obj)
        {
            if (obj != null && ! object.Equals(obj, System.DBNull.Value))
            {
                if (obj is Int32)
                {
                    return (Int32)obj;
                }

                try
                {
                    Int32 tempResult;
                    if (obj is string && Int32.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt32(obj);
                }
                catch { }
            }
            return 0;
        }

        /// <summary>
        /// Convert an object to fixed point 32 bit Int32
        /// </summary>
        public static Int32 DInt(object obj, Int32 DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is Int32)
                {
                    return (Int32)obj;
                }

                try
                {
                    Int32 tempResult;
                    if (obj is string && Int32.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt32(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Convert an object to fixed point 64 bit Int32
        /// </summary>
        public static Int64 DLng(object obj)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is Int64)
                {
                    return (Int64)obj;
                }

                try
                {
                    Int64 tempResult;
                    if (obj is string && Int64.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt64(obj);
                }
                catch { }
            }
            return 0;
        }

        /// <summary>
        /// Convert an object to fixed point 64 bit Int32
        /// </summary>
        public static Int64 DLng(object obj, Int64 DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is Int64)
                {
                    return (Int64)obj;
                }

                try
                {
                    Int64 tempResult;
                    if (obj is string && Int64.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt64(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Convert an object to a string
        /// </summary>
        public static string DStr(object obj)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is string)
                {
                    return (string)obj;
                }

                try
                {
                    return Convert.ToString(obj);
                }
                catch { }
            }
            return string.Empty;
        }

        /// <summary>
        /// Convert an object to a string
        /// </summary>
        public static string DStr(object obj, string DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is string)
                {
                    return (string)obj;
                }

                try
                {
                    return Convert.ToString(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Convert an object to a floating point double
        /// </summary>
        public static double DDbl(object obj)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is double)
                {
                    return (double)obj;
                }

                try
                {
                    double tempResult;
                    if (obj is string && double.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt64(obj);
                }
                catch { }
            }
            return 0.0D;
        }

        /// <summary>
        /// Convert an object to a floating point double
        /// </summary>
        public static double DDbl(object obj, double DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is double)
                {
                    return (double)obj;
                }

                try
                {
                    double tempResult;
                    if (obj is string && double.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToInt64(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Convert an object to a boolean value
        /// </summary>
        public static bool DBool(object obj)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is bool || obj is Boolean)
                {
                    return (bool)obj;
                }

                try
                {
                    bool tempResult;
                    if (obj is string && bool.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToBoolean(obj);
                }
                catch { }
            }
            return false;
        }

        /// <summary>
        /// Convert an object to a boolean value
        /// </summary>
        public static bool DBool(object obj, bool DefaultValue)
        {
            if (obj != null && !object.Equals(obj, System.DBNull.Value))
            {
                if (obj is bool || obj is Boolean)
                {
                    return (bool)obj;
                }

                try
                {
                    bool tempResult;
                    if (obj is string && bool.TryParse((string)obj, out tempResult))
                    {
                        return tempResult;
                    }

                    return Convert.ToBoolean(obj);
                }
                catch { }
            }
            return DefaultValue;
        }

        /// <summary>
        /// Return the LINQ value for a nullable INT32 object
        /// </summary>
        public static System.Nullable<Int32> v_Int32(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return new System.Nullable<Int32>(Convert.ToInt32(input));
                }
                catch { }
            }
            return new System.Nullable<Int32>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable INT64 object
        /// </summary>
        public static System.Nullable<Int64> v_Int64(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return new System.Nullable<Int64>(Convert.ToInt64(input));
                }
                catch { }
            }
            return new System.Nullable<Int64>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable DOUBLE object
        /// </summary>
        public static System.Nullable<double> v_Double(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return new System.Nullable<double>(Convert.ToDouble(input));
                }
                catch { }
            }
            return new System.Nullable<double>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable DECIMAL object
        /// </summary>
        public static System.Nullable<decimal> v_Decimal(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return new System.Nullable<decimal>(Convert.ToDecimal(input));
                }
                catch { }
            }
            return new System.Nullable<decimal>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable Boolean object
        /// </summary>
        public static System.Nullable<Boolean> v_Boolean(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    if (input is Int32)
                    {
                        bool ans = Convert.ToInt32(input) != 0;
                        return new System.Nullable<Boolean>(ans);
                    }

                    if (input is Int64)
                    {
                        bool ans = Convert.ToInt64(input) != 0L;
                        return new System.Nullable<Boolean>(ans);
                    }

                    if (input is string)
                    {
                        bool ans;
                        if (Boolean.TryParse(Convert.ToString(input), out ans))
                        {
                            return new System.Nullable<Boolean>(ans);
                        }
                        return new System.Nullable<Boolean>(false);
                    }

                    return new System.Nullable<Boolean>(Convert.ToBoolean(input));
                }
                catch { }
            }
            return new System.Nullable<Boolean>();
        }

        public static System.Nullable<decimal> v_Decimal(Int32? input)
        {
            if (!input.HasValue)
            {
                return new decimal?();
            }
            return new decimal?(Convert.ToDecimal(input.Value));
        }

        /// <summary>
        /// Return the LINQ value for a nullable DATETIME object
        /// </summary>
        public static System.Nullable<DateTime> v_DateTime(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    // Convert the date. We can't handle dates that are too early so prevent them here.
                    System.DateTime dt = Convert.ToDateTime(input);
                    if (dt.Year >= 1800)
                    {
                        return new System.Nullable<DateTime>(dt);
                    }
                }
                catch { }
            }
            return new System.Nullable<DateTime>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable CHAR object
        /// </summary>
        public static System.Nullable<Char> v_Char(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return new System.Nullable<Char>(Convert.ToChar(input));
                }
                catch { }
            }
            return new System.Nullable<char>();
        }

        /// <summary>
        /// Return the LINQ value for a nullable STRING object
        /// </summary>
        public static string v_String(object input)
        {
            if (input != null && !object.Equals(input, System.DBNull.Value))
            {
                try
                {
                    return Convert.ToString(input);
                }
                catch { }
            }
            return null;
        }

        /// <summary>
        /// Return the ADO.NET version of NULL if the value is NULL
        /// </summary>
        public static object ToDbNull(DateTime? input)
        {
            if (!input.HasValue)
            {
                return System.DBNull.Value;
            }
            return input.Value;
        }

        /// <summary>
        /// Return the ADO.NET version of NULL if the value is NULL
        /// </summary>
        public static object ToDbNull(Int32? input)
        {
            if (!input.HasValue)
            {
                return System.DBNull.Value;
            }
            return input.Value;
        }

        /// <summary>
        /// Return the ADO.NET version of NULL if the value is NULL
        /// </summary>
        public static object ToDbNull(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return System.DBNull.Value;
            }
            return input;
        }

        /// <summary>
        /// Return the ADO.NET version of NULL if the value is NULL
        /// </summary>
        public static object ToDbNull(object input)
        {
            if (input == null)
            {
                return System.DBNull.Value;
            }
            return input;
        }
    }
}
