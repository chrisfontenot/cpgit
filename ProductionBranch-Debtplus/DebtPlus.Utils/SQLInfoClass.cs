﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils.CS
{
    public class SQLInfoClass : IDisposable
    {
        public SQLInfoClass()
        {
            CommandTimeout = 15;
            UserName = String.Empty;
            Password = String.Empty;
            Server = String.Empty;
            Database = String.Empty;
            Workstation = String.Empty;
            NetworkLibrary = String.Empty;
            AttachDbFilename = String.Empty;
            FalloverPartner = String.Empty;
            TrustedConnection = false;
            Encrypt = false;
            ConnectionTimeout = 4;

            // Read the values from the registry for the current settings
            ConnectionTimeout = Configuration.ConnectionTimeout;
            CommandTimeout = Configuration.CommandTimeout;
            PacketSize = Configuration.PacketSize;
            UserName = Configuration.Username;
            Password = Configuration.Password;
            Database = Configuration.Database;
            Workstation = Configuration.Workstation;
            Server = Configuration.Server;
            NetworkLibrary = Configuration.NetworkLibrary;
            AttachDbFilename = Configuration.AttachDbFilename;
            FalloverPartner = Configuration.FalloverPartner;
            TrustedConnection = Configuration.TrustedConnection;
            Encrypt = Configuration.Encrypt;

            // If there is no user name then the connection is trusted, regardless of the value
            if (string.IsNullOrEmpty(UserName))
            {
                TrustedConnection = true;
            }

            MakeConnectionString();
        }

        public string Name
        {
            get
            {
                return "SQL_Info";
            }
        }

        public string ConnectionString { get; set; }
        public Int32 CommandTimeout { get; set; }

        public string  UserName { get; set; }
        public string  Password { get; set; }
        public string  Server { get; set; }
        public string  Database { get; set; }
        public string  Workstation { get; set; }
        public Int32   PacketSize { get; set; }
        public string  NetworkLibrary { get; set; }
        public string  AttachDbFilename { get; set; }
        public string  FalloverPartner { get; set; }
        public Boolean TrustedConnection { get; set; }
        public Boolean Encrypt { get; set; }
        public Int32   ConnectionTimeout { get; set; }

        private void MakeConnectionString()
        {
            System.Data.SqlClient.SqlConnectionStringBuilder sb = new System.Data.SqlClient.SqlConnectionStringBuilder();

            sb["Data Source"] = Server;
            sb["Initial Catalog"] = Database;

            // Include other fields that we use
            if (NetworkLibrary != String.Empty)
            {
                sb["Network Library"] = NetworkLibrary;
            }

            if (AttachDbFilename != String.Empty)
            {
                sb["AttachDBFilename"] = AttachDbFilename;
            }

            if (PacketSize > 0)
            {
                sb["Packet Size"] = PacketSize;
            }

            if (ConnectionTimeout > 0)
            {
                sb["Connection Timeout"] = ConnectionTimeout;
            }


            // If this is a trusted connection then so indicate
            if (TrustedConnection)
            {
                sb["Trusted_Connection"] = true;
            }
            else
            {
                sb["User ID"] = UserName;
                sb["Password"] = Password;
            }

            ConnectionString = sb.ConnectionString;
        }

#region IDisposable Support
        private System.Boolean disposedValue; // To detect redundant calls

        protected virtual void Dispose(System.Boolean disposing)
        {
            if (! this.disposedValue)
            {
                if (disposing)
                {
                    // TODO: Release any managed storage
                }
                // TODO: set large fields to null.
            }
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#endregion
    }
}
