﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Utils
{
    public static class Math
    {
        /// <summary>
        /// Truncate the number to zero decimal places
        /// </summary>
        /// <param name="Input">The number to be Truncated</param>
        /// <returns>The corresponding Truncated number without decimal places</returns>
        /// <remarks>For now, this is the equivalent of System.Math.Round</remarks>
        public static decimal Truncate(decimal Input)
        {
            return Truncate(Input, 0);
        }

        private static decimal[] decimal_Scales = new Decimal[] { 1M, 10M, 100M, 1000M, 10000M, 100000M, 1000000M, 10000000M, 100000000M, 1000000000M, 10000000000M, 100000000000M, 1000000000000M, 10000000000000M };
        /// <summary>
        /// Truncate the number to the specified number of decimal places
        /// </summary>
        /// <param name="Input">The number to be Truncated</param>
        /// <param name="Digits">The number of places to Truncate the number</param>
        /// <returns>The corresponding Truncated number without decimal places</returns>
        /// <remarks>For now, this is the equivalent of System.Math.Round</remarks>
        public static decimal Truncate(decimal Input, Int32 Digits)
        {
            if (Digits == 0)
            {
                return System.Decimal.Truncate(Input);
            }
            else if (Digits == 2)
            {
                return System.Decimal.Truncate(Input * 100M) / 100M;
            }

            decimal factor = decimal_Scales[Digits];
            return System.Decimal.Truncate(Input * factor) / factor;
        }

        /// <summary>
        /// Truncate the number to the specific number of decimal places
        /// </summary>
        /// <param name="Input">The number to be Truncated</param>
        /// <returns>The corresponding Truncated number without decimal places</returns>
        /// <remarks>For now, this is the equivalent of System.Math.Round</remarks>
        public static Double Truncate(Double Input)
        {
            return Truncate(Input, 0);
        }

        private static double[] double_Scales = new Double[] { 1D, 10D, 100D, 1000D, 10000D, 100000D, 1000000D, 10000000D, 100000000D, 1000000000D, 10000000000D, 100000000000D, 1000000000000D, 10000000000000D };
        /// <summary>
        /// Truncate the number to the specified number of decimal places
        /// </summary>
        /// <param name="Input">The number to be Truncated</param>
        /// <param name="Digits">The number of places to Truncate the number</param>
        /// <returns>The corresponding Truncated number without decimal places</returns>
        /// <remarks>For now, this is the equivalent of System.Math.Round</remarks>
        public static double Truncate(double Input, Int32 Digits)
        {
            if (Digits == 0)
            {
                return System.Math.Truncate(Input);
            }

            double factor = double_Scales[Digits];
            Int64 LongValue = Convert.ToInt64(System.Math.Truncate(Input * factor));
            return Convert.ToDouble(LongValue) / factor;
        }
    }
}
