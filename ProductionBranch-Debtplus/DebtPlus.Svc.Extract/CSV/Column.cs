#region Copyright 2000-2012 DebtPlus, L.L.C.
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Xml;
using System.Xml.Serialization;

namespace DebtPlus.Svc.Extract.CSV
{
    internal class Column
    {
        /// <summary>
        /// Construct a new instance of our class
        /// </summary>
        internal Column()
            : base()
        {
        }

        /// <summary>
        /// Construct a new instance of our class
        /// </summary>
        /// <param name="Ordinal"></param>
        /// <param name="Name"></param>
        internal Column(Int32 Ordinal, string Name)
            : base()
        {
            this.Ordinal = Ordinal;
            this.Name = Name;
        }

        /// <summary>
        /// Construct a new instance of our class
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Format"></param>
        internal Column(string Name, string Format)
            : base()
        {
            this.Name = Name;
            this.Format = Format;
        }

        private Int32 privateOrdinal = -1;
        /// <summary>
        /// Ordinal number for the column
        /// </summary>
        [XmlIgnore()]
        internal Int32 Ordinal
        {
            get
            {
                return privateOrdinal;
            }

            set
            {
                privateOrdinal = value;
            }
        }

        private string privateName = string.Empty;
        /// <summary>
        /// Name of the column
        /// </summary>
        [XmlAttribute()]
        internal string Name
        {
            get
            {
                return privateName;
            }

            set
            {
                privateName = value.Trim();
            }
        }

        private string privateFormat = "{0}";
        /// <summary>
        /// Formatting string for the column
        /// </summary>
        [XmlAttribute()]
        internal string Format
        {
            get
            {
                return privateFormat;
            }

            set
            {
                privateFormat = value;
            }
        }

        /// <summary>
        /// Translate the column value to a suitable string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal string Encode(object obj)
        {
            string Answer = string.Empty;

            if (obj != System.DBNull.Value)
            {
                if (Format != string.Empty)
                {
                    Answer = string.Format(Format, obj);
                }
                else if (obj is bool)
                {
                    if (Convert.ToBoolean(obj))
                    {
                        Answer = "Y";
                    }
                    else
                    {
                        Answer = "N";
                    }
                }
                else if (obj is decimal)
                {
                    Answer = (System.Math.Floor(Convert.ToDecimal(obj) * 100M)/100M).ToString();
                }
                else if (obj is DateTime)
                {
                    Answer = Convert.ToDateTime(obj).ToShortDateString();
                }
                else
                {
                    Answer = Convert.ToInt64(obj).ToString();
                }
            }
            return Answer;
        }
    }
}
