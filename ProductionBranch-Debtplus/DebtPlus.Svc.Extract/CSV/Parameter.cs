#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Extract.CSV
{
    internal class Parameter
    {
        // Special handling for parameters are based upon these values in the dialog type field.
        // If the type is a "from date" or a "to date" then the dialog is used to ask for a date range.
        // We don't have other types defined as of yet.
        internal enum DialogType
        {
            Standard = 0,
            FromDate = 1,
            ToDate = 2
        }

        /// <summary>
        /// Construct a new instance of our class
        /// </summary>
        internal Parameter()
            : base()
        {
            Name = string.Empty;
            Type = "int";
            DialogTypeValue = DialogType.Standard;
            Size = 0;
            Scale = 0;
            Precision = 0;
            Direction = "input";
        }

        /// <summary>
        /// Type of the parameter for the date range dialog
        /// </summary>
        internal DialogType DialogTypeValue { get; set; }

        /// <summary>
        /// Name of the parameter
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Parameter type
        /// </summary>
        internal string Type { get; set; }

        /// <summary>
        /// Parameter size
        /// </summary>
        internal Int32 Size { get; set; }

        /// <summary>
        /// Value associated with the parameter
        /// </summary>
        internal string Value { get; set; }

        /// <summary>
        /// Scale factor for decimal/double parameters
        /// </summary>
        internal byte Scale { get; set; }

        /// <summary>
        /// Precision factor for decimal/double parameters
        /// </summary>
        internal byte Precision { get; set; }

        /// <summary>
        /// Parameter direection (input/ output/ return value, etc.)
        /// </summary>
        internal string Direction { get; set; }

        /// <summary>
        /// Translate the direction string to the suitable Enum
        /// </summary>
        /// <returns></returns>
        internal ParameterDirection GetDirection()
        {
            ParameterDirection Answer = ParameterDirection.Input;
            switch (Direction.ToLower())
            {
                case "input":
                    Answer = ParameterDirection.Input;
                    break;

                case "inputoutput":
                    Answer = ParameterDirection.InputOutput;
                    break;

                case "output":
                    Answer = ParameterDirection.Output;
                    break;

                case "returnvalue":
                    Answer = ParameterDirection.ReturnValue;
                    break;
            }
            return Answer;
        }

        /// <summary>
        /// Translate the type string to a suitable Enum
        /// </summary>
        /// <returns></returns>
        internal SqlDbType GetDbType()
        {
            SqlDbType Answer = SqlDbType.VarChar;
            switch (Type.ToLower())
            {
                case "bigInt":
                    Answer = SqlDbType.BigInt;
                    break;

                case "binary":
                    Answer = SqlDbType.Binary;
                    break;

                case "bit":
                    Answer = SqlDbType.Bit;
                    break;

                case "char":
                    Answer = SqlDbType.Char;
                    break;

                case "date":
                    Answer = SqlDbType.Date;
                    break;

                case "datetime":
                    Answer = SqlDbType.DateTime;
                    break;

                case "datetime2":
                    Answer = SqlDbType.DateTime2;
                    break;

                case "datetimeoffset":
                    Answer = SqlDbType.DateTimeOffset;
                    break;

                case "decimal":
                    Answer = SqlDbType.Decimal;
                    break;

                case "float":
                    Answer = SqlDbType.Float;
                    break;

                case "image":
                    Answer = SqlDbType.Image;
                    break;

                case "int":
                    Answer = SqlDbType.Int;
                    break;

                case "money":
                    Answer = SqlDbType.Money;
                    break;

                case "nchar":
                    Answer = SqlDbType.NChar;
                    break;

                case "ntext":
                    Answer = SqlDbType.NText;
                    break;

                case "nvarchar":
                    Answer = SqlDbType.NVarChar;
                    break;

                case "real":
                    Answer = SqlDbType.Real;
                    break;

                case "smalldatetime":
                    Answer = SqlDbType.SmallDateTime;
                    break;

                case "smallint":
                    Answer = SqlDbType.SmallInt;
                    break;

                case "smallmoney":
                    Answer = SqlDbType.SmallMoney;
                    break;

                case "structured":
                    Answer = SqlDbType.Structured;
                    break;

                case "text":
                    Answer = SqlDbType.Text;
                    break;

                case "time":
                    Answer = SqlDbType.Time;
                    break;

                case "timestamp":
                    Answer = SqlDbType.Timestamp;
                    break;

                case "tinyint":
                    Answer = SqlDbType.TinyInt;
                    break;

                case "udt":
                    Answer = SqlDbType.Udt;
                    break;

                case "uniqueidentifier":
                    Answer = SqlDbType.UniqueIdentifier;
                    break;

                case "varbinary":
                    Answer = SqlDbType.VarBinary;
                    break;

                case "varchar":
                    Answer = SqlDbType.VarChar;
                    break;

                case "variant":
                    Answer = SqlDbType.Variant;
                    break;

                case "xml":
                    Answer = SqlDbType.Xml;
                    break;

                default:
                    Answer = SqlDbType.VarChar;
                    break;
            }
            return Answer;
        }

        /// <summary>
        /// Translate the value string to the suitable object for the value
        /// </summary>
        /// <returns></returns>
        internal object GetValue()
        {
            object Answer = System.DBNull.Value;
            if (Value != null)
            {
                switch (Type.ToLower())
                {
                    case "bit":
                        {
                            System.Boolean BooleanAnswer;
                            if (System.Boolean.TryParse(Value, out BooleanAnswer))
                            {
                                Answer = BooleanAnswer;
                            }
                        }
                        break;

                    case "date":
                    case "datetime":
                    case "smalldatetime":
                        {
                            DateTime DateAnswer;
                            if (DateTime.TryParse(Value, out DateAnswer))
                            {
                                Answer = DateAnswer;
                            }
                        }
                        break;

                    case "decimal":
                    case "money":
                        {
                            decimal decimalAnswer;
                            if (Decimal.TryParse(Value, out decimalAnswer))
                            {
                                Answer = decimalAnswer;
                            }
                        }
                        break;

                    case "float":
                    case "real":
                        {
                            double DoubleAnswer;
                            if (Double.TryParse(Value, out DoubleAnswer))
                            {
                                Answer = DoubleAnswer;
                            }
                        }
                        break;

                    case "int":
                    case "smallint":
                    case "tinyint":
                        {
                            Int32 IntAnswer;
                            if (Int32.TryParse(Value, out IntAnswer))
                            {
                                Answer = IntAnswer;
                            }
                        }
                        break;

                    default:
                        Answer = Value;
                        break;
                }
            }
            return Answer;
        }

        /// <summary>
        /// Construct and return the resulting parameter block
        /// </summary>
        /// <returns></returns>
        internal SqlParameter BuldParameter()
        {
            SqlParameter Answer = new SqlParameter();
            Answer.Direction = GetDirection();
            Answer.SqlDbType = GetDbType();
            Answer.ParameterName = Name;
            Answer.Value = GetValue();
            Answer.SourceVersion = DataRowVersion.Current;
            Answer.Size = Size;
            Answer.Scale = Scale;
            Answer.Precision = Precision;
            return Answer;
        }
    }
}
