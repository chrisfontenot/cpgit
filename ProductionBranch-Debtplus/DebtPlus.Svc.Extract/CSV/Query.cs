#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion;

using System;
using System.Data;
using System.Data.SqlClient;

namespace DebtPlus.Svc.Extract.CSV
{
    internal class Query
    {
        private string privateCommand;

        /// <summary>
        /// Command text to be executed for the database access
        /// </summary>
        internal string Command
        {
            get
            {
                return privateCommand;
            }
            set
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder(value);
                sb.Replace("\r", "");
                sb.Replace("\n", "");
                sb.Replace("\t", "");
                sb.Replace("\v", "");
                privateCommand = sb.ToString().Trim();
            }
        }

        private System.Collections.Generic.List<Parameter> privateParameters = new System.Collections.Generic.List<Parameter>();
        /// <summary>
        /// A list of the parameters for the query
        /// </summary>
        internal System.Collections.Generic.List<Parameter> Parameters
        {
            get
            {
                return privateParameters;
            }
            set
            {
                privateParameters = value;
            }
        }

        private Int32 CommandTimeout { get; set; }

        private string privateType = "text";
        /// <summary>
        /// Type of the parameter.
        /// </summary>
        internal string Type
        {
            get
            {
                return privateType;
            }
            set
            {
                privateType = value;
            }
        }

        /// <summary>
        /// Translate the type to a suitable CommandType
        /// </summary>
        /// <returns></returns>
        internal CommandType CommandType()
        {
            CommandType Answer = System.Data.CommandType.Text;
            switch( Type.ToLower() )
            {
                case "proc":
                case "procedure":
                case "storedprocedure":
                    Answer = System.Data.CommandType.StoredProcedure;
                    break;

                case "table":
                    Answer = System.Data.CommandType.TableDirect;
                    break;
            }
            return Answer;
        }

        /// <summary>
        /// Build the command block and parameters for the query
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        /// <returns></returns>
        internal SqlCommand BuildCommand(DebtPlus.LINQ.SQLInfoClass sqlInfo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = Command;
            cmd.CommandType = CommandType();
            cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, CommandTimeout);
            foreach( Parameter parm in Parameters )
            {
                cmd.Parameters.Add(parm.BuldParameter());
            }
            return cmd;
        }

        /// <summary>
        /// Do we need to show the date range dialog?
        /// </summary>
        /// <returns></returns>
        internal bool NeedDateDialog()
        {
            bool Answer = false;
            foreach( Parameter parm in Parameters )
            {
                if( parm.DialogTypeValue != Parameter.DialogType.Standard )
                {
                    Answer = true;
                    break;
                }
            }
            return Answer;
        }

        private System.Collections.Generic.List<Column> privateColumns = new System.Collections.Generic.List<Column>();
        /// <summary>
        /// A list of the resulting columns
        /// </summary>
        internal System.Collections.Generic.List<Column> Columns
        {
            get
            {
                return privateColumns;
            }
            set
            {
                privateColumns = value;
            }
        }
    }
}
