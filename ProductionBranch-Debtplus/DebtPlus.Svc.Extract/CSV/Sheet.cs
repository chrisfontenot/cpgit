#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DebtPlus.Svc.Extract.CSV
{
    internal class Sheet
    {
        private string privateSepChar = ",";
        [XmlAttribute()]
        internal string SepChar
        {
            get
            {
                return privateSepChar;
            }
            set
            {
                privateSepChar = value;
            }
        }

        private string privateQuoteChar = ",";
        [XmlAttribute()]
        internal string QuoteChar
        {
            get
            {
                return privateQuoteChar;
            }
            set
            {
                privateQuoteChar = value;
            }
        }

        private TriState privateQuote = TriState.UseDefault;
        [XmlAttribute()]
        internal TriState Quote
        {
            get
            {
                return privateQuote;
            }
            set
            {
                privateQuote = value;
            }
        }

        private string privateName = string.Empty;
        internal string Name
        {
            get
            {
                return privateName;
            }
            set
            {
                privateName = value.Trim();
            }
        }

        private string privateTitle = string.Empty;
        internal string Title
        {
            get
            {
                return privateTitle;
            }
            set
            {
                privateTitle = value.Trim();
            }
        }

        private string privateFile = "*.xls";
        internal string File
        {
            get
            {
                return privateFile;
            }
            set
            {
                privateFile = value;
            }
        }

        private string privateOptions = string.Empty;
        internal string Options
        {
            get
            {
                return privateOptions;
            }
            set
            {
                privateOptions = value.ToLower().Replace(" ", "");
            }
        }

        internal bool hasOption(string Optionstring)
        {
            return (string.Format(",{0},", Options)).IndexOf(string.Format(",{0},", Optionstring.ToLower())) >= 0;
        }

        private string privateLineEnd = "CR,LF";
        internal string LineEnd
        {
            get
            {
                return privateLineEnd;
            }
            set
            {
                privateLineEnd = value.ToUpper().Replace(" ", "");
            }
        }

        private string privatePrefix = string.Empty;
        internal string Prefix
        {
            get
            {
                return privatePrefix;
            }
            set
            {
                privatePrefix = value;
            }
        }

        private string privateSuffix = string.Empty;
        internal string Suffix
        {
            get
            {
                return privateSuffix;
            }
            set
            {
                privateSuffix = value;
            }
        }

        private System.Collections.Generic.List<Query> privateQueries = new System.Collections.Generic.List<Query>();
        internal System.Collections.Generic.List<Query> Queries
        {
            get
            {
                return privateQueries;
            }
            set
            {
                privateQueries = value;
            }
        }

        internal string QuoteField(string InputField)
        {
            string Answer;
            if (isQuoteReqd(InputField) && QuoteChar != string.Empty)
            {
                Answer = QuoteChar + InputField.Replace(QuoteChar, QuoteChar + QuoteChar) + QuoteChar;
            }
            else
            {
                Answer = InputField;
            }
            return Answer;
        }

        internal bool isQuoteReqd(string InputField)
        {
            bool Answer = false;
            switch (Quote)
            {
                case TriState.False:
                    Answer = false;
                    break;

                case TriState.True:
                    Answer = true;
                    break;

                case TriState.UseDefault:
                    Answer = SpaceConflict(InputField) || QuoteCharConflict(InputField) || SepCharConflict(InputField);
                    break;
            }
            return Answer;
        }

        internal static bool SpaceConflict(string InputField)
        {
            return InputField.EndsWith(" ") || InputField.StartsWith(" ");
        }

        internal bool QuoteCharConflict(string InputField)
        {
            bool Answer;
            if (QuoteChar != string.Empty)
            {
                Answer = InputField.IndexOf(QuoteChar) >= 0;
            }
            else
            {
                Answer = false;
            }
            return Answer;
        }

        internal bool SepCharConflict(string InputField)
        {
            bool Answer;
            if (SepChar != string.Empty)
            {
                Answer = InputField.IndexOf(SepChar) >= 0;
            }
            else
            {
                Answer = false;
            }
            return Answer;
        }
    }
}
