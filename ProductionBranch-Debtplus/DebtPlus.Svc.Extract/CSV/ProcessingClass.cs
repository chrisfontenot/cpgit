﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DebtPlus.Svc.Extract.CSV
{
    public class ProcessingClass
    {
        #region GetFilename
        // Events required to interface to the UI layer
        public class GetFilenameArgs : System.EventArgs
        {
            public GetFilenameArgs() : base() { IsValid = true; }
            public GetFilenameArgs(string FileName, string Title)
                : this()
            {
                // Replace the string [My Documents] with the current documents directory
                if (FileName.LastIndexOf("[My Documents]") >= 0)
                {
                    string DocumentsDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    FileName = FileName.Replace("[My Documents]", DocumentsDirectory);
                }

                // Save the values as both input and output for later processing
                this.FileName = FileName;
                this.Title = Title;
            }

            public string FileName { get; set; }
            public string Title { get; set; }
            public bool IsValid { get; set; }
        }
        public delegate void GetFilenameEventHandler(object sender, GetFilenameArgs e);
        public event GetFilenameEventHandler GetFilename;
        protected internal virtual void OnGetFilename(GetFilenameArgs e)
        {
            GetFilenameEventHandler evt = GetFilename;
            if (evt != null)
            {
                evt(this, e);
            }
        }
        protected internal void RaiseGetFilename(GetFilenameArgs e)
        {
            OnGetFilename(e);
        }
        #endregion
        #region GetDates
        public class GetDatesArgs : System.EventArgs
        {
            public GetDatesArgs()
                : base()
            {
                IsValid = true;
            }
            public GetDatesArgs(DateTime FromDate, DateTime ToDate)
                : this()
            {
                this.FromDate = FromDate;
                this.ToDate = ToDate;
            }

            public DateTime FromDate { get; set; }
            public DateTime ToDate { get; set; }
            public bool IsValid { get; set; }
        }
        public delegate void GetDatesEventHandler(object sender, GetDatesArgs e);
        public event GetDatesEventHandler GetDates;
        protected internal virtual void OnGetDates(GetDatesArgs e)
        {
            GetDatesEventHandler evt = GetDates;
            if (evt != null)
            {
                evt(this, e);
            }
        }
        protected internal void RaiseGetDates(GetDatesArgs e)
        {
            OnGetDates(e);
        }
        #endregion
        #region Feedback
        public class FeedbackArgs : System.EventArgs
        {
            public FeedbackArgs() : base() { }
            public FeedbackArgs(string FileName) : this() { this.FileName = FileName; }
            public string FileName { get; set; }
        }
        public delegate void FeedbackEventHandler(object sender, FeedbackArgs e);
        public event FeedbackEventHandler Feedback;
        protected internal virtual void OnFeedback(FeedbackArgs e)
        {
            FeedbackEventHandler evt = Feedback;
            if (evt != null)
            {
                evt(this, e);
            }
        }
        protected internal void RaiseFeedback(FeedbackArgs e)
        {
            OnFeedback(e);
        }
        #endregion

        private DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
        private System.Collections.Generic.LinkedList<Sheet> Sheets = new System.Collections.Generic.LinkedList<Sheet>();

        private DateTime FromDate = DateTime.MinValue;
        private DateTime ToDate = DateTime.MaxValue;
        private bool RequestedDates = false;

        /// <summary>
        /// Process the extract operation. Read the control file and process all of the sheet directives.
        /// </summary>
        public void PerformExtract(string ControlFile)
        {
            // Parse and load the parameter file contents
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<Sheet>));
            using (var inputfile = new System.IO.StreamReader(ControlFile))
            {
                System.Collections.Generic.List<Sheet> sheets = (System.Collections.Generic.List<Sheet>)serializer.Deserialize(inputfile);

                // Process each sheet entry to generate the output file
                foreach (Sheet sht in sheets)
                {
                    if (!ProcessSheet(sht))
                    {
                        break;
                    }
                }

                // Close the input file once it has been read
                inputfile.Close();
            }
        }

        /// <summary>
        /// Handle the processing of a sheet directive
        /// </summary>
        /// <param name="sht"></param>
        private bool ProcessSheet(Sheet sht)
        {
            // Ask the UI to help translate the filename at this point.
            GetFilenameArgs e = new GetFilenameArgs(sht.File, sht.Title);
            RaiseGetFilename(e);
            if (!e.IsValid)
            {
                return false;
            }
            string OutputName = e.FileName;

            // Open the file. It will create it if needed else simply overwrite it.
            using (System.IO.StreamWriter outputfile = new System.IO.StreamWriter(OutputName, false, System.Text.Encoding.ASCII, 4096))
            {
                // Change the line ending sequence to match what is requested
                switch (sht.LineEnd.ToLower())
                {
                    case "cr":
                        outputfile.NewLine = "\r";
                        break;

                    case "lf":
                        outputfile.NewLine = "\n";
                        break;

                    case "lf,cr":
                        outputfile.NewLine = "\n\r";
                        break;

                    case "cr,lf":
                        outputfile.NewLine = "\r\n";
                        break;

                    case "":
                        outputfile.NewLine = string.Empty;
                        break;

                    default:
                        outputfile.NewLine = Environment.NewLine;
                        break;
                }

                // If there is a prefix line, write it
                if (sht.Prefix != string.Empty)
                {
                    outputfile.WriteLine(sht.Prefix);
                }

                foreach (Query qry in sht.Queries)
                {
                    // Request the date range if so desired
                    if (qry.NeedDateDialog() && !RequestedDates)
                    {
                        RequestedDates = true;
                        GetDatesArgs eDate = new GetDatesArgs(FromDate, ToDate);
                        RaiseGetDates(eDate);

                        if (!eDate.IsValid)
                        {
                            return false;
                        }

                        FromDate = eDate.FromDate;
                        ToDate = eDate.ToDate;
                    }

                    // For the date values, find the corresponding items in the parameters
                    foreach (Parameter parm in qry.Parameters)
                    {
                        switch (parm.DialogTypeValue)
                        {
                            case Parameter.DialogType.FromDate:
                                parm.Value = FromDate.ToShortDateString();
                                break;

                            case Parameter.DialogType.ToDate:
                                parm.Value = ToDate.ToShortDateString() + " 23:59:59";
                                break;
                        }
                    }

                    // Tell the ancestor that we are processing the command as given.
                    RaiseFeedback(new FeedbackArgs(qry.Command));

                    using (var cmd = qry.BuildCommand(sqlInfo))
                    {
                        using (var cn = new SqlConnection(sqlInfo.ConnectionString))
                        {
                            cn.Open();
                            cmd.Connection = cn;

                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                if (rd != null && rd.Read() && rd.FieldCount > 0)
                                {
                                    // If we are to auto-fill the column names then do so now
                                    if (sht.hasOption("autofill"))
                                    {
                                        qry.Columns.Clear();

                                        // Populate the list of columns from the input set
                                        for (Int32 FieldNo = 0; FieldNo < rd.FieldCount; ++FieldNo)
                                        {
                                            string FieldName = rd.GetName(FieldNo);
                                            if (string.IsNullOrEmpty(FieldName))
                                            {
                                                FieldName = string.Format("Col{0:000}", FieldNo + 1);
                                            }
                                            qry.Columns.Add(new Column(FieldNo, FieldName));
                                        }
                                    }
                                    else
                                    {
                                        // Find the ordinal positions for the fields by field name
                                        for (Int32 FieldNo = 0; FieldNo < qry.Columns.Count; ++FieldNo)
                                        {
                                            qry.Columns[FieldNo].Ordinal = FindOrdinal(rd, qry.Columns[FieldNo].Name);
                                        }
                                    }

                                    // If we need to write the field names then do so here before the first record
                                    if (sht.hasOption("fieldnames"))
                                    {
                                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                        for (Int32 FieldNo = 0; FieldNo <= qry.Columns.Count - 1; ++FieldNo)
                                        {
                                            if (qry.Columns[FieldNo].Ordinal >= 0)
                                            {
                                                string value = qry.Columns[FieldNo].Encode(qry.Columns[FieldNo].Name);
                                                if (sb.Length > 0)
                                                {
                                                    sb.Append(sht.SepChar);
                                                }
                                                sb.Append(sht.QuoteField(value));
                                            }
                                        }
                                        outputfile.WriteLine(sb.ToString());
                                    }

                                    // Process the records until there are no more
                                    do
                                    {
                                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                        for (Int32 FieldNo = 0; FieldNo <= qry.Columns.Count - 1; ++FieldNo)
                                        {
                                            if (qry.Columns[FieldNo].Ordinal >= 0)
                                            {
                                                string value = qry.Columns[FieldNo].Encode(rd[qry.Columns[FieldNo].Ordinal]);
                                                if (sb.Length > 0)
                                                {
                                                    sb.Append(sht.SepChar);
                                                }
                                                sb.Append(sht.QuoteField(value));
                                            }
                                        }
                                        outputfile.WriteLine(sb.ToString());
                                    }
                                    while (rd.Read());
                                }
                            }
                        }
                    }
                }

                // If there is a suffix line, write it
                if (sht.Suffix != string.Empty)
                {
                    outputfile.WriteLine(sht.Suffix);
                }

                // Close the file
                outputfile.Close();
            }
            return true;
        }

        /// <summary>
        /// Locate the ordinal in the resulting set.
        /// </summary>
        /// <param name="rd"></param>
        /// <param name="Name"></param>
        /// <returns></returns>
        private static Int32 FindOrdinal(SqlDataReader rd, string Name)
        {
            Int32 Answer = -1;
            for (Int32 FieldNo = 0; FieldNo < rd.FieldCount; ++FieldNo)
            {
                if (string.Compare(Name, rd.GetName(FieldNo), true) == 0)
                {
                    Answer = FieldNo;
                    break;
                }
            }
            return Answer;
        }
    }
}
