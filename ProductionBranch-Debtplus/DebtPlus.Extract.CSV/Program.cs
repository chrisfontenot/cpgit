﻿namespace DebtPlus.Extract.CSV
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Program class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Process the transactions
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            processingClass cls = new processingClass(args);
            cls.PerformExtract();
        }

        /// <summary>
        /// Program class
        /// </summary>
        private class processingClass : DebtPlus.Svc.Extract.CSV.ProcessingClass
        {
            /// <summary>
            /// Process the transactions
            /// </summary>
            /// <param name="args"></param>
            public processingClass(string[] args)
                : base()
            {
                this.args = args;
            }
            string[] args;

            protected override void OnGetFilename(GetFilenameArgs e)
            {
                base.OnGetFilename(e);

                if (e.IsValid)
                {
                    // Try to find a match to the command line parameters. We will take any filename referenced in the list.
                    System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@"\[CommandLine\:\s*(?<argument>\d+)\]");
                    System.Text.RegularExpressions.Match FoundMatch = rx.Match(e.FileName);
                    Int32 Argument;
                    if (FoundMatch != null && Int32.TryParse(FoundMatch.Groups[1].Value, out Argument))
                    {
                        if (Argument < args.GetUpperBound(0))
                        {
                            e.FileName = args[Argument];
                        }
                    }
                }
            }

            public void PerformExtract()
            {
                try
                {
                    string ControlFile = args != null && args.GetUpperBound(0) >= 0 ? args[0].Trim() : string.Empty;
                    if( ControlFile != string.Empty )
                    {
                        PerformExtract(args[0]);
                    }
                    else
                    {
                        Console.WriteLine("Missing name of control file as a parameter. Extract cancelled.");
                    }
                }
                catch( Exception ex )
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
