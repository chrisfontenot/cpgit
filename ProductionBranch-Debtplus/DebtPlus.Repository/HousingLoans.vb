Imports System.Data.SqlClient

Public Module HousingLoans
#If False Then
    Private ReadOnly baseQuery As String = "SElECT [oID],[CurrentDetailID],[CurrentLenderID],[CurrentLoanBalanceAmt],[IntakeDetailID],[IntakeSameAsCurrent],[Loan1st2nd],[LoanDelinquencyAmount],[LoanDelinquencyMonths],[LoanOriginationDate],[MortgageClosingCosts],[OrigLenderID],[OrigLoanAmt],[PropertyID],[UseInReports],[UseOrigLenderID],[DsaDetailID] FROM housing_loans WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(baseQuery, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("{0} WHERE [oID]={1:f0}", baseQuery, id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByPropertyId(ByVal ds As DataSet, ByVal tableName As String, ByVal propertyId As Int32) As GetDataResult
        If propertyId <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("{0} WHERE [PropertyID]={1:f0}", baseQuery, propertyId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()
        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using da As SqlDataAdapter = New SqlDataAdapter

                    Using insertCommand As SqlCommand = New SqlCommand()
                        insertCommand.Connection = cn
                        insertCommand.CommandText = "xpr_insert_housing_loan"
                        insertCommand.CommandType = CommandType.StoredProcedure
                        insertCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
                        insertCommand.Parameters.Add("@PropertyID", System.Data.SqlDbType.Int, 0, "PropertyID")
                        insertCommand.Parameters.Add("@Loan1st2nd", System.Data.SqlDbType.Int, 0, "Loan1st2nd")
                        insertCommand.Parameters.Add("@UseOrigLenderID", System.Data.SqlDbType.Bit, 0, "UseOrigLenderID")
                        insertCommand.Parameters.Add("@CurrentLenderID", System.Data.SqlDbType.Int, 0, "CurrentLenderID")
                        insertCommand.Parameters.Add("@OrigLenderID", System.Data.SqlDbType.Int, 0, "OrigLenderID")
                        insertCommand.Parameters.Add("@OrigLoanAmt", System.Data.SqlDbType.Decimal, 0, "OrigLoanAmt")
                        insertCommand.Parameters.Add("@CurrentLoanBalanceAmt", System.Data.SqlDbType.Decimal, 0, "CurrentLoanBalanceAmt")
                        insertCommand.Parameters.Add("@IntakeSameAsCurrent", System.Data.SqlDbType.Bit, 0, "IntakeSameAsCurrent")
                        insertCommand.Parameters.Add("@IntakeDetailID", System.Data.SqlDbType.Int, 0, "IntakeDetailID")
                        insertCommand.Parameters.Add("@CurrentDetailID", System.Data.SqlDbType.Int, 0, "CurrentDetailID")
                        insertCommand.Parameters.Add("@MortgageClosingCosts", System.Data.SqlDbType.Decimal, 0, "MortgageClosingCosts")
                        insertCommand.Parameters.Add("@UseInReports", System.Data.SqlDbType.Bit, 0, "UseInReports")
                        insertCommand.Parameters.Add("@LoanDelinquencyMonths", System.Data.SqlDbType.Int, 0, "LoanDelinquencyMonths")
                        insertCommand.Parameters.Add("@LoanDelinquencyAmount", System.Data.SqlDbType.Decimal, 0, "LoanDelinquencyAmount")
                        insertCommand.Parameters.Add("@LoanOriginationDate", System.Data.SqlDbType.DateTime, 0, "LoanOriginationDate")
                        insertCommand.Parameters.Add("@DsaDetailID", System.Data.SqlDbType.Int, 0, "DsaDetailID")
                        da.InsertCommand = insertCommand

                        Using updateCommand As SqlCommand = New SqlCommand()
                            updateCommand.Connection = cn
                            updateCommand.CommandText = "UPDATE housing_loans SET [CurrentDetailID]=@CurrentDetailID,[CurrentLenderID]=@CurrentLenderID,[CurrentLoanBalanceAmt]=@CurrentLoanBalanceAmt,[IntakeDetailID]=@IntakeDetailID,[IntakeSameAsCurrent]=@IntakeSameAsCurrent,[Loan1st2nd]=@Loan1st2nd,[LoanDelinquencyAmount]=@LoanDelinquencyAmount,[LoanDelinquencyMonths]=@LoanDelinquencyMonths,[LoanOriginationDate]=@LoanOriginationDate,[MortgageClosingCosts]=@MortgageClosingCosts,[OrigLenderID]=@OrigLenderID,[OrigLoanAmt]=@OrigLoanAmt,[UseInReports]=@UseInReports,[UseOrigLenderID]=@UseOrigLenderID,[DsaDetailID]=@DsaDetailID WHERE [oID]=@oID"
                            updateCommand.CommandType = CommandType.Text
                            updateCommand.Parameters.Add("@Loan1st2nd", System.Data.SqlDbType.Int, 0, "Loan1st2nd")
                            updateCommand.Parameters.Add("@UseOrigLenderID", System.Data.SqlDbType.Bit, 0, "UseOrigLenderID")
                            updateCommand.Parameters.Add("@CurrentLenderID", System.Data.SqlDbType.Int, 0, "CurrentLenderID")
                            updateCommand.Parameters.Add("@OrigLenderID", System.Data.SqlDbType.Int, 0, "OrigLenderID")
                            updateCommand.Parameters.Add("@OrigLoanAmt", System.Data.SqlDbType.Decimal, 0, "OrigLoanAmt")
                            updateCommand.Parameters.Add("@CurrentLoanBalanceAmt", System.Data.SqlDbType.Decimal, 0, "CurrentLoanBalanceAmt")
                            updateCommand.Parameters.Add("@IntakeSameAsCurrent", System.Data.SqlDbType.Bit, 0, "IntakeSameAsCurrent")
                            updateCommand.Parameters.Add("@IntakeDetailID", System.Data.SqlDbType.Int, 0, "IntakeDetailID")
                            updateCommand.Parameters.Add("@CurrentDetailID", System.Data.SqlDbType.Int, 0, "CurrentDetailID")
                            updateCommand.Parameters.Add("@MortgageClosingCosts", System.Data.SqlDbType.Decimal, 0, "MortgageClosingCosts")
                            updateCommand.Parameters.Add("@UseInReports", System.Data.SqlDbType.Bit, 0, "UseInReports")
                            updateCommand.Parameters.Add("@LoanDelinquencyMonths", System.Data.SqlDbType.Int, 0, "LoanDelinquencyMonths")
                            updateCommand.Parameters.Add("@LoanDelinquencyAmount", System.Data.SqlDbType.Decimal, 0, "LoanDelinquencyAmount")
                            updateCommand.Parameters.Add("@LoanOriginationDate", System.Data.SqlDbType.DateTime, 0, "LoanOriginationDate")
                            updateCommand.Parameters.Add("@DsaDetailID", System.Data.SqlDbType.Int, 0, "DsaDetailID")
                            updateCommand.Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                            da.UpdateCommand = updateCommand

                            Using deleteCommand As SqlCommand = New SqlCommand()
                                deleteCommand.Connection = cn
                                deleteCommand.CommandText = "DELETE FROM housing_loans WHERE [oID]=@oID"
                                deleteCommand.CommandType = CommandType.Text
                                deleteCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
                                da.DeleteCommand = deleteCommand

                                ccr.RowsAffected = da.Update(dt)
                                Return ccr
                            End Using
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)
            End Try
        End Using
    End Function
#End If
End Module
