﻿Public Module HudTerminationReasons
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "lst_hud_termination_reasons"
        Return FillNamedDataTableFromSproc(query, ds, tableName)
    End Function
#End If
End Module
