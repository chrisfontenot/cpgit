Imports System.Data.SqlClient

Public Module ActionPlanGoals

    <Obsolete("Use LINQ Tables")> _
    Public Function GetByActionPlanId(ByVal ds As DataSet, ByVal tableName As String, ByVal actionPlanId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [action_plan],[text],[date_created],[created_by] FROM action_items_goals WITH (NOLOCK) WHERE [action_plan]={0:f0}", actionPlanId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "INSERT INTO action_items_goals([action_plan],[text]) VALUES (@action_plan,@text)"
            .CommandType = CommandType.Text
            .Parameters.Add("@action_plan", SqlDbType.Int, 0, "action_plan")
            .Parameters.Add("@text", SqlDbType.Text, 0, "text")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE action_items_goals SET [text]=@text WHERE [action_plan]=@action_plan"
            .CommandType = CommandType.Text
            .Parameters.Add("@text", SqlDbType.Text, 0, "text")
            .Parameters.Add("@action_plan", SqlDbType.Int, 0, "action_plan")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM action_items_goals WHERE [action_plan]=@action_plan"
            .CommandType = CommandType.Text
            .Parameters.Add("@action_plan", SqlDbType.Int, 0, "action_plan")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
