﻿''' <summary>
''' Exception generated when the repository discovers a fatal error condition
''' </summary>
Public Class RepositoryException
    Inherits Exception

    ''' <summary>
    ''' Initialize the class
    ''' </summary>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initialize the class with a message
    ''' </summary>
    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initialize the class with a message and an inner exception value
    ''' </summary>
    ''' <param name="Message">The message text</param>
    ''' <param name="InnerException">The inner exception that we want to track</param>
    ''' <remarks></remarks>
    Public Sub New(Message As String, InnerException As Exception)
        MyBase.New(Message, InnerException)
    End Sub

    ''' <summary>
    ''' Used to serialize the context information
    ''' </summary>
    ''' <param name="info"></param>
    ''' <param name="context"></param>
    ''' <remarks></remarks>
    Public Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    Public Shared Function NotInitialized() As RepositoryException
        Return New RepositoryException("Repository Not Initialized")
    End Function

    Public Shared Function NoDataTable() As RepositoryException
        Return New RepositoryException("Attempt to CommitChanges from null DataTable")
    End Function

    Public Shared Function NoDataRow() As RepositoryException
        Return New RepositoryException("Attempt to CommitChanges from null DataRow")
    End Function

    Public Shared Function NoDataRowCollection() As RepositoryException
        Return New RepositoryException("Attempt to CommitChanges from null DataRow Collection")
    End Function

    Public Shared Function BadParameters() As RepositoryException
        Return New RepositoryException("One or more parameters to a Repository method are invalid")
    End Function
End Class
