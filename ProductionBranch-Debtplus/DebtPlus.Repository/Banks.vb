Public Module Banks

    Private Const StdFields As String = "SELECT [bank],[description],[default],[ActiveFlag],[type],[ContactNameID],[bank_name],[BankAddressID],[aba],[account_number],[immediate_origin],[immediate_origin_name],[ach_priority],[ach_company_id],[ach_origin_dfi],[ach_enable_offset],[ach_company_identification],[ach_message_authentication],[ach_batch_company_id],[immediate_destination],[immediate_destination_name],[checknum],[max_clients_per_check],[max_amt_per_check],[batch_number],[transaction_number],NULL AS 'SignatureImage',[prefix_line],[suffix_line],[output_directory],[created_by],[date_created] FROM [banks] WITH (NOLOCK)"
    Private Const ProtectedFields As String = "SELECT [bank],[description],[default],[ActiveFlag],[type],[ContactNameID],[bank_name],[BankAddressID],[aba],[account_number],[immediate_origin],[immediate_origin_name],[ach_priority],[ach_company_id],[ach_origin_dfi],[ach_enable_offset],[ach_company_identification],[ach_message_authentication],[ach_batch_company_id],[immediate_destination],[immediate_destination_name],[checknum],[max_clients_per_check],[max_amt_per_check],[batch_number],[transaction_number],[SignatureImage],[prefix_line],[suffix_line],[output_directory],[created_by],[date_created] FROM [banks] WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableFromSelectCommand(StdFields, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal Id As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [bank]={1:f0}", StdFields, Id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetProtectedById(ByVal ds As DataSet, ByVal tableName As String, ByVal Id As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [bank]={1:f0}", ProtectedFields, Id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
