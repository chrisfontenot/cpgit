﻿Public Module RPPSMasks

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByCreditor(ByVal ds As DataSet, ByVal tableName As String, ByVal creditorId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT m.mask, m.Checkdigit FROM rpps_masks m INNER JOIN creditor_methods crm ON m.rpps_biller_id = crm.rpps_biller_id AND 'EFT' = crm.type INNER JOIN banks b on crm.bank = b.bank AND 'R' = b.type INNER JOIN creditors cr ON crm.creditor = cr.creditor_id WHERE cr.creditor_id = {0:f0}", creditorId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByCreditor(ByVal ds As DataSet, ByVal tableName As String, ByVal creditorName As String) As GetDataResult
        Dim query As String = String.Format("SELECT m.mask, m.Checkdigit FROM rpps_masks m INNER JOIN creditor_methods crm ON m.rpps_biller_id = crm.rpps_biller_id AND 'EFT' = crm.type INNER JOIN banks b on crm.bank = b.bank AND 'R' = b.type INNER JOIN creditors cr ON crm.creditor = cr.creditor_id WHERE cr.creditor = '{0}'", creditorName.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
