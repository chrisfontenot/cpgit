Imports System.Data.SqlClient

''' <summary>
''' Notes repository
''' </summary>
''' <remarks>
''' TODO - move all of the UI layer message boxes up to teh UI layer
''' </remarks>
Public Module CreditorNotes

    Private Const SelectStatement As String = "SELECT [creditor_note] as noteID, [creditor], [type], [dont_edit], [dont_delete], [dont_print], [subject], [date_created], dbo.notes_same_user([created_by]) as 'same_user', dbo.format_counselor_name([created_by]) as created_by, [expires], [note] FROM [creditor_notes]"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(SelectStatement, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal noteId As Int32, ByVal dataSet As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = SelectStatement + " WHERE [creditor_note]=@noteID"
        Return FillNamedDataTableFromSelectCommandById(query, "@noteID", noteId, dataSet, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllCreditorNotes(ByVal dataSet As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableFromSelectCommand(SelectStatement, dataSet, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DeleteCreditorNote(ByVal noteId As Int32) As GetDataResult
        Const query As String = "DELETE FROM creditor_notes WHERE creditor_note=@noteID AND (dont_delete=0 OR dbo.notes_same_user(created_by) <> 0)"
        Return DeleteRowUsingDeleteQueryAndId(query, "@noteID", noteId)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function SaveCreditorNote(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand
        With insertCommand
            .Connection = cn
            .CommandText = "INSERT INTO creditor_notes([creditor], [type], [dont_edit], [dont_delete], [dont_print], [subject], [expires], [note]) VALUES (@creditor, @type, @dont_edit, @dont_delete, @dont_print, @subject, @expires, @note)"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@creditor", SqlDbType.VarChar, 10, "creditor")
                .Add("@type", SqlDbType.Int, 0, "type")
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@expires", SqlDbType.DateTime, 0, "expires")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
            End With
        End With

        Dim updateCommand As SqlCommand = New SqlCommand
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE creditor_notes SET [creditor]=@creditor, [type]=@type, [dont_edit]=@dont_edit, [dont_delete]=@dont_delete, [dont_print]=@dont_print, [subject]=@subject, [expires]=@expires, [note]=@note WHERE creditor_note=@noteID"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@creditor", SqlDbType.VarChar, 10, "creditor")
                .Add("@type", SqlDbType.Int, 0, "type")
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@expires", SqlDbType.DateTime, 0, "expires")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
                .Add("@noteID", SqlDbType.Int, 0, "noteID")
            End With
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module

