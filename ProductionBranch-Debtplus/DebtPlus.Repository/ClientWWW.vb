Imports System.Data.SqlClient

Public Module ClientWWW
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByUsernameAndApplicationName(ByVal ds As DataSet, ByVal tableName As String, ByVal username As String, ByVal applicationName As String) As GetDataResult
        Dim query As String = String.Format("SELECT [PKID], [ApplicationName], [UserName], [Password], [Email], [PasswordQuestion], [PasswordAnswer], [Comment], [isLockedOut], [isApproved], [LastPasswordChangeDate], [LastLoginDate], [LastActivityDate], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart], [CreationDate], [created_by] FROM client_www WHERE [UserName]='{0}' AND [ApplicationName]='{1}'", username.Replace("'", "''"), applicationName.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "INSERT INTO client_www ([PKID], [ApplicationName], [UserName], [Password], [Email], [PasswordQuestion], [PasswordAnswer], [Comment], [isLockedOut], [isApproved], [LastPasswordChangeDate], [LastLoginDate], [LastActivityDate], [FailedPasswordAnswerAttemptCount], [FailedPasswordAnswerAttemptWindowStart], [FailedPasswordAttemptCount], [FailedPasswordAttemptWindowStart]) VALUES (@PKID, @ApplicationName, @UserName, @Password, @Email, @PasswordQuestion, @PasswordAnswer, @Comment, @isLockedOut, @isApproved, @LastPasswordChangeDate, @LastLoginDate, @LastActivityDate, @FailedPasswordAnswerAttemptCount, @FailedPasswordAnswerAttemptWindowStart, @FailedPasswordAttemptCount, @FailedPasswordAttemptWindowStart)"
            .CommandType = CommandType.Text
            .Parameters.Add("@PKID", SqlDbType.UniqueIdentifier, 0, "PKID")
            .Parameters.Add("@ApplicationName", SqlDbType.VarChar, 256, "ApplicationName")
            .Parameters.Add("@UserName", SqlDbType.VarChar, 256, "UserName")
            .Parameters.Add("@Password", SqlDbType.VarChar, 80, "Password")
            .Parameters.Add("@Email", SqlDbType.VarChar, 256, "Email")
            .Parameters.Add("@PasswordQuestion", SqlDbType.VarChar, 256, "PasswordQuestion")
            .Parameters.Add("@PasswordAnswer", SqlDbType.VarChar, 80, "PasswordAnswer")
            .Parameters.Add("@Comment", SqlDbType.VarChar, 256, "Comment")
            .Parameters.Add("@isLockedOut", SqlDbType.Bit, 0, "isLockedOut")
            .Parameters.Add("@isApproved", SqlDbType.Bit, 0, "isApproved")
            .Parameters.Add("@LastPasswordChangeDate", SqlDbType.DateTime, 0, "LastPasswordChangeDate")
            .Parameters.Add("@LastLoginDate", SqlDbType.DateTime, 0, "LastLoginDate")
            .Parameters.Add("@LastActivityDate", SqlDbType.DateTime, 0, "LastActivityDate")
            .Parameters.Add("@FailedPasswordAnswerAttemptCount", SqlDbType.Int, 0, "FailedPasswordAnswerAttemptCount")
            .Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", SqlDbType.DateTime, 0, "FailedPasswordAnswerAttemptWindowStart")
            .Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int, 0, "FailedPasswordAttemptCount")
            .Parameters.Add("@FailedPasswordAttemptWindowStart", SqlDbType.DateTime, 0, "FailedPasswordAttemptWindowStart")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_www SET [Password]=@Password, [Email]=@Email, [PasswordQuestion]=@PasswordQuestion, [PasswordAnswer]=@PasswordAnswer, [Comment]=@Comment, [isLockedOut]=@isLockedOut, [isApproved]=@isApproved, [LastPasswordChangeDate]=@LastPasswordChangeDate, [LastLoginDate]=@LastLoginDate, [LastActivityDate]=@LastActivityDate, [FailedPasswordAnswerAttemptCount]=@FailedPasswordAnswerAttemptCount, [FailedPasswordAnswerAttemptWindowStart]=@FailedPasswordAnswerAttemptWindowStart, [FailedPasswordAttemptCount]=@FailedPasswordAttemptCount, [FailedPasswordAttemptWindowStart]=@FailedPasswordAttemptWindowStart WHERE [UserName]=@UserName AND [ApplicationName]=@ApplicationName"
            .CommandType = CommandType.Text
            .Parameters.Add("@UserName", SqlDbType.VarChar, 256, "UserName")
            .Parameters.Add("@ApplicationName", SqlDbType.VarChar, 256, "ApplicationName")
            .Parameters.Add("@Password", SqlDbType.VarChar, 80, "Password")
            .Parameters.Add("@Email", SqlDbType.VarChar, 256, "Email")
            .Parameters.Add("@PasswordQuestion", SqlDbType.VarChar, 256, "PasswordQuestion")
            .Parameters.Add("@PasswordAnswer", SqlDbType.VarChar, 80, "PasswordAnswer")
            .Parameters.Add("@Comment", SqlDbType.VarChar, 256, "Comment")
            .Parameters.Add("@isLockedOut", SqlDbType.Bit, 0, "isLockedOut")
            .Parameters.Add("@isApproved", SqlDbType.Bit, 0, "isApproved")
            .Parameters.Add("@LastPasswordChangeDate", SqlDbType.DateTime, 0, "LastPasswordChangeDate")
            .Parameters.Add("@LastLoginDate", SqlDbType.DateTime, 0, "LastLoginDate")
            .Parameters.Add("@LastActivityDate", SqlDbType.DateTime, 0, "LastActivityDate")
            .Parameters.Add("@FailedPasswordAnswerAttemptCount", SqlDbType.Int, 0, "FailedPasswordAnswerAttemptCount")
            .Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", SqlDbType.DateTime, 0, "FailedPasswordAnswerAttemptWindowStart")
            .Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int, 0, "FailedPasswordAttemptCount")
            .Parameters.Add("@FailedPasswordAttemptWindowStart", SqlDbType.DateTime, 0, "FailedPasswordAttemptWindowStart")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM client_www WHERE [UserName]=@UserName AND [ApplicationName]=@ApplicationName"
            .CommandType = CommandType.Text
            .Parameters.Add("@UserName", SqlDbType.VarChar, 256, "UserName")
            .Parameters.Add("@ApplicationName", SqlDbType.VarChar, 256, "ApplicationName")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
