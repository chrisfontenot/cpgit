Imports System.Data.SqlClient

Public Module HudResults
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Const query As String = "lst_hud_results"
        Return FillNamedDataTableSchemaFromSproc(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetHudResultsByInterviewType(ByVal ds As DataSet, ByVal tableName As String, ByVal interviewType As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim cmd As SqlCommand = New SqlCommand
        cmd.CommandText = "lst_hud_results"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@interview_type", SqlDbType.Int).Value = interviewType

        Dim result = FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        If cmd IsNot Nothing Then cmd.Dispose()
        Return result
    End Function
#End If
End Module
