﻿Imports System.Data.SqlClient

Public Module ClientPOA
    Private Const stringQuerry As String = "SELECT [ID],[Client],[POAType],[POA_ID],[POA_ID_Issued],[POA_ID_Expires],[ApprovalBy],[RealEstate],[Financial],[Other],[Stage],[NameID],[TelephoneID],[TelephoneType],[Relation],[POA_Expires] FROM client_poa WITH (NOLOCK)"

    <Obsolete("Use LINQ tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [Client]={1:f0}", stringQuerry, clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function GetByPerson(ByVal ds As DataSet, ByVal tableName As String, ByVal personID As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [Person]={1:f0}", stringQuerry, personID)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function
End Module
