﻿Imports System.Data.SqlClient

Public Module ClientNotes

    Private Const SelectStatement As String = "SELECT [client_note] as noteID, [client], [client_creditor], convert(int,null) as deposit_batch_id, convert(int,null) as disbursement_register, [type], [dont_edit], [dont_delete], [dont_print], [subject], [date_created], dbo.notes_same_user([created_by]) as 'same_user', dbo.format_counselor_name([created_by]) as created_by, [expires], [note] FROM [client_notes]"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(SelectStatement, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetBydId(ByVal noteId As Int32, ByVal dataSet As DataSet) As GetDataResult
        Const query As String = SelectStatement + " WHERE [client_note]=@noteID"
        Return FillNamedDataTableFromSelectCommandById(query, "@noteID", noteId, dataSet, "client_notes")
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal clientId As Int32, ByVal subject As String, ByVal note As String) As GetDataResult
        Return Insert(clientId, subject, note, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal clientId As Int32, ByVal subject As String, ByVal note As String, ByVal txn As SqlTransaction) As GetDataResult
        Dim query As String = String.Format("INSERT INTO client_notes (Client,type,subject,note,dont_edit,dont_delete,dont_print) values ({0:f0}, 3, '{1}', '{2}', 1, 1, 0)", clientId, subject.Replace("'", "''"), note.Replace("'", "''"))
        Return ExecuteNonQuery(query, txn)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function InsertLetterSystemNote(ByVal letterType As Int32, ByVal clientId As Int32, ByVal creditor As String, ByVal debt As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        ' The letter type must be valid
        If letterType < 0 Then Throw New ArgumentOutOfRangeException("letterType<0")

        ' There must be a client, debt, or creditor to be a valid note.
        If clientId < 0 AndAlso debt <= 0 AndAlso String.IsNullOrEmpty(creditor) Then Throw RepositoryException.BadParameters

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "xpr_letter_system_note"
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@letter_type", SqlDbType.Int).Value = letterType
                cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                cmd.Parameters.Add("@debt", SqlDbType.Int).Value = debt

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            cn.Dispose()

        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function InsertClientNote(ByVal note As Models.ClientNote) As GetDataResult
        Return InsertClientNote(note, Nothing)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function InsertClientNote(ByVal note As Models.ClientNote, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        If note Is Nothing Then Throw New ArgumentNullException("note")

        Dim gdr As New GetDataResult()
        Dim cn As SqlConnection

        If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
            cn = txn.Connection
        Else
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            cn.Open()
        End If

        Try
            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = "INSERT INTO client_notes ([client],[client_creditor],[prior_note],[type],[expires],[dont_edit],[dont_delete],[dont_print],[subject],[note]) VALUES(@client,@client_creditor,@prior_note,@type,@expires,@dont_edit,@dont_delete,@dont_print,@subject,@note)"
                cmd.CommandType = CommandType.Text

                MapModelToCommandParameters(note, cmd)

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Sub MapModelToCommandParameters(ByVal note As Models.ClientNote, ByRef cmd As SqlCommand)
        If note Is Nothing OrElse cmd Is Nothing Then Return

        cmd.Parameters.Add("@client", SqlDbType.Int, 0).Value = note.ClientId
        cmd.Parameters.Add("@client_creditor", SqlDbType.Int, 0).Value = If(note.CreditorId, note.CreditorId, DBNull.Value)
        cmd.Parameters.Add("@prior_note", SqlDbType.Int, 0).Value = If(note.PriorNote, note.PriorNote, DBNull.Value)
        cmd.Parameters.Add("@type", SqlDbType.Int, 0).Value = note._Type
        cmd.Parameters.Add("@expires", SqlDbType.DateTime, 0).Value = If(note.Expires IsNot Nothing, note.Expires, DBNull.Value)
        cmd.Parameters.Add("@dont_edit", SqlDbType.Bit, 0).Value = note.DontEdit
        cmd.Parameters.Add("@dont_delete", SqlDbType.Bit, 0).Value = note.DontDelete
        cmd.Parameters.Add("@dont_print", SqlDbType.Bit, 0).Value = note.DontPrint
        cmd.Parameters.Add("@subject", SqlDbType.VarChar, 80).Value = If(Not String.IsNullOrWhiteSpace(note.Subject), note.Subject, DBNull.Value)
        cmd.Parameters.Add("@note", SqlDbType.VarChar, Int32.MaxValue).Value = If(Not String.IsNullOrWhiteSpace(note.Note), note.Note, DBNull.Value)
    End Sub

    <System.Obsolete("Use LINQ Tables")> _
    Public Function SaveClientNote(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand
        With insertCommand
            .Connection = cn
            .CommandText = "INSERT INTO client_notes([client],[client_creditor], [type], [dont_edit], [dont_delete], [dont_print], [subject], [expires], [note]) VALUES (@client,@client_creditor, @type, @dont_edit, @dont_delete, @dont_print, @subject, @expires, @note)"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@client", SqlDbType.Int, 0, "client")
                .Add("@client_creditor", SqlDbType.Int, 0, "client_creditor")
                .Add("@type", SqlDbType.Int, 0, "type")
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@expires", SqlDbType.DateTime, 0, "expires")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
            End With
        End With

        Dim updateCommand As SqlCommand = New SqlCommand
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_notes SET [type]=@type, [dont_edit]=@dont_edit, [dont_delete]=@dont_delete, [dont_print]=@dont_print, [subject]=@subject, [expires]=@expires, [note]=@note WHERE [client_note]=@noteID"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@type", SqlDbType.Int, 0, "type")
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@expires", SqlDbType.DateTime, 0, "expires")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
                .Add("@noteID", SqlDbType.Int, 0, "noteID")
            End With
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DeleteClientNote(ByVal noteId As Int32) As GetDataResult
        Const query As String = "DELETE FROM client_notes WHERE [client_note]=@noteID AND (dont_delete=0 OR dbo.notes_same_user(created_by) <> 0)"
        Return DeleteRowUsingDeleteQueryAndId(query, "@noteID", noteId)
    End Function

End Module
