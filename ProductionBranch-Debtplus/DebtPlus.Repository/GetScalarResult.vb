﻿Public Class GetScalarResult
    Implements DebtPlus.Interfaces.IRepositoryResult

    ''' <summary>
    ''' The single empty class for this type.
    ''' </summary>
    Private Shared emptyClass As GetScalarResult = Nothing

    ''' <summary>
    ''' Return an empty class for the result
    ''' </summary>
    ''' <remarks>Return the single empty class for this type</remarks>
    Public Shared ReadOnly Property Empty As GetScalarResult
        Get
            Try
                ' Look to see if some fool called Dispose of this empty class.
                If emptyClass IsNot Nothing AndAlso emptyClass.IsDisposed Then
                    emptyClass = Nothing
                End If

            Catch ex As System.Data.SqlClient.SqlException
                emptyClass = Nothing
            End Try

            ' Allocate a new empty copy if there is nothing
            If emptyClass Is Nothing Then
                emptyClass = New GetScalarResult()
            End If

            Return emptyClass
        End Get
    End Property

    ''' <summary>
    ''' Service method for the result
    ''' </summary>
    ''' <remarks></remarks>
    Public Overridable ReadOnly Property DataServiceMethod As String Implements Interfaces.IRepositoryResult.DataServiceMethod
        Get
            Return "GetScalar"
        End Get
    End Property

    ''' <summary>
    ''' Number of rows affected by the last request
    ''' </summary>
    ''' <value>The Int32 value of the number of rows</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RowsAffected As Int32 = 0 Implements Interfaces.IRepositoryResult.RowsAffected

    Private privateResult As Object = Nothing
    ''' <summary>
    ''' Current value from the ExecuteScalar request
    ''' </summary>
    ''' <value>The result of the ExecuteScalar</value>
    Public Property Result As Object
        Get
            Return privateResult
        End Get
        Set(value As Object)
            privateResult = If(System.Object.Equals(System.DBNull.Value, value), Nothing, value)
        End Set
    End Property

    ''' <summary>
    ''' Current exception thrown by the request
    ''' </summary>
    ''' <value>The Int32 value of the number of rows</value>
    Public Overridable Property ex As Exception = Nothing Implements Interfaces.IRepositoryResult.ex

    ''' <summary>
    ''' Return the retry status for the error condition
    ''' </summary>
    ''' <value>TRUE if the operation should just be retried. FALSE otherwise.</value>
    Public ReadOnly Property IsRetryable As Boolean
        Get
            Dim thisException As SqlClient.SqlException = TryCast(ex, SqlClient.SqlException)
            Return thisException IsNot Nothing AndAlso thisException.ErrorCode = 1205 ' Error 1205 is "choosen for deadlock".
        End Get
    End Property

    ''' <summary>
    ''' Initialize the class
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' True if the operation was successful. FALSE for a failure.
    ''' </summary>
    ''' <value>TRUE if the operation was successful</value>
    Public Overridable ReadOnly Property Success() As Boolean Implements Interfaces.IRepositoryResult.Success
        Get
            Return ex Is Nothing
        End Get
    End Property

    ''' <summary>
    ''' Exception handing routine
    ''' </summary>
    Public Overridable Function HandleException(ex As System.Exception) As Interfaces.IRepositoryResult Implements Interfaces.IRepositoryResult.HandleException

        If ex IsNot Nothing Then

            ' When we do an update, the update may result in zero rows being updated. IGNORE IT.
            If Not TypeOf ex Is DBConcurrencyException Then
                ex = ex
            End If
        End If

        Return Me
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls
    ''' <summary>
    ''' Return TRUE if the class has been Disposed but not Finalized
    ''' </summary>
    Public ReadOnly Property IsDisposed As Boolean
        Get
            Return disposedValue
        End Get
    End Property

    ''' <summary>
    ''' Handle the Dispose and Finialize events for the class
    ''' </summary>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If
            ex = Nothing
        End If
        Me.disposedValue = True
    End Sub

    ''' <summary>
    ''' Dispose of any allocated storage in the class
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
