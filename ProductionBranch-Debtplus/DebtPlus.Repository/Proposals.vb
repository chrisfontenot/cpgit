﻿Public Module Proposals
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllOpenProposals(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "lst_proposals_open"
        Return FillNamedDataTableFromSproc(query, ds, tableName)
    End Function
End Module
