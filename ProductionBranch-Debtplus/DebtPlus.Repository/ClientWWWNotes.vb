Imports System.Data.SqlClient

Public Module ClientWWWNotes

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByGuid(ByVal ds As DataSet, ByVal tableName As String, ByVal guid As Guid) As GetDataResult
        Dim query As String = String.Format("SELECT [client_www_note], [PKID], [message], [date_viewed], [date_shown], [date_created], [created_by] FROM client_www_notes WHERE [PKID]='{0}'", guid.ToString().Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_client_www_notes"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "client_www_note").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@PKID", SqlDbType.UniqueIdentifier, 0, "PKID")
            .Parameters.Add("@message", SqlDbType.VarChar, 256, "message")
            .Parameters.Add("@date_shown", SqlDbType.DateTime, 0, "date_shown")
            .Parameters.Add("@date_viewed", SqlDbType.DateTime, 0, "date_viewed")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_www_notes SET message=@message WHERE client_www_note=@client_www_note"
            .CommandType = CommandType.Text
            .Parameters.Add("@message", SqlDbType.VarChar, 256, "message")
            .Parameters.Add("@client_www_note", SqlDbType.Int, 0, "client_www_note")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM client_www_notes WHERE client_www_note=@client_www_note"
            .CommandType = CommandType.Text
            .Parameters.Add("@client_www_note", SqlDbType.Int, 0, "client_www_note")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
