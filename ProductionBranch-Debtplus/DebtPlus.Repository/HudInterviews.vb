Imports System.Data.SqlClient

Public Module HudInterviews
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [hud_interview],[client],[HUD_grant],[interview_type],[interview_date],[interview_counselor],[hud_result],[result_date],[result_counselor],[termination_reason],[termination_date],[termination_counselor],[HousingFeeAmount] FROM hud_interviews WITH (NOLOCK) WHERE [client]={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_housing_interview"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client", SqlDbType.Int, 4, "client")
            .Parameters.Add("@interview_type", SqlDbType.Int, 4, "interview_type")
            .Parameters.Add("@HUD_grant", SqlDbType.Int, 4, "HUD_grant")
            .Parameters.Add("@hud_result", SqlDbType.Int, 4, "hud_result")
            .Parameters.Add("@termination_reason", SqlDbType.Int, 4, "termination_reason")
            .Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE [hud_interviews] SET [HUD_grant]=@HUD_grant,[hud_result]=@hud_result,[termination_reason]=@termination_reason,[HousingFeeAmount]=@HousingFeeAmount WHERE [hud_interview]=@hud_interview"
            .CommandType = CommandType.Text
            .Parameters.Add("@HUD_grant", SqlDbType.Int, 4, "HUD_grant")
            .Parameters.Add("@hud_result", SqlDbType.Int, 4, "hud_result")
            .Parameters.Add("@termination_reason", SqlDbType.Int, 4, "termination_reason")
            .Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
            .Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM [hud_interviews] WHERE [hud_interview]=@hud_interview"
            .CommandType = CommandType.Text
            .Parameters.Add("@hud_interview", SqlDbType.Int, 0, "hud_interview")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
