Imports System.Data.SqlClient

Public Module DisbursementNotes

    Private Const SelectStatement As String = "SELECT [disbursement_note] as noteID, [client], convert(int,null) as client_creditor, [deposit_batch_id], [disbursement_register], [type], [dont_edit], [dont_delete], [dont_print], [subject], [date_created], dbo.notes_same_user([created_by]) as 'same_user', dbo.format_counselor_name([created_by]) as created_by, convert(datetime,null) as expires, [note] FROM [disbursement_notes]"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(SelectStatement, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllDisbursementNotes(ByVal dataSet As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableFromSelectCommand(SelectStatement, dataSet, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal noteId As Int32, ByVal dataSet As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = SelectStatement + " WHERE disbursement_note=@disbursement_note"
        Return FillNamedDataTableFromSelectCommandById(query, "@disbursement_note", noteId, dataSet, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientAndDepositBatchId(ByVal client As Int32, ByVal depositBatchId As Int32, ByVal dataSet As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If client <= 0 OrElse depositBatchId <= 0 OrElse dataSet Is Nothing Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Try
            cmd.Connection = cn
            cmd.CommandText = SelectStatement + " WHERE [client]=@client AND [deposit_batch_id]=@deposit_batch_id AND [type]=5"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = client
            cmd.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = depositBatchId

            cn.Open()

            dataSet.Clear()
            gdr.RowsAffected = da.Fill(dataSet, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function SaveDisbursementNote(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        ' Build the proper insert command based upon the type of the note
        Dim insertCommand As SqlCommand = New SqlCommand
        With insertCommand
            .Connection = cn
            .CommandText =
                "INSERT INTO disbursement_notes([client], [deposit_batch_id], [type], [dont_edit], [dont_delete], [dont_print], [subject], [note]) VALUES (@client, @deposit_batch_id, 5, @dont_edit, @dont_delete, @dont_print, @subject, @note)"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@client", SqlDbType.Int, 0, "client")
                .Add("@deposit_batch_id", SqlDbType.Int, 0, "deposit_batch_id")
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
            End With
        End With

        ' This should not be needed since the update will trigger the update of the disbursement notes and we won't come here.
        Dim updateCommand As SqlCommand = New SqlCommand
        With updateCommand
            .Connection = cn
            .CommandText =
                "UPDATE disbursement_notes SET [dont_edit]=@dont_edit, [dont_delete]=@dont_delete, [dont_print]=@dont_print, [subject]=@subject, [note]=@note WHERE [disbursement_note]=@noteID"
            .CommandType = CommandType.Text

            With .Parameters
                .Add("@dont_edit", SqlDbType.Bit, 0, "dont_edit")
                .Add("@dont_delete", SqlDbType.Bit, 0, "dont_delete")
                .Add("@dont_print", SqlDbType.Bit, 0, "dont_print")
                .Add("@subject", SqlDbType.VarChar, 80, "subject")
                .Add("@note", SqlDbType.VarChar, Int32.MaxValue, "note")
                .Add("@noteID", SqlDbType.Int, 0, "noteID")
            End With
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DeleteDisbursementNoteById(ByVal noteId As Int32) As GetDataResult
        Const query As String = "DELETE FROM disbursement_notes WHERE disbursement_note=@noteID AND (dont_delete=0 OR dbo.notes_same_user(created_by) <> 0)"
        Return DeleteRowUsingDeleteQueryAndId(query, "@noteID", noteId)
    End Function
End Module
