Imports System.Data.SqlClient

Public Module Assets

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [asset], [asset_id], [asset_amount], [amount], [frequency] FROM assets WHERE [client] = {0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal clientId As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_assets"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "asset").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
            .Parameters.Add("@asset_id", SqlDbType.Int, 0, "asset_id")
            .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
            .Parameters.Add("@asset_amount", SqlDbType.Decimal, 0, "asset_amount")
            .Parameters.Add("@frequency", SqlDbType.Int, 0, "frequency")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE assets SET [amount]=@amount,[frequency]=@frequency,[asset_amount]=@asset_amount WHERE [asset]=@asset"
            .CommandType = CommandType.Text
            .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
            .Parameters.Add("@frequency", SqlDbType.Int, 0, "frequency")
            .Parameters.Add("@asset", SqlDbType.Int, 0, "asset")
            .Parameters.Add("@asset_amount", SqlDbType.Decimal, 0, "asset_amount")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM assets WHERE [asset]=@asset"
            .CommandType = CommandType.Text
            .Parameters.Add("@asset", SqlDbType.Int, 0, "asset")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module

