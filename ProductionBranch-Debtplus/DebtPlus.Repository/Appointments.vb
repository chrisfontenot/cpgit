Imports System.Data.SqlClient

Public Module Appointments

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetFirstCompletedClientAppointment(ByVal clientId As Int32) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientID<0")

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "select dbo.map_client_to_first_pending_office_appt(@client)"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            Return ExecuteScalar(cmd)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetLatestCompletedClientAppointment(ByVal clientId As Int32) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientID<0")

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "select dbo.map_client_to_last_completed_office_appt(@client)"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            Return ExecuteScalar(cmd)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetLatestPendingClientAppointment(ByVal clientId As Int32) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientID<0")

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "select max(client_appointment) from client_appointments WHERE client = @client and status = 'P' AND office is not null"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            Return ExecuteScalar(cmd)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetLatestClientAppointment(ByVal clientId As Int32) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientID<0")

        Using cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "SELECT max(client_appointment) FROM client_appointments WHERE [client]=@client"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            Return ExecuteScalar(cmd)
        End Using
    End Function
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetLatestPendingAppointment() As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "SELECT max(client_appointment) FROM client_appointments WHERE [status]='P'"
            cmd.CommandType = CommandType.Text
            Return ExecuteScalar(cmd)
        End Using

    End Function

<System.Obsolete("Use LINQ Tables")> _
Public Function GetLatestAppointment() As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "SELECT max(client_appointment) FROM client_appointments"
            cmd.CommandType = CommandType.Text
            Return ExecuteScalar(cmd)
        End Using
    End Function
#End If

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllReferredByAppointments(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "lst_referred_by_appt"
        Return FillNamedDataTableFromSproc(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllWorkshopAppointments(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "xpr_appt_workshop_list"
        Return FillNamedDataTableFromSproc(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllBookedAppointmentsByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim cmd As SqlCommand = New SqlCommand()
        cmd.CommandText = "xpr_appt_booked"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
        Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function BookAppointment(ByRef NewAppointment As Int32, ByVal ClientId As Int32, ByVal apptTime As Int32?, ByVal priority As Boolean, ByVal apptType As Int32?, oldAppointment As Int32?, ByVal counselor As Int32?, ByVal bankruptcyClass As Int32?) As GetDataResult

        ' Create a client appointment structure to pass the parameters properly
        Dim ca As New Models.ClientAppointment()
        ca.ClientID = ClientId
        ca.appt_time = apptTime
        ca.priority = priority
        ca.appt_type = apptType
        ca.previous_appointment = oldAppointment
        ca.counselor = counselor
        ca.bankruptcy_class = bankruptcyClass

        ' Do the booking operation
        Dim gdr = BookAppointment(ca)
        If gdr.Success Then
            NewAppointment = ca.Id
        End If

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function BookAppointment(ByVal ca As Models.ClientAppointment) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If ca.ClientID < 0 Then Throw New ArgumentOutOfRangeException("ca.clientID < 0")

        Dim gdr As New GetDataResult()
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "xpr_appt_book"
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add("@client", SqlDbType.Int).Value = ca.ClientID
                cmd.Parameters.Add("@appt_time", SqlDbType.Int).Value = If(ca.appt_time.HasValue, ca.appt_time, DBNull.Value)
                cmd.Parameters.Add("@priority", SqlDbType.Int).Value = If(ca.priority, 1, 0)
                cmd.Parameters.Add("@appt_type", SqlDbType.Int).Value = If(ca.appt_type.HasValue, ca.appt_type, DBNull.Value)
                cmd.Parameters.Add("@old_appointment", SqlDbType.Int).Value = If(ca.previous_appointment.HasValue, ca.previous_appointment.Value, DBNull.Value)
                cmd.Parameters.Add("@counselor", SqlDbType.Int).Value = If(ca.counselor.HasValue, ca.counselor.Value, DBNull.Value)
                cmd.Parameters.Add("@bankruptcy_class", SqlDbType.Int).Value = If(ca.bankruptcy_class.HasValue, ca.bankruptcy_class.Value, DBNull.Value)

                gdr.RowsAffected = cmd.ExecuteNonQuery()

                ' Return the appointment ID to the caller
                If cmd.Parameters(0).Value IsNot Nothing AndAlso cmd.Parameters(0).Value IsNot System.DBNull.Value Then
                    ca.Id = Convert.ToInt32(cmd.Parameters(0).Value)
                End If
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function InsertClientAppointment(ByRef ClientAppointment As Int32, ByVal clientId As Int32, ByVal office As Int32?, ByVal workshop As Int32?, ByVal counselor As Int32?, ByVal apptType As Int32?, ByVal startDateTime As DateTime, ByVal status As String) As GetDataResult

        Dim ca As New Models.ClientAppointment()
        ca.ClientID = clientId
        ca.office = office
        ca.workshop = workshop
        ca.appt_type = apptType
        ca.counselor = counselor
        ca.appt_type = apptType
        ca.start_time = startDateTime
        ca.status = status

        Dim gdr = InsertClientAppointment(ca)
        If gdr.Success Then
            ClientAppointment = ca.Id
        End If

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function InsertClientAppointment(ByVal ca As Models.ClientAppointment) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Try
            cn.Open()
            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "xpr_insert_client_appointment"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

                cmd.Parameters.Add("@client", SqlDbType.Int).Value = ca.ClientID
                cmd.Parameters.Add("@office", SqlDbType.Int).Value = If(ca.office.HasValue, ca.office.Value, DBNull.Value)
                cmd.Parameters.Add("@workshop", SqlDbType.Int).Value = If(ca.workshop.HasValue, ca.workshop.Value, DBNull.Value)
                cmd.Parameters.Add("@counselor", SqlDbType.Int).Value = If(ca.counselor.HasValue, ca.counselor.Value, DBNull.Value)
                cmd.Parameters.Add("@appt_type", SqlDbType.Int).Value = If(ca.appt_type.HasValue, ca.appt_type.Value, DBNull.Value)
                cmd.Parameters.Add("@start_time", SqlDbType.DateTime).Value = ca.start_time
                cmd.Parameters.Add("@status", SqlDbType.VarChar, 1).Value = ca.status

                gdr.RowsAffected = cmd.ExecuteNonQuery()
                ca.Id = Convert.ToInt32(cmd.Parameters(0).Value)
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then
                cn.Dispose()
            End If
        End Try

        Return gdr
    End Function
End Module
