Imports System.Data.SqlClient
Imports System.Text

Public Module Clients

    Private Const SelectStatement_clients As String = "SELECT [client],[salutation],[AddressID],[HomeTelephoneID],[MsgTelephoneID],[county],[region],[active_status],[active_status_date],[dmp_status_date],[client_status],[client_status_date],[disbursement_date],[mail_error_date],[start_date],[restart_date],[drop_date],[drop_reason],[drop_reason_other],[referred_by],[cause_fin_problem1],[cause_fin_problem2],[cause_fin_problem3],[cause_fin_problem4],[office],[counselor],[csr],[hold_disbursements],[personal_checks],[ach_active],[stack_proration],[mortgage_problems],[intake_agreement],[ElectronicCorrespondence],[ElectronicStatements],[bankruptcy_class],[satisfaction_score],[fed_tax_owed],[state_tax_owed],[local_tax_owed],[fed_tax_months],[state_tax_months],[local_tax_months],[held_in_trust],[deposit_in_trust],[reserved_in_trust],[reserved_in_trust_cutoff],[marital_status],[language],[dependents],[household],[method_first_contact],[config_fee],[first_appt],[first_kept_appt],[first_resched_appt],[program_months],[first_deposit_date],[last_deposit_date],[last_deposit_amount],[preferred_contact],[InboundTelephoneGroupID],[created_by],[date_created],[DoNotDisturb] FROM clients WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id < 0 Then Throw New ArgumentOutOfRangeException("id can not be negative")
        Dim query As String = String.Format("{0} WHERE client={1:f0}", SelectStatement_clients, id)
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetStatusById(ByVal id As Int32) As GetScalarResult
        If id < 0 Then Throw New ArgumentOutOfRangeException("id can not be negative")
        Dim query As String = String.Format("SELECT active_status FROM clients WITH (NOLOCK) WHERE client={0:f0}", id)
        Return ExecuteScalar(query)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DropClientDebts(ByVal clientId As Int32, ByVal dropReason As Int32) As GetDataResult
        Return DropClientDebts(clientId, dropReason, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DropClientDebts(ByVal clientId As Int32, ByVal dropReason As Int32, ByVal txn As SqlTransaction) As GetDataResult

        ' Validate the parameters
        If clientId <= 0 Then Throw New ArgumentOutOfRangeException("clientId<=0")
        If dropReason <= 0 Then Throw New ArgumentOutOfRangeException("dropReason<=0")

        ' If there is no transaction then create one
        If txn Is Nothing OrElse txn.Connection Is Nothing Then
            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                privateDropClientDebts(clientId, dropReason, cn, Nothing)
                Return New DebtPlus.Repository.GetDataResult()
            End Using
        End If

        ' Use the passed transaction
        privateDropClientDebts(clientId, dropReason, txn.Connection, txn)
        Return New DebtPlus.Repository.GetDataResult()
    End Function

    Private Sub privateDropClientDebts(ByVal clientId As Int32, ByVal dropReason As Int32, cn As System.Data.SqlClient.SqlConnection, txn As System.Data.SqlClient.SqlTransaction)
        Using cmd As New System.Data.SqlClient.SqlCommand()
            cmd.Connection = cn
            cmd.Transaction = txn

            cmd.CommandText = "xpr_drop_client"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            cmd.Parameters.Add("@drop_reason", SqlDbType.Int).Value = dropReason
            cmd.Parameters.Add("@drop_reason_other", SqlDbType.VarChar, 50).Value = DBNull.Value

            cmd.ExecuteNonQuery()
        End Using
    End Sub

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordClientRow(ByVal dr As DataRow, ByVal clientId As Int32) As GetDataResult
        Return RecordClientRow(dr, clientId, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordClientRow(ByVal dr As DataRow, ByVal clientId As Int32, ByVal txn As SqlTransaction) As GetDataResult
        If dr Is Nothing Then Throw New ArgumentNullException("dr")

        Dim sb As New StringBuilder
        For Each col As DataColumn In dr.Table.Columns
            Dim CurrentItem As Object = dr(col)
            Dim OriginalItem As Object = dr(col, DataRowVersion.Original)
            If Common.Compare(CurrentItem, OriginalItem) <> 0 Then
                sb.AppendFormat(",[{0}]=@{0}", col.ColumnName)
            End If
        Next

        If sb.Length = 0 Then
            dr.AcceptChanges()
            Return New Repository.GetDataResult()
        End If

        sb.Remove(0, 1)
        sb.Insert(0, "UPDATE clients SET ")
        sb.Append(" WHERE [client]=@client")

        If txn Is Nothing OrElse txn.Connection Is Nothing Then
            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                privateRecordClientRow(dr, clientId, cn, Nothing, sb.ToString())
                Return New DebtPlus.Repository.GetDataResult()
            End Using
        End If

        privateRecordClientRow(dr, clientId, txn.Connection, txn, sb.ToString())
        Return New DebtPlus.Repository.GetDataResult()
    End Function

    Private Sub privateRecordClientRow(ByVal dr As DataRow, ByVal clientId As Int32, cn As System.Data.SqlClient.SqlConnection, txn As System.Data.SqlClient.SqlTransaction, ByVal cmdString As String)
        Using da As SqlDataAdapter = New SqlDataAdapter()
            Using updateCommand As SqlCommand = New SqlCommand(cmdString, cn)
                updateCommand.Transaction = txn
                updateCommand.CommandType = CommandType.Text
                updateCommand.Parameters.Add("@drop_date", SqlDbType.DateTime, 0, "drop_date")
                updateCommand.Parameters.Add("@active_status", SqlDbType.VarChar, 10, "active_status")
                updateCommand.Parameters.Add("@active_status_date", SqlDbType.DateTime, 0, "active_status_date")
                updateCommand.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                updateCommand.Parameters.Add("@bankruptcy_class", SqlDbType.Int, 0, "bankruptcy_class")
                updateCommand.Parameters.Add("@Cause_Fin_Problem1", SqlDbType.Int, 0, "Cause_Fin_Problem1")
                updateCommand.Parameters.Add("@Cause_Fin_Problem2", SqlDbType.Int, 0, "Cause_Fin_Problem2")
                updateCommand.Parameters.Add("@Cause_Fin_Problem3", SqlDbType.Int, 0, "Cause_Fin_Problem3")
                updateCommand.Parameters.Add("@Cause_Fin_Problem4", SqlDbType.Int, 0, "Cause_Fin_Problem4")
                updateCommand.Parameters.Add("@client_status", SqlDbType.VarChar, 10, "client_status")
                updateCommand.Parameters.Add("@config_fee", SqlDbType.Int, 0, "config_fee")
                updateCommand.Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                updateCommand.Parameters.Add("@county", SqlDbType.Int, 0, "county")
                updateCommand.Parameters.Add("@csr", SqlDbType.Int, 0, "csr")
                updateCommand.Parameters.Add("@dependents", SqlDbType.Int, 0, "dependents")
                updateCommand.Parameters.Add("@disbursement_date", SqlDbType.Int, 0, "disbursement_date")
                updateCommand.Parameters.Add("@dmp_status_date", SqlDbType.DateTime, 0, "dmp_status_date")
                updateCommand.Parameters.Add("@Drop_Reason", SqlDbType.Int, 0, "Drop_Reason")
                updateCommand.Parameters.Add("@Drop_Reason_other", SqlDbType.VarChar, 50, "Drop_Reason_other")
                updateCommand.Parameters.Add("@fed_tax_months", SqlDbType.Int, 0, "fed_tax_months")
                updateCommand.Parameters.Add("@fed_tax_owed", SqlDbType.Decimal, 0, "fed_tax_owed")
                updateCommand.Parameters.Add("@hold_disbursements", SqlDbType.Bit, 0, "hold_disbursements")
                updateCommand.Parameters.Add("@HomeTelephoneID", SqlDbType.Int, 0, "HomeTelephoneID")
                updateCommand.Parameters.Add("@household", SqlDbType.Int, 0, "household")
                updateCommand.Parameters.Add("@language", SqlDbType.Int, 0, "language")
                updateCommand.Parameters.Add("@local_tax_months", SqlDbType.Int, 0, "local_tax_months")
                updateCommand.Parameters.Add("@local_tax_owed", SqlDbType.Decimal, 0, "local_tax_owed")
                updateCommand.Parameters.Add("@mail_error_date", SqlDbType.DateTime, 0, "mail_error_date")
                updateCommand.Parameters.Add("@marital_status", SqlDbType.Int, 0, "marital_status")
                updateCommand.Parameters.Add("@mortgage_problems", SqlDbType.Bit, 0, "mortgage_problems")
                updateCommand.Parameters.Add("@MsgTelephoneID", SqlDbType.Int, 0, "MsgTelephoneID")
                updateCommand.Parameters.Add("@method_first_contact", SqlDbType.Int, 0, "method_first_contact")
                updateCommand.Parameters.Add("@office", SqlDbType.Int, 0, "office")
                updateCommand.Parameters.Add("@personal_checks", SqlDbType.Bit, 0, "personal_checks")
                updateCommand.Parameters.Add("@preferred_contact", SqlDbType.Int, 0, "preferred_contact")
                updateCommand.Parameters.Add("@Referred_By", SqlDbType.Int, 0, "Referred_By")
                updateCommand.Parameters.Add("@Region", SqlDbType.Int, 0, "Region")
                updateCommand.Parameters.Add("@reserved_in_trust", SqlDbType.Decimal, 0, "reserved_in_trust")
                updateCommand.Parameters.Add("@reserved_in_trust_cutoff", SqlDbType.DateTime, 0, "reserved_in_trust_cutoff")
                updateCommand.Parameters.Add("@salutation", SqlDbType.VarChar, 60, "salutation")
                updateCommand.Parameters.Add("@stack_proration", SqlDbType.Bit, 0, "stack_proration")
                updateCommand.Parameters.Add("@start_date", SqlDbType.DateTime, 0, "start_date")
                updateCommand.Parameters.Add("@state_tax_months", SqlDbType.Int, 0, "state_tax_months")
                updateCommand.Parameters.Add("@state_tax_owed", SqlDbType.Decimal, 0, "state_tax_owed")
                updateCommand.Parameters.Add("@ElectronicCorrespondence", SqlDbType.Int, 0, "ElectronicCorrespondence")
                updateCommand.Parameters.Add("@ElectronicStatements", SqlDbType.Int, 0, "ElectronicStatements")
                updateCommand.Parameters.Add("@satisfaction_score", SqlDbType.Int, 0, "satisfaction_score")
                updateCommand.Parameters.Add("@InboundTelephoneGroupID", SqlDbType.Int, 0, "InboundTelephoneGroupID")
                updateCommand.Parameters.Add("@client", SqlDbType.Int).Value = clientId

                da.UpdateCommand = updateCommand
                da.Update(New DataRow() {dr})
            End Using
        End Using
    End Sub
End Module
