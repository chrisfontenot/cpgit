Imports System.Data.SqlClient

Public Module HousingLenderServicers
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT [oID],[description],[ActiveFlag],[Default] FROM housing_lender_servicers WITH (NOLOCK)"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("SELECT [oID],[description],[ActiveFlag],[Default] FROM housing_lender_servicers WITH (NOLOCK) WHERE [oID]={0:f0}", id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_housing_lender_servicer"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
            .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 3, "ActiveFlag")
            .Parameters.Add("@Default", SqlDbType.Bit, 0, "DefaultRace")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE housing_lender_servicers SET [description]=@description,[ActiveFlag]=@ActiveFlag,[Default]=@Default WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
            .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 3, "ActiveFlag")
            .Parameters.Add("@Default", SqlDbType.Bit, 0, "DefaultRace")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM housing_lender_servicers WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
