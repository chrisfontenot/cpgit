
Public Module CreditorAddKeys

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByCreditorAndType(ByVal ds As DataSet, ByVal tableName As String, ByVal creditorId As Int32, ByVal type As String) As GetDataResult
        Dim query As String = String.Format("SELECT ca.type, ca.additional FROM creditor_addkeys ca INNER JOIN creditors cr ON ca.creditor = cr.creditor WHERE cr.creditor_id = {0:f0} AND ca.type = '{1}'", creditorId, type.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByCreditorAndType(ByVal ds As DataSet, ByVal tableName As String, ByVal creditorName As String, ByVal type As String) As GetDataResult
        Dim query As String = String.Format("SELECT ca.type, ca.additional FROM creditor_addkeys ca INNER JOIN creditors cr ON ca.creditor = cr.creditor WHERE cr.creditor = '{0}' AND ca.type = '{1}'", creditorName.Replace("'", "''"), type.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
