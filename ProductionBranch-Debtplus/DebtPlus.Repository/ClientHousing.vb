Imports System.Data.SqlClient

Public Module ClientHousing

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [client],[BackEnd_DTI],[DSA_CaseID],[DSA_NoDsaReason],[DSA_Program],[DSA_ProgramBenefit],[DSA_Specialist],[FrontEnd_DTI],[HECM_Certificate_Date],[HECM_Certificate_Expires],[HECM_Certificate_ID],[housing_status],[HUD_Assistance],[HUD_colonias],[HUD_DiscriminationVictim],[HUD_fair_housing],[HUD_FirstTimeHomeBuyer],[HUD_grant],[HUD_home_ownership_voucher],[HUD_housing_voucher],[HUD_LoanScam],[HUD_migrant_farm_worker],[HUD_predatory_lending],[NFMCP_decline_authorization],[NFMCP_level],[NFMCP_privacy_policy],[OtherHUDFunding] FROM client_housing WITH (NOLOCK) WHERE [client]={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal clientId As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_housing SET [BackEnd_DTI]=@BackEnd_DTI,[DSA_CaseID]=@DSA_CaseID,[DSA_NoDsaReason]=@DSA_NoDsaReason,[DSA_Program]=@DSA_Program,[DSA_ProgramBenefit]=@DSA_ProgramBenefit,[DSA_Specialist]=@DSA_Specialist,[FrontEnd_DTI]=@FrontEnd_DTI,[HECM_Certificate_Date]=@HECM_Certificate_Date,[HECM_Certificate_Expires]=@HECM_Certificate_Expires,[HECM_Certificate_ID]=@HECM_Certificate_ID,[housing_status]=@housing_status,[HUD_Assistance]=@HUD_Assistance,[HUD_colonias]=@HUD_colonias,[HUD_DiscriminationVictim]=@HUD_DiscriminationVictim,[HUD_fair_housing]=@HUD_fair_housing,[HUD_FirstTimeHomeBuyer]=@HUD_FirstTimeHomeBuyer,[HUD_grant]=@HUD_grant,[HUD_home_ownership_voucher]=@HUD_home_ownership_voucher,[HUD_housing_voucher]=@HUD_housing_voucher,[HUD_LoanScam]=@HUD_LoanScam,[HUD_migrant_farm_worker]=@HUD_migrant_farm_worker,[HUD_predatory_lending]=@HUD_predatory_lending,[NFMCP_decline_authorization]=@NFMCP_decline_authorization,[NFMCP_level]=@NFMCP_level,[NFMCP_privacy_policy]=@NFMCP_privacy_policy,[OtherHUDFunding]=@OtherHUDFunding WHERE [client]=@client"
            .CommandType = CommandType.Text
            .Parameters.Add("@client", System.Data.SqlDbType.Int, 4, "client")
            .Parameters.Add("@housing_status", System.Data.SqlDbType.Int, 0, "housing_status")
            .Parameters.Add("@HUD_fair_housing", System.Data.SqlDbType.Bit, 0, "HUD_fair_housing")
            .Parameters.Add("@HUD_colonias", System.Data.SqlDbType.Bit, 0, "HUD_colonias")
            .Parameters.Add("@HUD_migrant_farm_worker", System.Data.SqlDbType.Bit, 0, "HUD_migrant_farm_worker")
            .Parameters.Add("@HUD_predatory_lending", System.Data.SqlDbType.Bit, 0, "HUD_predatory_lending")
            .Parameters.Add("@HUD_FirstTimeHomeBuyer", System.Data.SqlDbType.Bit, 0, "HUD_FirstTimeHomeBuyer")
            .Parameters.Add("@HUD_DiscriminationVictim", System.Data.SqlDbType.Bit, 0, "HUD_DiscriminationVictim")
            .Parameters.Add("@HUD_LoanScam", System.Data.SqlDbType.Bit, 0, "HUD_LoanScam")
            .Parameters.Add("@HUD_Assistance", System.Data.SqlDbType.Int, 0, "HUD_Assistance")
            .Parameters.Add("@OtherHUDFunding", System.Data.SqlDbType.Decimal, 0, "OtherHUDFunding")
            .Parameters.Add("@HECM_Certificate_Date", System.Data.SqlDbType.DateTime, 0, "HECM_Certificate_Date")
            .Parameters.Add("@HECM_Certificate_Expires", System.Data.SqlDbType.DateTime, 0, "HECM_Certificate_Expires")
            .Parameters.Add("@HECM_Certificate_ID", System.Data.SqlDbType.VarChar, 80, "HECM_Certificate_ID")
            .Parameters.Add("@NFMCP_level", System.Data.SqlDbType.VarChar, 80, "NFMCP_level")
            .Parameters.Add("@NFMCP_privacy_policy", System.Data.SqlDbType.Bit, 0, "NFMCP_privacy_policy")
            .Parameters.Add("@NFMCP_decline_authorization", System.Data.SqlDbType.Bit, 0, "NFMCP_decline_authorization")
            .Parameters.Add("@HUD_home_ownership_voucher", System.Data.SqlDbType.Bit, 0, "HUD_home_ownership_voucher")
            .Parameters.Add("@HUD_housing_voucher", System.Data.SqlDbType.Bit, 0, "HUD_housing_voucher")
            .Parameters.Add("@FrontEnd_DTI", System.Data.SqlDbType.Float, 0, "FrontEnd_DTI")
            .Parameters.Add("@BackEnd_DTI", System.Data.SqlDbType.Float, 0, "BackEnd_DTI")
            .Parameters.Add("@HUD_grant", System.Data.SqlDbType.Int, 0, "HUD_grant")
            .Parameters.Add("@DSA_ProgramBenefit", System.Data.SqlDbType.Bit, 0, "DSA_ProgramBenefit")
            .Parameters.Add("@DSA_Program", System.Data.SqlDbType.Int, 0, "DSA_Program")
            .Parameters.Add("@DSA_NoDsaReason", System.Data.SqlDbType.Int, 0, "DSA_NoDsaReason")
            .Parameters.Add("@DSA_CaseID", System.Data.SqlDbType.Int, 0, "DSA_CaseID")
            .Parameters.Add("@DSA_Specialist", System.Data.SqlDbType.Int, 0, "DSA_Specialist")
        End With

        Try
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
End Module
