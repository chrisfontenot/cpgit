﻿
Imports System.Data.SqlClient

Public Module RppsResponseFiles

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetLabelByBatchId(ByVal batchId As Int32) As GetScalarResult
        If batchId <= 0 Then Throw New ArgumentOutOfRangeException("batchId must be positive")

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "SELECT [label] FROM rpps_response_files WHERE rpps_response_file = @rpps_response_file"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = batchId
            Return Repository.ExecuteScalar(cmd)
        End Using
    End Function

End Module
