Imports System.Data.SqlClient
Imports System.Text

Public Module People
#If False Then
    Private Const query As String = "SELECT [person],[MilitaryServiceID],[MilitaryDependentID],[MilitaryGradeID],[MilitaryStatusID],[CreditAgency],[Disabled],[birthdate],[CellTelephoneID],[client],[created_by],[date_created],[education],[EmailID],[employer],[fico_score],[NameID],[former],[Frequency],[gender],[gross_income],[Ethnicity],[emp_start_date],[emp_end_date],[job],[net_income],[no_fico_score_reason],[other_job],[race],[relation],[ssn],[WorkTelephoneID],[poa],[bkfileddate],[bkdischarge],[bkchapter],[bkPaymentCurrent],[bkAuthLetterDate] FROM people WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim queryString As String = String.Format("{0} WHERE client={1:f0}", query, clientId)
        Return FillNamedDataTableFromSelectCommand(queryString, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordPeopleRow(ByVal row As DataRow, ByVal clientId As Int32) As GetDataResult
        Return RecordPeopleRow(row, clientId, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordPeopleRow(ByVal row As DataRow, ByVal clientId As Int32, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If row Is Nothing Then Throw New ArgumentNullException("row")
        Dim gdr As New GetDataResult()

        Dim rowsUpdated As Int32

        Dim cn As SqlConnection

        If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
            cn = txn.Connection
        Else
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            cn.Open()
        End If

        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .Transaction = txn
            .CommandType = CommandType.StoredProcedure
            .CommandText = "xpr_insert_people"
            .Parameters.Add("@person", SqlDbType.Int, 0, "person").Direction = ParameterDirection.ReturnValue

            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
            .Parameters.Add("@relation", SqlDbType.Int, 0, "relation")
            .Parameters.Add("@NameID", SqlDbType.Int, 0, "NameID")
            .Parameters.Add("@CellTelephoneID", SqlDbType.Int, 0, "CellTelephoneID")
            .Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID")
            .Parameters.Add("@former", SqlDbType.VarChar, 80, "former")
            .Parameters.Add("@ssn", SqlDbType.VarChar, 20, "ssn")
            .Parameters.Add("@birthdate", SqlDbType.DateTime, 0, "birthdate")
            .Parameters.Add("@gender", SqlDbType.Int, 0, "gender")
            .Parameters.Add("@race", SqlDbType.Int, 0, "race")
            .Parameters.Add("@Ethnicity", SqlDbType.Int, 0, "Ethnicity")
            .Parameters.Add("@Disabled", SqlDbType.Int, 0, "Disabled")
            .Parameters.Add("@education", SqlDbType.Int, 0, "education")
            .Parameters.Add("@fico_score", SqlDbType.Int, 0, "fico_score")
            .Parameters.Add("@CreditAgency", SqlDbType.VarChar, 20, "CreditAgency")
            .Parameters.Add("@no_fico_score_reason", SqlDbType.Int, 0, "no_fico_score_reason")
            .Parameters.Add("@poa", SqlDbType.DateTime, 0, "poa")

            ' Employer/job fields
            .Parameters.Add("@employer", SqlDbType.Int, 0, "employer")
            .Parameters.Add("@gross_income", SqlDbType.Decimal, 0, "gross_income")
            .Parameters.Add("@net_income", SqlDbType.Decimal, 0, "net_income")
            .Parameters.Add("@emp_start_date", SqlDbType.DateTime, 0, "emp_start_date")
            .Parameters.Add("@emp_end_date", SqlDbType.DateTime, 0, "emp_end_date")
            .Parameters.Add("@job", SqlDbType.Int, 0, "job")
            .Parameters.Add("@Frequency", SqlDbType.Int, 0, "Frequency")
            .Parameters.Add("@WorkTelephoneID", SqlDbType.Int, 0, "WorkTelephoneID")
            .Parameters.Add("@other_job", SqlDbType.VarChar, 80, "other_job")

            ' Military fields
            .Parameters.Add("@MilitaryStatusID", SqlDbType.Int, 0, "MilitaryStatusID")
            .Parameters.Add("@MilitaryServiceID", SqlDbType.Int, 0, "MilitaryServiceID")
            .Parameters.Add("@MilitaryGradeID", SqlDbType.Int, 0, "MilitaryGradeID")
            .Parameters.Add("@MilitaryDependentID", SqlDbType.Int, 0, "MilitaryDependentID")

            'Bankruptcy Fileds
            .Parameters.Add("@bkfileddate", SqlDbType.DateTime, 0, "bkfileddate")
            .Parameters.Add("@bkdischarge", SqlDbType.DateTime, 0, "bkdischarge")
            .Parameters.Add("@bkchapter", SqlDbType.Int, 0, "bkchapter")
            .Parameters.Add("@bkPaymentCurrent", SqlDbType.Int, 0, "bkPaymentCurrent")
            .Parameters.Add("@bkAuthLetterDate", SqlDbType.DateTime, 0, "bkAuthLetterDate")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        If row.RowState = DataRowState.Modified Then
            Dim sb As New StringBuilder

            For Each col As DataColumn In row.Table.Columns

                ' Find the current and original versions of the column
                Dim CurrentItem As Object = row(col)
                Dim OriginalItem As Object = row(col, DataRowVersion.Original)

                ' If the value is changed then attempt to log it
                If Compare(CurrentItem, OriginalItem) <> 0 Then
                    sb.AppendFormat(",[{0}]=@{0}", col.ColumnName)
                End If
            Next

            If sb.Length = 0 Then
                row.AcceptChanges()
            Else
                sb.Remove(0, 1)
                sb.Insert(0, "UPDATE people SET ")
                sb.Append(" WHERE [person]=@person")

                da.UpdateCommand = updateCommand
                With updateCommand
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = sb.ToString()
                    .CommandType = CommandType.Text

                    .Parameters.Add("@relation", SqlDbType.Int, 0, "relation")
                    .Parameters.Add("@NameID", SqlDbType.Int, 0, "NameID")
                    .Parameters.Add("@CellTelephoneID", SqlDbType.Int, 0, "CellTelephoneID")
                    .Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID")
                    .Parameters.Add("@former", SqlDbType.VarChar, 80, "former")
                    .Parameters.Add("@ssn", SqlDbType.VarChar, 20, "ssn")
                    .Parameters.Add("@birthdate", SqlDbType.DateTime, 0, "birthdate")
                    .Parameters.Add("@gender", SqlDbType.Int, 0, "gender")
                    .Parameters.Add("@race", SqlDbType.Int, 0, "race")
                    .Parameters.Add("@Ethnicity", SqlDbType.Int, 0, "Ethnicity")
                    .Parameters.Add("@Disabled", SqlDbType.Int, 0, "Disabled")
                    .Parameters.Add("@education", SqlDbType.Int, 0, "education")
                    .Parameters.Add("@fico_score", SqlDbType.Int, 0, "fico_score")
                    .Parameters.Add("@CreditAgency", SqlDbType.VarChar, 20, "CreditAgency")
                    .Parameters.Add("@no_fico_score_reason", SqlDbType.Int, 0, "no_fico_score_reason")
                    .Parameters.Add("@poa", SqlDbType.DateTime, 0, "poa")

                    ' Employer/job fields
                    .Parameters.Add("@employer", SqlDbType.Int, 0, "employer")
                    .Parameters.Add("@gross_income", SqlDbType.Decimal, 0, "gross_income")
                    .Parameters.Add("@net_income", SqlDbType.Decimal, 0, "net_income")
                    .Parameters.Add("@emp_start_date", SqlDbType.DateTime, 0, "emp_start_date")
                    .Parameters.Add("@emp_end_date", SqlDbType.DateTime, 0, "emp_end_date")
                    .Parameters.Add("@job", SqlDbType.Int, 0, "job")
                    .Parameters.Add("@Frequency", SqlDbType.Int, 0, "Frequency")
                    .Parameters.Add("@WorkTelephoneID", SqlDbType.Int, 0, "WorkTelephoneID")
                    .Parameters.Add("@other_job", SqlDbType.VarChar, 80, "other_job")

                    ' Military fields
                    .Parameters.Add("@MilitaryStatusID", SqlDbType.Int, 0, "MilitaryStatusID")
                    .Parameters.Add("@MilitaryServiceID", SqlDbType.Int, 0, "MilitaryServiceID")
                    .Parameters.Add("@MilitaryGradeID", SqlDbType.Int, 0, "MilitaryGradeID")
                    .Parameters.Add("@MilitaryDependentID", SqlDbType.Int, 0, "MilitaryDependentID")

                    .Parameters.Add("@person", SqlDbType.Int, 0, "person")

                    'Bankruptcy Fileds
                    .Parameters.Add("@bkfileddate", SqlDbType.DateTime, 0, "bkfileddate")
                    .Parameters.Add("@bkdischarge", SqlDbType.DateTime, 0, "bkdischarge")
                    .Parameters.Add("@bkchapter", SqlDbType.Int, 0, "bkchapter")
                    .Parameters.Add("@bkPaymentCurrent", SqlDbType.Int, 0, "bkPaymentCurrent")
                    .Parameters.Add("@bkAuthLetterDate", SqlDbType.DateTime, 0, "bkAuthLetterDate")

                End With
            End If
        End If

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .Transaction = txn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM people WHERE [person]=@person"
            .Parameters.Add("@person", SqlDbType.Int, 0, "person")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            rowsUpdated = da.Update(New DataRow() {row})

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function
#End If
End Module
