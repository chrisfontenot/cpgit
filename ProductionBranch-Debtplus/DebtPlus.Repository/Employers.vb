Imports System.Data.SqlClient

Public Module Employers
#If False Then
    Private Const SelectStatement_employers = "SELECT [employer],[name],[Industry],[AddressID],[TelephoneID],[FAXID],[date_created],[created_by], dbo.format_address_block([AddressID]) AS address FROM [employers] WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Dim gdr = FillNamedDataTableSchemaFromSelectCommand(SelectStatement_employers, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "employer")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal employerId As Object) As GetDataResult
        If employerId Is Nothing OrElse employerId Is Convert.DBNull Then Throw RepositoryException.BadParameters()
        Dim gdr = FillNamedDataTableFromSelectCommand(String.Format("{0} WHERE [employer]={1:f0}", SelectStatement_employers, employerId), ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "employer")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllLikeKey(ByVal ds As DataSet, ByVal tableName As String, ByVal key As Char) As GetDataResult
        If key = "'"c Then Throw New ArgumentException("key can not be apostrophe")
        Dim gdr = FillNamedDataTableFromSelectCommand(String.Format("{0} WHERE [name] LIKE '{1}%'", SelectStatement_employers, key), ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "employer")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllNotLikeAtoZ(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Dim gdr = FillNamedDataTableFromSelectCommand(String.Format("{0} WHERE [name] NOT LIKE '[A-Z]%'", SelectStatement_employers), ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "employer")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_employers"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Employer").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@Name", SqlDbType.VarChar, 50, "name")
            .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
            .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
            .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
            .Parameters.Add("@Industry", SqlDbType.Int, 0, "Industry")
        End With

        Try
            da.InsertCommand = insertCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
