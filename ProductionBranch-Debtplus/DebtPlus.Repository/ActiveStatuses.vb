﻿Public Module ActiveStatuses

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT active_status, description FROM active_status WITH (NOLOCK)"
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            setPK(ds, tableName)
        End If
        Return gdr
    End Function

    Private Sub setPK(ByVal ds As DataSet, ByVal tableName As String)
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then
            Dim col As DataColumn = tbl.Columns("active_status")
            Debug.Assert(col IsNot Nothing)

            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                tbl.PrimaryKey = New DataColumn() {col}
            End If
        End If
    End Sub
End Module
