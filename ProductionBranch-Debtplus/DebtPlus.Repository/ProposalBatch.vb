﻿Imports System.Data.SqlClient

Public Module ProposalBatch

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByProposalBatchId(ByVal ds As DataSet, ByVal tableName As String, ByVal proposalBatchId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [proposal_batch_id],[proposal_type],[date_closed],[date_transmitted],[note],[bank],[date_created],[created_by] FROM proposal_batch_ids WHERE proposal_batch_id = {0:f0}", proposalBatchId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal startData As DateTime, ByVal endDate As DateTime, ByVal batchLabel As String, ByVal isStd As Int32, ByVal isFull As Int32, ByVal isRPPS As Int32) As Repository.GetDataResult
        Dim rslt As New DebtPlus.Repository.GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Try
            cn.Open()
            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "xpr_proposal_create_batch"
                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add("@from_date", SqlDbType.DateTime).Value = startData
                cmd.Parameters.Add("@to_date", SqlDbType.DateTime).Value = endDate
                cmd.Parameters.Add("@batch_label", SqlDbType.VarChar, 50).Value = batchLabel
                cmd.Parameters.Add("@include_pr_std", SqlDbType.Int).Value = isStd
                cmd.Parameters.Add("@include_pr_full", SqlDbType.Int).Value = isFull
                cmd.Parameters.Add("@include_pr_rpps", SqlDbType.Int).Value = isRPPS

                ' This can take longer than normal. Increase the timeout allowed.
                cmd.CommandTimeout = Math.Max(Setup.CommandTimeout, 600)

                ' Execute the command to create the batch
                rslt.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As SqlClient.SqlException
            rslt.HandleException(ex)
        Finally
            cn.Dispose()
        End Try

        Return rslt
    End Function

End Module
