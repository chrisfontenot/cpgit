﻿Imports System.Data.SqlClient

Public Module Ticklers
#If False Then
    Private Const SelectStatement_ticklers As String = "SELECT [tickler],[client],[counselor],[date_effective],[date_deleted],[tickler_type],[priority],[note],[date_created],[created_by] FROM ticklers WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [client]={1:f0}", SelectStatement_ticklers, clientId)
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            DebtPlus.Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "tickler")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByCounselorId(ByVal ds As DataSet, ByVal tableName As String, ByVal counselorId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [counselor]={1:f0}", SelectStatement_ticklers, counselorId)
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            DebtPlus.Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "tickler")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim insertCommand As SqlCommand = New SqlCommand()
        Dim updateCommand As SqlCommand = New SqlCommand()
        Dim deleteCommand As SqlCommand = New SqlCommand()

        Try
            cn.Open()

            Using da As SqlDataAdapter = New SqlDataAdapter()
                insertCommand.Connection = cn
                insertCommand.CommandText = "xpr_insert_ticklers"
                insertCommand.CommandType = CommandType.StoredProcedure
                insertCommand.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "tickler").Direction = ParameterDirection.ReturnValue
                insertCommand.Parameters.Add("@client", SqlDbType.Int, 0, "client")
                insertCommand.Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                insertCommand.Parameters.Add("@date_effective", SqlDbType.DateTime, 0, "date_effective")
                insertCommand.Parameters.Add("@tickler_type", SqlDbType.Int, 0, "tickler_type")
                insertCommand.Parameters.Add("@priority", SqlDbType.Int, 0, "priority")
                insertCommand.Parameters.Add("@note", SqlDbType.VarChar, 512, "note")
                da.InsertCommand = insertCommand

                updateCommand.Connection = cn
                updateCommand.CommandText = "UPDATE ticklers SET [client]=@client,[counselor]=@counselor,[date_effective]=@date_effective,[tickler_type]=@tickler_type,[priority]=@priority,[note]=@note WHERE [tickler]=@tickler"
                updateCommand.CommandType = CommandType.Text
                updateCommand.Parameters.Add("@client", SqlDbType.Int, 0, "client")
                updateCommand.Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
                updateCommand.Parameters.Add("@date_effective", SqlDbType.DateTime, 0, "date_effective")
                updateCommand.Parameters.Add("@tickler_type", SqlDbType.Int, 0, "tickler_type")
                updateCommand.Parameters.Add("@priority", SqlDbType.Int, 0, "priority")
                updateCommand.Parameters.Add("@note", SqlDbType.VarChar, 512, "note")
                updateCommand.Parameters.Add("@tickler", SqlDbType.Int, 0, "tickler")
                da.UpdateCommand = updateCommand

                deleteCommand.Connection = cn
                deleteCommand.CommandText = "UPDATE ticklers SET [date_deleted]=getdate() WHERE [tickler]=@tickler"
                deleteCommand.CommandType = CommandType.Text
                deleteCommand.Parameters.Add("@tickler", SqlDbType.Int, 0, "tickler")
                da.DeleteCommand = deleteCommand

                ccr.RowsAffected = da.Update(dt)
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            ccr.HandleException(ex)

        Finally
            updateCommand.Dispose()
            insertCommand.Dispose()
            deleteCommand.Dispose()
            cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
