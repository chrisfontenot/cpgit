Public Module ClientCreditorRegister

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByClientCreditorRegisterId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientCreditorRegisterId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [client_creditor_register],[client_creditor],[tran_type],[client],[creditor],[creditor_type],[disbursement_register],[trust_register],[invoice_register],[creditor_register],[credit_amt],[debit_amt],[fairshare_amt],[fairshare_pct],[account_number],[void],[date_created],[created_by] FROM registers_client_creditor WITH (NOLOCK) WHERE client_creditor_register={0:f0}", clientCreditorRegisterId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
