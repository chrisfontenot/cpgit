﻿Imports System.Data.SqlClient

Public Module LetterQueue
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT letter_queue,formatted_letter,letter_fields FROM letter_queue"
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByRef oID As Int32, ByVal queueName As String, ByVal letterType As Int32, ByVal clientId As Int32, ByVal creditor As String, ByVal topMargin As Double, ByVal bottomMargin As Double, ByVal leftMargin As Double, ByVal rightMargin As Double) As GetDataResult
        Return Insert(oID, queueName, letterType, clientId, creditor, topMargin, bottomMargin, leftMargin, rightMargin, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByRef oID As Int32, ByVal queueName As String, ByVal letterType As Int32, ByVal clientId As Int32, ByVal creditor As String, ByVal topMargin As Double, ByVal bottomMargin As Double, ByVal leftMargin As Double, ByVal rightMargin As Double, ByVal txn As SqlTransaction) As GetDataResult

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "xpr_letter_queue_insert"
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add("@queue_name", SqlDbType.VarChar, 80).Value = queueName
            cmd.Parameters.Add("@letter_type", SqlDbType.Int).Value = letterType
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId
            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor

            ' Include the margin settings. They'll be needed later when it is printed.
            cmd.Parameters.Add("@top_margin", SqlDbType.Float).Value = topMargin  ' Hundredth's of an inch -> inches
            cmd.Parameters.Add("@bottom_margin", SqlDbType.Float).Value = bottomMargin
            cmd.Parameters.Add("@left_margin", SqlDbType.Float).Value = leftMargin
            cmd.Parameters.Add("@right_margin", SqlDbType.Float).Value = rightMargin

            Dim gdr As GetDataResult = ExecuteNonQuery(cmd, txn)
            If gdr.Success Then
                oID = Convert.ToInt32(cmd.Parameters(0).Value)
            End If

            Return gdr
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal queueId As Int32) As CommitChangesResult
        Return CommitChanges(dt, queueId, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal queueId As Int32, ByVal txn As SqlTransaction) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection

        If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
            cn = txn.Connection
        Else
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            cn.Open()
        End If

        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .Transaction = txn
            .CommandText = "UPDATE letter_queue SET [formatted_letter]=@formatted_letter,[letter_fields]=@letter_fields WHERE letter_queue=@letter_queue"
            .CommandType = CommandType.Text
            .CommandText = "UPDATE letter_queue SET [formatted_letter]=@formatted_letter,[letter_fields]=@letter_fields WHERE letter_queue=@letter_queue"
            .Parameters.Add("@formatted_letter", SqlDbType.Text, Int32.MaxValue, "formatted_letter")
            .Parameters.Add("@letter_fields", SqlDbType.Text, Int32.MaxValue, "letter_fields")
            .Parameters.Add("@letter_queue", SqlDbType.Int).Value = queueId
        End With

        Try
            da.InsertCommand = updateCommand    ' I don't know why, but the original code assigned the update command to the da's insert command

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
