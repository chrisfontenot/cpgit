Imports System.Data.SqlClient
Imports Guidelight.Data

Public Module Names
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetManyNamesByNameIDs(ByVal ds As DataSet, ByVal tableName As String, ByVal nameIDs As String) As GetDataResult
        If String.IsNullOrWhiteSpace(tableName) OrElse String.IsNullOrWhiteSpace(nameIDs) Then Throw RepositoryException.BadParameters()

        ' we could fail here because of a mal-formed nameIDs string
        Dim query As String = String.Format("SELECT name,first,last FROM Names WHERE Name IN ({0})", nameIDs.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetManyNamesByNameIDs2(ByVal ds As DataSet, ByVal tableName As String, ByVal nameIDs As String) As GetDataResult
        If String.IsNullOrWhiteSpace(tableName) OrElse String.IsNullOrWhiteSpace(nameIDs) Then Throw RepositoryException.BadParameters()

        ' we could fail here because of a mal-formed nameIDs string
        Dim query As String = String.Format("SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WHERE Name IN ({0})", nameIDs.Replace("'", "''"))
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Const query As String = "SELECT name,first,last FROM Names"
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Const query As String = "SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WITH(NOLOCK)"
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function GetNameById(ByRef n As DebtPlus.Interfaces.Names.IName, ByVal oId As Object) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        If oId Is Nothing OrElse oId Is System.DBNull.Value Then Throw New ArgumentNullException("oId")

        Dim id As Int32 = Convert.ToInt32(oId)
        If id < 1 Then Throw New ArgumentOutOfRangeException("oId")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim reader As SqlDataReader = Nothing

        Dim gdr As New GetDataResult()

        Try
            cmd.Connection = cn
            cmd.CommandText = "SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM names WITH (NOLOCK) WHERE Name=@NameID"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@NameID", SqlDbType.Int).Value = id

            cn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
            If reader IsNot Nothing AndAlso reader.Read() Then
                gdr.RowsAffected = 1
                n = MapRowToModel(reader)
            End If

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If reader IsNot Nothing Then reader.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetNameById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM names WITH (NOLOCK) WHERE Name={0:f0}", id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function InsertName(ByVal name As DebtPlus.Interfaces.Names.IName) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If name Is Nothing Then Throw New ArgumentNullException("name")
        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand

        Try
            cmd.Connection = cn
            cmd.CommandText = "xpr_insert_names"
            cmd.CommandType = CommandType.StoredProcedure
            MapModelToCommandParams(name, cmd)
            cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

            cn.Open()
            cmd.ExecuteNonQuery()
            name.Id = cmd.Parameters("RETURN").Value

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateName(ByVal name As DebtPlus.Interfaces.Names.IName) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If name Is Nothing Then Throw New ArgumentNullException("name")

        Dim gdr As New GetDataResult()
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand

        Try
            cmd.Connection = cn
            cmd.CommandText = "UPDATE Names SET [prefix]=@prefix,[first]=@first,[middle]=@middle,[last]=@last,[suffix]=@suffix WHERE Name=@NameID"
            cmd.CommandType = CommandType.Text
            MapModelToCommandParams(name, cmd)
            cmd.Parameters.Add("@NameID", SqlDbType.Int).Value = name.Id

            cn.Open()
            gdr.RowsAffected = cmd.ExecuteNonQuery

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_names"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix")
            .Parameters.Add("@first", SqlDbType.VarChar, 80, "first")
            .Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle")
            .Parameters.Add("@last", SqlDbType.VarChar, 80, "last")
            .Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix")
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Name").Direction = ParameterDirection.ReturnValue
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE Names SET [prefix]=@prefix, [first]=@first, [middle]=@middle, [last]=@last, [suffix]=@suffix WHERE [Name]=@Name"
            .CommandType = CommandType.Text
            .Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix")
            .Parameters.Add("@first", SqlDbType.VarChar, 80, "first")
            .Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle")
            .Parameters.Add("@last", SqlDbType.VarChar, 80, "last")
            .Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix")
            .Parameters.Add("@Name", SqlDbType.Int, 0, "Name")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM Names WHERE [Name]=@Name"
            .CommandType = CommandType.Text
            .Parameters.Add("@Name", SqlDbType.Int, 0, "Name")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    ''' SqlDataReader row to Model.Name - with Id
    <Obsolete("Use LINQ Tables")> _
    Private Function MapRowToModel(ByVal reader As SqlDataReader) As DebtPlus.Interfaces.Names.IName
        Dim name As New DebtPlus.LINQ.Name()
        name.Id = reader.SafeGetIntOrZero("Name")
        name.Prefix = reader.SafeGetString("prefix")
        name.First = reader.SafeGetString("first")
        name.Middle = reader.SafeGetString("middle")
        name.Last = reader.SafeGetString("last")
        name.Suffix = reader.SafeGetString("suffix")
        Return name
    End Function

    ' Model.Name to SqlCommand Parameters - Without Id
    <Obsolete("Use LINQ Tables")> _
    Private Sub MapModelToCommandParams(ByVal name As DebtPlus.Interfaces.Names.IName, ByRef cmd As SqlCommand)
        If name Is Nothing OrElse cmd Is Nothing Then Throw New Exception("Bad parameters")

        cmd.Parameters.Add("@prefix", SqlDbType.VarChar, 10).SafeSetString(name.Prefix)
        cmd.Parameters.Add("@first", SqlDbType.VarChar, 60).SafeSetString(name.First)
        cmd.Parameters.Add("@middle", SqlDbType.VarChar, 60).SafeSetString(name.Middle)
        cmd.Parameters.Add("@last", SqlDbType.VarChar, 60).SafeSetString(name.Last)
        cmd.Parameters.Add("@suffix", SqlDbType.VarChar, 10).SafeSetString(name.Suffix)
    End Sub
#End If
End Module
