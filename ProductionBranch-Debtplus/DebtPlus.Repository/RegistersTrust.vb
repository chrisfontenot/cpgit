﻿Imports System.Data.SqlClient

Public Module RegistersTrust

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetBankIdByTrustRegisterId(ByVal trustRegisterId As Int32) As GetScalarResult
        Return GetBankIdByTrustRegisterId(trustRegisterId, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetBankIdByTrustRegisterId(ByVal trustRegisterId As Int32, ByVal txn As SqlTransaction) As GetScalarResult
        Dim query As String = String.Format("SELECT bank FROM registers_trust WHERE trust_register={0:f0}", trustRegisterId)
        Return ExecuteScalar(query, txn)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetBankIdBySequenceNumber(ByVal sequenceNumber As String) As GetScalarResult
        Return GetBankIdBySequenceNumber(sequenceNumber, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetBankIdBySequenceNumber(ByVal sequenceNumber As String, ByVal txn As SqlTransaction) As GetScalarResult
        Dim query As String = String.Format("SELECT TOP 1 bank FROM registers_trust WHERE sequence_number='{0}' AND tran_type in ('AD','MD','CM','CR','AR') AND ltrim(rtrim(cleared)) = ''", sequenceNumber.Replace("'", "''"))
        Return ExecuteScalar(query, txn)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetTrustRegisterBySequenceNumber(ByVal sequenceNumber As String) As GetScalarResult
        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "SELECT trust_register FROM registers_trust WHERE sequence_number=@marker"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@marker", SqlDbType.VarChar, 80).Value = sequenceNumber
            Return ExecuteScalar(cmd)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateSequenceNumberByTrustRegisterId(ByVal trustRegisterId As Int32, ByVal sequenceNumber As String) As GetDataResult
        Return UpdateSequenceNumberByTrustRegisterId(trustRegisterId, sequenceNumber, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateSequenceNumberByTrustRegisterId(ByVal trustRegisterId As Int32, ByVal sequenceNumber As String, ByVal txn As SqlTransaction) As GetDataResult
        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "UPDATE registers_trust SET sequence_number=@sequence_number WHERE trust_register=@trust_register"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@sequence_number", SqlDbType.VarChar, 80).Value = sequenceNumber
            cmd.Parameters.Add("@trust_register", SqlDbType.Int).Value = trustRegisterId
            Return ExecuteNonQuery(cmd, txn)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateCheckNumberAndClearedByTrustRegisterId(ByVal trustRegisterId As Int32, ByVal checkNumber As String) As GetDataResult
        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "UPDATE registers_trust SET cleared='P',checknum=@checknum WHERE trust_register=@trust_register"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@checknum", SqlDbType.VarChar, 20).Value = checkNumber
            cmd.Parameters.Add("@trust_register", SqlDbType.Int).Value = trustRegisterId
            Return ExecuteNonQuery(cmd)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateSequenceNumbersByBank(ByVal bank As Int32, ByVal sequenceNumber As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 Then Throw New ArgumentOutOfRangeException("Bank out of range")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "UPDATE registers_trust SET sequence_number=@sequence_number WHERE cleared = 'P' AND tran_type in ('AD','MD','CM','CR','AR') AND amount > 0 AND bank=@bank"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@sequence_number", SqlDbType.VarChar, 80).Value = sequenceNumber
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateSequenceNumbersByBankAndSequenceNumber(ByVal bank As Int32, ByVal oldSequenceNumber As String, ByVal newSequenceNumber As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 Then Throw New ArgumentOutOfRangeException("Bank number out of range")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "UPDATE registers_trust SET sequence_number=@newSequenceNumber WHERE cleared = 'P' AND tran_type in ('AD','MD','CM','CR','AR') AND amount > 0 AND bank=@bank AND sequence_number=@oldSequenceNumber"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@newSequenceNumber", SqlDbType.VarChar, 80).Value = newSequenceNumber
                cmd.Parameters.Add("@oldSequenceNumber", SqlDbType.VarChar, 80).Value = oldSequenceNumber
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateOneSequenceNumberByBank(ByVal bank As Int32, ByVal sequenceNumber As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 Then Throw New ArgumentOutOfRangeException("Bank number out of range")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SET ROWCOUNT 1; UPDATE registers_trust SET sequence_number=@sequence_number WHERE cleared = 'P' AND tran_type in ('AD','MD','CM','CR','AR') AND amount > 0 AND bank=@bank; SET ROWCOUNT 0;"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@sequence_number", SqlDbType.VarChar, 80).Value = sequenceNumber
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateOneSequenceNumberByBankAndSequenceNumber(ByVal bank As Int32, ByVal oldSequenceNumber As String, ByVal newSequenceNumber As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 Then Throw New ArgumentOutOfRangeException("bank must be greater than zero")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SET ROWCOUNT 1; UPDATE registers_trust SET sequence_number=@newSequenceNumber WHERE cleared = 'P' AND tran_type in ('AD','MD','CM','CR','AR') AND amount > 0 AND bank=@bank AND sequence_number=@oldSequenceNumber; SET ROWCOUNT 0;"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@newSequenceNumber", SqlDbType.VarChar, 80).Value = newSequenceNumber
                cmd.Parameters.Add("@oldSequenceNumber", SqlDbType.VarChar, 80).Value = oldSequenceNumber
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCheckCountAndBalanceByBank(ByVal bank As Int32, ByRef checkCount As Int32, ByRef checkBalance As Decimal) As Boolean
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 Then Return False

        Dim rd As SqlDataReader = Nothing
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand()

        Try
            cmd.Connection = cn
            cmd.CommandText = "SELECT COUNT(*) AS items, SUM(amount) AS amount FROM registers_trust WITH (NOLOCK) WHERE bank=@bank AND cleared = 'P' AND tran_type IN ('AD','MD','CM','CR','AR')"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank

            cn.Open()

            rd = cmd.ExecuteReader()

            If rd.Read Then
                If Not rd.IsDBNull(0) Then checkCount = Convert.ToInt32(rd.GetValue(0))
                If Not rd.IsDBNull(1) Then checkBalance = Convert.ToDecimal(rd.GetValue(1))
            End If

        Finally
            If cn IsNot Nothing Then cn.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If rd IsNot Nothing Then rd.Dispose()
        End Try

        Return True
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCheckCountAndBalanceByBankAndCheckId(ByVal bank As Int32, ByVal checksIdentifier As String, ByRef checkCount As Int32, ByRef checkBalance As Decimal) As Boolean
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If bank <= 0 OrElse String.IsNullOrWhiteSpace(checksIdentifier) Then Return False

        Const query As String = "SELECT COUNT(*) AS items, SUM(amount) AS amount FROM registers_trust WITH (NOLOCK) WHERE bank=@bank AND cleared = 'P' AND tran_type IN ('AD','MD','CM','CR','AR') AND sequence_number=@checksIdentifier"

        Dim rd As SqlDataReader = Nothing
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()

            Using cmd As New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = query
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank
                cmd.Parameters.Add("@checksIdentifier", SqlDbType.VarChar, 50).Value = checksIdentifier
                rd = cmd.ExecuteReader()
            End Using

            If rd.Read Then
                If Not rd.IsDBNull(0) Then checkCount = Convert.ToInt32(rd.GetValue(0))
                If Not rd.IsDBNull(1) Then checkBalance = Convert.ToDecimal(rd.GetValue(1))
            End If

        Finally
            If rd IsNot Nothing Then rd.Dispose()
            cn.Dispose()
        End Try

        Return True
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateManySequenceNumbersBySequenceNumberAndBankId(ByVal oldSequenceNumber As String, ByVal bankId As Int32, ByVal firstCheckNumber As Int32, ByVal lastCheckNumber As Int32, ByVal newSequenceNumber As String) As GetDataResult
        Return UpdateManySequenceNumbersBySequenceNumberAndBankId(oldSequenceNumber, bankId, firstCheckNumber, lastCheckNumber, newSequenceNumber, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateManySequenceNumbersBySequenceNumberAndBankId(ByVal oldSequenceNumber As String, ByVal bankId As Int32, ByVal firstCheckNumber As Int32, ByVal lastCheckNumber As Int32, ByVal newSequenceNumber As String, ByVal txn As SqlTransaction) As GetDataResult
        Dim query As String = String.Format("UPDATE [registers_trust] SET [sequence_number]='{1}' WHERE [sequence_number]='{0}' AND [tran_type] in ('AD','MD','CM','CR','AR') AND ltrim(rtrim([cleared])) = '' AND [bank] = {2:f0}", oldSequenceNumber.Replace("'", "''"), newSequenceNumber.Replace("'", "''"), bankId)
        If firstCheckNumber > 0 Then
            query += String.Format(" AND convert(int,[checknum]) >= {0:f0}", firstCheckNumber)

            If lastCheckNumber >= firstCheckNumber Then
                query += String.Format(" AND convert(int,[checknum]) <= {0:f0}", lastCheckNumber)
            End If
        End If

        Return ExecuteNonQuery(query, txn)
    End Function
End Module
