﻿Imports System.Data.SqlClient

Public Module HudTransactions
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT t.[hud_transaction],t.[hud_interview],t.[minutes],t.[GrantAmountUsed],t.[client_appointment],t.[date_created],t.[created_by] FROM hud_transactions t WITH (NOLOCK) INNER JOIN hud_interviews i WITH (NOLOCK) ON t.[hud_interview] = i.[hud_interview] WHERE i.[client]={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_housing_transaction"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@hud_transaction", SqlDbType.Int, 0, "hud_transaction").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@hud_interview", SqlDbType.Int, 0, "hud_interview")
            .Parameters.Add("@minutes", SqlDbType.Int, 0, "minutes")
            .Parameters.Add("@GrantAmountUsed", SqlDbType.Decimal, 0, "GrantAmountUsed")
            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE [hud_transactions] SET [minutes]=@minutes,[GrantAmountUsed]=@GrantAmountUsed,[client_appointment]=@client_appointment WHERE [hud_transaction]=@hud_transaction"
            .CommandType = CommandType.Text
            .Parameters.Add("@minutes", SqlDbType.Int, 0, "minutes")
            .Parameters.Add("@GrantAmountUsed", SqlDbType.Decimal, 0, "GrantAmountUsed")
            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
            .Parameters.Add("@hud_transaction", SqlDbType.Int, 0, "hud_transaction")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM [hud_transactions] WHERE [hud_transaction]=@hud_transaction"
            .CommandType = CommandType.Text
            .Parameters.Add("@hud_transaction", SqlDbType.Int, 0, "hud_transaction")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
