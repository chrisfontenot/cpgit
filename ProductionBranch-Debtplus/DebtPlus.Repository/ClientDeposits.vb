Imports System.Data.SqlClient

Public Module ClientDeposits
    Private Const SelectStatement_ClientDeposits As String = "SELECT [client_deposit],[client],[deposit_date],[deposit_amount],[one_time],[date_created],[created_by] FROM [client_deposits] WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_ClientDeposits, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client_deposit")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal ClientId As Int32) As GetDataResult
        Dim gdr = FillNamedDataTableFromSelectCommand(String.Format("{0} WHERE [client]={1:f0}", SelectStatement_ClientDeposits, ClientId), ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client_deposit")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetDepositsInTrustByClientId(ByVal clientId As Int32) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientId")

        Dim gsr As New GetScalarResult()
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Try
            cn.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT dbo.DepositsInTrust(@client)"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId

                gsr.Result = cmd.ExecuteScalar()
                If gsr.Result Is Nothing OrElse gsr.Result Is System.DBNull.Value Then
                    gsr.Result = 0D
                End If
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gsr.HandleException(ex)

        Finally
            cn.Dispose()
        End Try

        Return gsr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim insertCommand As SqlCommand = New SqlCommand()
        Dim updateCommand As SqlCommand = New SqlCommand()
        Dim deleteCommand As SqlCommand = New SqlCommand()

        Try
            cn.Open()
            Using da As SqlDataAdapter = New SqlDataAdapter()
                With insertCommand
                    .UpdatedRowSource = UpdateRowSource.FirstReturnedRecord
                    .Connection = cn
                    .CommandText = String.Format("INSERT INTO [client_deposits] ([client],[deposit_date],[deposit_amount],[one_time]) VALUES (@client,@deposit_date,@deposit_amount,@one_time); {0} WHERE [client_deposit]=scope_identity();", SelectStatement_ClientDeposits)
                    .CommandType = CommandType.Text
                    .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                    .Parameters.Add("@deposit_date", SqlDbType.VarChar, 0, "deposit_date")
                    .Parameters.Add("@deposit_amount", SqlDbType.Decimal, 0, "deposit_amount")
                    .Parameters.Add("@one_time", SqlDbType.Bit, 0, "one_time")
                End With
                da.InsertCommand = insertCommand

                With updateCommand
                    .Connection = cn
                    .CommandText = "UPDATE [client_deposits] SET [deposit_date]=@deposit_date,[deposit_amount]=@deposit_amount,[one_time]=@one_time WHERE [client_deposit]=@client_deposit"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@deposit_date", SqlDbType.VarChar, 0, "deposit_date")
                    .Parameters.Add("@deposit_amount", SqlDbType.Decimal, 0, "deposit_amount")
                    .Parameters.Add("@one_time", SqlDbType.Bit, 0, "one_time")
                    .Parameters.Add("@client_deposit", SqlDbType.Int, 0, "client_deposit")
                End With
                da.UpdateCommand = updateCommand

                With deleteCommand
                    .Connection = cn
                    .CommandText = "DELETE FROM [client_deposits] WHERE [client_deposit]=@client_deposit"
                    .Parameters.Add("@client_deposit", SqlDbType.Int, 0, "client_deposit")
                End With
                da.DeleteCommand = deleteCommand

                ccr.RowsAffected = da.Update(dt)
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            ccr.HandleException(ex)

        Finally
            updateCommand.Dispose()
            insertCommand.Dispose()
            deleteCommand.Dispose()
            cn.Dispose()
        End Try

        Return ccr
    End Function
End Module
