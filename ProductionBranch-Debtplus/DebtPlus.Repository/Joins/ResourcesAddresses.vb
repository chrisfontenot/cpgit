Namespace Joins

    Public Module ResourcesAddresses

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT r.Resource, r.Resource_type, r.Name, r.AddressID, r.TelephoneID, r.FAXID, r.EmailID, r.WWW, r.date_created, r.created_by, a.city, a.state, a.postalcode, dbo.format_city_state_zip (a.city, a.state, a.postalcode) as address3 FROM resources r WITH (NOLOCK) LEFT OUTER JOIN addresses a WITH (NOLOCK) ON r.AddressID = a.Address ORDER BY r.name"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

    End Module

End Namespace
