﻿Namespace Joins
    Public Module StatesCountries

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT s.state, s.MailingCode, s.Name, s.Country, s.TimeZoneID, s.USAFormat, s.Default, s.ActiveFlag, s.AddressFormat, s.FIPS, s.hud_9902, s.DoingBusiness, s.NegativeDebtWarning, s.created_by, s.date_created, c.description as FormattedCountry FROM states s WITH (NOLOCK) LEFT OUTER JOIN countries c WITH (NOLOCK) ON s.country = c.country"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT s.State, s.MailingCode, s.Name, c.description as Country, s.[default], s.[ActiveFlag] FROM states s WITH (NOLOCK) LEFT OUTER JOIN countries c ON s.country = c.country ORDER BY s.country, s.MailingCode, s.name"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function
    End Module
End Namespace