﻿<System.Obsolete("Use LINQ Tables")> _
Public Class CommitChangesResult
    Implements Interfaces.IRepositoryResult

    Public ReadOnly Property DataServiceMethod As String Implements Interfaces.IRepositoryResult.DataServiceMethod
        Get
            Return "CommitChanges"
        End Get
    End Property

    Public Property PendingInserts As Int32 = 0
    Public Property PendingUpdates As Int32 = 0
    Public Property PendingDeletes As Int32 = 0

    Public Sub New(dt As DataTable)
        If dt Is Nothing Then Return

        For Each dr As DataRow In dt.Rows
            UpdatePendingStates(dr)
        Next
    End Sub

    Public Sub New(dr As DataRow)
        If dr Is Nothing Then Return
        UpdatePendingStates(dr)
    End Sub

    Public Sub New(drs As DataRow())
        If drs Is Nothing Then Return
        For Each dr As DataRow In drs
            UpdatePendingStates(dr)
        Next
    End Sub

    Private Sub UpdatePendingStates(dr As DataRow)
        If dr Is Nothing Then Return

        If dr.RowState = DataRowState.Added Then
            PendingInserts += 1
        ElseIf dr.RowState = DataRowState.Modified Then
            PendingUpdates += 1
        ElseIf dr.RowState = DataRowState.Deleted Then
            PendingDeletes += 1
        End If
    End Sub

    ''' <summary>
    ''' Number of rows affected by the last request
    ''' </summary>
    ''' <value>The Int32 value of the number of rows</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property RowsAffected As Int32 = 0 Implements Interfaces.IRepositoryResult.RowsAffected

    Public ReadOnly Property Success() As Boolean Implements Interfaces.IRepositoryResult.Success
        Get
            Return (PendingInserts + PendingUpdates + PendingDeletes) = RowsAffected
        End Get
    End Property

    Public Overridable Function HandleException(ex As System.Exception) As Interfaces.IRepositoryResult Implements Interfaces.IRepositoryResult.HandleException
        If ex IsNot Nothing Then

            ' When we do an update, the update may result in zero rows being updated. IGNORE IT.
            If Not TypeOf ex Is DBConcurrencyException Then
                ex = ex
            End If
        End If

        Return Me
    End Function

    Public Property ex As System.Exception = Nothing Implements Interfaces.IRepositoryResult.ex

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
