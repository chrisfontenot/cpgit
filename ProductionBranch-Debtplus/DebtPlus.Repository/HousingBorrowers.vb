Imports System.Data.SqlClient

Public Module HousingBorrowers
#If False Then
    Private Const query As String = "SELECT [oID],[PersonID],[NameID],[EmailID],[Former],[SSN],[Gender],[Race],[Ethnicity],[Language],[Disabled],[MilitaryServiceID],[MilitaryDependentID],[MilitaryGradeID],[MilitaryStatusID],[marital_status],[Education],[Birthdate],[FICO_Score],[CreditAgency],[emp_start_date],[emp_end_date],[other_job],[bkfileddate],[bkdischarge],[bkchapter],[bkPaymentCurrent],[bkAuthLetterDate] FROM housing_borrowers WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim queryString As String = String.Format("{0} WHERE [oID]={1:f0}", query, id)
        Return FillNamedDataTableFromSelectCommand(queryString, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult

        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)
        Try
            Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()

                Using da As SqlDataAdapter = New SqlDataAdapter
                    Using insertCommand As SqlCommand = New SqlCommand()
                        insertCommand.Connection = cn
                        insertCommand.CommandText = "xpr_insert_housing_borrower"
                        insertCommand.CommandType = CommandType.StoredProcedure
                        insertCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
                        insertCommand.Parameters.Add("@PersonID", SqlDbType.Int, 4, "PersonID")
                        insertCommand.Parameters.Add("@NameID", SqlDbType.Int, 4, "NameID")
                        insertCommand.Parameters.Add("@EmailID", SqlDbType.Int, 4, "EmailID")
                        insertCommand.Parameters.Add("@Former", SqlDbType.VarChar, 80, "Former")
                        insertCommand.Parameters.Add("@SSN", SqlDbType.VarChar, 80, "SSN")
                        insertCommand.Parameters.Add("@Gender", SqlDbType.Int, 4, "Gender")
                        insertCommand.Parameters.Add("@Race", SqlDbType.Int, 4, "Race")
                        insertCommand.Parameters.Add("@Ethnicity", SqlDbType.Int, 4, "Ethnicity")
                        insertCommand.Parameters.Add("@Language", SqlDbType.Int, 4, "Language")
                        insertCommand.Parameters.Add("@Disabled", SqlDbType.Bit, 0, "Disabled")
                        insertCommand.Parameters.Add("@marital_status", SqlDbType.Int, 4, "marital_status")
                        insertCommand.Parameters.Add("@Education", SqlDbType.Int, 4, "Education")
                        insertCommand.Parameters.Add("@Birthdate", SqlDbType.DateTime, 8, "Birthdate")
                        insertCommand.Parameters.Add("@FICO_Score", SqlDbType.Int, 4, "FICO_Score")
                        insertCommand.Parameters.Add("@CreditAgency", SqlDbType.VarChar, 80, "CreditAgency")
                        insertCommand.Parameters.Add("@emp_start_date", SqlDbType.DateTime, 8, "emp_start_date")
                        insertCommand.Parameters.Add("@emp_end_date", SqlDbType.DateTime, 8, "emp_end_date")
                        insertCommand.Parameters.Add("@other_job", SqlDbType.VarChar, 80, "other_job")
                        insertCommand.Parameters.Add("@MilitaryServiceID", SqlDbType.Int, 4, "MilitaryServiceID")
                        insertCommand.Parameters.Add("@MilitaryDependentID", SqlDbType.Int, 4, "MilitaryDependentID")
                        insertCommand.Parameters.Add("@MilitaryGradeID", SqlDbType.Int, 4, "MilitaryGradeID")
                        insertCommand.Parameters.Add("@MilitaryStatusID", SqlDbType.Int, 4, "MilitaryStatusID")

                        'Bankruptcy fields
                        insertCommand.Parameters.Add("@bkfileddate", SqlDbType.DateTime, 8, "bkfileddate")
                        insertCommand.Parameters.Add("@bkdischarge", SqlDbType.DateTime, 8, "bkdischarge")
                        insertCommand.Parameters.Add("@bkchapter", SqlDbType.Int, 4, "bkchapter")
                        insertCommand.Parameters.Add("@bkPaymentCurrent", SqlDbType.Int, 4, "bkPaymentCurrent")
                        insertCommand.Parameters.Add("@bkAuthLetterDate", SqlDbType.DateTime, 8, "bkAuthLetterDate")

                        da.InsertCommand = insertCommand

                        Using updateCommand As SqlCommand = New SqlCommand()
                            updateCommand.Connection = cn
                            updateCommand.CommandText = "UPDATE housing_Borrowers SET [PersonID]=@PersonID,[NameID]=@NameID,[EmailID]=@EmailID,[Former]=@Former,[SSN]=@SSN,[Gender]=@Gender,[Race]=@Race,[Ethnicity]=@Ethnicity,[Language]=@Language,[Disabled]=@Disabled,[MilitaryServiceID]=@MilitaryServiceID,[MilitaryDependentID]=@MilitaryDependentID,[MilitaryGradeID]=@MilitaryGradeID,[MilitaryStatusID]=@MilitaryStatusID,[marital_status]=@marital_status,[Education]=@Education,[Birthdate]=@Birthdate,[FICO_Score]=@FICO_Score,[CreditAgency]=@CreditAgency,[emp_start_date]=@emp_start_date,[emp_end_date]=@emp_end_date,[other_job]=@other_job,[bkfileddate] = @bkfileddate,[bkdischarge] = @bkdischarge,[bkchapter]=@bkchapter,[bkPaymentCurrent]=@bkPaymentCurrent,[bkAuthLetterDate]=@bkAuthLetterDate WHERE [oID]=@oID"
                            updateCommand.CommandType = CommandType.Text
                            updateCommand.Parameters.Add("@PersonID", SqlDbType.Int, 4, "PersonID")
                            updateCommand.Parameters.Add("@NameID", SqlDbType.Int, 4, "NameID")
                            updateCommand.Parameters.Add("@EmailID", SqlDbType.Int, 4, "EmailID")
                            updateCommand.Parameters.Add("@Former", SqlDbType.VarChar, 80, "Former")
                            updateCommand.Parameters.Add("@SSN", SqlDbType.VarChar, 80, "SSN")
                            updateCommand.Parameters.Add("@Gender", SqlDbType.Int, 4, "Gender")
                            updateCommand.Parameters.Add("@Race", SqlDbType.Int, 4, "Race")
                            updateCommand.Parameters.Add("@Ethnicity", SqlDbType.Int, 4, "Ethnicity")
                            updateCommand.Parameters.Add("@Language", SqlDbType.Int, 4, "Language")
                            updateCommand.Parameters.Add("@Disabled", SqlDbType.Bit, 0, "Disabled")
                            updateCommand.Parameters.Add("@marital_status", SqlDbType.Int, 4, "marital_status")
                            updateCommand.Parameters.Add("@Education", SqlDbType.Int, 4, "Education")
                            updateCommand.Parameters.Add("@Birthdate", SqlDbType.DateTime, 8, "Birthdate")
                            updateCommand.Parameters.Add("@FICO_Score", SqlDbType.Int, 4, "FICO_Score")
                            updateCommand.Parameters.Add("@CreditAgency", SqlDbType.VarChar, 80, "CreditAgency")
                            updateCommand.Parameters.Add("@emp_start_date", SqlDbType.DateTime, 8, "emp_start_date")
                            updateCommand.Parameters.Add("@emp_end_date", SqlDbType.DateTime, 8, "emp_end_date")
                            updateCommand.Parameters.Add("@other_job", SqlDbType.VarChar, 80, "other_job")
                            updateCommand.Parameters.Add("@MilitaryServiceID", SqlDbType.Int, 4, "MilitaryServiceID")
                            updateCommand.Parameters.Add("@MilitaryDependentID", SqlDbType.Int, 4, "MilitaryDependentID")
                            updateCommand.Parameters.Add("@MilitaryGradeID", SqlDbType.Int, 4, "MilitaryGradeID")
                            updateCommand.Parameters.Add("@MilitaryStatusID", SqlDbType.Int, 4, "MilitaryStatusID")
                            updateCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID")

                            'Bankruptcy fields
                            updateCommand.Parameters.Add("@bkfileddate", SqlDbType.DateTime, 8, "bkfileddate")
                            updateCommand.Parameters.Add("@bkdischarge", SqlDbType.DateTime, 8, "bkdischarge")
                            updateCommand.Parameters.Add("@bkchapter", SqlDbType.Int, 4, "bkchapter")
                            updateCommand.Parameters.Add("@bkPaymentCurrent", SqlDbType.Int, 4, "bkPaymentCurrent")
                            updateCommand.Parameters.Add("@bkAuthLetterDate", SqlDbType.DateTime, 8, "bkAuthLetterDate")

                            da.UpdateCommand = updateCommand

                            Using deleteCommand As SqlCommand = New SqlCommand()
                                deleteCommand.Connection = cn
                                deleteCommand.CommandText = "DELETE FROM housing_borrowers WHERE [oID]=@oID"
                                deleteCommand.CommandType = CommandType.Text
                                deleteCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
                                da.DeleteCommand = deleteCommand

                                ccr.RowsAffected = da.Update(dt)
                            End Using
                        End Using
                    End Using
                End Using
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            ccr.HandleException(ex)
        End Try

        Return ccr
    End Function
#End If
End Module
