﻿Imports System.Data.SqlClient
Public Module HECMDefault
    <Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("Select [ReferralID],[Client],[Referral_Option],[Tier],[Checkup],[BDT_BEC],[Hardest_Hit_Ref_Date],[Ref_Food],[Save_Food],[Ref_Medical],[Save_Medical],[Ref_Prescription],[Save_Prescription],[Ref_Utility],[Save_Utility],[Ref_Car_Ins],[Save_Car_Ins],[Ref_Property_Taxes],[Save_Property_Taxes],[Ref_Home_Repair],[Save_Home_Repair],[Ref_Income],[Save_Income],[Ref_Social_Services],[Save_Social_Services],[Ref_SSI],[Save_SSI],[File_Status],[File_Status_Date] FROM [HECM_Default] With (NOLOCK) WHERE client={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function
End Module
