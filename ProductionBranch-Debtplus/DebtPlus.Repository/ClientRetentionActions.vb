Imports System.Data.SqlClient

Public Module ClientRetentionActions

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT a.client_retention_action,a.client_retention_event,a.retention_action,a.message,a.amount,a.date_created,a.created_by FROM client_retention_actions a WITH (NOLOCK) INNER JOIN client_retention_events e WITH (NOLOCK) ON a.client_retention_event = e.client_retention_event WHERE e.[client] = {0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_client_retention_action"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("RETURN", SqlDbType.Int, 0, "client_retention_action").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client_retention_event", SqlDbType.Int, 0, "client_retention_event")
            .Parameters.Add("@retention_action", SqlDbType.Int, 0, "retention_action")
            .Parameters.Add("@message", SqlDbType.VarChar, 1024, "message")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_retention_actions SET retention_action=@retention_action, message=@message WHERE client_retention_action=@client_retention_action"
            .CommandType = CommandType.Text
            .Parameters.Add("@retention_action", SqlDbType.Int, 0, "retention_action")
            .Parameters.Add("@message", SqlDbType.VarChar, 1024, "message")
            .Parameters.Add("@client_retention_action", SqlDbType.Int, 0, "client_retention_action")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM client_retention_actions WHERE [client_retention_action] = @client_retention_action"
            .CommandType = CommandType.Text
            .Parameters.Add("@client_retention_action", SqlDbType.Int, 0, "client_retention_action")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
