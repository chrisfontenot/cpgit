Imports System.Data.SqlClient

Public Module HousingProperties
#If False Then
    Private baseQuery As String = "SELECT [annual_ins_amount],[annual_property_tax],[CoOwnerID],[created_by],[date_created],[FcNoticeReceivedIND],[FcSaleDT],[ForSaleIND],[homeowner_ins],[HousingID],[HPF_foreclosureCaseID],[HPF_interviewID],[HPF_counselor],[HPF_worked_with_other_agency],[ImprovementsValue],[ins_delinq_state],[ins_due_date],[LandValue],[last_change_by],[last_change_date],[Occupancy],[oID],[OwnerID],[PlanToKeepHomeCD],[pmi_insurance],[PreviousAddress],[property_tax],[PropertyAddress],[PropertyType],[PurchasePrice],[PurchaseYear],[RealityCompany],[Residency],[SalePrice],[sales_contract_date],[tax_delinq_state],[tax_due_date],[TrustName],[UseHomeAddress],[UseInReports],[HPF_Program],[HPF_SubProgram] FROM housing_properties WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")>
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [HousingID]={1:f0}", baseQuery, clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        ' Try
        Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            cn.Open()

            Using insertCommand As SqlCommand = New SqlCommand()
                insertCommand.Connection = cn
                insertCommand.CommandText = "xpr_insert_housing_property"
                insertCommand.CommandType = CommandType.StoredProcedure
                insertCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue

                insertCommand.Parameters.Add("@HousingID", System.Data.SqlDbType.Int, 4, "HousingID")
                insertCommand.Parameters.Add("@UseHomeAddress", System.Data.SqlDbType.Bit, 0, "UseHomeAddress")
                insertCommand.Parameters.Add("@PropertyAddress", System.Data.SqlDbType.Int, 4, "PropertyAddress")
                insertCommand.Parameters.Add("@PreviousAddress", System.Data.SqlDbType.Int, 4, "PreviousAddress")
                insertCommand.Parameters.Add("@TrustName", System.Data.SqlDbType.VarChar, 80, "TrustName")
                insertCommand.Parameters.Add("@OwnerID", System.Data.SqlDbType.Int, 4, "OwnerID")
                insertCommand.Parameters.Add("@CoOwnerID", System.Data.SqlDbType.Int, 4, "CoOwnerID")
                insertCommand.Parameters.Add("@Residency", System.Data.SqlDbType.Int, 4, "Residency")
                insertCommand.Parameters.Add("@Occupancy", System.Data.SqlDbType.Int, 4, "Occupancy")
                insertCommand.Parameters.Add("@PropertyType", System.Data.SqlDbType.Int, 4, "PropertyType")
                insertCommand.Parameters.Add("@PurchaseYear", System.Data.SqlDbType.Int, 4, "PurchaseYear")
                insertCommand.Parameters.Add("@PurchasePrice", System.Data.SqlDbType.Decimal, 0, "PurchasePrice")
                insertCommand.Parameters.Add("@LandValue", System.Data.SqlDbType.Decimal, 0, "LandValue")
                insertCommand.Parameters.Add("@ImprovementsValue", System.Data.SqlDbType.Decimal, 0, "ImprovementsValue")
                insertCommand.Parameters.Add("@PlanToKeepHomeCD", System.Data.SqlDbType.Int, 4, "PlanToKeepHomeCD")
                insertCommand.Parameters.Add("@ForSaleIND", System.Data.SqlDbType.Bit, 0, "ForSaleIND")
                insertCommand.Parameters.Add("@SalePrice", System.Data.SqlDbType.Decimal, 0, "SalePrice")
                insertCommand.Parameters.Add("@RealityCompany", System.Data.SqlDbType.VarChar, 80, "RealityCompany")
                insertCommand.Parameters.Add("@sales_contract_date", System.Data.SqlDbType.DateTime, 8, "sales_contract_date")
                insertCommand.Parameters.Add("@FcNoticeReceivedIND", System.Data.SqlDbType.Bit, 0, "FcNoticeReceivedIND")
                insertCommand.Parameters.Add("@FcSaleDT", System.Data.SqlDbType.DateTime, 8, "FcSaleDT")
                insertCommand.Parameters.Add("@UseInReports", System.Data.SqlDbType.Bit, 0, "UseInReports")
                insertCommand.Parameters.Add("@HPF_interviewID", System.Data.SqlDbType.Int, 4, "HPF_interviewID")
                insertCommand.Parameters.Add("@HPF_foreclosureCaseID", System.Data.SqlDbType.Int, 4, "HPF_foreclosureCaseID")
                insertCommand.Parameters.Add("@HPF_worked_with_other_agency", System.Data.SqlDbType.Bit, 0, "HPF_worked_with_other_agency")
                insertCommand.Parameters.Add("@HPF_counselor", System.Data.SqlDbType.Int, 0, "HPF_counselor")
                insertCommand.Parameters.Add("@property_tax", System.Data.SqlDbType.Int, 4, "property_tax")
                insertCommand.Parameters.Add("@pmi_insurance", System.Data.SqlDbType.Int, 4, "pmi_insurance")
                insertCommand.Parameters.Add("@ins_delinq_state", System.Data.SqlDbType.Int, 4, "ins_delinq_state")
                insertCommand.Parameters.Add("@homeowner_ins", System.Data.SqlDbType.Int, 4, "homeowner_ins")
                insertCommand.Parameters.Add("@tax_delinq_state", System.Data.SqlDbType.Int, 4, "tax_delinq_state")
                insertCommand.Parameters.Add("@tax_due_date", System.Data.SqlDbType.DateTime, 8, "tax_due_date")
                insertCommand.Parameters.Add("@ins_due_date", System.Data.SqlDbType.DateTime, 8, "ins_due_date")
                insertCommand.Parameters.Add("@annual_property_tax", System.Data.SqlDbType.Decimal, 0, "annual_property_tax")
                insertCommand.Parameters.Add("@annual_ins_amount", System.Data.SqlDbType.Decimal, 0, "annual_ins_amount")
                insertCommand.Parameters.Add("@HPF_Program", System.Data.SqlDbType.Int, 4, "HPF_Program")
                insertCommand.Parameters.Add("@HPF_SubProgram", System.Data.SqlDbType.Int, 4, "HPF_SubProgram")

                Using updateCommand As SqlCommand = New SqlCommand()
                    updateCommand.Connection = cn
                    updateCommand.CommandText = "UPDATE housing_properties SET [annual_ins_amount]=@annual_ins_amount,[annual_property_tax]=@annual_property_tax,[CoOwnerID]=@CoOwnerID,[FcNoticeReceivedIND]=@FcNoticeReceivedIND,[FcSaleDT]=@FcSaleDT,[ForSaleIND]=@ForSaleIND,[homeowner_ins]=@homeowner_ins,[HPF_foreclosureCaseID]=@HPF_foreclosureCaseID,[HPF_interviewID]=@HPF_interviewID,[HPF_worked_with_other_agency]=@HPF_worked_with_other_agency,[ImprovementsValue]=@ImprovementsValue,[ins_delinq_state]=@ins_delinq_state,[ins_due_date]=@ins_due_date,[LandValue]=@LandValue,[Occupancy]=@Occupancy,[OwnerID]=@OwnerID,[PlanToKeepHomeCD]=@PlanToKeepHomeCD,[pmi_insurance]=@pmi_insurance,[PreviousAddress]=@PreviousAddress,[property_tax]=@property_tax,[PropertyAddress]=@PropertyAddress,[PropertyType]=@PropertyType,[PurchasePrice]=@PurchasePrice,[PurchaseYear]=@PurchaseYear,[RealityCompany]=@RealityCompany,[Residency]=@Residency,[SalePrice]=@SalePrice,[sales_contract_date]=@sales_contract_date,[tax_delinq_state]=@tax_delinq_state,[tax_due_date]=@tax_due_date,[TrustName]=@TrustName,[UseHomeAddress]=@UseHomeAddress,[UseInReports]=@UseInReports,[HPF_Program]=@HPF_Program,[HPF_SubProgram]=@HPF_SubProgram,[HPF_counselor]=@HPF_counselor WHERE [oID]=@oID"
                    updateCommand.CommandType = CommandType.Text

                    updateCommand.Parameters.Add("@HousingID", System.Data.SqlDbType.Int, 4, "HousingID")
                    updateCommand.Parameters.Add("@UseHomeAddress", System.Data.SqlDbType.Bit, 0, "UseHomeAddress")
                    updateCommand.Parameters.Add("@PropertyAddress", System.Data.SqlDbType.Int, 4, "PropertyAddress")
                    updateCommand.Parameters.Add("@PreviousAddress", System.Data.SqlDbType.Int, 4, "PreviousAddress")
                    updateCommand.Parameters.Add("@TrustName", System.Data.SqlDbType.VarChar, 80, "TrustName")
                    updateCommand.Parameters.Add("@OwnerID", System.Data.SqlDbType.Int, 4, "OwnerID")
                    updateCommand.Parameters.Add("@CoOwnerID", System.Data.SqlDbType.Int, 4, "CoOwnerID")
                    updateCommand.Parameters.Add("@Residency", System.Data.SqlDbType.Int, 4, "Residency")
                    updateCommand.Parameters.Add("@Occupancy", System.Data.SqlDbType.Int, 4, "Occupancy")
                    updateCommand.Parameters.Add("@PropertyType", System.Data.SqlDbType.Int, 4, "PropertyType")
                    updateCommand.Parameters.Add("@PurchaseYear", System.Data.SqlDbType.Int, 4, "PurchaseYear")
                    updateCommand.Parameters.Add("@PurchasePrice", System.Data.SqlDbType.Decimal, 0, "PurchasePrice")
                    updateCommand.Parameters.Add("@LandValue", System.Data.SqlDbType.Decimal, 0, "LandValue")
                    updateCommand.Parameters.Add("@ImprovementsValue", System.Data.SqlDbType.Decimal, 0, "ImprovementsValue")
                    updateCommand.Parameters.Add("@PlanToKeepHomeCD", System.Data.SqlDbType.Int, 4, "PlanToKeepHomeCD")
                    updateCommand.Parameters.Add("@ForSaleIND", System.Data.SqlDbType.Bit, 0, "ForSaleIND")
                    updateCommand.Parameters.Add("@SalePrice", System.Data.SqlDbType.Decimal, 0, "SalePrice")
                    updateCommand.Parameters.Add("@RealityCompany", System.Data.SqlDbType.VarChar, 80, "RealityCompany")
                    updateCommand.Parameters.Add("@sales_contract_date", System.Data.SqlDbType.DateTime, 8, "sales_contract_date")
                    updateCommand.Parameters.Add("@FcNoticeReceivedIND", System.Data.SqlDbType.Bit, 0, "FcNoticeReceivedIND")
                    updateCommand.Parameters.Add("@FcSaleDT", System.Data.SqlDbType.DateTime, 8, "FcSaleDT")
                    updateCommand.Parameters.Add("@UseInReports", System.Data.SqlDbType.Bit, 0, "UseInReports")
                    updateCommand.Parameters.Add("@HPF_interviewID", System.Data.SqlDbType.Int, 4, "HPF_interviewID")
                    updateCommand.Parameters.Add("@HPF_foreclosureCaseID", System.Data.SqlDbType.Int, 4, "HPF_foreclosureCaseID")
                    updateCommand.Parameters.Add("@HPF_counselor", System.Data.SqlDbType.Int, 0, "HPF_counselor")
                    updateCommand.Parameters.Add("@HPF_worked_with_other_agency", System.Data.SqlDbType.Bit, 0, "HPF_worked_with_other_agency")
                    updateCommand.Parameters.Add("@property_tax", System.Data.SqlDbType.Int, 4, "property_tax")
                    updateCommand.Parameters.Add("@pmi_insurance", System.Data.SqlDbType.Int, 4, "pmi_insurance")
                    updateCommand.Parameters.Add("@ins_delinq_state", System.Data.SqlDbType.Int, 4, "ins_delinq_state")
                    updateCommand.Parameters.Add("@homeowner_ins", System.Data.SqlDbType.Int, 4, "homeowner_ins")
                    updateCommand.Parameters.Add("@tax_delinq_state", System.Data.SqlDbType.Int, 4, "tax_delinq_state")
                    updateCommand.Parameters.Add("@tax_due_date", System.Data.SqlDbType.DateTime, 8, "tax_due_date")
                    updateCommand.Parameters.Add("@ins_due_date", System.Data.SqlDbType.DateTime, 8, "ins_due_date")
                    updateCommand.Parameters.Add("@annual_property_tax", System.Data.SqlDbType.Decimal, 0, "annual_property_tax")
                    updateCommand.Parameters.Add("@annual_ins_amount", System.Data.SqlDbType.Decimal, 0, "annual_ins_amount")
                    updateCommand.Parameters.Add("@HPF_Program", System.Data.SqlDbType.Int, 4, "HPF_Program")
                    updateCommand.Parameters.Add("@HPF_SubProgram", System.Data.SqlDbType.Int, 4, "HPF_SubProgram")

                    updateCommand.Parameters.Add("@oID", System.Data.SqlDbType.Int, 0, "oID")

                    Using deleteCommand As SqlCommand = New SqlCommand()
                        deleteCommand.Connection = cn
                        deleteCommand.CommandText = "DELETE FROM housing_properties WHERE [oID]=@oID"
                        deleteCommand.CommandType = CommandType.Text
                        deleteCommand.Parameters.Add("@oID", SqlDbType.Int, 4, "oID")

                        Using da As SqlDataAdapter = New SqlDataAdapter
                            da.InsertCommand = insertCommand
                            da.UpdateCommand = updateCommand
                            da.DeleteCommand = deleteCommand
                            ccr.RowsAffected = da.Update(dt)
                            Return ccr
                        End Using
                    End Using
                End Using
            End Using
        End Using

        ' Catch ex As System.Data.SqlClient.SqlException
        ' Return ccr.HandleException(ex)
        ' End Try
        Return ccr
    End Function
#End If
End Module
