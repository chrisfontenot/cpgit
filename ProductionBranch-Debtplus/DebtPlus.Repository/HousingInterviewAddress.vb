Imports System.Data.SqlClient

Public Module HousingInterviewAddress

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Const query As String = "SELECT [oID],[HousingID],[PrePostKeyID],[AddressID],[MortgageProgramCD],[FinanceProgramCD] FROM Housing_InterviewAddress WITH (NOLOCK)"
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByHousingId(ByVal ds As DataSet, ByVal tableName As String, ByVal housingId As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("SELECT [oID],[HousingID],[PrePostKeyID],[AddressID],[MortgageProgramCD],[FinanceProgramCD] FROM Housing_InterviewAddress WITH (NOLOCK) WHERE [HousingID]={0:f0}", housingId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_Housing_InterviewAddress"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@HousingID", SqlDbType.Int, 4, "HousingID")
            .Parameters.Add("@PrePostKeyID", SqlDbType.VarChar, 1, "PrePostKeyID")
            .Parameters.Add("@AddressID", SqlDbType.Int, 4, "AddressID")
            .Parameters.Add("@MortgageProgramCD", SqlDbType.Int, 4, "MortgageProgramCD")
            .Parameters.Add("@FinanceProgramCD", SqlDbType.Int, 4, "FinanceProgramCD")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE Housing_InterviewAddress SET [HousingID]=@HousingID,[PrePostKeyID]=@PrePostKeyID,[AddressID]=@AddressID,[MortgageProgramCD]=@MortgageProgramCD,[FinanceProgramCD]=@FinanceProgramCD WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@HousingID", SqlDbType.Int, 4, "HousingID")
            .Parameters.Add("@PrePostKeyID", SqlDbType.VarChar, 1, "PrePostKeyID")
            .Parameters.Add("@AddressID", SqlDbType.Int, 4, "AddressID")
            .Parameters.Add("@MortgageProgramCD", SqlDbType.Int, 4, "MortgageProgramCD")
            .Parameters.Add("@FinanceProgramCD", SqlDbType.Int, 4, "FinanceProgramCD")
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM Housing_InterviewAddress WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
