﻿Imports System.Data.SqlClient
Imports System.Transactions
Imports Guidelight.Data

Public Module Countries
    Friend Const selectString As String = "SELECT [country],[description],[country_code],[default],[ActiveFlag],[created_by],[date_created] FROM [countries] WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [Country]={1:f0}", selectString, id)
        Dim rslt As GetDataResult = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If rslt.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "country")
        End If
        Return rslt
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Return FillNamedDataTableSchemaFromSelectCommand(selectString, ds, tableName)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByRef c As Models.Countries, ByVal oId As Object) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        If oId IsNot Nothing AndAlso oId IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(oId)
            If id > 0 Then
                Return GetBySelection(c, String.Format("[Country]={0:f0}", id))
            End If
        End If

        Return Nothing
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function GetByCountryCode(ByRef c As Models.Countries, ByVal CountryCode As Object) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        If CountryCode IsNot Nothing AndAlso CountryCode IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(CountryCode)
            If id > 0 Then
                Return GetBySelection(c, String.Format("[country_code]={0:f0}", id))
            End If
        End If

        Return Nothing
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Function GetBySelection(ByRef c As Models.Countries, ByVal Clause As String)
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()
        Try
            cn.Open()
            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = String.Format("{0} WHERE {1}", selectString, Clause)
                cmd.CommandType = CommandType.Text

                Using reader As SqlDataReader = cmd.ExecuteReader()
                    If reader IsNot Nothing AndAlso reader.Read() Then
                        c = MapRowToModel(reader)
                    End If
                End Using
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal country As Models.Countries) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If country Is Nothing Then Throw New ArgumentNullException("country")
        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "xpr_insert_country"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                MapModelToCommandParameters(country, cmd)
                gdr.RowsAffected = cmd.ExecuteNonQuery()

                If gdr.RowsAffected = 1 AndAlso cmd.Parameters(0).Value IsNot DBNull.Value Then
                    country.Id = Convert.ToInt32(cmd.Parameters(0).Value)
                End If
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function Update(ByVal country As Models.Countries) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If country Is Nothing Then Throw New ArgumentNullException("country")

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim gdr As New GetDataResult()
        Dim cmd As SqlCommand = New SqlCommand

        Try
            cmd.Connection = cn
            cmd.CommandText = "xpr_update_country"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@country", SqlDbType.Int).Value = country.Id
            MapModelToCommandParameters(country, cmd)

            cn.Open()
            gdr.RowsAffected = cmd.ExecuteNonQuery()

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Function MapRowToModel(ByVal reader As SqlDataReader) As Models.Countries
        If reader Is Nothing Then Return Nothing

        Dim country = New Models.Countries
        country.Id = reader.SafeGetIntOrZero("country")
        country.country_code = reader.SafeGetIntOrZero("country_code")
        country.Description = reader.SafeGetNullableString("description")
        country.ActiveFlag = reader.SafeGetNullableBool("ActiveFlag")
        country.Default = reader.SafeGetNullableBool("Default")
        country.created_by = reader.SafeGetNullableString("created_by")
        country.date_created = reader.SafeGetDateTime("date_created")

        Return country
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Sub MapModelToCommandParameters(ByVal country As Models.Countries, ByVal cmd As SqlCommand)
        If country Is Nothing OrElse cmd Is Nothing Then Return

        cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = country.Description
        cmd.Parameters.Add("@country_code", SqlDbType.Int, 0).Value = country.country_code
        cmd.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0).Value = country.ActiveFlag
        cmd.Parameters.Add("@Default", SqlDbType.Bit, 0).Value = country.Default
    End Sub
End Module
