﻿Imports System.Data.SqlClient

Public Module Debts

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DropDebt(ByVal DebtId As Int32) As GetDataResult
        Return DropDebt(DebtId, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DropDebt(ByVal DebtId As Int32, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As New SqlCommand()
            cmd.CommandText = "xpr_drop_debt"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = DebtId

            Return Repository.Common.ExecuteNonQuery(cmd, txn)
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CreateDebt(ByRef debtID As Int32, ByVal clientId As Int32, ByVal myCreditor As Object, ByVal myAccountNumber As String, ByVal myUnnamedCreditor As Object) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId < 0 Then Throw New ArgumentOutOfRangeException("clientId")

        Dim gdr As New GetDataResult()
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Try
            cn.Open()

            Using cmd As New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "xpr_create_debt"
                cmd.CommandType = CommandType.StoredProcedure

                SqlCommandBuilder.DeriveParameters(cmd)

                cmd.Parameters("@client").Value = clientId
                cmd.Parameters("@Creditor").Value = myCreditor
                cmd.Parameters("@Creditor_Name").Value = myUnnamedCreditor
                cmd.Parameters("@Account_Number").Value = myAccountNumber

                cmd.ExecuteNonQuery()

                debtID = Convert.ToInt32(cmd.Parameters(0).Value)
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

End Module
