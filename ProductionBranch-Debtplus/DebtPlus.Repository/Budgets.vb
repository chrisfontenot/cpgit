﻿Public Module Budgets

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [budget],[client],[starting_date],[ending_date],[budget_type],[date_created],[created_by] FROM budgets WITH (NOLOCK) WHERE client={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
