﻿Public Module AppointmentAttributes

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT [oID],[AppointmentType],[AttributeType] FROM appointment_attributes"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
