Imports System.Data.SqlClient

Public Module ClientAddKeys

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [client_addkey],[client],[additional] FROM client_addkeys WITH (NOLOCK) WHERE client = {0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal clientId As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_client_addkey"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 4, "client_addkey").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client", SqlDbType.VarChar, 80).Value = clientId
            .Parameters.Add("@additional", SqlDbType.VarChar, 50, "additional")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_addkeys SET additional=@additional WHERE client_addkey=@client_addkey AND client=@client"
            .Parameters.Add("@additional", SqlDbType.VarChar, 50, "additional")
            .Parameters.Add("@client_addkey", SqlDbType.Int, 4, "client_addkey")
            .Parameters.Add("@client", SqlDbType.VarChar, 80).Value = clientId
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM client_addkeys WHERE client=@client AND client_addkey=@client_addkey"
            .Parameters.Add("@client", SqlDbType.VarChar, 80).Value = clientId
            .Parameters.Add("@client_addkey", SqlDbType.Int, 4, "client_addkey")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
