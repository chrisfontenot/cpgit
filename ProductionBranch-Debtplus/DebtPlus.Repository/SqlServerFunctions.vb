﻿
Public Module SqlServerFunctions

    <System.Obsolete("Use LINQ Tables")> _
    Public Function SuserSname() As Repository.GetScalarResult
        Const query As String = "SELECT suser_sname() AS 'answer'"
        Return Repository.ExecuteScalar(query)
    End Function
End Module

Public Module SqlUserFunctions

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DisbursementLockCount() As Repository.GetScalarResult
        Dim result As New Repository.GetScalarResult()
        Try
            Using cn As New SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
                Using cmd As New SqlClient.SqlCommand()
                    cmd.CommandText = "xpr_DisbursementLockCount"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Connection = cn
                    cmd.Parameters.Add("RESULT", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    cmd.ExecuteNonQuery()
                    result.Result = cmd.Parameters(0).Value
                    If result.Result Is DBNull.Value OrElse result.Result Is Nothing Then
                        result.Result = 0
                    End If
                End Using
            End Using

        Catch ex As SqlClient.SqlException
            result.HandleException(ex)
            result.Result = 0
        End Try

        Return result
    End Function
End Module
