Imports System.Data.SqlClient

Public Module HousingLoanDetails
#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SElECT [oID],[LoanTypeCD],[FinanceTypeCD],[MortgageTypeCD],[InterestRate],[Hybrid_ARM_Loan],[Option_ARM_Loan],[Interest_Only_Loan],[FHA_VA_Insured_Loan],[Privately_Held_Loan],[Arm_Reset],[Payment] FROM housing_loan_details WITH (NOLOCK)"
        Return FillNamedDataTableSchemaFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("SElECT [oID],[LoanTypeCD],[FinanceTypeCD],[MortgageTypeCD],[InterestRate],[Hybrid_ARM_Loan],[Option_ARM_Loan],[Interest_Only_Loan],[FHA_VA_Insured_Loan],[Privately_Held_Loan],[Arm_Reset],[Payment] FROM housing_loan_details WITH (NOLOCK) WHERE [oID]={0:f0}", id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_housing_loan_detail"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@LoanTypeCD", SqlDbType.Int, 4, "LoanTypeCD")
            .Parameters.Add("@FinanceTypeCD", SqlDbType.Int, 4, "FinanceTypeCD")
            .Parameters.Add("@MortgageTypeCD", SqlDbType.Int, 4, "MortgageTypeCD")
            .Parameters.Add("@InterestRate", SqlDbType.Float, 8, "InterestRate")
            .Parameters.Add("@Hybrid_ARM_Loan", SqlDbType.Bit, 0, "Hybrid_ARM_Loan")
            .Parameters.Add("@Option_ARM_Loan", SqlDbType.Bit, 0, "Option_ARM_Loan")
            .Parameters.Add("@Interest_Only_Loan", SqlDbType.Bit, 0, "Interest_Only_Loan")
            .Parameters.Add("@FHA_VA_Insured_Loan", SqlDbType.Bit, 0, "FHA_VA_Insured_Loan")
            .Parameters.Add("@ARM_Reset", SqlDbType.Bit, 0, "ARM_Reset")
            .Parameters.Add("@Privately_Held_Loan", SqlDbType.Bit, 0, "Privately_Held_Loan")
            .Parameters.Add("@Payment", SqlDbType.Decimal, 4, "Payment")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE housing_loan_details SET [LoanTypeCD]=@LoanTypeCD,[FinanceTypeCD]=@FinanceTypeCD,[MortgageTypeCD]=@MortgageTypeCD,[InterestRate]=@InterestRate,[Hybrid_ARM_Loan]=@Hybrid_ARM_Loan,[Option_ARM_Loan]=@Option_ARM_Loan,[Interest_Only_Loan]=@Interest_Only_Loan,[FHA_VA_Insured_Loan]=@FHA_VA_Insured_Loan,[Arm_Reset]=@Arm_Reset,[Privately_Held_Loan]=@Privately_Held_Loan,[Payment]=@Payment WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@LoanTypeCD", SqlDbType.Int, 4, "LoanTypeCD")
            .Parameters.Add("@FinanceTypeCD", SqlDbType.Int, 4, "FinanceTypeCD")
            .Parameters.Add("@MortgageTypeCD", SqlDbType.Int, 4, "MortgageTypeCD")
            .Parameters.Add("@InterestRate", SqlDbType.Float, 8, "InterestRate")
            .Parameters.Add("@Hybrid_ARM_Loan", SqlDbType.Bit, 0, "Hybrid_ARM_Loan")
            .Parameters.Add("@Option_ARM_Loan", SqlDbType.Bit, 0, "Option_ARM_Loan")
            .Parameters.Add("@Interest_Only_Loan", SqlDbType.Bit, 0, "Interest_Only_Loan")
            .Parameters.Add("@FHA_VA_Insured_Loan", SqlDbType.Bit, 0, "FHA_VA_Insured_Loan")
            .Parameters.Add("@Privately_Held_Loan", SqlDbType.Bit, 0, "Privately_Held_Loan")
            .Parameters.Add("@Arm_Reset", SqlDbType.Bit, 0, "Arm_Reset")
            .Parameters.Add("@Payment", SqlDbType.Decimal, 8, "Payment")
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM housing_loan_details WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
