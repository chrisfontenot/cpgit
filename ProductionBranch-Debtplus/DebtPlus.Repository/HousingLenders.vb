Imports System.Data.SqlClient

Public Module HousingLenders
#If False Then
    Private baseQuery As String = "SELECT [AcctNum],[AttemptContactDate],[CaseNumber],[ContactLenderDate],[ContactLenderSuccess],[FdicNcusNum],[InvestorAccountNumber],[InvestorID],[oID],[ServicerID],[ServicerName],[ts],[WorkoutPlanDate] FROM [housing_lenders] WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Return FillNamedDataTableSchemaFromSelectCommand(baseQuery, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        If id <= 0 Then Throw RepositoryException.BadParameters()
        Dim query As String = String.Format("{0} WHERE [oID]={1:f0}", baseQuery, id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_housing_lender"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue

            .Parameters.Add("@ServicerID", System.Data.SqlDbType.Int, 0, "ServicerID")
            .Parameters.Add("@ServicerName", System.Data.SqlDbType.VarChar, 80, "ServicerName")
            .Parameters.Add("@AcctNum", System.Data.SqlDbType.VarChar, 50, "AcctNum")
            .Parameters.Add("@FdicNcusNum", System.Data.SqlDbType.VarChar, 20, "FdicNcusNum")
            .Parameters.Add("@CaseNumber", System.Data.SqlDbType.VarChar, 20, "CaseNumber")
            .Parameters.Add("@InvestorID", System.Data.SqlDbType.Int, 0, "InvestorID")
            .Parameters.Add("@InvestorAccountNumber", System.Data.SqlDbType.VarChar, 50, "InvestorAccountNumber")
            .Parameters.Add("@ContactLenderDate", System.Data.SqlDbType.DateTime, 0, "ContactLenderDate")
            .Parameters.Add("@AttemptContactDate", System.Data.SqlDbType.DateTime, 0, "AttemptContactDate")
            .Parameters.Add("@ContactLenderSuccess", System.Data.SqlDbType.Bit, 0, "ContactLenderSuccess")
            .Parameters.Add("@WorkoutPlanDate", System.Data.SqlDbType.DateTime, 0, "WorkoutPlanDate")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE housing_lenders SET [AcctNum]=@AcctNum,[AttemptContactDate]=@AttemptContactDate,[CaseNumber]=@CaseNumber,[ContactLenderDate]=@ContactLenderDate,[ContactLenderSuccess]=@ContactLenderSuccess,[FdicNcusNum]=@FdicNcusNum,[InvestorAccountNumber]=@InvestorAccountNumber,[InvestorID]=@InvestorID,[ServicerID]=@ServicerID,[ServicerName]=@ServicerName,[WorkoutPlanDate]=@WorkoutPlanDate WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@ServicerID", System.Data.SqlDbType.Int, 0, "ServicerID")
            .Parameters.Add("@ServicerName", System.Data.SqlDbType.VarChar, 80, "ServicerName")
            .Parameters.Add("@AcctNum", System.Data.SqlDbType.VarChar, 50, "AcctNum")
            .Parameters.Add("@FdicNcusNum", System.Data.SqlDbType.VarChar, 20, "FdicNcusNum")
            .Parameters.Add("@CaseNumber", System.Data.SqlDbType.VarChar, 20, "CaseNumber")
            .Parameters.Add("@InvestorID", System.Data.SqlDbType.Int, 0, "InvestorID")
            .Parameters.Add("@InvestorAccountNumber", System.Data.SqlDbType.VarChar, 50, "InvestorAccountNumber")
            .Parameters.Add("@ContactLenderDate", System.Data.SqlDbType.DateTime, 0, "ContactLenderDate")
            .Parameters.Add("@AttemptContactDate", System.Data.SqlDbType.DateTime, 0, "AttemptContactDate")
            .Parameters.Add("@ContactLenderSuccess", System.Data.SqlDbType.Bit, 0, "ContactLenderSuccess")
            .Parameters.Add("@WorkoutPlanDate", System.Data.SqlDbType.DateTime, 0, "WorkoutPlanDate")

            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM housing_lenders WHERE [oID]=@oID"
            .CommandType = CommandType.Text
            .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function
#End If
End Module
