﻿This Repository module is at the base of the DebtPlus n-tier application.  It should have 
dependencies on System libraries only - no dependencies on any other modules in the system 
unless it also is in the base tier.

The easiest way to ensure this remains true to an n-tier architecture is to check the 
references in the project properties.  No DebtPlus modules should be listed there. 