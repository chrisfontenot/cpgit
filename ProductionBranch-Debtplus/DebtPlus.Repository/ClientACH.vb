Imports System.Data.SqlClient
Imports System.Text

Public Module ClientACH

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT [client],[CheckingSavings],[ABA],[AccountNumber],[StartDate],[EndDate],[ErrorDate],[PrenoteDate],[ContractDate],[EnrollDate],[isActive],[date_created],[created_by] FROM client_ach WHERE [client]={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordACHRow(ByVal row As DataRow) As Repository.GetDataResult
        Return RecordACHRow(row, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RecordACHRow(ByVal row As DataRow, ByVal txn As SqlTransaction) As GetDataResult
        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection

        If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
            cn = txn.Connection
        Else
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            cn.Open()
        End If

        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .Transaction = txn
            .CommandText = "INSERT INTO client_ach ([client],[CheckingSavings],[ABA],[AccountNumber],[StartDate],[EndDate],[ErrorDate],[PrenoteDate],[ContractDate],[EnrollDate],[isActive]) VALUES (@client,@CheckingSavings,@ABA,@AccountNumber,@StartDate,@EndDate,@ErrorDate,@PrenoteDate,@ContractDate,@EnrollDate,@isActive)"
            .Parameters.Add("@client", SqlDbType.Int, 0, "client")
            .Parameters.Add("@CheckingSavings", SqlDbType.VarChar, 256, "CheckingSavings")
            .Parameters.Add("@ABA", SqlDbType.VarChar, 256, "ABA")
            .Parameters.Add("@AccountNumber", SqlDbType.VarChar, 256, "AccountNumber")
            .Parameters.Add("@StartDate", SqlDbType.DateTime, 0, "StartDate")
            .Parameters.Add("@EndDate", SqlDbType.DateTime, 0, "EndDate")
            .Parameters.Add("@ErrorDate", SqlDbType.DateTime, 0, "ErrorDate")
            .Parameters.Add("@PrenoteDate", SqlDbType.DateTime, 0, "PrenoteDate")
            .Parameters.Add("@ContractDate", SqlDbType.DateTime, 0, "ContractDate")
            .Parameters.Add("@EnrollDate", SqlDbType.DateTime, 0, "EnrollDate")
            .Parameters.Add("@isActive", SqlDbType.Bit, 0, "isActive")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        If row.RowState = DataRowState.Modified Then
            Dim sb As New StringBuilder

            For Each col As DataColumn In row.Table.Columns
                Dim ignoreList As String() = New String() {"client", "date_created", "created_by"}
                If Not ignoreList.Contains(col.ColumnName.ToLower()) Then
                    Dim CurrentItem As Object = row(col)
                    Dim OriginalItem As Object = row(col, DataRowVersion.Original)

                    ' If the value is changed then attempt to log it
                    If Common.Compare(CurrentItem, OriginalItem) <> 0 Then
                        sb.AppendFormat(",[{0}]=@{0}", col.ColumnName)
                    End If
                End If
            Next

            If sb.Length = 0 Then
                row.AcceptChanges()
            Else
                sb.Remove(0, 1)
                sb.Insert(0, "UPDATE client_ach SET ")
                sb.Append(" WHERE [client]=@client")

                With updateCommand
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = sb.ToString()
                    .CommandType = CommandType.Text
                    .Parameters.Add("@CheckingSavings", SqlDbType.VarChar, 256, "CheckingSavings")
                    .Parameters.Add("@ABA", SqlDbType.VarChar, 256, "ABA")
                    .Parameters.Add("@AccountNumber", SqlDbType.VarChar, 256, "AccountNumber")
                    .Parameters.Add("@StartDate", SqlDbType.DateTime, 0, "StartDate")
                    .Parameters.Add("@EndDate", SqlDbType.DateTime, 0, "EndDate")
                    .Parameters.Add("@ErrorDate", SqlDbType.DateTime, 0, "ErrorDate")
                    .Parameters.Add("@PrenoteDate", SqlDbType.DateTime, 0, "PrenoteDate")
                    .Parameters.Add("@ContractDate", SqlDbType.DateTime, 0, "ContractDate")
                    .Parameters.Add("@EnrollDate", SqlDbType.DateTime, 0, "EnrollDate")
                    .Parameters.Add("@isActive", SqlDbType.Bit, 0, "isActive")
                    .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                End With
            End If
            da.UpdateCommand = updateCommand
        End If

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .Transaction = txn
            .CommandText = "DELETE client_ach WHERE [client]=@client"
            .Parameters.Add("@client", SqlDbType.Int, 0, "client")
        End With

        Try
            da.InsertCommand = insertCommand
            da.DeleteCommand = deleteCommand

            gdr.RowsAffected = da.Update(New DataRow() {row})

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

End Module

