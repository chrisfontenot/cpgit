Imports System.Data.SqlClient

Namespace LookupTables

    Public Module CreditorTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [type],[description],[sic_prefix],[send_pledge_letter],[send_contribution_letter],[send_car_letter],[contribution_pct],[status],[cycle],[nfcc_type],[proposals],[date_created],[created_by] FROM creditor_types ORDER BY type"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO creditor_types(type,description,nfcc_type,proposals) values (@type,@description,@nfcc_type,@proposals)"
                .Parameters.Add("@type", SqlDbType.VarChar, 10, "type")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@nfcc_type", SqlDbType.VarChar, 10, "nfcc_type")
                .Parameters.Add("@proposals", SqlDbType.Int, 0, "proposals")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE creditor_types SET [type]=@new_type,[description]=@description,[nfcc_type]=@nfcc_type,[proposals]=@proposals,[status]=@status,[cycle]=@cycle,[send_contribution_letter]=@send_contribution_letter,[send_pledge_letter]=@send_pledge_letter,[send_car_letter]=@send_car_letter,[contribution_pct]=@contribution_pct WHERE type=@old_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_type", SqlDbType.VarChar, 10, "type")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@nfcc_type", SqlDbType.VarChar, 10, "nfcc_type")
                .Parameters.Add("@proposals", SqlDbType.Int, 0, "proposals")
                .Parameters.Add("@send_car_letter", SqlDbType.Int, 0, "send_car_letter")
                .Parameters.Add("@send_contribution_letter", SqlDbType.Int, 0, "send_contribution_letter")
                .Parameters.Add("@send_pledge_letter", SqlDbType.Int, 0, "send_pledge_letter")
                .Parameters.Add("@status", SqlDbType.VarChar, 10, "status")
                .Parameters.Add("@cycle", SqlDbType.VarChar, 10, "cycle")
                .Parameters.Add("@contribution_pct", SqlDbType.Float, 0, "contribution_pct")
                .Parameters.Add(New SqlParameter("@old_type", SqlDbType.VarChar, 10, ParameterDirection.Input, False, CByte(0), CByte(0), "type", DataRowVersion.Original, ""))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM creditor_types WHERE type=@creditor_type"
                .Parameters.Add("@creditor_type", SqlDbType.Int, 0, "creditor_type")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
