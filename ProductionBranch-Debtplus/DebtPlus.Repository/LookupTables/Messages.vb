Imports System.Data.SqlClient

Namespace LookupTables
    Public Module Messages
        Private Const SelectStatement_messages As String = "SELECT [message],[item_type],[item_value],[additional],[description],[rpps],[note],[default],[ActiveFlag],[HUD_9902_section],[date_created],[created_by] FROM [messages] WITH (NOLOCK)"

        <Obsolete("Use LINQ access")> _
        Public Function GetMessagesByType(ByVal ds As DataSet, ByVal tablName As String, ByVal itemType As String)
            Debug.Assert(Not String.IsNullOrEmpty(itemType))

            Dim query As String = String.Format("{0} WHERE [item_type]='{1}' ORDER BY [description],[item_value]", SelectStatement_messages, itemType.Replace("'", "''"))
            Dim gdr As GetDataResult = FillNamedDataTableFromSelectCommand(query, ds, tablName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tablName, "message")
            End If

            Return gdr
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetMethodFirstContactMessages(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "select item_value as item_key, description from messages where item_type = 'METHOD FIRST CONTACT'"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetMethodFirstContactMessages3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "select item_value, description from messages where item_type = 'METHOD FIRST CONTACT'"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetMethodFirstContactMessages2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [item_value],[description],[ActiveFlag],[Default] FROM messages WHERE [item_type]='METHOD FIRST CONTACT'"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [message],[item_type],convert(int,[item_value]) AS item_value,[additional],[description],[rpps],[note],[default],[ActiveFlag],[hud_9902_section],[date_created],[created_by] FROM messages WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [message],[item_type],convert(int,[item_value]) AS item_value,[additional],[description],[rpps],[note],[default],[ActiveFlag],[hud_9902_section],[date_created],[created_by] FROM messages WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String, ByVal itemType As String) As GetDataResult
            Dim query As String = String.Format("SELECT [message],[item_type],convert(int,[item_value]) AS item_value,[additional],[description],[rpps],[note],[default],[ActiveFlag],[hud_9902_section],[date_created],[created_by] FROM messages WITH (NOLOCK) WHERE item_type='{0}'", itemType.Replace("'", "''"))
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function GetAllTicklerMessages(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT item_value as tickler_type, description as Description, [Default], ActiveFlag FROM messages t WITH (NOLOCK) WHERE t.item_type = 'TICKLER'"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <Obsolete("Use LINQ access")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_messages"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("RETURN", SqlDbType.Int, 0, "message").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@item_type", SqlDbType.VarChar, 20, "item_type")
                .Parameters.Add("@item_value", SqlDbType.Int, 0, "item_value")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@additional", SqlDbType.VarChar, 256, "additional")
                .Parameters.Add("@rpps", SqlDbType.VarChar, 4, "rpps")
                .Parameters.Add("@nfcc", SqlDbType.VarChar, 4, "nfcc")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@note", SqlDbType.VarChar, 256, "note")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 50, "HUD_9902_section")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE messages SET [item_type]=@item_type,[item_value]=@item_value,[additional]=@additional,[rpps]=@rpps,[default]=@default,[description]=@description,[note]=@note,[HUD_9902_section]=@HUD_9902_section WHERE message=@message"
                .CommandType = CommandType.Text
                .Parameters.Add("@message", SqlDbType.Int, 0, "message")
                .Parameters.Add("@item_type", SqlDbType.VarChar, 20, "item_type")
                .Parameters.Add("@item_value", SqlDbType.Int, 0, "item_value")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@additional", SqlDbType.VarChar, 256, "additional")
                .Parameters.Add("@rpps", SqlDbType.VarChar, 4, "rpps")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@note", SqlDbType.VarChar, 256, "note")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 50, "HUD_9902_section")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM messages WHERE message=@message"
                .Parameters.Add("@message", SqlDbType.Int, 0, "message")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
