﻿Imports System.Data.SqlClient
Imports System.Transactions

Namespace LookupTables
    Public Module TelephoneNumbers
        Private Const SelectStatement As String = "SELECT [TelephoneNumber],[Country],[Acode],[Number],[Ext] FROM TelephoneNumbers WITH (NOLOCK)"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            If id <= 0 Then Throw RepositoryException.BadParameters()
            Dim query As String = String.Format("{0} WHERE TelephoneNumber={1:f0}", SelectStatement, id)
            Dim rslt = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If rslt.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "TelephoneNumber")
            End If
            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim rslt = FillNamedDataTableSchemaFromSelectCommand(SelectStatement, ds, tableName)
            If rslt.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "TelephoneNumber")
            End If
            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetByClause(ByVal ds As DataSet, ByVal tableName As String, ByVal clause As String) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If String.IsNullOrWhiteSpace(clause) OrElse ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

            Dim gdr As GetDataResult = New GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Try
                cmd.Connection = cn
                cmd.CommandText = String.Format("{1}{0}", SelectStatement, clause)
                cmd.CommandType = CommandType.Text

                cn.Open()

                da.FillLoadOption = LoadOption.PreserveChanges
                da.SelectCommand = cmd
                gdr.RowsAffected = da.Fill(ds, tableName)

                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "TelephoneNumber")

            Catch ex As System.Data.SqlClient.SqlException
                Return gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                If cmd IsNot Nothing Then cmd.Dispose()
                If da IsNot Nothing Then da.Dispose()
            End Try

            Return gdr
        End Function

        <Obsolete("Use LINQ Tables")> _
        Public Function Insert(ByVal phone As DebtPlus.LINQ.TelephoneNumber) As Repository.GetDataResult
            Dim rslt As Repository.GetDataResult
            Dim answer As Boolean = False
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing

            Try
                cn.Open()
                txn = cn.BeginTransaction()
                rslt = Insert(phone, txn)

                ' Commit the transaction to the database
                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As SqlClient.SqlException
                rslt = New Repository.GetDataResult()
                rslt.HandleException(ex)

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch ex As System.Data.SqlClient.SqlException
                    End Try
                    txn.Dispose()
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Insert(ByVal phone As DebtPlus.LINQ.TelephoneNumber, ByVal txn As SqlTransaction) As Repository.GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If phone Is Nothing Then Throw New ArgumentNullException("phone can not be null")

            Dim rslt As New Repository.GetDataResult()

            Dim cn As SqlConnection = txn.Connection
            Try
                Using cmd As SqlCommand = New SqlCommand
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "xpr_insert_TelephoneNumbers"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

                    cmd.Parameters.Add("@country", SqlDbType.Int).Value = phone.Country
                    cmd.Parameters.Add("@acode", SqlDbType.VarChar, 50).Value = phone.Acode
                    cmd.Parameters.Add("@number", SqlDbType.VarChar, 50).Value = phone.Number
                    cmd.Parameters.Add("@ext", SqlDbType.VarChar, 50).Value = phone.Ext

                    cmd.ExecuteNonQuery()
                    phone.Id = Convert.ToInt32(cmd.Parameters("@RETURN_VALUE").Value)
                End Using

            Catch ex As SqlClient.SqlException
                rslt.HandleException(ex)

            End Try

            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_TelephoneNumbers"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "TelephoneNumber").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@Country", SqlDbType.Int, 0, "Country")
                .Parameters.Add("@Acode", SqlDbType.VarChar, 20, "Acode")
                .Parameters.Add("@Number", SqlDbType.VarChar, 80, "Number")
                .Parameters.Add("@Ext", SqlDbType.VarChar, 20, "Ext")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE TelephoneNumbers SET [Country]=@Country, [Acode]=@Acode, [Number]=@Number, [Ext]=@Ext WHERE [TelephoneNumber]=@TelephoneNumber"
                .CommandType = CommandType.Text
                .Parameters.Add("@Country", SqlDbType.Int, 0, "Country")
                .Parameters.Add("@Acode", SqlDbType.VarChar, 20, "Acode")
                .Parameters.Add("@Number", SqlDbType.VarChar, 80, "Number")
                .Parameters.Add("@Ext", SqlDbType.VarChar, 20, "Ext")
                .Parameters.Add("@TelephoneNumber", SqlDbType.Int, 0, "TelephoneNumber")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM TelephoneNumbers WHERE TelephoneNumber=@TelephoneNumber"
                .CommandType = CommandType.Text
                .Parameters.Add("@TelephoneNumber", SqlDbType.Int, 0, "TelephoneNumber")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                da.AcceptChangesDuringFill = True

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As SqlClient.SqlException
                ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace
