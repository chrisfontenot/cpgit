Imports System.Data.SqlClient

Namespace LookupTables

    Public Module RetentionActions

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT retention_action,upper(letter_code) as letter_code,description FROM retention_actions"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [retention_action],[description],[letter_code],[date_created],[created_by] FROM retention_actions"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT retention_action,description,letter_code,date_created,created_by FROM retention_actions WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_retention_actions"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@retention_action", SqlDbType.Int, 0, "retention_action").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@letter_code", SqlDbType.VarChar, 10, "letter_code")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE retention_actions SET [description]=@description, [letter_code]=@letter_code WHERE retention_action=@retention_action"
                .CommandType = CommandType.Text
                .Parameters.Add("@retention_action", SqlDbType.Int, 0, "retention_action")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@letter_code", SqlDbType.VarChar, 10, "letter_code")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM retention_actions WHERE retention_action=@retention_action"
                .Parameters.Add("@retention_action", SqlDbType.Int, 0, "retention_action")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
