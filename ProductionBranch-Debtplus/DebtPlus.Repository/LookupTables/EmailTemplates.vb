Imports System.Data.SqlClient

Namespace LookupTables

    Public Module EmailTemplates

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID], [From], [CC], [Subject], [ErrorsTo], [Body], [Language] FROM EmailTemplates"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO EmailTemplates ([oID],[From],[CC],[Subject],[ErrorsTo],[Body],[Language]) VALUES (@oID,@From,@CC,@Subject,@ErrorsTo,@Body,@Language)"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.VarChar, 50, "oID")
                .Parameters.Add("@From", SqlDbType.VarChar, 128, "From")
                .Parameters.Add("@CC", SqlDbType.VarChar, 128, "CC")
                .Parameters.Add("@ErrorsTo", SqlDbType.VarChar, 128, "ErrorsTo")
                .Parameters.Add("@Subject", SqlDbType.VarChar, 128, "Subject")
                .Parameters.Add("@Body", SqlDbType.Text, Int32.MaxValue, "Body")
                .Parameters.Add("@Language", SqlDbType.Int, 4, "Language")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE EmailTemplates SET [oID]=@oID, [From]=@From, [CC]=@CC, [ErrorsTo]=@ErrorsTo, [Subject]=@Subject, [Body]=@Body, [Language]=@Language WHERE oID=@OLDID"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.VarChar, 50, "oID")
                .Parameters.Add("@From", SqlDbType.VarChar, 128, "From")
                .Parameters.Add("@CC", SqlDbType.VarChar, 128, "CC")
                .Parameters.Add("@ErrorsTo", SqlDbType.VarChar, 128, "ErrorsTo")
                .Parameters.Add("@Subject", SqlDbType.VarChar, 128, "Subject")
                .Parameters.Add("@Body", SqlDbType.Text, Int32.MaxValue, "Body")
                .Parameters.Add("@Language", SqlDbType.Int, 4, "Language")
                .Parameters.Add(New SqlParameter("@OLDID", SqlDbType.VarChar, 128, ParameterDirection.Input, False, CByte(0), CByte(0), "oID", DataRowVersion.Original, Nothing))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM EmailTemplates WHERE [oID]=@oID"
                .Parameters.Add("@oID", SqlDbType.VarChar, 50, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
