Imports System.Data.SqlClient

Namespace LookupTables

    Public Module LetterTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT letter_code as item_key, description FROM letter_types WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT letter_type, description FROM letter_types"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [letter_code],[queue_name],[description],[menu_name],[letter_group],[display_mode],[date_created],[created_by],[letter_type],[region],[top_margin],[left_margin],[bottom_margin],[right_margin],[filename],[language],[default] FROM letter_types"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        ' same as GetAll2 but upper-cases the letter_code
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll4(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT upper(letter_code) as letter_code, description FROM letter_types"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_letter_types"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "letter_type").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@letter_code", SqlDbType.VarChar, 256, "letter_code")
                .Parameters.Add("@queue_name", SqlDbType.VarChar, 256, "queue_name")
                .Parameters.Add("@description", SqlDbType.VarChar, 256, "description")
                .Parameters.Add("@menu_name", SqlDbType.VarChar, 256, "menu_name")
                .Parameters.Add("@letter_group", SqlDbType.VarChar, 256, "letter_group")
                .Parameters.Add("@display_mode", SqlDbType.Int, 0, "display_mode")
                .Parameters.Add("@region", SqlDbType.Int, 0, "region")
                .Parameters.Add("@top_margin", SqlDbType.Float, 0, "top_margin")
                .Parameters.Add("@left_margin", SqlDbType.Float, 0, "left_margin")
                .Parameters.Add("@bottom_margin", SqlDbType.Float, 0, "bottom_margin")
                .Parameters.Add("@right_margin", SqlDbType.Float, 0, "right_margin")
                .Parameters.Add("@filename", SqlDbType.VarChar, 256, "filename")
                .Parameters.Add("@language", SqlDbType.Int, 0, "language")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE letter_types SET [letter_code]=@letter_code,[queue_name]=@queue_name,[description]=@description,[menu_name]=@menu_name,[letter_group]=@letter_group,[display_mode]=@display_mode,[region]=@region,[top_margin]=@top_margin,[left_margin]=@left_margin,[bottom_margin]=@bottom_margin,[right_margin]=@right_margin,[filename]=@filename,[language]=@language,[default]=@default WHERE [letter_type]=@letter_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@letter_code", SqlDbType.VarChar, 256, "letter_code")
                .Parameters.Add("@queue_name", SqlDbType.VarChar, 256, "queue_name")
                .Parameters.Add("@description", SqlDbType.VarChar, 256, "description")
                .Parameters.Add("@menu_name", SqlDbType.VarChar, 256, "menu_name")
                .Parameters.Add("@letter_group", SqlDbType.VarChar, 256, "letter_group")
                .Parameters.Add("@display_mode", SqlDbType.Int, 0, "display_mode")
                .Parameters.Add("@region", SqlDbType.Int, 0, "region")
                .Parameters.Add("@top_margin", SqlDbType.Float, 0, "top_margin")
                .Parameters.Add("@left_margin", SqlDbType.Float, 0, "left_margin")
                .Parameters.Add("@bottom_margin", SqlDbType.Float, 0, "bottom_margin")
                .Parameters.Add("@right_margin", SqlDbType.Float, 0, "right_margin")
                .Parameters.Add("@filename", SqlDbType.VarChar, 256, "filename")
                .Parameters.Add("@language", SqlDbType.Int, 0, "language")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@letter_type", SqlDbType.Int, 0, "letter_type")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM letter_types WHERE [letter_type]=@letter_type"
                .Parameters.Add("@letter_type", SqlDbType.Int, 0, "letter_type")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
