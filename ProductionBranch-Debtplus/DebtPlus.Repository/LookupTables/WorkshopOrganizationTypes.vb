Imports System.Data.SqlClient

Namespace LookupTables
    Public Module WorkshopOrganizationTypes
        Private Const SelectStatement_workshop_organization_types As String = "SELECT [organization_type], [name], [ActiveFlag], [date_created], [created_by] FROM [workshop_organization_types]"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_workshop_organization_types, ds, tableName)
            If gdr.Success Then
                TableProcessing.setAutoIncrementPK(ds, tableName, "organization_type")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dr As DataRow) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dr Is Nothing Then Throw RepositoryException.NoDataRow()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dr)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_workshop_organization_type"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "organization_type").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@name", SqlDbType.VarChar, 50, "name")
            End With

            Try
                da.InsertCommand = insertCommand
                da.AcceptChangesDuringUpdate = True

                cn.Open()

                ccr.RowsAffected = da.Update(New DataRow() {dr})

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace
