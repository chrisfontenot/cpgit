﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module Countries
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(DebtPlus.Repository.Countries.selectString, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "country")
            End If
            Return gdr
        End Function

        <Obsolete("Use GetAll")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Return GetAll(ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetDefaultCountry() As GetScalarResult
            Const query As String = "SELECT TOP 1 country FROM countries WITH (NOLOCK) WHERE [default]<>0 UNION ALL SELECT min(country) FROM countries WITH (NOLOCK) UNION ALL SELECT 1 AS 'country'"
            Return ExecuteScalar(query)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetDefaultCountryWithoutError() As Int32
            Using gsr As GetScalarResult = GetDefaultCountry()
                If gsr.Success AndAlso gsr.Result IsNot Nothing AndAlso gsr.Result IsNot System.DBNull.Value Then
                    Return Convert.ToInt32(gsr.Result)
                End If
            End Using
            Return 0
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Dim insertCommand As SqlCommand = New SqlCommand()
            Dim updateCommand As SqlCommand = New SqlCommand()
            Dim deleteCommand As SqlCommand = New SqlCommand()

            Try
                cn.Open()
                Using da As SqlDataAdapter = New SqlDataAdapter()
                    With insertCommand
                        .Connection = cn
                        .CommandText = "xpr_insert_countries"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                        .Parameters.Add("@country_code", SqlDbType.Int, 0, "country_code")
                        .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                        .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                        .Parameters.Add("@country", SqlDbType.Int, 0, "country").Direction = ParameterDirection.ReturnValue
                    End With

                    With updateCommand
                        .Connection = cn
                        .CommandText = "UPDATE countries SET description=@description, country_code=@country_code, [default]=@default, ActiveFlag=@ActiveFlag WHERE country=@country"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                        .Parameters.Add("@country_code", SqlDbType.Int, 0, "country_code")
                        .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                        .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                        .Parameters.Add("@country", SqlDbType.Int, 0, "country")
                    End With

                    With deleteCommand
                        .Connection = cn
                        .CommandText = "DELETE FROM countries WHERE country=@country"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@country", SqlDbType.Int, 0, "country")
                    End With

                    da.InsertCommand = insertCommand
                    da.UpdateCommand = updateCommand
                    da.DeleteCommand = deleteCommand

                    ccr.RowsAffected = da.Update(dt)
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                updateCommand.Dispose()
                insertCommand.Dispose()
                deleteCommand.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace
