Imports System.Data.SqlClient

Namespace LookupTables

    Public Module Resources

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_resources"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@resource", SqlDbType.Int, 4, "resource").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@resource_type", SqlDbType.Int, 4, "resource_type")
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
                .Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID")
                .Parameters.Add("@WWW", SqlDbType.VarChar, 80, "www")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE resources SET resource_type=@resource_type,name=@name,AddressID=@AddressID,TelephoneID=@TelephoneID,FAXID=@FAXID,EmailID=@EmailID,WWW=@WWW WHERE resource=@resource"
                .CommandType = CommandType.Text

                .Parameters.Add("@resource_type", SqlDbType.Int, 4, "resource_type")
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
                .Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID")
                .Parameters.Add("@WWW", SqlDbType.VarChar, 80, "www")
                .Parameters.Add("@resource", SqlDbType.Int, 4, "resource")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "xpr_delete_resources"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@resource", SqlDbType.Int, 0, "resource")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
