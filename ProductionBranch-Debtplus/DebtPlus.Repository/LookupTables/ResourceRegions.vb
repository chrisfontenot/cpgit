﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.Transactions

Namespace LookupTables
    Public Module ResourceRegions
        Public Function GetAllByResourceId(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As Repository.GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            Dim rslt As New GetDataResult()

            If ds IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(tableName) AndAlso id > 0 Then

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim cmd As SqlCommand = New SqlCommand
                Dim da As SqlDataAdapter = New SqlDataAdapter

                Try
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT office FROM resource_regions WITH (NOLOCK) WHERE resource=@resource"
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@resource", SqlDbType.Int).Value = id

                    cn.Open()

                    da.SelectCommand = cmd
                    da.Fill(ds, tableName)

                Catch ex As SqlClient.SqlException
                    rslt.HandleException(ex)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                    If cmd IsNot Nothing Then cmd.Dispose()
                    If da IsNot Nothing Then da.Dispose()
                End Try
            End If

            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function UpdateOffices(ByVal resourceId As Int32, ByVal officeIds As List(Of Int32)) As Repository.GetDataResult
            Dim query As New StringBuilder()
            Dim gdr As Repository.GetDataResult

            ' First, remove all of the items from the database
            query.AppendFormat("DELETE FROM resource_regions WHERE resource={0:d};", resourceId)

            ' Next, go through the list and add the items that are checked
            For Each id In officeIds
                query.AppendFormat("INSERT INTO resource_regions (resource,office) VALUES ({0:d},{1:d});", resourceId, id)
            Next id

            Using txn As TransactionScope = TransactionUtils.CreateTransactionScope()
                Using cmd As SqlCommand = New SqlCommand()
                    cmd.CommandText = query.ToString()
                    cmd.CommandType = CommandType.Text
                    gdr = ExecuteNonQuery(cmd)
                End Using

                txn.Complete()
            End Using

            Return gdr
        End Function
    End Module
End Namespace