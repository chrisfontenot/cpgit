Imports System.Data.SqlClient

Namespace LookupTables

    Public Module TranTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [tran_type],[description],[deposit_subtype],[note],[sort_order],[date_created],[created_by] FROM tran_types"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO tran_types(tran_type,description,sort_order,note,deposit_subtype) VALUES (@tran_type,@description,@sort_order,@note,@deposit_subtype)"
                .CommandType = CommandType.Text
                .Parameters.Add("@tran_type", SqlDbType.VarChar, 2, "tran_type")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@note", SqlDbType.VarChar, 512, "note")
                .Parameters.Add("@deposit_subtype", SqlDbType.Bit, 0, "deposit_subtype")
                .Parameters.Add("@sort_order", SqlDbType.Int, 0, "sort_order")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE tran_types SET tran_type=@tran_type, description = @description, sort_order = @sort_order, note = @note, deposit_subtype = @deposit_subtype WHERE tran_type = @old_tran_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@tran_type", SqlDbType.VarChar, 2, "tran_type")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@note", SqlDbType.VarChar, 512, "note")
                .Parameters.Add("@deposit_subtype", SqlDbType.Bit, 0, "deposit_subtype")
                .Parameters.Add("@sort_order", SqlDbType.Int, 0, "sort_order")
                .Parameters.Add(New SqlParameter("@old_tran_type", SqlDbType.VarChar, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "tran_type", DataRowVersion.Original, Nothing))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM tran_types WHERE tran_type=@tran_type"
                .Parameters.Add("@tran_type", SqlDbType.VarChar, 2, "tran_type")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
