Imports System.Data.SqlClient

Namespace LookupTables

    Public Module HousingTrainingCourses

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[TrainingTitle],[TrainingDuration],[Certificate],[ActiveFlag] FROM housing_training_courses"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[TrainingTitle],[TrainingDuration],[TrainingOrganization],[TrainingOrganizationOther],[TrainingSponsor],[TrainingSponsorOther],[ActiveFlag],[Certificate] FROM housing_training_courses"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_housing_training_course"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@TrainingTitle", SqlDbType.VarChar, 50, "TrainingTitle")
                .Parameters.Add("@TrainingDuration", SqlDbType.Int, 4, "TrainingDuration")
                .Parameters.Add("@TrainingOrganization", SqlDbType.VarChar, 10, "TrainingOrganization")
                .Parameters.Add("@TrainingOrganizationOther", SqlDbType.VarChar, 50, "TrainingOrganizationOther")
                .Parameters.Add("@TrainingSponsor", SqlDbType.VarChar, 10, "TrainingSponsor")
                .Parameters.Add("@TrainingSponsorOther", SqlDbType.VarChar, 50, "TrainingSponsorOther")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@Certificate", SqlDbType.Bit, 0, "Certificate")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_training_courses SET [TrainingTitle]=@TrainingTitle,[TrainingDuration]=@TrainingDuration,[TrainingOrganization]=@TrainingOrganization,[TrainingOrganizationOther]=@TrainingOrganizationOther,[TrainingSponsor]=@TrainingSponsor,[TrainingSponsorOther]=@TrainingSponsorOther,[ActiveFlag]=@ActiveFlag,[Certificate]=@Certificate WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@TrainingTitle", SqlDbType.VarChar, 50, "TrainingTitle")
                .Parameters.Add("@TrainingDuration", SqlDbType.Int, 4, "TrainingDuration")
                .Parameters.Add("@TrainingOrganization", SqlDbType.VarChar, 10, "TrainingOrganization")
                .Parameters.Add("@TrainingOrganizationOther", SqlDbType.VarChar, 50, "TrainingOrganizationOther")
                .Parameters.Add("@TrainingSponsor", SqlDbType.VarChar, 10, "TrainingSponsor")
                .Parameters.Add("@TrainingSponsorOther", SqlDbType.VarChar, 50, "TrainingSponsorOther")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@Certificate", SqlDbType.Bit, 0, "Certificate")
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_training_courses WHERE [oID]=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
