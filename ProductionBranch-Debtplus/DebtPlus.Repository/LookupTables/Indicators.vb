Imports System.Data.SqlClient

Namespace LookupTables

    Public Module Indicators

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [indicator],[description],[HUD_9902_section],[Hidden],[date_created],[created_by] FROM indicators"
            Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "indicator")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_indicators"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("RETURN", SqlDbType.Int, 0, "indicator").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 256, "HUD_9902_section")
                .Parameters.Add("@Hidden", SqlDbType.Bit, 0, "Hidden")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE indicators SET description=@description,HUD_9902_section=@HUD_9902_section,[Hidden]=@Hidden WHERE indicator=@indicator"
                .CommandType = CommandType.Text
                .Parameters.Add("@indicator", SqlDbType.Int, 0, "indicator")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 256, "HUD_9902_section")
                .Parameters.Add("@Hidden", SqlDbType.Bit, 0, "Hidden")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM indicators WHERE indicator=@indicator"
                .Parameters.Add("@indicator", SqlDbType.Int, 0, "indicator")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
