﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module CounselorSchedules
        Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

            Dim gdr As New Repository.GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT oID,Counselor,Office,DOW,Date FROM CounselorSchedules WHERE Counselor=@Counselor"
                    cmd.Parameters.Add("@Counselor", SqlDbType.Int).Value = id

                    Using da As SqlDataAdapter = New SqlDataAdapter(cmd)
                        gdr.RowsAffected = da.Fill(ds, tableName)
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim insertCommand As SqlCommand = New SqlCommand()
            Dim updateCommand As SqlCommand = New SqlCommand()
            Dim deleteCommand As SqlCommand = New SqlCommand()

            Try
                cn.Open()
                Using da As SqlDataAdapter = New SqlDataAdapter
                    With insertCommand
                        .Connection = cn
                        .CommandText = "INSERT INTO CounselorSchedules ([Counselor],[Office],[DOW],[Date]) VALUES (@Counselor,@Office,@DOW,@Date)"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@Office", SqlDbType.Int, 0, "Office")
                        .Parameters.Add("@DOW", SqlDbType.Int, 0, "DOW")
                        .Parameters.Add("@Date", SqlDbType.DateTime, 0, "Date")
                        .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                    End With
                    da.InsertCommand = insertCommand

                    With updateCommand
                        .Connection = cn
                        .CommandText = "UPDATE CounselorSchedules SET Office=@Office,DOW=@DOW,Date=@Date WHERE oID=@oID"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@Office", SqlDbType.Int, 0, "Office")
                        .Parameters.Add("@DOW", SqlDbType.Int, 0, "DOW")
                        .Parameters.Add("@Date", SqlDbType.DateTime, 0, "Date")
                        .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                    End With
                    da.UpdateCommand = updateCommand

                    With deleteCommand
                        .Connection = cn
                        .CommandText = "DELETE FROM CounselorSchedules WHERE [oID]=@oID"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                    End With
                    da.DeleteCommand = deleteCommand

                    da.ContinueUpdateOnError = True

                    ccr.RowsAffected = da.Update(dt)
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                ccr.HandleException(ex)

            Finally
                updateCommand.Dispose()
                insertCommand.Dispose()
                deleteCommand.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace