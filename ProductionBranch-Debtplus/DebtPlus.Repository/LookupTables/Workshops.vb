﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module Workshops
        Private Const SelectStatement_workshops As String = "SELECT [workshop],[workshop_type],[start_time],[workshop_location],[seats_available],[counselorID],[HUD_Grant],[HousingFeeAmount],[Guest],[date_created],[created_by] FROM workshops w WITH (NOLOCK)"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllRecent(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Return GetAllRecent(ds, tableName, 10)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllRecent(ByVal ds As DataSet, ByVal tableName As String, ByVal Days As Int32) As GetDataResult
            ' The days must be in the past
            If Days < 0 Then Throw New ArgumentException("Days must be positive")

            ' Make the number of days a negative number to denote the past # of days
            Days = 0 - Days

            Dim query As String = String.Format("{0} WHERE start_time >= dateadd(d, {1:f0}, getdate())", SelectStatement_workshops, Days)
            Dim rslt = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If rslt.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "workshop")
            End If
            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim rslt = FillNamedDataTableFromSelectCommand(SelectStatement_workshops, ds, tableName)
            If rslt.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "workshop")
            End If
            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Dim insertCommand As SqlCommand = New SqlCommand()
            Dim updateCommand As SqlCommand = New SqlCommand()
            Dim deleteCommand As SqlCommand = New SqlCommand()
            Try
                cn.Open()

                Using da As SqlDataAdapter = New SqlDataAdapter()
                    insertCommand.Connection = cn
                    insertCommand.CommandText = "insert into workshops ([workshop_type],[start_time],[workshop_location],[seats_available],[CounselorID],[HUD_Grant],[HousingFeeAmount],[Guest]) values ( @workshop_type,@start_time,@workshop_location,@seats_available,@counselorID,@HUD_Grant,@HousingFeeAmount,@Guest ); Select [workshop_type],[start_time],[workshop_location],[seats_available],[counselor],[HUD_Grant],[HousingFeeAmount],[Guest],[date_created],[created_by] from [workshops] WHERE [workshop] = scope_identity();"
                    insertCommand.CommandType = CommandType.Text
                    insertCommand.Parameters.Add("@workshop", SqlDbType.Int, 0, "workshop")
                    insertCommand.Parameters.Add("@workshop_type", SqlDbType.Int, 0, "workshop_type")
                    insertCommand.Parameters.Add("@start_time", SqlDbType.DateTime, 0, "start_time")
                    insertCommand.Parameters.Add("@workshop_location", SqlDbType.Int, 0, "workshop_location")
                    insertCommand.Parameters.Add("@seats_available", SqlDbType.Int, 0, "seats_available")
                    insertCommand.Parameters.Add("@counselorID", SqlDbType.Int, 0, "counselorID")
                    insertCommand.Parameters.Add("@HUD_Grant", SqlDbType.Int, 0, "HUD_Grant")
                    insertCommand.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
                    insertCommand.Parameters.Add("@Guest", SqlDbType.Int, 0, "Guest")
                    insertCommand.Parameters.Add("@date_created", SqlDbType.DateTime, 0, "date_created")
                    insertCommand.Parameters.Add("@created_by", SqlDbType.VarChar, 80, "created_by")
                    da.InsertCommand = insertCommand
                    da.InsertCommand.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord

                    updateCommand.Connection = cn
                    updateCommand.CommandText = "UPDATE workshops SET [workshop_type]=@workshop_type,[start_time]=@start_time,[workshop_location]=@workshop_location,[seats_available]=@seats_available,[counselorID]=@counselorID,[HUD_Grant]=@HUD_Grant,[HousingFeeAmount]=@HousingFeeAmount,[Guest]=@Guest WHERE workshop=@workshop"
                    updateCommand.CommandType = CommandType.Text
                    updateCommand.Parameters.Add("@workshop_type", SqlDbType.Int, 0, "workshop_type")
                    updateCommand.Parameters.Add("@start_time", SqlDbType.DateTime, 0, "start_time")
                    updateCommand.Parameters.Add("@workshop_location", SqlDbType.Int, 0, "workshop_location")
                    updateCommand.Parameters.Add("@seats_available", SqlDbType.Int, 0, "seats_available")
                    updateCommand.Parameters.Add("@counselorID", SqlDbType.Int, 0, "counselorID")
                    updateCommand.Parameters.Add("@HUD_Grant", SqlDbType.Int, 0, "HUD_Grant")
                    updateCommand.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
                    updateCommand.Parameters.Add("@workshop", SqlDbType.Int, 0, "workshop")
                    updateCommand.Parameters.Add("@Guest", SqlDbType.Int, 0, "Guest")
                    da.UpdateCommand = updateCommand

                    deleteCommand.Connection = cn
                    deleteCommand.CommandText = "DELETE FROM workshops WHERE [workshop]=@workshop"
                    deleteCommand.Parameters.Add("@workshop", SqlDbType.Int, 0, "workshop")
                    da.DeleteCommand = deleteCommand

                    ccr.RowsAffected = da.Update(dt)
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                updateCommand.Dispose()
                insertCommand.Dispose()
                deleteCommand.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace