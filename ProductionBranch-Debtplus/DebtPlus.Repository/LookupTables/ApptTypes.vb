Imports System.Data.SqlClient

Namespace LookupTables

    Public Module ApptTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [appt_type],[appt_name],[appt_duration],[default],[housing],[credit],[bankruptcy],[initial_appt],[contact_type],[missed_retention_event],[create_letter],[reschedule_letter],[cancel_letter],[bankruptcy_class],[date_created],[created_by] FROM appt_types"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [appt_type],[appt_name],[appt_duration],[create_letter],[reschedule_letter],[cancel_letter],[initial_appt],[contact_type],[missed_retention_event],[default] FROM appt_types WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_appt_types"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@appt_name", SqlDbType.VarChar, 50, "appt_name")
                .Parameters.Add("@appt_duration", SqlDbType.Int, 0, "appt_duration")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@housing", SqlDbType.Bit, 0, "housing")
                .Parameters.Add("@credit", SqlDbType.Bit, 0, "credit")
                .Parameters.Add("@bankruptcy", SqlDbType.Bit, 0, "bankruptcy")
                .Parameters.Add("@initial_appt", SqlDbType.Bit, 0, "initial_appt")
                .Parameters.Add("@missed_retention_event", SqlDbType.Int, 0, "missed_retention_event")
                .Parameters.Add("@contact_type", SqlDbType.Int, 4, "contact_type")
                .Parameters.Add("@create_letter", SqlDbType.VarChar, 10, "create_letter")
                .Parameters.Add("@reschedule_letter", SqlDbType.VarChar, 10, "reschedule_letter")
                .Parameters.Add("@cancel_letter", SqlDbType.VarChar, 10, "cancel_letter")
                .Parameters.Add("@bankruptcy_class", SqlDbType.Int, 0, "bankruptcy_class")
                .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type").Direction = ParameterDirection.ReturnValue
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE appt_types SET [appt_name]=@appt_name,[appt_duration]=@appt_duration,[default]=@default,[housing]=@housing,[credit]=@credit,[bankruptcy]=@bankruptcy,[initial_appt]=@initial_appt,[contact_type]=@contact_type,[missed_retention_event]=@missed_retention_event,[create_letter]=@create_letter,[reschedule_letter]=@reschedule_letter,[cancel_letter]=@cancel_letter,[bankruptcy_class]=@bankruptcy_class WHERE [appt_type]=@appt_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@appt_name", SqlDbType.VarChar, 50, "appt_name")
                .Parameters.Add("@appt_duration", SqlDbType.Int, 0, "appt_duration")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@housing", SqlDbType.Bit, 0, "housing")
                .Parameters.Add("@credit", SqlDbType.Bit, 0, "credit")
                .Parameters.Add("@bankruptcy", SqlDbType.Bit, 0, "bankruptcy")
                .Parameters.Add("@initial_appt", SqlDbType.Bit, 0, "initial_appt")
                .Parameters.Add("@missed_retention_event", SqlDbType.Int, 0, "missed_retention_event")
                .Parameters.Add("@contact_type", SqlDbType.Int, 4, "contact_type")
                .Parameters.Add("@create_letter", SqlDbType.VarChar, 10, "create_letter")
                .Parameters.Add("@reschedule_letter", SqlDbType.VarChar, 10, "reschedule_letter")
                .Parameters.Add("@cancel_letter", SqlDbType.VarChar, 10, "cancel_letter")
                .Parameters.Add("@bankruptcy_class", SqlDbType.Int, 0, "bankruptcy_class")
                .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM appt_types WHERE appt_type=@appt_type"
                .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
