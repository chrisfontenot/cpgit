﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module HousingARMReferenceInfo
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT groupId, id, longDesc, shortDesc, name FROM housing_ARM_ReferenceInfo"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllByGroupId(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) OrElse id <= 0 Then Throw RepositoryException.BadParameters()

            Dim gdr As GetDataResult = New GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT [GroupID],[name],[longDesc] FROM housing_ARM_referenceInfo WITH (NOLOCK) WHERE [groupID]=@GroupID"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@GroupID", SqlDbType.Int).Value = id

                cn.Open()

                da.SelectCommand = cmd
                gdr.RowsAffected = da.Fill(ds, tableName)
                da.FillSchema(ds, SchemaType.Source, tableName)

            Catch ex As System.Data.SqlClient.SqlException
                Return gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                If cmd IsNot Nothing Then cmd.Dispose()
                If da IsNot Nothing Then da.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllByGroupIds(ByVal ds As DataSet, ByVal tableName As String, ByVal ids As List(Of Int32)) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) OrElse ids Is Nothing OrElse Not ids.Any() Then Throw RepositoryException.BadParameters()

            Dim csvGroupIds As String = String.Join(",", ids.ToArray)

            Dim gdr As GetDataResult = New GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT [groupId],[id],[longDesc],[name],[shortDesc] FROM housing_ARM_referenceInfo WHERE [GroupID] IN (" + csvGroupIds + ")"
                cmd.CommandType = CommandType.Text

                cn.Open()

                da.SelectCommand = cmd
                gdr.RowsAffected = da.Fill(ds, tableName)
                da.FillSchema(ds, SchemaType.Source, tableName)

            Catch ex As System.Data.SqlClient.SqlException
                Return gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                If cmd IsNot Nothing Then cmd.Dispose()
                If da IsNot Nothing Then da.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DeleteAllByGroupId(ByVal groupId As Int32) As GetDataResult
            Return DeleteAllByGroupId(groupId, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DeleteAllByGroupId(ByVal groupId As Int32, ByVal txn As SqlTransaction) As GetDataResult
            Const query As String = "DELETE FROM housing_ARM_referenceInfo WHERE GroupID=@GroupID"
            Return DeleteRowUsingDeleteQueryAndId(query, "@GroupID", groupId, txn)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function InsertGroupItem(ByVal groupId As Int32, ByVal item As Models.Transactions.UpdateHousingARMReferenceInfo.HousingARMReferenceItem) As GetDataResult
            Return InsertGroupItem(groupId, item, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function InsertGroupItem(ByVal groupId As Int32, ByVal item As Models.Transactions.UpdateHousingARMReferenceInfo.HousingARMReferenceItem, ByVal txn As SqlTransaction) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If groupId < 0 Then Throw New ArgumentOutOfRangeException("groupId<0")
            If item Is Nothing Then Throw New ArgumentNullException("item")

            Dim gdr As New GetDataResult()

            Dim cn As SqlConnection

            If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
                cn = txn.Connection
            Else
                cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
            End If

            Try
                Dim cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = "INSERT INTO housing_ARM_ReferenceInfo([GroupID],[id],[LongDesc],[name],[ShortDesc]) VALUES (@GroupID,@id,@LongDesc,@name,@ShortDesc); SELECT scope_identity() AS RETURN"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@GroupID", SqlDbType.Int).Value = groupId
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = item.Id
                cmd.Parameters.Add("@LongDesc", SqlDbType.VarChar, 2048).Value = item.LongDesc
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 256).Value = item.Name
                cmd.Parameters.Add("@ShortDesc", SqlDbType.VarChar, 256).Value = item.ShortDesc
                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

                cmd.ExecuteNonQuery()

                item.Id = Convert.ToInt32(cmd.Parameters("RETURN").Value)

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO housing_ARM_ReferenceInfo ([groupId],[id],[name],[longDesc],[shortDesc]) values (@groupId,@id,@name,@longDesc,@shortDesc);"
                .Parameters.Add("@groupID", SqlDbType.Int, 0, "groupId")
                .Parameters.Add("@id", SqlDbType.Int, 0, "id")
                .Parameters.Add("@name", SqlDbType.VarChar, 256, "name")
                .Parameters.Add("@longDesc", SqlDbType.VarChar, 1024, "longDesc")
                .Parameters.Add("@shortDesc", SqlDbType.VarChar, 1024, "shortDesc")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_ARM_ReferenceInfo SET [groupId]=@groupId,[id]=@id,[name]=@name,[longDesc]=@longDesc,[shortDesc]=@shortDesc WHERE [groupId]=@old_groupId AND [id]=@old_id"
                .CommandType = CommandType.Text
                .Parameters.Add("@groupID", SqlDbType.Int, 0, "groupId")
                .Parameters.Add("@id", SqlDbType.Int, 0, "id")
                .Parameters.Add("@name", SqlDbType.VarChar, 256, "name")
                .Parameters.Add("@longDesc", SqlDbType.VarChar, 1024, "longDesc")
                .Parameters.Add("@shortDesc", SqlDbType.VarChar, 1024, "shortDesc")
                .Parameters.Add(New SqlParameter("@old_groupId", SqlDbType.Int, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "groupId", DataRowVersion.Original, ""))
                .Parameters.Add(New SqlParameter("@old_id", SqlDbType.Int, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "id", DataRowVersion.Original, ""))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_ARM_ReferenceInfo WHERE [groupId]=@groupId AND [id]=@id"
                .CommandType = CommandType.Text
                .Parameters.Add("@groupID", SqlDbType.Int, 0, "groupId")
                .Parameters.Add("@id", SqlDbType.Int, 0, "id")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace