Imports System.Data.SqlClient

Namespace LookupTables

    Public Module BudgetCategories
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [budget_category],[budget_category_group],[description],[ExpenseCode],[auto_factor],[person_factor],[maximum_factor],[rpps_code],[hpf],[date_created],[created_by],[heading],[detail],[living_expense],[housing_expense] FROM budget_categories"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "SET IDENTITY_INSERT budget_categories ON; INSERT INTO budget_categories (budget_category, description, auto_factor, person_factor, maximum_factor, heading, detail, living_expense, housing_expense, rpps_code) VALUES (@budget_category, @description, @auto_factor, @person_factor, @maximum_factor, @heading, @detail, @living_expense, @housing_expense, @rpps_code); SET IDENTITY_INSERT budget_categories OFF;"
                .CommandType = CommandType.Text
                .Parameters.Add("@budget_category", SqlDbType.Int, 0, "budget_category")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@auto_factor", SqlDbType.Decimal, 0, "auto_factor")
                .Parameters.Add("@person_factor", SqlDbType.Decimal, 0, "person_factor")
                .Parameters.Add("@maximum_factor", SqlDbType.Decimal, 0, "maximum_factor")
                .Parameters.Add("@heading", SqlDbType.Bit, 0, "heading")
                .Parameters.Add("@detail", SqlDbType.Bit, 0, "detail")
                .Parameters.Add("@living_expense", SqlDbType.Bit, 0, "living_expense")
                .Parameters.Add("@housing_expense", SqlDbType.Bit, 0, "housing_expense")
                .Parameters.Add("@rpps_code", SqlDbType.Int, 0, "rpps_code")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE budget_categories SET budget_category=@new_budget_category, description=@description, auto_factor=@auto_factor, person_factor=@person_factor, maximum_factor=@maximum_factor, heading=@heading, detail=@detail, living_expense=@living_expense, housing_expense=@housing_expense, rpps_code=@rpps_code WHERE budget_category=@old_budget_category"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_budget_category", SqlDbType.Int, 0, "budget_category")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@auto_factor", SqlDbType.Decimal, 0, "auto_factor")
                .Parameters.Add("@person_factor", SqlDbType.Decimal, 0, "person_factor")
                .Parameters.Add("@maximum_factor", SqlDbType.Decimal, 0, "maximum_factor")
                .Parameters.Add("@heading", SqlDbType.Bit, 0, "heading")
                .Parameters.Add("@detail", SqlDbType.Bit, 0, "detail")
                .Parameters.Add("@living_expense", SqlDbType.Bit, 0, "living_expense")
                .Parameters.Add("@housing_expense", SqlDbType.Bit, 0, "housing_expense")
                .Parameters.Add("@rpps_code", SqlDbType.Int, 0, "rpps_code")
                .Parameters.Add(New SqlParameter("@old_budget_category", SqlDbType.Int, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "budget_category", DataRowVersion.Original, Nothing))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM budget_categories WHERE budget_category=@budget_category"
                .Parameters.Add("@budget_category", SqlDbType.Int, 0, "budget_category")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
