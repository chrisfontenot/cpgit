﻿Imports System.Data.SqlClient

Namespace LookupTables
    Friend Module TableProcessing
        Friend Sub setAutoIncrementPK(ByVal ds As DataSet, ByVal tableName As String, ByVal colName As String)
            Dim tbl As DataTable = ds.Tables(tableName)
            If tbl IsNot Nothing Then
                Dim col As DataColumn = tbl.Columns(colName)
                Debug.Assert(col IsNot Nothing)

                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                    tbl.PrimaryKey = New DataColumn() {col}
                End If

                If Not col.AutoIncrement Then
                    col.AutoIncrement = True
                    col.AutoIncrementSeed = -1
                    col.AutoIncrementStep = -1
                End If
            End If
        End Sub
    End Module
End Namespace