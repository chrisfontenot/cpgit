Imports Guidelight.Data
Imports DebtPlus.Models
Imports System.Data.SqlClient

Namespace LookupTables
    Public Module HousingARMHCSIds

        Private Const SelectStatement_housing_arm_hcs_ids As String = "SELECT hcs_id,description,username,password,counseling_amount,faith_based,colonias,migrant_farm_worker,validation_date,[default],ActiveFlag FROM housing_arm_hcs_ids"
        <Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_housing_arm_hcs_ids, ds, tableName)
            If gdr.Success Then
                setPK(ds, tableName)
            End If
            Return gdr
        End Function

        <Obsolete("Use LINQ Tables")> _
        Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableSchemaFromSelectCommand(SelectStatement_housing_arm_hcs_ids, ds, tableName)
            If gdr.Success Then
                setPK(ds, tableName)
            End If
            Return gdr
        End Function

        <Obsolete("Use LINQ Tables")> _
        Private Sub setPK(ByVal ds As DataSet, ByVal tableName As String)
            Dim tbl As DataTable = ds.Tables(tableName)
            If tbl IsNot Nothing Then
                Dim col As DataColumn = tbl.Columns("hcs_id")
                Debug.Assert(col IsNot Nothing)

                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                    tbl.PrimaryKey = New DataColumn() {col}
                End If
            End If

            ' There are no auto-increment columns
        End Sub

        <Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Return GetAll(ds, tableName)
        End Function

        <Obsolete("Use LINQ Tables")> _
        Public Function GetAll2() As HousingDownloadFormStuff
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand
            Dim reader As SqlDataReader = Nothing

            Dim poco As HousingDownloadFormStuff = New HousingDownloadFormStuff()

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT hcs_id, UserName, Password FROM housing_ARM_hcs_ids WHERE [ActiveFlag]<>0 ORDER BY convert(int,[Default]) DESC, [hcs_id]"
                cmd.CommandType = CommandType.Text

                cn.Open()
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)

                If reader IsNot Nothing AndAlso reader.Read Then
                    poco.HcsId = reader.SafeGetString("hcs_id")
                    poco.UserName = reader.SafeGetString("UserName")
                    poco.Password = reader.SafeGetString("Password")
                End If

            Catch ex As System.Data.SqlClient.SqlException
                Using gdr As New GetDataResult()
                    gdr.HandleException(ex)
                End Using

            Finally
                If reader IsNot Nothing Then reader.Dispose()
                If cmd IsNot Nothing Then cmd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()

            End Try

            Return poco
        End Function

        <Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO housing_arm_hcs_ids(hcs_id,Description,UserName,Password,counseling_amount,faith_based,colonias,migrant_farm_worker,validation_date,[Default],[ActiveFlag]) values (@hcs_id,@Description,@UserName,@Password,@counseling_amount,@faith_based,@colonias,@migrant_farm_worker,@validation_date,@Default,@ActiveFlag);"
                .Parameters.Add("@hcs_id", SqlDbType.Int, 0, "hcs_id")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@UserName", SqlDbType.VarChar, 255, "UserName")
                .Parameters.Add("@Password", SqlDbType.VarChar, 255, "Password")
                .Parameters.Add("@counseling_amount", SqlDbType.Money, 0, "counseling_amount")
                .Parameters.Add("@faith_based", SqlDbType.Bit, 0, "faith_based")
                .Parameters.Add("@colonias", SqlDbType.Bit, 0, "colonias")
                .Parameters.Add("@migrant_farm_worker", SqlDbType.Bit, 0, "migrant_farm_worker")
                .Parameters.Add("@validation_date", SqlDbType.DateTime, 0, "validation_date")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_arm_hcs_ids SET hcs_id=@hcs_id, [Description]=@Description, UserName=@UserName, Password=@Password, counseling_amount=@counseling_amount, faith_based=@faith_based, colonias=@colonias, migrant_farm_worker=@migrant_farm_worker, validation_date=@validation_date, [Default]=@Default, [ActiveFlag]=@ActiveFlag WHERE hcs_id=@old_hcs_id"
                .CommandType = CommandType.Text
                .Parameters.Add("@hcs_id", SqlDbType.Int, 0, "hcs_id")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@UserName", SqlDbType.VarChar, 255, "UserName")
                .Parameters.Add("@Password", SqlDbType.VarChar, 255, "Password")
                .Parameters.Add("@counseling_amount", SqlDbType.Money, 0, "counseling_amount")
                .Parameters.Add("@faith_based", SqlDbType.Bit, 0, "faith_based")
                .Parameters.Add("@colonias", SqlDbType.Bit, 0, "colonias")
                .Parameters.Add("@migrant_farm_worker", SqlDbType.Bit, 0, "migrant_farm_worker")
                .Parameters.Add("@validation_date", SqlDbType.DateTime, 0, "validation_date")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add(New SqlParameter("@old_hcs_id", SqlDbType.Int, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "hcs_id", DataRowVersion.Original, ""))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_arm_hcs_ids WHERE hcs_id=@hcs_id"
                .Parameters.Add("@hcs_id", SqlDbType.Int, 0, "hcs_id")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
