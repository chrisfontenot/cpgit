﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module Counties
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [county],[state],[name],[bankruptcy_district],[default],[ActiveFlag] FROM counties WITH (NOLOCK)"
            Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "county")
            End If
            Return gdr
        End Function

        <Obsolete("Use GetAll")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Return GetAll(ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_counties"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@state", SqlDbType.Int, 0, "state")
                .Parameters.Add("@name", SqlDbType.VarChar, 50, "name")
                .Parameters.Add("@median_income", SqlDbType.Decimal, 0, "median_income")
                .Parameters.Add("@bankruptcy_district", SqlDbType.Int, 0, "bankruptcy_district")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@fips", SqlDbType.VarChar, 50, "fips")
                .Parameters.Add("@county", SqlDbType.Int, 0, "county").Direction = ParameterDirection.ReturnValue
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "update counties set [state]=@state,[name]=@name,[median_income]=@median_income,[bankruptcy_district]=@bankruptcy_district,[default]=@default,[ActiveFlag]=@ActiveFlag,[fips]=@fips where county = @county"
                .CommandType = CommandType.Text
                .Parameters.Add("@state", SqlDbType.Int, 0, "state")
                .Parameters.Add("@name", SqlDbType.VarChar, 50, "name")
                .Parameters.Add("@median_income", SqlDbType.Decimal, 0, "median_income")
                .Parameters.Add("@bankruptcy_district", SqlDbType.Int, 0, "bankruptcy_district")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@fips", SqlDbType.VarChar, 50, "fips")
                .Parameters.Add("@county", SqlDbType.Int, 0, "county")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM counties WHERE county=@county"
                .Parameters.Add("@county", SqlDbType.Int, 0, "county")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace