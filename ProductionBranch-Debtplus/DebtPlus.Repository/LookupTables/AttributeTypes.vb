
Imports System.Data.SqlClient

Namespace LookupTables
    Public Module AttributeTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[Grouping],[Attribute],[Default],[ActiveFlag],[LanguageID],[HUDLanguage],[HUDServiceType],[hpf],[RxOffice] FROM AttributeTypes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID], [Grouping], [Attribute], [Default], [ActiveFlag] FROM AttributeTypes WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        'select	oID					as 'item_key',
        '		[Attribute]			as 'description',
        '		[default]			as 'default',
        '		[ActiveFlag]		as 'ActiveFlag'
        'from	AttributeTypes
        'where	[Grouping] = 'LANGUAGE'
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllLanguages(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "lst_languages"
            Return FillNamedDataTableFromSproc(query, ds, tableName)
        End Function

        ' can't use the above since it renames the oID column
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAllLanguages2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT oID, Attribute, [default], [ActiveFlag] FROM AttributeTypes WHERE grouping = 'LANGUAGE'"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        ' can't use the above since it renames the oID column
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetLanguageId(ByVal language As Int32) As GetScalarResult
            Dim query As String = String.Format("SELECT LanguageID FROM AttributeTypes WHERE oID={0:f0}", language)
            Return ExecuteScalar(query)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_AttributeTypes"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@Grouping", SqlDbType.VarChar, 50, "Grouping")
                .Parameters.Add("@Attribute", SqlDbType.VarChar, 50, "Attribute")
                .Parameters.Add("@LanguageID", SqlDbType.VarChar, 50, "LanguageID")
                .Parameters.Add("@HUDLanguage", SqlDbType.VarChar, 10, "HUDLanguage")
                .Parameters.Add("@HUDServiceType", SqlDbType.VarChar, 10, "HUDLanguage")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE AttributeTypes SET [Grouping]=@Grouping,[Attribute]=@Attribute,[LanguageID]=@LanguageID,[HUDLanguage]=@HUDLanguage,[HUDServiceType]=@HUDServiceType,[Default]=@Default,[ActiveFlag]=@ActiveFlag WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@Grouping", SqlDbType.VarChar, 50, "Grouping")
                .Parameters.Add("@Attribute", SqlDbType.VarChar, 50, "Attribute")
                .Parameters.Add("@LanguageID", SqlDbType.VarChar, 50, "LanguageID")
                .Parameters.Add("@HUDLanguage", SqlDbType.VarChar, 10, "HUDLanguage")
                .Parameters.Add("@HUDServiceType", SqlDbType.VarChar, 10, "HUDLanguage")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM AttributeTypes WHERE [oID]=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module
End Namespace
