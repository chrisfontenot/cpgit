﻿Imports System.Data.SqlClient

Namespace LookupTables

    Public Module Calendar

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [dt],[isWeekday],[isHoliday],[isBankHoliday],[Y],[FY],[Q],[M],[FM],[D],[DW],[DOY],[DayNumber],[monthname],[dayname],[W],[UTCOffset],[HolidayDescription] FROM calendar"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [dt], [isWeekday], [isHoliday], [isBankHoliday] FROM calendar WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO calendar (dt, isWeekday, isHoliday, isBankHoliday, Y, FY, Q, M, FM, D, DW, DOY, DayNumber, monthname, dayname, W, UTCOffset,  HolidayDescription) values (@dt, @isWeekday, @isHoliday, @isBankHoliday, @Y, @FY, @Q, @M, @FM, @D, @DW, @DOY, @DayNumber, @monthname, @dayname, @W, @UTCOffset,  @HolidayDescription)"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_dt", SqlDbType.DateTime, 0, "dt")
                .Parameters.Add("@isWeekday", SqlDbType.Bit, 0, "isWeekday")
                .Parameters.Add("@isHoliday", SqlDbType.Bit, 0, "isHoliday")
                .Parameters.Add("@isBankHoliday", SqlDbType.Bit, 0, "isBankHoliday")
                .Parameters.Add("@Y", SqlDbType.Int, 0, "Y")
                .Parameters.Add("@FY", SqlDbType.Int, 0, "FY")
                .Parameters.Add("@Q", SqlDbType.Int, 0, "Q")
                .Parameters.Add("@M", SqlDbType.Int, 0, "M")
                .Parameters.Add("@FM", SqlDbType.Int, 0, "FM")
                .Parameters.Add("@D", SqlDbType.Int, 0, "D")
                .Parameters.Add("@DW", SqlDbType.Int, 0, "DW")
                .Parameters.Add("@DOY", SqlDbType.Int, 0, "DOY")
                .Parameters.Add("@DayNumber", SqlDbType.Int, 0, "DayNumber")
                .Parameters.Add("@monthname", SqlDbType.VarChar, 20, "monthname")
                .Parameters.Add("@dayname", SqlDbType.VarChar, 20, "dayname")
                .Parameters.Add("@W", SqlDbType.Int, 0, "W")
                .Parameters.Add("@UTCOffset", SqlDbType.Int, 0, "UTCOffset")
                .Parameters.Add("@HolidayDescription", SqlDbType.VarChar, 50, "HolidayDescription")
                .Parameters.Add(New SqlParameter("@old_dt", SqlDbType.DateTime, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "dt", DataRowVersion.Original, Nothing))
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE calendar SET dt=@new_dt, isWeekday=@isWeekday, isHoliday=@isHoliday, isBankHoliday=@isBankHoliday, Y=@Y, FY=@FY, Q=@Q, M=@M, FM=@FM, D=@D, DW=@DW, DOY=@DOY, DayNumber=@DayNumber, monthname=@monthname, dayname=@dayname, W=@W, UTCOffset=@UTCOffset,  HolidayDescription=@HolidayDescription WHERE dt=@old_dt"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_dt", SqlDbType.DateTime, 0, "dt")
                .Parameters.Add("@isWeekday", SqlDbType.Bit, 0, "isWeekday")
                .Parameters.Add("@isHoliday", SqlDbType.Bit, 0, "isHoliday")
                .Parameters.Add("@isBankHoliday", SqlDbType.Bit, 0, "isBankHoliday")
                .Parameters.Add("@Y", SqlDbType.Int, 0, "Y")
                .Parameters.Add("@FY", SqlDbType.Int, 0, "FY")
                .Parameters.Add("@Q", SqlDbType.Int, 0, "Q")
                .Parameters.Add("@M", SqlDbType.Int, 0, "M")
                .Parameters.Add("@FM", SqlDbType.Int, 0, "FM")
                .Parameters.Add("@D", SqlDbType.Int, 0, "D")
                .Parameters.Add("@DW", SqlDbType.Int, 0, "DW")
                .Parameters.Add("@DOY", SqlDbType.Int, 0, "DOY")
                .Parameters.Add("@DayNumber", SqlDbType.Int, 0, "DayNumber")
                .Parameters.Add("@monthname", SqlDbType.VarChar, 20, "monthname")
                .Parameters.Add("@dayname", SqlDbType.VarChar, 20, "dayname")
                .Parameters.Add("@W", SqlDbType.Int, 0, "W")
                .Parameters.Add("@UTCOffset", SqlDbType.Int, 0, "UTCOffset")
                .Parameters.Add("@HolidayDescription", SqlDbType.VarChar, 50, "HolidayDescription")
                .Parameters.Add(New SqlParameter("@old_dt", SqlDbType.DateTime, 0, ParameterDirection.Input, False, CByte(0), CByte(0), "dt", DataRowVersion.Original, Nothing))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM calendar WHERE dt=@dt"
                .Parameters.Add("@dt", SqlDbType.Int, 0, "dt")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAchEffectiveDate() As GetScalarResult
            Const query As String = "SELECT TOP 1 dt FROM calendar WHERE [isBankHoliday]=0 AND [isWeekday]=1 AND [dt] > getdate() ORDER BY [dt]"
            Return Common.ExecuteScalar(query)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DidGetAchEffectiveDateInformation(ByVal dateToValidate As System.DateTime, ByRef isWeekday As Boolean, ByRef isBankHoliday As Boolean, ByRef holidayDescription As String) As GetDataResult
            Dim rd As SqlClient.SqlDataReader = Nothing
            Dim gdr As New GetDataResult()

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "SELECT dt, isWeekday, isBankHoliday, HolidayDescription FROM calendar WHERE dt = @dt"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@dt", SqlDbType.DateTime).Value = dateToValidate
                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                End With

                If rd IsNot Nothing AndAlso rd.Read() Then
                    isWeekday = Convert.ToBoolean(rd.GetValue(1))
                    isBankHoliday = Convert.ToBoolean(rd.GetValue(2))
                    holidayDescription = String.Empty
                    If Not rd.IsDBNull(3) Then holidayDescription = rd.GetString(3)
                    gdr.RowsAffected = 1
                End If

            Catch ex As SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                cn.Dispose()
            End Try

            Return gdr
        End Function

    End Module

End Namespace