Imports System.Data.SqlClient

Namespace LookupTables

    Public Module HousingARMHCSIDLanguages

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[hcs_id],[attribute] FROM housing_arm_hcs_id_languages"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) OrElse id <= 0 Then Throw RepositoryException.BadParameters()

            Dim gdr As GetDataResult = New GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT [oID],[hcs_id],[attribute] FROM housing_arm_hcs_id_languages WHERE [hcs_id]=@hcs_id"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@hcs_id", SqlDbType.Int).Value = id

                cn.Open()

                da.SelectCommand = cmd
                gdr.RowsAffected = da.Fill(ds, tableName)
                da.FillSchema(ds, SchemaType.Source, tableName)

            Catch ex As System.Data.SqlClient.SqlException
                Return gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                If cmd IsNot Nothing Then cmd.Dispose()
                If da IsNot Nothing Then da.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_housing_arm_hcs_id_language"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@hcs_id", SqlDbType.Int, 0, "hcs_id")
                .Parameters.Add("@attribute", SqlDbType.Int, 0, "attribute")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_arm_hcs_id_languages WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
