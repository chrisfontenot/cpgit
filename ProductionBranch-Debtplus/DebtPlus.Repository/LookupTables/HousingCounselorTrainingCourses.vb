Imports System.Data.SqlClient

Namespace LookupTables

    Public Module HousingCounselorTrainingCourses

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

            If ds Is Nothing Then Throw New ArgumentNullException("ds")
            If String.IsNullOrWhiteSpace(tableName) Then Throw New ArgumentOutOfRangeException("tableName")

            Dim gdr As New GetDataResult()

            Try
                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand()
                        Using da As SqlDataAdapter = New SqlDataAdapter(cmd)
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [oID],[Counselor],[TrainingCourse],[TrainingDate],[date_created],[created_by] FROM housing_counselor_training_courses"
                            cmd.CommandType = CommandType.Text

                            If id > 0 Then
                                cmd.CommandText += " WHERE [counselor]=@counselor"
                                cmd.Parameters.Add("@Counselor", SqlDbType.Int).Value = id

                                da.Fill(ds, tableName)
                            Else
                                da.FillSchema(ds, SchemaType.Source, tableName)
                            End If
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable, ByVal Counselor As Object) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_housing_counselor_training_course"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@Counselor", SqlDbType.Int, 4).Value = Counselor
                .Parameters.Add("@TrainingCourse", SqlDbType.Int, 4, "TrainingCourse")
                .Parameters.Add("@TrainingDate", SqlDbType.DateTime, 8, "TrainingDate")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_counselor_training_courses SET [Counselor]=@Counselor,[TrainingCourse]=@TrainingCourse,[TrainingDate]=@TrainingDate WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@Counselor", SqlDbType.Int, 4).Value = Counselor
                .Parameters.Add("@TrainingCourse", SqlDbType.Int, 4, "TrainingCourse")
                .Parameters.Add("@TrainingDate", SqlDbType.DateTime, 8, "TrainingDate")
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_counselor_training_courses WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                da.ContinueUpdateOnError = True

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
