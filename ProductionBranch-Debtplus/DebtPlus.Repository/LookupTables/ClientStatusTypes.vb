
Imports System.Data.SqlClient

Namespace LookupTables

    Public Module ClientStatusTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [client_status], [description], [ActiveFlag], [Default] FROM ClientStatusTypes WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByRef dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO ClientStatusTypes([client_status],[Description],[Default],[ActiveFlag]) VALUES (@client_status,@Description,@Default,@ActiveFlag);"
                .Parameters.Add("@client_status", SqlDbType.VarChar, 10, "client_status")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE ClientStatusTypes SET [client_status]=@new_client_status,[Description]=@Description,[Default]=@Default,[ActiveFlag]=@ActiveFlag WHERE [client_status]=@old_client_status"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_client_status", SqlDbType.VarChar, 10, "client_status")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add(New SqlClient.SqlParameter("@old_client_status", SqlDbType.VarChar, 10, ParameterDirection.Input, False, CByte(0), CByte(0), "client_status", DataRowVersion.Original, ""))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM ClientStatusTypes WHERE client_status=@client_status"
                .Parameters.Add("@client_status", SqlDbType.VarChar, 10, "client_status")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace

