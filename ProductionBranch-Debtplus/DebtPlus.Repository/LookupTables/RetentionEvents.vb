Imports System.Data.SqlClient

Namespace LookupTables

    Public Module RetentionEvents

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT retention_event as item_key, description FROM retention_events WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [retention_event],[description],[priority],[initial_retention_action],[expire_type],[date_created],[created_by] FROM retention_events"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT retention_event,description,date_created,created_by FROM retention_events WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_retention_events"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@retention_event", SqlDbType.Int, 0, "retention_event").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@priority", SqlDbType.Int, 0, "priority")
                .Parameters.Add("@initial_retention_action", SqlDbType.Int, 0, "initial_retention_action")
                .Parameters.Add("@expire_type", SqlDbType.Int, 0, "expire_type")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE retention_events SET [description]=@description, [priority]=@priority, [initial_retention_action]=@initial_retention_action, [expire_type]=@expire_type WHERE retention_event=@retention_event"
                .CommandType = CommandType.Text
                .Parameters.Add("@retention_event", SqlDbType.Int, 0, "retention_event")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@priority", SqlDbType.Int, 0, "priority")
                .Parameters.Add("@initial_retention_action", SqlDbType.Int, 0, "initial_retention_action")
                .Parameters.Add("@expire_type", SqlDbType.Int, 0, "expire_type")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM retention_events WHERE retention_event=@retention_event"
                .Parameters.Add("@retention_event", SqlDbType.Int, 0, "retention_event")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module

End Namespace
