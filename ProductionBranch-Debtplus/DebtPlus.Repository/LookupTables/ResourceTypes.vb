﻿Imports DebtPlus.Models
Imports System.Data.SqlClient

Namespace LookupTables
    Public Module ResourceTypes
        Private Const SelectStatement As String = "SELECT [resource_type],[label],[description] FROM [resource_types] WITH (NOLOCK)"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim query As String = SelectStatement + " ORDER BY description"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Insert(ByVal resourceType As ResourceType) As Repository.GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If resourceType Is Nothing Then Throw New ArgumentNullException("resource type may not be null")

            Dim gdr As New Repository.GetDataResult()
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "INSERT INTO resource_types (label,description) VALUES('@Label','@Description'); SELECT scope_identity() AS resource_type"
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@Label", SqlDbType.VarChar, 14).Value = resourceType.Label
                    cmd.Parameters.Add("@Description", SqlDbType.VarChar, 50).Value = resourceType.Description

                    resourceType.Id = cmd.ExecuteScalar()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function LabelExists(ByVal label As String) As Boolean
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT resource_type FROM resource_types WITH (NOLOCK) WHERE label=@Label"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@Label", SqlDbType.VarChar, 14).Value = label

                cn.Open()
                Dim result = cmd.ExecuteScalar()

                If result IsNot Nothing Then Return True

            Catch ex As SqlClient.SqlException
                Dim gdr As New Repository.GetDataResult()
                gdr.HandleException(ex)
                ' TODO : We should handle the exception here but that causes a circular reference

            Finally
                If cmd IsNot Nothing Then cmd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return False
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DescriptionExists(ByVal description As String) As Boolean
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlCommand = New SqlCommand

            Try
                cmd.Connection = cn
                cmd.CommandText = "SELECT resource_type FROM resource_types WITH (NOLOCK) WHERE description=@Description"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@Description", SqlDbType.VarChar, 14).Value = description

                cn.Open()
                Dim result = cmd.ExecuteScalar()

                If result IsNot Nothing Then Return True

            Catch ex As SqlClient.SqlException
                Dim gdr As New Repository.GetDataResult()
                gdr.HandleException(ex)
                ' TODO : We should handle the exception here but that causes a circular reference

            Finally
                If cmd IsNot Nothing Then cmd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return False
        End Function
    End Module
End Namespace