Imports System.Data.SqlClient

Namespace LookupTables

    Public Module HousingPurposeOfVisitTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[description],[HUD_9902_section],[HourlyGrantAmountUsed],[ActiveFlag],[Default],[HousingFeeAmount] FROM housing_PurposeOfVisitTypes ORDER BY description, oID"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID], [description], [HUD_9902_section], [ActiveFlag], [Default], [HousingFeeAmount] FROM Housing_PurposeOfVisitTypes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_housing_PurposeOfVisitType"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 10, "HUD_9902_section")
                .Parameters.Add("@HourlyGrantAmountUsed", SqlDbType.Decimal, 8, "HourlyGrantAmountUsed")
                .Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 8, "HousingFeeAmount")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 1, "ActiveFlag")
                .Parameters.Add("@Default", SqlDbType.Bit, 1, "Default")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_PurposeOfVisitTypes SET [description]=@description,[HUD_9902_section]=@HUD_9902_section,[HourlyGrantAmountUsed]=@HourlyGrantAmountUsed,[HousingFeeAmount]=@HousingFeeAmount,[ActiveFlag]=@ActiveFlag,[Default]=@Default WHERE [oID]=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 10, "HUD_9902_section")
                .Parameters.Add("@HourlyGrantAmountUsed", SqlDbType.Decimal, 8, "HourlyGrantAmountUsed")
                .Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 8, "HousingFeeAmount")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 1, "ActiveFlag")
                .Parameters.Add("@Default", SqlDbType.Bit, 1, "Default")
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_PurposeOfVisitTypes WHERE [oID]=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 4, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
