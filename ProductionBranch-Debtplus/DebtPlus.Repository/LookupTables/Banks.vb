﻿Imports System.Drawing
Imports System.Data.SqlClient
Imports System.IO

Namespace LookupTables
    Public Module Banks
        Private Const SelectStatement_banks As String = "SELECT [bank],[description],[Default],[ActiveFlag],[type],[ContactNameID],[bank_name],[BankAddressID],[aba],[account_number],[immediate_origin],[immediate_origin_name],[ach_priority],[ach_company_id],[ach_origin_dfi],[ach_enable_offset],[ach_company_identification],[ach_message_authentication],[ach_batch_company_id],[immediate_destination],[immediate_destination_name],[checknum],[max_clients_per_check],[max_amt_per_check],[batch_number],[transaction_number],convert(image,null) as 'SignatureImage',[prefix_line],[suffix_line],[output_directory],[created_by],[date_created] FROM [banks] WITH (NOLOCK)"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr As GetDataResult = FillNamedDataTableFromSelectCommand(SelectStatement_banks, ds, tableName)
            If gdr.Success Then
                setPK(ds, tableName)
            End If
            Return gdr
        End Function

        Private Sub setPK(ByVal ds As DataSet, ByVal tableName As String)

            Dim tbl As DataTable = ds.Tables(tableName)
            If tbl Is Nothing Then Return

            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "bank")

            If Not tbl.Columns.Contains("formatted_check") Then
                tbl.Columns.Add("formatted_check", GetType(String), "iif([type]='A','ACH', iif([type]='C','Checking', iif([type]='D','Deposit', iif([type]='R','RPPS', 'Uknown'))))")
            End If

            If Not tbl.Columns.Contains("formatted_proposal") Then
                tbl.Columns.Add("formatted_proposal", GetType(String), "iif([type]='C','Printed', iif([type]='R','RPPS', 'Uknown'))")
            End If
        End Sub

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetNextCheckNumber(ByVal bank As Int32) As GetScalarResult
            If bank <= 0 Then Throw New ArgumentOutOfRangeException("bank<=0")

            Using cmd As SqlCommand = New SqlCommand()
                cmd.CommandText = "SELECT [checknum] FROM [banks] WITH (NOLOCK) WHERE [bank]=@bank"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank
                Return ExecuteScalar(cmd)
            End Using
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function UpdateCheckNumberByBank(ByVal newCheckNumber As Int64, ByVal bank As Int32) As GetDataResult
            Using cmd As SqlCommand = New SqlCommand()
                cmd.CommandText = "UPDATE [banks] SET [checknum]=@checknum WHERE [bank]=@bank"
                cmd.CommandType = CommandType.Text
                cmd.Parameters.Add("@checknum", SqlDbType.Int).Value = newCheckNumber
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank
                Return ExecuteNonQuery(cmd)
            End Using
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function SaveSignature(ByVal id As Int32, ByVal signatureImage As Image) As Repository.GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            Dim rslt As New Repository.GetDataResult()

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandType = CommandType.Text

                    ' If there is an image then save it
                    If signatureImage IsNot Nothing AndAlso Not signatureImage.Size.IsEmpty Then
                        Using memStream As New MemoryStream()
                            signatureImage.Save(memStream, Imaging.ImageFormat.Png)
                            Dim imageLength As Long = memStream.Length
                            If imageLength > Int32.MaxValue Then Throw New RepositoryException("Signature image too big")

                            cmd.CommandText = "UPDATE [banks] SET [SignatureImage] = @signature WHERE [bank] = @bank"
                            cmd.Parameters.Add("@signature", SqlDbType.Image, Convert.ToInt32(imageLength)).Value = memStream.ToArray()
                        End Using

                    Else

                        ' There is no image. Set the image to NULL in the tables
                        cmd.CommandText = "UPDATE [banks] SET [SignatureImage] = NULL WHERE [bank] = @bank"
                    End If

                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = id
                    cmd.ExecuteNonQuery()
                End Using

            Catch ex As SqlClient.SqlException
                rslt.HandleException(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()

            End Try

            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetSignatureByBank(ByVal id As Int32) As GetScalarResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If id <= 0 Then Throw New ArgumentException("id<=0")

            Dim gsr As New GetScalarResult()
            Dim reader As SqlDataReader = Nothing
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                Using cmd As New SqlCommand()
                    cmd.Connection = cn
                    cmd.CommandText = "SELECT [SignatureImage] FROM [banks] WHERE [bank] = @bank"
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = id

                    reader = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SequentialAccess)
                End Using

                If reader IsNot Nothing AndAlso reader.Read() AndAlso Not reader.IsDBNull(0) Then
                    gsr.RowsAffected = 1

                    Dim ab(4096) As Byte
                    Dim ByteCount As Long
                    Dim ByteOffset As Int32 = 0

                    Using memStream As New MemoryStream()
                        ByteCount = reader.GetBytes(0, 0, ab, 0, 4096)
                        Do While ByteCount = 4096
                            memStream.Write(ab, 0, 4096)
                            ByteOffset += 4096
                            ByteCount = reader.GetBytes(0, ByteOffset, ab, 0, 4096)
                        Loop

                        If ByteCount > 0 Then
                            memStream.Write(ab, 0, Convert.ToInt32(ByteCount))
                        End If

                        ' Convert the byte buffer to an image. Must rewind before starting read operation.
                        memStream.Seek(0, SeekOrigin.Begin)
                        gsr.Result = Image.FromStream(memStream)
                    End Using
                End If

            Catch ex As SqlClient.SqlException
                gsr.HandleException(ex)

            Finally
                If reader IsNot Nothing Then reader.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return gsr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim insertCommand As SqlCommand = New SqlCommand()
            Dim updateCommand As SqlCommand = New SqlCommand()
            Dim deleteCommand As SqlCommand = New SqlCommand()

            Try
                cn.Open()
                Using da As SqlDataAdapter = New SqlDataAdapter()

                    ' Insert command
                    insertCommand.Connection = cn
                    insertCommand.CommandText = "xpr_insert_banks"
                    insertCommand.CommandType = CommandType.StoredProcedure
                    insertCommand.Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                    insertCommand.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                    insertCommand.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                    insertCommand.Parameters.Add("@type", SqlDbType.VarChar, 1, "type")
                    insertCommand.Parameters.Add("@ContactNameID", SqlDbType.Int, 0, "ContactNameID")
                    insertCommand.Parameters.Add("@bank_name", SqlDbType.VarChar, 80, "bank_name")
                    insertCommand.Parameters.Add("@BankAddressID", SqlDbType.Int, 0, "BankAddressID")
                    insertCommand.Parameters.Add("@aba", SqlDbType.VarChar, 9, "aba")
                    insertCommand.Parameters.Add("@account_number", SqlDbType.VarChar, 40, "account_number")
                    insertCommand.Parameters.Add("@immediate_origin", SqlDbType.VarChar, 12, "immediate_origin")
                    insertCommand.Parameters.Add("@immediate_origin_name", SqlDbType.VarChar, 30, "immediate_origin_name")
                    insertCommand.Parameters.Add("@ach_priority", SqlDbType.VarChar, 2, "ach_priority")
                    insertCommand.Parameters.Add("@ach_company_id", SqlDbType.VarChar, 10, "ach_company_id")
                    insertCommand.Parameters.Add("@ach_origin_dfi", SqlDbType.VarChar, 12, "ach_origin_dfi")
                    insertCommand.Parameters.Add("@ach_enable_offset", SqlDbType.Bit, 0, "ach_enable_offset")
                    insertCommand.Parameters.Add("@ach_company_identification", SqlDbType.VarChar, 10, "ach_company_identification")
                    insertCommand.Parameters.Add("@ach_message_authentication", SqlDbType.VarChar, 19, "ach_message_authentication")
                    insertCommand.Parameters.Add("@ach_batch_company_id", SqlDbType.VarChar, 10, "ach_batch_company_id")
                    insertCommand.Parameters.Add("@immediate_destination", SqlDbType.VarChar, 30, "immediate_destination")
                    insertCommand.Parameters.Add("@immediate_destination_name", SqlDbType.VarChar, 30, "immediate_destination_name")
                    insertCommand.Parameters.Add("@checknum", SqlDbType.Int, 0, "checknum")
                    insertCommand.Parameters.Add("@max_clients_per_check", SqlDbType.Int, 0, "max_clients_per_check")
                    insertCommand.Parameters.Add("@max_amt_per_check", SqlDbType.Decimal, 0, "max_amt_per_check")
                    insertCommand.Parameters.Add("@batch_number", SqlDbType.Int, 0, "batch_number")
                    insertCommand.Parameters.Add("@transaction_number", SqlDbType.Int, 0, "transaction_number")
                    insertCommand.Parameters.Add("@prefix_line", SqlDbType.VarChar, 256, "prefix_line")
                    insertCommand.Parameters.Add("@suffix_line", SqlDbType.VarChar, 256, "suffix_line")
                    insertCommand.Parameters.Add("@output_directory", SqlDbType.VarChar, 256, "output_directory")
                    insertCommand.Parameters.Add("@bank", SqlDbType.Int, 0, "bank").Direction = ParameterDirection.ReturnValue
                    da.InsertCommand = insertCommand

                    ' Update command
                    updateCommand.Connection = cn
                    updateCommand.CommandText = "UPDATE banks SET [description]=@description,[default]=@default,[ActiveFlag]=@ActiveFlag,[type]=@type,[ContactNameID]=@ContactNameID,[bank_name]=@bank_name,[BankAddressID]=@BankAddressID,[aba]=@aba,[account_number]=@account_number,[immediate_origin]=@immediate_origin,[immediate_origin_name]=@immediate_origin_name,[ach_priority]=@ach_priority,[ach_company_id]=@ach_company_id,[ach_origin_dfi]=@ach_origin_dfi,[ach_enable_offset]=@ach_enable_offset,[ach_company_identification]=@ach_company_identification,[ach_message_authentication]=@ach_message_authentication,[ach_batch_company_id]=@ach_batch_company_id,[immediate_destination]=@immediate_destination,[immediate_destination_name]=@immediate_destination_name,[checknum]=@checknum,[max_clients_per_check]=@max_clients_per_check,[max_amt_per_check]=@max_amt_per_check,[batch_number]=@batch_number,[transaction_number]=@transaction_number,[prefix_line]=@prefix_line,[suffix_line]=@suffix_line,[output_directory]=@output_directory WHERE bank = @bank"
                    updateCommand.CommandType = CommandType.Text
                    updateCommand.Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                    updateCommand.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                    updateCommand.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                    updateCommand.Parameters.Add("@type", SqlDbType.VarChar, 1, "type")
                    updateCommand.Parameters.Add("@ContactNameID", SqlDbType.Int, 0, "ContactNameID")
                    updateCommand.Parameters.Add("@bank_name", SqlDbType.VarChar, 80, "bank_name")
                    updateCommand.Parameters.Add("@BankAddressID", SqlDbType.Int, 0, "BankAddressID")
                    updateCommand.Parameters.Add("@aba", SqlDbType.VarChar, 9, "aba")
                    updateCommand.Parameters.Add("@account_number", SqlDbType.VarChar, 40, "account_number")
                    updateCommand.Parameters.Add("@immediate_origin", SqlDbType.VarChar, 12, "immediate_origin")
                    updateCommand.Parameters.Add("@immediate_origin_name", SqlDbType.VarChar, 30, "immediate_origin_name")
                    updateCommand.Parameters.Add("@ach_priority", SqlDbType.VarChar, 2, "ach_priority")
                    updateCommand.Parameters.Add("@ach_company_id", SqlDbType.VarChar, 10, "ach_company_id")
                    updateCommand.Parameters.Add("@ach_origin_dfi", SqlDbType.VarChar, 12, "ach_origin_dfi")
                    updateCommand.Parameters.Add("@ach_enable_offset", SqlDbType.Bit, 0, "ach_enable_offset")
                    updateCommand.Parameters.Add("@ach_company_identification", SqlDbType.VarChar, 10, "ach_company_identification")
                    updateCommand.Parameters.Add("@ach_message_authentication", SqlDbType.VarChar, 19, "ach_message_authentication")
                    updateCommand.Parameters.Add("@ach_batch_company_id", SqlDbType.VarChar, 10, "ach_batch_company_id")
                    updateCommand.Parameters.Add("@immediate_destination", SqlDbType.VarChar, 30, "immediate_destination")
                    updateCommand.Parameters.Add("@immediate_destination_name", SqlDbType.VarChar, 30, "immediate_destination_name")
                    updateCommand.Parameters.Add("@checknum", SqlDbType.Int, 0, "checknum")
                    updateCommand.Parameters.Add("@max_clients_per_check", SqlDbType.Int, 0, "max_clients_per_check")
                    updateCommand.Parameters.Add("@max_amt_per_check", SqlDbType.Decimal, 0, "max_amt_per_check")
                    updateCommand.Parameters.Add("@batch_number", SqlDbType.Int, 0, "batch_number")
                    updateCommand.Parameters.Add("@transaction_number", SqlDbType.Int, 0, "transaction_number")
                    updateCommand.Parameters.Add("@prefix_line", SqlDbType.VarChar, 256, "prefix_line")
                    updateCommand.Parameters.Add("@suffix_line", SqlDbType.VarChar, 256, "suffix_line")
                    updateCommand.Parameters.Add("@output_directory", SqlDbType.VarChar, 256, "output_directory")
                    updateCommand.Parameters.Add("@bank", SqlDbType.Int, 0, "bank")
                    da.UpdateCommand = updateCommand

                    ' Delete command
                    deleteCommand.Connection = cn
                    deleteCommand.CommandText = "DELETE FROM [banks] WHERE [bank] = @bank"
                    deleteCommand.CommandType = CommandType.Text
                    deleteCommand.Parameters.Add("@bank", SqlDbType.Int, 0, "bank")
                    da.DeleteCommand = deleteCommand

                    ccr.RowsAffected = da.Update(dt)
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                ccr.HandleException(ex)

            Finally
                updateCommand.Dispose()
                insertCommand.Dispose()
                deleteCommand.Dispose()
                cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace
