Imports System.Data.SqlClient

Namespace LookupTables

    Public Module BankruptcyDistricts

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [bankruptcy_district] as 'bankruptcy_district', [description] as 'description', [default] as 'default', ActiveFlag as 'ActiveFlag' FROM bankruptcy_districts"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_bankruptcy_districts"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "bankruptcy_district").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE bankruptcy_districts SET description=@description,[default]=@default,[ActiveFlag]=@ActiveFlag WHERE bankruptcy_district=@bankruptcy_district"
                .CommandType = CommandType.Text
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@bankruptcy_district", SqlDbType.VarChar, 10, "bankruptcy_district")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM bankruptcy_districts WHERE [bankruptcy_district]=@bankruptcy_district"
                .Parameters.Add("@bankruptcy_district", SqlDbType.Int, 0, "bankruptcy_district")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
