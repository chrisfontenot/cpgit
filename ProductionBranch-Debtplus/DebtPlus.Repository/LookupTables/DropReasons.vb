﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module DropReasons

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [drop_reason],[description],[rpps_code],[Default],[ActiveFlag] FROM [drop_reasons] WITH (NOLOCK)"
            Dim gdr As GetDataResult = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                TableProcessing.setAutoIncrementPK(ds, tableName, "drop_reason")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                '.CommandText = "xpr_insert_drop_reasons"
                '.CommandType = CommandType.StoredProcedure
                .CommandText = "insert into drop_reasons ( [description], [rpps_code], [Default], [ActiveFlag]) values ( @description, @nfcc, @rpps_code, @Default, @ActiveFlag); Select drop_reason, [description], [rpps_code], [Default], [ActiveFlag] from drop_reasons where drop_reason = scope_identity();"
                .CommandType = CommandType.Text
                '.Parameters.Add("RETURN", SqlDbType.Int, 0, "drop_reason").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@rpps_code", SqlDbType.VarChar, 2, "rpps_code")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE drop_reasons SET description=@description,rpps_code=@rpps_code,[Default]=@Default,[ActiveFlag]=@ActiveFlag WHERE drop_reason=@drop_reason"
                .CommandType = CommandType.Text
                .Parameters.Add("@drop_reason", SqlDbType.Int, 0, "drop_reason")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@rpps_code", SqlDbType.VarChar, 10, "rpps_code")
                .Parameters.Add("@nfcc", SqlDbType.VarChar, 10, "nfcc")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM drop_reasons WHERE drop_reason=@drop_reason"
                .Parameters.Add("@drop_reason", SqlDbType.Int, 0, "drop_reason")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                da.InsertCommand.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                updateCommand.Dispose()
                insertCommand.Dispose()
                deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace