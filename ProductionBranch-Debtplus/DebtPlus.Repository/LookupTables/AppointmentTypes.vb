Imports System.Data.SqlClient

Namespace LookupTables

    Public Module AppointmentTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[Description],[Duration],[BookLetter],[RescheduleLetter],[CancelLetter],[ContactType],[BankruptcyClass],[MissedAppointmentRetentionEvent],[Default],[ActiveFlag],[isInitialAppointment] FROM AppointmentTypes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_AppointmentTypes"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@BankruptcyClass", SqlDbType.Bit, 0, "BankruptcyClass")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@Duration", SqlDbType.Int, 0, "Duration")
                .Parameters.Add("@BookLetter", SqlDbType.VarChar, 10, "BookLetter")
                .Parameters.Add("@RescheduleLetter", SqlDbType.VarChar, 10, "RescheduleLetter")
                .Parameters.Add("@CancelLetter", SqlDbType.VarChar, 10, "CancelLetter")
                .Parameters.Add("@MissedAppointmentRetentionEvent", SqlDbType.Int, 0, "MissedAppointmentRetentionEvent")
                .Parameters.Add("@ContactType", SqlDbType.VarChar, 4, "ContactType")
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE AppointmentTypes SET [Default]=@Default,[ActiveFlag]=@ActiveFlag,[BankruptcyClass]=@BankruptcyClass,[Description]=@Description,[Duration]=@Duration,[BookLetter]=@BookLetter,[RescheduleLetter]=@RescheduleLetter,[CancelLetter]=@CancelLetter,[MissedAppointmentRetentionEvent]=@MissedAppointmentRetentionEvent,[ContactType]=@ContactType WHERE [oID] = @oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@BankruptcyClass", SqlDbType.Bit, 0, "BankruptcyClass")
                .Parameters.Add("@Description", SqlDbType.VarChar, 50, "Description")
                .Parameters.Add("@Duration", SqlDbType.Int, 0, "Duration")
                .Parameters.Add("@BookLetter", SqlDbType.VarChar, 10, "BookLetter")
                .Parameters.Add("@RescheduleLetter", SqlDbType.VarChar, 10, "RescheduleLetter")
                .Parameters.Add("@CancelLetter", SqlDbType.VarChar, 10, "CancelLetter")
                .Parameters.Add("@MissedAppointmentRetentionEvent", SqlDbType.Int, 0, "MissedAppointmentRetentionEvent")
                .Parameters.Add("@ContactType", SqlDbType.VarChar, 4, "ContactType")
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM AppointmentTypes WHERE [oID]=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                da.ContinueUpdateOnError = True

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
