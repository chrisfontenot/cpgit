﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module CreditorContactTypes
        Private Const SelectStatement_creditor_contact_types As String = "SELECT [creditor_contact_type],[name],[description],[contact_type],[Default],[ActiveFlag],[date_created],[created_by] FROM [creditor_contact_types] WITH (NOLOCK)"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_creditor_contact_types, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "creditor_contact_type")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_creditor_contact_type"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "creditor_contact_type").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@name", SqlDbType.VarChar, 50, "name")
                .Parameters.Add("@contact_type", SqlDbType.VarChar, 50, "contact_type")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE creditor_contact_types SET [name]=@name,[description]=@description,[contact_type]=@contact_type WHERE creditor_contact_type=@creditor_contact_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@name", SqlDbType.VarChar, 50, "name")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@contact_type", SqlDbType.VarChar, 50, "contact_type")
                .Parameters.Add("@creditor_contact_type", SqlDbType.Int, 0, "creditor_contact_type")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM creditor_contact_types WHERE creditor_contact_type=@creditor_contact_type"
                .CommandType = CommandType.Text
                .Parameters.Add("@creditor_contact_type", SqlDbType.Int, 0, "creditor_contact_type")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace