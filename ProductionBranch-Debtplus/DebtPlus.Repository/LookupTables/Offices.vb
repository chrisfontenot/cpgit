﻿
Namespace LookupTables

    Public Module Offices

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [office],[name],[ActiveFlag],[Default] FROM offices ORDER BY name"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [Office], [Name], [Type], [Default], [ActiveFlag], [Directions] FROM Offices WITH (NOLOCK)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "lst_offices"
            Return FillNamedDataTableFromSproc(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll4(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT office, name, default, ActiveFlag FROM offices WITH (NOLOCK) ORDER BY office"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

    End Module

End Namespace