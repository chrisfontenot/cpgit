Imports System.Data.SqlClient

Namespace LookupTables

    Public Module RPPSBillerTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[ActiveFlag],[Default],[description] FROM RPPSBillerTypes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO RPPSBillerTypes([oID],[description],[Default],[ActiveFlag]) VALUES (@oID,@description,@Default,@ActiveFlag);"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE RPPSBillerTypes SET [oID]=@oID,[ActiveFlag]=@ActiveFlag,[Default]=@Default,[description]=@description WHERE [oID]=@oldOID"
                .CommandType = CommandType.Text
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add(New SqlParameter("@oldOID", SqlDbType.Int, 4, ParameterDirection.Input, False, CByte(0), CByte(0), "oID", DataRowVersion.Original, Nothing))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM RPPSBillerTypes WHERE oID=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
