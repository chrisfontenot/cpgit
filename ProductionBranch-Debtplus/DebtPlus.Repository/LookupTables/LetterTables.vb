Imports System.Data.SqlClient

Namespace LookupTables

    Public Module LetterTables

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [letter_table],[query_type],[query],[date_created],[created_by] FROM letter_tables"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [letter_table],[query_type],[query],[date_created],[created_by] FROM letter_tables"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_letter_tables"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("RETURN", SqlDbType.Int, 0, "letter_table").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@query_type", SqlDbType.Int, 0, "query_type")
                .Parameters.Add("@query", SqlDbType.VarChar, 256, "query")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE letter_tables SET query_type=@query_type, query=@query WHERE letter_table=@letter_table"
                .CommandType = CommandType.Text
                .Parameters.Add("@query_type", SqlDbType.Int, 0, "query_type")
                .Parameters.Add("@query", SqlDbType.VarChar, 256, "query")
                .Parameters.Add("@letter_table", SqlDbType.Int, 0, "letter_table")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM letter_fields WHERE letter_table=@letter_table; DELETE FROM letter_tables WHERE letter_table=@letter_table"
                .Parameters.Add("@letter_table", SqlDbType.Int, 0, "letter_table")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
