Imports System.Data.SqlClient

Namespace LookupTables

    Public Module RPPSErrorCodes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT rpps_reject_code, description, resend_funds FROM rpps_reject_codes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "INSERT INTO rpps_reject_codes(rpps_reject_code,description,resend_funds) values (@rpps_reject_code,@description,@resend_funds);"
                .Parameters.Add("@rpps_reject_code", SqlDbType.VarChar, 10, "rpps_reject_code")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@resend_funds", SqlDbType.Bit, 0, "resend_funds")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE rpps_reject_codes SET rpps_reject_code=@new_rpps_reject_code,description=@description,resend_funds=@resend_funds WHERE rpps_reject_code=@old_rpps_reject_code"
                .CommandType = CommandType.Text
                .Parameters.Add("@new_rpps_reject_code", SqlDbType.VarChar, 10, "rpps_reject_code")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@resend_funds", SqlDbType.Bit, 0, "resend_funds")
                .Parameters.Add(New SqlParameter("@old_rpps_reject_code", SqlDbType.VarChar, 10, ParameterDirection.Input, False, CByte(0), CByte(0), "rpps_reject_code", DataRowVersion.Original, ""))
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM rpps_reject_codes WHERE rpps_reject_code=@rpps_reject_code"
                .Parameters.Add("@rpps_reject_code", SqlDbType.VarChar, 10, "rpps_reject_code")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
