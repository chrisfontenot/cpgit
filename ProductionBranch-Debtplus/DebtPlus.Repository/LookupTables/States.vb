﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module States
        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [State], [MailingCode], [Name], [Country], [TimeZoneID], [USAFormat], [Default], [ActiveFlag], [AddressFormat], [hud_9902], [DoingBusiness], [created_by], [date_created] FROM [states] WITH (NOLOCK)"
            Dim gdr As GetDataResult = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                assignPK(ds, tableName)
            End If
            Return gdr
        End Function

        Private Sub assignPK(ByVal ds As DataSet, ByVal tableName As String)
            Dim dt As DataTable = ds.Tables(tableName)
            Dim pk As DataColumn = dt.Columns("State")

            ' ensure that there is a primary key to do a search by it
            If dt.PrimaryKey.GetUpperBound(0) < 0 Then
                dt.PrimaryKey = New DataColumn() {pk}
            End If

            ' If the primary key is not auto-increment, make it so
            If Not pk.AutoIncrement Then
                pk.AutoIncrement = True
                pk.AutoIncrementSeed = -1
                pk.AutoIncrementStep = -1
            End If

            ' Add the extra column that is used sometimes to format the state code
            If Not dt.Columns.Contains("FormattedMailingCode") Then
                dt.Columns.Add("FormattedMailingCode", GetType(String), "SUBSTRING(ISNULL([MailingCode],'')+'  ',1,2)")
            End If
        End Sub

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_states"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@Name", SqlDbType.VarChar, 50, "Name")
                .Parameters.Add("@MailingCode", SqlDbType.VarChar, 4, "MailingCode")
                .Parameters.Add("@Country", SqlDbType.Int, 0, "Country")
                .Parameters.Add("@USAFormat", SqlDbType.Bit, 0, "USAFormat")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@AddressFormat", SqlDbType.VarChar, 256, "AddressFormat")
                .Parameters.Add("@hud_9902", SqlDbType.Int, 0, "hud_9902")
                .Parameters.Add("@DoingBusiness", SqlDbType.Bit, 0, "DoingBusiness")
                .Parameters.Add("@TimeZoneID", SqlDbType.Int, 0, "TimeZoneID")
                .Parameters.Add("@state", SqlDbType.Int, 0, "state").Direction = ParameterDirection.ReturnValue
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE states SET [Name]=@Name, [MailingCode]=@MailingCode, [Country]=@Country, [USAFormat]=@USAFormat, [AddressFormat]=@AddressFormat, [Default]=@Default, ActiveFlag=@ActiveFlag, [hud_9902]=@hud_9902, [DoingBusiness]=@DoingBusiness,[TimeZoneID]=@TimeZoneID WHERE state=@state"
                .CommandType = CommandType.Text
                .Parameters.Add("@Name", SqlDbType.VarChar, 50, "Name")
                .Parameters.Add("@MailingCode", SqlDbType.VarChar, 4, "MailingCode")
                .Parameters.Add("@Country", SqlDbType.Int, 0, "Country")
                .Parameters.Add("@USAFormat", SqlDbType.Bit, 0, "USAFormat")
                .Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@AddressFormat", SqlDbType.VarChar, 256, "AddressFormat")
                .Parameters.Add("@hud_9902", SqlDbType.Int, 0, "hud_9902")
                .Parameters.Add("@DoingBusiness", SqlDbType.Bit, 0, "DoingBusiness")
                .Parameters.Add("@TimeZoneID", SqlDbType.Int, 0, "TimeZoneID")
                .Parameters.Add("@state", SqlDbType.Int, 0, "state")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM states WHERE state=@state"
                .CommandType = CommandType.Text
                .Parameters.Add("@state", SqlDbType.Int, 0, "state")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function
    End Module
End Namespace