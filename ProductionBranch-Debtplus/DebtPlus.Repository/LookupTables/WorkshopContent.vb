﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module WorkshopContent

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DeleteManyByIds(ByVal ids As List(Of Int32)) As GetDataResult
            Return DeleteManyByIds(ids, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function DeleteManyByIds(ByVal ids As List(Of Int32), ByVal txn As SqlTransaction) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ids Is Nothing Then Throw New ArgumentNullException("ids may not be null")

            Dim rslt As New GetDataResult()

            If ids.Count > 0 Then
                Dim cn As SqlConnection

                If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
                    cn = txn.Connection
                Else
                    cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                End If

                Dim cmd As SqlCommand = New SqlCommand

                Try
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "DELETE FROM workshop_contents WHERE oID in (" & String.Join(",", ids) & ")"

                    cmd.ExecuteNonQuery()

                Catch ex As SqlException
                    rslt.HandleException(ex)

                Finally
                    If cmd IsNot Nothing Then cmd.Dispose()
                    If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return rslt
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CopyManyByIds(ByVal ids As List(Of Int32), ByVal type As Int32) As GetDataResult
            Return CopyManyByIds(ids, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CopyManyByIds(ByVal ids As List(Of Int32), ByVal type As Int32, ByVal txn As SqlTransaction) As GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If ids Is Nothing Then Throw New ArgumentNullException("ids may not be null")

            Dim rslt As New GetDataResult()

            If ids.Count > 0 Then
                Dim cn As SqlConnection

                If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
                    cn = txn.Connection
                Else
                    cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                End If

                Dim cmd As SqlCommand = New SqlCommand

                Try
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "INSERT INTO workshop_contents ([workshop_type],[content_type]) SELECT " & type & ", content_type FROM workshop_content_types WHERE content_type in (" & String.Join(",", ids) & ")"
                    cmd.CommandType = CommandType.Text

                    cmd.ExecuteNonQuery()

                Catch ex As SqlException
                    rslt.HandleException(ex)

                Finally
                    If cmd IsNot Nothing Then cmd.Dispose()
                    If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return rslt
        End Function
    End Module
End Namespace