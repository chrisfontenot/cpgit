﻿Imports System.Data.SqlClient

Namespace LookupTables
    Public Module WorkshopTypes
        Private SelectStatement_workshopTypes As String = "SELECT [workshop_type], [description], [duration], [HUD_Grant], [HousingFeeAmount], [ActiveFlag] FROM [workshop_types]"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_workshopTypes, ds, tableName)
            If gdr.Success Then
                TableProcessing.setAutoIncrementPK(ds, tableName, "workshop_type")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Insert(ByVal type As Models.WorkshopType) As Repository.GetDataResult
            Return Insert(type, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Insert(ByVal type As Models.WorkshopType, ByVal txn As SqlTransaction) As Repository.GetDataResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If type Is Nothing Then Throw New ArgumentNullException("type may not be null")

            Dim rslt As New Repository.GetDataResult()

            Dim cn As SqlConnection

            If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
                cn = txn.Connection
            Else
                cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
            End If

            Dim cmd As SqlCommand = New SqlCommand

            Try
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = "xpr_insert_workshop_type"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = type.Description
                cmd.Parameters.Add("@duration", SqlDbType.Int).Value = type.Duration
                cmd.Parameters.Add("@ActiveFlag", SqlDbType.Bit).Value = type.ActiveFlag
                cmd.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal).Value = type.HousingFeeAmount
                cmd.Parameters.Add("@HUD_Grant", SqlDbType.Int).Value = type.HudGrant

                cmd.ExecuteNonQuery()
                type.Id = cmd.Parameters(0).Value

            Catch ex As SqlClient.SqlException
                rslt.HandleException(ex)

            Finally
                If cmd IsNot Nothing Then cmd.Dispose()
                If txn Is Nothing AndAlso cn IsNot Nothing Then cn.Dispose()
            End Try

            Return rslt
        End Function
    End Module
End Namespace