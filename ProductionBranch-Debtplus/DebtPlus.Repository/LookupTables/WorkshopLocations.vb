Imports System.Data.SqlClient

Namespace LookupTables

    Public Module WorkshopLocations
        Const SelectStatement_workshop_locations As String = "SELECT [workshop_location],[name],[organization],[type],[AddressID],[TelephoneID],[directions],[milage],[ActiveFlag],[hcs_id],[date_created],[created_by] FROM workshop_locations"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_workshop_locations, ds, tableName)
            If gdr.Success Then
                TableProcessing.setAutoIncrementPK(ds, tableName, "workshop_location")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_workshop_location"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@organization", SqlDbType.VarChar, 80, "organization")
                .Parameters.Add("@type", SqlDbType.Int, 4, "type")
                .Parameters.Add("@AddressID", SqlDbType.Int, 4, "AddressID")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 4, "TelephoneID")
                .Parameters.Add("@directions", SqlDbType.VarChar, 80, "directions")
                .Parameters.Add("@milage", SqlDbType.Float, 8, "milage")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@hcs_id", SqlDbType.Int, 4, "hcs_id")
                .Parameters.Add("@workshop_location", SqlDbType.Int, 4, "workshop_location").Direction = ParameterDirection.ReturnValue
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE workshop_locations SET [name]=@name,[organization]=@organization,[type]=@type,[AddressID]=@AddressID,[TelephoneID]=@TelephoneID,[directions]=@directions,[milage]=@milage,[ActiveFlag]=@ActiveFlag,[hcs_id]=@hcs_id WHERE [workshop_location]=@workshop_location"
                .CommandType = CommandType.Text
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@organization", SqlDbType.VarChar, 80, "organization")
                .Parameters.Add("@type", SqlDbType.Int, 4, "type")
                .Parameters.Add("@AddressID", SqlDbType.Int, 4, "AddressID")
                .Parameters.Add("@TelephoneID", SqlDbType.Int, 4, "TelephoneID")
                .Parameters.Add("@directions", SqlDbType.VarChar, 80, "directions")
                .Parameters.Add("@milage", SqlDbType.Float, 8, "milage")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@hcs_id", SqlDbType.Int, 4, "hcs_id")
                .Parameters.Add("@workshop_location", SqlDbType.Int, 4, "workshop_location")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.AcceptChangesDuringUpdate = True

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
