﻿Imports System.Data.SqlClient

Namespace LookupTables

    Public Module FaqItems

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [faq_item],[grouping],[description],[attributes],[date_created],[created_by] FROM faq_items"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_faq_items"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@faq_item", SqlDbType.Int, 0, "faq_item").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@grouping", SqlDbType.VarChar, 50, "grouping")
                .Parameters.Add("@attributes", SqlDbType.VarChar, 50, "attributes")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE faq_items SET description=@description, [grouping]=@grouping, [attributes]=@attributes WHERE faq_item=@faq_item"
                .CommandType = CommandType.Text
                .Parameters.Add("@faq_item", SqlDbType.Int, 0, "faq_item")
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@grouping", SqlDbType.VarChar, 50, "grouping")
                .Parameters.Add("@attributes", SqlDbType.VarChar, 50, "attributes")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM faq_items WHERE faq_item=@faq_item"
                .Parameters.Add("@faq_item", SqlDbType.Int, 0, "faq_item")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace