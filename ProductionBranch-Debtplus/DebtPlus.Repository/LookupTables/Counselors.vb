Imports System.Data.SqlClient

Namespace LookupTables
    Public Module Counselors
        Private SelectStatement_counselors = "SELECT [Counselor],[NameID],[EmailID],[TelephoneID],[Person],[Note],[Default],[ActiveFlag],[Manager],[Office],[Menu_Level],[Color],[Image],[SSN],[billing_mode],[rate],[emp_start_date],[emp_end_date],[HUD_id],[date_created],[created_by] FROM [counselors]"

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim gdr = FillNamedDataTableFromSelectCommand(SelectStatement_counselors, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "Counselor")
            End If
            Return gdr
        End Function

        <Obsolete("Use GetAll()")>
        Public Function GetAll2(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [Counselor],[Person],[Office],[Default],[ActiveFlag],[Manager],[NameID],[Menu_level],[TelephoneID],[EmailID],[Color],[Image],[Note] FROM counselors WITH (NOLOCK)"
            Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "Counselor")
            End If
            Return gdr
        End Function

        <Obsolete("Use GetAll()")>
        Public Function GetAll3(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [Counselor],[Person],[Office],[Default],[ActiveFlag],[Manager],[NameID],[Menu_level],[TelephoneID],[EmailID],[Color],[Image],[SSN],[emp_start_date],[emp_end_date],[HUD_ID],[Note] FROM counselors WITH (NOLOCK)"
            Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
            If gdr.Success Then
                Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "Counselor")
            End If
            Return gdr
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll4(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "lst_counselors"
            Return FillNamedDataTableFromSproc(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)
            Try
                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using da As New SqlDataAdapter()
                        Using insertCommand As New SqlCommand()
                            insertCommand.Connection = cn
                            insertCommand.CommandText = "xpr_insert_counselors"
                            insertCommand.CommandType = CommandType.StoredProcedure
                            insertCommand.Parameters.Add("@Counselor", SqlDbType.Int, 4, "Counselor").Direction = ParameterDirection.ReturnValue
                            insertCommand.Parameters.Add("@NameID", SqlDbType.Int, 4, "NameID")
                            insertCommand.Parameters.Add("@EmailID", SqlDbType.Int, 4, "EmailID")
                            insertCommand.Parameters.Add("@TelephoneID", SqlDbType.Int, 4, "TelephoneID")
                            insertCommand.Parameters.Add("@Person", SqlDbType.VarChar, 80, "Person")
                            insertCommand.Parameters.Add("@Note", SqlDbType.VarChar, 80, "Note")
                            insertCommand.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                            insertCommand.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                            insertCommand.Parameters.Add("@Office", SqlDbType.Int, 4, "Office")
                            insertCommand.Parameters.Add("@Menu_Level", SqlDbType.Int, 4, "Menu_Level")
                            insertCommand.Parameters.Add("@Color", SqlDbType.Int, 4, "Color")
                            insertCommand.Parameters.Add("@Image", SqlDbType.VarChar, 80, "Image")
                            insertCommand.Parameters.Add("@SSN", SqlDbType.VarChar, 80, "SSN")
                            insertCommand.Parameters.Add("@billing_mode", SqlDbType.VarChar, 80, "billing_mode")
                            insertCommand.Parameters.Add("@rate", SqlDbType.Decimal, 0, "rate")
                            insertCommand.Parameters.Add("@emp_start_date", SqlDbType.DateTime, 8, "emp_start_date")
                            insertCommand.Parameters.Add("@emp_end_date", SqlDbType.DateTime, 8, "emp_end_date")
                            insertCommand.Parameters.Add("@HUD_id", SqlDbType.Int, 4, "HUD_id")

                            da.InsertCommand = insertCommand

                            Using updateCommand As New SqlCommand()
                                updateCommand.Connection = cn
                                updateCommand.CommandText = "UPDATE counselors SET [NameID]=@NameID,[EmailID]=@EmailID,[TelephoneID]=@TelephoneID,[Person]=@Person,[Note]=@Note,[Default]=@Default,[ActiveFlag]=@ActiveFlag,[Manager]=@Manager,[Office]=@Office,[Menu_Level]=@Menu_Level,[Color]=@Color,[Image]=@Image,[SSN]=@SSN,[billing_mode]=@billing_mode,[rate]=@rate,[emp_start_date]=@emp_start_date,[emp_end_date]=@emp_end_date,[HUD_id]=@HUD_id WHERE [Counselor]=@Counselor"
                                updateCommand.CommandType = CommandType.Text
                                updateCommand.Parameters.Add("@Person", SqlDbType.VarChar, 50, "Person")
                                updateCommand.Parameters.Add("@NameID", SqlDbType.Int, 0, "NameID")
                                updateCommand.Parameters.Add("@Office", SqlDbType.Int, 0, "Office")
                                updateCommand.Parameters.Add("@Menu_level", SqlDbType.Int, 0, "Menu_Level")
                                updateCommand.Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID")
                                updateCommand.Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                                updateCommand.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                                updateCommand.Parameters.Add("@Manager", SqlDbType.Int, 0, "Manager")
                                updateCommand.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default")
                                updateCommand.Parameters.Add("@Color", SqlDbType.Int, 0, "Color")
                                updateCommand.Parameters.Add("@Image", SqlDbType.Image, 0, "Image")
                                updateCommand.Parameters.Add("@Note", SqlDbType.VarChar, 256, "Note")
                                updateCommand.Parameters.Add("@SSN", SqlDbType.VarChar, 10, "SSN")
                                updateCommand.Parameters.Add("@billing_mode", SqlDbType.VarChar, 10, "billing_mode")
                                updateCommand.Parameters.Add("@rate", SqlDbType.Money, 0, "rate")
                                updateCommand.Parameters.Add("@emp_start_date", SqlDbType.DateTime, 0, "emp_start_date")
                                updateCommand.Parameters.Add("@emp_end_date", SqlDbType.DateTime, 0, "emp_end_date")
                                updateCommand.Parameters.Add("@HUD_id", SqlDbType.Int, 0, "HUD_id")
                                updateCommand.Parameters.Add("@Counselor", SqlDbType.Int, 0, "Counselor")

                                da.UpdateCommand = updateCommand

                                Using deleteCommand As New SqlCommand()
                                    deleteCommand.Connection = cn
                                    deleteCommand.CommandText = "xpr_delete_counselor"
                                    deleteCommand.CommandType = CommandType.StoredProcedure
                                    deleteCommand.Parameters.Add("@counselor", SqlDbType.Int, 0, "Counselor")

                                    da.DeleteCommand = deleteCommand

                                    ' Do the update for the table
                                    ccr.RowsAffected = da.Update(dt)
                                End Using
                            End Using
                        End Using
                    End Using
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                ccr.HandleException(ex)
            End Try

            Return ccr
        End Function
    End Module
End Namespace
