Imports System.Data.SqlClient

Namespace LookupTables

    Public Module HousingTerminationReasonTypes

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [oID],[Note],[ActiveFlag],[Default],[rpps],[description],[HUD_9902_section] FROM housing_TerminationReasonTypes"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
            If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
            If dt Is Nothing Then Throw RepositoryException.NoDataTable()

            Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim da As SqlDataAdapter = New SqlDataAdapter

            Dim insertCommand As SqlCommand = New SqlCommand()
            With insertCommand
                .Connection = cn
                .CommandText = "xpr_insert_housing_TerminationReasonType"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@nfcc", SqlDbType.VarChar, 4, "nfcc")
                .Parameters.Add("@rpps", SqlDbType.VarChar, 4, "rpps")
                .Parameters.Add("@note", SqlDbType.VarChar, 256, "note")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 256, "HUD_9902_section")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
            End With

            Dim updateCommand As SqlCommand = New SqlCommand()
            With updateCommand
                .Connection = cn
                .CommandText = "UPDATE housing_TerminationReasonTypes SET [Note]=@Note,[ActiveFlag]=@ActiveFlag,[Default]=@Default,[rpps]=@rpps,[description]=@description,[HUD_9902_section]=@HUD_9902_section WHERE oID=@oID"
                .CommandType = CommandType.Text
                .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
                .Parameters.Add("@rpps", SqlDbType.VarChar, 4, "rpps")
                .Parameters.Add("@note", SqlDbType.VarChar, 256, "note")
                .Parameters.Add("@HUD_9902_section", SqlDbType.VarChar, 256, "HUD_9902_section")
                .Parameters.Add("@default", SqlDbType.Bit, 0, "default")
                .Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag")
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Dim deleteCommand As SqlCommand = New SqlCommand()
            With deleteCommand
                .Connection = cn
                .CommandText = "DELETE FROM housing_TerminationReasonTypes WHERE oID=@oID"
                .Parameters.Add("@oID", SqlDbType.Int, 0, "oID")
            End With

            Try
                da.InsertCommand = insertCommand
                da.UpdateCommand = updateCommand
                da.DeleteCommand = deleteCommand

                cn.Open()

                ccr.RowsAffected = da.Update(dt)

            Catch ex As System.Data.SqlClient.SqlException
                Return ccr.HandleException(ex)

            Finally
                If updateCommand IsNot Nothing Then updateCommand.Dispose()
                If insertCommand IsNot Nothing Then insertCommand.Dispose()
                If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
                If da IsNot Nothing Then da.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return ccr
        End Function

    End Module

End Namespace
