﻿Public Module ProposalMessages

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAll(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT [proposal_message],[message_text],[short_text],[date_created],[created_by] FROM proposal_messages WITH (NOLOCK)"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

End Module
