Imports System.Data.SqlClient

Public Module Common

    '##############################################################################
    '####
    '####       TOTAL HACK - COPIED FROM DEBTPLUS.DATA - TODO - FIX IT
    '####
    '##############################################################################

    <System.Obsolete("Use LINQ Tables")>
    Public Function Compare(ByVal obj1 As Object, ByVal obj2 As Object) As Int32
        Return Compare(obj1, obj2, True)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function Compare(ByVal obj1 As Object, ByVal obj2 As Object, ByVal IgnoreCase As Boolean) As Int32

        ' If both are missing then they are equal
        If ((obj1 Is Nothing) OrElse (obj1 Is DBNull.Value)) AndAlso ((obj2 Is Nothing) OrElse (obj2 Is DBNull.Value)) _
            Then Return 0

        ' If the first object is missing, it is less
        If ((obj1 Is Nothing) OrElse (obj1 Is DBNull.Value)) Then Return -1

        ' If the second object is missing, it is greater
        If ((obj2 Is Nothing) OrElse (obj2 Is DBNull.Value)) Then Return 1

        ' Do comparisons on booleans
        If TypeOf obj1 Is Boolean Then
            Dim bool1 As Boolean = Convert.ToBoolean(obj1)
            Dim bool2 As Boolean = Convert.ToBoolean(obj2)
            If bool1 = bool2 Then Return 0
            If bool2 Then Return 1
            Return -1
        End If

        ' Do comparisons on strings without a test for case
        If TypeOf obj1 Is String Then Return String.Compare(Convert.ToString(obj1), Convert.ToString(obj2), IgnoreCase)

        ' Comparisons on numerical quantites are easier
        If TypeOf obj1 Is Date OrElse TypeOf obj1 Is DateTime Then
            Return DateTime.Compare(Convert.ToDateTime(obj1), Convert.ToDateTime(obj2))
        End If
        If TypeOf obj1 Is Decimal Then Return Decimal.Compare(Convert.ToDecimal(obj1), Convert.ToDecimal(obj2))
        If TypeOf obj1 Is Double OrElse TypeOf obj1 Is Single Then
            Return Convert.ToDouble(obj1).CompareTo(Convert.ToDouble(obj2))
        End If
        If TypeOf obj1 Is Byte Then Return Convert.ToByte(obj1).CompareTo(Convert.ToByte(obj2))

        ' Do the same for fixed point values
        If TypeOf obj1 Is Int16 OrElse TypeOf obj1 Is Int32 OrElse TypeOf obj1 Is Int64 Then
            Return Convert.ToInt64(obj1).CompareTo(Convert.ToInt64(obj2))
        End If

        ' The items must not be simple types.
        ' If they implement ICompariable then use it to return the results
        Dim ic As IComparable = TryCast(obj1, IComparable)
        If ic IsNot Nothing Then Return ic.CompareTo(obj2)

        ' This is just a non-event
        'Errors.DebuggingException()
        Return -1
    End Function

    '##############################################################################
    '####
    '####       END TOTAL HACK - COPIED FROM DEBTPLUS.DATA - TODO - FIX IT
    '####
    '##############################################################################

    '##############################################################################
    '####
    '####  Please do not change these method patterns.  There is a specific pattern being used that has been proven to be the most maintainable by MS research.
    '####
    '####   1) Check the environment
    '####   2) Check the parameters
    '####   3) Make all variables
    '####   4) Enter a try block
    '####   5) Instatiate variables
    '####   6) Conduct one db call
    '####   7) Catch exceptions and return failure condition (this is the only return from within the try-catch-finally
    '####   8) Finally dispose of all resources assigned to variables
    '####   9) Return success condition
    '####
    '####   This is the best pattern for db routines because:
    '####   1) Minimal nesting - a primary cause of maintenance issues
    '####   2) Guaranteed cleanup
    '####   3) Expresses closure
    '####   4) Minimizes connection use
    '####
    '##############################################################################

    ''' <summary>
    ''' Execute a text-based select command and fill a dataset's datatable with the results.  Name the DataSet's resulting DataTable as 'tableName'
    ''' </summary>
    ''' <param name="selectCommand">Parameterless select command</param>
    ''' <param name="ds">The dataset</param>
    ''' <param name="tableName">the name of the table (a DataTable) to be created</param>
    ''' <returns>True on success</returns>
    <System.Obsolete("Use LINQ Tables")>
    Public Function FillNamedDataTableFromSelectCommand(ByVal selectCommand As String, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(selectCommand) OrElse (ds Is Nothing) OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd = New SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = selectCommand
            cmd.CommandType = CommandType.Text

            da = New SqlDataAdapter()
            da.SelectCommand = cmd

            cn.Open()

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    ''' <summary>
    ''' Select a row by id and add a named datatable to the dataset
    ''' </summary>
    <System.Obsolete("Use LINQ Tables")>
    Friend Function FillNamedDataTableFromSelectCommandById(ByVal selectCommand As String, ByVal idParamName As String, ByVal id As Int32, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(selectCommand) OrElse String.IsNullOrWhiteSpace(idParamName) OrElse id <= 0 OrElse (ds Is Nothing) OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd = New SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = selectCommand
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add(idParamName, SqlDbType.Int).Value = id

            da = New SqlDataAdapter()
            da.SelectCommand = cmd

            cn.Open()

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function FillNamedDataTableFromSproc(ByVal sproc As String, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(sproc) OrElse (ds Is Nothing) OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd = New SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = sproc
            cmd.CommandType = CommandType.StoredProcedure

            da = New SqlDataAdapter()
            da.SelectCommand = cmd

            cn.Open()

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    ''' <summary>
    ''' Execute a prepared SqlCommand and fill a dataset's datatable with the results.  Name the DataSet's resulting DataTable as 'tableName'
    ''' </summary>
    ''' <param name="cmd">A fully prepared SqlCommand (including any params).  Also, since you made the cmd, then you need to dispose of it.</param>
    ''' <param name="ds">The dataset</param>
    ''' <param name="tableName">the name of the table (a DataTable) to be created</param>
    ''' <returns>True on success</returns>
    <System.Obsolete("Use LINQ Tables")>
    Friend Function FillNamedDataTableFromSqlCommand(ByVal cmd As SqlCommand, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If cmd Is Nothing OrElse ds Is Nothing OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd.Connection = cn

            da = New SqlDataAdapter()
            da.SelectCommand = cmd

            cn.Open()

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function DeleteRowUsingDeleteQueryAndId(ByVal deleteCommand As String, ByVal idParamName As String, ByVal id As Int32) As GetDataResult
        Return DeleteRowUsingDeleteQueryAndId(deleteCommand, idParamName, id, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function DeleteRowUsingDeleteQueryAndId(ByVal deleteCommand As String, ByVal idParamName As String, ByVal id As Int32, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        If String.IsNullOrWhiteSpace(deleteCommand) Then Throw New ArgumentException("deleteCommand")
        If String.IsNullOrWhiteSpace(idParamName) Then Throw New ArgumentException("idParamName")
        If id <= 0 Then Throw New ArgumentException("id")

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = deleteCommand
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add(idParamName, SqlDbType.Int).Value = id
            Return ExecuteNonQuery(cmd, txn)
        End Using
    End Function

    '''<summary>
    ''' Execute a non-query command based on the provided SqlCommand.
    '''</summary>
    <System.Obsolete("Use LINQ Tables")>
    Public Function ExecuteNonQuery(ByVal query As String, ByVal txn As SqlTransaction) As GetDataResult

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = query
            cmd.CommandType = CommandType.Text
            Return ExecuteNonQuery(cmd, txn)
        End Using

    End Function

    '''<summary>
    ''' Execute a non-query command based on the provided SqlCommand.
    '''</summary>
    <System.Obsolete("Use LINQ Tables")>
    Public Function ExecuteNonQuery(ByVal cmd As SqlCommand) As GetDataResult
        Return ExecuteNonQuery(cmd, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function ExecuteNonQuery(ByVal cmd As SqlCommand, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        ' If there is a transaction then extract the connection object and run it
        If txn IsNot Nothing AndAlso txn.Connection IsNot Nothing Then
            Return ExecuteNonQuery(cmd, txn.Connection, txn)
        End If

        ' Otherwise, create/dispose a connection and execute the command without a transaction
        Dim cn As SqlConnection = Nothing
        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Return ExecuteNonQuery(cmd, cn, txn)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

    End Function

    '''<summary>
    ''' Execute a non-query command based on the provided SqlCommand.
    '''</summary
    <System.Obsolete("Use LINQ Tables")>
    Friend Function ExecuteNonQuery(ByVal cmd As SqlCommand, ByVal cn As SqlConnection, ByVal txn As SqlTransaction) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If cn Is Nothing Then Throw New ArgumentNullException("cn may not be null")
        If cmd Is Nothing Then Throw New ArgumentNullException("cmd may not be null")

        Dim gdr As New GetDataResult()

        ' Open the connection if needed
        Dim originalState As System.Data.ConnectionState = cn.State

        Try
            If originalState = ConnectionState.Closed Then
                cn.Open()
            End If

            ' Execute the command at this point.
            cmd.Connection = cn
            cmd.Transaction = txn
            gdr.RowsAffected = cmd.ExecuteNonQuery()

        Catch ex As SqlException
            gdr.HandleException(ex)

        Finally
            ' If the connection was closed when we were called, close it again.
            If originalState = ConnectionState.Closed AndAlso cn.State <> ConnectionState.Closed Then
                cn.Close()
            End If
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function ExecuteScalar(ByVal query As String) As GetScalarResult
        Return ExecuteScalar(query, Nothing)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function ExecuteScalar(ByVal query As String, ByVal txn As SqlTransaction) As GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(query) Then Throw New ArgumentException("query may not be empty")

        Dim rslt As New GetScalarResult()

        Dim cn As SqlConnection = Nothing
        Dim newCn As Boolean = txn Is Nothing OrElse txn.Connection Is Nothing

        Try
            If newCn Then
                cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                cn.Open()
            Else
                cn = txn.Connection
            End If

            Using cmd = New SqlCommand()
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = query
                cmd.CommandType = CommandType.Text

                rslt.Result = cmd.ExecuteScalar()
                If rslt.Result Is System.DBNull.Value Then
                    rslt.Result = Nothing
                End If
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            rslt.HandleException(ex)

        Finally
            If newCn AndAlso cn IsNot Nothing Then cn.Dispose()
        End Try

        Return rslt
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function ExecuteScalar(ByVal cmd As SqlCommand) As Repository.GetScalarResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If cmd Is Nothing Then Throw New ArgumentNullException("cmd may not be empty")

        Dim rslt As New GetScalarResult()
        Dim cn As SqlConnection = Nothing
        Dim obj As Object = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd.Connection = cn

            cn.Open()
            rslt.Result = cmd.ExecuteScalar()
            If rslt.Result Is System.DBNull.Value Then
                rslt.Result = Nothing
            End If

        Catch ex As System.Data.SqlClient.SqlException
            rslt.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return rslt
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function FillNamedDataTableSchemaFromSelectCommand(ByVal selectCommand As String, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(selectCommand) OrElse (ds Is Nothing) OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd = New SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = selectCommand
            cmd.CommandType = CommandType.Text

            da = New SqlDataAdapter()
            da.SelectCommand = cmd

            cn.Open()

            da.FillSchema(ds, SchemaType.Source, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Friend Function FillNamedDataTableSchemaFromSproc(ByVal sproc As String, ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(sproc) OrElse (ds Is Nothing) OrElse String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim da As SqlDataAdapter = Nothing

        Try
            cn = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            cmd = New SqlCommand()
            cmd.Connection = cn
            cmd.CommandText = sproc
            cmd.CommandType = CommandType.StoredProcedure

            da = New SqlDataAdapter(cmd)

            cn.Open()

            da.FillSchema(ds, SchemaType.Source, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

End Module

