Imports System.Data.SqlClient

Public Module ClientAppointments

    Private Const SelectStatement_ClientAppointments As String = "SELECT [client_appointment],[client],[appt_time],[counselor],[start_time],[end_time],[office],[workshop],[workshop_people],[appt_type],[status],[result],[previous_appointment],[referred_to],[referred_by],[bankruptcy_class],[priority],[housing],[post_purchase],[credit],[callback_ph],[HousingFeeAmount],[confirmation_status],[date_confirmed],[date_updated],[date_created],[created_by] FROM [client_appointments]"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal appointmentId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [client_appointment]={1:f0}", SelectStatement_ClientAppointments, appointmentId)
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client_appointment")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal ClientId As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [client]={1:f0}", SelectStatement_ClientAppointments, ClientId)
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client_appointment")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetClientApptsByWorkshopAndStatus(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32, ByVal workshopId As Int32, ByVal status As String) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [client]={1:f0} AND [workshop]={2:f0} AND [status]='{3}'", SelectStatement_ClientAppointments, clientId, workshopId, status.Replace("'", "''"))
        Dim gdr = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            Repository.LookupTables.TableProcessing.setAutoIncrementPK(ds, tableName, "client_appointment")
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAvailableAppointments(ByVal ds As DataSet, ByVal tableName As String, ByVal ClientId As Int32, ByVal apptType As Int32?, ByVal days As String, ByVal time As String, ByVal zipCode As String, ByVal counselorId As Int32?, ByVal fromDate As DateTime, ByVal toDate As DateTime) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Try
            cmd.Connection = cn
            cmd.CommandText = "xpr_appt_select"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId
            cmd.Parameters.Add("@appt_type", SqlDbType.Int).Value = If(apptType.HasValue, apptType.Value, DBNull.Value)
            cmd.Parameters.Add("@date_part", SqlDbType.VarChar, 80).Value = If(days Is Nothing, DBNull.Value, days)
            cmd.Parameters.Add("@am_pm", SqlDbType.VarChar, 80).Value = If(time Is Nothing, DBNull.Value, time)
            cmd.Parameters.Add("@zipcode", SqlDbType.VarChar, 80).Value = If(zipCode Is Nothing, DBNull.Value, zipCode)
            cmd.Parameters.Add("@counselor", SqlDbType.Int).Value = If(counselorId.HasValue, counselorId.Value, DBNull.Value)
            cmd.Parameters.Add("@fromdate", SqlDbType.DateTime).Value = fromDate
            cmd.Parameters.Add("@todate", SqlDbType.DateTime).Value = toDate

            cn.Open()

            da.SelectCommand = cmd

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
            If da IsNot Nothing Then da.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal drs As DataRow(), ByVal clientId As Int32, ByVal workshop As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If drs Is Nothing Then Throw RepositoryException.NoDataRowCollection()

        Dim ccr As CommitChangesResult = New CommitChangesResult(drs)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim insertCommand As SqlCommand = New SqlCommand()
        Dim updateCommand As SqlCommand = New SqlCommand()
        Dim deleteCommand As SqlCommand = New SqlCommand()

        Try
            cn.Open()
            Using da As SqlDataAdapter = New SqlDataAdapter()
                insertCommand.Connection = cn
                insertCommand.CommandText = "xpr_appt_workshop_book"
                insertCommand.CommandType = CommandType.StoredProcedure
                insertCommand.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "client_appointment").Direction = ParameterDirection.ReturnValue
                insertCommand.Parameters.Add("@client", SqlDbType.Int).Value = clientId
                insertCommand.Parameters.Add("@workshop", SqlDbType.Int).Value = workshop
                insertCommand.Parameters.Add("@attending", SqlDbType.Int, 0, "workshop_people")
                insertCommand.Parameters.Add("@referred_by", SqlDbType.Int, 0, "referred_by")
                insertCommand.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
                da.InsertCommand = insertCommand

                updateCommand.Connection = cn
                updateCommand.CommandText = "UPDATE client_appointments SET workshop_people=@workshop_people,referred_by=@referred_by,status=@status,HousingFeeAmount=@HousingFeeAmount WHERE client_appointment=@client_appointment"
                updateCommand.CommandType = CommandType.Text
                updateCommand.Parameters.Add("@workshop_people", SqlDbType.Int, 0, "workshop_people")
                updateCommand.Parameters.Add("@referred_by", SqlDbType.Int, 0, "referred_by")
                updateCommand.Parameters.Add("@status", SqlDbType.VarChar, 10, "status")
                updateCommand.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
                updateCommand.Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
                da.UpdateCommand = updateCommand

                deleteCommand.Connection = cn
                deleteCommand.CommandText = "UPDATE client_appointments SET workshop_people=0,status='C',date_updated=getdate() WHERE client_appointment=@client_appointment"
                deleteCommand.CommandType = CommandType.Text
                deleteCommand.Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
                da.DeleteCommand = deleteCommand

                ccr.RowsAffected = da.Update(drs)
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            ccr.HandleException(ex)

        Finally
            updateCommand.Dispose()
            insertCommand.Dispose()
            deleteCommand.Dispose()
            cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges2(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_appointments SET [appt_time]=@appt_time,[counselor]=@counselor,[start_time]=@start_time,[end_time]=@end_time,[office]=@office,[workshop]=@workshop,[workshop_people]=@workshop_people,[appt_type]=@appt_type,[status]=@status,[result]=@result,[previous_appointment]=@previous_appointment,[referred_to]=@referred_to,[referred_by]=@referred_by,[bankruptcy_class]=@bankruptcy_class,[satisfaction_score]=@satisfaction_score,[priority]=@priority,[housing]=@housing,[post_purchase]=@post_purchase,[credit]=@credit,[callback_ph]=@callback_ph,[confirmation_status]=@confirmation_status,[HousingFeeAmount]=@HousingFeeAmount WHERE [client_appointment]=@client_appointment"
            .CommandType = CommandType.Text
            .Parameters.Add("@appt_time", SqlDbType.Int, 0, "appt_time")
            .Parameters.Add("@start_time", SqlDbType.DateTime, 0, "start_time")
            .Parameters.Add("@end_time", SqlDbType.DateTime, 0, "end_time")
            .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
            .Parameters.Add("@office", SqlDbType.Int, 0, "office")
            .Parameters.Add("@workshop", SqlDbType.Int, 0, "workshop")
            .Parameters.Add("@workshop_people", SqlDbType.Int, 0, "workshop_people")
            .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
            .Parameters.Add("@status", SqlDbType.VarChar, 20, "status")
            .Parameters.Add("@result", SqlDbType.VarChar, 20, "result")
            .Parameters.Add("@previous_appointment", SqlDbType.Int, 0, "previous_appointment")
            .Parameters.Add("@referred_to", SqlDbType.Int, 0, "referred_to")
            .Parameters.Add("@referred_by", SqlDbType.Int, 0, "referred_by")
            .Parameters.Add("@bankruptcy_class", SqlDbType.Int, 0, "bankruptcy_class")
            .Parameters.Add("@priority", SqlDbType.Bit, 0, "priority")
            .Parameters.Add("@housing", SqlDbType.Bit, 0, "housing")
            .Parameters.Add("@post_purchase", SqlDbType.Bit, 0, "post_purchase")
            .Parameters.Add("@credit", SqlDbType.Bit, 0, "credit")
            .Parameters.Add("@callback_ph", SqlDbType.VarChar, 20, "callback_ph")
            .Parameters.Add("@confirmation_status", SqlDbType.Int, 0, "confirmation_status")
            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
            .Parameters.Add("@HousingFeeAmount", SqlDbType.Money, 0, "HousingFeeAmount")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "xpr_appt_cancel"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@Appt", SqlDbType.Int, 0, "client_appointment")
        End With

        Try
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges3(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_appointments SET [status]=@status,[counselor]=@counselor,[office]=@office,[confirmation_status]=@confirmation_status,[referred_by]=@referred_by,[date_confirmed]=getdate() WHERE [client_appointment]=@client_appointment"
            .CommandType = CommandType.Text
            .Parameters.Add("@status", SqlDbType.VarChar, 1, "status")
            .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
            .Parameters.Add("@office", SqlDbType.Int, 0, "office")
            .Parameters.Add("@confirmation_status", SqlDbType.Int, 0, "confirmation_status")
            .Parameters.Add("@referred_by", SqlDbType.Int, 0, "referred_by")
            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
        End With

        Try
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function UpdateAppointmentInformation(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "xpr_appt_result"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment")
            .Parameters.Add("@client_status", SqlDbType.VarChar, 80, "result")
            .Parameters.Add("@appt_type", SqlDbType.Int, 0, "appt_type")
            .Parameters.Add("@referred_to", SqlDbType.Int, 0, "referred_to")
            .Parameters.Add("@end_time", SqlDbType.DateTime, 0, "end_time")
            .Parameters.Add("@office", SqlDbType.Int, 0, "office")
            .Parameters.Add("@credit", SqlDbType.Bit, 0, "credit")
            .Parameters.Add("@counselor", SqlDbType.Int, 0, "counselor")
            .Parameters.Add("@referred_by", SqlDbType.Int, 0, "referred_by")
        End With

        Try
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
