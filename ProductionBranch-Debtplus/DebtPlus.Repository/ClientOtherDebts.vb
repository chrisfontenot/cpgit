Imports System.Data.SqlClient

Public Module ClientOtherDebts

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT client_other_debt,creditor_name,account_number,balance,payment,interest_rate FROM client_other_debts WITH (NOLOCK) WHERE client={0:f0}", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal clientId As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_client_other_debt"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("RETURN", SqlDbType.Int, 0, "client_other_debt").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
            .Parameters.Add("@creditor_name", SqlDbType.VarChar, 50, "creditor_name")
            .Parameters.Add("@account_number", SqlDbType.VarChar, 50, "account_number")
            .Parameters.Add("@balance", SqlDbType.Decimal, 0, "balance")
            .Parameters.Add("@payment", SqlDbType.Decimal, 0, "payment")
            .Parameters.Add("@interest_rate", SqlDbType.Float, 0, "interest_rate")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE client_other_debts SET creditor_name=@creditor_name,account_number=@account_number,balance=@balance,payment=@payment,interest_rate=@interest_rate WHERE client_other_debt=@client_other_debt AND client=@client"
            .CommandType = CommandType.Text
            .Parameters.Add("@creditor_name", SqlDbType.VarChar, 50, "creditor_name")
            .Parameters.Add("@account_number", SqlDbType.VarChar, 50, "account_number")
            .Parameters.Add("@balance", SqlDbType.Decimal, 0, "balance")
            .Parameters.Add("@payment", SqlDbType.Decimal, 0, "payment")
            .Parameters.Add("@interest_rate", SqlDbType.Float, 0, "interest_rate")
            .Parameters.Add("@client_other_debt", SqlDbType.Int, 0, "client_other_debt")
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM client_other_debts WHERE client_other_debt=@client_other_debt AND client=@client"
            .CommandType = CommandType.Text
            .Parameters.Add("@client_other_debt", SqlDbType.Int, 0, "client_other_debt")
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
