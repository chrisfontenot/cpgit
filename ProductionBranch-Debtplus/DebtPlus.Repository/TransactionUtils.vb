﻿Imports System.Transactions

Public Class TransactionUtils

    Public Shared Function CreateTransactionScope() As TransactionScope
        Dim transactionOptions = New TransactionOptions()
        transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted
        transactionOptions.Timeout = TransactionManager.MaximumTimeout
        Return New TransactionScope(TransactionScopeOption.Required, transactionOptions)
    End Function

End Class
