Imports System.Data.SqlClient

Public Module SecuredProperty

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByClientId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT p.secured_property, p.client, p.class, p.secured_type, p.school_name, p.general_outcome_id, p.type_of_workout_id, p.borrower_id, p.description, p.sub_description, p.original_price, p.current_value, p.year_acquired, p.year_mfg, convert(money,isnull(sum(l.balance),0)) as balance FROM secured_properties p LEFT OUTER JOIN secured_loans l ON p.secured_property = l.secured_property WHERE client = {0:f0} GROUP BY p.secured_property, p.client, p.class, p.secured_type,  p.school_name, p.general_outcome_id, p.type_of_workout_id, p.borrower_id, p.description, p.sub_description, p.original_price, p.current_value, p.year_acquired, p.year_mfg", clientId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllByClientIdAndSecuredPropertyId(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32, ByVal securedPropertyId As Int32) As GetDataResult
        Dim query As String = String.Format("SELECT p.secured_property, p.client, p.class, p.secured_type, p.school_name, p.general_outcome_id, p.type_of_workout_id, p.borrower_id, p.description, p.sub_description, p.original_price, p.current_value, p.year_acquired, p.year_mfg, convert(money,isnull(sum(l.balance),0)) as balance FROM secured_properties p LEFT OUTER JOIN secured_loans l ON p.secured_property = l.secured_property WHERE client={0:f0} AND p.secured_property={1:f0} GROUP BY p.secured_property, p.client, p.class, p.secured_type,  p.school_name, p.general_outcome_id, p.type_of_workout_id, p.borrower_id, p.description, p.sub_description, p.original_price, p.current_value, p.year_acquired, p.year_mfg", clientId, securedPropertyId)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitChanges(ByVal dt As DataTable, ByVal clientId As Int32) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "UPDATE secured_properties SET class=@class,secured_type=@secured_type, school_name=@school_name, general_outcome_id=@general_outcome_id, type_of_workout_id=@type_of_workout_id, borrower_id=@borrower_id, description=@description, sub_description=@sub_description, original_price=@original_price,current_value=@current_value,year_acquired=@year_acquired,year_mfg=@year_mfg WHERE secured_property=@secured_property"
            .CommandType = CommandType.Text
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
            .Parameters.Add("@class", SqlDbType.Int, 0, "class")
            .Parameters.Add("@secured_type", SqlDbType.Int, 0, "secured_type")
            .Parameters.Add("@school_name", SqlDbType.VarChar, 50, "school_name")
            .Parameters.Add("@general_outcome_id", SqlDbType.Int, 0, "general_outcome_id")
            .Parameters.Add("@type_of_workout_id", SqlDbType.Int, 0, "type_of_workout_id")
            .Parameters.Add("@borrower_id", SqlDbType.Int, 0, "borrower_id")
            .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
            .Parameters.Add("@sub_description", SqlDbType.VarChar, 50, "sub_description")
            .Parameters.Add("@original_price", SqlDbType.Decimal, 0, "original_price")
            .Parameters.Add("@current_value", SqlDbType.Decimal, 0, "current_value")
            .Parameters.Add("@year_acquired", SqlDbType.Int, 0, "year_acquired")
            .Parameters.Add("@year_mfg", SqlDbType.Int, 0, "year_mfg")
            .Parameters.Add("@secured_property", SqlDbType.Int, 0, "secured_property")
        End With

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE secured_properties SET class=@class,secured_type=@secured_type, school_name=@school_name, general_outcome_id=@general_outcome_id, type_of_workout_id=@type_of_workout_id, borrower_id=@borrower_id, description=@description,sub_description=@sub_description,original_price=@original_price,current_value=@current_value,year_acquired=@year_acquired,year_mfg=@year_mfg WHERE secured_property=@secured_property"
            .CommandType = CommandType.Text
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
            .Parameters.Add("@class", SqlDbType.Int, 0, "class")
            .Parameters.Add("@secured_type", SqlDbType.Int, 0, "secured_type")
            .Parameters.Add("@school_name", SqlDbType.VarChar, 50, "school_name")
            .Parameters.Add("@general_outcome_id", SqlDbType.Int, 0, "general_outcome_id")
            .Parameters.Add("@type_of_workout_id", SqlDbType.Int, 0, "type_of_workout_id")
            .Parameters.Add("@borrower_id", SqlDbType.Int, 0, "borrower_id")
            .Parameters.Add("@description", SqlDbType.VarChar, 50, "description")
            .Parameters.Add("@sub_description", SqlDbType.VarChar, 50, "sub_description")
            .Parameters.Add("@original_price", SqlDbType.Decimal, 0, "original_price")
            .Parameters.Add("@current_value", SqlDbType.Decimal, 0, "current_value")
            .Parameters.Add("@year_acquired", SqlDbType.Int, 0, "year_acquired")
            .Parameters.Add("@year_mfg", SqlDbType.Int, 0, "year_mfg")
            .Parameters.Add("@secured_property", SqlDbType.Int, 0, "secured_property")
        End With

        Dim deleteCommand As SqlCommand = New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE secured_loans FROM secured_loans l inner join secured_properties p on l.secured_property = p.secured_property WHERE p.secured_property=@secured_property AND p.client = @client; DELETE FROM secured_properties WHERE secured_property=@secured_property AND client=@client"
            .CommandType = CommandType.Text
            .Parameters.Add("@secured_property", SqlDbType.Int, 0, "secured_property")
            .Parameters.Add("@client", SqlDbType.Int).Value = clientId
        End With

        Try
            da.InsertCommand = insertCommand
            da.UpdateCommand = updateCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

End Module
