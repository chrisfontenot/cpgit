﻿Imports DebtPlus.Models.Transactions
Imports System.Data.SqlClient

Namespace Transactions

    Public Module UpdateHousingARMReferenceInfoItems

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Commit(ByVal txnModel As UpdateHousingARMReferenceInfo) As GetDataResult
            Dim gdr As New GetDataResult()
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing

            Try
                cn.Open()
                txn = cn.BeginTransaction()

                ' clean out current groupid items
                gdr = LookupTables.HousingARMReferenceInfo.DeleteAllByGroupId(txnModel.GroupId, txn)
                If Not gdr.Success Then
                    Return gdr
                End If

                ' add the new ones
                For Each item As Models.Transactions.UpdateHousingARMReferenceInfo.HousingARMReferenceItem In txnModel.Items
                    gdr = LookupTables.HousingARMReferenceInfo.InsertGroupItem(txnModel.GroupId, item, txn)
                    If Not gdr.Success Then
                        Return gdr
                    End If
                Next

                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Data.SqlClient.SqlException
                    End Try
                    txn.Dispose()
                End If

                cn.Dispose()
            End Try

            Return gdr
        End Function

    End Module

End Namespace