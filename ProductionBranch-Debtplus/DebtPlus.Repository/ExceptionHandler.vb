Imports System.Data.SqlClient

Public Module ExceptionHandler

    <System.Obsolete("Use LINQ Tables")> _
    Friend Function xxxHandleException(ByVal ex As Exception) As Boolean

        ' how did this happen?
        If ex Is Nothing Then Return False

        ' When we do an update, the update may result in zero rows being updated. IGNORE IT.
        If TypeOf ex Is DBConcurrencyException Then Return True

        Dim sqlEx As SqlException = TryCast(ex, SqlException)
        If (sqlEx IsNot Nothing) Then
            Select Case sqlEx.Number

                Case 11 ' Indicates that the given object or entity does not exist.
                    Return False

                Case 12 'A special severity for queries that do not use locking because of special query hints. In some cases, read operations performed by these statements could result in inconsistent data, since locks are not taken to guarantee consistency.
                    Return False

                Case 13 ' Indicates transaction deadlock errors.
                    Return False

                Case 14 ' Indicates security-related errors, such as permission denied.
                    Return False

                Case 15 ' Indicates syntax errors in the Transact-SQL command.
                    Return False

                Case 16 ' Indicates general errors that can be corrected by the user.
                    Return False

            End Select

            Return False
        End If

        ' Most likely a table definition error (about as rare as an albino jackalope)
        If TypeOf ex Is SystemException Then Return False

        ' catch-all
        If TypeOf ex Is Exception Then
            ' TODO - log the exception - either with log4net or nlog - to the Event Log?  or just a flat file?
        End If

        Return False
    End Function

End Module
