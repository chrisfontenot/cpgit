﻿Imports System.Data.SqlClient
Imports System.Text

Public Module Creditors
    Private Const base_query As String = "SELECT [creditor],[creditor_id],[type],[sic],[creditor_name],[comment],[division],[creditor_class],[voucher_spacing],[mail_priority],[payment_balance],[prohibit_use],[full_disclosure],[suppress_invoice],[proposal_budget_info],[proposal_income_info],[contrib_cycle],[contrib_bill_month],[pledge_amt],[pledge_cycle],[pledge_bill_month],[min_accept_amt],[min_accept_pct],[min_accept_per_bill],[lowest_apr_pct],[medium_apr_pct],[highest_apr_pct],[medium_apr_amt],[highest_apr_amt],[max_clients_per_check],[max_amt_per_check],[max_fairshare_per_debt],[chks_per_invoice],[returned_mail],[po_number],[usual_priority],[percent_balance],[distrib_mtd],[distrib_ytd],[contrib_mtd_billed],[contrib_ytd_billed],[contrib_mtd_received],[contrib_ytd_received],[first_payment],[last_payment],[acceptance_days],[check_payments],[check_bank],[creditor_contribution_pct],[date_created],[created_by] FROM creditors WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Dim gdr As GetDataResult = FillNamedDataTableSchemaFromSelectCommand(base_query, ds, tableName)
        If gdr.Success Then
            setPK(ds, tableName)
        End If
        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetByName(ByVal ds As DataSet, ByVal tableName As String, ByVal creditorName As String) As GetDataResult
        Dim query As String = String.Format("{0} WHERE creditor='{1}'", base_query, creditorName.Replace("'", "''"))
        Dim gdr As GetDataResult = FillNamedDataTableFromSelectCommand(query, ds, tableName)
        If gdr.Success Then
            setPK(ds, tableName)
        End If
        Return gdr
    End Function

    Private Sub setPK(ByVal ds As DataSet, ByVal tableName As String)
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then
            Dim col As DataColumn = tbl.Columns("creditor")
            Debug.Assert(col IsNot Nothing)

            ' the primary key is the string "creditor".
            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                tbl.PrimaryKey = New DataColumn() {col}
            End If

            ' Look for the ID column. This is auto-increment.
            If tbl.Columns.Contains("creditor_id") Then
                col = tbl.Columns("creditor_id")
                Debug.Assert(col IsNot Nothing)

                If Not col.AutoIncrement Then
                    col.AutoIncrement = True
                    col.AutoIncrementSeed = -1
                    col.AutoIncrementStep = -1
                End If
            End If
        End If
    End Sub

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Exists(ByVal creditorName As String) As GetScalarResult
        Dim query As String = String.Format("SELECT [creditor] FROM [creditors] WITH (NOLOCK) WHERE creditor='{0}'", creditorName)
        Dim gsr As GetScalarResult = ExecuteScalar(query)
        gsr.Result = gsr.Result IsNot System.DBNull.Value AndAlso Not String.IsNullOrEmpty(gsr.Result)
        Return gsr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Exists(ByVal creditorId As Int32) As GetScalarResult
        Dim gsr As GetScalarResult = GetCreditorById(creditorId)
        gsr.Result = gsr.Result IsNot System.DBNull.Value AndAlso Not String.IsNullOrEmpty(gsr.Result)
        Return gsr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCreditorById(ByVal creditorId As Int32) As GetScalarResult
        Dim query As String = String.Format("SELECT [creditor] FROM [creditors] WITH (NOLOCK) WHERE creditor_id={0:f0}", creditorId)
        Return ExecuteScalar(query)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetNameById(ByVal creditor As String) As GetScalarResult
        Dim query As String = String.Format("SELECT [creditor_name] FROM [creditors] WHERE creditor='{0}'", creditor.Replace("'", "''"))
        Return ExecuteScalar(query)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal creditorType As String, ByRef creditorId As Int32, ByRef creditorString As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim rd As SqlDataReader = Nothing

        ' Default the return value should there be an error or something else happen
        creditorString = String.Empty
        Dim gdr As New GetDataResult()

        Try
            cn.Open()
            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "xpr_create_creditor"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                cmd.Parameters.Add("@Type", SqlDbType.VarChar, 10).Value = Convert.ToString(creditorType)

                rd = cmd.ExecuteReader(CommandBehavior.SingleRow)
                creditorId = Convert.ToInt32(cmd.Parameters("RETURN").Value)
            End Using

            If rd IsNot Nothing AndAlso rd.Read() Then
                gdr.RowsAffected = 1
                creditorString = Convert.ToString(rd.GetValue(0))
            End If

        Catch ex As SqlException
            gdr.HandleException(ex)

        Finally
            If rd IsNot Nothing Then rd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CreditorProhibitUse(ByVal editValue As String, ByRef creditorExists As Boolean, ByRef prohibitUse As Boolean) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        ' Default the value should there be an error or the creditor does not exist.
        creditorExists = True
        prohibitUse = False
        Dim gdr As New GetDataResult

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "SELECT [prohibit_use] FROM creditors WITH (NOLOCK) WHERE creditor=@creditor"
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = editValue

                Dim obj As Object = cmd.ExecuteScalar
                If obj Is System.DBNull.Value Then
                    creditorExists = False
                Else
                    If obj IsNot Nothing Then
                        prohibitUse = Convert.ToBoolean(obj)
                    End If
                End If
            End Using

        Catch ex As SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCreditorInfoById(ByVal myCreditorID As String, ByRef readCreditor As String, ByRef prohibitUse As Boolean) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim rd As SqlDataReader = Nothing

        ' Default the values should there not be a valid answer.
        readCreditor = String.Empty
        prohibitUse = False

        Dim gdr As New GetDataResult()
        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "SELECT [creditor], [prohibit_use] FROM creditors WITH (NOLOCK) WHERE creditor=@creditor"
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = myCreditorID
                rd = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
            End Using

            If rd.Read Then
                gdr.RowsAffected = 1
                If rd(0) IsNot Nothing AndAlso Not rd.IsDBNull(0) Then readCreditor = Convert.ToString(rd.GetValue(0))
                If rd(1) IsNot Nothing AndAlso Not rd.IsDBNull(1) Then prohibitUse = Convert.ToInt32(rd.GetValue(1)) <> 0
            End If

        Catch ex As SqlException
            gdr.HandleException(ex)

        Finally
            If rd IsNot Nothing Then rd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCreditorsByAccountNumber(ByVal ds As DataSet, ByVal tableName As String, ByVal accountNumber As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim sb As New StringBuilder
        sb.Append("SELECT c.creditor_id as creditor_id, c.creditor as creditor, c.sic, pct.fairshare_pct_eft, pct.fairshare_pct_check, coalesce(c.creditor_name+' '+c.division,c.creditor_name,c.division,'') as name, c.comment, dbo.format_contribution_type(pct.creditor_type_eft,pct.creditor_type_check) as fairshare, dbo.address_block_6(v.attn,v.addr1,v.addr2,v.addr3,v.addr4,v.addr5) as address FROM creditors c WITH (NOLOCK) RIGHT OUTER JOIN view_creditor_addresses v WITH (NOLOCK) ON v.creditor=c.creditor AND v.type = 'P' LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) ON c.creditor_contribution_pct = pct.creditor_contribution_pct")
        sb.Append(" WHERE c.prohibit_use=0 AND c.creditor_id IN (")
        sb.Append("SELECT creditor FROM creditor_methods m WITH (NOLOCK)")
        sb.Append(" INNER JOIN rpps_biller_ids ids WITH (NOLOCK) on m.rpps_biller_id = ids.rpps_biller_id")
        sb.Append(" INNER JOIN banks b WITH (NOLOCK) on m.bank = b.bank")
        sb.Append(" INNER JOIN rpps_masks mk WITH (NOLOCK) on ids.rpps_biller_id = mk.rpps_biller_id")
        sb.Append(" WHERE b.type = 'R'")
        sb.AppendFormat(" AND '{0}' LIKE dbo.map_rpps_masks ( mk.mask ) ", accountNumber.Replace("'", "''"))
        sb.AppendFormat(" AND {0} = mk.length)", accountNumber.Length)

        Return FillNamedDataTableFromSelectCommand(sb.ToString(), ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetCreditorsByName(ByVal ds As DataSet, ByVal tableName As String, ByVal myCreditorName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim sb As New StringBuilder
        sb.Append("SELECT c.creditor_id as creditor_id, c.creditor as creditor, c.sic, pct.fairshare_pct_eft, pct.fairshare_pct_check, coalesce(c.creditor_name+' '+c.division,c.creditor_name,c.division,'') as name, c.comment, dbo.format_contribution_type(pct.creditor_type_eft,pct.creditor_type_check) as fairshare, dbo.address_block_6(v.attn,v.addr1,v.addr2,v.addr3,v.addr4,v.addr5) as address FROM creditors c WITH (NOLOCK) RIGHT OUTER JOIN view_creditor_addresses v on v.creditor = c.creditor and v.type = 'P' LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) ON c.creditor_contribution_pct = pct.creditor_contribution_pct")
        sb.Append(" WHERE c.prohibit_use=0 AND c.creditor IN")
        sb.AppendFormat(" (SELECT creditor FROM creditors WITH (NOLOCK) WHERE creditor_name LIKE '%{0}%' OR division LIKE '%{0}%' UNION ALL SELECT creditor FROM creditor_addkeys WITH (NOLOCK) WHERE additional LIKE '%{0}%' AND type='A')", myCreditorName.Replace("'", "''"))

        Return FillNamedDataTableFromSelectCommand(sb.ToString(), ds, tableName)
    End Function
End Module
