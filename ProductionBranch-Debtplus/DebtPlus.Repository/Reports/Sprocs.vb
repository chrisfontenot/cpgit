
Imports System.Data.SqlClient

Namespace Reports
    Public Module Sprocs

        <System.Obsolete("Use LINQ Tables")> _
        Public Function PrintChecksVoid(ByVal bank As Int32, ByVal marker As String) As GetDataResult
            Return PrintChecksVoid(bank, marker, Nothing)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function PrintChecksVoid(ByVal bank As Int32, ByVal marker As String, ByVal txn As SqlTransaction) As GetDataResult
            Using cmd As SqlCommand = New SqlCommand
                cmd.CommandText = "rpt_PrintChecks_Void"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank
                cmd.Parameters.Add("@marker", SqlDbType.VarChar, 50).Value = marker
                Return ExecuteNonQuery(cmd, txn)
            End Using
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function AchBalance(ByVal ds As DataSet, ByVal tableName As String, ByVal pullDate As DateTime) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "rpt_ach_balance"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@ACH_Pull_Date", SqlDbType.DateTime).Value = pullDate
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function InvalidProposals(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_invalid_proposals"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function ProposalStandard(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32, ByVal proposalMode As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_proposal_standard"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int).Value = id
            cmd.Parameters.Add("@proposal_mode", SqlDbType.Int).Value = proposalMode   ' 0 = id is a proposal id, 2 = id is a batch id
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function ProposalFull(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32, ByVal proposalMode As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_proposal_proposal"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int).Value = id
            cmd.Parameters.Add("@proposal_mode", SqlDbType.Int).Value = proposalMode   ' 0 = id is a proposal id, 2 = id is a batch id
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function ProposalAddressInformation(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_Proposal_AddressInformation"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int).Value = id
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function CreditorAddressL(ByVal ds As DataSet, ByVal tableName As String, ByVal creditor As String) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_CreditorAddress_L"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function SummaryCreditorRefund(ByVal ds As DataSet, ByVal tableName As String, ByVal fromDate As DateTime, toDate As DateTime) As GetDataResult
            Using cmd As SqlCommand = New SqlCommand
                cmd.CommandText = "rpt_Summary_Creditor_Refund"
                cmd.CommandType = CommandType.StoredProcedure

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()

                    cmd.Connection = cn
                    SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                Catch ex As SqlClient.SqlException
                    Dim gdr As New GetDataResult()
                    gdr.HandleException(ex)
                    Return gdr

                Finally
                    cn.Dispose()
                End Try

                cmd.Parameters(1).Value = fromDate
                cmd.Parameters(2).Value = toDate
                cmd.CommandTimeout = 0
                cmd.Connection = Nothing
                Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
            End Using
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function InvalidAccounts(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_invalid_accounts_2"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function HeldInTrust(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT [group],[held_in_trust],[client],[name],[reserved_in_trust],[reserved_in_trust_cutoff],[counselor],[counselor_name],[last_name],[state],[disbursement_cycle] FROM view_held_in_trust WITH (NOLOCK) WHERE ([held_in_trust] <> 0)"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function HeldInTrustByState(ByVal ds As DataSet, ByVal tableName As String, ByVal state As String) As GetDataResult
            If String.IsNullOrWhiteSpace(state) Then Throw RepositoryException.BadParameters()
            Dim query As String = String.Format("SELECT [group],[held_in_trust],[client],[name],[reserved_in_trust],[reserved_in_trust_cutoff],[counselor],[counselor_name],[last_name],[state],[disbursement_cycle] FROM view_held_in_trust WITH (NOLOCK) WHERE ([held_in_trust] <> 0) AND ([state]='{0}')", state.Replace("'", "''"))
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function Balance(ByVal ds As DataSet, ByVal tableName As String, ByVal fromDate As DateTime, ByVal toDate As DateTime) As GetDataResult
            Using cmd As SqlCommand = New SqlCommand()
                cmd.CommandText = "rpt_balance"
                cmd.CommandType = CommandType.StoredProcedure

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()

                    cmd.Connection = cn
                    SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                Catch ex As SqlClient.SqlException
                    Dim gdr As New GetDataResult()
                    gdr.HandleException(ex)
                    Return gdr

                Finally
                    cn.Dispose()
                End Try

                cmd.Parameters(1).Value = fromDate
                cmd.Parameters(2).Value = toDate
                cmd.CommandTimeout = 0
                cmd.Connection = Nothing
                Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
            End Using
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function RppsFiles(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
            Const query As String = "SELECT TOP 50 rpps_response_file, date_posted, filename, label, file_date, date_created, created_by FROM rpps_response_files ORDER BY rpps_response_file DESC"
            Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCie(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cie"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCda(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cda"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdr(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdr"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdp(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdp"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdt(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdt"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdv(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdv"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdm(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdn" ' don't know why this is "cdn"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCdc(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_cdc"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseCoa(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_coa"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

        <System.Obsolete("Use LINQ Tables")> _
        Public Function GetRppsResponseDfs(ByVal ds As DataSet, ByVal tableName As String, ByVal rppsFileId As Int32) As GetDataResult
            Dim cmd As SqlCommand = New SqlCommand
            cmd.CommandText = "rpt_rpps_response_dfs"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@rpps_response_file", SqlDbType.Int).Value = rppsFileId
            cmd.CommandTimeout = 0
            Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
        End Function

    End Module
End Namespace
