﻿Imports System.Data.SqlClient
Imports Guidelight.Data

Public Module Addresses
    Private Const BaseQueryString As String = "SELECT [Address], [creditor_prefix_1], [creditor_prefix_2], [house], [direction], [street], [suffix], [modifier], [modifier_value], [address_line_2], [address_line_3], [city], [state], [postalcode] FROM Addresses WITH (NOLOCK)"

    <System.Obsolete("Use LINQ Tables")>
    Public Function GetById(ByVal ds As DataSet, ByVal tableName As String, ByVal id As Int32) As GetDataResult
        Dim query As String = String.Format("{0} WHERE [Address]={1:f0}", BaseQueryString, id)
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function GetSchema(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If String.IsNullOrWhiteSpace(tableName) Then Throw RepositoryException.BadParameters()
        Return FillNamedDataTableSchemaFromSelectCommand(BaseQueryString, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function GetById(ByRef a As Models.Address, ByVal oId As Object) As GetDataResult
        Return GetById(a, oId, False, False, False, False)
    End Function

    <Obsolete("Use LINQ Tables")>
    Public Function GetById(ByRef a As Models.Address, ByVal oId As Object, ShowAttn As Boolean, ShowCreditor1 As Boolean, ShowCreditor2 As Boolean, ShowLine3 As Boolean) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim id As Int32 = If(oId IsNot DBNull.Value, Convert.ToInt32(oId), 0)
        Dim gdr As New GetDataResult()

        If id > 0 Then
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim reader As SqlDataReader = Nothing

            Try
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand
                    cmd.Connection = cn
                    cmd.CommandText = String.Format("{0} WHERE [Address]=@AddressID", BaseQueryString)
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@AddressId", SqlDbType.Int).Value = id

                    reader = cmd.ExecuteReader()
                    If reader IsNot Nothing AndAlso reader.Read() Then
                        gdr.RowsAffected = 1
                        a = MapRowToModel(reader, ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3)
                    End If
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                gdr.HandleException(ex)

            Finally
                If reader IsNot Nothing Then reader.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End If

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")>
    Public Function Insert(ByVal address As Models.Address) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If address Is Nothing Then Throw New ArgumentNullException("address")
        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Try
            cn.Open()

            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "xpr_insert_address"
                cmd.CommandType = CommandType.StoredProcedure
                MapModelToCommandParameters(address, cmd)
                cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

                gdr.RowsAffected = cmd.ExecuteNonQuery()
                If gdr.RowsAffected = 1 AndAlso cmd.Parameters("@RETURN_VALUE").Value IsNot DBNull.Value Then
                    address.Id = Convert.ToInt32(cmd.Parameters("@RETURN_VALUE").Value)
                End If
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")>
    Public Function Update(ByVal address As Models.Address) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If address Is Nothing Then Throw New ArgumentNullException("address")
        Dim gdr As New GetDataResult()
        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()

            Using cmd As New SqlCommand
                cmd.Connection = cn
                cmd.CommandText = "xpr_update_address"
                cmd.CommandType = CommandType.StoredProcedure
                MapModelToCommandParameters(address, cmd)
                cmd.Parameters.Add("@Address", SqlDbType.Int).Value = address.Id

                gdr.RowsAffected = cmd.ExecuteNonQuery()
            End Using

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function CommitChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_insert_address"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@Address", SqlDbType.Int, 0, "Address").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@Creditor_Prefix_1", SqlDbType.VarChar, 80, "creditor_Prefix_1")
            .Parameters.Add("@Creditor_Prefix_2", SqlDbType.VarChar, 80, "creditor_Prefix_2")
            .Parameters.Add("@House", SqlDbType.VarChar, 10, "house")
            .Parameters.Add("@Direction", SqlDbType.VarChar, 10, "direction")
            .Parameters.Add("@Street", SqlDbType.VarChar, 60, "street")
            .Parameters.Add("@Suffix", SqlDbType.VarChar, 10, "suffix")
            .Parameters.Add("@Modifier", SqlDbType.VarChar, 10, "modifier")
            .Parameters.Add("@Modifier_Value", SqlDbType.VarChar, 10, "modifier_Value")
            .Parameters.Add("@Address_Line_2", SqlDbType.VarChar, 80, "address_Line_2")
            .Parameters.Add("@Address_Line_3", SqlDbType.VarChar, 80, "address_Line_3")
            .Parameters.Add("@City", SqlDbType.VarChar, 60, "city")
            .Parameters.Add("@State", SqlDbType.Int, 0, "state")
            .Parameters.Add("@PostalCode", SqlDbType.VarChar, 30, "postalCode")
        End With

        Dim updateCommand As New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "xpr_update_address"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@Address", SqlDbType.Int, 0, "Address")
            .Parameters.Add("@Creditor_Prefix_1", SqlDbType.VarChar, 80, "creditor_Prefix_1")
            .Parameters.Add("@Creditor_Prefix_2", SqlDbType.VarChar, 80, "creditor_Prefix_2")
            .Parameters.Add("@House", SqlDbType.VarChar, 10, "house")
            .Parameters.Add("@Direction", SqlDbType.VarChar, 10, "direction")
            .Parameters.Add("@Street", SqlDbType.VarChar, 60, "street")
            .Parameters.Add("@Suffix", SqlDbType.VarChar, 10, "suffix")
            .Parameters.Add("@Modifier", SqlDbType.VarChar, 10, "modifier")
            .Parameters.Add("@Modifier_Value", SqlDbType.VarChar, 10, "modifier_Value")
            .Parameters.Add("@Address_Line_2", SqlDbType.VarChar, 80, "address_Line_2")
            .Parameters.Add("@Address_Line_3", SqlDbType.VarChar, 80, "address_Line_3")
            .Parameters.Add("@City", SqlDbType.VarChar, 60, "city")
            .Parameters.Add("@State", SqlDbType.Int, 0, "state")
            .Parameters.Add("@PostalCode", SqlDbType.VarChar, 30, "postalCode")
        End With

        Dim deleteCommand As New SqlCommand()
        With deleteCommand
            .Connection = cn
            .CommandText = "DELETE FROM addresses WHERE address=@Address"
            .CommandType = CommandType.Text
            .Parameters.Add("@Address", SqlDbType.Int, 0, "Address")
        End With

        Try
            da.UpdateCommand = updateCommand
            da.InsertCommand = insertCommand
            da.DeleteCommand = deleteCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If deleteCommand IsNot Nothing Then deleteCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")>
    Public Function CreateSpecificNewAddressID(ByVal Address As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "SET identity_insert Addresses ON;" +
                                  "INSERT INTO Addresses(Address,Country) VALUES (@Telephone,0);" +
                                  "SET identity_insert Addresses OFF;"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@Address", SqlDbType.Int).Value = Address
            Return ExecuteNonQuery(cmd)
        End Using

    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function CreateNewAddressID(ByRef a As Models.Address) As GetDataResult
        a = New Models.Address(False, False, False, False)
        Return Insert(a)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function CreateNewAddressID(ByVal address As Int32) As GetDataResult
        Dim v As New Models.Address(False, False, False, False)
        v.Id = address
        Return Insert(v)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Function MapRowToModel(ByVal reader As SqlDataReader, ShowAttn As Boolean, ShowCreditor1 As Boolean, ShowCreditor2 As Boolean, ShowLine3 As Boolean) As Models.Address
        If reader Is Nothing Then Return Nothing

        Dim address = New Models.Address(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3)

        address.Id = reader.SafeGetIntOrZero("Address")
        address.Creditor_Prefix_1 = reader.SafeGetNullableString("creditor_prefix_1")
        address.Creditor_Prefix_2 = reader.SafeGetNullableString("creditor_prefix_2")
        address.House = reader.SafeGetNullableString("house")
        address.Direction = reader.SafeGetNullableString("direction")
        address.Street = reader.SafeGetNullableString("street")
        address.Suffix = reader.SafeGetNullableString("suffix")
        address.Modifier = reader.SafeGetNullableString("modifier")
        address.Modifier_Value = reader.SafeGetNullableString("modifier_value")
        address.Address_Line_2 = reader.SafeGetNullableString("address_line_2")
        address.Address_Line_3 = reader.SafeGetNullableString("address_line_3")
        address.City = reader.SafeGetNullableString("city")
        address.State = reader.SafeGetIntOrZero("state")
        address.PostalCode = reader.SafeGetNullableString("postalcode")

        Return address
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Sub MapModelToCommandParameters(ByVal address As Models.Address, ByVal cmd As SqlCommand)

        If address IsNot Nothing AndAlso cmd IsNot Nothing Then
            cmd.Parameters.Add("@Creditor_Prefix_1", SqlDbType.VarChar, 80).Value = address.Creditor_Prefix_1
            cmd.Parameters.Add("@Creditor_Prefix_2", SqlDbType.VarChar, 80).Value = address.Creditor_Prefix_2
            cmd.Parameters.Add("@House", SqlDbType.VarChar, 10).Value = address.House
            cmd.Parameters.Add("@Direction", SqlDbType.VarChar, 10).Value = address.Direction
            cmd.Parameters.Add("@Street", SqlDbType.VarChar, 60).Value = address.Street
            cmd.Parameters.Add("@Suffix", SqlDbType.VarChar, 10).Value = address.Suffix
            cmd.Parameters.Add("@Modifier", SqlDbType.VarChar, 10).Value = address.Modifier
            cmd.Parameters.Add("@Modifier_Value", SqlDbType.VarChar, 10).Value = address.Modifier_Value
            cmd.Parameters.Add("@Address_Line_2", SqlDbType.VarChar, 80).Value = address.Address_Line_2
            cmd.Parameters.Add("@Address_Line_3", SqlDbType.VarChar, 80).Value = address.Address_Line_3
            cmd.Parameters.Add("@City", SqlDbType.VarChar, 60).Value = address.City
            cmd.Parameters.Add("@State", SqlDbType.Int, 0).Value = address.State
            cmd.Parameters.Add("@PostalCode", SqlDbType.VarChar, 30).Value = address.PostalCode
        End If

    End Sub
End Module
