﻿Imports System.Data.SqlClient
Imports System.Transactions
Imports Guidelight.Data

Public Module referred_by_types
#If False Then
    <Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal referredBy As Models.ResourceType) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If referredBy Is Nothing Then Throw New ArgumentNullException("referredBy")
        Dim gdr As New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

        Try
            cn.Open()
            Using cmd As SqlCommand = New SqlCommand()
                cmd.Connection = cn
                cmd.CommandText = "INSERT INTO [referred_by_types] ([description]) VALUES (@description); SELECT scope_identity() AS referred_by_type;"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("description", SqlDbType.VarChar, 50).Value = referredBy.Description
                referredBy.Id = cmd.ExecuteScalar()
            End Using

            gdr.RowsAffected = 1

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            cn.Dispose()
        End Try

        Return gdr
    End Function
#End If
End Module
