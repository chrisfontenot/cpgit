﻿Imports System.Data.SqlClient
Imports Guidelight.Data

Public Module TelephoneNumbers
#If False Then
    <Obsolete("Use LINQ Tables")> _
    Public Function GetById(ByVal oId As Object, ByRef Telephone As Models.TelephoneNumber) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim gdr As New GetDataResult()
        Dim Result As Models.TelephoneNumber = Nothing

        If oId IsNot DBNull.Value AndAlso oId IsNot Nothing Then
            Dim id As Int32 = Convert.ToInt32(oId)
            If id >= 1 Then

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim reader As SqlDataReader = Nothing

                Try
                    cn.Open()

                    Using cmd As SqlCommand = New SqlCommand
                        cmd.Connection = cn
                        cmd.CommandText = "SELECT [TelephoneNumber],[Country],[ACode],[Number],[Ext] FROM TelephoneNumbers WITH (NOLOCK) WHERE TelephoneNumber=@TelephoneNumber"
                        cmd.CommandType = CommandType.Text
                        cmd.Parameters.Add("@TelephoneNumber", SqlDbType.Int).Value = id

                        reader = cmd.ExecuteReader()
                        If reader IsNot Nothing AndAlso reader.Read Then
                            Result = MapRowToModel(reader)
                        End If
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    gdr.HandleException(ex)

                Finally
                    If reader IsNot Nothing Then reader.Dispose()
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If
        End If

        Telephone = Result
        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateValuesById(ByVal id As Int32, ByVal Country As Int32, ByVal Acode As String, ByVal Number As String, ByVal Ext As String) As GetDataResult
        Dim e As New Models.TelephoneNumber(Country, Acode, Number, Ext)
        e.Id = id
        Return UpdateById(e)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateValuesById(ByVal id As Int32, ByVal Country As Int32, ByVal Acode As String, ByVal Number As String, ByVal Ext As String, ByVal txn As SqlTransaction) As GetDataResult
        Dim e As New Models.TelephoneNumber(Country, Acode, Number, Ext)
        e.Id = id
        Return UpdateById(e, txn)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateById(ByVal e As Models.TelephoneNumber) As GetDataResult
        Return UpdateById(e, Nothing)
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateById(ByVal e As Models.TelephoneNumber, ByVal txn As SqlTransaction) As GetDataResult
        Dim query As String = String.Format("UPDATE TelephoneNumbers SET [Country]={0:f0},[ACode]='{1}',[Number]='{2}',[Ext]='{3}' WHERE [TelephoneNumber]={4:f0}", e.Country, e.Acode.Replace("'", "''"), e.Number.Replace("'", "''"), e.Ext.Replace("'", "''"), e.Id)
        Return ExecuteNonQuery(query, txn)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CreateSpecificNewTelephoneID(ByVal telephone As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "SET identity_insert TelephoneNumbers ON;" +
                                  "INSERT INTO TelephoneNumbers(TelephoneNumber,Country,Acode,Number,Ext) VALUES (@Telephone,1,'','','');" +
                                  "SET identity_insert TelephoneNumbers OFF;"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@Telephone", SqlDbType.Int).Value = telephone
            Return ExecuteNonQuery(cmd)
        End Using

    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function Insert(ByVal e As Models.TelephoneNumber) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "xpr_insert_TelephoneNumbers"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("RETURN", SqlDbType.Int, 0, "TelephoneNumber").Direction = ParameterDirection.ReturnValue

            TelephoneNumbers.MapModelToCommandParameters(e, cmd)

            Dim gdr = ExecuteNonQuery(cmd)
            If gdr.Success AndAlso cmd.Parameters("RETURN") IsNot Nothing Then
                e.Id = Convert.ToInt32(cmd.Parameters("RETURN").Value)
            End If

            Return gdr
        End Using
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Function MapRowToModel(ByVal reader As SqlDataReader) As Models.TelephoneNumber
        If reader Is Nothing Then Return Nothing

        Dim TelephoneNumber = New Models.TelephoneNumber() With {
            .Id = reader.SafeGetIntOrZero("TelephoneNumber"),
            .Country = reader.SafeGetNullableInt("Country"),
            .Acode = reader.SafeGetNullableString("Acode"),
            .Number = reader.SafeGetNullableString("Number"),
            .Ext = reader.SafeGetNullableString("Ext")
        }

        Return TelephoneNumber
    End Function

    <Obsolete("Use LINQ Tables")> _
    Friend Sub MapModelToCommandParameters(ByVal TelephoneNumber As Models.TelephoneNumber, ByRef cmd As SqlCommand)
        If TelephoneNumber IsNot Nothing AndAlso cmd IsNot Nothing Then
            cmd.Parameters.Add("@Country", SqlDbType.Int, 0).Value = TelephoneNumber.Country
            cmd.Parameters.Add("@ACode", SqlDbType.VarChar, 80).Value = If(TelephoneNumber.Acode Is Nothing, System.DBNull.Value, TelephoneNumber.Acode)
            cmd.Parameters.Add("@Number", SqlDbType.VarChar, 80).Value = If(TelephoneNumber.Number Is Nothing, System.DBNull.Value, TelephoneNumber.Number)
            cmd.Parameters.Add("@Ext", SqlDbType.VarChar, 80).Value = If(TelephoneNumber.Ext Is Nothing, System.DBNull.Value, TelephoneNumber.Ext)
        End If
    End Sub
#End If
End Module
