Imports System.Data.SqlClient
Imports Guidelight.Data

Public Module General
    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetRegisterPeopleNameTransactions(ByVal ds As DataSet, ByVal tableName As String, ByVal trustRegister As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If trustRegister <= 0 Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Try
            cmd.Connection = cn
            cmd.CommandText = "SELECT rcc.client_creditor_register, rcc.client_creditor, rcc.tran_type, rcc.client, rcc.creditor, rcc.creditor_type, rcc.disbursement_register, rcc.trust_register, rcc.invoice_register, rcc.creditor_register, rcc.credit_amt, rcc.debit_amt, rcc.fairshare_amt, rcc.fairshare_pct, rcc.account_number, rcc.void, rcc.date_created, rcc.created_by, dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name FROM registers_client_creditor rcc WITH (NOLOCK) LEFT OUTER JOIN people p WITH (NOLOCK) on rcc.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE rcc.trust_register = @trust_register AND rcc.tran_type in ('AD','MD','CM')"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@trust_register", SqlDbType.Int).Value = trustRegister

            cn.Open()

            da.SelectCommand = cmd

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RPPSProposalSearch(ByVal ds As DataSet, ByVal tableName As String, ByVal billerName As String) As GetDataResult
        Dim cmd As SqlCommand = New SqlCommand()

        cmd.CommandText = "xpr_rpps_proposal_search"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add(New SqlParameter("@biller_name", SqlDbType.VarChar, 256)).Value = billerName

        Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllRPPSResponseFiles(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT [rpps_response_file],[date_posted],[filename],[label],[file_date],[date_created],[created_by] FROM rpps_response_files WHERE [date_posted] IS NULL"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitRPPSResponseFileChanges(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim updateCommand As SqlCommand = New SqlCommand()
        With updateCommand
            .Connection = cn
            .CommandText = "UPDATE rpps_response_files SET label=@label WHERE rpps_response_file=@rpps_response_file"
            .CommandType = CommandType.Text
            .Parameters.Add("@label", SqlDbType.VarChar, 80, "label")
            .Parameters.Add("@rpps_response_file", SqlDbType.Int, 0, "rpps_response_file")
        End With

        Try
            da.UpdateCommand = updateCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If updateCommand IsNot Nothing Then updateCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CommitRPPSResponseFileChanges2(ByVal dt As DataTable) As CommitChangesResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If dt Is Nothing Then Throw RepositoryException.NoDataTable()

        Dim ccr As CommitChangesResult = New CommitChangesResult(dt)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Dim insertCommand As SqlCommand = New SqlCommand()
        With insertCommand
            .Connection = cn
            .CommandText = "xpr_rpps_response_file"
            .CommandType = CommandType.StoredProcedure
            .Parameters.Add("@rpps_response_file", SqlDbType.Int, 0, "rpps_response_file").Direction = ParameterDirection.ReturnValue
            .Parameters.Add("@label", SqlDbType.VarChar, 50, "label")
        End With

        Try
            da.InsertCommand = insertCommand

            cn.Open()

            ccr.RowsAffected = da.Update(dt)

        Catch ex As System.Data.SqlClient.SqlException
            Return ccr.HandleException(ex)

        Finally
            If insertCommand IsNot Nothing Then insertCommand.Dispose()
            If da IsNot Nothing Then da.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return ccr
    End Function

#If False Then
    <System.Obsolete("Use LINQ Tables")> _
    Public Function RegistersDisursement(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT TOP 50 ids.disbursement_register AS 'ID', ids.date_created AS 'Date', ids.created_by AS 'Creator', isnull(ids.note,'') as 'Note' FROM registers_disbursement ids WITH (NOLOCK) ORDER BY 2 desc"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function DepositBatchIdsJoinedToBanks(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT TOP 50 ids.deposit_batch_id AS 'ID', ids.date_created AS 'Date', ids.created_by AS 'Creator', coalesce(bnk.description,'Bank #' + convert(varchar,ids.bank),'') as 'Bank', isnull(ids.note,'') as 'Note', coalesce(ids.note, 'Unlabeled batch #' + convert(varchar, ids.deposit_batch_id), '') AS DisplayMember FROM deposit_batch_ids ids WITH (NOLOCK) LEFT OUTER JOIN banks bnk WITH (NOLOCK) ON ids.bank = bnk.bank WHERE ids.date_posted IS NULL AND ids.batch_type='CL' ORDER BY 2 desc"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetClientInfo(ByVal ds As DataSet, ByVal tableName As String, ByVal clientId As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()
        If clientId <= 0 Then Throw RepositoryException.BadParameters()

        Dim gdr As GetDataResult = New GetDataResult()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter

        Try
            cmd.Connection = cn
            cmd.CommandText = "SELECT c.[active_status],dbo.format_normal_name(pn.[prefix],pn.[first],pn.[middle],pn.[last],pn.[suffix]) as client_name,dbo.format_address_line_1(a.[house],a.[direction],a.[street],a.[suffix],a.[modifier],a.[modifier_value]) as addr1,a.[address_line_2] as addr2,dbo.format_city_state_zip(a.[city],a.[state],a.[postalcode]) as addr3 FROM clients c WITH (NOLOCK) INNER JOIN people p WITH (NOLOCK) on c.[client] = p.[client] and 1 = p.[relation] LEFT OUTER JOIN Names pn WITH (NOLOCK) ON p.[NameID] = pn.[Name] LEFT OUTER JOIN Addresses a WITH (NOLOCK) ON c.[AddressID] = a.[Address] WHERE c.[client] = @client"
            cmd.CommandType = CommandType.Text
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientId

            cn.Open()

            da.SelectCommand = cmd

            gdr.RowsAffected = da.Fill(ds, tableName)

        Catch ex As System.Data.SqlClient.SqlException
            Return gdr.HandleException(ex)

        Finally
            If da IsNot Nothing Then da.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()
        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Function LetterFetch(ByRef ltr As Models.LetterInfo, ByVal letterCode As String, ByVal client As Int32, ByVal creditorId As Int32, ByVal creditor As String, ByVal debt As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand
        Dim reader As SqlDataReader = Nothing

        Dim gdr As New GetDataResult()

        Try
            cmd.Connection = cn
            cmd.CommandText = "xpr_letter_fetch"
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@letter_code", SqlDbType.VarChar, 10).Value = letterCode
            cmd.Parameters.Add("@client", SqlDbType.Int, 0).Value = client
            cmd.Parameters.Add("@creditor_id", SqlDbType.Int, 0).Value = creditorId
            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
            cmd.Parameters.Add("@debt", SqlDbType.Int, 0).Value = debt

            cn.Open()
            reader = cmd.ExecuteReader()
            If reader IsNot Nothing AndAlso reader.Read() Then
                gdr.RowsAffected = 1
                ltr = MapRowToLetterInfoModel(reader)
            End If

        Catch ex As System.Data.SqlClient.SqlException
            gdr.HandleException(ex)

        Finally
            If reader IsNot Nothing Then reader.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
            If cn IsNot Nothing Then cn.Dispose()

        End Try

        Return gdr
    End Function

    <Obsolete("Use LINQ Tables")> _
    Private Function MapRowToLetterInfoModel(ByVal reader As SqlDataReader) As Models.LetterInfo
        If reader Is Nothing Then Return Nothing

        Dim model = New Models.LetterInfo
        model.TopMargin = reader.SafeGetDecimal("top_margin")
        model.BottomMargin = reader.SafeGetDecimal("bottom_margin")
        model.LeftMargin = reader.SafeGetDecimal("left_margin")
        model.RightMargin = reader.SafeGetDecimal("right_margin")
        model.Description = reader.SafeGetString("Description")
        model.Filename = reader.SafeGetString("filename")
        model.Language = reader.SafeGetIntOrZero("language")
        model.LetterCode = reader.SafeGetNullableString("letter_code")
        model.LetterType = reader.SafeGetIntOrZero("letter_type")
        model.DisplayMode = reader.SafeGetIntOrZero("display_mode")
        model.QueueName = reader.SafeGetString("queue_name")

        Return model
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function CreateReconcileBatch(ByVal note As String, ByRef BatchID As Int32) As GetDataResult
        If String.IsNullOrWhiteSpace(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString) Then Throw RepositoryException.NotInitialized()

        Using cmd As SqlCommand = New SqlCommand()
            cmd.CommandText = "xpr_insert_recon_batches"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
            cmd.Parameters.Add("@note", SqlDbType.VarChar, 80).Value = note

            Dim gdr As GetDataResult = ExecuteNonQuery(cmd)
            If gdr.Success Then
                BatchID = Convert.ToInt32(cmd.Parameters(0).Value)
            End If

            Return gdr
        End Using
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function GetAllReconcileBatches(ByVal ds As DataSet, ByVal tableName As String) As GetDataResult
        Const query As String = "SELECT recon_batch AS 'BatchID', note AS 'Note', created_by, date_created FROM recon_batches WITH (NOLOCK) WHERE date_posted IS NULL ORDER BY date_created DESC"
        Return FillNamedDataTableFromSelectCommand(query, ds, tableName)
    End Function

    <System.Obsolete("Use LINQ Tables")> _
    Public Function RPPSBillerSearch(ByVal ds As DataSet, ByVal tableName As String, ByVal billerName As String) As GetDataResult
        Dim cmd As SqlCommand = New SqlCommand()

        cmd.CommandText = "xpr_rpps_biller_search"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add(New SqlParameter("@biller_name", SqlDbType.VarChar, 256)).Value = billerName

        Return FillNamedDataTableFromSqlCommand(cmd, ds, tableName)
    End Function
#End If
End Module

