﻿using System;

namespace DebtPlus.Format
{
    // facade to Utils.Format.CsutomerFomatter
    public class CustomFormatter : ICustomFormatter, IFormatProvider
    {
        private readonly Utils.Format.CustomFormatter _cf = new Utils.Format.CustomFormatter();

        public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
        public object GetFormat(Type formatType) { return this; }
    }
}
