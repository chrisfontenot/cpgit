﻿using System;

namespace DebtPlus.Format
{
    public static class Counselor
    {
        public static String FormatCounselor(object counselorName) { return Utils.Format.Counselor.FormatCounselor(counselorName); }

        public class CustomFormatter : ICustomFormatter, IFormatProvider
        {
            private readonly Utils.Format.Counselor.CustomFormatter _cf = new Utils.Format.Counselor.CustomFormatter();

            public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
            public object GetFormat(Type formatType) { return this; }
        }
    }
}
