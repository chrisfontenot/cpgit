﻿using System;

namespace DebtPlus.Format
{
    public static class SSN
    {
        public static String FormatSSN(object ssn) { return Utils.Format.SSN.FormatSSN(ssn); }

        public class CustomFormatter : ICustomFormatter, IFormatProvider
        {
            private readonly Utils.Format.SSN.CustomFormatter _cf = new Utils.Format.SSN.CustomFormatter();

            public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
            public object GetFormat(Type formatType) { return this; }
        }
    }
}
