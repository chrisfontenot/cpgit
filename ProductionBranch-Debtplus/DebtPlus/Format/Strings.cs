﻿using System;

namespace DebtPlus.Format
{
    public static class Strings
    {
        public static Boolean IsRTF(String inputString) { return Utils.Format.Strings.IsRTF(inputString); }
        public static Boolean IsHTML(String inputString) { return Utils.Format.Strings.IsHTML(inputString); }
        public static String DigitsOnly(String inputString) { return Utils.Format.Strings.DigitsOnly(inputString); }
        public static long FormattedWholeDollars(Decimal curValue) { return Utils.Format.Strings.FormattedWholeDollars(curValue); }
        public static long FormattedMoney(Decimal curValue) { return Utils.Format.Strings.FormattedMoney(curValue); }
    }
}
