﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Format
{
    public static class PostalCode
    {
        public static String FormatPostalCode(object input) { return Utils.Format.PostalCode.FormatPostalCode(input); }

        public class CustomFormatter : ICustomFormatter, IFormatProvider
        {
            private readonly Utils.Format.PostalCode.CustomFormatter _cf = new Utils.Format.PostalCode.CustomFormatter();

            public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
            public object GetFormat(Type formatType) { return this; }
        }
    }
}
