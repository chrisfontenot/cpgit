﻿using System;

namespace DebtPlus.Format
{
    public static class Time
    {
        public static String HoursAndMinutes(object value) { return Utils.Format.Time.HoursAndMinutes(value); }
    }
}
