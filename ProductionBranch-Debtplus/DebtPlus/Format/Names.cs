﻿using System;

namespace DebtPlus.Format
{
    public static class Names
    {
        public static String FormatNormalName(object prefix, object first, object middle, object last, object suffix) { return Utils.Format.Names.FormatNormalName(prefix, first, middle, last, suffix); }
        public static String FormatReverseName(object prefix, object first, object middle, object last, object suffix) { return Utils.Format.Names.FormatReverseName(prefix, first, middle, last, suffix); }
    }
}
