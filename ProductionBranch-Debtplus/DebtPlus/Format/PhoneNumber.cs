﻿using System;

namespace DebtPlus.Format
{
    public static class PhoneNumber
    {
        public static String ExtractPhoneNumber(object input) { return Utils.Format.PhoneNumber.ExtractPhoneNumber(input); }
        public static String ExtractExtension(object input) { return Utils.Format.PhoneNumber.ExtractExtension(input); }
        public static String FormatPhoneNumber(object input) { return Utils.Format.PhoneNumber.FormatPhoneNumber(input); }
        public static String FormatExtension(object input) { return Utils.Format.PhoneNumber.FormatExtension(input); }
        public static String FormatPhoneAndExtension(object input) { return Utils.Format.PhoneNumber.FormatPhoneAndExtension(input); }

        public class CustomFormatter : ICustomFormatter, IFormatProvider
        {
            private readonly Utils.Format.PhoneNumber.CustomFormatter _cf = new Utils.Format.PhoneNumber.CustomFormatter();

            public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
            public object GetFormat(Type formatType) { return this; }
        }
    }
}
