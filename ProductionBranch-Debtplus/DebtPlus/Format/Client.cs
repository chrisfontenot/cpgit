﻿using System;

namespace DebtPlus.Format
{
    // facade to Utils.Format.Client
    public static class Client
    {
        public static String FormatClientID(object clientObject) { return Utils.Format.Client.FormatClientID(clientObject); }

        public class CustomFormatter : ICustomFormatter, IFormatProvider
        {
            private readonly Utils.Format.Client.CustomFormatter _cf = new Utils.Format.Client.CustomFormatter();

            public String Format(string format1, object arg, IFormatProvider formatProvider) { return _cf.Format(format1, arg, formatProvider); }
            public object GetFormat(Type formatType) { return this; }
        }
    }
}
