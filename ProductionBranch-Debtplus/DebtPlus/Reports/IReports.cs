﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DebtPlus.Reports
{
    public interface IReports: IDisposable
    {
        String ReportTitle { get; }
        String ReportSubTitle { get; }

        bool AllowParameterChangesByUser { get; set; }
        void SetReportParameter(string parameterName, object value);
        DialogResult RequestReportParameters();
        Dictionary<string, string> ParametersDictionary();

        void RunReport();
        void RunReportInSeparateThread();

        DialogResult ShowPageSetupDialog();

        void PrepareReport();
        void ShowReport();
        void ShowPreview();
        void ShowPreviewDialog();
        void Print();
        void Print(String printerName);

        void CreateRtfDocument(String path);
        void CreateRtfDocument(Stream stream);

    }
}
