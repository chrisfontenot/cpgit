﻿namespace DebtPlus.Reports
{
    public interface IReportFilter
    {
        ReportFilterItems ReportFilter { get; set; }
    }
}
