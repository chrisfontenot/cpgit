#region Copyright 2000-2012 DebtPlus, L.L.C.

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Linq;
using System.Windows.Forms;
  
// **************************************************************************************
// **                                                                                  **
// **  THIS IS A COPY OF THE FILE THAT IS STORED IN THE LOCATION                       **
// **  DEBTPLUS.DATA.GENERALCLASSES.ERRORS.CS. IT IS MEANT TO BE SO. THE REAON IS      **
// **  THAT THE REPORTS EXPECT TO SEE A PROCEDURE CALLED DEBTPLUS.ERRORS AND IF IT     **
// **  IS NOT THERE THEN THEY DON'T RUN PROPERLY.                                      **
// **                                                                                  **
// **  SO, FOR THE TIME BEING, UNTIL THE REPORTS MAY BE MODIFIED, WE NEED TO HAVE      **
// **  THIS PROCEDURE AND ALL OF LOGGING THAT GOES ALONG WITH IT.                      **
// **                                                                                  **
// **************************************************************************************
  
namespace DebtPlus
{
    public class Errors
    {
        /// <summary>
        /// Don't do anything. This is just a convient spot to put a breakpoint when debugging the code.
        /// All exceptions call this routine, even if they don't do anything about the error.
        /// </summary>
        /// <remarks>This routine does nothing by default.</remarks>
        public static void DebuggingException()
        {
            DebuggingException((Exception)null);
        }
        
        /// <summary>
        /// Don't do anything. This is just a convient spot to put a breakpoint when debugging the code.
        /// All exceptions call this routine, even if they don't do anything about the error.
        /// </summary>
        /// <param name="ex">The current exception</param>
        /// <remarks>This routine does nothing by default.</remarks>
        public static void DebuggingException(Exception ex)
        {
        }
        
        /// <summary>
        /// Generate a message box showing the database error that occured. This is not neceissarly an
        /// error condition (indicating that it is broken), but it indicates that the expected operation did not
        /// complete successfully.
        /// </summary>
        /// <param name="ex">The database error exception</param>
        /// <remarks></remarks>
        [Obsolete("Should use DebtPlus.Data.Errors.ExceptionMessageBox()")]
        public static void DatabaseError(Exception ex)
        {
            ExceptionMessageBox(ex);
        }
        
        /// <summary>
        /// Generate a message box showing the database error that occured. This is not neceissarly an
        /// error condition (indicating that it is broken), but it indicates that the expected operation did not
        /// complete successfully.
        /// </summary>
        /// <param name="ex">The database error exception</param>
        /// <param name="caption">Caption for the message box</param>
        /// <remarks></remarks>
        [Obsolete("Should use DebtPlus.Data.Errors.ExceptionMessageBox()")]
        public static void DatabaseError(Exception ex, string caption)
        {
            ExceptionMessageBox(ex, caption);
        }
            
        private class ExceptionArguments
        {
            // Information about the message box
            public DialogResult Result = DialogResult.OK;
            public bool ShowStackTrace = true;
            
            // Arguments to display in the message box
            public readonly Exception ex;
            public readonly string Caption = "Database Error";
            public readonly MessageBoxButtons Buttons = MessageBoxButtons.OK;
            public readonly MessageBoxDefaultButton DefaultButton = MessageBoxDefaultButton.Button1;
            public readonly MessageBoxOptions Options = MessageBoxOptions.DefaultDesktopOnly;
            public readonly MessageBoxIcon Icon = MessageBoxIcon.Error;
            public readonly bool IsModal;
                
            public string MessageText
            {
                get
                {
                    if (ShowStackTrace)
                    {
                        return ex.ToString();
                    }
                    else
                    {
                        return ex.Message;
                    }
                }
            }

            public ExceptionArguments(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton DefaultButton, MessageBoxOptions Options, bool IsModal)
            {
                this.ex = ex;
                if (Caption != string.Empty)
                {
                    this.Caption = Caption;
                }
                if (Buttons != MessageBoxButtons.OK)
                {
                    this.Buttons = Buttons;
                }
                if (DefaultButton != MessageBoxDefaultButton.Button1)
                {
                    this.DefaultButton = DefaultButton;
                }
                if (Options != MessageBoxOptions.DefaultDesktopOnly)
                {
                    this.Options = Options;
                }
                this.IsModal = IsModal;
            }
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        public static DialogResult ExceptionMessageBox(Exception ex)
        {
            return ConductDialog(new ExceptionArguments(ex, string.Empty, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <param name="Options">Options for the message box</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton, MessageBoxOptions Options)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, Options, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        public static DialogResult ExceptionMessageBoxDialog(Exception ex)
        {
            return ConductDialog(new ExceptionArguments(ex, string.Empty, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <param name="Options">Options for the message box</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton, MessageBoxOptions Options)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, Options, true));
        }
            
        private static DialogResult ConductDialog(ExceptionArguments Args)
        {
            // Go to the common handler
            DebuggingException();
            
            // if there is a single button of OK, then start a thread to run the message in the background.
            // Otherwise, do it in the foreground so that we can return the proper result.
            if (Args.Buttons == MessageBoxButtons.OK && !Args.IsModal)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(DatabaseError_Thread);
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Start(Args);
                return DialogResult.OK;
            }
            else
            {
                DatabaseError_Thread(Args);
                return Args.Result;
            }
        }
        
        /// <summary>
        /// Thread to handle the message box for the database error. We do this in a seperate
        /// thread to allow the calling thread to return and do the cleanup operation on the
        /// failure while the user is looking at the error window. It prevents a lockup condition
        /// should a transaction be used.
        /// </summary>
        /// <param name="Obj">Parameters for the messagebox event</param>
        /// <remarks></remarks>
        private static void DatabaseError_Thread(object Obj)
        {
            ExceptionArguments Args = ((ExceptionArguments)Obj);
            
            // Write the information to the system log
            using (System.Diagnostics.EventLog log = new System.Diagnostics.EventLog() { Source = "DebtPlus" })
            {
                try
                {
                    log.WriteEntry(Args.ex.ToString(), System.Diagnostics.EventLogEntryType.Error); //Write the event to the system log so that we don't loose it.
                    Args.ShowStackTrace = false;
                }
                catch (System.Security.SecurityException)
                {
                    // Recognized exception. Ignore it.
                }
                catch (Exception exLog)
                {
                    // All others generate an additional alert condition
                    System.Windows.Forms.MessageBox.Show(exLog.ToString(), "Error writing to the event log", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        
            // Tell the user that the event and read the response.
            System.Windows.Forms.MessageBox.Show(Args.MessageText, Args.Caption, Args.Buttons, Args.Icon, Args.DefaultButton);
        }
    }
}