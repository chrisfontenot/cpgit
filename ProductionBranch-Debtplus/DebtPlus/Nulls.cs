﻿using System;

namespace DebtPlus
{
    // Pass-thru facade to DebtPlus.Utils.Nulls
    public static class Nulls
    {
        public static Decimal DDec(object obj) { return Utils.Nulls.DDec(obj); }
        public static Decimal DDec(object obj, decimal defaultValue) { return Utils.Nulls.DDec(obj, defaultValue); }
        public static Int32 DInt(object obj) { return Utils.Nulls.DInt(obj); }
        public static Int32 DInt(object obj, Int32 defaultValue) { return Utils.Nulls.DInt(obj, defaultValue); }
        public static Int64 DLng(object obj) { return Utils.Nulls.DLng(obj); }
        public static Int64 DLng(object obj, Int64 defaultValue) { return Utils.Nulls.DLng(obj, defaultValue); }
        public static String DStr(object obj) { return Utils.Nulls.DStr(obj);  }
        public static String DStr(object obj, string defaultValue) { return Utils.Nulls.DStr(obj, defaultValue); }
        public static Double DDbl(object obj) { return Utils.Nulls.DDbl(obj);  }
        public static Double DDbl(object obj, double defaultValue) { return Utils.Nulls.DDbl(obj, defaultValue); }
        public static Boolean DBool(object obj) { return Utils.Nulls.DBool(obj); }
        public static Boolean DBool(object obj, bool defaultValue) { return Utils.Nulls.DBool(obj, defaultValue); }
    }
}
