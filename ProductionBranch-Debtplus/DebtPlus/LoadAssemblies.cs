﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus
{
    public static class LoadAssemblies
    {
        /// <summary>
        /// Force all referenced assemblies to be loaded into the current process. This is used by the
        /// Desktop process to ensure that the referenced types are able to be located. Of course, it slows
        /// down the loading of the desktop to force all possible referenes to be resolved, but it was slow
        /// in the first place.
        /// </summary>
        public static void ForceAllToLoad()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            var loadedPaths = loadedAssemblies.Select(a => a.Location).ToArray();
            var referencedPaths = System.IO.Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
            var toLoad = referencedPaths.Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase)).ToList();

            try
            {
                toLoad.ForEach(path => loadedAssemblies.Add(AppDomain.CurrentDomain.Load(System.Reflection.AssemblyName.GetAssemblyName(path))));
            }
            catch { }
        }
    }
}
