using System;

namespace DebtPlus.Storage
{
    public interface IDatabaseInfo : IDisposable
    {
        string ConnectionString { get; set; }
        Int32 CommandTimeout { get; set; }

        System.Data.SqlClient.SqlConnection CreateNewConnection();
        System.Data.SqlClient.SqlCommand CreateNewCommand();
        System.Data.SqlClient.SqlCommand CreateNewCommand(System.Data.SqlClient.SqlConnection cn);
        System.Data.SqlClient.SqlCommand CreateNewCommand(string commandText);
        System.Data.SqlClient.SqlCommand CreateNewCommand(System.Data.SqlClient.SqlConnection cn, string commandText);
    }
}
