using System.Data.SqlClient;

namespace DebtPlus.Storage
{
    public class SqlDatabaseInfo : IDatabaseInfo
    {
        public string ConnectionString { get; set; }
        public int CommandTimeout { get; set; }

        public SqlDatabaseInfo() {}

        public SqlDatabaseInfo(IDatabaseInfo databaseInfo)
        {
            ConnectionString = databaseInfo.ConnectionString;
            CommandTimeout = databaseInfo.CommandTimeout;
        }

        public SqlConnection CreateNewConnection() { return new SqlConnection(ConnectionString); }

        public SqlCommand CreateNewCommand()                                        { return CreateNewCommand(null, string.Empty); }
        public SqlCommand CreateNewCommand(SqlConnection cn)                        { return CreateNewCommand(cn, string.Empty); }
        public SqlCommand CreateNewCommand(string commandText)                      { return CreateNewCommand(null, commandText); }
        public SqlCommand CreateNewCommand(SqlConnection cn, string commandText)    { return new SqlCommand() { Connection = cn, CommandText = commandText, CommandTimeout = CommandTimeout }; }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}
