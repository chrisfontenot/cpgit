using System;
using System.Collections.Generic;
using System.Text;
using Excel.Core.OpenXmlFormat;
using System.IO;
using Excel.Core;
using System.Data;
using System.Xml;
using System.Globalization;

namespace Excel
{
	public class ExcelOpenXmlReader : IExcelDataReader
	{
		#region Members

		private XlsxWorkbook _workbook;
		private bool _isValid;
		private bool _isClosed;
		private bool _isFirstRead;
		private string _exceptionMessage;
		private int _depth;
		private int _resultIndex;
		private int _emptyRowCount;
		private ZipWorker _zipWorker;
		private XmlReader _xmlReader;
		private Stream _sheetStream;
		private object[] _cellsValues;
		private object[] _savedCellsValues;

		private bool disposed;
		private bool _isFirstRowAsColumnNames;
		private const string COLUMN = "Column";

		private List<int> _defaultDateTimeStyles;
		#endregion

        /// <summary>
        /// 
        /// </summary>
		internal ExcelOpenXmlReader()
		{
			_isValid = true;
			_isFirstRead = true;

			_defaultDateTimeStyles = new List<int>(new int[] 
			{
				14, 15, 16, 17, 18, 19, 20, 21, 22, 45, 46, 47
			});
		}

        /// <summary>
        /// 
        /// </summary>
		private void ReadGlobals()
		{
			_workbook = new XlsxWorkbook(
				_zipWorker.GetWorkbookStream(),
				_zipWorker.GetWorkbookRelsStream(),
				_zipWorker.GetSharedStringsStream(),
				_zipWorker.GetStylesStream());

			CheckDateTimeNumFmts(_workbook.Styles.NumFmts);

		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
		private void CheckDateTimeNumFmts(List<XlsxNumFmt> list)
		{
			if (list.Count == 0) return;

			foreach (XlsxNumFmt numFmt in list)
			{
				if (string.IsNullOrEmpty(numFmt.FormatCode)) continue;
				string fc = numFmt.FormatCode;

				int pos;
				while ((pos = fc.IndexOf('"')) > 0)
				{
					int endPos = fc.IndexOf('"', pos + 1);

					if (endPos > 0) fc = fc.Remove(pos, endPos - pos + 1);
				}

				if (fc.IndexOfAny(new char[] { 'y', 'm', 'd', 's', 'h' }) >= 0)
				{
					_defaultDateTimeStyles.Add(numFmt.Id);
				}
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheet"></param>
		private void ReadSheetGlobals(XlsxWorksheet sheet)
		{
			_sheetStream = _zipWorker.GetWorksheetStream(sheet.Path);

			if (null == _sheetStream) return;

			_xmlReader = XmlReader.Create(_sheetStream);

			while (_xmlReader.Read())
			{
				if (_xmlReader.NodeType == XmlNodeType.Element && _xmlReader.Name == XlsxWorksheet.N_dimension)
				{
					string dimValue = _xmlReader.GetAttribute(XlsxWorksheet.A_ref);

					if (dimValue.IndexOf(':') > 0)
					{
						sheet.Dimension = new XlsxDimension(dimValue);
					}
					else
					{
						_xmlReader.Close();
						_sheetStream.Close();
					}

					break;
				}
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns></returns>
		private bool ReadSheetRow(XlsxWorksheet sheet)
		{
			if (null == _xmlReader) return false;

			if (_emptyRowCount != 0)
			{
				_cellsValues = new object[sheet.ColumnsCount];
				_emptyRowCount--;
				_depth++;

				return true;
			}

			if (_savedCellsValues != null)
			{
				_cellsValues = _savedCellsValues;
				_savedCellsValues = null;
				_depth++;

				return true;
			}

            if ((_xmlReader.NodeType == XmlNodeType.Element && _xmlReader.Name == XlsxWorksheet.N_row) ||
                _xmlReader.ReadToFollowing(XlsxWorksheet.N_row))
			{
				_cellsValues = new object[sheet.ColumnsCount];

				int rowIndex = int.Parse(_xmlReader.GetAttribute(XlsxWorksheet.A_r));
				if (rowIndex != (_depth + 1))
				{
					_emptyRowCount = rowIndex - _depth - 1;
				}
				bool hasValue = false;
				string a_s = String.Empty;
				string a_t = String.Empty;
				string a_r = String.Empty;
				int col = 0;
				int row = 0;

 				while (_xmlReader.Read())
 				{
 					if (_xmlReader.Depth == 2) break;
 
					if (_xmlReader.NodeType == XmlNodeType.Element)
 					{
						hasValue = false;
 
						if (_xmlReader.Name == XlsxWorksheet.N_c)
						{
							a_s = _xmlReader.GetAttribute(XlsxWorksheet.A_s);
							a_t = _xmlReader.GetAttribute(XlsxWorksheet.A_t);
							a_r = _xmlReader.GetAttribute(XlsxWorksheet.A_r);
							XlsxDimension.XlsxDim(a_r, out col, out row);
						}
						else if (_xmlReader.Name == XlsxWorksheet.N_v)
						{
							hasValue = true;
						}
					}
 
					if (_xmlReader.NodeType == XmlNodeType.Text && hasValue)
					{
						object o = _xmlReader.Value;
 
 						if (null != a_t && a_t == XlsxWorksheet.A_s)
 						{
							o = _workbook.SST[Convert.ToInt32(o)];
						}
						else if (null != a_s)
						{
							XlsxXf xf = _workbook.Styles.CellXfs[int.Parse(a_s)];

							if (xf.ApplyNumberFormat && IsDateTimeStyle(xf.NumFmtId) && o != null && o.ToString() != string.Empty)
							{
								o = DateTime.FromOADate(Convert.ToDouble(o, CultureInfo.InvariantCulture));
							}
						}

						if (col - 1 < _cellsValues.Length)
							_cellsValues[col - 1] = o;
					}
				}

				if (_emptyRowCount > 0)
				{
					_savedCellsValues = _cellsValues;
					return ReadSheetRow(sheet);
				}
				else
					_depth++;

				return true;
			}

			_xmlReader.Close();
			if (_sheetStream != null) _sheetStream.Close();

			return false;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		private bool InitializeSheetRead()
		{
			if (ResultsCount <= 0) return false;

			ReadSheetGlobals(_workbook.Sheets[_resultIndex]);

			if (_workbook.Sheets[_resultIndex].Dimension == null) return false;

			_isFirstRead = false;

			_depth = 0;
			_emptyRowCount = 0;

			return true;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="styleId"></param>
        /// <returns></returns>
		private bool IsDateTimeStyle(int styleId)
		{
			return _defaultDateTimeStyles.Contains(styleId);
		}


		#region IExcelDataReader Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileStream"></param>
		public void Initialize(System.IO.Stream fileStream)
		{
			_zipWorker = new ZipWorker();
			_zipWorker.Extract(fileStream);

			if (!_zipWorker.IsValid)
			{
				_isValid = false;
				_exceptionMessage = _zipWorker.ExceptionMessage;

				Close();

				return;
			}

			ReadGlobals();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public System.Data.DataSet AsDataSet()
		{
			return AsDataSet(true);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="convertOADateTime"></param>
        /// <returns></returns>
		public System.Data.DataSet AsDataSet(bool convertOADateTime)
		{
			if (!_isValid) return null;

			DataSet dataset = new DataSet();

			for (_resultIndex = 0; _resultIndex < _workbook.Sheets.Count; _resultIndex++)
			{
				DataTable table = new DataTable(_workbook.Sheets[_resultIndex].Name);

				ReadSheetGlobals(_workbook.Sheets[_resultIndex]);

				if (_workbook.Sheets[_resultIndex].Dimension == null) continue;

				_depth = 0;
				_emptyRowCount = 0;

				//DataTable columns
				if (!_isFirstRowAsColumnNames)
				{
					for (int i = 0; i < _workbook.Sheets[_resultIndex].ColumnsCount; i++)
					{
						table.Columns.Add();
					}
				}
				else if (ReadSheetRow(_workbook.Sheets[_resultIndex]))
				{
					for (int index = 0; index < _cellsValues.Length; index++)
					{
						if (_cellsValues[index] != null && _cellsValues[index].ToString().Length > 0)
							table.Columns.Add(_cellsValues[index].ToString());
						else
							table.Columns.Add(string.Concat(COLUMN, index));
					}
				}
				else continue;

				while (ReadSheetRow(_workbook.Sheets[_resultIndex]))
				{
					table.Rows.Add(_cellsValues);
				}

				if (table.Rows.Count > 0)
					dataset.Tables.Add(table);
			}

			return dataset;
		}

        /// <summary>
        /// 
        /// </summary>
		public bool IsFirstRowAsColumnNames
		{
			get
			{
				return _isFirstRowAsColumnNames;
			}
			set
			{
				_isFirstRowAsColumnNames = value;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public bool IsValid
		{
			get { return _isValid; }
		}

        /// <summary>
        /// 
        /// </summary>
		public string ExceptionMessage
		{
			get { return _exceptionMessage; }
		}

        /// <summary>
        /// 
        /// </summary>
		public string Name
		{
			get
			{
				return (_resultIndex >= 0 && _resultIndex < ResultsCount) ? _workbook.Sheets[_resultIndex].Name : null;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public void Close()
		{
			_isClosed = true;

			if (_xmlReader != null) _xmlReader.Close();

			if (_sheetStream != null) _sheetStream.Close();

			if (_zipWorker != null) _zipWorker.Dispose();
		}

        /// <summary>
        /// 
        /// </summary>
		public int Depth
		{
			get { return _depth; }
		}

        /// <summary>
        /// 
        /// </summary>
		public int ResultsCount
		{
			get { return _workbook == null ? -1 : _workbook.Sheets.Count; }
		}

		public bool IsClosed
		{
			get { return _isClosed; }
		}

		public bool NextResult()
		{
			if (_resultIndex >= (this.ResultsCount - 1)) return false;

			_resultIndex++;

			_isFirstRead = true;

			return true;
		}

        /// <summary>
        /// 
        /// </summary>
        public bool Read()
		{
			if (!_isValid) return false;

			if (_isFirstRead && !InitializeSheetRead())
			{
				return false;
			}

			return ReadSheetRow(_workbook.Sheets[_resultIndex]);
		}

        /// <summary>
        /// 
        /// </summary>
		public int FieldCount
		{
			get { return (_resultIndex >= 0 && _resultIndex < ResultsCount) ? _workbook.Sheets[_resultIndex].ColumnsCount : -1; }
		}

        /// <summary>
        /// 
        /// </summary>
        public bool GetBoolean(int i)
		{
			if (IsDBNull(i)) return false;

			return Boolean.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public DateTime GetDateTime(int i)
		{
			if (IsDBNull(i)) return DateTime.MinValue;

			try
			{
				return (DateTime)_cellsValues[i];
			}
			catch (InvalidCastException)
			{
				return DateTime.MinValue;
			}

		}

        /// <summary>
        /// 
        /// </summary>
        public decimal GetDecimal(int i)
		{
			if (IsDBNull(i)) return decimal.MinValue;

			return decimal.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public double GetDouble(int i)
		{
			if (IsDBNull(i)) return double.MinValue;

			return double.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public float GetFloat(int i)
		{
			if (IsDBNull(i)) return float.MinValue;

			return float.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public short GetInt16(int i)
		{
			if (IsDBNull(i)) return short.MinValue;

			return short.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public int GetInt32(int i)
		{
			if (IsDBNull(i)) return int.MinValue;

			return int.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public long GetInt64(int i)
		{
			if (IsDBNull(i)) return long.MinValue;

			return long.Parse(_cellsValues[i].ToString());
		}

        /// <summary>
        /// 
        /// </summary>
        public string GetString(int i)
		{
			if (IsDBNull(i)) return null;

			return _cellsValues[i].ToString();
		}

        /// <summary>
        /// 
        /// </summary>
        public object GetValue(int i)
		{
			return _cellsValues[i];
		}

        /// <summary>
        /// 
        /// </summary>
        public bool IsDBNull(int i)
		{
			return (null == _cellsValues[i]) || (DBNull.Value == _cellsValues[i]);
		}

        /// <summary>
        /// 
        /// </summary>
        public object this[int i]
		{
			get { return _cellsValues[i]; }
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);

			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.disposed)
			{
				if (disposing)
				{
					if (_zipWorker != null) _zipWorker.Dispose();
					if (_xmlReader != null) _xmlReader.Close();
					if (_sheetStream != null) _sheetStream.Close();
				}

				_zipWorker = null;
				_xmlReader = null;
				_sheetStream = null;

				_workbook = null;
				_cellsValues = null;
				_savedCellsValues = null;

				disposed = true;
			}
		}

		~ExcelOpenXmlReader()
		{
			Dispose(false);
		}

		#endregion

		#region  Not Supported IDataReader Members


		public DataTable GetSchemaTable()
		{
			throw new NotSupportedException();
		}

		public int RecordsAffected
		{
			get { throw new NotSupportedException(); }
		}

		#endregion

		#region Not Supported IDataRecord Members


		public byte GetByte(int i)
		{
			throw new NotSupportedException();
		}

		public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			throw new NotSupportedException();
		}

		public char GetChar(int i)
		{
			throw new NotSupportedException();
		}

		public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
		{
			throw new NotSupportedException();
		}

		public IDataReader GetData(int i)
		{
			throw new NotSupportedException();
		}

		public string GetDataTypeName(int i)
		{
			throw new NotSupportedException();
		}

		public Type GetFieldType(int i)
		{
			throw new NotSupportedException();
		}

		public Guid GetGuid(int i)
		{
			throw new NotSupportedException();
		}

		public string GetName(int i)
		{
			throw new NotSupportedException();
		}

		public int GetOrdinal(string name)
		{
			throw new NotSupportedException();
		}

		public int GetValues(object[] values)
		{
			throw new NotSupportedException();
		}

		public object this[string name]
		{
			get { throw new NotSupportedException(); }
		}

		#endregion

	}
}
