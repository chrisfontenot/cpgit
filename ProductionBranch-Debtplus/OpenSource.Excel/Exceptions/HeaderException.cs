using System;

namespace Excel.Exceptions
{
    /// <summary>
    /// 
    /// </summary>
	public class HeaderException : Exception
	{
        /// <summary>
        /// 
        /// </summary>
		public HeaderException()
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
		public HeaderException(string message)
			: base(message)
		{
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
		public HeaderException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}
}
