using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ExcelDataReader")]
[assembly: AssemblyDescription("Read Microsoft Excel spreadsheets for .NET programs")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("ExcelDataReader")]
[assembly: AssemblyCopyright("Copyright © 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Allow non-signed programs to call us
[assembly: SecurityRules(SecurityRuleSet.Level1)]
[assembly: SecurityTransparent()]
// [assembly: AllowPartiallyTrustedCallers()]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b9191c7c-887c-4482-b7fd-36bafa138528")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("17.4.20.0227")]
[assembly: AssemblyFileVersion("17.4.20.0227")]
[assembly: AssemblyInformationalVersion("17.4.20.0227")]
