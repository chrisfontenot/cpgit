using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using System.Windows.Forms;
using Microsoft.Win32;
using DebtPlus.Utils;
using System.Xml.Serialization;
using System.Xml;
using DevExpress.XtraSpellChecker;

namespace DebtPlus.UI.Common
{
    public class SpellCheckerDictionaries
    {
        private SpellCheckerDictionaries()
        {
            // can't create this static class
        }

        /// <summary>
        /// Define the dictionaries for the current speller
        /// </summary>
        public static void SetSpellCheckerDictionaries(ref SharedDictionaryStorage ctl)
        {
            try
            {
                LoadCustomWords(ref ctl);
                LoadLanguageDictionaries(ref ctl);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error loading spelling dictionaries");
            }
        }

        /// <summary>
        /// Define the custom word list dictionaries
        /// </summary>
        private static void LoadCustomWords(ref SharedDictionaryStorage ctl)
        {
            if (!LoadOfficeWords(ref ctl))
            {
                LoadDefaultWords(ref ctl);
            }
        }

        /// <summary>
        /// Define the Microsoft Office custom word list dictionaries
        /// </summary>
        private static bool LoadOfficeWords(ref SharedDictionaryStorage ctl)
        {
            string pathName = null;
            bool answer = false;

            //-- Use the registry location to try to find the custom dictionaries for "Microsoft Word". If you don't have
            //-- word installed, well too bad ....
            RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                reg = reg.OpenSubKey(@"Software\Microsoft\Shared Tools\Proofing Tools\Custom Dictionaries", false);

                if (reg != null)
                {
                    //-- We choose the first item. You can have more than one, but the first item is all that we use.
                    string[] names = reg.GetValueNames();
                    foreach (string keyName in names)
                    {
                        string fileName = Convert.ToString(reg.GetValue(keyName));

                        if (!Path.IsPathRooted(fileName))
                        {
                            string applicationDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                            pathName = Path.Combine(applicationDataFolder, @"Microsoft\Proof");
                            fileName = Path.Combine(pathName, fileName);
                        }

                        LoadCustomWordList(ref ctl, fileName);
                        answer = true;
                    }
                }
            }

            finally
            {
                if (reg != null)
                {
                    reg.Close();
                }
            }

            return answer;
        }

/// <summary>
/// Define the custom word list dictionary in the Documents directory
/// </summary>

        private static void LoadDefaultWords(ref SharedDictionaryStorage ctl)
        {
            //-- Create a path to the location documents directory
            string PathName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string FileName = Path.Combine(PathName, "Custom Spelling Word List.txt");

            //-- Create the file if it does not exist
            if (!File.Exists(FileName))
            {
                FileStream sr = null;
                try
                {
                    sr = File.Create(FileName);
                }

                catch (IOException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                        sr.Dispose();
                    }
                }
            }

            //-- Create the dictionary against the file
            CultureInfo CurrentCulture = CultureInfo.CurrentCulture;
            SpellCheckerCustomDictionary Dictionary = new SpellCheckerCustomDictionary(FileName, CurrentCulture);
            ctl.Dictionaries.Add(Dictionary);
        }

/// <summary>
/// Create dictionary object for a custom word list file
/// </summary>

        private static void LoadCustomWordList(ref SharedDictionaryStorage ctl, string FileName)
        {
            //-- Ensure that the path exists to the file
            string PathName = Path.GetDirectoryName(FileName);
            if (!Directory.Exists(PathName))
            {
                Directory.CreateDirectory(PathName);
            }

            //-- Create the file if it does not exist in the directory
            if (!File.Exists(FileName))
            {
                FileStream sr = null;
                try
                {
                    sr = File.Create(FileName);

                }
                catch (IOException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                }
                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                        sr.Dispose();
                    }
                }
            }

            //-- Create the dictionary against the file
            CultureInfo CurrentCulture = CultureInfo.CurrentCulture;
            SpellCheckerCustomDictionary Dictionary = new SpellCheckerCustomDictionary(FileName, CurrentCulture);
            ctl.Dictionaries.Add(Dictionary);
        }

        /// <summary>
        /// Load the standard list of dictionary words
        /// </summary>
        private static void LoadLanguageDictionaries(ref SharedDictionaryStorage ctl)
        {
            string CommonFolderName = Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles);
            string PathName = Path.Combine(CommonFolderName, @"DebtPlus\Dictionaries");
            if (Directory.Exists(PathName))
            {
                string[] Folders = Directory.GetDirectories(PathName);
                foreach (string folder in Folders)
                {
                    LoadLanguageDictionary(ref ctl, folder);
                }
            }
        }

        /// <summary>
        /// Load a specific dictionary word list
        /// </summary>
        private static void LoadLanguageDictionary(ref SharedDictionaryStorage ctl, string FolderName)
        {
            Dictionary Dict = ReadDictionaryObject(FolderName);

            if (Dict.Enabled)
            {
                //-- For now, we only support Open Office formats
                if (string.Compare(Dict.Format, "OpenOffice", true) != 0)
                {
                    throw new ApplicationException("Only OPENOFFICE dictionary formats are supported at this time");
                }

                //-- Create the spelling dictionary object and set the culture
                SpellCheckerOpenOfficeDictionary LanguageDictionary = new SpellCheckerOpenOfficeDictionary();
                LanguageDictionary.Culture = new CultureInfo(Dict.Culture);

                Stream dicStream = null;
                Stream affStream = null;
                try
                {
                    dicStream = Dict.GetDICStream(FolderName);
                    affStream = Dict.GetAFFStream(FolderName);
                    LanguageDictionary.LoadFromStream(dicStream, affStream, null);
                    ctl.Dictionaries.Add(LanguageDictionary);
                }

                finally
                {
                    if (affStream != null)
                    {
                        affStream.Dispose();
                    }

                    if (dicStream != null)
                    {
                        dicStream.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Read the Dictionary.xml file and generate a Dictionary object
        /// </summary>
        private static Dictionary ReadDictionaryObject(string PathName)
        {
            Dictionary answer = null;
            string FileName = Path.Combine(PathName, "Dictionary.xml");

            if (File.Exists(FileName))
            {
                using (var fs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
                {
                    try
                    {
                        System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(Dictionary));
                        answer = (Dictionary)ser.Deserialize(fs);
                    }

#pragma warning disable 168
                    catch (XmlException ex)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show(string.Format("The file {0} is not a valid XML document", FileName), "Error loading dictionary objects", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
#pragma warning restore 168
                }
            }

            if (answer == null)
            {
                answer = DefaultDictionary(PathName);
            }

            return answer;
        }

        /// <summary>
        /// Create a default dictionary object from the path files
        /// </summary>
        private static Dictionary DefaultDictionary(string PathName)
        {
            Dictionary answer = new Dictionary();

            //-- Look for the files in the directory to fill in the defaults
            string[] Files = Directory.GetFiles(PathName, "*.aff");
            if (Files.GetUpperBound(0) >= 0)
            {
                answer.aff = Path.GetFileName(Files[0]);
            }

            Files = Directory.GetFiles(PathName, "*.dic");
            if (Files.GetUpperBound(0) >= 0)
            {
                answer.dic = Path.GetFileName(Files[0]);
            }

            //-- If the files are present then enable the dictionary item
            answer.Enabled = answer.aff != string.Empty && answer.dic != string.Empty;
            return answer;
        }
    }
}
