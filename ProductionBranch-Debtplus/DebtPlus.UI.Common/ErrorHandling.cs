﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Common
{
    public static class ErrorHandling
    {
        /// <summary>
        /// Class of arguments for the error processing dialog
        /// </summary>
        private class Arguments
        {
            public System.Exception ex { get; set; }
            public MessageBoxButtons buttons { get; set; }
            public MessageBoxDefaultButton defaultButton { get; set; }
            public MessageBoxIcon icon { get; set; }

            private string ErrorCaption
            {
                get
                {
                    return ex is System.Data.SqlClient.SqlException ? "Database" : "General";
                }
            }

            /// <summary>
            /// Text to be displayed in the message box. It varies depending upon the error.
            /// </summary>
            public string Text
            {
                get
                {
                    return string.Format("A {0} error occurred:{1}{1}{2}{1}{1}Stack:{1}{1}{3}", ErrorCaption, Environment.NewLine, ex.Message, ex.StackTrace);
                }
            }

            private string privateTitle;
            public string Title
            {
                get
                {
                    if (string.IsNullOrEmpty(privateTitle))
                    {
                        return ErrorCaption + " Error";
                    }
                    return privateTitle;
                }
                set
                {
                    privateTitle = value;
                }
            }

            public Arguments(DebtPlus.Interfaces.IRepositoryResult ErrorResult, string Title, MessageBoxButtons Buttons, MessageBoxDefaultButton DefaultButton, MessageBoxIcon Icon)
            {
                this.ex = ErrorResult.ex;
                this.Title = Title;
                this.buttons = Buttons;
                this.defaultButton = DefaultButton;
                this.icon = Icon;
            }

            public Arguments(System.Exception ex, string Title, MessageBoxButtons Buttons, MessageBoxDefaultButton DefaultButton, MessageBoxIcon Icon)
            {
                this.ex = ex;
                this.Title = Title;
                this.buttons = Buttons;
                this.defaultButton = DefaultButton;
                this.icon = Icon;
            }
        }

#region Obsolete interfaces
        // DO NOT REMOVE THESE INTERFACES. THE REPORT REPX FILES USE THEM AND UNTIL ALL OF THE REPORTS ARE CHANGED WE MUST KEEP THESE ROUTINES!!!!

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult)
        {
            return HandleErrors(new Arguments(ErrorResult, null, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult, string Title, MessageBoxButtons Buttons)
        {
            return HandleErrors(new Arguments(ErrorResult, Title, Buttons, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult, string Title, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return HandleErrors(new Arguments(ErrorResult, Title, Buttons, MessageBoxDefaultButton.Button1, Icon));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult, string Title, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton DefaultButton)
        {
            return HandleErrors(new Arguments(ErrorResult, Title, Buttons, DefaultButton, Icon));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(DebtPlus.Interfaces.IRepositoryResult ErrorResult, string Title)
        {
            return HandleErrors(new Arguments(ErrorResult, Title, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }
#endregion

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(System.Exception ex)
        {
            return HandleErrors(new Arguments(ex, null, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(System.Exception ex, string Title, MessageBoxButtons Buttons)
        {
            return HandleErrors(new Arguments(ex, Title, Buttons, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(System.Exception ex, string Title, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return HandleErrors(new Arguments(ex, Title, Buttons, MessageBoxDefaultButton.Button1, Icon));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(System.Exception ex, string Title, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton DefaultButton)
        {
            return HandleErrors(new Arguments(ex, Title, Buttons, DefaultButton, Icon));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        public static System.Windows.Forms.DialogResult HandleErrors(System.Exception ex, string errorCaption)
        {
            return HandleErrors(new Arguments(ex, errorCaption, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxIcon.Error));
        }

        /// <summary>
        /// Handle the error conditions for the UI layer. This will interact with the user.
        /// </summary>
        private static System.Windows.Forms.DialogResult HandleErrors(Arguments args)
        {
            try
            {
                // Generate the error condition for the operation
                System.Exception ex = args.ex;
                if (ex == null)
                {
                    return DialogResult.OK;
                }

                // Go to the Service layer. It may log the event.
                // If the result is 0 (None) then go to the UI to display the error.
                Int32 resultSvc = DebtPlus.Svc.Common.ErrorHandling.HandleErrors(args.ex);
                if (resultSvc == 0)
                {
                    return DebtPlus.Data.Forms.MessageBox.Show(args.Text, args.Title, args.buttons, args.icon, args.defaultButton);
                }

                // The result is not "None", but we need to show it to the user anyway. Do that here and return the original result.
                DebtPlus.Data.Forms.MessageBox.Show(args.Text, args.Title, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                return (System.Windows.Forms.DialogResult)resultSvc;
            }

            // Do not do anything with an error. Force the return to be simply OK.
            catch
            {
                return DialogResult.OK;
            }
        }
    }
}

