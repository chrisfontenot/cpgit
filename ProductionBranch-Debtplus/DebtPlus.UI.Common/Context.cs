using System.Data;

// This class is a design solution for unifying the separate apps into one executable.  It defines a
// data context equivalent that was present in the individual apps. Specifically, each app had a 
// static (aka 'Shared') System.Data.DataSet that was used globally among all of the forms in the app.
// Since we can't have a global data context in a unified executable, we need to mimic the global data
// space. Hence this class.
//
// The burden this imposes on the executable is that each module's MainForm must instantiate a Context
// object, then pass it to each form.  Any form that has subforms, but also pass the context. Because
// the context maintains a 'Unit of Context' for the module, we can open multiple instances of the module
// without having any data collisions between module contexts.  
//
// Note that this is the UI-layer resolution for multiple data contexts. It does not address any issues
// regarding data-layer concurrency - such as what might be seen if we open up one database object in 
// two different module contexts, edit the module in each context, then try to save each instance of the
// object back to the database. That issue is appropriately handled in the Repository layer.

namespace DebtPlus.UI.Common
{
    public class Context : System.IDisposable
    {
        public DataSet ds = new DataSet("ds");

        #region IDisposable Support
        private static bool IsDisposed = false;

        /// <summary>
        /// Dispose of the locally allocated storage
        /// </summary>
        /// <param name="Disposing">TRUE if Dispose. False if Finalize</param>
        protected virtual void Dispose(bool Disposing)
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                if (Disposing)
                {
                    ds.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalize the storage removal.
        /// </summary>
        ~Context()
        {
            Dispose(false);
        }
        #endregion
    }
}
