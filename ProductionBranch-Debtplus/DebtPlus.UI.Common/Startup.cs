using System.Windows.Forms;
using DevExpress.Data;

namespace DebtPlus.UI.Common
{
    public static class Startup
    {
        /// <summary>
        /// Register the top level application to the system. It is expected to be called by the
        /// application startup routine as it will perform the common DebtPlus functions for a new
        /// application.
        /// </summary>
        /// <param name="Arguments">The list of argument strings as passed to the application</param>
        public static void Register(string[] Arguments)
        {
            // Enable the visual style processing logic
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Ensure that the skinning engine is enabled
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();

            // Suppress the trap event if we update a table between two threads. It is expected.
            // we have one thread read the table completely and terminate while another one update rows after the table has been read.
#pragma warning disable 618
            DevExpress.Data.CurrencyDataController.DisableThreadingProblemsDetection = true;
#pragma warning restore 618
        }
    }
}
